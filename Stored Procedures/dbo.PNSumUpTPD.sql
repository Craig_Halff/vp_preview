SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PNSumUpTPD]
  @strPlanID VARCHAR(32),
  @strCalcLab VARCHAR(1) = 'Y',
  @strCalcExp VARCHAR(1) = 'Y',
  @strCalcCon VARCHAR(1) = 'Y'
AS

BEGIN -- Procedure PNSumUpTPD

  SET NOCOUNT ON

  DECLARE @strAccordionFormatID varchar(32)

  DECLARE @intOutlineLevel integer

  DECLARE @intHrDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intLabRevDecimals int
  DECLARE @intECURevDecimals int
  
  DECLARE @intPlannedLabCount int
  DECLARE @intPlannedExpCount int
  DECLARE @intPlannedConCount int

  DECLARE @dtMinDate datetime
  DECLARE @dtMaxDate datetime
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get decimal settings.
  
  SELECT @intHrDecimals = 2,
         @intAmtCostDecimals = 4,
         @intAmtBillDecimals = 4,
         @intLabRevDecimals = 4, 
         @intECURevDecimals = 4

 /*
 
  SELECT @intHrDecimals = HrDecimals,
         @intAmtCostDecimals = AmtCostDecimals,
         @intAmtBillDecimals = AmtBillDecimals,
         @intLabRevDecimals = LabRevDecimals, 
         @intECURevDecimals = ECURevDecimals
    FROM dbo.PN$tabDecimals(@strPlanID)

*/

  -- Check to see if there is any time-phased data
  
  IF (@strCalcLab = 'Y') SELECT @intPlannedLabCount = COUNT(*) FROM PNPlannedLabor WHERE PlanID = @strPlanID
  IF (@strCalcExp = 'Y') SELECT @intPlannedExpCount = COUNT(*) FROM PNPlannedExpenses WHERE PlanID = @strPlanID
  IF (@strCalcCon = 'Y') SELECT @intPlannedConCount = COUNT(*) FROM PNPlannedConsultant WHERE PlanID = @strPlanID

 -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find the MIN and MAX dates of all Tasks.
  
  SELECT @dtMinDate = MINDate, @dtMaxDate = MAXDate
    FROM
      (SELECT MIN(StartDate) AS MINDate, MAX(EndDate) AS MAXDate FROM PNTask WHERE PlanID = @strPlanID) AS X

  -- Adjust PNPlan Start/End Dates, if needed

  UPDATE PNPlan SET
    StartDate = CASE WHEN @dtMinDate < StartDate THEN @dtMinDate ELSE StartDate END,
    EndDate = CASE WHEN @dtMaxDate > EndDate THEN @dtMaxDate ELSE EndDate END
    WHERE PlanID = @strPlanID AND
      (@dtMinDate < StartDate OR @dtMaxDate > EndDate)

  -- Adjust PNAccordionFormat Start/End Dates, if needed

  UPDATE PNAccordionFormat SET
    StartDate = CASE WHEN @dtMinDate < StartDate THEN @dtMinDate ELSE StartDate END,
    EndDate = CASE WHEN @dtMaxDate > EndDate THEN @dtMaxDate ELSE EndDate END
    WHERE PlanID = @strPlanID AND
      (@dtMinDate < StartDate OR @dtMaxDate > EndDate)

  -- Need to recalculate the Calendar Intervals.

  IF (@@ROWCOUNT > 0)
    BEGIN
      SELECT @strAccordionFormatID = AccordionFormatID FROM PNAccordionFormat WHERE PlanID = @strPlanID
      EXEC dbo.PNMakeCI @strAccordionFormatID
    END -- END IF (@@ROWCOUNT > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get Maximum OutlineLevel.

  SELECT @intOutlineLevel = ISNULL(MAX(OutlineLevel), -1) FROM PNTask
    WHERE PNTask.PlanID = @strPlanID

  -- Loop through all Task rows starting from the highest OutlineLevel
  -- to insert summed-up time-phased data rows.

  WHILE (@intOutlineLevel >= 0)
    BEGIN

      -- Insert Labor time-phased data rows.

      IF (@strCalcLab = 'Y' AND @intPlannedLabCount > 0)
        INSERT INTO PNPlannedLabor(TimePhaseID, PlanID, TaskID,
          StartDate, EndDate, PeriodHrs, PeriodCost, PeriodBill, PeriodRev,
          CreateDate, ModDate)
        (SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, PlanID AS PlanID, TaskID AS TaskID, 
           MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate, 
           ROUND(ISNULL(SUM(PeriodHrs), 0), @intHrDecimals) AS PeriodHrs, 
           ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals) AS PeriodCost,
           ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals) AS PeriodBill, 
           ROUND(ISNULL(SUM(PeriodRev), 0), @intLabRevDecimals) AS PeriodRev, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
           FROM (SELECT CI.StartDate AS CIStartDate, T.PlanID AS PlanID, T.TaskID AS TaskID, 
                   MIN(TPD.StartDate) AS StartDate, MAX(TPD.EndDate) AS EndDate, 
                   ISNULL(SUM(PeriodHrs), 0) AS PeriodHrs, ISNULL(SUM(PeriodCost), 0) AS PeriodCost,
                   ISNULL(SUM(PeriodBill), 0) AS PeriodBill, ISNULL(SUM(PeriodRev), 0) AS PeriodRev
                   FROM PNCalendarInterval AS CI 
                     LEFT JOIN PNTask AS T ON CI.PlanID = T.PlanID
                     INNER JOIN PNPlannedLabor AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                       AND TPD.StartDate >= CI.StartDate AND TPD.EndDate <= CI.EndDate
                       AND TPD.AssignmentID IS NOT NULL
                   WHERE CI.PlanID = @strPlanID AND TPD.TimePhaseID IS NOT NULL 
                     AND T.OutlineLevel = @intOutlineLevel
                   GROUP BY CI.StartDate, T.TaskID, T.PlanID
                 UNION ALL
                 SELECT CI.StartDate AS CIStartDate, T.PlanID AS PlanID, T.TaskID AS TaskID, 
                   MIN(TPD.StartDate) AS StartDate, MAX(TPD.EndDate) AS EndDate, 
                   ISNULL(SUM(PeriodHrs), 0) AS PeriodHrs, ISNULL(SUM(PeriodCost), 0) AS PeriodCost,
                   ISNULL(SUM(PeriodBill), 0) AS PeriodBill, ISNULL(SUM(PeriodRev), 0) AS PeriodRev
                   FROM PNCalendarInterval AS CI 
                     LEFT JOIN PNTask AS T ON CI.PlanID = T.PlanID
                     INNER JOIN PNTask AS CT ON T.PlanID = CT.PlanID AND T.OutlineNumber = CT.ParentOutlineNumber
                     INNER JOIN PNPlannedLabor AS TPD ON TPD.PlanID = CT.PlanID AND TPD.TaskID = CT.TaskID 
                       AND TPD.StartDate >= CI.StartDate AND TPD.EndDate <= CI.EndDate
                       AND TPD.AssignmentID IS NULL
                   WHERE CI.PlanID = @strPlanID AND TPD.TimePhaseID IS NOT NULL 
                     AND T.ChildrenCount <> 0  AND T.OutlineLevel = @intOutlineLevel
                   GROUP BY T.Name, CI.StartDate, T.TaskID, T.PlanID) AS X
           GROUP BY PlanID, TaskID, CIStartDate)
       
      -- Insert Expense time-phased data rows.

      IF (@strCalcExp = 'Y' AND @intPlannedExpCount > 0)
        INSERT INTO PNPlannedExpenses(TimePhaseID, PlanID, TaskID,
          StartDate, EndDate, PeriodCost, PeriodBill, PeriodRev,
          PeriodCount, PeriodScale, CreateDate, ModDate)
        (SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, PlanID AS PlanID, TaskID AS TaskID, 
           MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate, 
           ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals) AS PeriodCost, 
           ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals) AS PeriodBill, 
           ROUND(ISNULL(SUM(PeriodRev), 0), @intECURevDecimals) AS PeriodRev,
           PeriodCount AS PeriodCount, PeriodScale AS PeriodScale, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
           FROM (SELECT CI.StartDate AS CIStartDate, T.PlanID AS PlanID, T.TaskID AS TaskID, 
                   MIN(TPD.StartDate) AS StartDate, MAX(TPD.EndDate) AS EndDate, 
                   ISNULL(SUM(PeriodCost), 0) AS PeriodCost, ISNULL(SUM(PeriodBill), 0) AS PeriodBill, ISNULL(SUM(PeriodRev), 0) AS PeriodRev,
                   1 AS PeriodCount, CI.PeriodScale AS PeriodScale
                   FROM PNCalendarInterval AS CI LEFT JOIN PNTask AS T ON CI.PlanID = T.PlanID
                   INNER JOIN PNPlannedExpenses AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                     AND TPD.StartDate >= CI.StartDate AND TPD.EndDate <= CI.EndDate
                     AND TPD.ExpenseID IS NOT NULL
                   WHERE CI.PlanID = @strPlanID AND TPD.TimePhaseID IS NOT NULL 
                     AND T.OutlineLevel = @intOutlineLevel
                   GROUP BY CI.StartDate, CI.PeriodScale, T.TaskID, T.PlanID
                 UNION ALL
                 SELECT CI.StartDate AS CIStartDate, T.PlanID AS PlanID, T.TaskID AS TaskID, 
                   MIN(TPD.StartDate) AS StartDate, MAX(TPD.EndDate) AS EndDate, 
                   ISNULL(SUM(PeriodCost), 0) AS PeriodCost, ISNULL(SUM(PeriodBill), 0) AS PeriodBill, ISNULL(SUM(PeriodRev), 0) AS PeriodRev,
                   1 AS PeriodCount, CI.PeriodScale AS PeriodScale
                   FROM PNCalendarInterval AS CI LEFT JOIN PNTask AS T ON CI.PlanID = T.PlanID
                   INNER JOIN PNTask AS CT ON T.PlanID = CT.PlanID AND T.OutlineNumber = CT.ParentOutlineNumber
                   INNER JOIN PNPlannedExpenses AS TPD ON TPD.PlanID = CT.PlanID AND TPD.TaskID = CT.TaskID 
                     AND TPD.StartDate >= CI.StartDate AND TPD.EndDate <= CI.EndDate
                     AND TPD.ExpenseID IS NULL
                   WHERE CI.PlanID = @strPlanID AND TPD.TimePhaseID IS NOT NULL 
                     AND T.ChildrenCount <> 0  AND T.OutlineLevel = @intOutlineLevel
                   GROUP BY T.Name, CI.StartDate, CI.PeriodScale, T.TaskID, T.PlanID) AS X
           GROUP BY PlanID, TaskID, CIStartDate, PeriodCount, PeriodScale)
        
      -- Insert Consultant time-phased data rows.

      IF (@strCalcCon = 'Y' AND @intPlannedConCount > 0)
        INSERT INTO PNPlannedConsultant(TimePhaseID, PlanID, TaskID,
          StartDate, EndDate, PeriodCost, PeriodBill, PeriodRev,
          PeriodCount, PeriodScale, CreateDate, ModDate)
        (SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, PlanID AS PlanID, TaskID AS TaskID, 
           MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate, 
           ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals) AS PeriodCost, 
           ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals) AS PeriodBill, 
           ROUND(ISNULL(SUM(PeriodRev), 0), @intECURevDecimals) AS PeriodRev,
           PeriodCount AS PeriodCount, PeriodScale AS PeriodScale, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
           FROM (SELECT CI.StartDate AS CIStartDate, T.PlanID AS PlanID, T.TaskID AS TaskID, 
                   MIN(TPD.StartDate) AS StartDate, MAX(TPD.EndDate) AS EndDate, 
                   ISNULL(SUM(PeriodCost), 0) AS PeriodCost, ISNULL(SUM(PeriodBill), 0) AS PeriodBill, ISNULL(SUM(PeriodRev), 0) AS PeriodRev,
                   1 AS PeriodCount, CI.PeriodScale AS PeriodScale
                   FROM PNCalendarInterval AS CI LEFT JOIN PNTask AS T ON CI.PlanID = T.PlanID
                   INNER JOIN PNPlannedConsultant AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                     AND TPD.StartDate >= CI.StartDate AND TPD.EndDate <= CI.EndDate
                     AND TPD.ConsultantID IS NOT NULL
                   WHERE CI.PlanID = @strPlanID AND TPD.TimePhaseID IS NOT NULL 
                     AND T.OutlineLevel = @intOutlineLevel
                   GROUP BY CI.StartDate, CI.PeriodScale, T.TaskID, T.PlanID
                 UNION ALL
                 SELECT CI.StartDate AS CIStartDate, T.PlanID AS PlanID, T.TaskID AS TaskID, 
                   MIN(TPD.StartDate) AS StartDate, MAX(TPD.EndDate) AS EndDate, 
                   ISNULL(SUM(PeriodCost), 0) AS PeriodCost, ISNULL(SUM(PeriodBill), 0) AS PeriodBill, ISNULL(SUM(PeriodRev), 0) AS PeriodRev,
                   1 AS PeriodCount, CI.PeriodScale AS PeriodScale
                   FROM PNCalendarInterval AS CI LEFT JOIN PNTask AS T ON CI.PlanID = T.PlanID
                   INNER JOIN PNTask AS CT ON T.PlanID = CT.PlanID AND T.OutlineNumber = CT.ParentOutlineNumber
                   INNER JOIN PNPlannedConsultant AS TPD ON TPD.PlanID = CT.PlanID AND TPD.TaskID = CT.TaskID 
                     AND TPD.StartDate >= CI.StartDate AND TPD.EndDate <= CI.EndDate
                     AND TPD.ConsultantID IS NULL
                   WHERE CI.PlanID = @strPlanID AND TPD.TimePhaseID IS NOT NULL 
                     AND T.ChildrenCount <> 0  AND T.OutlineLevel = @intOutlineLevel
                   GROUP BY T.Name, CI.StartDate, CI.PeriodScale, T.TaskID, T.PlanID) AS X
           GROUP BY PlanID, TaskID, CIStartDate, PeriodCount, PeriodScale)

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++   
           
      -- Update summary Task rows.
    
      UPDATE PNTask
        SET PlannedLaborHrs = TPD.PlannedLaborHrs,
            PlannedLabCost = TPD.PlannedLabCost,
            PlannedLabBill = TPD.PlannedLabBill,
            LabRevenue = TPD.LabRevenue,
            PlannedExpCost = TPD.PlannedExpCost,
            PlannedExpBill = TPD.PlannedExpBill,
            ExpRevenue = TPD.ExpRevenue,
            PlannedConCost = TPD.PlannedConCost,
            PlannedConBill = TPD.PlannedConBill,
            ConRevenue = TPD.ConRevenue,
            PlannedDirExpCost = TPD.PlannedDirExpCost,
            PlannedDirExpBill = TPD.PlannedDirExpBill,
            PlannedDirConCost = TPD.PlannedDirConCost,
            PlannedDirConBill = TPD.PlannedDirConBill,
            StartDate = ISNULL(TPD.StartDate, T.StartDate),
            EndDate = ISNULL(TPD.EndDate, T.EndDate)
        FROM PNTask AS T INNER JOIN
          (SELECT XT.PlanID, XT.TaskID,
                  ROUND(SUM(ISNULL(LabHrs, 0)), @intHrDecimals) AS PlannedLaborHrs,
                  ROUND(SUM(ISNULL(LabCost, 0)), @intAmtCostDecimals) AS PlannedLabCost,
                  ROUND(SUM(ISNULL(LabBill, 0)), @intAmtBillDecimals)AS PlannedLabBill,
                  ROUND(SUM(ISNULL(LabRev, 0)), @intLabRevDecimals) AS LabRevenue,
                  ROUND(SUM(ISNULL(ExpCost, 0)), @intAmtCostDecimals) AS PlannedExpCost,
                  ROUND(SUM(ISNULL(ExpBill, 0)), @intAmtBillDecimals) AS PlannedExpBill,
                  ROUND(SUM(ISNULL(ExpRev, 0)), @intECURevDecimals) AS ExpRevenue,
                  ROUND(SUM(ISNULL(ConCost, 0)), @intAmtCostDecimals) AS PlannedConCost,
                  ROUND(SUM(ISNULL(ConBill, 0)), @intAmtBillDecimals) AS PlannedConBill,
                  ROUND(SUM(ISNULL(ConRev, 0)), @intECURevDecimals) AS ConRevenue,
                  ROUND(SUM(ISNULL(ExpDirCost, 0)), @intAmtCostDecimals) AS PlannedDirExpCost,
                  ROUND(SUM(ISNULL(ExpDirBill, 0)), @intAmtBillDecimals) AS PlannedDirExpBill,
                  ROUND(SUM(ISNULL(ConDirCost, 0)), @intAmtCostDecimals) AS PlannedDirConCost,
                  ROUND(SUM(ISNULL(ConDirBill, 0)), @intAmtBillDecimals) AS PlannedDirConBill,
                  MIN(XTPD.StartDate) AS StartDate,
                  MAX(XTPD.EndDate) AS EndDate
            FROM PNTask AS XT LEFT JOIN
               (SELECT LT.PlanID AS PlanID, LT.TaskID AS TaskID,
                       SUM(ISNULL(XA.PlannedLaborHrs, 0)) AS LabHrs, 
                       SUM(ISNULL(XA.PlannedLabCost, 0)) AS LabCost, 
                       SUM(ISNULL(XA.PlannedLabBill, 0)) AS LabBill, 
                       SUM(ISNULL(XA.LabRevenue, 0)) AS LabRev,
                       0 AS ExpCost, 0 AS ExpBill, 0 AS ExpRev,
                       0 AS ConCost, 0 AS ConBill, 0 AS ConRev,
                       0 AS ExpDirCost, 0 AS ExpDirBill, 
                       0 AS ConDirCost, 0 AS ConDirBill,
                       MIN(XA.StartDate) AS StartDate,
                       MAX(XA.EndDate) AS EndDate
                  FROM PNTask AS LT LEFT JOIN PNAssignment AS XA 
                    ON LT.PlanID = XA.PlanID AND LT.TaskID = XA.TaskID
                  WHERE LT.PlanID = @strPlanID AND LT.OutlineLevel = @intOutlineLevel AND LT.ChildrenCount > 0
                  GROUP BY LT.PlanID, LT.TaskID
                UNION ALL
                SELECT ET.PlanID AS PlanID, ET.TaskID AS TaskID,
                       0 AS LabHrs, 0 AS LabCost, 0 AS LabBill, 0 AS LabRev,
                       SUM(ISNULL(XE.PlannedExpCost, 0)) AS ExpCost, 
                       SUM(ISNULL(XE.PlannedExpBill, 0)) AS ExpBill, 
                       SUM(ISNULL(XE.ExpRevenue, 0)) AS ExpRev,
                       0 AS ConCost, 0 AS ConBill, 0 AS ConRev,
                       SUM(ISNULL(XE.PlannedDirExpCost, 0)) AS ExpDirCost,
                       SUM(ISNULL(XE.PlannedDirExpBill, 0)) AS ExpDirBill,
                       0 AS ConDirCost, 0 AS ConDirBill,
                       MIN(XE.StartDate) AS StartDate,
                       MAX(XE.EndDate) AS EndDate
                  FROM PNTask AS ET LEFT JOIN PNExpense AS XE 
                    ON ET.PlanID = XE.PlanID AND ET.TaskID = XE.TaskID
                  WHERE ET.PlanID = @strPlanID AND ET.OutlineLevel = @intOutlineLevel AND ET.ChildrenCount > 0
                  GROUP BY ET.PlanID, ET.TaskID
                UNION ALL
                SELECT CT.PlanID AS PlanID, CT.TaskID AS TaskID,
                       0 AS LabHrs, 0 AS LabCost, 0 AS LabBill, 0 AS LabRev,
                       0 AS ExpCost, 0 AS ExpBill, 0 AS ExpRev,
                       SUM(ISNULL(XC.PlannedConCost, 0)) AS ConCost, 
                       SUM(ISNULL(XC.PlannedConBill, 0)) AS ConBill, 
                       SUM(ISNULL(XC.ConRevenue, 0)) AS ConRev,
                       0 AS ExpDirCost, 0 AS ExpDirBill,
                       SUM(ISNULL(XC.PlannedDirConCost, 0)) AS ConDirCost,
                       SUM(ISNULL(XC.PlannedDirConBill, 0)) AS ConDirBill,
                       MIN(XC.StartDate) AS StartDate,
                       MAX(XC.EndDate) AS EndDate
                   FROM PNTask AS CT LEFT JOIN PNConsultant AS XC 
                     ON CT.PlanID = XC.PlanID AND CT.TaskID = XC.TaskID
                   WHERE CT.PlanID = @strPlanID AND CT.OutlineLevel = @intOutlineLevel AND CT.ChildrenCount > 0
                   GROUP BY CT.PlanID, CT.TaskID
                UNION ALL
                SELECT PT.PlanID, PT.TaskID,
                       SUM(ISNULL(KT.PlannedLaborHrs, 0)) AS LabHrs, 
                       SUM(ISNULL(KT.PlannedLabCost, 0)) AS LabCost, 
                       SUM(ISNULL(KT.PlannedLabBill, 0)) AS LabBill, 
                       SUM(ISNULL(KT.LabRevenue, 0)) AS LabRev,
                       SUM(ISNULL(KT.PlannedExpCost, 0)) AS ExpCost, 
                       SUM(ISNULL(KT.PlannedExpBill, 0)) AS ExpBill, 
                       SUM(ISNULL(KT.ExpRevenue, 0)) AS ExpRev,
                       SUM(ISNULL(KT.PlannedConCost, 0)) AS ConCost, 
                       SUM(ISNULL(KT.PlannedConBill, 0)) AS ConBill, 
                       SUM(ISNULL(KT.ConRevenue, 0)) AS ConRev,
                       SUM(ISNULL(KT.PlannedDirExpCost, 0)) AS ExpDirCost,
                       SUM(ISNULL(KT.PlannedDirExpBill, 0)) AS ExpDirBill,
                       SUM(ISNULL(KT.PlannedDirConCost, 0)) AS ConDirCost,
                       SUM(ISNULL(KT.PlannedDirConBill, 0)) AS ConDirBill,
                       MIN(KT.StartDate) AS StartDate,
                       MAX(KT.EndDate) AS EndDate
                  FROM PNTask AS PT INNER JOIN PNTask AS KT
                    ON PT.PlanID = KT.PlanID AND PT.OutlineNumber = KT.ParentOutlineNumber
                  WHERE PT.PlanID = @strPlanID AND PT.OutlineLevel = @intOutlineLevel AND PT.ChildrenCount > 0
                  GROUP BY PT.PlanID, PT.TaskID) AS XTPD
               ON XT.PlanID = XTPD.PlanID AND XT.TaskID = XTPD.TaskID
               WHERE XT.PlanID = @strPlanID AND XT.ChildrenCount > 0
             GROUP BY XT.PlanID, XT.TaskID) AS TPD 
          ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID
          LEFT JOIN PNPlan AS XP ON T.PlanID = XP.PlanID
        WHERE T.OutlineLevel = @intOutlineLevel AND T.ChildrenCount > 0

      -- Ready to move up one level.

      SET @intOutlineLevel = @intOutlineLevel - 1
    
    END -- While

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Fix ChildrenCount and ParentState, just in case...

  UPDATE PNTask SET ChildrenCount = X.ChildrenCount
    FROM PNTask AS T INNER JOIN
      (SELECT COUNT(C.TaskID) AS ChildrenCount, P.TaskID, P.PlanID
         FROM PNTask AS P LEFT JOIN PNTask AS C
         ON P.PlanID = C.PlanID AND P.OutlineNumber = C.ParentOutlineNumber
         GROUP BY P.TaskID, P.PlanID) AS X
      ON T.PlanID = @strPlanID AND T.PlanID = X.PlanID AND T.TaskID = X.TaskID

  UPDATE PNTask SET LabParentState = X.LabParentState
	  FROM PNTask AS T 
      INNER JOIN
        (SELECT PT.TaskID AS TaskID, 
           CASE WHEN (COUNT(CT.PlanID) + COUNT(A.PlanID)) > 0 THEN 'O' ELSE 'N' END AS LabParentState
           FROM PNTask AS PT
             LEFT JOIN PNTask AS CT ON PT.PlanID = CT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber
             LEFT JOIN PNAssignment AS A ON PT.PlanID = A.PlanID AND A.TaskID = PT.TaskID
           WHERE PT.PlanID = @strPlanID
           GROUP BY PT.TaskID
        ) AS X
        ON T.TaskID = X.TaskID
    WHERE
      ((T.LabParentState IN ('O', 'C') AND X.LabParentState = 'N') OR
       (T.LabParentState = 'N' AND X.LabParentState = 'O'))

  UPDATE PNTask SET ExpParentState = X.ExpParentState
	  FROM PNTask AS T 
      INNER JOIN
        (SELECT PT.TaskID AS TaskID, 
           CASE WHEN (COUNT(CT.PlanID) + COUNT(E.PlanID)) > 0 THEN 'O' ELSE 'N' END AS ExpParentState
           FROM PNTask AS PT
             LEFT JOIN PNTask AS CT ON PT.PlanID = CT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber
             LEFT JOIN PNExpense AS E ON PT.PlanID = E.PlanID AND E.TaskID = PT.TaskID
           WHERE PT.PlanID = @strPlanID
           GROUP BY PT.TaskID
        ) AS X
        ON T.TaskID = X.TaskID
    WHERE
      ((T.ExpParentState IN ('O', 'C') AND X.ExpParentState = 'N') OR
       (T.ExpParentState = 'N' AND X.ExpParentState = 'O'))

  UPDATE PNTask SET ConParentState = X.ConParentState
	  FROM PNTask AS T 
      INNER JOIN
        (SELECT PT.TaskID AS TaskID, 
           CASE WHEN (COUNT(CT.PlanID) + COUNT(C.PlanID)) > 0 THEN 'O' ELSE 'N' END AS ConParentState
           FROM PNTask AS PT
             LEFT JOIN PNTask AS CT ON PT.PlanID = CT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber
             LEFT JOIN PNConsultant AS C ON PT.PlanID = C.PlanID AND C.TaskID = PT.TaskID
           WHERE PT.PlanID = @strPlanID
           GROUP BY PT.TaskID
        ) AS X
        ON T.TaskID = X.TaskID
    WHERE
      ((T.ConParentState IN ('O', 'C') AND X.ConParentState = 'N') OR
       (T.ConParentState = 'N' AND X.ConParentState = 'O'))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF

END -- PNSumUpTPD
GO
