SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[HAI_ProjectLocatorReconcile]
AS
    /*
		Copyright (c) 2019 Halff Associates Inc. All rights reserved.
		11/10/2019 Joseph Sagel & Craig H. Anderson
				   Populate Address information into Vision from HAI_PrjLocator nightly
	*/
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;
    BEGIN

        -- Update Project Records
        UPDATE p
        SET p.Address1 = l.Address
          , p.City = l.City
          , p.State = d.Code
          , p.Zip = l.Zip
          , p.County = l.County
          , p.Country = l.Country
        FROM dbo.PR                                   p
            LEFT OUTER JOIN dbo.HAI_PrjLocator        l
                ON RIGHT(p.WBS1, LEN(p.WBS1) - 1) = l.WBS1
            LEFT OUTER JOIN dbo.CFGStatesDescriptions d
                ON l.State = d.Description
        WHERE
            l.WBS1 IS NOT NULL
            AND (
                p.WBS2 = ' '
                    OR p.WBS2 = ''
            );

        -- Update DataMart ProjectJTD
        UPDATE p
        SET p.Address1 = l.Address
          , p.City = l.City
          , p.State = d.Code
          , p.Zip = l.Zip
          , p.County = l.County
          , p.Country = l.Country
        FROM DataMart.dbo.ProjectJTD                  p
            LEFT OUTER JOIN dbo.HAI_PrjLocator        l
                ON RIGHT(p.WBS1, LEN(p.WBS1) - 1) = l.WBS1
            LEFT OUTER JOIN dbo.CFGStatesDescriptions d
                ON l.State = d.Description
        WHERE l.WBS1 IS NOT NULL;

        -- Delete record from Project Locator Table
        DELETE FROM dbo.HAI_PrjLocator WHERE WBS1 IS NOT NULL;
    END;
GO
