SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_CollectionsInsert]
AS
/* 
Copyright (c) 2020 Central Consulting Group.  All rights reserved.
06/17/2020	David Springer
			Populate UDIC_Collections
			Call this from a Collections scheduled workflow 
08/25/2020	Craig H. Anderson
			Added UPDATE code to update UDIC_Collections record  status as 'Completed' when AR is paid.
09/03/2020	Craig H. Anderson
			Added UPDATE code to update CustDaysOutstanding for Open Invoices each day.
10/23/2020  Craig H. Anderson
			Fix for paid Legal Involvement invoiced that have been paid.
11/30/2020	Craig H. Anderson
			Fix to mark closed Invoices with zero balance, but with 0 or 999999 as the PaidPeriod
			
*/
BEGIN
    SET NOCOUNT ON;

    INSERT INTO dbo.UDIC_Collection
    (
        UDIC_UID
      , CreateDate
      , CustInvoiceNumber
      , CustInvoiceDate
      , CustInvoiceTotal
      , CustReceived
      , CustBalance
      , CustCollectionStatus
      , CustRequestedActionOwner
      , CustProject
      , CustProjectNo
      , CustBillingClient
      , CustBillingContact
      , CustBillingPrimaryContact
      , CustManagementLeader
      , CustOperationsManager
      , CustDirector
      , CustPrincipal
      , CustProjMgr
      , CustBiller
      , CustDaysOutstanding
      , CustDaysSinceLastCharge
    )
    SELECT  REPLACE(NEWID(), '-', '') ID
          , GETDATE()
          , x.Invoice
          , x.InvoiceDate
          , x.Amount
          , x.Payment
          , x.Amount - x.Payment      Balance
          , 'Open'
          , p.ProjMgr
          , x.WBS1
          , x.WBS1
          , p.BillingClientID
          , p.BillingContactID
          , px.CustClientProjectContact
          , px.CustManagementLeader
          , px.CustOperationsManager
          , px.CustDirector
          , p.Principal
          , p.ProjMgr
          , p.Biller
          , DATEDIFF(DAY, x.InvoiceDate, GETDATE())
          , ISNULL(DATEDIFF(DAY, px.CustLastChargeDate, GETDATE()), 0)
    FROM    dbo.PR p
          , dbo.ProjectCustomTabFields px
          , (
        SELECT  ISNULL(g.MainWBS1, a.WBS1)  WBS1
              , a.Invoice
              , a.PaidPeriod
              , a.InvoiceDate
              , SUM(i.Amount)               Amount
              , SUM(ISNULL(pay.Payment, 0)) Payment
        FROM    (
            SELECT  WBS1, Invoice, MAX(PaidPeriod) PaidPeriod, MAX(InvoiceDate) InvoiceDate
            FROM    dbo.AR
            WHERE   PaidPeriod = 999999
            GROUP BY WBS1, Invoice
        )                      a
        LEFT JOIN dbo.BTBGSubs g
            ON a.WBS1 = g.SubWBS1
               AND  g.MainWBS1 <> g.SubWBS1
        JOIN (
            SELECT  WBS1, Invoice, -SUM(Amount) Amount
            FROM    LedgerAR
            WHERE   (
                TransType = 'IN'
                OR  (
                    TransType = 'CR'
                    AND SubType = 'T'
                )
            )
                    AND Invoice IS NOT NULL
            GROUP BY WBS1, Invoice
        )                      i
            ON a.WBS1 = i.WBS1
               AND  a.Invoice = i.Invoice
        LEFT JOIN (
            SELECT  WBS1, Invoice, -SUM(Amount) Payment
            FROM    dbo.LedgerAR
            WHERE   TransType = 'CR'
                    AND SubType = 'R'
            GROUP BY WBS1, Invoice
        )                      pay
            ON pay.WBS1 = i.WBS1
               AND  pay.Invoice = i.Invoice
        GROUP BY ISNULL(g.MainWBS1, a.WBS1), a.Invoice, a.PaidPeriod, a.InvoiceDate
    ) x
    WHERE   p.WBS1 = px.WBS1
            AND p.WBS2 = px.WBS2
            AND p.WBS2 = ' '
            AND p.Org NOT LIKE '02%'
            AND p.WBS1 = x.WBS1
            AND NOT EXISTS (
        SELECT  'x'
        FROM    dbo.UDIC_Collection uc
        WHERE   uc.CustProject = x.WBS1
                AND uc.CustInvoiceNumber = x.Invoice
    )
    ORDER BY x.Invoice;

    --
    -- Mark CustCollectionStatus as 'Completed' when AR reflect that they have been paid.
    --
    BEGIN TRANSACTION;
    UPDATE  uc
    SET uc.CustCollectionStatus = 'Completed'
      , uc.CustReceived = uc.CustInvoiceTotal
      , uc.CustBalance = 0
      , uc.ModDate = GETDATE()
      , uc.ModUser = 'CollectionInsert'
    FROM    dbo.UDIC_Collection uc
    INNER JOIN (
        SELECT  DISTINCT
                a1.Invoice, a1.PaidPeriod
        FROM    dbo.AR a1 WITH (NOLOCK)
    )                           a
        ON uc.CustInvoiceNumber = a.Invoice
    WHERE   uc.CustCollectionStatus IN ( 'Open', 'Legal Involvement' )
            AND (
                a.PaidPeriod NOT IN ( 0, 999999 )
                OR  uc.CustBalance = 0
            );
    COMMIT TRANSACTION;

    --
    -- Mark CustCollectionStatus as 'Open' when AR reflect that they are not been paid, but previously the code found them as paid
    --
    BEGIN TRANSACTION;
    UPDATE  uc
    SET uc.CustCollectionStatus = 'Open', uc.ModDate = GETDATE(), uc.ModUser = 'CollectionInsert'
    FROM    dbo.UDIC_Collection uc
    INNER JOIN (
        SELECT  DISTINCT
                a1.Invoice, a1.PaidPeriod
        FROM    dbo.AR a1 WITH (NOLOCK)
    )                           a
        ON uc.CustInvoiceNumber = a.Invoice
    WHERE   uc.CustCollectionStatus = 'Completed'
            AND a.PaidPeriod IN ( 0, 999999 )
            AND uc.CustBalance > 0;
    COMMIT TRANSACTION;

    --
    -- Update Paid
    --
    BEGIN TRANSACTION;
    UPDATE  uc
    SET uc.CustReceived = ISNULL(a.Payment, 0)
      , uc.CustBalance = uc.CustInvoiceTotal - ISNULL(a.Payment, 0)
      , uc.ModDate = GETDATE()
      , uc.ModUser = 'CollectionInsert'
    FROM    dbo.UDIC_Collection uc
    LEFT JOIN (
        SELECT  ISNULL(g.MainWBS1, l.WBS1) WBS1, l.Invoice, -SUM(l.Amount) Payment
        FROM    dbo.LedgerAR   l
        LEFT JOIN dbo.BTBGSubs g
            ON g.SubWBS1 = l.WBS1
               AND  g.MainWBS1 <> g.SubWBS1
        WHERE   l.TransType = 'CR'
                AND l.SubType = 'R'
        GROUP BY ISNULL(g.MainWBS1, l.WBS1), l.Invoice
    )                           a
        ON uc.CustProject = a.WBS1
           AND  uc.CustInvoiceNumber = a.Invoice
    WHERE   uc.CustCollectionStatus = 'Open'
            AND ISNULL(a.Payment, 0) <> uc.CustReceived;
    COMMIT TRANSACTION;

    --
    -- Update CustDaysOutstanding and CustDaysSinceLastCharge 
    --
    BEGIN TRANSACTION;
    UPDATE  uc
    SET uc.CustDaysOutstanding = DATEDIFF(DAY, uc.CustInvoiceDate, GETDATE())
      , uc.CustDaysSinceLastCharge = DATEDIFF(
                                         DAY, ISNULL(px.CustLastChargeDate, GETDATE()), GETDATE()
                                     )
      , uc.ModDate = GETDATE()
      , uc.ModUser = 'CollectionInsert'
    FROM    dbo.UDIC_Collection           AS uc
    INNER JOIN dbo.ProjectCustomTabFields AS px
        ON uc.CustProject = px.WBS1
           AND  px.WBS2 = ' '
    WHERE   uc.CustCollectionStatus = 'Open';
    COMMIT TRANSACTION;
END;
GO
