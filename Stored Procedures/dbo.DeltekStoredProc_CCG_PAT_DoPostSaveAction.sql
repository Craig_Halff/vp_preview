SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_DoPostSaveAction] ( @PayableSeq int, @User varchar(50)= null)
             AS EXEC spCCG_PAT_DoPostSaveAction @PayableSeq,@User
GO
