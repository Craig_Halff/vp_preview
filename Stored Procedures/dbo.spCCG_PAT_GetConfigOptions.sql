SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_GetConfigOptions] (
	@user		Nvarchar(32),
	@type		varchar(50)
) AS
BEGIN
	-- EXEC [spCCG_PAT_GetConfigOptions] null, 'BatchImportSource'

	SET NOCOUNT ON;

	SELECT *
        FROM CCG_PAT_ConfigOptions
        WHERE type = @type
			AND (ISNULL(@user,N'') = N'' OR ChangedBy = @user)
        ORDER BY datechanged DESC;
END;
GO
