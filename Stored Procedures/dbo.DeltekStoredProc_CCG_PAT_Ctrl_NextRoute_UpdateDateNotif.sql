SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Ctrl_NextRoute_UpdateDateNotif]
	@seq		int,
	@nextSeq	int
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CCG_PAT_Payable SET DateLastNotificationSent = getutcdate() WHERE Seq = @seq;
	IF @nextSeq is not null and @nextSeq > -1
		UPDATE CCG_PAT_Pending SET DateLastNotificationSent = getutcdate() WHERE Seq = @nextSeq;
END;

GO
