SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ServicesProvided_Get]
	@WBS1	NVARCHAR(30)
AS
BEGIN
  ------------------------------------------------------------------------
  SET NOCOUNT ON;

  ------------------------------------------------------------------------
  SELECT
	SP.WBS1
    ,SP.WBS2
    ,SP.WBS3
    ,SP.Seq
    ,SP.CreateUser
    ,SP.CreateDate
    ,SP.ModUser
    ,SP.ModDate
    ,SP.CustServicesProjectType
    ,SP.CustService
	,PT.CustProjectTypeCode
	,PT.CustProjectTypeName
	,PT.CustProjectTypeCategory
	,PT.CustProjectTypeSubCategory
	,SVC.CustomCurrencyCode
    ,SVC.CustServiceCode
    ,SVC.CustServiceName
    ,SVC.CustServiceCategory
    ,SVC.CustServiceSubCategory
  FROM
    dbo.Projects_ServicesProvided AS SP
  LEFT JOIN dbo.UDIC_ProjectType AS PT
	ON PT.UDIC_UID = CustServicesProjectType
  INNER JOIN dbo.UDIC_Service AS SVC
	ON SVC.UDIC_UID = CustService 
  WHERE
	SP.WBS1 = @WBS1
		AND SP.WBS2 = ' '
		AND SP.WBS3 = ' '
  ORDER BY
    SP.CustServicesProjectType    ASC
  ------------------------------------------------------------------------
END
GO
