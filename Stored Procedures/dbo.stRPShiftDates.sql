SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPShiftDates]
  @strRowID nvarchar(255),
  @strFromStartDate varchar(8),
  @strFromEndDate varchar(8),
  @strToStartDate varchar(8),
  @strToEndDate varchar(8),
  @intShiftMode int, /* 1 = Maintain Pattern, 2 = Distribute Evenly, 3 = No Redistribute */
  @bitShiftAll bit, /* This flag is applicable only for ShiftMode = {1, 3}. 0 = Shift using the "From" range, 1 = Shift the entire Assignment Row */
  @bitFixStartDate bit, /* This flag is applicable only for ShiftMode = {1, 3}. 1 = Hold "To" StartDate fixed, 0 = Hold "To" EndDate fixed */
  @bitCalledFromRM bit = 0,
  @bitMatchChildrenDates bit = 0 /* When 1, makes all children dates match new parent dates. */
AS

BEGIN -- Procedure stRPShiftDates

  SET NOCOUNT ON
  
  DECLARE @dtFromStartDate datetime
  DECLARE @dtFromEndDate datetime

  DECLARE @dtToStartDate datetime
  DECLARE @dtToEndDate datetime

  DECLARE @dtMinTPDDate datetime
  DECLARE @dtMaxTPDDate datetime
  DECLARE @dtASGStartDate datetime
  DECLARE @dtASGEndDate datetime
  DECLARE @dtTSKStartDate datetime
  DECLARE @dtTSKEndDate datetime
   
  DECLARE @strCompany nvarchar(14)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strAT_TaskID varchar(32)
  DECLARE @strAssignmentID varchar(32)
  DECLARE @strResourceID nvarchar(20)
  DECLARE @strGenericResourceID nvarchar(20)
  DECLARE @strResourceType varchar(1)
  DECLARE @strIDPrefix nvarchar(255)
  DECLARE @strUserName nvarchar(32)
  DECLARE @strCalcID varchar(34)

  DECLARE @decSumHrs decimal(19,4) = 0

  DECLARE @siGRMethod smallint
  DECLARE @siCostRtMethod smallint
  DECLARE @siBillingRtMethod smallint
  
  DECLARE @intCostRtTableNo int
  DECLARE @intBillingRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int

  DECLARE @siLabRateDecimals smallint = 4
 
  -- Declare Temp tables.
  
  DECLARE @tabAssignment TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ResourceID nvarchar(20) COLLATE database_default,
    Category smallint,
    GRLBCD nvarchar(14) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    CostRate decimal(19,4),
    BillingRate decimal(19,4),
    AT_OutlineNumber varchar(255) COLLATE database_default
    UNIQUE(PlanID, TaskID, AssignmentID)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

  -- Set Dates

  SET @dtFromStartDate = 
    CASE 
      WHEN DATALENGTH(@strFromStartDate) = 0 
      THEN NULL
      ELSE CONVERT(datetime, @strFromStartDate)
    END

  SET @dtFromEndDate = 
    CASE
      WHEN DATALENGTH(@strFromEndDate) = 0
      THEN NULL
      ELSE CONVERT(datetime, @strFromEndDate)
    END

  SET @dtToStartDate = 
    CASE
      WHEN DATALENGTH(@strToStartDate) = 0
      THEN NULL
      ELSE CONVERT(datetime, @strToStartDate)
    END

  SET @dtToEndDate = 
    CASE
    WHEN DATALENGTH(@strToEndDate) = 0
    THEN NULL
    ELSE CONVERT(datetime, @strToEndDate)
  END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. E~<PNAssignment.ResourceID>|<PNTask.TaskID>
  --   2. G~<PNAssignment.GenericResourceID>|<PNTask.TaskID>

  SET @strIDPrefix = LEFT(@strRowID, CHARINDEX('|', @strRowID) - 1)
  SET @strResourceType = SUBSTRING(@strIDPrefix, 1, 1)

  IF (@strResourceType = 'E')
    BEGIN
      SET @strResourceID = REPLACE(@strIDPrefix, 'E~', '')
      SET @strGenericResourceID = NULL
    END
  ELSE IF (@strResourceType = 'G')
    BEGIN
      SET @strGenericResourceID = REPLACE(@strIDPrefix, 'G~', '')
      SET @strResourceID = NULL
    END
  ELSE
    BEGIN
      SET @strResourceID = NULL
      SET @strGenericResourceID = NULL
    END

  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

  -- Get various parameters.

  SELECT 
    @strCompany = P.Company,
    @strPlanID = T.PlanID,
    @dtTSKStartDate = T.StartDate,
    @dtTSKEndDate = T.EndDate,
    @siGRMethod = P.GRMethod,
    @intGenResTableNo = P.GenResTableNo,
    @intGRBillTableNo = P.GRBillTableNo,
    @siCostRtMethod = P.CostRtMethod,
    @siBillingRtMethod = P.BillingRtMethod,
    @intCostRtTableNo = P.CostRtTableNo,
    @intBillingRtTableNo = P.BillingRtTableNo
    FROM PNTask AS T
      INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID
    WHERE T.TaskID = @strTaskID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@intShiftMode = 1) /* 1 = Maintain Pattern */
    BEGIN

      EXEC dbo.stRPShiftTasks
        @strRowID,
        @strFromStartDate,
        @strFromEndDate,
        @strToStartDate,
        @strToEndDate,
        @bitShiftAll, 
        @bitFixStartDate 

    END /* END IF (@intShiftMode = 1) */
    
  ELSE IF (@intShiftMode IN (2, 3)) /* --> (@intShiftMode = 2 or 3) */
    BEGIN

      EXEC dbo.stRPChangeDuration
        @strRowID,
        @strToStartDate,
        @strToEndDate,
        @intShiftMode,
        @bitMatchChildrenDates

    END /* END ELSE IF (@intShiftMode = 3) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    -- If this SP was called from Resource Management then need to update RP tables.
    -- A shift of dates from Resource Management will affect Expense and Consultant data as well as Assignment data.

    IF (@bitCalledFromRM = 1)
      BEGIN

        -- The most efficient way is to delete existing TPD in RPPlannedLabor for the given Plan and reinsert from PNPlannedLabor.

        DELETE RPPlannedLabor WHERE PlanID = @strPlanID 

        INSERT RPPlannedLabor(
          TimePhaseID,
          PlanID, 
          TaskID,
          AssignmentID,
          StartDate, 
          EndDate, 
          PeriodHrs,
          CostRate,
          BillingRate,
          CreateUser,
          ModUser,
          CreateDate,
          ModDate
        )
           SELECT
            TPD.TimePhaseID AS TimePhaseID,
            TPD.PlanID AS PlanID, 
            TPD.TaskID AS TaskID,
            TPD.AssignmentID AS AssignmentID,
            TPD.StartDate AS StartDate, 
            TPD.EndDate AS EndDate, 
            TPD.PeriodHrs AS PeriodHrs,
            TPD.CostRate AS CostRate,
            TPD.BillingRate AS BillingRate,
            CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stRPShiftDates' ELSE 'SHD_' + @strUserName END AS CreateUser,
            CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stRPShiftDates' ELSE 'SHD_' + @strUserName END AS ModUser,
            LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As CreateDate,
            LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As ModDate 
          FROM PNPlannedLabor AS TPD
          WHERE TPD.PlanID = @strPlanID

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        DELETE RPPlannedExpenses WHERE PlanID = @strPlanID 

        INSERT RPPlannedExpenses(
          TimePhaseID,
          PlanID, 
          TaskID,
          ExpenseID,
          StartDate, 
          EndDate, 
          PeriodCost,
          PeriodBill,
          CreateUser,
          ModUser,
          CreateDate,
          ModDate
        )
           SELECT
            TPD.TimePhaseID AS TimePhaseID,
            TPD.PlanID AS PlanID, 
            TPD.TaskID AS TaskID,
            TPD.ExpenseID AS ExpenseID,
            TPD.StartDate AS StartDate, 
            TPD.EndDate AS EndDate, 
            TPD.PeriodCost AS PeriodCost,
            TPD.PeriodBill AS PeriodBill,
            CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stRPShiftDates' ELSE 'SHD_' + @strUserName END AS CreateUser,
            CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stRPShiftDates' ELSE 'SHD_' + @strUserName END AS ModUser,
            LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As CreateDate,
            LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As ModDate 
          FROM PNPlannedExpenses AS TPD
          WHERE TPD.PlanID = @strPlanID

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        DELETE RPPlannedConsultant WHERE PlanID = @strPlanID 

        INSERT RPPlannedConsultant(
          TimePhaseID,
          PlanID, 
          TaskID,
          ConsultantID,
          StartDate, 
          EndDate, 
          PeriodCost,
          PeriodBill,
          CreateUser,
          ModUser,
          CreateDate,
          ModDate
        )
           SELECT
            TPD.TimePhaseID AS TimePhaseID,
            TPD.PlanID AS PlanID, 
            TPD.TaskID AS TaskID,
            TPD.ConsultantID AS ConsultantID,
            TPD.StartDate AS StartDate, 
            TPD.EndDate AS EndDate, 
            TPD.PeriodCost AS PeriodCost,
            TPD.PeriodBill AS PeriodBill,
            CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stRPShiftDates' ELSE 'SHD_' + @strUserName END AS CreateUser,
            CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stRPShiftDates' ELSE 'SHD_' + @strUserName END AS ModUser,
            LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As CreateDate,
            LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As ModDate 
          FROM PNPlannedConsultant AS TPD
          WHERE TPD.PlanID = @strPlanID

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        UPDATE RA SET
          StartDate = CASE WHEN RA.StartDate <> PA.StartDate THEN PA.StartDate ELSE RA.StartDate END, 
          EndDate = CASE WHEN RA.EndDate <> PA.EndDate THEN PA.EndDate ELSE RA.EndDate END
          FROM RPAssignment AS RA
            INNER JOIN PNAssignment AS PA
              ON RA.PlanID = PA.PlanID AND RA.TaskID = PA.TaskID AND RA.AssignmentID = PA.AssignmentID
          WHERE RA.PlanID = @strPlanID 
            AND (RA.StartDate <> PA.StartDate OR RA.EndDate <> PA.EndDate)
 
        UPDATE RE SET
          StartDate = CASE WHEN RE.StartDate <> PE.StartDate THEN PE.StartDate ELSE RE.StartDate END, 
          EndDate = CASE WHEN RE.EndDate <> PE.EndDate THEN PE.EndDate ELSE RE.EndDate END
          FROM RPExpense AS RE
            INNER JOIN PNExpense AS PE
              ON RE.PlanID = PE.PlanID AND RE.TaskID = PE.TaskID AND RE.ExpenseID = PE.ExpenseID
          WHERE RE.PlanID = @strPlanID 
            AND (RE.StartDate <> PE.StartDate OR RE.EndDate <> PE.EndDate)

        UPDATE RC SET
          StartDate = CASE WHEN RC.StartDate <> PC.StartDate THEN PC.StartDate ELSE RC.StartDate END, 
          EndDate = CASE WHEN RC.EndDate <> PC.EndDate THEN PC.EndDate ELSE RC.EndDate END
          FROM RPConsultant AS RC
            INNER JOIN PNConsultant AS PC
              ON RC.PlanID = PC.PlanID AND RC.TaskID = PC.TaskID AND RC.ConsultantID = PC.ConsultantID
          WHERE RC.PlanID = @strPlanID 
            AND (RC.StartDate <> PC.StartDate OR RC.EndDate <> PC.EndDate)

         UPDATE RT SET
          StartDate = CASE WHEN RT.StartDate <> PT.StartDate THEN PT.StartDate ELSE RT.StartDate END, 
          EndDate = CASE WHEN RT.EndDate <> PT.EndDate THEN PT.EndDate ELSE RT.EndDate END
          FROM RPTask AS RT
            INNER JOIN PNTask AS PT
              ON RT.PlanID = PT.PlanID AND RT.TaskID = PT.TaskID 
          WHERE RT.PlanID = @strPlanID 
            AND (RT.StartDate <> PT.StartDate OR RT.EndDate <> PT.EndDate)

      END /* END IF (@bitCalledFromRM = 1) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET @strCalcID = 'P~' + @strPlanID
  EXECUTE dbo.stRPReCalcPlan @strCalcID, 0, @bitCalledFromRM

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Update VesionID.

  EXECUTE dbo.stRPUpdateVersionID @strPlanID, @bitCalledFromRM 

  -- Set LastPlanAction.

  IF (@bitCalledFromRM = 0)
    BEGIN
      UPDATE PNPlan SET LastPlanAction = 'SAVED' WHERE PlanID = @strPlanID
    END /* END IF (@bitCalledFromRM = 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     
  SET NOCOUNT OFF

END -- stRPShiftDates
GO
