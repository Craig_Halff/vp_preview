SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FormRouting_DoSave]
	@updateValuesSql	Nvarchar(max),
	@insertValuesSql	Nvarchar(max),
	@deleteSeqs			Nvarchar(max),
	@seq				Nvarchar(20),
	@emp				Nvarchar(50)
AS
BEGIN
	/*
	[DeltekStoredProc_CCG_PAT_FormRouting_DoSave]
		@updateValuesSql = '',--(2, null,null, null, 2112), (3, 'Y',null, null, 2115)',
		@insertValuesSql = '',
		@deleteSeqs = '39',
		@seq = '49' ,
		@emp = '00001'
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	int;
	DECLARE @ids		TABLE (id int);
	DECLARE @i			int;
	DECLARE @newI		int;
	--DECLARE @seq		Nvarchar(50);

	-- Ensure that our inputs are safe (preventing SQL injection)
	SET @safeSql = dbo.fnCCG_PAT_RegexMatch(N'<INSERT VALUES>|', @updateValuesSql) * 1;
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch(N'<INSERT VALUES>|', @insertValuesSql) * 2;
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch(N'<CSV>', @deleteSeqs) * 4;
	IF @safeSql < 7 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS Nvarchar(4));
		RETURN;
	END;

	BEGIN TRANSACTION;
	-- Pending updates
	IF @updateValuesSql <> '' AND @updateValuesSql IS NOT NULL BEGIN
		SET @sSQL = N'
			UPDATE pen
				SET
					[SortOrder]					= ISNULL(temp.[SortOrder], pen.SortOrder),
					[Parallel]					= ISNULL(temp.[Parallel], pen.Parallel),
					[Description]				= ISNULL(temp.[Description], pen.Description)
				FROM CCG_PAT_Pending pen
					JOIN (VALUES ' + @updateValuesSql + ')
					temp (SortOrder,Parallel, Description, Seq) ON temp.Seq = pen.Seq';
		--PRINT @sSQL;
		EXEC (@sSQL);
	END;

	-- Pending inserts
	IF @insertValuesSql <> '' AND @insertValuesSql IS NOT NULL BEGIN
		SET @sSQL = N'
			INSERT INTO CCG_PAT_Pending (PayableSeq, SortOrder,Parallel, Employee, Description, CreateDateTime)
				VALUES ' + @insertValuesSql;
		--PRINT @sSQL;
		EXEC (@sSQL);
	END;

	-- Deletes
	If ISNULL(@deleteSeqs, '') <> '' BEGIN
		SET @sSQL = N'INSERT INTO CCG_PAT_History(PayableSeq,Stage,ActionDate,ActionTaken,ActionTakenBy,ActionRecipient,Description)
                     SELECT  '+ @seq + ',null,GETUTCDATE(),N''Delete Route'', N'''+ @emp+ ''',Employee,Description from CCG_PAT_Pending  WHERE Seq in (' + @deleteSeqs + ');
		DELETE FROM CCG_PAT_Pending where Seq in (' + @deleteSeqs + ')';
		EXEC (@sSQL);
	END;

	COMMIT TRANSACTION;
END;

GO
