SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_PAT_RouteDetail_Load]
	@VISION_LANGUAGE	varchar(10)
AS
BEGIN
	-- [spCCG_PAT_RouteDetail_Load] 'en-US'
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    SELECT T1.[Route], T1.[RouteType], T1.RouteValue,
            max(IsNull(T2.Description, IsNull(T1.Description, ''))) as Description, T1.[SortOrder], T1.[Param1], T1.[Param2], T1.[Parallel]
        FROM CCG_PAT_ConfigRouteDetail T1
            LEFT JOIN CCG_PAT_ConfigRouteDetailDescriptions T2
				ON T2.Route = T1.Route and T2.RouteType = T1.RouteType AND T2.RouteValue = T1.RouteValue and T2.SortOrder = T1.SortOrder and T2.UICultureName = @VISION_LANGUAGE
		GROUP BY T1.[Route], T1.[RouteType], T1.RouteValue, T1.SortOrder, T1.[Param1], T1.[Param2], T1.[Parallel]
        ORDER BY Route, T1.SortOrder
END
GO
