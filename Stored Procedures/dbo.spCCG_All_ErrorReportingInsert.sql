SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_All_ErrorReportingInsert] 
	@ErrorNumber [varchar](64) = NULL,
	@lineNumber [int] = NULL,
	@errorMessage [varchar](max) = NULL,
	@details [varchar](max) = NULL,
	@product [varchar](64) = NULL,
	@productVersion [varchar](64) = NULL,
	@user Nvarchar(32) = NULL,
	@SessionID [varchar](64) = NULL,
	@ComputerName [varchar](64) = NULL
AS
BEGIN
	-- spCCG_All_ErrorReportingInsert 'ERROR NUMBER!', '123', 'ERROR MESSAGE!!!', 'Details!', 'ARM', '4.1.1.23', 'Mark', 'SessionID', 'MyComputerName'
	-- select * from [CCG_All_ErrorReporting]
	SET NOCOUNT ON

	--If ComputerName is not passed in Set to what Server sees.
	IF (@ComputerName IS NULL)	
		SET @ComputerName = CONVERT([varchar](64),serverproperty('MachineName'),(0))

	IF (@errorMessage IS NOT NULL)	
	BEGIN
		INSERT INTO [dbo].[CCG_All_ErrorReporting] ([ErrorNumber],[lineNumber],[errorMessage],[details],[product],[productVersion],[user],[SessionID],[ComputerName])
			SELECT									@ErrorNumber, @lineNumber, @errorMessage, @details, @product, @productVersion, @user, @SessionID, @ComputerName
	END
END
GO
