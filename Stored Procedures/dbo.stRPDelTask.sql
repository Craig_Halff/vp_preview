SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPDelTask]
  @strPlanID varchar(32),
  @strTaskID varchar(32)
AS

BEGIN -- Procedure pmDelTask

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to delete a Task row and its associated rows.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  
  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED


  DELETE EMActivity
    FROM EMActivity
    INNER JOIN Activity ON EMActivity.ActivityID = Activity.ActivityID
    INNER JOIN RPAssignment ON Activity.AssignmentID = RPAssignment.AssignmentID
    WHERE RPAssignment.PlanID = @strPlanID AND RPAssignment.TaskID = @strTaskID
  
  DELETE ActivityCustomTabFields 
    FROM ActivityCustomTabFields
    INNER JOIN Activity ON ActivityCustomTabFields.ActivityID = Activity.ActivityID
    INNER JOIN RPAssignment ON Activity.AssignmentID = RPAssignment.AssignmentID
    WHERE RPAssignment.PlanID = @strPlanID AND RPAssignment.TaskID = @strTaskID

  DELETE Activity 
    FROM Activity
    INNER JOIN RPAssignment ON Activity.AssignmentID = RPAssignment.AssignmentID
    WHERE RPAssignment.PlanID = @strPlanID AND RPAssignment.TaskID = @strTaskID
  
  DELETE RPPlannedLabor WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPPlannedExpenses WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPPlannedConsultant WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPPlannedUnit WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPBaselineLabor WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPBaselineExpenses WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPBaselineConsultant WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPBaselineUnit WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPAssignment WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPExpense WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPConsultant WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPUnit WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPCompensationFee WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPConsultantFee WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPReimbAllowance WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPEVT WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPDependency WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPPlannedRevenueLabor WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE RPTask WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  
  DELETE PNPlannedLabor WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE PNPlannedExpenses WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE PNPlannedConsultant WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE PNBaselineLabor WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE PNBaselineExpenses WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE PNBaselineConsultant WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE PNAssignment WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE PNExpense WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE PNConsultant WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE PNPlannedRevenueLabor WHERE PlanID = @strPlanID AND TaskID = @strTaskID
  DELETE PNTask WHERE PlanID = @strPlanID AND TaskID = @strTaskID

  SET NOCOUNT OFF

END -- stRPDelTask
GO
