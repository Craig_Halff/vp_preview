SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_TR_ChangeLaborBillStatus](@Username varchar(32), @OrigTable varchar(4), 
	@OrigPeriod varchar(6), @OrigPostSeq varchar(32), @OrigPKey varchar(32), 
	@NewPeriod  varchar(6), @NewPostSeq  varchar(32), @NewPKey  varchar(32),
	@OrigBillStatus varchar(1), @OrigRegHrs decimal(19,4), @OrigOvtHrs decimal(19,4), @OrigSpecialOvtHrs decimal(19,4),
	@NewBillStatus  varchar(1), @NewRegHrs  decimal(19,4), @NewOvtHrs  decimal(19,4), @NewSpecialOvtHrs  decimal(19,4),
	@ApprovalComment varchar(255), @IncludeFuture varchar(1), @FNStatus varchar(2))
AS BEGIN

	declare @partial int, @origRecordNewBillStatus varchar(1), @sql varchar(max), @origTableWhereClause varchar(255), @emp varchar(32)
	select @emp=Employee from SEUser where Username=@Username

	if @NewPKey = ''
		set @NewPKey = Left(Replace(Convert(varchar(255), newid()),'-',''),31)
	set @origTableWhereClause = ' where Period=' + @OrigPeriod + ' and PostSeq=' + @OrigPostSeq + ' and PKey=''' + @OrigPkey + ''' '

	if @OrigRegHrs <> @NewRegHrs or @OrigOvtHrs <> @NewOvtHrs or @OrigSpecialOvtHrs <> @NewSpecialOvtHrs
	begin
		set @partial = 1
		set @origRecordNewBillStatus = 'R'			
	end else begin
		set @partial = 0
		set @origRecordNewBillStatus = @NewBillStatus	
	end
	
	set @sql = 'update ' + @OrigTable + ' set BillStatus=''' + @origRecordNewBillStatus + '''' +  @origTableWhereClause + ' and BillStatus=''' + @OrigBillStatus + ''' '
	
	if @partial = 1
	begin
		
		declare @commonColumns varchar(2000), @nonCommonColumns varchar(1000), @sqlAmts varchar(255)
		declare @regMult varchar(50), @ovtMult varchar(50), @specialOvtMult varchar(50), @billMult varchar(50)

		set @nonCommonColumns = 'Period,PostSeq,PKey,BillStatus,RegHrs,OvtHrs,SpecialOvtHrs,BillExt,' +
			'RegAmt,OvtAmt,SpecialOvtAmt,' +
			'RegAmtProjectCurrency,OvtAmtProjectCurrency,SpecialOvtAmtProjectCurrency,' +
			'RegAmtBillingCurrency,OvtAmtBillingCurrency,SpecialOvtAmtBillingCurrency,' +
			'RegAmtEmployeeCurrency,OvtAmtEmployeeCurrency,SpecialOvtAmtEmployeeCurrency,ModUser,'

		select @commonColumns = STUFF((
			SELECT ',' + c.name
			FROM sys.columns c
			WHERE c.object_id = OBJECT_ID('BILD') and c.name in (select cols.name from sys.columns cols where cols.object_id=OBJECT_ID('LD'))
			  and charindex(','+c.name+',', ','+@nonCommonColumns) < 1
			FOR XML PATH('')), 1, 1, '')
		print @commonColumns
		set @regMult = Cast(@NewRegHrs as varchar) + ' / ' + Cast(@OrigRegHrs as varchar)
		set @ovtMult = Cast(@NewOvtHrs as varchar) + ' / ' + Cast(@OrigOvtHrs as varchar)
		set @specialOvtMult = Cast(@NewSpecialOvtHrs as varchar) + ' / ' + Cast(@OrigSpecialOvtHrs as varchar)
		if @OrigRegHrs = 0 set @regMult = '1'
		if @OrigOvtHrs = 0 set @ovtMult = '1'
		if @OrigSpecialOvtHrs = 0 set @specialOvtMult = '1'
		if @OrigOvtHrs = 0 and @OrigSpecialOvtHrs = 0
			set @billMult = @regMult
		else if @OrigRegHrs = 0 and @OrigSpecialOvtHrs = 0
			set @billMult = @ovtMult
		else if @OrigRegHrs = 0 and @OrigOvtHrs = 0
			set @billMult = @specialOvtMult
		else
			set @billMult = '1'

		
		set @sql = @sql +
			' insert into BILD (' + @nonCommonColumns + @commonColumns + ') select ' + @NewPeriod + ',' + @NewPostSeq + ',''' + @NewPKey + 'a'',''' + @NewBillStatus + ''',' + 
				Cast(@NewRegHrs as varchar) + ',' + Cast(@NewOvtHrs as varchar) + ',' + Cast(@NewSpecialOvtHrs as varchar) +
				', Round(BillExt * ' + @billMult + ',2)' +
				', RegAmt  * ' + @regMult + ', OvtAmt * ' + @ovtMult + ', SpecialOvtAmt * ' + @specialOvtMult +
				', RegAmtProjectCurrency  * ' + @regMult + ', OvtAmtProjectCurrency  * ' + @ovtMult + ', SpecialOvtAmtProjectCurrency  * ' + @specialOvtMult +
				', RegAmtBillingCurrency  * ' + @regMult + ', OvtAmtBillingCurrency  * ' + @ovtMult + ', SpecialOvtAmtBillingCurrency  * ' + @specialOvtMult +
				', RegAmtEmployeeCurrency * ' + @regMult + ', OvtAmtEmployeeCurrency * ' + @ovtMult + ', SpecialOvtAmtEmployeeCurrency * ' + @specialOvtMult +
				',''' + @Username + ''', ' + @commonColumns +
			' from ' + @OrigTable + @origTableWhereClause
		
		set @sql = @sql +
			' insert into BILD (' + @nonCommonColumns + @commonColumns + ') select ' + @NewPeriod + ',' + @NewPostSeq + ',''' + @NewPKey + 'b'',''' + @OrigBillStatus + ''',' + 
				Cast(@OrigRegHrs - @NewRegHrs as varchar) + ',' + Cast(@OrigOvtHrs - @NewOvtHrs as varchar) + ',' + Cast(@OrigSpecialOvtHrs - @NewSpecialOvtHrs as varchar) + 
				', Round(BillExt - (BillExt * ' + @billMult + '),2)' +
				', RegAmt  - (RegAmt  * ' + @regMult + '), OvtAmt - (OvtAmt * ' + @ovtMult + '), SpecialOvtAmt - (SpecialOvtAmt * ' + @specialOvtMult + ')' +
				', RegAmtProjectCurrency  - (RegAmtProjectCurrency  * ' + @regMult + '), OvtAmtProjectCurrency  - (OvtAmtProjectCurrency  * ' + @ovtMult + '), SpecialOvtAmtProjectCurrency  - (SpecialOvtAmtProjectCurrency  * ' + @specialOvtMult + ')' +
				', RegAmtBillingCurrency  - (RegAmtBillingCurrency  * ' + @regMult + '), OvtAmtBillingCurrency  - (OvtAmtBillingCurrency  * ' + @ovtMult + '), SpecialOvtAmtBillingCurrency  - (SpecialOvtAmtBillingCurrency  * ' + @specialOvtMult + ')' +
				', RegAmtEmployeeCurrency - (RegAmtEmployeeCurrency * ' + @regMult + '), OvtAmtEmployeeCurrency - (OvtAmtEmployeeCurrency * ' + @ovtMult + '), SpecialOvtAmtEmployeeCurrency - (SpecialOvtAmtEmployeeCurrency * ' + @specialOvtMult + ')' +
				',''' + @Username + ''', ' + @commonColumns +
			' from ' + @OrigTable + @origTableWhereClause
	end
	
	if @partial = 0
		set @sql = @sql + ' ; exec spCCG_TR_InsertApproval ''' + @OrigTable + ''',' + Cast(@OrigPeriod as varchar) + ',' + 
			Cast(@OrigPostSeq as varchar) + ',''' + @OrigPKey + ''',null,''' + @emp + ''',''' + IsNull(@ApprovalComment,'') + ''',''' + @IncludeFuture + ''',null,''' + @FNStatus + ''' '
	else
	begin
		set @sql = @sql + 
			' ; exec spCCG_TR_InsertApproval ''' + 'BILD' + ''',' + Cast(@NewPeriod as varchar) + ',' + 
				Cast(@NewPostSeq as varchar) + ',''' + @NewPKey + 'a'',''' + @emp + ''',''' + @emp + ''',''' + IsNull(@ApprovalComment,'') + ''',''' + @IncludeFuture + ''',null,''' + @FNStatus + ''' ' +
			' ; exec spCCG_TR_InsertApproval ''' + 'BILD' + ''',' + Cast(@NewPeriod as varchar) + ',' + 
				Cast(@NewPostSeq as varchar) + ',''' + @NewPKey + 'b'',''' + @emp + ''',''' + @emp + ''',''' + IsNull(@ApprovalComment,'') + ''',null,null,''' + @FNStatus + ''' '
		
		set @sql = @sql +
			' ; update CCG_TR_History set OriginalTable=''BILD'', OriginalPeriod=' + Cast(@NewPeriod as varchar) + ', OriginalPostSeq=' +
				Cast(@NewPostSeq as varchar) + ', OriginalPKey=''' + @NewPKey + 'a'' ' +
				' where OriginalTable=''' + @OrigTable + ''' and OriginalPeriod=' + @OrigPeriod + ' and OriginalPostSeq=' + @OrigPostSeq + ' and OriginalPKey=''' + @OrigPkey + ''' '
	end
	print @sql
	execute(@sql)
END
GO
