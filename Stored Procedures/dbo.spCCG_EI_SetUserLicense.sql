SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_SetUserLicense] (@Username Nvarchar(32), @OkdLicense varchar(1))
WITH ENCRYPTION
AS
BEGIN
/*
	Copyright (c) 2017 Central Consulting Group. All rights reserved.
*/
	set nocount on
	BEGIN TRY
		if exists (select * from CCG_EI_User where Username=@Username)
			update CCG_EI_User set OkdLicense=@OkdLicense where Username=@Username

		else
			insert into CCG_EI_User (Username, OkdLicense) values (@Username, @OkdLicense)
		select 0 as Result, '' as ErrorMessage
	END TRY
	BEGIN CATCH
		/* SQL found some error - return the message and number: */
		select -1 as Result, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage
	END CATCH
END
GO
