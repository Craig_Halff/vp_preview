SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_LoadRoles] (@visionLanguage varchar(10))
AS
BEGIN
	/* Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved. */
	-- [spCCG_EI_LoadRoles] @visionLanguage = 'en-US'
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	select T1.Role, VisionField, IsNull(T2.ColumnLabel, '') as ColumnLabel,
			ColumnOrder, IsNull(T2.RoleDescription, '') as RoleDescription,
			MarkupColor, HighlightColor, HidePdfTools, Status, AllowMessaging,
			AllowInputColControls, AllowResetStageMenu, MessageDelay, EditAllAnnot,
			DocRights
	from CCG_EI_ConfigRoles T1 left join CCG_EI_ConfigRolesDescriptions T2 on T2.Role = T1.Role AND T2.UICultureName = @visionLanguage
	order by ColumnOrder
END
GO
