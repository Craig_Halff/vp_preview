SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[spCCG_ProjectCollectionsHistoryInsert] @WBS1 varchar (32), @PAResponsibleForCollections varchar (1)
AS 
/* 
Copyright (c) 2020 Central Consulting Group.  All rights reserved.
07/16/2020	David Springer
			Update all open Invoices in Colletions where PA to Follow Up or PM to Follow Up
			Populate UDIC_Collections History grid
			Call this from a Project CHANGE workflow when PA Response for Collections has changed
*/
BEGIN
SET NOCOUNT ON

	Insert Into UDIC_Collection_History
	(UDIC_UID, Seq, CustHistoryAction, CustHistoryActionStart, CustHistoryActionOwner, CustHistoryResponseNotes)
	Select UDIC_UID, Replace (NewID(), '-', '') Seq, CustRequestedAction, getDate(), CustRequestedActionOwner,
		'Collection responsibility changed from the ' + IIF (@PAResponsibleForCollections='Y','PM','PA') + ' to the ' + IIF (@PAresponsibleForCollections='Y','PA','PM')
	From UDIC_Collection
	Where CustProject = @WBS1
	  and CustCollectionStatus = 'Open'
	  and CustRequestedAction = IIF (@PAResponsibleForCollections='Y', 'PM to Follow Up', 'PA to Follow Up')

	Update UDIC_Collection
	Set CustRequestedAction = IIF (@PAResponsibleForCollections='Y','PA to Follow Up','PM to Follow Up'),
		CustRequestedActionOwner = IIF (@PAResponsibleForCollections='Y', CustBiller, CustProjMgr)
	Where CustProject = @WBS1
	  and CustCollectionStatus = 'Open'
	  and CustRequestedAction = IIF (@PAResponsibleForCollections='Y', 'PM to Follow Up', 'PA to Follow Up')
END
GO
