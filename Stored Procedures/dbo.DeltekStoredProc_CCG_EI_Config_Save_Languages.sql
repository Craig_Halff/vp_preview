SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Save_Languages]
	@Language_Updates	Nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sSQL Nvarchar(max);
	DECLARE @safeSql	int;

	-- Ensure that our inputs are safe (preventing SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<INSERT VALUES>', @Language_Updates);
	IF @safeSql <> 1 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS varchar(4));
		RETURN;
	END;

	SET @sSQL = N'
		UPDATE lang
			SET Label = vals.Label
			FROM CCG_Language_Labels lang
				INNER JOIN (
					VALUES ' + @Language_Updates + N'
				) vals (Label, Id, Product, UICultureName) On lang.Id = vals.Id
					AND lang.Product = vals.Product
					AND lang.UICultureName = vals.UICultureName';
	EXEC (@sSQL);
END;
GO
