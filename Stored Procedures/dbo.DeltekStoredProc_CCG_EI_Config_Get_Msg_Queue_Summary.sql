SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Get_Msg_Queue_Summary]
AS
BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_EI_Config_Get_Msg_Queue_Summary]
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT ToList, MIN(CreateDateTime) As OldestMessage, COUNT(*) As MessageCount
		FROM CCG_Email_Messages
		WHERE SourceApplication = 'Electronic Invoicing'
		GROUP BY ToList, SourceApplication
		ORDER BY 1;
END;
GO
