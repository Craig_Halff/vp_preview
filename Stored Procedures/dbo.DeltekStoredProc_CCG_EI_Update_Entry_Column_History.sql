SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Update_Entry_Column_History]
	@KEY_EMPLOYEEID			Nvarchar(32),
	@wbs1					Nvarchar(30),
	@wbs2					Nvarchar(30),
	@wbs3					Nvarchar(30),
	@columnDataType			varchar(100),
	@customColumnId			varchar(100),
	@customColumnLabel		varchar(100),
	@databaseField			varchar(500),
	@isDecimalValue			bit
AS
BEGIN
	/*
	DeltekStoredProc_CCG_EI_Update_Entry_Column_History
		@KEY_EMPLOYEEID = '00001',
		@wbs1 = '1999015.00',
		@wbs2 = ' ',
		@wbs3 = ' ',
		@columnDataType = 'D',
		@customColumnId = '404',
		@customColumnLabel = 'Hold Until Date',
		@databaseField = 'HoldUntilDate',
		@isDecimalValue = '0'
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	bit;

	-- Ensure that our inputs are safe (prevent SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @wbs1);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @wbs2);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @wbs3);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @KEY_EMPLOYEEID);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @columnDataType);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<FIELD>', @customColumnId);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @customColumnLabel);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<FIELD>', @databaseField);
	IF @safeSql = 0 BEGIN
		SELECT 'ERROR: Unsafe parameters used! Execution cancelled.';
		RETURN;
	END;

	SET @sSQL = N'
		INSERT INTO CCG_EI_HistoryUpdateColumns
			(ActionTaken, ActionDate, ActionTakenBy,
				WBS1, WBS2, WBS3,
				DataType, ConfigCustomColumn, ColumnLabel,
				OldValue, NewValue, OldValueDecimal, NewValueDecimal)
			SELECT TOP 1 ''Update'', getutcdate(), ''' + @KEY_EMPLOYEEID + N''',
				''' + @wbs1 + N''', ''' + @wbs2 + N''', ''' + @wbs3 + N''',
				''' + @columnDataType + N''', ' + @customColumnId + N', ''' + @customColumnLabel + N''',
				NULL, eicc.' + @databaseField + N',
				NULL, ' +
					CASE
						WHEN @isDecimalValue = 1 THEN N'eicc.' + @databaseField
						ELSE N'NULL'
					END + N'
			FROM CCG_EI_CustomColumns eicc
			WHERE WBS1 = ''' + @wbs1 + N''' and WBS2 = ''' + @wbs2 + N''' and WBS3 = ''' + @wbs3 + N'''';

	--PRINT @sSQL;
	EXEC (@sSQL);

END;
GO
