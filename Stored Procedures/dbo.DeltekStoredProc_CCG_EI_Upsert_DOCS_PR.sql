SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Upsert_DOCS_PR] ( @FILEID varchar(50), @WBS1 nvarchar(30), @WBS2 nvarchar(7), @WBS3 nvarchar(7), @Invoice nvarchar(32), @FilePath nvarchar(500), @FileDescription nvarchar(500), @FileSize int, @CreateUser nvarchar(32), @CreateDate datetime, @StorageSeq int, @StoragePathOrID nvarchar(500), @RevisionSeq int, @Filestate varchar(50), @Category varchar(50))
             AS EXEC spCCG_EI_Upsert_DOCS_PR @FILEID,@WBS1,@WBS2,@WBS3,@Invoice,@FilePath,@FileDescription,@FileSize,@CreateUser,@CreateDate,@StorageSeq,@StoragePathOrID,@RevisionSeq,@Filestate,@Category
GO
