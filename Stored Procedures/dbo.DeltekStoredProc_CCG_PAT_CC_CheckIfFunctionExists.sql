SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_CC_CheckIfFunctionExists] (
	@functionName		Nvarchar(500)
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- EXEC [DeltekStoredProc_CCG_PAT_CC_CheckIfFunctionExists] 'fnCCG_PAT_JTDBilledToClient'
	SELECT oname, otype,
            MAX(case when parameter_id = 0 and is_output = 1 then system_type_id else null end) as rtype,
            MAX(case when parameter_id = 1 and UPPER(name) = N'@PayableSeq' then system_type_id else null end) as param1,
            MAX(case when parameter_id = 2 and UPPER(name) = N'@WBS1' then system_type_id else null end) as param1,
            MAX(case when parameter_id = 3 and UPPER(name) = N'@WBS2' then system_type_id else null end) as param2,
            MAX(case when parameter_id = 4 and UPPER(name) = N'@WBS3' then system_type_id else null end) as param3
        FROM (
            SELECT o.name as oname, type as otype, p.name, p.parameter_id, p.system_type_id, p.is_output FROM
                sys.objects o, sys.parameters p
                WHERE o.object_id = p.object_id and
                    o.object_id = OBJECT_ID(N'[' + Replace(@functionName, N'dbo.', N'dbo].[') + N']')
        ) d
        GROUP BY oname, otype;
END


GO
