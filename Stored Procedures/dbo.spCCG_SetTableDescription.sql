SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_SetTableDescription] (
	@table nvarchar(400), @column nvarchar(400), @descr nvarchar(4000))
AS
BEGIN
	/*
	Copyright (c) 2017 Central Consulting Group. All rights reserved.
	SELECT * FROM CCG_PAT_Config
	EXEC spCCG_SetTableDescription 'CCG_PAT_Config', '', 'Configuration settings for the entire PAT application'
	EXEC spCCG_SetTableDescription 'CCG_PAT_Config', 'Version', 'PAT version number currently installed'

	SELECT * FROM sys.extended_properties ex
				INNER JOIN sys.all_objects obj ON obj.object_id = ex.major_id
				LEFT JOIN sys.all_columns col ON col.object_id = ex.major_id AND col.name = 'Version' AND ex.minor_id = col.column_id
	*/

	SET @column = replace(replace(rtrim(replace(ISNULL(@column,''),char(9),'')),'[',''),']','')

	DECLARE @tableColExists BIT =
		CASE WHEN EXISTS (SELECT 'x' FROM sys.all_objects obj
				LEFT JOIN sys.all_columns col ON col.name = @column and col.object_id = obj.object_id
			WHERE LOWER(obj.name) = LOWER(@table) AND (@column = '' OR LOWER(col.name) = LOWER(@column))) THEN 1
		ELSE 0 END
	IF @tableColExists = 0 BEGIN
		PRINT 'NO TABLE '+@table+' OR NO COLUMN '+@column+' - SKIPPED'
		RETURN
	END

	DECLARE @descrExists BIT =
		CASE WHEN EXISTS (SELECT 'x' FROM sys.extended_properties ex
				INNER JOIN sys.all_objects obj ON obj.object_id = ex.major_id
				LEFT JOIN sys.all_columns col ON col.object_id = ex.major_id AND col.name = @column AND ex.minor_id = col.column_id
			WHERE ex.name = 'MS_Description'
				AND LOWER(obj.name) = LOWER(@table) AND ((@column = '' AND minor_id = 0) OR (LOWER(col.name) = LOWER(@column) AND minor_id > 0))) THEN 1
		ELSE 0 END

	PRINT @descrExists

	IF @column <> '' BEGIN
		IF @descrExists = 1 BEGIN
			EXEC sp_updateextendedproperty
				@name = N'MS_Description' , @value = @descr,
				@level0type = N'Schema', @level0name = dbo ,
				@level1type = N'Table', @level1name = @table,
				@level2type = N'Column', @level2name = @column;
		END
		ELSE BEGIN
			EXEC sp_addextendedproperty
				@name = N'MS_Description', @value = @descr,
				@level0type = N'Schema', @level0name = dbo ,
				@level1type = N'Table', @level1name = @table ,
				@level2type = N'Column', @level2name = @column;
		END
	END
	ELSE BEGIN
		PRINT 'update table descr'

		IF @descrExists = 1 BEGIN
			EXEC sp_updateextendedproperty
				@name = N'MS_Description', @value = @descr,
				@level0type = N'Schema', @level0name = dbo ,
				@level1type = N'Table', @level1name = @table
		END
		ELSE BEGIN
			EXEC sp_addextendedproperty
				@name = N'MS_Description', @value = @descr,
				@level0type = N'Schema', @level0name = dbo ,
				@level1type = N'Table', @level1name = @table
		END
	END
END
GO
