SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_OpportunityPhaseValidationTasks] @OpportunityID varchar (32), @PhaseCode varchar (32)
AS
/*
Copyright 2017 Central Consulting Group.   All rights reserved.
03/10/2017 David Springer
           Call this from an Opportunity Phase CHANGE workflow when any Fee has changed.
*/
SET NOCOUNT ON
BEGIN
   If exists (Select 'x' From Opportunities_Tasks Where OpportunityID = @OpportunityID and CustTaskPhaseCode = @PhaseCode)
      RAISERROR ('Halff Custom Error:  Change Task fee, not Phase fee.                     ', 16, 1); -- 16 = severity; 1 = state

END
GO
