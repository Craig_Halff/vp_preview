SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_FormLookupWBS_LoadTreeNodes]
	@exactMatch		bit,
	@filter			Nvarchar(max),
	@statusFilter	Nvarchar(max),
	@projectFilter	Nvarchar(max),
	@VP2			bit = 0
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	DECLARE @sql Nvarchar(max)

	set @filter = replace(@filter, '''', '''''')

	IF @exactMatch = 1 AND ISNULL(@filter, '') <> ''
		SET @sql = N'
        SELECT WBS1, Name, ISNULL(Status, ''D''), Sublevel, ReadyForProcessing
			FROM PR
			WHERE WBS2 = N'' '' and WBS1 = N''' + ISNULL(@filter, N'') + ''' ' 
				+ (case when @VP2 = 1 then ' and (PR.Stage is null or PR.Stage in (select Code from CFGProjectStageData ps where ps.Step = ''W'')) ' else '' end) + '
			ORDER BY 1'
	ELSE 
		SET @sql = N'
        SELECT WBS1, Name, ISNULL(Status, ''D''), Sublevel, ReadyForProcessing
			FROM PR
			WHERE WBS2 = N'' '' and (WBS1 = N'''+ISNULL(@filter, N'')+''' OR (Status ' + @statusFilter + ')) ' +
				@projectFilter + ' ' 
				+ (case when @VP2 = 1 then ' and (PR.Stage is null or PR.Stage in (select Code from CFGProjectStageData ps where ps.Step = ''W'')) ' else '' end) + '
			ORDER BY 1'
	EXEC (@sql)
END
GO
