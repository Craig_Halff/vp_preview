SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FormLookupWBS_TreeGrid] ( @wbs1 nvarchar(100), @wbs2 nvarchar(100), @VP2 bit= 0)
             AS EXEC spCCG_PAT_FormLookupWBS_TreeGrid @wbs1,@wbs2,@VP2
GO
