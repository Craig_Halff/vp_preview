SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_GetDelegateForList] ( @Employee nvarchar(20), @IncludeSelf bit= 1)
             AS EXEC spCCG_PAT_GetDelegateForList @Employee,@IncludeSelf
GO
