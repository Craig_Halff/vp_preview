SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_ARM_Web_NValue]
	@FN			varchar(500),
	@ProcessId	varchar(15),
	@ActionId	varchar(15),
	@Invoice	varchar(12),
	@WBS1		varchar(37),
	@NumValues	int = 0,
	@DbParams	varchar(max) = ''
AS
BEGIN
	/*
	[DeltekStoredProc_CCG_ARM_Web_NValue]
		@FN='fnCCG_ARM_ARDetails',
		@ProcessId='1006',
		@ActionId='',
		@Invoice='',
		@WBS1=' ',
		@NumValues=0,
		@DbParams = '-1'
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		varchar(max);
	Set @DbParams = ISNULL(@DbParams, '-1');

	IF @Invoice <> 'null' SET @Invoice = ''''+@Invoice+''''
	IF @WBS1 <> 'null' SET @WBS1 = ''''+@WBS1+''''

	SET @sSQL = rtrim((CASE WHEN ISNULL(@ProcessId,'-1')<>'-1' THEN @ProcessId+', ' ELSE '' END) +
				(CASE WHEN ISNULL(@ActionId,'-1')<>'-1' THEN @ActionId+', ' ELSE '' END) +
				(CASE WHEN ISNULL(@Invoice,'')<>'' THEN @Invoice+', ' ELSE '' END) +
				(CASE WHEN ISNULL(@WBS1,'-1')<>'-1' THEN @WBS1+', ' ELSE '' END))

	SET @sSQL = @sSQL +
				(CASE WHEN LTRIM(ISNULL(@DbParams,'-1'))<>'-1' THEN SUBSTRING(RTRIM(@sSQL), 1, LEN(@sSQL)-1)+', ' ELSE '' END);

	SET @sSQL = '
		SELECT *
			FROM dbo.' + @FN + '(' + SUBSTRING(RTRIM(@sSQL), 1, LEN(@sSQL)-1) + ')
			ORDER BY TopOrder, Descr ';
	If @NumValues > 0 SET @sSQL = @sSQL + ', Value1';

	PRINT @sSQL;
	EXEC (@sSQL);
END;
GO
