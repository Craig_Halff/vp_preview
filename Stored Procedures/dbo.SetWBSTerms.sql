SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SetWBSTerms]
    @whereClause Nvarchar(max)
AS
BEGIN
   DECLARE @WBS1 Nvarchar(30)
   DECLARE @WBS2 Nvarchar(7)
   DECLARE @WBS3 Nvarchar(7)
   DECLARE @BillWBS1 Nvarchar(30)
   DECLARE @BillWBS2 Nvarchar(7)
   DECLARE @BillWBS3 Nvarchar(7)

   DECLARE @btmWBS1 Nvarchar(30)
   DECLARE @ConsolidatePrint Nvarchar(1)
   DECLARE @SeparateTerms Nvarchar(1)

   DECLARE @ProjWBS1 Nvarchar(30)
   DECLARE @ProjConsolidatePrint Nvarchar(1)
   DECLARE @ProjSubLevelTerms Nvarchar(1)
   DECLARE @ProjSubLevelRollup smallint
   DECLARE @ProjWBS3Rollup smallint
   DECLARE @ConsolidateWBS3 Nvarchar(1)

   DECLARE @PhaseWBS1 Nvarchar(30)
   DECLARE @PhaseWBS2 Nvarchar(7)
   DECLARE @PhaseConsolidatePrint Nvarchar(1)
   DECLARE @PhaseSubLevelTerms Nvarchar(1)
   DECLARE @PhaseSubLevelRollup smallint
   DECLARE @PhaseWBS3Rollup smallint

   DECLARE @TaskWBS1 Nvarchar(30)
   DECLARE @TaskWBS2 Nvarchar(7)
   DECLARE @TaskWBS3 Nvarchar(7)

   DECLARE @WBS2Length smallint
   DECLARE @WBS3Length smallint
   DECLARE @UseWBS1 Nvarchar(30)
   DECLARE @UseWBS2 Nvarchar(7)
   DECLARE @UseWBS3 Nvarchar(7)
   DECLARE @LastWBS1 Nvarchar(30)
   DECLARE @LastWBS2 Nvarchar(7)
   DECLARE @LastWBS3 Nvarchar(7)

   SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
   SET NOCOUNT ON

   IF @whereClause IS NOT NULL AND @whereClause <> ''
       SELECT @whereClause = ' AND ' + @whereClause
   ELSE
       SELECT @whereClause = ''

   SELECT @WBS2Length = WBS2Length FROM CFGFormat
   SELECT @WBS3Length = WBS3Length FROM CFGFormat

execute('   DECLARE Projects INSENSITIVE CURSOR FOR ' +
'	SELECT LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3, LEVEL3.BillWBS1, LEVEL3.BillWBS2, LEVEL3.BillWBS3, ' +
'		BTBG.ConsolidatePrint, BTBG.SeparateTerms,  ' +
'               BTProj.ConsolidatePrinting AS ProjConsolidatePrint, BTProj.ConsolidateWBS3,  ' +
'		BTProj.SubLevelTerms AS ProjSubLevelTerms, BTProj.SubLevelRollup AS ProjSubLevelRollup, ' +
'		BTProj.WBS3Rollup AS ProjWBS3Rollup, BTPhase.ConsolidatePrinting AS PhaseConsolidatePrint,  ' +
'		BTPhase.SubLevelTerms AS PhaseSubLevelTerms, BTPhase.SubLevelRollup AS PhaseSubLevelRollup,  ' +
'		BTPhase.WBS3Rollup AS PhaseWBS3Rollup, ' +
'		BTMain.WBS1 AS btmWBS1,  ' +
'		BTProj.WBS1 AS ProjWBS1,  ' +
'		BTPhase.WBS1 AS PhaseWBS1,  ' +
'		BTPhase.WBS2 AS PhaseWBS2,  ' +
'		BTTask.WBS1 AS TaskWBS1,  ' +
'		BTTask.WBS2 AS TaskWBS2, ' +
'		BTTask.WBS3 AS TaskWBS3 ' +
'	FROM (((((((((PR AS LEVEL3 WITH (NOLOCK) ' +
'		INNER JOIN PR AS LEVEL2 WITH (NOLOCK) ON LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2) ' +
'		INNER JOIN PR WITH (NOLOCK) ON PR.WBS1 = LEVEL3.WBS1) ' +
'		LEFT JOIN CL WITH (NOLOCK) ON PR.ClientID = CL.ClientID) ' +
'		LEFT JOIN CL AS CLBill WITH (NOLOCK) ON PR.BillingClientID = CLBill.ClientID) ' +
'		LEFT JOIN BT AS BTTask WITH (NOLOCK) ON LEVEL3.WBS1 = BTTask.WBS1 AND LEVEL3.WBS2 = BTTask.WBS2 AND LEVEL3.WBS3 = BTTask.WBS3) ' +
'		LEFT JOIN BT AS BTPhase WITH (NOLOCK) ON LEVEL3.WBS1 = BTPhase.WBS1 AND LEVEL3.WBS2 = BTPhase.WBS2) ' +
'		LEFT JOIN BT AS BTProj WITH (NOLOCK) ON LEVEL3.WBS1 = BTProj.WBS1) ' +
'		LEFT JOIN BTBGSubs WITH (NOLOCK) ON LEVEL3.WBS1 = BTBGSubs.SubWBS1) ' +
'		LEFT JOIN BTBG WITH (NOLOCK) ON BTBGSubs.MainWBS1 = BTBG.MainWBS1) ' +
'		LEFT JOIN BT AS BTMain WITH (NOLOCK) ON BTBG.MainWBS1 = BTMain.WBS1 ' +
'		LEFT JOIN PR AS MainWBS1PR WITH (NOLOCK) ON BTBGSubs.MainWBS1 = MainWBS1PR.WBS1 AND N'' '' = MainWBS1PR.WBS2 AND N'' '' = MainWBS1PR.WBS3 ' +
'	WHERE (LEVEL3.SubLevel = ''N'' OR (LEVEL3.WBS2 <> N'' '' AND LEVEL3.WBS3 <> N'' '')) ' +
'               AND (PR.WBS2 = N'' '' AND PR.WBS3 = N'' '' AND LEVEL2.WBS3 = N'' '') ' +
'		AND (BTPhase.WBS3 = N'' '' OR BTPhase.WBS3 IS NULL) ' +
'		AND ((BTProj.WBS2 = N'' '' AND BTProj.WBS3 = N'' '') OR (BTProj.WBS2 IS NULL AND BTProj.WBS3 IS NULL)) ' +
'		AND ((BTMain.WBS2 = N'' '' AND BTMain.WBS3 = N'' '') OR (BTMain.WBS2 IS NULL AND BTMain.WBS3 IS NULL)) ' + @whereClause +
'	ORDER BY LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3')

   OPEN Projects

   FETCH NEXT FROM Projects INTO @WBS1, 
                                 @WBS2, 
				 @WBS3,
                                 @BillWBS1, 
                                 @BillWBS2,
				 @BillWBS3,
                                 @ConsolidatePrint,
                                 @SeparateTerms,
                                 @ProjConsolidatePrint,
                                 @ConsolidateWBS3,
                                 @ProjSubLevelTerms,
                                 @ProjSubLevelRollup,
				 @ProjWBS3Rollup,
                                 @PhaseConsolidatePrint,
                                 @PhaseSubLevelTerms,
                                 @PhaseSubLevelRollup,
				 @PhaseWBS3Rollup,
                                 @btmWBS1,
                                 @ProjWBS1,
                                 @PhaseWBS1,
                                 @PhaseWBS2,
                                 @TaskWBS1,
                                 @TaskWBS2,
                                 @TaskWBS3

   WHILE @@FETCH_STATUS = 0
      BEGIN
         SELECT @UseWBS1 = NULL
         SELECT @UseWBS2 = NULL
         SELECT @UseWBS3 = NULL

         IF (@btmWBS1 IS NULL OR ( @ConsolidatePrint = 'N' AND @SeparateTerms = 'Y' AND @ProjWBS1 IS NOT NULL))
           BEGIN
             SELECT @UseWBS1 = @ProjWBS1
	     IF (@ProjConsolidatePrint = 'Y' OR @ProjSubLevelTerms = 'N')
	       BEGIN
             	 SELECT @UseWBS2 = ' '
		 SELECT @UseWBS3 = ' '
	       END
	     ELSE
	       BEGIN
		 IF @ProjSubLevelRollup > 0 AND @ProjSubLevelRollup < @WBS2Length AND @LastWBS1 = @WBS1 AND
		 SUBSTRING(@LastWBS2, 1, @ProjSubLevelRollup) = SUBSTRING(@WBS2, 1, @ProjSubLevelRollup)
		   BEGIN
		     SELECT @UseWBS2 = @LastWBS2
		     SELECT @UseWBS3 = @LastWBS3
		   END
		 ELSE
		   SELECT @UseWBS2 = IsNull(@PhaseWBS2, ' ')
		 IF (@ProjSubLevelRollup > 0 OR @PhaseConsolidatePrint = 'Y' OR @PhaseSubLevelTerms = 'N' OR @ConsolidateWBS3 = 'Y')
		   SELECT @USEWBS3 = ' '
		 ELSE	IF (@UseWBS3 IS NULL)
			   BEGIN
			     IF @PhaseSubLevelRollup > 0 AND @PhaseSubLevelRollup < @WBS3Length AND @LastWBS1 = @WBS1 AND @LastWBS2 = @WBS2 AND
			     SUBSTRING(@LastWBS3, 1, @ProjSubLevelRollup) = SUBSTRING(@WBS3, 1, @ProjSubLevelRollup)
			       SELECT @UseWBS3 = @LastWBS3
			     ELSE
			       SELECT @UseWBS3 = IsNull(@TaskWBS3, ' ')
			   END
	      END
           END
         ELSE
	   BEGIN
	     SELECT @UseWBS1 = @btmWBS1
	     SELECT @UseWBS2 = ' '
	     SELECT @UseWBS3 = ' '
	   END

	   IF @UseWBS1 IS NULL AND @BillWBS1 IS NOT NULL
          UPDATE PR SET PR.BillWBS1 = NULL, PR.BillWBS2 = NULL, PR.BillWBS3 = NULL
           WHERE PR.WBS1 = @WBS1 AND PR.WBS2 = @WBS2 AND PR.WBS3 = @WBS3

       IF @UseWBS1 IS NOT NULL AND (@UseWBS1 <> IsNull(@BillWBS1, ' ') OR @UseWBS2 <> IsNull(@BillWBS2, ' ') OR @UseWBS3 <> IsNull(@BillWBS3, ' '))
          UPDATE PR SET PR.BillWBS1 = @UseWBS1, PR.BillWBS2 = @UseWBS2, PR.BillWBS3 = @UseWBS3
           WHERE PR.WBS1 = @WBS1 AND PR.WBS2 = @WBS2 AND PR.WBS3 = @WBS3
         
         SELECT @LastWBS1 = @UseWBS1
         SELECT @LastWBS2 = @UseWBS2
         SELECT @LastWBS3 = @UseWBS3

         FETCH NEXT FROM Projects INTO @WBS1, 
                                       @WBS2, 
                                       @WBS3,
                                       @BillWBS1, 
                                       @BillWBS2,
                                       @BillWBS3,
                                       @ConsolidatePrint,
                                       @SeparateTerms,
                                       @ProjConsolidatePrint,
                                       @ConsolidateWBS3,
                                       @ProjSubLevelTerms,
                                       @ProjSubLevelRollup,
                                       @ProjWBS3Rollup,
                                       @PhaseConsolidatePrint,
                                       @PhaseSubLevelTerms,
                                       @PhaseSubLevelRollup,
                                       @PhaseWBS3Rollup,
                                       @btmWBS1,
                                       @ProjWBS1,
                                       @PhaseWBS1,
                                       @PhaseWBS2,
                                       @TaskWBS1,
                                       @TaskWBS2,
                                       @TaskWBS3
      END

   CLOSE Projects
   DEALLOCATE Projects
END
GO
