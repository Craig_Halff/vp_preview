SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[spCCG_ClientPerDiem] @ClientID varchar (32), @PerDiem varchar (255)
AS 
/* 
Copyright (c) 2019 Central Consulting Group.  All rights reserved.
08/15/2019 David Springer
           Update project Per Diem Reimbursed for active regular projects
*/
BEGIN
SET NOCOUNT ON

	Update px
	Set px.CustPerDiemReimbursed = @PerDiem
	From PR p, ProjectCustomTabFields px
	Where p.WBS2 = ' '
	  and p.ChargeType = 'R'
	  and p.Status = 'A'
	  and p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.BillingClientID = @ClientID

END
GO
