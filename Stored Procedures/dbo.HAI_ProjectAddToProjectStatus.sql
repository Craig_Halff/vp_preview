SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[HAI_ProjectAddToProjectStatus] @myWBS1 AS VARCHAR(30)
AS
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    BEGIN
        DECLARE @TeamLength    INT
              , @OfficeLength  INT
              , @CompanyLength INT;

        SELECT @TeamLength    = Org3Length
             , @OfficeLength  = Org2Length
             , @CompanyLength = Org1Length
        FROM VPTest.dbo.CFGFormat;

        DECLARE @modUser VARCHAR(30) = (
                    SELECT TOP 1 ModUser FROM dbo.Projects_CustPhaseTeams ORDER BY ModDate DESC
                );
        ----------------------------------------------------------
        -- PhaseTeam Labor
        ----------------------------------------------------------
        INSERT INTO DataMart.dbo.HAI_ProjectStatus (
            WBS1
          , WBS2
          , TeamNum
          , GroupingType
          , TotalCompensation
          , EACInputLevel
          , EAC
          , ETC
          , ETCRemainder
          , StartDate
          , EndDate
          , SpreadType
          , LastModified
          , LastModifiedUserID
        )
        SELECT pht.WBS1
             , pht.CustTeamPhaseCode
             , RIGHT(pht.CustTeamOrg, @TeamLength)
             , 'Labor'
             , pht.CustTeamLaborFee
             , 'Phase'
             , pht.CustTeamLaborFee
             , 0.0
             , 0.0
             , ph.CustPhaseStartDate
             , ph.CustPhaseEndDate
             , ph.CustPhaseInitialDistribution
             , GETDATE()
             , @modUser
        FROM dbo.Projects_CustPhaseTeams   AS pht
            INNER JOIN dbo.Projects_Phases AS ph
                ON pht.WBS1 = pht.WBS1
                    AND pht.CustTeamPhaseCode = ph.CustPhaseCode
        WHERE pht.WBS1 = @myWBS1;


        ----------------------------------------------------------
        -- PhaseTeam Expense & Consultant
        ----------------------------------------------------------
        INSERT INTO DataMart.dbo.HAI_ProjectStatus (
            WBS1
          , WBS2
          , TeamNum
          , GroupingType
          , TotalCompensation
          , EACInputLevel
          , EAC
          , ETC
          , ETCRemainder
          , StartDate
          , EndDate
          , SpreadType
          , LastModifiedUserID
          , LastModified
        )
        SELECT ph.WBS1
             , ph.CustPhaseCode
             , NULL
             , 'Expense & Consultant'
             , ISNULL(ph.CustPhaseExpenseFee, 0.0) + ISNULL(ph.CustPhaseConsultantFee, 0.0)
             , 'Phase'               AS EACInputLevel
             , ISNULL(ph.CustPhaseExpenseFee, 0.0) + ISNULL(ph.CustPhaseConsultantFee, 0.0)
             , 0.0
             , 0.0
             , ph.CustPhaseStartDate
             , ph.CustPhaseEndDate
             , ph.CustPhaseInitialDistribution
             , @modUser
             , GETDATE()
        FROM dbo.Projects_Phases AS ph
        WHERE
            ph.WBS1 = @myWBS1
            AND ISNULL(ph.CustPhaseExpenseFee, 0.0) + ISNULL(ph.CustPhaseConsultantFee, 0.0) <> 0;
        SELECT ph.WBS1
             , ph.CustPhaseCode
             , NULL
             , 'Expense & Consultant'
             , ISNULL(ph.CustPhaseExpenseFee, 0.0) + ISNULL(ph.CustPhaseConsultantFee, 0.0)
             , 'Phase'               AS EACInputLevel
             , ISNULL(ph.CustPhaseExpenseFee, 0.0) + ISNULL(ph.CustPhaseConsultantFee, 0.0)
             , 0.0
             , 0.0
             , ph.CustPhaseStartDate
             , ph.CustPhaseEndDate
             , ph.CustPhaseInitialDistribution
             , @modUser
             , GETDATE()
        FROM dbo.Projects_Phases AS ph
        WHERE
            ph.WBS1 = @myWBS1
            AND ISNULL(ph.CustPhaseExpenseFee, 0.0) + ISNULL(ph.CustPhaseConsultantFee, 0.0) <> 0;
        ----------------------------------------------------------
        -- Phase 
        ----------------------------------------------------------
        INSERT INTO DataMart.dbo.HAI_ProjectStatus (
            WBS1
          , WBS2
          , TeamNum
          , GroupingType
          , TotalCompensation
          , EACInputLevel
          , EAC
          , ETC
          , ETCRemainder
          , StartDate
          , EndDate
          , SpreadType
          , LastModifiedUserID
          , LastModified
        )
        SELECT ph.WBS1
             , ph.CustPhaseCode
             , NULL
             , NULL
             , ISNULL(ph.CustPhaseExpenseFee, 0.0) + ISNULL(ph.CustPhaseConsultantFee, 0.0)
               + ISNULL(ph.CustPhaseFeeDirLab, 0.0)
             , 'Phase' AS EACInputLevel
             , ISNULL(ph.CustPhaseExpenseFee, 0.0) + ISNULL(ph.CustPhaseConsultantFee, 0.0)
               + ISNULL(ph.CustPhaseFeeDirLab, 0.0)
             , 0.0
             , 0.0
             , ph.CustPhaseStartDate
             , ph.CustPhaseEndDate
             , ph.CustPhaseInitialDistribution
             , @modUser
             , GETDATE()
        FROM dbo.Projects_Phases AS ph
        WHERE
            ph.WBS1 = @myWBS1
            AND ISNULL(ph.CustPhaseExpenseFee, 0.0) + ISNULL(ph.CustPhaseConsultantFee, 0.0)
                + ISNULL(ph.CustPhaseFeeDirLab, 0.0) <> 0;

        ----------------------------------------------------------
        -- ProjectTeam Labor and Expense & Consultant
        ----------------------------------------------------------
        INSERT INTO DataMart.dbo.HAI_ProjectStatus (
            WBS1
          , WBS2
          , TeamNum
          , GroupingType
          , TotalCompensation
          , EACInputLevel
          , EAC
          , ETC
          , ETCRemainder
          , StartDate
          , EndDate
          , SpreadType
          , LastModified
          , LastModifiedUserID
        )
        SELECT ps.WBS1
             , ' '
             , ps.TeamNum
             , ps.GroupingType
             , SUM(ps.TotalCompensation)
             , 'Phase' AS asEACInputLevel
             , SUM(ps.EAC)
             , SUM(ps.ETC)
             , SUM(ps.ETCRemainder)
             , MIN(ps.StartDate)
             , MAX(ps.EndDate)
             , 'Linear'
             , MAX(ps.LastModified)
             , @modUser
        FROM DataMart.dbo.HAI_ProjectStatus ps
        WHERE
            ps.WBS1 = @myWBS1
            AND ps.WBS2 <> ' '
            AND ps.GroupingType IS NOT NULL
            AND ps.GroupingType IN ( 'Labor', 'Expense & Consultant' )
        GROUP BY
            ps.WBS1
          , ps.TeamNum
          , ps.GroupingType
          , ps.EACInputLevel
          , ps.StartDate
          , ps.EndDate
          , ps.SpreadType
          , ps.LastModifiedUserID;

        ----------------------------------------------------------
        -- Project
        ----------------------------------------------------------
        PRINT 'Project';
        INSERT INTO DataMart.dbo.HAI_ProjectStatus (
            WBS1
          , WBS2
          , TeamNum
          , GroupingType
          , TotalCompensation
          , EACInputLevel
          , EAC
          , ETC
          , ETCRemainder
          , StartDate
          , EndDate
          , SpreadType
          , LastModified
          , LastModifiedUserID
        )
        SELECT ps.WBS1
             , ' '
             , NULL
             , NULL
             , SUM(ps.TotalCompensation)
             , 'Phase' AS EACInputLevel
             , SUM(ps.EAC)
             , SUM(ps.ETC)
             , SUM(ps.ETCRemainder)
             , MIN(ps.StartDate)
             , MAX(ps.EndDate)
             , 'Linear'
             , MAX(ps.LastModified)
             , @modUser
        FROM DataMart.dbo.HAI_ProjectStatus ps
        WHERE
            ps.WBS1 = @myWBS1
            AND ps.WBS2 = ' '
            AND ps.GroupingType IS NOT NULL
            AND ps.GroupingType IN ( 'Labor', 'Expense & Consultant' )
        GROUP BY
            ps.WBS1
          , ps.SpreadType
          , ps.LastModifiedUserID;
    END;
GO
