SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_OpportunityRFQGreaterThanStart]
    @OpportunityID VARCHAR(32), @RFQDate DATETIME, @EstStartDate DATETIME
AS
/* =======================================================================================
	Copyright (c) 2020  Halff Associates, Inc.  All rights reserved.
	
	20200704 - Craig H. Anderson
			RFQ Deadline must be less than Est. Start Date.
   ======================================================================================= */
SET NOCOUNT ON;
    BEGIN
        IF @RFQDate >= @EstStartDate
            RAISERROR('RFQ Deadline must be less than Est. Start Date.                        ', 16, 1);
    END;
GO
