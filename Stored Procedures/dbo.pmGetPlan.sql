SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[pmGetPlan]
  @strPlanID VARCHAR(32),
  @strETCDate VARCHAR(10), -- Date must be in format: 'yyyy-mm-dd'
  @strRetrievalMode VARCHAR(1) = '3'
AS

BEGIN -- Procedure pmGetPlan

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  
  DECLARE @startTime datetime
  DECLARE @totalTime int
  
  SET @startTime = GetDate()
  
  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  
  -- Put all work after this point so that we can calculate execution time.
  
  DECLARE @strMultiCompanyEnabled VARCHAR(1)
  
  DECLARE @intHrDecimals int
  DECLARE @intQtyDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intRtCostDecimals int
  DECLARE @intRtBillDecimals int
  DECLARE @intLabRevDecimals int
  DECLARE @intECURevDecimals int

  DECLARE @sintAmtDecimals smallint
    
  DECLARE @strActiveCompany Nvarchar(14)
  DECLARE @strUserName Nvarchar(32)

  DECLARE @strExpTab varchar(1)
  DECLARE @strConTab varchar(1)
  DECLARE @strUntTab varchar(1)

  DECLARE @strPlanCompany Nvarchar(14)
  DECLARE @strBudgetType varchar(1)

  DECLARE @siGRMethod smallint
  DECLARE @intGRBillTableNo int
  DECLARE @intGenResTableNo int
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled Flag.
  
  SELECT @strMultiCompanyEnabled = MultiCompanyEnabled FROM FW_CFGSystem
            
  -- Save Active Company string for use later.
  
  SET @strActiveCompany = dbo.GetActiveCompany()
  SET @strUserName = dbo.GetVisionAuditUserName()
          
  -- Get decimal settings.
  
  SELECT @intHrDecimals = HrDecimals,
         @intQtyDecimals = QtyDecimals,
         @intAmtCostDecimals = AmtCostDecimals,
         @intAmtBillDecimals = AmtBillDecimals,
         @intRtCostDecimals = RtCostDecimals,
         @intRtBillDecimals = RtBillDecimals,
         @intLabRevDecimals = LabRevDecimals, 
         @intECURevDecimals = ECURevDecimals
    FROM dbo.RP$tabDecimals(@strPlanID)
    
  -- Get Plan's settings.
  
  SELECT 
    @strPlanCompany = Company,
    @strBudgetType = BudgetType,
    @siGRMethod = GRMethod,
    @intGRBillTableNo = GRBillTableNo,
    @intGenResTableNo = GenResTableNo
    FROM RPPlan WHERE PlanID = @strPlanID
    
  -- Get flags to determine which features are being used.
  
  SELECT
     @strExpTab = ExpTab,
     @strConTab = ConTab,
     @strUntTab = UntTab,
     @sintAmtDecimals = AmtDecimals
     FROM CFGResourcePlanning
     WHERE CFGResourcePlanning.Company = @strActiveCompany
     
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   

  -- Get AccordionFormat.

  SELECT 1 AS Tag, NULL AS Parent,
    AccordionFormatID AS [AF!1!AccordionFormatID!Element], 
    PlanID AS [AF!1!PlanID!Element], 
    REPLACE(CONVERT(VARCHAR, StartDate, 121), ' ', 'T') AS [AF!1!StartDate!Element], 
    REPLACE(CONVERT(VARCHAR, EndDate, 121), ' ', 'T') AS [AF!1!EndDate!Element], 
    MajorScale AS [AF!1!MajorScale!Element], 
    MinorScale AS [AF!1!MinorScale!Element]
    FROM RPAccordionFormat AS AF
    WHERE PlanID = @strPlanID
    ORDER BY StartDate 
    FOR XML EXPLICIT

  -- Get WBSLevelFormat

  SELECT 1 AS Tag, NULL AS Parent,
    WBSFormatID AS [LF!1!WBSFormatID!Element], 
    PlanID AS [LF!1!PlanID!Element], 
    FmtLevel AS [LF!1!FmtLevel!Element], 
    WBSType AS [LF!1!WBSType!Element], 
    WBSMatch  AS [LF!1!WBSMatch!Element]
    FROM RPWBSLevelFormat AS LF
    WHERE PlanID = @strPlanID
    ORDER BY FmtLevel 
    FOR XML EXPLICIT    

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -->>> Get CalendarInterval.

  SELECT 1 AS Tag, NULL AS Parent,
    CONVERT(INT, 0) AS [CI!1!SD],
    CONVERT(INT, DATEADD(d, -1, MIN(StartDate))) AS [CI!1!ED],
    '' AS [CI!1!TL],
    '' AS [CI!1!BL],
    'o' AS [CI!1!TS],
    'o' AS [CI!1!BS]
    FROM RPCalendarInterval 
    WHERE PlanID = @strPlanID
    HAVING MIN(StartDate) > 0
  UNION ALL
  SELECT 1 AS Tag, NULL AS Parent,
    CONVERT(INT, CI.StartDate) AS [CI!1!SD], 
    CONVERT(INT, CI.EndDate) AS [CI!1!ED], 
    CASE 
      WHEN MajorScale = 'a' AND (CS.Label IS NULL OR CE.Label IS NULL) 
        THEN (CASE WHEN MONTH(CI.StartDate) = MONTH(CI.EndDate) AND  YEAR(CI.StartDate) = YEAR(CI.EndDate) 
                     THEN LEFT(DATENAME(m, CI.StartDate), 3) + ' ' + DATENAME(yyyy, CI.StartDate) 
                   WHEN MONTH(CI.StartDate) != MONTH(CI.EndDate) AND YEAR(CI.StartDate) = YEAR(CI.EndDate) 
                     THEN LEFT(DATENAME(m, CI.StartDate), 3) + '-' + LEFT(DATENAME(m, CI.EndDate), 3) + '''' +  RIGHT(YEAR(CI.StartDate), 2) 
                   WHEN MONTH(CI.StartDate) != MONTH(CI.EndDate) AND  YEAR(CI.StartDate) != YEAR(CI.EndDate) 
                     THEN LEFT(DATENAME(m, CI.StartDate), 3) + '''' + RIGHT(DATENAME(yyyy, CI.StartDate), 2) +  '-' + LEFT(DATENAME(m, CI.EndDate), 3) + '''' + RIGHT(DATENAME(yyyy, CI.EndDate), 2) END) 
      WHEN MajorScale = 'a' AND CS.EndDate < CI.EndDate 
        THEN ISNULL(CS.Label, CONVERT(VARCHAR, CI.StartDate, 2)) + ' - ' + ISNULL(CE.Label, CONVERT(VARCHAR, CI.EndDate, 2)) 
      WHEN MajorScale = 'a' AND CS.EndDate >= CI.EndDate 
        THEN ISNULL(CS.Label, CONVERT(VARCHAR, CI.StartDate, 2)) 
    END AS [CI!1!TL], 
    CASE 
      WHEN MinorScale = 'a'
        THEN ISNULL(CS.Label, CONVERT(VARCHAR, CI.StartDate, 2)) 
        ELSE '' 
    END AS [CI!1!BL], 
    F.MajorScale AS [CI!1!TS],
    F.MinorScale AS [CI!1!BS]
    FROM RPCalendarInterval AS CI 
    LEFT JOIN RPAccordionFormat AS F ON CI.AccordionFormatID = F.AccordionFormatID 
    LEFT JOIN CFGAcctngCalendar AS CS ON (CS.StartDate <= CI.StartDate AND CS.EndDate >= CI.StartDate) 
    LEFT JOIN CFGAcctngCalendar AS CE ON (CE.StartDate <= CI.EndDate AND CE.EndDate >= CI.EndDate) 
    WHERE CI.PlanID = @strPlanID
  UNION ALL
  SELECT   1 AS Tag, NULL AS Parent,
    CONVERT(INT, DATEADD(d, 1, MAX(EndDate))) AS [CI!1!SD],
    CONVERT(INT, DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate)))) AS [CI!1!ED],
    '' AS [CI!1!TL],
    '' AS [CI!1!BL],
    'o' AS [CI!1!TS],
    'o' AS [CI!1!BS]
    FROM RPCalendarInterval WHERE PlanID = @strPlanID
    GROUP BY PlanID
    ORDER BY [CI!1!SD] 
    FOR XML EXPLICIT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get Task Dependencies.

  SELECT 1 AS Tag, NULL AS Parent,
    DependencyID AS [TD!1!DependencyID!Element], 
    PlanID AS [TD!1!PlanID!Element], 
    TaskID AS [TD!1!TaskID!Element], 
    PredecessorID AS [TD!1!PredecessorID!Element], 
    DependencyType AS [TD!1!DependencyType!Element],
    LagValue AS [TD!1!LagValue!Element],
    LagScale AS [TD!1!LagScale!Element]
    FROM RPDependency AS TD
    WHERE PlanID = @strPlanID
    FOR XML EXPLICIT    

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
  -->>> Task.
  
  SELECT 1 AS Tag, NULL AS Parent,
    T.TaskID AS [T!1!I],
    T.OutlineNumber AS [T!1!ON],
    T.ParentOutlineNumber AS [T!1!PON],
    REPLACE(ISNULL(T.Name, ''), '"', '\"') + '|' + 
    ISNULL(T.WBSType, '') + '|' +  
    ISNULL(T.WBS1, '') + '|' +  
    ISNULL(T.WBS2, '') + '|' +  
    ISNULL(T.WBS3, '') + '|' +  
    ISNULL(T.LaborCode, '') + '|' +  
    ISNULL(CONVERT(VARCHAR, CONVERT(INT, T.StartDate)), '') + '|' +  
    ISNULL(CONVERT(VARCHAR, CONVERT(INT, T.EndDate)), '') + '|' +  
    LTRIM(STR(T.CostRate, 19, @intRtCostDecimals)) + '|' +  
    LTRIM(STR(T.BillingRate, 19, @intRtBillDecimals)) + '|' +  
    '|' + -- PlannedLaborHrs
    '|' + -- PlannedLabCost
    '|' + -- PlannedLabBill
    LTRIM(STR(T.PctCompleteLabCost, 19, 2)) + '|' +
    LTRIM(STR(T.PctCompleteLabBill, 19, 2)) + '|' +
    '|' + -- JTDLabHrs
    '|' + -- JTDLabCost
    '|' + -- JTDLabBill
    '|' + -- ETCLabHrs
    '|' + -- ETCLabCost
    '|' + -- ETCLabBill 
    '|' + -- EACLabHrs
    '|' + -- EACLabCost
    '|' + -- EACLabBill
    '|' + -- VarLabHrs
    '|' + -- VarLabCost
    '|' + -- VarLabBill
    LTRIM(STR(T.BaselineLaborHrs, 19, @intHrDecimals)) + '|' +  
    LTRIM(STR(T.BaselineLabCost, 19, @intAmtCostDecimals)) + '|' +  
    LTRIM(STR(T.BaselineLabBill, 19, @intAmtBillDecimals)) + '|' +  
    '|' + -- LabRevenue
    '|' + -- CompensationFee
    '|' + -- CompensationFeeBill 
    '|' + -- ConsultantFee
    '|' + -- ConsultantFeeBill 
    '|' + -- CompensationFeeDirLab
    '|' + -- CompensationFeeDirLabBill
    '|' + -- CompensationFeeDirExp 
    '|' + -- CompensationFeeDirExpBill
    '|' + -- ReimbAllowance
    '|' + -- ReimbAllowanceBill
    '|' + -- ReimbAllowanceExp
    '|' + -- ReimbAllowanceExpBill
    '|' + -- ReimbAllowanceCon 
    '|' + -- ReimbAllowanceConBill
    '|' + -- CCRCost
    '|' + -- CCRBill
    '|' + -- WeightedLabCost
    '|' + -- WeightedLabBill
    '|' + -- BaselineStart
    '|' + -- BaselineFinish
    '|' + -- ResourceID
    '|' + -- GenericResourceID
    '|' + -- Category
    '|' + -- GRLBCD
    '|' + -- emBillingCategory
    '|' + -- emProvCostRate
    '|' + -- emProvBillRate
    '|' + -- emCurrencyCode
    CONVERT(VARCHAR, T.OutlineLevel) + '|' + 
    ISNULL(T.OutlineNumber, '') + '|' + 
    ISNULL(T.ParentOutlineNumber, '') + '|' + 
    CONVERT(VARCHAR, T.ChildrenCount) + '|' + 
    '|' + -- SortSeq
    ISNULL(T.LabParentState, 'N') + '|' + 
    ISNULL(T.ExpParentState, 'N') + '|' + 
    ISNULL(T.ConParentState, 'N') + '|' + 
    ISNULL(T.UntParentState, 'N') + '|' + 
    ISNULL(T.LabVState, 'Y') + '|' + 
    ISNULL(T.ExpVState, 'Y') + '|' + 
    ISNULL(T.ConVState, 'Y') + '|' + 
    ISNULL(T.UntVState, 'Y') + '|' + 
    '|' + -- UnmatchedFlg
    CASE TD.Flag
      WHEN -1 THEN NCHAR(10003)
      WHEN 0 THEN NCHAR(10003)
      WHEN 1 THEN NCHAR(10003)
      ELSE ''
    END + '|' + -- DependencyFlg 
    '|' + -- Account
    '|' + -- veName
    '|' + -- PlannedExpCost
    LTRIM(STR(T.ExpBillRate, 19, 4)) + '|' +  
    '|' + -- PlannedExpBill
    LTRIM(STR(T.PctCompleteExpCost, 19, 2)) + '|' +
    LTRIM(STR(T.PctCompleteExpBill, 19, 2)) + '|' +
    '|' + -- JTDExpCost
    '|' + -- JTDExpBill
    '|' + -- ActualDirExpCost
    '|' + -- ActualDirExpBill
    '|' + -- ETCExpCost
    '|' + -- ETCExpBill
    '|' + -- ETCDirExpCost
    '|' + -- ETCDirExpBill
    '|' + -- EACExpCost
    '|' + -- EACExpBill
    '|' + -- VarExpCost
    '|' + -- VarExpBill
    LTRIM(STR(T.BaselineExpCost, 19, @intAmtCostDecimals)) + '|' +  
    LTRIM(STR(T.BaselineExpBill, 19, @intAmtBillDecimals)) + '|' +  
    '|' + -- ExpRevenue
    '|' + -- PlannedDirExpCost
    '|' + -- PlannedDirExpBill
    '|' + -- WeightedExpCost
    '|' + -- WeightedExpBill
    '|' + -- DirectAcctFlg
    '|' + -- Vendor
    '|' + -- PlannedConCost
    LTRIM(STR(T.ConBillRate, 19, 4)) + '|' +  
    '|' + -- PlannedConBill
    LTRIM(STR(T.PctCompleteConCost, 19, 2)) + '|' +
    LTRIM(STR(T.PctCompleteConBill, 19, 2)) + '|' +
    '|' + -- JTDConCost
    '|' + -- JTDConBill
    '|' + -- ActualDirConCost
    '|' + -- ActualDirConBill
    '|' + -- ETCConCost
    '|' + -- ETCConBill
    '|' + -- ETCDirConCost
    '|' + -- ETCDirConBill
    '|' + -- EACConCost
    '|' + -- EACConBill
    '|' + -- VarConCost
    '|' + -- VarConBill
    LTRIM(STR(T.BaselineConCost, 19, @intAmtCostDecimals)) + '|' +  
    LTRIM(STR(T.BaselineConBill, 19, @intAmtBillDecimals)) + '|' +  
    '|' + -- ConRevenue
    '|' + -- PlannedDirConCost
    '|' + -- PlannedDirConBill
    '|' + -- WeightedConCost
    '|' + -- WeightedConBill
    '|' + -- Unit
    '|' + -- UnitTable
    LTRIM(STR(T.UntCostRate, 19, @intRtCostDecimals)) + '|' +  
    LTRIM(STR(T.UntBillRate, 19, @intRtBillDecimals)) + '|' +  
    '|' + -- PlannedUntQty
    '|' + -- PlannedUntCost
    '|' + -- PlannedUntBill
    LTRIM(STR(T.PctCompleteUntCost, 19, 2)) + '|' +
    LTRIM(STR(T.PctCompleteUntBill, 19, 2)) + '|' +
    '|' + -- JTDUntQty
    '|' + -- JTDUntCost
    '|' + -- JTDUntBill
    '|' + -- ActualDirUntCost
    '|' + -- ActualDirUntBill
    '|' + -- ETCUntQty
    '|' + -- ETCUntCost
    '|' + -- ETCUntBill
    '|' + -- ETCDirUntCost
    '|' + -- ETCDirUntBill
    '|' + -- EACUntQty
    '|' + -- EACUntCost
    '|' + -- EACUntBill
    '|' + -- VarUntQty
    '|' + -- VarUntCost
    '|' + -- VarUntBill
    LTRIM(STR(T.BaselineUntQty, 19, @intQtyDecimals)) + '|' +  
    LTRIM(STR(T.BaselineUntCost, 19, @intAmtCostDecimals)) + '|' +  
    LTRIM(STR(T.BaselineUntBill, 19, @intAmtBillDecimals)) + '|' +  
    '|' + -- UntRevenue
    '|' + -- PlannedDirUntCost
    '|' + -- PlannedDirUntBill
    '|' + -- unCostRate
    '|' + -- unBillRate
    '|' + -- WeightedUntCost
    '' AS [T!1!!Element] -- WeightedUntBill
    FROM RPTask AS T 
      LEFT JOIN (
        SELECT SUM(Flag) AS Flag, TaskID AS TaskID FROM (
          SELECT DISTINCT 1 AS Flag, TaskID AS TaskID FROM RPDependency WHERE PlanID = @strPlanID
          UNION
          SELECT DISTINCT -1 AS Flag, PredecessorID  AS TaskID FROM RPDependency WHERE PlanID = @strPlanID
        ) AS X
          GROUP BY TaskID
      ) AS TD ON T.TaskID = TD.TaskID
    WHERE T.PlanID = @strPlanID
    ORDER BY T.OutlineNumber
    FOR XML EXPLICIT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   

  -->>> Assignment.
  
  SELECT DISTINCT 1 AS Tag, NULL AS Parent,
    T.TaskID AS [TC!1!P],
    NULL AS [A!2!I],
    NULL AS [A!2!S1!Hide],
    NULL AS [A!2!S2!Hide],
    NULL AS [A!2!!Element]
    FROM RPTask AS T
      INNER JOIN RPAssignment AS A ON T.PlanID = A.PlanID AND T.TaskID = A.TaskID
    WHERE T.PlanID = @strPlanID
  UNION ALL
  SELECT 2 AS Tag, 1 AS Parent,
    A.TaskID AS [TC!1!P],
    A.AssignmentID AS [A!2!I],
    A.SortSeq AS [A!2!S1!Hide],
    (CASE WHEN A.ResourceID IS NULL Then 1 ELSE 0 END) AS [A!2!S2!Hide],
    REPLACE((CASE WHEN A.ResourceID IS NOT NULL THEN COALESCE(EM.LastName + COALESCE(', ' + EM.FirstName, ''), '') 
                  WHEN A.GenericResourceID IS NOT NULL THEN ISNULL(GR.Name, '') 
                  ELSE '' END), '"', '\"') + '|' + 
    '|' + -- WBSType
    ISNULL(A.WBS1, '') + '|' +  
    ISNULL(A.WBS2, '') + '|' +  
    ISNULL(A.WBS3, '') + '|' +  
    ISNULL(A.LaborCode, '') + '|' +  
    ISNULL(CONVERT(VARCHAR, CONVERT(INT, A.StartDate)), '') + '|' +  
    ISNULL(CONVERT(VARCHAR, CONVERT(INT, A.EndDate)), '') + '|' +  
    LTRIM(STR(A.CostRate, 19, @intRtCostDecimals)) + '|' +  
    LTRIM(STR(A.BillingRate, 19, @intRtBillDecimals)) + '|' +  
    '|' + -- PlannedLaborHrs
    '|' + -- PlannedLabCost 
    '|' + -- PlannedLabBill
    LTRIM(STR(A.PctCompleteLabCost, 19, 2)) + '|' +
    LTRIM(STR(A.PctCompleteLabBill, 19, 2)) + '|' +
    '|' + -- JTDLabHrs
    '|' + -- JTDLabCost
    '|' + -- JTDLabBill
    '|' + -- ETCLabHrs
    '|' + -- ETCLabCost
    '|' + -- ETCLabBill
    '|' + -- EACLabHrs
    '|' + -- EACLabCost
    '|' + -- EACLabBill
    '|' + -- VarLabHrs
    '|' + -- VarLabCost
    '|' + -- VarLabBill
    LTRIM(STR(A.BaselineLaborHrs, 19, @intHrDecimals)) + '|' +  
    LTRIM(STR(A.BaselineLabCost, 19, @intAmtCostDecimals)) + '|' +  
    LTRIM(STR(A.BaselineLabBill, 19, @intAmtBillDecimals)) + '|' +  
    '|' + -- LabRevenue
    '|' + -- CompensationFee
    '|' + -- CompensationFeeBill
    '|' + -- ConsultantFee
    '|' + -- ConsultantFeeBill
    '|' + -- CompensationFeeDirLab
    '|' + -- CompensationFeeDirLabBill
    '|' + -- CompensationFeeDirExp 
    '|' + -- CompensationFeeDirExpBill
    '|' + -- ReimbAllowance
    '|' + -- ReimbAllowanceBill
    '|' + -- ReimbAllowanceExp
    '|' + -- ReimbAllowanceExpBill
    '|' + -- ReimbAllowanceCon 
    '|' + -- ReimbAllowanceConBill
    '|' + -- CCRCost
    '|' + -- CCRBill
    '|' + -- WeightedLabCost
    '|' + -- WeightedLabBill
    '|' + -- BaselineStart
    '|' + -- BaselineFinish
    ISNULL(A.ResourceID, '') + '|' + 
    ISNULL(A.GenericResourceID, '') + '|' + 
    CONVERT(VARCHAR, A.Category) + '|' + 
    ISNULL(A.GRLBCD, '') + '|' + 
    CONVERT(VARCHAR, (ISNULL(EM.BillingCategory, 0))) + '|' + 
    LTRIM(STR(ISNULL(EM.ProvCostRate, 0), 19, @intRtCostDecimals)) + '|' + 
    LTRIM(STR(ISNULL(EM.ProvBillRate, 0), 19, @intRtBillDecimals)) + '|' + 
    ISNULL(MD.FunctionalCurrencyCode, ' ') + '|' +
    '|' + -- OutlineLevel
    '|' + -- OutlineNumber
    '|' + -- ParentOutlineNumber
    '|' + -- ChildrenCount
    CONVERT(VARCHAR, A.SortSeq) + '|' +
    'N|' + -- LabParentState
    'N|' + -- ExpParentState
    'N|' + -- ConParentState
    'N|' + -- UntParentState
    ISNULL(A.LabVState, 'Y') + '|' + 
    ISNULL(A.ExpVState, 'Y') + '|' + 
    ISNULL(A.ConVState, 'Y') + '|' + 
    ISNULL(A.UntVState, 'Y') + '|' +
    '|' + -- UnmatchedFlg 
    '|' + -- DependencyFlg 
    '|' + -- Account 
    '|' + -- veName 
    '|' + -- PlannedExpCost
    '|' + -- ExpBillRate
    '|' + -- PlannedExpBill
    '|' + -- PctCompleteExpCost
    '|' + -- PctCompleteExpBill
    '|' + -- JTDExpCost
    '|' + -- JTDExpBill
    '|' + -- ActualDirExpCost
    '|' + -- ActualDirExpBill
    '|' + -- ETCExpCost
    '|' + -- ETCExpBill
    '|' + -- ETCDirExpCost
    '|' + -- ETCDirExpBill
    '|' + -- EACExpCost
    '|' + -- EACExpBill
    '|' + -- VARExpCost
    '|' + -- VARExpBill
    '|' + -- BaselineExpCost
    '|' + -- BaselineExpBill
    '|' + -- ExpRevenue
    '|' + -- PlannedDirExpCost
    '|' + -- PlannedDirExpBill
    '|' + -- WeightedExpCost
    '|' + -- WeightedExpBill
    '|' + -- DirectAcctFlg
    '|' + -- Vendor
    '|' + -- PlannedConCost
    '|' + -- ConBillRate
    '|' + -- PlannedConBill
    '|' + -- PctCompleteConCost
    '|' + -- PctCompleteConBill
    '|' + -- JTDConCost
    '|' + -- JTDConBill
    '|' + -- ActualDirConCost
    '|' + -- ActualDirConBill
    '|' + -- ETCConCost
    '|' + -- ETCConBill
    '|' + -- ETCDirConCost
    '|' + -- ETCDirConBill
    '|' + -- EACConCost
    '|' + -- EACConBill
    '|' + -- VARConCost
    '|' + -- VARConBill
    '|' + -- BaselineConCost
    '|' + -- BaselineConBill
    '|' + -- ConRevenue
    '|' + -- PlannedDirConCost
    '|' + -- PlannedDirConBill
    '|' + -- WeightedConCost
    '|' + -- WeightedConBill
    '|' + -- Unit
    '|' + -- UnitTable
    '|' + -- UntCostRate
    '|' + -- UntBillRate
    '|' + -- PlannedUntQty
    '|' + -- PlannedUntCost
    '|' + -- PlannedUntBill
    '|' + -- PctCompleteUntCost
    '|' + -- PctCompleteUntBill
    '|' + -- JTDUntQty
    '|' + -- JTDUntCost
    '|' + -- JTDUntBill
    '|' + -- ActualDirUntCost
    '|' + -- ActualDirUntBill
    '|' + -- ETCUntQty
    '|' + -- ETCUntCost
    '|' + -- ETCUntBill
    '|' + -- ETCDirUntCost
    '|' + -- ETCDirUntBill
    '|' + -- EACUntQty
    '|' + -- EACUntCost
    '|' + -- EACUntBill
    '|' + -- VarianceUntQty
    '|' + -- VarianceUntCost
    '|' + -- VarianceUntBill
    '|' + -- BaselineUntQty
    '|' + -- BaselineUntCost
    '|' + -- BaselineUntBill
    '|' + -- UntRevenue
    '|' + -- PlannedDirUntCost
    '|' + -- PlannedDirUntBill
    '|' + -- unCostRate
    '|' + -- unBillRate
    '|' + -- WeightedUntCost               
    '' AS [A!2!!Element] -- WeightedUntBill 
    FROM RPAssignment AS A
      LEFT JOIN EM ON A.ResourceID = EM.Employee
      LEFT JOIN GR ON A.GenericResourceID = GR.Code
      LEFT JOIN CFGMainData AS MD ON ISNULL(EM.Org, '%') LIKE CASE WHEN @strMultiCompanyEnabled = 'Y' THEN MD.Company + '%' ELSE '%' END
    WHERE A.PlanID = @strPlanID
    ORDER BY [TC!1!P], Tag, [A!2!S1!Hide], [A!2!S2!Hide]
    FOR XML EXPLICIT
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   

  -->>> Expense.
  
  IF (@strExpTab = 'Y')
    BEGIN 

      SELECT DISTINCT 1 AS Tag, NULL AS Parent,
        T.TaskID AS [TC!1!P],
        NULL AS [E!2!I],
        NULL AS [E!2!SortSeq!Hide],
        NULL AS [E!2!!Element]
        FROM RPTask AS T
          INNER JOIN RPExpense AS E ON T.PlanID = E.PlanID AND T.TaskID = E.TaskID
        WHERE T.PlanID = @strPlanID
      UNION ALL
      SELECT 2 AS Tag, 1 AS Parent,
        E.TaskID AS [TC!1!P],
        E.ExpenseID AS [E!2!I],
        E.SortSeq AS [E!2!SortSeq!Hide],
        REPLACE(ISNULL(CA.Name, ''), '"', '\"') + '|' + 
        '|' + -- WBSType
        ISNULL(E.WBS1, '') + '|' +  
        ISNULL(E.WBS2, '') + '|' +  
        ISNULL(E.WBS3, '') + '|' +  
        '|' + -- LaborCode
        ISNULL(CONVERT(VARCHAR, CONVERT(INT, E.StartDate)), '') + '|' +  
        ISNULL(CONVERT(VARCHAR, CONVERT(INT, E.EndDate)), '') + '|' +  
        '|' + -- CostRate
        '|' + -- BillingRate
        '|' + -- PlannedLaborHrs
        '|' + -- PlannedLabCost 
        '|' + -- PlannedLabBill
        '|' + -- PctCompleteLabCost
        '|' + -- PctCompleteLabBill
        '|' + -- JTDLabHrs
        '|' + -- JTDLabCost
        '|' + -- JTDLabBill
        '|' + -- ETCLabHrs
        '|' + -- ETCLabCost
        '|' + -- ETCLabBill
        '|' + -- EACLabHrs
        '|' + -- EACLabCost
        '|' + -- EACLabBill
        '|' + -- VARLabHrs
        '|' + -- VARLabCost
        '|' + -- VARLabBill
        '|' + -- BaselineLaborHrs
        '|' + -- BaselineLabCost
        '|' + -- BaselineLabBill
        '|' + -- LabRevenue
        '|' + -- CompensationFee
        '|' + -- CompensationFeeBill
        '|' + -- ConsultantFee
        '|' + -- ConsultantFeeBill
        '|' + -- CompensationFeeDirLab
        '|' + -- CompensationFeeDirLabBill
        '|' + -- CompensationFeeDirExp 
        '|' + -- CompensationFeeDirExpBill
        '|' + -- ReimbAllowance
        '|' + -- ReimbAllowanceBill
        '|' + -- ReimbAllowanceExp
        '|' + -- ReimbAllowanceExpBill
        '|' + -- ReimbAllowanceCon 
        '|' + -- ReimbAllowanceConBill
        '|' + -- CCRCost
        '|' + -- CCRBill
        '|' + -- WeightedLabCost
        '|' + -- WeightedLabBill
        '|' + -- BaselineStart
        '|' + -- BaselineFinish 
        '|' + -- ResourceID
        '|' + -- GenericResourceID
        '|' + -- Category
        '|' + -- GRLBCD
        '|' + -- emBillingCategory
        '|' + -- emProvCostRate
        '|' + -- emProvBillRate
        '|' + -- emCurrencyCode
        '|' + -- OutlineLevel
        '|' + -- OutlineNumber
        '|' + -- ParentOutlineNumber
        '|' + -- ChildrenCount
        CONVERT(VARCHAR, SortSeq) + '|' +
        'N|' + -- LabParentState
        'N|' + -- ExpParentState
        'N|' + -- ConParentState
        'N|' + -- UntParentState
        ISNULL(E.LabVState, 'Y') + '|' + 
        ISNULL(E.ExpVState, 'Y') + '|' + 
        ISNULL(E.ConVState, 'Y') + '|' + 
        ISNULL(E.UntVState, 'Y') + '|' + 
        '|' + -- UnmatchedFlg 
        '|' + -- DependencyFlg 
        ISNULL(E.Account, '') + '|' + 
        REPLACE(ISNULL(VE.Name, ''), '"', '\"') + '|' + 
        '|' + -- PlannedExpCost
        LTRIM(STR(E.ExpBillRate, 19, 4)) + '|' +  
        '|' + -- PlannedExpBill
        LTRIM(STR(E.PctCompleteExpCost, 19, 2)) + '|' +
        LTRIM(STR(E.PctCompleteExpBill, 19, 2)) + '|' +
        '|' + -- JTDExpCost
        '|' + -- JTDExpBill
        '|' + -- ActualDirExpCost
        '|' + -- ActualDirExpBill
        '|' + -- ETCExpCost
        '|' + -- ETCExpBill
        '|' + -- ETCDirExpCost
        '|' + -- ETCDirExpBill
        '|' + -- EACExpCost
        '|' + -- EACExpBill
        '|' + -- VarExpCost
        '|' + -- VarExpBill
        LTRIM(STR(E.BaselineExpCost, 19, @intAmtCostDecimals)) + '|' + 
        LTRIM(STR(E.BaselineExpBill, 19, @intAmtBillDecimals)) + '|' +  
        '|' + -- ExpRevenue
        '|' + -- PlannedDirExpCost
        '|' + -- PlannedDirExpBill
        '|' + -- WeightedExpCost
        '|' + -- WeightedExpBill 
        ISNULL(E.DirectAcctFlg, '') + '|' +
        ISNULL(E.Vendor, '') + '|' +
        '|' + -- PlannedConCost
        '|' + -- ConBillRate
        '|' + -- PlannedConBill
        '|' + -- PctCompleteConCost
        '|' + -- PctCompleteConBill
        '|' + -- JTDConCost
        '|' + -- JTDConBill
        '|' + -- ActualDirConCost
        '|' + -- ActualDirConBill
        '|' + -- ETCConCost
        '|' + -- ETCConBill
        '|' + -- ETCDirConCost
        '|' + -- ETCDirConBill
        '|' + -- EACConCost
        '|' + -- EACConBill
        '|' + -- VARConCost
        '|' + -- VARConBill
        '|' + -- BaselineConCost
        '|' + -- BaselineConBill
        '|' + -- ConRevenue
        '|' + -- PlannedDirConCost
        '|' + -- PlannedDirConBill
        '|' + -- WeightedConCost
        '|' + -- WeightedConBill
        '|' + -- Unit
        '|' + -- UnitTable
        '|' + -- UntCostRate
        '|' + -- UntBillRate
        '|' + -- PlannedUntQty
        '|' + -- PlannedUntCost
        '|' + -- PlannedUntBill
        '|' + -- PctCompleteUntCost
        '|' + -- PctCompleteUntBill
        '|' + -- JTDUntQty
        '|' + -- JTDUntCost
        '|' + -- JTDUntBill
        '|' + -- ActualDirUntCost
        '|' + -- ActualDirUntBill
        '|' + -- ETCUntQty
        '|' + -- ETCUntCost
        '|' + -- ETCUntBill
        '|' + -- ETCDirUntCost
        '|' + -- ETCDirUntBill
        '|' + -- EACUntQty
        '|' + -- EACUntCost
        '|' + -- EACUntBill
        '|' + -- VarianceUntQty
        '|' + -- VarianceUntCost
        '|' + -- VarianceUntBill
        '|' + -- BaselineUntQty
        '|' + -- BaselineUntCost
        '|' + -- BaselineUntBill
        '|' + -- UntRevenue
        '|' + -- PlannedDirUntCost
        '|' + -- PlannedDirUntBill
        '|' + -- unCostRate
        '|' + -- unBillRate
        '|' + -- WeightedUntCost               
        '' AS [E!2!!Element] -- WeightedUntBill
        FROM RPExpense AS E
          LEFT JOIN CA ON E.Account = CA.Account
          LEFT JOIN VE ON E.Vendor = VE.Vendor
        WHERE E.PlanID = @strPlanID 
        ORDER BY [TC!1!P], Tag, [E!2!SortSeq!Hide]
        FOR XML EXPLICIT

    END --IF (@strExpTab = 'Y')
       
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   
    
  -->>> Consultant.
  
  IF (@strConTab = 'Y')
    BEGIN
    
      SELECT DISTINCT 1 AS Tag, NULL AS Parent,
        T.TaskID AS [TC!1!P],
        NULL AS [C!2!I],
        NULL AS [C!2!SortSeq!Hide],
        NULL AS [C!2!!Element]
        FROM RPTask AS T
          INNER JOIN RPConsultant AS C ON T.PlanID = C.PlanID AND T.TaskID = C.TaskID
        WHERE T.PlanID = @strPlanID
      UNION ALL
      SELECT 2 AS Tag, 1 AS Parent,
        C.TaskID AS [TC!1!P],
        C.ConsultantID AS [C!2!I],
        C.SortSeq AS [C!2!SortSeq!Hide],
        REPLACE(ISNULL(CA.Name, ''), '"', '\"') + '|' + 
        '|' + -- WBSType
        ISNULL(C.WBS1, '') + '|' +  
        ISNULL(C.WBS2, '') + '|' +  
        ISNULL(C.WBS3, '') + '|' +  
        '|' + -- LaborCode
        ISNULL(CONVERT(VARCHAR, CONVERT(INT, C.StartDate)), '') + '|' +  
        ISNULL(CONVERT(VARCHAR, CONVERT(INT, C.EndDate)), '') + '|' +  
        '|' + -- CostRate
        '|' + -- BillingRate
        '|' + -- PlannedLaborHrs
        '|' + -- PlannedLabCost 
        '|' + -- PlannedLabBill
        '|' + -- PctCompleteLabCost
        '|' + -- PctCompleteLabBill
        '|' + -- JTDLabHrs
        '|' + -- JTDLabCost
        '|' + -- JTDLabBill
        '|' + -- ETCLabHrs
        '|' + -- ETCLabCost
        '|' + -- ETCLabBill
        '|' + -- EACLabHrs
        '|' + -- EACLabCost
        '|' + -- EACLabBill
        '|' + -- VARLabHrs
        '|' + -- VARLabCost
        '|' + -- VARLabBill
        '|' + -- BaselineLaborHrs
        '|' + -- BaselineLabCost
        '|' + -- BaselineLabBill
        '|' + -- LabRevenue
        '|' + -- CompensationFee
        '|' + -- CompensationFeeBill
        '|' + -- ConsultantFee
        '|' + -- ConsultantFeeBill
        '|' + -- CompensationFeeDirLab
        '|' + -- CompensationFeeDirLabBill
        '|' + -- CompensationFeeDirExp 
        '|' + -- CompensationFeeDirExpBill
        '|' + -- ReimbAllowance
        '|' + -- ReimbAllowanceBill
        '|' + -- ReimbAllowanceExp
        '|' + -- ReimbAllowanceExpBill
        '|' + -- ReimbAllowanceCon 
        '|' + -- ReimbAllowanceConBill
        '|' + -- CCRCost
        '|' + -- CCRBill
        '|' + -- WeightedLabCost
        '|' + -- WeightedLabBill
        '|' + -- BaselineStart
        '|' + -- BaselineFinish
        '|' + -- ResourceID
        '|' + -- GenericResourceID
        '|' + -- Category
        '|' + -- GRLBCD
        '|' + -- emBillingCategory
        '|' + -- emProvCostRate
        '|' + -- emProvBillRate
        '|' + -- emCurrencyCode
        '|' + -- OutlineLevel
        '|' + -- OutlineNumber
        '|' + -- ParentOutlineNumber
        '|' + -- ChildrenCount
        CONVERT(VARCHAR, SortSeq) + '|' +
        'N|' + -- LabParentState
        'N|' + -- ExpParentState
        'N|' + -- ConParentState
        'N|' + -- UntParentState
        ISNULL(C.LabVState, 'Y') + '|' + 
        ISNULL(C.ExpVState, 'Y') + '|' + 
        ISNULL(C.ConVState, 'Y') + '|' + 
        ISNULL(C.UntVState, 'Y') + '|' + 
        '|' + -- UnmatchedFlg 
        '|' + -- DependencyFlg 
        ISNULL(C.Account, '') + '|' + 
        REPLACE(ISNULL(VE.Name, ''), '"', '\"') + '|' + 
        '|' + -- PlannedExpCost
        '|' + -- ExpBillRate
        '|' + -- PlannedExpBill
        '|' + -- PctCompleteExpCost
        '|' + -- PctCompleteExpBill
        '|' + -- JTDExpCost
        '|' + -- JTDExpBill
        '|' + -- ActualDirExpCost
        '|' + -- ActualDirExpBill
        '|' + -- ETCExpCost
        '|' + -- ETCExpBill
        '|' + -- ETCDirExpCost
        '|' + -- ETCDirExpBill
        '|' + -- EACExpCost
        '|' + -- EACExpBill
        '|' + -- VARExpCost
        '|' + -- VARExpBill
        '|' + -- BaselineExpCost
        '|' + -- BaselineExpBill
        '|' + -- ExpRevenue
        '|' + -- PlannedDirExpCost
        '|' + -- PlannedDirExpBill
        '|' + -- WeightedExpCost
        '|' + -- WeightedExpBill
        ISNULL(C.DirectAcctFlg, '') + '|' +
        ISNULL(C.Vendor, '') + '|' +
        '|' + -- PlannedConCost
        LTRIM(STR(C.ConBillRate, 19, 4)) + '|' +  
        '|' + -- PlannedConBill 
        LTRIM(STR(C.PctCompleteConCost, 19, 2)) + '|' +
        LTRIM(STR(C.PctCompleteConBill, 19, 2)) + '|' +
        '|' + -- JTDConCost
        '|' + -- JTDConBill
        '|' + -- ActualDirConCost
        '|' + -- ActualDirConBill
        '|' + -- ETCConCost
        '|' + -- ETCConBill
        '|' + -- ETCDirConCost
        '|' + -- ETCDirConBill
        '|' + -- EACConCost
        '|' + -- EACConBill
        '|' + -- VarConCost
        '|' + -- VarConBill
        LTRIM(STR(C.BaselineConCost, 19, @intAmtCostDecimals)) + '|' +  
        LTRIM(STR(C.BaselineConBill, 19, @intAmtBillDecimals)) + '|' +  
        '|' + -- ConRevenue
        '|' + -- PlannedDirConCost
        '|' + -- PlannedDirConBill
        '|' + -- WeightedConCost 
        '|' + -- WeightedConBill
        '|' + -- Unit
        '|' + -- UnitTable
        '|' + -- UntCostRate
        '|' + -- UntBillRate
        '|' + -- PlannedUntQty
        '|' + -- PlannedUntCost
        '|' + -- PlannedUntBill
        '|' + -- PctCompleteUntCost
        '|' + -- PctCompleteUntBill
        '|' + -- JTDUntQty
        '|' + -- JTDUntCost
        '|' + -- JTDUntBill
        '|' + -- ActualDirUntCost
        '|' + -- ActualDirUntBill
        '|' + -- ETCUntQty
        '|' + -- ETCUntCost
        '|' + -- ETCUntBill
        '|' + -- ETCDirUntCost
        '|' + -- ETCDirUntBill
        '|' + -- EACUntQty
        '|' + -- EACUntCost
        '|' + -- EACUntBill
        '|' + -- VarianceUntQty
        '|' + -- VarianceUntCost
        '|' + -- VarianceUntBill
        '|' + -- BaselineUntQty
        '|' + -- BaselineUntCost
        '|' + -- BaselineUntBill
        '|' + -- UntRevenue
        '|' + -- PlannedDirUntCost
        '|' + -- PlannedDirUntBill
        '|' + -- unCostRate
        '|' + -- unBillRate
        '|' + -- WeightedUntCost               
        '' AS [C!2!!Element] -- WeightedUntBill
        FROM RPConsultant AS C
          LEFT JOIN CA ON C.Account = CA.Account
          LEFT JOIN VE ON C.Vendor = VE.Vendor
        WHERE C.PlanID = @strPlanID 
        ORDER BY [TC!1!P], Tag, [C!2!SortSeq!Hide]
        FOR XML EXPLICIT
 
     END --IF (@strConTab = 'Y')
       
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   
    
  -->>> Unit.
  
  IF (@strUntTab = 'Y')
    BEGIN
    
      SELECT DISTINCT 1 AS Tag, NULL AS Parent,
        T.TaskID AS [TC!1!P],
        NULL AS [U!2!I],
        NULL AS [U!2!SortSeq!Hide],
        NULL AS [U!2!!Element]
        FROM RPTask AS T
          INNER JOIN RPUnit AS U ON T.PlanID = U.PlanID AND T.TaskID = U.TaskID
        WHERE T.PlanID = @strPlanID
      UNION ALL
      SELECT 2 AS Tag, 1 AS Parent,
        U.TaskID AS [TC!1!P],
        U.UnitID AS [U!2!I],
        U.SortSeq AS [U!2!SortSeq!Hide],
        REPLACE(ISNULL(UN.Name, ''), '"', '\"') + '|' + 
        '|' + -- WBSType
        ISNULL(U.WBS1, '') + '|' +  
        ISNULL(U.WBS2, '') + '|' +  
        ISNULL(U.WBS3, '') + '|' +  
        '|' + -- LaborCode
        ISNULL(CONVERT(VARCHAR, CONVERT(INT, U.StartDate)), '') + '|' +  
        ISNULL(CONVERT(VARCHAR, CONVERT(INT, U.EndDate)), '') + '|' +  
        '|' + -- CostRate
        '|' + -- BillingRate
        '|' + -- PlannedLaborHrs
        '|' + -- PlannedLabCost 
        '|' + -- PlannedLabBill
        '|' + -- PctCompleteLabCost
        '|' + -- PctCompleteLabBill
        '|' + -- JTDLabHrs
        '|' + -- JTDLabCost
        '|' + -- JTDLabBill
        '|' + -- ETCLabHrs
        '|' + -- ETCLabCost
        '|' + -- ETCLabBill
        '|' + -- EACLabHrs
        '|' + -- EACLabCost
        '|' + -- EACLabBill
        '|' + -- VARLabHrs
        '|' + -- VARLabCost
        '|' + -- VARLabBill
        '|' + -- BaselineLaborHrs
        '|' + -- BaselineLabCost
        '|' + -- BaselineLabBill
        '|' + -- LabRevenue
        '|' + -- CompensationFee
        '|' + -- CompensationFeeBill
        '|' + -- ConsultantFee
        '|' + -- ConsultantFeeBill
        '|' + -- CompensationFeeDirLab
        '|' + -- CompensationFeeDirLabBill
        '|' + -- CompensationFeeDirExp 
        '|' + -- CompensationFeeDirExpBill
        '|' + -- ReimbAllowance
        '|' + -- ReimbAllowanceBill
        '|' + -- ReimbAllowanceExp
        '|' + -- ReimbAllowanceExpBill
        '|' + -- ReimbAllowanceCon 
        '|' + -- ReimbAllowanceConBill
        '|' + -- CCRCost
        '|' + -- CCRBill
        '|' + -- WeightedLabCost
        '|' + -- WeightedLabBill
        '|' + -- BaselineStart
        '|' + -- BaselineFinish
        '|' + -- ResourceID
        '|' + -- GenericResourceID
        '|' + -- Category
        '|' + -- GRLBCD
        '|' + -- emBillingCategory
        '|' + -- emProvCostRate
        '|' + -- emProvBillRate
        '|' + -- emCurrencyCode
        '|' + -- OutlineLevel
        '|' + -- OutlineNumber
        '|' + -- ParentOutlineNumber
        '|' + -- ChildrenCount
        CONVERT(VARCHAR, SortSeq) + '|' +
        'N|' + -- LabParentState
        'N|' + -- ExpParentState
        'N|' + -- ConParentState
        'N|' + -- UntParentState
        ISNULL(U.LabVState, 'Y') + '|' + 
        ISNULL(U.ExpVState, 'Y') + '|' + 
        ISNULL(U.ConVState, 'Y') + '|' + 
        ISNULL(U.UntVState, 'Y') + '|' + 
        '|' + -- UnmatchedFlg 
        '|' + -- DependencyFlg 
        ISNULL(U.Account, '') + '|' + 
        '|' + -- Vendor
        '|' + -- PlannedExpCost
        '|' + -- ExpBillRate
        '|' + -- PlannedExpBill
        '|' + -- PctCompleteExpCost
        '|' + -- PctCompleteExpBill
        '|' + -- JTDExpCost
        '|' + -- JTDExpBill
        '|' + -- ActualDirExpCost
        '|' + -- ActualDirExpBill
        '|' + -- ETCExpCost
        '|' + -- ETCExpBill
        '|' + -- ETCDirExpCost
        '|' + -- ETCDirExpBill
        '|' + -- EACExpCost
        '|' + -- EACExpBill
        '|' + -- VARExpCost
        '|' + -- VARExpBill
        '|' + -- BaselineExpCost
        '|' + -- BaselineExpBill
        '|' + -- ExpRevenue
        '|' + -- PlannedDirExpCost
        '|' + -- PlannedDirExpBill
        '|' + -- WeightedExpCost
        '|' + -- WeightedExpBill
        ISNULL(U.DirectAcctFlg, '') + '|' +
        '|' + -- Vendor
        '|' + -- PlannedConCost
        '|' + -- ConBillRate
        '|' + -- PlannedConBill
        '|' + -- PctCompleteConCost
        '|' + -- PctCompleteConBill
        '|' + -- JTDConCost
        '|' + -- JTDConBill
        '|' + -- ActualDirConCost
        '|' + -- ActualDirConBill
        '|' + -- ETCConCost
        '|' + -- ETCConBill
        '|' + -- ETCDirConCost
        '|' + -- ETCDirConBill
        '|' + -- EACConCost
        '|' + -- EACConBill
        '|' + -- VARConCost
        '|' + -- VARConBill
        '|' + -- BaselineConCost
        '|' + -- BaselineConBill
        '|' + -- ConRevenue
        '|' + -- PlannedDirConCost
        '|' + -- PlannedDirConBill
        '|' + -- WeightedConCost
        '|' + -- WeightedConBill
        ISNULL(U.Unit, '') + '|' + 
        ISNULL(U.UnitTable, '') + '|' + 
        LTRIM(STR(U.UntCostRate, 19, @intRtCostDecimals)) + '|' +  
        LTRIM(STR(U.UntBillRate, 19, @intRtBillDecimals)) + '|' +  
        '|' + -- PlannedUntQty
        '|' + -- PlannedUntCost
        '|' + -- PlannedUntBill
        LTRIM(STR(U.PctCompleteUntCost, 19, 2)) + '|' +
        LTRIM(STR(U.PctCompleteUntBill, 19, 2)) + '|' +
        '|' + -- JTDUntQty
        '|' + -- JTDUntCost
        '|' + -- JTDUntBill
        '|' + -- ActualDirUntCost
        '|' + -- ActualDirUntBill
        '|' + -- ETCUntQty
        '|' + -- ETCUntCost
        '|' + -- ETCUntBill
        '|' + -- ETCDirUntCost
        '|' + -- ETCDirUntBill
        '|' + -- EACUntQty
        '|' + -- EACUntCost
        '|' + -- EACUntBill
        '|' + -- VarUntQty
        '|' + -- VarUntCost
        '|' + -- VarUntBill
        LTRIM(STR(U.BaselineUntQty, 19, @intQtyDecimals)) + '|' +  
        LTRIM(STR(U.BaselineUntCost, 19, @intAmtCostDecimals)) + '|' +  
        LTRIM(STR(U.BaselineUntBill, 19, @intAmtBillDecimals)) + '|' +  
        '|' + -- UntRevenue
        '|' + -- PlannedDirUntCost
        '|' + -- PlannedDirUntBill
        LTRIM(STR(ISNULL(UN.CostRate, 0), 19, @intRtCostDecimals)) + '|' +  
        LTRIM(STR(ISNULL(UN.BillingRate, 0), 19, @intRtBillDecimals)) + '|' +  
        '|' + -- WeightedUntCost
        '' AS [U!2!!Element] -- WeightedUntBill
        FROM RPUnit AS U
          LEFT JOIN UN ON U.Unit = UN.Unit AND U.UnitTable = UN.UnitTable AND UN.Company = @strPlanCompany
        WHERE U.PlanID = @strPlanID 
        ORDER BY [TC!1!P], Tag, [U!2!SortSeq!Hide]
        FOR XML EXPLICIT

    END --IF (@strUntTab = 'Y')

  -- Calculate and return execution time.
  
  SET @totalTime = DATEDIFF(ms, @startTime, GetDate())

  SELECT 1 Tag, NULL Parent, @totalTime [time!1] FOR XML EXPLICIT

  SET NOCOUNT OFF

END -- pmGetPlan
GO
