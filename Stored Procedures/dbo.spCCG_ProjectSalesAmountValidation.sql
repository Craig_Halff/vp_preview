SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_ProjectSalesAmountValidation] @WBS1 varchar (30)
AS
/*
Copyright 2021 Central Consulting Group.   All rights reserved.
07/14/2021	David Springer
			Validate that the Sales grid matches the Revenue (Est Fee).
			Call this from an Project CHANGE workflow when Ready for Processing is checked or Stage = Awarded.
*/
SET NOCOUNT ON
BEGIN
   If exists (
   Select 'x'
   From PR p
        Left Join
	   (Select WBS1, Sum (CustSalesAmount) Sales
	    From Projects_Sales
		Group by WBS1) s on s.WBS1 = p.WBS1
   Where p.WBS1 = @WBS1
     and p.Revenue <> IsNull (s.Sales, 0)
	         )
			 RAISERROR ('Sales grid Amount must equal Est Fee.                        ', 16, 1)
END
GO
