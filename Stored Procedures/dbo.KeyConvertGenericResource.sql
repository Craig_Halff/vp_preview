SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyConvertGenericResource]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg Nvarchar(1000) output,
@UserName Nvarchar(32) = 'VisionKeyConvert', -- Add by DW 6.2 (Vision Europe)
@Period integer = 0,
@EntityType Nvarchar(20)
as
begin -- Procedure
	set nocount on
	declare @OldValue Nvarchar(20)
	declare @NewValue Nvarchar(20)
	Declare @Existing integer
	declare @Entity Nvarchar(20)
	declare @length integer
	declare @CvtType integer
	declare @KeysFetch integer
	declare @RetVal integer
	declare @diag smallint
	declare @Errmsg2 Nvarchar(1000),
			@custlabel	Nvarchar(40),
			@Custlabelplural Nvarchar(40)
	declare @message nvarchar(max)
	set @diag = 0
	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'GenericResource'
	set @length = 10
	set @Existing = 0
	set @KeysFetch = 0

	declare @NewGRName Nvarchar(20)
	declare @NewCategory smallint
	declare @NewLaborCode Nvarchar(14)
	
	-- execute InsertKeyCvtDriver @Entity -- no need to populate driver table, we will convert here	
	
	DECLARE @OldName Nvarchar(40)
	DECLARE @NewName Nvarchar(40)
	DECLARE @PKey Nvarchar(50)
	
	DECLARE @Supervisor Nvarchar(20)
	DECLARE @OldOrg Nvarchar(14)
	DECLARE @OldStatus Nvarchar(1)
	
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0
--
	select @custlabel = 'Generic Resource Code',
			 @Custlabelplural = 'Generic Resource Codes'
	declare KeysCursor cursor for
		Select K.OldCode, IsNull(K.NewCode, '') As NewCode, K.PKey, K.Name,
		NewCategory = case K.NewCategory when 0 then O.Category else  K.NewCategory  end,   
		ISNULL(K.NewLaborCode, ISNULL(O.LaborCode,'')) as NewLaborCode, O.Org as OldOrg, 
		O.Supervisor as OldSupervisor, O.Status as OldStatus
		from KeyConvertWorkGR K inner join GR O on O.Code = K.OldCode 

	open KeysCursor
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue, 
		@PKey,
		@NewName,
		@NewCategory,
		@NewLaborCode,
		@OldOrg,
		@Supervisor,
		@OldStatus
		

	set @KeysFetch = @@Fetch_Status

While (@KeysFetch = 0)
	begin
		begin tran
-- Add by DP 4.0
		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany Nvarchar(14)
			DECLARE @RecordCount integer

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				set @RecordCount = (select count(*) from KeyConvertWork where Entity = @Entity)

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),N'KC','Y',  @custlabel + ' Key Convert ('+rtrim(convert(Nvarchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, @custlabel + ' Key Convert', @custlabel);
			end

		end
		set @Existing = 0
		select @Existing =  1, @NewGRName = Name from GR where Code = @NewValue
--

--  if the new generic resource is empty then, we only need to update the existing GR record
	If (@NewValue <> '')
		BEGIN   
		If (@Existing = 1)
			begin
			  if (@DeleteExisting = 0)
				begin
				SET @message = dbo.GetMessage('MsgNumAnExistingConvWillNotCont',@custlabel,@NewValue,@custlabel,@custLabelPlural,'','','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end
				if (@NewValue = @OldValue)
					begin
					SET @message = dbo.GetMessage('MsgAreSameNumConvNotCont',@custlabelPlural,'','','','','','','','')
					RAISERROR(@message,16,3)
						close keysCursor
						deallocate keysCursor
						if (@@Trancount > 0)
						   ROLLBACK TRANSACTION
						return(50002) -- user defined error
					end
				
				-- update the existing  Gr with the new labor categor and labor code
				Update GR Set Category = @NewCategory, LaborCode = @NewLaborCode Where Code = @NewValue
				Delete from GRSkills where GRSkills.Code = @OldValue
						and exists (select 'x' from GRSkills tmp where tmp.Code = @newValue and tmp.Skill = GRSkills.skill)

			END --Existing
		Else	-- Not Existing
			begin
				if (@NewLaborCode <> '')
					begin
						INSERT INTO  GR (Code,Name,Category,LaborCode) VALUES(@NewValue,@NewName,@NewCategory,@NewLaborCode)
					end
				else			
					begin
						INSERT INTO  GR (Code,Name,Category) VALUES(@NewValue,@NewName,@NewCategory)
					end
			-- For the brand new generic resource, we need to keep all other fields for the new gr
			Update GR Set Org = @OldOrg, Status = @OldStatus, Supervisor = @Supervisor Where Code = @NewValue
			
			end
		
	END --If (@NewValue <> '')
	
/* Derong Wang: not using standard sp to convert
		exec @RetVal = KeyCvt @Entity = @Entity,
					 @CvtType = @CvtType,
					 @Length = @Length,
					 @ErrMsg = @ErrMsg2 output,
					 @Diag = @Diag,
					 @NewValue = @NewValue,
					 @OldValue = @OldValue,
					 @ConstrFlag = 3 -- UpdateTables
*/
-- DW : Add the convert sql here
		--Update the RPAssignment record to link to the Category
	set @RetVal = 0

	If (@NewValue = '')		
		BEGIN
			Update GR Set Category = @NewCategory, LaborCode= @NewLaborCode Where Code = @OldValue
			Update RPAssignment Set  Category = @NewCategory WHERE GenericResourceID = @OldValue
			AND PlanID in (Select PlanID from RPPlan WHERE PlanID = RPAssignment.PlanID and GRMethod = 0)
			--Update the RPAssignment record to link to the LaborCode
			Update RPAssignment Set  GRLBCD = @NewLaborCode WHERE GenericResourceID = @OldValue
			AND PlanID in (Select PlanID from RPPlan WHERE PlanID = RPAssignment.PlanID and GRMethod = 1)

			Update PNAssignment Set  Category = @NewCategory WHERE GenericResourceID = @OldValue
			AND PlanID in (Select PlanID from PNPlan WHERE PlanID = PNAssignment.PlanID and GRMethod = 0)
			Update PNAssignment Set  GRLBCD = @NewLaborCode WHERE GenericResourceID = @OldValue
			AND PlanID in (Select PlanID from PNPlan WHERE PlanID = PNAssignment.PlanID and GRMethod = 1)
			
			--Update the RPTopdownGR record to link to the Category
			Update RPTopdownGR Set  Category = @NewCategory WHERE GenericResourceID = @OldValue
			AND PlanID in (Select PlanID from RPPlan WHERE PlanID = RPTopdownGR.PlanID and GRMethod = 0)
			--Update the RPTopdownGR record to link to the LaborCode
			Update RPTopdownGR Set LaborCode = @NewLaborCode WHERE GenericResourceID = @OldValue
			AND PlanID in (Select PlanID from RPPlan WHERE PlanID = RPTopdownGR.PlanID and GRMethod = 1)
		END
	ELSE
		BEGIN			
--*****************************************************
			If (@Existing = 1)
				begin
					declare @OldHardBooked		varchar(1)
					declare @NewHardBooked		varchar(1)

					declare @TempFetch integer
					declare @TempFetch2 integer
					declare @OldTaskID		varchar(32)
					declare @NewTaskID		varchar(32)
					declare @ResourceID		Nvarchar(20),
							@OldAssignmentID	Nvarchar(32),
							@NewAssignmentID	Nvarchar(32)
					declare @WBS1	Nvarchar(30),
							@WBS2	Nvarchar(30),
							@WBS3	Nvarchar(30)
					declare @PlanID varchar(32)

-- Move Planned and Basedline for the duplicate RPAssignment, Delete duplicate RPAssignment
					declare AssignmentCursor cursor for
						select PlanID, TaskID, max(HardBooked) as HardBooked, isnull(WBS1,'') as WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(ResourceID,'') as ResourceID
						, MIN(AssignmentID) as AssignmentID  
						from RPAssignment where GenericResourceID=@OldValue and
						exists (select 'x' from RPAssignment new where GenericResourceID=@NewValue
							and isnull(new.PlanID,'') = isnull(RPAssignment.PlanID,'')
							and isnull(new.TaskID,'') = isnull(RPAssignment.TaskID,'')
							and isnull(new.wbs1,'') = isnull(RPAssignment.wbs1,'')
							and isnull(new.wbs2,'') = isnull(RPAssignment.wbs2,'')
							and isnull(new.wbs3,'') = isnull(RPAssignment.wbs3,'')
							and isnull(new.ResourceID,'') = isnull(RPAssignment.ResourceID,''))
						group by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(ResourceID,'')
						Order by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(ResourceID,'')
					open AssignmentCursor
					fetch next from AssignmentCursor into @PlanID, @OldTaskID, @OldHardBooked, @WBS1, @WBS2, @WBS3, @ResourceID, @OldAssignmentID
					set @TempFetch = @@Fetch_Status
					While (@TempFetch = 0)
					begin
						select @NewTaskID=TaskID, @NewAssignmentID=AssignmentID, @NewHardBooked=HardBooked from RPAssignment 
							where PlanID=@PlanID 
							and TaskID=@OldTaskID 
							and isnull(WBS1,'')=isnull(@WBS1,'') 
							and isnull(WBS2,'')=isnull(@WBS2,'') 
							and isnull(WBS3,'')=isnull(@WBS3,'') 
							and isnull(ResourceID,'')=isnull(@ResourceID,'') 
							and isnull(GenericResourceID,'')=@NewValue 

						Update RPPlannedLabor Set PlanID=@PlanID, TaskID=@NewTaskID, AssignmentID=@NewAssignmentID where AssignmentID=@OldAssignmentID
						Update RPBaselineLabor Set PlanID=@PlanID, TaskID=@NewTaskID, AssignmentID=@NewAssignmentID where AssignmentID=@OldAssignmentID

						UPDATE A SET 
							StartDate = CASE WHEN XTPD.StartDate < A.StartDate THEN XTPD.StartDate ELSE A.StartDate END, 
							EndDate = CASE WHEN XTPD.EndDate > A.EndDate THEN XTPD.EndDate ELSE A.EndDate END
							FROM RPAssignment AS A
							  INNER JOIN (
								SELECT PlanID, TaskID, AssignmentID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
								  FROM RPPlannedLabor
								  WHERE AssignmentID=@NewAssignmentID
								  GROUP BY PlanID, TaskID, AssignmentID
							  ) AS XTPD 
								ON A.PlanID = XTPD.PlanID AND A.TaskID = XTPD.TaskID AND A.AssignmentID = XTPD.AssignmentID

						if (@NewHardBooked='N' and @OldHardBooked='Y')
							Update RPAssignment Set HardBooked='Y' WHere AssignmentID=@NewAssignmentID

						delete RPAssignment Where PlanID=@PlanID 
							and TaskID=@OldTaskID 
							and isnull(WBS1,'')=isnull(@WBS1,'') 
							and isnull(WBS2,'')=isnull(@WBS2,'') 
							and isnull(WBS3,'')=isnull(@WBS3,'') 
							and isnull(ResourceID,'')=isnull(@ResourceID,'')
							and isnull(GenericResourceID,'')=@OldValue 

						fetch next from AssignmentCursor into @PlanID, @OldTaskID, @OldHardBooked, @WBS1, @WBS2, @WBS3, @ResourceID, @OldAssignmentID
						set @TempFetch = @@Fetch_Status
					end
					close AssignmentCursor
					deallocate AssignmentCursor

-- Move Planned and Basedline for the duplicate PNAssignment, Delete duplicate PNAssignment
					declare AssignmentCursor cursor for
						select PlanID, TaskID, max(HardBooked) as HardBooked, isnull(WBS1,'') as WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(ResourceID,'') as ResourceID
						, MIN(AssignmentID) as AssignmentID  
						from PNAssignment where GenericResourceID=@OldValue and
						exists (select 'x' from PNAssignment new where GenericResourceID=@NewValue
							and isnull(new.PlanID,'') = isnull(PNAssignment.PlanID,'')
							and isnull(new.TaskID,'') = isnull(PNAssignment.TaskID,'')
							and isnull(new.wbs1,'') = isnull(PNAssignment.wbs1,'')
							and isnull(new.wbs2,'') = isnull(PNAssignment.wbs2,'')
							and isnull(new.wbs3,'') = isnull(PNAssignment.wbs3,'')
							and isnull(new.ResourceID,'') = isnull(PNAssignment.ResourceID,''))
						group by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(ResourceID,'')
						Order by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(ResourceID,'')
					open AssignmentCursor
					fetch next from AssignmentCursor into @PlanID, @OldTaskID, @OldHardBooked, @WBS1, @WBS2, @WBS3, @ResourceID, @OldAssignmentID
					set @TempFetch = @@Fetch_Status
					While (@TempFetch = 0)
					begin
						select @NewTaskID=TaskID, @NewAssignmentID=AssignmentID, @NewHardBooked=HardBooked from PNAssignment 
							where PlanID=@PlanID 
							and TaskID=@OldTaskID 
							and isnull(WBS1,'')=isnull(@WBS1,'') 
							and isnull(WBS2,'')=isnull(@WBS2,'') 
							and isnull(WBS3,'')=isnull(@WBS3,'') 
							and isnull(ResourceID,'')=isnull(@ResourceID,'') 
							and isnull(GenericResourceID,'')=@NewValue 

						Update PNPlannedLabor Set PlanID=@PlanID, TaskID=@NewTaskID, AssignmentID=@NewAssignmentID where AssignmentID=@OldAssignmentID
						Update PNBaselineLabor Set PlanID=@PlanID, TaskID=@NewTaskID, AssignmentID=@NewAssignmentID where AssignmentID=@OldAssignmentID

						UPDATE A SET 
							StartDate = CASE WHEN XTPD.StartDate < A.StartDate THEN XTPD.StartDate ELSE A.StartDate END, 
							EndDate = CASE WHEN XTPD.EndDate > A.EndDate THEN XTPD.EndDate ELSE A.EndDate END
							FROM PNAssignment AS A
							  INNER JOIN (
								SELECT PlanID, TaskID, AssignmentID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
								  FROM PNPlannedLabor
								  WHERE AssignmentID=@NewAssignmentID
								  GROUP BY PlanID, TaskID, AssignmentID
							  ) AS XTPD 
								ON A.PlanID = XTPD.PlanID AND A.TaskID = XTPD.TaskID AND A.AssignmentID = XTPD.AssignmentID

						if (@NewHardBooked='N' and @OldHardBooked='Y')
							Update PNAssignment Set HardBooked='Y' WHere AssignmentID=@NewAssignmentID

						delete PNAssignment Where PlanID=@PlanID 
							and TaskID=@OldTaskID 
							and isnull(WBS1,'')=isnull(@WBS1,'') 
							and isnull(WBS2,'')=isnull(@WBS2,'') 
							and isnull(WBS3,'')=isnull(@WBS3,'') 
							and isnull(ResourceID,'')=isnull(@ResourceID,'')
							and isnull(GenericResourceID,'')=@OldValue 

						fetch next from AssignmentCursor into @PlanID, @OldTaskID, @OldHardBooked, @WBS1, @WBS2, @WBS3, @ResourceID, @OldAssignmentID
						set @TempFetch = @@Fetch_Status
					end
					close AssignmentCursor
					deallocate AssignmentCursor

				END --Existing
--*****************************************************

			Update RPAssignment Set GenericResourceID = @NewValue,Category = @NewCategory WHERE GenericResourceID = @OldValue
			AND PlanID in (Select PlanID from RPPlan WHERE PlanID = RPAssignment.PlanID and GRMethod = 0)
			--Update the RPAssignment record to link to the LaborCode
			Update RPAssignment Set GenericResourceID = @NewValue,GRLBCD = @NewLaborCode WHERE GenericResourceID = @OldValue
			AND PlanID in (Select PlanID from RPPlan WHERE PlanID = RPAssignment.PlanID and GRMethod = 1)
			
			Update PNAssignment Set GenericResourceID = @NewValue,Category = @NewCategory WHERE GenericResourceID = @OldValue
			AND PlanID in (Select PlanID from PNPlan WHERE PlanID = PNAssignment.PlanID and GRMethod = 0)
			Update PNAssignment Set GenericResourceID = @NewValue,GRLBCD = @NewLaborCode WHERE GenericResourceID = @OldValue
			AND PlanID in (Select PlanID from PNPlan WHERE PlanID = PNAssignment.PlanID and GRMethod = 1)
			
			--Update the RPTopdownGR record to link to the Category
			Update RPTopdownGR Set GenericResourceID = @NewValue,Category = @NewCategory WHERE GenericResourceID = @OldValue
			AND PlanID in (Select PlanID from RPPlan WHERE PlanID = RPTopdownGR.PlanID and GRMethod = 0)
			--Update the RPTopdownGR record to link to the LaborCode
			Update RPTopdownGR Set GenericResourceID = @NewValue,LaborCode = @NewLaborCode WHERE GenericResourceID = @OldValue
			AND PlanID in (Select PlanID from RPPlan WHERE PlanID = RPTopdownGR.PlanID and GRMethod = 1)

			--Update GRSKills
			Update GRSKills Set Code = @NewValue Where Code = @OldValue
			
			--Remove the old GR record
			Delete From GR Where Code = @OldValue
		END --If (@NewValue = '')
		
--
		if (@RetVal = 0)
			begin
-- Add by DP 4.0
				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq)
					select PKey, 'GenericResource', OldCode, NewCode, Name, Name, @Period, @LastPostSeq from keyConvertWorkGR where Pkey = @PKey
				end
--
				delete from keyConvertWorkGR where current of keysCursor
				commit Transaction
			end
		else
			begin
				set @ErrMsg = @ErrMsg + @ErrMsg2
				rollback transaction
			end
			
			
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue, 
		@PKey,
		@NewName,
		@NewCategory,
		@NewLaborCode,
		@OldOrg,
		@Supervisor,
		@OldStatus
	set @KeysFetch = @@Fetch_Status
	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	return(0)
End -- Procedure
GO
