SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Object_Exists] ( @objectName varchar(512), @type varchar(50), @isDeltekSP bit= 0)
             AS EXEC spCCG_EI_Object_Exists @objectName,@type,@isDeltekSP
GO
