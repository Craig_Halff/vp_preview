SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_EmployeeRolesGridInsert] @Employee varchar(32)
AS 
/* 
Copyright (c) 2019 Central Consulting Group.  All rights reserved.
10/18/2019	David Springer
			Write Employee Role Log record
			Call this from an Employee INSERT or CHANGE workflow
03/06/2021	Craig H. Anderson
			Added support for 'Practice Leader - Regional'
*/
BEGIN
SET NOCOUNT ON

--	Show Action as Removed when unchecked
	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Authorized Agent'
	  and ex.CustRoleAuthorizedAgent = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Board of Directors'
	  and ex.CustRoleBoardofDirectors = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Chairman Emeritus'
	  and ex.CustRoleChairmanEmeritus = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Chairman of the Board'
	  and ex.CustRoleChairmanoftheBoard = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Chief Financial Officer'
	  and ex.CustRoleChiefFinancialOfficer = 'N'
	
	UPDATE l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Chief Marketing Officer'
	  and ex.CustRoleChiefMarketingOfficer = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Chief Operating Officer'
	  and ex.CustRoleChiefOperatingOfficer = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Chief Strategy Officer'
	  and ex.CustRoleChiefStrategyOfficer = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Corporate Secretary'
	  and ex.CustRoleCorporateSecretary = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Deputy Corporate Secretary'
	  and ex.CustRoleDeputyCorporateSecretary = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Director'
	  and ex.CustRoleDirector = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Executive Vice President'
	  and ex.CustRoleExecutiveVicePresident = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'General Counsel'
	  and ex.CustRoleGeneralCounsel = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Management Team'
	  and ex.CustRoleManagementTeam = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Practice Leader'
	  and ex.CustRolePracticeLeader = 'N'
	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Practice Leader - Regional'
	  and ex.CustRolePracticeLeaderRegional = 'N'
	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Operations Manager'
	  and ex.CustOperationsManager = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'President'
	  and ex.CustRolePresident = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Project Manager'
	  and ex.CustRoleProjectManager = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Secretary of Real Estate Services'
	  and ex.CustRoleSecofRealEstateServices = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Senior Vice President'
	  and ex.CustRoleSeniorVicePresident = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Stockholder'
	  and ex.CustRoleStockholder = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Team Leader'
	  and ex.CustTeamLeader = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Vice Chairman of the Board'
	  and ex.CustRoleViceChairmanoftheBoard = 'N'

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = @Employee
	  and ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Vice President'
	  and ex.CustRoleVicePresident = 'N'

--	Insert Role when missing from Role Log
	Insert Into Employees_RoleLog
	(Employee, Seq, CustRole, CustDate, CustAction)
	Select Employee, Replace (NewID(), '-', '') Seq, 'Authorized Agent' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleAuthorizedAgent = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Authorized Agent' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Board of Directors' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleBoardofDirectors = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Board of Directors' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Chairman Emeritus' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleChairmanEmeritus = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Chairman Emeritus' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Chairman of the Board' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleChairmanoftheBoard = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Chairman of the Board' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Chief Financial Officer' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleChiefFinancialOfficer = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Chief Financial Officer' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Chief Operating Officer' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleChiefOperatingOfficer = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Chief Operating Officer' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Chief Marketing Officer' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and e.CustRoleChiefMarketingOfficer = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Chief Marketing Officer' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Chief Strategy Officer' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleChiefStrategyOfficer = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Chief Strategy Officer' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Corporate Secretary' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleCorporateSecretary = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Corporate Secretary' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Deputy Corporate Secretary' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleDeputyCorporateSecretary = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Deputy Corporate Secretary' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Director' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleDirector = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Director' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Executive Vice President' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleExecutiveVicePresident = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Executive Vice President' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'General Counsel' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleGeneralCounsel = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'General Counsel' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Management Team' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleManagementTeam = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Management Team' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Operations Manager' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustOperationsManager = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Operations Manager' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Practice Leader' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRolePracticeLeader = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Practice Leader' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Practice Leader - Regional' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and e.CustRolePracticeLeaderRegional = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Practice Leader - Regional' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'President' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRolePresident = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'President' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Project Manager' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleProjectManager = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Project Manager' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Secretary of Real Estate Services' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleSecofRealEstateServices = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Secretary of Real Estate Services' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Senior Vice President' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleSeniorVicePresident = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Senior Vice President' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Stockholder' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleStockholder = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Stockholder' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Team Leader' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustTeamLeader = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Team Leader' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Vice Chairman of the Board' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleViceChairmanoftheBoard = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Vice Chairman of the Board' and CustAction = 'Added')
	UNION ALL
	Select Employee, Replace (NewID(), '-', '') Seq, 'Vice President' Role, getDate(), 'Added'
	From EmployeeCustomTabFields e
	Where Employee = @Employee
	  and CustRoleVicePresident = 'Y'
	  and not exists (Select 'x' From Employees_RoleLog Where Employee = e.Employee and CustRole = 'Vice President' and CustAction = 'Added')

END
GO
