SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--------------------------------------------------------------------------
CREATE   PROCEDURE [dbo].[UnitTypes_Get]
AS
BEGIN
  ------------------------------------------------------------------------
  SET NOCOUNT ON;

  ------------------------------------------------------------------------
  SELECT
    CCVD.InfoCenterArea
   ,CCVD.GridID
   ,CCVD.ColName
   ,CCVD.Code
   ,CCVD.UICultureName
   ,CCVD.DataValue
   ,CCVD.Seq
   ,CCVD.CreateUser
   ,CCVD.CreateDate
   ,CCVD.ModUser
   ,CCVD.ModDate
  FROM 
    dbo.FW_CustomColumnValuesData AS CCVD
  WHERE
    CCVD.GridID       = 'CustUnitsOfMeasure' 
    AND CCVD.ColName  = 'CustUnitOfMeasure'
  ORDER BY
    CCVD.Seq ASC
  ------------------------------------------------------------------------
END
GO
