SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_History_Add]
	@actionTakenStr	Nvarchar(20),
	@takenBy		Nvarchar(32),
	@recipient		Nvarchar(32),
	@itemSeq		varchar(20),
	@itemStageId	varchar(30),
	@description	Nvarchar(255),
	@delegateFor	Nvarchar(32) = null

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO CCG_PAT_History (ActionTaken, ActionDate, ActionTakenBy, ActionRecipient,
			PayableSeq,
			Stage, [Description],delegatefor)
		VALUES (@actionTakenStr, getutcdate(), @takenBy, @recipient,
			CASE WHEN ISNULL(@itemSeq, '') = '' THEN NULL ELSE CAST(@itemSeq as Int) END,
			NULLIF(@itemStageId, ''), Left(@description, 255),@delegateFor);

	SELECT @@ROWCOUNT As RowsAffected;
END;

GO
