SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Get_Custom_Columns]
AS
BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_EI_Config_Get_Custom_Columns]
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT syscolumns.name as KeyValue, syscolumns.name as Display
		FROM sysobjects
			INNER JOIN syscolumns on syscolumns.id = sysobjects.id
		WHERE sysobjects.Name = 'CCG_EI_CustomColumns'
			AND sysobjects.xtype = 'U'
			AND syscolumns.name not in ('WBS1','WBS2','WBS3')
			AND syscolumns.name not like '%_ModEmp'
		ORDER BY syscolumns.name;
END;
GO
