SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_GetPOData] ( @SelectType varchar(20), @SelectParam1 varchar(255), @SelectParam2 varchar(255), @OutputType varchar(20), @User varchar(50)= null)
             AS EXEC spCCG_PAT_GetPOData @SelectType,@SelectParam1,@SelectParam2,@OutputType,@User
GO
