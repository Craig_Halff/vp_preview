SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_ProjectAutoNumber]
    @Org VARCHAR(20)
  , @ContractType VARCHAR(255)
  , @MultiTeam VARCHAR(1)
  , @ChargeType VARCHAR(255)
  , @retAutoNo VARCHAR(30) OUTPUT
AS
    /*
		Copyright (c) 2021 Central Consulting Group. All rights reserved.
		07/01/2021	David Springer
					Project Number auto number
		08/04/2021  Jeremy Baummer
					Changed reference to Contract Type from Master Contract Type
		08/11/2021	David Springer
					Added Multi-Team Pursuit suffix
		10/13/2021	Craig H. anderson
					Added code to account for ChargeType so that NC or P projects don't get assigned regular numbers.
	*/
    DECLARE @Root VARCHAR(5);
    BEGIN
        IF ISNULL(@Org, '') = '' RAISERROR('Organization is required to derive project number suffix.', 16, 1);

        IF ISNULL(@ContractType, '') = '' RAISERROR('Contract Type is required to derive project number suffix.', 16, 1);

        IF ISNULL(@ChargeType, '') = '' RAISERROR('Charge Type is required to derive project number suffix.', 16, 1);

        SET @Org = IIF(LEFT(@Org, 2) = '02', 'G', '0');
        IF @ChargeType = 'R'
            BEGIN
                SELECT @Root = MAX(SUBSTRING(WBS1, 2, 5)) + 1
                FROM dbo.PR
                WHERE
                    ChargeType = 'R'
                    AND ISNUMERIC(SUBSTRING(WBS1, 2, 5)) = 1
                    AND WBS1 LIKE '0%'
                    AND WBS1 NOT LIKE '099%';

                IF @ContractType = 'Master Service Agmt (MSA)' SET @retAutoNo = @Org + @Root + '.000';
                ELSE SET @retAutoNo = @Org + @Root + '.001';

                IF @MultiTeam = 'Y' SET @retAutoNo = @Org + @Root + '.M01';
            END;
        ELSE IF @ChargeType = 'P'
                 BEGIN
                     SET @retAutoNo = 'P99999.999';
                 END;
        ELSE IF @ChargeType = 'H'
				BEGIN
					SET @retAutoNo = 'NC9999.999';
				END;
    END;
GO
