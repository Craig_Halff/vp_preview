SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Delete_Messages]
	@clearOptions		varchar(100),
	@optionDetail		Nvarchar(100)
AS
BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_EI_Config_Delete_Messages]
	SET NOCOUNT ON;
	SET @clearOptions = LOWER(@clearOptions);

	IF @clearOptions = 'selected'
		DELETE
			FROM CCG_EMAIL_MESSAGES
			WHERE SourceApplication = 'Electronic Invoicing' AND ToList = @optionDetail;
	ELSE IF @clearOptions = 'notvisionemployee'
		DELETE
			FROM CCG_EMAIL_MESSAGES
			WHERE SourceApplication = 'Electronic Invoicing'
				AND ToList not in (SELECT distinct email FROM EM WHERE not email is null);
	ELSE IF @clearOptions = 'before'
		DELETE
			FROM CCG_EMAIL_MESSAGES
			WHERE SourceApplication = 'Electronic Invoicing'
				AND CreateDateTime <= Cast(@optionDetail As DateTime);
	ELSE IF @clearOptions = 'after'
		DELETE
			FROM CCG_EMAIL_MESSAGES
			WHERE SourceApplication = 'Electronic Invoicing'
				AND CreateDateTime >= Cast(@optionDetail As DateTime);
	ELSE IF @clearOptions = 'all'
		DELETE
			FROM CCG_EMAIL_MESSAGES
			WHERE SourceApplication = 'Electronic Invoicing';

	SELECT @@ROWCOUNT;
END;
GO
