SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Ctrl_DoEntryColumnUpdate] (
	@spName					varchar(500),
	@EMPLOYEEID				Nvarchar(32),
	@DataType				Nvarchar(200),
	@ConfigCustomColumn		int,
	@ColumnLabel			Nvarchar(500),
	@DatabaseField			Nvarchar(100),
	@seq					int,
	@decValue				bit
) AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max);
	DECLARE @ParmDefinition Nvarchar(max);
	DECLARE @rtnMsg Nvarchar(max);
	DECLARE @rtnVal int;

	IF ISNULL(@spName, '') <> '' BEGIN
		SET @sql = N'EXEC '+@spName+' @PayableSeq, @ReturnValue OUTPUT, @ReturnMessage OUTPUT';
		SET @ParmDefinition=N'
			@PayableSeq int,
			@ReturnValue INT OUTPUT,
			@ReturnMessage Nvarchar(255) OUTPUT';

		EXECUTE sp_executesql
			@sql,
			@ParmDefinition,
			@PayableSeq = @seq,
			@ReturnValue = @rtnVal OUTPUT,
			@ReturnMessage = @rtnMsg OUTPUT;
	END;

    SET @sql = N'
        INSERT INTO CCG_PAT_HistoryUpdateColumns (ActionTaken, ActionDate, ActionTakenBy, PayableSeq, DataType,
				ConfigCustomColumn, ColumnLabel, OldValue, NewValue,
				OldValueDecimal, NewValueDecimal)
			SELECT TOP 1 N''Update'', getutcdate(), N''' + @EMPLOYEEID + ''', ' + @seq + ', ''' + @DataType + ''',
					' + @ConfigCustomColumn + ', N''' + Replace(@ColumnLabel, '''', '''''') + ''', NULL, eicc.' + @DatabaseField + ',
					NULL, ' + (CASE WHEN @decValue = 1 THEN 'eicc.' + @DatabaseField ELSE 'NULL' END) + '
				FROM CCG_PAT_CustomColumns eicc
				WHERE PayableSeq = ' + @seq;
	EXEC(@sql);

	SELECT @rtnVal as ReturnValue, @rtnMsg as ReturnMessage;
END;

GO
