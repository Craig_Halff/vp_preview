SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_OpportunityClientGrid]
    @OpportunityID  VARCHAR(32), 
    @OldClientID    VARCHAR (32), 
    @NewClientID    VARCHAR (32), 
    @RoleID         VARCHAR (32)
AS 
/* 
Copyright (c) 2020 Central Consulting Group.  All rights reserved.
04/09/2020  David Springer
            Remove companies from the Companies grid when they are removed 
            from Billing (01) or Owner (sysOwner).
            Insert for Billing Client and Update of Owner Company.
            Protect Billing Client if Owner Company is changed.
            Pass RoleID of the field being changed (sysOwner or 01).
            Call this on Opportunity INSERT for Billing Client not empty.
            Call this on Opportunity CHANGE for both Billing Client & Owner Company changed.
4/5/2021	Craig H. Anderson
			Added code to reduce chance of creating duplicate sysOwners
*/
BEGIN
SET NOCOUNT ON
--  If Billing Company now remains, change the Role to Billing (01)
       Update a
       Set a.Role = @RoleID
       From OpportunityClientAssoc a, OpportunityCustomTabFields o
       Where o.OpportunityID = @OpportunityID
         and o.OpportunityID = a.OpportunityID
         and o.CustBillingCompany = a.ClientID

--  If Client Company now remains, change the Role to Owner (sysOwner)
       Update a
       Set a.Role = 'sysOwner'
       From OpportunityClientAssoc a, Opportunity o
       Where o.OpportunityID = @OpportunityID
         and o.OpportunityID = a.OpportunityID
         and o.ClientID      = a.ClientID

--  Delete Company record
       Delete a
       From OpportunityClientAssoc a
       Where OpportunityID = @OpportunityID
         and ClientID = @OldClientID
         and not exists (Select 'x' From Opportunity Where OpportunityID = a.OpportunityID and ClientID = a.ClientID)
         and not exists (Select 'x' From OpportunityCustomTabFields Where OpportunityID = a.OpportunityID and CustBillingCompany = a.ClientID)

--  Delete Contact record that belongs to the same company
       Delete o
       From OpportunityContactAssoc o, Contacts c
       Where o.OpportunityID = @OpportunityID
         and o.ContactID = c.ContactID
         and c.ClientID = @OldClientID
         and not exists (Select 'x' From Opportunity Where OpportunityID = o.OpportunityID and ContactID = o.ContactID)
         and not exists (Select 'x' From OpportunityCustomTabFields Where OpportunityID = o.OpportunityID and CustBillingContact = o.ContactID)

       If @NewClientID <> '' and 
              not exists (Select * From OpportunityClientAssoc Where OpportunityID = @OpportunityID and ClientID = @NewClientID)
              Begin
              Insert Into OpportunityClientAssoc
              (OpportunityID, ClientID, Role, CreateDate, CreateUser)
              Values (@OpportunityID, @NewClientID, @RoleID, getDate(), 'CCG_Procedure')
              End

--  This is because Vision creates the sysOwner company record before this procedure fires.
--  Therefore, we need to set the role for the new company after this procedure deletes the old company.
		If @RoleID = 'sysOwner'
			Begin
				Update OpportunityClientAssoc 
				Set Role = 'sysOwner' 
				Where OpportunityID = @OpportunityID
				and ClientID = @NewClientID
				AND NOT EXISTS ( SELECT 'x' FROM OpportunityClientAssoc WHERE OpportunityID = @OpportunityID and Role = 'sysOwner' ) -- added to avoid duplicate sysOwners

				Update o
				Set Role = 'sysOwner'
				From OpportunityContactAssoc o, Contacts c
				Where o.OpportunityID = @OpportunityID
				and o.ContactID   = c.ContactID
				and c.ClientID    = @NewClientID
				AND NOT EXISTS ( SELECT 'x' FROM OpportunityContactAssoc WHERE OpportunityID = @OpportunityID and Role = 'sysOwner' ) -- added to avoid duplicate sysOwners
			End
END

GO
