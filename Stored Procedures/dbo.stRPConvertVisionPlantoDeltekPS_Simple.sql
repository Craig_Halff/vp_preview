SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPConvertVisionPlantoDeltekPS_Simple]
AS

BEGIN -- Procedure stRPConvertVisionPlantoDeltekPS_Simple

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to convert Plans from Vision format to DeltekPS format. 
-- The Vision DB must have DeltekPS 1.0 Schema applied already.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Attempt to correct Vision Plans mapping issues.

  EXECUTE dbo.RPFixMappings
  EXECUTE dbo.sp_stRPFixDuplicateAssignments
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @strPlanID varchar(32)
  DECLARE @strWBS1 nvarchar(30)

  DECLARE @tabPlan TABLE (
    PlanID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default
    UNIQUE(PlanID)
  )

  DECLARE @tabPlanHasTPDOnWBSLevelOnly TABLE (
    PlanID varchar(32) COLLATE database_default 
    UNIQUE(PlanID)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find Vision Plans that are iAcces-Compliant.
  -- Must use a temp table here otherwise the cursor will get confused when Plans are being changed inside a loop. 

  INSERT @tabPlan(
    PlanID,
    WBS1
  )
    SELECT DISTINCT 
      RP.PlanID AS PlanID,
      RP.WBS1 AS WBS1
      FROM RPPlan AS RP
        INNER JOIN PR ON RP.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
        LEFT JOIN PNPlan AS PN ON RP.PlanID = PN.PlanID 
        CROSS APPLY dbo.stRP$tabPlanValidate(RP.PlanID) AS V
      WHERE RP.UtilizationIncludeFlg = 'Y'
        AND PN.PlanID IS NULL
        AND V.MappedToMultipleProjs = 0
        AND V.HasIllegalLevel = 0
        AND V.HasUnmappedWBSRows = 0
        AND V.HasNegativeTPDAmount = 0
        AND V.HasMultipleTSKMappedtoPR = 0
		and V.HasMissingResourceAssignment = 0
        AND RP.PlanID NOT IN (
          SELECT PlanID
            FROM (
              SELECT
                T.PlanID,
                PR.WBS1 AS WBS1, 
                COUNT(*) OVER (PARTITION BY PR.WBS1) AS CountRows
                FROM (
                  SELECT DISTINCT
                    XT.PlanID,
                    XT.WBS1,
                    XT.WBS2,
                    XT.WBS3
                    FROM RPTask AS XT 
                    WHERE XT.WBSType = 'WBS1' AND XT.WBS2 IS NULL AND XT.WBS3 IS NULL
                ) AS T
                  INNER JOIN PR ON T.WBS1 = PR.WBS1 AND ISNULL(T.WBS2, ' ') = PR.WBS2 AND ISNULL(T.WBS3, ' ') = PR.WBS3
                WHERE PR.WBS2 = ' ' AND PR.WBS3 = ' '
            ) AS X
            WHERE CountRows > 1
        )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  --Back up vision plan data for those converted plan
  Select *  INTO RPPlan_Backup 
  FROM RPPlan WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPTask_Backup 
  FROM RPTask WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPAssignment_Backup 
  FROM RPAssignment WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPExpense_Backup 
  FROM RPExpense WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPConsultant_Backup 
  FROM RPConsultant WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPUnit_Backup 
  FROM RPUnit WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPPlannedLabor_Backup 
  FROM RPPlannedLabor WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPPlannedExpenses_Backup 
  FROM RPPlannedExpenses WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPPlannedConsultant_Backup 
  FROM RPPlannedConsultant WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPPlannedUnit_Backup 
  FROM RPPlannedUnit WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPCompensationFee_Backup 
  FROM RPCompensationFee WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPConsultantFee_Backup 
  FROM RPConsultantFee WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPReimbAllowance_Backup 
  FROM RPReimbAllowance WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPBaselineLabor_Backup 
  FROM RPBaselineLabor WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPBaselineExpenses_Backup 
  FROM RPBaselineExpenses WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPBaselineConsultant_Backup 
  FROM RPBaselineConsultant WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPBaselineUnit_Backup 
  FROM RPBaselineUnit WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPEVT_Backup 
  FROM RPEVT WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

  Select *  INTO RPDependency_Backup 
  FROM RPDependency WHERE PlanID IN (SELECT PlanID FROM @tabPlan)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

--Check if the plan contains Planned and/or Baseline TPD Hours Only On Mapped WBS or Labor Code Row  
  INSERT @tabPlanHasTPDOnWBSLevelOnly(
    PlanID
  )
    SELECT DISTINCT P.PlanID  FROM RPPlannedLabor P
  INNER JOIN @tabPlan TP ON TP.PlanID = P.PlanID
  INNER JOIN RPTask T ON P.PlanID = T.PlanID AND P.TaskID = T.TaskID AND T.ChildrenCount = 0
  LEFT JOIN RPAssignment A ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID AND (
  A.ResourceID IS NOT NULL OR A.GenericResourceID IS NOT NULL)
  WHERE PeriodHrs > 0 AND P.AssignmentID IS NULL  AND A.AssignmentID IS NULL
    
  UNION
    SELECT DISTINCT P.PlanID  FROM RPBaselineLabor P
  INNER JOIN @tabPlan TP ON TP.PlanID = P.PlanID
  INNER JOIN RPTask T ON P.PlanID = T.PlanID AND P.TaskID = T.TaskID AND T.ChildrenCount = 0
  LEFT JOIN RPAssignment A ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID AND (
  A.ResourceID IS NOT NULL OR A.GenericResourceID IS NOT NULL)
  WHERE PeriodHrs > 0 AND P.AssignmentID IS NULL  AND A.AssignmentID IS NULL

 IF EXISTS (SELECT 'X' FROM @tabPlanHasTPDOnWBSLevelOnly)
 BEGIN
  --Create a unallocated generic resource
  DECLARE @strGRName nvarchar(50)
  DECLARE @strGRCode nvarchar(50)

  IF EXISTS(SELECT 'X'  FROM GR WHERE Code = 'UNALLOCATED') 
  BEGIN
    SET @strGRCode = 'UNALLOCATED1'
    END    
  ELSE
  BEGIN
    SET @strGRCode = 'UNALLOCATED'
    END  
  
  IF EXISTS(SELECT 'X'  FROM GR WHERE Name = 'UNALLOCATED (CREATED FOR UPGRADE)') 
  BEGIN
    SET @strGRName = 'UNALLOCATED (CREATED FOR UPGRADE)1'
    END    
  ELSE
  BEGIN
    SET @strGRName = 'UNALLOCATED (CREATED FOR UPGRADE)'
    END    

  --Create a NonAllocated Generic Resource
  INSERT INTO GR (Code,Name,Status) Values (@strGRCode, @strGRName,'I')
 END

 -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  --Use the cursor to convert all plans in @tabPlan 
  DECLARE csr20161206_RPPlan CURSOR LOCAL FAST_FORWARD FOR
    SELECT PlanID, WBS1
      FROM @tabPlan AS RP

  OPEN csr20161206_RPPlan
  FETCH NEXT FROM csr20161206_RPPlan INTO @strPlanID, @strWBS1

  WHILE (@@FETCH_STATUS = 0)
    BEGIN

      PRINT '>>> Start Processing PlanID = "' + @strPlanID + '", WBS1 = "' + @strWBS1 + '"'
      EXECUTE dbo.stRPCreatePNPlanFromRPPlan @strWBS1
      PRINT '<<< End Processing PlanID = "' + @strPlanID + '", WBS1 = "' + @strWBS1 + '"'

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      FETCH NEXT FROM csr20161206_RPPlan INTO @strPlanID, @strWBS1

    END -- While

  CLOSE csr20161206_RPPlan
  DEALLOCATE csr20161206_RPPlan

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   -- Now we can remove all plans which has no PNPlan record
  DECLARE deleteRPPlanCursor CURSOR FOR
  SELECT PlanID FROM RPPlan WHERE PlanID NOT IN (SELECT PlanID FROM PNPlan)
      
  OPEN deleteRPPlanCursor
  FETCH NEXT FROM deleteRPPlanCursor INTO @strPlanID 

  WHILE (@@FETCH_STATUS = 0)
    BEGIN
	  EXECUTE dbo.PMDelPlan @strPlanID

      FETCH NEXT FROM deleteRPPlanCursor INTO @strPlanID 
    END -- While

  CLOSE deleteRPPlanCursor
  DEALLOCATE deleteRPPlanCursor
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
END -- stRPConvertVisionPlantoDeltekPS_Simple
GO
