SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_PctCompleteSaveToVision] ( @wbs1 nvarchar(30), @wbs2 nvarchar(7), @wbs3 nvarchar(7), @seq varchar(20), @wbsList nvarchar(max), @removeFromEI int)
             AS EXEC spCCG_EI_PctCompleteSaveToVision @wbs1,@wbs2,@wbs3,@seq,@wbsList,@removeFromEI
GO
