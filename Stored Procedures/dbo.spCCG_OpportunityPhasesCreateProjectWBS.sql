SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_OpportunityPhasesCreateProjectWBS] @OpportunityID varchar (32)
AS
/*
Copyright 2017 Central Consulting Group.   All rights reserved.
01/26/2017 David Springer
           The function to create the regular project must be called before this procedure.
           Call this from a workflow with identical conditions to the function above.
           The function must NOT invoke after all changes are saved.
           This procedure MUST invoke after all changes are saved.
03/14/2017 David Springer
           Added Sales grid instead of mapping to default Contract No and Date
04/11/2017 David Springer
           Added Reimb Expense, Reimb Consultant & Labor Multipliers
01/19/2018 David Springer
           Added Reimb Mileage Multiplier for EnLink
05/25/2018 David Springer
           Added UnitTableAllLevels
06/05/2018 David Springer
           Unit table based on company & timesheet comments for Tritex
*/

Declare @RegularWBS1  varchar (32),
        @BillingType  varchar (255),
        @ExpenseType  varchar (255),
        @WBS2         varchar (7),
        @WBS3         varchar (7),
        @Fee          float,
        @ConsultFee   float,
        @ReimbAllow   float

BEGIN
SET NOCOUNT ON

   Select @RegularWBS1 = PRWBS1 From Opportunity Where OpportunityID = @OpportunityID

-- Populate Documents Grid
   Insert Into Projects_FinalInvPkgUserDocuments
   (WBS1, WBS2, WBS3, Seq, CustOrder, CustDocType, CustDocument)
   Values
   (@RegularWBS1, ' ', ' ', Replace (NewID(), '-', ''), 1, 'Template', 'Invoice')

-- EnLink Mileage Multiplier
   Update px
   Set px.CustReimbMileageMultiplier = 1.1009
   From PR p, ProjectCustomTabFields px
   Where p.WBS1 = @RegularWBS1
     and p.WBS1 = px.WBS1
	 and p.WBS2 = px.WBS2
	 and p.WBS3 = px.WBS3
	 and p.BillingClientID = '001776'  -- EnLink

-- Project level tax code
   Update px
   Set px.CustTaxCode = ox.CustTaxCode
   From PR p, ProjectCustomTabFields px, OpportunityCustomTabFields ox
   Where p.WBS1 = @RegularWBS1
     and p.WBS1 = px.WBS1
     and p.WBS2 = px.WBS2
     and px.WBS2 = ' ' -- project level
	 and px.CustBillingType <> 'Sublevel Terms'
	 and p.OpportunityID = ox.OpportunityID

   Update p
   Set p.Memo = o.Description,
       p.UnitTable = CASE When Left (p.Org, (Select Org1Length From CFGFormat)) = '01' and p.ClientID <> '000301' Then '000001 - STANDARD UNIT TABLE' 
						When Left (p.Org, (Select Org1Length From CFGFormat)) = '01' and p.ClientID = '000301' Then 'ATMOS MASTER'
						When Left (p.Org, (Select Org1Length From CFGFormat)) = '02' Then '000002-GLD STANDARD UNIT TABLE'
						Else p.UnitTable END,
	   p.RequireComments = CASE When p.ClientID = '000301' or Left (p.Org, (Select Org1Length From CFGFormat)) = '03' Then 'Y'  Else p.RequireComments END,
	-- Fee added for project level projects without phases
	   p.FeeDirLab = ox.CustRevenueFeeDirLab,
       p.FeeDirExp = ox.CustRevenueFeeDirExp,
       p.Fee = ox.CustRevenueFeeDirLab + ox.CustRevenueFeeDirExp,
	   p.ConsultFee = ox.CustRevenueConsultFee,
	   p.ReimbAllowCons = ox.CustRevenueReimbAllowCons,
	   p.ReimbAllowExp = ox.CustRevenueReimbAllowExp,
	   p.ReimbAllow = ox.CustRevenueReimbAllowCons + ox.CustRevenueReimbAllowExp
   From PR p, Opportunity o, OpportunityCustomTabFields ox
   Where p.WBS1 = @RegularWBS1
     and WBS2 = ' '
	 and o.OpportunityID = @OpportunityID
	 and o.OpportunityID = ox.OpportunityID

If exists (Select * From Opportunities_Phases Where OpportunityID = @OpportunityID)
   Begin
-- Create phases from grid
   Insert Into PR
   (WBS1, WBS2, WBS3, OpportunityID, ProposalWBS1, Name, LongName, 
    FeeDirLab, FeeDirExp, Fee, ConsultFee, ReimbAllowCons, ReimbAllowExp, ReimbAllow, 
	ProjectCurrencyCode, BillingCurrencyCode, UnitTable, ChargeType, Org, SubLevel, Status,
    Principal, ProjMgr, Supervisor, 
    ClientID, CLAddress, ContactID, 
    BillingClientID, CLBillingAddr, BillingContactID, 
    RevType, RevType2, RevType3, RevenueMethod, RequireComments, AvailableForCRM, ReadyForApproval, ReadyForProcessing)
   Select 
      p.WBS1, t.CustPhaseCode, ' ' WBS3, p.OpportunityID, p.ProposalWBS1, Left (t.CustPhaseName, 40), t.CustPhaseName, 
      t.CustPhaseFeeDirLab, t.CustPhaseFeeDirExp, t.CustPhaseFeeDirLab + t.CustPhaseFeeDirExp, CustPhaseConsultFee, t.CustPhaseReimbAllowCons, t.CustPhaseReimbAllowExp, t.CustPhaseReimbAllowCons + t.CustPhaseReimbAllowExp, 
	  p.ProjectCurrencyCode, p.BillingCurrencyCode, p.UnitTable, p.ChargeType, p.Org, 'N' SubLevel, 'A' Status, 
      p.Principal, p.ProjMgr, p.Supervisor, 
      p.ClientID, p.CLAddress, p.ContactID, 
      p.BillingClientID, p.CLBillingAddr, p.BillingContactID,
      p.RevType, p.RevType2, p.RevType3, p.RevenueMethod,  p.RequireComments,
      p.AvailableForCRM, p.ReadyForApproval, p.ReadyForProcessing
   From Opportunities_Phases t, PR p
   Where t.OpportunityID = @OpportunityID
     and p.WBS1 = @RegularWBS1
     and p.WBS2 = ' '  -- Project level only
     and t.CustPhaseCode is not null
     and t.CustPhaseTotal <> 0

-- Add Custom Phase records
   Insert Into ProjectCustomTabFields
   (WBS1, WBS2, WBS3, CustFunctionalArea, CustManagementLeader, CustPracticeArea, CustPracticeAreaLeader, CustDirector, CustAdministrativeAssistant, 
    CustBillingType, CustLaborFringeMultiplier, CustLaborOvhProfitMultiplier, CustReimbExpenseMultiplier, CustReimbConsultantMultiplier, CustReimbMileageMultiplier, CustTaxCode, CustUnitTableAllLevels)
   Select @RegularWBS1, o.CustPhaseCode WBS2, ' ' WBS3, o.CustPhaseFunctionalArea, px.CustManagementLeader, px.CustPracticeArea, px.CustPracticeAreaLeader, px.CustDirector, px.CustAdministrativeAssistant, 
    CASE When t.CustBillingType = 'Sublevel Terms' Then o.CustPhaseBillingType Else null END BillingType, 
	CASE When t.CustBillingType = 'Sublevel Terms' and IsNull (o.CustPhaseBillingType, 'Non-Billable') <> 'Non-Billable' Then px.CustLaborFringeMultiplier Else 0 END, 
	CASE When t.CustBillingType = 'Sublevel Terms' and ISNull (o.CustPhaseBillingType, 'Non-Billable') <> 'Non-Billable' Then px.CustLaborOvhProfitMultiplier Else 0 END, 
	CASE When t.CustBillingType = 'Sublevel Terms' and ISNull (o.CustPhaseBillingType, 'Non-Billable') <> 'Non-Billable' Then px.CustReimbExpenseMultiplier Else 0 END, 
	CASE When t.CustBillingType = 'Sublevel Terms' and ISNull (o.CustPhaseBillingType, 'Non-Billable') <> 'Non-Billable' Then px.CustReimbConsultantMultiplier Else 0 END,
	CASE When t.CustBillingType = 'Sublevel Terms' and ISNull (o.CustPhaseBillingType, 'Non-Billable') <> 'Non-Billable' Then px.CustReimbMileageMultiplier Else 0 END,
	CASE When t.CustBillingType = 'Sublevel Terms' and o.CustPhaseTax = 'Y' Then t.CustTaxCode Else null END TaxCode, 'Y' UnitTableAllLevels
   From Opportunities_Phases o, ProjectCustomTabFields px, OpportunityCustomTabFields t
   Where o.OpportunityID = @OpportunityID
     and o.CustPhaseCode is not null
     and o.CustPhaseTotal <> 0
	 and px.WBS1 = @RegularWBS1
	 and px.WBS2 = ' '
	 and o.OpportunityID = t.OpportunityID

-- Create tasks from grid
   Insert Into PR
   (WBS1, WBS2, WBS3, OpportunityID, ProposalWBS1, Name, LongName, 
    FeeDirLab, FeeDirExp, Fee, ConsultFee, ReimbAllowCons, ReimbAllowExp, ReimbAllow, 
	ProjectCurrencyCode, BillingCurrencyCode, UnitTable, ChargeType, Org, SubLevel, Status,
    Principal, ProjMgr, Supervisor, 
    ClientID, CLAddress, ContactID, 
    BillingClientID, CLBillingAddr, BillingContactID,
    RevType, RevType2, RevType3, RevenueMethod, RequireComments, AvailableForCRM, ReadyForApproval, ReadyForProcessing)
   Select 
      p.WBS1, t.CustTaskPhaseCode, t.CustTaskCode WBS3, p.OpportunityID, p.ProposalWBS1, Left (t.CustTaskName, 40), t.CustTaskName, 
      t.CustTaskFeeDirLab, t.CustTaskFeeDirExp, t.CustTaskFeeDirLab + t.CustTaskFeeDirExp, t.CustTaskConsultFee, 
	  t.CustTaskReimbAllowCons, t.CustTaskReimbAllowExp, t.CustTaskReimbAllowCons + t.CustTaskReimbAllowExp, 
	  p.ProjectCurrencyCode, p.BillingCurrencyCode, p.UnitTable, p.ChargeType, p.Org, 'N' SubLevel, 'A' Status, 
      p.Principal, p.ProjMgr, p.Supervisor, 
      p.ClientID, p.CLAddress, p.ContactID, 
      p.BillingClientID, p.CLBillingAddr, p.BillingContactID,
      p.RevType, p.RevType2, p.RevType3, p.RevenueMethod, p.RequireComments,
      p.AvailableForCRM, p.ReadyForApproval, p.ReadyForProcessing
   From Opportunities_Tasks t, PR p
   Where t.OpportunityID = @OpportunityID
     and p.WBS1 = @RegularWBS1
     and p.WBS2 = ' '  -- Project level only
     and t.CustTaskCode is not null
     and t.CustTaskTotal <> 0

-- Add Custom Task records
   Insert Into ProjectCustomTabFields
   (WBS1, WBS2, WBS3, CustFunctionalArea, CustManagementLeader, CustPracticeArea, CustPracticeAreaLeader, CustDirector, CustAdministrativeAssistant, CustUnitTableAllLevels)
   Select @RegularWBS1, o.CustTaskPhaseCode WBS2, o.CustTaskCode WBS3, o.CustTaskFunctionalArea, px.CustManagementLeader, px.CustPracticeArea, px.CustPracticeAreaLeader, px.CustDirector, px.CustAdministrativeAssistant, 'Y' UnitTableAllLevels
   From Opportunities_Tasks o, ProjectCustomTabFields px
   Where o.OpportunityID = @OpportunityID
     and o.CustTaskCode is not null
     and o.CustTaskTotal <> 0
	 and px.WBS1 = @RegularWBS1
	 and px.WBS2 = ' ' -- project level

-- Rollup task fees to phase & set Sublevel flag
   Update p
   Set p.FeeDirLab = t.FeeDirLab, 
       p.FeeDirExp = t.FeeDirExp, 
       p.Fee = t.Fee, 
       p.ConsultFee = t.ConsultFee, 
	   p.ReimbAllowCons = t.ReimbAllowCons, 
	   p.ReimbAllowExp = t.ReimbAllowExp, 
	   p.ReimbAllow = t.ReimbAllow,
	   p.Sublevel = 'Y'
   From PR p,
  (Select WBS1, WBS2,
          IsNull (sum (FeeDirLab) , 0) FeeDirLab, 
          IsNull (sum (FeeDirExp) , 0) FeeDirExp,
          IsNull (sum (Fee) , 0) Fee, 
          IsNull (sum (ConsultFee), 0) ConsultFee,
          IsNull (sum (ReimbAllowCons), 0) ReimbAllowCons,
          IsNull (sum (ReimbAllowExp), 0) ReimbAllowExp,
          IsNull (sum (ReimbAllow), 0) ReimbAllow
   From PR
   Where WBS3 <> ' ' -- Task records
     and FeeDirLab + FeeDirExp + ConsultFee + ReimbAllowCons + ReimbAllowExp <> 0
   Group by WBS1, WBS2) t -- Group by Phase
   Where p.WBS1 = @RegularWBS1
     and p.WBS2 <> ' ' -- Phase level
     and p.WBS3 = ' '  -- Phase level only
     and p.WBS1 = t.WBS1 
     and p.WBS2 = t.WBS2

-- Rollup phase fees to project & set Sublevel flag
   Update p
   Set p.FeeDirLab = t.FeeDirLab, 
       p.FeeDirExp = t.FeeDirExp, 
       p.Fee = t.Fee, 
       p.ConsultFee = t.ConsultFee, 
	   p.ReimbAllowCons = t.ReimbAllowCons, 
	   p.ReimbAllowExp = t.ReimbAllowExp, 
	   p.ReimbAllow = t.ReimbAllow,
	   p.SubLevel = 'Y'
   From PR p,
  (Select WBS1, 
          IsNull (sum (FeeDirLab) , 0) FeeDirLab, 
          IsNull (sum (FeeDirExp) , 0) FeeDirExp,
          IsNull (sum (Fee) , 0) Fee, 
          IsNull (sum (ConsultFee), 0) ConsultFee,
          IsNull (sum (ReimbAllowCons), 0) ReimbAllowCons,
          IsNull (sum (ReimbAllowExp), 0) ReimbAllowExp,
          IsNull (sum (ReimbAllow), 0) ReimbAllow
   From PR
   Where WBS2 <> ' ' -- Phase records
     and WBS3 = ' '  -- Exclude tasks
   Group by WBS1) t -- Group by Project
   Where p.WBS1 = @RegularWBS1
     and p.WBS1 = t.WBS1 
     and p.WBS2 = ' '  -- Project level only

	End -- Phases exist

-- Set Billing & Functional Currency at all levels
   Update PR
   Set FeeDirLabBillingCurrency = FeeDirlab, 
       FeeDirLabFunctionalCurrency = FeeDirLab, 
       FeeDirExpBillingCurrency = FeeDirExp, 
       FeeDirExpFunctionalCurrency = FeeDirExp, 
       FeeBillingCurrency = Fee, 
       FeeFunctionalCurrency = Fee, 
       ConsultFeeBillingCurrency = ConsultFee, 
       ConsultFeeFunctionalCurrency = ConsultFee,
       ReimbAllowConsBillingCurrency = ReimbAllowCons,
       ReimbAllowConsFunctionalCurrency = ReimbAllowCons,
       ReimbAllowExpBillingCurrency = ReimbAllowExp,
       ReimbAllowExpFunctionalCurrency = ReimbAllowExp,
       ReimbAllowBillingCurrency = ReimbAllow,
       ReimbAllowFunctionalCurrency = ReimbAllow
   Where WBS1 = @RegularWBS1

-- Create Billing Terms
   Declare curBillingTypeWBS insensitive cursor for
        Select WBS2, WBS3, CustBillingType
        From ProjectCustomTabFields
        Where WBS1 = @RegularWBS1
          and IsNull (CustBillingType, 'Non-Billable') <> 'Non-Billable'
        Order by 1, 2

   OPEN curBillingTypeWBS
   FETCH NEXT From curBillingTypeWBS into @WBS2, @WBS3, @BillingType
   WHILE @@FETCH_STATUS = 0
      BEGIN

      exec spCCG_ProjectBillingTermsInsert @RegularWBS1, @WBS2, @WBS3, @BillingType
   
      FETCH NEXT From curBillingTypeWBS into @WBS2, @WBS3, @BillingType
      
      END
   CLOSE curBillingTypeWBS
   DEALLOCATE curBillingTypeWBS

-- Create Contract Management grids
   exec spCCG_ProjectContractMgmtInsert @RegularWBS1

-- Create Sales grid with Contract No = Original
   Insert Into Projects_Sales
   (WBS1, WBS2, WBS3, Seq, CustSalesContract, CustSalesDate, CustSalesTeam, CustSalesAmount)
   Select @RegularWBS1, ' ', ' ', Replace (NewID(), '-', ''), '000', getDate(), CustSalesTeam, CustSalesAmount
   From Opportunities_Sales
   Where OpportunityID = @OpportunityID

-- Copy Attachments
   Insert Into FW_Attachments
   (PKey, Key1, Key2, Key3, Application, FileID, CategoryCode, CreateUser, CreateDate, ModUser, ModDate)
   Select Replace (NewID(), '-', '') PKey, @RegularWBS1, ' ' WBS2, ' ' WBS3, 'Projects', FileID, CategoryCode, CreateUser, CreateDate, ModUser, ModDate
   From FW_Attachments
   Where Key1 = @OpportunityID
     and Application = 'Opportunities'

END
GO
