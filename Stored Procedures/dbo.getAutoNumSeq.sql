SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[getAutoNumSeq] @navID varchar(50), @chargeType varchar(1), 
@resetPart Nvarchar(50), @nonSeqPart Nvarchar(50),
@retAutoNum			Nvarchar(50)	OUTPUT
as
declare
  @mainAppTable			varchar(50), 
  @numCol			varchar(50),
  @seqLen			integer,
  @AutoNumPos			integer,
  @maxLen			integer,
  @leadZeros			varchar(1),
  @delimit1			varchar(1),
  @delimit1Pos			smallint,
  @delimit2			varchar(1),
  @delimit2Pos			smallint,
  @NextAutoNum			integer,
  @LoopStartNum			integer,
  @NextAutoNumStr		Nvarchar(50),
  @TempStr			Nvarchar(50),
  @i				integer,
  @SqlStmt			Nvarchar(800),
  @wkQry 			Nvarchar(800),
  @Taken			varchar(1),
  @exitLoop			bit,
  @exitFunction			bit
BEGIN
	SET @exitLoop = 0
	SET @retAutoNum = ''
		SELECT @exitFunction = CASE 
		WHEN @navID = 'client' 		THEN 0
		WHEN @navID = 'employee' 	THEN 0
		WHEN @navID = 'wbs1' 		THEN 0
		WHEN @navID = 'vendor' 		THEN 0
		WHEN @navID = 'mktCampaign' 	THEN 0
		WHEN @navID = 'equipment' 	THEN 0
		WHEN @navID = 'opp' 		THEN 0
		WHEN @navID = 'rp' 		THEN 0
	    WHEN SUBSTRING(@navid,1,5) = 'UDIC_' THEN 0
		ELSE	1		END
	If (@exitFunction = 1)	
	BEGIN
		RAISERROR ('Auto number could not be generated for this info center.',16,3,@navID,@navID)
		RETURN 0
	END
	
	IF @navID IS NULL set @navID = ''
	IF @chargeType IS NULL set @chargeType = ' '
	IF (SUBSTRING(@navid,1,5) = 'UDIC_')
		BEGIN
			SET @MaxLen = 20
			SET @leadZeros = 'N'
			SET @delimit1 = ''
			SET @delimit1Pos = 0
			SET @delimit2 = ''
			SET @delimit2Pos = 0
			SELECT @seqLen = AutoNumSeqLen, 
				   @AutoNumPos = AutoNumSeqPos,
		           @mainAppTable = TableName,
				   @numCol = FW_CustomColumns.Name
			FROM FW_UDIC
			JOIN FW_CUSTOMCOLUMNS ON INFOCENTERAREA = UDIC_ID 
			WHERE UDIC_ID = @navID AND FW_CustomColumns.DataType = 'recordID'
		END
	ELSE
		BEGIN	
  			-- Rahul Panchal 02/20/2020 TFS:1252967 Marketing Campaigns: Unable to save new Marketing Campaigns due to auto-numbering. Error Message: The value of the next auto-number is greater than the maximum allowable auto-number field length.
			-- max length for mktCampaign, equipment, rp based on the table because it’s not available in CFGFormat
  			SELECT @maxLen = CASE (SELECT @navID)
				WHEN 'client' 		THEN clientLength
				WHEN 'employee' 	THEN employeeLength
				WHEN 'wbs1' 		THEN wbs1Length
				WHEN 'vendor' 		THEN vendorLength
				WHEN 'mktCampaign' 	THEN 50
				WHEN 'equipment' 	THEN 50
				WHEN 'rp' 			THEN 30 
				ELSE	0		END
			FROM CFGFormat			
			SELECT @leadZeros = CASE (SELECT @navID)
				WHEN 'client' 		THEN clientLeadZeros
				WHEN 'employee' 	THEN employeeLeadZeros
				WHEN 'wbs1' 		THEN WBS1LeadZeros
				WHEN 'vendor' 		THEN vendorLeadZeros	END
			FROM CFGFormat
			SELECT @delimit1 = CASE WHEN @leadZeros='Y' THEN
				CASE (SELECT @navID)
					WHEN 'client' 		THEN IsNull(clientDelimiter,'')
					WHEN 'employee' 	THEN IsNull(employeeDelimiter,'')
					WHEN 'wbs1' 		THEN IsNull(WBS1Delimiter1,'')
					WHEN 'vendor' 		THEN IsNull(vendorDelimiter,'')
				END	ELSE	'' END
				FROM CFGFormat
			SELECT @delimit1Pos = CASE WHEN @leadZeros='Y' THEN
				CASE (SELECT @navID)
					WHEN 'client' 		THEN clientDelimiterPosition
					WHEN 'employee' 	THEN employeeDelimiterPosition
					WHEN 'wbs1' 		THEN WBS1Delimiter1Position
					WHEN 'vendor' 		THEN vendorDelimiterPosition
				END	ELSE	0 END
			FROM CFGFormat
			SELECT @delimit2 = CASE WHEN @leadZeros='Y' THEN
				CASE (SELECT @navID)
					WHEN 'wbs1' 		THEN IsNull(WBS1Delimiter2,'')
				END	ELSE	'' END
			FROM CFGFormat
			SELECT @delimit2Pos = CASE WHEN @leadZeros='Y' THEN
				CASE (SELECT @navID)
					WHEN 'wbs1' 		THEN WBS1Delimiter2Position
				END	ELSE	0 END
			FROM CFGFormat
			SELECT @seqLen = CASE (SELECT @navID)
				WHEN 'client' 		THEN clientAutoNumSeqLen
				WHEN 'employee' 	THEN employeeAutoNumSeqLen
				WHEN 'wbs1' 		THEN 
					CASE (SELECT @chargeType)
						WHEN 'P' THEN wbs1AutoNumSeqLenPromo
						WHEN 'H' THEN wbs1AutoNumSeqLenOH
						ELSE	wbs1AutoNumSeqLen END
				WHEN 'vendor' 		THEN vendorAutoNumSeqLen
				WHEN 'mktCampaign' 	THEN mktCampaignAutoNumSeqLen
				WHEN 'equipment' 	THEN equipmentAutoNumSeqLen
				WHEN 'opp' 		THEN oppAutoNumSeqLen
				WHEN 'rp' 		THEN rpAutoNumSeqLen	END
			FROM CFGFormat
			SELECT @AutoNumPos = CASE (SELECT @navID)
				WHEN 'client' 		THEN clientAutoNumSeqPos
				WHEN 'employee' 	THEN employeeAutoNumSeqPos
				WHEN 'wbs1' 		THEN 
					CASE (SELECT @chargeType)
						WHEN 'P' THEN wbs1AutoNumSeqPosPromo
						WHEN 'H' THEN wbs1AutoNumSeqPosOH
						ELSE	wbs1AutoNumSeqPos END
				WHEN 'vendor' 		THEN vendorAutoNumSeqPos
				WHEN 'mktCampaign' 	THEN mktCampaignAutoNumSeqPos
				WHEN 'equipment' 	THEN equipmentAutoNumSeqPos
				WHEN 'opp' 		THEN oppAutoNumSeqPos
				WHEN 'rp' 		THEN rpAutoNumSeqPos	END
			FROM CFGFormat
			---Doug Baker 11/29/2018 TFS:1040427 - now use clendor table 
			SELECT @mainAppTable = CASE (SELECT @navID)
				WHEN 'client' 		THEN 'Clendor'
				WHEN 'employee' 	THEN 'EM'
				WHEN 'wbs1' 		THEN 'PR'
				WHEN 'vendor' 		THEN 'VE'
				WHEN 'mktCampaign' 	THEN 'MktCampaign'
				WHEN 'equipment' 	THEN 'Equipment'
				WHEN 'opp' 		THEN 'Opportunity'
				WHEN 'rp' 		THEN 'RPPlan'	END
			SELECT @numCol = CASE (SELECT @navID)
				WHEN 'client' 		THEN 'Client'
				WHEN 'employee' 	THEN 'Employee'
				WHEN 'wbs1' 		THEN 'WBS1'
				WHEN 'vendor' 		THEN 'Vendor'
				WHEN 'mktCampaign' 	THEN 'Number'
				WHEN 'equipment' 	THEN 'EquipmentNumber'
				WHEN 'opp' 		THEN 'Opportunity'
				WHEN 'rp' 		THEN 'PlanNumber'	END
		END 
	SELECT @resetPart  = Replace(@resetPart,'''','')
	SELECT @nonSeqPart = Replace(@nonSeqPart,'''','')
	if (@navID <> 'mktCampaign' and @navID <> 'opp' and @navID <> 'equipment')
	BEGIN
		SELECT @resetPart  = Replace(Replace(Replace(Replace(Replace(@resetPart,':',''),'.',''),',',''),'_',''),'''','')
		SELECT @nonSeqPart = Replace(Replace(Replace(Replace(@nonSeqPart,':',''),'.',''),',',''),'_','')
	END
	BEGIN TRANSACTION
    -- get the next number from CFGAutoNumSeqs, and update.
	SELECT @NextAutoNum = LastAutoSeq+1 FROM CFGAutoNumSeqs 
		WHERE NavID=''+@navID+'' AND ChargeType=''+@chargeType+''
			AND ResetPart=N''+@resetPart+''
    set nocount on
    if (@NextAutoNum is not null)
    BEGIN
	   UPDATE CFGAutoNumSeqs 
	   SET LastAutoSeq = @NextAutoNum 
	   WHERE NavID=''+@navID+'' AND ChargeType=''+@chargeType+''
					AND ResetPart=N''+@resetPart+''
	   COMMIT
    END
  
	if (@NextAutoNum is null)
	BEGIN
		IF SUBSTRING(@navid,1,5) = 'UDIC_'
			SELECT @NextAutoNum = AutoNumSeqStart 
			FROM FW_UDIC
			WHERE UDIC_ID = @navID
		ELSE
			SELECT @NextAutoNum = CASE (SELECT @navID)
				WHEN 'client' 		THEN clientAutoNumSeqStart
				WHEN 'employee' 	THEN employeeAutoNumSeqStart
				WHEN 'wbs1' 		THEN 
					CASE (SELECT @chargeType)
						WHEN 'P' THEN wbs1AutoNumSeqStartPromo
						WHEN 'H' THEN wbs1AutoNumSeqStartOH
						ELSE	wbs1AutoNumSeqStart END
				WHEN 'vendor' 		THEN vendorAutoNumSeqStart
				WHEN 'mktCampaign' 	THEN mktCampaignAutoNumSeqStart
				WHEN 'equipment' 	THEN equipmentAutoNumSeqStart
				WHEN 'opp' 		THEN oppAutoNumSeqStart
				WHEN 'rp' 		THEN rpAutoNumSeqStart	END
			FROM CFGFormat
		set nocount on
		INSERT INTO CFGAutoNumSeqs
		(NavID,ChargeType,ResetPart,LastAutoSeq)
		VALUES(''+@navID+'',''+@ChargeType+'',N''+@resetPart+'',@NextAutoNum)
		COMMIT
	END
	
	SET @LoopStartNum = @NextAutoNum
	
	WHILE (@exitLoop = 0)
	BEGIN
		SET @NextAutoNumStr = Convert(varchar(50),@NextAutoNum)
		SET @i = LEN(@NextAutoNumStr)
	
		if (@i > @seqLen)
		BEGIN
			SET @NextAutoNum = 0
			BEGIN TRANSACTION
			-- SET the next number IN CFGAutoNumSeqs TO zero, rollover
    			set nocount on
			UPDATE CFGAutoNumSeqs 
			SET LastAutoSeq = @NextAutoNum 
			WHERE NavID=''+@navID+'' AND ChargeType=''+@chargeType+''
					AND ResetPart=N''+@resetPart+''
			COMMIT
			SET @NextAutoNumStr = Convert(varchar(50),@NextAutoNum)
			SET @i = LEN(@NextAutoNumStr)
		END
		WHILE (@i < @seqLen)
		BEGIN
			SET @NextAutoNumStr = '0' + @NextAutoNumStr
			SET @i = @i+1
		END
		SET @NextAutoNumStr = Substring(@nonSeqPart,1,@AutoNumPos - 1)+@NextAutoNumStr+Substring(@nonSeqPart,@AutoNumPos,Len(@nonSeqPart))

		SELECT @TempStr = CASE WHEN @leadZeros='N' or @delimit1=''	THEN @NextAutoNumStr
		ELSE	CASE (SELECT @navID)
			WHEN 'mktCampaign' 	THEN @NextAutoNumStr
			WHEN 'equipment' 	THEN @NextAutoNumStr
			WHEN 'opp' 		THEN @NextAutoNumStr
			WHEN 'rp' 		THEN @NextAutoNumStr
			WHEN 'wbs1' 		THEN Substring(@NextAutoNumStr,0,@delimit1Pos) +@delimit1 +
				CASE WHEN  @delimit2Pos = 0	THEN SubString(@NextAutoNumStr,@delimit1Pos,@maxLen)
				ELSE	SubString(@NextAutoNumStr,@delimit1Pos,@delimit2Pos-@delimit1Pos-1)+@delimit2
					+SubString(@NextAutoNumStr,@delimit2Pos-1,@maxLen)
				END
			ELSE	Substring(@NextAutoNumStr,0,@delimit1Pos)
				+@delimit1 + SubString(@NextAutoNumStr,@delimit1Pos,@maxLen)
			END
		END

		--Doug Baker 1/8/2020 TFS:1232969 only add leading zeros, when the length of the NextAutoNumStr is less than MaxLen
		SELECT @TempStr = CASE WHEN @leadZeros='Y' AND @delimit1=''	THEN
		                 CASE WHEN @maxlen-len(convert(varchar(100),@NextAutoNumStr)) > 0 THEN replicate('0', @maxlen-len(convert(varchar(100),@NextAutoNumStr))) ELSE '' END +
				       convert(varchar(100), @NextAutoNumStr) ELSE @TempStr end


		SET  @NextAutoNumStr = @TempStr
		
		--Doug Baker 1/8/2020 TFS:1232969 check the nextAutoNumStr, when the length is more than maxlen, create an error and exit
	     if (LEN(@NextAutoNumStr) > @maxLen)
		  BEGIN
			 SET @exitLoop = 1
			 RAISERROR ('The value of the next auto-number is greater than the maximum allowable auto-number field length.',16,3,@NextAutoNumStr, @maxlen)
		  END

		--confirm value is not already uses in app table. someone else could be in middle of a transaction, so get dirty values.
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
		SET @wkQry =  'SELECT CASE WHEN count(0) = 0 THEN ''N'' ELSE ''Y'' END AS Taken
		FROM ' +@mainAppTable +' WHERE '+@numCol+ '='''+@NextAutoNumStr+''''
		set @SqlStmt = 'declare ChkCursor cursor for ' + @wkQry
		execute (@SqlStmt)
		
		if (@@Error <> 0)
			BEGIN
				SET @exitLoop = 1
				RETURN @@Error + '---------' + @nonSeqPart
			END
		open ChkCursor
		fetch ChkCursor into @Taken
		close ChkCursor
		deallocate ChkCursor

		---value not used, so set return and exit
	  	IF (@Taken = 'N')
		BEGIN
		   SET @retAutoNum = @NextAutoNumStr
		   SET @exitLoop = 1
		   BREAK
		END
		--- value used, increment in the table, so others don't get the same value
		IF (@Taken = 'Y')
		  BEGIN
			 --SET @NextAutoNum = @NextAutoNum + 1
			 BEGIN TRANSACTION
		      -- get the next number from CFGAutoNumSeqs, and update.
			 SELECT @NextAutoNum = LastAutoSeq+1 FROM CFGAutoNumSeqs 
			 WHERE NavID=''+@navID+'' AND ChargeType=''+@chargeType+''
				AND ResetPart=N''+@resetPart+''
			 set nocount on
			 UPDATE CFGAutoNumSeqs 
			 SET LastAutoSeq = @NextAutoNum 
			 WHERE NavID=''+@navID+'' AND ChargeType=''+@chargeType+''
					AND ResetPart=N''+@resetPart+''
			COMMIT

			--- Checked all values, back where we started, exit with a blank value, will trigger error message to client
			if (@NextAutoNum = @LoopStartNum)
			BEGIN
				SET @retAutoNum = ''
				SET @exitLoop = 1
				--RAISERROR ('Auto number could not be generated for this record. Please enter the number manually.',
				--16,3,@navID,@navID)
			END
		  END
	END
END -- getAutoNumSeq
GO
