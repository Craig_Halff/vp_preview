SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Ultra_Get_Invoice_Groups]
	@invoiceGroup	Nvarchar(100)
AS BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_EI_Ultra_Get_Invoice_Groups] 'Amy'
	SET NOCOUNT ON;

	SELECT InvoiceGroup, ThruPeriod,  Convert(varchar(10),ThruDate,126) as ThruDate
		FROM CCG_EI_ConfigInvoiceGroups
		WHERE InvoiceGroup = @invoiceGroup
END

GO
