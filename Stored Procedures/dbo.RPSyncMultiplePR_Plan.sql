SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RPSyncMultiplePR_Plan]
AS

BEGIN -- Procedure RPSyncMultiplePR_Plan

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to fix WBS Structure in a Vision Plan when it is out-of-sync with Project Structure. 
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @strPlanID varchar(32)
  DECLARE @strWBS1 nvarchar(30)

  DECLARE @tabPlan TABLE (
    PlanID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default
    UNIQUE(PlanID, WBS1)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find Plans that have differences between PR and PNTask.
  -- Must use a temp table here otherwise the cursor will get confused when Plans are being changed inside a loop. 

  INSERT @tabPlan(
    PlanID,
    WBS1
  )
    SELECT DISTINCT
      X.PlanID,
      X.WBS1
      FROM (
        SELECT DISTINCT 
          PlanID,
          WBS1
          FROM (
            SELECT
              T.PlanID AS PlanID,
              T.WBS1 AS WBS1,
              COUNT(*) OVER (PARTITION BY T.PlanID, T.WBS1, T.WBS2, T.WBS3) AS CountRows  
              FROM RPTask AS T
                INNER JOIN PR ON PR.WBS1 = T.WBS1 AND PR.WBS2 = ISNULL(T.WBS2, ' ') AND PR.WBS3 = ISNULL(T.WBS3, ' ')
                  AND WBSType IN ('WBS1', 'WBS2', 'WBS3')
          ) AS XA
          WHERE CountRows > 1 
      ) AS X

  DECLARE csr20170505_PNPlan CURSOR LOCAL FAST_FORWARD FOR
    SELECT PlanID, WBS1
      FROM @tabPlan AS RP

  OPEN csr20170505_PNPlan
  FETCH NEXT FROM csr20170505_PNPlan INTO @strPlanID, @strWBS1

  WHILE (@@FETCH_STATUS = 0)
    BEGIN

      PRINT '>>> Start Processing PlanID = "' + @strPlanID + '", WBS1 = "' + @strWBS1 + '"'
      EXECUTE dbo.RPSyncWBSStructure @strPlanID
      EXECUTE dbo.RPReCalc @strPlanID
      PRINT '<<< End Processing PlanID = "' + @strPlanID + '", WBS1 = "' + @strWBS1 + '"'

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      FETCH NEXT FROM csr20170505_PNPlan INTO @strPlanID, @strWBS1

    END -- While

  CLOSE csr20170505_PNPlan
  DEALLOCATE csr20170505_PNPlan

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

END -- RPSyncMultiplePR_Plan
GO
