SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[UpdateTeamVendor] @Period int,@PostSeq int, @TransType varchar(2), @Username nvarchar(32)
as
begin
	DECLARE
		@WBS1 as varchar(30),
		@WBS2 as varchar(7),
		@WBS3 as varchar(7),
		@Vendor as varchar(20),
		@Client as varchar(32),
		@YN as varchar(1),
		@ActiveYN as varchar(1),
		@YN_PRClient varchar(1)
		
	DECLARE @tempAddress varchar(20)
	DECLARE @tempCLAddress varchar(20)
			
	DECLARE teamCursor INSENSITIVE CURSOR FOR 
	SELECT
		LedgerAP.WBS1,
		LedgerAP.WBS2,
		LedgerAP.WBS3,
		LedgerAP.Vendor,
		isNull(CL.ClientID,'')
	FROM LedgerAP INNER JOIN PR PR1 on LedgerAP.WBS1 = PR1.WBS1 AND PR1.WBS2 = N' ' And PR1.WBS3 = N' '
	LEFT JOIN CL ON isNull(CL.LinkedVendor,N'') = LedgerAP.Vendor 
	WHERE Period =@Period AND PostSeq = @PostSeq AND LedgerAP.Vendor is not Null
		AND ChargeType <> 'H'
	GROUP BY LedgerAP.WBS1, LedgerAP.WBS2, LedgerAP.WBS3, LedgerAP.Vendor, isNull(CL.ClientID,'')
	BEGIN
		OPEN teamCursor
		FETCH NEXT FROM teamCursor INTO 
		@WBS1,
		@WBS2,
		@WBS3,
		@Vendor,
		@Client
		WHILE @@FETCH_STATUS = 0
		BEGIN
			Select Top 1 @tempAddress = isNull(Address,'<Default>') from VEAddress where PrimaryInd = 'Y' and Vendor = @Vendor
			if @tempAddress is null 
				SET @tempAddress = '<Default>'
			DECLARE teamCursorWBS3 INSENSITIVE CURSOR FOR 
			SELECT 'Y',case when TeamStatus='Active' then 'Y' else 'N' End ActiveStatus From VEProjectAssoc
			Where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3 and Vendor=@Vendor
			BEGIN
				OPEN teamCursorWBS3
				FETCH NEXT FROM teamCursorWBS3 INTO @YN, @ActiveYN
				if @@FETCH_STATUS = 0
					begin
						if @ActiveYN <> 'Y'
							begin
								Update VEProjectAssoc
								SET TeamStatus = 'Active', 
								ModUser = @username,
								ModDate = getdate()
								Where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3 and Vendor=@Vendor and TeamStatus<>'Active'
							end
					end
				else
					begin
						INSERT INTO VEProjectAssoc (
						WBS1,
						WBS2,
						WBS3,
						Vendor,
						Role,
						RoleDescription,
						TeamStatus,
						VEAddress,
                        VendorInd,
						CreateUser,
						ModUser,
						ModDate)
						Select @WBS1, @WBS2, @WBS3, @Vendor, ' ', Null, 'Active', @tempAddress, 'Y', @UserName,
						@Username, getdate()
					end
				Close teamCursorWBS3
				DEALLOCATE teamCursorWBS3
			END
			if isnull(@WBS3,' ') <> ' '
				BEGIN
					DECLARE teamCursorWBS2 INSENSITIVE CURSOR FOR 
					SELECT 'Y', case when TeamStatus='Active' then 'Y' else 'N' End ActiveStatus From VEProjectAssoc
					Where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=' ' and Vendor=@Vendor
					BEGIN
						OPEN teamCursorWBS2
						FETCH NEXT FROM teamCursorWBS2 INTO @YN, @ActiveYN
						if @@FETCH_STATUS = 0
							begin
								if @ActiveYN <> 'Y'
									begin
										Update VEProjectAssoc
										SET TeamStatus = 'Active', 
										ModUser = @Username,
										ModDate = getdate()
										Where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=' ' and Vendor=@Vendor and TeamStatus<>'Active'
									end
							end
						else
							begin
								INSERT INTO VEProjectAssoc (
								WBS1,
								WBS2,
								WBS3,
								Vendor,
								Role,
								RoleDescription,
								TeamStatus,
								VEAddress,
                                VendorInd,
								CreateUser,
								ModUser,
								ModDate)
								Select @WBS1, @WBS2, ' ', @Vendor, ' ', Null, 'Active', @tempAddress, 'Y', @Username,
								@Username, getdate()
							end
						Close teamCursorWBS2
						DEALLOCATE teamCursorWBS2
					END
				END
			if isnull(@WBS2, ' ') <> ' ' 
				BEGIN
					DECLARE teamCursorWBS1 INSENSITIVE CURSOR FOR 
					SELECT 'Y', case when TeamStatus='Active' then 'Y' else 'N' End ActiveStatus From VEProjectAssoc
					Where WBS1=@WBS1 and WBS2=' ' and WBS3=' ' and Vendor=@Vendor
					BEGIN
						OPEN teamCursorWBS1
						FETCH NEXT FROM teamCursorWBS1 INTO @YN, @ActiveYN
						if @@FETCH_STATUS = 0
							begin
								if @ActiveYN <> 'Y'
									begin
										Update VEProjectAssoc
										SET TeamStatus = 'Active', 
										ModUser = @Username,
										ModDate = getdate()
										Where WBS1=@WBS1 and WBS2=' ' and WBS3=' ' and Vendor=@Vendor and TeamStatus<>'Active'
									end
							end
						else
							begin
								INSERT INTO VEProjectAssoc (
								WBS1,
								WBS2,
								WBS3,
								Vendor,
								Role,
								RoleDescription,
								TeamStatus,
								VEAddress,
                                VendorInd,
								CreateUser,
								ModUser,
								ModDate)
								Select @WBS1, ' ', ' ', @Vendor, ' ', Null, 'Active', @tempAddress, 'Y', @Username,
								@Username, getdate()
							end
						Close teamCursorWBS1
						DEALLOCATE teamCursorWBS1
					END
				END
			if @Client <> '' and @WBS1<> '' -- begin update team client
				begin
					Select Top 1 @tempCLAddress = isNull(Address,'<Default>') from CLAddress where PrimaryInd = 'Y' and ClientID = @Client
					if @tempCLAddress is null 
						SET @tempCLAddress = '<Default>'
					DECLARE teamCursorPRClient INSENSITIVE CURSOR FOR 
					SELECT 'Y' From PRClientAssoc, CL 
					Where CL.ClientID = PRClientAssoc.ClientId And (PRClientAssoc.WBS1 = @WBS1) And isNull(CL.LinkedVendor,N'') = @Vendor
					BEGIN
						OPEN teamCursorPRClient
						FETCH NEXT FROM teamCursorPRClient INTO @YN_PRClient
						if @@FETCH_STATUS = 0
							begin
								Update ClendorProjectAssoc
								SET VendorInd = 'Y', 
								ModUser = @username,
								ModDate = getdate()
								From ClendorProjectAssoc, CL 
								Where ClendorProjectAssoc.ClientID = @Client And CL.ClientID = ClendorProjectAssoc.ClientId And (ClendorProjectAssoc.WBS1 = @WBS1) And isNull(CL.LinkedVendor,N'') = @Vendor
								and ClendorProjectAssoc.VendorInd <> 'Y'
							end
						else
							begin
								INSERT INTO PRClientAssoc (
								WBS1,
								ClientID,
								Role,
								VendorInd,
								Address,
								CreateUser,
								ModUser,
								ModDate)
								Select @WBS1, @Client, ' ', 'Y', @tempAddress, @UserName,
								@Username, getdate()
							end
						Close teamCursorPRClient
						DEALLOCATE teamCursorPRClient
					END
				END-- UpdateTeamClient
			FETCH NEXT FROM teamCursor INTO @WBS1,@WBS2,@WBS3,@Vendor,@Client
  		END 
		Close teamCursor
		DEALLOCATE teamCursor
	END
	
end -- UpdateTeamVendor
GO
