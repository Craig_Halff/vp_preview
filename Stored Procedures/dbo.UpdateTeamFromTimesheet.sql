SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[UpdateTeamFromTimesheet] @Period int,@PostSeq int, @Username nvarchar(32)
as
begin
	DECLARE
		@WBS1 as varchar(30),
		@WBS2 as varchar(7),
		@WBS3 as varchar(7),
		@Employee as varchar(20),
		@YN as varchar(1),
		@ActiveYN as varchar(1)
		
	DECLARE teamCursor INSENSITIVE CURSOR FOR 
	SELECT
		WBS1,
		WBS2,
		WBS3,
		LD.Employee
	FROM LD INNER JOIN EM on LD.Employee = EM.Employee
	WHERE Period =@Period AND PostSeq = @PostSeq 
		AND ProjectCost = 'Y'
		AND ChargeType <> 'H'
	GROUP BY WBS1, WBS2, WBS3, LD.Employee
	BEGIN
		OPEN teamCursor
		FETCH NEXT FROM teamCursor INTO 
		@WBS1,
		@WBS2,
		@WBS3,
		@Employee
		WHILE @@FETCH_STATUS = 0
		BEGIN
			DECLARE teamCursorWBS3 INSENSITIVE CURSOR FOR 
			SELECT 'Y', case when TeamStatus='Active' then 'Y' else 'N' End ActiveStatus From EMProjectAssoc
			Where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3 and Employee=@Employee
			BEGIN
				OPEN teamCursorWBS3
				FETCH NEXT FROM teamCursorWBS3 INTO @YN, @ActiveYN
				if @@FETCH_STATUS = 0
					begin
						if @ActiveYN <> 'Y'
							begin
								Update EMProjectAssoc
								SET TeamStatus = 'Active', 
								ModUser = @username,
								ModDate = getdate()
								Where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3 and Employee=@Employee and TeamStatus<>'Active'
							end
					end
				else
					begin
						INSERT INTO EMProjectAssoc (
						RecordID,
						WBS1,
						WBS2,
						WBS3,
						Employee,
						Role,
						RoleDescription,
						TeamStatus,
						CreateUser,
						ModUser,
						ModDate)
						Select replace(convert(varchar(50),newid()),'-',''),
						@WBS1, @WBS2, @WBS3, @Employee, ' ', Null, 'Active', @UserName,
						@Username, getdate()
					end
				Close teamCursorWBS3
				DEALLOCATE teamCursorWBS3
			END
			if isnull(@WBS3,' ') <> ' '
				BEGIN
					DECLARE teamCursorWBS2 INSENSITIVE CURSOR FOR 
					SELECT 'Y',case when TeamStatus='Active' then 'Y' else 'N' End ActiveStatus From EMProjectAssoc
					Where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=' ' and Employee=@Employee
					BEGIN
						OPEN teamCursorWBS2
						FETCH NEXT FROM teamCursorWBS2 INTO @YN, @ActiveYN
						if @@FETCH_STATUS = 0
							begin
								if @ActiveYN <> 'Y'
									begin
										Update EMProjectAssoc
										SET TeamStatus = 'Active', 
										ModUser = @Username,
										ModDate = getdate()
										Where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=' ' and Employee=@Employee and TeamStatus<>'Active'
									end
							end
						else
							begin
								INSERT INTO EMProjectAssoc (
								RecordID,
								WBS1,
								WBS2,
								WBS3,
								Employee,
								Role,
								RoleDescription,
								TeamStatus,
								CreateUser,
								ModUser,
								ModDate)
								Select replace(convert(varchar(50),newid()),'-',''),
								@WBS1, @WBS2, ' ', @Employee, ' ', Null, 'Active', @Username,
								@Username, getdate()
							end
						Close teamCursorWBS2
						DEALLOCATE teamCursorWBS2
					END
				END
			if isnull(@WBS2, ' ') <> ' ' 
				BEGIN
					DECLARE teamCursorWBS1 INSENSITIVE CURSOR FOR 
					SELECT 'Y',case when TeamStatus='Active' then 'Y' else 'N' End ActiveStatus From EMProjectAssoc
					Where WBS1=@WBS1 and WBS2=' ' and WBS3=' ' and Employee=@Employee
					BEGIN
						OPEN teamCursorWBS1
						FETCH NEXT FROM teamCursorWBS1 INTO @YN, @ActiveYN
						if @@FETCH_STATUS = 0
							begin
								if @ActiveYN <> 'Y'
									begin
										Update EMProjectAssoc
										SET TeamStatus = 'Active', 
										ModUser = @Username,
										ModDate = getdate()
										Where WBS1=@WBS1 and WBS2=' ' and WBS3=' ' and Employee=@Employee and TeamStatus<>'Active'
									end
							end
						else
							begin
								INSERT INTO EMProjectAssoc (
								RecordID,
								WBS1,
								WBS2,
								WBS3,
								Employee,
								Role,
								RoleDescription,
								TeamStatus,
								CreateUser,
								ModUser,
								ModDate)
								Select replace(convert(varchar(50),newid()),'-',''),
								@WBS1, ' ', ' ', @Employee, ' ', Null, 'Active', @Username,
								@Username, getdate()
							end
						Close teamCursorWBS1
						DEALLOCATE teamCursorWBS1
					END
				END
			FETCH NEXT FROM teamCursor INTO @WBS1,@WBS2,@WBS3,@Employee
  		END
		Close teamCursor
		DEALLOCATE teamCursor
	END
	
end -- UpdateTeamFromTimesheet
GO
