SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectBillingTermsLimitAbove]
   @WBS1        varchar(32), 
   @WBS2        varchar(7), 
   @WBS3        varchar(7),
   @LimitAtLevelAbove varchar (1)
AS
/*
Copyright (c) 2017 Central Consulting Group. All rights reserved.
04/03/2017 David Springer
           Update billing terms
           Call from Project CHANGE workflow when LimitAtLevelAbove has changed.
05/24/2017 David Springer
           Distinguish phase & task levels
		   Added to spCCG_ProjectBillingTermsUpdate and spCCG_ProjectBillingTermsInsert procedures
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
BEGIN
-- Phase level
   If @WBS2 <> ' ' and @WBS3 = ' '
      Begin
      Update ProjectCustomTabFields
	  Set CustLimitatLevelAbove = @LimitAtLevelAbove
	  Where WBS1 = @WBS1
		and WBS2 <> @WBS2
	    and CustBillingType like 'Cost Plus Max%'

      If @LimitAtLevelAbove = 'Y'
	     Begin
      -- Update project level billing terms if Limit at Level Above is checked
         Update b
         Set b.LimitMeth = '3',
	         b.LimitsBySubLevel = 'N',  -- limit by phase
	         b.LabLimit = f.Labor,
             b.ConLimit = f.Cons,
             b.ExpLimit = f.Exp
         From BT b, 
	     (Select p.WBS1, Sum (p.FeeDirLab + p.FeeDirExp + p.ConsultFee) Labor,
		                 Sum (p.ReimbAllowCons) Cons,
						 Sum (p.ReimbAllowExp) Exp
		  From PR p, ProjectCustomTabFields px
		  Where p.WBS1 = px.WBS1
		    and p.WBS2 = px.WBS2
			and p.WBS3 = px.WBS3
			and px.CustBillingType like 'Cost Plus Max%'
			and px.CustLimitatLevelAbove = 'Y'
		 Group by p.WBS1) f
         Where b.WBS1 = @WBS1
           and b.WBS2 = ' ' -- project level
           and b.WBS1 = f.WBS1

      -- Update phase level billing terms if Limit at Level Above is checked
         Update b
         Set b.LimitMeth = '0',
	         b.LimitsBySubLevel = 'Y',  -- limit by phase
	         b.LabLimit = 0,
             b.ConLimit = 0,
             b.ExpLimit = 0
         From BT b, ProjectCustomTabFields px
		 Where b.WBS1 = @WBS1
           and b.WBS2 <> ' ' -- phase level
           and b.WBS3 = ' '
           and b.WBS1 = px.WBS1
		   and b.WBS2 = px.WBS2
		   and b.WBS3 = px.WBS3
		   and px.CustBillingType like 'Cost Plus Max%'
		   and px.CustLimitatLevelAbove = 'Y'

         End

      If @LimitAtLevelAbove = 'N'
	     Begin
         Update b
         Set b.LimitMeth = '0',
	         b.LimitsBySubLevel = 'Y',
	         b.LabLimit = 0,
             b.ConLimit = 0,
             b.ExpLimit = 0
         From BT b
         Where b.WBS1 = @WBS1
           and b.WBS2 = ' ' -- project level

         Update b
         Set b.LimitMeth = '1',
	         b.LimitsBySubLevel = 'N',
	         b.LabLimit = p.FeeDirLab + p.FeeDirExp + p.ConsultFee,
             b.ConLimit = p.ReimbAllowCons,
             b.ExpLimit = p.ReimbAllowExp
         From BT b, PR p, ProjectCustomTabFields px
         Where b.WBS1 = @WBS1
           and b.WBS2 <> ' '  -- phase level
           and b.WBS3 = ' '
           and b.WBS1 = p.WBS1
           and b.WBS2 = p.WBS2
           and b.WBS3 = p.WBS3
           and b.WBS1 = px.WBS1
		   and b.WBS2 = px.WBS2
		   and b.WBS3 = px.WBS3
		   and px.CustBillingType like 'Cost Plus Max%'
		   and px.CustLimitatLevelAbove = 'N'

        End
      End -- Phase level

-- Task level
   If @WBS3 <> ' '
      Begin
      Update ProjectCustomTabFields
	  Set CustLimitatLevelAbove = @LimitAtLevelAbove
	  Where WBS1 = @WBS1
		and WBS2 = @WBS2
		and WBS3 <> @WBS3
	    and CustBillingType like 'Cost Plus Max%'

      If @LimitAtLevelAbove = 'Y'
	     Begin
      -- Update phase level billing terms if Limit at Level Above is checked
         Update b
         Set b.LimitMeth = '3',
	         b.LimitsBySubLevel = 'N',  -- limit by task
	         b.LabLimit = f.Labor,
             b.ConLimit = f.Cons,
             b.ExpLimit = f.Exp
         From BT b, 
	     (Select p.WBS1, p.WBS2,
		         Sum (p.FeeDirLab + p.FeeDirExp + p.ConsultFee) Labor,
		         Sum (p.ReimbAllowCons) Cons,
				 Sum (p.ReimbAllowExp) Exp
		  From PR p, ProjectCustomTabFields px
		  Where p.WBS1 = px.WBS1
		    and p.WBS2 = px.WBS2
			and p.WBS3 = px.WBS3
			and px.CustBillingType like 'Cost Plus Max%'
			and px.CustLimitatLevelAbove = 'Y'
		 Group by p.WBS1, p.WBS2) f
         Where b.WBS1 = @WBS1
           and b.WBS2 = @WBS2
           and b.WBS3 = ' ' -- phaselevel
           and b.WBS1 = f.WBS1
           and b.WBS2 = f.WBS2

      -- Update task level billing terms if Limit at Level Above is checked
         Update b
         Set b.LimitMeth = '0',
	         b.LimitsBySubLevel = 'Y',  -- limit by phase
	         b.LabLimit = 0,
             b.ConLimit = 0,
             b.ExpLimit = 0
         From BT b, ProjectCustomTabFields px
		 Where b.WBS1 = @WBS1
		   and b.WBS2 = @WBS2
           and b.WBS3 <> ' ' -- task level
           and b.WBS1 = px.WBS1
		   and b.WBS2 = px.WBS2
		   and b.WBS3 = px.WBS3
		   and px.CustBillingType like 'Cost Plus Max%'
		   and px.CustLimitatLevelAbove = 'Y'

         End

      If @LimitAtLevelAbove = 'N'
	     Begin
         Update b
         Set b.LimitMeth = '0',
	         b.LimitsBySubLevel = 'Y',
	         b.LabLimit = 0,
             b.ConLimit = 0,
             b.ExpLimit = 0
         From BT b
         Where b.WBS1 = @WBS1
           and b.WBS2 = @WBS2
           and b.WBS3 = ' ' -- phase level
         End

         Update b
         Set b.LimitMeth = '1',
	         b.LimitsBySubLevel = 'N',
	         b.LabLimit = p.FeeDirLab + p.FeeDirExp + p.ConsultFee,
             b.ConLimit = p.ReimbAllowCons,
             b.ExpLimit = p.ReimbAllowExp
         From BT b, PR p, ProjectCustomTabFields px
         Where b.WBS1 = @WBS1
           and b.WBS2 = @WBS2
           and b.WBS3 <> ' '  -- task level
           and b.WBS1 = p.WBS1
           and b.WBS2 = p.WBS2
           and b.WBS3 = p.WBS3
           and b.WBS1 = px.WBS1
		   and b.WBS2 = px.WBS2
		   and b.WBS3 = px.WBS3
		   and px.CustBillingType like 'Cost Plus Max%'
		   and px.CustLimitatLevelAbove = 'N'

      End -- Task level
END
GO
