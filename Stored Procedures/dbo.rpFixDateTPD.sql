SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpFixDateTPD]
  @strPlanID VARCHAR(32)
AS

BEGIN -- Procedure rpFixDateTPD

  SET NOCOUNT ON

  -- If TPD End Date is less than Start Date then set TPD EndDate 
  -- to Calendar Interval End Date.
  
  UPDATE RPPlannedLabor
    SET EndDate = CI.EndDate
  FROM RPPlannedLabor AS TPD INNER JOIN RPCalendarInterval AS CI  
    ON CI.PlanID = TPD.PlanID AND TPD.StartDate >= CI.StartDate AND TPD.StartDate <= CI.EndDate
  WHERE CI.PlanID = @strPlanID AND TPD.EndDate < TPD.StartDate

  UPDATE RPPlannedExpenses
    SET EndDate = CI.EndDate
  FROM RPPlannedExpenses AS TPD INNER JOIN RPCalendarInterval AS CI  
    ON CI.PlanID = TPD.PlanID AND TPD.StartDate >= CI.StartDate AND TPD.StartDate <= CI.EndDate
  WHERE CI.PlanID = @strPlanID AND TPD.EndDate < TPD.StartDate

  UPDATE RPPlannedConsultant
    SET EndDate = CI.EndDate
  FROM RPPlannedConsultant AS TPD INNER JOIN RPCalendarInterval AS CI  
    ON CI.PlanID = TPD.PlanID AND TPD.StartDate >= CI.StartDate AND TPD.StartDate <= CI.EndDate
  WHERE CI.PlanID = @strPlanID AND TPD.EndDate < TPD.StartDate

  SET NOCOUNT OFF

END -- Procedure rpFixDateTPD
GO
