SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Ultra_Get_Main_Project]
	@wbs1	Nvarchar(32)
AS BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_EI_Ultra_Get_Main_Project] '2002002.00'
	SET NOCOUNT ON;

	SELECT MAINWBS1
		FROM BTBGSubs
		WHERE SubWBS1 = @wbs1 and MainWBS1 <> SubWBS1
END

GO
