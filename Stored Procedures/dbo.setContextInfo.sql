SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
-- Leave NVARCHAR alone since converting binary and need to know position for parsing and nvarchar is double byte
CREATE PROCEDURE [dbo].[setContextInfo]
  @strCompany nvarchar(14) = ' ',
  @strAuditingEnabled varchar(1) = 'N',
  @strUserName nvarchar(32) = '',
  @strCultureName varchar(10) = 'en-US',
  @strAuditingDetail varchar(1) = '',
  @strAuditSource nvarChar(3) =''
AS

BEGIN -- Procedure setContextInfo
  DECLARE @vbinContextInfo varbinary(128), @auditTime datetime
  
 IF (@strAuditingEnabled = 'Y')
	BEGIN
		if (@strAuditingDetail = '')
			set @strAuditingDetail = dbo.getVisionAuditingDetail()
		
		set @auditTime = dbo.GetVisionAuditTime()

		if (@auditTime = '1900-01-01 00:00:00.000')
			BEGIN
                set @auditTime = GetUTCDate()
            END
		EXEC sp_set_session_context @key = N'Company', @value = @strCompany;
		EXEC sp_set_session_context @key = N'Culture', @value = @strCultureName;
		EXEC sp_set_session_context @key = N'Username', @value = @strUserName;
		EXEC sp_set_session_context @key = N'AuditTime', @value = @auditTime;
		EXEC sp_set_session_context @key = N'AuditDetail', @value = @strAuditingDetail;
		EXEC sp_set_session_context @key = N'AuditSource', @value = @strAuditSource;
	END
  ELSE
   BEGIN
		EXEC sp_set_session_context @key = N'Company', @value = @strCompany;
		EXEC sp_set_session_context @key = N'Culture', @value = @strCultureName;
		EXEC sp_set_session_context @key = N'Username', @value = @strUserName;
		EXEC sp_set_session_context @key = N'AuditTime', @value = '1900-01-01 00:00:00.000' ;
		EXEC sp_set_session_context @key = N'AuditDetail', @value = NULL;
		EXEC sp_set_session_context @key = N'AuditSource', @value = NULL;
   END
    
-- Set "old-style" context info data so that the username can be found by DCO
SET @vbinContextInfo =  CONVERT(varbinary(28),  @strCompany + replicate(' ', 14 - len(@strCompany))) + 
			   CONVERT(varbinary(20),  @strCultureName+ replicate(' ', 10- len(@strCultureName) )) +
			   CONVERT(varbinary(64),  @strUserName + replicate(' ', 32- len(@strUserName))) 
SET CONTEXT_INFO @vbinContextInfo 
 
END -- setContextInfo
GO
