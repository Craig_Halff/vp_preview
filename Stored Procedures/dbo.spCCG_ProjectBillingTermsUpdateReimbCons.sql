SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[spCCG_ProjectBillingTermsUpdateReimbCons]
   @WBS1        varchar(32), 
   @WBS2        varchar(7), 
   @WBS3        varchar(7)
AS
/*
Copyright (c) 2020 Central Consulting Group. All rights reserved.
03/10/2020	David Springer
			Update billing terms multiplier when CustReimbConsultantMultiplier has changed
*/
BEGIN
SET NOCOUNT ON
-- Project level
If @WBS2 = ' '
   Begin

      Update b
      Set b.ConTable = t.TableNo,
	      b.ConMult = 1
      From BT b, ProjectCustomTabFields px, BTEA t
      Where px.WBS1 = @WBS1
	    and px.WBS2 = ' ' -- project level Reimb Consultant Multiplier
      	and px.WBS1 = b.WBS1
      	and t.TableName = 'Consultant ' + convert (varchar, convert (decimal (18,4), px.CustReimbConsultantMultiplier))

   -- Push Reimb Consultant Multiplier to lower levels with Billing Type <> Non-Billable
      Update l
      Set l.CustReimbConsultantMultiplier = p.CustReimbConsultantMultiplier
	  From ProjectCustomTabFields p, ProjectCustomTabFields l
	  Where p.WBS1 = @WBS1
	    and p.WBS2 = ' '
		and p.WBS1 = l.WBS1
		and l.WBS2 <> ' '
		and IsNull (l.CustBillingType, 'Non-Billable') <> 'Non-Billable'

   End -- Project level

-- Phase level
If @WBS2 <> ' ' and @WBS3 = ' '
   Begin

   -- Update Phase & Task levels of the billing terms
      Update b
      Set b.ConTable = t.TableNo,
	      b.ConMult = 1
      From BT b, ProjectCustomTabFields px, BTEA t
      Where b.WBS1 = @WBS1
      	and b.WBS2 = @WBS2
      	and b.WBS1 = px.WBS1
	    and b.WBS2 = px.WBS2
      	and b.WBS3 = px.WBS3
	    and IsNull (px.CustBillingType, 'Non-Billable') <> 'Non-Billable'
      	and t.TableName = 'Consultant ' + convert (varchar, convert (decimal (18,4), px.CustReimbConsultantMultiplier))

   -- Push Reimb Consultant Multiplier to lower levels with Billing Type <> Non-Billable
      Update l
      Set l.CustReimbConsultantMultiplier = p.CustReimbConsultantMultiplier
	  From ProjectCustomTabFields p, ProjectCustomTabFields l
	  Where p.WBS1 = @WBS1
	    and p.WBS2 = @WBS2
	    and p.WBS3 = ' '
		and p.WBS1 = l.WBS1
		and l.WBS3 <> ' '
		and IsNull (l.CustBillingType, 'Non-Billable') <> 'Non-Billable'

   End -- Phase level

   -- Task Level
If @WBS3 <> ' '
   Begin

      Update b
      Set b.ConTable = t.TableNo,
	      b.ConMult = 1
      From BT b, ProjectCustomTabFields px, BTEA t
      Where b.WBS1 = @WBS1
      	and b.WBS2 = @WBS2
      	and b.WBS3 = @WBS2
      	and b.WBS1 = px.WBS1
	    and b.WBS2 = px.WBS2
      	and b.WBS3 = px.WBS3
	    and IsNull (px.CustBillingType, 'Non-Billable') <> 'Non-Billable'
      	and t.TableName = 'Consultant ' + convert (varchar, convert (decimal (18,4), px.CustReimbConsultantMultiplier))

   End -- Task level

END
GO
