SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_DelegationGetSuperDelegates] 
             AS EXEC spCCG_PAT_DelegationGetSuperDelegates 
GO
