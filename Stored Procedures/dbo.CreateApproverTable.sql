SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[CreateApproverTable]  
	@WBS1 nvarchar(30) = '',	-- Optional Parameter: Filter by WBS1
	@Employee nvarchar(20) = ''	-- Optional Parameter: Filter by Employee

as
declare
	@sql nvarchar(max),
	@owner varchar(100),
	@tname varchar(100),
	@workTable varchar(100),
	@code varchar(10), 
	@Approve varchar(1),
	@Reject varchar(1),
	@columnName Nvarchar(250)
 
begin
	Begin
		begin tran
			Set @tname = '##tmpApprover_' + REPLACE(NEWID() , '-' , '')
			set @workTable = @tname
			select @sql = 'CREATE TABLE ' + @workTable + ' (WBS1 nvarchar(30) COLLATE database_default, Employee nvarchar(20) COLLATE database_default, Approve nvarchar(1), Reject nvarchar(1), EmployeeName nvarchar(127)  '
			select @sql = @sql + 'CONSTRAINT ' + REPLACE(@workTable, '##', '') + 'PK PRIMARY KEY NONCLUSTERED (WBS1 ASC, Employee ASC) WITH FILLFACTOR = 90)'
			exec (@sql)
		commit
	End
/* populate Approver List from standard Approval Role sysPM, sysPR, sysSP, and sysBill */
	Select @sql = 'INSERT INTO ' + @workTable + ' (WBS1, Employee, Approve, Reject, EmployeeName) ' +
	'SELECT WBS1, Isnull(Delegate.DelegateEmployee, A.Employee) as Employee, Max(Approve) As Approve, Max(Reject) As Reject, Max(EMMain.LastName + '','' + EMMain.FirstName + isnull('' '' + EMMain.MiddleName,'''')) as EmployeeName ' +
	'From ( ' +
	'SELECT PR.WBS1, EMMain.Employee, MAX(CASE WHEN IsNull(AA.Action, '''') = ''A'' THEN ''Y'' ELSE '' '' END) As Approve, MAX(CASE WHEN IsNull(AA.Action, '''') = ''R'' THEN ''Y'' ELSE '' '' END) As Reject ' +
	'FROM PR Inner JOin EMMain on EMMain.Employee = PR.ProjMgr ' + 
	case when @Employee > '' then 'And EMMain.Employee = N'''+@Employee+''' ' else '' End +
	'Left Join FW_CFGSystem on 1=1 ' +
	'Left Join CFGFormat on 1=1 ' +
	'Inner Join CFGBillMainData on CFGBillMainData.Company = Case When FW_CFGSystem.MulticompanyEnabled = ''N'' Then '' '' Else Left(PR.Org, CFGFormat.Org1Length) End ' +
	'Inner Join BT on PR.WBS1=BT.WBS1 and BT.WBS2 = N'' '' and BT.WBS3 = N'' '' and BT.InvoiceApprovalEnabled=''Y'' ' +
	'Inner Join CFGBillApprovalActions AA on IsNull(BT.InvoiceApprovalCode, CFGBillMainData.DefaultInvoiceApprovalCode) =AA.Code ' +
	'Where AA.ActionRole LIKE (''%sysPM%'') and AA.Action in (''A'', ''R'') and PR.WBS2='' '' ' +
	case when @WBS1 > '' then 'And PR.WBS1 = N'''+@WBS1+''' ' else '' End +
	'Group By PR.WBS1, EMMain.Employee ' +
	'Union All ' +
	'SELECT PR.WBS1, EMMain.Employee, MAX(CASE WHEN IsNull(AA.Action, '''') = ''A'' THEN ''Y'' ELSE '' '' END) As Approve, MAX(CASE WHEN IsNull(AA.Action, '''') = ''R'' THEN ''Y'' ELSE '' '' END) As Reject ' +
	'FROM PR Inner JOin EMMain on EMMain.Employee = PR.Principal  ' +
	case when @Employee > '' then 'And EMMain.Employee = N'''+@Employee+''' ' else '' End +
	'Left Join FW_CFGSystem on 1=1 ' +
	'Left Join CFGFormat on 1=1 ' +
	'Inner Join CFGBillMainData on CFGBillMainData.Company = Case When FW_CFGSystem.MulticompanyEnabled = ''N'' Then '' '' Else Left(PR.Org, CFGFormat.Org1Length) End ' +
	'Inner Join BT on PR.WBS1=BT.WBS1 and BT.WBS2 = N'' '' and BT.WBS3 = N'' '' and BT.InvoiceApprovalEnabled=''Y'' ' +
	'Inner Join CFGBillApprovalActions AA on IsNull(BT.InvoiceApprovalCode, CFGBillMainData.DefaultInvoiceApprovalCode) =AA.Code ' +
	'Where AA.ActionRole LIKE (''%sysPR%'') and AA.Action in (''A'', ''R'') and PR.WBS2='' '' ' +
	case when @WBS1 > '' then 'And PR.WBS1 = N'''+@WBS1+''' ' else '' End +
	'Group By PR.WBS1, EMMain.Employee ' +
	'Union All ' +
	'SELECT PR.WBS1, EMMain.Employee, MAX(CASE WHEN IsNull(AA.Action, '''') = ''A'' THEN ''Y'' ELSE '' '' END) As Approve, MAX(CASE WHEN IsNull(AA.Action, '''') = ''R'' THEN ''Y'' ELSE '' '' END) As Reject ' +
	'FROM PR Inner JOin EMMain on EMMain.Employee = PR.Supervisor  ' +
	case when @Employee > '' then 'And EMMain.Employee = N'''+@Employee+''' ' else '' End +
	'Left Join FW_CFGSystem on 1=1 ' +
	'Left Join CFGFormat on 1=1 ' +
	'Inner Join CFGBillMainData on CFGBillMainData.Company = Case When FW_CFGSystem.MulticompanyEnabled = ''N'' Then '' '' Else Left(PR.Org, CFGFormat.Org1Length) End ' +
	'Inner Join BT on PR.WBS1=BT.WBS1 and BT.WBS2 = N'' '' and BT.WBS3 = N'' '' and BT.InvoiceApprovalEnabled=''Y'' ' +
	'Inner Join CFGBillApprovalActions AA on IsNull(BT.InvoiceApprovalCode, CFGBillMainData.DefaultInvoiceApprovalCode) =AA.Code ' +
	'Where AA.ActionRole LIKE (''%sysSP%'') and AA.Action in (''A'', ''R'') and PR.WBS2='' '' ' +
	case when @WBS1 > '' then 'And PR.WBS1 = N'''+@WBS1+''' ' else '' End +
	'Group By PR.WBS1, EMMain.Employee ' +
	'Union All ' +
	'SELECT PR.WBS1, EMMain.Employee, MAX(CASE WHEN IsNull(AA.Action, '''') = ''A'' THEN ''Y'' ELSE '' '' END) As Approve, MAX(CASE WHEN IsNull(AA.Action, '''') = ''R'' THEN ''Y'' ELSE '' '' END) As Reject ' +
	'FROM PR Inner JOin EMMain on EMMain.Employee = PR.Biller  ' +
	case when @Employee > '' then 'And EMMain.Employee = N'''+@Employee+''' ' else '' End +
	'Left Join FW_CFGSystem on 1=1 ' +
	'Left Join CFGFormat on 1=1 ' +
	'Inner Join CFGBillMainData on CFGBillMainData.Company = Case When FW_CFGSystem.MulticompanyEnabled = ''N'' Then '' '' Else Left(PR.Org, CFGFormat.Org1Length) End ' +
	'Inner Join BT on PR.WBS1=BT.WBS1 and BT.WBS2 = N'' '' and BT.WBS3 = N'' '' and BT.InvoiceApprovalEnabled=''Y'' ' +
	'Inner Join CFGBillApprovalActions AA on IsNull(BT.InvoiceApprovalCode, CFGBillMainData.DefaultInvoiceApprovalCode) =AA.Code ' +
	'Where AA.ActionRole LIKE (''%sysBill%'') and AA.Action in (''A'', ''R'') and PR.WBS2='' '' ' +
	case when @WBS1 > '' then 'And PR.WBS1 = N'''+@WBS1+''' ' else '' End +
	'Group By PR.WBS1, EMMain.Employee '

/* populate custom Employee fields Approval Roles */
	Declare CustomEMApprovalRoleCursor Cursor For 
	SELECT FW_CustomColumns.name, CFGBillApprovalActions.Code, MAX(CASE WHEN IsNull(CFGBillApprovalActions.Action, '') = 'A' THEN 'Y' ELSE ' ' END) As Approve, MAX(CASE WHEN IsNull(CFGBillApprovalActions.Action, '') = 'R' THEN 'Y' ELSE ' ' END) As Reject 
	FROM CFGBillApprovalActions, PR INNER JOIN ProjectCustomTabFields On PR.WBS1 = ProjectCustomTabFields.WBS1 And ProjectCustomTabFields.WBS2 = N' ' AND ProjectCustomTabFields.WBS3 = N' ', FW_CustomColumns
	WHERE InfocenterArea = 'Projects' AND CFGBillApprovalActions.ActionRole LIKE ('%' + FW_CustomColumns.name + '%')
	AND FW_CustomColumns.DataType = 'employee' AND CFGBillApprovalActions.Action in ('A', 'R')
	GROUP BY FW_CustomColumns.name, CFGBillApprovalActions.Code
	ORDER BY CFGBillApprovalActions.Code

	Open CustomEMApprovalRoleCursor
	Fetch Next From CustomEMApprovalRoleCursor Into @columnName, @code, @Approve, @Reject
	While (@@FETCH_STATUS = 0)
	BEGIN
/* populate Approver List from each of the custom Employee fields Approval Role */
		select @sql = @sql + 'Union All ' +
		'SELECT PR.WBS1, EMMain.Employee, MAX(CASE WHEN IsNull(AA.Action, '''') = ''A'' THEN ''Y'' ELSE '' '' END) As Approve, MAX(CASE WHEN IsNull(AA.Action, '''') = ''R'' THEN ''Y'' ELSE '' '' END) As Reject ' +
		'FROM PR ' +
		'INNER JOIN ProjectCustomTabFields On PR.WBS1 = ProjectCustomTabFields.WBS1 And ProjectCustomTabFields.WBS2 = N'' '' AND ProjectCustomTabFields.WBS3 = N'' ''' +
		'Inner JOin EMMain on EMMain.Employee = ProjectCustomTabFields.' + @columnName + ' ' +
		case when @Employee > '' then 'And EMMain.Employee = N'''+@Employee+''' ' else '' End +
		'Left Join FW_CFGSystem on 1=1 ' +
		'Left Join CFGFormat on 1=1 ' +
		'Inner Join CFGBillMainData on CFGBillMainData.Company = Case When FW_CFGSystem.MulticompanyEnabled = ''N'' Then '' '' Else Left(PR.Org, CFGFormat.Org1Length) End ' +
		'Inner Join BT on PR.WBS1=BT.WBS1 and BT.WBS2 = N'' '' and BT.WBS3 = N'' '' and BT.InvoiceApprovalEnabled=''Y'' And IsNull(BT.InvoiceApprovalCode, CFGBillMainData.DefaultInvoiceApprovalCode) = ''' + @code + ''' ' +
		'Inner Join CFGBillApprovalActions AA on IsNull(BT.InvoiceApprovalCode, CFGBillMainData.DefaultInvoiceApprovalCode) =AA.Code ' +
		'Where AA.ActionRole LIKE (''%' + @columnName + '%'') and AA.Code = ''' + @code + ''' and AA.Action in (''A'', ''R'') and PR.WBS2='' '' ' +
		case when @WBS1 > '' then 'And PR.WBS1 = N'''+@WBS1+''' ' else '' End +
		'Group By PR.WBS1, EMMain.Employee '

		FETCH NEXT from CustomEMApprovalRoleCursor Into @columnName, @code, @Approve, @Reject
	END
	Close CustomEMApprovalRoleCursor
	Deallocate CustomEMApprovalRoleCursor

/* populate final Approver List from the temp table; Join Delegate Employee */
	select @sql = @sql + ' ) A ' +
	'Left Join (select Min(DelegateEmployee) as DelegateEmployee, Employee from SEUser where DelegateInvoiceApproval=''Y'' and DelegateEmployee IS NOT NULL Group by Employee ' +
	'Union All ' +
	'select Employee, Employee from SEUser where DelegateInvoiceApproval=''Y'' and DelegateEmployee IS NOT NULL Group by Employee ' +
	') Delegate on Delegate.Employee=A.Employee ' + 
	' Left Join EMMain on EMMain.Employee=Isnull(Delegate.DelegateEmployee, A.Employee) ' +
	'GROUP BY WBS1, Isnull(Delegate.DelegateEmployee, A.Employee) '
	Exec (@sql)
	/* ARM run inserts, updates and deletes on InvoiceApprover table */
	Begin
		begin tran
			select @sql = 'INSERT INTO InvoiceApprover (WBS1, Employee, Approve, Reject, EmployeeName) Select TA.WBS1, TA.Employee, TA.Approve, TA.Reject, TA.EmployeeName From ' + @workTable + ' As TA Where Not Exists (SELECT ''X'' FROM InvoiceApprover WHERE InvoiceApprover.WBS1 = TA.WBS1 AND InvoiceApprover.Employee = TA.Employee) '
			execute (@sql)
		commit
	End
		
	Begin
		begin tran
			select @sql = 'DELETE FROM InvoiceApprover Where Not Exists (SELECT ''X'' FROM ' + @workTable + ' As TA WHERE InvoiceApprover.WBS1 = TA.WBS1 AND InvoiceApprover.Employee = TA.Employee) ' +
			case when @WBS1 > '' then 'And InvoiceApprover.WBS1 = N'''+@WBS1+''' ' else '' End +
			case when @Employee > '' then 'And InvoiceApprover.Employee = N'''+@Employee+''' ' else '' End
			Exec (@sql)
		commit
	End
	Begin
		begin tran
			select @sql = 'UPDATE InvoiceApprover SET InvoiceApprover.Approve = TA.Approve, InvoiceApprover.Reject = TA.Reject FROM InvoiceApprover INNER JOIN ' + @workTable + ' TA ON InvoiceApprover.WBS1 = TA.WBS1 AND InvoiceApprover.Employee = TA.Employee '
			Exec (@sql)
		commit
	End
	
END -- [CreateApproverTable]
GO
