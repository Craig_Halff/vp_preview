SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Ultra_Doc_Button_Column]
	@wbs1		Nvarchar(32),
	@User		Nvarchar(32)
AS BEGIN
	SET NOCOUNT ON;
	DECLARE @safeSql	bit;

	-- Ensure that our inputs are safe (prevent SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @wbs1);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @User);
	IF @safeSql = 0 BEGIN
		SELECT 'ERROR: Unsafe parameters used! Execution cancelled.';
		RETURN;
	END;

	EXEC (N'
		SELECT *
			FROM dbo.fnCCG_EI_DocButtonColumn(''' + @wbs1 + N''', ''' + @User + N''')'
	);
END;

GO
