SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ngRPReCalcPlan]
  @strRowID Nvarchar(255),
  @ResetOutlinenumber	integer = 0
AS

BEGIN -- Procedure ngRPReCalcPlan

  SET NOCOUNT ON
  
  DECLARE @strPlanID varchar(32) = ''
  DECLARE @strTaskID varchar(32) = ''
  DECLARE @strInputType varchar(1) = ''
  DECLARE @strMessage varchar(max)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. E~<RPAssignment.ResourceID>|<RPTask.TaskID>
  --   2. G~<RPAssignment.GenericResourceID>|<RPTask.TaskID>
  --   3. P~<RPPlan.PlanID>
  
  IF (CHARINDEX('~', @strRowID) = 0)
    BEGIN
      SET @strInputType = 'T'
    END
  ELSE
    BEGIN
      SET @strInputType = LEFT(@strRowID, CHARINDEX('~', @strRowID) - 1)
    END

  IF (@strInputType IN ('E', 'G', 'T'))
    BEGIN

      SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

      -- Get various Plan parameters.

      SELECT 
        @strPlanID = T.PlanID
        FROM RPTask AS T
          INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID
        WHERE T.TaskID = @strTaskID

    END
  ELSE IF (@strInputType IN ('P'))
    BEGIN

      SET @strPlanID = REPLACE(@strRowID, 'P~', '')

    END /* END ELSE IF (@strInputType IN ('P')) */





/************************************************************************************/
if (@ResetOutlinenumber = 1)	--reset the Outlinenumber
	Begin

--Print '@ResetOutlinenumber'

		DECLARE @CurLevel as integer = 0
		DECLARE @CurRow as integer = 0
		DECLARE @TaskID as varchar(32)
		DECLARE @WBS1 Nvarchar(30)
		DECLARE @WBS2 Nvarchar(7)
		DECLARE @WBS3 Nvarchar(7)
		DECLARE @Name Nvarchar(100)
		DECLARE @ParentOutlineNumber varchar(255)
		DECLARE @OutlineNumber varchar(255)
		DECLARE @OutlineLevel as integer
		DECLARE @ChildrenCount as integer


		DECLARE @tabTask TABLE (
			TaskID varchar(32) COLLATE database_default,
			WBS1 Nvarchar(30) COLLATE database_default,
			WBS2 Nvarchar(7) COLLATE database_default,
			WBS3 Nvarchar(7) COLLATE database_default,
			Name Nvarchar(100) COLLATE database_default,
			ParentOutlineNumber varchar(255) COLLATE database_default,
			OutlineNumber varchar(255) COLLATE database_default,
			OutlineLevel integer,
			ChildrenCount integer,
			NewParentOutlineNumber varchar(255) COLLATE database_default,
			NewOutlineNumber varchar(255) COLLATE database_default,
			UpdatedFlag varchar(1)
			--PRIMARY KEY(WBS1, WBS2, WBS3, OutlineLevel, OutlineNumber, Name)
			)

		Insert @tabTask
		SELECT
			TaskID,
			WBS1,
			WBS2,
			WBS3,
			Name,
			ParentOutlineNumber,
			OutlineNumber,
			OutlineLevel,
			ChildrenCount,
			ParentOutlineNumber as NewParentOutlineNumber,
			OutlineNumber as NewOutlineNumber,
			'N' as UpdatedFlag
			FROM RPTask Where PlanID = @strPlanID
			order by OutlineLevel, WBS1, isnull(WBS2,'<none>'), isnull(WBS3,'<none>'), OutlineNumber, Name


		DECLARE curTask CURSOR FOR
		SELECT
			TaskID,
			WBS1,
			WBS2,
			WBS3,
			Name,
			ParentOutlineNumber,
			OutlineNumber,
			OutlineLevel,
			ChildrenCount
			FROM @tabTask
			order by OutlineLevel, WBS1, isnull(WBS2,'<none>'), isnull(WBS3,'<none>'), OutlineNumber, Name

		Open curTask
		FETCH NEXT FROM curTask INTO 
			@TaskID,
			@WBS1,
			@WBS2,
			@WBS3,
			@Name,
			@ParentOutlineNumber,
			@OutlineNumber,
			@OutlineLevel,
			@ChildrenCount

		DECLARE @OutlineLength as integer = 3
		DECLARE @LastLevel as integer = 0
		DECLARE @LastRow as integer = 0
		DECLARE @TempOutlineNumber as varchar(255) = '001'
		DECLARE @TempParentOutlineNumber as varchar(255) = ''
		DECLARE @UpdatedFlag as varchar(1)

		WHILE (@@FETCH_STATUS = 0)
			BEGIN /* WHILE */
				if (@OutlineLevel > 0)
				  BEGIN
					select @UpdatedFlag = UpdatedFlag from @tabTask Where TaskID = @TaskID
-- Update the lower level tasks
					if (@LastLevel <> @OutlineLevel)
					  BEGIN
						Set @LastLevel = @OutlineLevel
						Set @OutlineLength = 3 + 4*@OutlineLevel
					  END

					if (@TempParentOutlineNumber <> @ParentOutlineNumber)
					  BEGIN
						Set @TempParentOutlineNumber = @ParentOutlineNumber
						Set @LastRow = 1
					  END
					ELSE
					  BEGIN
						Set @LastRow = @LastRow + 1
					  END
			
					Set @TempOutlineNumber = @TempParentOutlineNumber + '.' + RIGHT('000' + CONVERT(VARCHAR(3), @LastRow), 3)

--Print '@TempOutlineNumber = ' + @TempOutlineNumber
--Print '@OutlineNumber = ' + @OutlineNumber

					if (@TempOutlineNumber <> @OutlineNumber and @UpdatedFlag = 'N')
					  BEGIN
						Update @tabTask Set NewOutlineNumber = @TempOutlineNumber, NewParentOutlineNumber = @TempParentOutlineNumber, UpdatedFlag = 'Y' Where TaskID = @TaskID

						Update @tabTask Set NewOutlineNumber = @TempOutlineNumber + SubString(OutlineNumber, @OutlineLength+1, 255)
						, NewParentOutlineNumber = @TempOutlineNumber + SubString(ParentOutlineNumber, @OutlineLength+1, 255), UpdatedFlag = 'Y' 
						WHERE OutlineNumber like @OutlineNumber+'.%' and UpdatedFlag = 'N' 
					  End
				  END
				ELSE
				  BEGIN
-- Update the top level task
					if (@TempOutlineNumber <> @OutlineNumber)
						BEGIN
						Update @tabTask Set NewOutlineNumber = @TempOutlineNumber, NewParentOutlineNumber = null, UpdatedFlag = 'Y' Where TaskID = @TaskID

						Update @tabTask Set NewOutlineNumber = @TempOutlineNumber + SubString(OutlineNumber, @OutlineLength+1, 255)
						, NewParentOutlineNumber = @TempOutlineNumber + SubString(ParentOutlineNumber, @OutlineLength+1, 255), UpdatedFlag = 'Y' 
						WHERE OutlineNumber like @OutlineNumber+'.%'
						End
				  END

				FETCH NEXT FROM curTask INTO 
					@TaskID,
					@WBS1,
					@WBS2,
					@WBS3,
					@Name,
					@ParentOutlineNumber,
					@OutlineNumber,
					@OutlineLevel,
					@ChildrenCount

			END /* WHILE */

		CLOSE curTask
		DEALLOCATE curTask

--Print 'Update OutlineNumber'
		UPDATE T
			SET
			T.OutlineNumber = XT.NewOutlineNumber,
			T.ParentOutlineNumber = XT.NewParentOutlineNumber
			FROM RPTask AS T
			INNER JOIN @tabTask AS XT
			ON T.TaskID = XT.TaskID
			Where T.OutlineNumber <> XT.NewOutlineNumber or 
			T.ParentOutlineNumber <> XT.NewParentOutlineNumber
	End


-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION

    -- Extend StartDate and EndDate of RPAssignment rows when Start/End Dates of TPD rows were extended.

    UPDATE A SET 
      StartDate = CASE WHEN X.StartDate < A.StartDate THEN X.StartDate ELSE A.StartDate END, 
      EndDate = CASE WHEN X.EndDate > A.EndDate THEN X.EndDate ELSE A.EndDate END
      FROM RPAssignment AS A 
        INNER JOIN (
          SELECT PlanID, TaskID, AssignmentID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
            FROM RPPlannedLabor
            WHERE PlanID = @strPlanID AND AssignmentID IS NOT NULL
            GROUP BY PlanID, TaskID, AssignmentID
        ) AS X 
        ON A.PlanID = X.PlanID AND A.TaskID = X.TaskID AND A.AssignmentID = X.AssignmentID
        WHERE A.PlanID = @strPlanID AND 
          (X.StartDate < A.StartDate OR X.EndDate > A.EndDate)

    -- Extend StartDate and EndDate of RPTask rows when Start/End Dates of RPAssignment rows were extended.

    UPDATE T SET 
      StartDate = CASE WHEN X.StartDate < T.StartDate THEN X.StartDate ELSE T.StartDate END, 
      EndDate = CASE WHEN X.EndDate > T.EndDate THEN X.EndDate ELSE T.EndDate END
      FROM RPTask AS T 
        INNER JOIN (
          SELECT PlanID, TaskID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
            FROM RPAssignment
            WHERE PlanID = @strPlanID
            GROUP BY PlanID, TaskID
        ) AS X 
        ON T.PlanID = X.PlanID AND T.TaskID = X.TaskID
        WHERE T.PlanID = @strPlanID AND 
          (X.StartDate < T.StartDate OR X.EndDate > T.EndDate)

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    DECLARE csr20151113_RPTask CURSOR LOCAL FAST_FORWARD FOR
      SELECT PlanID, TaskID FROM RPTask WHERE PlanID = @strPlanID ORDER BY PlanID, OutlineNumber DESC

    OPEN csr20151113_RPTask
    FETCH NEXT FROM csr20151113_RPTask INTO @strPlanID, @strTaskID

    WHILE (@@FETCH_STATUS = 0)
      BEGIN

        UPDATE T SET
          StartDate = CASE WHEN X.StartDate < T.StartDate THEN X.StartDate ELSE T.StartDate END, 
          EndDate = CASE WHEN X.EndDate > T.EndDate THEN X.EndDate ELSE T.EndDate END
          FROM RPTask AS T
            INNER JOIN (
              SELECT 
                PlanID, 
                ParentOutlineNumber, 
                MIN(StartDate) AS StartDate, 
                MAX(EndDate) AS EndDate 
                FROM RPTask 
                WHERE RPTask.PlanID = @strPlanID 
                GROUP BY PlanID, ParentOutlineNumber
            ) AS X ON T.OutlineNumber = X.ParentOutlineNumber
          WHERE T.PlanID = @strPlanID AND T.TaskID = @strTaskID AND X.ParentOutlineNumber IS NOT NULL	AND
            (X.StartDate < T.StartDate OR X.EndDate > T.EndDate)

        FETCH NEXT FROM csr20151113_RPTask INTO @strPlanID, @strTaskID

      END -- While

    CLOSE csr20151113_RPTask
    DEALLOCATE csr20151113_RPTask

	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	--Update ChildrenCount for the whole plan
	UPDATE RPTask SET ChildrenCount = X.ChildrenCount
	  FROM RPTask AS T INNER JOIN
		(SELECT COUNT(C.TaskID) AS ChildrenCount, P.TaskID, P.PlanID
		   FROM RPTask AS P LEFT JOIN RPTask AS C
		   ON P.PlanID = C.PlanID AND P.OutlineNumber = C.ParentOutlineNumber
		   Where P.PlanID=@strPlanID
		   GROUP BY P.TaskID, P.PlanID) AS X
		ON T.PlanID = X.PlanID AND T.TaskID = X.TaskID
	  Where T.PlanID=@strPlanID
	-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- For now, do not perform any calculations on Labor data such as caculating Amounts, Revenues...
    -- There will be no summing up of TPD to higher ancestor rows in the world of ngRP.

    /* EXECUTE RPCalcTPD @strPlanID, 'Y', 'N', 'N', 'N', 'N', 'N', 'N' */

  COMMIT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     
  SET NOCOUNT OFF

END -- ngRPReCalcPlan
GO
