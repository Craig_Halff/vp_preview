SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_InvoiceGroup] ( @WBS1 varchar(32))
             AS EXEC spCCG_EI_InvoiceGroup @WBS1
GO
