SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   Procedure [dbo].[spCCG_ProjectContractMgmtInsert] @WBS1 varchar (32)
AS
/*
Copyright 2017 (c) Central Consulting Group, Inc.  All rights reserved.
03/10/2017	David Springer
			Create Contract grids.
			Call this from spCCG_OpportunityPhasesCreateProjectWBS procedure.
09/07/2021	David Springer
			Revised for Vantagepoint WBS2 level Contract Details; Added spCCG_ProjectTotalCompensation
*/
BEGIN
SET NOCOUNT ON

If exists (Select 'x' From ProjectCustomTabFields Where WBS1 = @WBS1 and WBS2 = ' ' and CustMasterContractType is null)
	BEGIN
--	Insert Contracts for Project level
    Insert Into Contracts
    (WBS1, ContractNumber, ContractDescription, ContractType, ContractStatus, FeeIncludeInd, 
    RequestDate, ApprovedDate, Period, CreateUser, CreateDate)
    Values (@WBS1, '000', '000', '01', '04', 'Y', 
            GetDate(), GetDate(), convert (varchar (6), getDate(), 112), 'CCG Procedure', GetDate())

--	Insert Contract Details for Project level
    Insert Into ContractDetails
    (ContractNumber, WBS1, WBS2, WBS3, 
    FeeDirLab, FeeDirExp, Fee, ConsultFee, ReimbAllowCons, ReimbAllowExp, ReimbAllow, Total,
    FeeDirLabBillingCurrency, FeeDirExpBillingCurrency, FeeBillingCurrency, ConsultFeeBillingCurrency, ReimbAllowConsBillingCurrency, ReimbAllowExpBillingCurrency, ReimbAllowBillingCurrency, TotalBillingCurrency,
    FeeDirLabFunctionalCurrency, FeeDirExpFunctionalCurrency, FeeFunctionalCurrency, ConsultFeeFunctionalCurrency, ReimbAllowConsFunctionalCurrency, ReimbAllowExpFunctionalCurrency, ReimbAllowFunctionalCurrency, TotalFunctionalCurrency,
    CreateUser, CreateDate)
    Select '000', WBS1, WBS2, WBS3, 
    FeeDirLab, FeeDirExp, Fee, ConsultFee, ReimbAllowCons, ReimbAllowExp, ReimbAllow, FeeDirLab + FeeDirExp + ConsultFee + ReimbAllowCons + ReimbAllowExp,
    FeeDirLab, FeeDirExp, Fee, ConsultFee, ReimbAllowCons, ReimbAllowExp, ReimbAllow, FeeDirLab + FeeDirExp + ConsultFee + ReimbAllowCons + ReimbAllowExp,
    FeeDirLab, FeeDirExp, Fee, ConsultFee, ReimbAllowCons, ReimbAllowExp, ReimbAllow, FeeDirLab + FeeDirExp + ConsultFee + ReimbAllowCons + ReimbAllowExp,
    'CCG Procedure', GETDATE()
    From PR
	Where WBS1 = @WBS1
	and WBS2 = ' ' -- project level
	and FeeDirLab + FeeDirExp + ConsultFee + ReimbAllowCons + ReimbAllowExp <> 0

--	Insert Contract Details for Phase  level
    Insert Into ContractDetails
    (ContractNumber, WBS1, WBS2, WBS3, 
    FeeDirLab, FeeDirExp, Fee, ConsultFee, ReimbAllowCons, ReimbAllowExp, ReimbAllow, Total,
    FeeDirLabBillingCurrency, FeeDirExpBillingCurrency, FeeBillingCurrency, ConsultFeeBillingCurrency, ReimbAllowConsBillingCurrency, ReimbAllowExpBillingCurrency, ReimbAllowBillingCurrency, TotalBillingCurrency,
    FeeDirLabFunctionalCurrency, FeeDirExpFunctionalCurrency, FeeFunctionalCurrency, ConsultFeeFunctionalCurrency, ReimbAllowConsFunctionalCurrency, ReimbAllowExpFunctionalCurrency, ReimbAllowFunctionalCurrency, TotalFunctionalCurrency,
    CreateUser, CreateDate)
    Select '000', WBS1, WBS2, WBS3, 
    FeeDirLab, FeeDirExp, Fee, ConsultFee, ReimbAllowCons, ReimbAllowExp, ReimbAllow, FeeDirLab + FeeDirExp + ConsultFee + ReimbAllowCons + ReimbAllowExp,
    FeeDirLab, FeeDirExp, Fee, ConsultFee, ReimbAllowCons, ReimbAllowExp, ReimbAllow, FeeDirLab + FeeDirExp + ConsultFee + ReimbAllowCons + ReimbAllowExp,
    FeeDirLab, FeeDirExp, Fee, ConsultFee, ReimbAllowCons, ReimbAllowExp, ReimbAllow, FeeDirLab + FeeDirExp + ConsultFee + ReimbAllowCons + ReimbAllowExp,
    'CCG Procedure', GETDATE()
    From PR
	Where WBS1 = @WBS1
	and WBS2 <> ' ' -- phase level
	and WBS3 = ' '
			and FeeDirLab + FeeDirExp + ConsultFee + ReimbAllowCons + ReimbAllowExp <> 0

--	Insert Contract Details for Task level
    Insert Into ContractDetails
    (ContractNumber, WBS1, WBS2, WBS3, 
    FeeDirLab, FeeDirExp, Fee, ConsultFee, ReimbAllowCons, ReimbAllowExp, ReimbAllow, Total,
    FeeDirLabBillingCurrency, FeeDirExpBillingCurrency, FeeBillingCurrency, ConsultFeeBillingCurrency, ReimbAllowConsBillingCurrency, ReimbAllowExpBillingCurrency, ReimbAllowBillingCurrency, TotalBillingCurrency,
    FeeDirLabFunctionalCurrency, FeeDirExpFunctionalCurrency, FeeFunctionalCurrency, ConsultFeeFunctionalCurrency, ReimbAllowConsFunctionalCurrency, ReimbAllowExpFunctionalCurrency, ReimbAllowFunctionalCurrency, TotalFunctionalCurrency,
    CreateUser, CreateDate)
    Select '000', WBS1, WBS2, WBS3, 
    FeeDirLab, FeeDirExp, Fee, ConsultFee, ReimbAllowCons, ReimbAllowExp, ReimbAllow, FeeDirLab + FeeDirExp + ConsultFee + ReimbAllowCons + ReimbAllowExp,
    FeeDirLab, FeeDirExp, Fee, ConsultFee, ReimbAllowCons, ReimbAllowExp, ReimbAllow, FeeDirLab + FeeDirExp + ConsultFee + ReimbAllowCons + ReimbAllowExp,
    FeeDirLab, FeeDirExp, Fee, ConsultFee, ReimbAllowCons, ReimbAllowExp, ReimbAllow, FeeDirLab + FeeDirExp + ConsultFee + ReimbAllowCons + ReimbAllowExp,
    'CCG Procedure', GETDATE()
    From PR
	Where WBS1 = @WBS1
	and WBS3 <> ' ' -- task level
			and FeeDirLab + FeeDirExp + ConsultFee + ReimbAllowCons + ReimbAllowExp <> 0

--	Set Billing & Functional Currency at all levels
    Update dbo.ContractDetails
    Set FeeDirLabBillingCurrency = FeeDirLab
        , FeeDirLabFunctionalCurrency = FeeDirLab
        , FeeDirExpBillingCurrency = FeeDirExp
        , FeeDirExpFunctionalCurrency = FeeDirExp
        , FeeBillingCurrency = Fee
        , FeeFunctionalCurrency = Fee
        , ConsultFeeBillingCurrency = ConsultFee
        , ConsultFeeFunctionalCurrency = ConsultFee
        , ReimbAllowConsBillingCurrency = ReimbAllowCons
        , ReimbAllowConsFunctionalCurrency = ReimbAllowCons
        , ReimbAllowExpBillingCurrency = ReimbAllowExp
        , ReimbAllowExpFunctionalCurrency = ReimbAllowExp
        , ReimbAllowBillingCurrency = ReimbAllow
        , ReimbAllowFunctionalCurrency = ReimbAllow
		, TotalBillingCurrency = Total
		, TotalFunctionalCurrency = Total
    Where WBS1 = @WBS1;

	exec spCCG_ProjectTotalCompensation @WBS1

	END

	Else -- This is an IDIQ, so populate Original Fee fields

	Update px
	Set 
		px.CustOriginalFeeDirLab = p.FeeDirLab,
		px.CustOriginalFeeDirExp = p.FeeDirExp,
		px.CustOriginalConsultFee = p.ConsultFee,
		px.CustOriginalReimbAllowExp = p.ReimbAllowExp,
		px.CustOriginalReimbAllowCons = p.ReimbAllowCons,
		px.CustOriginalFeeTotal = p.FeeDirLab + p.FeeDirExp + p.ConsultFee + p.ReimbAllowExp + p.ReimbAllowCons
	From PR p, ProjectCustomTabFields px
	Where p.WBS1 = @WBS1
	  and p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.WBS2 = ' '
END
GO
