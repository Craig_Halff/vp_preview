SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_CheckIfEmployeeIsSupervisor] ( @Employee nvarchar(20))
             AS EXEC spCCG_PAT_CheckIfEmployeeIsSupervisor @Employee
GO
