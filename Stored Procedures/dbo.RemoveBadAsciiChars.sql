SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RemoveBadAsciiChars]
    @tableName Nvarchar(40)
AS
BEGIN
DECLARE @tabname Nvarchar(100)
DECLARE @colname Nvarchar(100)
DECLARE @asciivalue smallint

DECLARE varchars INSENSITIVE CURSOR FOR
   SELECT sys.objects.name AS tabname,sys.columns.name AS colname 
    FROM sys.objects INNER JOIN sys.columns 
      ON sys.objects.object_id=sys.columns.object_id INNER JOIN sys.types
      ON sys.types.system_type_id=sys.columns.system_type_id
   WHERE sys.objects.type='U' 
     AND (sys.types.name='varchar' or sys.types.name = 'nvarchar')
     AND sys.objects.name LIKE @tableName
   
BEGIN
  OPEN varchars
  FETCH NEXT FROM varchars INTO @tabname, @colname   
  WHILE @@FETCH_STATUS = 0
  BEGIN
    PRINT 'Correcting carriage returns and line feeds in table ' + @tabname + ', column ' + @colname + '...'
    EXECUTE ('UPDATE ' + @tabname + 
             ' SET ' + @colname + ' = REPLACE(' + @colname + ',char(13) + char(10),char(10))' +
	     ' WHERE CHARINDEX(char(13) + char(10),' + @colname + ') > 0')
    EXECUTE ('UPDATE ' + @tabname + 
	     ' SET ' + @colname + ' = REPLACE(' + @colname + ',char(13),char(10))' +
	     ' WHERE CHARINDEX(char(13),' + @colname + ') > 0')
    SET @asciivalue = 0
    WHILE @asciivalue <= 31
    BEGIN
	PRINT 'Removing ASCII character ' + CONVERT(Nvarchar(2),@asciivalue) + ' from table ' + @tabname + ', column ' + @colname + '...'
        EXECUTE ('UPDATE ' + @tabname + 
	         ' SET ' + @colname + ' = REPLACE(' + @colname + ',CHAR('+ @asciivalue + '),'''')' +
	         ' WHERE CHARINDEX(CHAR('+ @asciivalue + '),' + @colname + ') > 0')
	SET @asciivalue = @asciivalue + 1
        IF @asciivalue = 10 SET @asciivalue = @asciivalue + 1
        IF @asciivalue = 13 SET @asciivalue = @asciivalue + 1
    END
    FETCH NEXT FROM varchars INTO @tabname, @colname
  END   
  CLOSE varchars
  DEALLOCATE varchars
END

DECLARE texts INSENSITIVE CURSOR FOR
  SELECT sys.objects.name AS tabname,sys.columns.name AS colname 
    FROM sys.objects INNER JOIN sys.columns 
      ON sys.objects.object_id=sys.columns.object_id INNER JOIN sys.types
      ON sys.types.system_type_id=sys.columns.system_type_id
   WHERE sys.objects.type='U' 
     AND sys.types.name='text' 
     AND sys.objects.name LIKE @tableName
   
BEGIN
  OPEN texts
  FETCH NEXT FROM texts INTO @tabname, @colname   
  WHILE @@FETCH_STATUS = 0
  BEGIN
    PRINT 'Removing vertical tabs from table ' + @tabname + ', column ' + @colname + '...'
    EXECUTE ('UPDATE ' + @tabname + 
             ' SET ' + @colname + ' = REPLACE(CAST(' + @colname + ' AS Nvarchar(max)),char(11),'''')' +
             ' WHERE CHARINDEX(char(11),' + @colname + ') > 0')
    FETCH NEXT FROM texts INTO @tabname, @colname
  END   
  CLOSE texts
  DEALLOCATE texts
END

END
GO
