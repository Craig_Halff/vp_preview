SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- EDIT 2 LINES BELOW BASED ON WHICH UTILIZATION
CREATE Procedure [dbo].[~HAI_spCCG_UtilizationNotification_Director]
--**COMMENTED @Email Out
@Email VARCHAR(100),
@RetVal NVARCHAR(MAX) OUTPUT

--COMMENTED TWO CODE LINES OUT BELOW
--CREATE Procedure [dbo].[HAI_spCCG_UtilizationNotification_Director_TEST]
AS
/*
- Created on 5/17/2018 by Conrad Harrison and Jackson Gray
*/

SET NOCOUNT ON
DECLARE 
		--**UNCOMMENTED @Email
		--@Email								varchar (100),
	    @TeamLeader							varchar (100),
		--ADDED LINE BELOW
		@Label								varchar (100),
		@Org								varchar (100),
		@PreviousOrg						varchar (100),
		@CurrentOrg							varchar (100),
		@RowCount							integer,
		@Director							varchar (100),
		@OpsMngr							varchar (100),
		@PracticeLeader						varchar (100),
		@MgmtLeader							varchar (100),
		@Office								varchar (100),
		@Practice							varchar (100),
		@LastName							varchar (50),
		@FirstName							varchar (50),
		@PreferredName						varchar (50),
		@Employee							varchar (100),
		@CustTargetRatio					decimal (19,1),
		@Cust1WeekTargetRatio				decimal (19,1),
		@Cust4WeekTargetRatio				decimal (19,1),
		@CustAnnualTargetRatio				decimal (19,1),
		@Cust1WeekTargetRatio_SubTotal    	decimal (19,1),
		@Cust4WeekTargetRatio_SubTotal      decimal (19,1),
		@CustAnnualTargetRatio_SubTotal     decimal (19,1),
		@CustTargetRatio_FinalTotal      	decimal (19,1),
		@Cust1WeekTargetRatio_FinalTotal    decimal (19,1),
		@Cust4WeekTargetRatio_FinalTotal    decimal (19,1),
		@CustAnnualTargetRatio_FinalTotal   decimal (19,1),
		@Cust1WeekUtilization				decimal (19,1),
		@Cust1WeekUtilization_SubTotal		decimal (19,1),
		@Cust1WeekUtilization_FinalTotal	decimal (19,1),
		@Diff1W								decimal (19,1),
		@Diff1W_SubTotal					decimal (19,1),
		@Diff1W_FinalTotal					decimal (19,1),
		@Cust4WeekUtilization				decimal (19,1),
		@Cust4WeekUtilization_SubTotal		decimal (19,1),
		@Cust4WeekUtilization_FinalTotal	decimal (19,1),
		@Diff4W								decimal (19,1),
		@Diff4W_SubTotal					decimal (19,1),
		@Diff4W_FinalTotal					decimal (19,1),
		@CustAnnualUtilization				decimal (19,1),
		@CustAnnualUtilization_SubTotal		decimal (19,1),
		@CustAnnualUtilization_FinalTotal	decimal (19,1),
		@DiffAnnual							decimal (19,1),
		@DiffAnnual_SubTotal				decimal (19,1),
		@DiffAnnual_FinalTotal				decimal (19,1),
		@PercentSymbol						varchar (5),
		@Body								varchar (max),
		@CSS            					varchar (max),
        @Loop1								integer,
		--ADDED TEXT STYLING VARIABLES BELOW
		@LW									varchar (100),
		@LWF								varchar (100),
		@L4W								varchar (100),
		@L4WF								varchar (100),
		@L12Wr								varchar (100),
		@L12WrF								varchar (100),
		@redText							varchar(50),
		@redTextLW							varchar(50),
		@redTextLWS							varchar(50),
		@redTextLWF							varchar(50),  
		@redText4W							varchar(50),
		@redText4WS							varchar(50),    
		@redText4WF							varchar(50),  
		@redText12Wr						varchar(50),
		@redText12WrS						varchar(50),  
		@redText12WrF						varchar(50)    

		SET @RowCount = 0
		SET @Cust1WeekTargetRatio_SubTotal = 0
		SET @Cust4WeekTargetRatio_SubTotal = 0
		SET @CustAnnualTargetRatio_SubTotal = 0
		SET @Cust1WeekUtilization_SubTotal = 0
		SET @Diff1W_SubTotal = 0
		SET @Cust4WeekUtilization_SubTotal = 0
		SET @Diff4W_SubTotal = 0
		SET @CustAnnualUtilization_SubTotal = 0
		SET @DiffAnnual_SubTotal = 0
				
		SET @Cust1WeekTargetRatio_FinalTotal = 0
		SET @Cust4WeekTargetRatio_FinalTotal = 0
		SET @CustAnnualTargetRatio_FinalTotal = 0
		SET @Cust1WeekUtilization_FinalTotal = 0
		SET @Diff1W_FinalTotal = 0
		SET @Cust4WeekUtilization_FinalTotal = 0
		SET @Diff4W_FinalTotal = 0
		SET @CustAnnualUtilization_FinalTotal = 0
		SET @DiffAnnual_FinalTotal = 0
		Set @PercentSymbol = '%'
		SET @PreviousOrg = ''
		SET @CurrentOrg = ''

		--**ADDED EMAIL INPUT
		--SET @Email = 'ddowns@halff.com'

		
BEGIN

 --CSS style rules
--**COMMENTED STYLING OUT FOR COMBINED PROCEDURE
--	SET @CSS = 
--'
--<style type="text/css">
--	--p {padding-top:15px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;}
--	th {border: 2px solid black;padding: 5px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;font-size:10.5pt;}
--	th {background-color: #808080;color: white;text-align:center;}
--	.team {text-align:left; border-left: 2px solid Black; background-color: white; border-bottom: 1px solid #6E6E6E;}
--	.teamB {font-weight: bold; text-align:left; border-left: 2px solid Black; background-color: white; border-bottom: 1px solid #6E6E6E;}
--	.LW {color: black; background-color: white; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
--	.LWb {font-weight: bold; color: black; background-color: white; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
--	.L4W {color: black; background-color: #e6e6e6; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;} 
--	.L4Wb {font-weight: bold; color: black; background-color: #e6e6e6; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;} 
--	.L12W {background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
--	.L12Wb {font-weight: bold; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
--	.L12Wr {color: black; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-right: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
--	.L12Wrb {font-weight: bold; color: black; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-right: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
--	.LWF {color: black; font-weight: bold; background-color: white; text-align:right; border-left: 2px solid Black; border-top: 2px solid Black;}
--	.LWFLeft {font-weight: bold; background-color: white; text-align:left; border-left: 2px solid Black; border-top: 2px solid Black;}
--	.L4WF {color: black; font-weight: bold; background-color: #e6e6e6; text-align:right; border-left: 2px solid Black; border-top: 2px solid Black;} 
--	.L12WF {font-weight: bold; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-top: 2px solid Black;}
--	.L12WrF {color: black; font-weight: bold; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-right: 2px solid Black; border-top: 2px solid Black;}
--	.redText {color:#FF0000;}

--	</style>
--'

DECLARE curUtilization insensitive cursor for

-- Director Utilization
SELECT  'Team ' + Org + ' ' + B.Label AS Label, A.*
FROM
(
-- Director Utilization
Select oe.Email, 
	oe.LastName + ', ' + IsNull (oe.PreferredName, oe.FirstName) Director, 
	RIGHT(e.Org, 2) AS Org,
	e.LastName + ', ' + IsNull (e.PreferredName, e.FirstName) Employee, 
	u.Cust1WeekUtilization, 
	u.CustTargetRatio Cust1WeekTargetRatio,
	CASE When u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit = 0 Then 0 
		Else u.Cust1WeekUtilization - u.CustTargetRatio 
		END Diff1W, 
	u.Cust4WeekUtilization,
	u.CustTargetRatio Cust4WeekTargetRatio, 
	CASE When u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit = 0 Then 0 
		Else u.Cust4WeekUtilization - u.CustTargetRatio 
		END Diff4W, 
	u.CustAnnualUtilization,
	u.CustTargetRatio CustAnnualTargetRatio, 
	CASE When u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit = 0 Then 0 
		Else u.CustAnnualUtilization - u.CustTargetRatio 
		END DiffAnnual
From UDIC_Utilization u,
	EM e, 
	EmployeeCustomTabFields  ex
	Left Join EM oe on oe.Employee = ex.CustEKDirector
Where u.CustEmployeeNumber = e.Employee
  and e.Employee = ex.Employee
  and oe.Email = @Email
  ) AS A
  INNER JOIN
  (
	SELECT Code, Label 
	FROM CFGOrgCodes 
	WHERE OrgLevel = 3
  ) AS B
  ON B.Code = A.Org
Order by Org

Set @Body = 
'<table cellpadding=''5'' cellspacing=''0'' style=''border:1px solid black;border-collapse:collapse;''>
	<thead>
	    <tr>
               	<th rowspan="2">Name</th>
                <th colspan="3">Last Week</a></th>
                <th colspan="3">Last 4 Weeks</a></th>
                <th colspan="3">Last 12 Months</a></th>
            </tr>
            <tr>
                <th>Actual</th>
                <th>Goal</th>
                <th>Variance</th>
                <th>Actual</th>
                <th>Goal</th>
                <th>Variance</th>
                <th>Actual</th>
                <th>Goal</th>
                <th>Variance</th>
            </tr>
	</thead>
	<tbody>
'	
  
    -- Get each Utilization
	OPEN curUtilization
	FETCH NEXT FROM curUtilization INTO 
	@Label
	, @Email
	, @Director
	, @Org
	, @Employee
	, @Cust1WeekUtilization 
	, @Cust1WeekTargetRatio
	, @Diff1W
	, @Cust4WeekUtilization
	, @Cust4WeekTargetRatio
	, @Diff4W
	, @CustAnnualUtilization
	, @CustAnnualTargetRatio
	, @DiffAnnual

	Set @Loop1 = @@FETCH_STATUS
	While @Loop1 = 0
						
	BEGIN
	
				SET @RowCount = @RowCount + 1
								
				Set @PreviousOrg = (
				 	Case
						When @Org = @CurrentOrg Then @PreviousOrg
						When @Org != @CurrentOrg Then @CurrentOrg
					End)
					
				Select @redTextLW = (Case
								When @Cust1WeekUtilization < @Cust1WeekTargetRatio Then ' style="color:#FF0000;"'
								When @Cust1WeekUtilization >= @Cust1WeekTargetRatio Then '' End)

				Select @redText4W = (Case
								When @Cust4WeekUtilization < @Cust4WeekTargetRatio Then ' style="color:#FF0000;"'
								When @Cust4WeekUtilization >= @Cust4WeekTargetRatio Then '' End)

				Select @redText12Wr = (Case
								When @CustAnnualUtilization < @CustAnnualTargetRatio Then ' style="color:#FF0000;"'
								When @CustAnnualUtilization >= @CustAnnualTargetRatio Then '' End)
			
SELECT @Cust1WeekTargetRatio_SubTotal = Cust1WeekTargetRatio,
@Cust1WeekUtilization_SubTotal = Cust1WeekUtilization,
@Diff1W_SubTotal = Diff1W,
@Cust4WeekTargetRatio_SubTotal = Cust4WeekTargetRatio,
@Cust4WeekUtilization_SubTotal = Cust4WeekUtilization,
@Diff4W_SubTotal = Diff4W,
@CustAnnualTargetRatio_SubTotal = CustAnnualTargetRatio,
@CustAnnualUtilization_SubTotal = CustAnnualUtilization,
@DiffAnnual_SubTotal = DiffAnnual
	
			FROM
(
Select oe.Email, 
oe.LastName + ', ' + IsNull (oe.PreferredName, oe.FirstName) Director, 
--UNCOMMENTING TWO LINES BELOW
--RIGHT(e.Org, 2) AS Org,
	--e.LastName + ', ' + IsNull (e.PreferredName, e.FirstName) Employee, 

       CASE When Sum (u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit) = 0 Then 0 
			Else Sum (u.Cust1WeekHoursDirect) / Sum (u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit) * 100 
			END Cust1WeekUtilization,
	   CASE When Sum (u.Cust1WeekHoursExpected) = 0 Then 0 
			Else Sum (u.CustTargetRatio * u.Cust1WeekHoursExpected) / Sum (u.Cust1WeekHoursExpected) 
			END Cust1WeekTargetRatio,
       CASE When Sum (u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit) = 0 Then 0 
			Else (Sum (u.Cust1WeekHoursDirect) / Sum (u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit) * 100) - Sum (u.CustTargetRatio * u.Cust1WeekHoursExpected) / Sum (u.Cust1WeekHoursExpected) 
			END Diff1W, 
       CASE When Sum (u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit) = 0 Then 0 
			Else Sum (u.Cust4WeekHoursDirect) / Sum (u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit) * 100 
			END Cust4WeekUtilization,
	   CASE When Sum (u.Cust4WeekHoursExpected) = 0 Then 0 
			Else Sum (u.CustTargetRatio * u.Cust4WeekHoursExpected) / Sum (u.Cust4WeekHoursExpected) 
			END Cust4WeekTargetRatio,
       CASE When Sum (u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit) = 0 Then 0 
			Else (Sum (u.Cust4WeekHoursDirect) / Sum (u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit) * 100) - Sum (u.CustTargetRatio * u.Cust4WeekHoursExpected) / Sum (u.Cust4WeekHoursExpected) 
			END Diff4W, 
       CASE When Sum (u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit) = 0 Then 0 
			Else Sum (u.CustAnnualHoursDirect) / Sum (u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit) * 100 
			END CustAnnualUtilization,
       CASE When Sum (u.CustAnnualHoursExpected) = 0 Then 0 
			Else Sum (u.CustTargetRatio * u.CustAnnualHoursExpected) / Sum (u.CustAnnualHoursExpected) 
			END CustAnnualTargetRatio,
       CASE When Sum (u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit) = 0 Then 0 
			Else (Sum (u.CustAnnualHoursDirect) / Sum (u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit) * 100) - Sum (u.CustTargetRatio * u.CustAnnualHoursExpected) / Sum (u.CustAnnualHoursExpected) 
			END DiffAnnual
			
From UDIC_Utilization u, 
	EM e, 
	EmployeeCustomTabFields  ex
       Left Join EM oe on oe.Employee = ex.CustEKDirector

Where u.CustEmployeeNumber = e.Employee
  and e.Employee = ex.Employee
  and oe.Email = @Email -- 'medwards@halff.com
  and RIGHT(e.Org, 2) = @Org -- @Org

Group by oe.Email, oe.LastName + ', ' + IsNull (oe.PreferredName, oe.FirstName) 
--Having count (distinct Right (e.Org, 2)) > 1

  ) AS A			


  				Select @redTextLWS = (Case
								When @Cust1WeekUtilization_SubTotal < @Cust1WeekTargetRatio_SubTotal Then ' style="color:#FF0000;"'
								When @Cust1WeekUtilization_SubTotal >= @Cust1WeekTargetRatio_SubTotal Then '' End)

				Select @redText4WS = (Case
								When @Cust4WeekUtilization_SubTotal < @Cust4WeekTargetRatio_SubTotal Then ' style="color:#FF0000;"'
								When @Cust4WeekUtilization_SubTotal >= @Cust4WeekTargetRatio_SubTotal Then '' End)

				Select @redText12WrS = (Case
								When @CustAnnualUtilization_SubTotal < @CustAnnualTargetRatio_SubTotal Then ' style="color:#FF0000;"'
								When @CustAnnualUtilization_SubTotal >= @CustAnnualTargetRatio_SubTotal Then '' End)

						Set @Body = 			(Case 
				When @CurrentOrg = @PreviousOrg Then 
				@Body + 
				'				
				<tr>
                    <td class="teamB">' + @Label + '</td> 
                    <td class="LWb">' + CAST(@Cust1WeekUtilization_SubTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="LWb">' + CAST(@Cust1WeekTargetRatio_SubTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
					<td class="LWb" ' + @redTextLWS + '>' + CAST(@Diff1W_SubTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
					<td class="L4Wb">' + CAST(@Cust4WeekUtilization_SubTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="L4Wb">' + CAST(@Cust4WeekTargetRatio_SubTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="L4Wb" ' +@redText4WS + '>' + CAST(@Diff4W_SubTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="L12Wb">' + CAST(@CustAnnualUtilization_SubTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="L12Wb">' + CAST(@CustAnnualTargetRatio_SubTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="L12Wrb" ' + @redText12WrS + '>' + CAST(@DiffAnnual_SubTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                </tr>
				'

				When @CurrentOrg != @PreviousOrg Then
							@Body + 
							''
			End)

			            Set @Body =
			@Body + 

'				<tr>
                    <td class="team">' + @Employee + '</td> 
                    <td class="LW">' + CAST(@Cust1WeekUtilization AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="LW">' + CAST(@Cust1WeekTargetRatio AS VARCHAR(10)) + @PercentSymbol + '</td>
					<td class="LW" ' + @redTextLW + '>' + CAST(@Diff1W AS VARCHAR(10)) + @PercentSymbol + '</td>
					<td class="L4W">' + CAST(@Cust4WeekUtilization AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="L4W">' + CAST(@Cust4WeekTargetRatio AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="L4W" ' + @redText4W + '>' + CAST(@Diff4W AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="L12W">' + CAST(@CustAnnualUtilization AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="L12W">' + CAST(@CustAnnualTargetRatio AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="L12Wr" ' + @redText12Wr + '>' + CAST(@DiffAnnual AS VARCHAR(10)) + @PercentSymbol + '</td>
                </tr>
'

					Set @CurrentOrg = @Org	

	FETCH NEXT FROM curUtilization INTO 
	@Label
	, @Email
	, @Director
	, @Org
	, @Employee
	, @Cust1WeekUtilization 
	, @Cust1WeekTargetRatio
	, @Diff1W
	, @Cust4WeekUtilization
	, @Cust4WeekTargetRatio
	, @Diff4W
	, @CustAnnualUtilization
	, @CustAnnualTargetRatio
	, @DiffAnnual

	Set @Loop1 = @@FETCH_STATUS

	END
	CLOSE curUtilization
	DEALLOCATE curUtilization
	
	SET @Body = @Body + '</tbody>'

SELECT @Cust1WeekTargetRatio_FinalTotal = Cust1WeekTargetRatio,
@Cust1WeekUtilization_FinalTotal = Cust1WeekUtilization,
@Diff1W_FinalTotal = Diff1W,
@Cust4WeekTargetRatio_FinalTotal = Cust4WeekTargetRatio,
@Cust4WeekUtilization_FinalTotal = Cust4WeekUtilization,
@Diff4W_FinalTotal = Diff4w,
@CustAnnualTargetRatio_FinalTotal = CustAnnualTargetRatio,
@CustAnnualUtilization_FinalTotal = CustAnnualUtilization,
@DiffAnnual_FinalTotal = DiffAnnual
	
			FROM
(
Select oe.Email, 
oe.LastName + ', ' + IsNull (oe.PreferredName, oe.FirstName) Director, 
--RIGHT(oe.Org, 2) AS Org,
	--e.LastName + ', ' + IsNull (e.PreferredName, e.FirstName) Employee, 

       CASE When Sum (u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit) = 0 Then 0 
			Else Sum (u.Cust1WeekHoursDirect) / Sum (u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit) * 100 
			END Cust1WeekUtilization,
	   CASE When Sum (u.Cust1WeekHoursExpected) = 0 Then 0 
			Else Sum (u.CustTargetRatio * u.Cust1WeekHoursExpected) / Sum (u.Cust1WeekHoursExpected) 
			END Cust1WeekTargetRatio,
       CASE When Sum (u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit) = 0 Then 0 
			Else (Sum (u.Cust1WeekHoursDirect) / Sum (u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit) * 100) - Sum (u.CustTargetRatio * u.Cust1WeekHoursExpected) / Sum (u.Cust1WeekHoursExpected) 
			END Diff1W, 
       CASE When Sum (u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit) = 0 Then 0 
			Else Sum (u.Cust4WeekHoursDirect) / Sum (u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit) * 100 
			END Cust4WeekUtilization,
	   CASE When Sum (u.Cust4WeekHoursExpected) = 0 Then 0 
			Else Sum (u.CustTargetRatio * u.Cust4WeekHoursExpected) / Sum (u.Cust4WeekHoursExpected) 
			END Cust4WeekTargetRatio,
       CASE When Sum (u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit) = 0 Then 0 
			Else (Sum (u.Cust4WeekHoursDirect) / Sum (u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit) * 100) - Sum (u.CustTargetRatio * u.Cust4WeekHoursExpected) / Sum (u.Cust4WeekHoursExpected) 
			END Diff4W, 
       CASE When Sum (u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit) = 0 Then 0 
			Else Sum (u.CustAnnualHoursDirect) / Sum (u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit) * 100 
			END CustAnnualUtilization,
       CASE When Sum (u.CustAnnualHoursExpected) = 0 Then 0 
			Else Sum (u.CustTargetRatio * u.CustAnnualHoursExpected) / Sum (u.CustAnnualHoursExpected) 
			END CustAnnualTargetRatio,
       CASE When Sum (u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit) = 0 Then 0 
			Else (Sum (u.CustAnnualHoursDirect) / Sum (u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit) * 100) - Sum (u.CustTargetRatio * u.CustAnnualHoursExpected) / Sum (u.CustAnnualHoursExpected) 
			END DiffAnnual
			
From UDIC_Utilization u, 
	EM e, 
	EmployeeCustomTabFields  ex
       Left Join EM oe on oe.Employee = ex.CustEKDirector

Where u.CustEmployeeNumber = e.Employee
  and e.Employee = ex.Employee
  and oe.Email = @Email -- 'medwards@halff.com'

Group by oe.Email, oe.LastName + ', ' + IsNull (oe.PreferredName, oe.FirstName)

  ) AS A			

  				Select @redTextLWF = (Case
								When @Cust1WeekUtilization_FinalTotal < @Cust1WeekTargetRatio_FinalTotal Then ' style="color:#FF0000;"'
								When @Cust1WeekUtilization_FinalTotal >= @Cust1WeekTargetRatio_FinalTotal Then '' End)

				Select @redText4WF = (Case
								When @Cust4WeekUtilization_FinalTotal < @Cust4WeekTargetRatio_FinalTotal Then ' style="color:#FF0000;"'
								When @Cust4WeekUtilization_FinalTotal >= @Cust4WeekTargetRatio_FinalTotal Then '' End)

				Select @redText12WrF = (Case
								When @CustAnnualUtilization_FinalTotal < @CustAnnualTargetRatio_FinalTotal Then ' style="color:#FF0000;"'
								When @CustAnnualUtilization_FinalTotal >= @CustAnnualTargetRatio_FinalTotal Then '' End)

	Set @Body = @Body + 
'
	    <tfoot>
            <tr>
                <td class="LWFleft" >Final Totals</td>
                <td class="LWF">' + CAST(@Cust1WeekUtilization_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="LWF">' + CAST(@Cust1WeekTargetRatio_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="LWF" ' + @redTextLWF + '>' + CAST(@Diff1W_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L4WF">' + CAST(@Cust4WeekUtilization_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L4WF">' + CAST(@Cust4WeekTargetRatio_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L4WF" ' + @redText4WF + '>' + CAST(@Diff4W_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L12WF">' + CAST(@CustAnnualUtilization_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L12WF">' + CAST(@CustAnnualTargetRatio_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L12WrF" ' + @redText12WrF + '>' + CAST(@DiffAnnual_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
            </tr>    
      </tfoot>
	  </table>
	  <br/>
'

	SET @RetVal = @Body;

	--**COMMENTED EMAIL RECIPIENT OUT BELOW FOR COMBINED PROCEDURE
	--Email Recipient Info Below
	--Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
	--Values ('Director Utilization', 'charrison@halff.com', 'charrison@halff.com', 'charrison@halff.com', 'Director Utilization', @CSS + @Body, getDate(), 1)
	
END
GO
