SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_All_ErrorReportingHistory] ( @product varchar(64)= '', @user nvarchar(32)= '', @ErrorNumber varchar(64)= null)
             AS EXEC spCCG_All_ErrorReportingHistory @product,@user,@ErrorNumber
GO
