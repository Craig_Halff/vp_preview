SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_ProjectLowestLevelsKeyConvert]
AS

/*
    Copyright (c) 2020 Halff Associates Inc. All rights reserved.
    Apply Key Conversions to Project Lowest Levels IC

    12/08/2020	Craig H. Anderson - Created
	04/12/2021	Craig H. Anderson
				Modified fetching of PLL IDs to allow for multiple returns
*/

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @Entity     VARCHAR(20)
      , @Period     INT
      , @PostSeq    INT
      , @OldKey     VARCHAR(32)
      , @OldKey2    VARCHAR(32)
      , @OldKey3    VARCHAR(32)
      , @NewKey     VARCHAR(32)
      , @NewKey2    VARCHAR(32)
      , @NewKey3    VARCHAR(32)
      , @NewName    VARCHAR(125)
      , @NewName2   VARCHAR(40)
      , @NewName3   VARCHAR(40)
      , @pName      VARCHAR(50)
      ----
      , @pllNameOld VARCHAR(255)
      , @pllNameNew VARCHAR(255)
      , @pllID      VARCHAR(255);
--
DECLARE key_convert CURSOR FOR
/* Projects */
SELECT kcl.Entity
     , kcl.Period
     , kcl.PostSeq
     , kcl.OldKey
     , kcl.OldKey2
     , kcl.OldKey3
     , kcl.NewKey
     , kcl.NewKey2
     , kcl.NewKey3
     , kcl.NewName
     , kcl.NewName2
     , kcl.NewName3
     , p.Name
FROM dbo.KeyConvertLog                     kcl
    INNER JOIN dbo.UDIC_ProjectLowestLevel AS pll
        ON kcl.OldKey = pll.CustWBS1
    INNER JOIN dbo.PR                      AS p
        ON kcl.NewKey = p.WBS1
           AND p.WBS2 = ' '
WHERE
    kcl.Entity = 'WBS1'
    AND kcl.Period >= (DATEPART(YEAR, pll.CreateDate) * 100 + DATEPART(MONTH, pll.CreateDate))
UNION
/* Phases */
SELECT kcl.Entity
     , kcl.Period
     , kcl.PostSeq
     , kcl.OldKey
     , kcl.OldKey2
     , kcl.OldKey3
     , kcl.NewKey
     , kcl.NewKey2
     , kcl.NewKey3
     , kcl.NewName
     , kcl.NewName2
     , kcl.NewName3
     , p.Name
FROM dbo.KeyConvertLog                     kcl
    INNER JOIN dbo.UDIC_ProjectLowestLevel AS pll
        ON kcl.OldKey = pll.CustWBS1
           AND kcl.OldKey2 = pll.CustWBS2
    INNER JOIN dbo.PR                      AS p
        ON kcl.NewKey = p.WBS1
           AND kcl.NewKey2 = p.WBS2
           AND p.WBS3 = ' '
           AND p.WBS3 <> ' '
WHERE
    kcl.Entity = 'WBS2'
    AND kcl.Period >= (DATEPART(YEAR, pll.CreateDate) * 100 + DATEPART(MONTH, pll.CreateDate))
UNION
/* Tasks */
SELECT kcl.Entity
     , kcl.Period
     , kcl.PostSeq
     , kcl.OldKey
     , kcl.OldKey2
     , kcl.OldKey3
     , kcl.NewKey
     , kcl.NewKey2
     , kcl.NewKey3
     , kcl.NewName
     , kcl.NewName2
     , kcl.NewName3
     , p.Name
FROM dbo.KeyConvertLog                     kcl
    INNER JOIN dbo.UDIC_ProjectLowestLevel AS pll
        ON kcl.OldKey = pll.CustWBS1
           AND kcl.OldKey2 = pll.CustWBS2
           AND kcl.OldKey3 = pll.CustWBS3
    INNER JOIN dbo.PR                      AS p
        ON kcl.NewKey = p.WBS1
           AND kcl.NewKey2 = p.WBS2
           AND kcl.NewKey3 = p.WBS3
           AND p.WBS3 <> ' '
WHERE
    kcl.Entity = 'WBS3'
    AND kcl.Period >= (DATEPART(YEAR, pll.CreateDate) * 100 + DATEPART(MONTH, pll.CreateDate))
ORDER BY
    kcl.Period
  , kcl.PostSeq
  , kcl.Entity;

/****************************************************
	Begin Loop
*****************************************************/
OPEN key_convert;
FETCH NEXT FROM key_convert
INTO @Entity
   , @Period
   , @PostSeq
   , @OldKey
   , @OldKey2
   , @OldKey3
   , @NewKey
   , @NewKey2
   , @NewKey3
   , @NewName
   , @NewName2
   , @NewName3
   , @pName;
WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @pllID = ' ';
        SET @pllNameNew = '';
        BEGIN
            --For debugging
            /*  PRINT CONCAT(
                  'Entity -> '
                , @Entity
                , '   WBS1-> PllID: '
                , @pllID
                , '   oldWBS1: '
                , @OldKey
                , '  New WBS1:'
                , @NewKey
                , '  oldWBS2: '
                , @OldKey2
                , '  NewWBS2: '
                , @NewKey2
                , '  oldKey3: '
                , @OldKey3
                , ' NewWBS3: '
                , @NewKey3
              );
			  */
            IF @Entity = 'WBS1'
                BEGIN
                    /*
            SET @pllID = (
                SELECT   pll.UDIC_UID
                FROM    dbo.UDIC_ProjectLowestLevel AS pll
                WHERE   pll.CustWBS1 = @OldKey
                        AND pll.CustWBS2 = @OldKey2
                        AND pll.CustWBS3 = @OldKey3
            );
			*/
                    SET @pllNameNew = CONCAT(@NewKey, ' ', @NewName);
                    --SELECT * FROM dbo.UDIC_ProjectLowestLevel AS pll WHERE pll.UDIC_UID = @pllID;

                    UPDATE dbo.UDIC_ProjectLowestLevel
                    SET CustWBS1 = @NewKey
                      , CustWBS1Name = @NewName
                      , CustName = @pllNameNew
                      , ModDate = GETDATE()
                      , ModUser = 'KeyConvert'
                    WHERE
                        UDIC_UID IN
                            (
                                SELECT pll.UDIC_UID
                                FROM dbo.UDIC_ProjectLowestLevel AS pll
                                WHERE
                                    pll.CustWBS1 = @OldKey
                                    AND pll.CustWBS2 = @OldKey2
                                    AND pll.CustWBS3 = @OldKey3
                            );
                --SELECT * FROM dbo.UDIC_ProjectLowestLevel AS pll WHERE pll.UDIC_UID = @pllID;
                END;

            IF @Entity = 'WBS2'
                BEGIN
                    /*
                    SET @pllID =
                        (
                            SELECT pll.UDIC_UID
                            FROM dbo.UDIC_ProjectLowestLevel AS pll
                            WHERE
                                pll.CustWBS1 = @OldKey
                                AND pll.CustWBS2 = @OldKey2
                                AND pll.CustWBS3 = @OldKey3
                        );
				*/
                    SET @pllNameNew = CONCAT(@NewKey, ' | ', @NewKey2, ' ', @NewName, ' | ', @NewName2);
                    --SELECT * FROM dbo.UDIC_ProjectLowestLevel AS pll WHERE pll.UDIC_UID = @pllID;
                    UPDATE dbo.UDIC_ProjectLowestLevel
                    SET CustWBS1 = @NewKey
                      , CustWBS1Name = @NewName
                      , CustWBS2 = @NewKey2
                      , CustWBS2Name = @NewName2
                      , CustName = @pllNameNew
                      , ModDate = GETDATE()
                      , ModUser = 'KeyConvert'
                    WHERE
                        UDIC_UID IN
                            (
                                SELECT pll.UDIC_UID
                                FROM dbo.UDIC_ProjectLowestLevel AS pll
                                WHERE
                                    pll.CustWBS1 = @OldKey
                                    AND pll.CustWBS2 = @OldKey2
                                    AND pll.CustWBS3 = @OldKey3
                            );

                END;

            IF @Entity = 'WBS3'
               AND @OldKey3 <> @NewKey3
                BEGIN
                    SET @pllID =
                        (
                            SELECT pll.UDIC_UID
                            FROM dbo.UDIC_ProjectLowestLevel AS pll
                            WHERE
                                pll.CustWBS1 = @OldKey
                                AND pll.CustWBS2 = @OldKey2
                                AND pll.CustWBS3 = @OldKey3
                        );
                    SET @pllNameNew = CONCAT(@NewKey, ' | ', @NewKey2, ' | ', @NewKey3, ' ', @NewName, ' | ', @NewName2, ' | ', @NewName3);

                    --SELECT * FROM dbo.UDIC_ProjectLowestLevel AS pll WHERE pll.UDIC_UID = @pllID;

                    UPDATE dbo.UDIC_ProjectLowestLevel
                    SET CustWBS1 = @NewKey
                      , CustWBS1Name = @NewName
                      , CustWBS2 = @NewKey2
                      , CustWBS2Name = @NewName2
                      , CustWBS3 = @NewKey3
                      , CustWBS3Name = @NewName3
                      , CustName = @pllNameNew
                      , ModDate = GETDATE()
                      , ModUser = 'KeyConvert'
                    WHERE UDIC_UID = @pllID;
                --SELECT * FROM dbo.UDIC_ProjectLowestLevel AS pll WHERE pll.UDIC_UID = @pllID;
                END;
        END;

        FETCH NEXT FROM key_convert
        INTO @Entity
           , @Period
           , @PostSeq
           , @OldKey
           , @OldKey2
           , @OldKey3
           , @NewKey
           , @NewKey2
           , @NewKey3
           , @NewName
           , @NewName2
           , @NewName3
           , @pName;
    END;
CLOSE key_convert;
DEALLOCATE key_convert;
GO
