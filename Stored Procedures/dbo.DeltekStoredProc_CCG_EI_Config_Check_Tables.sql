SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Check_Tables]
AS
BEGIN
	SET NOCOUNT ON;

	-- Check if PAT exists for module links
	SELECT COUNT('x') AS PAT_Exists					-- Either 1 or 0
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[CCG_PAT_Config]') AND type in (N'U');

	-- Check if ARM exists for module links:
    SELECT COUNT('x') AS ARM_Exists					-- Either 1 or 0
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[CCG_ARM_Config]') AND type in (N'U');

	-- Check if Mobile Docs exists:
	Select COUNT('x') AS MD_Exists
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[CCG_MD_Config]') AND type in (N'U')

	-- Determine if EVPay exists:
	SELECT COUNT('x') AS EVPay_Exists 					-- 1 or 0
		FROM sys.objects
		WHERE (object_id = OBJECT_ID(N'[dbo].[EVA_EVPay_Config]'))
			AND type in (N'U');

    -- Determine if CCGGrid exists:
	SELECT COUNT('x') AS CCGGrid 					-- Should be 2 if grid exists
		FROM sys.objects
		WHERE (object_id = OBJECT_ID(N'[dbo].[CCG_GRID_Config]')
			OR object_id = OBJECT_ID(N'[dbo].[CCG_EI_ConfigGrid]'))
			AND type in (N'U');



END;


GO
