SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Ctrl_LoadPayables] ( @custFields nvarchar(max), @outerApplies nvarchar(max), @filter nvarchar(max), @employeeId nvarchar(32), @hasDisbursement bit, @joinVE bit, @joinVEAccounting bit, @joinVECustom bit, @getVisionAmounts bit, @useVisionAmountField nvarchar(100), @useSqlFilter bit= 1, @showAllRelevantContracts bit= 0, @payableFilterIsTableFunction bit= 0, @applyPayableFilterLast bit= 1)
             AS EXEC spCCG_PAT_Ctrl_LoadPayables @custFields,@outerApplies,@filter,@employeeId,@hasDisbursement,@joinVE,@joinVEAccounting,@joinVECustom,@getVisionAmounts,@useVisionAmountField,@useSqlFilter,@showAllRelevantContracts,@payableFilterIsTableFunction,@applyPayableFilterLast
GO
