SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Cust_Config_Get_Windows_Username]
	@KEY_EMPLOYEEID	Nvarchar(20)
AS
BEGIN
	-- [DeltekStoredProc_CCG_PAT_Cust_Config_Get_Windows_Username] '00001'
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	DECLARE @safeSql		int;

	-- Ensure that our inputs are safe (prevent SQL injection)
	SET @safeSql = dbo.fnCCG_PAT_RegexMatch(N'<STRING VALUE>', @KEY_EMPLOYEEID);
	IF @safeSql < 1 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS Nvarchar(4));
		RETURN;
	END;

	IF EXISTS(
		SELECT * FROM sys.objects b, sys.columns a
		WHERE b.object_id = OBJECT_ID(N'[dbo].[EmployeeCustomTabFields]') AND type in (N'U')
			AND a.object_id = b.object_id
			AND LOWER(a.name) = 'custwindowsusername') BEGIN
		EXEC (N'
			SELECT custWindowsUsername
			FROM EmployeeCustomTabFields
			WHERE Employee = N''' + @KEY_EMPLOYEEID + ''';');
	END
	ELSE SELECT '' as custWindowsUsername;
END;


GO
