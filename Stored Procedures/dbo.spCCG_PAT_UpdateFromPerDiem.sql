SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_PAT_UpdateFromPerDiem] (
      @UDIC_UID varchar(50),--should be set from workflow
      @UpdatePDStatus varchar(50) = null,--set value if PO Status should be updated.
      @Employee varchar(50) = null--used as create by and history records, defaults to PO moduser
      )
AS
BEGIN
      set nocount on

      BEGIN TRY
            --select * from UDIC_PurchaseOrder
            --select * from CCG_PAT_History where payableseq > 161
            --select * from CCG_PAT_Payable where SourcePKey = 'A9856B5D7664436688D741976B268901'
			--select * from CCG_PAT_ProjectAmount where PayableSeq > 161
            --exec spCCG_PAT_UpdateFromPO 'A9856B5D7664436688D741976B268901'
            declare @useEmp as varchar(50)
            declare @POApprover as varchar(50)
            declare @POStatus as varchar(50)
            select @useEmp = ISNULL(@Employee,ue.Employee), @POStatus = PD.CustPDStatus, 
                        @POApprover = PD.CustApprover --if there is an approver defined in PO UDIC set here
                  FROM [UDIC_PerDiemRequest] PD
					LEFT JOIN (select USERNAME,MAX(Employee) as Employee from SEUser Group By Username ) ue on PD.ModUser = ue.USERNAME
                  WHERE PD.UDIC_UID = @UDIC_UID
            
            declare @PATSeq int
            select @PATSeq = Seq from CCG_PAT_Payable where PayableType = 'P' and SourcePKey = @UDIC_UID
            
            declare @useRoute as varchar(50)
            if(@POApprover is null)
                  set @useRoute = 'HALFF'
            else 
                  set @useRoute = 'PO Approver'
                        
            declare @useStage as varchar(50)
            select @useStage = Max(Stage) from CCG_PAT_ConfigStages where [TYPE] = 'Default'
            --select * from CCG_PAT_ConfigStages
            
            IF @PATSeq is null--no existing, full insert
            BEGIN
                  declare  @payableSeq int 
                  insert into CCG_PAT_Payable ( Vendor,Company,PayableType ,  PayableGroup , PayableNumber , Stage , PayableDate , PayTerms , PayDate , Address , SourceType , SourcePKey , SourceSeq ,[Route], ModDate , ModUser , CreateDate , CreateUser  ) 
                  --values ('0000000100', ' ' ,'P', NULL, NULL, '12233',  'Pending' , '2015-01-29',  NULL, NULL, '', null, null, NULL, NULL, NULL, NULL, NULL, 'UDIC_PurchaseOrder', 'DA87E161BDE342E18122B23944266C0D', NULL, NULL, getutcdate() , '00001', getutcdate() , '00001')   
                  SELECT  '008570', (select max(left(PR.Org, 2)) from [UDIC_PerDiemRequest_PerDiemBreakdown] items inner join PR on PR.WBS1 = items.CustProjectNumber and PR.WBS2 = ' ' where PD.UDIC_UID = items.UDIC_UID), 
						'P',  NULL,  CustNumber, @useStage, PD.CustRequestDate,         NULL,       NULL, NULL,             
						'PD',[UDIC_UID],NULL,@useRoute,getutcdate() , @useEmp, getutcdate() , @useEmp
                    FROM [UDIC_PerDiemRequest] PD
                    WHERE PD.UDIC_UID = @UDIC_UID
                  SET @payableSeq = SCOPE_IDENTITY() 				  
                  
                  --History inserts
                  insert into CCG_PAT_History (ActionTaken, ActionDate, ActionTakenBy, PayableSeq, Stage, Description)  
                  values ('Create', getutcdate(), @useEmp, @payableSeq,'' , 'PD:Update PAT' ) 
                  insert into CCG_PAT_History (ActionTaken, ActionDate, ActionTakenBy, PayableSeq, Stage, Description)  
                  values ('Stage Change', getutcdate(), @useEmp, @payableSeq,@useStage , 'Default' ) 
                  
                  --Project Amounts
                  insert into  CCG_PAT_ProjectAmount  ( 
							Pkey, PayableSeq ,WBS1 ,WBS2 ,WBS3 , Amount , NetAmount, ExpenseCode , GLAccount , Description , SuppressBill , TaxCode , Tax2Code  , TaxAmount , Tax2Amount  ) 
                  --values (@payableSeq, '2003005.00', '1PD', 'COD', 99.99,'','','pa desc','N',NULL,NULL, 99.99, 0, 0 ); 
                        SELECT Replace(NEWID(),'-', ''), @payableSeq, items.CustProjectNumber, isnull(items.CustPhaseNumber, ' '), isnull(items.CustTaskNumber, ' '),
								items.CustPerDiem, items.CustPerDiem, null, 
								(case when PR.ChargeType = 'R' then '520.06' else '783.06' end), cast(items.CustDay as varchar(20)),'N',NULL,NULL,0,0
                          FROM [UDIC_PerDiemRequest] PD
                          INNER JOIN [UDIC_PerDiemRequest_PerDiemBreakdown] items on PD.UDIC_UID = items.UDIC_UID
						  LEFT JOIN PR On PR.WBS1 = items.CustProjectNumber and PR.WBS2 = ' ' and PR.WBS3 = ' '
                          WHERE PD.UDIC_UID = @UDIC_UID

				  insert into CCG_PAT_Pending (PayableSeq,Route,Description,SortOrder,Employee,CreateDateTime) 
						select @payableSeq, null, @useRoute, 1, @POApprover, GETUTCDATE()		

                  --ROUTING
                  --IF ISNULL(@useRoute,'') = 'PO Auto'
                  --BEGIN
                  --    exec spCCG_PAT_SPRoute1 @payableSeq,'PO Auto',1,'1000',''
                  --END
                  --ELSE IF ISNULL(@useRoute,'') = 'PO Approver'
                  --BEGIN 
                  --    insert into CCG_PAT_Pending (PayableSeq,Route,Description,SortOrder,Employee,CreateDateTime) 
                  --    values ( @payableSeq, 'PO Default','Approver',1, '00003',GETUTCDATE() ) 
                  --END
                  --if route and pending not set here can be updated in PAT later
                  
                  --select * from CCG_PAT_Payable where Seq =  @payableSeq  
                  
                  --PO updates
                  if ISNULL(@UpdatePDStatus,'') != ''
                  BEGIN 
                        update [UDIC_PerDiemRequest] 
                        set CustPDStatus = @UpdatePDStatus
                        WHERE UDIC_UID = @UDIC_UID
                  END				 
            END
            
            ELSE-- existing PAT records found, just update
            BEGIN
                  --update main PAT record as needed
                  update CCG_PAT_Payable set ModDate = GETUTCDATE(),ModUser = @useEmp
                  WHERE Seq = @PATSeq--WHERE CLAUSE IMPORTANT, DON'T UPDATE ALL
                  
                  --History inserts
                  insert into CCG_PAT_History (ActionTaken, ActionDate, ActionTakenBy, PayableSeq, Stage, Description)  
                  values ('Modify', getutcdate(), @useEmp, @payableSeq,'' , 'PD:Update PAT' ) 
                  
                  --Project Amounts
                  delete from CCG_PAT_ProjectAmount WHERE PayableSeq = @PATSeq--WHERE CLAUSE IMPORTANT, DON'T DELETE ALL
                  insert into  CCG_PAT_ProjectAmount  ( 
							Pkey, PayableSeq ,WBS1 ,WBS2 ,WBS3 , Amount , NetAmount, ExpenseCode , GLAccount , Description , SuppressBill , TaxCode , Tax2Code  , TaxAmount , Tax2Amount  ) 
                  --values (@payableSeq, '2003005.00', '1PD', 'COD', 99.99,'','','pa desc','N',NULL,NULL, 99.99, 0, 0 ); 
                        SELECT Replace(NEWID(),'-', ''), @payableSeq, items.CustProjectNumber, isnull(items.CustPhaseNumber, ' '), isnull(items.CustTaskNumber, ' '),
								items.CustPerDiem, items.CustPerDiem, null, 
								(case when PR.ChargeType = 'R' then '520.06' else '783.06' end), cast(items.CustDay as varchar(20)),'N',NULL, NULL, 0,0
                          FROM [UDIC_PerDiemRequest] PD
                          INNER JOIN [UDIC_PerDiemRequest_PerDiemBreakdown] items on PD.UDIC_UID = items.UDIC_UID
						  LEFT JOIN PR On PR.WBS1 = items.CustProjectNumber and PR.WBS2 = ' ' and PR.WBS3 = ' '
                          WHERE PD.UDIC_UID = @UDIC_UID                

                  --PO updates
                  --if ISNULL(@UpdatePDStatus,'') != ''
                  --BEGIN 
                  --    update [UDIC_PurchaseOrder] 
                  --    set CustStatus = @UpdatePDStatus
                  --    WHERE UDIC_UID = @UDIC_UID and CustStatus = 'New'
                  --END
            END
      END TRY
      BEGIN CATCH
            
            select -1 as Result, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage

      END CATCH

END
GO
