SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpCreateActivity]
  @strPlanID VARCHAR(32),
  @strUserName as Nvarchar(32)
AS

BEGIN -- Procedure rpCreateActivity

declare @AssignmentID varchar(32)
declare @TaskID varchar(32)
declare @StartDate datetime
declare @EndDate datetime
declare @ResourceID as Nvarchar(20)
declare @ActivityID as varchar(32)
declare @Subject as Nvarchar(255)


-- Get all records in RPAssignment with ResourceID not null

   DECLARE AssignmentInfo INSENSITIVE CURSOR FOR
     select TaskID, AssignmentID, StartDate, EndDate,ResourceID from RPAssignment where PlanID = @strPlanID
		    and ResourceID is not Null

   BEGIN
     OPEN AssignmentInfo
     FETCH NEXT from AssignmentInfo INTO @TaskID, @AssignmentID,@StartDate,@EndDate,@ResourceID   
     WHILE @@FETCH_STATUS = 0
     BEGIN
		--Insert a Activity Record and EMActivity record
		set @ActivityID = REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')
		set @Subject = dbo.GetConCatTaskName(@strPlanID,@TaskID)
		insert into Activity 
		(ActivityID, 
		 AssignmentID,
		 Subject,
		 StartDate,
		 EndDate,
		 Employee,
		 ReminderMinHrDay,
		 ShowTimeAs,
		 Type,
		 RP,
		 CreateDate,
		 CreateUser,
		 ModDate,
		 ModUser)
		 Values(@ActivityID,
		 @AssignmentID,
		 @Subject,
		 @StartDate,
		 @EndDate,
		 @ResourceID,
		 'Minutes',
		 'Tentative',
		 'Task',
		 'Y',
		 CONVERT(VARCHAR, GETUTCDATE(), 121), 
		 @strUserName,
		 CONVERT(VARCHAR, GETUTCDATE(), 121),
		 @strUserName)
		 
		 --EMActivity
		 Insert into EMActivity 
			(activityID, 
			owner, 
			employee, 
			createdate,
			createUser, 
			moddate,
			ModUser) 
			values(@ActivityID,
			'Y',
			@ResourceID,
			CONVERT(VARCHAR, GETUTCDATE(), 121), 
			@strUserName,
			CONVERT(VARCHAR, GETUTCDATE(), 121),
			@strUserName)
			    
	    FETCH NEXT from AssignmentInfo INTO @TaskID, @AssignmentID,@StartDate,@EndDate,@ResourceID   
		 
     END   
     CLOSE AssignmentInfo
     DEALLOCATE AssignmentInfo
   end
  
END -- rpCreateActivity
GO
