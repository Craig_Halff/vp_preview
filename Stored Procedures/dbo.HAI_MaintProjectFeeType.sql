SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create PROCEDURE [dbo].[HAI_MaintProjectFeeType] 
	-- Add the parameters for the stored procedure here

AS
BEGIN

	  Update ProjectCustomTabFields
	  Set CustFeeType = 'Cost Plus'
      Where CustBillingType like 'Cost Plus%'

	  Update ProjectCustomTabFields
	  Set CustFeeType = 'Cost Plus Max'
      Where CustBillingType like 'Cost Plus%Max%'

	  Update ProjectCustomTabFields
	  Set CustFeeType = 'Cost Plus Fixed Fee'
      Where CustBillingType like 'Cost Plus Fixed Fee%'

	  Update ProjectCustomTabFields
	  Set CustFeeType = 'Lump Sum'
      Where CustBillingType like 'Lump Sum%'

	  Update ProjectCustomTabFields
	  Set CustFeeType = 'Non-Billable'
      Where CustBillingType = 'Non-Billable'

	  Update ProjectCustomTabFields
	  Set CustFeeType = 'Percent of Construction'
      Where CustBillingType = 'Percent of Construction'

	  Update p2
	     Set p2.CustFeeType = p1.CustFeeType
		 From ProjectCustomTabFields p1, ProjectCustomTabFields p2
		 Where p1.WBS2 = ' ' -- project level
		   and p1.WBS1 = p2.WBS1
		   and p2.WBS2 <> ' ' -- phase level
		   and p1.CustFeeType is not null
		   and p2.CustBillingType is null

         Update p3
	     Set p3.CustFeeType = p2.CustFeeType
		 From ProjectCustomTabFields p2, ProjectCustomTabFields p3
		 Where p2.WBS2 <> ' ' -- phase level
		   and p2.WBS3 = ' '
		   and p2.WBS1 = p3.WBS1
		   and p2.WBS2 = p3.WBS2
		   and p3.WBS3 <> ' ' -- task level
		   and p2.CustFeeType is not null
		   and p3.CustBillingType is null

	  Update ProjectCustomTabFields
	  Set CustFeeType = 'Lump Sum'
      Where CustLumpSumPhase = 'Y'

   -- Propagate Fee Type to lower levels
	  Update p3
	  Set p3.CustFeeType = 'Lump Sum'
	  From ProjectCustomTabFields p2, ProjectCustomTabFields p3
      Where p2.WBS2 <> ' '
        and p2.WBS3 = ' ' -- phase level for checkbox
		and p2.CustLumpSumPhase = 'Y'
		and p2.WBS1 = p3.WBS1
		and p2.WBS2 = p3.WBS2
		and p3.WBS3 <> ' '  -- task level for sublevel

/* 
Select p.Biller, p.WBS1, p.WBS2, p.WBS3, px.CustFeeType
from PR p, ProjectCustomTabFields px
where p.ChargeType = 'R'
	and p.WBS1 = px.WBS1
	and p.WBS2 = px.WBS2
	and p.WBS3 = px.WBS3
	and CustFeeType is null
	and exists (Select 'x' From PR Where WBS1 = px.WBS1 and WBS2 = ' ' and Status = 'A')
*/
	
END
GO
