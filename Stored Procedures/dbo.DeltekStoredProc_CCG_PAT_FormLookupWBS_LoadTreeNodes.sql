SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FormLookupWBS_LoadTreeNodes] ( @exactMatch bit, @filter nvarchar(max), @statusFilter nvarchar(max), @projectFilter nvarchar(max), @VP2 bit= 0)
             AS EXEC spCCG_PAT_FormLookupWBS_LoadTreeNodes @exactMatch,@filter,@statusFilter,@projectFilter,@VP2
GO
