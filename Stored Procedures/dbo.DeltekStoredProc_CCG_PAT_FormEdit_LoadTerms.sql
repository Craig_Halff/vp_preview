SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FormEdit_LoadTerms]
	@v					bit,
	@mc					bit,
	@ve					Nvarchar(max),
	@company			Nvarchar(max)
AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_FormEdit_LoadTerms] 1, 1, '00FIDELITY', ''
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max);

	SET @sql = N'
		SELECT code, description, max(vedef), min(sortorder)
            FROM (' +
            (CASE WHEN @v = 1 THEN '
                SELECT UPPER(payterms) as code,
                        case when UPPER(payterms) like ''[0-9]%'' then ''DATE '' + UPPER(payterms) else UPPER(payterms) end as description,
                        max(case when vendor = N''' + @ve + ''' then UPPER(payterms) else '''' end) as vedef,
                        case when UPPER(payterms) like ''[0-9]%'' then 10 + LEN(UPPER(payterms)) else 1 end as sortorder
                    FROM VEAccounting
                    WHERE not payterms is null and vendor = N''' + @ve + ''' ' +
                        (CASE WHEN @mc = 1 THEN ' and VEAccounting.company = N''' + @company + '''' ELSE '' END) + '
                    GROUP BY UPPER(payterms)
                UNION ' ELSE ''
			END) + '
                SELECT ''Date'' as code, ''DATE'' as description, '''' as vedef, 1 as sortorder
                UNION
                SELECT ''Hold'' as code, ''HOLD'' as description, '''' as vedef, 1 as sortorder
                UNION
                SELECT ''Next'' as code, ''NEXT'' as description, '''' as vedef, 1 as sortorder
                UNION
                SELECT ''PWP'' as code, ''PWP'' as description, '''' as vedef, 1 as sortorder
            ) vedef
            GROUP BY code, description
            ORDER BY 4, 2';
	EXEC (@sql);
END;
GO
