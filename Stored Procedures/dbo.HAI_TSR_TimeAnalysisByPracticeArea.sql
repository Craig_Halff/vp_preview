SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Conrad Harrison>
-- Create date: <10-10-2018>
-- Description:	<Deltek scripts from 6-19-18 with Declared Variable Pass throughs>
-- =============================================
CREATE PROCEDURE [dbo].[HAI_TSR_TimeAnalysisByPracticeArea]
	@CompanyId varchar(2),
	@PracticeArea varchar(75),
	@LastSunday DATE,
	@LastSundayMinus363Days DATE,
	@LastSundayAndTime DATETIME,
	@LastSundayMinus363DaysAndTime DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;IF EXISTS (SELECT name FROM sysobjects WHERE name = 'setContextInfo' AND type = 'P')  
Execute dbo.setContextInfo 
--@StrCompany = /*N*/'01',
@StrAuditingEnabled  = 'N',
@strUserName = /*N*/'HIT',
@StrCultureName = 'en-US';
-- Halff Associates Time Analysis
-- Group by Company, Office, Team, Team Practice Area
/*
DECLARE '2017-05-29T00:00:00.000'	date = '01/01/2005',
		'2018-05-27T00:00:00.000'	date = '12/31/2005'
*/
--ABOVE THIS LINE IS FOR TESTING ONLY

/**********************************************************************************************************************************

Query originally written by David Spring and sent to Darrell Aaron on 2018-04-20

Modified by Darrell Aaron on 2018-05-01 to make Vision reportable
	-Changed input parameters to custStartDate but needed a DECLARE of @StartDate and @EndDate to modify those dates
	-Added Deltek standard grouping options, Deltek sorting and Deltek WHERE clause
	-Converted Expected to float so that it would SUM in the report properly
	-Changed old SQL style joins to modern so that Deltek script injection would work properly
	-Deleted custom filtering parameters for Company, Team, TPA, Office because those can all be handled in Vision UI

Modified 2018-06-19 by Darrell Aaron via changes David sent to me
	-Added limitation to EndDate based on max timesheet dates
	-Changed 8 hours per day constant to EM.HoursPerDay

**********************************************************************************************************************************/


DECLARE @StartDate date, @EndDate date, @TKMaxDate date
BEGIN
--  Backup Start Date to prior Monday
	--Set @StartDate = DateAdd (day, -DatePart (dw, '2017-05-29T00:00:00.000') + 2, '2017-05-29T00:00:00.000')
	Set @StartDate = @LastSundayMinus363Days
--  Backup End Date to prior Sunday
	--Set @EndDate = DateAdd (day, -DatePart (dw, '2018-05-27T00:00:00.000') + 1, '2018-05-27T00:00:00.000')
	Set @EndDate = @LastSunday
	SELECT @TKMaxDate = MAX(EndDate) FROM tkMaster WHERE Submitted = 'P'
	IF @EndDate > @TKMaxDate SET @EndDate = @TKMaxDate

	Select ISNULL(substring(EM.Org, 7, 3), '') As group1, Min(groupCFGOrgCodes3.Label) As groupDesc1, 
		org1.Code + ' - ' + org1.Label Company, org2.Code + ' - ' + org2.Label Office, tpa.TeamPracticeArea, org3.Code + ' - ' + org3.Label Team, 
		EM.Employee + ' - ' + EM.LastName + ', ' + IsNull (EM.PreferredName, EM.FirstName) Employee,
		dbo.WorkDays (dbo.GreatestDate (@StartDate, EM.HireDate), dbo.LeastDate (IsNull (EM.TerminationDate, @EndDate), @EndDate)) * EM.HoursPerDay StdHrs,
	--  Least of the Total Hours or the Std Hours, but if rehired, then check the Hire Date against the Trans Date
		CONVERT(float, dbo.Least (Sum (l.RegHrs + l.OvtHrs + l.SpecialOvtHrs), 
					CASE When EM.HireDate > min (l.TransDate) Then 9999999 Else
					abs (dbo.WorkDays (dbo.GreatestDate (@StartDate, EM.HireDate), dbo.LeastDate (IsNull (EM.TerminationDate, @EndDate), @EndDate))) * EM.HoursPerDay
					END
				  )) Expected,
		Sum (l.RegHrs + l.OvtHrs + l.SpecialOvtHrs) Total,
		Sum (CASE When l.ChargeType = 'R' Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) Direct,
		Sum (CASE When l.ChargeType <> 'R' Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) InDirect,
		-- Direct / (Expected - Benefits) calculate this column
		EM.TargetRatio, -- Hide this column
		-- Target = Sum (TargetRatio * Expected) / Sum (Expected) calculate this column  /*MATHEMATICALLY THIS IS JUST TARGET RATIO ON AN INDIVIDUAL BASIS*/
		-- Variance from Goal = TargetRatio - (Direct / (Expected - Benefits)) calculate this column
		Sum (CASE When pto.StartWBS1 is not null Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) PTO,
		Sum (CASE When oh.StartWBS1 is not null Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) Overhead,
		Sum (CASE When l.ChargeType = 'P' Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) Prospect,
		Sum (CASE When mktg.StartWBS1 is not null Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) Marketing,
		Sum (CASE When pd.StartWBS1 is not null Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) ProfDevelop,
		Sum (CASE When conf.StartWBS1 is not null Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) Conference,
		Sum (CASE When b.StartWBS1 is not null Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) Benefits -- Hide this column
		-- Other Benefits = Benefits - PTO calculate this column
	From EM
		Left Join CFGOrgCodes org1 on org1.Code = Left (EM.Org, (Select Org1Length From CFGFormat)) and org1.OrgLevel = '1' -- Company
		Left Join CFGOrgCodes org2 on org2.Code = Substring (EM.Org, (Select Org1Length + 2 From CFGFormat), (Select Org2Length From CFGFormat)) and org2.OrgLevel = '2'  -- Office
		Left Join CFGOrgCodes org3 on org3.Code = Right (EM.Org, (Select Org3Length From CFGFormat)) and org3.OrgLevel = '3'  -- Team
		INNER JOIN EmployeeCustomTabFields ECT ON ECT.Employee = EM.Employee
		Left Join
		(Select Code, DataValue TeamPracticeArea From FW_CustomColumnValues
		Where InfocenterArea = 'Employees' and ColName = 'CustTeamPracticeArea') tpa on tpa.Code = ECT.CustTeamPracticeArea
		INNER JOIN LD l ON l.Employee = EM.Employee
		Left Join  -- Total Benefits
		(Select a.StartWBS1, a.EndWBS1 From CFGTimeAnalysisHeadingsData h, CFGTimeAnalysis a
		Where h.ReportColumn = a.ReportColumn and h.Benefit = 'Y') b on l.WBS1 between b.StartWBS1 and b.EndWBS1
		Left Join  -- PTO
		(Select a.StartWBS1, a.EndWBS1  From CFGTimeAnalysisHeadings h, CFGTimeAnalysis a
		Where h.ReportColumn = a.ReportColumn and h.Label = 'PTO') pto on l.WBS1 between pto.StartWBS1 and pto.EndWBS1
		Left Join  -- Overhead
		(Select a.StartWBS1, a.EndWBS1 From CFGTimeAnalysisHeadings h, CFGTimeAnalysis a
		Where h.ReportColumn = a.ReportColumn and h.Label = 'Overhead') oh on l.WBS1 between oh.StartWBS1 and oh.EndWBS1
		Left Join  -- Marketing
		(Select a.StartWBS1, a.EndWBS1 From CFGTimeAnalysisHeadings h, CFGTimeAnalysis a
		Where h.ReportColumn = a.ReportColumn and h.Label = 'Marketing') mktg on l.WBS1 between mktg.StartWBS1 and mktg.EndWBS1
		Left Join  -- Professional Development
		(Select a.StartWBS1, a.EndWBS1 From CFGTimeAnalysisHeadings h, CFGTimeAnalysis a
		Where h.ReportColumn = a.ReportColumn and h.Label = 'Prof Develop') pd on l.WBS1 between pd.StartWBS1 and pd.EndWBS1
		Left Join  -- Conference
		(Select a.StartWBS1, a.EndWBS1 From CFGTimeAnalysisHeadings h, CFGTimeAnalysis a
		Where h.ReportColumn = a.ReportColumn and h.Label = 'Conference') conf on l.WBS1 between conf.StartWBS1 and conf.EndWBS1
		

	 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(EM.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3

	Where l.TransDate between @StartDate and @EndDate  AND (((SUBSTRING(EM.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select 'x' FROM EmployeeCustomTabFields INNER JOIN  FW_CustomColumnValues CCV ON CCV.Code = EmployeeCustomTabFields.CustTeamPracticeArea AND CCV.InfoCenterArea = /*N*/'Employees' AND CCV.ColName = 'CustTeamPracticeArea' WHERE EM.Employee = EmployeeCustomTabFields.Employee AND (CCV.DataValue = /*N*/@PracticeArea)) AND ( EXISTS (Select 'x' FROM CFGEmployeeStatus WHERE CFGEmployeeStatus.status = EM.Status AND ((CFGEmployeeStatus.Status = /*N*/'A'))) OR ((EM.TerminationDate >= convert(dateTime,Replace(@LastSundayMinus363Days,'T',' '),121)) AND (EM.TerminationDate <= convert(dateTime,Replace(@LastSunday,'T',' '),121))))))
	Group by ISNULL(substring(EM.Org, 7, 3), ''),  org1.code, org1.Label, org2.Code, org2.Label, org3.Code, org3.Label, tpa.TeamPracticeArea, EM.Employee, EM.LastName, IsNull (EM.PreferredName, EM.FirstName), EM.TargetRatio, EM.HoursPerDay, EM.HireDate, EM.TerminationDate
	 ORDER BY group1
--  Order by should be "Variance from Goal" ascending (done in report builder)
END
END
GO
