SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_LoadFeeDataCategories]
	@filterBillGrp						bit,
	@filterSQL							bit,
	@isRoleAcct							bit,
	@supportBuiltInFeePctCpl			varchar(10),
	@isSingleProject					bit,
	@invoiceGroup						Nvarchar(255),
	@User								Nvarchar(32),
	@wbs1List							Nvarchar(max),			-- .Replace(" ", "").Replace(",", "','")
	@includeDormant						varchar(5),
	@isSingleWBS2						bit,
	@wbs2								Nvarchar(7),
	@wbs3								Nvarchar(7),
	@seq								int,
	@VISION_LANGUAGE					varchar(10) = null
AS
BEGIN
	/*
	[spCCG_EI_LoadFeeDataCategories] @filterBillGrp = '0', @filterSQL = '1',
		 @isRoleAcct = '1', @supportBuiltInFeePctCpl = 'Q', @isSingleProject = '1',
		 @invoiceGroup = '', @User = '', @wbs1List = '2003005.00', @includeDormant = 'N', @isSingleWBS2 = 0, @wbs2 = '', @wbs3 = '', @seq = null, @VISION_LANGUAGE = ''
	*/
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	declare @EmpCompany	varchar(14) = ''
	if exists (select 'x' from CFGSystem where MulticompanyEnabled = 'Y')
	begin
		declare @sql2 nvarchar(max) = 'select @homeCompany = HomeCompany from SEUSER u left join EM on u.Employee = EM.Employee 
			where u.Username = '''+@User+''' or '''+isnull(@User,'')+''' = '''''
		EXECUTE SP_EXECUTESQL @sql2, N'@homeCompany varchar(14) OUTPUT', @homeCompany = @EmpCompany OUTPUT
	end
	DECLARE @Categories int = (Select max(m.RevenueCategories) from CFGMainData m where m.Company = @EmpCompany)

	DECLARE @VisionLang		varchar(20)
	if isnull(@VISION_LANGUAGE, '') = ''
		select @VisionLang = min(ISNULL(EM.Language,dbo.FW_GetActiveCultureName())) from SEUSER u
			left join EM on u.Employee = EM.Employee
			where u.Username = @User or isnull(@User, '') = ''
	else
		set @VisionLang = @VISION_LANGUAGE

	DECLARE @Cat1Label varchar(200) = '', @Cat2Label varchar(200) = '', @Cat3Label varchar(200) = '', @Cat4Label varchar(200) = '', @Cat5Label varchar(200) = ''
	SELECT  @Cat1Label = max(Case When UICultureName = @VisionLang and LabelName = 'revType1Label' Then LabelValue Else '' End),
			@Cat2Label = max(Case When UICultureName = @VisionLang and LabelName = 'revType2Label' and @Categories >= 2 Then LabelValue Else '' End),
			@Cat3Label = max(Case When UICultureName = @VisionLang and LabelName = 'revType3Label' and @Categories >= 3 Then LabelValue Else '' End),
			@Cat4Label = max(Case When UICultureName = @VisionLang and LabelName = 'revType4Label' and @Categories >= 4 Then LabelValue Else '' End),
			@Cat5Label = max(Case When UICultureName = @VisionLang and LabelName = 'revType5Label' and @Categories >= 5 Then LabelValue Else '' End)
		FROM FW_CFGLabelData WHERE LabelName like 'revType%'
	if @Categories = 0 
	begin
		set @Cat1Label = 'Labor'		-- Defaults
		set @Cat2Label = 'Consultant'
		set @Cat3Label = 'Expenses'
	end

	DECLARE @sSQL Nvarchar(max)

	-- Overall percent completes:
	--------------------------------------------------------------
    SET @sSQL = N'
		SELECT ''Y'' as Editable, BT.WBS1, BT.WBS2, BT.WBS3, PR1.Name as WBS1Name, IsNull(PR2.Name, '''') as WBS2Name, IsNull(PR3.Name, '''') as WBS3Name,
			'''' as BTFwbs1, '''' as BTFwbs2, '''' as BTFwbs3, isnull(BTF.Seq, 0) as BTFSeq, '''' as BillingPhase, '''' as BillingPhaseName, '''' as PhaseGroup, BT.FeeMeth,
			''Y'' as HasCat1, ''Y'' as HasCat2, ''Y'' as HasCat3, ''Y'' as HasCat4, ''Y'' as HasCat5,

			-- FEE
			BT.Fee1 [Fee1], BT.Fee2 [Fee2], BT.Fee3 [Fee3], BT.Fee4 [Fee4], BT.Fee5 [Fee5],

			-- CUR % CPL
			Case When BT.FeeMeth = 1 Then BT.FeePctCpl1 Else null End as [CurFeePctCpl1], Case When BT.FeeMeth = 1 Then BT.FeePctCpl2 Else null End as [CurFeePctCpl2],
			Case When BT.FeeMeth = 1 Then BT.FeePctCpl3 Else null End as [CurFeePctCpl3], Case When BT.FeeMeth = 1 Then BT.FeePctCpl4 Else null End as [CurFeePctCpl4],
			Case When BT.FeeMeth = 1 Then BT.FeePctCpl5 Else null End as [CurFeePctCpl5],

			-- CUR FEE TO DATE
			convert(decimal(19, 4), BT.FeeToDate1) as [CurFeeToDate1], convert(decimal(19, 4), BT.FeeToDate2) as [CurFeeToDate2],
			convert(decimal(19, 4), BT.FeeToDate3) as [CurFeeToDate3], convert(decimal(19, 4), BT.FeeToDate4) as [CurFeeToDate4],
			convert(decimal(19, 4), BT.FeeToDate5) as [CurFeeToDate5],

			-- NEW % CPL
			Case When BT.FeeMeth = 1 Then fpc.FeePctCpl1 ELSE NULL End as [NewFeePctCpl1], Case When BT.FeeMeth = 1 Then fpc.FeePctCpl2 ELSE NULL End as [NewFeePctCpl2],
			Case When BT.FeeMeth = 1 Then fpc.FeePctCpl3 ELSE NULL End as [NewFeePctCpl3], Case When BT.FeeMeth = 1 Then fpc.FeePctCpl4 ELSE NULL End as [NewFeePctCpl4],
			Case When BT.FeeMeth = 1 Then fpc.FeePctCpl5 ELSE NULL End as [NewFeePctCpl5],

			-- NEW FEE TO DATE
			convert(decimal(19, 4), fpc.FeeToDate1) as [NewFeeToDate1], convert(decimal(19, 4), fpc.FeeToDate2) as [NewFeeToDate2],
			convert(decimal(19, 4), fpc.FeeToDate3) as [NewFeeToDate3], convert(decimal(19, 4), fpc.FeeToDate4) as [NewFeeToDate4],
			convert(decimal(19, 4), fpc.FeeToDate5) as [NewFeeToDate5]
		FROM BT
			INNER JOIN PR on PR.wbs1 = BT.wbs1 and PR.wbs2 = BT.wbs2 and PR.wbs3 = BT.wbs3
			INNER JOIN BT BT2 on BT2.wbs1 = BT.wbs1 and BT2.wbs2 = BT.wbs2 and BT2.wbs3 = N'' ''
			INNER JOIN BT BT1 on BT1.wbs1 = BT.wbs1 and BT1.wbs2 = N'' '' and BT1.wbs3 = N'' ''
			INNER JOIN ProjectCustomTabFields PCTF on PCTF.WBS1 = BT.WBS1 and PCTF.WBS2 = N'' '' and PCTF.WBS3 = N'' '' '

	IF @filterBillGrp = 1 SET @sSQL += N'
			LEFT JOIN BTBGSubs on BTBGSubs.SubWBS1 = BT1.WBS1'

	-- Use the filter function only for accounting users and only if viewing more than 1 row at a time:
	IF @filterSQL = 1 AND @isRoleAcct = 1 AND @isSingleProject = 0 SET @sSQL += N'
			INNER JOIN (
				SELECT Distinct wbs1
					FROM dbo.fnCCG_EI_ProjectFilter(''' + @invoiceGroup + N''', ''' + @User + N''')
			) ProjectFilter on ProjectFilter.wbs1 = PR.wbs1 '

	SET @sSQL = @sSQL + N'
			LEFT JOIN PR PR3 on PR3.wbs1 = BT.wbs1 and PR3.WBS2 = BT.wbs2 and PR3.WBS3 = BT.WBS3 and PR3.WBS3 <> N'' ''
			LEFT JOIN PR PR2 on PR2.wbs1 = BT.wbs1 and PR2.WBS2 = BT.wbs2 and PR.WBS2 <> N'' '' and PR2.WBS3 = N'' ''
			LEFT JOIN PR PR1 on PR1.wbs1 = BT.wbs1 and PR1.WBS2 = N'' '' and PR1.WBS3 = N'' ''
			LEFT JOIN CCG_EI_FeePctCpl fpc on fpc.WBS1 = BT.WBS1 and fpc.WBS2 = BT.WBS2 and fpc.WBS3 = BT.WBS3
			LEFT JOIN CCG_EI on CCG_EI.WBS1 = BT.WBS1
			LEFT JOIN BTF on BTF.wbs1 = BT.wbs1 and BTF.wbs2 = BT.wbs2 and BTF.wbs3 = BT.wbs3 and BT.FeeMeth = 2
			LEFT JOIN (
				SELECT StageFlow, Stage, Max(CanModify) as CanModify
					FROM CCG_EI_ConfigRights
					WHERE CanModify = ''Y''
					GROUP BY StageFlow, Stage
			) As rights on rights.StageFlow = PCTF.CustInvoiceStageFlow and rights.Stage = CCG_EI.InvoiceStage
		WHERE BT.FeeMeth in (1, 4, 5) and BT.FeeByDetailEnabled = ''Y'' '

	IF @isRoleAcct = 1 AND @supportBuiltInFeePctCpl <> 'Q' SET @sSQL += N'
			AND PCTF.CustInvoiceGroup = ''' + @invoiceGroup + N''' '
	ELSE IF @isRoleAcct = 1 AND @supportBuiltInFeePctCpl = 'Q' SET @sSQL += N'
			AND BT.WBS1 = N''' + @wbs1List + N''' '
	ELSE IF @supportBuiltInFeePctCpl = 'P' OR @supportBuiltInFeePctCpl = 'Q' OR @isSingleProject = 1 SET @sSQL += N'
			AND BT.WBS1 = N''' + @wbs1List + N''' '			-- PM - Selected project only
	ELSE IF @supportBuiltInFeePctCpl = 'U' SET @sSQL += N'
			AND BT.WBS1 in (''' + @wbs1List + N''') '
	IF @isSingleWBS2 = 1 set @sSQL += '
			AND BT.WBS2 = N'''+@wbs2+''''
	IF @isSingleWBS2 = 1 set @sSQL += '
			AND BT.WBS3 = N'''+@wbs3+''''

	-- 5.2 fix:  If we're not filtering by SQL and we're showing multiple projects, then exclude dormant WBS1s:
	IF @filterSQL = 0 AND @isSingleProject = 0 SET @sSQL += N'
			AND PR1.Status <> ''D'' and PR1.ChargeType = ''R'' '

	SET @sSQL += N'
			AND ((BT.WBS2 = N'' '' and BT.WBS3 = N'' '')
				OR (BT.WBS2 <> N'' '' and BT.WBS3 = N'' ''  and BT1.SubLevelTerms = ''Y'')
				OR (BT.WBS2 <> N'' '' and BT.WBS3 <> N'' '' and BT1.SubLevelTerms = ''Y'' and BT2.SubLevelTerms = ''Y'')
			) '

	IF @filterBillGrp = 1 SET @sSQL = @sSQL + N'
			AND (BTBGSubs.MainWBS1 is null OR BTBGSubs.MainWBS1 = BTBGSubs.SubWBS1) '

	-- Billing phases:
	--------------------------------------------------------------
	SET @sSQL += N'
		UNION ALL
		SELECT Case When BT.FeeMeth = 2 Then ''N'' else ''Y'' end as Editable, BT.WBS1, BT.WBS2, BT.WBS3,
			PR1.Name as WBS1Name, IsNull(PR2.Name, '''') as WBS2Name, IsNull(PR3.Name, '''') as WBS3Name,
			Case When BT.PostFeesByPhase = ''Y'' and BTF.Postwbs1 is not null Then BTF.Postwbs1 Else BTF.wbs1 End as BTFwbs1,
			Case When BT.PostFeesByPhase = ''Y'' and BTF.Postwbs2 is not null Then BTF.Postwbs2 Else BTF.wbs2 End as BTFwbs2,
			Case When BT.PostFeesByPhase = ''Y'' and BTF.Postwbs3 is not null Then BTF.Postwbs3 Else BTF.wbs3 End as BTFwbs3,
			BTF.Seq as BTFSeq, BTF.Phase as BillingPhase, BTF.Name as BillingPhaseName, pgd.Description as PhaseGroup, BT.FeeMeth,
			Case When BT.FeeMeth = 2 Then ''Y'' else (case when bc1.Seq is null then ''N'' else ''Y'' end) end as HasCat1,
			Case When BT.FeeMeth = 2 Then ''Y'' else (case when bc2.Seq is null then ''N'' else ''Y'' end) end as HasCat2,
			Case When BT.FeeMeth = 2 Then ''Y'' else (case when bc3.Seq is null then ''N'' else ''Y'' end) end as HasCat3,
			Case When BT.FeeMeth = 2 Then ''Y'' else (case when bc4.Seq is null then ''N'' else ''Y'' end) end as HasCat4,
			Case When BT.FeeMeth = 2 Then ''Y'' else (case when bc5.Seq is null then ''N'' else ''Y'' end) end as HasCat5,

			-- FEE
			case when BT.FeeMeth = 2 then BT.Fee1 else bc1.Fee end [Fee1], case when BT.FeeMeth = 2 then BT.Fee2 else bc2.Fee end [Fee2],
			case when BT.FeeMeth = 2 then BT.Fee3 else bc3.Fee end [Fee3], case when BT.FeeMeth = 2 then BT.Fee4 else bc4.Fee end [Fee4],
			case when BT.FeeMeth = 2 then BT.Fee5 else bc5.Fee end [Fee5],

			-- CUR % CPL
			case when BT.FeeMeth = 2 then BTF.PctComplete else bc1.PctComplete end as CurFeePctCpl1,
			case when BT.FeeMeth = 2 then BTF.PctComplete else bc2.PctComplete end as CurFeePctCpl2,
			case when BT.FeeMeth = 2 then BTF.PctComplete else bc3.PctComplete end as CurFeePctCpl3,
			case when BT.FeeMeth = 2 then BTF.PctComplete else bc4.PctComplete end as CurFeePctCpl4,
			case when BT.FeeMeth = 2 then BTF.PctComplete else bc5.PctComplete end as CurFeePctCpl5,

			-- CUR FEE TO DATE
			case when BT.FeeMeth = 2 then BTF.PctComplete / 100.0 * BT.Fee1 * BTF.PctOfFee / 100.0 else bc1.FeeToDate end as CurFeeToDate1,
			case when BT.FeeMeth = 2 then BTF.PctComplete / 100.0 * BT.Fee2 * BTF.PctOfFee / 100.0 else bc2.FeeToDate end as CurFeeToDate2,
			case when BT.FeeMeth = 2 then BTF.PctComplete / 100.0 * BT.Fee3 * BTF.PctOfFee / 100.0 else bc3.FeeToDate end as CurFeeToDate3,
			case when BT.FeeMeth = 2 then BTF.PctComplete / 100.0 * BT.Fee4 * BTF.PctOfFee / 100.0 else bc4.FeeToDate end as CurFeeToDate4,
			case when BT.FeeMeth = 2 then BTF.PctComplete / 100.0 * BT.Fee5 * BTF.PctOfFee / 100.0 else bc5.FeeToDate end as CurFeeToDate5,

			-- NEW % CPL
			case when BT.FeeMeth = 2 then fpc.FeePctCpl else fpc.FeePctCpl1 end as NewFeePctCpl1,
			case when BT.FeeMeth = 2 then fpc.FeePctCpl else fpc.FeePctCpl2 end as NewFeePctCpl2,
			case when BT.FeeMeth = 2 then fpc.FeePctCpl else fpc.FeePctCpl3 end as NewFeePctCpl3,
			case when BT.FeeMeth = 2 then fpc.FeePctCpl else fpc.FeePctCpl4 end as NewFeePctCpl4,
			case when BT.FeeMeth = 2 then fpc.FeePctCpl else fpc.FeePctCpl5 end as NewFeePctCpl5,

			-- NEW FEE TO DATE
			case when BT.FeeMeth = 2 then isnull(fpc.FeePctCpl, BTF.PctComplete) / 100.0 * BT.Fee1 * BTF.PctOfFee / 100.0
				when BT.FeeMeth = 3 then isnull(convert(decimal(19, 4), fpc.FeeToDate1), fpc.FeePctCpl1 / 100.0 * bc1.Fee)
				else convert(decimal(19, 4), fpc.FeeToDate1) end as NewFeeToDate1,
			case when BT.FeeMeth = 2 then isnull(fpc.FeePctCpl, BTF.PctComplete) / 100.0 * BT.Fee2 * BTF.PctOfFee / 100.0
				when BT.FeeMeth = 3 then isnull(convert(decimal(19, 4), fpc.FeeToDate2), fpc.FeePctCpl2 / 100.0 * bc2.Fee)
				else convert(decimal(19, 4), fpc.FeeToDate2) end as NewFeeToDate2,
			case when BT.FeeMeth = 2 then isnull(fpc.FeePctCpl, BTF.PctComplete) / 100.0 * BT.Fee3 * BTF.PctOfFee / 100.0
				when BT.FeeMeth = 3 then isnull(convert(decimal(19, 4), fpc.FeeToDate3), fpc.FeePctCpl3 / 100.0 * bc3.Fee)
				else convert(decimal(19, 4), fpc.FeeToDate3) end as NewFeeToDate3,
			case when BT.FeeMeth = 2 then isnull(fpc.FeePctCpl, BTF.PctComplete) / 100.0 * BT.Fee4 * BTF.PctOfFee / 100.0
				when BT.FeeMeth = 3 then isnull(convert(decimal(19, 4), fpc.FeeToDate4), fpc.FeePctCpl4 / 100.0 * bc4.Fee)
				else convert(decimal(19, 4), fpc.FeeToDate4) end as NewFeeToDate4,
			case when BT.FeeMeth = 2 then isnull(fpc.FeePctCpl, BTF.PctComplete) / 100.0 * BT.Fee5 * BTF.PctOfFee / 100.0
				when BT.FeeMeth = 3 then isnull(convert(decimal(19, 4), fpc.FeeToDate5), fpc.FeePctCpl5 / 100.0 * bc5.Fee)
				else convert(decimal(19, 4), fpc.FeeToDate5) end as NewFeeToDate5
		FROM BT
			INNER JOIN BTF on BTF.wbs1 = BT.wbs1 and BTF.wbs2 = BT.wbs2 and BTF.wbs3 = BT.wbs3
			LEFT JOIN BTFCategory bc1 on bc1.Seq = btf.Seq and bc1.WBS1 = btf.WBS1 and bc1.WBS2 = btf.WBS2 and bc1.WBS3 = btf.WBS3 and bc1.Category = 1
			LEFT JOIN BTFCategory bc2 on bc2.Seq = btf.Seq and bc2.WBS1 = btf.WBS1 and bc2.WBS2 = btf.WBS2 and bc2.WBS3 = btf.WBS3 and bc2.Category = 2
			LEFT JOIN BTFCategory bc3 on bc3.Seq = btf.Seq and bc3.WBS1 = btf.WBS1 and bc3.WBS2 = btf.WBS2 and bc3.WBS3 = btf.WBS3 and bc3.Category = 3
			LEFT JOIN BTFCategory bc4 on bc4.Seq = btf.Seq and bc4.WBS1 = btf.WBS1 and bc4.WBS2 = btf.WBS2 and bc4.WBS3 = btf.WBS3 and bc4.Category = 4
			LEFT JOIN BTFCategory bc5 on bc5.Seq = btf.Seq and bc5.WBS1 = btf.WBS1 and bc5.WBS2 = btf.WBS2 and bc5.WBS3 = btf.WBS3 and bc5.Category = 5
			LEFT JOIN BTFPhaseGroupDescriptions pgd on pgd.WBS1 = BTF.WBS1 and pgd.WBS2 = BTF.WBS2 and pgd.WBS3 = BTF.WBS3
				and pgd.PhaseGroup = BTF.PhaseGroup and pgd.UICultureName = ''' + @VisionLang + N'''
			INNER JOIN PR on PR.wbs1 = BT.wbs1 and PR.wbs2 = BT.wbs2 and PR.wbs3 = BT.wbs3
			INNER JOIN BT BT2 on BT2.wbs1 = BT.wbs1 and BT2.wbs2 = BT.wbs2 and BT2.wbs3 = N'' ''
			INNER JOIN BT BT1 on BT1.wbs1 = BT.wbs1 and BT1.wbs2 = N'' '' and BT1.wbs3 = N'' ''
			INNER JOIN ProjectCustomTabFields PCTF on PCTF.WBS1 = BT.WBS1 and PCTF.WBS2 = N'' '' and PCTF.WBS3 = N'' '' '

	IF @filterBillGrp = 1 SET @sSQL += N'
			LEFT JOIN BTBGSubs on BTBGSubs.SubWBS1 = BT1.WBS1'

	-- Use the filter function only for accounting users and only if viewing more than 1 row at a time:
	-- EI 5.2 - defect found by GHT - missing this piece here but not in above lump sum SQL
	IF @filterSQL = 1 AND @isRoleAcct = 1 AND @isSingleProject = 0 SET @sSQL += N'
			INNER JOIN (
				SELECT Distinct wbs1
					FROM dbo.fnCCG_EI_ProjectFilter(''' + @invoiceGroup + N''', ''' + @User + N''')
			) ProjectFilter on ProjectFilter.wbs1 = PR.wbs1 '

	SET @sSQL += N'
			LEFT JOIN PR PR3 on PR3.wbs1 = BT.wbs1 and PR3.wbs2 = BT.wbs2 and PR3.wbs3 = BT.wbs3 and PR3.wbs3 <> N'' ''
			LEFT JOIN PR PR2 on PR2.wbs1 = BT.wbs1 and PR2.wbs2 = BT.wbs2 and PR.wbs2 <> N'' '' and PR2.wbs3 = N'' ''
			LEFT JOIN PR PR1 on PR1.wbs1 = BT.wbs1 and PR1.wbs2 = N'' '' and PR1.wbs3 = N'' ''
			LEFT JOIN CCG_EI_FeePctCpl fpc
				On fpc.WBS1 = BTF.WBS1 and fpc.WBS2 = BTF.WBS2 and fpc.WBS3 = BTF.WBS3 and fpc.Seq = BTF.Seq
			LEFT JOIN CCG_EI on CCG_EI.WBS1 = BT.WBS1
			LEFT JOIN (
				SELECT StageFlow, Stage, Max(CanModify) as CanModify
					FROM CCG_EI_ConfigRights
					WHERE CanModify = ''Y''
					GROUP BY StageFlow, Stage
			) As rights on rights.StageFlow = PCTF.CustInvoiceStageFlow and rights.Stage = CCG_EI.InvoiceStage '

	IF @isRoleAcct = 1 AND @supportBuiltInFeePctCpl <> 'Q' SET @sSQL += N'
		WHERE PCTF.CustInvoiceGroup = ''' + @invoiceGroup + N''' '
	ELSE IF @isRoleAcct = 1 AND @supportBuiltInFeePctCpl = 'Q' SET @sSQL += N'
		WHERE BT.WBS1 = N''' + @wbs1List + N''' '
	ELSE IF @supportBuiltInFeePctCpl = 'P' OR @supportBuiltInFeePctCpl = 'Q' OR @isSingleProject = 1 SET @sSQL += N'
		WHERE BT.WBS1 = N''' + @wbs1List + N''' '			-- Selected project only
	ELSE SET @sSQL += N'
		WHERE BT.WBS1 IN (''' + @wbs1List + N''') '
	IF @isSingleWBS2 = 1 set @sSQL += '
			AND BT.WBS2 = N'''+@wbs2+''''
	IF @isSingleWBS2 = 1 set @sSQL += '
			AND BT.WBS3 = N'''+@wbs3+''''
	IF @isSingleWBS2 = 1 set @sSQL += '
			AND BTF.Seq = '+cast(@seq as varchar(10))

	-- 5.2 fix:  If we're not filtering by SQL and we're showing multiple projects, then exclude dormant WBS1s:
	IF @filterSQL = 0 AND @isSingleProject = 0 SET @sSQL += N'
			AND PR1.Status <> ''D'' AND PR1.ChargeType = ''R'' '

	SET @sSQL += N'
			and BT.FeeByDetailEnabled = ''Y'' AND (PR2.WBS2 = N'' '' OR IsNull(PR2.Status, ''A'') <> ''D'' OR ''' + @includeDormant + N''' = ''Y'')
			AND BT.FeeMeth IN (2, 3)
			AND ((BT.WBS2 = N'' '' and BT.WBS3 = N'' '')
				OR (BT.WBS2 <> N'' '' AND BT.WBS3 = N'' '' AND BT1.SubLevelTerms = ''Y'')
				OR (BT.WBS2 <> N'' '' AND BT.WBS3 <> N'' '' AND BT1.SubLevelTerms = ''Y'' AND BT2.SubLevelTerms = ''Y'')
			) '

	IF @filterBillGrp = 1 SET @sSQL += N'
			AND (BTBGSubs.MainWBS1 is null OR BTBGSubs.MainWBS1 = BTBGSubs.SubWBS1) '

	SET @sSQL += N'
		--ORDER BY BT.WBS1, BT.WBS2, BT.WBS3, BillingPhase '

	SET @sSQL = '
	select x.Editable, x.WBS1, x.WBS2, x.WBS3, x.BTFSeq, x.BillingPhaseName, v.Category, v.CategoryName,
			(case v.Category when 1 then Fee1          when 2 then Fee2          when 3 then Fee3          when 4 then Fee4          when 5 then Fee5          end) as Fee,
			(case v.Category when 1 then CurFeePctCpl1 when 2 then CurFeePctCpl2 when 3 then CurFeePctCpl3 when 4 then CurFeePctCpl4 when 5 then CurFeePctCpl5 end) as CurFeePctCpl,
			(case v.Category when 1 then CurFeeToDate1 when 2 then CurFeeToDate2 when 3 then CurFeeToDate3 when 4 then CurFeeToDate4 when 5 then CurFeeToDate5 end) as CurFeeToDate,
			(case v.Category when 1 then isnull(NewFeePctCpl1, CurFeePctCpl1) when 2 then isnull(NewFeePctCpl2, CurFeePctCpl2) when 3 then isnull(NewFeePctCpl3, CurFeePctCpl3)
							 when 4 then isnull(NewFeePctCpl4, CurFeePctCpl4) when 5 then isnull(NewFeePctCpl5, CurFeePctCpl5) end) as NewFeePctCpl,
			(case v.Category when 1 then isnull(NewFeeToDate1, isnull(NewFeePctCpl1, CurFeePctCpl1) * Fee1 / 100.0)
							 when 2 then isnull(NewFeeToDate2, isnull(NewFeePctCpl2, CurFeePctCpl2) * Fee2 / 100.0)
							 when 3 then isnull(NewFeeToDate3, isnull(NewFeePctCpl3, CurFeePctCpl3) * Fee3 / 100.0)
							 when 4 then isnull(NewFeeToDate4, isnull(NewFeePctCpl4, CurFeePctCpl4) * Fee4 / 100.0)
							 when 5 then isnull(NewFeeToDate5, isnull(NewFeePctCpl5, CurFeePctCpl5) * Fee5 / 100.0) end) as NewFeeToDate,
			(case v.Category when 1 then isnull(NewFeeToDate1, isnull(NewFeePctCpl1, CurFeePctCpl1) * Fee1 / 100.0) - CurFeeToDate1
						     when 2 then isnull(NewFeeToDate2, isnull(NewFeePctCpl2, CurFeePctCpl2) * Fee2 / 100.0) - CurFeeToDate2
							 when 3 then isnull(NewFeeToDate3, isnull(NewFeePctCpl3, CurFeePctCpl3) * Fee3 / 100.0) - CurFeeToDate3
							 when 4 then isnull(NewFeeToDate4, isnull(NewFeePctCpl4, CurFeePctCpl4) * Fee4 / 100.0) - CurFeeToDate4
							 when 5 then isnull(NewFeeToDate5, isnull(NewFeePctCpl5, CurFeePctCpl5) * Fee5 / 100.0) - CurFeeToDate5
				end) as CurBilling
		from (
			' + @sSQL + '
		) x
		outer apply (values (1, '''+Replace(@Cat1label, '''', '''''')+'''), (2, '''+Replace(@Cat2label, '''', '''''')+'''), (3, '''+Replace(@Cat3label, '''', '''''')+'''),
			(4, '''+Replace(@Cat4label, '''', '''''')+'''), (5, '''+Replace(@Cat5label, '''', '''''')+''')
		) v (Category, CategoryName)
		WHERE v.CategoryName <> '''' and ((x.HasCat1 = ''Y'' and v.Category = 1) or (x.HasCat2 = ''Y'' and v.Category = 2)
			 or (x.HasCat3 = ''Y'' and v.Category = 3) or (x.HasCat4 = ''Y'' and v.Category = 4) or (x.HasCat5 = ''Y'' and v.Category = 5))
		ORDER BY x.WBS1, x.WBS2, x.WBS3, x.BillingPhaseName
	'

	--select @sSQL
	EXEC (@sSQL)
END
GO
