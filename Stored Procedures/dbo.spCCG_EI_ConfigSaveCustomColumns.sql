SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_ConfigSaveCustomColumns]
	@fieldsSQL			varchar(max),
	@updateValuesSql	varchar(max),
	@insertValuesSql	varchar(max),
	@deleteSeqs			varchar(max),
	@VISION_LANGUAGE	varchar(10),
	@VerifyCCParams		varchar(1)
AS
BEGIN
	/*
	[spCCG_EI_ConfigSaveCustomColumns]
		@fieldsSQL = 'Label, DataType, DisplayOrder, InitialValue, DatabaseField, ColumnUpdate, Width, Decimals, ViewRoles, EditRoles, RequiredRoles, RequiredExpression, Description, Link, HighlightColor, EnabledWBS1, EnabledWBS2, EnabledWBS3, Status, ModDate, ModUser',
		@updateValuesSql = '(''Acct Notes'', ''A'', 1, '''', ''AcctNotes'', '''', 0, 0, ''Accounting'', '''', '''', '''', ''Input column simply for biller notes'', '''', ''0'', ''Y'', ''N'', ''N'', ''A'', GetDate(), ''ADMIN'', 291), (''New\nMsgs'', ''N'', 3, ''dbo.fnCCG_EI_NewMessages'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''N'', ''N'', ''A'', GetDate(), ''ADMIN'', 465), (''Draft\nDate'', ''D'', 3, ''dbo.fnCCG_EI_DraftDate'', '''', '''', 0, 0, ''Accounting'', '''', '''', '''', '''', '''', ''0'', ''Y'', ''N'', ''N'', ''A'', GetDate(), ''ADMIN'', 442), (''Stage\nHistory'', ''L'', 5, ''dbo.fnCCG_EI_StageHistoryLink'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''N'', ''N'', ''I'', GetDate(), ''ADMIN'', 443), (''Backlog'', ''L'', 6, ''dbo.fnCCG_EI_BacklogLink'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''N'', ''N'', ''I'', GetDate(), ''ADMIN'', 464), (''Msg'', ''N'', 10, ''dbo.fnCCG_EI_Msg'', '''', '''', 0, 0, '''', '''', '''', '''', ''tt'', '''', ''0'', ''Y'', ''N'', ''N'', ''I'', GetDate(), ''ADMIN'', 444), (''BT Descr'', ''L'', 12, ''dbo.fnCCG_EI_BTDescrLink'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 458), (''Budget'', ''C'', 15, ''dbo.fnCCG_EI_Budget'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 446), (''JTD\nBilled'', ''C'', 20, ''dbo.fnCCG_EI_JTDBilled'', '''', '''', 0, 0, '''', '''', '''', '''', '''', ''dbo.fnCCG_EI_JTDBilledLink'', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 447), (''JTD\nSpent'', ''C'', 25, ''dbo.fnCCG_EI_JTDSpentBilling'', '''', '''', 0, 0, '''', '''', '''', '''', '''', ''dbo.fnCCG_EI_JTDSpentBillingLink'', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 457), (''JTD\nSpent %'', ''P'', 30, ''dbo.fnCCG_EI_JTDSpentPct'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 449), (''Billing Terms'', ''A'', 50, ''dbo.fnCCG_EI_BillingTerms'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 450), (''Cur Fee % Cpl'', ''N'', 55, ''dbo.fnCCG_EI_FeePctCpl'', '''', '''', 0, 4, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''I'', GetDate(), ''ADMIN'', 386), (''Fee % Cpl'', ''N'', 60, ''dbo.fnCCG_EI_FeePctCpl'', ''FeePctCpl'', ''spCCG_EI_FeePctCpl'', 0, 4, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''I'', GetDate(), ''ADMIN'', 387), (''Draft Amt'', ''C'', 65, ''dbo.fnCCG_EI_DraftInvoiceAmt'', '''', '''', 0, 2, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 451), (''AR Over 60'', ''C'', 70, ''dbo.fnCCG_EI_AROver60'', '''', '''', 0, 0, '''', '''', '''', '''', '''', ''dbo.fnCCG_EI_AROver60Link'', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 452), (''Req Exp\nBackup?'', ''B'', 72, ''dbo.fnCCG_EI_RequiresExpenseBackup'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''N'', ''N'', ''A'', GetDate(), ''ADMIN'', 453), (''% Completes'', ''L'', 75, ''dbo.fnCCG_EI_EnterPctCompLink'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 390), (''Hold\nUntil Date'', ''D'', 80, ''dbo.fnCCG_EI_HoldUntilDate'', ''HoldUntilDate'', ''spCCG_EI_HoldUntilDate'', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''N'', ''N'', ''A'', GetDate(), ''ADMIN'', 404), (''Hold Notes'', ''A'', 81, '''', ''HoldNotes'', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''N'', ''N'', ''A'', GetDate(), ''ADMIN'', 405), (''Reason Not\nInvoiced'', ''V'', 82, ''dbo.fnCCG_EI_CustReasonNotInvoiced'', ''CustReasonNotInvoiced'', ''spCCG_EI_CustReasonNotInvoiced'', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''N'', ''N'', ''A'', GetDate(), ''ADMIN'', 463), (''ETC'', ''C'', 89, '''', ''ETC'', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''N'', ''A'', GetDate(), ''ADMIN'', 462), (''Month\n+ 1'', ''C'', 90, '''', ''MonthPlus1'', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''N'', ''I'', GetDate(), ''ADMIN'', 459), (''Month\n+ 2'', ''C'', 91, '''', ''MonthPlus2'', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''N'', ''I'', GetDate(), ''ADMIN'', 460), (''Month\n+ 3'', ''C'', 92, '''', ''MonthPlus3'', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''N'', ''I'', GetDate(), ''ADMIN'', 461), (''BT Description'', ''A'', 99, ''dbo.fnCCG_EI_BTDescr'', ''BTDescr'', ''spCCG_EI_BTDescr'', 0, 0, ''Accounting'', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 305)',
		@insertValuesSql = '(''New THING'', ''A'', 100, ''fnCCG_EI_AROver60Link'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'')',
		@deleteSeqs = '',
		@VISION_LANGUAGE = 'en-US'
	*/
	SET NOCOUNT ON
	declare @sSQL		varchar(max)
	declare @ids		TABLE (id int)
	declare @i			int
	declare @newI		int
	declare @seq		varchar(50)

	BEGIN TRY
	BEGIN TRANSACTION
	-- Custom column updates
	if @updateValuesSql <> '' AND @updateValuesSql IS NOT NULL begin
		set @sSQL = '
			update cc
				set
					[Label]					= temp.[Label],
					[DataType]				= temp.[DataType],
					[DisplayOrder]			= temp.[DisplayOrder],
					[InitialValue]			= temp.[InitialValue],
					[DbColumnName]			= temp.[DbColumnName],
					[DatabaseField]			= temp.[DatabaseField],
					[ColumnUpdate]			= temp.[ColumnUpdate],
					[Width]					= temp.[Width],
					[Decimals]				= temp.[Decimals],
					[ViewRoles]				= temp.[ViewRoles],
					[EditRoles]				= temp.[EditRoles],
					[RequiredRoles]			= temp.[RequiredRoles],
					[RequiredExpression]	= temp.[RequiredExpression],
					[EnabledWBS1]			= temp.[EnabledWBS1],
					[EnabledWBS2]			= temp.[EnabledWBS2],
					[EnabledWBS3]			= temp.[EnabledWBS3],
					[Status]				= temp.[Status],
					[ModDate]				= temp.[ModDate],
					[ModUser]				= temp.[ModUser],
					[Link]					= temp.[Link],
					[Description]			= temp.[Description],
					[HighlightColor]		= temp.[HighlightColor],
					[GroupName]				= temp.[GroupName],
					[QuickLoad]				= temp.[QuickLoad],
					[WhichGrids]			= temp.[WhichGrids]
				from CCG_EI_ConfigCustomColumns cc
				join (
					values ' + @updateValuesSql + '
				) temp (' + @fieldsSQL + ', Seq) ON temp.Seq = cc.Seq'
		--PRINT @sSQL
		exec (@sSQL)
	end

	-- Custom column inserts
	if @insertValuesSql <> '' AND @insertValuesSql IS NOT NULL
	begin
		set @sSQL = '
			insert into CCG_EI_ConfigCustomColumns
				(' + @fieldsSQL + ')
				values
				' + @insertValuesSql
		--PRINT @sSQL
		exec (@sSQL)
	end

	-- Do custom column deletes
	set @i = 0
	while @i >= 0 And @i < Len(@deleteSeqs)
	begin
		set @newI = CHARINDEX('|', @deleteSeqs, @i + 1)
		if @newI <= 0 SET @newI = Len(@deleteSeqs) + 1
		set @seq = SUBSTRING(@deleteSeqs, @i, @newI - @i)

		--PRINT @seq
		delete from CCG_EI_ConfigCustomColumns where Seq = @seq
		delete from CCG_EI_ConfigCustomColumnsDescriptions where Seq = @seq

		set @i = @newI + 1
	end

	-- Update the multilingual descriptions table
	set @sSQL = '
		MERGE CCG_EI_ConfigCustomColumnsDescriptions
			with (HOLDLOCK) AS Target
		using CCG_EI_ConfigCustomColumns AS Source
			on Target.Seq = Source.Seq and Target.UICultureName = '''+@VISION_LANGUAGE+'''
		when matched then
			update set Target.Label = Source.Label, Target.GroupName = Source.GroupName, Target.Description = Source.Description
		when not matched by target then
			insert (Seq, UICultureName, Label, GroupName, Description)
				values (Source.Seq, '''+@VISION_LANGUAGE+''', Source.Label, Source.GroupName, Source.Description);'				-- semicolin is necessary for MERGE statement

	exec (@sSQL)
	delete from CCG_EI_SQLPartsCache where [Type] like 'CC_Fields_%'
	update CCG_EI_Config set VerifyCCParams = @VerifyCCParams

	COMMIT TRANSACTION
	select 0 as ErrorNumber, '' as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		select ERROR_NUMBER() as ErrorNumber, isnull(error_message(), 'Unknown error') + ' (' + cast(isnull(error_message(), -1) as varchar(20)) + ')' as ErrorMessage
	END CATCH
END
GO
