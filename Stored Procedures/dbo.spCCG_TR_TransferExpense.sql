SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_TR_TransferExpense](@Username varchar(32), @Vendor varchar(32), @OrigTable varchar(12), @OrigAccount varchar(32),
	@OrigPeriod varchar(6), @OrigPostSeq varchar(32), @OrigPKey varchar(32), 
	@NewPeriod  varchar(6), @NewPostSeq  varchar(32), @NewPKey  varchar(32),
	@OrigWBS1 varchar(30), @OrigWBS2 varchar(7), @OrigWBS3 varchar(7),
	@NewWBS1  varchar(30), @NewWBS2  varchar(7), @NewWBS3  varchar(7),
	@OrigAmount decimal(19,4), @NewAmount  decimal(19,4),
	@NewBillExt decimal(19,4),  
	@ApprovalComment varchar(255), @AutoApprove char(1) , @FNStatus varchar(2))
AS BEGIN

	BEGIN TRY
		declare @origOrg varchar(32), @newOrg varchar(32)
		declare @partial int, @sql varchar(max), @origTableWhereClause varchar(255), @lineQuery varchar(512),@lineQuery2 varchar(512)
		declare @commonColumns varchar(2000), @nonCommonColumns varchar(1000), @sqlAmts varchar(255)
		declare @mult varchar(50), @emp varchar(32), @maxLine int
		select @emp=Employee from SEUser where Username=@Username

		if @NewAmount=0 return
		if Upper(@OrigAccount)<>'NULL' set @OrigAccount='''' + @OrigAccount + ''''	

		if @NewPKey = ''
			set @NewPKey = Left(Replace(Convert(varchar(255), newid()),'-',''),31)

		select @newOrg=Org  from PR where WBS1=@NewWBS1 and WBS2=@NewWBS2 and WBS3=@NewWBS3
		select @origOrg=Org from PR where WBS1=@OrigWBS1 and WBS2=@OrigWBS2 and WBS3=@OrigWBS3
		
		
		declare @Multicompany varchar(32), @Multicurrency varchar(32)
		select @Multicompany=MulticompanyEnabled, @Multicurrency=MulticurrencyEnabled from CFGSystem
		if @Multicompany='Y'
		begin
			declare @CompLength int
			select @CompLength=Org1Length from CFGFormat
			if Left(@newOrg,@CompLength) <> Left(@origOrg,@CompLength)
			begin
				RAISERROR(N'Inter-company transfers are not allowed.', 16, 1)
				return
			end
		end
		if @Multicurrency='Y'
		begin
			declare @newProjectCurrency varchar(32), @origProjectCurrency varchar(32)
			declare @newBillingCurrency varchar(32), @origBillingCurrency varchar(32)
			select @newProjectCurrency =ProjectCurrencyCode, @newBillingCurrency =BillingCurrencyCode from PR where WBS1=@NewWBS1  and WBS2=' ' and WBS3=' '
			select @origProjectCurrency=ProjectCurrencyCode, @origBillingCurrency=BillingCurrencyCode from PR where WBS1=@OrigWBS1 and WBS2=' ' and WBS3=' '
			if @newProjectCurrency <> @origProjectCurrency
			begin
				RAISERROR(N'Transfers between project currencies are not allowed.', 16, 1)
				return
			end
			if @newBillingCurrency <> @origBillingCurrency
			begin
				RAISERROR(N'Transfers between billing currencies are not allowed.', 16, 1)
				return
			end
		end

		
		if @OrigTable='BIED' and @OrigPostSeq=0
			set @NewPostSeq=0

		
		set @nonCommonColumns = 'Period,PostSeq,PKey,WBS1,WBS2,WBS3,XferWBS1,XferWBS2,XferWBS3,XferAccount,Org,' +
			'BillStatus,Amount,BillExt,' +
			'TransactionAmount,AmountProjectCurrency,AmountBillingCurrency,AmountSourceCurrency,ModUser,Line,'
		select @commonColumns = STUFF((
			SELECT ',' + c.name
			FROM sys.columns c
			WHERE c.object_id = OBJECT_ID('BIED') and c.name in (select cols.name from sys.columns cols where cols.object_id=OBJECT_ID('LedgerAP'))
				and charindex(','+c.name+',', ','+@nonCommonColumns) < 1
			FOR XML PATH('')), 1, 1, '')

		set @origTableWhereClause     = ' where Period=' + @OrigPeriod + ' and PostSeq=' + @OrigPostSeq + ' and PKey=''' + @OrigPkey + ''' '
		
		set @lineQuery = 'IsNull((select Max(Case When Line=0 Then 0 Else Line+1 End) from ' + @OrigTable + 
			' as T where (T.Vendor is null and T.Voucher=Orig.Voucher) or (T.Vendor=Orig.Vendor and T.Voucher=Orig.Voucher)),Line), '
		set @lineQuery2 = 'IsNull((select Max(Case When Line=0 Then 0 Else Line+2 End) from ' + @OrigTable + 
			' as T where (T.Vendor is null and T.Voucher=Orig.Voucher) or (T.Vendor=Orig.Vendor and T.Voucher=Orig.Voucher)),Line), '
		
		if @OrigAmount=@NewAmount
			set @mult = '1'
		else 
			set @mult = Cast(@NewAmount as varchar) + ' / ' + Cast(@OrigAmount as varchar)

		
		declare @OrigPostedTable varchar(32)
		set @OrigPostedTable=@OrigTable
		if @OrigTable='BIED' and @OrigPostSeq='0' 
			set @OrigPostedTable='BIED'
		else if @OrigTable='BIED'
		begin
			declare @TransType varchar(32)
			select @TransType=TransType from BIED where Period=@OrigPeriod and PostSeq=@OrigPostSeq and PKey=@OrigPKey
			if @TransType='AP'
				set @OrigPostedTable='LedgerAP'
			else if @TransType='EX'
				set @OrigPostedTable='LedgerEX'
			else if @TransType='JE'
				set @OrigPostedTable='LedgerMisc'
		end

		
		set @sql = 'if not exists (select ''x'' from ' + @OrigTable + @origTableWhereClause + ' and BillStatus<>''T'') ' +
			' RAISERROR(N''The record has already been transferred or modified.'', 16, 1) '

		set @sql = @sql +
			' insert into ' + @OrigPostedTable + ' (' + @nonCommonColumns + @commonColumns + ') select ' + @NewPeriod + ',' + @NewPostSeq + ',''' + @NewPKey + 'a'',' +
				'WBS1,WBS2,WBS3,XferWBS1,XferWBS2,XferWBS3,XferAccount,Org,''T'',' + 
				Cast(-@NewAmount as varchar) + 
				', Round(-BillExt * ' + @mult + ',2)' +
				', -TransactionAmount * ' + @mult + ', -AmountProjectCurrency * ' + @mult + ', -AmountBillingCurrency * ' + @mult + ', -AmountSourceCurrency * ' + @mult +
				',''' + @Username + ''',' + @lineQuery + @commonColumns +
			' from ' + @OrigTable + ' as Orig ' + @origTableWhereClause
		set @sql = @sql +
			' insert into ' + @OrigPostedTable + ' (' + @nonCommonColumns + @commonColumns + ') select ' + @NewPeriod + ',' + @NewPostSeq + ',''' + @NewPKey + 'b'',' +
				'''' + @NewWBS1 + ''',''' + @NewWBS2 + ''',''' + @NewWBS3 + ''',''' + @OrigWBS1 + ''',''' + @OrigWBS2 + ''',''' + @OrigWBS3 + ''',' + @OrigAccount + ',''' + @NewOrg + ''',BillStatus,' + 
				Cast(@NewAmount as varchar) + 
				', Round(' + Cast(@NewBillExt as varchar) + ',2)' +
				', TransactionAmount * ' + @mult + ', AmountProjectCurrency * ' + @mult + ', AmountBillingCurrency * ' + @mult + ', AmountSourceCurrency * ' + @mult +
				',''' + @Username + ''',' + @lineQuery2 + @commonColumns +
			' from ' + @OrigTable + ' as Orig ' + @origTableWhereClause
		
		if @OrigAmount <> @NewAmount
		begin
			
			set @sql = @sql +
				' insert into BIED (' + @nonCommonColumns + @commonColumns + ') select Period,PostSeq,''' + @NewPKey + 'c''' +
					',WBS1,WBS2,WBS3,XferWBS1,XferWBS2,XferWBS3,XferAccount,Org,BillStatus,' + 
					Cast(@OrigAmount - @NewAmount as varchar) +
					', Round(BillExt - (BillExt * ' + @mult + '),2)' +
					', TransactionAmount     - (TransactionAmount      * ' + @mult + ')' +
					', AmountProjectCurrency - (AmountProjectCurrency  * ' + @mult + ')' +
					', AmountBillingCurrency - (AmountBillingCurrency  * ' + @mult + ')' +
					', AmountSourceCurrency  - (AmountSourceCurrency   * ' + @mult + ')' +
					',''' + @Username + ''', Line, ' + @commonColumns +
				' from ' + @OrigTable + @origTableWhereClause
		end

		
		if @OrigTable='BIED' and @OrigPostSeq<>0
			set @sql = @sql + ' delete from BIED ' + @origTableWhereClause
		else
		begin
			if exists (select 'x' from syscolumns a,sysobjects b where a.id = b.id and a.name = 'TransferredPeriod' and b.name = @OrigTable)
				set @sql = @sql + ' update ' + @OrigTable + ' set TransferredPeriod=' + @NewPeriod + ', TransferredBillStatus=BillStatus ' +  @origTableWhereClause
			set @sql = @sql + ' update ' + @OrigTable + ' set BillStatus=''T'' ' +  @origTableWhereClause
		end

		print @sql
		execute(@sql)

		if @Vendor is not null
		begin
			
			if not exists (select 'x' from VEProjectAssoc where WBS1=@NewWBS1 and WBS2=@NewWBS2 and WBS3=@NewWBS3 and Vendor=@Vendor)
				insert into VEProjectAssoc (WBS1, WBS2, WBS3, Vendor, Role, TeamStatus, CreateUser, CreateDate, ModUser, ModDate)
				values (@NewWBS1, @NewWBS2, @NewWBS3, @Vendor, '', 'Active', @Username, getutcdate(), @Username, getutcdate())
			
			if @NewWBS3<>' '
			if not exists (select 'x' from VEProjectAssoc where WBS1=@NewWBS1 and WBS2=@NewWBS2 and WBS3=' ' and Vendor=@Vendor)
				insert into VEProjectAssoc (WBS1, WBS2, WBS3, Vendor, Role, TeamStatus, CreateUser, CreateDate, ModUser, ModDate)
				values (@NewWBS1, @NewWBS2, ' ', @Vendor, '', 'Active', @Username, getutcdate(), @Username, getutcdate())
			
			if @NewWBS2<>' '
			if not exists (select 'x' from VEProjectAssoc where WBS1=@NewWBS1 and WBS2=' ' and WBS3=' ' and Vendor=@Vendor)
				insert into VEProjectAssoc (WBS1, WBS2, WBS3, Vendor, Role, TeamStatus, CreateUser, CreateDate, ModUser, ModDate)
				values (@NewWBS1, ' ', ' ', @Vendor, '', 'Active', @Username, getutcdate(), @Username, getutcdate())
		end
		
		
		
		set @sql = ''
		if @AutoApprove='Y' or (@AutoApprove='P' and @NewWBS1=@OrigWBS1)
			set @sql = 'exec spCCG_TR_InsertApproval ''' + @OrigPostedTable + ''',' + Cast(@NewPeriod as varchar) + ',' + 
				Cast(@NewPostSeq as varchar) + ',''' + @NewPKey + 'b'',''' + @emp + ''',''' + @emp + ''',''' + IsNull(@ApprovalComment,'') + ''',null,null,''' + @FNStatus + ''' '

		if @OrigAmount <> @NewAmount
			set @sql = @sql + 
				' ; exec spCCG_TR_InsertApproval ''' + 'BIED' + ''',' + Cast(@OrigPeriod as varchar) + ',' + 
					Cast(@OrigPostSeq as varchar) + ',''' + @NewPKey + 'c'',''' + @emp + ''',''' + @emp + ''',''' + IsNull(@ApprovalComment,'') + ''',null,null,''' + @FNStatus + ''' '

		
		if @OrigAmount <> @NewAmount
			set @sql = @sql +
				' ; insert into CCG_TR_History (OriginalTable, OriginalPeriod, OriginalPostSeq, OriginalPKey, NewTable, NewPeriod, NewPostSeq, NewPKey, ActionStatus, ActionTaken, ActionDetail, ActionDate, ActionTakenBy, WBS1, WBS2, WBS3, OldValue, NewValue, OldValueDecimal, NewValueDecimal, ModificationComment, ApprovalComment, DateLastNotificationSent) ' +
				' select ''BILD'', OriginalPeriod, OriginalPostSeq, ''' + @NewPKey + 'c'', NewTable, NewPeriod, NewPostSeq, NewPKey, ActionStatus, ActionTaken, ActionDetail, ActionDate, ActionTakenBy, WBS1, WBS2, WBS3, OldValue, NewValue, OldValueDecimal, NewValueDecimal, ModificationComment, ApprovalComment, DateLastNotificationSent ' +
					' from CCG_TR_History where OriginalTable=''' + @OrigTable + ''' and OriginalPeriod=' + @OrigPeriod + ' and OriginalPostSeq=' + @OrigPostSeq + ' and OriginalPKey=''' + @OrigPkey + ''' '
		
		set @sql = @sql +
			' ; update CCG_TR_History set OriginalTable=''' + @OrigPostedTable + ''', OriginalPeriod=' + Cast(@NewPeriod as varchar) + ', OriginalPostSeq=' +
				Cast(@NewPostSeq as varchar) + ', OriginalPKey=''' + @NewPKey + 'b'' ' +
				' where OriginalTable=''' + @OrigTable + ''' and OriginalPeriod=' + @OrigPeriod + ' and OriginalPostSeq=' + @OrigPostSeq + ' and OriginalPKey=''' + @OrigPkey + ''' '

		print @sql
		execute(@sql)
	END TRY
	BEGIN CATCH
		declare @ErrorMessage nvarchar(4000), @ErrorSeverity int, @ErrorState int
		select @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
		-- Use RAISERROR inside the CATCH block to return error information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
	END CATCH
END
GO
