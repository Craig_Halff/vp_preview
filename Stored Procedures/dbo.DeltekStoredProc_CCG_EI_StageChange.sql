SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_StageChange] ( @WBS1 varchar(30), @OldStage varchar(70), @NewStage varchar(70), @Username varchar(255), @Role varchar(255))
             AS EXEC spCCG_EI_StageChange @WBS1,@OldStage,@NewStage,@Username,@Role
GO
