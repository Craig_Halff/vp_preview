SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[spCCG_EI_PopWin_JTDUnbilledBillingDetailsByAcct] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7), @Key varchar(32), @AcctName varchar(200), @UserName varchar(32))
AS BEGIN
/*
	Copyright (c) 2018 Elevia Software.  All rights reserved.
*/
	declare @ThruPeriod int, @ThruDate datetime

	select @ThruPeriod=ThruPeriod, @ThruDate=ThruDate
		from CCG_EI_ConfigInvoiceGroups cig
			inner join ProjectCustomTabFields pctf on pctf.custInvoiceGroup=cig.InvoiceGroup
		where pctf.WBS1=@WBS1 and pctf.WBS2=' '

	if @ThruPeriod is null	set @ThruPeriod=999999
	if @ThruDate is null	set @ThruDate='12/31/2050'

	declare @T TABLE (TopOrder	int, Row_Detail varchar(3000), Descr varchar(3000), Value1 datetime, Value2 money, Value3 varchar(3000))
	insert into @T (TopOrder, Descr, Value1, Value2, Value3)
		select 2 as TopOrder, ISNULL(NameDesc,''), TransDate, BillExt, Descr
			from (
				select Account, TransDate, Case When TransType='UN' Then UN.Name Else VE.Name End as NameDesc, Amount, BillExt, BillStatus, IsNull(Desc1,'') + ' / ' + IsNull(Desc2,'') as Descr 
				from LedgerMisc L left join VE on VE.Vendor = L.Vendor 
					left join UN on UN.Unit=L.Unit and UN.UnitTable=L.UnitTable
				where L.Account=@Key and WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillExt <> 0 and BillStatus in ('B','H')   
					and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
				UNION ALL
				select Account, TransDate, VE.Name as NameDesc, Amount, BillExt, BillStatus, IsNull(Desc1,'') + ' / ' + IsNull(Desc2,'') as Descr 
				from LedgerAP L left join VE on VE.Vendor = L.Vendor
				where L.Account=@Key and WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillExt <> 0 and BillStatus in ('B','H')    
					and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
				UNION ALL
				select Account, TransDate, Desc1 as NameDesc, Amount, BillExt, BillStatus, IsNull(Desc1,'') + ' / ' + IsNull(Desc2,'') as Descr 
				from LedgerEX L 
				where L.Account=@Key and WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillExt <> 0 and BillStatus in ('B','H')   
					and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
				UNION ALL
				select Account, TransDate, VE.Name as NameDesc, Amount, BillExt, BillStatus, IsNull(Desc1,'') + ' / ' + IsNull(Desc2,'') as Descr 
				from LedgerAR L left join VE on VE.Vendor = L.Vendor
				where L.Account=@Key and WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and BillExt <> 0 and BillStatus in ('B','H') 
					and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
			) as Expenses left join CA on CA.Account=Expenses.Account 

	
	-- Add grand total line:
	insert into @T (TopOrder, Row_Detail, Descr, Value2)
		select 90 as TopOrder, 'BackgroundColor=#888888;ForegroundColor=#ffffff;Bold=Y', 'TOTAL',sum(Value2) from @T where TopOrder in (2)
	
	-- Final recordsets
	select '', '', 'D;Align=Center', 'C2;NS', 'A;ML',   'JTD Unbilled by Account: '+ @AcctName +'  '+ @WBS1 + case when @WBS2 <> '' then +' - ' + @WBS2 else '' end + case when @WBS3 <> '' then +' - ' + @WBS3 else '' end 
	select Descr as [Vendor], Row_Detail, Value1 as [Date], Value2 as [Amount], Value3 as [Description] from @T
END
GO
