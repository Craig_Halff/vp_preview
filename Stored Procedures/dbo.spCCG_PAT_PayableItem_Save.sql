SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[spCCG_PAT_PayableItem_Save]
	@Seq				int,
	@VendorID			Nvarchar(max),
	@Company			Nvarchar(max),
	@ParentSeqValue		varchar(max),
	@Group				Nvarchar(max),
	@ItemNumber			Nvarchar(max),
	@PONumber			Nvarchar(max),
	@POPKey				varchar(max),
	@CheckNumber		Nvarchar(max),
	@Voucher			Nvarchar(max),
	@ItemDate			varchar(max),
	@TransDate			varchar(max),
	@FileName			Nvarchar(max),
	@GLAccount			Nvarchar(max),
	@ControlAmt			Nvarchar(max),
	@recurSeq			varchar(max),
	@LiabCode			Nvarchar(max),
	@BankCode			Nvarchar(max),
	@PayTerms			Nvarchar(max),
	@PayDate			varchar(max),
	@Address			Nvarchar(max),
	@storageSeq			varchar(max),
	@storagePathOrID	varchar(max),
	@ApprovedDetail		Nvarchar(max),
	@EmpId				Nvarchar(max),
	@PayableType		varchar(max),
	@sourceSeqNum		varchar(max),
	@POSeqNum			varchar(max),
	@StageObjectName	varchar(max),
	@SourceType			varchar(max),
	@SourcePKey			varchar(max),
	@SourceBatch		Nvarchar(max),
	@ModUser			Nvarchar(max),	
	@fieldsSql			Nvarchar(max),
	@newValuesSql		Nvarchar(max),
	@pendingInserts		Nvarchar(max),
	@taxFieldsSql		Nvarchar(max) = null,
	@taxValuesSql		Nvarchar(max) = null
AS
BEGIN
	SET NOCOUNT ON;

	Declare @payableSeq int--for inserts it is not passed in
	Declare @StorageChange bit
	declare @ModDate datetime = getutcdate()
	set @StorageChange = 0;--default to no

	BEGIN TRY
	BEGIN TRANSACTION;

		IF @Seq >= 0 BEGIN
			set @payableSeq = @Seq -- so we have a consistant way location to store for inserts or updates
			IF NOT EXISTS(select Seq from CCG_PAT_Payable where Seq = @Seq and StoragePathOrID = @storagePathOrID and StorageSeq = @storageSeq)
			BEGIN
				set @StorageChange = 1;
				update xfdf set xfdf.RevisionSeq = xfdf.RevisionSeq + 1, xfdf.REVID = ISNULL(xfdf.REVID,files.REVID)
				FROM CCG_PAT_XFDF xfdf
				left join (select top 1 PayableSeq,RevID as REVID from CCG_PAT_FILES  where PayableSeq = @Seq and RevisionSeq = 0) files
					on files.PayableSeq = xfdf.PayableSeq
				where xfdf.PayableSeq = @Seq;
				UPDATE CCG_PAT_FILES set RevisionSeq = RevisionSeq + 1,FileState = ISNULL(FileState,'REVISION') where PayableSeq = @Seq;
			END

			UPDATE CCG_PAT_Payable
				SET Vendor = @VendorID, Company = @Company,
					ParentSeq = (CASE
						WHEN LOWER(@ParentSeqValue) = 'null' THEN NULL
						WHEN @ParentSeqValue = '' THEN ParentSeq
						ELSE CAST(@ParentSeqValue AS INT)
					END),
					PayableGroup = NULLIF(@Group, N''), PayableNumber = @ItemNumber,
					PONumber = NULLIF(@PONumber, N''), POPKey = NULLIF(@POPKey, ''), CheckNumber = @CheckNumber,
					Voucher = @Voucher, PayableDate = NULLIF(@ItemDate, ''),
					TransDate = NULLIF(@TransDate, ''), PayableFileName = @FileName, GLAccount = @GLAccount,
					ControlAmount = (CASE WHEN @ControlAmt = N'' THEN NULL ELSE CAST(@ControlAmt AS DECIMAL(19,5)) END),
					RecurSeq = (CASE WHEN @recurSeq = '' THEN NULL ELSE CAST(@recurSeq AS INT) END),
					LiabCode = NULLIF(@LiabCode, N''), BankCode = NULLIF(@BankCode, N''), PayTerms = NULLIF(@PayTerms, N''),
					PayDate = NULLIF(@PayDate, ''), Address = NULLIF(@Address, N''),
					StorageSeq = (CASE WHEN @storageSeq = '' THEN NULL ELSE CAST(@storageSeq AS INT) END),
					StoragePathOrID = NULLIF(@storagePathOrID, ''),
					ApprovedDetail = NULLIF(@ApprovedDetail, N''), ModDate = @ModDate, ModUser = @ModUser
				WHERE Seq = @Seq;
				if @StorageChange = 1 and ISNULL(@storagePathOrID,'') <> ''
				BEGIN


					INSERT INTO CCG_PAT_Files(RevisionSeq,StorageSeq,StoragePathOrID,PayableSeq,FileName,FileState,ModUser,ModDate)
					SELECT 0,StorageSeq,StoragePathOrID,Seq,PayableFileName,null, @ModUser, @ModDate
					 FROM CCG_PAT_Payable where Seq = @Seq  AND ISNULL(StoragePathOrID,'') <> '';
				END
			--SELECT @Seq;
			--History
			INSERT INTO CCG_PAT_History (ActionTaken, ActionDate, ActionTakenBy,PayableSeq,Stage,[Description])
			VALUES ('Modify', @ModDate, @EmpId, @payableSeq,null,null);
		END
		ELSE BEGIN

			INSERT INTO CCG_PAT_Payable
				(Vendor, Company, PayableType,
					ParentSeq,
					PayableGroup, PayableNumber, POSeq, PONumber, POPKey,
					CheckNumber, Voucher, Stage, PayableDate, TransDate,
					PayableFileName, GLAccount, ControlAmount,
					RecurSeq,
					LiabCode, BankCode, PayTerms, PayDate, Address,
					SourceType, SourcePKey,
					SourceSeq, SourceBatch,
					StorageSeq, StoragePathOrID,
					ApprovedDetail, ModDate, ModUser, CreateDate, CreateUser)
				VALUES (@VendorID, @Company, @PayableType,
					(CASE
						WHEN LOWER(@ParentSeqValue) = 'null' OR @ParentSeqValue = '' THEN NULL
						ELSE CAST(@ParentSeqValue AS INT)
					END),
					NULLIF(@Group, N''), @ItemNumber, NULLIF(@POSeqNum, ''), NULLIF(@PONumber, ''), NULLIF(@POPKey, ''),
					@CheckNumber, @Voucher, NULLIF(@StageObjectName, N''), NULLIF(@ItemDate, ''), NULLIF(@TransDate, ''),
					@FileName, @GLAccount, (CASE WHEN @ControlAmt = N'' THEN NULL ELSE CAST(@ControlAmt AS DECIMAL(19,5)) END),
					(CASE WHEN @recurSeq = '' THEN NULL ELSE CAST(@recurSeq AS INT) END),
					NULLIF(@LiabCode, N''), NULLIF(@BankCode, N''), NULLIF(@PayTerms, N''), NULLIF(@PayDate, ''), NULLIF(@Address, N''),
					NULLIF(LEFT(@SourceType, 50), ''), NULLIF(LEFT(@SourcePKey, 512), ''),
					(CASE WHEN @sourceSeqNum = '' THEN NULL ELSE CAST(@sourceSeqNum AS INT) END), NULLIF(LEFT(@SourceBatch, 50), ''),
					(CASE WHEN @storageSeq = '' THEN NULL ELSE CAST(@storageSeq AS INT) END), NULLIF(@storagePathOrID, ''),
					NULLIF(@ApprovedDetail, N''), @ModDate, @EmpId, @ModDate, @EmpId);

				SET @payableSeq = SCOPE_IDENTITY();
				select @moduser = username from SEUser where Employee = @EmpId;

				INSERT INTO CCG_PAT_Files(RevisionSeq,StorageSeq,StoragePathOrID,PayableSeq,FileName,FileState,ModUser,ModDate)
				SELECT 0,StorageSeq,StoragePathOrID,Seq,PayableFileName,null,@moduser, @ModDate
				 FROM CCG_PAT_Payable where Seq = @payableSeq  AND ISNULL(StoragePathOrID,'') <> '';
			--SELECT @payableSeq

			--History
			INSERT INTO CCG_PAT_History (ActionTaken, ActionDate, ActionTakenBy,PayableSeq,Stage,[Description])
			VALUES ('Create',getutcdate(),@EmpId,@payableSeq,null,null);

			if ISNULL(@StageObjectName,'') <> ''
				INSERT INTO CCG_PAT_History(PayableSeq,Stage,ActionDate,ActionTaken,ActionTakenBy,[Description])
					VALUES(@PayableSeq,@StageObjectName,getutcdate(),N'Stage Change',@EmpId, N'Default')
		END;

		--start detail and tax tables

		DECLARE @sSQL		Nvarchar(max);
		--DECLARE @safeSql	int;			-- INT value indicates which parameter failed the security check

		-- Ensure that our inputs are safe (preventing SQL injection)
		--SET @safeSql = dbo.fnCCG_PAT_RegexMatch('<CSV>', @fieldsSQL);
		--SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch('<INSERT VALUES>', @newValuesSql) * 2;
		--IF @safeSql < 3 BEGIN
		--	SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS Nvarchar(4));
		--	RETURN;
		--END;



		--valuesSql may have substitution text @newPayableSeq for new records, needs to be replaced here
		IF ISNULL(@newValuesSql, N'') <> N'' AND ISNULL(@fieldsSQL, N'') <> N'' 
		BEGIN
			--mostly for updates, but ok on insert too to make sure we start out blank
			--only delete when we have something to reinsert since possible to save without PA when using Vision amounts and at least one PA rows are required
			DELETE FROM CCG_PAT_ProjectAmount where PayableSeq = @payableSeq
			DELETE from CCG_PAT_ProjectAmountTax where PayableSeq = @payableSeq

			SET @sSQL = N'
				INSERT INTO CCG_PAT_ProjectAmount
					(' + @fieldsSQL + ')
					VALUES
					' + Replace(@newValuesSql,'@newPayableSeq',@payableSeq);
			--PRINT @sSQL;
			EXEC (@sSQL);
		END;

		IF ISNULL(@taxValuesSql,N'') <> N'' AND ISNULL(@taxFieldsSQL,N'') <> N'' BEGIN
			SET @sSQL = N'
				INSERT INTO CCG_PAT_ProjectAmountTax
					(' + @taxFieldsSQL + ')
					VALUES
					' + Replace(@taxValuesSql,'@newPayableSeq',@payableSeq);
			--PRINT (@sSQL);
			EXEC (@sSQL);
		END;

		-- Pending inserts - new records from editForm only
		IF ISNULL(@pendingInserts,N'') <> N'' BEGIN
			SET @sSQL = N'
				INSERT INTO CCG_PAT_Pending (PayableSeq, SortOrder,Parallel, Employee, Description, CreateDateTime)
					VALUES ' + Replace(@pendingInserts,'@newPayableSeq',@payableSeq);
			--PRINT @sSQL;
			EXEC (@sSQL);
		END;

		--successful response
		select @payableSeq as Result, '' as ErrorMessage, convert(varchar(23), @ModDate, 121) as ModDate
	COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		-- SQL found some error - return the message and number:
		select -1 as ReturnVal, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage
		rollback transaction
	END CATCH
END;

GO
