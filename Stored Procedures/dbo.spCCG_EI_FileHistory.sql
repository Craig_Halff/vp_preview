SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_FileHistory]
	@WBS1		Nvarchar(30),
	@Category   Nvarchar(50) = null,--null for all
	@DOCS_PR_FILEID	varchar(50) = null,
	@RevisionHistory int = 0, --0 = none, 1 = File only, 2 = File and markup
	@Language varchar(10) = 'en-US',
	@Username Nvarchar(32) = null
AS
BEGIN
	-- TEST: EXEC [dbo].[spCCG_EI_FileHistory] '2003005.00', '', '',0
	-- TEST: EXEC [dbo].[spCCG_EI_FileHistory] '2003005.00', 'INVOICE', null,0
	-- TEST: EXEC [dbo].[spCCG_EI_FileHistory] '2003005.00', '',null,2
	-- EXEC [dbo].[spCCG_EI_FileHistory] '2003005.00', '','2f362d96-dd09-4270-b73d-fc421e66bcb7','2'--fcfab392-8b5b-45b1-8d98-bd029d924640
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	if @Category = ''
	set @Category = null

	IF ISNULL(@RevisionHistory,0) = 0
	BEGIN
		SELECT dpr.createdate as sortdate,dpr.category as displayCategory,ISNULL(x.RevisionSeq,999999) as MasterRevisionSeq,dpr.Invoice,dpr.createuser as FileCreateUser,ISNULL(x.CreateUser,dpr.createuser) as FileModUser,convert(Varchar(19),ISNULL(x.CreateDate,dpr.CreateDate), 120)  as ModDateString,convert(Varchar(19),  dpr.Createdate, 120) as CreateDateString, 
			dpr.FILEID, dpr.FilePath,dpr.FileDescription,dpr.category,dpr.FileSize,dpr.StorageSeq,dpr.StoragePathOrID, '' as OpenButton
		FROM CCG_DOCS_PR dpr
			LEFT JOIN CCG_EI_XFDF x on dpr.FILEID = x.DOCS_PR_FILEID and x.RevisionSeq = 0
		WHERE dpr.WBS1 = @wbs1 and ISNULL(@Category,dpr.Category) = dpr.Category
		order by dpr.SortBy desc, dpr.CreateDate desc
	END
	ELSE IF ISNULL(@DOCS_PR_FILEID,'') <> ''
	BEGIN
	print 'FILEID'
		SELECT ISNULL(x.Createdate,dpr.Createdate) as sortdate,dpr.category as displayCategory,x.RevisionSeq as MasterRevisionSeq,dpr.invoice,dpr.createuser as FileCreateUser,ISNULL(x.CreateUser,dpr.createuser) as FileModUser,convert(Varchar(19),ISNULL(x.CreateDate,dpr.CreateDate), 120)  as ModDateString,convert(Varchar(19),  ISNULL(x.Createdate,dpr.Createdate), 120) as CreateDateString, 
			dpr.*,x.XFDFID,'' as OpenButton
		FROM CCG_DOCS_PR dpr
			LEFT JOIN CCG_EI_XFDF x on dpr.FILEID = x.DOCS_PR_FILEID
		WHERE dpr.FILEID = @DOCS_PR_FILEID 
		UNION 
		--get initial file entry without XFDF revisions
		SELECT dpr.createdate as sortdate,dpr.category as displayCategory,999999 as MasterRevisionSeq,dpr.invoice,dpr.createuser as FileCreateUser,dpr.createuser as FileModUser,convert(Varchar(19),dpr.CreateDate, 120)  as ModDateString,convert(Varchar(19),  dpr.Createdate, 120) as CreateDateString, 
			dpr.*,null as XFDFID,'' as OpenButton
		FROM CCG_DOCS_PR dpr
		WHERE dpr.FILEID = @DOCS_PR_FILEID 
		order by 1 desc,2
	END
	ELSE 
	BEGIN

		SELECT ISNULL(x.Createdate,dpr.Createdate) as sortdate,dpr.category as displayCategory,x.RevisionSeq as MasterRevisionSeq,dpr.invoice,dpr.createuser as FileCreateUser,ISNULL(x.CreateUser,dpr.createuser) as FileModUser,convert(Varchar(19),ISNULL(x.CreateDate,dpr.CreateDate), 120)  as ModDateString,convert(Varchar(19),  ISNULL(x.Createdate,dpr.Createdate), 120) as CreateDateString, 
			dpr.*,x.XFDFID,'' as OpenButton
		FROM CCG_DOCS_PR dpr
			LEFT JOIN CCG_EI_XFDF x on dpr.FILEID = x.DOCS_PR_FILEID
		WHERE dpr.WBS1 = @wbs1 and ISNULL(@Category,dpr.Category) = dpr.Category
		UNION 
		--get initial file entry without XFDF revisions
		SELECT dpr.createdate as sortdate,dpr.category as displayCategory,999999 as MasterRevisionSeq,dpr.invoice,dpr.createuser as FileCreateUser,ISNULL(x.CreateUser,dpr.createuser) as FileModUser,convert(Varchar(19),ISNULL(x.CreateDate,dpr.CreateDate), 120)  as ModDateString,convert(Varchar(19),  dpr.Createdate, 120) as CreateDateString, 
			dpr.*,null as XFDFID,'' as OpenButton
		FROM CCG_DOCS_PR dpr
			LEFT JOIN CCG_EI_XFDF x on dpr.FILEID = x.DOCS_PR_FILEID
		WHERE dpr.WBS1 = @wbs1 and ISNULL(@Category,dpr.Category) = dpr.Category and x.RevisionSeq = 0
		order by 1 desc,2
	END

END;
GO
