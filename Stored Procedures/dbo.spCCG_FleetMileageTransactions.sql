SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_FleetMileageTransactions]
AS
/*
Copyright 2019 (c) Central Consulting Group.   All rights reserved.
08/08/2019	David Springer
			Write mileage transactions to Unit Data Entry import table
			Use Vehicle Unit.
			Call this from a Vehicle scheduled workflow button.

Select *
From UNControl
Where Batch = 'Test Mileage'

Select *
From UNMaster
Where Batch = 'Test Mileage'

Select *
From UNDetail
Where Batch = 'Test Mileage'

Delete From UNControl Where Batch = 'Test Mileage'
Delete From UNMaster Where Batch = 'Test Mileage'
Delete From UNDetail Where Batch = 'Test Mileage'


-- Create it if necessary:
Select Replace (NewID(), '-', '') PKey, Left (p.Org, (Select Org1Length From dbo.CFGFormat)) AS EmployeeCompany
, l.CustWBS1 AS WBS1, l.CustWBS2 AS WBS2, l.CustWBS3 AS WBS3, 
		p.UnitTable, f.CustUnit AS Unit, m.CustDate AS TransDate, m.CustComments AS DetailDescription, m.CustMiles AS Quantity
		INTO dbo.VisionImportDEUnit_FleetMileage  FROM dbo.UDIC_FleetManagement f, dbo.UDIC_FleetManagement_MileageLog m, dbo.UDIC_ProjectLowestLevel l
 
		Left Join dbo.PR p on p.WBS1 = l.CustWBS1 and p.WBS2 = l.CustWBS2 and p.WBS3 = l.CustWBS3
	Where f.UDIC_UID = m.UDIC_UID
	  and m.CustPosted = 'N'
	  and m.CustProject = l.UDIC_UID
	  and l.CustWBS1 is not null -- Personal & Missing
	
	CREATE TABLE [dbo].[VisionImportDEUnit_FleetMileage](
	[ErrorMessage] [VARCHAR](MAX) NULL,
	[PKey] [VARCHAR](32) NULL,
	[TransDate] [DATETIME] NULL,
	[WBS1] [VARCHAR](30) NULL,
	[WBS2] [VARCHAR](30) NULL,
	[WBS3] [VARCHAR](30) NULL,
	[Unit] [VARCHAR](30) NULL,
	[UnitTable] [VARCHAR](30) NULL,
	[Account] [VARCHAR](13) NULL,
	[Quantity] [DECIMAL](19, 4) NULL,
	[DetailDescription] [VARCHAR](255) NULL,
	[Employee] [VARCHAR](20) NULL,
	[EmployeeCompany] [VARCHAR](14) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


*/
SET NOCOUNT ON
BEGIN
	--Select * From VisionImportDEUnit_FleetMileage
	Delete From dbo.VisionImportDEUnit_FleetMileage Where Unit is not null

	Insert INTO dbo.VisionImportDEUnit_FleetMileage
	(PKey, EmployeeCompany, WBS1, WBS2, WBS3, UnitTable, Unit, TransDate, DetailDescription, Quantity)
	Select Replace (NewID(), '-', '') PKey, Left (p.Org, (Select Org1Length From dbo.CFGFormat)) Company, l.CustWBS1, l.CustWBS2, l.CustWBS3, 
		p.UnitTable, f.CustUnit, m.CustDate, m.CustComments, m.CustMiles
		FROM dbo.UDIC_FleetManagement f, dbo.UDIC_FleetManagement_MileageLog m, dbo.UDIC_ProjectLowestLevel l
 
		Left Join dbo.PR p on p.WBS1 = l.CustWBS1 and p.WBS2 = l.CustWBS2 and p.WBS3 = l.CustWBS3
	Where f.UDIC_UID = m.UDIC_UID
	  and m.CustPosted = 'N'
	  and m.CustProject = l.UDIC_UID
	  and l.CustWBS1 is not null -- Personal & Missing
END


GO
