SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_LoadEmployeeObjects]
AS
BEGIN
	/* Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved. */
	-- exec [dbo].[spCCG_EI_LoadEmployeeObjects]
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	declare @ApprovalRequired varchar(1)
	select @ApprovalRequired = isnull(c.DelegationApproval, 'N') from CCG_EI_Config c

	select EM.Employee, LastName, FirstName, PreferredName, Email, isnull(MIN(u.Username), MIN(SEUser.UserName)) as Username, EM.Language, EM.Supervisor,
			del.Employee as EmployeeWithDelegate, del.Delegate, del.Dual, del.ForWBS1, del.ForClientId, del.ToDate,
			case when del.Employee = EM.Employee then 'HasDelegate' when del.Delegate = EM.Employee then 'IsDelegateFor' else null end as Type,
			EM.Status
		from EM
			left join SEUSER on SEUSER.EMPLOYEE = EM.EMPLOYEE and SEUSER.DisableLogin = 'N'
			left join CCG_EI_User u on u.Username = SEUser.Username
            left join CCG_EI_Delegation del on
				(del.Employee = EM.Employee or del.Delegate = EM.Employee) and
					(isnull(del.ApprovedBy,N'') <> N'' or @ApprovalRequired = 'N') and GETDATE() between del.FromDate and dateadd(d, 1, del.ToDate)
		group by EM.Employee, LastName, FirstName, PreferredName, Email, EM.Status, EM.Language, EM.Supervisor,
			del.Employee, del.Delegate, del.Dual, del.ToDate, del.ForWBS1, del.ForClientId
		order by LastName, FirstName
END
GO
