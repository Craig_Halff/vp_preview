SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_LoadEmployeeObjects]
	@orderByName		bit,
	@rolesToInclude		Nvarchar(max) = ''		-- CSV list without quotes or spaces, e.g. ACCOUNTING,DEFAULT
AS
BEGIN
	-- [spCCG_PAT_LoadEmployeeObjects] 0
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @sSQL		Nvarchar(max)

	SET @sSQL = N'
		SELECT EM.Employee, LastName, FirstName, PreferredName,
				Email, isnull(MIN(u.Username), MIN(SEUser.UserName)) as Username, EM.Status, EM.Language, EM.Supervisor,
				del.Delegate, del.Dual, del.MaxAmount, del.ToDate, SEUSER.Role
			FROM EM
				INNER JOIN SEUSER on SEUSER.EMPLOYEE = EM.EMPLOYEE and SEUSER.DisableLogin = ''N''
				LEFT JOIN CCG_PAT_User u on u.Username = SEUser.Username
                LEFT JOIN (
					SELECT Employee, MAX(Delegate) Delegate, Max(Dual) Dual,
							Max(MaxAmount) MaxAmount, Max(ToDate) ToDate
						FROM CCG_PAT_Delegation d
						WHERE ISNULL(d.ApprovedBy,'''') <> '''' AND GETDATE() BETWEEN d.FromDate AND d.ToDate
						GROUP BY Employee
				) del ON del.Employee = EM.Employee 
				'+(case when isnull(@rolesToInclude,'') = '' then '' else '
			WHERE SEUSER.Role in ('''+replace(@rolesToInclude, ',', ''',''')+''') ' end)+'
			GROUP BY EM.Employee, LastName, FirstName, PreferredName, Email, EM.Status, EM.Language, EM.Supervisor,
				del.Delegate, del.Dual, del.MaxAmount, del.ToDate, SEUSER.Role'

	IF @orderByName = 1 SET @sSQL = @sSQL + '
			ORDER BY 2, 1'
	ELSE SET @sSQL = @sSQL + '
			ORDER BY 1, 2'

	EXEC(@sSQL)
END
GO
