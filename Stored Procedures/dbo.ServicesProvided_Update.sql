SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ServicesProvided_Update]
	@WBS1							NVARCHAR(30)
	--, @WBS2						NVARCHAR(30)
	--, @WBS3						NVARCHAR(30)
    , @Seq							NVARCHAR(32)
    , @CreateUser					NVARCHAR(32)
    , @ModUser						NVARCHAR(32)
    , @CustServicesProjectType		NVARCHAR(32)
	, @CustService					NVARCHAR(32)

AS 
BEGIN
  ------------------------------------------------------------------------
  SET NOCOUNT ON;

  ------------------------------------------------------------------------

IF NOT EXISTS (
	SELECT 'x'
	FROM dbo.Projects_ServicesProvided
	WHERE WBS1 = @WBS1
		AND WBS2 = ' ' --@WBS2
		AND WBS3 = ' ' --@WBS3
		AND Seq = @Seq
)

	INSERT INTO dbo.Projects_ServicesProvided
	(
		WBS1
		, WBS2
		, WBS3
		, Seq
		, CreateUser
		, CreateDate
		, ModUser
		, ModDate
		, CustServicesProjectType
		, CustService
	)
	VALUES
	(
		@WBS1					
		, ' ' --@WBS2					
		, ' ' --@WBS3					
		, REPLACE(CAST(NEWID() AS NVARCHAR(36)), '-', '')					
		, @CreateUser			
		, GETDATE()			
		, @ModUser				
		, GETDATE()				
		, @CustServicesProjectType		
		, @CustService				
	)

ELSE

	UPDATE dbo.Projects_ServicesProvided
	SET ModUser = @ModUser
		, ModDate = GETDATE()
		, CustServicesProjectType = @CustServicesProjectType
		, CustService = @CustService
	WHERE WBS1 = @WBS1
		AND WBS2 = ' ' --@WBS2
		AND WBS3 = ' ' --@WBS3
		AND Seq = @Seq
  ------------------------------------------------------------------------
END
GO
