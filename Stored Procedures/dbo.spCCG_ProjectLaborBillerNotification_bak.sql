SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Procedure [dbo].[spCCG_ProjectLaborBillerNotification_bak] 
AS
/*
Copyright 2017 (c) Central Consulting Group.   All rights reserved.
08/24/2017 David Springer
           Write Biller into EI CCG_Email table with regular projects with BillExt = 0.
*/
SET NOCOUNT ON
DECLARE @Biller     varchar (8000),
        @Email      varchar (100),
		@WBS1		varchar (32),
		@WBS2		varchar (7),
		@WBS3		varchar (7),
		@Period		varchar (6),
		@Employee	varchar (255),
		@Category	varchar (8000),
        @Body       varchar (8000),
        @Loop1      integer

DECLARE curBiller insensitive cursor for
		Select distinct IsNull (p.Biller, '001975'), e.EMail
		From PR p, LD l, EMMain e
		Where p.WBS1 = l.WBS1
		  and p.WBS2 = ' '  --- project level for Biller
		  and p.WBS1 not like 'Z%'
		  and p.ChargeType = 'R'
		  and l.LaborCode = '01'
		  and l.BillStatus = 'B'
		  and l.TransDate > '04-24-2017'
		  and p.Biller = e.Employee
		  and l.BillExt = 0

BEGIN
-- Get each biller
	OPEN curBiller
	FETCH NEXT FROM curBiller INTO @Biller, @Email
	Set @Loop1 = @@FETCH_STATUS
	While @Loop1 = 0
	BEGIN
		Declare curProjects insensitive cursor for
		Select distinct l.WBS1, l.WBS2, l.WBS3, l.Name, l.Period, ltrim (str (l.Category)) + ' - ' + c.Description
		From PR p, LD l
			Left Join BTLaborCats c on c.Category = l.Category
		Where p.WBS1 = l.WBS1
		  and p.WBS2 = ' '  --- project level for Biller
		  and p.WBS1 not like 'Z%'
		  and p.ChargeType = 'R'
		  and l.LaborCode = '01'
		  and l.BillStatus = 'B'
		  and l.TransDate > '04-24-2017'
		  and p.Biller = @Biller
		  and l.BillExt = 0

	-- Open the table with headers
		Set @Body = '<table><th>Project</th><th>Phase</th><th>Task</th><th>Employee</th><th>Period</th><th>Category</th>'
		Open curProjects
		Fetch next from curProjects Into @WBS1, @WBS2, @WBS3, @Employee, @Period, @Category
		While @@FETCH_STATUS = 0
		Begin
			Set @Body = @Body + '<tr><td>' + @WBS1 + '</td><td>  ' + @WBS2 + '</td><td>  ' + @WBS3 + '</td><td>  ' +  @Employee + '</td><td>  ' +  @Period + '</td><td>  ' +  @Category+ '</td></tr>'
			Fetch next from curProjects Into @WBS1, @WBS2, @WBS3, @Employee, @Period, @Category
		End
		Close curProjects
		Deallocate curProjects

	-- Close the table
		Set @Body = @Body + '</table>'

		Insert Into CCG_Email_Messages
		(SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
		Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @Body, getDate(), 1)

	FETCH NEXT FROM curBiller INTO @Biller, @Email
	Set @Loop1 = @@FETCH_STATUS

	END
	CLOSE curBiller
	DEALLOCATE curBiller

END

GO
