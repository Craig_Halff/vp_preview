SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FileHelper_insertXFDFDB] ( @User nvarchar(32), @payableSeq int, @XFDFFile nvarchar(max))
             AS EXEC spCCG_PAT_FileHelper_insertXFDFDB @User,@payableSeq,@XFDFFile
GO
