SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_ProjectRevenueFromContractDetails] @WBS1 nvarchar (32)
AS
/*
Copyright 2021 (c) Central Consulting Group, Inc.  All rights reserved.
07/29/2021	David Springer
			Calculate Revenue from Contract Details
			Call from a project Contract Details INSERT, CHANGE and DELETE workflow
08/05/2021	David Springer
			Added Estimated Labor, Expense & Consultant Fees
*/

BEGIN

	Update p
	Set p.Revenue = IsNull (c.Total, 0),
		p.WeightedRevenue = IsNull (c.Total, 0) * p.Probability / 100
	From PR p
		Left Join
		(Select d.WBS1, Sum (d.Total) Total
		From Contracts c, ContractDetails d
		Where c.WBS1 = d.WBS1
		  and c.ContractNumber = d.ContractNumber
		  and c.FeeIncludeInd = 'Y'
		Group by d.WBS1) c  on c.WBS1 = p.WBS1
	Where p.WBS1 = @WBS1
	  and p.WBS2 = ' '

	Update p
	Set p.CustEstimatedLaborFee = IsNull (c.Labor, 0),
		p.CustEstimatedExpenseFee = IsNull (c.Exp, 0),
		p.CustEstimatedConsultantFee = IsNull (c.Cons, 0)
	From ProjectCustomTabFields p
		Left Join
		(Select d.WBS1, 
				Sum (d.FeeDirLab) Labor,
				Sum (d.FeeDirExp + d.ReimbAllowExp) Exp,
				Sum (d.ConsultFee + d.ReimbAllowCons) Cons
		From Contracts c, ContractDetails d
		Where c.WBS1 = d.WBS1
		  and c.ContractNumber = d.ContractNumber
		  and c.FeeIncludeInd = 'Y'
		Group by d.WBS1) c  on c.WBS1 = p.WBS1
	Where p.WBS1 = @WBS1
	  and p.WBS2 = ' '
END
GO
