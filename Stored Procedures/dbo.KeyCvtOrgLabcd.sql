SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyCvtOrgLabcd]
	@Entity 			Nvarchar(40),
	@CvtType 		integer, -- 0 oldVal -> NewVal; 1 --Length Change; 2 --Remove leading zeros
	@ErrMsg 			Nvarchar(1000) output,
	@ChangeSide	   Nvarchar(5) = 'Left', --left or right
	@cLevels			integer,
	@cDelimiter		Nvarchar(1),
	@c1Start			integer,
	@c1Length		integer,
	@c2Start			integer,
	@c2Length		integer,
	@c3Start			integer,
	@c3Length		integer,
	@c4Start			integer,
	@c4Length		integer,
	@c5Start			integer,
	@c5Length		integer,
	@Order			Nvarchar(5) = '12345', --used when swaping the order of levels
	@ConstrFlag		integer = 0, -- 0 ALL, 1=Drop Constraints, 2=EnableConstraints, 3=UpdateTables
	@Diag integer = 0,
	@UserName Nvarchar(32) = 'VisionKeyFormat' -- Add by DP 4.0
	,@Period integer = 0
as
begin

set nocount on
declare 	@CvtEntity Nvarchar(40),
			@TableName  		Nvarchar(100),
			@ColumnName 		Nvarchar(100),
			@orgSubcode		Nvarchar(50),
			@Val 			Nvarchar(20),
			@UpdateValue 		Nvarchar(500),
			@ErrorNum 			integer,
			@SqlStr			Nvarchar(1000),
			@SqlStr2		Nvarchar(1000),
			@CursorString 		Nvarchar(1200),
			@test 				integer,
			@custlabel		Nvarchar(40),
			@Custlabelplural 	Nvarchar(40),
			@OldLevels			integer,
			@OldDelimiter		Nvarchar(1),
			@Old1Start			integer,
			@Old1Length			integer,
			@Old2Start			integer,
			@Old2Length			integer,
			@Old3Start			integer,
			@Old3Length			integer,
			@Old4Start			integer,
			@Old4Length			integer,
			@Old5Start			integer,
			@Old5Length			integer,
			@LastColumnName 	Nvarchar(100),
			@CvtFetch			integer,
			@AddlWhere		Nvarchar(500),
			@NewLevels			integer,
			@Length				integer,
			@i						integer,
			--@Diag					integer,
			@theLen				integer,
			@tmpVal				integer,
			@LabelSql			Nvarchar(1000),
			@EMDefLCSql			Nvarchar(1000),
			@where 				Nvarchar(200),
			@testVal			Nvarchar(1),
			@newOrgCode			Nvarchar(30)
			
	DECLARE @LabCDCount integer
	DECLARE @message nvarchar(max)

	--set @Diag = 0
	set @LastColumnName = ''
	set @AddlWhere = ''
	set @LabelSql = ''
	set @EMDefLCSql = ''
	set @orgSubcode = ''
	select @OldLevels= 0,
			@OldDelimiter= '',
			@Old1Start= 0,
			@Old1Length= 0,
			@Old2Start= 0,
			@Old2Length= 0,
			@Old3Start= 0,
			@Old3Length= 0,
			@Old4Start= 0,
			@Old4Length= 0,
			@Old5Start= 0,
			@Old5Length= 0

	if (@Diag <> 0)
		print 'Beginning KeyCvtOrgLabcd Procedure: ' + @Entity
	if (@Diag <> 0)
		print 'Checking for Record loss ' + @Entity
	begin transaction
	if (@cvtType <> 0)  --duplicate record check
		begin
		if (@Entity = 'Org')
			begin
				select @OldLevels     =	OrgLevels,
						@OldDelimiter  =	isNull(OrgDelimiter,''),
						@Old1Start     =	Org1Start,
						@Old1Length    =	Org1Length,
						@Old2Start     =	Org2Start,
						@Old2Length    =	Org2Length,
						@Old3Start     =	Org3Start,
						@Old3Length    =	Org3Length,
						@Old4Start     =	Org4Start,
						@Old4Length    =	Org4Length,
						@Old5Start     =	Org5Start,
						@Old5Length    =	Org5Length from cfgformat
				update cfgformat
				set OrgLevels      =	@cLevels,
					OrgDelimiter   =	@cDelimiter,
					Org1Start      =	@c1Start,
					Org1Length     =	@c1Length,
					Org2Start      =	@c2Start,
					Org2Length     =	@c2Length,
					Org3Start      =	@c3Start,
					Org3Length     =	@c3Length,
					Org4Start      =	@c4Start,
					Org4Length     =	@c4Length,
					Org5Start      =	@c5Start,
					Org5Length     =	@c5Length

				exec KeyNewValOrgLabcd
					@ColumnName 	='Org',
					@cvtType			= 1, -- 0 oldVal -> NewVal; 1 --Length Change; 2 --Remove leading zeros
					@RetValue 		= @UpdateValue output,
					@ChangeSide 	= @ChangeSide,
					@cLevels			= @cLevels,
					@cDelimiter		= @cDelimiter,
					@c1Start			= @c1Start,
					@c1Length		= @c1Length,
					@c2Start			= @c2Start,
					@c2Length		= @c2Length,	
					@c3Start			= @c3Start,
					@c3Length		= @c3Length,
					@c4Start			= @c4Start,
					@c4Length		= @c4Length,
					@c5Start			= @c5Start,
					@c5Length		= @c5Length,
					@OldLevels		= @OldLevels,
					@OldDelimiter	= @OldDelimiter,
					@Old1Start		= @Old1Start,
					@Old1Length		= @Old1Length,
					@Old2Start		= @Old2Start,
					@Old2Length		= @Old2Length,	
					@Old3Start		= @Old3Start,
					@Old3Length		= @Old3Length,
					@Old4Start		= @Old4Start,
					@Old4Length		= @Old4Length,
					@Old5Start		= @Old5Start,
					@Old5Length		= @Old5Length,
					@Order			= @Order

				select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'orgLabel'
				select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'orgLabelPlural'

				if (@cLevels > 0)
   				begin
						if (@Diag <> 0)
							print 'declare countCursor cursor for select 1 from Organization group by ' + @UpdateValue + ' having count(*) > 1'
						execute('declare countCursor cursor for select 1 from Organization group by ' + @UpdateValue + ' having count(*) > 1')
		
						open countCursor
						fetch next from countCursor into @test
						close countCursor
						deallocate countCursor
						if (@@FETCH_STATUS = 0) --duplicate records
							begin
								SET @message = dbo.GetMessage('MsgSomeWillLostIfNumShortConvNot',@custlabelplural,@custlabel,'','','','','','','')
								RAISERROR(@message,16,3)
									if (@@Trancount > 0)
									   ROLLBACK TRANSACTION
									return(50001) -- user defined error
							end
					end

				set @NewLevels = (@cLevels - @OldLevels)
				if (@NewLevels > 0) --Adding levels
					begin
						set @newOrgCode=''
						set @i = @OldLevels + 1
						while (@i <= @cLevels)
							begin
								select @length = case @i
									when 1 then @c1Length
									when 2 then @c2Length
									when 3 then @c3Length
									when 4 then @c4Length
									when 5 then @c5Length
								end
							   if (@Diag <> 0)
									print 'Insert into cfgorgcodes (OrgLevel,  Code,  Label) Values   (' + convert(Nvarchar,@i) + ',replicate(''0'','+ convert(Nvarchar,@Length) + '),''<Conversion>'')'
								Insert into cfgorgcodes (OrgLevel,  Code,  Label) Values   (@i,replicate('0',@Length),'<Conversion>')

								if (@i > @OldLevels + 1)
									set @newOrgCode = @newOrgCode + @cDelimiter

								set @newOrgCode = @newOrgCode + replicate('0',@Length)
								set @i = @i + 1
							end
						 if (@OldLevels = 0)--Create a default org when turning on Orgs
							 begin
								if (@Diag <> 0)
									print 'Inserting default organization with org ='+@newOrgCode
								Insert into organization (Org,Name) values (@newOrgCode,'<Default>')
							end
					end
				else -- deleting levels
					begin
						set @where = ''
						set @i=1
						while (@i <= len(@order))
						  begin
							set  @where = case @i when 1 then substring(@order,@i,1) else @where + ',' + substring(@order,@i,1)  end
							set @i = @i + 1
						  end
						  
						  --Dougw  changed the following to use the tables instead of the CFGOrgCodes view
						set @SqlStr = 'delete from cfgOrgcodesData ' + case @where when '' then '' else 'where OrgLevel not in (' + @where + ')' end
						if (@Diag <> 0)
						  print @SqlStr
						execute(@SqlStr)
						
						set @SqlStr = 'delete from cfgOrgcodesDescriptions ' + case @where when '' then '' else 'where OrgLevel not in (' + @where + ')' end
						if (@Diag <> 0)
						  print @SqlStr
						execute(@SqlStr)

						if (@cLevels = 0) --turning off orgs
							begin
								delete from Organization
								delete from FirmOrgAssociations
							end
					end--end delete levels

					set @i = 1
					set @ColumnName = 'Code'
					declare @MulticompanyEnabled Nvarchar(1)
					declare @MulticurrencyEnabled Nvarchar(1)
					declare @NewFunctionalCurrencyCode Nvarchar(3)
					declare @OldFunctionalCurrencyCode Nvarchar(3)
					select @MulticompanyEnabled = MulticompanyEnabled,
				   		 @MulticurrencyEnabled = MulticurrencyEnabled from FW_CFGSystem
					if (@MulticompanyEnabled = 'Y') and (left(@order,1) <> '1')
    					begin
							SET @message = dbo.GetMessage('MsgMultiCmpEnabledCantChangeLoc',@custlabel,'','','','','','','','')
							RAISERROR(@message,16,3)
								if (@@Trancount > 0)
								   ROLLBACK TRANSACTION
								return(50010) -- user defined error
		   			end

-- Added by DP 08/29/08
				select * into #TempOrgLabel from FW_CFGLabelData where LabelName not like 'orgLabel%' and LabelName like 'org%'

					while (@i <= @cLevels)
						begin
							set @tmpVal = convert(integer,substring(@Order,@i,1))
							select @theLen = 
								case @i when 1 then @c1Length
								when 2 then @c2Length
								when 3 then @c3Length
								when 4 then @c4Length
								when 5 then @c5Length
								end
							if (@ChangeSide = 'Left')
							begin	
								select @SqlStr =  case @TmpVal
									when 1 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old1Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
									when 2 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old2Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
									when 3 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old3Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
									when 4 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old4Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
									when 5 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old5Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
									end
							end
							--check for duplicates on each level
							if (@ChangeSide = 'Right')
							begin	
								select @SqlStr =  case @TmpVal
									when 1 then 'left(isNull(substring(' + @ColumnName + ',' +  convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old1Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
									when 2 then 'left(isNull(substring(' + @ColumnName + ',' +  convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old2Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
									when 3 then 'left(isNull(substring(' + @ColumnName + ',' +  convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old3Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
									when 4 then 'left(isNull(substring(' + @ColumnName + ',' +  convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old4Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
									when 5 then 'left(isNull(substring(' + @ColumnName + ',' +  convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old5Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
									end
							end
							--check for duplicates on each level
 							set @CursorString = 'declare orgCursor cursor for select 1 from cfgOrgCodes where  OrgLevel = ' + convert(Nvarchar,@tmpVal) + ' group by ' + @SqlStr + ' having count(*) > 1'
							if (@Diag <> 0)
								print @CursorString
							execute(@CursorString)
							open orgCursor
							fetch next from orgCursor into @test
							close orgCursor
							deallocate orgCursor
							if (@@FETCH_STATUS = 0) --duplicate records
								begin
									SET @message = dbo.GetMessage('MsgSomeLostCFGOrgCodesLevel',@custlabelplural,@i,@custlabel,'','','','','','')
									RAISERROR(@message,16,3)
										if (@@Trancount > 0)
										   ROLLBACK TRANSACTION
										return(50001) -- user defined error
								end

							--for first level org update company columns
							if (@i = 1 and @MulticompanyEnabled = 'Y')
  						     begin
									execute InsertKeyCvtDriver 'OrgSubCode'
									set @orgSubcode = 'OrgSubCode'
							  end
							  
							 --Dougw  changed the following to use the tables instead of the CFGOrgCodes view
							set @SqlStr2 = 'update CFGOrgCodesData set code = ' + @SqlStr + ' where OrgLevel = ' + convert(Nvarchar,@TmpVal)
							if (@Diag <> 0)
								print @Sqlstr2
							execute(@Sqlstr2)
							
							set @SqlStr2 = 'update CFGOrgCodesDescriptions set code = ' + @SqlStr + ' where OrgLevel = ' + convert(Nvarchar,@TmpVal)
							if (@Diag <> 0)
								print @Sqlstr2
							execute(@Sqlstr2)
							
							update cfgOrgCodesData
							set orgLevel = @i * 10,
							code = code
							where orgLevel = @tmpVal
							
							update cfgOrgCodesDescriptions
							set orgLevel = @i * 10,
							code = code
							where orgLevel = @tmpVal

-- Added by DP 08/29/08
--- dougw 10/21/08 changed the following so it would update labels of all cultures
							set @LabelSql = 'delete from #TempOrgLabel where LabelName = ''org' + convert(Nvarchar,@i) + 'Label'''
							execute(@LabelSql)
							set @LabelSql = 'Insert into #TempOrgLabel  (UICultureName, LabelName, LabelValue, PlaceHolder, Gender, LeadingVowelTreatment) Select UICultureName, ''org' + convert(Nvarchar,@i) + 'Label'', LabelValue, ''[Org Level '+ convert(Nvarchar,@i) + ']'', Gender, LeadingVowelTreatment from FW_CFGLabelData Where LabelName=''org' + convert(Nvarchar,@tmpVal) + 'Label'''
							execute(@LabelSql)
							set @LabelSql = 'delete from #TempOrgLabel where  LabelName = ''org' + convert(Nvarchar,@i) + 'LabelPlural'''
							execute(@LabelSql)
							set @LabelSql = 'Insert into #TempOrgLabel  (UICultureName, LabelName, LabelValue, PlaceHolder, Gender, LeadingVowelTreatment) Select UICultureName, ''org' + convert(Nvarchar,@i) + 'LabelPlural'', LabelValue, ''[Org Level '+ convert(Nvarchar,@i) + 's]'', Gender, LeadingVowelTreatment from FW_CFGLabelData Where LabelName=''org' + convert(Nvarchar,@tmpVal) + 'LabelPlural'''
							execute(@LabelSql)

							--update EM Default labor codes

							set @i = @i + 1
						end
						
					update cfgOrgCodesData set orgLevel = orgLevel/10
					update cfgOrgCodesDescriptions set orgLevel = orgLevel/10

-- Added by DP 08/29/08
					Delete from FW_CFGLabelData where LabelName not like 'orgLabel%' and LabelName like 'org%'
					Insert FW_CFGLabelData select * from #TempOrgLabel 
					Drop Table #TempOrgLabel

			end -- Org
		if (@Entity = 'labcd')
			begin
				set @LabCDCount = isnull((select count(*) from CFGLCCodes),0)
				select @OldLevels     =	LCLevels,
						@OldDelimiter  =	isNull(LCDelimiter,''),
						@Old1Start     =	LC1Start,
						@Old1Length    =	LC1Length,
						@Old2Start     =	LC2Start,
						@Old2Length    =	LC2Length,
						@Old3Start     =	LC3Start,
						@Old3Length    =	LC3Length,
						@Old4Start     =	LC4Start,
						@Old4Length    =	LC4Length,
						@Old5Start     =	LC5Start,
						@Old5Length    =	LC5Length from cfgformat
				update cfgformat
				set LCLevels      =	@cLevels,
					LCDelimiter   =	@cDelimiter,
					LC1Start      =	@c1Start,
					LC1Length     =	@c1Length,
					LC2Start      =	@c2Start,
					LC2Length     =	@c2Length,
					LC3Start      =	@c3Start,
					LC3Length     =	@c3Length,
					LC4Start      =	@c4Start,
					LC4Length     =	@c4Length,
					LC5Start      =	@c5Start,
					LC5Length     =	@c5Length

				exec KeyNewValOrgLabcd
					@ColumnName 	='LaborCode',
					@cvtType			= 1, -- 0 oldVal -> NewVal; 1 --Length Change; 2 --Remove leading zeros
					@RetValue 		= @UpdateValue output,
					@ChangeSide 	= @ChangeSide,                  
					@cLevels			= @cLevels,
					@cDelimiter		= @cDelimiter,
					@c1Start			= @c1Start,
					@c1Length		= @c1Length,
					@c2Start			= @c2Start,
					@c2Length		= @c2Length,	
					@c3Start			= @c3Start,
					@c3Length		= @c3Length,
					@c4Start			= @c4Start,
					@c4Length		= @c4Length,
					@c5Start			= @c5Start,
					@c5Length		= @c5Length,
					@OldLevels		= @OldLevels,
					@OldDelimiter	= @OldDelimiter,
					@Old1Start		= @Old1Start,
					@Old1Length		= @Old1Length,
					@Old2Start		= @Old2Start,
					@Old2Length		= @Old2Length,	
					@Old3Start		= @Old3Start,
					@Old3Length		= @Old3Length,
					@Old4Start		= @Old4Start,
					@Old4Length		= @Old4Length,
					@Old5Start		= @Old5Start,
					@Old5Length		= @Old5Length,
					@Order		   = @Order
			-- LB
				select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'labcdLabel'
				select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'labcdLabelPlural'

				set @sqlstr = 'declare countCursor cursor for select 1 from LB group by WBS1,WBS2,WBS3 ' + case @UpdateValue when 'null' then '' else ',' + @updateValue end + ' having count(*) > 1'
				if (@Diag <> 0)
					print @sqlstr
				execute(@sqlstr)
				open countCursor
				fetch next from countCursor into @test
				close countCursor
				deallocate countCursor
				if (@@FETCH_STATUS = 0) --duplicate records
					begin
					   SET @message = dbo.GetMessage('MsgSomeRecWillBeLostLabBudgets',@custlabelplural,@custlabel,'','','','','','','')
					   RAISERROR(@message,16,3)
							if (@@Trancount > 0)
							   ROLLBACK TRANSACTION
							return(50001) -- user defined error
					end
			-- removed duplicate record check, Laborcode not part of the primary key in BTRLTCodes
				set @NewLevels = (@cLevels - @OldLevels)
				if (@diag <>0)
					print 'NewLevels ' + convert(Nvarchar,@NewLevels)
				if (@NewLevels > 0) --Adding levels
					begin
						set @i = @OldLevels + 1
						while (@i <= @cLevels)
							begin
								select @length = case @i
									when 1 then @c1Length
									when 2 then @c2Length
									when 3 then @c3Length
									when 4 then @c4Length
									when 5 then @c5Length
								end
							   if (@Diag <> 0)
									print 'Insert into cfgLCcodes (LCLevel,  Code,  Label) Values   (' + convert(Nvarchar,@i) + ',replicate(''0'','+ convert(Nvarchar,@Length) + '),''<Conversion>'')'
								Insert into cfgLCcodes (LCLevel,  Code,  Label) Values   (@i,replicate('0',@Length),'<Conversion>')

								set @i = @i + 1
							end
					end
				else -- deleting levels
					begin
						set @where = ''
						set @i=1
						while (@i <= len(@order))
						  begin
							set  @where = case @i when 1 then substring(@order,@i,1) else @where + ',' + substring(@order,@i,1)  end
							set @i = @i + 1
						  end
						set @SqlStr = 'delete from cfglccodesData ' + case @where when '' then '' else 'where LCLevel not in (' + @where + ')' end
						if (@Diag <> 0)
						  print @SqlStr
						execute(@SqlStr)
						
						set @SqlStr = 'delete from cfglccodesDescriptions ' + case @where when '' then '' else 'where LCLevel not in (' + @where + ')' end
						if (@Diag <> 0)
						  print @SqlStr
						execute(@SqlStr)

						if (@cLevels = 0)
							begin
								delete from BTRLTCodes --Labor code rate tables have no meaning
							end
					end
					set @i = 1
					set @ColumnName = 'Code'


-- Added by DP 08/29/08
				select * into #TempLabcdLabel from FW_CFGLabelData where LabelName not like 'labcdLabel%' and LabelName like 'labcd%'

					while (@i <= @cLevels)
						begin
							set @tmpVal = convert(integer,substring(@Order,@i,1))
							select @theLen = 
								case @i when 1 then @c1Length
								when 2 then @c2Length
								when 3 then @c3Length
								when 4 then @c4Length
								when 5 then @c5Length
								end
							if (@ChangeSide = 'Left')
							begin	
								select @SqlStr =  case @TmpVal
									when 1 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old1Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
									when 2 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old2Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
									when 3 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old3Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
									when 4 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old4Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
									when 5 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old5Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
									end
							end
							--check for duplicates on each level
							if (@ChangeSide = 'Right')
							begin	
								select @SqlStr =  case @TmpVal
									when 1 then 'left(isNull(substring(' + @ColumnName + ',' +  convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old1Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
									when 2 then 'left(isNull(substring(' + @ColumnName + ',' +  convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old2Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
									when 3 then 'left(isNull(substring(' + @ColumnName + ',' +  convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old3Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
									when 4 then 'left(isNull(substring(' + @ColumnName + ',' +  convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old4Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
									when 5 then 'left(isNull(substring(' + @ColumnName + ',' +  convert(Nvarchar,1) + ',' + convert(Nvarchar,@Old5Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
									end
							end
							--check for duplicates on each level

 							set @CursorString = 'declare lcCursor cursor for select 1 from cfgLCCodes where  LCLevel = ' + convert(Nvarchar,@tmpVal) + ' group by ' + @SqlStr + ' having count(*) > 1'
							if (@Diag <> 0)
								print @CursorString
							execute(@CursorString)
							open lcCursor
							fetch next from lcCursor into @test
							close lcCursor
							deallocate lcCursor
							if (@@FETCH_STATUS = 0) --duplicate records
								begin
									SET @message = dbo.GetMessage('MsgSomeWillLostCFGLCCodesLevel',@custlabelplural,@i,@custlabel,'','','','','','')
									RAISERROR(@message,16,3)
										if (@@Trancount > 0)
										   ROLLBACK TRANSACTION
										return(50001) -- user defined error
								end
							set @SqlStr2 = 'update cfgLCCodesData set code = ' + @SqlStr + ' where LCLevel = ' + convert(Nvarchar,@TmpVal)
							if (@Diag <> 0)
								print @Sqlstr2
							execute(@Sqlstr2)
							
							update cfgLCCodesData
							set lcLevel = @i * 10, code = code
							where lcLevel = @tmpVal

							set @SqlStr2 = 'update cfgLCCodesDescriptions set code = ' + @SqlStr + ' where LCLevel = ' + convert(Nvarchar,@TmpVal)
							if (@Diag <> 0)
								print @Sqlstr2
							execute(@Sqlstr2)
							
							update cfgLCCodesDescriptions
							set lcLevel = @i * 10, code = code
							where lcLevel = @tmpVal

-- Added by DP 08/29/08
							set @LabelSql = 'delete from #TempLabcdLabel where LabelName = ''labcd' + convert(Nvarchar,@i) + 'Label'''
							execute(@LabelSql)
							set @LabelSql = 'Insert into #TempLabcdLabel  (UICultureName, LabelName, LabelValue, PlaceHolder, Gender, LeadingVowelTreatment) Select UICultureName, ''labcd' + convert(Nvarchar,@i) + 'Label'', LabelValue, ''[Labcd Level '+ convert(Nvarchar,@i) + ']'', Gender, LeadingVowelTreatment from FW_CFGLabelData Where LabelName=''labcd' + convert(Nvarchar,@tmpVal) + 'Label'''
							execute(@LabelSql)
							set @LabelSql = 'delete from #TempLabcdLabel where  LabelName = ''labcd' + convert(Nvarchar,@i) + 'LabelPlural'''
							execute(@LabelSql)
							set @LabelSql = 'Insert into #TempLabcdLabel  (UICultureName, LabelName, LabelValue, PlaceHolder, Gender, LeadingVowelTreatment) Select UICultureName, ''labcd' + convert(Nvarchar,@i) + 'LabelPlural'', LabelValue, ''[Labcd Level '+ convert(Nvarchar,@i) + 's]'', Gender, LeadingVowelTreatment from FW_CFGLabelData Where LabelName=''labcd' + convert(Nvarchar,@tmpVal) + 'LabelPlural'''
							execute(@LabelSql)

							--update EM Default labor codes

							set @SqlStr = replace(@Sqlstr,@ColumnName,'DefaultLC' + convert(Nvarchar,@i))
							set @SqlStr2 = 'update EMCompany set defaultLC' + convert(Nvarchar,@i) + ' = ' + @SqlStr + ' where defaultLC' + convert(Nvarchar,@i) + ' is not null' 
							set @EMDefLCSQL = @EMDefLCSQL + case @i when 1 then '' else ',' end +
									'defaultLC' + convert(Nvarchar,@i) + ' = defaultLC' + convert(Nvarchar,@tmpVal)
							if (@Diag <> 0)
							print @Sqlstr2
							execute(@Sqlstr2)

							set @i = @i + 1
						end
						update cfgLCCodesData set lcLevel = lcLevel/10
						update cfgLCCodesDescriptions set lcLevel = lcLevel/10
-- Added by DP 08/29/08
					Delete from FW_CFGLabelData where LabelName not like 'labcdLabel%' and LabelName like 'labcd%'
					Insert FW_CFGLabelData select * from #TempLabcdLabel
					Drop Table #TempLabcdLabel

						if (@cLevels <> 0 )
						begin
							select @EMDefLCSql = 'Update EMCompany set ' + @EMDefLCSql
							if (@Diag <> 0)
							print @EMDefLCSql
							execute(@EMDefLCSql)
						end


			end -- labcd


	end --duplicate record check

	if (@CvtType <> 0)
		begin
			execute InsertKeyCvtDriver @Entity -- populate driver table
		end

	declare cvtCursor cursor scroll static for
	select Entity,TableName, ColumnName from keyConvertDriver where Entity = @Entity or Entity = @orgSubcode
		and case @cvtType when 0 then skipValue else 'N' end = 'N' --skiping some tables/columns on value change
		order by ColumnName,TableName
	open cvtCursor
	--fetch next from cvtCursor  into
	--@TableName,
	--@ColumnName


	if (@ConstrFlag = 0 or @ConstrFlag = 1)
		begin
			if (@Diag <> 0)
			  Print 'Removing Constraints'
			fetch first  from cvtCursor  into
			@CvtEntity,
			@TableName,
			@ColumnName
			While (@@Fetch_Status = 0)
			begin
				execute('alter table ' + @TableName + ' nocheck constraint all')
				fetch next from cvtCursor into
				@CvtEntity,
				@TableName,
				@ColumnName
				--set @CvtFetch = @@Fetch_Status			
			end
		end
	if (@ConstrFlag = 0 or @ConstrFlag = 3)
		begin
			fetch first  from cvtCursor  into
			@CvtEntity,
			@TableName,
			@ColumnName
			While (@@FETCH_STATUS = 0 )
			begin
				if (@Diag <> 0)
					Print 'Converting '  + @TableName
				if (@ColumnName <> @LastColumnName)
					begin
							if (@CvtEntity = 'OrgSubCode')
							begin
								exec KeyNewValOrgLabcd
									@ColumnName 	= @ColumnName,
									@cvtType			= 3, -- 0 oldVal -> NewVal; 1 --Length Change; 2 --Remove leading zeros; 3--company subcodes
									@CvtLevel 		= 1,
									@RetValue 		= @UpdateValue output,
									@ChangeSide 	= @ChangeSide,             
									@cLevels			= 1,
									@cDelimiter		= @cDelimiter,
									@c1Start			= @c1Start,
									@c1Length		= @c1Length,
									@c2Start			= @c2Start,
									@c2Length		= @c2Length,	
									@c3Start			= @c3Start,
									@c3Length		= @c3Length,
									@c4Start			= @c4Start,
									@c4Length		= @c4Length,
									@c5Start			= @c5Start,
									@c5Length		= @c5Length,
									@OldLevels		= @OldLevels,
									@OldDelimiter	= @OldDelimiter,
									@Old1Start		= @Old1Start,
									@Old1Length		= @Old1Length,
									@Old2Start		= @Old2Start,
									@Old2Length		= @Old2Length,	
									@Old3Start		= @Old3Start,
									@Old3Length		= @Old3Length,
									@Old4Start		= @Old4Start,
									@Old4Length		= @Old4Length,
									@Old5Start		= @Old5Start,
									@Old5Length		= @Old5Length,
									@Order			= @Order
								end
							else
								begin
								exec KeyNewValOrgLabcd
									@ColumnName 	= @ColumnName,
									@cvtType			= 1, -- 0 oldVal -> NewVal; 1 --Length Change; 2 --Remove leading zeros
									@RetValue 		= @UpdateValue output,
									@ChangeSide 	= @ChangeSide,             
									@cLevels			= @cLevels,
									@cDelimiter		= @cDelimiter,
									@c1Start			= @c1Start,
									@c1Length		= @c1Length,
									@c2Start			= @c2Start,
									@c2Length		= @c2Length,	
									@c3Start			= @c3Start,
									@c3Length		= @c3Length,
									@c4Start			= @c4Start,
									@c4Length		= @c4Length,
									@c5Start			= @c5Start,
									@c5Length		= @c5Length,
									@OldLevels		= @OldLevels,
									@OldDelimiter	= @OldDelimiter,
									@Old1Start		= @Old1Start,
									@Old1Length		= @Old1Length,
									@Old2Start		= @Old2Start,
									@Old2Length		= @Old2Length,	
									@Old3Start		= @Old3Start,
									@Old3Length		= @Old3Length,
									@Old4Start		= @Old4Start,
									@Old4Length		= @Old4Length,
									@Old5Start		= @Old5Start,
									@Old5Length		= @Old5Length,
									@Order			= @Order
								end
						set @LastColumnName = @ColumnName
					end
				if (@TableName = 'LB')
					begin
						set @UpdateValue = 'ISNull(' + @UpdateValue + ','' '')'
						set @LastColumnName = ''
					end
				set @SQLStr = 'Update ' + @TableName + ' Set ' + @ColumnName + ' = ' + @UpdateValue
				set @testVal = ''
				if @LabCDCount = 0 and @ColumnName = 'LaborCode' and (@TableName = 'BILD' or @TableName = 'billLabDetail' or @TableName = 'CFGTKCategory' or @TableName = 'ICBillingWK' or @TableName = 'ICBillLabDetail' or @TableName = 'laDetail' or @TableName = 'LB' or @TableName = 'LD' or @TableName = 'RPAssignment' or @TableName = 'RPTask' or @TableName = 'RPTopDownGR' or @TableName = 'tkDetail' or @TableName = 'tsDetail' or @TableName = 'XChargeWk')
					set @AddlWhere = ' '
				else
					set @AddlWhere = ' AND ' + @ColumnName + ' <> N'' '''

				select @testVal = 'x' where @tableName + ',' + @ColumnName 
									in ('ADPCode,LaborCodeMask', 
										'BILD,XferLaborCode',
										'BTRLTCodes,LaborCodeMask',
										'LD,XferLaborCode',
										'CFGBillMainData,NonBillLaborCode',
										'RPAssignment,LaborCode',
										'RPTask,LaborCode')
				if (@TestVal = 'x')
				begin
					if @LabCDCount = 0 and @ColumnName = 'LaborCode' and (@TableName = 'BILD' or @TableName = 'billLabDetail' or @TableName = 'CFGTKCategory' or @TableName = 'ICBillingWK' or @TableName = 'ICBillLabDetail' or @TableName = 'laDetail' or @TableName = 'LB' or @TableName = 'LD' or @TableName = 'RPAssignment' or @TableName = 'RPTask' or @TableName = 'RPTopDownGR' or @TableName = 'tkDetail' or @TableName = 'tsDetail' or @TableName = 'XChargeWk')
						set @AddlWhere = ' AND ' + @ColumnName + ' not in (N''<none>'')'
					else
						set @AddlWhere = ' AND ' + @ColumnName + ' <> N'' '' and ' + @ColumnName + ' not in (N''<none>'')'
				end
				if (@CvtType <> 0)
				begin
					if @LabCDCount = 0 and @ColumnName = 'LaborCode' and (@TableName = 'BILD' or @TableName = 'billLabDetail' or @TableName = 'CFGTKCategory' or @TableName = 'ICBillingWK' or @TableName = 'ICBillLabDetail' or @TableName = 'laDetail' or @TableName = 'LB' or @TableName = 'LD' or @TableName = 'RPAssignment' or @TableName = 'RPTask' or @TableName = 'RPTopDownGR' or @TableName = 'tkDetail' or @TableName = 'tsDetail' or @TableName = 'XChargeWk')
						set @SQLStr = @SQLStr + ' Where (' + @ColumnName + ' is null or ' + @ColumnName + ' = N'' '') ' + @AddlWhere	
					else
						set @SQLStr = @SQLStr + ' Where ' + @ColumnName + ' is Not null' + ' ' + @AddlWhere	

					--If Talent Management Web Service is enabled, update TalentModDate to track changes relevant to Talent Management Integration.
					if (@TableName = 'EMMain' and @ColumnName = 'HomeCompany') or (@TableName = 'EMCompany' and @ColumnName = 'Org')
						if (Select EnableWebService From CFGIntegrationWS Where Type = 'Talent') = 'Y'
							--We update EM view since we are only tracking changes to home company records.
							set @SqlStr = @SqlStr + '; Update EM Set TalentModDate = GETUTCDATE() Where ' + @ColumnName + ' Is Not Null ' + @AddlWhere
				end
				if (@Diag <> 0)
					print  @SQLStr
				execute(@SQLStr)
				set @ErrorNum = @@Error
				if (@ErrorNum <> 0)
					begin
						if (@Diag <> 0)
							print 'Error :' + convert(Nvarchar,@ErrorNum)
						Rollback Transaction
						close cvtCursor
						deallocate cvtCursor
						select @ErrMsg = ' SQL:' + @SqlStr
						set nocount off
						return(@ErrorNum)
					end
				fetch next from cvtCursor into
				@CvtEntity,
				@TableName,
				@ColumnName
			end --While
		end --If Update
	if (@ConstrFlag = 0 or @ConstrFlag = 2)
		begin
			if (@Diag <> 0)
			  Print 'Applying Constraints '
			fetch first  from cvtCursor  into
			@CvtEntity,
			@TableName,
			@ColumnName
			While (@@FETCH_STATUS = 0)
			begin
				execute('alter table ' + @TableName + ' check constraint all')
				fetch next from cvtCursor into
				@CvtEntity,
				@TableName,
				@ColumnName
			end
		end
	if (@Diag <> 0)
		print 'Transactions: ' + convert(Nvarchar,@@Trancount)

-- Add by DP 7.0
	if (@entity = 'labcd' and @OldLevels > @cLevels)
	begin
		set @i = @OldLevels
		While (@i > @cLevels)
		begin
			set @SqlStr = 'Update PR Set BudgetedLevels = Replace(BudgetedLevels,'''+convert(varchar,@i)+''','''') where BudgetedLevels like ''%'+convert(varchar,@i)+'%'''
			if (@Diag <> 0)
				print  @SQLStr
			execute(@SqlStr)
			set @SqlStr = 'Update prDefaults Set BudgetedLevels = Replace(BudgetedLevels,'''+convert(varchar,@i)+''','''') where BudgetedLevels like ''%'+convert(varchar,@i)+'%'''
			if (@Diag <> 0)
				print  @SQLStr
			execute(@SqlStr)
			set @SqlStr = 'Update PRTemplate Set BudgetedLevels = Replace(BudgetedLevels,'''+convert(varchar,@i)+''','''') where BudgetedLevels like ''%'+convert(varchar,@i)+'%'''
			if (@Diag <> 0)
				print  @SQLStr
			execute(@SqlStr)
			set @i = @i - 1
		end
	end

-- Add by DP 4.0
	DECLARE @LastPostSeq integer
	DECLARE @strCompany Nvarchar(14)
	DECLARE @strComment Nvarchar(255)

	if (@Period = 0)
		set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
	if (@Period > 0)
	begin
		set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
		if (IsNull(@LastPostSeq, 0) = 0)
			  Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

		set @strCompany = dbo.GetActiveCompany()
		if (@strCompany = '')
			set @strCompany = (select MIN(company) from CFGMainData)

		set @strComment = @custlabel + ' Key Convert Format (Delimiter = ' + @cDelimiter + ', Order = ' + @Order + ', Level 1 Length = ' + convert(Nvarchar, @c1Length) 
		if (@c2Length > 0)
			set @strComment =  @strComment + ', Level 2 Length = ' + convert(Nvarchar, @c2Length)
		if (@c3Length > 0)
			set @strComment =  @strComment + ', Level 3 Length = ' + convert(Nvarchar, @c3Length)
		if (@c4Length > 0)
			set @strComment =  @strComment + ', Level 4 Length = ' + convert(Nvarchar, @c4Length)
		if (@c5Length > 0)
			set @strComment =  @strComment + ', Level 5 Length = ' + convert(Nvarchar, @c5Length)
		set @strComment =  @strComment + ')'

		Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
		Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, QueueName, PaymentProcessRun) 
			Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),N'KF','Y',  @strComment, @strCompany, @Entity, left(@custlabel + ' Key Convert Format', 40), @custlabel);
	end
--

	commit Transaction
	if (@Diag <> 0)
		print 'Transactions: ' + convert(Nvarchar,@@Trancount)
	close cvtCursor
	deallocate cvtCursor
	set nocount off
	return (0) -- no error
end
GO
