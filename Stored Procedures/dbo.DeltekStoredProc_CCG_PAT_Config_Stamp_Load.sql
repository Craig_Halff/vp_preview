SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_Stamp_Load]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT *
        FROM CCG_PAT_ConfigStamps
        ORDER BY DisplayOrder
END;

GO
