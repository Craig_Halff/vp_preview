SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectRelatedPursuits] @WBS1 varchar (30)
AS
/*
Copyright (c) 2021 Central Consulting Group. All rights reserved.
09/20/2021	David Springer
			Write to Projects Related Pursuits grid when Multi-Team Pursuit checkbox is checked
			Each pursuit project should reference all other pursuit projects
			Call this from a Project INSERT workflow when Multi-Team Pursuit checkbox is checked
			Call this from a Project CHANGE workflow when Multi-Team Pursuit checkbox, Total Fee, Probability or Billing Client is changed
*/
BEGIN

	Delete From Projects_CustRelatedPursuits Where WBS1 like Left (@WBS1, 6) + '%'

	Insert Into Projects_CustRelatedPursuits
	(WBS1, WBS2, WBS3, Seq, CustRelatedPursuit, CustRelatedPursuitRelationship, CustRelatedPursuitTotalFee, CustRelatedPursuitProbability, CustRelatedPursuitBillingClient)
	Select px1.WBS1, px1.WBS2, px1.WBS3, Replace (NewID(), '-', '') Seq, px2.WBS1, 'DDCode1', p2.Revenue, p2.Probability, p2.BillingClientID
	From ProjectCustomTabFields px1, ProjectCustomTabFields px2, PR  p2
	Where px1.WBS1 like Left (@WBS1, 6) + '%'
	  and px1.WBS2 = ' '
	  and px1.CustMultiTeamPursuit = 'Y'
	  and Left (px1.WBS1, 6) = Left (px2.WBS1, 6)
	  and px2.WBS2 = ' '
	  and px2.CustMultiTeamPursuit = 'Y'
	  and px1.WBS1 <> px2.WBS1
	  and px2.WBS1 = p2.WBS1
	  and px2.WBS2 = p2.WBS2

END;

GO
