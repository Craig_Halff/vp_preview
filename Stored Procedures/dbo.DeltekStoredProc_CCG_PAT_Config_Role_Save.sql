SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_Role_Save]
	@fieldsSQL			Nvarchar(max),
	@updateValuesSql	Nvarchar(max),
	@insertValuesSql	Nvarchar(max),
	@deleteRoles		Nvarchar(max),
	@VISION_LANGUAGE	Nvarchar(10)
AS
BEGIN
	/*
	[DeltekStoredProc_CCG_PAT_Config_Role_Save]
		@fieldsSQL = '',
		@updateValuesSql = '',
		@insertValuesSql = '',
		@deleteRoles = ''
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	int;

	-- Ensure that our inputs are safe (preventing SQL injection)
	SET @safeSql = dbo.fnCCG_PAT_RegexMatch(N'<CSV>', @fieldsSQL);
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch(N'<INSERT VALUES>|', @updateValuesSql) * 2;
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch(N'<INSERT VALUES>|', @insertValuesSql) * 4;
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch(N'<CSV>|', @deleteRoles) * 8;
	IF @safeSql < 15 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS Nvarchar(4));
		RETURN;
	END;

	BEGIN TRANSACTION;

	-- Updates
	IF ISNULL(@updateValuesSql, N'') <> N'' BEGIN
		SET @sSQL = N'
			UPDATE ut
				SET
					[VisionField]		= temp.[VisionField],
					[ColumnLabel]		= temp.[ColumnLabel],
					[RoleDescription]	= temp.[RoleDescription],
					[HidePdfTools]		= temp.[HidePdfTools],
					[HighlightColor]	= temp.[HighlightColor],
					[MarkupColor]		= temp.[MarkupColor],
					[Status]			= temp.[Status],
					[AdvancedOptions]	= temp.[AdvancedOptions],
					[AllowMessaging]	= temp.[AllowMessaging],
					[ColumnOrder]		= temp.[ColumnOrder]
				FROM CCG_PAT_ConfigRoles ut
				JOIN (
					VALUES ' + @updateValuesSql + '
				) temp (' + @fieldsSQL + ') ON temp.Role = ut.Role';
		--PRINT @sSQL;
		EXEC (@sSQL);
	END;

	-- Inserts
	IF ISNULL(@insertValuesSql, '') <> '' BEGIN
		SET @sSQL = N'
			INSERT INTO CCG_PAT_ConfigRoles
				(' + @fieldsSQL + ')
				VALUES
				' + @insertValuesSql;
		--PRINT @sSQL;
		EXEC (@sSQL);
	END;

	-- Deletes
	IF ISNULL(@deleteRoles, '') <> '' BEGIN
		SET @sSQL = N'
			DELETE FROM CCG_PAT_ConfigRoles
				WHERE [Role] IN (' + @deleteRoles + ')';
		--PRINT @sSQL;
		EXEC (@sSQL);
	END;

	-- Update the multilingual descriptions table
	SET @sSQL = N'
		DELETE a FROM CCG_PAT_ConfigRolesDescriptions a LEFT JOIN CCG_PAT_ConfigRoles b ON a.Role = b.Role
			WHERE b.Role IS NULL		-- No match

		MERGE CCG_PAT_ConfigRolesDescriptions WITH (HOLDLOCK) AS Target
		USING CCG_PAT_ConfigRoles AS Source ON Target.Role = Source.Role AND Target.UICultureName = '''+@VISION_LANGUAGE+'''
		WHEN MATCHED THEN
			UPDATE SET
				[ColumnLabel]		= Source.[ColumnLabel],
				[RoleDescription]	= Source.[RoleDescription]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (Role, UICultureName, ColumnLabel, RoleDescription)
				VALUES (Source.Role, '''+@VISION_LANGUAGE+''', Source.ColumnLabel, Source.RoleDescription)
		WHEN NOT MATCHED BY SOURCE AND Target.UICultureName = '''+@VISION_LANGUAGE+''' THEN DELETE;';
	EXEC (@sSQL);

	COMMIT TRANSACTION;
END;
GO
