SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_PctCompleteUpdateHistory] ( @insertValuesSql nvarchar(max))
             AS EXEC spCCG_EI_PctCompleteUpdateHistory @insertValuesSql
GO
