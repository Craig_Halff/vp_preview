SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_JTDInvoicedFromVendor_Details_PopupWin] ( @PayableSeq int, @WBS1 varchar(30)= null, @WBS2 varchar(7)= null, @WBS3 varchar(7)= null, @Username varchar(30))
             AS EXEC spCCG_PAT_JTDInvoicedFromVendor_Details_PopupWin @PayableSeq,@WBS1,@WBS2,@WBS3,@Username
GO
