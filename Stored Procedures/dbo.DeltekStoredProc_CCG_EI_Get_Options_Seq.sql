SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Get_Options_Seq]
	@empId			Nvarchar(32),
	@dbDescr		varchar(50),
	@type			varchar(max) = 'GridOptions'
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Max(Seq) as Seq
		FROM CCG_EI_ConfigOptions
		WHERE ChangedBy = @empId and Description = @dbDescr and Type = @type
END;
GO
