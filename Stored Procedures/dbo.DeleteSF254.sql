SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteSF254] @SF254ID varchar(32)
AS
    delete from SF254Disciplines where SF254ID = @SF254ID
    delete from SF254FeeSummary where SF254ID = @SF254ID
    delete from SF254FirmProfile where SF254ID = @SF254ID
    delete from SF254LocationsCount where SF254ID = @SF254ID
    delete from SF254Projects where SF254ID = @SF254ID    
GO
