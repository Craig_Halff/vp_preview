SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_RetainagePct] ( @PayableSeq int, @WBS1 nvarchar(32), @Vendor nvarchar(20)= '', @Contract nvarchar(50)= '')
             AS EXEC spCCG_PAT_RetainagePct @PayableSeq,@WBS1,@Vendor,@Contract
GO
