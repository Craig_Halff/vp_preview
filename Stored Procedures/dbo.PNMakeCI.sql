SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PNMakeCI]
  @strAccordionID VARCHAR(32)
AS

BEGIN -- Procedure PNMakeCI

  SET NOCOUNT ON
  
  DECLARE @strScale char
  DECLARE @strPlanID varchar(32)
  DECLARE @dtStartDate datetime
  DECLARE @dtEndDate datetime
  DECLARE @dtIntervalEnd datetime
  DECLARE @strCompany Nvarchar(14)
	
  DECLARE @intWkEndDay AS int
  
  -- Get information from AccordionFormat.
  
  SELECT @strScale = MinorScale,
         @dtStartDate = StartDate,
         @dtEndDate = EndDate,
         @strPlanID = PlanID
    FROM PNAccordionFormat WHERE AccordionFormatID = @strAccordionID
    
  -- Get Company Name and compute the end day of a week.
    
  SELECT @intWkEndDay = (CASE WHEN StartingDayOfWeek = 1 THEN 7 ELSE StartingDayOfWeek - 1 END), 
         @strCompany = Company
    FROM PNPlan AS P INNER JOIN PNAccordionFormat AS AF ON P.PlanID = AF.PlanID
    WHERE AF.AccordionFormatID = @strAccordionID
    
  -- Delete existing Calendar Interval records.
  
  DELETE PNCalendarInterval WHERE PlanID = @strPlanID AND AccordionFormatID = @strAccordionID
    
  IF (@strScale = 'a')
    BEGIN

      INSERT PNCalendarInterval
        (PlanID, 
         AccordionFormatID, 
         StartDate, 
         EndDate, 
         NumWorkingDays, 
         PeriodScale, 
         PeriodCount,
         CreateDate,
         ModDate)
        SELECT @strPlanID AS PlanID,
               @strAccordionID AS AccordionFormatID,
               CASE WHEN C.StartDate < @dtStartDate
                    THEN @dtStartDate
                    ELSE C.StartDate END AS StartDate, 
               CASE WHEN C.EndDate > @dtEndDate
                    THEN @dtEndDate
                    ELSE C.EndDate END AS EndDate,
               dbo.DLTK$NumWorkingDays(CASE WHEN C.StartDate < @dtStartDate
                                            THEN @dtStartDate
                                            ELSE C.StartDate END, 
                                       CASE WHEN C.EndDate > @dtEndDate
                                            THEN @dtEndDate
                                            ELSE C.EndDate END,
                                       @strCompany) AS NumWorkingDays,
               @strScale AS PeriodScale,
               1 AS PeriodCount,
               LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As CreateDate,
               LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As ModDate 
          FROM
            (SELECT StartDate, EndDate, 1 AS StartFlg, 1 AS EndFlg FROM CFGAcctngCalendar 
              WHERE StartDate <= @dtEndDate AND EndDate >= @dtStartDate) AS C 
             INNER JOIN 
              (SELECT * FROM 
               (SELECT SIGN(COUNT(*)) AS StartFlg FROM CFGAcctngCalendar 
                WHERE StartDate <= @dtStartDate AND EndDate >= @dtStartDate) AS S, 
               (SELECT SIGN(COUNT(*)) AS EndFlg FROM CFGAcctngCalendar 
                WHERE StartDate <= @dtEndDate AND EndDate >= @dtEndDate) AS E) AS X 
            ON C.StartFlg = X.StartFlg AND C.EndFlg = X.EndFlg
            
      IF @@ROWCOUNT = 0
        BEGIN
        
          DECLARE @strStartDate varchar(10)
          DECLARE @strEndDate varchar(10)
          
          SET @strStartDate = LEFT(CONVERT(VARCHAR, @dtStartDate, 121), 10)
          SET @strEndDate = LEFT(CONVERT(VARCHAR, @dtEndDate, 121), 10)
        
          RAISERROR ('Accounting Calendar does not contain data for period between %s and %s. %s', 16, 1,
                     @strStartDate, @strEndDate,
                     'Please make sure that Accounting Calendar is set up with data to cover all possible planning ranges.')
        
        END -- End If
    
    END -- End If-Then
  ELSE
    BEGIN
    
      WHILE (@dtStartDate <= @dtEndDate)
        BEGIN
        
          -- Compute End Date of interval.

          IF (@strScale = 'd') 
            SET @dtIntervalEnd = @dtStartDate
          ELSE
            SET @dtIntervalEnd = dbo.DLTK$IntervalEnd(@dtStartDate, @strScale, @intWkEndDay)
        
          IF (@dtIntervalEnd > @dtEndDate) 
            SET @dtIntervalEnd = @dtEndDate
            
          -- Insert new Calendar Interval record.
          
          INSERT PNCalendarInterval
                   (PlanID, 
                    AccordionFormatID, 
                    StartDate, 
                    EndDate, 
                    NumWorkingDays, 
                    PeriodScale, 
                    PeriodCount,
                    CreateDate,
                    ModDate)
            VALUES (@strPlanID,
                    @strAccordionID,
                    @dtStartDate,
                    @dtIntervalEnd,
                    dbo.DLTK$NumWorkingDays(@dtStartDate, @dtIntervalEnd, @strCompany),
                    @strScale,
                    1,
                    LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19),
                    LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19))
          
          -- Set Start Date for next interval.
          
          SET @dtStartDate = DATEADD(d, 1, @dtIntervalEnd)
        
        END -- End While
    
    END -- End If-Else

       
  SET NOCOUNT OFF

END -- PNMakeCI
GO
