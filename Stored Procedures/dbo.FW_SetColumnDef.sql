SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FW_SetColumnDef] @tname nvarchar(100), @cname nvarchar(100), @newdef nvarchar(2000)
AS
  DECLARE @olddef nvarchar(2000),
	  @sqlstr nvarchar(max)

  set @sqlstr = 'DECLARE DropConstCursor cursor for select d.name from sys.tables t join sys.default_constraints d on d.parent_object_id = t.object_id join sys.columns c on c.object_id = t.object_id and c.column_id = d.parent_column_id where t.name = ''' + @tname + ''' and c.name = ''' + @cname + '''' 

  exec (@sqlstr)
  open DropConstCursor
  fetch DropConstCursor into @olddef
  while @@fetch_status = 0
  begin
	set @sqlstr = 'alter table ' + @tname + ' drop constraint ' + @olddef
        -- print @sqlstr
	exec (@sqlstr) 

	fetch DropConstCursor into @sqlstr
  end
  close DropConstCursor
  deallocate DropConstCursor

  if isnull(@newdef,'') <> ''
  begin
    set @sqlstr = 'alter table ' + @tname + ' with nocheck add constraint DF__' + @tname + '_' + @cname + '_Def default(' + @newdef + ') for ' + @cname
    -- print @sqlstr
    exec (@sqlstr)
  end

GO
