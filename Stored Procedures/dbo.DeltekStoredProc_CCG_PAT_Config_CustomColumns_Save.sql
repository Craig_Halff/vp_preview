SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_CustomColumns_Save]
	@fieldsSQL			Nvarchar(max),
	@updateValuesSql	Nvarchar(max),
	@insertValuesSql	Nvarchar(max),
	@deleteSeqs			Nvarchar(max),
	@VISION_LANGUAGE	Nvarchar(10)
AS
BEGIN
	/*
	[DeltekStoredProc_CCG_PAT_Config_CustomColumns_Save]
		@fieldsSQL = 'Label, DataType, DisplayOrder, InitialValue, DatabaseField, ColumnUpdate, Width, Decimals, ViewRoles, EditRoles, RequiredRoles, RequiredExpression, Description, Link, HighlightColor, EnabledWBS1, EnabledWBS2, EnabledWBS3, Status, ModDate, ModUser',
		@updateValuesSql = '(''Acct Notes'', ''A'', 1, '''', ''AcctNotes'', '''', 0, 0, ''Accounting'', '''', '''', '''', ''Input column simply for biller notes'', '''', ''0'', ''Y'', ''N'', ''N'', ''A'', GetDate(), ''ADMIN'', 291), (''New\nMsgs'', ''N'', 3, ''dbo.fnCCG_EI_NewMessages'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''N'', ''N'', ''A'', GetDate(), ''ADMIN'', 465), (''Draft\nDate'', ''D'', 3, ''dbo.fnCCG_EI_DraftDate'', '''', '''', 0, 0, ''Accounting'', '''', '''', '''', '''', '''', ''0'', ''Y'', ''N'', ''N'', ''A'', GetDate(), ''ADMIN'', 442), (''Stage\nHistory'', ''L'', 5, ''dbo.fnCCG_EI_StageHistoryLink'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''N'', ''N'', ''I'', GetDate(), ''ADMIN'', 443), (''Backlog'', ''L'', 6, ''dbo.fnCCG_EI_BacklogLink'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''N'', ''N'', ''I'', GetDate(), ''ADMIN'', 464), (''Msg'', ''N'', 10, ''dbo.fnCCG_EI_Msg'', '''', '''', 0, 0, '''', '''', '''', '''', ''tt'', '''', ''0'', ''Y'', ''N'', ''N'', ''I'', GetDate(), ''ADMIN'', 444), (''BT Descr'', ''L'', 12, ''dbo.fnCCG_EI_BTDescrLink'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 458), (''Budget'', ''C'', 15, ''dbo.fnCCG_EI_Budget'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 446), (''JTD\nBilled'', ''C'', 20, ''dbo.fnCCG_EI_JTDBilled'', '''', '''', 0, 0, '''', '''', '''', '''', '''', ''dbo.fnCCG_EI_JTDBilledLink'', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 447), (''JTD\nSpent'', ''C'', 25, ''dbo.fnCCG_EI_JTDSpentBilling'', '''', '''', 0, 0, '''', '''', '''', '''', '''', ''dbo.fnCCG_EI_JTDSpentBillingLink'', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 457), (''JTD\nSpent %'', ''P'', 30, ''dbo.fnCCG_EI_JTDSpentPct'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 449), (''Billing Terms'', ''A'', 50, ''dbo.fnCCG_EI_BillingTerms'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 450), (''Cur Fee % Cpl'', ''N'', 55, ''dbo.fnCCG_EI_FeePctCpl'', '''', '''', 0, 4, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''I'', GetDate(), ''ADMIN'', 386), (''Fee % Cpl'', ''N'', 60, ''dbo.fnCCG_EI_FeePctCpl'', ''FeePctCpl'', ''spCCG_EI_FeePctCpl'', 0, 4, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''I'', GetDate(), ''ADMIN'', 387), (''Draft Amt'', ''C'', 65, ''dbo.fnCCG_EI_DraftInvoiceAmt'', '''', '''', 0, 2, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 451), (''AR Over 60'', ''C'', 70, ''dbo.fnCCG_EI_AROver60'', '''', '''', 0, 0, '''', '''', '''', '''', '''', ''dbo.fnCCG_EI_AROver60Link'', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 452), (''Req Exp\nBackup?'', ''B'', 72, ''dbo.fnCCG_EI_RequiresExpenseBackup'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''N'', ''N'', ''A'', GetDate(), ''ADMIN'', 453), (''% Completes'', ''L'', 75, ''dbo.fnCCG_EI_EnterPctCompLink'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 390), (''Hold\nUntil Date'', ''D'', 80, ''dbo.fnCCG_EI_HoldUntilDate'', ''HoldUntilDate'', ''spCCG_EI_HoldUntilDate'', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''N'', ''N'', ''A'', GetDate(), ''ADMIN'', 404), (''Hold Notes'', ''A'', 81, '''', ''HoldNotes'', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''N'', ''N'', ''A'', GetDate(), ''ADMIN'', 405), (''Reason Not\nInvoiced'', ''V'', 82, ''dbo.fnCCG_EI_CustReasonNotInvoiced'', ''CustReasonNotInvoiced'', ''spCCG_EI_CustReasonNotInvoiced'', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''N'', ''N'', ''A'', GetDate(), ''ADMIN'', 463), (''ETC'', ''C'', 89, '''', ''ETC'', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''N'', ''A'', GetDate(), ''ADMIN'', 462), (''Month\n+ 1'', ''C'', 90, '''', ''MonthPlus1'', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''N'', ''I'', GetDate(), ''ADMIN'', 459), (''Month\n+ 2'', ''C'', 91, '''', ''MonthPlus2'', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''N'', ''I'', GetDate(), ''ADMIN'', 460), (''Month\n+ 3'', ''C'', 92, '''', ''MonthPlus3'', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''N'', ''I'', GetDate(), ''ADMIN'', 461), (''BT Description'', ''A'', 99, ''dbo.fnCCG_EI_BTDescr'', ''BTDescr'', ''spCCG_EI_BTDescr'', 0, 0, ''Accounting'', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'', 305)',
		@insertValuesSql = '(''New THING'', ''A'', 100, ''fnCCG_EI_AROver60Link'', '''', '''', 0, 0, '''', '''', '''', '''', '''', '''', ''0'', ''Y'', ''Y'', ''Y'', ''A'', GetDate(), ''ADMIN'')',
		@deleteSeqs = ''
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	int;

	-- Ensure that our inputs are safe (preventing SQL injection)
	SET @safeSql = dbo.fnCCG_PAT_RegexMatch(N'<CSV>', @fieldsSQL);
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch(N'<INSERT VALUES>|', @updateValuesSql) * 2;
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch(N'<INSERT VALUES>|', @insertValuesSql) * 4;
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch(N'<CSV>|', @deleteSeqs) * 8;
	IF @safeSql < 15 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS Nvarchar(4));
		RETURN;
	END;

	BEGIN TRANSACTION;
	-- Custom column updates
	IF ISNULL(@updateValuesSql, '') <> '' BEGIN
		SET @sSQL = N'
			UPDATE cc
				SET
					[Label]					= temp.[Label],
					[DataType]				= temp.[DataType],
					[DisplayOrder]			= temp.[DisplayOrder],
					[InitialValue]			= temp.[InitialValue],
					[DatabaseField]			= temp.[DatabaseField],
					[ColumnUpdate]			= temp.[ColumnUpdate],
					[Width]					= temp.[Width],
					[Decimals]				= temp.[Decimals],
					[ViewRoles]				= temp.[ViewRoles],
					[EditRoles]				= temp.[EditRoles],
					[RequiredRoles]			= temp.[RequiredRoles],
					[RequiredExpression]	= temp.[RequiredExpression],
					[EnabledWBS1]			= temp.[EnabledWBS1],
					[EnabledWBS2]			= temp.[EnabledWBS2],
					[EnabledWBS3]			= temp.[EnabledWBS3],
					[Status]				= temp.[Status],
					[ModDate]				= temp.[ModDate],
					[ModUser]				= temp.[ModUser],
					[Link]					= temp.[Link],
					[Description]			= temp.[Description],
					[EditOptions]			= temp.[EditOptions]
				FROM CCG_PAT_ConfigCustomColumns cc
				JOIN (
					VALUES ' + @updateValuesSql + '
				) temp (' + @fieldsSQL + ', Seq) ON temp.Seq = cc.Seq';
		--PRINT @sSQL;
		EXEC (@sSQL);
	END;

	-- Custom column inserts
	IF ISNULL(@insertValuesSql, '') <> '' BEGIN
		SET @sSQL = N'
			INSERT INTO CCG_PAT_ConfigCustomColumns
				(' + @fieldsSQL + ')
				VALUES
				' + @insertValuesSql;
		--PRINT @sSQL;
		EXEC (@sSQL);
	END;

	-- Do custom column deletes
	IF ISNULL(@deleteSeqs, '') <> '' BEGIN
		SET @sSQL = N'
			DELETE FROM CCG_PAT_ConfigCustomColumns WHERE [Seq] IN (' + @deleteSeqs + ');
			DELETE FROM CCG_PAT_ConfigCustomColumnsDescriptions WHERE [Seq] IN (' + @deleteSeqs + '); ';
		--PRINT @sSQL;
		EXEC (@sSQL);
	END;

	-- Update the multilingual descriptions table
	SET @sSQL = N'
		DELETE a FROM CCG_PAT_ConfigCustomColumnsDescriptions a LEFT JOIN CCG_PAT_ConfigCustomColumns b ON a.Seq = b.Seq
			WHERE b.Seq IS NULL		-- No match

		MERGE CCG_PAT_ConfigCustomColumnsDescriptions
			WITH (HOLDLOCK) AS Target
		USING CCG_PAT_ConfigCustomColumns AS Source ON Target.Seq = Source.Seq AND Target.UICultureName = '''+@VISION_LANGUAGE+'''
		WHEN MATCHED THEN
			UPDATE SET Target.Label = Source.Label, Target.Description = Source.Description
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (Seq, UICultureName, Label, Description)
				VALUES (Source.Seq, '''+@VISION_LANGUAGE+''', Source.Label, Source.Description)
		WHEN NOT MATCHED BY SOURCE AND Target.UICultureName = '''+@VISION_LANGUAGE+''' THEN DELETE;';

	EXEC(@sSQL);

	COMMIT TRANSACTION;
END;
GO
