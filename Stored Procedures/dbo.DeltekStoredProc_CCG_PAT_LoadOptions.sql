SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_LoadOptions] (
	@useFN		bit,
	@user		Nvarchar(32),
	@role		Nvarchar(30)
) AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_LoadOptions] 0, '', ''

	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max);

	SET @sql = N'
		SELECT Seq, ChangedBy, DateChanged, Description, Detail, ' +
			(CASE
				WHEN @useFN = 1 THEN 'dbo.fnCCG_PAT_BatchLoadOptions(Seq, N''' + @user + ''', N''' + @role + ''')'
				ELSE '-1' END) + ' as Rights
            FROM CCG_PAT_ConfigOptions
            WHERE type = ''BatchImportSource''
            ORDER BY datechanged desc';

	PRINT @sql
	EXEC(@sql);
END;

GO
