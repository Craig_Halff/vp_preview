SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_ProjectStructureValidation] @WBS1 varchar (30)
AS
/*
Copyright 2021 Central Consulting Group.   All rights reserved.
08/30/2021	David Springer
			Call this from a Project CHANGE workflow when Create Project Structure is checked
*/
DECLARE @WBS2	varchar (7),
		@Error	varchar (8000)
BEGIN
SET NOCOUNT ON

If not exists (Select 'x' From ProjectCustomTabFields Where WBS1 = @WBS1 and CustContractType = 'Individual Project/Task Order')
	Begin
	Delete From Projects_Tasks Where WBS1 = @WBS1
	Delete From Projects_CustPhaseTeams Where WBS1 = @WBS1
	Delete From Projects_Phases Where WBS1 = @WBS1
	End
Else
	Begin
    IF NOT EXISTS (SELECT * FROM dbo.Projects_Phases WHERE WBS1 = @WBS1)
        RAISERROR('<h3>Phases must be setup in the Project Setup tab before changing to the Create AVO stage.</h3>', 16, 1);
    --Check to see if the Phase - Teams grid is populated
    IF NOT EXISTS (SELECT * FROM dbo.Projects_CustPhaseTeams WHERE WBS1 = @WBS1)
        RAISERROR('<h3>Phase - Teams must be setup in the Project Setup tab before changing to the Create AVO stage.</h3>', 16, 1);

	Select @WBS2 = p.PhaseCode
	From 
		(Select WBS1, CustPhaseCode PhaseCode, Sum (CustPhaseFeeDirLab) PhaseLabor
		From Projects_Phases 
		Where WBS1 = @WBS1 and WBS2 = ' ' 
		Group by WBS1, CustPhaseCode
		) p
		Left Join
		(Select WBS1, CustTeamPhaseCode PhaseCode, Sum (CustTeamLaborFee) TeamLabor
		From Projects_CustPhaseTeams 
		Where WBS2 = ' ' 
		Group by WBS1, CustTeamPhaseCode
		) t on t.WBS1 = p.WBS1 and t.PhaseCode = p.PhaseCode
	Where p.PhaseLabor <> IsNull (t.TeamLabor, 0)

	If @WBS2 is not null
		Begin
		Set @Error = '<br><h3>Halff Custom Error:  Sum of Team Labor Fees does equal the Phase Labor Fee for:<br>Phase: ' + @WBS2 + '.'
		RAISERROR (@Error, 16, 1); -- 16 = severity; 1 = state
		End

	If exists (Select 'x' From Projects_Tasks Where WBS1 = @WBS1 and WBS2 = ' ')
		Begin
		If exists (
			Select 'x'
			From 
				(Select WBS1, CustPhaseCode PhaseCode, Sum (CustPhaseFeeDirLab) PhaseLabor
				From Projects_Phases 
				Where WBS1 = @WBS1 and WBS2 = ' ' 
				Group by WBS1, CustPhaseCode
				) p
				Join
				(Select WBS1, CustTaskPhaseCode PhaseCode, Sum (CustTaskFeeDirLab) TaskLabor
				From Projects_Tasks
				Where WBS2 = ' ' 
				Group by WBS1, CustTaskPhaseCode
				) t on t.WBS1 = p.WBS1 and t.PhaseCode = p.PhaseCode
			Where p.PhaseLabor <> t.TaskLabor)
			RAISERROR ('<br><h3>Halff Custom Error:  Task Labor Fee does equal the Phase Labor Fee.</h3>', 16, 1); -- 16 = severity; 1 = state

		If exists (
			Select 'x'
			From 
				(Select WBS1, CustPhaseCode PhaseCode, Sum (CustPhaseTotal) PhaseTotal
				From Projects_Phases 
				Where WBS1 = @WBS1 and WBS2 = ' ' 
				Group by WBS1, CustPhaseCode
				) p
				Join
				(Select WBS1, CustTaskPhaseCode PhaseCode, Sum (CustTaskTotal) TaskTotal
				From Projects_Tasks
				Where WBS2 = ' ' 
				Group by WBS1, CustTaskPhaseCode
				) t on t.WBS1 = p.WBS1 and t.PhaseCode = p.PhaseCode
			Where p.PhaseTotal <> t.TaskTotal)
			RAISERROR ('<br><h3>Halff Custom Error:  Task Total Fee does equal the Phase Total Fee.</h3>', 16, 1); -- 16 = severity; 1 = state
		End
	End
END

GO
