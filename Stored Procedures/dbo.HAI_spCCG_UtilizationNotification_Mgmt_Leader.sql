SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- EDIT 2 LINES BELOW BASED ON WHICH UTILIZATION
CREATE Procedure [dbo].[HAI_spCCG_UtilizationNotification_Mgmt_Leader]
@Email VARCHAR(100),
@RetVal NVARCHAR(MAX) OUTPUT

--COMMENTED TWO CODE LINES OUT BELOW
--CREATE Procedure [dbo].[HAI_spCCG_UtilizationNotification_Mgmt_Leader_TEST]
AS
/*
- Created on 5/14/2018 by Conrad Harrison
- Updated on: 2018-10-17 by Conrad Harrison - Changed "Variance" to "Diff" in the headers, return blank values when expected hours = 0, and adjust color coding for red/black
*/

SET NOCOUNT ON
DECLARE --@Email								varchar (100),
		@Team								varchar (100),
	    @TeamLeader							varchar (100),
		@ManagementLeader					varchar (100),
		@PreviousTeamLeader					varchar (100),
		@RowCount							integer,
		@Director							varchar (100),
		@OpsMngr							varchar (100),
		@PracticeLeader						varchar (100),
		@MgmtLeader							varchar (100),
		@Office								varchar (100),
		@Practice							varchar (100),
		@LastName							varchar (50),
		@FirstName							varchar (50),
		@PreferredName						varchar (50),
		@Employee							varchar (100),
		@CustTargetRatio					decimal (19,1),
		@Cust1WeekTargetRatio				decimal (19,1),
		@Cust4WeekTargetRatio				decimal (19,1),
		@CustAnnualTargetRatio				decimal (19,1),
		@Cust1WeekUtilization				decimal (19,1),
		@Diff1W								decimal (19,1),
		@Cust4WeekUtilization				decimal (19,1),
		@Diff4W								decimal (19,1),
		@CustAnnualUtilization				decimal (19,1),
		@DiffAnnual							decimal (19,1),
		@PercentSymbol						varchar (5),
		@Body1								varchar (max),
		@CSS            					varchar (max),
        @Loop1								integer,
		@redText							varchar(50),
		@redTextLW							varchar(50),
		@redTextLWF							varchar(50),  
		@redText4W							varchar(50),  
		@redText4WF							varchar(50),  
		@redText12Wr						varchar(50),  
		@redText12WrF						varchar(50),
		@total1								DECIMAL (19,1),
		@total2								DECIMAL (19,1),
		@total3								DECIMAL (19,1),
		@total4								DECIMAL (19,1),
		@total5								DECIMAL (19,1),
		@total6								DECIMAL (19,1),
		@total7								DECIMAL (19,1),
		@total8								DECIMAL (19,1),
		@total9								DECIMAL (19,1),
		--**ADDED IN FOR BLANK INPUTS
		@Cust1WeekUtilizationTableInsert	varchar(100),
		@Cust4WeekUtilizationTableInsert	varchar(100),
		@CustAnnualUtilizationTableInsert	varchar(100),
		@Diff1WTableInsert					varchar(100),
		@Diff4WTableInsert					varchar(100),
		@DiffAnnualTableInsert				varchar(100),
		@CustTargetRatio1WTableInsert		varchar(100),
		@CustTargetRatio4WTableInsert		varchar(100),
		@CustTargetRatioAnnualTableInsert	varchar(100)       

		SET @RowCount = 0
		Set @PercentSymbol = '%'
		SET @total1 = 0
		SET @total2 = 0
		SET @total3 = 0
		SET @total4 = 0
		SET @total5 = 0
		SET @total6 = 0
		SET @total7 = 0
		SET @total8 = 0
		SET @total9 = 0
						--**ADDED IN FOR BLANK INPUTS
		SET @Cust1WeekUtilizationTableInsert = ''
		SET @Cust1WeekUtilizationTableInsert = ''
		SET @Cust4WeekUtilizationTableInsert = ''
		SET @CustAnnualUtilizationTableInsert = ''
		SET @Diff1WTableInsert = ''
		SET @Diff4WTableInsert = ''
		SET @DiffAnnualTableInsert = ''
		SET @CustTargetRatio1WTableInsert = ''
		SET @CustTargetRatio4WTableInsert = ''
		SET @CustTargetRatioAnnualTableInsert = ''
					
BEGIN

-- CSS style rules

--	SET @CSS = 
--'
--<style type="text/css">
--	--p {padding-top:15px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;}
--	th {border: 2px solid black;padding: 5px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;font-size:10.5pt;}
--	th {background-color: #808080;color: white;text-align:center;}
--	.team {text-align:left; border-left: 2px solid Black; background-color: white; border-bottom: 1px solid #6E6E6E;}
--	.teamB {font-weight: bold; text-align:left; border-left: 2px solid Black; background-color: white; border-bottom: 1px solid #6E6E6E;}
--	.LW {color: black; background-color: white; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
--	.L4W {color: black; background-color: #e6e6e6; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;} 
--	.L12W {background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
--	.L12Wr {color: black; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-right: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
--	.LWF {color: black; font-weight: bold; background-color: white; text-align:right; border-left: 2px solid Black; border-top: 2px solid Black;}
--	.LWFLeft {font-weight: bold; background-color: white; text-align:left; border-left: 2px solid Black; border-top: 2px solid Black;}
--	.L4WF {color: black; font-weight: bold; background-color: #e6e6e6; text-align:right; border-left: 2px solid Black; border-top: 2px solid Black;} 
--	.L12WF {font-weight: bold; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-top: 2px solid Black;}
--	.L12WrF {color: black; font-weight: bold; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-right: 2px solid Black; border-top: 2px solid Black;}
--	.redText {color:#FF0000;}

--	</style>
--'

DECLARE curUtilization insensitive cursor for
--  Management Leader Utilization
Select oe.Email, 
	oe.LastName + ', ' + IsNull (oe.PreferredName, oe.FirstName) ManagementLeader, 
	org.Code + ' - ' + org.Label Team,
	CASE When Sum (u.Cust1WeekHoursExpected) = 0 Then 0 
		Else Sum (u.CustTargetRatio * u.Cust1WeekHoursExpected) / Sum (u.Cust1WeekHoursExpected) 
		END Cust1WeekTargetRatio,
	CASE When Sum (u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit) = 0 Then 0 
		Else Sum (u.Cust1WeekHoursDirect) / Sum (u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit) * 100 
		END Cust1WeekUtilization,
	CASE When Sum (u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit) = 0 Then 0 
		Else (Sum (u.Cust1WeekHoursDirect) / Sum (u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit) * 100) - Sum (u.CustTargetRatio * u.Cust1WeekHoursExpected) / Sum (u.Cust1WeekHoursExpected) 
		END Diff, 
	CASE When Sum (u.Cust4WeekHoursExpected) = 0 Then 0 
		Else Sum (u.CustTargetRatio * u.Cust4WeekHoursExpected) / Sum (u.Cust4WeekHoursExpected) 
		END Cust4WeekTargetRatio,
	CASE When Sum (u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit) = 0 Then 0 
		Else Sum (u.Cust4WeekHoursDirect) / Sum (u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit) * 100 
		END Cust4WeekUtilization,
	CASE When Sum (u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit) = 0 Then 0 
		Else (Sum (u.Cust4WeekHoursDirect) / Sum (u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit) * 100) - Sum (u.CustTargetRatio * u.Cust4WeekHoursExpected) / Sum (u.Cust4WeekHoursExpected) 
		END Diff, 
	CASE When Sum (u.CustAnnualHoursExpected) = 0 Then 0 
		Else Sum (u.CustTargetRatio * u.CustAnnualHoursExpected) / Sum (u.CustAnnualHoursExpected) 
		END CustAnnualTargetRatio,
	CASE When Sum (u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit) = 0 Then 0 
		Else Sum (u.CustAnnualHoursDirect) / Sum (u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit) * 100 
		END CustAnnualUtilization,
	CASE When Sum (u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit) = 0 Then 0 
		Else (Sum (u.CustAnnualHoursDirect) / Sum (u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit) * 100) - Sum (u.CustTargetRatio * u.CustAnnualHoursExpected) / Sum (u.CustAnnualHoursExpected) 
		END Diff
From UDIC_Utilization u, 
	CFGOrgCodes org, 
	EM e, 
	EmployeeCustomTabFields  ex
	Left Join EM oe on oe.Employee = ex.CustEKManagementLeader
Where u.CustEmployeeNumber = e.Employee
  and e.Employee = ex.Employee
  and Right (e.org, 3) = org.Code
  and org.OrgLevel = '3'
  and oe.Email = @Email
Group by oe.Email, 
	oe.LastName + ', ' + IsNull (oe.PreferredName, oe.FirstName), 
	org.Code, 
	org.Label
Order by 2, 3
	
			Set @Body1 = 
'<table cellpadding=''5'' cellspacing=''0'' style=''border:1px solid black;border-collapse:collapse;''>
	<thead>
	    <tr>
               	<th rowspan="2">Name</th>
                <th colspan="3">Last Week</a></th>
                <th colspan="3">Last 4 Weeks</a></th>
                <th colspan="3">Last 12 Months</a></th>
            </tr>
            <tr>
                <th>Actual</th>
                <th>Goal</th>
                <th>Diff</th>
                <th>Actual</th>
                <th>Goal</th>
                <th>Diff</th>
                <th>Actual</th>
                <th>Goal</th>
                <th>Diff</th>
            </tr>
	</thead>
	<tbody>
'	
    -- Get each Utilization
	OPEN curUtilization
	FETCH NEXT FROM curUtilization INTO @Email
	, @ManagementLeader
	, @Team
	, @Cust1WeekTargetRatio
	, @Cust1WeekUtilization 
	, @Diff1W
	, @Cust4WeekTargetRatio
	, @Cust4WeekUtilization
	, @Diff4W
	, @CustAnnualTargetRatio
	, @CustAnnualUtilization
	, @DiffAnnual

	Set @Loop1 = @@FETCH_STATUS
	While @Loop1 = 0
						
	BEGIN

				SET @RowCount = @RowCount + 1

				Select @redTextLW = (Case
								When @Cust1WeekUtilization < @Cust1WeekTargetRatio Then ' style="color:#FF0000;"'
								When @Cust1WeekUtilization >= @Cust1WeekTargetRatio Then '' End)

				Select @redText4W = (Case
								When @Cust4WeekUtilization < @Cust4WeekTargetRatio Then ' style="color:#FF0000;"'
								When @Cust4WeekUtilization >= @Cust4WeekTargetRatio Then '' End)

				Select @redText12Wr = (Case
								When @CustAnnualUtilization < @CustAnnualTargetRatio Then ' style="color:#FF0000;"'
								When @CustAnnualUtilization >= @CustAnnualTargetRatio Then '' End)

				--**ADDED IN FOR BLANK INPUTS
				Select @Cust1WeekUtilizationTableInsert = (Case 
								When @Cust1WeekUtilization = 0 Then '&nbsp'
								When @Cust1WeekUtilization is null Then '&nbsp'
								When @Cust1WeekUtilization != 0 Then (CAST(@Cust1WeekUtilization AS VARCHAR(10)) + @PercentSymbol) END)

				Select @Cust4WeekUtilizationTableInsert = (Case 
								When @Cust4WeekUtilization = 0 Then '&nbsp'
								When @Cust4WeekUtilization is null Then '&nbsp'
								When @Cust4WeekUtilization != 0 Then (CAST(@Cust4WeekUtilization AS VARCHAR(10)) + @PercentSymbol) END)

				Select @CustAnnualUtilizationTableInsert = (Case 
								When @CustAnnualUtilization = 0 Then '&nbsp'
								When @CustAnnualUtilization is null Then '&nbsp'
								When @CustAnnualUtilization != 0 Then (CAST(@CustAnnualUtilization AS VARCHAR(10)) + @PercentSymbol) END)

				--**ADDED IN FOR BLANK INPUTS
				Select @Diff1WTableInsert = (Case 
								When @Cust1WeekUtilization = 0 Then '&nbsp'
								When @Cust1WeekUtilization is null Then '&nbsp'
								When @Cust1WeekUtilization != 0 Then (CAST(@Diff1W AS VARCHAR(10)) + @PercentSymbol) END)

				Select @Diff4WTableInsert = (Case 
								When @Cust4WeekUtilization = 0 Then '&nbsp'
								When @Cust4WeekUtilization is null Then '&nbsp'
								When @Cust4WeekUtilization != 0 Then (CAST(@Diff4W AS VARCHAR(10)) + @PercentSymbol) END)

				Select @DiffAnnualTableInsert = (Case 
								When @CustAnnualUtilization = 0 Then '&nbsp'
								When @CustAnnualUtilization is null Then '&nbsp'
								When @CustAnnualUtilization != 0 Then (CAST(@DiffAnnual AS VARCHAR(10)) + @PercentSymbol) END)

								--**ADDED IN FOR BLANK INPUTS
				Select @CustTargetRatio1WTableInsert = (Case 
								When @Cust1WeekUtilization = 0 Then '&nbsp'
								When @Cust1WeekUtilization is null Then '&nbsp'
								When @Cust1WeekUtilization != 0 Then (CAST(@Cust1WeekTargetRatio AS VARCHAR(10)) + @PercentSymbol) END)

				Select @CustTargetRatio4WTableInsert = (Case 
								When @Cust4WeekUtilization = 0 Then '&nbsp'
								When @Cust4WeekUtilization is null Then '&nbsp'
								When @Cust4WeekUtilization != 0 Then (CAST(@Cust4WeekTargetRatio AS VARCHAR(10)) + @PercentSymbol) END)

				Select @CustTargetRatioAnnualTableInsert = (Case 
								When @CustAnnualUtilization = 0 Then '&nbsp'
								When @CustAnnualUtilization is null Then '&nbsp'
								When @CustAnnualUtilization != 0 Then (CAST(@CustAnnualTargetRatio AS VARCHAR(10)) + @PercentSymbol) END)
												
			Set @Body1 = @Body1 + 
            
'				<tr>
                    <td class="team">' + @Team + '</td> 
                    <td class="LW">' + @Cust1WeekUtilizationTableInsert + '</td>
                    <td class="LW">' + @CustTargetRatio1WTableInsert + '</td>
					<td class="LW" ' + @redTextLW + '>' + @Diff1WTableInsert + '</td>
					<td class="L4W">' + @Cust4WeekUtilizationTableInsert + '</td>
                    <td class="L4W">' + @CustTargetRatio4WTableInsert + '</td>
                    <td class="L4W" ' + @redText4W + '>' + @Diff4WTableInsert + '</td>
                    <td class="L12W">' + @CustAnnualUtilizationTableInsert + '</td>
                    <td class="L12W">' + @CustTargetRatioAnnualTableInsert + '</td>
                    <td class="L12Wr" ' + @redText12Wr + '>' + @DiffAnnualTableInsert + '</td>
                </tr>
'
	
	FETCH NEXT FROM curUtilization INTO @Email,
	@ManagementLeader
	, @Team
	, @Cust1WeekTargetRatio
	, @Cust1WeekUtilization 
	, @Diff1W
	, @Cust4WeekTargetRatio
	, @Cust4WeekUtilization
	, @Diff4W
	, @CustAnnualTargetRatio
	, @CustAnnualUtilization
	, @DiffAnnual

	Set @Loop1 = @@FETCH_STATUS

	END
	CLOSE curUtilization
	DEALLOCATE curUtilization

	SET @Body1 = @Body1 + '</tbody>'

			SELECT @total1 = Cust1WeekTargetRatio,
			@total2 = Cust1WeekUtilization,
			@total3 = Diff1W,
			@total4 = Cust4WeekTargetRatio,
			@total5 = Cust4WeekUtilization,
			@total6 = Diff4w,
			@total7 = CustAnnualTargetRatio,
			@total8 = CustAnnualUtilization,
			@total9 = DiffAnnual
	
			FROM
(
Select oe.Email, 
oe.LastName + ', ' + IsNull (oe.PreferredName, oe.FirstName) ManagementLeader, 
	CASE When Sum (u.Cust1WeekHoursExpected) = 0 Then 0 
		Else Sum (u.CustTargetRatio * u.Cust1WeekHoursExpected) / Sum (u.Cust1WeekHoursExpected) 
		END Cust1WeekTargetRatio,
	CASE When Sum (u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit) = 0 Then 0 
		Else Sum (u.Cust1WeekHoursDirect) / Sum (u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit) * 100 
		END Cust1WeekUtilization,
	CASE When Sum (u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit) = 0 Then 0 
		Else (Sum (u.Cust1WeekHoursDirect) / Sum (u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit) * 100) - Sum (u.CustTargetRatio * u.Cust1WeekHoursExpected) / Sum (u.Cust1WeekHoursExpected) 
		END Diff1W, 
	CASE When Sum (u.Cust4WeekHoursExpected) = 0 Then 0 
		Else Sum (u.CustTargetRatio * u.Cust4WeekHoursExpected) / Sum (u.Cust4WeekHoursExpected) 
		END Cust4WeekTargetRatio,
	CASE When Sum (u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit) = 0 Then 0 
		Else Sum (u.Cust4WeekHoursDirect) / Sum (u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit) * 100 
		END Cust4WeekUtilization,
	CASE When Sum (u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit) = 0 Then 0 
		Else (Sum (u.Cust4WeekHoursDirect) / Sum (u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit) * 100) - Sum (u.CustTargetRatio * u.Cust4WeekHoursExpected) / Sum (u.Cust4WeekHoursExpected) 
		END Diff4W, 
	CASE When Sum (u.CustAnnualHoursExpected) = 0 Then 0 
		Else Sum (u.CustTargetRatio * u.CustAnnualHoursExpected) / Sum (u.CustAnnualHoursExpected) 
		END CustAnnualTargetRatio,
	CASE When Sum (u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit) = 0 Then 0 
		Else Sum (u.CustAnnualHoursDirect) / Sum (u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit) * 100 
		END CustAnnualUtilization,
	CASE When Sum (u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit) = 0 Then 0 
		Else (Sum (u.CustAnnualHoursDirect) / Sum (u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit) * 100) - Sum (u.CustTargetRatio * u.CustAnnualHoursExpected) / Sum (u.CustAnnualHoursExpected) 
		END DiffAnnual
From UDIC_Utilization u, EM e, EmployeeCustomTabFields  ex
	Left Join EM oe on oe.Employee = ex.CustEKManagementLeader
Where u.CustEmployeeNumber = e.Employee
  and e.Employee = ex.Employee
  and oe.Email = @Email
Group by oe.Email, 
	oe.LastName + ', ' + IsNull (oe.PreferredName, oe.FirstName)
	--ADDED >= over > below to adjust for only one group
Having count (distinct Right (e.org, 3)) >= 1

  ) AS A

  Select @redTextLWF = (Case
								When @total3 < 0 Then ' style="color:#FF0000;"'
								When @total3 >= 0 Then '' End)

				Select @redText4WF = (Case
								When @total6 < 0 Then ' style="color:#FF0000;"'
								When @total6 >= 0 Then '' End)

				Select @redText12WrF = (Case
								When @total9 < 0 Then ' style="color:#FF0000;"'
								When @total9 >= 0 Then '' End)

	Set @Body1 = @Body1 + 
'
	    <tfoot>
            <tr>
                <td class="LWFleft" >Final Totals</td>
                <td class="LWF">' + CAST(@total2 AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="LWF">' + CAST(@total1 AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="LWF" ' + @redTextLWF + '>' + CAST(@total3 AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L4WF">' + CAST(@total5 AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L4WF">' + CAST(@total4 AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L4WF" ' + @redText4WF + '>' + CAST(@total6 AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L12WF">' + CAST(@total8 AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L12WF">' + CAST(@total7 AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L12WrF" ' + @redText12WrF + '>' + CAST(@total9 AS VARCHAR(10)) + @PercentSymbol + '</td>
            </tr>  
      </tfoot>
	  </table>
	  <br/>
'

	SET @RetVal = @Body1;

	--Email Recipient Info Below
	--Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
	--Values ('Management Leader Utilization', 'charrison@halff.com', 'charrison@halff.com', 'charrison@halff.com', 'Management Leader Utilization', @CSS + @Body, getDate(), 1)
	
END
GO
