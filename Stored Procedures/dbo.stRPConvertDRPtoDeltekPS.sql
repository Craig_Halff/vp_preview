SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPConvertDRPtoDeltekPS]
  @strDelimiter AS nvarchar(1)
AS

BEGIN -- Procedure stRPConvertDRPtoDeltekPS

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to convert data from DRP format to DeltekPS format. 
-- The DRP DB must have DeltekPS 1.0 Schema applied already.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Declare Temp tables.

  DECLARE @tabTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1WBS2WBS3 nvarchar(255) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    WBSType varchar(4) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int
    UNIQUE (PlanID, TaskID, WBS1, WBS2, WBS3, OutlineNumber)
  )

  DECLARE @tabLD TABLE (
    Period int,
    PostSeq int,
    PKey varchar(32) COLLATE database_default,
    WBS1WBS2WBS3 nvarchar(255) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default
    UNIQUE (Period, PostSeq, PKey)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabTask(
    PlanID,
    TaskID,
    WBS1WBS2WBS3,
    WBS1,
    WBS2,
    WBS3,
    WBSType,
    OutlineNumber,
    OutlineLevel
  )
 
    SELECT
      PlanID,
      TaskID,
      WBS1 AS WBS1WBS2WBS3,

      LEFT(WBS1, CASE WHEN CHARINDEX(@strDelimiter, WBS1) > 0 THEN CHARINDEX(@strDelimiter, WBS1) - 1 ELSE LEN(WBS1) END) AS WBS1,

      CASE 
        WHEN (CHARINDEX(@strDelimiter, WBS1) > 0) AND (CHARINDEX(@strDelimiter, WBS1, CHARINDEX(@strDelimiter, WBS1) + 1) - CHARINDEX(@strDelimiter, WBS1) > 0)
        THEN 
          SUBSTRING(
            WBS1, 
            CHARINDEX(@strDelimiter, WBS1) + 1, 
            CHARINDEX(@strDelimiter, WBS1, CHARINDEX(@strDelimiter, WBS1) + 1) - CHARINDEX(@strDelimiter, WBS1) - 1
          )
        WHEN (CHARINDEX(@strDelimiter, WBS1) > 0) AND (CHARINDEX(@strDelimiter, WBS1, CHARINDEX(@strDelimiter, WBS1) + 1) = 0)
        THEN 
          SUBSTRING(
            WBS1, 
            CHARINDEX(@strDelimiter, WBS1) + 1, 
            LEN(WBS1)
          )
       ELSE NULL
      END AS WBS2,

      CASE 
        WHEN (CHARINDEX(@strDelimiter, WBS1) > 0) AND (CHARINDEX(@strDelimiter, WBS1, CHARINDEX(@strDelimiter, WBS1) + 1) > 0)
        THEN 
          SUBSTRING(
            WBS1, 
            CHARINDEX(@strDelimiter, WBS1, CHARINDEX(@strDelimiter, WBS1) + 1) + 1,
            LEN(WBS1)
          )
        ELSE NULL
      END AS WBS3,

      CASE OutlineLevel
        WHEN 0 THEN 'WBS1'
        WHEN 1 THEN 'WBS2'
        WHEN 2 THEN 'WBS3'
        ELSE NULL
      END AS WBSType, 

      OutlineNumber,
      OutlineLevel

      FROM RPTask AS T
      WHERE WBS1 <> '<none>' 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save off rows in LD table.
  -- Rows in LD table have unique PK {Period, PostSeq, PKey}
  -- WBS2 and WBS3 in LD table are <blank> and not NULL like in RPTask/PNTask table.

  INSERT @tabLD(
    Period,
    PostSeq,
    PKey,
    WBS1WBS2WBS3,
    WBS1,
    WBS2,
    WBS3
  )
 
    SELECT
      Period,
      PostSeq,
      PKey,
      WBS1 AS WBS1WBS2WBS3,

      LEFT(WBS1, CASE WHEN CHARINDEX(@strDelimiter, WBS1) > 0 THEN CHARINDEX(@strDelimiter, WBS1) - 1 ELSE LEN(WBS1) END) AS WBS1,

      CASE 
        WHEN (CHARINDEX(@strDelimiter, WBS1) > 0) AND (CHARINDEX(@strDelimiter, WBS1, CHARINDEX(@strDelimiter, WBS1) + 1) - CHARINDEX(@strDelimiter, WBS1) > 0)
        THEN 
          SUBSTRING(
            WBS1, 
            CHARINDEX(@strDelimiter, WBS1) + 1, 
            CHARINDEX(@strDelimiter, WBS1, CHARINDEX(@strDelimiter, WBS1) + 1) - CHARINDEX(@strDelimiter, WBS1) - 1
          )
        WHEN (CHARINDEX(@strDelimiter, WBS1) > 0) AND (CHARINDEX(@strDelimiter, WBS1, CHARINDEX(@strDelimiter, WBS1) + 1) = 0)
        THEN 
          SUBSTRING(
            WBS1, 
            CHARINDEX(@strDelimiter, WBS1) + 1, 
            LEN(WBS1)
          )
       ELSE ' '
      END AS WBS2,

      CASE 
        WHEN (CHARINDEX(@strDelimiter, WBS1) > 0) AND (CHARINDEX(@strDelimiter, WBS1, CHARINDEX(@strDelimiter, WBS1) + 1) > 0)
        THEN 
          SUBSTRING(
            WBS1, 
            CHARINDEX(@strDelimiter, WBS1, CHARINDEX(@strDelimiter, WBS1) + 1) + 1,
            LEN(WBS1)
          )
        ELSE ' '
      END AS WBS3

      FROM LD

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  UPDATE T SET
    WBS1 = XT.WBS1,
    WBS2 = XT.WBS2,
    WBS3 = XT.WBS3,
    WBSType = XT.WBSType
  FROM RPTask AS T
    INNER JOIN @tabTask AS XT ON T.PlanID = XT.PlanID AND T.TaskID = XT.TaskID

  UPDATE A SET
    WBS1 = T.WBS1,
    WBS2 = T.WBS2,
    WBS3 = T.WBS3
  FROM RPAssignment AS A
    INNER JOIN RPTask AS T ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID

  UPDATE P SET
    WBS1 = T.WBS1
  FROM RPPlan AS P
    INNER JOIN RPTask AS T ON P.PlanID = T.PlanID
    WHERE T.WBS2 IS NULL AND T.WBS3 IS NULL AND OutlineLevel = 0 AND WBSType = 'WBS1' AND T.WBS1 <> '<none>' 

  UPDATE LD SET
    WBS1 = XLD.WBS1,
    WBS2 = XLD.WBS2,
    WBS3 = XLD.WBS3
  FROM LD AS LD
    INNER JOIN @tabLD AS XLD ON LD.Period = XLD.Period AND LD.PostSeq = XLD.PostSeq AND LD.PKey = XLD.PKey

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT PR (
    WBS1,
    WBS2,
    WBS3,
    Name,
    LongName,
    ChargeType,
    SubLevel,
    ProjMgr,
    Org,
    ClientID,
    Status,
    StartDate,
    EndDate,
    ProjectCurrencyCode,
    BillingCurrencyCode
  )
    SELECT
      T.WBS1 AS WBS1,
      ISNULL(T.WBS2, ' ') AS WBS2,
      ISNULL(T.WBS3, ' ') AS WBS3,
      LEFT(T.Name, 40) AS Name,
      T.Name AS LongName,
      T.ChargeType AS ChargeType,
      CASE
        WHEN (T.ChildrenCount > 0 AND OutlineLevel < 2)
        THEN 'Y'
        ELSE 'N'
      END AS SubLevel,
      T.ProjMgr AS ProjMgr,
      T.Org AS Org,
      T.ClientID AS ClientID,
      T.Status AS Status,
      T.StartDate AS StartDate,
      T.EndDate AS EndDate,
      P.CostCurrencyCode AS ProjectCurrencyCode,
      P.BillingCurrencyCode AS BillingCurrencyCode
      FROM RPTask AS T
        INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID
      WHERE T.WBS1 <> '<none>' AND T.OutlineLevel <= 2

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT PNPlan (
    PlanID,
    PlanName,
    PlanNumber,
    OpportunityID,
    ClientID,
    ProjMgr,
    Principal,
    Supervisor,
    Org,
    StartDate,
    EndDate,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    BaselineExpCost,
    BaselineExpBill,
    BaselineConCost,
    BaselineConBill,
    BaselineRevenue,
    BaselineStart,
    BaselineFinish,
    PlannedLaborHrs,
    PlannedLabCost,
    PlannedLabBill,
    PlannedExpCost,
    PlannedExpBill,
    PlannedConCost,
    PlannedConBill,
    LabRevenue,
    PlannedDirExpCost,
    PlannedDirExpBill,
    PlannedDirConCost,
    PlannedDirConBill,
    CostRtMethod,
    BillingRtMethod,
    CostRtTableNo,
    BillingRtTableNo,
    ContingencyPct,
    ContingencyAmt,
    OverheadPct,
    AvailableFlg,
    GenResTableNo,
    UnPostedFlg,
    BudgetType,
    PctCompleteFormula,
    PctComplete,
    PctCompleteBill,
    PctCompleteLabCost,
    PctCompleteLabBill,
    PctCompleteExpCost,
    PctCompleteExpBill,
    PctCompleteConCost,
    PctCompleteConBill,
    WeightedLabCost,
    WeightedLabBill,
    WeightedExpCost,
    WeightedExpBill,
    WeightedConCost,
    WeightedConBill,
    UtilizationIncludeFlg,
    WBS1,
    Probability,
    LabMultType,
    Multiplier,
    ProjectedMultiplier,
    ProjectedRatio,
    CalcExpBillAmtFlg,
    CalcConBillAmtFlg,
    ExpBillRtMethod,
    ExpBillRtTableNo,
    ConBillRtMethod,
    ConBillRtTableNo,
    GRBillTableNo,
    ExpBillMultiplier,
    ConBillMultiplier,
    ReimbMethod,
    ExpRevenue,
    ConRevenue,
    TargetMultCost,
    TargetMultBill,
    BaselineUntQty,
    BaselineUntCost,
    BaselineUntBill,
    PlannedUntQty,
    PlannedUntCost,
    PlannedUntBill,
    PctCompleteUntCost,
    PctCompleteUntBill,
    PlannedDirUntCost,
    PlannedDirUntBill,
    WeightedUntCost,
    WeightedUntBill,
    UntRevenue,
    Status,
    PctComplByPeriodFlg,
    RevenueMethod,
    AnalysisBasis,
    GRMethod,
    Company,
    StartingDayOfWeek,
    CostCurrencyCode,
    BillingCurrencyCode,

    EVFormula,
    LabBillMultiplier,
    UntBillMultiplier,
    BaselineDirExpCost,
    BaselineDirExpBill,
    BaselineDirConCost,
    BaselineDirConBill,
    BaselineDirUntCost,
    BaselineDirUntBill,
    CommitmentFlg,
    TopdownFee,
    ExpWBSLevel,
    ConWBSLevel,
    NeedsRecalc,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT 
      P.PlanID,
      P.PlanName AS PlanName,
      P.PlanNumber,
      P.OpportunityID AS OpportunityID,
      P.ClientID AS ClientID,
      P.ProjMgr AS ProjMgr,
      P.Principal AS Principal,
      P.Supervisor AS Supervisor,
      P.Org AS Org,
      P.StartDate,
      P.EndDate,
      BaselineLaborHrs,
      BaselineLabCost,
      BaselineLabBill,
      BaselineExpCost,
      BaselineExpBill,
      BaselineConCost,
      BaselineConBill,
      BaselineRevenue,
      BaselineStart,
      BaselineFinish,
      PlannedLaborHrs,
      PlannedLabCost,
      PlannedLabBill,
      PlannedExpCost,
      PlannedExpBill,
      PlannedConCost,
      PlannedConBill,
      LabRevenue,
      PlannedDirExpCost,
      PlannedDirExpBill,
      PlannedDirConCost,
      PlannedDirConBill,
      CostRtMethod,
      BillingRtMethod,
      CostRtTableNo,
      BillingRtTableNo,
      ContingencyPct,
      ContingencyAmt,
      OverheadPct,
      AvailableFlg,
      GenResTableNo,
      UnPostedFlg,
      BudgetType,
      PctCompleteFormula,
      PctComplete,
      PctCompleteBill,
      PctCompleteLabCost,
      PctCompleteLabBill,
      PctCompleteExpCost,
      PctCompleteExpBill,
      PctCompleteConCost,
      PctCompleteConBill,
      WeightedLabCost,
      WeightedLabBill,
      WeightedExpCost,
      WeightedExpBill,
      WeightedConCost,
      WeightedConBill,
      UtilizationIncludeFlg,
      P.WBS1,
      ISNULL(P.Probability, 0) AS Probability,
      LabMultType,
      Multiplier,
      ProjectedMultiplier,
      ProjectedRatio,
      CalcExpBillAmtFlg,
      CalcConBillAmtFlg,
      ExpBillRtMethod,
      ExpBillRtTableNo,
      ConBillRtMethod,
      ConBillRtTableNo,
      GRBillTableNo,
      ExpBillMultiplier,
      ConBillMultiplier,
      ReimbMethod,
      ExpRevenue,
      ConRevenue,
      TargetMultCost,
      TargetMultBill,
      BaselineUntQty,
      BaselineUntCost,
      BaselineUntBill,
      PlannedUntQty,
      PlannedUntCost,
      PlannedUntBill,
      PctCompleteUntCost,
      PctCompleteUntBill,
      PlannedDirUntCost,
      PlannedDirUntBill,
      WeightedUntCost,
      WeightedUntBill,
      UntRevenue,
      P.Status,
      PctComplByPeriodFlg,
      P.RevenueMethod,
      AnalysisBasis,
      GRMethod,
      Company,
      StartingDayOfWeek,
      P.CostCurrencyCode,
      P.BillingCurrencyCode,
 
      EVFormula,
      LabBillMultiplier,
      UntBillMultiplier,
      BaselineDirExpCost,
      BaselineDirExpBill,
      BaselineDirConCost,
      BaselineDirConBill,
      BaselineDirUntCost,
      BaselineDirUntBill,
      CommitmentFlg,
      TopdownFee,
      ExpWBSLevel,
      ConWBSLevel,
      NeedsRecalc,
      P.CreateUser,
      P.CreateDate,
      P.ModUser AS ModUser,
      CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate
    FROM RPPlan AS P

  INSERT PNTask (
    TaskID,
    PlanID,
    Name,
    LaborCode,
    WBS1,
    WBS2,
    WBS3,
    WBSType,
    ParentOutlineNumber,
    OutlineNumber,
    ChildrenCount,
    OutlineLevel,
    Priority,
    ProjMgr,
    ChargeType,
    ProjectType,
    ClientID,
    Status,
    Org,
    Notes,
    PctComplete,
    PctCompleteLabCost,
    PctCompleteLabBill,
    PctCompleteExpCost,
    PctCompleteExpBill,
    PctCompleteConCost,
    PctCompleteConBill,
    OverheadPct,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    BaselineExpCost,
    BaselineExpBill,
    BaselineConCost,
    BaselineConBill,
    BaselineRevenue,
    PlannedLaborHrs,
    PlannedLabCost,
    PlannedLabBill,
    PlannedExpCost,
    PlannedExpBill,
    PlannedConCost,
    PlannedConBill,
    LabRevenue,
    PlannedDirExpCost,
    PlannedDirExpBill,
    PlannedDirConCost,
    PlannedDirConBill,
    WeightedLabCost,
    WeightedLabBill,
    WeightedExpCost,
    WeightedExpBill,
    WeightedConCost,
    WeightedConBill,
    Duration,
    ActualDuration,
    BaselineDuration,
    RemainingDuration,
    ScheduleConstraint,
    StartDate,
    ActualStart,
    BaselineStart,
    EarlyStart,
    LateStart,
    ScheduledStart,
    EndDate,
    ActualFinish,
    BaselineFinish,
    EarlyFinish,
    LateFinish,
    ScheduledFinish,
    CostRate,
    BillingRate,
    ExpBillRate,
    ConBillRate,
    ExpRevenue,
    ConRevenue,
    UntCostRate,
    UntBillRate,
    BaselineUntQty,
    BaselineUntCost,
    BaselineUntBill,
    PlannedUntQty,
    PlannedUntCost,
    PlannedUntBill,
    PctCompleteUntCost,
    PctCompleteUntBill,
    PlannedDirUntCost,
    PlannedDirUntBill,
    WeightedUntCost,
    WeightedUntBill,
    UntRevenue,
    BaselineDirExpCost,
    BaselineDirExpBill,
    BaselineDirConCost,
    BaselineDirConBill,
    BaselineDirUntCost,
    BaselineDirUntBill,
    CompensationFee,
    ConsultantFee,
    ReimbAllowance,
    CompensationFeeBill,
    ConsultantFeeBill,
    ReimbAllowanceBill,
    CompensationFeeDirLab,
    CompensationFeeDirExp,
    ReimbAllowanceExp,
    ReimbAllowanceCon,
    CompensationFeeDirLabBill,
    CompensationFeeDirExpBill,
    ReimbAllowanceExpBill,
    ReimbAllowanceConBill,
    UnmatchedFlg,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT 
      TaskID,
      PlanID,
      Name,
      LaborCode,
      WBS1,
      WBS2,
      WBS3,
      WBSType,
      ParentOutlineNumber,
      OutlineNumber,
      ChildrenCount,
      OutlineLevel,
      Priority,
      ProjMgr,
      ChargeType,
      ProjectType,
      ClientID,
      Status,
      Org,
      Notes,
      PctComplete,
      PctCompleteLabCost,
      PctCompleteLabBill,
      PctCompleteExpCost,
      PctCompleteExpBill,
      PctCompleteConCost,
      PctCompleteConBill,
      OverheadPct,
      BaselineLaborHrs,
      BaselineLabCost,
      BaselineLabBill,
      BaselineExpCost,
      BaselineExpBill,
      BaselineConCost,
      BaselineConBill,
      BaselineRevenue,
      PlannedLaborHrs,
      PlannedLabCost,
      PlannedLabBill,
      PlannedExpCost,
      PlannedExpBill,
      PlannedConCost,
      PlannedConBill,
      LabRevenue,
      PlannedDirExpCost,
      PlannedDirExpBill,
      PlannedDirConCost,
      PlannedDirConBill,
      WeightedLabCost,
      WeightedLabBill,
      WeightedExpCost,
      WeightedExpBill,
      WeightedConCost,
      WeightedConBill,
      Duration,
      ActualDuration,
      BaselineDuration,
      RemainingDuration,
      ScheduleConstraint,
      StartDate,
      ActualStart,
      BaselineStart,
      EarlyStart,
      LateStart,
      ScheduledStart,
      EndDate,
      ActualFinish,
      BaselineFinish,
      EarlyFinish,
      LateFinish,
      ScheduledFinish,
      CostRate,
      BillingRate,
      ExpBillRate,
      ConBillRate,
      ExpRevenue,
      ConRevenue,
      UntCostRate,
      UntBillRate,
      BaselineUntQty,
      BaselineUntCost,
      BaselineUntBill,
      PlannedUntQty,
      PlannedUntCost,
      PlannedUntBill,
      PctCompleteUntCost,
      PctCompleteUntBill,
      PlannedDirUntCost,
      PlannedDirUntBill,
      WeightedUntCost,
      WeightedUntBill,
      UntRevenue,
      BaselineDirExpCost,
      BaselineDirExpBill,
      BaselineDirConCost,
      BaselineDirConBill,
      BaselineDirUntCost,
      BaselineDirUntBill,
      CompensationFee,
      ConsultantFee,
      ReimbAllowance,
      CompensationFeeBill,
      ConsultantFeeBill,
      ReimbAllowanceBill,
      CompensationFeeDirLab,
      CompensationFeeDirExp,
      ReimbAllowanceExp,
      ReimbAllowanceCon,
      CompensationFeeDirLabBill,
      CompensationFeeDirExpBill,
      ReimbAllowanceExpBill,
      ReimbAllowanceConBill,
      UnmatchedFlg,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    FROM RPTask

  INSERT PNAssignment (
    AssignmentID,
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    LaborCode,
    ResourceID,
    GenericResourceID,
    Category,
    GRLBCD,
    ActivityID,
    CostRate,
    BillingRate,
    PctCompleteLabCost,
    PctCompleteLabBill,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    BaselineRevenue,
    BaselineStart,
    BaselineFinish,
    PlannedLaborHrs,
    PlannedLabCost,
    PlannedLabBill,
    LabRevenue,
    WeightedLabCost,
    WeightedLabBill,
    StartDate,
    EndDate,
    SortSeq,
    HardBooked,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT 
      AssignmentID,
      PlanID,
      TaskID,
      WBS1,
      WBS2,
      WBS3,
      LaborCode,
      ResourceID,
      GenericResourceID,
      Category,
      GRLBCD,
      ActivityID,
      CostRate,
      BillingRate,
      PctCompleteLabCost,
      PctCompleteLabBill,
      BaselineLaborHrs,
      BaselineLabCost,
      BaselineLabBill,
      BaselineRevenue,
      BaselineStart,
      BaselineFinish,
      PlannedLaborHrs,
      PlannedLabCost,
      PlannedLabBill,
      LabRevenue,
      WeightedLabCost,
      WeightedLabBill,
      StartDate,
      EndDate,
      SortSeq,
      HardBooked,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPAssignment
 
  INSERT PNPlannedLabor (
    TimePhaseID,
    TaskID,
    PlanID,
    AssignmentID,
    StartDate,
    EndDate,
    PeriodHrs,
    PeriodCost,
    PeriodBill,
    CostRate,
    BillingRate,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT 
      TimePhaseID,
      TaskID,
      PlanID,
      AssignmentID,
      StartDate,
      EndDate,
      PeriodHrs,
      PeriodCost,
      PeriodBill,
      CostRate,
      BillingRate,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPPlannedLabor

  INSERT PNWBSLevelFormat (
    WBSFormatID,
    PlanID,
    FmtLevel,
    WBSType,
    WBSMatch,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT 
      WBSFormatID,
      PlanID,
      FmtLevel,
      WBSType,
      WBSMatch,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPWBSLevelFormat

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  COMMIT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

END -- stRPConvertDRPtoDeltekPS
GO
