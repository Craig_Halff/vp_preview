SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Main_UpdateBrowseFilter]
	@activeOnly			bit,
	@pr					bit,
	@ve					bit,
	@filter				Nvarchar(max),
	@CFGLABELS_WBS1		Nvarchar(200),
	@CFGLABELS_VENDOR	Nvarchar(200)
AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_Main_UpdateBrowseFilter] 0,1,1,'','',''
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
	DECLARE @sql			Nvarchar(max) = N'';
	DECLARE @statusFilter	Nvarchar(max) = N' <> ''D'' ';
	DECLARE @projectFilter	Nvarchar(max) = N'';
	DECLARE @filterEscaped	Nvarchar(max) = Replace(@filter, N'''', N'''''');

    IF @activeOnly = 1
        SET @statusFilter = ' = ''A'' and ChargeType = ''R'' ';

    IF ISNULL(@filter, '') <> ''
        SET @projectFilter = ' and (WBS1 like ''%' + @filterEscaped + '%'' or name like ''%' + @filterEscaped + '%'')';

    IF @pr = 1 BEGIN
        SET @sql = N'
            SELECT N''' + @CFGLABELS_WBS1 + ''' as Type, WBS1 as Code, Name, ''WBS'' as TypeCode, Status, Sublevel, ''New'' as NewContract
                FROM PR
                WHERE WBS2 = N'' '' and Status ' + @statusFilter + @projectFilter;

        SET @statusFilter = ' <> ''D'' ';
        IF @activeOnly = 1
            SET @statusFilter = ' = ''A'' ';
    END;

    SET @projectFilter = '';

    IF ISNULL(@filter, '') <> ''
        SET @projectFilter = ' and (Vendor like ''%' + @filterEscaped + '%'' or name like ''%' + @filterEscaped + '%'')';

    IF ISNULL(@sql, '') <> '' AND @ve = 1
        SET @sql = @sql + '
			UNION ';

    IF @ve = 1
        SET @sql = @sql + '
			Select N''' + @CFGLABELS_VENDOR + ''' as Type, Vendor as Code, Name,''VE'' as TypeCode, Status,
					''N'' as Sublevel, ''New'' as NewContract
				FROM VE
				WHERE Status ' + @statusFilter + @projectFilter + '
			UNION
			Select N''' + @CFGLABELS_VENDOR + ''' as Type, VE.Vendor as Code, VendorAlias.Alias, ''VE'' as TypeCode,
					Status, ''N'' as Sublevel, ''New'' as NewContract
				FROM VE
					INNER JOIN VendorAlias on VE.Vendor = VendorAlias.Vendor
					WHERE Status ' + @statusFilter + '
						and VendorAlias.Alias like ''%' + @filterEscaped + '%'' ';

	SET @sql = @sql + '
			ORDER BY 1';

	PRINT @sql;
	EXEC (@sql);
END;

GO
