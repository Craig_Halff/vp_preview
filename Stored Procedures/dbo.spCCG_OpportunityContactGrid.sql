SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[spCCG_OpportunityContactGrid] 
	@OpportunityID	varchar(32),
	@OldContactID	varchar (32), 
	@NewContactID	varchar (32), 
	@RoleID			varchar (32)
AS 
BEGIN
SET NOCOUNT ON
/* 
Copyright (c) 2020 Central Consulting Group.  All rights reserved.
04/09/2020	David Springer
			Remove contacts from the Contacts grid when they are removed 
			from Billing Contact (01) or Owner Contact (sysOwner).
			Insert for Billing Contact and Update of Owner Contact.
			Protect Billing Contact if Owner Contact is changed.
			Pass RoleID of the field being changed (sysOwner or 01).
			Call this on Opportunity INSERT for Billing Contact not empty.
			Call this on Opportunity CHANGE for both Billing Contact & Owner Contact changed.
*/
-- If Billing Contact now remains, change the Role to Billing (01)
Update a
Set a.Role = @RoleID
From OpportunityContactAssoc a, OpportunityCustomTabFields o
Where o.OpportunityID = @OpportunityID
  and o.OpportunityID = a.OpportunityID
  and o.custBillingContact = a.ContactID

-- If Owner Contact now remains, change the Role to Owner (sysOwner)
Update a
Set a.Role = 'sysOwner'
From OpportunityContactAssoc a, Opportunity o
Where o.OpportunityID = @OpportunityID
  and o.OpportunityID = a.OpportunityID
  and o.ContactID     = a.ContactID

-- Delete Contact record when not used by Opportunity ContactID
Delete a
From OpportunityContactAssoc a
Where OpportunityID = @OpportunityID
  and ContactID = @OldContactID
  and not exists (Select 'x' From Opportunity Where OpportunityID = a.OpportunityID and ContactID = a.ContactID)
  and not exists (Select 'x' From OpportunityCustomTabFields Where OpportunityID = a.OpportunityID and custBillingContact = a.ContactID)

If @NewContactID <> '' and 
   not exists (Select * From OpportunityContactAssoc Where OpportunityID = @OpportunityID and ContactID = @NewContactID)
   Begin
      Insert Into OpportunityContactAssoc
      (OpportunityID, ContactID, Role, CreateDate, CreateUser)
      Values (@OpportunityID, @NewContactID, @RoleID, getDate(), 'CCG_Procedure')
   End
END
GO
