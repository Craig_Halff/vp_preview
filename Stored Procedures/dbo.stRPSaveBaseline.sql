SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPSaveBaseline]
  @strPlanID VARCHAR(32)
AS

BEGIN -- Procedure stRPSaveBaseline

  -->>> This procedure must be called after all of the recalculation is done.

  SET NOCOUNT ON
  
  -- Delete all Baseline time-phased data.
  
  DELETE PNBaselineLabor WHERE PlanID = @strPlanID
  DELETE PNBaselineExpenses WHERE PlanID = @strPlanID
  DELETE PNBaselineConsultant WHERE PlanID = @strPlanID
  
  -- Copy Planned time-phased to make new Baseline time-phased data.
  
 INSERT PNBaselineLabor
         (TimePhaseID, 
          PlanID, 
          TaskID,
          AssignmentID,
          StartDate, 
          EndDate, 
          CostRate,
          BillingRate, 
          PeriodHrs, 
          PeriodCost, 
          PeriodBill, 
          PeriodRev, 
          PeriodCount, 
          PeriodScale,
          CreateDate,
          ModDate)
    SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
           PlanID, 
           TaskID,
           AssignmentID,
           StartDate, 
           EndDate, 
           CostRate,
           BillingRate, 
           PeriodHrs, 
           PeriodCost, 
           PeriodBill, 
           PeriodRev, 
           PeriodCount, 
           PeriodScale,
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
    FROM PNPlannedLabor
    WHERE PlanID = @strPlanID
  
  INSERT PNBaselineExpenses
         (TimePhaseID, 
          PlanID, 
          TaskID,
          ExpenseID,
          StartDate, 
          EndDate, 
          PeriodCost, 
          PeriodBill, 
          PeriodRev, 
          PeriodCount, 
          PeriodScale,
          CreateDate,
          ModDate)
    SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
           PlanID, 
           TaskID,
           ExpenseID,
           StartDate, 
           EndDate, 
           PeriodCost, 
           PeriodBill, 
           PeriodRev, 
           PeriodCount, 
           PeriodScale,
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
    FROM PNPlannedExpenses
    WHERE PlanID = @strPlanID
  
  INSERT PNBaselineConsultant
         (TimePhaseID, 
          PlanID, 
          TaskID,
          ConsultantID,
          StartDate, 
          EndDate, 
          PeriodCost, 
          PeriodBill, 
          PeriodRev, 
          PeriodCount, 
          PeriodScale,
          CreateDate,
          ModDate)
    SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
           PlanID, 
           TaskID,
           ConsultantID,
           StartDate, 
           EndDate, 
           PeriodCost, 
           PeriodBill, 
           PeriodRev, 
           PeriodCount, 
           PeriodScale,
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
    FROM PNPlannedConsultant
    WHERE PlanID = @strPlanID
    
  -- Update Plan.
  
  UPDATE PNPlan
    SET BaselineLaborHrs = PlannedLaborHrs,
        BaselineLabCost = PlannedLabCost,
        BaselineLabBill = PlannedLabBill,
        BaselineExpCost = PlannedExpCost,
        BaselineExpBill = PlannedExpBill,
        BaselineConCost = PlannedConCost,
        BaselineConBill = PlannedConBill,
        BaselineUntQty = PlannedUntQty,
        BaselineUntCost = PlannedUntCost,
        BaselineUntBill = PlannedUntBill,
        BaselineDirExpCost = PlannedDirExpCost,
        BaselineDirExpBill = PlannedDirExpBill,
        BaselineDirConCost = PlannedDirConCost,
        BaselineDirConBill = PlannedDirConBill,
        BaselineDirUntCost = PlannedDirUntCost,
        BaselineDirUntBill = PlannedDirUntBill,
        BaselineStart = StartDate,
        BaselineFinish = EndDate
    WHERE PlanID = @strPlanID

  -- Update Tasks.
  
  UPDATE PNTask
    SET BaselineStart = StartDate,
        BaselineFinish = EndDate
    WHERE PlanID = @strPlanID
  
  -- Update Assignments.
  
  UPDATE PNAssignment
    SET BaselineStart = StartDate,
        BaselineFinish = EndDate
    WHERE PlanID = @strPlanID
    
  -- Update Expenses.
  
  UPDATE PNExpense
    SET BaselineStart = StartDate,
        BaselineFinish = EndDate
    WHERE PlanID = @strPlanID
    
  -- Update Consultants.
  
  UPDATE PNConsultant
    SET BaselineStart = StartDate,
        BaselineFinish = EndDate
    WHERE PlanID = @strPlanID
  
  --Update the LastPlanAction Field to SAVEDBASELINE status.

  UPDATE PNPlan SET LastPlanAction = 'SAVEDBASELINE'  WHERE PlanID = @strPlanID

  SET NOCOUNT OFF

END -- stRPSaveBaseline
GO
