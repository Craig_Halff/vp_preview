SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[HAI_ProjectSyncCubeColumns]
AS
    /* 
	Copyright (c) 2021 Halff Associates, Inc.  All rights reserved.
	9/22/2021	Craig H. Anderson

	Sync the numeric duplicated columns used by the data cube to their production (text based, etc.) equivalents.
	To be used nightly prior to the cube rebuilds, after revenue recognition has run.
*/
    BEGIN
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
        UPDATE px
        SET px.CustEffortatCompletionDuplicate = DataMart.dbo.Text2Decimal(ISNULL(px.CustEffortatCompletion, '0'))
          , px.CustWriteoffAdjmtDuplicate = DataMart.dbo.Text2Decimal(ISNULL(px.CustWriteoffAdjmt, '0'))
          , px.CustEfforttoCompleteDuplicate = DataMart.dbo.Text2Decimal(ISNULL(px.CustEfforttoComplete, '0'))
          , px.CustFeeTypeRollupDuplicate = px.CustFeeTypeRollup
        FROM dbo.ProjectCustomTabFields px
            INNER JOIN dbo.PR           AS p
                ON px.WBS1 = p.WBS1
                    AND px.WBS2 = p.WBS2
                    AND px.WBS3 = p.WBS3
        WHERE
            p.ChargeType <> 'P'
            AND px.CustLastChargeDate > DATEADD(MONTH, -2, GETDATE());

    END;
GO
