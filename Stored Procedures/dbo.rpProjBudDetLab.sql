SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpProjBudDetLab]
  (
   @company as Nvarchar(4),@ETCDate as datetime,@rptWBSListKey as varchar(32),
   @includeFlags as smallint,
   @wbs1Activity varchar(1) = 'Y',
   @wbs2Activity varchar(1) = 'Y',
   @wbs3Activity varchar(1) = 'Y',
   @workTable varchar(100),
   @adapterTable varchar(100)
  )
as begin

/* 
NOTE: @ETCDate should be the client date entered. down below, 1 day is added to the @ETCDate to get the actual start of etc calculations.
So, if the client types 12/13/06 into the ETC date basis selection, 12/13/06 should be passed in here. From here on, it gets changed to 12/14/06.
*/

/***UNCOMMENT THIS TO TEST THE BIG QUERY BELOW
declare @wbs1Activity as varchar(1);
declare @wbs2Activity as varchar(1);
declare @wbs3Activity as varchar(1);
declare @company as Nvarchar(4);
declare @etcDate as datetime;
declare @rptWBSListKey as varchar(32);
declare @includeFlags as smallint
declare @workTable as varchar(100);

	set @company = ' ';
	set @etcDate = getDate();
	set @rptWBSListKey = '436c0c1bd8734d4';
	set @includeFlags = 1;
	set @workTable = '##RWL_436c0c1bd8734d4';
	set @wbs1Activity = 'N'
	set @wbs2Activity = 'N'
	set @wbs3Activity = 'N'
***/
	
	declare @etcDateString as varchar(100);
	declare @sqlQuery as varchar(max);
	declare @temptable as varchar(100);
	set @etcDate = dateadd(d,1,@etcDate);
	declare @seecostrates as varchar(1);

	select @seecostrates='0';
	if (@includeFlags >= 1) 
		begin
			set @seecostrates = '1'
			set @includeFlags = @includeFlags - 1
		end
	--@includeFlags should be 0 at this point

	declare @wbsActivityLevel as varchar(1)
	set @wbsActivityLevel = '3'
	if (@wbs1Activity = 'Y')
		begin
			set @wbsActivityLevel = '1'
		end

	if (@wbs2Activity = 'Y')
		begin
			set @wbsActivityLevel = '2'
		end

	if (@wbs3Activity = 'Y')
		begin
			set @wbsActivityLevel = '3'
		end

	set @temptable = '##rpProjDetLab_' + @rptWBSListKey
	set @etcDateString = CAST(@ETCDate as varchar(19));
	IF OBJECT_ID('tempdb..' + @temptable) IS NOT NULL	
		Begin
			select @sqlQuery = 'DELETE FROM ' + @temptable  + ' '
			exec (@sqlQuery)
		End
	Else
		Begin	

			/* 
			NOTE: @ETCDate should be the client date entered. down below, 1 day is added to the @ETCDate to get the actual start of etc calculations.
			So, if the client types 12/13/06 into the ETC date basis selection, 12/13/06 should be passed in here. From here on, it gets changed to 12/14/06.
			*/
			select @sqlQuery = 'CREATE TABLE ' + @temptable +  '
				(	wbs1 Nvarchar(30) COLLATE database_default,
					wbs2 Nvarchar(30) COLLATE database_default,
					wbs3 Nvarchar(30) COLLATE database_default,
					laborcode Nvarchar(14) COLLATE database_default,
					datatype smallint,
					recordtype char(1) COLLATE database_default,
					subtype char(1) COLLATE database_default,
					resourceid Nvarchar(32) COLLATE database_default,
					category smallint,
					pctcompleteformula smallint,
					targetmultcost decimal(19,4),
					targetmultbill decimal(19,4),
					plannnedrevenue decimal(19,4),
					baselinerevenue decimal(19,4),
					plannedhrs decimal (19,4),
					baselinehrs decimal(19,4),
					plannedcost decimal(19,4),
					plannedbill decimal (19,4),
					baselinecost decimal (19,4),
					baselinebill decimal (19,4),
					pctcmpplannedcost decimal (19,4),
					pctcmpplannedbill decimal (19,4),
					pctcmpbaselinecost decimal (19,4),
					pctcmpbaselinebill decimal (19,4),
					pctcmpplanhrscost decimal (19,4),
					pctcmpplanhrsbill decimal (19,4),
					pctcmpbasehrscost decimal (19,4),
					pctcmpbasehrsbill decimal (19,4),
					etcrev decimal (19,4),
					etchrs decimal (19,4),
					etccost decimal (19,4),
					etcbill decimal (19,4)
					primary key (wbs1,wbs2,wbs3,laborcode,datatype,recordtype,subtype,resourceid,category) 
				)';

			exec (@sqlQuery);
		End

	set @sqlQuery = ' insert into ' + @temptable + ' '
		+ ' SELECT	distinct t.wbs1 														wbs1, '
		+ '         CASE WHEN t.wbs2 = ''<none>'' then '' '' ELSE Isnull(t.wbs2,'' '')	END	wbs2, '
		+ '         CASE WHEN t.wbs3 = ''<none>'' then '' '' ELSE Isnull(t.wbs3,'' '')	END	wbs3, '
		+ '         Isnull(t.laborcode, '''')										laborcode, '
		+ '         1																datatype, '
		+ ' 	    ''1''                                                           recordtype, '
		+ ' 		'' ''                                                           subtype, '
		+ ' 		Isnull(res.resourceid, '''')                                    resourceid, '
		+ ' 		Isnull(res.category, '''')										category, '
		+ ' 		2																PctcompleteFormula, '
		+ ' 		Max(p.targetmultcost)												targetmultcost, '
		+ ' 		Max(p.targetmultbill)												targetmultbill, '      
		+ ' 		SUM(PlanCalcLab.[PlannedCost] * p.multiplier)						planrevenue, '
		+ ' 		SUM(PlanCalcLab.[BaselineCost] * p.multiplier)						baserevenue, '
		+ ' 		SUM(PlanCalcLab.[PlannedHrs])										planhrs, '
		+ ' 		SUM(PlanCalcLab.[BaselineHrs])										basehrs, '
		+ ' 		SUM(PlanCalcLab.[PlannedCost])										plancost, '
		+ ' 		SUM(PlanCalcLab.[PlannedBill])										planbill, '
		+ ' 		SUM(PlanCalcLab.[BaselineCost])									basecost, '
		+ ' 		SUM(PlanCalcLab.[BaselineBill])										basebill, '
		+ ' 		SUM((PlanCalcLab.[CalcPctComplCost]/100) * PlanCalcLab.[PlannedCost])		pctcmpplancost, '
		+ ' 		SUM((PlanCalcLab.[CalcPctComplBill]/100) * PlanCalcLab.[PlannedBill])		pctcmpplanbill, '
		+ ' 		SUM((PlanCalcLab.[CalcPctComplCost]/100) * PlanCalcLab.[BaselineCost])		pctcmpbasecost, '
		+ ' 		SUM((PlanCalcLab.[CalcPctComplBill]/100) * PlanCalcLab.[BaselineBill])		pctcmpbasebill, '
		+ ' 		SUM((PlanCalcLab.[CalcPctComplCost]/100) * PlanCalcLab.[PlannedHrs])		pctcmpplanhrscost, '
		+ ' 		SUM((PlanCalcLab.[CalcPctComplBill]/100) * PlanCalcLab.[PlannedHrs])		pctcmpplanhrsbill, '
		+ ' 		SUM((PlanCalcLab.[CalcPctComplCost]/100) * PlanCalcLab.[BaselineHrs])		pctcmpbasehrscost, '
		+ ' 		SUM((PlanCalcLab.[CalcPctComplBill]/100) * PlanCalcLab.[BaselineHrs])		pctcmpbasehrsbill, '
		+ ' 		SUM(PlanCalcLab.[ETCCost] * p.multiplier)							etcrev, '
		+ ' 		SUM(PlanCalcLab.[ETCHrs])											etcplanhrs, '
		+ ' 		SUM(PlanCalcLab.[ETCCost])											etcplancost, '
		+ ' 		SUM(PlanCalcLab.[ETCBill])											etcplanbill '
		+ ' FROM   rpplan p, '
		+ '               rptask t '
		+ '               LEFT OUTER JOIN pr AS level2 '
		+ '                            ON t.wbs1 = level2.wbs1 '
		+ '                               AND t.wbs2 = level2.wbs2 '
		+ '                               AND level2.wbs3 = '' '' '
		+ '               LEFT OUTER JOIN pr AS level3 '
		+ '                            ON t.wbs1 = level3.wbs1 '
		+ '                               AND t.wbs2 = level3.wbs2 '
		+ '                               AND t.wbs3 = level3.wbs3, '
		+ '               rpassignment res, '
		+ '               ' + @workTable + ' wbs, '              
		+ '			      ' + @adapterTable + ' planCalcLab  '
		+ 'WHERE  p.planid = t.planid '
		+ 'AND p.utilizationincludeflg = ''Y'' '
		+ 'AND Isnull(t.wbs1, '''') > '''' '
		+ 'AND t.planid = res.planid '
		+ 'AND t.taskid = res.taskid '
		+ 'AND res.assignmentid IS NOT NULL '		
		
		+ 'AND planCalcLab.planid = t.planid '
		+ 'AND planCalcLab.taskid = t.taskid '
		+ 'AND planCalcLab.assignmentid = res.assignmentid '

		+ ' and ((' + @wbsActivityLevel + '=3 and isnull(t.wbs1, '''') = wbs.wbs1 and isnull(level2.wbs2, '''') = wbs.wbs2 and isnull(level3.wbs3, '''') = wbs.wbs3) '
		+ ' or (' + @wbsActivityLevel + '=2 and isnull(t.wbs1, '''') = wbs.wbs1 and isnull(level2.wbs2, '''') = wbs.wbs2 and wbs.wbs3='''') '
		+ ' or (' + @wbsActivityLevel + '=1 and isnull(t.wbs1, '''') = wbs.wbs1 and wbs.wbs2='''' and wbs.wbs3='''')) '
		+ ' and wbs.sessionId = ''' + @rptWBSListKey + ''' '
		+ ' group by t.wbs1, ' 
		+ ' case when t.wbs2 = ''<none>'' then '' '' else Isnull(t.wbs2,'' '') end, ' 
		+ ' case when t.wbs3 = ''<none>'' then '' '' else Isnull(t.wbs3,'' '') end, t.laborcode, res.resourceid, res.category ';
	exec(@sqlQuery);
	print (@sqlQuery)

end
GO
