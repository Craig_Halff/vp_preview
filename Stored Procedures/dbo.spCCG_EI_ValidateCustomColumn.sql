SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_ValidateCustomColumn] (
	@objectName			varchar(100),
	@hasParamOrCol		varchar(20),
	@lookForErrors		int,						-- 0 or 1
	@ValidateOnly		int,						/* 0 = only do validation of input fields, 1 = do validation and show returned fields, 2 = only show returned fields */
	@oName				varchar(10) output,
	@oType				varchar(10) output,
	@HasParamCol		varchar(1) output,
	@HasWBS1			varchar(1) output,
	@HasWBS2			varchar(1) output,
	@HasWBS3			varchar(1) output,
	@HasUserNameParam	varchar(1) output,
	@HasEmployeeParam	varchar(1) output,
	@InitError			varchar(max) output
)
AS BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	declare @type varchar(10) = (select type as otype from sys.objects o
		where o.object_id = OBJECT_ID(N'[' + Replace(@objectName, 'dbo.', 'dbo].[') + ']'))

	--print @type
	if ISNULL(@type,'') = '' RETURN
	if CHARINDEX('CCG_', @objectName) <= 0 RETURN

	if @ValidateOnly < 2
	begin
		if @type = 'V'
			select	@oName = oname,
					@oType = otype,
					@HasParamCol =		isnull(max(case when upper(colName) = upper(@hasParamOrCol) then 'Y' else '' end), 'N'),
					@HasWBS1 =			isnull(max(case when upper(colName) = 'WBS1' then 'Y' else '' end), 'N'),
					@HasWBS2 =			isnull(max(case when upper(colName) = 'WBS2' then 'Y' else '' end), 'N'),
					@HasWBS3 =			isnull(max(case when upper(colName) = 'WBS3' then 'Y' else '' end), 'N'),
					@HasUserNameParam = isnull(max(case when upper(colName) = 'USERNAME' then 'Y' else '' end), 'N'),
					@HasEmployeeParam = isnull(max(case when upper(colName) = 'EMPLOYEE' then 'Y' else '' end), 'N')
				from (
					select o.name as oname, type as otype, c.name as colName, c.system_type_id
						from sys.objects o
							left join sys.columns c ON o.object_id = c.object_id
						where o.object_id = OBJECT_ID(N'[' + Replace(@objectName, 'dbo.', 'dbo].[') + ']')
					) d
				group by oname, otype;
		else if @type = 'TF'
			select	@oName = oname,
					@oType = otype,
					@HasParamCol =		max(case when isnull(d.colname, '') ='' then 'N' else 'Y' end),
					@HasWBS1 =			isnull(max(case when upper(paramName) = '@WBS1' then 'Y' else '' end), 'N'),
					@HasWBS2 =			isnull(max(case when upper(paramName) = '@WBS2' then 'Y' else '' end), 'N'),
					@HasWBS3 =			isnull(max(case when upper(paramName) = '@WBS3' then 'Y' else '' end), 'N'),
					@HasUserNameParam = isnull(max(case when upper(paramName) = '@USERNAME' then 'Y' else '' end), 'N'),
					@HasEmployeeParam = isnull(max(case when upper(paramName) = '@EMPLOYEE' then 'Y' else '' end), 'N')
				from (
					select o.name as oname, type as otype, c.name as colName, p.system_type_id, p.name as paramName, is_output, parameter_id
						from sys.objects o
							left join sys.columns c ON o.object_id = c.object_id AND UPPER(c.name) = UPPER(@hasParamOrCol)
							left join sys.parameters p ON o.object_id = p.object_id
						where o.object_id = OBJECT_ID(N'[' + Replace(@objectName, 'dbo.', 'dbo].[') + ']')
					) d
				group by oname, otype;
		else if @type = 'FN'
			select	@oName = oname,
					@oType = otype,
					@HasParamCol =		isnull(max(case when parameter_id = 0 and is_output = 1 then 'Y' else '' end), 'N'),
					@HasWBS1 =			isnull(max(case when upper(paramName) = '@WBS1' then 'Y' else '' end), 'N'),
					@HasWBS2 =			isnull(max(case when upper(paramName) = '@WBS2' then 'Y' else '' end), 'N'),
					@HasWBS3 =			isnull(max(case when upper(paramName) = '@WBS3' then 'Y' else '' end), 'N'),
					@HasUserNameParam = isnull(max(case when upper(paramName) = '@USERNAME' then 'Y' else '' end), 'N'),
					@HasEmployeeParam = isnull(max(case when upper(paramName) = '@EMPLOYEE' then 'Y' else '' end), 'N')
				from (
					select o.name as oname, type as otype, p.system_type_id, p.name as paramName, is_output, parameter_id
						from sys.objects o
							left join sys.parameters p ON o.object_id = p.object_id
						where o.object_id = OBJECT_ID(N'[' + Replace(@objectName, 'dbo.', 'dbo].[') + ']')
					) d
				group by oname, otype;
		else if @type = 'U'
			select	@oName = oname,
					@oType = otype,
					@HasParamCol =		isnull(max(case when upper(colName) = UPPER(@hasParamOrCol) then 'Y' else '' end), 'N'),
					@HasWBS1 =			isnull(max(case when upper(colName) = 'WBS1' then 'Y' else '' end), 'N'),
					@HasWBS2 =			isnull(max(case when upper(colName) = 'WBS2' then 'Y' else '' end), 'N'),
					@HasWBS3 =			isnull(max(case when upper(colName) = 'WBS3' then 'Y' else '' end), 'N'),
					@HasUserNameParam = isnull(max(case when upper(colName) = 'USERNAME' then 'Y' else '' end), 'N'),
					@HasEmployeeParam = isnull(max(case when upper(colName) = 'EMPLOYEE' then 'Y' else '' end), 'N')
				from (
					select o.name as oname, type as otype, c.name as colName
						from sys.objects o
							left join sys.columns c ON o.object_id = c.object_id
						where o.object_id = OBJECT_ID(N'[' + Replace(@objectName, 'dbo.', 'dbo].[') + ']')
					) d
				group by oname, otype;

		if @lookForErrors = 1
		begin
			if isnull(@oName,'') = ''
				set @InitError = 'SQL Function ' + @objectName + ' does not exist'
			else if isnull(@oType,'') = ''
				-- rtype will be null for table functions
				set @InitError = 'Unknown type for SQL Function ' + @objectName
			else if isnull(@HasWBS1,'') = '' or isnull(@HasWBS2,'') = '' or isnull(@HasWBS3,'') = ''
				set @InitError = 'Invalid parameters for SQL Function ' + @objectName
		end
	end

	IF @ValidateOnly <> 1 BEGIN
		SELECT o.name as oname, type as otype, c.name as colName, c.system_type_id
			FROM sys.objects o
				LEFT JOIN sys.columns c ON o.object_id = c.object_id
			WHERE o.object_id = OBJECT_ID(N'[' + Replace(@objectName, N'dbo.', N'dbo].[') + N']')
				AND UPPER(c.name) not in (N'WBS1', N'WBS2', N'WBS3', N'USERNAME', N'EMPLOYEE')
	END
END
GO
