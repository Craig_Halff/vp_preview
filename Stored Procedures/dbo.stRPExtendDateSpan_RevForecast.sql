SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPExtendDateSpan_RevForecast]
  @strPlanID varchar(32),
  @bitCalledFromRM bit = 0
AS

BEGIN -- Procedure stRPExtendDateSpan

  SET NOCOUNT ON

  DECLARE @iLevel int

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @iLevel = MAX(OutlineLevel) - 1 FROM PNTask WHERE PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION


    -- Extend StartDate and EndDate of PNTask rows when Start/End Dates of Revenue Labor TPD rows were extended.

    DECLARE @strUserName nvarchar(20) = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

    UPDATE T SET 
      RevenueStartDate = CASE WHEN X.RevenueStartDate < T.RevenueStartDate THEN X.RevenueStartDate ELSE T.RevenueStartDate END, 
      RevenueEndDate = CASE WHEN X.RevenueEndDate > T.RevenueEndDate THEN X.RevenueEndDate ELSE T.RevenueEndDate END,
      ModDate=LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19),
      ModUser=@strUserName
      FROM PNTask AS T 
        INNER JOIN (
          SELECT PlanID, TaskID, MIN(StartDate) AS RevenueStartDate, MAX(EndDate) AS RevenueEndDate 
            FROM PNPlannedRevenueLabor
            WHERE PlanID = @strPlanID
            GROUP BY PlanID, TaskID
        ) AS X 
        ON T.PlanID = X.PlanID AND T.TaskID = X.TaskID
        WHERE T.PlanID = @strPlanID 
          AND (X.RevenueStartDate < T.RevenueStartDate OR X.RevenueEndDate > T.RevenueEndDate)

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Traverse upward from bottom level.
    -- Update StartDate and EndDate of parent rows.

    WHILE (@iLevel >= 0) 
      BEGIN

        UPDATE T SET
          RevenueStartDate = CASE WHEN X.RevenueStartDate < T.RevenueStartDate THEN X.RevenueStartDate ELSE T.RevenueStartDate END, 
          RevenueEndDate = CASE WHEN X.RevenueEndDate > T.RevenueEndDate THEN X.RevenueEndDate ELSE T.RevenueEndDate END,
		  ModDate=LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19),
		  ModUser=@strUserName
          FROM PNTask AS T
            INNER JOIN (
              SELECT 
                PlanID, 
                ParentOutlineNumber, 
                MIN(RevenueStartDate) AS RevenueStartDate, 
                MAX(RevenueEndDate) AS RevenueEndDate 
                FROM PNTask 
                WHERE PNTask.PlanID = @strPlanID 
                GROUP BY PlanID, ParentOutlineNumber
            ) AS X ON T.PlanID = X.PlanID AND T.OutlineNumber = X.ParentOutlineNumber
          WHERE T.PlanID = @strPlanID AND T.OutlineLevel = @iLevel
            AND (X.RevenueStartDate < T.RevenueStartDate OR X.RevenueEndDate > T.RevenueEndDate)

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        SET @iLevel = @iLevel - 1

      END

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- If this SP was called from Resource Management then need to update RP tables.

    IF (@bitCalledFromRM = 1)
      BEGIN
         UPDATE RT SET
          RevenueStartDate = CASE WHEN RT.RevenueStartDate <> PT.RevenueStartDate THEN PT.RevenueStartDate ELSE RT.RevenueStartDate END, 
          RevenueEndDate = CASE WHEN RT.RevenueEndDate <> PT.RevenueEndDate THEN PT.RevenueEndDate ELSE RT.RevenueEndDate END
          FROM RPTask AS RT
            INNER JOIN PNTask AS PT
              ON RT.PlanID = PT.PlanID AND RT.TaskID = PT.TaskID 
          WHERE RT.PlanID = @strPlanID 
            AND (RT.RevenueStartDate <> PT.RevenueStartDate OR RT.RevenueEndDate <> PT.RevenueEndDate)

      END /* END IF (@bitCalledFromRM = 1) */

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  COMMIT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF
     
END -- stRPExtendDateSpan_RevForecast
GO
