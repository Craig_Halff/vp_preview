SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DiaryInTables] @Diary Nvarchar(10)
as
declare
  @BaseStmt		Nvarchar(1000),
  @SqlStmt		Nvarchar(max)

begin 
  set @BaseStmt = 
	'Select ''?Table'' as TableName from ?Table where ?Diary = N''' + @Diary + ''''  

  set @SqlStmt = 'Select top 1 TableName from  ('

  if rtrim(@Diary) <> '' 
  begin
    set @SqlStmt = @SqlStmt + replace(replace(@BaseStmt,'?Table','apppChecks'),'?Diary','Diary')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','exChecks'),'?Diary','Diary')

    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','BIED'),'?Diary','Diary')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerAP'),'?Diary','Diary')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerAR'),'?Diary','Diary')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerEX'),'?Diary','Diary')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerMisc'),'?Diary','Diary')

    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','apMaster'),'?Diary','Diary')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','cdMaster'),'?Diary','Diary')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','crMaster'),'?Diary','Diary')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','erMaster'),'?Diary','Diary')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','inMaster'),'?Diary','Diary')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','jeMaster'),'?Diary','Diary')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','miMaster'),'?Diary','Diary')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','prMaster'),'?Diary','Diary')

    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','exControl'),'?Diary','Diary')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','laControl'),'?Diary','Diary')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','tsControl'),'?Diary','Diary')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','unControl'),'?Diary','Diary')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','upControl'),'?Diary','Diary')

    set @SqlStmt = @SqlStmt + ') as TableCheck'

    execute (@SqlStmt)
  end
end -- DiaryInTables  
GO
