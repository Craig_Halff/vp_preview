SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_CheckForDuplicateInvoice]
	@vendor				Nvarchar(20),			/* Optional. Will not check if blank */
	@itemNumber			Nvarchar(50),
	@itemDate			varchar(30),				/* Optional. Will not check if blank */
	@seq				int,
	@company			Nvarchar(14),			/* Optional. Will not check if blank */
	@transType			varchar(2),
	@voucher			Nvarchar(12)
AS
BEGIN
	-- spCCG_PAT_CheckForDuplicateInvoice @vendor='0000000103', @itemNumber='612312', @itemDate='2017-09-29', @seq='-1', @company='', @transType='ap', @voucher=''
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

	IF SUBSTRING(@vendor + ' ', 1, 1) = '''' SET @vendor = SUBSTRING(SUBSTRING(@vendor, 1, LEN(@vendor)-1), 2, 100)
	IF SUBSTRING(@itemNumber + ' ', 1, 1) = '''' SET @itemNumber = SUBSTRING(SUBSTRING(@itemNumber, 1, LEN(@itemNumber)-1), 2, 100)
	IF SUBSTRING(@itemDate + ' ', 1, 1) = '''' SET @itemDate = SUBSTRING(SUBSTRING(@itemDate, 1, LEN(@itemDate)-1), 2, 100)
	DECLARE @dItemDate DATETIME = case when isnull(@itemDate,'')='' then NULL else CAST(@itemDate as Datetime) end

	If @transType = 'cv'
		SELECT count(dupcount), MIN(Source), MAX(Source)
			FROM  (
				SELECT distinct vendor as dupcount, 'ZPAT' as source
					FROM CCG_PAT_Payable
					WHERE PayableNumber = @itemNumber and PayableType <> 'C'
						and Seq <> @seq
						and (isnull(@vendor,'') = '' or Vendor = @vendor)
						and (isnull(@itemDate,'') = '' or PayableDate = @dItemDate)
						and (isnull(@company,'') = '' or Company = @company)
				UNION ALL
				SELECT distinct vendor as dupcount, 'BTransactionCenter' as source
					FROM cvMaster Master
						inner join cvControl Control on Master.Batch = Control.Batch
					WHERE Master.Invoice = @itemNumber and Master.posted = 'N'
						and (isnull(@vendor,'') = '' or Vendor = @vendor)
						and (isnull(@itemDate,'') = '' or InvoiceDate = @dItemDate)
						and (ISNULL(@voucher,'') = '' OR Voucher <> @voucher)
						and (isnull(@company,'') = '' or Company = @company)
				UNION All
				SELECT distinct vendor as dupcount, 'ALedgerAP' as source
					FROM VO
					WHERE Invoice = @itemNumber
						and (isnull(@vendor,'') = '' or Vendor = @vendor)
						and (isnull(@itemDate,'') = '' or InvoiceDate = @dItemDate)
						and (ISNULL(@voucher,'') = '' OR Voucher <> @voucher)
						and (isnull(@company,'') = '' or Company = @company)
			) duptables
	Else If @transType = 'ap'
		SELECT count(dupcount), MIN(Source), MAX(Source)
			FROM  (
				SELECT distinct vendor as dupcount, 'ZPAT' as source
					FROM CCG_PAT_Payable
					WHERE PayableNumber = @itemNumber and PayableType <> 'C'
						and Seq <> @seq
						and (isnull(@vendor,'') = '' or Vendor = @vendor)
						and (isnull(@itemDate,'') = '' or PayableDate = @dItemDate)
						and (isnull(@company,'') = '' or Company = @company)
				UNION ALL
				SELECT distinct vendor as dupcount, 'BTransactionCenter' as source
					FROM apMaster Master
						inner join apControl Control on Master.Batch = Control.Batch
					WHERE Master.Invoice = @itemNumber and Master.posted = 'N'
						and (isnull(@vendor,'') = '' or Vendor = @vendor)
						and (isnull(@itemDate,'') = '' or InvoiceDate = @dItemDate)
						and (ISNULL(@voucher,'') = '' OR Voucher <> @voucher)
						and (isnull(@company,'') = '' or Company = @company)
				UNION All
				SELECT distinct vendor as dupcount, 'ALedgerAP' as source
					FROM VO
					WHERE Invoice = @itemNumber
						and (isnull(@vendor,'') = '' or Vendor = @vendor)
						and (isnull(@itemDate,'') = '' or InvoiceDate = @dItemDate)
						and (ISNULL(@voucher,'') = '' OR Voucher <> @voucher)
						and (isnull(@company,'') = '' or Company = @company)
			) duptables
END
GO
