SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectBillingTermsExpTableValidation]
   @WBS1        varchar(32), 
   @WBS2        varchar(7), 
   @WBS3        varchar(7)
AS
/*
Copyright (c) 2018 Central Consulting Group. All rights reserved.
01/24/2018 David Springer
           Validate Reimb Expense Multipler and Reimb Mileage Multiplier result in a valid table.
		   Validate Reimb Consultant Multiplier results in a valid table.
		   Call from spCCG_ProjectBillingTermsInsert procedure.
           Call from spCCG_ProjectBillingTermsMulitpliers procedure.
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
DECLARE @ExpTableName varchar (255),
		@ConTableName varchar (255),
		@ErrorMessage varchar (8000)
BEGIN

Select 
	@ExpTableName = 'Expense ' + convert (varchar, convert (decimal (18,4), px.CustReimbExpenseMultiplier)) + ', Mileage ' + convert (varchar, convert (decimal (18,4), px.CustReimbMileageMultiplier)),
	@ConTableName = 'Consultant ' + convert (varchar, convert (decimal (18,4), px.CustReimbConsultantMultiplier))
From ProjectCustomTabFields px
Where px.WBS1 = @WBS1
  and px.WBS2 = @WBS2
  and px.WBS3 = @WBS3

If not exists (Select 'x' From BTEA Where TableName = @ExpTableName)
	Begin
	Set @ErrorMessage = @ExpTableName + ' is not a valid expense table.  Contact AccountingHelp@halff.com with issue.                                                              '
	RAISERROR (@ErrorMessage, 16, 1)
	End
 
If not exists (Select 'x' From BTEA Where TableName = @ConTableName)
	Begin
	Set @ErrorMessage = @ConTableName + ' is not a valid expense table.  Contact AccountingHelp@halff.com with issue.                                                              '
	RAISERROR (@ErrorMessage, 16, 1)
	End
 
END
GO
