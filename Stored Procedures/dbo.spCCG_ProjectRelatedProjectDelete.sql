SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectRelatedProjectDelete] @WBS1 varchar(32)
AS
/*
Copyright (c) 2019 Central Consulting Group. All rights reserved.
12/09/2019	David Springer
			Delete suffix Project from parent Project Related Projects grid.
			Call from Project DELETE workflow.
*/ 
BEGIN
SET NOCOUNT ON

	Delete From Projects_RelatedProjects Where CustRelatedProjectNo = @WBS1

END
GO
