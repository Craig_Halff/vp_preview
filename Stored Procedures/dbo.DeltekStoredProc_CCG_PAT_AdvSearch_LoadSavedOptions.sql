SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_AdvSearch_LoadSavedOptions]
	@TABLENAME			Nvarchar(200),
	@SAVED_OPTION_KEY	Nvarchar(200),
	@EMPLOYEEID			Nvarchar(32)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max);

	SET @sql = N'
		SELECT * FROM ' + @TABLENAME + '
			WHERE type = ''' + @SAVED_OPTION_KEY + ''' and ChangedBy = N''' + @EMPLOYEEID + '''';

	EXEC (@sql);
END;

GO
