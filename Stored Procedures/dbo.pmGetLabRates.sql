SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[pmGetLabRates]
  @txtID Nvarchar(max),
  @siRtMethod smallint,
  @intRtTableNo int,
  @siGRMethod smallint,
  @intGRTableNo int,
  @strType varchar(1) = 'C'
AS

BEGIN -- Procedure pmGetLabRates

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to retrieve Cost or Billing Rates when rate is refreshed on the client side.
-- Since the client-side rate refresh can only be done by either Cost or Billing (not both), this SP is designed to
-- return only one kind of rate. However, this SP will return rates for all rows included in the @txtID parameter
-- (for Employees, Generic Resources, and Leaf Tasks).
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  
  DECLARE @hDoc int

  DECLARE @tabID
    TABLE(RowID varchar(32) COLLATE database_default,
          ResourceID Nvarchar(20) COLLATE database_default,
          Category smallint,
          GRLBCD Nvarchar(14) COLLATE database_default,
          LaborCode Nvarchar(14) COLLATE database_default
          PRIMARY KEY(RowID))
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Load XML into a temp table variable and deallocate the XML DOM maximize performance.
  
  EXEC sp_xml_preparedocument @hDoc OUTPUT, @txtID
  
  INSERT INTO @tabID
  SELECT *
	  FROM OPENXML (@hdoc, '/Root/Row', 1)
  WITH(RowID varchar(32),
       ResourceID Nvarchar(20),
       Category smallint,
       GRLBCD Nvarchar(14),
       LaborCode Nvarchar(14))

  EXEC sp_xml_removedocument @hDoc

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get rates to be cached on the client side.

  --> Employee & Leaf Task (only when Rate Method is Labor Code) Rates

  IF (@siRtMethod = 2) -- Labor Rate Table
    BEGIN
    
      SELECT DISTINCT 1 AS Tag, NULL AS Parent,
        A.RowID AS [CR!1!P],
        @strType AS [CR!1!T],
        NULL AS [R!2!ED!Hide],
        NULL AS [R!2!!Element]
        FROM @tabID AS A
          INNER JOIN BTRRTEmpls AS R ON A.ResourceID = R.Employee AND R.TableNo = @intRtTableNo 
      UNION ALL
      SELECT 2 AS Tag, 1 AS Parent,
        RowID AS [CR!1!P],
        @strType AS [CR!1!T],
        R.EffectiveDate AS [R!2!ED!Hide],
        CASE WHEN R.StartDate = '19000101' AND R.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '|' +
        CONVERT(VARCHAR, CONVERT(INT, R.StartDate)) + '|' +
        CONVERT(VARCHAR, CONVERT(INT, R.EndDate)) + '|' +
        LTRIM(STR(R.Rate, 19, 4)) [R!2!!Element]
        FROM @tabID AS A
          INNER JOIN BTRRTEmpls AS R ON A.ResourceID = R.Employee AND R.TableNo = @intRtTableNo 
        ORDER BY [CR!1!P], Tag, [R!2!ED!Hide]
        FOR XML EXPLICIT
            
    END -- IF (@siRtMethod = 2)
    
  ELSE IF (@siRtMethod = 3) -- Labor Category Table
    BEGIN

      SELECT DISTINCT 1 AS Tag, NULL AS Parent,
        A.RowID AS [CR!1!P],
        @strType AS [CR!1!T],
        NULL AS [R!2!ED!Hide],
        NULL AS [R!2!!Element]
        FROM @tabID AS A
          INNER JOIN EM ON A.ResourceID = EM.Employee
		      LEFT JOIN BTRCTEmpls AS OC ON EM.Employee = OC.Employee AND OC.TableNo = @intRtTableNo
		      INNER JOIN BTRCTCats AS R ON R.TableNo = @intRtTableNo AND R.Category = ISNULL(OC.Category, EM.BillingCategory)
        WHERE ResourceID IS NOT NULL
      UNION ALL   
      SELECT 2 AS Tag, 1 AS Parent,
        RowID AS [CR!1!P],
        @strType AS [CR!1!T],
        R.EffectiveDate AS [R!2!ED!Hide],
        CASE WHEN R.StartDate = '19000101' AND R.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '|' +
        CONVERT(VARCHAR, CONVERT(INT, R.StartDate)) + '|' +
        CONVERT(VARCHAR, CONVERT(INT, R.EndDate)) + '|' +
        LTRIM(STR(R.Rate, 19, 4)) [R!2!!Element]
        FROM @tabID AS A
          INNER JOIN EM ON A.ResourceID = EM.Employee
		      LEFT JOIN BTRCTEmpls AS OC ON EM.Employee = OC.Employee AND OC.TableNo = @intRtTableNo
		      INNER JOIN BTRCTCats AS R ON R.TableNo = @intRtTableNo AND R.Category = ISNULL(OC.Category, EM.BillingCategory)
        WHERE ResourceID IS NOT NULL
        ORDER BY [CR!1!P], Tag, [R!2!ED!Hide]
        FOR XML EXPLICIT
    
    END -- IF (@siRtMethod = 3)
    
  ELSE IF (@siRtMethod = 4) -- Labor Code Table
    BEGIN
    
      SELECT DISTINCT 1 AS Tag, NULL AS Parent, -- Employee
        A.RowID AS [CR!1!P],
        @strType AS [CR!1!T],
        NULL AS [R!2!ED!Hide],
        NULL AS [R!2!!Element]
        FROM @tabID AS A
          INNER JOIN CFGFormat ON 1 = 1
          INNER JOIN BTRLTCodes ON TableNo = @intRtTableNo
            AND 
              ((SUBSTRING(LaborCode, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
                SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(LaborCode, LC1Start, LC1Length)) AND  
               (SUBSTRING(LaborCode, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
                SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(LaborCode, LC2Start, LC2Length)) AND  
               (SUBSTRING(LaborCode, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
                SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(LaborCode, LC3Start, LC3Length)) AND  
               (SUBSTRING(LaborCode, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
                SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(LaborCode, LC4Start, LC4Length)) AND  
               (SUBSTRING(LaborCode, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
                SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(LaborCode, LC5Start, LC5Length)))
        WHERE ResourceID IS NOT NULL
          AND (A.LaborCode IS NOT NULL AND A.LaborCode != '<none>')
      UNION ALL
      SELECT 2 AS Tag, 1 AS Parent, -- Employee
        RowID AS [CR!1!P],
        @strType AS [CR!1!T],
        RA.EffectiveDate AS [R!2!ED!Hide],
        CASE WHEN RA.StartDate = '19000101' AND RA.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '|' +
        CONVERT(VARCHAR, CONVERT(INT, StartDate)) + '|' +
        CONVERT(VARCHAR, CONVERT(INT, EndDate)) + '|' +
        LTRIM(STR(RA.Rate, 19, 4)) [R!2!!Element]
        FROM BTRLTCodes AS RA
          INNER JOIN
            (SELECT A.RowID, MIN(LaborCodeMask) AS LaborCodeMask
              FROM @tabID AS A
                INNER JOIN CFGFormat ON 1 = 1
                INNER JOIN BTRLTCodes ON TableNo = @intRtTableNo
                  AND 
                    ((SUBSTRING(LaborCode, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
                      SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(LaborCode, LC1Start, LC1Length)) AND  
                     (SUBSTRING(LaborCode, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
                      SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(LaborCode, LC2Start, LC2Length)) AND  
                     (SUBSTRING(LaborCode, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
                      SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(LaborCode, LC3Start, LC3Length)) AND  
                     (SUBSTRING(LaborCode, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
                      SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(LaborCode, LC4Start, LC4Length)) AND  
                     (SUBSTRING(LaborCode, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
                      SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(LaborCode, LC5Start, LC5Length)))
              WHERE ResourceID IS NOT NULL
                AND (A.LaborCode IS NOT NULL AND A.LaborCode != '<none>')
              GROUP BY RowID
            ) AS RB ON RA.LaborCodeMask = RB.LaborCodeMask AND TableNo = @intRtTableNo
          ORDER BY [CR!1!P], Tag, [R!2!ED!Hide]
          FOR XML EXPLICIT
                  
    END -- IF (@siRtMethod = 4)
    
  --> Generic Resource Rates.
  
  IF (@siGRMethod = 0) -- Labor Category
    BEGIN
    
      SELECT DISTINCT 1 AS Tag, NULL AS Parent,
        A.RowID AS [CR!1!P],
        @strType AS [CR!1!T],
        NULL AS [R!2!ED!Hide],
        NULL AS [R!2!!Element]
        FROM @tabID AS A
		      INNER JOIN BTRCTCats AS R ON R.TableNo = @intGRTableNo AND A.Category = R.Category
        WHERE ResourceID IS NULL
      UNION ALL   
      SELECT 2 AS Tag, 1 AS Parent,
        RowID AS [CR!1!P],
        @strType AS [CR!1!T],
        R.EffectiveDate AS [R!2!ED!Hide],
        CASE WHEN R.StartDate = '19000101' AND R.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '|' +
        CONVERT(VARCHAR, CONVERT(INT, R.StartDate)) + '|' +
        CONVERT(VARCHAR, CONVERT(INT, R.EndDate)) + '|' +
        LTRIM(STR(R.Rate, 19, 4)) [R!2!!Element]
        FROM @tabID AS A
		      INNER JOIN BTRCTCats AS R ON R.TableNo = @intGRTableNo AND A.Category = R.Category
        WHERE ResourceID IS NULL
      ORDER BY [CR!1!P], Tag, [R!2!ED!Hide]
      FOR XML EXPLICIT

    END -- IF (@siGRMethod = 0)
    
  ELSE IF (@siGRMethod = 1) -- Labor Code
    BEGIN
    
      SELECT DISTINCT 1 AS Tag, NULL AS Parent,
        A.RowID AS [CR!1!P],
        @strType AS [CR!1!T],
        NULL AS [R!2!ED!Hide],
        NULL AS [R!2!!Element]
        FROM @tabID AS A
          INNER JOIN CFGFormat ON 1 = 1
          INNER JOIN BTRLTCodes ON TableNo = @intGRTableNo
            AND 
              ((SUBSTRING(GRLBCD, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
                SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(GRLBCD, LC1Start, LC1Length)) AND  
               (SUBSTRING(GRLBCD, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
                SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(GRLBCD, LC2Start, LC2Length)) AND  
               (SUBSTRING(GRLBCD, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
                SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(GRLBCD, LC3Start, LC3Length)) AND  
               (SUBSTRING(GRLBCD, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
                SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(GRLBCD, LC4Start, LC4Length)) AND  
               (SUBSTRING(GRLBCD, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
                SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(GRLBCD, LC5Start, LC5Length)))
        WHERE ResourceID IS NULL
      UNION ALL
      SELECT 2 AS Tag, 1 AS Parent,
        RowID AS [CR!1!P],
        @strType AS [CR!1!T],
        RA.EffectiveDate AS [R!2!ED!Hide],
        CASE WHEN RA.StartDate = '19000101' AND RA.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '|' +
        CONVERT(VARCHAR, CONVERT(INT, StartDate)) + '|' +
        CONVERT(VARCHAR, CONVERT(INT, EndDate)) + '|' +
        LTRIM(STR(RA.Rate, 19, 4)) [R!2!!Element]
        FROM BTRLTCodes AS RA
          INNER JOIN
            (SELECT A.RowID, MIN(LaborCodeMask) AS LaborCodeMask
              FROM @tabID AS A
                INNER JOIN CFGForMat ON 1 = 1
                INNER JOIN BTRLTCodes ON TableNo = @intGRTableNo
                  AND 
                    ((SUBSTRING(GRLBCD, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
                      SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(GRLBCD, LC1Start, LC1Length)) AND  
                     (SUBSTRING(GRLBCD, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
                      SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(GRLBCD, LC2Start, LC2Length)) AND  
                     (SUBSTRING(GRLBCD, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
                      SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(GRLBCD, LC3Start, LC3Length)) AND  
                     (SUBSTRING(GRLBCD, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
                      SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(GRLBCD, LC4Start, LC4Length)) AND  
                     (SUBSTRING(GRLBCD, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
                      SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(GRLBCD, LC5Start, LC5Length)))
            WHERE ResourceID IS NULL
            GROUP BY RowID
          ) AS RB
        ON RA.LaborCodeMask = RB.LaborCodeMask AND TableNo = @intGRTableNo
      ORDER BY [CR!1!P], Tag, [R!2!ED!Hide]
      FOR XML EXPLICIT
  
    END -- IF (@siGRMethod = 1)

  SET NOCOUNT OFF

END -- pmGetLabRates
GO
