SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_LoadContracts] (
	@hasVendor		bit,
	@vendor			Nvarchar(20),
	@hasOptionRecur	bit
) AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_LoadContracts] 0, '', 1

	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max)

	SET @sql = N'
		SELECT convert(Nvarchar(30), Seq), ' +
			(Case WHEN @hasVendor = 1 THEN '' ELSE 'Vendor + '' '' ' END) + ' PayableNumber /*,PayableDate */, 1 as sortorder
			FROM CCG_PAT_Payable
			WHERE PayableType = ''C'' ';

	IF @hasVendor = 1
		SET	@sql = @sql + ' and Vendor = N''' + @vendor + ''' ';

	IF @hasVendor = 1
		SET @sql = @sql + '
		UNION ALL
		SELECT ''CF|'' + ID, GroupDescription, min(SortOrder)
			FROM dbo.fnCCG_PAT_CopyFrom(N''' + @vendor + ''')
			GROUP BY Vendor, ID, GroupDescription ';

	IF @hasVendor = 1 AND @hasOptionRecur = 1
		SET @sql = @sql + '
		UNION ALL
		SELECT ''RC|'' + convert(Nvarchar(30), Seq), Description, 2
			FROM CCG_PAT_RECUR
			WHERE Vendor = N''' + @vendor + ''' and status = ''A'' ';

	SET @sql = @sql + '
		ORDER BY 3, 2';

	PRINT @sql
	EXEC(@sql);
END;

GO
