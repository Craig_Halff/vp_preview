SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Pack_Delete_Template]
	@seqNumber			int,
	@deleteParamOnly	bit
AS
BEGIN
	SET NOCOUNT ON;

	If @deleteParamOnly = 0 BEGIN
		DELETE FROM CCG_EI_TemplateParameters WHERE TemplateSeq = @seqNumber;
		DELETE FROM CCG_EI_Templates WHERE Seq = @seqNumber;
	END
	ELSE DELETE FROM CCG_EI_TemplateParameters WHERE Seq = @seqNumber;
END;
GO
