SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PNDelConsultant]
  @strPlanID varchar(32),
  @strConsultantID varchar(32)
AS

BEGIN -- Procedure PNDelConsultant

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to delete a Consultant row and its associated rows.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    
  DELETE PNPlannedConsultant WHERE PlanID = @strPlanID AND ConsultantID = @strConsultantID
  DELETE PNBaselineConsultant WHERE PlanID = @strPlanID AND ConsultantID = @strConsultantID
  DELETE PNConsultant WHERE PlanID = @strPlanID AND ConsultantID = @strConsultantID

  SET NOCOUNT OFF

END -- PNDelConsultant
GO
