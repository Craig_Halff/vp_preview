SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_PAT_Load_Stage_ML]
	@Stage varchar(50),
	@UICultureName varchar(10)
AS
BEGIN
	-- EXEC [dbo].[spCCG_PAT_Load_Stage_ML] 'Pending','en-US'
	SET NOCOUNT ON;

	SELECT *
        FROM CCG_PAT_ConfigStagesDescriptions sd
		WHERE Stage = @Stage and UICultureName = @UICultureName and ISNULL(EmailSubject, '') <> '';
END;
GO
