SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FormExport_UpdatePayableBatch] ( @Batch nvarchar(max), @IDs varchar(max))
             AS EXEC spCCG_PAT_FormExport_UpdatePayableBatch @Batch,@IDs
GO
