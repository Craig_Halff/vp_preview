SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_All_GetDependencies] ( @depLevel int)
             AS EXEC spCCG_All_GetDependencies @depLevel
GO
