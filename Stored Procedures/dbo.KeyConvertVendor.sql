SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyConvertVendor]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg Nvarchar(1000) output,
@UserName Nvarchar(32) = 'VisionKeyConvert', -- Add by DP 4.0
@Period integer = 0,
@EntityType Nvarchar(20)
as
begin -- Procedure
	set nocount on
	declare @OldValue Nvarchar(20)
	declare @NewValue Nvarchar(20)
	Declare @Existing integer
	declare @Entity Nvarchar(20)
	declare @length integer
	declare @CvtType integer
	declare @KeysFetch integer
	declare @RetVal integer
	declare @diag smallint
	declare @Errmsg2 Nvarchar(1000),
			@custlabel	Nvarchar(40),
			@Custlabelplural Nvarchar(40)
	declare @TableName 		Nvarchar(100)
	declare @Sql 				Nvarchar(1000)
	declare @message nvarchar(max)

	set @diag = 0
	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'Vendor'
	set @length = 20
	set @Existing = 0
	set @KeysFetch = 0

	execute InsertKeyCvtDriver @Entity -- populate driver table

-- Add by DP 4.0
	delete from keyconvertDriver where entity = N'Vendor' and TableName = N'Clendor'
	DECLARE @OldName Nvarchar(125)
	DECLARE @NewName Nvarchar(125)
	DECLARE @PKey Nvarchar(50)
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0
--
	select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'vendorLabel'
	select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'vendorLabelPlural'

	declare KeysCursor cursor for
		select OldKey, NewKey, Pkey from KeyConvertWork where Entity = @Entity
	open KeysCursor
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue, 
		@PKey

	set @KeysFetch = @@Fetch_Status

While (@KeysFetch = 0)
	begin
		begin tran
-- Add by DP 4.0
		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany Nvarchar(14)
			DECLARE @RecordCount integer
			set @LastPostSeq = 0

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				set @RecordCount = (select count(*) from KeyConvertWork where Entity = @Entity)

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),'KC','Y',  @custlabel + ' Key Convert ('+rtrim(convert(Nvarchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, left(@custlabel + ' Key Convert', 40), @custlabel);
			end

			Set @PostControlFlag = 1
		end

		set @Existing = 0
		select @Existing =  1, @NewName = Name from Clendor where Vendor = @NewValue and (vendorIND = 'Y' and vendor is not null)
		select @OldName = Name from Clendor where Vendor = @OldValue and (vendorIND = 'Y' and vendor is not null)
--

	If (@Existing = 1)
		begin
		  if (@DeleteExisting = 0)
			begin
			SET @message = dbo.GetMessage('MsgNumAnExistingConvWillNotCont',@custlabel,@NewValue,@custlabel,@custLabelPlural,'','','','','')
			RAISERROR(@message,16,3)
				close keysCursor
				deallocate keysCursor
				if (@@Trancount > 0)
				   ROLLBACK TRANSACTION
				return(50002) -- user defined error
			end
			if (@NewValue = @OldValue)
				begin
				SET @message = dbo.GetMessage('MsgAreSameNumConvNotCont',@custlabelPlural,'','','','','','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end

/*new for 3.0*/
			Update VEAccounting
				Set VEAccounting.ThisYear1099 = VEAccounting.ThisYear1099 + old.ThisYear1099,  
				VEAccounting.LastYear1099 = VEAccounting.LastYear1099 + old.LastYear1099
				from VEAccounting old inner join VEAccounting on (old.Company = VEAccounting.Company)
				Where VEAccounting.Vendor = @newValue And old.Vendor = @OldValue
	
--VEAccounting	Vendor
--VEAccounting	Company
	delete from VEAccounting where Vendor = @OldValue 
				and exists (select 'x' from VEAccounting new 
				where new.Vendor = @newValue and new.Company = VEAccounting.Company)

			delete from VEFileLinks where Vendor = @OldValue 
				and exists (select 'x' from VEFileLinks new 
				where new.Vendor = @newValue and new.LinkID = VEFileLinks.LinkID)

			delete from form1099data where vendor  = @OldValue
			and Exists (select 'x' from form1099data as tmp 
				where tmp.Year = form1099data.year 
				and tmp.Company = form1099data.company 
				and tmp.Vendor = @newValue)
-- Removed by DP
--			Delete From VE Where Vendor = @OldValue

--VendorItem	Item
--VendorItem	Vendor
--TotalQty and TotalAmt, and use Max(LastPriceDate) 
			Update VendorItem
				Set vendorItem.TotalQty = vendorItem.TotalQty + old.TotalQty,  
				vendorItem.TotalAmt = vendorItem.TotalAmt + old.TotalAmt
				from VendorItem old inner join vendorItem on (old.Item = vendorItem.Item)
				Where vendorItem.Vendor = @newValue
				  And old.Vendor = @OldValue

delete from VendorItem where Vendor = @OldValue 
				and exists (select 'x' from VendorItem new 
				where new.Vendor = @newValue and new.Item = VendorItem.Item)
/*end for 3.0*/
/* New for 2.0
BudgetMilestoneSeq     AlertID 
BudgetMilestoneSeq     PlanID  
BudgetMilestoneSeq     Account 
BudgetMilestoneSeq     Vendor  */

			Delete from BudgetMilestoneSeq where Vendor = @OldValue 
				and exists (select 'x' from BudgetMilestoneSeq new where  BudgetMilestoneSeq.AlertID = new.AlertID and
																		BudgetMilestoneSeq.PlanID = new.PlanID and
																		new.Vendor =  @newValue and
																		BudgetMilestoneSeq.Account = new.Account)
/*
VEProjectAssocTemplate Vendor  
VEProjectAssocTemplate WBS1    
VEProjectAssocTemplate WBS2    
VEProjectAssocTemplate WBS3    
*/

			Delete from VEProjectAssocTemplate where Vendor = @OldValue 
				and exists (select 'x' from VEProjectAssocTemplate new where  VEProjectAssocTemplate.WBS1 = new.WBS1 and
																		VEProjectAssocTemplate.WBS2 = new.WBS2 and
																		VEProjectAssocTemplate.WBS3 = new.WBS3 and 
																		new.Vendor =  @newValue)

			Delete from CustomProposalVendor where Vendor = @OldValue 
				and exists (select 'x' from CustomProposalVendor new where  CustomProposalVendor.CustomPropID = new.CustomPropID and
																		CustomProposalVendor.SectionID = new.SectionID and
																		new.Vendor =  @newValue)


/*
OpportunityVEAssoc
OpportunityID
Vendor
*/

if (@Diag =1)
	print 'Delete from OpportunityVEAssoc where Vendor = N''' +  @OldValue + '''
				and exists (select ''x'' from OpportunityVEAssoc new where  
																		OpportunityVEAssoc.OpportunityID = new.OpportunityID and
																		OpportunityVEAssoc.Vendor = N''' + @newValue + ''')'
			Delete from OpportunityVEAssoc where Vendor = @OldValue 
				and exists (select 'x' from OpportunityVEAssoc new where  
																		OpportunityVEAssoc.OpportunityID = new.OpportunityID and
																		new.Vendor =  @newValue)
/*End 2.0*/


			update EB
				set eb.AmtBud =  eb.AmtBud + old.AmtBud,
				eb.BillBud = eb.BillBud + old.BillBud,
				eb.EtcAmt =  eb.EtcAmt + old.EtcAmt,
				eb.EacAmt = eb.EacAmt + old.EacAmt
				from eb old inner join eb on (eb.WBS1 = old.WBS1 and eb.WBS2 = old.WBS2 and eb.WBS3 = old.WBS3 and eb.Account = old.Account)
				where
					eb.Vendor = @newValue and
					old.Vendor = @OldValue

			Delete from EB where Vendor = @OldValue 
				and exists (select 'x' from EB new where  EB.WBS1 = new.WBS1 and
																		EB.WBS2 = new.WBS2 and
																		EB.WBS3 = new.WBS3 and 
																		new.Vendor =  @newValue and
																		eb.Account = new.Account)

/* 1.0 Changed VE.LinkedClient to Clendor.ClientID */
			Update Clendor Set ClientInd = isnull((Select ClientInd from Clendor a Where a.Vendor=@OldValue and ClientInd = 'Y' and (vendorIND = 'Y' and vendor is not null)),'N')
			From Clendor Where Clendor.Vendor=@newValue and ClientInd = 'N' and (vendorIND = 'Y' and vendor is not null)

			Update Clendor Set VendorInd = isnull((Select VendorInd from Clendor a Where a.Vendor=@OldValue and VendorInd = 'Y' and (vendorIND = 'Y' and vendor is not null)),'N')
			From Clendor Where Clendor.Vendor=@newValue and VendorInd = 'N' and (vendorIND = 'Y' and vendor is not null)


		   declare @testVal Nvarchar(1)
			set @TestVal = 'N'
			Select  @testVal = 'Y' From VO Inner Join VO As VOtmp On VO.Voucher = VOTmp.Voucher
			Where VO.Vendor = @oldValue And VOtmp.Vendor = @newValue
			if (@testVal = 'Y')
				begin
				SET @message = dbo.GetMessage('MsgDupVoucherNumExistsNotContinue',@custlabelplural,@custlabel,@OldValue,@custlabel,@NewValue,'','','','')
				RAISERROR(@message,16,3)
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end

			set @TestVal = 'N'
			Select  @testVal = 'Y' From VO Inner Join VO As VOtmp On VO.Invoice = VOTmp.Invoice and VO.InvoiceDate = VOTmp.InvoiceDate and VO.Company = VOTmp.Company 
			Where VO.Vendor = @oldValue And VOtmp.Vendor = @newValue
			if (@testVal = 'Y')
				begin
			   	RAISERROR ('Duplicate invoice numbers/invoice date/company exist for these %s. Conversion will not continue.  Old %s:%s New %s:%s',
			      16, 3,  @custlabelplural,@custlabel,@OldValue,@custlabel,@NewValue)
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end

--VEAddress(Vendor, Address) Preserver duplicate address codes by renumbering them
			declare @NewAddress Nvarchar(20)
			declare @OldAddress Nvarchar(20)
			declare @AddressFetch integer
			declare @AppendInt	integer
			set @AppendInt = 0
			declare AddressCursor cursor for select Address from VEAddress where Vendor = @OldValue 
				and exists (select 'x' from VEAddress new where new.Vendor = @newValue and VEAddress.Address = new.Address)
			open addressCursor
			fetch next from AddressCursor into @OldAddress
			set @AddressFetch = @@FETCH_STATUS
			while (@AddressFetch = 0)
			begin
				SET @AppendInt = @AppendInt + 1
				Set @NewAddress = left(@OldAddress,20 - len(convert(Nvarchar,@AppendInt))) + convert(Nvarchar,@AppendInt)

				if not exists (select 'x' from VEAddress where (Vendor = @oldValue or Vendor = @newValue) and Address = @NewAddress)
				begin
					update VEAddress set Address = @NewAddress where Vendor = @OldValue and Address = @OldAddress
					update OpportunityVEAssoc set VEAddress = @NewAddress where Vendor = @OldValue and VEAddress = @OldAddress
					update VEProjectAssoc set VEAddress = @NewAddress where Vendor = @OldValue and VEAddress = @OldAddress
					update Contacts set VEAddress = @NewAddress where Vendor = @OldValue and VEAddress = @OldAddress

					update VEProjectAssocTemplate set VEAddress = @NewAddress where Vendor = @OldValue and VEAddress = @OldAddress
					update apMaster set Address = @NewAddress where Vendor = @OldValue and Address = @OldAddress
					update apppChecks set Address = @NewAddress where Vendor = @OldValue and Address = @OldAddress
					update cvMaster set Address = @NewAddress where Vendor = @OldValue and Address = @OldAddress
					update ItemRequestVendorDetail set Address = @NewAddress where Vendor = @OldValue and Address = @OldAddress
					update POMaster set Address = @NewAddress where Vendor = @OldValue and Address = @OldAddress
					update POPQVendorDetail set Address = @NewAddress where Vendor = @OldValue and Address = @OldAddress
					update POPRVendorDetail set Address = @NewAddress where Vendor = @OldValue and Address = @OldAddress
					update POVoucherMaster set Address = @NewAddress where Vendor = @OldValue and Address = @OldAddress
					update VO set Address = @NewAddress where Vendor = @OldValue and Address = @OldAddress

					fetch next from AddressCursor into @OldAddress
					set @AddressFetch = @@FETCH_STATUS
					SET @AppendInt = 0
				end
			end
			close addressCursor
			deallocate addressCursor

			update VEAddress set PrimaryInd ='N' where Vendor = @OldValue

			Delete from VEAddress where Vendor = @OldValue 
				and exists (select 'x' from VEAddress new where new.Vendor = @newValue and
				VEAddress.Address = new.Address)
--

			
			Delete From BTEVVends Where Vendor = @OldValue 
			      And TableNo In (Select TableNo From BTEVVends Where Vendor = @newValue)

			delete from VEProjectAssoc where VEProjectAssoc.Vendor = @OldValue  and
			exists (select 'x' from VEProjectAssoc new where new.Vendor = @newValue and VEProjectAssoc.WBS1 = new.WBS1)

			delete from VESubscr where VESubscr.Vendor = @OldValue and
			exists (select 'x' from VESubscr new where new.Vendor = @newValue and VESubscr.username = new.username)

				/*handle duplicate custom tab records, possible when copying one project to another*/
				declare custTable cursor for
				select tableName from FW_CustomGrids where infocenterarea = N'Vendors'
				open custTable
				fetch next from custTable into @TableName
				while (@@FETCH_STATUS = 0)
				begin
					set @Sql = 'Delete FROM ' + @TableName + ' WHERE ' + @TableName + '.Vendor =N''' +  @OldValue + ''' and ' +
								  'exists (select ''x'' from ' + @TableName + ' new where new.Vendor = N''' + @NewValue + ''' and new.seq = ' + @TableName + '.seq)'
					if (@Diag = 1)
						print @Sql
					execute(@Sql)
					fetch next from custTable into @TableName
				end
				close custTable
				deallocate custTable				
	    END --Existing
---- Add by DP 4.0
--		Else	-- Not Existing
--		begin
--			select * into #TempKeyConvert from Clendor where Vendor = @OldValue and (vendorIND = 'Y' and vendor is not null)
--			Update #TempKeyConvert set Vendor=@NewValue
--			Insert Clendor select * from #TempKeyConvert
--			Drop Table #TempKeyConvert
--			set @NewName = @OldName
--		end

		exec @RetVal = KeyCvt @Entity = @Entity,
					 @CvtType = @CvtType,
					 @Length = 20,
					 @ErrMsg = @ErrMsg2 output,
					 @Diag = @Diag,
					 @NewValue = @NewValue,
					 @OldValue = @OldValue,
					 @ConstrFlag = 3 -- UpdateTables


		--Delete From Clendor Where Vendor = @OldValue and (vendorIND = 'Y' and vendor is not null)
		Update Clendor Set Vendor = @NewValue Where Vendor = @OldValue and (vendorIND = 'Y' and vendor is not null)
--
		if (@RetVal = 0)
			begin
-- Add by DP 4.0
				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq)
					select PKey, Entity, OldKey, NewKey, @OldName, @NewName, @Period, @LastPostSeq from keyConvertWork where Pkey = @PKey
				end
--
				delete from keyConvertWork where current of keysCursor
				commit Transaction
			end
		else
			begin
				set @ErrMsg = @ErrMsg + @ErrMsg2
				rollback transaction
			end
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue,
		@PKey
	set @KeysFetch = @@Fetch_Status
	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	return(0)
End -- Procedure
GO
