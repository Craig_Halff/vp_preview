SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_AddInputColumn50](
	@Header			varchar(255),
	@FieldName		varchar(255),
	@FieldType		varchar(255),
	@NullString		varchar(20),
	@ColType		char(1),
	@ColSeq			int,
	@ColDecimals	int,
	@ColWidth		int = 0,
	@ColEnabledWBS1	char(1)='Y',
	@ColEnabledWBS2	char(1)='Y',
	@ColEnabledWBS3	char(1)='Y',
	@ColStatus		char(1)='Y',
	@ColViewRoles			varchar(255)='',
	@ColEditRoles			varchar(255)='',
	@ColRequiredRoles		varchar(255)='',
	@ColRequiredExpression	varchar(255)='')
AS BEGIN
/*
	Copyright (c) 2011 Central Consulting Group.  All rights reserved.
	
	select * from CCG_EI_ConfigCustomColumns
	select * from CCG_EI_CustomColumns

	Examples:
	exec spCCG_EI_AddInputColumn50 'Fee % Compl', 'FeePctComplete', 'decimal(19,5)', 'NULL', 'N', 100, 0, 0, 'Y', 'Y', 'N'
	exec spCCG_EI_AddInputColumn50 'Fee Billing', 'FeeBilling', 'decimal(19,5)', 'NULL', 'N', 100, 2, 0
*/
	set nocount on
	declare @sql	varchar(max)
	declare @debug	char(1)
	set @debug='N'
	
	print @Header
	set @Header = REPLACE(@Header,'''','''''')
	
	-- Add column into CCG_EI_CustomColumns
	set @sql='if not exists (select ''x'' from syscolumns a,sysobjects b where a.id=b.id and a.name=''' + @FieldName + ''' and b.name=''CCG_EI_CustomColumns'') ' + CHAR(13) + CHAR(10) +
		' begin' + CHAR(13) + CHAR(10) +
		'	print ''	Adding column ' + @FieldName + ' to CCG_EI_CustomColumns'' ' + CHAR(13) + CHAR(10) +
		'	alter table CCG_EI_CustomColumns with nocheck add ' + @FieldName + ' ' + @FieldType + ' ' + @NullString + CHAR(13) + CHAR(10) +
		' end'
	if @debug='Y' print @sql
	execute(@sql)
	
	set @sql='if not exists (select ''x'' from syscolumns a,sysobjects b where a.id=b.id and a.name=''' + @FieldName + '_ModEmp'' and b.name=''CCG_EI_CustomColumns'') ' + CHAR(13) + CHAR(10) +
		' begin' + CHAR(13) + CHAR(10) +
		'	print ''	Adding column ' + @FieldName + '_ModEmp to CCG_EI_CustomColumns'' ' + CHAR(13) + CHAR(10) +
		'	alter table CCG_EI_CustomColumns with nocheck add ' + @FieldName + '_ModEmp varchar(20) ' + @NullString + CHAR(13) + CHAR(10) +
		' end'
	if @debug='Y' print @sql
	execute(@sql)

	-- Add column into EI configuration
	set @sql='if not exists (select ''x'' from CCG_EI_ConfigCustomColumns where DatabaseField=''' + @FieldName + ''')' + CHAR(13) + CHAR(10) +
		' begin' + CHAR(13) + CHAR(10) +
		'	print ''	Adding configuration row for ' + @FieldName + '''' + CHAR(13) + CHAR(10) +
		'	insert into CCG_EI_ConfigCustomColumns (Label, DataType, DisplayOrder, InitialValue,' + CHAR(13) + CHAR(10) +
		'		DatabaseField, ColumnUpdate, Width, Decimals,' + CHAR(13) + CHAR(10) +
		'		ViewRoles, EditRoles, RequiredRoles, RequiredExpression,' + CHAR(13) + CHAR(10) +
		'		EnabledWBS1, EnabledWBS2, EnabledWBS3, Status, ModDate, ModUser)' + CHAR(13) + CHAR(10) +
		'	values (''' + @Header + ''', ''' + @ColType + ''', ' + Cast(@ColSeq as varchar(10)) + ', ''dbo.fnCCG_EI_' + @FieldName + ''',' + CHAR(13) + CHAR(10) +
		'		''' + @FieldName + ''', ''spCCG_EI_' + @FieldName  + ''', ' + Cast(@ColWidth as varchar(10)) + ', ' + + Cast(@ColDecimals as varchar(10)) + + ', ' + CHAR(13) + CHAR(10) +
		'		''' + @ColViewRoles   + ''', ''' + @ColEditRoles   + ''', ''' + @ColRequiredRoles + ''', ''' + @ColRequiredExpression + ''',   ' + CHAR(13) + CHAR(10) +
		'		''' + @ColEnabledWBS1 + ''', ''' + @ColEnabledWBS2 + ''', ''' + @ColEnabledWBS3   + ''', ''' + @ColStatus             + ''',   ' + CHAR(13) + CHAR(10) +
		'		GETDATE(),''CCG'')' + CHAR(13) + CHAR(10) +
		' end'
	if @debug='Y' print @sql
	execute(@sql)
	
	set @sql= 'if not exists (select ''x'' from CCG_EI_ConfigCustomColumnsDescriptions where Label=''' + @Header + ''')' + CHAR(13) + CHAR(10) +
		' begin' + CHAR(13) + CHAR(10) +
		'	insert into CCG_EI_ConfigCustomColumnsDescriptions (Seq,UICultureName, Label)' + CHAR(13) + CHAR(10) +
		'	(select Seq,' + '''en-US''' + ', Label from CCG_EI_ConfigCustomColumns where Label=''' + @Header + ''')' + CHAR(13) + CHAR(10) +
		' end'
	if @debug='Y' print @sql
	execute(@sql)
END
GO
