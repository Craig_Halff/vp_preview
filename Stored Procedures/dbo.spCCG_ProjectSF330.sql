SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[spCCG_ProjectSF330] @WBS1 varchar (32), @Code varchar (10), @Type varchar (10)
AS
/*
Copyright 2019 (c) Central Consulting Group, Inc.  All rights reserved.
12/04/2019	David Springer initial creation
			SF330 Percents grid with SF330 Percent Code, SF330 Percent
			Call this from Insert and Delete workflow on Project Codes table.
			Type = Insert or Delete
*/
BEGIN
SET NOCOUNT ON

If LEN (@Code) = 3 
	Begin
	If @Type = 'Insert' and not exists (Select * From Projects_SF330Percents Where WBS1 = @WBS1 and CustSF330PercentCode = @Code)
		Begin
		Insert Into Projects_SF330Percents
		(WBS1, WBS2, WBS3, Seq, CustSF330PercentCode)
		Values (@WBS1, ' ', ' ', @WBS1 + @Code, @Code)
		End

	If @Type = 'Delete'
		Begin
		Delete Projects_SF330Percents
		Where WBS1 = @WBS1 and custSF330PercentCode = @Code
		End
	End
END
GO
