SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_KeyConvert_DoPendingItems] (@entityType varchar(max) = '')
AS BEGIN
	-- EXEC [spCCG_KeyConvert_GetSessionDetails] null, 'Vendor'
	declare @EI bit = (case when exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[spCCG_EI_KeyConvert]') AND type in (N'P', N'PC')) then 1 else 0 end)
	declare @PAT bit = (case when exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[spCCG_PAT_KeyConvert]') AND type in (N'P', N'PC')) then 1 else 0 end)
	declare @ARM bit = (case when exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[spCCG_ARM_KeyConvert]') AND type in (N'P', N'PC')) then 1 else 0 end)

	declare @countAccounts int=0, @countEmployees int=0, @countVendors int=0, @countWBS1 int=0, @countWBS2 int=0, @countWBS3 int=0, @errorsVendors int=0
	declare @seq uniqueidentifier
	set @seq = newid()

	declare @keyConvertResults TABLE (RowsAffected int, ErrorMessage varchar(max))
	declare @Entity varchar(50), @NewKey varchar(32), @NewKey2 varchar(7), @NewKey3 varchar(7),
		@OldKey varchar(32), @OldKey2 varchar(7), @OldKey3 varchar(7), @PKey uniqueidentifier

	declare cursorKeyConvert cursor for
		select w.Entity, w.NewKey, w.NewKey2, w.NewKey3, w.OldKey, w.OldKey2, w.OldKey3, w.PKey
			from KeyConvertWork w
			where w.PKey not in (select l.PKey from CCG_KeyConvertLog l where l.Entity = w.Entity and isnull(l.Notes,'') = '')
				and (Entity = @entityType or isnull(@entityType,'') = '')

	open cursorKeyConvert
	fetch next from cursorKeyConvert into @Entity, @NewKey, @NewKey2, @NewKey3, @OldKey, @OldKey2, @OldKey3, @PKey

	BEGIN TRY
		BEGIN TRAN
		while @@FETCH_STATUS = 0
		begin
			declare @spCall varchar(max) = ''
	
			if (@Entity <> 'WBS2' and @Entity <> 'WBS3') or (@Entity = 'WBS2' and @OldKey = @NewKey) 
				or (@Entity = 'WBS3' and @OldKey = @NewKey and @OldKey2 = @NewKey2)						-- Make sure we can do this key convert
			begin
				delete from @keyConvertResults 
				if @PAT=1 set @spCall = 'spCCG_PAT_KeyConvert @What='''+@Entity+''', @OldValue='''+@OldKey+''', @NewValue='''+@NewKey+''', @WBS1='''+isnull(@OldKey,'')+''', @WBS2='''+isnull(@OldKey2,'')+''''
				insert into @keyConvertResults exec(@spCall)

				if @PAT=0 or @Entity <> 'Vendor' or not exists (select 1 from @keyConvertResults where RowsAffected < 0) 
				begin 
					if @EI=1 set @spCall = 'spCCG_EI_KeyConvert @What='''+@Entity+''', @OldValue='''+@OldKey+''', @NewValue='''+@NewKey+''', @WBS1='''+isnull(@OldKey,'')+''', @WBS2='''+isnull(@OldKey2,'')+''''
					insert into @keyConvertResults exec(@spCall)
					if @ARM=1 set @spCall = 'spCCG_ARM_KeyConvert @What='''+@Entity+''', @OldValue='''+@OldKey+''', @NewValue='''+@NewKey+''', @WBS1='''+isnull(@OldKey,'')+''''
					insert into @keyConvertResults exec(@spCall)

					if not exists (select * from @keyConvertResults where RowsAffected < 0)		-- If no errors encountered...
					begin 
						if @Entity = 'Account' set @countAccounts += 1
						else if @Entity = 'Employee' set @countEmployees += 1
						else if @Entity = 'Vendor' set @countVendors += 1
						else if @Entity = 'WBS1' set @countWBS1 += 1
						else if @Entity = 'WBS2' set @countWBS2 += 1
						else if @Entity = 'WBS3' set @countWBS3 += 1

						insert into CCG_KeyConvertLog (SessionSeq, Products, Entity, OldKey, OldKey2, OldKey3, NewKey, NewKey2, NewKey3, PKey)
							values (@seq, case when @EI=1 then 'EI;' else '' end + case when @PAT=1 then 'PAT;' else '' end + case when @ARM=1 then 'ARM;' else '' end,
								@Entity, @OldKey, @OldKey2, @OldKey3, @NewKey, @NewKey2, @NewKey3, @PKey)
					end
				end

				-- Log errors 
				if exists (select 1 from @keyConvertResults where RowsAffected < 0)
				begin
					insert into CCG_KeyConvertLog (SessionSeq, Products, Entity, OldKey, OldKey2, OldKey3, NewKey, NewKey2, NewKey3, PKey, Notes)
						values (
							@seq, case when @EI=1 then 'EI;' else '' end + case when @PAT=1 then 'PAT;' else '' end + case when @ARM=1 then 'ARM;' else '' end,
							@Entity, @OldKey, @OldKey2, @OldKey3, @NewKey, @NewKey2, @NewKey3, @PKey, 
								(select STUFF((select ErrorMessage + '<br/>' from @keyConvertResults where RowsAffected < 0 FOR XML PATH(''), TYPE).value('.', 'nvarchar(MAX)'),1,0,''))
						)
					set @errorsVendors += 1
				end
			end
			fetch next from cursorKeyConvert into @Entity, @NewKey, @NewKey2, @NewKey3, @OldKey, @OldKey2, @OldKey3, @PKey
		end

		insert into CCG_KeyConvertSessions (Seq, CountAccounts, CountEmployees, CountVendors, CountWBS1s, CountWBS2s, CountWBS3s, ErrorsVendors)
			select @seq, @countAccounts, @countEmployees, @countVendors, @countWBS1, @countWBS2, @countWBS3, @errorsVendors

		COMMIT TRAN
		select 0 as ReturnValue, '' as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		select -1 as  ReturnValue, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage
	END CATCH

	close cursorKeyConvert
	deallocate cursorKeyConvert
END
GO
