SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_FileHelper_getXFDFDB] ( @FILEID varchar(50), @revisionSeq int)
             AS EXEC spCCG_EI_FileHelper_getXFDFDB @FILEID,@revisionSeq
GO
