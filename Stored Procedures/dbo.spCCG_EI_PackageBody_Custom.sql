SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_EI_PackageBody_Custom] (
	@WBS1			 /*N*/varchar(30) = NULL
)
AS BEGIN
/* Copyright (c) 2013 Central Consulting Group. All rights reserved. */

/* Returns setup data/options mainly from the project info center that is used by
	the package building software in XML format.

	This will need to modified when pic fields are added or removed from the standard set. */

/*
 exec spCCG_EI_PackageBody '2003005.00'
*/
--declare @WBS1 varchar(20)

-- Declarations
------------------------------------------------

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

declare @CustFinalInvPkgSubExpOrder varchar(20),
	@CustFinalInvPkgIncludeSubInvoicesMarkup varchar(20),
	@CustFinalInvPkgIncludeExpReceiptsMarkup varchar(20),
	@CustFinalInvPkgIncludeInvoiceMarkup varchar(20),
	@CustFinalInvPkgSubInvoicesMinimum decimal (19,5),
	@CustFinalInvPkgExpReceiptsMinimum decimal (19,5),
	@CustFinalInvPkgIncludeDirectExp varchar(20),
	@CustFinalInvPkgIncludeDirectSub varchar(20)

declare @bCustFinalInvPkgSubExpOrder bit,
	@bCustFinalInvPkgIncludeSubInvoicesMarkup bit,
	@bCustFinalInvPkgIncludeExpReceiptsMarkup bit,
	@bCustFinalInvPkgIncludeInvoiceMarkup bit,
	@bCustFinalInvPkgSubInvoicesMinimum bit,
	@bCustFinalInvPkgExpReceiptsMinimum bit,
	@bCustFinalInvPkgIncludeDirectExp bit,
	@bCustFinalInvPkgIncludeDirectSub bit,
	@bCustFinalInvPkgIncludeInvoice bit,
	@bCustFinalInvPkgEmailTemplate bit,
	@bCustFinalInvPkgIncludeTimesheetData bit,
	@bCustFinalInvPkgTimesheetTemplate bit,
	@bCustFinalInvPkgIncludeSubInvoices bit,
	@bCustFinalInvPkgIncludeExpReceipts bit,
	@bProjects_FinalInvPkgUserDocuments bit

set @CustFinalInvPkgIncludeInvoiceMarkup = 'N'--default

-- Verify which UDFs exist
------------------------------------------------
SELECT @bCustFinalInvPkgSubExpOrder = ISNULL(MAX(CASE WHEN UPPER(colName) = UPPER('CustFinalInvPkgSubExpOrder') THEN 1 ELSE null END), 0),
		@bCustFinalInvPkgIncludeSubInvoicesMarkup = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeSubInvoicesMarkup') then 1 else null end), 0),
		@bCustFinalInvPkgIncludeExpReceiptsMarkup = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeExpReceiptsMarkup') then 1 else null end), 0),
		@bCustFinalInvPkgSubInvoicesMinimum = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgSubInvoicesMinimum') then 1 else null end), 0),
		@bCustFinalInvPkgExpReceiptsMinimum = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgExpReceiptsMinimum') then 1 else null end), 0),
		@bCustFinalInvPkgIncludeInvoiceMarkup = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeInvoiceMarkup') then 1 else null end), 0),
		@bCustFinalInvPkgIncludeDirectExp = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeDirectExp') then 1 else null end), 0),
		@bCustFinalInvPkgIncludeDirectSub = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeDirectSub') then 1 else null end), 0),
		@bCustFinalInvPkgIncludeInvoice = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeInvoice') then 1 else null end), 0),
		@bCustFinalInvPkgEmailTemplate = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgEmailTemplate') then 1 else null end), 0),
		@bCustFinalInvPkgIncludeTimesheetData = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeTimesheetData') then 1 else null end), 0),
		@bCustFinalInvPkgTimesheetTemplate = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgTimesheetTemplate') then 1 else null end), 0),
		@bCustFinalInvPkgIncludeSubInvoices = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeSubInvoices') then 1 else null end), 0),
		@bCustFinalInvPkgIncludeExpReceipts = ISNULL(MAX(case when UPPER(colName) = UPPER('CustFinalInvPkgIncludeExpReceipts') then 1 else null end), 0)
	FROM (
		SELECT o.name as oname, type as otype, c.name as colName, c.system_type_id
			FROM sys.objects o
				LEFT JOIN sys.columns c ON o.object_id = c.object_id
			WHERE o.object_id = OBJECT_ID(N'[' + Replace('ProjectCustomTabFields', 'dbo.', 'dbo].[') + ']')
		) d
	GROUP BY oname, otype;

SELECT @bProjects_FinalInvPkgUserDocuments = CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END
	FROM sys.objects o
	WHERE o.object_id = OBJECT_ID(N'[' + Replace('Projects_FinalInvPkgUserDocuments', 'dbo.', 'dbo].[') + ']')

-- Get the values for the UDFs
------------------------------------------------
DECLARE @T AS TABLE(
	CustFinalInvPkgSubExpOrder varchar(20),
	CustFinalInvPkgIncludeSubInvoicesMarkup varchar(20),
	CustFinalInvPkgIncludeExpReceiptsMarkup varchar(20),
	CustFinalInvPkgSubInvoicesMinimum decimal (19,5),
	CustFinalInvPkgExpReceiptsMinimum decimal (19,5),
	CustFinalInvPkgIncludeInvoiceMarkup varchar(20),
	CustFinalInvPkgIncludeDirectExp varchar(20),
	CustFinalInvPkgIncludeDirectSub varchar(20))

DECLARE @sql VARCHAR(MAX) = '
		SELECT '+(CASE WHEN @bCustFinalInvPkgSubExpOrder=1 THEN 'CustFinalInvPkgSubExpOrder' ELSE '''INVOICE''' END)+',
				'+(CASE WHEN @bCustFinalInvPkgIncludeSubInvoicesMarkup=1 THEN 'CustFinalInvPkgIncludeSubInvoicesMarkup' ELSE '''Y''' END)+',
				'+(CASE WHEN @bCustFinalInvPkgIncludeExpReceiptsMarkup=1 THEN 'CustFinalInvPkgIncludeExpReceiptsMarkup' ELSE '''Y''' END)+',
				'+(CASE WHEN @bCustFinalInvPkgSubInvoicesMinimum=1 THEN 'CustFinalInvPkgSubInvoicesMinimum' ELSE '''0.00''' END)+',
				'+(CASE WHEN @bCustFinalInvPkgExpReceiptsMinimum=1 THEN 'CustFinalInvPkgExpReceiptsMinimum' ELSE '''0.00''' END)+',
				'+(CASE WHEN @bCustFinalInvPkgIncludeInvoiceMarkup=1 THEN 'CustFinalInvPkgIncludeInvoiceMarkup' ELSE '''Y''' END)+',
				'+(CASE WHEN @bCustFinalInvPkgIncludeDirectExp=1 THEN 'CustFinalInvPkgIncludeDirectExp' ELSE '''N''' END)+',
				'+(CASE WHEN @bCustFinalInvPkgIncludeDirectSub=1 THEN 'CustFinalInvPkgIncludeDirectSub' ELSE '''N''' END)+'
			FROM ProjectCustomTabFields
			WHERE WBS1 = '''+@WBS1+''' and WBS2 = '' '''
INSERT INTO @T EXECUTE(@sql)

--SELECT * FROM @T
SELECT TOP 1 @CustFinalInvPkgSubExpOrder = CustFinalInvPkgSubExpOrder,
		@CustFinalInvPkgIncludeSubInvoicesMarkup = CustFinalInvPkgIncludeSubInvoicesMarkup,
		@CustFinalInvPkgIncludeExpReceiptsMarkup = CustFinalInvPkgIncludeExpReceiptsMarkup,
		@CustFinalInvPkgSubInvoicesMinimum = CustFinalInvPkgSubInvoicesMinimum,
		@CustFinalInvPkgExpReceiptsMinimum = CustFinalInvPkgExpReceiptsMinimum,
		@CustFinalInvPkgIncludeInvoiceMarkup = CustFinalInvPkgIncludeInvoiceMarkup,
		@CustFinalInvPkgIncludeDirectExp = CustFinalInvPkgIncludeDirectExp,
		@CustFinalInvPkgIncludeDirectSub = CustFinalInvPkgIncludeDirectSub
	FROM @T

-- Create the XML Results
---------------------------------------
DECLARE @SQTable AS TABLE(
	contentOrder int,
	docType varchar(255),
	documentDetail varchar(255))

SET @sql = '
	SELECT -99 as contentOrder, ''1.0'' as docType, ''_Header_'' as documentDetail --an entry that always returns and provides sp version information 
	
	/* Customized portion of the SQL */
	/* ---------------------------- */
	union all
	select CustFinalInvPkgIncludeExpenseReport as contentOrder, ''Expense Report'' as docType, ''Expense Report'' as DocumentDetail from ProjectCustomTabFields 
		where wbs1 = @WBS1 and wbs2 = '' '' and CustFinalInvPkgIncludeExpenseReport > 0
	union all
	select CustFinalInvPkgIncludeUnits as contentOrder,''Units Report'' as docType, ''Unit'' as DocumentDetail from ProjectCustomTabFields 
		where wbs1 = @WBS1 and wbs2 = '' '' and CustFinalInvPkgIncludeUnits > 0
	/* ---------------------------- */
	'

	+ (CASE WHEN @bCustFinalInvPkgEmailTemplate=1 THEN '
	------- Email Template
	UNION all
	SELECT 0 as contentOrder, ''EmailTemplate'' as docType, CustFinalInvPkgEmailTemplate as documentDetail
		FROM ProjectCustomTabFields WHERE wbs1 = @WBS1 and wbs2 = '' '' and ISNULL(CustFinalInvPkgEmailTemplate,'''') <> '''' '
	ELSE '' END) + '

	------- Invoice Template
	UNION all '
	+ (CASE WHEN @bCustFinalInvPkgIncludeInvoice=1 THEN '
	SELECT CustFinalInvPkgIncludeInvoice as contentOrder, ''Invoice'' as docType, null as DocumentDetail
		FROM ProjectCustomTabFields WHERE wbs1 = @WBS1 and wbs2 = '' '' and CustFinalInvPkgIncludeInvoice > 0 '
	ELSE '
	SELECT 1 as contentOrder, ''Invoice'' as docType, null as DocumentDetail '
	END) +

	+ (CASE WHEN @bCustFinalInvPkgIncludeTimesheetData=1 AND @bCustFinalInvPkgTimesheetTemplate=1 THEN '
	------- Timesheet Template
	UNION all
	SELECT CustFinalInvPkgIncludeTimesheetData as contentOrder, ''Timesheet'' as docType, CustFinalInvPkgTimesheetTemplate as DocumentDetail
		FROM ProjectCustomTabFields WHERE wbs1 = @WBS1 and wbs2 = '' '' and CustFinalInvPkgIncludeTimesheetData > 0 '
	ELSE '' END) +

	+ (CASE WHEN @bCustFinalInvPkgIncludeSubInvoices=1 THEN '
	------- Sub
	UNION all
	SELECT CustFinalInvPkgIncludeSubInvoices as contentOrder, ''Sub'' as docType, null as DocumentDetail
		FROM ProjectCustomTabFields WHERE wbs1 = @WBS1 and wbs2 = '' '' and CustFinalInvPkgIncludeSubInvoices > 0 '
	ELSE '' END) +

	+ (CASE WHEN @bCustFinalInvPkgIncludeExpReceipts=1 THEN '
	------- Exp
	UNION all
	SELECT CustFinalInvPkgIncludeExpReceipts as contentOrder, ''Exp'' as docType, null as DocumentDetail
		FROM ProjectCustomTabFields WHERE wbs1 = @WBS1 and wbs2 = '' '' and CustFinalInvPkgIncludeExpReceipts > 0 '
	ELSE '' END) +

	+ (CASE WHEN @bProjects_FinalInvPkgUserDocuments=1 THEN '
	------- User Documents
	UNION all
	SELECT CustOrder as contentOrder, CustDocType as docType, CustDocument as documentDetail from Projects_FinalInvPkgUserDocuments where wbs1 = @WBS1 and wbs2 = '' '' and CustOrder > 0 '
	ELSE '' END) + '

	UNION all
	SELECT Seq, ''Template'', Name from CCG_EI_Templates where ISNULL(@WBS1,'''') = '''' and Status = ''A'''

SET @sql = REPLACE(@sql, '@WBS1', ''''+ISNULL(@WBS1,'')+'''')
--print @sql
INSERT INTO @SQTable EXECUTE(@sql)

SELECT isnull(c.documentDetail,c.docType) as '@name', c.contentOrder, c.docType, t.Seq as templateID, ISNULL(t.Type,c.docType) as contentClass,
	CASE when c.docType in ('File','FileEdit','FileEdit') then c.documentDetail else t.Source end as templateSourceFile,
	(--parameters subquery for 1 to many relationship
		SELECT
			p.SourceType as '@source', p.[Type] as '@dataType', p.Name as '@name', p.Value as 'value'
		FROM (
			SELECT Seq, TemplateSeq, SourceType, [Type], Name, Value from CCG_EI_TemplateParameters
			UNION all-- include parameters from pic. TemplateSeq = -7 for Invoice
			SELECT -1,-7,'static','bool','markup',@CustFinalInvPkgIncludeInvoiceMarkup
			UNION all-- include parameters from pic. TemplateSeq = -5 for Exp
			SELECT -1,-5,'static','string','detailOrder',@CustFinalInvPkgSubExpOrder
			UNION all
			SELECT -1,-5,'static','bool','markup',@CustFinalInvPkgIncludeExpReceiptsMarkup
			UNION all
			SELECT -1,-5,'static','bool','direct',@CustFinalInvPkgIncludeDirectExp
			UNION all
			SELECT -1,-5,'static','numeric','minimum',convert(varchar(20),@CustFinalInvPkgExpReceiptsMinimum)
			UNION all-- include parameters from pic. TemplateSeq = -3 for Sub
			SELECT -1,-3,'static','string','detailOrder',@CustFinalInvPkgSubExpOrder
			UNION all
			SELECT -1,-3,'static','bool','markup',@CustFinalInvPkgIncludeSubInvoicesMarkup
			UNION all
			SELECT -1,-3,'static','bool','direct',@CustFinalInvPkgIncludeDirectSub
			UNION all
			SELECT -1,-3,'static','numeric','minimum',convert(varchar(20),@CustFinalInvPkgSubInvoicesMinimum)
		) p
		WHERE t.Seq = p.TemplateSeq or (c.docType = 'Exp' and p.TemplateSeq = -5)
			or (c.docType ='Sub' and p.TemplateSeq = -3) or (c.docType ='Invoice' and p.TemplateSeq = -7)
		FOR XML PATH('parameter'),TYPE
	)
	FROM
		@SQTable c
		LEFT JOIN CCG_EI_Templates t on c.documentDetail = t.Name
	ORDER BY 2
	FOR XML path('content'),Root('packagebody'),TYPE
END

GO
