SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Get_VEAddress] (
	@vendor	Nvarchar(20),
	@email	Nvarchar(100)
) AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_Get_VEAddress] '', ''
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	SELECT top 1 Address
        FROM VEAddress
        WHERE Vendor = @vendor and EMail = @email
        ORDER BY primaryind DESC;
END;

GO
