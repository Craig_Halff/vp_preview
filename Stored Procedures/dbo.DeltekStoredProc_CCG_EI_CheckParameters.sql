SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_CheckParameters] ( @objectName nvarchar(100))
             AS EXEC spCCG_EI_CheckParameters @objectName
GO
