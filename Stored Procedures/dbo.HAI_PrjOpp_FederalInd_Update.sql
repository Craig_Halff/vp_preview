SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
	Copyright (c) 2021 Halff Associates, Inc.  All rights reserved.
	5/14/2020	Craig H. Anderson

	Set The Federal Indicator on Project and Opportunities based on the Client Type.

	20210525	Craig H. Anderson
				Added WITH (NOLOCK) hint for read-only tables.
	20210812	Craig H. Anderson
				Removed transaction around both updates
*/
CREATE PROCEDURE [dbo].[HAI_PrjOpp_FederalInd_Update]
AS
    BEGIN
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
      
        UPDATE ox
        SET ox.CustFederalInd = IIF(c.Type = 'FEDGOV', 'Y', 'N')
        FROM dbo.Opportunity o 
           , dbo.OpportunityCustomTabFields ox 
           , dbo.CL c 
        WHERE
            o.OpportunityID = ox.OpportunityID
            AND o.ClientID = c.ClientID
            AND ox.CustFederalInd <> IIF(c.Type = 'FEDGOV', 'Y', 'N');

        UPDATE p
        SET p.FederalInd = IIF(c.Type = 'FEDGOV', 'Y', 'N')
        FROM dbo.PR p
           , dbo.CL c WITH (NOLOCK)
        WHERE
            p.ClientID = c.ClientID
            AND p.WBS2 = ' '
            AND p.FederalInd <> IIF(c.Type = 'FEDGOV', 'Y', 'N');
    END;
GO
