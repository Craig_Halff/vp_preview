SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Clear_Vision_CC]
	@spName			Nvarchar(100),
	@WBS1List		Nvarchar(max),
	@role			Nvarchar(255),
	@username		Nvarchar(32)
AS
BEGIN
	/*
	DeltekStoredProc_CCG_EI_Clear_Vision_CC
		@Username = 'ADMIN',
		@Role = 'ACCT',
		@WBS1List = '1999015.00,2002005.00,2002010.00,2002019.00,2003005.00',
		@spName = 'spCCG_EI_HoldUntilDate_ClearVision'
 	*/
	SET NOCOUNT ON;
	DECLARE @returnVal		int;
	DECLARE @returnMessage	Nvarchar(max);
	DECLARE @safeSql		int;

	-- Ensure that our inputs are safe (prevent SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<CSV>', @WBS1List);
	IF @safeSql < 1 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + cast(@safeSQL As varchar(10));
		RETURN;
	END;

	IF ISNULL(@spName, '') = '' BEGIN
		RETURN;
	END
	ELSE BEGIN
		-- Determine if this SP has a ReturnValue output parameter
		IF EXISTS(
			SELECT 'x'
				FROM [sys].[parameters] p
				JOIN [sys].[objects] o ON o.[object_id] = p.[object_id]
				WHERE o.name = @spName And Lower(p.name) = '@returnvalue'
		) BEGIN
			SELECT 'ERROR: Old version SP with output parameters not supported!';
		END
		ELSE
			EXEC (@spName +
				N' @Username = ''' + @username +
				N''', @Role = ''' + @role +
				N''', @WBS1List = ''' + @WBS1List + N'''');
	END;
END;
GO
