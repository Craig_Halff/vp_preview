SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[HAI_TSR_AgedAccountsReceivableByOffice]
	@CompanyId varchar(2),
	@Office varchar(2),
	@JobDate INT,
	@AgingDate varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- CREATE A UNIQUE ID
DECLARE @SessionId varchar(32) = REPLACE(CAST(NEWID() AS varchar(36)), '-', '')

SET TRANSACTION isolation level READ uncommitted; 

IF EXISTS (SELECT NAME 
           FROM   sysobjects 
           WHERE  NAME = 'setContextInfo' 
                  AND type = 'P') 
  EXECUTE dbo.Setcontextinfo 
    @StrCompany = /*N*/@CompanyId, 
    @StrAuditingEnabled = 'N', 
    @strUserName = /*N*/'HIT', 
    @StrCultureName = 'en-US'; 

DECLARE @sql NVARCHAR(MAX) = N'CREATE TABLE ##arbal_araging__5428899efd6748768b6d77adffaaea0b 
  ( 
     wbs1             NVARCHAR(30) COLLATE database_default, 
     invoice          NVARCHAR(12) COLLATE database_default, 
     wbs1invoicebal   DECIMAL(19, 4), 
     wbs1retainagebal DECIMAL(19, 4) 
     PRIMARY KEY(wbs1, invoice) 
  ) 

INSERT ##arbal_araging__5428899efd6748768b6d77adffaaea0b 
       (wbs1, 
        invoice, 
        wbs1invoicebal, 
        wbs1retainagebal) 
SELECT ledgerar.wbs1, 
       ledgerar.invoice, 
       Sum(CASE 
             WHEN ledgerar.taxcode IS NULL THEN 
               CASE 
                 WHEN ledgerar.transtype = /*N*/''IN'' THEN -amount 
                 ELSE 
                   CASE 
                     WHEN ledgerar.transtype = /*N*/''CR'' 
                          AND ledgerar.subtype = ''T'' THEN -amount 
                     ELSE amount 
                   END 
               END 
             ELSE 0.0 
           END) AS WBS1InvoiceBal, 
       Sum(CASE 
             WHEN ledgerar.taxcode IS NULL THEN 
               CASE 
                 WHEN ledgerar.transtype = /*N*/''IN'' 
                      AND ledgerar.subtype = ''R'' 
                      AND amount < 0.0 THEN -amount 
                 ELSE 0 
               END 
             ELSE 0.0 
           END) AS WBS1RetainageBal 
FROM   ledgerar, 
       ar 
       INNER JOIN pr 
               ON ar.wbs1 = pr.wbs1 
                  AND pr.wbs2 = /*N*/'' '' 
                  AND pr.wbs3 = /*N*/'' '' 
       INNER JOIN pr AS LEVEL2 
               ON ar.wbs1 = LEVEL2.wbs1 
                  AND ar.wbs2 = LEVEL2.wbs2 
                  AND LEVEL2.wbs3 = /*N*/'' '' 
       INNER JOIN pr AS LEVEL3 
               ON ar.wbs1 = LEVEL3.wbs1 
                  AND ar.wbs2 = LEVEL3.wbs2 
                  AND LEVEL3.wbs3 = ar.wbs3 
       LEFT JOIN btbgsubs 
              ON pr.wbs1 = btbgsubs.subwbs1 
       LEFT JOIN cl 
              ON cl.clientid = pr.clientid 
       LEFT JOIN cl AS CLBill 
              ON CLBill.clientid = pr.billingclientid 
       LEFT JOIN contacts AS BLCT 
              ON BLCT.contactid = pr.billingcontactid 
WHERE  ar.wbs1 = ledgerar.wbs1 
       AND ar.wbs2 = ledgerar.wbs2 
       AND ar.wbs3 = ledgerar.wbs3 
       AND ar.invoice = ledgerar.invoice 
       AND ( ( ledgerar.transtype = /*N*/''IN'' 
               AND ( ledgerar.subtype <> ''X'' 
                      OR ledgerar.subtype IS NULL ) ) 
              OR ( ledgerar.transtype = /*N*/''CR'' 
                   AND ledgerar.subtype IN ( ''R'', ''T'' ) ) ) 
       AND ledgerar.autoentry = ''N'' 
       AND amount <> 0 
       AND ledgerar.period <= @JobDate 
       AND (( ( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
              AND ( Substring(pr.org, 4, 2) = /*N*/@Office ) )) 
GROUP  BY ledgerar.wbs1, 
          ledgerar.invoice 

CREATE TABLE ##minar_araging__5428899efd6748768b6d77adffaaea0b 
  ( 
     minwbs1          NVARCHAR(30) COLLATE database_default, 
     mininvoice       NVARCHAR(12) COLLATE database_default, 
     mininvoicedate   DATETIME, 
     minretainagedate DATETIME, 
     minduedate       DATETIME 
     PRIMARY KEY(minwbs1, mininvoice) 
  ) 

INSERT ##minar_araging__5428899efd6748768b6d77adffaaea0b 
       (minwbs1, 
        mininvoice, 
        mininvoicedate, 
        minretainagedate, 
        minduedate) 
SELECT ar.wbs1             AS minWBS1, 
       ar.invoice          AS minInvoice, 
       Min(ar.invoicedate) AS minInvoiceDate, 
       Min(CASE 
             WHEN paidperiod > @JobDate THEN retainagedate 
             ELSE NULL 
           END)            AS minRetainageDate, 
       Min(CASE 
             WHEN paidperiod > @JobDate THEN ar.duedate 
             ELSE NULL 
           END)            AS minDueDate 
FROM   ar, 
       pr 
       LEFT JOIN btbgsubs 
              ON pr.wbs1 = btbgsubs.subwbs1 
       LEFT JOIN cl 
              ON cl.clientid = pr.clientid 
       LEFT JOIN cl AS CLBill 
              ON CLBill.clientid = pr.billingclientid 
       LEFT JOIN contacts AS BLCT 
              ON BLCT.contactid = pr.billingcontactid, 
       pr AS LEVEL2, 
       pr AS LEVEL3 
WHERE  ar.wbs1 = pr.wbs1 
       AND pr.wbs2 = /*N*/'' '' 
       AND pr.wbs3 = /*N*/'' '' 
       AND ar.wbs1 = LEVEL2.wbs1 
       AND ar.wbs2 = LEVEL2.wbs2 
       AND LEVEL2.wbs3 = /*N*/'' '' 
       AND ar.wbs1 = LEVEL3.wbs1 
       AND ar.wbs2 = LEVEL3.wbs2 
       AND ar.wbs3 = LEVEL3.wbs3 
       AND (( ( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
              AND ( Substring(pr.org, 4, 2) = /*N*/@Office ) )) 
GROUP  BY ar.wbs1, 
          ar.invoice 

CREATE TABLE ##joinedar_araging__5428899efd6748768b6d77adffaaea0b 
  ( 
     minwbs1          NVARCHAR(30) COLLATE database_default, 
     mininvoice       NVARCHAR(12) COLLATE database_default, 
     wbs1invoicebal   DECIMAL(19, 4), 
     wbs1retainagebal DECIMAL(19, 4), 
     mininvoicedate   DATETIME, 
     minretainagedate DATETIME, 
     minduedate       DATETIME 
     PRIMARY KEY(minwbs1, mininvoice) 
  ) 

INSERT ##joinedar_araging__5428899efd6748768b6d77adffaaea0b 
       (minwbs1, 
        mininvoice, 
        wbs1invoicebal, 
        wbs1retainagebal, 
        mininvoicedate, 
        minretainagedate, 
        minduedate) 
SELECT B.minwbs1, 
       B.mininvoice, 
       A.wbs1invoicebal, 
       A.wbs1retainagebal, 
       B.mininvoicedate, 
       B.minretainagedate, 
       B.minduedate 
FROM   ##arbal_araging__5428899efd6748768b6d77adffaaea0b A 
       INNER JOIN ##minar_araging__5428899efd6748768b6d77adffaaea0b B 
               ON A.wbs1 = B.minwbs1 
                  AND A.invoice = B.mininvoice 

SELECT ARAgingQuery.group1                                              AS 
       group1, 
       Min(ARAgingQuery.groupdesc1)                                     AS 
       groupDesc1, 
       ARAgingQuery.group2                                              AS 
       group2, 
       Min(ARAgingQuery.groupdesc2)                                     AS 
       groupDesc2, 
       Min(Isnull(projectcustomtabfields.custjtdbilled, 0.00))          AS 
       UDCol_CustJTDBilled, 
       Min(Isnull(projectcustomtabfields.custjtdreceived, 0.00))        AS 
       UDCol_CustJTDReceived, 
       Min(ARAgingQuery.wbs1)                                           AS WBS1, 
       Min(ARAgingQuery.wbs2)                                           AS WBS2, 
       Min(ARAgingQuery.wbs3)                                           AS WBS3, 
       Min(ARAgingQuery.linkwbs1)                                       AS 
       linkWBS1, 
       Min(ARAgingQuery.linkwbs2)                                       AS 
       linkWBS2, 
       Min(ARAgingQuery.linkwbs3)                                       AS 
       linkWBS3, 
       Min(ARAgingQuery.currencycodeproj)                               AS 
       currencyCodeProj, 
       Min(ARAgingQuery.currencycodebill)                               AS 
       currencyCodeBill, 
       Min(ARAgingQuery.currencycodefunct)                              AS 
       currencyCodeFunct, 
       Min(ARAgingQuery.currencycodepres)                               AS 
       currencyCodePres, 
       Count(DISTINCT ARAgingQuery.currencycodeproj)                    AS 
       currencyCodeProjCount, 
       Count(DISTINCT ARAgingQuery.currencycodebill)                    AS 
       currencyCodeBillCount, 
       Count(DISTINCT ARAgingQuery.currencycodefunct)                   AS 
       currencyCodeFunctCount, 
       Count(DISTINCT ARAgingQuery.currencycodeprescount)               AS 
       currencyCodePresCount, 
       Min(ARAgingQuery.decimalplacesproj)                              AS 
       decimalPlacesProj, 
       Min(ARAgingQuery.decimalplacesbill)                              AS 
       decimalPlacesBill, 
       Min(ARAgingQuery.decimalplacesfunct)                             AS 
       decimalPlacesFunct, 
       Min(ARAgingQuery.decimalplacespres)                              AS 
       decimalPlacesPres, 
       Min(ARAgingQuery.currencysymbolproj)                             AS 
       currencySymbolProj, 
       Min(ARAgingQuery.currencysymbolbill)                             AS 
       currencySymbolBill, 
       Min(ARAgingQuery.currencysymbolfunct)                            AS 
       currencySymbolFunct, 
       Min(ARAgingQuery.currencysymbolpres)                             AS 
       currencySymbolPres, 
       Min(ARAgingQuery.sublevel1)                                      AS 
       subLevel1, 
       Min(ARAgingQuery.sublevel2)                                      AS 
       subLevel2, 
       Min(ARAgingQuery.sublevel3)                                      AS 
       subLevel3, 
       Min(ARAgingQuery.invoicedate)                                    AS 
       InvoiceDate, 
       Isnull(Min(ARAgingQuery.duedate), Min(ARAgingQuery.invoicedate)) AS 
       DueDate, 
       Max(ARAgingQuery.daysold)                                        AS 
       DaysOld, 
       Max(ARAgingQuery.invoicenumber)                                  AS 
       InvoiceNumber, 
       Max(ARAgingQuery.invcomment)                                     AS 
       InvComment, 
       Sum(ARAgingQuery.invoicebalance)                                 AS 
       InvoiceBalance, 
       Sum(ARAgingQuery.interest)                                       AS 
       Interest, 
       Sum(ARAgingQuery.retainage)                                      AS 
       Retainage, 
       Sum(ARAgingQuery.retainer)                                       AS 
       Retainer, 
       Min(ARAgingQuery.wbs1name)                                       AS 
       WBS1Name, 
       Min(ARAgingQuery.wbs2name)                                       AS 
       WBS2Name, 
       Min(ARAgingQuery.wbs3name)                                       AS 
       WBS3Name, 
       Min(ARAgingQuery.clientid)                                       AS 
       ClientID, 
       Min(ARAgingQuery.claddress)                                      AS 
       ClAddress, 
       Min(ARAgingQuery.contactid)                                      AS 
       ContactID, 
       Min(ARAgingQuery.billclientid)                                   AS 
       BillClientID, 
       Min(ARAgingQuery.billcladdress)                                  AS 
       BillClAddress, 
       Min(ARAgingQuery.billcontactid)                                  AS 
       BillContactID, 
       Max(ARAgingQuery.billclientid)                                   AS 
       groupingClient, 
       Max(ARAgingQuery.billinggroupid)                                 AS 
       BillingGroupID, 
       Max(ARAgingQuery.billinggroupname)                               AS 
       BillingGroupName, 
       Min(ARAgingQuery.artype)                                         AS 
       ArType, 
       CASE 
         WHEN ( 0 = 0 
                AND Max(ARAgingQuery.daysold) <= 30 ) 
               OR ( Max(ARAgingQuery.daysold) >= 0 
                    AND Max(ARAgingQuery.daysold) <= 30 ) THEN CASE 
         WHEN ''N'' = ''N'' 
              AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
         ELSE Sum(ARAgingQuery.invoicebalance) 
                                                               END - 
         CASE 
           WHEN ''N'' = ''N'' THEN Sum(ARAgingQuery.interest) 
           ELSE 0 
         END 
         ELSE 0.00 
       END                                                              AS 
       Age1Amount, 
       CASE 
         WHEN ( 31 = 0 
                AND Max(ARAgingQuery.daysold) <= 60 ) 
               OR ( Max(ARAgingQuery.daysold) >= 31 
                    AND Max(ARAgingQuery.daysold) <= 60 ) THEN CASE 
         WHEN ''N'' = ''N'' 
              AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
         ELSE Sum(ARAgingQuery.invoicebalance) 
                                                               END - 
         CASE 
           WHEN ''N'' = ''N'' THEN Sum(ARAgingQuery.interest) 
           ELSE 0 
         END 
         ELSE 0.00 
       END                                                              AS 
       Age2Amount, 
       CASE 
         WHEN ( 61 = 0 
                AND Max(ARAgingQuery.daysold) <= 90 ) 
               OR ( Max(ARAgingQuery.daysold) >= 61 
                    AND Max(ARAgingQuery.daysold) <= 90 ) THEN CASE 
         WHEN ''N'' = ''N'' 
              AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
         ELSE Sum(ARAgingQuery.invoicebalance) 
                                                               END - 
         CASE 
           WHEN ''N'' = ''N'' THEN Sum(ARAgingQuery.interest) 
           ELSE 0 
         END 
         ELSE 0.00 
       END                                                              AS 
       Age3Amount, 
       CASE 
         WHEN ( 91 = 0 
                AND Max(ARAgingQuery.daysold) <= 120 ) 
               OR ( Max(ARAgingQuery.daysold) >= 91 
                    AND Max(ARAgingQuery.daysold) <= 120 ) THEN CASE 
         WHEN ''N'' = ''N'' 
              AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
         ELSE Sum(ARAgingQuery.invoicebalance) 
                                                                END - 
         CASE 
           WHEN ''N'' = ''N'' THEN Sum(ARAgingQuery.interest) 
           ELSE 0 
         END 
         ELSE 0.00 
       END                                                              AS 
       Age4Amount, 
       CASE 
         WHEN ( 121 = 0 
                AND Max(ARAgingQuery.daysold) <= 180 ) 
               OR ( Max(ARAgingQuery.daysold) >= 121 
                    AND Max(ARAgingQuery.daysold) <= 180 ) THEN CASE 
         WHEN ''N'' = ''N'' 
              AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
         ELSE Sum(ARAgingQuery.invoicebalance) 
                                                                END - 
         CASE 
           WHEN ''N'' = ''N'' THEN Sum(ARAgingQuery.interest) 
           ELSE 0 
         END 
         ELSE 0.00 
       END                                                              AS 
       Age5Amount, 
       CASE 
         WHEN ( 181 = 0 
                AND Max(ARAgingQuery.daysold) <= 999999 ) 
               OR ( Max(ARAgingQuery.daysold) >= 181 
                    AND Max(ARAgingQuery.daysold) <= 999999 ) THEN CASE 
         WHEN ''N'' = ''N'' 
              AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
         ELSE Sum(ARAgingQuery.invoicebalance) 
                                                                   END - CASE 
         WHEN ''N'' = ''N'' THEN Sum(ARAgingQuery.interest) 
         ELSE 0 
                                                                         END 
         ELSE 0.00 
       END                                                              AS 
       Age6Amount, 
       /***Age7Amount***/0.00                                           AS 
       Age7Amount, 
       /***Age8Amount***/0.00                                           AS 
       Age8Amount, 
       /***Age9Amount***/0.00                                           AS 
       Age9Amount, 
       /***Age10Amount***/0.00                                          AS 
       Age10Amount, 
       CASE 
         WHEN ( 0 = 0 
                AND Max(ARAgingQuery.daysold) <= 30 ) 
               OR ( Max(ARAgingQuery.daysold) >= 0 
                    AND Max(ARAgingQuery.daysold) <= 30 ) THEN Sum( 
         ARAgingQuery.interest) 
         ELSE 0.00 
       END                                                              AS 
       Age1AmountInt, 
       CASE 
         WHEN ( 31 = 0 
                AND Max(ARAgingQuery.daysold) <= 60 ) 
               OR ( Max(ARAgingQuery.daysold) >= 31 
                    AND Max(ARAgingQuery.daysold) <= 60 ) THEN Sum( 
         ARAgingQuery.interest) 
         ELSE 0.00 
       END                                                              AS 
       Age2AmountInt, 
       CASE 
         WHEN ( 61 = 0 
                AND Max(ARAgingQuery.daysold) <= 90 ) 
               OR ( Max(ARAgingQuery.daysold) >= 61 
                    AND Max(ARAgingQuery.daysold) <= 90 ) THEN Sum( 
         ARAgingQuery.interest) 
         ELSE 0.00 
       END                                                              AS 
       Age3AmountInt, 
       CASE 
         WHEN ( 91 = 0 
                AND Max(ARAgingQuery.daysold) <= 120 ) 
               OR ( Max(ARAgingQuery.daysold) >= 91 
                    AND Max(ARAgingQuery.daysold) <= 120 ) THEN Sum( 
         ARAgingQuery.interest) 
         ELSE 0.00 
       END                                                              AS 
       Age4AmountInt, 
       CASE 
         WHEN ( 121 = 0 
                AND Max(ARAgingQuery.daysold) <= 180 ) 
               OR ( Max(ARAgingQuery.daysold) >= 121 
                    AND Max(ARAgingQuery.daysold) <= 180 ) THEN Sum( 
         ARAgingQuery.interest) 
         ELSE 0.00 
       END                                                              AS 
       Age5AmountInt, 
       CASE 
         WHEN ( 181 = 0 
                AND Max(ARAgingQuery.daysold) <= 999999 ) 
               OR ( Max(ARAgingQuery.daysold) >= 181 
                    AND Max(ARAgingQuery.daysold) <= 999999 ) THEN Sum( 
         ARAgingQuery.interest) 
         ELSE 0.00 
       END                                                              AS 
       Age6AmountInt, 
       /***Age7AmountInt***/0.00                                        AS 
       Age7AmountInt, 
       /***Age8AmountInt***/0.00                                        AS 
       Age8AmountInt, 
       /***Age9AmountInt***/0.00                                        AS 
       Age9AmountInt, 
       /***Age10AmountInt***/0.00                                       AS 
       Age10AmountInt, 
       CASE 
         WHEN ( 0 = 0 
                AND Max(ARAgingQuery.daysold) <= 30 ) 
               OR ( Max(ARAgingQuery.daysold) >= 0 
                    AND Max(ARAgingQuery.daysold) <= 30 ) THEN 
           CASE 
             WHEN ''N'' = ''N'' 
                  AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
             ELSE Sum(ARAgingQuery.invoicebalance) 
           END 
         ELSE 0.00 
       END                                                              AS 
       Age1AmountTotal1, 
       CASE 
         WHEN ( 31 = 0 
                AND Max(ARAgingQuery.daysold) <= 60 ) 
               OR ( Max(ARAgingQuery.daysold) >= 31 
                    AND Max(ARAgingQuery.daysold) <= 60 ) THEN 
           CASE 
             WHEN ''N'' = ''N'' 
                  AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
             ELSE Sum(ARAgingQuery.invoicebalance) 
           END 
         ELSE 0.00 
       END                                                              AS 
       Age2AmountTotal1, 
       CASE 
         WHEN ( 61 = 0 
                AND Max(ARAgingQuery.daysold) <= 90 ) 
               OR ( Max(ARAgingQuery.daysold) >= 61 
                    AND Max(ARAgingQuery.daysold) <= 90 ) THEN 
           CASE 
             WHEN ''N'' = ''N'' 
                  AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
             ELSE Sum(ARAgingQuery.invoicebalance) 
           END 
         ELSE 0.00 
       END                                                              AS 
       Age3AmountTotal1, 
       CASE 
         WHEN ( 91 = 0 
                AND Max(ARAgingQuery.daysold) <= 120 ) 
               OR ( Max(ARAgingQuery.daysold) >= 91 
                    AND Max(ARAgingQuery.daysold) <= 120 ) THEN 
           CASE 
             WHEN ''N'' = ''N'' 
                  AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
             ELSE Sum(ARAgingQuery.invoicebalance) 
           END 
         ELSE 0.00 
       END                                                              AS 
       Age4AmountTotal1, 
       CASE 
         WHEN ( 121 = 0 
                AND Max(ARAgingQuery.daysold) <= 180 ) 
               OR ( Max(ARAgingQuery.daysold) >= 121 
                    AND Max(ARAgingQuery.daysold) <= 180 ) THEN 
           CASE 
             WHEN ''N'' = ''N'' 
                  AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
             ELSE Sum(ARAgingQuery.invoicebalance) 
           END 
         ELSE 0.00 
       END                                                              AS 
       Age5AmountTotal1, 
       CASE 
         WHEN ( 181 = 0 
                AND Max(ARAgingQuery.daysold) <= 999999 ) 
               OR ( Max(ARAgingQuery.daysold) >= 181 
                    AND Max(ARAgingQuery.daysold) <= 999999 ) THEN 
           CASE 
             WHEN ''N'' = ''N'' 
                  AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
             ELSE Sum(ARAgingQuery.invoicebalance) 
           END 
         ELSE 0.00 
       END                                                              AS 
       Age6AmountTotal1, 
       /***Age7AmountTotal1***/0.00                                     AS 
       Age7AmountTotal1, 
       /***Age8AmountTotal1***/0.00                                     AS 
       Age8AmountTotal1, 
       /***Age9AmountTotal1***/0.00                                     AS 
       Age9AmountTotal1, 
       /***Age10AmountTotal1***/0.00                                    AS 
       Age10AmountTotal1, 
       CASE 
         WHEN ( 0 = 0 
                AND Max(ARAgingQuery.daysold) <= 30 ) 
               OR ( Max(ARAgingQuery.daysold) >= 0 
                    AND Max(ARAgingQuery.daysold) <= 30 ) THEN CASE 
         WHEN ''N'' = ''N'' 
              AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
         ELSE Sum(ARAgingQuery.invoicebalance) 
                                                               END - Sum( 
         ARAgingQuery.interest) 
         ELSE 0.00 
       END                                                              AS 
       Age1AmountTotal2, 
       CASE 
         WHEN ( 31 = 0 
                AND Max(ARAgingQuery.daysold) <= 60 ) 
               OR ( Max(ARAgingQuery.daysold) >= 31 
                    AND Max(ARAgingQuery.daysold) <= 60 ) THEN CASE 
         WHEN ''N'' = ''N'' 
              AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
         ELSE Sum(ARAgingQuery.invoicebalance) 
                                                               END - Sum( 
         ARAgingQuery.interest) 
         ELSE 0.00 
       END                                                              AS 
       Age2AmountTotal2, 
       CASE 
         WHEN ( 61 = 0 
                AND Max(ARAgingQuery.daysold) <= 90 ) 
               OR ( Max(ARAgingQuery.daysold) >= 61 
                    AND Max(ARAgingQuery.daysold) <= 90 ) THEN CASE 
         WHEN ''N'' = ''N'' 
              AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
         ELSE Sum(ARAgingQuery.invoicebalance) 
                                                               END - Sum( 
         ARAgingQuery.interest) 
         ELSE 0.00 
       END                                                              AS 
       Age3AmountTotal2, 
       CASE 
         WHEN ( 91 = 0 
                AND Max(ARAgingQuery.daysold) <= 120 ) 
               OR ( Max(ARAgingQuery.daysold) >= 91 
                    AND Max(ARAgingQuery.daysold) <= 120 ) THEN CASE 
         WHEN ''N'' = ''N'' 
              AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
         ELSE Sum(ARAgingQuery.invoicebalance) 
                                                                END - Sum( 
         ARAgingQuery.interest) 
         ELSE 0.00 
       END                                                              AS 
       Age4AmountTotal2, 
       CASE 
         WHEN ( 121 = 0 
                AND Max(ARAgingQuery.daysold) <= 180 ) 
               OR ( Max(ARAgingQuery.daysold) >= 121 
                    AND Max(ARAgingQuery.daysold) <= 180 ) THEN CASE 
         WHEN ''N'' = ''N'' 
              AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
         ELSE Sum(ARAgingQuery.invoicebalance) 
                                                                END - Sum( 
         ARAgingQuery.interest) 
         ELSE 0.00 
       END                                                              AS 
       Age5AmountTotal2, 
       CASE 
         WHEN ( 181 = 0 
                AND Max(ARAgingQuery.daysold) <= 999999 ) 
               OR ( Max(ARAgingQuery.daysold) >= 181 
                    AND Max(ARAgingQuery.daysold) <= 999999 ) THEN 
         CASE 
         WHEN ''N'' = ''N'' 
              AND Min(ARAgingQuery.artype) = ''R'' THEN 0 
         ELSE Sum(ARAgingQuery.invoicebalance) 
                                                                   END - Sum( 
         ARAgingQuery.interest) 
         ELSE 0.00 
       END                                                              AS 
       Age6AmountTotal2, 
       /***Age7AmountTotal2***/0.00                                     AS 
       Age7AmountTotal2, 
       /***Age8AmountTotal2***/0.00                                     AS 
       Age8AmountTotal2, 
       /***Age9AmountTotal2***/0.00                                     AS 
       Age9AmountTotal2, 
       /***Age10AmountTotal2***/0.00                                    AS 
       Age10AmountTotal2, 
       CASE 
         WHEN Min(ARAgingQuery.artype) = ''R'' THEN Sum( 
         ARAgingQuery.invoicebalance) 
         ELSE 0.00 
       END                                                              AS 
       UReceipts, 
       Min(ARAgingQuery.invoicedate)                                    AS 
       LastRecDate, 
       ''''                                                               AS 
       LastRecInv, 
       0                                                                AS 
       LastRecAmt, 
       Min(ARAgingQuery.preinvoice)                                     AS 
       PreInvoice 
FROM   (SELECT Isnull(Substring(pr.org, 7, 3), '''')     AS group1, 
               Min(groupCFGOrgCodes3.label)            AS groupDesc1, 
               Isnull(pr.wbs1, '''')                     AS group2, 
               Min(pr.NAME)        AS groupDesc2, 
ledgerar.wbs1                           AS WBS1, 
Min(ledgerar.wbs2)                      AS WBS2, 
Min(ledgerar.wbs3)                      AS WBS3, 
''''                                      AS linkWBS1, 
''''                                      AS linkWBS2, 
''''                                      AS linkWBS3, 
Min(pr.sublevel)                        AS SubLevel1, 
Min(LEVEL2.sublevel)                    AS SubLevel2, 
Min(LEVEL3.sublevel)                    AS SubLevel3, 
''''                                      AS currencyCodeProj, 
''''                                      AS currencyCodeBill, 
''''                                      AS currencyCodeFunct, 
''''                                      AS currencyCodePres, 
0                                       AS currencyCodeProjCount, 
0                                       AS currencyCodeBillCount, 
0                                       AS currencyCodeFunctCount 
, 
0                                       AS 
currencyCodePresCount, 
0                                       AS decimalPlacesProj, 
0                                       AS decimalPlacesBill, 
0                                       AS decimalPlacesFunct, 
0                                       AS decimalPlacesPres, 
''''                                      AS currencySymbolProj, 
''''                                      AS currencySymbolBill, 
''''                                      AS currencySymbolFunct, 
''''                                      AS currencySymbolPres, 
Min(CASE 
  WHEN wbs1retainagebal <> 0.0 
       AND wbs1invoicebal <= wbs1retainagebal THEN 
  Isnull(MinAR.minretainagedate, MinAR.mininvoicedate) 
  ELSE MinAR.mininvoicedate 
END)                                AS InvoiceDate, 
Min(MinAR.minduedate)                   AS DueDate, 
CASE 
WHEN Min(CASE 
         WHEN wbs1retainagebal <> 0.0 
              AND wbs1invoicebal <= wbs1retainagebal THEN 
         Isnull(MinAR.minretainagedate, MinAR.mininvoicedate) 
         ELSE MinAR.mininvoicedate 
       END) IS NULL THEN 0 
ELSE Datediff(day, Min(CASE 
                       WHEN wbs1retainagebal <> 0.0 
                            AND wbs1invoicebal <= 
                                wbs1retainagebal 
                     THEN 
Isnull(MinAR.minretainagedate, MinAR.mininvoicedate) 
ELSE MinAR.mininvoicedate 
END), @AgingDate) 
END                                     AS DaysOld, 
ar.invoice                              AS InvoiceNumber, 
''''                                      AS InvComment, 
Sum(CASE 
WHEN ledgerar.transtype = /*N*/''IN'' THEN -amount 
ELSE 
CASE 
WHEN ledgerar.transtype = /*N*/''CR'' 
AND ledgerar.subtype = ''T'' THEN -amount 
ELSE amount 
END 
END)                                AS InvoiceBalance, 
Sum(CASE 
WHEN ledgerar.transtype = /*N*/''IN'' 
AND ledgerar.subtype = ''I'' THEN -amount 
ELSE 0 
END)                                AS Interest, 
Sum(CASE 
WHEN ledgerar.transtype = /*N*/''IN'' 
AND ledgerar.subtype = ''R'' THEN amount 
ELSE 0 
END)                                AS Retainage, 
0                                       AS Retainer, 
Max(pr.NAME)                            AS WBS1Name, 
Max(LEVEL2.NAME)                        AS WBS2Name, 
Max(LEVEL3.NAME)                        AS WBS3Name, 
Max(pr.clientid)                        AS ClientID, 
Max(pr.claddress)                       AS ClAddress, 
Max(pr.contactid)                       AS ContactID, 
Max(pr.billingclientid)                 AS BillClientID, 
Max(pr.clbillingaddr)                   AS BillClAddress, 
Max(pr.billingcontactid)                AS BillContactID, 
Max(Isnull(btbgsubs.mainwbs1, pr.wbs1)) AS BillingGroupID, 
Max(Isnull(MAINPR.NAME, pr.NAME))       AS BillingGroupName, 
Min(CASE 
WHEN ( gainsandlossestype IS NULL 
AND ledgerar.transtype = /*N*/''CR'' 
AND ledgerar.subtype = ''R'' ) THEN ''R'' 
WHEN ( gainsandlossestype IS NOT NULL 
AND ledgerar.transtype = N''CR'' 
AND ledgerar.subtype = ''R'' ) THEN ''Z'' 
WHEN ( ledgerar.transtype = /*N*/''CR'' 
AND ledgerar.subtype = ''T'' ) THEN ''T'' 
WHEN ( ledgerar.transtype = /*N*/''IN'' 
AND Isnull(ledgerar.subtype, '' '') = '' '' ) THEN ''I'' 
ELSE '' '' 
END)                                AS ArType, 
Max(CASE 
WHEN ledgerar.transtype = /*N*/''IN'' 
AND ledgerar.subtype IS NULL THEN ''Y'' 
ELSE ''N'' 
END)                                AS invoiceExists, 
''N''                                     AS PreInvoice, 
NULL                                    AS LastRecDate, 
''''                                      AS LastRecInv, 
0                                       AS LastRecAmt 
FROM   ar 
INNER JOIN ledgerar 
    ON ar.wbs1 = ledgerar.wbs1 
       AND ar.wbs2 = ledgerar.wbs2 
       AND ar.wbs3 = ledgerar.wbs3 
       AND ar.invoice = ledgerar.invoice 
INNER JOIN pr 
    ON ar.wbs1 = pr.wbs1 
       AND pr.wbs2 = /*N*/'' '' 
       AND pr.wbs3 = /*N*/'' '' 
LEFT JOIN cfgorgcodes AS groupCFGOrgCodes3 
   ON Substring(pr.org, 7, 3) = groupCFGOrgCodes3.code 
      AND groupCFGOrgCodes3.orglevel = 3 
INNER JOIN pr AS LEVEL2 
    ON ar.wbs1 = LEVEL2.wbs1 
       AND ar.wbs2 = LEVEL2.wbs2 
       AND LEVEL2.wbs3 = /*N*/'' '' 
/***groupL2Join***/ 
INNER JOIN pr AS LEVEL3 
    ON ar.wbs1 = LEVEL3.wbs1 
       AND ar.wbs2 = LEVEL3.wbs2 
       AND ar.wbs3 = LEVEL3.wbs3 
LEFT JOIN projectcustomtabfields 
   ON projectcustomtabfields.wbs1 = LEVEL3.wbs1 
      AND projectcustomtabfields.wbs2 = '' '' 
      AND projectcustomtabfields.wbs3 = '' '' 
/***groupL3Join***/ 
LEFT JOIN btbgsubs 
   ON ar.wbs1 = btbgsubs.subwbs1 
/***subledgersJoinCompany***/ 
LEFT JOIN pr AS MAINPR 
   ON btbgsubs.mainwbs1 = MAINPR.wbs1 
      AND MAINPR.wbs2 = /*N*/'' '' 
      AND MAINPR.wbs3 = /*N*/'' '' 
/***subledgersJoinWBS***/ 
LEFT JOIN cl 
   ON cl.clientid = pr.clientid 
LEFT JOIN cl AS CLBill 
   ON CLBill.clientid = pr.billingclientid 
LEFT JOIN contacts AS BLCT 
   ON BLCT.contactid = pr.billingcontactid 
LEFT JOIN em AS PRPRINCIPAL 
   ON pr.principal = PRPRINCIPAL.employee 
LEFT JOIN em AS PRProjMgr 
   ON pr.projmgr = PRProjMgr.employee 
/***ReceiptJoin***/ 
, 
##joinedar_araging__5428899efd6748768b6d77adffaaea0b MinAR 
/***addARBalanceTable***/ 
WHERE  ar.wbs1 = MinAR.minwbs1 
AND ar.invoice = MinAR.mininvoice 
/***whereclauseARBalanceTable***/ 
AND ( ledgerar.transtype = /*N*/''IN'' 
  AND ( ledgerar.subtype <> ''X'' 
         OR ledgerar.subtype IS NULL ) 
   OR ( ledgerar.transtype = /*N*/''CR'' 
        AND ledgerar.subtype IN ( ''R'', ''T'' ) ) ) 
AND ledgerar.autoentry = ''N'' 
AND ledgerar.period <= @JobDate 
AND amount <> 0 
AND ledgerar.period >= 0 
AND ledgerar.period <= @JobDate 
AND (( ( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
   AND ( Substring(pr.org, 4, 2) = /*N*/@Office ) )) 
AND ar.linkcompany IS NULL 
GROUP  BY Isnull(Substring(pr.org, 7, 3), ''''), 
Isnull(pr.wbs1, ''''), 
ledgerar.wbs1, 
/***WBS2Group***/ 
/***WBS3Group***/ 
ar.invoice, 
PRPrincipal.lastname, 
PRProjMgr.lastname 
HAVING ( Sum(CASE 
    WHEN ledgerar.transtype = /*N*/''IN'' THEN -amount 
    ELSE 
      CASE 
        WHEN ledgerar.transtype = /*N*/''CR'' 
             AND ledgerar.subtype = ''T'' THEN -amount 
        ELSE amount 
      END 
  END) <> 0 
OR Sum(CASE 
        WHEN ledgerar.transtype = /*N*/''IN'' 
             AND ledgerar.subtype = ''R'' THEN amount 
        ELSE 0 
      END) <> 0 ) 
UNION ALL 
SELECT Isnull(Substring(pr.org, 7, 3), '''')     AS group1, 
Min(groupCFGOrgCodes3.label)            AS groupDesc1, 
Isnull(pr.wbs1, '''')                     AS group2, 
Min(pr.NAME + ( CASE WHEN PRPrincipal.lastname IS NULL THEN '''' 
            ELSE 
                '' / Team Leader: '' 
END + 
                          Isnull(PRPrincipal.lastname, '''') ) + ( 
CASE WHEN PRProjMgr.lastname IS NULL THEN '''' ELSE 
'' / Project Manager: '' 
END + 
Isnull( 
  PRProjMgr.lastname, '''') ))        AS groupDesc2, 
ledgerar.wbs1                           AS WBS1, 
Min(ledgerar.wbs2)                      AS WBS2, 
Min(ledgerar.wbs3)                      AS WBS3, 
''''                                      AS linkWBS1, 
''''                                      AS linkWBS2, 
''''                                      AS linkWBS3, 
Min(pr.sublevel)                        AS SubLevel1, 
Min(LEVEL2.sublevel)                    AS SubLevel2, 
Min(LEVEL3.sublevel)                    AS SubLevel3, 
''''                                      AS currencyCodeProj, 
''''                                      AS currencyCodeBill, 
''''                                      AS currencyCodeFunct, 
''''                                      AS currencyCodePres, 
0                                       AS currencyCodeProjCount, 
0                                       AS currencyCodeBillCount, 
0                                       AS currencyCodeFunctCount 
, 
0                                       AS 
currencyCodePresCount, 
0                                       AS decimalPlacesProj, 
0                                       AS decimalPlacesBill, 
0                                       AS decimalPlacesFunct, 
0                                       AS decimalPlacesPres, 
''''                                      AS currencySymbolProj, 
''''                                      AS currencySymbolBill, 
''''                                      AS currencySymbolFunct, 
''''                                      AS currencySymbolPres, 
NULL                                    AS InvoiceDate, 
NULL                                    AS DueDate, 
0                                       AS DaysOld, 
NULL                                    AS InvoiceNumber, 
NULL                                    AS InvComment, 
0                                       AS InvoiceBalance, 
0                                       AS Interest, 
0                                       AS Retainage, 
Sum(-amount)                            AS Retainer, 
Max(pr.NAME)                            AS WBS1Name, 
Max(LEVEL2.NAME)                        AS WBS2Name, 
Max(LEVEL3.NAME)                        AS WBS3Name, 
Max(pr.clientid)                        AS ClientID, 
Max(pr.claddress)                       AS ClAddress, 
Max(pr.contactid)                       AS ContactID, 
Max(pr.billingclientid)                 AS BillClientID, 
Max(pr.clbillingaddr)                   AS BillClAddress, 
Max(pr.billingcontactid)                AS BillContactID, 
Max(Isnull(btbgsubs.mainwbs1, pr.wbs1)) AS BillingGroupID, 
Max(Isnull(MAINPR.NAME, pr.NAME))       AS BillingGroupName, 
''T''                                     AS ArType, 
''N''                                     AS invoiceExists, 
''N''                                     AS PreInvoice, 
NULL                                    AS LastRecDate, 
''''                                      AS LastRecInv, 
0                                       AS LastRecAmt 
FROM   (SELECT NULL AS InvoiceNumber, 
    NULL AS minInvoice, 
    NULL AS minInvoiceDate) AS MinAR, 
ledgerar 
LEFT JOIN btbgsubs 
   ON ledgerar.wbs1 = btbgsubs.subwbs1 
INNER JOIN pr 
    ON ledgerar.wbs1 = pr.wbs1 
       AND pr.wbs2 = /*N*/'' '' 
       AND pr.wbs3 = /*N*/'' '' 
INNER JOIN pr AS LEVEL2 
    ON ledgerar.wbs1 = LEVEL2.wbs1 
       AND ledgerar.wbs2 = LEVEL2.wbs2 
       AND LEVEL2.wbs3 = /*N*/'' '' 
INNER JOIN pr AS LEVEL3 
    ON ledgerar.wbs1 = LEVEL3.wbs1 
       AND ledgerar.wbs2 = LEVEL3.wbs2 
       AND ledgerar.wbs3 = LEVEL3.wbs3 
LEFT JOIN ar 
   ON ar.wbs1 = ledgerar.wbs1 
      AND ar.wbs2 = ledgerar.wbs2 
      AND ar.wbs3 = ledgerar.wbs3 
      AND ar.invoice = ledgerar.invoice 
LEFT JOIN pr AS MAINPR 
   ON btbgsubs.mainwbs1 = MAINPR.wbs1 
      AND MAINPR.wbs2 = /*N*/'' '' 
      AND MAINPR.wbs3 = /*N*/'' '' 
LEFT JOIN cfgorgcodes AS groupCFGOrgCodes3 
   ON Substring(pr.org, 7, 3) = groupCFGOrgCodes3.code 
      AND groupCFGOrgCodes3.orglevel = 3 
LEFT JOIN cl 
   ON cl.clientid = pr.clientid 
LEFT JOIN cl AS CLBill 
   ON CLBill.clientid = pr.billingclientid 
LEFT JOIN contacts AS BLCT 
   ON BLCT.contactid = pr.billingcontactid 
LEFT JOIN em AS PRPRINCIPAL 
   ON pr.principal = PRPRINCIPAL.employee 
LEFT JOIN em AS PRProjMgr 
   ON pr.projmgr = PRProjMgr.employee 
/***groupL2Join***/ 
LEFT JOIN projectcustomtabfields 
   ON projectcustomtabfields.wbs1 = LEVEL3.wbs1 
      AND projectcustomtabfields.wbs2 = LEVEL3.wbs2 
      AND projectcustomtabfields.wbs3 = LEVEL3.wbs3 
/***groupL3Join***/ 
WHERE  ledgerar.transtype = /*N*/''CR'' 
AND ledgerar.subtype = ''T'' 
AND ledgerar.autoentry = ''N'' 
AND ledgerar.period <= @JobDate 
AND ledgerar.period >= 0 
AND ledgerar.period <= @JobDate 
AND (( ( Substring(pr.org, 1, 2) = /*N*/@CompanyId ) 
   AND ( Substring(pr.org, 4, 2) = /*N*/@Office ) )) 
AND ar.linkcompany IS NULL 
GROUP  BY Isnull(Substring(pr.org, 7, 3), ''''), 
Isnull(pr.wbs1, ''''), 
ledgerar.wbs1 
/***WBS23Group***/ 
HAVING Sum(amount) <> 0 
/***PreInvoiceUnion***/ 
/***PreInvBeg*** ISNULL(substring(PR.Org, 7, 3), '''') As group1, Min(groupCFGOrgCodes3.Label) As groupDesc1, ISNULL(PR.WBS1, '''') As group2, Min(PR.Name + (case when PRPrincipal.LastName is null then '''' else '' / Team Leader: '' end+ISNULL(PRPrincipal.LastName,''''))+(case when PRProjMgr.LastName is null then '''' else '' / Project Manager: '' end+ISNULL(PRProjMgr.LastName,''''))) As groupDesc2,   ***PreInvEnd***/
/***PreInvBeg***   ***PreInvEnd***/ 
/***PrevUnion2***/ 
/***PreInvBeg***   LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3  ***PreInvEnd***/
/***PreInvBeg***     ***PreInvEnd***/ 
/***PreInvBeg*** /***groupL2Join***/   ***PreInvEnd***/ 
/***PreInvBeg***    ***PreInvEnd***/ 
/***PreInvBeg***   /***groupL3Join***/  ***PreInvEnd***/ 
/***PrevUnion3***/ 
/***PreInvBeg***     ***PreInvEnd***/ 
/***PrevUnion5***/ 
/***PreInvBeg*** /***preInvoiceDate***/ ***PreInvEnd***/ 
/***PreInvBeg***     ***PreInvEnd***/ 
/***PreInvBeg***      ***PreInvEnd***/ 
/***PreInvBeg***    AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND (SUBSTRING(PR.Org, 4, 2) = /*N*/@Office))) ***PreInvEnd***/
/***PreInvBeg***     ***PreInvEnd***/ 
/***PrevUnion6***/ 
/***PreInvBeg***  ISNULL(substring(PR.Org, 7, 3), ''''), ISNULL(PR.WBS1, ''''),   ***PreInvEnd***/ 
/***PrevUnion7***/ 
/***PreInvBeg***   /***AdditionalWBS23Group***/  ***PreInvEnd***/ 
/***PrevUnion8***/ 
) AS ARAgingQuery 
LEFT JOIN pr 
ON pr.wbs1 = ARAgingQuery.wbs1 
AND pr.wbs2 = '' '' 
AND pr.wbs3 = '' '' 
LEFT JOIN pr AS LEVEL2 
ON LEVEL2.wbs1 = ARAgingQuery.wbs1 
AND LEVEL2.wbs2 = ARAgingQuery.wbs2 
AND LEVEL2.wbs3 = '' '' 
LEFT JOIN pr AS LEVEL3 
ON LEVEL3.wbs1 = ARAgingQuery.wbs1 
AND LEVEL3.wbs2 = ARAgingQuery.wbs2 
AND LEVEL3.wbs3 = ARAgingQuery.wbs3 
LEFT JOIN projectcustomtabfields 
ON projectcustomtabfields.wbs1 = ARAgingQuery.wbs1 
AND projectcustomtabfields.wbs2 = '' '' 
AND projectcustomtabfields.wbs3 = '' '' 
GROUP  BY ARAgingQuery.group1, 
ARAgingQuery.group2, 
ARAgingQuery.invoicenumber, 
ARAgingQuery.invoicedate, 
ARAgingQuery.duedate 
/***BalanceHaving***/ 
ORDER  BY group1, 
group2, 
ARAgingQuery.invoicenumber, 
ARAgingQuery.invoicedate, 
ARAgingQuery.duedate OPTION (RECOMPILE)

DROP TABLE ##arbal_araging__5428899efd6748768b6d77adffaaea0b
DROP TABLE ##minar_araging__5428899efd6748768b6d77adffaaea0b
DROP TABLE ##joinedar_araging__5428899efd6748768b6d77adffaaea0b'

DECLARE @ParmDefinition nvarchar(max) = N'@CompanyId NVARCHAR(2), @Office NVARCHAR(10), @JobDate NVARCHAR(10), @AgingDate NVARCHAR(10)'; 
EXECUTE sp_executesql @sql, @ParmDefinition, @CompanyId, @Office, @JobDate, @AgingDate

END
GO
