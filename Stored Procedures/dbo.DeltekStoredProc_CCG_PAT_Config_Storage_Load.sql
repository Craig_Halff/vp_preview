SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_Storage_Load]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Seq, Description, UseType, StorageType, Server, Catalog, Root, UserName, Password,
			RevisionType, MarkupType, Status, DateChanged, ChangedBy, Detail, AllowDelete
        FROM CCG_PAT_ConfigStorage
END;
GO
