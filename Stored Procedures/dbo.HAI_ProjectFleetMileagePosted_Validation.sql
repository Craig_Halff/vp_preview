SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--SET QUOTED_IDENTIFIER ON|OFF
--SET ANSI_NULLS ON|OFF
--GO
CREATE PROCEDURE [dbo].[HAI_ProjectFleetMileagePosted_Validation]
    @wbs1 AS varchar(30), @wbs2 AS varchar(7), @wbs3 AS varchar(7)
-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| 'user_name'
AS
BEGIN
    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    DECLARE @errorMessage varchar(255) = '', @myCount int = 0;

    SET @myCount = (
        SELECT  COUNT(*)
        FROM    dbo.UDIC_FleetManagement_MileageLog AS fmml
        INNER JOIN dbo.UDIC_ProjectLowestLevel      AS pll
            ON fmml.CustProject = pll.UDIC_UID
        WHERE   pll.CustWBS1 = @wbs1
                AND pll.CustWBS2 = @wbs2
                AND pll.CustWBS3 = @wbs3
                AND fmml.CustPosted = 'N'
    );
	 
    IF @myCount > 0
    BEGIN
        SET @errorMessage = CONCAT('Cannot close ',@wbs1,'-', @wbs2, '-', @wbs3, ' because there are ', @myCount, ' unposted mileage transactions.                                           ');
        RAISERROR(@errorMessage, 16, 1);
    END;
END;
GO
