SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_Vision_AddUDF](
	@Label			varchar(255),
	@FieldName		varchar(255),--without the 'Cust' prefix
	@FieldType		varchar(255),--vision datatype values = string,date,dropdown,checkbox,numeric,employee
	@ColDecimals	int = 0,
	@ColWidth		int = NULL,--use to override only, otherwise set based on FieldType
	@ValueList		varchar(255)='Default',
	@TabID			varchar(255)='accounting',
	@InfoCenterArea	varchar(255)='Projects',--currently only Projects is supported
	@Culture		varchar(255)='en-US',
	@GridID			varchar(255)='X',--other values needed for grids or pre Vision 7
	@SQLType		varchar(255)=NULL--use to override only, otherwise set based on FieldType
	)
	--@ColStatus		char(1)='Y',
	--@ColRequiredExpression	varchar(255)='')
AS BEGIN
/*
	Copyright (c) 2011 Central Consulting Group.  All rights reserved.
	
	Examples:
	exec spCCG_Vision_AddUDF 'Test Alpha', 'EITestAlpha', 'string'
	exec spCCG_Vision_AddUDF 'Test Check', 'EITestCheck', 'checkbox'
	exec spCCG_Vision_AddUDF 'Test Decimal', 'EITestDecimal', 'numeric',0,5
	
	exec spCCG_Vision_AddUDF 'Final Inv Pkg - Email Template', 'FinalInvPkgEmailTemplate', 'string'
	exec spCCG_Vision_AddUDF 'Final Inv Pkg - Include Invoice', 'FinalInvPkgIncludeInvoice', 'numeric',0,3
	exec spCCG_Vision_AddUDF 'Final Inv Pkg - Include Invoice Markup', 'FinalInvPkgIncludeInvoiceMarkup', 'checkbox'
	exec spCCG_Vision_AddUDF 'Final Inv Pkg - Include Timesheet Data', 'FinalInvPkgIncludeTimesheetData', 'numeric',0,3
	exec spCCG_Vision_AddUDF 'Final Inv Pkg - Timesheet Template', 'FinalInvPkgTimesheetTemplate', 'string'
	exec spCCG_Vision_AddUDF 'Final Inv Pkg - Include Sub Invoices', 'FinalInvPkgIncludeSubInvoices', 'numeric',0,3
	exec spCCG_Vision_AddUDF 'Final Inv Pkg - Sub Invoices Minimum', 'FinalInvPkgSubInvoicesMinimum', 'numeric',0,6
	exec spCCG_Vision_AddUDF 'Final Inv Pkg - Include Sub Invoices Markup', 'FinalInvPkgIncludeSubInvoicesMarkup', 'checkbox'
	exec spCCG_Vision_AddUDF 'Final Inv Pkg - Include Exp Receipts', 'FinalInvPkgIncludeExpReceipts', 'numeric',0,3
	exec spCCG_Vision_AddUDF 'Final Inv Pkg - Exp Receipts Minimum', 'FinalInvPkgExpReceiptsMinimum', 'numeric',0,6
	exec spCCG_Vision_AddUDF 'Final Inv Pkg - Include Exp Receipts Markup', 'FinalInvPkgIncludeExpReceiptsMarkup','checkbox'
	exec spCCG_Vision_AddUDF 'Final Inv Pkg - Sub/Exp Order', 'FinalInvPkgSubExpOrder', 'dropdown',0,NULL,'Default'
*/
	set nocount on
	declare @sql	varchar(max)
	declare @debug	char(1)
	set @debug='Y'
	
	print @Label
	set @Label = REPLACE(@Label,'''','''''')
	
	if @ColWidth is null
	begin
		set @ColWidth = 
			case 
				when @FieldType = 'numeric' then 20
				when @FieldType = 'checkbox' then 1
				else 20
			end
	end
	
	set @TabID = CASE WHEN @TabID IS NULL then 'NULL' else '''' + @TabID + '''' end
	
	IF EXISTS (SELECT * FROM CFGSystem where Version like '7%')
	BEGIN
--select * from FW_CustomCOlumnsData where infocenterarea = 'Projects'
--select * from FW_CustomCOlumnCaptions where infocenterarea = 'Projects'
--select * from FW_CustomColumnValuesData where infocenterarea = 'Projects'
--delete from FW_CustomColumnValuesData where gridid = '[FORM]'
	/* VISION VERSION 7 */
	/* custInvoiceGroup */
		set @sql='if not exists (select ''x'' from FW_CustomColumnsdata where GridID = '''+ @GridID+''' and Name=''Cust' + @FieldName + ''' and InfoCenterArea='''+ @InfoCenterArea+''')' + CHAR(13) + CHAR(10) +
			' begin' + CHAR(13) + CHAR(10) +
			'	print ''	Adding data row for ' + @FieldName + '''' + CHAR(13) + CHAR(10) +
			' INSERT INTO FW_CustomColumnsData ([InfoCenterArea],[GridID],[Name],[TabID],[DataType],[DisplayWidth],[Lines],[Sort],[LimitToList],[Total],[Decimals],[MinValue],[MaxValue],[Required],[ReqWBSLevel],[Seq],[CurrencyCode],[AvailableForCubes],[ColumnID],[FieldType],[PropertyBag],[SearchBy],[SearchResults]) ' +
			' SELECT '''+ @InfoCenterArea+''' as InfoCenterArea, '''+ @GridID+''' as GridID, ''Cust' + @FieldName+ ''' as Name,'+ @TabID+' as TabID, ' +
			' ''' + @FieldType + '''  as DataType,' +	Cast(@ColWidth as varchar(10)) + ' as DisplayWidth, 0 as Lines, ''N'' as Sort, ''Y'' as LimitToList, ''N'' as Total, '+
			Cast(@ColDecimals as varchar(10)) + ' as Decimals, NULL as MinValue, NULL as MaxValue, '+
			' NULL as Required, NULL as ReqWBSLevel, '+
			' ISNULL((select max(seq)+1 from FW_CustomColumnsdata WHERE InfoCenterArea='''+ @InfoCenterArea+''' and GridID = '''+ @GridID+'''), 1) as seq, NULL as CurrencyCode,' +
			' ''N'','''',''C'',NULL,''N'',''N'' ' +
			' end '
			
		if @debug='Y' print @sql
		execute(@sql)
		
		set @sql='if not exists (select ''x'' from FW_CustomColumnCaptions where GridID = '''+ @GridID+''' and Name=''Cust' + @FieldName + ''' and InfoCenterArea='''+ @InfoCenterArea+''' and UICultureName ='''+ @Culture +''')' + CHAR(13) + CHAR(10) +
			' begin' + CHAR(13) + CHAR(10) +
			'	print ''	Adding caption row for ' + @Label + '''' + CHAR(13) + CHAR(10) +
			'INSERT INTO FW_CustomColumnCaptions ([InfoCenterArea],[GridID],UICultureName,[Name],Label,DefaultValue) '+
			' SELECT '''+ @InfoCenterArea+''' as InfoCenterArea, '''+ @GridID+''' as GridID, ''' + @Culture + ''',''Cust' + @FieldName+ ''' as Name, '+
			' ''' + @Label+ ''' as Label,	NULL as DefaultValue ' + 
			' end '
		if @debug='Y' print @sql
		execute(@sql)

		if @FieldType = 'dropdown' and @ValueList IS NOT NULL	
		BEGIN	--TODO multiple values
			set @sql='IF NOT EXISTS (SELECT * FROM FW_CustomColumnValuesData where GridID = '''+ @GridID+''' and [ColName] = ''Cust' + @FieldName+ ''' and InfoCenterArea = '''+ @InfoCenterArea+''' and UICultureName ='''+ @Culture +''') ' +
				' INSERT INTO FW_CustomColumnValuesData ([InfoCenterArea],[GridID],[ColName],[DataValue],[Seq] ,[Code] ,[UICultureName]) ' +
				' VALUES ('''+ @InfoCenterArea+''' ,'''+ @GridID+''' ,''Cust' + @FieldName+ ''','''+ @ValueList + ''',1,'''+ @ValueList + ''',''' + @Culture + ''') ' 
			if @debug='Y' print @sql
			execute(@sql)
		END
	
	END--IF VISION 7
	
	if @SQLType is null
	begin
		set @SQLType = 
			case 
				when @FieldType = 'string' then '[varchar](255) NULL'
				when @FieldType = 'dropdown' then '[varchar](255) NULL'
				when @FieldType = 'employee' then '[varchar](20) NULL'
				when @FieldType = 'numeric' then '[decimal](19, 5) NOT NULL DEFAULT 0'
				when @FieldType = 'checkbox' then '[varchar](1) NULL'
				else '[varchar](255) NULL'
			end
	end
	

	if @InfoCenterArea = 'Projects' and @GridID = 'X'
	BEGIN
		
		set @sql='if not exists (select ''x'' from syscolumns a,sysobjects b where a.id=b.id and a.name=''Cust' + @FieldName + ''' and b.name=''ProjectCustomTabFields'') ' + CHAR(13) + CHAR(10) +
		' begin' + CHAR(13) + CHAR(10) +
		'	print ''	Adding column Cust' + @FieldName + ' to ProjectCustomTabFields'' ' + CHAR(13) + CHAR(10) +
		'	alter table ProjectCustomTabFields with nocheck add Cust' + @FieldName + ' ' + @SQLType + CHAR(13) + CHAR(10) +
		' end'
		if @debug='Y' print @sql
		execute(@sql)
		
		set @sql='if not exists (select ''x'' from syscolumns a,sysobjects b where a.id=b.id and a.name=''Cust' + @FieldName + ''' and b.name=''ProjectCustomTabFieldsTemplate'') ' + CHAR(13) + CHAR(10) +
		' begin' + CHAR(13) + CHAR(10) +
		'	print ''	Adding column Cust' + @FieldName + ' to ProjectCustomTabFieldsTemplate'' ' + CHAR(13) + CHAR(10) +
		'	alter table ProjectCustomTabFieldsTemplate with nocheck add Cust' + @FieldName + ' ' + @SQLType + CHAR(13) + CHAR(10) +
		' end'
		if @debug='Y' print @sql
		execute(@sql)
		-- Add column into CCG_EI_CustomColumns
		--ALTER TABLE ProjectCustomTabFields ADD CustTestVisionUDF VARCHAR(255) NULL
		--ALTER TABLE ProjectCustomTabFieldsTemplate ADD CustTestVisionUDF VARCHAR(255) NULL
		--CREATE TRIGGER dbo.VisionAudit_Insert_ProjectCustomTabFieldsTemplate and dbo.VisionAudit_Insert_ProjectCustomTabFields and update on both
	END 
END
GO
