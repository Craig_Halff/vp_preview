SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FormRoutePrompt_SetOptions]
	@sqlWhere	Nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max);
	DECLARE @safeSql	bit;

	-- Ensure that our inputs are safe (prevent SQL injection)
	SET @safeSql = dbo.fnCCG_PAT_RegexMatch(N'<SQL PHRASE>', @sqlWhere);
	IF @safeSql = 0 BEGIN
		SELECT 'ERROR: Unsafe parameters used! Execution cancelled.';
		RETURN;
	END;

	SET @sql = N'SELECT employee FROM EM WHERE ' + @sqlWhere;
	EXEC(@sql);
END;

GO
