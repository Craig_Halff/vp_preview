SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AddIndustryDefaultsAEesES]
AS
BEGIN

PRINT ' '
PRINT 'Adding AE Industry Defaults for es-ES'

BEGIN TRANSACTION

DELETE FROM BTLaborCatsDescriptions WHERE UICultureName = 'es-ES';
--..........................................................................................v(50)
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 1, 'es-ES','Principal'         WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 1);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 2, 'es-ES','Gestor de proyectos'   WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 2);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 3, 'es-ES','Consultor sénior' WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 3);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 4, 'es-ES','Arquitecto sénior'  WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 4);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 5, 'es-ES','Ingeniero sénior'   WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 5);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 6, 'es-ES','Ingeniero'          WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 6);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 7, 'es-ES','Arquitecto'         WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 7);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 8, 'es-ES','Diseñador'          WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 8);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 9, 'es-ES','Delineante'      WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 9);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 10,'es-ES','Topógrafo'          WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 10);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 11,'es-ES','Administrativo'    WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 11);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 12,'es-ES','Marketing'         WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 12);

DELETE FROM CFGActivitySubjectData WHERE UICultureName = 'es-ES';
--..................................................................................v(50)
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Venta por teléfono'               ,'es-ES',3 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Reunión inicial'        ,'es-ES',1 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Correo electrónico de presentación'      ,'es-ES',4 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Llamada de calificación'      ,'es-ES',2 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Mención en las redes sociales'    ,'es-ES',6 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Correo electrónico de liderazgo de pensamiento','es-ES',5 ,'N');

DELETE FROM CFGActivityTypeDescriptions WHERE UICultureName = 'es-ES';
--.........................................................................................................v(15)
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'EMail'     ,'es-ES','Correo electrónico'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'EMail');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Event'     ,'es-ES','Evento'          ,4 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Event');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Mailing'   ,'es-ES','Envío postal'        ,2 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Mailing');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Meeting'   ,'es-ES','Reunión'        ,3 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Meeting');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Merge'     ,'es-ES','Creación de documentos',5 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Merge');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Milestone' ,'es-ES','Hito'      ,6 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Milestone');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Phone Call','es-ES','Llamada telefónica'     ,7 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Phone Call');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Task'      ,'es-ES','Tarea'           ,8 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Task');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Touchpoint','es-ES','Punto de contacto'     ,9 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Touchpoint');

DELETE FROM CFGARLedgerHeadingsDescriptions WHERE UICultureName = 'es-ES';
--...............................................................................................v(12)
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 1,'es-ES','Tarifas'       WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 1);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 2,'es-ES','reemb,'     WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 2);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 3,'es-ES','Consultor' WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 3);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 4,'es-ES','Retenedor'   WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 4);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 5,'es-ES','Impuestos'      WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 5);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 6,'es-ES','Intereses'   WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 6);

DELETE FROM CFGAwardTypeDescriptions WHERE UICultureName = 'es-ES';
--............................................................................................v(255)
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '1','es-ES','IDIQ - GWAC'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 1);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '2','es-ES','IDIQ - Específico de la agencia',2 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 2);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '4','es-ES','BPA'                   ,3 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 4);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '5','es-ES','Otro'                 ,4 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 5);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '6','es-ES','Tarea / Orden de entrega' ,5 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 6);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '7','es-ES','Sin determinar'          ,6 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 7);

DELETE FROM CFGBillMainDescriptions WHERE UICultureName = 'es-ES';
--..............................................................................................................................................................................................................v(15)..v(15)....v(15).........v(15)......v(15)...v(15).....v(15)..v(15)
INSERT INTO CFGBillMainDescriptions (Company,UICultureName,FooterMessage,FeeLabel,LabLabel,ConLabel,ExpLabel,UnitLabel,AddOnLabel,TaxLabel,InterestLabel,OvtIndicator) SELECT CFGMainData.Company,'es-ES',NULL,'Tarifa' ,'Mano de obra' ,'Consultor' ,'Gastos' ,'Unidad' ,'Complemento' ,'Impuestos' ,'Intereses' ,'Ovt' FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGBillMainData WHERE Company = CFGMainData.Company);

DELETE FROM CFGCampaignActionDescriptions WHERE UICultureName = 'es-ES';
--..................................................................................................v(50)
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '01','es-ES','Desarrollo de contenido' ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '01');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '02','es-ES','Envío de invitaciones' ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '02');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '03','es-ES','Envío masivo de correos electrónicos',3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '03');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '04','es-ES','Seguimiento'       ,4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '04');

DELETE FROM CFGCampaignAudienceDescriptions WHERE UICultureName = 'es-ES';
--....................................................................................................v(50)
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '01','es-ES','Ejecutivos'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '01');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '02','es-ES','Finanzas'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '02');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '03','es-ES','Marketing'           ,3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '03');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '04','es-ES','Desarrollo empresarial',4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '04');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '05','es-ES','Gestores de proyectos'    ,5 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '05');

DELETE FROM CFGCampaignObjectiveDescriptions WHERE UICultureName = 'es-ES';
--.....................................................................................................v(50)
INSERT INTO CFGCampaignObjectiveDescriptions (Code,UICultureName,Description,Seq) SELECT '01','es-ES','Nueva generación de negocios' ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignObjectiveData WHERE Code = '01');
INSERT INTO CFGCampaignObjectiveDescriptions (Code,UICultureName,Description,Seq) SELECT '02','es-ES','Expansión del negocio existente',2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignObjectiveData WHERE Code = '02');

DELETE FROM CFGCampaignStatusDescriptions WHERE UICultureName = 'es-ES';
--..................................................................................................v(50)
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','es-ES','Activo'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '01');
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','es-ES','Planificación' ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '02');
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','es-ES','Completado',3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '03');

DELETE FROM CFGCampaignTypeDescriptions WHERE UICultureName = 'es-ES';
--................................................................................................v(50)
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','es-ES','Correo electrónico'      ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '01');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','es-ES','Correo directo',2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '02');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','es-ES','Webinario'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '03');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','es-ES','Feria'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '04');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '05','es-ES','Quedada'     ,5 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '05');

DELETE FROM CFGClientCurrentStatusDescriptions WHERE UICultureName = 'es-ES';
--...............................................................................................................v(50)
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Existing','es-ES','Existente',1 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Existing');
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Former'  ,'es-ES','Anterior'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Former');
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Prospect','es-ES','Potencial',2 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Prospect');

DELETE FROM CFGClientRelationshipDescriptions WHERE UICultureName = 'es-ES';
--......................................................................................................v(50)
INSERT INTO CFGClientRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01','es-ES','Competidor',1 WHERE EXISTS (SELECT 'x' FROM CFGClientRelationshipData WHERE Code = '01');

DELETE FROM CFGClientRoleDescriptions WHERE UICultureName = 'es-ES';
--....................................................................................................v(50)
INSERT INTO CFGClientRoleDescriptions (Code,UICultureName,Description,Seq) SELECT '01'      ,'es-ES','Subcontratista',2 WHERE EXISTS (SELECT 'x' FROM CFGClientRoleData WHERE Code = '01');
INSERT INTO CFGClientRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'sysOwner','es-ES','Cliente'       ,1 WHERE EXISTS (SELECT 'x' FROM CFGClientRoleData WHERE Code = 'sysOwner');

DELETE FROM CFGClientTypeDescriptions WHERE UICultureName = 'es-ES';
--..............................................................................................v(50)
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','es-ES','Formación'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '01');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','es-ES','Empresa'          ,2 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '02');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','es-ES','Finanzas y banca' ,3 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '03');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','es-ES','Salud y bienestar' ,4 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '04');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '05','es-ES','Sector hotelero'         ,5 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '05');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '06','es-ES','Servicios de información',6 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '06');

DELETE FROM CFGCompetitionTypeDescriptions WHERE UICultureName = 'es-ES';
--...................................................................................................v(255)
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '37','es-ES','Retirada 8(a)'                                       ,2  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '37')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '38','es-ES','Retirada SDB'                                        ,3  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '38')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '39','es-ES','Retirada pequeña empresa'                                  ,4  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '39')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '40','es-ES','Completo y abierto / Sin restricción'                         ,5  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '40')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '44','es-ES','Zona de hub'                                              ,6  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '44')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '45','es-ES','Pequeña empresa propiedad de veterano discapacitado durante el servicio'        ,7  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '45')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '47','es-ES','Sin determinar'                                         ,1  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '47')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '48','es-ES','Otro'                                                ,8  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '48')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '64','es-ES','Única fuente'                                          ,9  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '64')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '65','es-ES','Pequeña empresa propiedad de una mujer en reserva'                 ,10 WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '65')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '66','es-ES','Pequeña empresa propiedad de veterano'                         ,11 WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '66')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '67','es-ES','Pequeña empresa propiedad de mujer económicamente desfavorecida',12 WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '67')

DELETE FROM CFGContactRelationshipDescriptions WHERE UICultureName = 'es-ES';
--.......................................................................................................v(50)
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01','es-ES','Antiguo cliente'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '01');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '02','es-ES','Antiguo compañero de trabajo' ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '02');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '03','es-ES','Colega del sector'   ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '03');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '04','es-ES','Contacto personal',4 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '04');

DELETE FROM CFGContactRoleDescriptions WHERE UICultureName = 'es-ES';
--.......................................................................................................v(50)
INSERT INTO CFGContactRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Contractor','es-ES','Subcontratista',1 WHERE EXISTS (SELECT 'x' FROM CFGContactRoleData WHERE Code = 'Contractor');
INSERT INTO CFGContactRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'sysOwner'  ,'es-ES','Propietario'        ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactRoleData WHERE Code = 'sysOwner');

DELETE FROM CFGContactSourceDescriptions WHERE UICultureName = 'es-ES';
--...................................................................................................v(50)
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '01','es-ES','Referencia de cliente'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '01');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '02','es-ES','Evento'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '02');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '03','es-ES','Lista'              ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '03');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '04','es-ES','Contacto personal'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '04');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '05','es-ES','Redes sociales'      ,5 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '05');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '06','es-ES','Liderazgo de pensamiento',6 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '06');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '07','es-ES','Sitio web'           ,7 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '07');

DELETE FROM CFGContactTitleDescriptions WHERE UICultureName = 'es-ES';
--.................................................................................................v(50)
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Director' ,'es-ES','Director' ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Director');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Executive','es-ES','Ejecutivo',1 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Executive');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Finance'  ,'es-ES','Finanzas'  ,5 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Finance');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Marketing','es-ES','Marketing',6 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Marketing');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Sales'    ,'es-ES','Ventas'    ,4 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Sales');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'VP'       ,'es-ES','Vicepresidente'       ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'VP');

DELETE FROM CFGContactTypeDescriptions WHERE UICultureName = 'es-ES';
--..........................................................................................v(100)
INSERT INTO CFGContactTypeDescriptions (Code,UICultureName,Description) SELECT 'C','es-ES','Cliente' WHERE EXISTS (SELECT 'x' FROM CFGContactTypeData WHERE Code = 'C');

DELETE FROM CFGContractStatusDescriptions WHERE UICultureName = 'es-ES';
--..................................................................................................v(50)
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','es-ES','Revisión interna'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '01');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','es-ES','Enviado al cliente'   ,2 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '02');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','es-ES','Negociación'      ,3 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '03');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '04','es-ES','Revisión legal'     ,4 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '04');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '05','es-ES','Firmado y ejecutado',5 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '05');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '06','es-ES','Perdido/No hay acuerdo'     ,6 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '06');

DELETE FROM CFGContractTypeDescriptions WHERE UICultureName = 'es-ES';
--................................................................................................v(50)
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','es-ES','Original'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '01');
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','es-ES','Orden de cambio'       ,2 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '02');
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','es-ES','Servicios adicionales',3 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '03');
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','es-ES','Carta de acuerdo',4 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '04');

DELETE FROM CFGCubeTranslationDescriptions WHERE UICultureName = 'es-ES';
--..............................................................................................................................................v(200)
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'U'                       ,'es-ES','Desconocido');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'UM'                      ,'es-ES','Mes desconocido');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'H'                       ,'es-ES','Historial');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('YesNo'          ,'Y'                       ,'es-ES','Sí');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('YesNo'          ,'N'                       ,'es-ES','No');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('Status'         ,'A'                       ,'es-ES','Activo');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('Status'         ,'I'                       ,'es-ES','Inactivo');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'1-Asset'                 ,'es-ES','Activo');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'2-Liability'             ,'es-ES','Pasivo');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'3-NetWorth'              ,'es-ES','Valor neto');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'4-Revenue'               ,'es-ES','Ingresos');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'5-Reimbursable'          ,'es-ES','reembolsable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'6-ReimbursableConsultant','es-ES','Consultor reembolsable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'7-Direct'                ,'es-ES','directo');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'8-DirectConsultant'      ,'es-ES','Consultor directo');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'9-Indirect'              ,'es-ES','Indirecto');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'10-OtherCharges'         ,'es-ES','Otros cargos');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'U-Unknown'               ,'es-ES','Desconocido');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ExpenseType'    ,'5-ReimbursableOther'     ,'es-ES','Otros reembolsables');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ExpenseType'    ,'7-DirectOther'           ,'es-ES','Otro directo');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ReportType'     ,'BalanceSheet'            ,'es-ES','Hoja de balance');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ReportType'     ,'IncomeStatement'         ,'es-ES','Balance de ingresos');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'1-Asset','es-ES'         ,'Other Assets');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'2-Liability'             ,'es-ES','Otros pasivos');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'3-NetWorth'              ,'es-ES','Otro valor neto');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'4-Revenue'               ,'es-ES','Otro ingreso');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'5-Reimbursable'          ,'es-ES','Otro reembolsable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'6-Reimbursable'          ,'es-ES','Otro reembolsable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'7-Direct'                ,'es-ES','Otro directo');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'8-Direct'                ,'es-ES','Otro directo');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'9-Indirect'              ,'es-ES','Otros indirectos');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'10-OtherCharges'         ,'es-ES','Otros cargos');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'B'                       ,'es-ES','Facturable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'H'                       ,'es-ES','Retenido');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'W'                       ,'es-ES','Por cancelar');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'X'                       ,'es-ES','Cancelado');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'D'                       ,'es-ES','Marcado para eliminación');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'N'                       ,'es-ES','No facturable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'F'                       ,'es-ES','Facturado de forma definitiva');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'T'                       ,'es-ES','Transferido');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'R'                       ,'es-ES','Parcialmente retenido/liberado');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'M'                       ,'es-ES','Modificado');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'O'                       ,'es-ES','Eliminado');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AP'                      ,'es-ES','Vales de cuentas por pagar');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BA'                      ,'es-ES','Procesamiento de horas de beneficios');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BE'                      ,'es-ES','Transferencias de gastos de facturación');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BL'                      ,'es-ES','Transferencias de mano de obra de facturación');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BU'                      ,'es-ES','Transferencias de [Unit] de facturación');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CD'                      ,'es-ES','Desembolsos de efectivo');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CP'                      ,'es-ES','Convertir [Organization] del [WBS1]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CR'                      ,'es-ES','Recibos de efectivo');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CV'                      ,'es-ES','Desembolsos de cuentas por pagar');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EP'                      ,'es-ES','Procesamiento de pagos de [Employee]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','ER'                      ,'es-ES','Plazos de [Employee]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EX'                      ,'es-ES','Gastos de [Employee]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IN'                      ,'es-ES','Facturas');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','JE'                      ,'es-ES','Entradas de diario');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','KC'                      ,'es-ES','Conversión de clave');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','LA'                      ,'es-ES','Ajustes de mano de obra');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','LG'                      ,'es-ES','Ganancias/pérdidas y revalorizaciones');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','MI'                      ,'es-ES','Gastos varios');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PP'                      ,'es-ES','Procesamiento de pagos de cuentas por pagar');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PR'                      ,'es-ES','Impresiones y reproducciones');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PY'                      ,'es-ES','Procesamiento de nómina');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','RG'                      ,'es-ES','Generación de ingresos');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','TS'                      ,'es-ES','Hojas de asistencia');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','UN'                      ,'es-ES','[Units]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','UP'                      ,'es-ES','[Units] por [WBS1]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AL'                      ,'es-ES','Contabilización de conversión de clave, apertura de un nuevo período/trimestre W2/año de devengo de beneficios, cierre de un período');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AX'                      ,'es-ES','Vale de cuentas por pagar convertido');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CN'                      ,'es-ES','Gasto(s) de consultor convertido(s)');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CT'                      ,'es-ES','Transacción que indica cuándo se realizó la conversión de una liberación anterior');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EC'                      ,'es-ES','Ajuste(s) de análisis temporal convertido(s)');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IH'                      ,'es-ES','Factura convertida');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IX'                      ,'es-ES','Factura(s) convertida(s)');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','JX'                      ,'es-ES','Conversión de MCFMS');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PX'                      ,'es-ES','Procesamiento de pagos de cuentas por pagar convertido');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','RX'                      ,'es-ES','Conversión de MCFMS');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','XD'                      ,'es-ES','Datos de cuenta convertidos');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','XE'                      ,'es-ES','Gastos de [WBS1] convertidos');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HA'                      ,'es-ES','Historial de saldos de [Account]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HB'                      ,'es-ES','Historial de devengo de beneficios de [Employee]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HE'                      ,'es-ES','Historial de mano de obra y recibos de [WBS1]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HL'                      ,'es-ES','Historial de mano de obra y recibos de [WBS1]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HU'                      ,'es-ES','Ingreso sin facturar');

DELETE FROM CFGEMDegreeDescriptions WHERE UICultureName = 'es-ES';
--............................................................................................v(50)
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','es-ES','Licenciatura en ciencias',1 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '01');
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','es-ES','Licenciatura en humanidades'   ,2 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '02');
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','es-ES','Másteres'            ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '03');

DELETE FROM CFGEmployeeRelationshipDescriptions WHERE UICultureName = 'es-ES';
--..............................................................................................................v(50)
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01'      ,'es-ES','Antiguo empleador'            ,3 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '01');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '02'      ,'es-ES','Antiguo compañero de trabajo'            ,2 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '02');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '03'      ,'es-ES','Contacto personal'           ,5 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '03');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '04'      ,'es-ES','Contacto general del sector',4 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '04');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT 'SysOwner','es-ES','Propietario'                      ,1 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = 'SysOwner');

DELETE FROM CFGEmployeeRoleDescriptions WHERE UICultureName = 'es-ES';
--........................................................................................................v(50)
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Admin'     ,'es-ES','Administrativo'         ,7  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Admin');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'AirQuality','es-ES','Especialista en calidad del aire' ,8  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'AirQuality');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Architect' ,'es-ES','Arquitecto'              ,9  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Architect');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Associate' ,'es-ES','Asociado'              ,10 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Associate');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Biologist' ,'es-ES','Biólogo'              ,11 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Biologist');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'CADDTechi' ,'es-ES','Técnico de CADD'        ,12 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'CADDTechi');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'CivilEngr' ,'es-ES','Ingeniero civil'         ,13 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'CivilEngr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ConstIns'  ,'es-ES','Inspector de obra' ,14 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ConstIns');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ConstMgr'  ,'es-ES','Jefe de obra'   ,15 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ConstMgr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ElectEngr' ,'es-ES','Ingeniero eléctrico'    ,16 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ElectEngr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'EntitleSpl','es-ES','Especialista en subsidios' ,17 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'EntitleSpl');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'EnvirPlnr' ,'es-ES','Planificador ambiental'  ,18 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'EnvirPlnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Estimator' ,'es-ES','Tasador'              ,19 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Estimator');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'GISSpecial','es-ES','Especialista en SIG'         ,20 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'GISSpecial');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Hydrolgst' ,'es-ES','Hidrólogo'            ,21 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Hydrolgst');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'InfoTech'  ,'es-ES','Tecnología de la información' ,22 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'InfoTech');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'InteDsnr'  ,'es-ES','Diseñador de interiores'      ,23 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'InteDsnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'LandArch'  ,'es-ES','Arquitecto paisajista'    ,24 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'LandArch');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'MechEng'   ,'es-ES','Ingeniero mecánico'    ,25 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'MechEng');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'MEPCoordin','es-ES','Coordinador de MEP'        ,26 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'MEPCoordin');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Planner'   ,'es-ES','Planificador'                ,27 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Planner');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjAdmin' ,'es-ES','Administrador de proyectos'  ,28 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjAdmin');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjArch'  ,'es-ES','Arquitecto de proyectos'      ,29 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjArch');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjDsnr'  ,'es-ES','Diseñador de proyectos'       ,30 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjDsnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjEng'   ,'es-ES','Ingeniero de proyectos'       ,31 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjEng');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjPlnr'  ,'es-ES','Planificador de proyectos'        ,32 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjPlnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjSvor'  ,'es-ES','Supervisor de proyectos'       ,33 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjSvor');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'QAQCMgr'   ,'es-ES','Encargado de control y aseguramiento de la calidad'          ,34 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'QAQCMgr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'SpecWriter','es-ES','Redactor de especificaciones'  ,35 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'SpecWriter');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'StructEng' ,'es-ES','Ingeniero de estructuras'    ,36 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'StructEng');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Surveyor'  ,'es-ES','Topógrafo'               ,37 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Surveyor');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TechEditor','es-ES','Editor técnico'       ,38 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TechEditor');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TrafficSpe','es-ES','Especialista en tráfico'     ,39 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TrafficSpe');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TransEngr' ,'es-ES','Ingeniero de transportes',40 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TransEngr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TransPlnr' ,'es-ES','Planificador de transportes' ,41 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TransPlnr');

DELETE FROM CFGEmployeeTitleDescriptions WHERE UICultureName = 'es-ES';
--..................................................................................................v(50)
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Architect','es-ES','Arquitecto'              ,1  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Architect');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CFO'      ,'es-ES','Director financiero',2  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CFO');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CivilEng' ,'es-ES','Ingeniero civil'         ,3  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CivilEng');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Designer' ,'es-ES','Diseñador'               ,4  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Designer');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Director' ,'es-ES','Director'               ,5  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Director');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'EE'       ,'es-ES','Ingeniero medioambiental' ,8  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'EE');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'EIT'      ,'es-ES','Ingeniero en formación'   ,7  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'EIT');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Engineer' ,'es-ES','Ingeniero'               ,6  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Engineer');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'GE'       ,'es-ES','Ingeniero geotécnico'  ,10 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'GE');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'GM'       ,'es-ES','Director general'        ,9  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'GM');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'ID'       ,'es-ES','Diseñador de interiores'      ,12 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'ID');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'LA'       ,'es-ES','Arquitecto paisajista'    ,13 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'LA');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'PIC'      ,'es-ES','Encargado principal'    ,17 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'PIC');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Planner'  ,'es-ES','Planificador'                ,14 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Planner');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'PM'       ,'es-ES','Gestor de proyectos'        ,18 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'PM');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'President','es-ES','Presidente'              ,15 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'President');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Principal','es-ES','Principal'              ,16 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Principal');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'SUP'      ,'es-ES','Supervisor'            ,20 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'SUP');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'TC'       ,'es-ES','Consultor de formación'    ,19 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'TC');

DELETE FROM CFGEmployeeTypeDescriptions WHERE UICultureName = 'es-ES';
--.....................................................................................v(30)
INSERT INTO CFGEmployeeTypeDescriptions (Type,UICultureName,Label) SELECT 'E','es-ES','Empleado'  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTypeData WHERE Type = 'E');
INSERT INTO CFGEmployeeTypeDescriptions (Type,UICultureName,Label) SELECT 'P','es-ES','Principal' WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTypeData WHERE Type = 'P');

DELETE FROM CFGEMRegistrationDescriptions WHERE UICultureName = 'es-ES';
--...................................................................................................v(70)
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'AIA','es-ES','American Institute of Architects',2 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'AIA');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'EIT','es-ES','Ingeniero en formación'            ,5 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'EIT');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'PE' ,'es-ES','Ingeniero profesional'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'PE');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'PLS','es-ES','Topógrafo profesional'      ,4 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'PLS');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'RLS','es-ES','Topógrafo registrado'        ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'RLS');

DELETE FROM CFGEMSkillDescriptions WHERE UICultureName = 'es-ES';
--.............................................................................................v(70)
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '01'  ,'es-ES','Administrativo'                          ,1  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '01SF','es-ES','Ingeniero acústico'                     ,2  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '02'  ,'es-ES','Arquitectos'                              ,3  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '02');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '03SF','es-ES','Fotógrafo aéreo'                     ,4  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '03SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '04SF','es-ES','Ingeniero aeronáutico'                   ,5  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '04SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '05'  ,'es-ES','Delineantes'                               ,6  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '05');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '05SF','es-ES','Arqueólogo'                            ,7  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '05SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '07'  ,'es-ES','Inspectores de obra'                 ,8  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '07');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '07SF','es-ES','Biólogo'                               ,9  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '07SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '08'  ,'es-ES','Tasadores'                              ,10 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '08');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '08SF','es-ES','Técnico de CADD'                         ,11 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '08SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '09SF','es-ES','Cartógrafo'                            ,12 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '09SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '10'  ,'es-ES','Ingenieros sanitarios'                      ,13 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '10');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '11'  ,'es-ES','Economistas'                              ,14 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '11');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '11SF','es-ES','Químico'                                 ,15 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '11SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '12'  ,'es-ES','Ecologistas'                              ,16 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '12');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '13SF','es-ES','Ingeniero de comunicaciones'                 ,17 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '13SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '14'  ,'es-ES','Ingenieros químicos'                      ,18 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '14');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '14SF','es-ES','Programador informático'                     ,19 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '14SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '15'  ,'es-ES','Ingenieros civiles'                         ,20 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '15');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '16SF','es-ES','Jefe de obra'                    ,21 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '16SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '17SF','es-ES','Ingeniero de corrosión'                      ,22 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '17SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '18SF','es-ES','Ingeniero de costes/tasador'                 ,23 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '18SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '19'  ,'es-ES','Ingenieros eléctricos'                    ,24 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '19');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '21'  ,'es-ES','Topógrafos'                               ,25 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '21');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '22SF','es-ES','Ingeniero de electrónica'                    ,26 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '22SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '23SF','es-ES','Ingeniero medioambiental'                  ,27 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '23SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '24SF','es-ES','Científico medioambiental'                 ,28 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '24SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '25SF','es-ES','Ingeniero de protección contra incendios'                ,29 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '25SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '26SF','es-ES','Ingeniero forense'                       ,30 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '26SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '27'  ,'es-ES','Ingenieros mecánicos'                    ,31 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '27');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '27SF','es-ES','Ingeniero geotécnico/de cimientos'        ,32 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '27SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '28'  ,'es-ES','Ingenieros de minería'                        ,33 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '28');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '28SF','es-ES','Topógrafo geodésico'                       ,34 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '28SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '29SF','es-ES','Especialista en sistemas de información geográfica',35 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '29SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '30'  ,'es-ES','Ingenieros de suelos'                         ,36 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '30');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '31'  ,'es-ES','Redactores de especificaciones'                  ,37 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '31');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '31SF','es-ES','Planificador de centros de salud'                 ,38 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '31SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '32'  ,'es-ES','Ingenieros de estructuras'                    ,39 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '32');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '32SF','es-ES','Ingeniero hidráulico'                      ,40 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '32SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '33'  ,'es-ES','Ingenieros de transportes'                ,41 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '33SF','es-ES','Topógrafo hidrográfico'                   ,42 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '35SF','es-ES','Ingeniero industrial'                     ,43 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '36'  ,'es-ES','Geólogo'                               ,44 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '36SF','es-ES','Higienista industrial'                    ,45 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '38SF','es-ES','Topógrafo'                           ,46 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '40'  ,'es-ES','Hidrólogos'                            ,47 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '40SF','es-ES','Ingeniero de materiales'                      ,48 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '41SF','es-ES','Ingeniero de manejo de materiales'             ,49 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '42'  ,'es-ES','Diseñadores de interiores'                      ,50 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '43'  ,'es-ES','Arquitectos paisajistas'                    ,51 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '44'  ,'es-ES','Oceanógrafos'                          ,52 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '45'  ,'es-ES','Planificadores: urbanos/regionales'                ,53 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '45SF','es-ES','Intérprete de fotografías'                       ,54 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '46SF','es-ES','Fotogrametrista'                        ,55 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '48SF','es-ES','Gestor de proyectos'                         ,56 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '49SF','es-ES','Especialista en teledetección'               ,57 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '50SF','es-ES','Asesor de riesgos'                           ,58 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '51SF','es-ES','Ingeniero de seguridad/salud ocupacional'     ,59 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '53SF','es-ES','Programador'                               ,60 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '54SF','es-ES','Especialista en seguridad'                     ,61 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '58SF','es-ES','Técnico/analista'                      ,62 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '59SF','es-ES','Toxicólogo'                            ,63 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '61SF','es-ES','Ingeniero de valor'                          ,64 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '62SF','es-ES','Ingeniero de recursos hídricos'                ,65 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');

DELETE FROM CFGEMSkillLevelDescriptions WHERE UICultureName = 'es-ES';
--.............................................................................................v(50)
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 1,'es-ES','Básico'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 1);
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 2,'es-ES','Avanzada',2 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 2);
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 3,'es-ES','Experto'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 3);

DELETE FROM CFGEMSkillUsageDescriptions WHERE UICultureName = 'es-ES';
--.............................................................................................v(255)
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 1,'es-ES','Elemental'        ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 1);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 2,'es-ES','1-2 años'    ,2 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 2);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 3,'es-ES','3-5 años'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 3);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 4,'es-ES','6-10 años'   ,4 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 4);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 5,'es-ES','Más de 10 años',5 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 5);

DELETE FROM CFGLeadRatingDescriptions WHERE UICultureName = 'es-ES';
--..............................................................................................v(50)
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '01','es-ES','Caliente' ,1 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '01');
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '02','es-ES','Tibio',2 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '02');
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '03','es-ES','Frío',3 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '03');

DELETE FROM CFGOpportunityClosedReasonDescriptions WHERE UICultureName = 'es-ES';
--...........................................................................................................v(50)
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '01','es-ES','Precio'    ,1 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '01');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '02','es-ES','Servicios' ,2 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '02');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '03','es-ES','Recursos',3 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '03');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '04','es-ES','Plazos'   ,4 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '04');

DELETE FROM CFGOpportunitySourceDescriptions WHERE UICultureName = 'es-ES';
--...........................................................................................................v(50)
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'CRef'    ,'es-ES','Referencia de cliente'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'CRef');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Event'   ,'es-ES','Evento'                     ,2 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Event');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'List'    ,'es-ES','Lista'                      ,5 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'List');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'PersCont','es-ES','Contacto personal'          ,6 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'PersCont');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Social'  ,'es-ES','Redes sociales'              ,7 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Social');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'TLead'   ,'es-ES','Liderazgo de pensamiento'        ,8 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'TLead');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Web'     ,'es-ES','Web'                       ,9 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Web');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'WIQ'     ,'es-ES','GovWin IQ'                 ,3 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'WIQ');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'WOM'     ,'es-ES','Gestor de oportunidades de GovWin',4 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'WOM');

DELETE FROM CFGPrefixDescriptions WHERE UICultureName = 'es-ES';
--........................................................................................v(10)
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Dr.'  ,'es-ES','Dr.'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Dr.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Miss' ,'es-ES','Sra.' ,2 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Miss');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Mr.'  ,'es-ES','Sr.'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Mr.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Mrs.' ,'es-ES','Sra.' ,4 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Mrs.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Ms.'  ,'es-ES','Sra.'  ,5 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Ms.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Prof.','es-ES','Profesional',6 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Prof.');

DELETE FROM CFGProbabilityDescriptions WHERE UICultureName = 'es-ES';
--.....................................................................................................v(50)
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 5  ,'es-ES','05' ,1  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 5);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 10 ,'es-ES','10' ,2  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 10);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 20 ,'es-ES','20' ,3  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 20);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 30 ,'es-ES','30' ,4  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 30);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 40 ,'es-ES','40' ,5  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 40);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 50 ,'es-ES','50' ,6  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 50);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 60 ,'es-ES','60' ,7  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 60);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 70 ,'es-ES','70' ,8  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 70);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 80 ,'es-ES','80' ,9  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 80);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 90 ,'es-ES','90' ,10 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 90);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 95 ,'es-ES','95' ,11 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 95);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 100,'es-ES','100',12 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 100);

DELETE FROM CFGProjectCodeDescriptions WHERE UICultureName = 'es-ES';
--.......................................................................................................v(100)
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '001','es-ES','Acústica; reducción de ruido'                                                                    ,1   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '001');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '002','es-ES','Fotografía aérea; recopilación y análisis de datos y imágenes en el aire'                         ,2   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '002');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '003','es-ES','Desarrollo agrícola; almacenaje del grano; mecanización de granjas'                                   ,3   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '003');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '004','es-ES','Control de la contaminación atmosférica'                                                                         ,4   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '004');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '005','es-ES','Aeropuertos; sistemas de navegación; iluminación de aeropuertos; repostaje para aeronaves'                                         ,5   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '005');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '006','es-ES','Aeropuertos; terminales y hangares; manejo de mercancías'                                              ,6   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '006');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '007','es-ES','Instalaciones árticas'                                                                             ,7   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '007');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '008','es-ES','Auditorios y teatros'                                                                        ,8   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '008');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '009','es-ES','Automatización, controles; instrumentación'                                                         ,9   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '009');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '010','es-ES','Barracones; dormitorios'                                                                         ,10  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '010');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '011','es-ES','Puentes'                                                                                       ,11  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '011');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '012','es-ES','Cementerios (planificación y reubicación)'                                                            ,12  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '012');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '013','es-ES','Procesamiento y almacenamiento de productos químicos'                                                                 ,13  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '013');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '014','es-ES','Iglesias; capillas'                                                                             ,14  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '014');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '015','es-ES','Códigos; normas; ordenanzas'                                                                  ,15  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '015');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '016','es-ES','Almacenamiento en frío; refrigeración; congelación rápida'                                                      ,16  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '016');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '017','es-ES','Edificios comerciales; (construcción baja); centros comerciales'                                             ,17  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '017');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '018','es-ES','Sistemas de comunicación; televisión; microondas'                                                         ,18  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '018');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '019','es-ES','Instalaciones informáticas; servicio informático'                                                         ,19  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '019');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '020','es-ES','Conservación y gestión de recursos'                                                          ,20  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '020');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '021','es-ES','Gestión de obras'                                                                       ,21  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '021');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '022','es-ES','Control de corrosión; protección catódica; electrólisis'                                          ,22  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '022')
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '023','es-ES','Estimación de costes; ingeniería y análisis de costes; cálculo de costes paramétricos; previsión'               ,23  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '023');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '024','es-ES','Presas (de hormigón, de arco)'                                                                         ,24  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '024');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '025','es-ES','Presas (de tierra, roca); diques'                                                             ,25  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '025');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '026','es-ES','Desalinización (procesos e instalaciones)'                                                         ,26  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '026');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '027','es-ES','Comedores; clubes; restaurantes'                                                              ,27  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '027');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '028','es-ES','Investigaciones ecológicas y arqueológicas'                                                     ,28  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '028');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '029','es-ES','Instalaciones educativas; aulas'                                                            ,29  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '029');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '030','es-ES','Electrónica'                                                                                   ,30  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '030');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '031','es-ES','Ascensores; escaleras mecánicas; transporte de personas'                                                          ,31  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '031');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '032','es-ES','Conservación de energía; nuevas fuentes de energía'                                                       ,32  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '032');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '033','es-ES','Estudios de impacto medioambiental, evaluaciones o declaraciones'                                       ,33  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '033');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '034','es-ES','Refugios antinucleares; diseño resistente a explosiones'                                                      ,34  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '034');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '035','es-ES','Pabellones; gimnasios; estadios'                                                            ,35  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '035');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '036','es-ES','Protección contra incendios'                                                                               ,36  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '036');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '037','es-ES','Lonjas; escalas de peces'                                                                       ,37  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '037');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '038','es-ES','Silvicultura y productos forestales'                                                                    ,38  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '038');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '039','es-ES','Talleres; instalaciones de mantenimiento de vehículos; aparcamientos'                                       ,39  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '039');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '040','es-ES','Sistemas de gas (propano, natural, etc.)'                                                          ,40  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '040');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '041','es-ES','Diseño gráfico'                                                                                ,41  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '041');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '042','es-ES','Puertos; embarcaderos; muelles; instalaciones de terminal de barcos'                                             ,42  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '042');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '043','es-ES','Calefacción, ventilación, aire acondicionado'                                                        ,43  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '043');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '044','es-ES','Planificación de sistemas de salud'                                                                       ,44  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '044');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '045','es-ES','Edificaciones altas, con derechos de sobreelevación'                                                           ,45  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '045');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '046','es-ES','Carreteras, calles, pavimentación de aeródromos, aparcamientos'                                              ,46  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '046');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '047','es-ES','Preservación histórica'                                                                       ,47  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '047');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '048','es-ES','Hospitales e instalaciones médicas'                                                                ,48  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '048');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '049','es-ES','Hoteles; Moteles'                                                                                ,49  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '049');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '050','es-ES','Vivienda (residencial, multifamiliar, apartamentos, pisos)'                                   ,50  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '050');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '051','es-ES','Hidráulica y neumática'                                                                       ,51  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '051');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '052','es-ES','Edificios industriales; plantas de fabricación'                                                    ,52  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '052');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '053','es-ES','Procesos industriales; control de calidad'                                                         ,53  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '053');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '054','es-ES','Tratamiento de residuos industriales'                                                                     ,54  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '054');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '055','es-ES','Diseño de interiores; planificación de espacios'                                                               ,55  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '055');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '056','es-ES','Riego, drenaje'                                                                          ,56  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '056');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '057','es-ES','Instalaciones judiciales y juzgados'                                                             ,57  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '057');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '058','es-ES','Laboratorios; instalaciones de investigación médica'                                                      ,58  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '058');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '059','es-ES','Arquitectura del paisaje'                                                                        ,59  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '059');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '060','es-ES','Bibliotecas, museos, galerías'                                                                 ,60  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '060');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '061','es-ES','Iluminación (interior, exhibiciones, teatros, etc.)'                                                 ,61  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '061');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '062','es-ES','Iluminación (exteriores, calles, monumentos, pistas de atletismo)'                                      ,62  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '062');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '063','es-ES','Sistemas de manipulación de materiales, transportadores, clasificadores'                                                ,63  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '063');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '064','es-ES','Metalurgia'                                                                                    ,64  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '064');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '065','es-ES','Microclimatología; ingeniería tropical'                                                        ,65  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '065');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '066','es-ES','Estándares de diseño militar'                                                                     ,66  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '066');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '067','es-ES','Minería y mineralogía'                                                                         ,67  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '067');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '068','es-ES','Instalaciones de misiles (silos, combustibles, transporte)'                                                  ,68  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '068');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '069','es-ES','Diseño de sistemas modulares; estructuras o componentes prefabricación'                                      ,69  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '069');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '070','es-ES','Arquitectura naval; plataformas en alta mar'                                                       ,70  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '070');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '071','es-ES','Instalaciones nucleares; blindaje nuclear'                                                         ,71  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '071');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '072','es-ES','Edificios de oficinas; parques industriales'                                                             ,72  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '072');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '073','es-ES','Ingeniería oceanográfica'                                                                     ,73  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '073');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '074','es-ES','Normas; municiones; armas especiales'                                                       ,74  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '074');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '075','es-ES','Exploración petrolera; refinado'                                                               ,75  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '075');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '076','es-ES','Petróleo y combustibles (almacenamiento y distribución)'                                                 ,76  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '076');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '077','es-ES','Tuberías (a nivel nacional; líquido y gas)'                                                       ,77  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '077');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '078','es-ES','Planificación (comunidad, regional, a nivel de área y estatal)'                                              ,78  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '078');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '079','es-ES','Planificación (sitio, instalación y proyecto)'                                                     ,79  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '079');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '080','es-ES','Fontanería y diseño de tuberías'                                                                        ,80  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '080');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '081','es-ES','Estructuras neumáticas; edificios de apoyo aéreo'                                                   ,81  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '081');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '082','es-ES','Instalaciones de correos'                                                                             ,82  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '082');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '083','es-ES','Generación, transmisión, distribución de energía'                                                  ,83  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '083');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '084','es-ES','Prisiones y centros penitenciarios'                                                             ,84  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '084');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '085','es-ES','Diseño de productos, máquinas y equipos'                                                           ,85  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '085');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '086','es-ES','Radar, sónar, radio y telescopios de radar'                                                        ,86  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '086');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '087','es-ES','Transporte ferroviario y de gran velocidad'                                                                    ,87  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '087');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '088','es-ES','Instalaciones recreativas (parques, puertos deportivos, etc.)'                                                ,88  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '088');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '089','es-ES','Rehabilitación (edificios, estructuras, instalaciones)'                                            ,89  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '089');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '090','es-ES','Recuperación de recursos; reciclaje'                                                                 ,90  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '090');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '091','es-ES','Sistemas de radiofrecuencia y blindaje'                                                          ,91  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '091');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '092','es-ES','Canales fluviales; vías fluviales; control de inundaciones'                                                       ,92  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '092');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '093','es-ES','Ingeniería de seguridad; estudios de accidentes; estudios de la OSHA (Administración de Seguridad y Salud Ocupacional)'                                            ,93  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '093');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '094','es-ES','Sistemas de seguridad; detección de intrusos y humos'                                                  ,94  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '094');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '095','es-ES','Diseño y estudios sísmicos'                                                                      ,95  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '095');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '096','es-ES','Recolección, tratamiento y eliminación de aguas residuales'                                                       ,96  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '096');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '097','es-ES','Estudios geológicos y de suelos; cimientos'                                                         ,97  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '097');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '098','es-ES','Aprovechamiento de la energía solar'                                                                      ,98  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '098');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '099','es-ES','Desechos sólidos, incineración, vertederos'                                                          ,99  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '099');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '100','es-ES','Entornos especiales, salas blancas, etc.'                                                       ,100 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '100');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '101','es-ES','Diseño estructural; estructuras especiales'                                                         ,101 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '101');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '102','es-ES','Topografía, estudios del terreno, creación de mapas, estudios de llanura aluvial'                                             ,102 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '102');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '103','es-ES','Piscinas'                                                                                ,103 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '103');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '104','es-ES','Manejo e instalaciones de aguas pluviales'                                                              ,104 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '104');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '105','es-ES','Sistemas telefónicos (rural, móvil, intercomunicadores, etc.)'                                             ,105 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '105');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '106','es-ES','Servicios de pruebas e inspección'                                                                 ,106 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '106');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '107','es-ES','Ingeniería de tráfico y transporte'                                                          ,107 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '107');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '108','es-ES','Torres (con y sin personal)'                                                      ,108 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '108');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '109','es-ES','Túneles y subterráneos'                                                                             ,109 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '109');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '110','es-ES','Reformas urbanas; desarrollo comunitario'                                                         ,110 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '110');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '111','es-ES','Servicios públicos (gas y vapor)'                                                                     ,111 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '111');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '112','es-ES','Análisis de valor; coste del ciclo de vida'                                                            ,112 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '112');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '113','es-ES','Almacenes y depósitos'                                                                           ,113 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '113');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '114','es-ES','Recursos hídricos; hidrología; aguas subterráneas'                                                      ,114 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '114');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '115','es-ES','Abastecimiento, tratamiento y distribución de agua'                                                      ,115 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '115');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '116','es-ES','Túneles de viento; diseño de instalaciones de investigación/pruebas'                                              ,116 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '116');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '117','es-ES','Zonificación; estudios de uso del suelo'                                                                      ,117 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '117');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'A08','es-ES','Instalaciones para animales'                                                                             ,118 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'A08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'A09','es-ES','Protección militar/antiterrorismo'                                                               ,119 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'A09');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'A10','es-ES','Reducción de amianto'                                                                            ,120 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'A10');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C01','es-ES','Cartografía'                                                                                   ,121 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C01');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C03','es-ES','Cartografía: náutica y aeronáutica'                                                           ,122 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C05','es-ES','Instalaciones de cuidado/desarrollo infantil'                                                             ,123 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C07','es-ES','Ingeniería costera'                                                                           ,124 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C07');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C11','es-ES','Instalaciones comunitarias'                                                                          ,125 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C11');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C16','es-ES','Topografía de la construcción'                                                                        ,126 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C16');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C19','es-ES','Instalaciones criogénicas'                                                                          ,127 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C19');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D04','es-ES','Diseño, creación y preparación de solicitudes de propuestas'                                          ,128 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D05','es-ES','Desarrollo de modelos de terreno y elevación digital'                                               ,129 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D06','es-ES','Ortofotografía digital'                                                                      ,130 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D06');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D08','es-ES','Estudios y diseño de dragado'                                                                   ,131 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E03','es-ES','Estudios y diseño eléctrico'                                                                 ,132 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E06','es-ES','Embajadas y cancillerías'                                                                      ,133 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E06');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E08','es-ES','Economía de la ingeniería'                                                                         ,134 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E10','es-ES','Mapeo de recursos naturales y medioambientales'                                                    ,135 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E10');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E11','es-ES','Planificación medioambiental'                                                                        ,136 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E11');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E12','es-ES','Saneamiento medioambiental'                                                                     ,137 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E12');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E13','es-ES','Análisis y pruebas medioambientales'                                                            ,138 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E13');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'F05','es-ES','Ingeniería forense'                                                                          ,139 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'F05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'G03','es-ES','Topografía geodésica: terrestre y aerotransportada'                                                       ,140 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'G03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'G04','es-ES','Servicios de sistema de información geográfica: desarrollo, análisis y recopilación de datos'            ,141 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'G04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'G05','es-ES','Conversión de datos geoespaciales: escaneo, digitalización, recopilación, atribución, trazado, diseño',142 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'G05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'H02','es-ES','Manipulación y almacenamiento de materiales peligrosos'                                                      ,143 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'H02');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'H03','es-ES','Saneamiento de desechos peligrosos, tóxicos y radiactivos'                                               ,144 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'H03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'H13','es-ES','Topografía hidrográfica'                                                                        ,145 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'H13');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'I04','es-ES','Sistemas de transporte inteligentes'                                                            ,146 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'I04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'L02','es-ES','Topografía del terreno'                                                                                ,147 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'L02');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'M01','es-ES','Sistemas de dirección/localización de mapeo'                                                           ,148 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'M01');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'N02','es-ES','Estructuras de navegación; esclusas'                                                                  ,149 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'N02');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'P03','es-ES','Fotogrametría'                                                                                ,150 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'P03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'P13','es-ES','Instalaciones de seguridad pública'                                                                      ,151 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'P13');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R05','es-ES','Instalaciones y sistemas de refrigeración'                                                                  ,152 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R07','es-ES','Teledetección'                                                                                ,153 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R07');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R08','es-ES','Instalaciones de investigación'                                                                           ,154 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R10','es-ES','Análisis de riesgos'                                                                                 ,155 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R10');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R12','es-ES','Tejados'                                                                                       ,156 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R12');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'S11','es-ES','Diseño sostenible'                                                                            ,157 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'S11');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'T04','es-ES','Inspección topográfica y cartografía'                                                             ,158 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'T04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'U01','es-ES','Eliminación de municiones sin explotar'                                                               ,159 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'U01');

DELETE FROM CFGProjectMilestoneDescriptions WHERE UICultureName = 'es-ES';
--...........................................................................................................................v(255)
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysEstStart',           'es-ES','Inicio estimado',                   1 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysEstStart')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysEstCompletion',      'es-ES','Finalización estimada',              2 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysEstCompletion')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysContract',           'es-ES','Contrato adjudicado',                  3 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysContract')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysBidSubmitted',       'es-ES','Puja enviada',                     4 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysBidSubmitted')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysStart',              'es-ES','Inicio real',                      5 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysStart')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysActualCompl',        'es-ES','Finalización real',                 6 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysActualCompl')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysProfServicesCompl',  'es-ES','Finalización de los servicios profesionales',  7 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysProfServicesCompl')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysConstCompl',         'es-ES','Finalización de obra',           8 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysConstCompl')


DELETE FROM CFGProposalSourceDescriptions WHERE UICultureName = 'es-ES';
--..................................................................................................v(50)
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '01','es-ES','Referencia de cliente'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '01');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '02','es-ES','Evento'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '02');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '03','es-ES','Lista'              ,3 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '03');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '04','es-ES','Contacto personal'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '04');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '05','es-ES','Redes sociales'      ,5 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '05');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '06','es-ES','Liderazgo de pensamiento',6 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '06');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '07','es-ES','Sitio web'           ,7 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '07');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '08','es-ES','GovWinIQ'          ,8 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '08');

DELETE FROM CFGProposalStatusDescriptions WHERE UICultureName = 'es-ES';
--..................................................................................................v(50)
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','es-ES','En curso',1 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '01');
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','es-ES','En revisión'  ,2 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '02');
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','es-ES','Enviado'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '03');

DELETE FROM CFGPRResponsibilityDescriptions WHERE UICultureName = 'es-ES';
--....................................................................................................v(80)
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'C' ,'es-ES','Consultor'           ,3 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'C');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'IE','es-ES','Experiencia individual',4 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'IE');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'JV','es-ES','Empresa conjunta'        ,2 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'JV');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'P' ,'es-ES','Preferencial'                ,1 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'P');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'S' ,'es-ES','Subcontratista'        ,5 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'S');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'U' ,'es-ES','Sin determinar'         ,6 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'U');

DELETE FROM CFGPYAccrualsDescriptions WHERE UICultureName = 'es-ES';
--............................................................................................................................v(40)
INSERT INTO CFGPYAccrualsDescriptions (Code,Company,UICultureName,Description) SELECT 'Sick Lv.',CFGMainData.Company,'es-ES','Baja' FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGPYAccrualsData WHERE Code = 'Sick Lv.' AND Company = CFGMainData.Company)
INSERT INTO CFGPYAccrualsDescriptions (Code,Company,UICultureName,Description) SELECT 'Vacation',CFGMainData.Company,'es-ES','Vacaciones'   FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGPYAccrualsData WHERE Code = 'Vacation' AND Company = CFGMainData.Company)

DELETE FROM CFGRGMethodsDescriptions WHERE UICultureName = 'es-ES';
--..........................................................................................v(40)
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'B','es-ES','Facturaciones JTD'                   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'B');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'M','es-ES','(DL JTD*mult) + gasto reemb. JTD'  WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'M');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'N','es-ES','Sin reconocimiento de ingresos'         WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'N');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'P','es-ES','(Porc. Comp*Tarifa) + Gastos reemb. JTD' WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'P');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'R','es-ES','Recibos JTD'                   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'R');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'W','es-ES','Facturación JTD + trabajo en curso en facturación'   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'W');

DELETE FROM CFGServiceProfileDescriptions WHERE UICultureName = 'es-ES';
--..................................................................................................................v(40)
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.18' ,'es-ES','Plan de aprovechamiento/desarrollo del terreno','Area in hectares'               WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.18');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.19' ,'es-ES','Plan de edificación/aprovechamiento del terreno','Area in hectares'                          WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.19');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.23' ,'es-ES','Plan de paisajismo','Area in hectares'                                  WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.23');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.24' ,'es-ES','Plan de espacio abierto/verde','Area in hectares'                           WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.24');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.25' ,'es-ES','Plan de estructura paisajística','Area in hectares'                        WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.25');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.26' ,'es-ES','Plan de conservación del paisaje','Area in hectares'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.26');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.27' ,'es-ES','Plan de mantenimiento y desarrollo','Area in hectares'                WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.27');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.34' ,'es-ES','Edificios e interiores','Chargeable costs in Euro'                 WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.34');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.39' ,'es-ES','Instalaciones al aire libre','Chargeable costs in Euro'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.39');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.43' ,'es-ES','Estructuras de ingeniería civil','Chargeable costs in Euro'            WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.43');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.47' ,'es-ES','Instalaciones de transporte','Chargeable costs in Euro'               WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.47');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.51' ,'es-ES','Planificación estructural','Chargeable costs in Euro'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.51');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.55' ,'es-ES','Equipamiento técnico','Chargeable costs in Euro'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.55');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A01','es-ES','Estudios de impacto medioambiental','Area in hectares'                    WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A01');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A02','es-ES','Aislamiento térmico y equilibrio energético','Chargeable costs in Euro' WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A02');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A03','es-ES','Acústica de edificios','Chargeable costs in Euro'                      WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A03');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A04','es-ES','Acústica de salas','Chargeable costs in Euro'                          WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A04');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A05','es-ES','Geotecnia','Chargeable costs in Euro'                             WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A05');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A06','es-ES','Operaciones de medición/topografía','Accounting units'                  WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A06');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A07','es-ES','Inspección de ingeniería','Chargeable costs in Euro'                   WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A07');

DELETE FROM CFGSuffixDescriptions WHERE UICultureName = 'es-ES';
--......................................................................................v(150)
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'I'  ,'es-ES','I'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'I');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'II' ,'es-ES','II' ,4 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'II');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'III','es-ES','III',5 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'III');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'Jr.','es-ES','Jr.',1 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'Jr.');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'Sr.','es-ES','Sr.',2 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'Sr.');

DELETE FROM CFGUnitTypeDescriptions WHERE UICultureName = 'es-ES';
--...................................................................................................v(50)
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Equipment','es-ES','Equipo',1 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Equipment');
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Expenses ','es-ES','Gastos' ,2 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Expenses');
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Labor'    ,'es-ES','Mano de obra'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Labor');

DELETE FROM CFGVendorTypeDescriptions WHERE UICultureName = 'es-ES';
--...................................................................................v(30)
INSERT INTO CFGVendorTypeDescriptions (Type,UICultureName,Label) SELECT 'C','es-ES','Consultor' WHERE EXISTS (SELECT 'x' FROM CFGVendorTypeData WHERE Type = 'C');
INSERT INTO CFGVendorTypeDescriptions (Type,UICultureName,Label) SELECT 'T','es-ES','Comercio'      WHERE EXISTS (SELECT 'x' FROM CFGVendorTypeData WHERE Type = 'T');

DELETE FROM FW_CFGAttachmentCategoryDesc WHERE UICultureName = 'es-ES';
--................................................................................................................................................v(255)
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Notes'          ,'Activities'         ,'es-ES','Notas'                   ,1  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Notes' and Application ='Activities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'NDA'            ,'Contacts'           ,'es-ES','Acuerdo de confidencialidad',2  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'NDA' and Application ='Contacts');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Notes'          ,'Contacts'           ,'es-ES','Notas'                   ,3  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Notes' and Application ='Contacts');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'CompanyOverview','TextLibrary'        ,'es-ES','Resumen de la empresa'        ,4  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'CompanyOverview' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Case Study'     ,'TextLibrary'        ,'es-ES','Caso práctico'              ,5  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Case Study' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Reference'      ,'TextLibrary'        ,'es-ES','Referencia'               ,6  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Reference' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Resume'         ,'Employees'          ,'es-ES','Currículum vitae'                  ,7  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Resume' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Award'          ,'Employees'          ,'es-ES','Premio'                   ,8  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Award' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Leadership'     ,'Employees'          ,'es-ES','Liderazgo de pensamiento'      ,9  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Leadership' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Firms'              ,'es-ES','Contrato de servicio principal',10 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Firms'              ,'es-ES','Declaración de trabajo'       ,11 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'NDA'            ,'Firms'              ,'es-ES','Acuerdo de confidencialidad',12 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'NDA' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'ClientACH'      ,'Firms'              ,'es-ES','ACH de cliente'              ,13 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'ClientACH' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'VendorACH'      ,'Firms'              ,'es-ES','ACH de proveedor'              ,14 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'VendorACH' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'DigitalArtifact','Marketing Campaigns','es-ES','Documento digital'        ,15 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'DigitalArtifact' and Application ='Marketing Campaigns');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Estimate'       ,'Opportunities'      ,'es-ES','Estimación'                ,16 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Estimate' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Proposal'       ,'Opportunities'      ,'es-ES','Propuesta'                ,17 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Proposal' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Opportunities'      ,'es-ES','Declaración de trabajo'       ,18 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Opportunities'      ,'es-ES','Contrato de servicio principal',19 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Estimate'       ,'Projects'           ,'es-ES','Estimación'                ,20 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Estimate' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Proposal'       ,'Projects'           ,'es-ES','Propuesta'                ,21 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Proposal' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Projects'           ,'es-ES','Declaración de trabajo'       ,22 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Projects'           ,'es-ES','Contrato de servicio principal',23 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Projects');

DELETE FROM FW_CFGLabelData WHERE UICultureName = 'es-ES';
--............................................................................................................................................................................v(100)
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','accountLabel'                            ,'Cuenta'                       ,'[Account]'                    ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','accountLabelPlural'                      ,'Cuentas'                      ,'[Accounts]'                   ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','clientLabel'                             ,'Cliente'                        ,'[Client]'                     ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','clientLabelPlural'                       ,'Clientes'                       ,'[Clients]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','contactLabel'                            ,'Contacto'                       ,'[Contact]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','contactLabelPlural'                      ,'Contactos'                      ,'[Contacts]'                   ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','employeeLabel'                           ,'Empleado'                      ,'[Employee]'                   ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','employeeLabelPlural'                     ,'Empleados'                     ,'[Employees]'                  ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','labcd1Label'                             ,'Código de mano de obra Nivel 1'            ,'[Labcd Level 1]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','labcd1LabelPlural'                       ,'Código de mano de obra Nivel 1s'           ,'[Labcd Level 1s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','labcd2Label'                             ,'Código de mano de obra Nivel 2'            ,'[Labcd Level 2]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','labcd2LabelPlural'                       ,'Código de mano de obra Nivel 2s'           ,'[Labcd Level 2s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','labcd3Label'                             ,'Código de mano de obra Nivel 3'            ,'[Labcd Level 3]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','labcd3LabelPlural'                       ,'Código de mano de obra Nivel 3s'           ,'[Labcd Level 3s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','labcd4Label'                             ,'Código de mano de obra Nivel 4'            ,'[Labcd Level 4]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','labcd4LabelPlural'                       ,'Código de mano de obra Nivel 4s'           ,'[Labcd Level 4s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','labcd5Label'                             ,'Código de mano de obra Nivel 5'            ,'[Labcd Level 5]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','labcd5LabelPlural'                       ,'Código de mano de obra Nivel 5s'           ,'[Labcd Level 5s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','labcdLabel'                              ,'Código de mano de obra'                    ,'[Labor Code]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','labcdLabelPlural'                        ,'Códigos de mano de obra'                   ,'[Labor Codes]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','firmLabel'                               ,'Firma'                          ,'[Firm]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','firmLabelPlural'                         ,'Firmas'                         ,'[Firms]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','mktLabel'                                ,'Campaña de marketing'            ,'[Marketing Campaign]'         ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','mktLabelPlural'                          ,'Campañas de marketing'           ,'[Marketing Campaigns]'        ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','org1Label'                               ,'Organización 1'                ,'[Org Level 1]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','org1LabelPlural'                         ,'Organización 1s'               ,'[Org Level 1s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','org2Label'                               ,'Organización 2'                ,'[Org Level 2]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','org2LabelPlural'                         ,'Organización 2s'               ,'[Org Level 2s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','org3Label'                               ,'Organización 3'                ,'[Org Level 3]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','org3LabelPlural'                         ,'Organización 3s'               ,'[Org Level 3s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','org4Label'                               ,'Organización 4'                ,'[Org Level 4]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','org4LabelPlural'                         ,'Organización 4s'               ,'[Org Level 4s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','org5Label'                               ,'Organización 5'                ,'[Org Level 5]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','org5LabelPlural'                         ,'Organización 5s'               ,'[Org Level 5s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','orgLabel'                                ,'Organización'                  ,'[Organization]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','orgLabelPlural'                          ,'Organizaciones'                 ,'[Organizations]'              ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','proposalLabel'                           ,'Propuesta'                      ,'[Proposal]'                   ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','proposalLabelPlural'                     ,'Propuestas'                     ,'[Proposals]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','textLibLabel'                            ,'Patrón'                   ,'[Text Library]'               ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','textLibLabelPlural'                      ,'Patrones'                  ,'[Text Libraries]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','unitLabel'                               ,'Unidad'                          ,'[Unit]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','unitLabelPlural'                         ,'Unidades'                         ,'[Units]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','vendorLabel'                             ,'Proveedor'                        ,'[Vendor]'                     ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','vendorLabelPlural'                       ,'Proveedores'                       ,'[Vendors]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','wbs1Label'                               ,'Proyecto'                       ,'[WBS1]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','wbs1LabelPlural'                         ,'Proyectos'                      ,'[WBS1s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','wbs2Label'                               ,'Fase'                         ,'[WBS2]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','wbs2LabelPlural'                         ,'Fases'                        ,'[WBS2s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','wbs3Label'                               ,'Tarea'                          ,'[WBS3]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','wbs3LabelPlural'                         ,'Tareas'                         ,'[WBS3s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','sysPM'                                   ,'Gestor de proyectos'               ,'[Project Manager]'            ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','sysPR'                                   ,'Principal'                     ,'[Principal]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','sysSP'                                   ,'Supervisor'                    ,'[Supervisor]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','equipmentLabel'                          ,'Equipo'                     ,'[Equipment]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','equipmentLabelPlural'                    ,'Equipo'                     ,'[Equipments]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','jobToDateLabel'                          ,'trabajo hasta la fecha'                   ,'[JobToDate]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','JTDLabel'                                ,'JTD'                           ,'[JTD]'                        ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','estimateToCompleteLabel'                 ,'Estimado para completar'          ,'[EstimateToComplete]'         ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','ETCLabel'                                ,'ETC'                           ,'[ETC]'                        ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','estimateAtCompletionLabel'               ,'Fecha de finalización estimada'        ,'[EstimateAtCompletion]'       ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','EACLabel'                                ,'EAC'                           ,'[EAC]'                        ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','sysPRM'                                  ,'Gestor de propuestas'              ,'[Proposal Manager]'           ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','sysMC'                                   ,'Coordinador de Marketing'         ,'[Marketing Coordinator]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','sysBDL'                                  ,'Cliente potencial de desarrollo empresarial'     ,'[Business Development Lead]'  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','consultantLabel'                         ,'Consultor'                    ,'[Consultant]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','consultantLabelPlural'                   ,'Consultores'                   ,'[Consultants]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','compensationLabel'                       ,'Compensación'                  ,'[Compensation]'               ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','revType1Label'                           ,'Mano de obra'                         ,'[Revenue Method Type 1]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','revType2Label'                           ,'Consultor'                    ,'[Revenue Method Type 2]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','revType3Label'                           ,' reembolsable'                         ,'[Revenue Method Type 3]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','revType4Label'                           ,'Método de ingreso 4'              ,'[Revenue Method Type 4]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','revType5Label'                           ,'Método de ingreso 5'              ,'[Revenue Method Type 5]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','feeEstimatingCostGroupLabel'             ,'Grupo de costes'                    ,'[Cost Group]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','feeEstimatingCostGroupLabelPlural'       ,'Grupos de costes'                   ,'[Cost Groups]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','feeEstimatingFunctionalGroupLabel'       ,'Grupo funcional'              ,'[Functional Group]'           ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','feeEstimatingFunctionalGroupLabelPlural' ,'Grupos funcionales'             ,'[Functional Groups]'          ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','feeEstimatingServiceProfileLabel'        ,'Perfil de servicio'               ,'[Service Profile]'            ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('es-ES','feeEstimatingServiceProfileLabelPlural'  ,'Perfiles de servicio'              ,'[Service Profiles]'           ,'N','N',NULL);

COMMIT TRANSACTION

END
GO
