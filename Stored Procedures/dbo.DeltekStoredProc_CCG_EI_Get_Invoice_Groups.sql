SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Get_Invoice_Groups] ( @KEY_INVOICE_GROUP_FIELD varchar(100), @VISION_LANGUAGE varchar(100), @UserId nvarchar(32))
             AS EXEC spCCG_EI_Get_Invoice_Groups @KEY_INVOICE_GROUP_FIELD,@VISION_LANGUAGE,@UserId
GO
