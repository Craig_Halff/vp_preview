SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BankCodeInUse] @bankcode Nvarchar(20),@TableName Nvarchar(50) output
as
declare
  @SqlStmt		Nvarchar(max),
  @oneQry 		Nvarchar(400),
  @wkQry 		Nvarchar(max)
begin 
  set @TableName = ''
  set @wkQry = ''

	DECLARE QryCursor CURSOR FOR
		---     Select TOP 1 'ADPCode' as TableName from ADPCode where WBS1 = '1999009.00'

        Select 'SELECT TOP 1 ''' + sys.objects.name + ''' as TableName FROM [' + sys.objects.name + '] WHERE ' + sys.columns.name 
        + '= '''+ @bankcode + '''' As Qry
		FROM sys.columns
		INNER JOIN sys.objects ON sys.columns.object_id = sys.objects.object_id and 
		sys.objects.Name not in ('CFGBankTextFormat','LedgerAR','LedgerAP','LedgerEX','LedgerMisc','apppChecks','exChecks','CFGBankEntriesCodeData','CFGBankEntriesCodeDescriptions')
		WHERE (sys.columns.name like '%BankCode%' OR sys.columns.name like '%DefaultBank%') 
		      AND sys.objects.type = 'U' and left(sys.objects.name, 12) <> 'VisionImport' 
			  AND sys.objects.schema_id=1
		ORDER BY sys.objects.name, sys.columns.name
	OPEN QryCursor
	FETCH NEXT FROM QryCursor INTO @oneQry
	WHILE @@FETCH_STATUS = 0
	begin
		if (@wkQry > '') 	set @wkQry = @wkQry + ' UNION '
		set @wkQry = @wkQry + rtrim (@oneQry)
		fetch next from QryCursor into @oneQry
	End
	close QryCursor
	deallocate QryCursor

  set @SqlStmt = 'declare ChkCursor cursor for SELECT TOP 1 TableName from  (' + @wkQry + ') as TableCheck'

      execute (@SqlStmt)
      open ChkCursor
      fetch ChkCursor into @TableName
      close ChkCursor
      deallocate ChkCursor
end -- BankCodeInUse  
GO
