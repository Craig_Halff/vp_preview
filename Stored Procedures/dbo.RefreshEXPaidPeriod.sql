SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RefreshEXPaidPeriod] AS 
BEGIN
	SET NOCOUNT ON
	UPDATE EX SET EX.PaidPeriod = 0 WHERE EX.PaidPeriod <> 0

	UPDATE EX SET EX.PaidPeriod = L.MaxPeriod
	From EX Inner Join (
		SELECT LedgerEX.Employee, LedgerEX.Voucher, Max(LedgerEX.Period) as MaxPeriod
		FROM LedgerEX LEFT JOIN CFGPostControl ON LedgerEX.Period = CFGPostControl.Period AND LedgerEX.PostSeq = CFGPostControl.PostSeq
		WHERE ((LedgerEX.TransType = N'EX'  And (LedgerEX.SubType<>'X' or LedgerEx.Subtype is NULL)) OR
			(LedgerEX.TransType = N'EP'  And (LedgerEX.SubType<>'X' or LedgerEx.Subtype is NULL)))
			AND ISNULL(Desc2,N'') NOT LIKE N'Applied Advance Credit%'
			AND ISNULL(CFGPostControl.TransType, '') <> N'BE'
		GROUP BY LedgerEX.Employee, LedgerEX.Voucher
		HAVING Sum(CASE WHEN LedgerEX.TransType = N'EX' AND ISNULL(LedgerEX.SubType,'') <> 'A' THEN LedgerEX.AmountSourceCurrency When LedgerEX.TransType=N'EP' or ISNULL(LedgerEX.SubType,'') = 'A' Then -LedgerEX.AmountSourceCurrency Else 0 END)  BETWEEN -0.00005 AND 0.00005
	) L on EX.Employee=L.Employee and EX.Voucher=L.Voucher
END
GO
