SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AddIndustryDefaultsAEenUS]
AS
BEGIN

PRINT ' '
PRINT 'Adding AE Industry Defaults for en-US'

BEGIN TRANSACTION

DELETE FROM BTLaborCatsDescriptions WHERE UICultureName = 'en-US';
--..........................................................................................v(50)
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 1, 'en-US','Principal'         WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 1);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 2, 'en-US','Project Manager'   WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 2);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 3, 'en-US','Senior Consultant' WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 3);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 4, 'en-US','Senior Architect'  WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 4);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 5, 'en-US','Senior Engineer'   WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 5);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 6, 'en-US','Engineer'          WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 6);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 7, 'en-US','Architect'         WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 7);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 8, 'en-US','Designer'          WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 8);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 9, 'en-US','Draftsperson'      WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 9);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 10,'en-US','Surveyor'          WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 10);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 11,'en-US','Administrative'    WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 11);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 12,'en-US','Marketing'         WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 12);

DELETE FROM CFGActivitySubjectData WHERE UICultureName = 'en-US';
--..................................................................................v(50)
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Cold Call'               ,'en-US',3 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Initital Meeting'        ,'en-US',1 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Introduction Email'      ,'en-US',4 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Qualification Call'      ,'en-US',2 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Social Media Mention'    ,'en-US',6 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Thought Leadership Email','en-US',5 ,'N');

DELETE FROM CFGActivityTypeDescriptions WHERE UICultureName = 'en-US';
--.........................................................................................................v(15)
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'EMail'     ,'en-US','EMail'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'EMail');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Event'     ,'en-US','Event'          ,4 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Event');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Mailing'   ,'en-US','Mailing'        ,2 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Mailing');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Meeting'   ,'en-US','Meeting'        ,3 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Meeting');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Merge'     ,'en-US','Create Document',5 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Merge');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Milestone' ,'en-US','Milestone'      ,6 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Milestone');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Phone Call','en-US','Phone Call'     ,7 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Phone Call');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Task'      ,'en-US','Task'           ,8 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Task');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Touchpoint','en-US','Touchpoint'     ,9 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Touchpoint');

DELETE FROM CFGARLedgerHeadingsDescriptions WHERE UICultureName = 'en-US';
--...............................................................................................v(12)
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 1,'en-US','Fees'       WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 1);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 2,'en-US','Reimb.'     WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 2);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 3,'en-US','Consultant' WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 3);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 4,'en-US','Retainer'   WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 4);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 5,'en-US','Taxes'      WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 5);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 6,'en-US','Interest'   WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 6);

DELETE FROM CFGAwardTypeDescriptions WHERE UICultureName = 'en-US';
--............................................................................................v(255)
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '1','en-US','IDIQ - GWAC'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 1);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '2','en-US','IDIQ - Agency Specific',2 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 2);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '4','en-US','BPA'                   ,3 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 4);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '5','en-US','Other'                 ,4 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 5);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '6','en-US','Task / Delivery Order' ,5 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 6);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '7','en-US','Undetermined'          ,6 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 7);

DELETE FROM CFGBillMainDescriptions WHERE UICultureName = 'en-US';
--..............................................................................................................................................................................................................v(15)..v(15)....v(15).........v(15)......v(15)...v(15).....v(15)..v(15)
INSERT INTO CFGBillMainDescriptions (Company,UICultureName,FooterMessage,FeeLabel,LabLabel,ConLabel,ExpLabel,UnitLabel,AddOnLabel,TaxLabel,InterestLabel,OvtIndicator) SELECT CFGMainData.Company,'en-US',NULL,'Fee' ,'Labor' ,'Consultant' ,'Expense' ,'Unit' ,'Add-on' ,'Tax' ,'Interest' ,'Ovt' FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGBillMainData WHERE Company = CFGMainData.Company);

DELETE FROM CFGCampaignActionDescriptions WHERE UICultureName = 'en-US';
--..................................................................................................v(50)
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '01','en-US','Develop Content' ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '01');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '02','en-US','Send Invitation' ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '02');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '03','en-US','Send Email Blast',3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '03');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '04','en-US','Follow-Up'       ,4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '04');

DELETE FROM CFGCampaignAudienceDescriptions WHERE UICultureName = 'en-US';
--....................................................................................................v(50)
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '01','en-US','Executives'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '01');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '02','en-US','Finance'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '02');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '03','en-US','Marketing'           ,3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '03');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '04','en-US','Business Development',4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '04');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '05','en-US','Project Managers'    ,5 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '05');

DELETE FROM CFGCampaignObjectiveDescriptions WHERE UICultureName = 'en-US';
--.....................................................................................................v(50)
INSERT INTO CFGCampaignObjectiveDescriptions (Code,UICultureName,Description,Seq) SELECT '01','en-US','New Business Generation' ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignObjectiveData WHERE Code = '01');
INSERT INTO CFGCampaignObjectiveDescriptions (Code,UICultureName,Description,Seq) SELECT '02','en-US','Expand Existing Business',2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignObjectiveData WHERE Code = '02');

DELETE FROM CFGCampaignStatusDescriptions WHERE UICultureName = 'en-US';
--..................................................................................................v(50)
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','en-US','Active'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '01');
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','en-US','Planning' ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '02');
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','en-US','Completed',3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '03');

DELETE FROM CFGCampaignTypeDescriptions WHERE UICultureName = 'en-US';
--................................................................................................v(50)
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','en-US','Email'      ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '01');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','en-US','Direct Mail',2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '02');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','en-US','Webinar'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '03');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','en-US','Tradeshow'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '04');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '05','en-US','Meetup'     ,5 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '05');

DELETE FROM CFGClientCurrentStatusDescriptions WHERE UICultureName = 'en-US';
--...............................................................................................................v(50)
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Existing','en-US','Existing',1 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Existing');
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Former'  ,'en-US','Former'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Former');
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Prospect','en-US','Prospect',2 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Prospect');

DELETE FROM CFGClientRelationshipDescriptions WHERE UICultureName = 'en-US';
--......................................................................................................v(50)
INSERT INTO CFGClientRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01','en-US','Competitor',1 WHERE EXISTS (SELECT 'x' FROM CFGClientRelationshipData WHERE Code = '01');

DELETE FROM CFGClientRoleDescriptions WHERE UICultureName = 'en-US';
--....................................................................................................v(50)
INSERT INTO CFGClientRoleDescriptions (Code,UICultureName,Description,Seq) SELECT '01'      ,'en-US','Subcontractor',2 WHERE EXISTS (SELECT 'x' FROM CFGClientRoleData WHERE Code = '01');
INSERT INTO CFGClientRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'sysOwner','en-US','Client'       ,1 WHERE EXISTS (SELECT 'x' FROM CFGClientRoleData WHERE Code = 'sysOwner');

DELETE FROM CFGClientTypeDescriptions WHERE UICultureName = 'en-US';
--..............................................................................................v(50)
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','en-US','Education'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '01');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','en-US','Enterprise'          ,2 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '02');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','en-US','Finance and Banking' ,3 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '03');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','en-US','Health and Wellness' ,4 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '04');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '05','en-US','Hospitality'         ,5 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '05');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '06','en-US','Information Services',6 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '06');

DELETE FROM CFGCompetitionTypeDescriptions WHERE UICultureName = 'en-US';
--...................................................................................................v(255)
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '37','en-US','8(a) Set-Aside'                                       ,2  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '37')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '38','en-US','SDB Set-Aside'                                        ,3  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '38')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '39','en-US','Small Bus Set-Aside'                                  ,4  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '39')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '40','en-US','Full and Open / Unrestricted'                         ,5  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '40')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '44','en-US','Hubzone'                                              ,6  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '44')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '45','en-US','Service Disabled Veteran Owned Small Business'        ,7  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '45')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '47','en-US','Undetermined'                                         ,1  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '47')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '48','en-US','Other'                                                ,8  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '48')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '64','en-US','Sole Source'                                          ,9  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '64')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '65','en-US','Woman Owned Small Business Set Aside'                 ,10 WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '65')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '66','en-US','Veteran Owned Small Business'                         ,11 WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '66')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '67','en-US','Economically Disadvantaged Women Owned Small Business',12 WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '67')

DELETE FROM CFGContactRelationshipDescriptions WHERE UICultureName = 'en-US';
--.......................................................................................................v(50)
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01','en-US','Former Client'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '01');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '02','en-US','Former Coworker' ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '02');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '03','en-US','Industry Peer'   ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '03');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '04','en-US','Personal Contact',4 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '04');

DELETE FROM CFGContactRoleDescriptions WHERE UICultureName = 'en-US';
--.......................................................................................................v(50)
INSERT INTO CFGContactRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Contractor','en-US','Subcontractor',1 WHERE EXISTS (SELECT 'x' FROM CFGContactRoleData WHERE Code = 'Contractor');
INSERT INTO CFGContactRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'sysOwner'  ,'en-US','Owner'        ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactRoleData WHERE Code = 'sysOwner');

DELETE FROM CFGContactSourceDescriptions WHERE UICultureName = 'en-US';
--...................................................................................................v(50)
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '01','en-US','Client Reference'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '01');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '02','en-US','Event'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '02');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '03','en-US','List'              ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '03');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '04','en-US','Personal Contact'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '04');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '05','en-US','Social Media'      ,5 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '05');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '06','en-US','Thought Leadership',6 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '06');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '07','en-US','Website'           ,7 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '07');

DELETE FROM CFGContactTitleDescriptions WHERE UICultureName = 'en-US';
--.................................................................................................v(50)
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Director' ,'en-US','Director' ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Director');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Executive','en-US','Executive',1 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Executive');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Finance'  ,'en-US','Finance'  ,5 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Finance');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Marketing','en-US','Marketing',6 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Marketing');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Sales'    ,'en-US','Sales'    ,4 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Sales');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'VP'       ,'en-US','VP'       ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'VP');

DELETE FROM CFGContactTypeDescriptions WHERE UICultureName = 'en-US';
--..........................................................................................v(100)
INSERT INTO CFGContactTypeDescriptions (Code,UICultureName,Description) SELECT 'C','en-US','Client' WHERE EXISTS (SELECT 'x' FROM CFGContactTypeData WHERE Code = 'C');

DELETE FROM CFGContractStatusDescriptions WHERE UICultureName = 'en-US';
--..................................................................................................v(50)
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','en-US','Internal Review'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '01');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','en-US','Sent to Client'   ,2 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '02');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','en-US','Negotiation'      ,3 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '03');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '04','en-US','Legal Review'     ,4 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '04');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '05','en-US','Signed & Executed',5 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '05');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '06','en-US','Lost/No Deal'     ,6 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '06');

DELETE FROM CFGContractTypeDescriptions WHERE UICultureName = 'en-US';
--................................................................................................v(50)
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','en-US','Original'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '01');
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','en-US','Change Order'       ,2 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '02');
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','en-US','Additional Services',3 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '03');
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','en-US','Letter of Agreement',4 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '04');

DELETE FROM CFGCubeTranslationDescriptions WHERE UICultureName = 'en-US';
--..............................................................................................................................................v(200)
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'U'                       ,'en-US','Unknown');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'UM'                      ,'en-US','Unknown Month');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'H'                       ,'en-US','History');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('YesNo'          ,'Y'                       ,'en-US','Yes');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('YesNo'          ,'N'                       ,'en-US','No');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('Status'         ,'A'                       ,'en-US','Active');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('Status'         ,'I'                       ,'en-US','Inactive');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'1-Asset'                 ,'en-US','Asset');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'2-Liability'             ,'en-US','Liability');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'3-NetWorth'              ,'en-US','Net Worth');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'4-Revenue'               ,'en-US','Revenue');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'5-Reimbursable'          ,'en-US','Reimbursable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'6-ReimbursableConsultant','en-US','Reimbursable Consultant');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'7-Direct'                ,'en-US','Direct');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'8-DirectConsultant'      ,'en-US','Direct Consultant');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'9-Indirect'              ,'en-US','Indirect');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'10-OtherCharges'         ,'en-US','Other Charges');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'U-Unknown'               ,'en-US','Unknown');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ExpenseType'    ,'5-ReimbursableOther'     ,'en-US','Reimbursable Other');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ExpenseType'    ,'7-DirectOther'           ,'en-US','Direct Other');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ReportType'     ,'BalanceSheet'            ,'en-US','Balance Sheet');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ReportType'     ,'IncomeStatement'         ,'en-US','Income Statement');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'1-Asset','en-US'         ,'Other Assets');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'2-Liability'             ,'en-US','Other Liabilities');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'3-NetWorth'              ,'en-US','Other Net Worth');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'4-Revenue'               ,'en-US','Other Revenue');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'5-Reimbursable'          ,'en-US','Other Reimbursable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'6-Reimbursable'          ,'en-US','Other Reimbursable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'7-Direct'                ,'en-US','Other Direct');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'8-Direct'                ,'en-US','Other Direct');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'9-Indirect'              ,'en-US','Other Indirect');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'10-OtherCharges'         ,'en-US','Other Charges');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'B'                       ,'en-US','Billable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'H'                       ,'en-US','Held');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'W'                       ,'en-US','To Be Written Off');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'X'                       ,'en-US','Written Off');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'D'                       ,'en-US','Marked for Deletion');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'N'                       ,'en-US','Non-Billable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'F'                       ,'en-US','Final Billed');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'T'                       ,'en-US','Transferred');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'R'                       ,'en-US','Partially Held/Released');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'M'                       ,'en-US','Modified');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'O'                       ,'en-US','Deleted');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AP'                      ,'en-US','AP Vouchers');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BA'                      ,'en-US','Benefit Hours Processing');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BE'                      ,'en-US','Billing Expense Transfers');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BL'                      ,'en-US','Billing Labor Transfers');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BU'                      ,'en-US','Billing [Unit] Transfers');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CD'                      ,'en-US','Cash Disbursements');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CP'                      ,'en-US','Convert [WBS1] [Organization]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CR'                      ,'en-US','Cash Receipts');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CV'                      ,'en-US','AP Disbursements');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EP'                      ,'en-US','[Employee] Payment Processing');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','ER'                      ,'en-US','[Employee] Repayments');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EX'                      ,'en-US','[Employee] Expenses');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IN'                      ,'en-US','Invoices');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','JE'                      ,'en-US','Journal Entries');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','KC'                      ,'en-US','Key Conversion');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','LA'                      ,'en-US','Labor Adjustments');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','LG'                      ,'en-US','Gains/Losses and Revaluations');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','MI'                      ,'en-US','Miscellaneous Expenses');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PP'                      ,'en-US','AP Payment Processing');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PR'                      ,'en-US','Prints and Reproductions');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PY'                      ,'en-US','Payroll Processing');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','RG'                      ,'en-US','Revenue Generation');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','TS'                      ,'en-US','Timesheets');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','UN'                      ,'en-US','[Units]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','UP'                      ,'en-US','[Units] by [WBS1]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AL'                      ,'en-US','Key conversion posting, opening of a new period/W2 quarter/benefit accrual year, closing of a period');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AX'                      ,'en-US','Converted AP Voucher');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CN'                      ,'en-US','Converted Consultant Expense(s)');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CT'                      ,'en-US','Transaction indicating when the conversion from a previous release was performed');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EC'                      ,'en-US','Converted Time Analysis Adjustment(s)');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IH'                      ,'en-US','Converted Invoice');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IX'                      ,'en-US','Converted Invoice(s)');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','JX'                      ,'en-US','MCFMS Conversion');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PX'                      ,'en-US','Converted AP Payment Processing');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','RX'                      ,'en-US','MCFMS Conversion');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','XD'                      ,'en-US','Converted Account data');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','XE'                      ,'en-US','Converted [WBS1] Expenses');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HA'                      ,'en-US','[Account] Balances History');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HB'                      ,'en-US','[Employee] Benefit Accrual History');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HE'                      ,'en-US','[WBS1] Labor and Expense History');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HL'                      ,'en-US','[WBS1] Labor and Expense History');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HU'                      ,'en-US','Unbilled Revenue');

DELETE FROM CFGEMDegreeDescriptions WHERE UICultureName = 'en-US';
--............................................................................................v(50)
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','en-US','Bachelor of Science',1 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '01');
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','en-US','Bachelor of Arts'   ,2 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '02');
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','en-US','Masters'            ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '03');

DELETE FROM CFGEmployeeRelationshipDescriptions WHERE UICultureName = 'en-US';
--..............................................................................................................v(50)
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01'      ,'en-US','Former Employer'            ,3 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '01');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '02'      ,'en-US','Former Coworker'            ,2 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '02');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '03'      ,'en-US','Personal Contact'           ,5 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '03');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '04'      ,'en-US','General Industry Connection',4 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '04');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT 'SysOwner','en-US','Owner'                      ,1 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = 'SysOwner');

DELETE FROM CFGEmployeeRoleDescriptions WHERE UICultureName = 'en-US';
--........................................................................................................v(50)
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Admin'     ,'en-US','Administrative'         ,7  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Admin');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'AirQuality','en-US','Air Quality Specialist' ,8  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'AirQuality');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Architect' ,'en-US','Architect'              ,9  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Architect');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Associate' ,'en-US','Associate'              ,10 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Associate');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Biologist' ,'en-US','Biologist'              ,11 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Biologist');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'CADDTechi' ,'en-US','CADD Technician'        ,12 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'CADDTechi');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'CivilEngr' ,'en-US','Civil Engineer'         ,13 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'CivilEngr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ConstIns'  ,'en-US','Construction Inspector' ,14 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ConstIns');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ConstMgr'  ,'en-US','Construction Manager'   ,15 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ConstMgr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ElectEngr' ,'en-US','Electrical Engineer'    ,16 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ElectEngr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'EntitleSpl','en-US','Entitlement Specialist' ,17 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'EntitleSpl');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'EnvirPlnr' ,'en-US','Environmental Planner'  ,18 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'EnvirPlnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Estimator' ,'en-US','Estimator'              ,19 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Estimator');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'GISSpecial','en-US','GIS Specialist'         ,20 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'GISSpecial');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Hydrolgst' ,'en-US','Hydrologist'            ,21 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Hydrolgst');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'InfoTech'  ,'en-US','Information Technology' ,22 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'InfoTech');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'InteDsnr'  ,'en-US','Interior Designer'      ,23 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'InteDsnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'LandArch'  ,'en-US','Landscape Architect'    ,24 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'LandArch');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'MechEng'   ,'en-US','Mechanical Engineer'    ,25 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'MechEng');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'MEPCoordin','en-US','MEP Coordinator'        ,26 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'MEPCoordin');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Planner'   ,'en-US','Planner'                ,27 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Planner');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjAdmin' ,'en-US','Project Administrator'  ,28 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjAdmin');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjArch'  ,'en-US','Project Architect'      ,29 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjArch');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjDsnr'  ,'en-US','Project Designer'       ,30 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjDsnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjEng'   ,'en-US','Project Engineer'       ,31 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjEng');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjPlnr'  ,'en-US','Project Planner'        ,32 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjPlnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjSvor'  ,'en-US','Project Surveyor'       ,33 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjSvor');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'QAQCMgr'   ,'en-US','QA/QC Manager'          ,34 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'QAQCMgr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'SpecWriter','en-US','Specifications Writer'  ,35 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'SpecWriter');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'StructEng' ,'en-US','Structural Engineer'    ,36 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'StructEng');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Surveyor'  ,'en-US','Surveyor'               ,37 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Surveyor');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TechEditor','en-US','Technical Editor'       ,38 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TechEditor');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TrafficSpe','en-US','Traffic Specialist'     ,39 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TrafficSpe');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TransEngr' ,'en-US','Transportation Engineer',40 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TransEngr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TransPlnr' ,'en-US','Transportation Planner' ,41 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TransPlnr');
--CFGEmployeeRoles view uses descriptions from FW_CFGLabelData for Sys roles

DELETE FROM CFGEmployeeTitleDescriptions WHERE UICultureName = 'en-US';
--..................................................................................................v(50)
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Architect','en-US','Architect'              ,1  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Architect');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CFO'      ,'en-US','Chief Financial Officer',2  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CFO');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CivilEng' ,'en-US','Civil Engineer'         ,3  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CivilEng');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Designer' ,'en-US','Designer'               ,4  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Designer');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Director' ,'en-US','Director'               ,5  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Director');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'EE'       ,'en-US','Environmental Engineer' ,8  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'EE');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'EIT'      ,'en-US','Engineer-in-Training'   ,7  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'EIT');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Engineer' ,'en-US','Engineer'               ,6  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Engineer');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'GE'       ,'en-US','Geotechnical Engineer'  ,10 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'GE');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'GM'       ,'en-US','General Manager'        ,9  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'GM');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'ID'       ,'en-US','Interior Designer'      ,12 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'ID');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'LA'       ,'en-US','Landscape Architect'    ,13 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'LA');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'PIC'      ,'en-US','Principal-in-Charge'    ,17 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'PIC');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Planner'  ,'en-US','Planner'                ,14 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Planner');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'PM'       ,'en-US','Project Manager'        ,18 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'PM');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'President','en-US','President'              ,15 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'President');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Principal','en-US','Principal'              ,16 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Principal');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'SUP'      ,'en-US','Supervisior'            ,20 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'SUP');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'TC'       ,'en-US','Training Consultant'    ,19 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'TC');

DELETE FROM CFGEmployeeTypeDescriptions WHERE UICultureName = 'en-US';
--.....................................................................................v(30)
INSERT INTO CFGEmployeeTypeDescriptions (Type,UICultureName,Label) SELECT 'E','en-US','Employee'  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTypeData WHERE Type = 'E');
INSERT INTO CFGEmployeeTypeDescriptions (Type,UICultureName,Label) SELECT 'P','en-US','Principal' WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTypeData WHERE Type = 'P');

DELETE FROM CFGEMRegistrationDescriptions WHERE UICultureName = 'en-US';
--...................................................................................................v(70)
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'AIA','en-US','American Institute of Architects',2 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'AIA');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'EIT','en-US','Engineer In Training'            ,5 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'EIT');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'PE' ,'en-US','Professional Engineer'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'PE');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'PLS','en-US','Professional Land Surveyor'      ,4 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'PLS');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'RLS','en-US','Registered Land Surveyor'        ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'RLS');

DELETE FROM CFGEMSkillDescriptions WHERE UICultureName = 'en-US';
--.............................................................................................v(70)
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '01'  ,'en-US','Administrative'                          ,1  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '01SF','en-US','Acoustical Engineer'                     ,2  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '02'  ,'en-US','Architects'                              ,3  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '02');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '03SF','en-US','Aerial Photographer'                     ,4  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '03SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '04SF','en-US','Aeronautical Engineer'                   ,5  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '04SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '05'  ,'en-US','Draftsmen'                               ,6  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '05');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '05SF','en-US','Archeologist'                            ,7  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '05SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '07'  ,'en-US','Construction Inspectors'                 ,8  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '07');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '07SF','en-US','Biologist'                               ,9  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '07SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '08'  ,'en-US','Estimators'                              ,10 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '08');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '08SF','en-US','CADD Technician'                         ,11 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '08SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '09SF','en-US','Cartographer'                            ,12 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '09SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '10'  ,'en-US','Sanitary Engineers'                      ,13 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '10');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '11'  ,'en-US','Economists'                              ,14 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '11');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '11SF','en-US','Chemist'                                 ,15 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '11SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '12'  ,'en-US','Ecologists'                              ,16 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '12');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '13SF','en-US','Communications Engineer'                 ,17 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '13SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '14'  ,'en-US','Chemical Engineers'                      ,18 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '14');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '14SF','en-US','Computer Programmer'                     ,19 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '14SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '15'  ,'en-US','Civil Engineers'                         ,20 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '15');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '16SF','en-US','Construction Manager'                    ,21 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '16SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '17SF','en-US','Corrosion Engineer'                      ,22 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '17SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '18SF','en-US','Cost Engineer/Estimator'                 ,23 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '18SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '19'  ,'en-US','Electrical Engineers'                    ,24 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '19');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '21'  ,'en-US','Surveyors'                               ,25 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '21');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '22SF','en-US','Electronics Engineer'                    ,26 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '22SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '23SF','en-US','Environmental Engineer'                  ,27 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '23SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '24SF','en-US','Environmental Scientist'                 ,28 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '24SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '25SF','en-US','Fire Protection Engineer'                ,29 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '25SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '26SF','en-US','Forensic Engineer'                       ,30 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '26SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '27'  ,'en-US','Mechanical Engineers'                    ,31 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '27');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '27SF','en-US','Foundation/Geotechnical Engineer'        ,32 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '27SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '28'  ,'en-US','Mining Engineers'                        ,33 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '28');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '28SF','en-US','Geodetic Surveyor'                       ,34 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '28SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '29SF','en-US','Geographic Information System Specialist',35 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '29SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '30'  ,'en-US','Soils Engineers'                         ,36 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '30');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '31'  ,'en-US','Specifications Writers'                  ,37 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '31');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '31SF','en-US','Health Facility Planner'                 ,38 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '31SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '32'  ,'en-US','Structural Engineers'                    ,39 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '32');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '32SF','en-US','Hydraulic Engineer'                      ,40 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '32SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '33'  ,'en-US','Transportation Engineers'                ,41 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '33SF','en-US','Hydrographic Surveyor'                   ,42 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '35SF','en-US','Industrial Engineer'                     ,43 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '36'  ,'en-US','Geologist'                               ,44 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '36SF','en-US','Industrial Hygienist'                    ,45 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '38SF','en-US','Land Surveyor'                           ,46 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '40'  ,'en-US','Hydrologists'                            ,47 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '40SF','en-US','Materials Engineer'                      ,48 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '41SF','en-US','Materials Handling Engineer'             ,49 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '42'  ,'en-US','Interior Designers'                      ,50 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '43'  ,'en-US','Landscape Architects'                    ,51 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '44'  ,'en-US','Oceanographers'                          ,52 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '45'  ,'en-US','Planners: Urban/Regional'                ,53 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '45SF','en-US','Photo Interpreter'                       ,54 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '46SF','en-US','Photogrammetrist'                        ,55 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '48SF','en-US','Project Manager'                         ,56 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '49SF','en-US','Remote Sensing Specialist'               ,57 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '50SF','en-US','Risk Assessor'                           ,58 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '51SF','en-US','Safety/Occupational Health Engineer'     ,59 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '53SF','en-US','Scheduler'                               ,60 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '54SF','en-US','Security Specialist'                     ,61 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '58SF','en-US','Technician/Analyst'                      ,62 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '59SF','en-US','Toxicologist'                            ,63 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '61SF','en-US','Value Engineer'                          ,64 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '62SF','en-US','Water Resources Engineer'                ,65 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');

DELETE FROM CFGEMSkillLevelDescriptions WHERE UICultureName = 'en-US';
--.............................................................................................v(50)
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 1,'en-US','Basic'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 1);
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 2,'en-US','Advanced',2 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 2);
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 3,'en-US','Expert'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 3);

DELETE FROM CFGEMSkillUsageDescriptions WHERE UICultureName = 'en-US';
--.............................................................................................v(255)
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 1,'en-US','Entry'        ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 1);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 2,'en-US','1-2 years'    ,2 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 2);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 3,'en-US','3-5 years'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 3);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 4,'en-US','6-10 years'   ,4 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 4);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 5,'en-US','Over 10 years',5 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 5);

DELETE FROM CFGLeadRatingDescriptions WHERE UICultureName = 'en-US';
--..............................................................................................v(50)
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '01','en-US','Hot' ,1 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '01');
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '02','en-US','Warm',2 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '02');
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '03','en-US','Cold',3 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '03');

DELETE FROM CFGOpportunityClosedReasonDescriptions WHERE UICultureName = 'en-US';
--...........................................................................................................v(50)
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '01','en-US','Price'    ,1 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '01');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '02','en-US','Services' ,2 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '02');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '03','en-US','Resources',3 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '03');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '04','en-US','Timing'   ,4 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '04');

DELETE FROM CFGOpportunitySourceDescriptions WHERE UICultureName = 'en-US';
--...........................................................................................................v(50)
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'CRef'    ,'en-US','Client Reference'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'CRef');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Event'   ,'en-US','Event'                     ,2 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Event');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'List'    ,'en-US','List'                      ,5 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'List');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'PersCont','en-US','Personal Contact'          ,6 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'PersCont');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Social'  ,'en-US','Social Media'              ,7 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Social');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'TLead'   ,'en-US','Thought Leadership'        ,8 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'TLead');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Web'     ,'en-US','Web'                       ,9 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Web');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'WIQ'     ,'en-US','GovWin IQ'                 ,3 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'WIQ');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'WOM'     ,'en-US','GovWin Opportunity Manager',4 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'WOM');

DELETE FROM CFGPrefixDescriptions WHERE UICultureName = 'en-US';
--........................................................................................v(10)
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Dr.'  ,'en-US','Dr.'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Dr.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Miss' ,'en-US','Miss' ,2 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Miss');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Mr.'  ,'en-US','Mr.'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Mr.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Mrs.' ,'en-US','Mrs.' ,4 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Mrs.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Ms.'  ,'en-US','Ms.'  ,5 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Ms.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Prof.','en-US','Prof.',6 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Prof.');

DELETE FROM CFGProbabilityDescriptions WHERE UICultureName = 'en-US';
--.....................................................................................................v(50)
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 5  ,'en-US','05' ,1  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 5);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 10 ,'en-US','10' ,2  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 10);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 20 ,'en-US','20' ,3  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 20);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 30 ,'en-US','30' ,4  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 30);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 40 ,'en-US','40' ,5  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 40);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 50 ,'en-US','50' ,6  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 50);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 60 ,'en-US','60' ,7  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 60);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 70 ,'en-US','70' ,8  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 70);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 80 ,'en-US','80' ,9  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 80);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 90 ,'en-US','90' ,10 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 90);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 95 ,'en-US','95' ,11 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 95);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 100,'en-US','100',12 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 100);

DELETE FROM CFGProjectCodeDescriptions WHERE UICultureName = 'en-US';
--.......................................................................................................v(100)
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '001','en-US','Acoustics; Noise Abatement'                                                                    ,1   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '001');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '002','en-US','Aerial Photography; Airborne Data and Imagery Collection and Analysis'                         ,2   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '002');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '003','en-US','Agricultural Development; Grain Storage; Farm Mechanization'                                   ,3   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '003');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '004','en-US','Air Pollution Control'                                                                         ,4   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '004');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '005','en-US','Airports; Navaids; Airport Lighting; Aircraft Fueling'                                         ,5   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '005');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '006','en-US','Airports; Terminals; & Hangers; Frieght Handling'                                              ,6   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '006');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '007','en-US','Arctic Facilities'                                                                             ,7   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '007');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '008','en-US','Auditoriums & Theatres'                                                                        ,8   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '008');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '009','en-US','Automation; Controls; Instrumentation'                                                         ,9   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '009');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '010','en-US','Barracks; Dormitories'                                                                         ,10  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '010');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '011','en-US','Bridges'                                                                                       ,11  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '011');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '012','en-US','Cemeteries (Planning & Relocation)'                                                            ,12  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '012');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '013','en-US','Chemical Processing & Storage'                                                                 ,13  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '013');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '014','en-US','Churches; Chapels'                                                                             ,14  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '014');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '015','en-US','Codes; Standards; Ordinances'                                                                  ,15  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '015');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '016','en-US','Cold Storage; Refrigeration; Fast Freeze'                                                      ,16  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '016');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '017','en-US','Commercial Building; (low rise); Shopping Centers'                                             ,17  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '017');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '018','en-US','Communications Systems; TV; Microwave'                                                         ,18  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '018');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '019','en-US','Computer Facilities; Computer Service'                                                         ,19  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '019');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '020','en-US','Conservation and Resource Management'                                                          ,20  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '020');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '021','en-US','Construction Management'                                                                       ,21  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '021');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '022','en-US','Corrosion Control; Cathodic Protection; Electrolysis'                                          ,22  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '022')
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '023','en-US','Cost Estimating; Cost Engineering and Analysis; Parametric Costing; Forecasting'               ,23  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '023');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '024','en-US','Dams (Concrete; Arch)'                                                                         ,24  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '024');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '025','en-US','Dams (Earth; Rock); Dikes; Levees'                                                             ,25  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '025');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '026','en-US','Desalinization (Process & Facilities)'                                                         ,26  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '026');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '027','en-US','Dining Halls; Clubs; Restaurants'                                                              ,27  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '027');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '028','en-US','Ecological & Archeological Investigations'                                                     ,28  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '028');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '029','en-US','Educational Facilities; Classrooms'                                                            ,29  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '029');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '030','en-US','Electronics'                                                                                   ,30  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '030');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '031','en-US','Elevators; Escalators; People-Movers'                                                          ,31  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '031');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '032','en-US','Energy Conservation; New Energy Sources'                                                       ,32  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '032');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '033','en-US','Environmental Impact Studies, Assessments or Statements'                                       ,33  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '033');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '034','en-US','Fallout Shelters; Blast-Resistant Design'                                                      ,34  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '034');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '035','en-US','Field Houses; Gymnasiums; Stadiums'                                                            ,35  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '035');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '036','en-US','Fire Protection'                                                                               ,36  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '036');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '037','en-US','Fisheries; Fish Ladders'                                                                       ,37  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '037');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '038','en-US','Forestry & Forest Products'                                                                    ,38  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '038');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '039','en-US','Garages; Vehicle Maintenanace Facilities; Parking Decks'                                       ,39  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '039');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '040','en-US','Gas Systems (Propane, Natural, etc.)'                                                          ,40  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '040');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '041','en-US','Graphic Design'                                                                                ,41  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '041');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '042','en-US','Harbors; jetties; Piers; Ship Terminal Facilities'                                             ,42  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '042');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '043','en-US','Heating, Ventilating, Air Conditioning'                                                        ,43  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '043');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '044','en-US','Health Systems Planning'                                                                       ,44  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '044');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '045','en-US','Highrise; Air-Rights-Type Buildings'                                                           ,45  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '045');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '046','en-US','Highways; Streets; Airfield Paving; Parking Lots'                                              ,46  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '046');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '047','en-US','Historical Preservation'                                                                       ,47  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '047');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '048','en-US','Hospitals & Medical Facilities'                                                                ,48  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '048');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '049','en-US','Hotels; Motels'                                                                                ,49  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '049');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '050','en-US','Housing (Residential, Multifamily, Apartments, Condominiums'                                   ,50  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '050');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '051','en-US','Hydraulics & Pneumatics'                                                                       ,51  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '051');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '052','en-US','Industrial Buildings; Manufacturing Plants'                                                    ,52  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '052');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '053','en-US','Industrial Processes; Quality Control'                                                         ,53  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '053');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '054','en-US','Industial Waste Treatment'                                                                     ,54  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '054');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '055','en-US','Interior Design; Space Planning'                                                               ,55  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '055');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '056','en-US','Irrigation; Drainage'                                                                          ,56  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '056');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '057','en-US','Judicial and Courtroom Facilities'                                                             ,57  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '057');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '058','en-US','Laboratories; Medical Research Facilites'                                                      ,58  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '058');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '059','en-US','Landscape Architecture'                                                                        ,59  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '059');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '060','en-US','Libraries; Museums; Galleries'                                                                 ,60  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '060');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '061','en-US','Lighting (Interior; Displays; Theatres; etc.)'                                                 ,61  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '061');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '062','en-US','Lighting (Exteriors; Street; Memorials; Athletic Fields)'                                      ,62  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '062');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '063','en-US','Materials Handling Systems; Conveyors; Sorters'                                                ,63  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '063');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '064','en-US','Metallurgy'                                                                                    ,64  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '064');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '065','en-US','Microclimatology; Tropical Engineering'                                                        ,65  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '065');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '066','en-US','Military Design Standards'                                                                     ,66  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '066');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '067','en-US','Mining and Mineralogy'                                                                         ,67  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '067');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '068','en-US','Missile Facilities (Silos; Fuels; Transport)'                                                  ,68  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '068');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '069','en-US','Modular Systems Design; Pre-fab Structures or Components'                                      ,69  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '069');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '070','en-US','Naval Architecture; Off-Shore Platforms'                                                       ,70  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '070');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '071','en-US','Nuclear Facilities; Nuclear Shielding'                                                         ,71  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '071');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '072','en-US','Office Building; Industrial Parks'                                                             ,72  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '072');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '073','en-US','Oceanographic Engineering'                                                                     ,73  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '073');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '074','en-US','Ordinanaces; Munitions; Special Weapons'                                                       ,74  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '074');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '075','en-US','Petroleum Exploration; Refining'                                                               ,75  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '075');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '076','en-US','Petroleum and Fuel (Storage and Distribution)'                                                 ,76  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '076');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '077','en-US','Pipelines (Cross-country--Liquid & Gas)'                                                       ,77  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '077');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '078','en-US','Planning (Community; Regional; Areawide & State)'                                              ,78  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '078');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '079','en-US','Planning (Site, Installation and Project)'                                                     ,79  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '079');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '080','en-US','Plumbing & Pipe Design'                                                                        ,80  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '080');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '081','en-US','Pneumatic Structures; Air Support Buildings'                                                   ,81  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '081');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '082','en-US','Postal Facilities'                                                                             ,82  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '082');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '083','en-US','Power Generation, Transmission, Distribution'                                                  ,83  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '083');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '084','en-US','Prisons & Correctional Facilities'                                                             ,84  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '084');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '085','en-US','Product, Machine & Equipment Design'                                                           ,85  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '085');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '086','en-US','Radar; Sonar; Radio & Radar Telescopes'                                                        ,86  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '086');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '087','en-US','Railroad and Rapid Transit'                                                                    ,87  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '087');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '088','en-US','Recreational Facilities (Parks; Marinas; ect.)'                                                ,88  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '088');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '089','en-US','Rehabilitation (Buildings; Structures; Facilities)'                                            ,89  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '089');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '090','en-US','Resources Recovery; Recycling'                                                                 ,90  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '090');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '091','en-US','Radio Frequency Systems & Shieldings'                                                          ,91  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '091');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '092','en-US','Rivers Canals; Waterways; Flood Control'                                                       ,92  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '092');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '093','en-US','Safety Engineering; Accident Studies; OSHA Studies'                                            ,93  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '093');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '094','en-US','Security Systems; Intruder & Smoke Detection'                                                  ,94  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '094');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '095','en-US','Seismic Design & Studies'                                                                      ,95  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '095');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '096','en-US','Sewage Collection, Treatment & Disposal'                                                       ,96  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '096');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '097','en-US','Soils & Geologic Studies; Foundations'                                                         ,97  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '097');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '098','en-US','Solar Energy Utilization'                                                                      ,98  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '098');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '099','en-US','Solid Wastes; Incineration; Landfill'                                                          ,99  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '099');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '100','en-US','Special Environments; Clean Rooms; Etc.'                                                       ,100 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '100');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '101','en-US','Structural Design; Special Structures'                                                         ,101 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '101');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '102','en-US','Surveying; Platting; Mapping; Flood Plain Studies'                                             ,102 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '102');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '103','en-US','Swimming Pools'                                                                                ,103 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '103');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '104','en-US','Stormwater Handling & Facilities'                                                              ,104 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '104');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '105','en-US','Telephone Systems (Rural; Mobile; Intercom; etc.)'                                             ,105 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '105');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '106','en-US','Testing & Inspection Services'                                                                 ,106 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '106');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '107','en-US','Traffic & Transportation Engineering'                                                          ,107 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '107');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '108','en-US','Towers (Self-supporting & Guyed Systems)'                                                      ,108 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '108');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '109','en-US','Tunnels & Subways'                                                                             ,109 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '109');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '110','en-US','Urban Renewals; Community Development'                                                         ,110 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '110');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '111','en-US','Utilities (Gas and Steam)'                                                                     ,111 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '111');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '112','en-US','Value Analysis; Life-Cycle Costing'                                                            ,112 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '112');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '113','en-US','Warehouses & Depots'                                                                           ,113 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '113');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '114','en-US','Water Resources; Hydrology; Ground Water'                                                      ,114 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '114');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '115','en-US','Water Supply; Treatment and Distribution'                                                      ,115 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '115');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '116','en-US','Wind Tunnels; Research/Testing Facilities Design'                                              ,116 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '116');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '117','en-US','Zoning; Land Use Studies'                                                                      ,117 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '117');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'A08','en-US','Animal Facilities'                                                                             ,118 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'A08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'A09','en-US','Anti-Terrorism/Force Protection'                                                               ,119 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'A09');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'A10','en-US','Asbestos Abatement'                                                                            ,120 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'A10');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C01','en-US','Cartography'                                                                                   ,121 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C01');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C03','en-US','Charting: Nautical and Aeronautical'                                                           ,122 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C05','en-US','Child Care/Development Facilities'                                                             ,123 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C07','en-US','Coastal Engineering'                                                                           ,124 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C07');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C11','en-US','Community Facilities'                                                                          ,125 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C11');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C16','en-US','Construction Surveying'                                                                        ,126 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C16');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C19','en-US','Cryogenic Facilities'                                                                          ,127 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C19');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D04','en-US','Design-Build - Preparation of Requests for Proposals'                                          ,128 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D05','en-US','Digital Elevation and Terrain Model Development'                                               ,129 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D06','en-US','Digital Orthophotography'                                                                      ,130 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D06');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D08','en-US','Dredging Studies and Design'                                                                   ,131 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E03','en-US','Electrical Studies and Design'                                                                 ,132 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E06','en-US','Embassies and Chanceries'                                                                      ,133 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E06');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E08','en-US','Engineering Economics'                                                                         ,134 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E10','en-US','Environmental and Natural Resource Mapping'                                                    ,135 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E10');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E11','en-US','Environmental Planning'                                                                        ,136 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E11');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E12','en-US','Environmental Remediation'                                                                     ,137 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E12');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E13','en-US','Environmental Testing and Analysis'                                                            ,138 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E13');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'F05','en-US','Forensic Engineering'                                                                          ,139 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'F05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'G03','en-US','Geodetic Surveying: Ground and Airborne'                                                       ,140 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'G03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'G04','en-US','Geographic Information System Services: Development, Analysis, and Data Collection'            ,141 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'G04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'G05','en-US','Geospatial Data Conversion: Scanning, Digitizing, Compilation, Attributing, Scribing, Drafting',142 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'G05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'H02','en-US','Hazardous Materials Handling and Storage'                                                      ,143 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'H02');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'H03','en-US','Hazardous, Toxic, Radioactive Waste Remediation'                                               ,144 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'H03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'H13','en-US','Hydrographic Surveying'                                                                        ,145 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'H13');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'I04','en-US','Intelligent Transportation Systems'                                                            ,146 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'I04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'L02','en-US','Land Surveying'                                                                                ,147 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'L02');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'M01','en-US','Mapping Location/Addressing Systems'                                                           ,148 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'M01');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'N02','en-US','Navigation Structures; Locks'                                                                  ,149 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'N02');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'P03','en-US','Photogrammetry'                                                                                ,150 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'P03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'P13','en-US','Public Safety Facilities'                                                                      ,151 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'P13');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R05','en-US','Refrigeration Plants/Systems'                                                                  ,152 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R07','en-US','Remote Sensing'                                                                                ,153 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R07');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R08','en-US','Research Facilities'                                                                           ,154 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R10','en-US','Risk Analysis'                                                                                 ,155 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R10');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R12','en-US','Roofing'                                                                                       ,156 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R12');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'S11','en-US','Sustainable Design'                                                                            ,157 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'S11');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'T04','en-US','Topographic Surveying and Mapping'                                                             ,158 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'T04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'U01','en-US','Unexploded Ordnance Remediation'                                                               ,159 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'U01');

DELETE FROM CFGProjectMilestoneDescriptions WHERE UICultureName = 'en-US';
--...........................................................................................................................v(255)
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysEstStart',           'en-US','Estimated Start Date',                   1 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysEstStart')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysEstCompletion',      'en-US','Estimated Completion Date',              2 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysEstCompletion')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysContract',           'en-US','Contract Awarded Date',                  3 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysContract')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysBidSubmitted',       'en-US','Bid Submitted Date',                     4 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysBidSubmitted')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysStart',              'en-US','Actual Start Date',                      5 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysStart')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysActualCompl',        'en-US','Actual Completion Date',                 6 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysActualCompl')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysProfServicesCompl',  'en-US','Professional Services Completion Date',  7 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysProfServicesCompl')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysConstCompl',         'en-US','Construction Completion Date',           8 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysConstCompl')


DELETE FROM CFGProposalSourceDescriptions WHERE UICultureName = 'en-US';
--..................................................................................................v(50)
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '01','en-US','Client Reference'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '01');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '02','en-US','Event'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '02');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '03','en-US','List'              ,3 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '03');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '04','en-US','Personal Contact'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '04');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '05','en-US','Social Media'      ,5 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '05');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '06','en-US','Thought Leadership',6 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '06');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '07','en-US','Website'           ,7 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '07');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '08','en-US','GovWinIQ'          ,8 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '08');

DELETE FROM CFGProposalStatusDescriptions WHERE UICultureName = 'en-US';
--..................................................................................................v(50)
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','en-US','In Progress',1 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '01');
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','en-US','In Review'  ,2 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '02');
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','en-US','Submitted'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '03');

DELETE FROM CFGPRResponsibilityDescriptions WHERE UICultureName = 'en-US';
--....................................................................................................v(80)
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'C' ,'en-US','Consultant'           ,3 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'C');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'IE','en-US','Individual Experience',4 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'IE');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'JV','en-US','Joint Venture'        ,2 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'JV');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'P' ,'en-US','Prime'                ,1 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'P');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'S' ,'en-US','Subcontractor'        ,5 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'S');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'U' ,'en-US','Undetermined'         ,6 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'U');

DELETE FROM CFGPYAccrualsDescriptions WHERE UICultureName = 'en-US';
--............................................................................................................................v(40)
INSERT INTO CFGPYAccrualsDescriptions (Code,Company,UICultureName,Description) SELECT 'Sick Lv.',CFGMainData.Company,'en-US','Sick Leave' FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGPYAccrualsData WHERE Code = 'Sick Lv.' AND Company = CFGMainData.Company)
INSERT INTO CFGPYAccrualsDescriptions (Code,Company,UICultureName,Description) SELECT 'Vacation',CFGMainData.Company,'en-US','Vacation'   FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGPYAccrualsData WHERE Code = 'Vacation' AND Company = CFGMainData.Company)

DELETE FROM CFGRGMethodsDescriptions WHERE UICultureName = 'en-US';
--..........................................................................................v(40)
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'B','en-US','JTD Billings'                   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'B');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'M','en-US','(JTD DL*Mult) + JTD Reimb Exp'  WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'M');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'N','en-US','No Revenue Recognition'         WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'N');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'P','en-US','(Pct Comp*Fee) + JTD Reimb Exp' WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'P');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'R','en-US','JTD Receipts'                   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'R');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'W','en-US','JTD Billings + WIP @ Billing'   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'W');

DELETE FROM CFGServiceProfileDescriptions WHERE UICultureName = 'en-US';
--..................................................................................................................v(40)
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.18' ,'en-US','Land utilization/development plan','Area in hectares'               WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.18');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.19' ,'en-US','Land-use/building plan','Area in hectares'                          WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.19');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.23' ,'en-US','Landscape plan','Area in hectares'                                  WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.23');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.24' ,'en-US','Open/green space plan','Area in hectares'                           WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.24');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.25' ,'en-US','Landscape structure plan','Area in hectares'                        WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.25');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.26' ,'en-US','Landscape conservation plan','Area in hectares'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.26');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.27' ,'en-US','Maintenance and development plan','Area in hectares'                WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.27');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.34' ,'en-US','Buildings and interiors','Chargeable costs in Euro'                 WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.34');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.39' ,'en-US','Open air facilities','Chargeable costs in Euro'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.39');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.43' ,'en-US','Civil engineering structures','Chargeable costs in Euro'            WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.43');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.47' ,'en-US','Transportation facilities','Chargeable costs in Euro'               WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.47');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.51' ,'en-US','Structural planning','Chargeable costs in Euro'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.51');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.55' ,'en-US','Technical equipment','Chargeable costs in Euro'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.55');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A01','en-US','Environmental impact studies','Area in hectares'                    WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A01');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A02','en-US','Thermal insulation and energy balancing','Chargeable costs in Euro' WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A02');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A03','en-US','Building acoustics','Chargeable costs in Euro'                      WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A03');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A04','en-US','Room acoustics','Chargeable costs in Euro'                          WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A04');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A05','en-US','Geotechnics','Chargeable costs in Euro'                             WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A05');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A06','en-US','Measuring/surveying operations','Accounting units'                  WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A06');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A07','en-US','Engineering surveying','Chargeable costs in Euro'                   WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A07');

DELETE FROM CFGSuffixDescriptions WHERE UICultureName = 'en-US';
--......................................................................................v(150)
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'I'  ,'en-US','I'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'I');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'II' ,'en-US','II' ,4 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'II');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'III','en-US','III',5 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'III');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'Jr.','en-US','Jr.',1 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'Jr.');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'Sr.','en-US','Sr.',2 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'Sr.');

DELETE FROM CFGUnitTypeDescriptions WHERE UICultureName = 'en-US';
--...................................................................................................v(50)
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Equipment','en-US','Equipment',1 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Equipment');
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Expenses ','en-US','Expenses' ,2 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Expenses');
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Labor'    ,'en-US','Labor'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Labor');

DELETE FROM CFGVendorTypeDescriptions WHERE UICultureName = 'en-US';
--...................................................................................v(30)
INSERT INTO CFGVendorTypeDescriptions (Type,UICultureName,Label) SELECT 'C','en-US','Consultant' WHERE EXISTS (SELECT 'x' FROM CFGVendorTypeData WHERE Type = 'C');
INSERT INTO CFGVendorTypeDescriptions (Type,UICultureName,Label) SELECT 'T','en-US','Trade'      WHERE EXISTS (SELECT 'x' FROM CFGVendorTypeData WHERE Type = 'T');

DELETE FROM FW_CFGAttachmentCategoryDesc WHERE UICultureName = 'en-US';
--................................................................................................................................................v(255)
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Notes'          ,'Activities'         ,'en-US','Notes'                   ,1  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Notes' and Application ='Activities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'NDA'            ,'Contacts'           ,'en-US','Non-Disclosure Agreement',2  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'NDA' and Application ='Contacts');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Notes'          ,'Contacts'           ,'en-US','Notes'                   ,3  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Notes' and Application ='Contacts');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'CompanyOverview','TextLibrary'        ,'en-US','Company Overview'        ,4  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'CompanyOverview' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Case Study'     ,'TextLibrary'        ,'en-US','Case Study'              ,5  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Case Study' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Reference'      ,'TextLibrary'        ,'en-US','Reference'               ,6  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Reference' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Resume'         ,'Employees'          ,'en-US','Resume'                  ,7  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Resume' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Award'          ,'Employees'          ,'en-US','Award'                   ,8  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Award' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Leadership'     ,'Employees'          ,'en-US','Thought Leadership'      ,9  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Leadership' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Firms'              ,'en-US','Master Service Agreement',10 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Firms'              ,'en-US','Statement of Work'       ,11 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'NDA'            ,'Firms'              ,'en-US','Non-Disclosure Agreement',12 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'NDA' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'ClientACH'      ,'Firms'              ,'en-US','Client ACH'              ,13 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'ClientACH' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'VendorACH'      ,'Firms'              ,'en-US','Vendor ACH'              ,14 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'VendorACH' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'DigitalArtifact','Marketing Campaigns','en-US','Digital Artifact'        ,15 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'DigitalArtifact' and Application ='Marketing Campaigns');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Estimate'       ,'Opportunities'      ,'en-US','Estimate'                ,16 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Estimate' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Proposal'       ,'Opportunities'      ,'en-US','Proposal'                ,17 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Proposal' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Opportunities'      ,'en-US','Statement of Work'       ,18 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Opportunities'      ,'en-US','Master Service Agreement',19 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Estimate'       ,'Projects'           ,'en-US','Estimate'                ,20 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Estimate' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Proposal'       ,'Projects'           ,'en-US','Proposal'                ,21 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Proposal' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Projects'           ,'en-US','Statement of Work'       ,22 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Projects'           ,'en-US','Master Service Agreement',23 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Projects');

DELETE FROM FW_CFGLabelData WHERE UICultureName = 'en-US';
--............................................................................................................................................................................v(100)
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','accountLabel'                            ,'Account'                       ,'[Account]'                    ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','accountLabelPlural'                      ,'Accounts'                      ,'[Accounts]'                   ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','clientLabel'                             ,'Client'                        ,'[Client]'                     ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','clientLabelPlural'                       ,'Clients'                       ,'[Clients]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','contactLabel'                            ,'Contact'                       ,'[Contact]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','contactLabelPlural'                      ,'Contacts'                      ,'[Contacts]'                   ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','employeeLabel'                           ,'Employee'                      ,'[Employee]'                   ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','employeeLabelPlural'                     ,'Employees'                     ,'[Employees]'                  ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','labcd1Label'                             ,'Labor Code Level 1'            ,'[Labcd Level 1]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','labcd1LabelPlural'                       ,'Labor Code Level 1s'           ,'[Labcd Level 1s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','labcd2Label'                             ,'Labor Code Level 2'            ,'[Labcd Level 2]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','labcd2LabelPlural'                       ,'Labor Code Level 2s'           ,'[Labcd Level 2s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','labcd3Label'                             ,'Labor Code Level 3'            ,'[Labcd Level 3]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','labcd3LabelPlural'                       ,'Labor Code Level 3s'           ,'[Labcd Level 3s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','labcd4Label'                             ,'Labor Code Level 4'            ,'[Labcd Level 4]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','labcd4LabelPlural'                       ,'Labor Code Level 4s'           ,'[Labcd Level 4s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','labcd5Label'                             ,'Labor Code Level 5'            ,'[Labcd Level 5]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','labcd5LabelPlural'                       ,'Labor Code Level 5s'           ,'[Labcd Level 5s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','labcdLabel'                              ,'Labor Code'                    ,'[Labor Code]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','labcdLabelPlural'                        ,'Labor Codes'                   ,'[Labor Codes]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','firmLabel'                               ,'Firm'                          ,'[Firm]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','firmLabelPlural'                         ,'Firms'                         ,'[Firms]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','mktLabel'                                ,'Marketing Campaign'            ,'[Marketing Campaign]'         ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','mktLabelPlural'                          ,'Marketing Campaigns'           ,'[Marketing Campaigns]'        ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','org1Label'                               ,'Organization 1'                ,'[Org Level 1]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','org1LabelPlural'                         ,'Organization 1s'               ,'[Org Level 1s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','org2Label'                               ,'Organization 2'                ,'[Org Level 2]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','org2LabelPlural'                         ,'Organization 2s'               ,'[Org Level 2s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','org3Label'                               ,'Organization 3'                ,'[Org Level 3]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','org3LabelPlural'                         ,'Organization 3s'               ,'[Org Level 3s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','org4Label'                               ,'Organization 4'                ,'[Org Level 4]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','org4LabelPlural'                         ,'Organization 4s'               ,'[Org Level 4s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','org5Label'                               ,'Organization 5'                ,'[Org Level 5]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','org5LabelPlural'                         ,'Organization 5s'               ,'[Org Level 5s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','orgLabel'                                ,'Organization'                  ,'[Organization]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','orgLabelPlural'                          ,'Organizations'                 ,'[Organizations]'              ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','proposalLabel'                           ,'Proposal'                      ,'[Proposal]'                   ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','proposalLabelPlural'                     ,'Proposals'                     ,'[Proposals]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','textLibLabel'                            ,'Boilerplate'                   ,'[Text Library]'               ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','textLibLabelPlural'                      ,'Boilerplates'                  ,'[Text Libraries]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','unitLabel'                               ,'Unit'                          ,'[Unit]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','unitLabelPlural'                         ,'Units'                         ,'[Units]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','vendorLabel'                             ,'Vendor'                        ,'[Vendor]'                     ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','vendorLabelPlural'                       ,'Vendors'                       ,'[Vendors]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','wbs1Label'                               ,'Project'                       ,'[WBS1]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','wbs1LabelPlural'                         ,'Projects'                      ,'[WBS1s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','wbs2Label'                               ,'Phase'                         ,'[WBS2]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','wbs2LabelPlural'                         ,'Phases'                        ,'[WBS2s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','wbs3Label'                               ,'Task'                          ,'[WBS3]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','wbs3LabelPlural'                         ,'Tasks'                         ,'[WBS3s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','sysPM'                                   ,'Project Manager'               ,'[Project Manager]'            ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','sysPR'                                   ,'Principal'                     ,'[Principal]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','sysSP'                                   ,'Supervisor'                    ,'[Supervisor]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','equipmentLabel'                          ,'Equipment'                     ,'[Equipment]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','equipmentLabelPlural'                    ,'Equipment'                     ,'[Equipments]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','jobToDateLabel'                          ,'Job-to-Date'                   ,'[JobToDate]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','JTDLabel'                                ,'JTD'                           ,'[JTD]'                        ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','estimateToCompleteLabel'                 ,'Estimate-to-Complete'          ,'[EstimateToComplete]'         ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','ETCLabel'                                ,'ETC'                           ,'[ETC]'                        ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','estimateAtCompletionLabel'               ,'Estimate-at-Completion'        ,'[EstimateAtCompletion]'       ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','EACLabel'                                ,'EAC'                           ,'[EAC]'                        ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','sysPRM'                                  ,'Proposal Manager'              ,'[Proposal Manager]'           ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','sysMC'                                   ,'Marketing Coordinator'         ,'[Marketing Coordinator]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','sysBDL'                                  ,'Business Development Lead'     ,'[Business Development Lead]'  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','consultantLabel'                         ,'Consultant'                    ,'[Consultant]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','consultantLabelPlural'                   ,'Consultants'                   ,'[Consultants]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','compensationLabel'                       ,'Compensation'                  ,'[Compensation]'               ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','revType1Label'                           ,'Labor'                         ,'[Revenue Method Type 1]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','revType2Label'                           ,'Consultant'                    ,'[Revenue Method Type 2]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','revType3Label'                           ,'Reimb'                         ,'[Revenue Method Type 3]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','revType4Label'                           ,'Revenue Method 4'              ,'[Revenue Method Type 4]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','revType5Label'                           ,'Revenue Method 5'              ,'[Revenue Method Type 5]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','feeEstimatingCostGroupLabel'             ,'Cost Group'                    ,'[Cost Group]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','feeEstimatingCostGroupLabelPlural'       ,'Cost Groups'                   ,'[Cost Groups]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','feeEstimatingFunctionalGroupLabel'       ,'Functional Group'              ,'[Functional Group]'           ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','feeEstimatingFunctionalGroupLabelPlural' ,'Functional Groups'             ,'[Functional Groups]'          ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','feeEstimatingServiceProfileLabel'        ,'Service Profile'               ,'[Service Profile]'            ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('en-US','feeEstimatingServiceProfileLabelPlural'  ,'Service Profiles'              ,'[Service Profiles]'           ,'N','N',NULL);

COMMIT TRANSACTION

END
GO
