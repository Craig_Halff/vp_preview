SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[spCCG_ProjectSalesClear] @WBS1 varchar(32), @WBS2 varchar(32), @WBS3 varchar(32)
AS 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
/* 
Copyright (c) 2018 Central Consulting Group.  All rights reserved.
08/02/2018 David Springer
           Validate Sales record is only changed in the current month
		   Call this from a Projects_Sales INSERT, CHANGE or DELETE workflow.
*/
BEGIN
    
	If @WBS2 <> ' '
		delete from Projects_Sales where @WBS1 = WBS1 and @WBS2 = WBS2 and @WBS3 = WBS3

END
GO
