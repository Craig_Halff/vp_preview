SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_PAT_GLAccount_Load]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

	SELECT Account, Name, Type, GlobalAccount, Status FROM CA

	--For multicompany mappings, will be empty for single company
	SELECT m.Account, Company 
		from CACompanyMapping m
			inner join CA on m.Account = CA.Account --make sure we only get valid ones
		WHERE CA.GlobalAccount = 'N'  --ignore unnecessary mapping for global accounts
		ORDER BY m.Account

	-- AR Mapping Accounts (Config -> Accounting -> Accounts Receivable -> AR Mapping Accounts)
	SELECT CA.Account, m.Company 
		from CA 
			inner join CFGARMap m on m.Account = CA.Account
		ORDER BY m.Account
END
GO
