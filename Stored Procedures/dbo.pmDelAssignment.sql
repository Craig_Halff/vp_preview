SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[pmDelAssignment]
  @strPlanID varchar(32),
  @strAssignmentID varchar(32)
AS

BEGIN -- Procedure pmDelAssignment
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to delete a Assignment row and its associated rows. When the passed in AssignmentID is null or empty string, delete all of the record for the plan.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

  DECLARE @sqlstr as nvarchar(max)

  SET @sqlstr = '
    DELETE EMActivity
    FROM EMActivity
    INNER JOIN Activity ON EMActivity.ActivityID = Activity.ActivityID
	INNER JOIN RPAssignment ON Activity.AssignmentID = RPAssignment.AssignmentID
     WHERE RPAssignment.PlanID = ''' + @strPlanID +''''
	IF isnull(@strAssignmentID,'') <> '' SET @sqlstr = @sqlstr  + ' AND  RPAssignment.AssignmentID = ''' + @strAssignmentID + ''''
	EXECUTE (@SQLSTR)
  
  SET @sqlstr = '
	DELETE ActivityCustomTabFields 
    FROM ActivityCustomTabFields
    INNER JOIN Activity ON ActivityCustomTabFields.ActivityID = Activity.ActivityID
    INNER JOIN RPAssignment ON Activity.AssignmentID = RPAssignment.AssignmentID
	WHERE RPAssignment.PlanID = ''' + @strPlanID +''''
	IF isnull(@strAssignmentID,'') <> ''  SET @sqlstr = @sqlstr  + ' AND  RPAssignment.AssignmentID = ''' + @strAssignmentID + ''''
	EXECUTE (@SQLSTR)

   SET @sqlstr = '
	DELETE Activity 
    FROM Activity
    INNER JOIN RPAssignment ON Activity.AssignmentID = RPAssignment.AssignmentID
	WHERE RPAssignment.PlanID = ''' + @strPlanID +''''
	IF isnull(@strAssignmentID,'') <> ''  SET @sqlstr = @sqlstr  + ' AND  RPAssignment.AssignmentID = ''' + @strAssignmentID + ''''
    EXECUTE (@sqlstr)
  
  SET @sqlstr = '
	DELETE RPPlannedLabor
	WHERE PlanID = ''' + @strPlanID +''''
	IF isnull(@strAssignmentID,'') <> ''  SET @sqlstr = @sqlstr  + ' AND  AssignmentID = ''' + @strAssignmentID + ''''
    EXECUTE (@sqlstr)

  SET @sqlstr = '
	DELETE RPBaselineLabor
  	WHERE PlanID = ''' + @strPlanID +''''
	IF isnull(@strAssignmentID,'') <> ''  SET @sqlstr = @sqlstr  + ' AND  AssignmentID = ''' + @strAssignmentID + ''''
    EXECUTE (@sqlstr)

  SET @sqlstr = '
	DELETE RPAssignment
	WHERE PlanID = ''' + @strPlanID +''''
	IF isnull(@strAssignmentID,'') <> '' SET @sqlstr = @sqlstr  + ' AND  AssignmentID = ''' + @strAssignmentID + ''''
    EXECUTE (@sqlstr)

  SET NOCOUNT OFF

END -- pmDelAssignment
GO
