SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SimpleSearch_ProjectsByMarketingName]
  @criteria VARCHAR(50) = NULL
AS
BEGIN
  ------------------------------------------------------------------------
  SET NOCOUNT ON;

  --------------------------------------------------------------------------
  DECLARE @search_criteria VARCHAR(52) = '%' 
                                       + COALESCE(@criteria, '') 
                                       + '%'

  ------------------------------------------------------------------------
  SELECT DISTINCT
    P.WBS1                AS "WBS1"
   ,P2.[Name]             AS "ProjectName"
   ,PX.CustMarketingName  AS "MarketingName"

  FROM
    dbo.PR AS P

    INNER JOIN dbo.PR AS P2
      ON P.WBS1           = P2.WBS1
        AND P2.WBS2       = ' '
        AND P2.ChargeType = 'R'

    INNER JOIN dbo.ProjectCustomTabFields AS PX
      ON P2.WBS1    = PX.WBS1
        AND PX.WBS2 = ' '

  WHERE
    PX.CustMarketingName LIKE @search_criteria

  ORDER BY
    P.WBS1
   ,P2.[Name]
   ,PX.CustMarketingName;
--------------------------------------------------------------------------
END
GO
