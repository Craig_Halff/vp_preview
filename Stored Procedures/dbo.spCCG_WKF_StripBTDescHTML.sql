SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_WKF_StripBTDescHTML](@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
AS BEGIN
	/* Copyright (c) 2018 Central Consulting Group.  All rights reserved. */
	 --select Replace(dbo.fnCCG_ConvertHtml_LeaveNewLines(dbo.fnCCG_ConvertHtml_LeaveNewLines(replace(Description, CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10), CHAR(13)+CHAR(10)))),
			--CHAR(13)+CHAR(10), '<br/>')
	update BT 
		set Description = Replace(dbo.fnCCG_ConvertHtml_LeaveNewLines(replace(Description, CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10), CHAR(13)+CHAR(10))),
			CHAR(13)+CHAR(10), '<br/>')
		where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3	
END

GO
