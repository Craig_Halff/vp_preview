SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_CustomColumn_GetMissingDatabaseFields]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	--exec [DeltekStoredProc_CCG_PAT_CustomColumn_GetMissingDatabaseFields]
	SET NOCOUNT ON;
	SELECT syscolumns.name
        FROM sysobjects
            INNER JOIN syscolumns on syscolumns.id=sysobjects.id
        WHERE sysobjects.Name = N'CCG_PAT_CustomColumns' and sysobjects.xtype = N'U' and not syscolumns.name = N'PayableSeq'
        ORDER BY syscolumns.name;
END;

GO
