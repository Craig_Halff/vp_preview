SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_LoadFeeData]
	@rolesFieldsSql						Nvarchar(max),
	@rolesJoinsSql						Nvarchar(max),
	@filterBillGrp						bit,
	@filterSQL							bit,
	@isRoleAcct							bit,
	@supportBuiltInFeePctCpl			varchar(10),
	@isSingleProject					bit,
	@invoiceGroup						Nvarchar(255),
	@User								Nvarchar(32),
	@wbs1List							Nvarchar(max),			-- .Replace(" ", "").Replace(",", "','")
	@includeDormant						varchar(5),
	@langOverallPercentComplete			Nvarchar(255),
	@langCumulativeUnitOrFee			Nvarchar(255),
	@langCurrentInitOrFee				Nvarchar(255),
	@langLumpSum						Nvarchar(255),
	@langPercentOfConstruction			Nvarchar(255),
	@langUnitBased						Nvarchar(255),
	@langPercentCompleteByPhase			Nvarchar(255),
	@langPercentCompleteByPhaseFixed	Nvarchar(255),
	@VISION_LANGUAGE					varchar(10) = null
AS
BEGIN
	/*
	-- was called DeltekStoredProc_CCG_EI_Load_Fee_Data
	spCCG_EI_LoadFeeData @rolesFieldsSql = '', @rolesJoinsSql = '', @filterBillGrp = '0',
		 @filterSQL = '1', @isRoleAcct = '1', @supportBuiltInFeePctCpl = 'Q', @isSingleProject = '1', @invoiceGroup = '', @User = 'ADMIN',
		 @wbs1List = '2003005.00', @includeDormant = 'N', @langOverallPercentComplete = 'Overall Percent Complete#',
		 @langCumulativeUnitOrFee = 'Cumulative unit or fee#', @langCurrentInitOrFee = 'Current unit or fee#',
		 @langLumpSum = 'Lump sum#', @langPercentOfConstruction = 'Percent of construction#',
		 @langUnitBased = 'Unit-based#', @langPercentCompleteByPhase = 'Percent Complete by Phase, as Percent of Fee#',
		 @langPercentCompleteByPhaseFixed = 'Percent Complete by Phase, as Fixed Amount#',
		 @VISION_LANGUAGE = 'en-GB'
	*/
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @sSQL Nvarchar(max)

	DECLARE @VisionLang		varchar(20)
	if @VISION_LANGUAGE IS NULL
		select @VisionLang = ISNULL(EM.Language,dbo.FW_GetActiveCultureName()) from SEUSER
			left join EM on SEUSER.Employee = EM.Employee
	else
		set @VisionLang = @VISION_LANGUAGE
	-- Alternatively:
	/*
	Set @langOverallPercentComplete = (
		SELECT Label FROM CCG_Language_Labels
		WHERE Product in ('','EI') and UICultureName = @VISION_LANGUAGE And Id = 'Overall Percent Complete'
	);
	*/

	-- Overall percent completes:
	--------------------------------------------------------------
    SET @sSQL = N'
		SELECT ''Y'' as Editable, ' + @rolesFieldsSql + N'
			BT.WBS1, BT.WBS2, BT.WBS3,
			PR1.Name as WBS1Name, IsNull(PR2.Name, '''') as WBS2Name, IsNull(PR3.Name, '''') as WBS3Name,
			'''' as BTFwbs1, '''' as BTFwbs2, '''' as BTFwbs3, 0 as BTFSeq, '''' as BillingPhase,
			'''' as BillingPhaseName, '''' as PhaseGroup, BT.FeeMeth, IsNull(BT.FeeBasis, '''') as FeeBasis, BT.FeeFactor1,
			BT.FeeFactor2, BT.FeeLabel, PR.BillingCurrencyCode,
			Case BT.FeeBasis
				When ''L'' Then BT.FeeFactor1
				When ''P'' Then BT.FeeFactor1 * BT.FeeFactor2 / 100.0
				When ''U'' Then BT.FeeFactor1 * BT.FeeFactor2
			End as [Fee],
			Case When BT.FeeMeth = 1 Then BT.FeePctCpl Else null End as [CurFeePctCpl],
			convert(decimal(19, 4),
				Case When BT.FeeByDetailEnabled = ''Y'' Then BT.FeeToDate1 + BT.FeeToDate2 + BT.FeeToDate3 + BT.FeeToDate4 + BT.FeeToDate5
					When BT.FeeMeth = 1 Then BT.FeeToDate
					When BT.FeeBasis = ''L'' Then BT.FeeFactor1
					When BT.FeeBasis = ''U'' Then BT.FeeFactor1 * BT.FeeFactor2
					When BT.FeeBasis = ''P'' Then BT.FeeFactor1 * BT.FeeFactor2 / 100.0
				End
			) as [CurFeeToDate],
			Case When BT.FeeMeth = 1 Then isnull(fpc.FeePctCpl, BT.FeePctCpl) ELSE NULL End as [NewFeePctCpl],
			CONVERT(decimal(19, 4),
				CASE When BT.FeeByDetailEnabled = ''Y'' then isnull(fpc.FeeToDate1, BT.FeeToDate1) + isnull(fpc.FeeToDate2, BT.FeeToDate2)
						+ isnull(fpc.FeeToDate3, BT.FeeToDate3) + isnull(fpc.FeeToDate4, BT.FeeToDate4) + isnull(fpc.FeeToDate5, BT.FeeToDate5)
				ELSE isnull(fpc.FeeToDate,
					Case When BT.FeeMeth = 1 Then BT.FeeToDate
						When BT.FeeBasis = ''L'' Then BT.FeeFactor1
						When BT.FeeBasis = ''U'' Then BT.FeeFactor1 * BT.FeeFactor2
						When BT.FeeBasis = ''P'' Then BT.FeeFactor1 * BT.FeeFactor2 / 100.0
					End)
				END) as [NewFeeToDate],
			(CASE When BT.FeeByDetailEnabled = ''Y'' then isnull(fpc.FeeToDate1, BT.FeeToDate1) + isnull(fpc.FeeToDate2, BT.FeeToDate2)
						+ isnull(fpc.FeeToDate3, BT.FeeToDate3) + isnull(fpc.FeeToDate4, BT.FeeToDate4) + isnull(fpc.FeeToDate5, BT.FeeToDate5)
			ELSE isnull(fpc.FeeToDate,
				Case When BT.FeeMeth = 1 Then BT.FeeToDate
					When BT.FeeBasis = ''L'' Then BT.FeeFactor1
					When BT.FeeBasis = ''U'' Then BT.FeeFactor1 * BT.FeeFactor2
					When BT.FeeBasis = ''P'' Then BT.FeeFactor1 * BT.FeeFactor2 / 100.0
				End)
			END) -
			(Case When BT.FeeByDetailEnabled = ''Y'' Then BT.FeeToDate1 + BT.FeeToDate2 + BT.FeeToDate3 + BT.FeeToDate4 + BT.FeeToDate5
				When BT.FeeMeth = 1 Then BT.FeeToDate
				When BT.FeeBasis = ''L'' Then BT.FeeFactor1
				When BT.FeeBasis = ''U'' Then BT.FeeFactor1 * BT.FeeFactor2
				When BT.FeeBasis = ''P'' Then BT.FeeFactor1 * BT.FeeFactor2 / 100.0
			End) as [CurBilling],
			Case When BT.FeeMeth <> 4 and BT.FeeBasis = ''U'' Then 0.0 Else null End as [CurUnits],
			Case When BT.FeeMeth <> 5 and BT.FeeBasis = ''U'' Then 0.0 Else null End as [UnitsToDate],
			Case BT.FeeMeth
				When 1 Then ''' + @langOverallPercentComplete + N'''
				When 4 Then ''' + @langCumulativeUnitOrFee + N'''
				When 5 Then ''' + @langCurrentInitOrFee + N'''
			End as [FeeMethod],
			Case BT.FeeBasis
				When ''L'' Then ''' + @langLumpSum + N'''
				When ''P'' Then ''' + @langPercentOfConstruction + N'''
				When ''U'' Then ''' + @langUnitBased + N'''
				Else ''''
			End as [FeeBasisDescr],
			Case When IsNull(rights.CanModify, ''N'') = ''Y'' Then ''Yes'' Else ''No'' End as [NonAcctStage],
			BT.ScheduledBilling,
			(SELECT Max(ScheduledBilling) as ScheduledBilling FROM CFGBillMain) AS [ScheduledBillingEnabledGlobally],
			(SELECT Max(FeeByCategoryEnabled) as FeeByCategoryEnabled FROM CFGBillMain) as [FeeByCategoryEnabledGlobally],
			BT.FeeByDetailEnabled, fpc.Seq
		FROM BT
			INNER JOIN PR on PR.wbs1 = BT.wbs1 and PR.wbs2 = BT.wbs2 and PR.wbs3 = BT.wbs3
			INNER JOIN BT BT2 on BT2.wbs1 = BT.wbs1 and BT2.wbs2 = BT.wbs2 and BT2.wbs3 = N'' ''
			INNER JOIN BT BT1 on BT1.wbs1 = BT.wbs1 and BT1.wbs2 = N'' '' and BT1.wbs3 = N'' ''
			INNER JOIN ProjectCustomTabFields PCTF on PCTF.WBS1 = BT.WBS1 and PCTF.WBS2 = N'' '' and PCTF.WBS3 = N'' '' ' + @rolesJoinsSql

	IF @filterBillGrp = 1 SET @sSQL = @sSQL + N'
			LEFT JOIN BTBGSubs on BTBGSubs.SubWBS1 = BT1.WBS1'

	-- Use the filter function only for accounting users and only if viewing more than 1 row at a time:
	IF @filterSQL = 1 AND @isRoleAcct = 1 AND @isSingleProject = 0 SET @sSQL = @sSQL + N'
			INNER JOIN (
				SELECT Distinct wbs1 FROM dbo.fnCCG_EI_ProjectFilter(''' + @invoiceGroup + N''', ''' + @User + N''')
			) ProjectFilter on ProjectFilter.wbs1 = PR.wbs1 '

	SET @sSQL = @sSQL + N'
			LEFT JOIN PR PR3 on PR3.wbs1 = BT.wbs1 and PR3.WBS2 = BT.wbs2 and PR3.WBS3 = BT.WBS3 and PR3.WBS3 <> N'' ''
			LEFT JOIN PR PR2 on PR2.wbs1 = BT.wbs1 and PR2.WBS2 = BT.wbs2 and PR.WBS2 <> N'' '' and PR2.WBS3 = N'' ''
			LEFT JOIN PR PR1 on PR1.wbs1 = BT.wbs1 and PR1.WBS2 = N'' '' and PR1.WBS3 = N'' ''
			LEFT JOIN CCG_EI_FeePctCpl fpc on fpc.WBS1 = BT.WBS1 and fpc.WBS2 = BT.WBS2 and fpc.WBS3 = BT.WBS3
			LEFT JOIN CCG_EI on CCG_EI.WBS1 = BT.WBS1
			LEFT JOIN (
				SELECT StageFlow, Stage, Max(CanModify) as CanModify
					FROM CCG_EI_ConfigRights WHERE CanModify = ''Y''
					GROUP BY StageFlow, Stage
			) As rights on rights.StageFlow = PCTF.CustInvoiceStageFlow and rights.Stage = CCG_EI.InvoiceStage
		WHERE BT.FeeMeth in (1, 4, 5) '

	IF @isRoleAcct = 1 AND @supportBuiltInFeePctCpl <> 'Q' SET @sSQL = @sSQL + N'
			AND PCTF.CustInvoiceGroup = ''' + @invoiceGroup + N''' '
	ELSE IF @isRoleAcct = 1 AND @supportBuiltInFeePctCpl = 'Q' SET @sSQL = @sSQL + N'
			AND BT.WBS1 = N''' + @wbs1List + N''' '
	ELSE IF @supportBuiltInFeePctCpl = 'P' OR @supportBuiltInFeePctCpl = 'Q' OR @isSingleProject = 1 SET @sSQL = @sSQL + N'
			AND BT.WBS1 = N''' + @wbs1List + N''' '			-- PM - Selected project only
	ELSE IF @supportBuiltInFeePctCpl = 'U' SET @sSQL = @sSQL + N'
			AND BT.WBS1 in (''' + @wbs1List + N''') '

	-- 5.2 fix:  If we're not filtering by SQL and we're showing multiple projects, then exclude dormant WBS1s:
	IF @filterSQL = 0 AND @isSingleProject = 0 SET @sSQL = @sSQL + N'
			AND PR1.Status <> ''D'' and PR1.ChargeType = ''R'' '

	SET @sSQL = @sSQL + N'
			AND ((BT.WBS2 = N'' '' and BT.WBS3 = N'' '')
				OR (BT.WBS2 <> N'' '' and BT.WBS3 = N'' ''  and BT1.SubLevelTerms = ''Y'')
				OR (BT.WBS2 <> N'' '' and BT.WBS3 <> N'' '' and BT1.SubLevelTerms = ''Y'' and BT2.SubLevelTerms = ''Y'')
			) '

	IF @filterBillGrp = 1 SET @sSQL = @sSQL + N'
			AND (BTBGSubs.MainWBS1 is null OR BTBGSubs.MainWBS1 = BTBGSubs.SubWBS1) '

	-- Billing phases:
	--------------------------------------------------------------
	SET @sSQL = @sSQL + N'
		UNION ALL
		SELECT Case When BT.FeeMeth = 2 Then ''N'' else ''Y'' end as Editable, ' + @rolesFieldsSql + N'
			BT.WBS1, BT.WBS2, BT.WBS3,
			PR1.Name as WBS1Name, IsNull(PR2.Name, '''') as WBS2Name, IsNull(PR3.Name, '''') as WBS3Name,
			Case
				When BT.PostFeesByPhase = ''Y'' and BTF.Postwbs1 is not null Then BTF.Postwbs1
				Else BTF.wbs1
			End as BTFwbs1,
			Case
				When BT.PostFeesByPhase = ''Y'' and BTF.Postwbs2 is not null Then BTF.Postwbs2
				Else BTF.wbs2
			End as BTFwbs2,
			Case
				When BT.PostFeesByPhase = ''Y'' and BTF.Postwbs3 is not null Then BTF.Postwbs3
				Else BTF.wbs3
			End as BTFwbs3,
			BTF.Seq as BTFSeq, BTF.Phase as BillingPhase, BTF.Name as BillingPhaseName, pgd.Description as PhaseGroup,
			BT.FeeMeth, IsNull(BT.FeeBasis, '''') as FeeBasis, BT.FeeFactor1, BT.FeeFactor2,
			BT.FeeLabel, PR.BillingCurrencyCode,
			Case BT.FeeMeth
				When 2 Then
					Case BT.FeeBasis
						When ''L'' Then BTF.PctOfFee / 100.0 * BT.FeeFactor1
						When ''P'' Then BTF.PctOfFee / 100.0 * BT.FeeFactor1 * BT.FeeFactor2 / 100.0
						When ''U'' Then BTF.PctOfFee / 100.0 * BT.FeeFactor1 * BT.FeeFactor2
					End
				When 3 Then BTF.Fee
			End as Fee,
			BTF.PctComplete as CurFeePctCpl,
			BTF.FeeToDate as CurFeeToDate,
			IsNull(fpc.FeePctCpl, BTF.PctComplete) as NewFeePctCpl,
			IsNull(fpc.FeeToDate, BTF.FeeToDate) as NewFeeToDate,
			IsNull(fpc.FeeToDate, BTF.FeeToDate) - BTF.FeeToDate as CurBilling,
			Case BT.FeeBasis When ''U'' Then 0 Else null End as CurUnits,
			Case BT.FeeBasis When ''U'' Then 0 Else null End as UnitsToDate,
			Case BT.FeeMeth
				When 2 Then ''' + @langPercentCompleteByPhase + N'''
				When 3 Then ''' + @langPercentCompleteByPhaseFixed + N'''
				Else ''''
			End as FeeMethod,
			Case BT.FeeBasis
				When ''L'' Then ''' + @langLumpSum + N'''
				When ''P'' Then ''' + @langPercentOfConstruction + N'''
				When ''U'' Then ''' + @langUnitbased + N'''
				Else ''''
			End as FeeBasisDescr,
			Case
				When IsNull(rights.CanModify, ''N'') = ''Y'' Then ''Yes''
				Else ''No''
			End as NonAcctStage,
			BT.ScheduledBilling,
			(SELECT Max(ScheduledBilling) as ScheduledBilling FROM CFGBillMain) as ScheduledBillingEnabledGlobally,
			(SELECT Max(FeeByCategoryEnabled) as FeeByCategoryEnabled FROM CFGBillMain) as FeeByCategoryEnabledGlobally,
			BT.FeeByDetailEnabled, fpc.Seq
		FROM BT
			INNER JOIN BTF on BTF.wbs1 = BT.wbs1 and BTF.wbs2 = BT.wbs2 and BTF.wbs3 = BT.wbs3
			LEFT JOIN BTFPhaseGroupDescriptions pgd on pgd.WBS1 = BTF.WBS1 and pgd.WBS2 = BTF.WBS2 and pgd.WBS3 = BTF.WBS3
				and pgd.PhaseGroup = BTF.PhaseGroup and pgd.UICultureName = ''' + @VisionLang + N'''
			INNER JOIN PR on PR.wbs1 = BT.wbs1 and PR.wbs2 = BT.wbs2 and PR.wbs3 = BT.wbs3
			INNER JOIN BT BT2 on BT2.wbs1 = BT.wbs1 and BT2.wbs2 = BT.wbs2 and BT2.wbs3 = N'' ''
			INNER JOIN BT BT1 on BT1.wbs1 = BT.wbs1 and BT1.wbs2 = N'' '' and BT1.wbs3 = N'' ''
			INNER JOIN ProjectCustomTabFields PCTF on PCTF.WBS1 = BT.WBS1 and PCTF.WBS2 = N'' '' and PCTF.WBS3 = N'' '' ' + @rolesJoinsSql

	IF @filterBillGrp = 1 SET @sSQL = @sSQL + N'
			LEFT JOIN BTBGSubs on BTBGSubs.SubWBS1 = BT1.WBS1'

	-- Use the filter function only for accounting users and only if viewing more than 1 row at a time:
	-- EI 5.2 - defect found by GHT - missing this piece here but not in above lump sum SQL
	IF @filterSQL = 1 AND @isRoleAcct = 1 AND @isSingleProject = 0 SET @sSQL = @sSQL + N'
			INNER JOIN (
				SELECT Distinct wbs1 FROM dbo.fnCCG_EI_ProjectFilter(''' + @invoiceGroup + N''', ''' + @User + N''')
			) ProjectFilter on ProjectFilter.wbs1 = PR.wbs1 '

	SET @sSQL = @sSQL + N'
			LEFT JOIN PR PR3 on PR3.wbs1 = BT.wbs1 and PR3.wbs2 = BT.wbs2 and PR3.wbs3 = BT.wbs3 and PR3.wbs3 <> N'' ''
			LEFT JOIN PR PR2 on PR2.wbs1 = BT.wbs1 and PR2.wbs2 = BT.wbs2 and PR.wbs2 <> N'' '' and PR2.wbs3 = N'' ''
			LEFT JOIN PR PR1 on PR1.wbs1 = BT.wbs1 and PR1.wbs2 = N'' '' and PR1.wbs3 = N'' ''
			LEFT JOIN CCG_EI_FeePctCpl fpc On fpc.WBS1 = BTF.WBS1 and fpc.WBS2 = BTF.WBS2 and fpc.WBS3 = BTF.WBS3 and fpc.Seq = BTF.Seq
			LEFT JOIN CCG_EI on CCG_EI.WBS1 = BT.WBS1
			LEFT JOIN (
				SELECT StageFlow, Stage, Max(CanModify) as CanModify
					FROM CCG_EI_ConfigRights WHERE CanModify = ''Y''
					GROUP BY StageFlow, Stage
			) As rights on rights.StageFlow = PCTF.CustInvoiceStageFlow and rights.Stage = CCG_EI.InvoiceStage '

	IF @isRoleAcct = 1 AND @supportBuiltInFeePctCpl <> 'Q' SET @sSQL = @sSQL + N'
		WHERE PCTF.CustInvoiceGroup = ''' + @invoiceGroup + N''' '
	ELSE IF @isRoleAcct = 1 AND @supportBuiltInFeePctCpl = 'Q' SET @sSQL = @sSQL + N'
		WHERE BT.WBS1 = N''' + @wbs1List + N''' '
	ELSE IF @supportBuiltInFeePctCpl = 'P' OR @supportBuiltInFeePctCpl = 'Q' OR @isSingleProject = 1 SET @sSQL = @sSQL + N'
		WHERE BT.WBS1 = N''' + @wbs1List + N''' '			-- Selected project only
	ELSE SET @sSQL = @sSQL + N'
		WHERE BT.WBS1 IN (''' + @wbs1List + N''') '

	-- 5.2 fix:  If we're not filtering by SQL and we're showing multiple projects, then exclude dormant WBS1s:
	IF @filterSQL = 0 AND @isSingleProject = 0 SET @sSQL = @sSQL + N'
			AND PR1.Status <> ''D'' AND PR1.ChargeType = ''R'' '

	SET @sSQL = @sSQL + N'
			AND (PR2.WBS2 = N'' '' OR IsNull(PR2.Status, ''A'') <> ''D'' OR ''' + @includeDormant + N''' = ''Y'')
			AND BT.FeeMeth IN (2, 3)
			AND ((BT.WBS2 = N'' '' and BT.WBS3 = N'' '')
				OR (BT.WBS2 <> N'' '' AND BT.WBS3 = N'' '' AND BT1.SubLevelTerms = ''Y'')
				OR (BT.WBS2 <> N'' '' AND BT.WBS3 <> N'' '' AND BT1.SubLevelTerms = ''Y'' AND BT2.SubLevelTerms = ''Y'')
			) '

	IF @filterBillGrp = 1 SET @sSQL = @sSQL + N'
			AND (BTBGSubs.MainWBS1 is null OR BTBGSubs.MainWBS1 = BTBGSubs.SubWBS1) '

	SET @sSQL = @sSQL + N'
		ORDER BY BT.WBS1, BT.WBS2, BT.WBS3, BillingPhase '

	--PRINT @sSQL
	EXEC (@sSQL)
END
GO
