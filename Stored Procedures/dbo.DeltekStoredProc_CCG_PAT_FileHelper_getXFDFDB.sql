SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FileHelper_getXFDFDB] ( @payableSeq int, @revisionSeq int)
             AS EXEC spCCG_PAT_FileHelper_getXFDFDB @payableSeq,@revisionSeq
GO
