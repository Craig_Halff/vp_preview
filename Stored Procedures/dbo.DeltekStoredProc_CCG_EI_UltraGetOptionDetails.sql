SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_UltraGetOptionDetails] ( @employeeId nvarchar(32), @type varchar(max)= 'GridOptions')
             AS EXEC spCCG_EI_UltraGetOptionDetails @employeeId,@type
GO
