SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Save_Options]
	@seq			int,
	@empId			Nvarchar(32),
	@dbDescr		Nvarchar(50),
	@dbOptions		Nvarchar(max),
	@type			varchar(max) = 'GridOptions'
AS
BEGIN
	/*
	DeltekStoredProc_CCG_EI_Save_Options
		@seq = '4',
		@empId = '00001',
		@dbDescr = '',
		@dbOptions = ''
	*/
	SET NOCOUNT ON;
	SET @dbOptions = ISNULL(@dbOptions, '');

	IF @seq <= 0 BEGIN -- This is a new message
		INSERT into CCG_EI_ConfigOptions (DateChanged, ChangedBy, [Description], [Type], Detail)
			VALUES (getdate(), @empId, @dbDescr, @type, @dbOptions);

		SELECT @@IDENTITY As Seq;
	END
	ELSE BEGIN -- This is an update
		IF @dbOptions <> ''
			UPDATE CCG_EI_ConfigOptions
				SET DateChanged = getdate(),
					ChangedBy = @empId,
					[Description] = @dbDescr,
					Detail = @dbOptions
				WHERE seq = @seq;
		ELSE -- If no options passed in it is a delete
			Delete FROM CCG_EI_ConfigOptions WHERE seq = @seq;

		SELECT @seq As Seq;
	END;

END;
GO
