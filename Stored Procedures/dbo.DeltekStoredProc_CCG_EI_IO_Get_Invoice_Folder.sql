SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_IO_Get_Invoice_Folder]
	@wbs1		Nvarchar(30)
AS
BEGIN
	-- DeltekStoredProc_CCG_EI_IO_Get_Invoice_Folder @wbs1 = '2002005.00'
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT custInvoiceFolder, custInvoiceFolderFinals
		FROM ProjectCustomTabFields
		WHERE WBS1 = @wbs1 AND wbs2 = ' ' AND wbs3 = ' '
END;
GO
