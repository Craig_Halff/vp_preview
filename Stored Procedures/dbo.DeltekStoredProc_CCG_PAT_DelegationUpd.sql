SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_DelegationUpd] ( @Id varchar(32), @Username nvarchar(32), @Employee nvarchar(20), @Delegate nvarchar(20), @FromDate datetime, @ToDate datetime, @MaxAmount money, @Dual char(1), @ApprovedBy nvarchar(20), @ApprovedOn datetime, @Permanent char(1))
             AS EXEC spCCG_PAT_DelegationUpd @Id,@Username,@Employee,@Delegate,@FromDate,@ToDate,@MaxAmount,@Dual,@ApprovedBy,@ApprovedOn,@Permanent
GO
