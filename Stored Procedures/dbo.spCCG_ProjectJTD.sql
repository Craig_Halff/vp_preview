SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectJTD] @WBS1 varchar (32)
AS
/*
Copyright (c) 2017 Central Consulting Group. All rights reserved.
08/11/2017 David Springer
           Populate project UDF fields for JTD values.
           Call this from a Project scheduled workflow and a button on the project info center.
08/18/2017 David Springer
           Added Billed Tax
12/15/2020 Craig H. Anderson
		   Added exclusions for CustUnbilledAmountBilling and CustUnbilledTransactions where SuppressBill = 'Y'
06/01/2021	Craig H. Anderson
			Added  "WITH (NOLOCK)" hints and removed HalffImport db name references.
				
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
BEGIN

DECLARE @currentPeriod INT = DATEPART(YEAR, GETDATE()) * 100 + DATEPART(MONTH, GETDATE());
DECLARE @firstDayOfPeriod DATE = LEFT(CAST(@currentPeriod AS VARCHAR(8)), 4) + RIGHT(CAST(@currentPeriod AS VARCHAR(8)), 2) + '01';
DECLARE @lastDayOfPeriod DATE = EOMONTH(@firstDayOfPeriod);

-- Reset Values that may no longer exist; others will always exist and be incrementing
	Update ProjectCustomTabFields
	Set CustUnbilledAmountBilling = 0,
		CustUnbilledTransactions = 0,
		CustJTDRevenue = 0,
		CustYTDRevenue = 0,
		CustJTDARBalance = 0,
		CustJTDBilled = 0,
		CustJTDBilledTax = 0,
		CustYTDBilled = 0,
		CustJTDReceived = 0,
		CustYTDReceived = 0,
		CustJTDSpentBilling = 0,
		CustYTDSpentBilling = 0,
		CustJTDProfit = 0,
		CustYTDProfit = 0,
		CustJTDProfitPercent = 0,
		CustYTDProfitPercent = 0,
		CustJTDWorkinProcess = 0,
		CustJTDVariance = 0,
		CustYTDVariance = 0,
		CustJTDGoalMultiplier = 0,
		CustYTDGoalMultiplier = 0,
		CustJTDEffectiveMultiplier = 0,
		CustYTDEffectiveMultiplier = 0
	Where WBS1 = @WBS1

-- ##########   Project (WBS1) level data   ##########
    --p.CustUnbilledAmountBilling = IsNull (ap.APUnbilled, 0) + IsNull (ex.ExUnbilled, 0) + IsNull (m.MiscUnbilled, 0) + IsNull (l.LaborUnbilled, 0),
	Update p
	Set 
		p.CustUnbilledAmountBilling = ISNULL(dbo.fnCCG_EI_JTDUnbilledBillingThru(p.WBS1, p.WBS2, p.WBS3, @lastDayOfPeriod),0.0)
		,p.CustUnbilledTransactions = IsNull (ap.APQty, 0) + IsNull (ex.ExQty, 0) + IsNull (m.MiscQty, 0) + IsNull (l.LaborQty, 0)
	From ProjectCustomTabFields p
		Left Join (Select WBS1, Sum (BillExt) APUnbilled, Count (*) APQty
					From LedgerAP WITH (NOLOCK) WHERE BillStatus in ('B', 'H', 'D', 'W') and SuppressBill <> 'Y'
					Group by WBS1) ap on p.WBS1 = ap.WBS1
		Left Join (Select WBS1, Sum (BillExt) ExUnbilled, Count (*) EXQty
					From LedgerEX WITH (NOLOCK)  Where BillStatus in ('B', 'H', 'D', 'W') and SuppressBill <> 'Y'
					Group by WBS1)ex on p.WBS1 = ex.WBS1
		Left Join (Select WBS1, Sum (BillExt) MiscUnbilled, Count (*) MiscQty
					From LedgerMisc WITH (NOLOCK)  Where BillStatus in ('B', 'H', 'D', 'W') and SuppressBill <> 'Y'
					Group by WBS1) m on p.WBS1 = m.WBS1
		Left Join (Select WBS1, Sum (BillExt) LaborUnbilled, Count (*) LaborQty
					From LD WITH (NOLOCK)  Where BillStatus in ('B', 'H', 'D', 'W') and SuppressBill <> 'Y'
					Group by WBS1) l on p.WBS1 = l.WBS1
	Where p.WBS1 = @WBS1
	  and p.WBS2 = ' '  -- Project level only
	  

	Update p
	Set p.CustJTDRevenue = IsNull (s.JTDRevenue, 0),
		p.CustYTDRevenue = IsNull (s.YTDRevenue, 0),
		p.CustJTDBilled = IsNull (s.JTDBilled, 0),
		p.CustJTDBilledTax = IsNull (t.Tax, 0),
		p.CustYTDBilled = IsNull (s.YTDBilled, 0),
		p.CustJTDReceived = IsNull (s.JTDReceived, 0),
		p.CustYTDReceived = IsNull (s.YTDReceived, 0),
		p.CustJTDARBalance = IsNull (s.JTDBilled, 0) + IsNull (t.Tax, 0) + IsNull (t.Interest, 0) - IsNull (s.JTDReceived, 0) - IsNull (i.InterestPaid, 0),
		p.CustJTDSpentBilling = IsNull (s.JTDSpentBilling, 0),
		p.CustYTDSpentBilling = IsNull (s.YTDSpentBilling, 0),
		p.CustJTDProfit = IsNull (s.JTDProfit, 0),
		p.CustYTDProfit = IsNull (s.YTDProfit, 0),
		p.CustJTDProfitPercent = Round (CASE When IsNull (s.JTDRevenue, 0) = 0 Then 0 Else IsNull (s.JTDProfit, 0) / s.JTDRevenue * 100 END, 2),
		p.CustYTDProfitPercent = Round (CASE When IsNull (s.YTDRevenue, 0) = 0 Then 0 Else IsNull (s.YTDProfit, 0) / s.YTDRevenue * 100 END, 2),
		p.CustJTDVariance = IsNull (s.JTDRevenue, 0) - IsNull (s.JTDSpentBilling, 0),
		p.CustYTDVariance = IsNull (s.YTDRevenue, 0) - IsNull (s.YTDSpentBilling, 0),
		p.CustJTDWorkinProcess = IsNull (s.WIP, 0)
	From ProjectCustomTabFields p
		Left Join
		(Select WBS1,
			Sum (Revenue) JTDRevenue,
			Sum (CASE When ROUND(Period/100,0,1) = Year (getDate()) Then Revenue Else 0 END) YTDRevenue,
			Sum (Billed) JTDBilled,
			Sum (CASE When ROUND(Period/100,0,1) = Year (getDate()) Then Billed Else 0 END) YTDBilled,
			Sum (Received) JTDReceived,
			Sum (CASE When ROUND(Period/100,0,1) = Year (getDate()) Then Received Else 0 END) YTDReceived,
			Sum (SpentBilling) JTDSpentBilling,
			Sum (CASE When ROUND(Period/100,0,1) = Year (getDate()) Then SpentBilling Else 0 END) YTDSpentBilling,
			Sum (ProfitCostLessOH - Overhead) JTDProfit,
			Sum (CASE When ROUND(Period/100,0,1) = Year (getDate()) Then ProfitCostLessOH - Overhead Else 0 END) YTDProfit,
			Sum (Unbilled) WIP
		From PRSummaryMain WITH (NOLOCK) 
		Group by WBS1) s on s.WBS1 = p.WBS1
		Left Join
		(Select WBS1, 
				- Sum (CASE When InvoiceSection = 'T' Then Amount Else 0 END) Tax,
				- Sum (CASE When InvoiceSection = 'I' Then Amount Else 0 END) Interest
		From LedgerAR WITH (NOLOCK) 
		Where TransType = 'IN'
		  and InvoiceSection in ('I', 'T')
		Group by WBS1
		Having Sum (Amount) <> 0) t on t.WBS1 = p.WBS1
		Left Join
		(Select WBS1, - Sum (Amount) InterestPaid
		From LedgerAR WITH (NOLOCK) 
		Where TransType = 'CR'
		  and Account = '407.00' -- Interest Revenue
		Group by WBS1
		Having Sum (Amount) <> 0) i on i.WBS1 = p.WBS1
	Where p.WBS1 = @WBS1
	  and p.WBS2 = ' '  -- Project level only

-- JTD Multipliers
	Update p
	Set p.CustJTDGoalMultiplier = Round (IsNull (s.JTDGoalMult, 0), 2),
		p.CustJTDEffectiveMultiplier = Round (IsNull (s.JTDEffectiveMult, 0), 2)
	From ProjectCustomTabFields p
		Left Join
		(Select WBS1,
			Sum (LaborBilling + DirectConsBilling + DirectOtherBilling + ReimbConsBilling + ReimbOtherBilling
				- DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) /
			Sum (LaborCost) JTDGoalMult,
			Sum (Revenue - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) /
			Sum (LaborCost) JTDEffectiveMult
		From PRSummarySub WITH (NOLOCK) 
		Group by WBS1
		Having Sum (LaborCost) <> 0) s on s.WBS1 = p.WBS1
	Where p.WBS1 = @WBS1
	  and p.WBS2 = ' '  -- Project level only

-- YTD Multipliers
	Update p
	Set p.CustYTDGoalMultiplier = Round (IsNull (s.YTDGoalMult, 0), 2),
		p.CustYTDEffectiveMultiplier = Round (IsNull (s.YTDEffectiveMult, 0), 2)
	From ProjectCustomTabFields p
		Left Join
		(Select WBS1,
			Sum (LaborBilling + DirectConsBilling + DirectOtherBilling + ReimbConsBilling + ReimbOtherBilling
				 - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) /
			Sum (LaborCost) YTDGoalMult,
			Sum (Revenue - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) /
			Sum (LaborCost) YTDEffectiveMult
		From PRSummarySub WITH (NOLOCK) 
		Where ROUND(Period/100,0,1) = Year (getDate())
		Group by WBS1
		Having Sum (LaborCost) <> 0) s on s.WBS1 = p.WBS1
	Where p.WBS1 = @WBS1
	  and p.WBS2 = ' '  -- Project level only

-- CustLastChargeDate
	UPDATE p
	SET p.CustLastChargeDate = A.MaxTransDate
	FROM ProjectCustomTabFields p
	LEFT JOIN
	(
		SELECT WBS1, MAX(TransDate) AS MaxTransDate
		FROM
		(
			SELECT WBS1, MAX(TransDate) AS TransDate
			FROM [dbo].[LD] WITH (NOLOCK) 
			WHERE WBS1 = @WBS1
			GROUP BY WBS1

			UNION ALL

			SELECT WBS1, MAX(TransDate) AS TransDate
			FROM [dbo].[LedgerMisc] WITH (NOLOCK) 
			WHERE WBS1 = @WBS1
				and BillStatus is not null
			GROUP BY WBS1

			UNION ALL

			SELECT WBS1, MAX(TransDate) AS TransDate
			FROM [dbo].[LedgerEX] WITH (NOLOCK) 
			WHERE WBS1 = @WBS1
			GROUP BY WBS1

			UNION ALL

			SELECT WBS1, MAX(TransDate) AS TransDate
			FROM .[dbo].[LedgerAP] WITH (NOLOCK) 
			WHERE WBS1 = @WBS1
			GROUP BY WBS1
		) AS B
		GROUP BY WBS1
	) AS A
	ON A.WBS1 = p.WBS1

	WHERE p.WBS1 = @WBS1
	AND p.WBS2 = ' '


-- ##########   Phase (WBS2) level data   ##########

	Update p
	Set p.CustUnbilledAmountBilling = ISNULL(dbo.fnCCG_EI_JTDUnbilledBillingThru(p.WBS1,p.WBS2, p.WBS3, @lastDayOfPeriod),0.0),
		p.CustUnbilledTransactions = IsNull (ap.APQty, 0) + IsNull (ex.ExQty, 0) + IsNull (m.MiscQty, 0) + IsNull (l.LaborQty, 0)
	From ProjectCustomTabFields p
		Left Join (Select WBS1, WBS2, Sum (BillExt) APUnbilled, Count (*) APQty
					From LedgerAP WITH (NOLOCK)  Where BillStatus in ('B', 'H', 'D', 'W') and SuppressBill <> 'Y'
					Group by WBS1, WBS2) ap on p.WBS1 = ap.WBS1 and p.WBS2 = ap.WBS2
		Left Join (Select WBS1, WBS2, Sum (BillExt) ExUnbilled, Count (*) EXQty
					From LedgerEX WITH (NOLOCK)  Where BillStatus in ('B', 'H', 'D', 'W') and SuppressBill <> 'Y'
					Group by WBS1, WBS2)ex on p.WBS1 = ex.WBS1 and p.WBS2 = ex.WBS2
		Left Join (Select WBS1, WBS2, Sum (BillExt) MiscUnbilled, Count (*) MiscQty
					From LedgerMisc WITH (NOLOCK)  Where BillStatus in ('B', 'H', 'D', 'W') and SuppressBill <> 'Y'
					Group by WBS1, WBS2) m on p.WBS1 = m.WBS1 and p.WBS2 = m.WBS2
		Left Join (Select WBS1, WBS2, Sum (BillExt) LaborUnbilled, Count (*) LaborQty
					From LD WITH (NOLOCK)  Where BillStatus in ('B', 'H', 'D', 'W') and SuppressBill <> 'Y'
					Group by WBS1, WBS2) l on p.WBS1 = l.WBS1 and p.WBS2 = l.WBS2
	Where p.WBS1 = @WBS1
	and p.WBS2 <> ' '  -- Phase level only
	and p.WBS3 = ' '

	Update p
	Set p.CustJTDRevenue = IsNull (s.JTDRevenue, 0),
		p.CustYTDRevenue = IsNull (s.YTDRevenue, 0),
		p.CustJTDBilled = IsNull (s.JTDBilled, 0),
		p.CustJTDBilledTax = IsNull (t.Tax, 0),
		p.CustYTDBilled = IsNull (s.YTDBilled, 0),
		p.CustJTDReceived = IsNull (s.JTDReceived, 0),
		p.CustYTDReceived = IsNull (s.YTDReceived, 0),
		p.CustJTDARBalance = IsNull (s.JTDBilled, 0) + IsNull (t.Tax, 0) + IsNull (t.Interest, 0) - IsNull (s.JTDReceived, 0) - IsNull (i.InterestPaid, 0),
		p.CustJTDSpentBilling = IsNull (s.JTDSpentBilling, 0),
		p.CustYTDSpentBilling = IsNull (s.YTDSpentBilling, 0),
		p.CustJTDProfit = IsNull (s.JTDProfit, 0),
		p.CustYTDProfit = IsNull (s.YTDProfit, 0),
		p.CustJTDProfitPercent = Round (CASE When IsNull (s.JTDRevenue, 0) = 0 Then 0 Else IsNull (s.JTDProfit, 0) / s.JTDRevenue * 100 END, 2),
		p.CustYTDProfitPercent = Round (CASE When IsNull (s.YTDRevenue, 0) = 0 Then 0 Else IsNull (s.YTDProfit, 0) / s.YTDRevenue * 100 END, 2),
		p.CustJTDVariance = IsNull (s.JTDRevenue, 0) - IsNull (s.JTDSpentBilling, 0),
		p.CustYTDVariance = IsNull (s.YTDRevenue, 0) - IsNull (s.YTDSpentBilling, 0),
		p.CustJTDWorkinProcess = IsNull (s.WIP, 0)
	From ProjectCustomTabFields p
		Left Join
		(Select WBS1, WBS2,
			Sum (Revenue) JTDRevenue,
			Sum (CASE When ROUND(Period/100,0,1) = Year (getDate()) Then Revenue Else 0 END) YTDRevenue,
			Sum (Billed) JTDBilled,
			Sum (CASE When ROUND(Period/100,0,1) = Year (getDate()) Then Billed Else 0 END) YTDBilled,
			Sum (Received) JTDReceived,
			Sum (CASE When ROUND(Period/100,0,1) = Year (getDate()) Then Received Else 0 END) YTDReceived,
			Sum (SpentBilling) JTDSpentBilling,
			Sum (CASE When ROUND(Period/100,0,1) = Year (getDate()) Then SpentBilling Else 0 END) YTDSpentBilling,
			Sum (ProfitCostLessOH - Overhead) JTDProfit,
			Sum (CASE When ROUND(Period/100,0,1) = Year (getDate()) Then ProfitCostLessOH - Overhead Else 0 END) YTDProfit,
			Sum (Unbilled) WIP
		From PRSummaryMain WITH (NOLOCK) 
		Group by WBS1, WBS2) s on s.WBS1 = p.WBS1 and s.WBS2 = p.WBS2
		Left Join
		(Select WBS1, WBS2, 
				- Sum (CASE When InvoiceSection = 'T' Then Amount Else 0 END) Tax,
				- Sum (CASE When InvoiceSection = 'I' Then Amount Else 0 END) Interest
		From LedgerAR WITH (NOLOCK) 
		Where TransType = 'IN'
		  and InvoiceSection in ('I', 'T')
		Group by WBS1, WBS2
		Having Sum (Amount) <> 0) t on t.WBS1 = p.WBS1 and t.WBS2 = p.WBS2
		Left Join
		(Select WBS1, WBS2, - Sum (Amount) InterestPaid
		From LedgerAR WITH (NOLOCK) 
		Where TransType = 'CR'
		  and Account = '407.00' -- Interest Revenue
		Group by WBS1, WBS2
		Having Sum (Amount) <> 0) i on i.WBS1 = p.WBS1 and i.WBS2 = p.WBS2
	Where p.WBS1 = @WBS1
	  and p.WBS2 <> ' '  -- Phase level only
	  and p.WBS3 = ' '

-- JTD Multipliers
	Update p
	Set p.CustJTDGoalMultiplier = Round (IsNull (s.JTDGoalMult, 0), 2),
		p.CustJTDEffectiveMultiplier = Round (IsNull (s.JTDEffectiveMult, 0), 2)
	From ProjectCustomTabFields p
		Left Join
		(Select WBS1, WBS2,
			Sum (LaborBilling + DirectConsBilling + DirectOtherBilling + ReimbConsBilling + ReimbOtherBilling
				- DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) /
			Sum (LaborCost) JTDGoalMult,
			Sum (Revenue - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) /
			Sum (LaborCost) JTDEffectiveMult
		From PRSummarySub WITH (NOLOCK) 
		Group by WBS1, WBS2
		Having Sum (LaborCost) <> 0) s on s.WBS1 = p.WBS1 and s.WBS2 = p.WBS2
	Where p.WBS1 = @WBS1
	  and p.WBS2 <> ' '  -- Phase level only
	  and p.WBS3 = ' '

-- YTD Multipliers
	Update p
	Set p.CustYTDGoalMultiplier = Round (IsNull (s.YTDGoalMult, 0), 2),
		p.CustYTDEffectiveMultiplier = Round (IsNull (s.YTDEffectiveMult, 0), 2)
	From ProjectCustomTabFields p
		Left Join
		(Select WBS1, WBS2,
			Sum (LaborBilling + DirectConsBilling + DirectOtherBilling + ReimbConsBilling + ReimbOtherBilling
				 - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) /
			Sum (LaborCost) YTDGoalMult,
			Sum (Revenue - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) /
			Sum (LaborCost) YTDEffectiveMult
		From PRSummarySub WITH (NOLOCK) 
		Where ROUND(Period/100,0,1) = Year (getDate())
		Group by WBS1, WBS2
		Having Sum (LaborCost) <> 0) s on s.WBS1 = p.WBS1 and s.WBS2 = p.WBS2
	Where p.WBS1 = @WBS1
	  and p.WBS2 <> ' '  -- Phase level only
	  and p.WBS3 = ' '

-- CustLastChargeDate
	UPDATE p
	SET p.CustLastChargeDate = A.MaxTransDate
	FROM ProjectCustomTabFields p
	LEFT JOIN
	(
		SELECT WBS1, WBS2, MAX(TransDate) AS MaxTransDate
		FROM
		(
			SELECT WBS1, WBS2, MAX(TransDate) AS TransDate
			FROM [dbo].[LD] WITH (NOLOCK) 
			WHERE WBS1 = @WBS1
			GROUP BY WBS1, WBS2

			UNION ALL

			SELECT WBS1, WBS2, MAX(TransDate) AS TransDate
			FROM [dbo].[LedgerMisc] WITH (NOLOCK) 
			WHERE WBS1 = @WBS1
				and BillStatus is not null
			GROUP BY WBS1, WBS2

			UNION ALL

			SELECT WBS1, WBS2, MAX(TransDate) AS TransDate
			FROM [dbo].[LedgerEX] WITH (NOLOCK) 
			WHERE WBS1 = @WBS1
			GROUP BY WBS1, WBS2

			UNION ALL

			SELECT WBS1, WBS2, MAX(TransDate) AS TransDate
			FROM [dbo].[LedgerAP] WITH (NOLOCK) 
			WHERE WBS1 = @WBS1
			GROUP BY WBS1, WBS2
		) AS B
		GROUP BY WBS1, WBS2
	) AS A
	ON A.WBS1 = p.WBS1
	AND A.WBS2 = p.WBS2

	WHERE p.WBS1 = @WBS1
	AND p.WBS2 <> ' '
	AND p.WBS3 = ' '

-- ##########   Task (WBS3) level data   ##########

	Update p
	Set p.CustUnbilledAmountBilling =  ISNULL(dbo.fnCCG_EI_JTDUnbilledBillingThru(p.WBS1,p.WBS2, p.WBS3, @lastDayOfPeriod),0.0),
		p.CustUnbilledTransactions = IsNull (ap.APQty, 0) + IsNull (ex.ExQty, 0) + IsNull (m.MiscQty, 0) + IsNull (l.LaborQty, 0)
	From ProjectCustomTabFields p
		Left Join (Select WBS1, WBS2, WBS3, Sum (BillExt) APUnbilled, Count (*) APQty
					From LedgerAP WITH (NOLOCK)  Where BillStatus in ('B', 'H', 'D', 'W') and SuppressBill <> 'Y'
					Group by WBS1, WBS2, WBS3) ap on p.WBS1 = ap.WBS1 and p.WBS2 = ap.WBS2 and p.WBS3 = ap.WBS3
		Left Join (Select WBS1, WBS2, WBS3, Sum (BillExt) ExUnbilled, Count (*) EXQty
					From LedgerEX WITH (NOLOCK)  Where BillStatus in ('B', 'H', 'D', 'W') and SuppressBill <> 'Y'
					Group by WBS1, WBS2, WBS3)ex on p.WBS1 = ex.WBS1 and p.WBS2 = ex.WBS2 and p.WBS3 = ex.WBS3
		Left Join (Select WBS1, WBS2, WBS3, Sum (BillExt) MiscUnbilled, Count (*) MiscQty
					From LedgerMisc WITH (NOLOCK)  Where BillStatus in ('B', 'H', 'D', 'W') and SuppressBill <> 'Y'
					Group by WBS1, WBS2, WBS3) m on p.WBS1 = m.WBS1 and p.WBS2 = m.WBS2 and p.WBS3 = m.WBS3
		Left Join (Select WBS1, WBS2, WBS3, Sum (BillExt) LaborUnbilled, Count (*) LaborQty
					From LD WITH (NOLOCK)  Where BillStatus in ('B', 'H', 'D', 'W') and SuppressBill <> 'Y'
					Group by WBS1, WBS2, WBS3) l on p.WBS1 = l.WBS1 and p.WBS2 = l.WBS2 and p.WBS3 = l.WBS3
	Where p.WBS1 = @WBS1
	  and p.WBS3 <> ' '  -- Task level only

	Update p
	Set p.CustJTDRevenue = IsNull (s.JTDRevenue, 0),
		p.CustYTDRevenue = IsNull (s.YTDRevenue, 0),
		p.CustJTDBilled = IsNull (s.JTDBilled, 0),
		p.CustJTDBilledTax = IsNull (t.Tax, 0),
		p.CustYTDBilled = IsNull (s.YTDBilled, 0),
		p.CustJTDReceived = IsNull (s.JTDReceived, 0),
		p.CustYTDReceived = IsNull (s.YTDReceived, 0),
		p.CustJTDARBalance = IsNull (s.JTDBilled, 0) + IsNull (t.Tax, 0) + IsNull (t.Interest, 0) - IsNull (s.JTDReceived, 0) - IsNull (i.InterestPaid, 0),
		p.CustJTDSpentBilling = IsNull (s.JTDSpentBilling, 0),
		p.CustYTDSpentBilling = IsNull (s.YTDSpentBilling, 0),
		p.CustJTDProfit = IsNull (s.JTDProfit, 0),
		p.CustYTDProfit = IsNull (s.YTDProfit, 0),
		p.CustJTDProfitPercent = Round (CASE When IsNull (s.JTDRevenue, 0) = 0 Then 0 Else IsNull (s.JTDProfit, 0) / s.JTDRevenue * 100 END, 2),
		p.CustYTDProfitPercent = Round (CASE When IsNull (s.YTDRevenue, 0) = 0 Then 0 Else IsNull (s.YTDProfit, 0) / s.YTDRevenue * 100 END, 2),
		p.CustJTDVariance = IsNull (s.JTDRevenue, 0) - IsNull (s.JTDSpentBilling, 0),
		p.CustYTDVariance = IsNull (s.YTDRevenue, 0) - IsNull (s.YTDSpentBilling, 0),
		p.CustJTDWorkinProcess = IsNull (s.WIP, 0)
	From ProjectCustomTabFields p
		Left Join
		(Select WBS1, WBS2, WBS3,
			Sum (Unbilled) Unbilled,
			Sum (Revenue) JTDRevenue,
			Sum (CASE When ROUND(Period/100,0,1) = Year (getDate()) Then Revenue Else 0 END) YTDRevenue,
			Sum (Billed) JTDBilled,
			Sum (CASE When ROUND(Period/100,0,1) = Year (getDate()) Then Billed Else 0 END) YTDBilled,
			Sum (Received) JTDReceived,
			Sum (CASE When ROUND(Period/100,0,1) = Year (getDate()) Then Received Else 0 END) YTDReceived,
			Sum (SpentBilling) JTDSpentBilling,
			Sum (CASE When ROUND(Period/100,0,1) = Year (getDate()) Then SpentBilling Else 0 END) YTDSpentBilling,
			Sum (ProfitCostLessOH - Overhead) JTDProfit,
			Sum (CASE When ROUND(Period/100,0,1) = Year (getDate()) Then ProfitCostLessOH - Overhead Else 0 END) YTDProfit,
			Sum (Unbilled) WIP
		From PRSummaryMain WITH (NOLOCK) 
		Group by WBS1, WBS2, WBS3) s on s.WBS1 = p.WBS1 and s.WBS2 = p.WBS2 and s.WBS3 = p.WBS3
		Left Join
		(Select WBS1, WBS2, WBS3, 
				- Sum (CASE When InvoiceSection = 'T' Then Amount Else 0 END) Tax,
				- Sum (CASE When InvoiceSection = 'I' Then Amount Else 0 END) Interest
		From LedgerAR WITH (NOLOCK) 
		Where TransType = 'IN'
		  and InvoiceSection in ('I', 'T')
		Group by WBS1, WBS2, WBS3
		Having Sum (Amount) <> 0) t on t.WBS1 = p.WBS1 and t.WBS2 = p.WBS2 and t.WBS3 = p.WBS3
		Left Join
		(Select WBS1, WBS2, WBS3, - Sum (Amount) InterestPaid
		From LedgerAR WITH (NOLOCK) 
		Where TransType = 'CR'
		  and Account = '407.00' -- Interest Revenue
		Group by WBS1, WBS2, WBS3
		Having Sum (Amount) <> 0) i on i.WBS1 = p.WBS1 and i.WBS2 = p.WBS2 and i.WBS3 = p.WBS3
	Where p.WBS1 = @WBS1
	  and p.WBS3 <> ' ' -- Task level only

-- JTD Multipliers
	Update p
	Set p.CustJTDGoalMultiplier = Round (IsNull (s.JTDGoalMult, 0), 2),
		p.CustJTDEffectiveMultiplier = Round (IsNull (s.JTDEffectiveMult, 0), 2)
	From ProjectCustomTabFields p
		Left Join
		(Select WBS1, WBS2, WBS3,
			Sum (LaborBilling + DirectConsBilling + DirectOtherBilling + ReimbConsBilling + ReimbOtherBilling
				- DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) /
			Sum (LaborCost) JTDGoalMult,
			Sum (Revenue - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) /
			Sum (LaborCost) JTDEffectiveMult
		From PRSummarySub WITH (NOLOCK) 
		Group by WBS1, WBS2, WBS3
		Having Sum (LaborCost) <> 0) s on s.WBS1 = p.WBS1 and s.WBS2 = p.WBS2 and s.WBS3 = p.WBS3
	Where p.WBS1 = @WBS1
	  and p.WBS3 <> ' '  -- Task level only

-- YTD Multipliers
	Update p
	Set p.CustYTDGoalMultiplier = Round (IsNull (s.YTDGoalMult, 0), 2),
		p.CustYTDEffectiveMultiplier = Round (IsNull (s.YTDEffectiveMult, 0), 2)
	From ProjectCustomTabFields p
		Left Join
		(Select WBS1, WBS2, WBS3,
			Sum (LaborBilling + DirectConsBilling + DirectOtherBilling + ReimbConsBilling + ReimbOtherBilling
				 - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) /
			Sum (LaborCost) YTDGoalMult,
			Sum (Revenue - DirectConsCost - DirectOtherCost - ReimbConsCost - ReimbOtherCost) /
			Sum (LaborCost) YTDEffectiveMult
		From PRSummarySub WITH (NOLOCK) 
		Where ROUND(Period/100,0,1) = Year (getDate())
		Group by WBS1, WBS2, WBS3
		Having Sum (LaborCost) <> 0) s on s.WBS1 = p.WBS1 and s.WBS2 = p.WBS2 and s.WBS3 = p.WBS3
	Where p.WBS1 = @WBS1
	  and p.WBS3 <> ' '  -- Task level only

-- CustLastChargeDate
	UPDATE p
	SET p.CustLastChargeDate = A.MaxTransDate
	FROM ProjectCustomTabFields p
	LEFT JOIN
	(
		SELECT WBS1, WBS2, WBS3, MAX(TransDate) AS MaxTransDate
		FROM
		(
			SELECT WBS1, WBS2, WBS3, MAX(TransDate) AS TransDate
			FROM [dbo].[LD] WITH (NOLOCK) 
			WHERE WBS1 = @WBS1
			GROUP BY WBS1, WBS2, WBS3

			UNION ALL

			SELECT WBS1, WBS2, WBS3, MAX(TransDate) AS TransDate
			FROM [dbo].[LedgerMisc] WITH (NOLOCK) 
			WHERE WBS1 = @WBS1
				and BillStatus is not null
			GROUP BY WBS1, WBS2, WBS3

			UNION ALL

			SELECT WBS1, WBS2, WBS3, MAX(TransDate) AS TransDate
			FROM [dbo].[LedgerEX] WITH (NOLOCK) 
			WHERE WBS1 = @WBS1
			GROUP BY WBS1, WBS2, WBS3

			UNION ALL

			SELECT WBS1, WBS2, WBS3, MAX(TransDate) AS TransDate
			FROM [dbo].[LedgerAP] WITH (NOLOCK) 
			WHERE WBS1 = @WBS1
			GROUP BY WBS1, WBS2, WBS3
		) AS B
		GROUP BY WBS1, WBS2, WBS3
	) AS A
	ON A.WBS1 = p.WBS1
	AND A.WBS2 = p.WBS2
	AND A.WBS3 = p.WBS3

	WHERE p.WBS1 = @WBS1
	AND p.WBS2 <> ' '
	AND p.WBS3 <> ' '

-- Fee types rollup
--exec spCCG_ProjectJTD_UpdateFeeTypes

END
GO
