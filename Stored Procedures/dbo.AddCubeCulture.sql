SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Masarrat>
-- Create date: <01-June-2020,>
-- Description:	<add default data for culture in cubes>
-- =============================================
Create PROCEDURE [dbo].[AddCubeCulture]

@UICultureName varchar(10)  ='en-US'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


--- Delete all existing data of given culture
Delete  ETLTableVersion WHERE UICultureName = @UICultureName OR UICultureName IS NULL;

 print ' '
print 'Adding UICultureName Data  '  + @UICultureName
	--------------- Insert Records For UICultureName : en-US -------------------------

INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('consolidations',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('Eliminations',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('ProjectOverhead ',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('ConsolidationBudgets ',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('ConsolidationBudgetsDetail ',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('Contacts ',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('Employees ',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('DimPlanning',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('ARAging',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('ContractCredit',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('Contracts',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('PBW',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('FactPlanning',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('Labor',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('Expenses',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('LD',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('BILD',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('Client',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('CL',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('GL',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('ACCOUNT',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('GLBudget',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('GLBudgetDetail',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('Projects',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('Unit',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('Vendor',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('TimeAnalysis',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('Activity',@UICultureName,-1,-1)
INSERT INTO ETLTableVersion (TableName,UICultureName,LastProcessedVersion,CurrentVersion) VALUES ('ReportingGroup',@UICultureName,-1,-1)

END
GO
