SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[HAI_ProjectMilestoneDateSync]
    @WBS1 VARCHAR(30),
    @Field VARCHAR(255), -- Name of the date field
    @myDate DATE
AS
/**************************************************************************************
			Copyright (c) 2021 Halff Associates Inc. All rights reserved.

			Insert or Update Milestone End Date in the Project Milestone Table when 
			the cooresponding field is edited and Update the PR table for System 
			fields in the Milestone List.

			08/13/2021	Jeremy Baummer - Created
			08/16/2021  Jeremy Baummer - Adjusted to populate Milestone Grid from 
										 Pursit Tab date fields rather than require 
										 date already be in the grid.
			08/23/2021  Jeremy Baummer - Adjusted to insert system dates into the PR 
										 Fields before entering into the Milestone Grid.
			09/20/2022	Craig Anderson - Cleaned up code and removed insertion of notes into milestone grid w/ David Springer

***************************************************************************************/
SET NOCOUNT ON;
DECLARE @MSG VARCHAR(100) = @Field + ' auto populated from Pursuit Tab to ' + CONVERT(VARCHAR(30), @myDate, 111);

IF @myDate IS NULL
    RAISERROR('<h3>Date can not be null.</h3>', 16, 1);

BEGIN
    -- Simply updating the system date field will trigger Deltek to insert the milestone recod.
    IF @Field = 'SysEstStart'
    BEGIN
        UPDATE dbo.PR
        SET EstStartDate = @myDate,
            ModUser = 'Automated',
            ModDate = GETDATE()
        WHERE WBS1 = @WBS1
              AND WBS2 = ' ';
    END;
    ELSE -- Now, if this is not a SYSTEM date....
    BEGIN
        IF  EXISTS
        (
            SELECT 1
            FROM dbo.PRMilestone AS prm
            WHERE prm.WBS1 = @WBS1
                  AND prm.WBS2 = ' '
                  AND prm.Code = @Field
        )
        BEGIN
		    UPDATE dbo.PRMilestone
            SET EndDate = @myDate,
                Notes = 'Update to ' + @MSG,
                ModUser = 'Auto',
                ModDate = GETDATE()
            WHERE WBS1 = @WBS1
                  AND WBS2 = ' '
                  AND Code = @Field;
        END;
        ELSE -- The record already exists in the milestone table, so just update it.
        BEGIN
           INSERT INTO dbo.PRMilestone
            (
                WBS1,
                WBS2,
                WBS3,
                RecordID,
                Code,
                EndDate,
                CreateUser,
                CreateDate,
                ModUser,
                ModDate
            )
            VALUES
            (@WBS1, ' ', ' ', REPLACE(NEWID(), '-', ''), @Field, @myDate,'InsertPursuitMilestoneDate', GETDATE(), 'InsertPursuitMilestoneDate', GETDATE());
        END;
    END;
END;
GO
