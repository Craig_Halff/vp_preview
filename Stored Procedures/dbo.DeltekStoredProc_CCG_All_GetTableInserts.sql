SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_All_GetTableInserts] ( @tableName nvarchar(200), @inserts varchar(max), @singleInsert char(1)= 'Y', @checkExistence char(1)= 'N')
             AS EXEC spCCG_All_GetTableInserts @tableName,@inserts,@singleInsert,@checkExistence
GO
