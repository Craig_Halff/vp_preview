SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_OpportunityNoSalesIDIQ_Validation]
    @OpportunityID VARCHAR(32)
  , @ContractType VARCHAR(32)
AS
/* =======================================================================================
    Halff Associates, Inc.   All rights reserved.
    20200624 - Craig H. Anderson
            Validate that the Sales grid is empty for IDIQ/On Call/MSA/GSA type contracts.
   ======================================================================================= */
SET NOCOUNT ON;
    BEGIN
        IF EXISTS
            (
                SELECT 'x'
                FROM dbo.Opportunity o
                    LEFT JOIN
                        (
                            SELECT OpportunityID
                                 , SUM(CustSalesAmount) AS Sales
                            FROM dbo.Opportunities_Sales WITH ( NOLOCK )
                            GROUP BY OpportunityID
                        )            s
                        ON s.OpportunityID = o.OpportunityID
                WHERE o.OpportunityID = @OpportunityID
                      AND @ContractType <> 'Individual Project/Task Order'
                      AND ISNULL(s.Sales, 0) <> 0
            )
            RAISERROR('Sales must be zero for IDIQ/On Call/MSA/GSA type Contracts.                        ', 16, 1);
    END;
GO
