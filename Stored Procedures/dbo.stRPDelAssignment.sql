SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPDelAssignment]
  @strRowID nvarchar(255),
  @bitCalledFromRM bit = 0
AS

BEGIN -- Procedure stRPDelAssignment

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to delete a Assignment row and its associated rows.
-- If bCalledFromRM = True then use RP* tables, else PN* tables.
-- If the Assignment has JTD then only delete planned labor rows, if the Assignment does not also delete the Assignment.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strAssignmentID varchar(32)
  DECLARE @strResourceType varchar(1)
  DECLARE @strIDPrefix nvarchar(255)
  DECLARE @strResourceID nvarchar(20)
  DECLARE @strGenericResourceID nvarchar(20)
  DECLARE @strWBS1 nvarchar(30)
  DECLARE @strWBS2 nvarchar(30) = ' '
  DECLARE @strWBS3 nvarchar(30) = ' '
  DECLARE @strLaborCode nvarchar(14)

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime

  DECLARE @bitJTDExists bit
  DECLARE @bitPNBaselineExists bit
  
  -- Get JTD Date
   
  SELECT 
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE())))
    FROM CFGRMSettings

  -- Parse RowID

  SET @strResourceType = 
    CASE 
      WHEN CHARINDEX('~', @strRowID) = 0 
      THEN ''
      ELSE SUBSTRING(@strRowID, 1, 1)
    END

  SET @strIDPrefix = LEFT(@strRowID, CHARINDEX('|', @strRowID) - 1)

  IF (@strResourceType = 'E')
    BEGIN
      SET @strResourceID = REPLACE(@strIDPrefix, 'E~', '')
      SET @strGenericResourceID = NULL
    END
  ELSE IF (@strResourceType = 'G')
    BEGIN
      SET @strGenericResourceID = REPLACE(@strIDPrefix, 'G~', '')
      SET @strResourceID = NULL
    END
  ELSE
    BEGIN
      SET @strResourceID = NULL
      SET @strGenericResourceID = NULL
    END

  -- Parsing for TaskID.

  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

  -- Get WBS123 from task
  
    SELECT
      @strPlanID = PlanID,
      @strWBS1 = WBS1,
      @strWBS2 = ISNULL(WBS2, ' '),
      @strWBS3 = ISNULL(WBS3, ' '),
	  @strLaborCode = ISNULL(LaborCode, ' ')
      FROM PNTask
      WHERE TaskID = @strTaskID
  
  -- Figure out if there are any JTD for this assignment

  SELECT 
    @bitJTDExists = 
      CASE 
        WHEN 
          EXISTS(
            SELECT
              'x'
            FROM (			
				SELECT sum(LD.RegHrs + LD.OvtHrs + LD.SpecialOvtHrs) AS JTDHours		
				FROM LD
				  WHERE LD.WBS1 = @strWBS1 AND
						LD.WBS2 = @strWBS2 AND 
						LD.WBS3 = @strWBS3 AND 
						ISNULL(LD.LaborCode,' ') = @strLaborCode AND
						LD.ProjectCost = 'Y' AND 
						LD.TransDate <= @dtJTDDate AND 
						LD.Employee = @strResourceID 
						AND @strResourceID IS NOT NULL
				  GROUP BY LD.WBS1,LD.WBS2,LD.WBS3,LD.Employee           
				UNION ALL
				  SELECT sum(TD.RegHrs + TD.OvtHrs + TD.SpecialOvtHrs) AS JTDHours		
				  FROM tkDetail AS TD -- TD table has entries at only the leaf level.
					INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
				  WHERE TD.WBS1 = @strWBS1 AND
						TD.WBS2 = @strWBS2 AND 
						TD.WBS3 = @strWBS3 AND
						ISNULL(TD.LaborCode,' ') = @strLaborCode AND
						TD.Employee = @strResourceID AND
						TD.TransDate  <= @dtJTDDate AND 
						@strResourceID IS NOT NULL
				  GROUP BY TD.WBS1,TD.WBS2,TD.WBS3,TD.Employee      
				 UNION ALL
				  SELECT sum(TD.RegHrs + TD.OvtHrs + TD.SpecialOvtHrs) AS JTDHours	
				  FROM tsDetail AS TD INNER JOIN tsControl AS TC ON TD.Batch = TC.Batch AND TC.Posted = 'N'
				  WHERE TD.WBS1 = @strWBS1 AND
						TD.WBS2 = @strWBS2 AND 
						TD.WBS3 = @strWBS3 AND
						ISNULL(TD.LaborCode,' ') = @strLaborCode AND
						TD.Employee = @strResourceID AND 
						TD.TransDate  <= @dtJTDDate AND 
						@strResourceID IS NOT NULL
				 GROUP BY TD.WBS1,TD.WBS2,TD.WBS3,TD.Employee
			  ) AS tabLD
		  HAVING SUM (tabLD.JTDHours) > 0 )
	  THEN 1
	  ELSE 0
      END

  SELECT @strAssignmentID = A.AssignmentID
    FROM PNAssignment A
    WHERE A.TaskID = @strTaskID  AND
      (ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') AND 
       ISNULL(A.GenericResourceID, '|') = ISNULL(@strGenericResourceID, '|'))

  SELECT 
    @bitPNBaselineExists = 
      CASE 
        WHEN EXISTS(SELECT TimePhaseID FROM PNBaselineLabor BE WHERE BE.AssignmentID = @strAssignmentID)					
        THEN 1 
        ELSE 0 
      END

--******************************
--			Do deletes
--******************************

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

  BEGIN TRANSACTION

    DELETE FROM PNPlannedLabor WHERE AssignmentID = @strAssignmentID

    IF (@bitJTDExists = 0 AND @bitPNBaselineExists = 0)
      BEGIN
        DELETE FROM PNAssignment WHERE AssignmentID = @strAssignmentID
      END

    IF (@bitCalledFromRM = 1)
      BEGIN
 
        DELETE FROM RPPlannedLabor WHERE AssignmentID = @strAssignmentID

        IF (@bitJTDExists = 0 AND @bitPNBaselineExists = 0)
          BEGIN
            DELETE FROM RPAssignment WHERE AssignmentID = @strAssignmentID
          END

      END

    -- Update VesionID.

    EXECUTE dbo.stRPUpdateVersionID @strPlanID, @bitCalledFromRM 

    SET NOCOUNT OFF

  COMMIT

END -- stRPDelAssignment
GO
