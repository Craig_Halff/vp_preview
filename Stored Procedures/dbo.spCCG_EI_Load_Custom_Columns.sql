SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_EI_Load_Custom_Columns]
	@VISION_LANGUAGE varchar(10)
AS
BEGIN
	-- EXEC [dbo].[spCCG_EI_Load_Custom_Columns] 'en-US'
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SELECT T1.Seq, IsNull(IsNull(T2.Label, T3.Label), '') as Label, DataType, DisplayOrder, InitialValue, QuickLoad, WhichGrids,
			IsNull(DbColumnName,'') as DbColumnName, DatabaseField, ColumnUpdate, Width, Decimals, ViewRoles,
			EditRoles, RequiredRoles, RequiredExpression, EnabledWBS1, EnabledWBS2, EnabledWBS3, Status, ModDate,
			ModUser, Link, IsNull(IsNull(T2.Description, T3.Description), '') as Description, HighlightColor,
			IsNull(IsNull(T2.GroupName, T3.GroupName), '') AS GroupName
		FROM CCG_EI_ConfigCustomColumns T1
			left join CCG_EI_ConfigCustomColumnsDescriptions T2 on T2.Seq = T1.Seq and T2.UICultureName = @VISION_LANGUAGE
			left join CCG_EI_ConfigCustomColumnsDescriptions T3 on T3.Seq = T1.Seq and T3.UICultureName = 'en-US'
		ORDER BY DisplayOrder, T1.Seq
END
GO
