SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spCCG_PAT_Config_TaxCode_LoadObj]
	@VISION_LANGUAGE		varchar(10)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	SELECT c.Code, ISNULL(d.Description, c.Code) as Description, c.Rate, c.HighRate, c.LowRate, c.Company
        FROM CFGBillTaxesData c
            LEFT JOIN CFGBillTaxesDescriptions d on c.Code = d.Code and UICultureName = @VISION_LANGUAGE
		WHERE
			 c.ApplyToTransactionEntry = 'Y' AND c.Status = 'A'
        ORDER BY 1

	select d.Company,d.TaxCode,d.Seq from
	  CFGMainDataDefaultTaxCodes d
	  INNER JOIN CFGBillTaxesData c on d.TaxCode = c.Code and d.Company = c.Company
	  WHERE
		c.ApplyToTransactionEntry = 'Y' AND c.Status = 'A'
	  order by d.Company,d.Seq,d.TaxCode
END
GO
