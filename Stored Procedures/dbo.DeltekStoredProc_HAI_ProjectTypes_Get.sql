SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--------------------------------------------------------------------------
CREATE   PROCEDURE [dbo].[DeltekStoredProc_HAI_ProjectTypes_Get]
AS
BEGIN
  ------------------------------------------------------------------------
  SET NOCOUNT ON;

  ------------------------------------------------------------------------
  SELECT
    PT.UDIC_UID
   ,PT.CustomCurrencyCode
   ,PT.CreateUser
   ,PT.CreateDate
   ,PT.ModUser
   ,PT.ModDate
   ,PT.CustProjectTypeCode
   ,PT.CustProjectTypeName
   ,PT.CustProjectTypeCategory
   ,PT.CustProjectTypeSubCategory
   ,PT.CustSF330Code
   ,PT.CustSortOrder
  FROM
    dbo.UDIC_ProjectType AS PT
  ORDER BY
    PT.CustProjectTypeCategory    ASC
   ,PT.CustProjectTypeSubCategory ASC
   ,PT.CustSortOrder              ASC
  ------------------------------------------------------------------------
END
GO
