SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_PctCmplClearEIChanges] (@WBS1 Nvarchar(32), @WBS1List Nvarchar(max))
AS
BEGIN
	/* Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved. */
	-- [spCCG_EI_PctCmplClearEIChanges] @WBS1 = '2003005.00'
	SET NOCOUNT ON
	if isnull(@WBS1List, '') = '' delete from CCG_EI_FeePctCpl where WBS1 = @WBS1
	else begin
		declare @sql Nvarchar(max) = 'delete from CCG_EI_FeePctCpl where WBS1 in ('+@WBS1List+')'
		exec(@sql)
	end
	select @@ROWCOUNT as clearedRows
END
GO
