SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_Get_Invoice_Stage]
	@sDateDifference	varchar(100),
	@sRole				varchar(32),
	@user				Nvarchar(32),
	@employee			Nvarchar(20)
AS
BEGIN
	-- Copyright (c) 2019 EleVia Software and Central Consulting Group.  All rights reserved. 
	-- EXEC [dbo].[spCCG_EI_Get_Invoice_Stage] '99999999', 'ACCT', 'ADMIN', '00001'
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	declare @sql Nvarchar(max) = '', 
			@roleFieldsSql Nvarchar(max),
			@activeColumnsSql Nvarchar(max) = '',
			@custFieldsSelectSql Nvarchar(max) = '',
			@custFieldsGroupSql Nvarchar(max) = ''

	exec [spCCG_EI_GetSQLRoleFields] @sqlPart = @roleFieldsSql OUTPUT
	exec [spCCG_EI_GetSQLCustomColumns] 0, 9, @sRole, 0, 0, @sqlPart = @custFieldsSelectSql OUTPUT, @outerApplies = @activeColumnsSql OUTPUT, @sqlGroupBy = @custFieldsGroupSql OUTPUT
	set @roleFieldsSql = replace(replace(@roleFieldsSql, N'[:Employee]', @employee), N'[:User]', @user)
	set @custFieldsSelectSql = replace(replace(@custFieldsSelectSql, N'[:Employee]', @employee), N'[:User]', @user)
	set @activeColumnsSql = replace(replace(@activeColumnsSql, N'[:Employee]', @employee), N'[:User]', @user)

	set @sql = '
	SELECT PR.WBS1, PR.InvoiceStage as InvoiceStage '+@custFieldsSelectSql+'
		FROM CCG_EI PR
			left join CCG_EI_CustomColumns EI_Cust on EI_Cust.WBS1 = PR.WBS1 and EI_Cust.WBS2 = N'' '' and EI_Cust.WBS3 = N'' '' '
			+@activeColumnsSql+'
		WHERE DATEDIFF(SECOND, PR.DateChanged, GETUTCDATE()) < '+@sDateDifference
	exec(@sql)
END
GO
