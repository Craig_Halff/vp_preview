SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Save_Grids]
	@Grids_Columns				Nvarchar(max),
	@Grids_Updates				Nvarchar(max),
	@GridMultiProjectSelectMode	bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sSQL Nvarchar(max);
	DECLARE @safeSql	int;

	-- Ensure that our inputs are safe (preventing SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<CSV>', @Grids_Columns);
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<INSERT VALUES>', @Grids_Updates) * 2;
	IF @safeSql < 3 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS varchar(4));
		RETURN;
	END;

	BEGIN TRANSACTION;
	DELETE FROM CCG_EI_ConfigGrid;

	SET @sSQL = N'
		INSERT INTO CCG_EI_ConfigGrid (' + @Grids_Columns + N') Values ' + @Grids_Updates + N';';
	EXEC (@sSQL);

	IF @@ERROR <> 0 BEGIN
		ROLLBACK TRANSACTION;
		RETURN;
	END;

	UPDATE CCG_EI_Config SET GridMultiProjectSelectMode = @GridMultiProjectSelectMode;
	COMMIT TRANSACTION;
END;
GO
