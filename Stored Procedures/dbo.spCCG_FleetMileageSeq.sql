SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_FleetMileageSeq] @UDIC_UID varchar (32), @Seq varchar (32), @Date date
AS
/*
Copyright 2019 (c) Central Consulting Group.   All rights reserved.
08/02/2019  David Springer
            Generate CustSequence for order purposes
			Call this on a Mileage INSERT workflow.
*/
DECLARE @Sequence integer
BEGIN
SET NOCOUNT ON
	Set @Sequence = - convert (int, Right(convert (varchar(8), @Date, 112),6))

	Update UDIC_FleetManagement_MileageLog
	Set CustSequence = @Sequence
	Where UDIC_UID = @UDIC_UID
	  and Seq = @Seq

END
GO
