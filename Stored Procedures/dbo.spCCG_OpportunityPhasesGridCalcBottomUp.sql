SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_OpportunityPhasesGridCalcBottomUp] @OpportunityID varchar (32)
AS
/*
Copyright 2017 Central Consulting Group.   All rights reserved.
03/10/2017 David Springer
           Call this from an Opportunity Phase INSERT, CHANGE & DELETE workflows.
		   Called from spCCG_OpportunityTaskGridCalculations procedure.
*/
SET NOCOUNT ON
BEGIN
-- Update opportunity Revenue from Phases grid after Phases are up to date
   Update o
   Set o.Revenue = IsNull (p.Total, 0),
       o.WeightedRevenue = IsNull (p.Total, 0) * o.Probability / 100
   From Opportunity o, OpportunityCustomTabFields ox
        Left Join
       (Select OpportunityID, 
         Sum (CustPhaseTotal)  Total
	    From Opportunities_Phases
	    Group by OpportunityID) p on p.OpportunityID = ox.OpportunityID
   Where o.OpportunityID = @OpportunityID
     and o.OpportunityID = ox.OpportunityID

-- Update opportunity Custom Revenue from Phases grid after Phases are up to date
   Update ox
   Set ox.CustRevenueFeeDirLab =  IsNull (p.Labor, 0),
       ox.CustRevenueFeeDirExp =  IsNull (p.DirExp, 0),
       ox.CustRevenueConsultFee = IsNull (p.DirCons, 0),
       ox.CustRevenueReimbAllowExp =  IsNull (p.ReimbExp, 0),
       ox.CustRevenueReimbAllowCons = IsNull (p.ReimbCons, 0)
   From Opportunity o, OpportunityCustomTabFields ox
        Left Join
       (Select OpportunityID,
         Sum (CustPhaseFeeDirLab) Labor,
         Sum (CustPhaseFeeDirExp) DirExp,
         Sum (CustPhaseConsultFee) DirCons,
         Sum (CustPhaseReimbAllowExp) ReimbExp,
         Sum (CustPhaseReimbAllowCons) ReimbCons
	    From Opportunities_Phases
	    Group by OpportunityID) p on p.OpportunityID = ox.OpportunityID
   Where o.OpportunityID = @OpportunityID
     and o.OpportunityID = ox.OpportunityID

-- Calculate Phase Percent
   Update p
   Set p.CustPhasePercent = p.CustPhaseTotal / o.Revenue * 100
   From Opportunities_Phases p, Opportunity o
   Where p.OpportunityID = @OpportunityID
     and p.OpportunityID = o.OpportunityID
     and o.Revenue <> 0

-- If there are more than one Billing Type/Tax Checkbox used in all the Phases
-- Set the Project Billing Type = Sublevel Terms
   If 1 < (Select Count (Distinct CustPhaseBillingType + CustPhaseTax) 
           From Opportunities_Phases
           Where OpportunityID = @OpportunityID)
      or 'Lump Sum' = (Select distinct CustPhaseBillingType From Opportunities_Phases Where OpportunityID = @OpportunityID) 
      or 'Lump Sum by Task' = (Select distinct CustPhaseBillingType From Opportunities_Phases Where OpportunityID = @OpportunityID)
      or 'Cost Plus Max' = (Select distinct Left (CustPhaseBillingType, 13) From Opportunities_Phases Where OpportunityID = @OpportunityID)
      Begin
      Update OpportunityCustomTabFields
      Set CustBillingType = 'Sublevel Terms'
      Where OpportunityID = @OpportunityID
      End

-- If only one Billing Type/Tax Checkbox used in all the Phases
-- Set the Project Billing Type = Phase Billing Type
   If 1 = (Select Count (Distinct CustPhaseBillingType + CustPhaseTax) 
           From Opportunities_Phases
           Where OpportunityID = @OpportunityID
		     and CustPhaseBillingType not like 'Lump Sum%'
			 and CustPhaseBillingType not like 'Cost Plus%Max%') and 
      not exists (Select 'x' From Opportunities_Phases
	              Where OpportunityID = @OpportunityID
		            and (CustPhaseBillingType like 'Lump Sum%'
					 or  CustPhaseBillingType like 'Cost Plus%Max%'))
      Begin
      Update o
      Set o.CustBillingType = p.CustPhaseBillingType
	  From OpportunityCustomTabFields o, 
	      (Select distinct CustPhaseBillingType
		   From Opportunities_Phases
		   Where OpportunityID = @OpportunityID) p
      Where o.OpportunityID = @OpportunityID
      End
END
GO
