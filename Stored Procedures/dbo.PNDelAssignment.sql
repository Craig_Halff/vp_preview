SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PNDelAssignment]
  @strPlanID varchar(32),
  @strAssignmentID varchar(32)
AS

BEGIN -- Procedure PNDelAssignment

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to delete a Assignment row and its associated rows.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    
  DELETE EMActivity
    FROM EMActivity
    INNER JOIN Activity ON EMActivity.ActivityID = Activity.ActivityID
    INNER JOIN PNAssignment ON Activity.AssignmentID = PNAssignment.AssignmentID
    WHERE PNAssignment.PlanID = @strPlanID AND PNAssignment.AssignmentID = @strAssignmentID
  
  DELETE ActivityCustomTabFields 
    FROM ActivityCustomTabFields
    INNER JOIN Activity ON ActivityCustomTabFields.ActivityID = Activity.ActivityID
    INNER JOIN PNAssignment ON Activity.AssignmentID = PNAssignment.AssignmentID
    WHERE PNAssignment.PlanID = @strPlanID AND PNAssignment.AssignmentID = @strAssignmentID

  DELETE Activity 
    FROM Activity
    INNER JOIN PNAssignment ON Activity.AssignmentID = PNAssignment.AssignmentID
    WHERE PNAssignment.PlanID = @strPlanID AND PNAssignment.AssignmentID = @strAssignmentID
  
  DELETE PNPlannedLabor WHERE PlanID = @strPlanID AND AssignmentID = @strAssignmentID
  DELETE PNBaselineLabor WHERE PlanID = @strPlanID AND AssignmentID = @strAssignmentID
  DELETE PNAssignment WHERE PlanID = @strPlanID AND AssignmentID = @strAssignmentID

  SET NOCOUNT OFF

END -- PNDelAssignment
GO
