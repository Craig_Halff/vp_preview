SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_FirmOrgSync]
AS 
/* 
Copyright (c) 2020 Central Consulting Group.  All rights reserved.
03/16/2020	David Springer
			Sync Firms Associated Orgs configuration table with Organizations & Office info center.
			Call this from ascheduled workflow
*/
BEGIN
SET NOCOUNT ON

INSERT INTO dbo.FirmOrgAssociations
(FirmID, Org, CreateUser, CreateDate)
SELECT f.FirmID, o.Org, 'CCG Procedure', GETDATE()
FROM Organization o, dbo.UDIC_Office x, dbo.CFGFormat y, Firm f
WHERE SUBSTRING (o.org, y.Org1Length + 2, y.Org2Length) = x.CustOfficeNumber
  AND x.CustOfficeName = f.OfficeLocation
  AND o.Status = 'A'
  AND NOT EXISTS (SELECT 'x' FROM dbo.FirmOrgAssociations WHERE Org = o.Org)

END
GO
