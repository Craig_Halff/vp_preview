SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create Procedure [dbo].[GetExchangeRate] @fromCurrencyCode Nvarchar(3), @toCurrencyCode Nvarchar(3), @targetDate datetime, @rate float output, @inverse bit
as
Declare 
	@rateCursorEOF bit,
	@effectiveDate dateTime,
	@foundExchangeRate int
	
 
begin
	Select @rate = 0
	Select @foundExchangeRate = 0
	 
	declare rateCursor Cursor For 
		Select Rate, EffectiveDate From CFGCurrencyExchange Where
		FromCode = @fromCurrencyCode and ToCode = @toCurrencyCode 
		and @targetDate between StartDate and EndDate
	
	Open rateCursor
	
	Fetch Next From rateCursor into @rate, @effectiveDate
	Select @rateCursorEOF = @@FETCH_STATUS

	if @rateCursorEOF = 0
		Select @foundExchangeRate = 1
 	
	if @rate <> 0
	begin
		if @inverse = 1
			Select @rate = 1 / @rate
	end


	Close rateCursor
	Deallocate rateCursor
	
	return @foundExchangeRate
	
end

 
/* SP_InsertKeyConvertDriver.SQL */
/* Stored Procedure for key convert utility */
/* Last Updated : 11/5/2018 */

/* 2013 */
/* 1/11 Add InProcessAccount for CFGBanks table */
/* 3/27 Add UnbilledRealizedGains###, UnbilledRealizedLosses###, UnbilledUnrealizedGains###,UnbilledUnrealizedLosses### for CFGAutoPosting and Organization tablesn*/
/* 04/08 Add Biller for PR, PRTemplate tablesn*/
/* 08/06 Added codes to include fields in FW_UDIC table */
/* 08/06 Added column OriginatingVendor to apDetail, BIED, cvDetail, ekDetail, exDetail, LedgerAP, LedgerAR, LedgerEX, LedgerMisc */
/* 08/06 Added table BillingXferAudit */
/* 08/06 Added tables tkRevisionMaster and tkRevisionDetail */
/* 08/21 Added TaxCode and CompoundOnTaxCode for MTC tables */
/* 09/18 Remove TaxCode and CompoundOnTaxCode for MTC tables including exChecks and apppchecks */
/* 09/18 Added table exChecksTax, apppChecksTax */
/* 09/26 Fixed scripts for custom columns in UDIC */
/* 10/31 Added CFGTransAutoNumBank, CFGTransAutoNumType, and ReportPresentationCurrency, CFGEKCategoryTaxCodes, tkBillingXferReasons, tkRevExplanations */
/* 11/19 Added CFGDiaryAutoNum */
/* 11/20 Added CFGBillTaxesVATGridNumber, CFGBillTaxesICBillingExclusionAccounts */
/* 2014 */
/* 03/04 Replaced references to now obsolete CFGTimeAnalysisData and CFGTimeAnalysisDescriptions tables with reference to new CFGTimeAnalysis table, as former view has been removed */
/* 03/04 Replaced references to now obsolete CFGVendorTypeAPLiabilityData and CFGVendorTypeAPLiabilityDescriptions tables with reference to new CFGVendorTypeAPLiability table, as former view has been removed */
/* 05/14 Added Credit Card related tables : CFGCreditCard, CFGCreditCardSecondary ... */
/* 07/31 Revisions for split of CFGBillMain into CFGBillMainData and CFGBillMainDescriptions tables. */
/* 08/06 Fix the query for the UDIC column. */
/* 08/26 Added CFGServiceProfileCompanyMapping */
/* 09/26 Added 7.4 fields */
/* 11/21 Remove CFGPOApprover table. */
/* 2014 */
/* 02/18 Added columns Vendor and ContactIDForVendor to Activity and WorkflowActivity */
/* 2015 */
/* 04/30 Added columns Employee to ekDetailTax */
/* 06/05 Added columns FICAAPLiability */
/* 06/08 Added columns FICAAPBank */
/* 07/02 Added column Company to EMAccrual, EMAccrualDetail, EMDirectDeposit, EMEKGroups, EMLocale, EMPayrollContribution, EMPayrollWithholding, EMTKGroups, ekMaster, 
tkBreakTime, tkDetail, tkMaster, tkRevisionDetail, tkRevisionMaster and tkUnitDetail, EMPayroll, EMPayrollContributionWage, EMPayrollWithholdingWage, unDetail and upDetail */
/* 07/02 Added 7.5 fields */
/* 07/31 Remove EmployeeCompany for EMTKGroups and EMEKGroups fields */
/* 3/23/2016 JYC Add check for Employee Expense Report Approval */

/* 9/23/2016 DP Replace CL/VE with Clendor */
/* 11/16/2016 DP Add Tax for billTaxDetail table */
/* 12/16/2016 DP 1.0 changes */
/* 1/18/2017 DP more 1.0 changes */
/* 1/26/2017 DP remove VEProjectAssoc */
/* 4/28/2017 TW Add EMCitizenship for Employee*/
/* 5/1/2017 DP Add CustomTables.TableName for 'ClientCustomTabFields' */
/* 9/14/2017 DP Add Opportunity.ProposalManager, Opportunity.MarketingCoordinator, Opportunity.BusinessDeveloperLead, and Clendor.Owner for Employee Key Convert' */

/* 3/14/2018 DP Add RPPlan.PlanNumber and PNPlan.PlanNumber for WBS1 Key Convert' */
/* 5/16/2018 DP Add CFGRMSettings.DefaultCompany */
/* 6/13/2018 DP 2.0 changes */
/* 7/27/2018 DP Add InvoiceApprover */
/* 8/15/2018 DP Remove Opportunity related table */
/* 11/5/2018 DP Add PR.ProposalManager, PR.MarketingCoordinator, PR.BusinessDeveloperLead for Employee Key Convert' */
/* 7/17/2019 TW Add CFGIntegrationCompany for Company Key Convert */

Print ' '
Print 'Drop InsertKeyCvtDriver procedure (if it exists)...'
GO
