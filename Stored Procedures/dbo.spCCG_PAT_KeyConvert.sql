SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_KeyConvert](@What varchar(32), @OldValue Nvarchar(32), @NewValue Nvarchar(32),
	@WBS1 Nvarchar(30), @WBS2 Nvarchar(7))
AS BEGIN
	set nocount on
	declare @NumRowsAffected int, @resMessage varchar(max) = ''
	set @NumRowsAffected=0
	print @What
	BEGIN TRY
		BEGIN TRAN
		if @What = 'WBS1'
		begin
			update CCG_PAT_ProjectAmount		set WBS1=@NewValue where WBS1=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
		end
		else if @What = 'WBS2'
		begin
			update CCG_PAT_ProjectAmount		set WBS2=@NewValue where WBS1=@WBS1 and WBS2=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
		end
		else if @What = 'WBS3'
		begin
			update CCG_PAT_ProjectAmount		set WBS3=@NewValue where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
		end
		else if @What = 'Account'
		begin
			update CCG_PAT_ProjectAmount		set GLAccount=@NewValue where GLAccount=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
		end
		else if @What = 'Stage'
		begin
			update CCG_PAT_Payable					set Stage=@NewValue where Stage=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_PAT_Pending set Stage=@NewValue where Stage=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
		end
		else if @What = 'Vendor'
		begin
			-- 4.1 Check for duplicate vendor invoices
			declare @checkVendor char(1) = 'Y', @checkDate char(1) = 'Y', @checkCompany char(1) = 'Y'
			select @NumRowsAffected = -1, @resMessage = 'An existing invoice was found in [:source] matching Vendor ('+@NewValue+'), Invoice # ('+p.PayableNumber+'), Invoice Date ('+convert(varchar(10), p.PayableDate, 101)+').|'+duptables.Source 
				from (select Seq, PayableNumber, PayableType, @NewValue as Vendor, '' as Voucher, PayableDate, Company 
							from CCG_PAT_Payable py
							where py.Vendor = @OldValue
						union select -1, VO.Invoice, 'I', @NewValue as Vendor, '' as Voucher, VO.InvoiceDate, VO.Company
							from VO 
							where VO.Vendor = @OldValue
					) p
					outer apply (
						SELECT count(dupcount) as dupcount, MIN(Source) as Source, MAX(Source) as Source2
						FROM  (
							SELECT distinct vendor as dupcount, 'PAT' as source
								FROM CCG_PAT_Payable p2
								WHERE p2.PayableNumber = p.PayableNumber and p2.PayableType <> 'C'
									and p2.Seq <> p.Seq and (@checkVendor = 'N' or Vendor = p.Vendor)
									and (@checkDate = 'N' or PayableDate = p.PayableDate) and (@checkCompany = 'N' or Company = p.Company)
							UNION ALL
							/* SELECT distinct vendor as dupcount, 'BTransactionCenter' as source
								FROM apMaster Master
									inner join apControl Control on Master.Batch = Control.Batch
								WHERE Master.Invoice = p.PayableNumber and Master.posted = 'N'
									and (@checkVendor = 'N' or Vendor = p.Vendor) and (@checkDate = 'N' or InvoiceDate = p.PayableDate)
									and (ISNULL(p.Voucher,'') = '' OR Voucher <> p.Voucher) and (@checkCompany = 'N' or Company = p.Company)
							UNION All -- NOT DOING THIS AT THIS TIME [JAM] */
							SELECT distinct vendor as dupcount, 'LedgerAP' as source
								FROM VO
								WHERE Invoice = p.PayableNumber
									and (@checkVendor = 'N' or Vendor = p.Vendor) and (@checkDate = 'N' or InvoiceDate = p.PayableDate)
									and (ISNULL(p.Voucher,'') = '' OR Voucher <> p.Voucher) and (@checkCompany = 'N' or Company = p.Company)
						) x
					) as duptables
				where isnull(duptables.dupcount, 0) > 0

			if isnull(@NumRowsAffected, 0) = 0
			begin
				update CCG_PAT_Payable set Vendor=@NewValue where Vendor=@OldValue
				set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
				update CCG_PAT_Recur set Vendor=@NewValue where Vendor=@OldValue
				set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			end
		end
		else if @What = 'Employee'
		begin
			-- CCG_PAT_Pending
			UPDATE CCG_PAT_Pending set Employee=@NewValue where Employee=@OldValue
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT

			-- CCG_PAT_History
			UPDATE CCG_PAT_History set ActionTakenBy=@NewValue where ActionTakenBy=@OldValue
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			UPDATE CCG_PAT_History set ActionRecipient=@NewValue where ActionRecipient=@OldValue
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT

			-- CCG_PAT_ConfigOptions
			UPDATE CCG_PAT_ConfigOptions set ChangedBy = @NewValue where ChangedBy = @OldValue
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT

			-- CCG_PAT_HistoryUpdateColumns
			UPDATE CCG_PAT_HistoryUpdateColumns set ActionTakenBy = @NewValue where ActionTakenBy = @OldValue
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT

			-- CCG_PAT_Delegation
			UPDATE CCG_PAT_Delegation SET Employee = @NewValue WHERE Employee = @OldValue
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			UPDATE CCG_PAT_Delegation SET Delegate = @NewValue WHERE Delegate = @OldValue
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			UPDATE CCG_PAT_Delegation SET ApprovedBy = @NewValue WHERE ApprovedBy = @OldValue
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT

			-- CCG_PAT_Payable
			UPDATE CCG_PAT_Payable SET CreateUser = @NewValue WHERE CreateUser = @OldValue		-- These should be USER names not employee ids [JAM]
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			UPDATE CCG_PAT_Payable SET ModUser = @NewValue WHERE ModUser = @OldValue
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT

			-- CCG_PAT_DelegationPermanentApprovals
			/*
			UPDATE CCG_PAT_DelegationPermanentApprovals SET Employee = @NewValue WHERE Employee = @OldValue
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			UPDATE CCG_PAT_DelegationPermanentApprovals SET Delegate = @NewValue WHERE Delegate = @OldValue
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			UPDATE CCG_PAT_DelegationPermanentApprovals SET ApprovedBy = @NewValue WHERE ApprovedBy = @OldValue
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			*/

			-- CCG_PAT_HistoryDelegation
			UPDATE CCG_PAT_HistoryDelegation SET Employee = @NewValue WHERE Employee = @OldValue
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			UPDATE CCG_PAT_HistoryDelegation SET Delegate = @NewValue WHERE Delegate = @OldValue
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			UPDATE CCG_PAT_HistoryDelegation SET ActionTakenBy = @NewValue WHERE ActionTakenBy = @OldValue
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT

			-- CCG_PAT_Renotify
			UPDATE CCG_PAT_Renotify set Employee = @NewValue where Employee = @OldValue
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			UPDATE CCG_PAT_Renotify set ChangedBy = @NewValue where ChangedBy = @OldValue
			SET @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
		end
		COMMIT TRAN
		select @NumRowsAffected as ReturnValue, @resMessage as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		select -1 as  ReturnValue, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage
	END CATCH
END
GO
