SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AddIndustryDefaultsAEfrFR]
AS
BEGIN

PRINT ' '
PRINT 'Adding AE Industry Defaults for fr-FR'

BEGIN TRANSACTION

DELETE FROM BTLaborCatsDescriptions WHERE UICultureName = 'fr-FR';
--..........................................................................................v(50)
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 1, 'fr-FR','Principal'         WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 1);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 2, 'fr-FR','Chef de projet'   WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 2);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 3, 'fr-FR','Consultant senior' WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 3);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 4, 'fr-FR','Architecte senior'  WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 4);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 5, 'fr-FR','Ingénieur senior'   WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 5);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 6, 'fr-FR','Ingénieur'          WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 6);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 7, 'fr-FR','Architecte'         WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 7);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 8, 'fr-FR','Concepteur'          WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 8);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 9, 'fr-FR','Rapporteur'      WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 9);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 10,'fr-FR','Arpenteur'          WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 10);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 11,'fr-FR','Administratif'    WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 11);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 12,'fr-FR','Marketing'         WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 12);

DELETE FROM CFGActivitySubjectData WHERE UICultureName = 'fr-FR';
--..................................................................................v(50)
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Appel à froid'               ,'fr-FR',3 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Réunion initiale'        ,'fr-FR',1 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('E-mail d''introduction'      ,'fr-FR',4 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Appel de qualification'      ,'fr-FR',2 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Mention des médias sociaux'    ,'fr-FR',6 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('E-mail leadership pensé','fr-FR',5 ,'N');

DELETE FROM CFGActivityTypeDescriptions WHERE UICultureName = 'fr-FR';
--.........................................................................................................v(15)
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'EMail'     ,'fr-FR','E-mail'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'EMail');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Event'     ,'fr-FR','Événement'          ,4 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Event');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Mailing'   ,'fr-FR','Publipostage'        ,2 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Mailing');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Meeting'   ,'fr-FR','Réunion'        ,3 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Meeting');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Merge'     ,'fr-FR','Créer un Document',5 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Merge');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Milestone' ,'fr-FR','Jalon'      ,6 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Milestone');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Phone Call','fr-FR','Appel téléphonique'     ,7 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Phone Call');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Task'      ,'fr-FR','Tâche'           ,8 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Task');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Touchpoint','fr-FR','Point de contact'     ,9 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Touchpoint');

DELETE FROM CFGARLedgerHeadingsDescriptions WHERE UICultureName = 'fr-FR';
--...............................................................................................v(12)
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 1,'fr-FR','Honoraires'       WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 1);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 2,'fr-FR','Remb.'     WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 2);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 3,'fr-FR','Consultant' WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 3);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 4,'fr-FR','Provision'   WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 4);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 5,'fr-FR','Taxes'      WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 5);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 6,'fr-FR','Intérêts'   WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 6);

DELETE FROM CFGAwardTypeDescriptions WHERE UICultureName = 'fr-FR';
--............................................................................................v(255)
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '1','fr-FR','IDIQ - GWAC'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 1);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '2','fr-FR','IDIQ - Spécifique à l''agence',2 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 2);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '4','fr-FR','BPA'                   ,3 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 4);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '5','fr-FR','Autre'                 ,4 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 5);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '6','fr-FR','Tâche/Commande de livraison' ,5 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 6);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '7','fr-FR','Non déterminé'          ,6 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 7);

DELETE FROM CFGBillMainDescriptions WHERE UICultureName = 'fr-FR';
--..............................................................................................................................................................................................................v(15)..v(15)....v(15).........v(15)......v(15)...v(15).....v(15)..v(15)
INSERT INTO CFGBillMainDescriptions (Company,UICultureName,FooterMessage,FeeLabel,LabLabel,ConLabel,ExpLabel,UnitLabel,AddOnLabel,TaxLabel,InterestLabel,OvtIndicator) SELECT CFGMainData.Company,'fr-FR',NULL,'Honoraires' ,'Main-d''œuvre' ,'Consultant' ,'Frais' ,'Unité' ,'Supplément' ,'Taxe' ,'Intérêts' ,'Ovt' FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGBillMainData WHERE Company = CFGMainData.Company);

DELETE FROM CFGCampaignActionDescriptions WHERE UICultureName = 'fr-FR';
--..................................................................................................v(50)
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-FR','Développer le contenu' ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '01');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-FR','Envoyer l''invitation' ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '02');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-FR','Envoyer un ensemble d''e-mails',3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '03');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-FR','Suivi'       ,4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '04');

DELETE FROM CFGCampaignAudienceDescriptions WHERE UICultureName = 'fr-FR';
--....................................................................................................v(50)
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-FR','Cadres supérieurs'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '01');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-FR','Finance'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '02');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-FR','Marketing'           ,3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '03');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-FR','Développement d''activité',4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '04');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '05','fr-FR','Chefs de projets'    ,5 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '05');

DELETE FROM CFGCampaignObjectiveDescriptions WHERE UICultureName = 'fr-FR';
--.....................................................................................................v(50)
INSERT INTO CFGCampaignObjectiveDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-FR','Nouvelle génération d''entreprises' ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignObjectiveData WHERE Code = '01');
INSERT INTO CFGCampaignObjectiveDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-FR','Développer les activités existantes',2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignObjectiveData WHERE Code = '02');

DELETE FROM CFGCampaignStatusDescriptions WHERE UICultureName = 'fr-FR';
--..................................................................................................v(50)
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-FR','Actif'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '01');
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-FR','Planification' ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '02');
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-FR','Exécuté',3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '03');

DELETE FROM CFGCampaignTypeDescriptions WHERE UICultureName = 'fr-FR';
--................................................................................................v(50)
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-FR','E-mail'      ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '01');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-FR','Courrier direct',2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '02');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-FR','Webinaire'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '03');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-FR','Salon professionnel'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '04');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '05','fr-FR','Meetup'     ,5 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '05');

DELETE FROM CFGClientCurrentStatusDescriptions WHERE UICultureName = 'fr-FR';
--...............................................................................................................v(50)
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Existing','fr-FR','Existant',1 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Existing');
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Former'  ,'fr-FR','Ancien'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Former');
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Prospect','fr-FR','Client potentiel',2 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Prospect');

DELETE FROM CFGClientRelationshipDescriptions WHERE UICultureName = 'fr-FR';
--......................................................................................................v(50)
INSERT INTO CFGClientRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-FR','Concurrent',1 WHERE EXISTS (SELECT 'x' FROM CFGClientRelationshipData WHERE Code = '01');

DELETE FROM CFGClientRoleDescriptions WHERE UICultureName = 'fr-FR';
--....................................................................................................v(50)
INSERT INTO CFGClientRoleDescriptions (Code,UICultureName,Description,Seq) SELECT '01'      ,'fr-FR','Sous-traitant',2 WHERE EXISTS (SELECT 'x' FROM CFGClientRoleData WHERE Code = '01');
INSERT INTO CFGClientRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'sysOwner','fr-FR','Client'       ,1 WHERE EXISTS (SELECT 'x' FROM CFGClientRoleData WHERE Code = 'sysOwner');

DELETE FROM CFGClientTypeDescriptions WHERE UICultureName = 'fr-FR';
--..............................................................................................v(50)
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-FR','Formation'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '01');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-FR','Entreprise'          ,2 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '02');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-FR','Finance et Banque' ,3 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '03');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-FR','Santé et bien-être' ,4 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '04');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '05','fr-FR','Hospitalité'         ,5 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '05');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '06','fr-FR','Services d''information',6 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '06');

DELETE FROM CFGCompetitionTypeDescriptions WHERE UICultureName = 'fr-FR';
--...................................................................................................v(255)
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '37','fr-FR','8(a) Marché réservé'                                       ,2  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '37')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '38','fr-FR','Marché réservé SDB'                                        ,3  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '38')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '39','fr-FR','Marché réservé aux petites entreprises'                                  ,4  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '39')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '40','fr-FR','Ouvert et complet/illimité'                         ,5  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '40')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '44','fr-FR','Hubzone'                                              ,6  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '44')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '45','fr-FR','Petite entreprise détenue par un vétéran invalide'        ,7  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '45')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '47','fr-FR','Non déterminé'                                         ,1  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '47')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '48','fr-FR','Autre'                                                ,8  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '48')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '64','fr-FR','Source unique'                                          ,9  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '64')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '65','fr-FR','Marché réservé aux petites entreprises détenues par des femmes'                 ,10 WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '65')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '66','fr-FR','Petite entreprise détenue par un vétéran'                         ,11 WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '66')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '67','fr-FR','Petite entreprise détenue par les femmes économiquement défavorisées',12 WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '67')

DELETE FROM CFGContactRelationshipDescriptions WHERE UICultureName = 'fr-FR';
--.......................................................................................................v(50)
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-FR','Ancien client'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '01');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-FR','Ancien collaborateur' ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '02');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-FR','Pair de l''industrie'   ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '03');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-FR','Contact personnel',4 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '04');

DELETE FROM CFGContactRoleDescriptions WHERE UICultureName = 'fr-FR';
--.......................................................................................................v(50)
INSERT INTO CFGContactRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Contractor','fr-FR','Sous-traitant',1 WHERE EXISTS (SELECT 'x' FROM CFGContactRoleData WHERE Code = 'Contractor');
INSERT INTO CFGContactRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'sysOwner'  ,'fr-FR','Propriétaire'        ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactRoleData WHERE Code = 'sysOwner');

DELETE FROM CFGContactSourceDescriptions WHERE UICultureName = 'fr-FR';
--...................................................................................................v(50)
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '01','fr-FR','Référence du client'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '01');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '02','fr-FR','Événement'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '02');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '03','fr-FR','Liste'              ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '03');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '04','fr-FR','Contact personnel'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '04');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '05','fr-FR','Médias sociaux'      ,5 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '05');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '06','fr-FR','Leadership éclairé',6 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '06');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '07','fr-FR','Site Web'           ,7 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '07');

DELETE FROM CFGContactTitleDescriptions WHERE UICultureName = 'fr-FR';
--.................................................................................................v(50)
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Director' ,'fr-FR','Directeur' ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Director');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Executive','fr-FR','Executive',1 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Executive');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Finance'  ,'fr-FR','Finance'  ,5 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Finance');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Marketing','fr-FR','Marketing',6 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Marketing');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Sales'    ,'fr-FR','Ventes'    ,4 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Sales');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'VP'       ,'fr-FR','VP'       ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'VP');

DELETE FROM CFGContactTypeDescriptions WHERE UICultureName = 'fr-FR';
--..........................................................................................v(100)
INSERT INTO CFGContactTypeDescriptions (Code,UICultureName,Description) SELECT 'C','fr-FR','Client' WHERE EXISTS (SELECT 'x' FROM CFGContactTypeData WHERE Code = 'C');

DELETE FROM CFGContractStatusDescriptions WHERE UICultureName = 'fr-FR';
--..................................................................................................v(50)
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-FR','Examen interne'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '01');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-FR','Envoyé au client'   ,2 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '02');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-FR','Négociation'      ,3 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '03');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-FR','Examen juridique'     ,4 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '04');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '05','fr-FR','Signé et exécuté',5 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '05');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '06','fr-FR','Affaire perdue/aucune affaire'     ,6 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '06');

DELETE FROM CFGContractTypeDescriptions WHERE UICultureName = 'fr-FR';
--................................................................................................v(50)
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-FR','Originaux'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '01');
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-FR','Modifier la commande'       ,2 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '02');
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-FR','Services supplémentaires',3 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '03');
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-FR','Lettre d''entente',4 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '04');

DELETE FROM CFGCubeTranslationDescriptions WHERE UICultureName = 'fr-FR';
--..............................................................................................................................................v(200)
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'U'                       ,'fr-FR','Inconnu');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'UM'                      ,'fr-FR','Mois inconnu');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'H'                       ,'fr-FR','Historique');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('YesNo'          ,'Y'                       ,'fr-FR','Oui');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('YesNo'          ,'N'                       ,'fr-FR','Non');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('Status'         ,'A'                       ,'fr-FR','Actif');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('Status'         ,'I'                       ,'fr-FR','Inactif');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'1-Asset'                 ,'fr-FR','Actif');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'2-Liability'             ,'fr-FR','Passif');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'3-NetWorth'              ,'fr-FR','Valeur nette');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'4-Revenue'               ,'fr-FR','Revenus');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'5-Reimbursable'          ,'fr-FR','remboursable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'6-ReimbursableConsultant','fr-FR','Hon. remb. de consultants');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'7-Direct'                ,'fr-FR','direct');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'8-DirectConsultant'      ,'fr-FR','Consultant direct');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'9-Indirect'              ,'fr-FR','Indirect');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'10-OtherCharges'         ,'fr-FR','Autres débits');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'U-Unknown'               ,'fr-FR','Inconnu');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ExpenseType'    ,'5-ReimbursableOther'     ,'fr-FR','Remboursables - Autres');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ExpenseType'    ,'7-DirectOther'           ,'fr-FR','Frais directs - Autres');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ReportType'     ,'BalanceSheet'            ,'fr-FR','Bilan');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ReportType'     ,'IncomeStatement'         ,'fr-FR','Déclaration de revenus');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'1-Asset','fr-FR'         ,'Other Assets');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'2-Liability'             ,'fr-FR','Autres passifs');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'3-NetWorth'              ,'fr-FR','Autre valeur nette');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'4-Revenue'               ,'fr-FR','Autre revenu');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'5-Reimbursable'          ,'fr-FR','Autres remboursements');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'6-Reimbursable'          ,'fr-FR','Autres remboursements');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'7-Direct'                ,'fr-FR','Autres coûts directs');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'8-Direct'                ,'fr-FR','Autres coûts directs');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'9-Indirect'              ,'fr-FR','Autres coûts indirects');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'10-OtherCharges'         ,'fr-FR','Autres débits');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'B'                       ,'fr-FR','Facturable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'H'                       ,'fr-FR','En suspens');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'W'                       ,'fr-FR','À amortir');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'X'                       ,'fr-FR','Amortir');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'D'                       ,'fr-FR','Marqué pour la suppression');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'N'                       ,'fr-FR','Non facturable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'F'                       ,'fr-FR','Facturation finale');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'T'                       ,'fr-FR','Transféré(es)');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'R'                       ,'fr-FR','Partiellement suspendu/libéré');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'M'                       ,'fr-FR','Modifié');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'O'                       ,'fr-FR','Supprimé');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AP'                      ,'fr-FR','Bons CF');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BA'                      ,'fr-FR','Traitement des heures en avantages');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BE'                      ,'fr-FR','Transferts des frais de facturation');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BL'                      ,'fr-FR','Transferts de mains-d''oeuvre pour la facturation');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BU'                      ,'fr-FR','Transferts de facturation d''[Unit]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CD'                      ,'fr-FR','Sorties de fonds');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CP'                      ,'fr-FR','Convertir l''[Organization] de [WBS1]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CR'                      ,'fr-FR','Encaissements');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CV'                      ,'fr-FR','Sorties CF');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EP'                      ,'fr-FR','Traitement des paiements de l''[Employee]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','ER'                      ,'fr-FR','Remboursements des [Employee]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EX'                      ,'fr-FR','Frais des [Employee]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IN'                      ,'fr-FR','Factures');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','JE'                      ,'fr-FR','Écritures de journal');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','KC'                      ,'fr-FR','Conversion clé');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','LA'                      ,'fr-FR','Rajustements de la main-d''œuvre');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','LG'                      ,'fr-FR','Gains/pertes et réévaluations');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','MI'                      ,'fr-FR','Frais divers');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PP'                      ,'fr-FR','Traitement des paiements CF');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PR'                      ,'fr-FR','Impressions et reproductions');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PY'                      ,'fr-FR','Traitement de la paie');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','RG'                      ,'fr-FR','Génération de revenu');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','TS'                      ,'fr-FR','Feuilles de temps');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','UN'                      ,'fr-FR','[Units]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','UP'                      ,'fr-FR','[Units] par [WBS1]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AL'                      ,'fr-FR','Rapport de conversion clé, ouverture d''un nouvel exercice/trimestre/an de régularisation des avantages, fermeture d''une période');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AX'                      ,'fr-FR','Document commercial CF converti');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CN'                      ,'fr-FR','Frais de consultant convertis');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CT'                      ,'fr-FR','Transaction indiquant la date d''exécution de la conversion d''un appel de livraison précédent');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EC'                      ,'fr-FR','Ajustement(s) d''analyse de temps convertie');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IH'                      ,'fr-FR','Facture convertie');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IX'                      ,'fr-FR','Facture(s) convertie(s)');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','JX'                      ,'fr-FR','Conversion MCFMS');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PX'                      ,'fr-FR','Traitement des paiements CF convertis');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','RX'                      ,'fr-FR','Conversion MCFMS');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','XD'                      ,'fr-FR','Données de compte converties');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','XE'                      ,'fr-FR','Frais du [WBS1] convertis');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HA'                      ,'fr-FR','Historiques des soldes de [Account]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HB'                      ,'fr-FR','[Employee] - Historique de régularisation des prestations');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HE'                      ,'fr-FR','[WBS1] - Historique main-d''œuvre et frais');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HL'                      ,'fr-FR','[WBS1] - Historique main-d''œuvre et frais');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HU'                      ,'fr-FR','Revenus non facturés');

DELETE FROM CFGEMDegreeDescriptions WHERE UICultureName = 'fr-FR';
--............................................................................................v(50)
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-FR','Licence en sciences',1 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '01');
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-FR','Licence en arts'   ,2 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '02');
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-FR','Masters'            ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '03');

DELETE FROM CFGEmployeeRelationshipDescriptions WHERE UICultureName = 'fr-FR';
--..............................................................................................................v(50)
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01'      ,'fr-FR','Ancien employeur'            ,3 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '01');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '02'      ,'fr-FR','Ancien collaborateur'            ,2 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '02');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '03'      ,'fr-FR','Contact personnel'           ,5 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '03');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '04'      ,'fr-FR','Connexion générale de l''industrie',4 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '04');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT 'SysOwner','fr-FR','Propriétaire'                      ,1 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = 'SysOwner');

DELETE FROM CFGEmployeeRoleDescriptions WHERE UICultureName = 'fr-FR';
--........................................................................................................v(50)
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Admin'     ,'fr-FR','Administratif'         ,7  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Admin');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'AirQuality','fr-FR','Spécialiste de la qualité de l''air' ,8  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'AirQuality');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Architect' ,'fr-FR','Architecte'              ,9  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Architect');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Associate' ,'fr-FR','Associer'              ,10 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Associate');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Biologist' ,'fr-FR','Biologiste'              ,11 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Biologist');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'CADDTechi' ,'fr-FR','Technicien CADD'        ,12 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'CADDTechi');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'CivilEngr' ,'fr-FR','Ingénieur civil'         ,13 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'CivilEngr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ConstIns'  ,'fr-FR','Inspecteur de la construction' ,14 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ConstIns');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ConstMgr'  ,'fr-FR','Directeur de travaux'   ,15 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ConstMgr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ElectEngr' ,'fr-FR','Ingénieur électricien'    ,16 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ElectEngr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'EntitleSpl','fr-FR','Spécialiste des droits' ,17 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'EntitleSpl');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'EnvirPlnr' ,'fr-FR','Planificateur environnemental'  ,18 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'EnvirPlnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Estimator' ,'fr-FR','Estimateur'              ,19 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Estimator');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'GISSpecial','fr-FR','Spécialiste en SIG'         ,20 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'GISSpecial');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Hydrolgst' ,'fr-FR','Hydrologue'            ,21 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Hydrolgst');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'InfoTech'  ,'fr-FR','Technologie de l’information' ,22 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'InfoTech');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'InteDsnr'  ,'fr-FR','Architecte d''intérieur'      ,23 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'InteDsnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'LandArch'  ,'fr-FR','Architecte paysagiste'    ,24 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'LandArch');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'MechEng'   ,'fr-FR','Ingénieur mécanicien'    ,25 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'MechEng');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'MEPCoordin','fr-FR','Coordonnatrice MEP'        ,26 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'MEPCoordin');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Planner'   ,'fr-FR','Planificateur'                ,27 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Planner');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjAdmin' ,'fr-FR','Administrateur de projet'  ,28 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjAdmin');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjArch'  ,'fr-FR','Architecte de projet'      ,29 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjArch');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjDsnr'  ,'fr-FR','Concepteur de projets'       ,30 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjDsnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjEng'   ,'fr-FR','Ingénieur de projet'       ,31 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjEng');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjPlnr'  ,'fr-FR','Planificateur de projet'        ,32 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjPlnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjSvor'  ,'fr-FR','Arpenteur de projet'       ,33 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjSvor');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'QAQCMgr'   ,'fr-FR','Responsable AQ/QC'          ,34 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'QAQCMgr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'SpecWriter','fr-FR','Rédacteur de spécifications'  ,35 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'SpecWriter');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'StructEng' ,'fr-FR','Ingénieur en structures'    ,36 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'StructEng');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Surveyor'  ,'fr-FR','Arpenteur'               ,37 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Surveyor');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TechEditor','fr-FR','Rédacteur technique'       ,38 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TechEditor');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TrafficSpe','fr-FR','Spécialiste de la circulation'     ,39 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TrafficSpe');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TransEngr' ,'fr-FR','Ingénieur des transports',40 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TransEngr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TransPlnr' ,'fr-FR','Planificateur de transport' ,41 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TransPlnr');

DELETE FROM CFGEmployeeTitleDescriptions WHERE UICultureName = 'fr-FR';
--..................................................................................................v(50)
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Architect','fr-FR','Architecte'              ,1  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Architect');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CFO'      ,'fr-FR','Directeur financier',2  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CFO');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CivilEng' ,'fr-FR','Ingénieur civil'         ,3  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CivilEng');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Designer' ,'fr-FR','Concepteur'               ,4  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Designer');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Director' ,'fr-FR','Directeur'               ,5  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Director');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'EE'       ,'fr-FR','Ingénieur environnement' ,8  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'EE');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'EIT'      ,'fr-FR','Ingénieur en formation'   ,7  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'EIT');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Engineer' ,'fr-FR','Ingénieur'               ,6  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Engineer');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'GE'       ,'fr-FR','Ingénieur géotechnique'  ,10 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'GE');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'GM'       ,'fr-FR','Directeur général'        ,9  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'GM');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'ID'       ,'fr-FR','Architecte d''intérieur'      ,12 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'ID');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'LA'       ,'fr-FR','Architecte paysagiste'    ,13 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'LA');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'PIC'      ,'fr-FR','Chef de projet principal'    ,17 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'PIC');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Planner'  ,'fr-FR','Planificateur'                ,14 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Planner');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'PM'       ,'fr-FR','Chef de projet'        ,18 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'PM');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'President','fr-FR','Président'              ,15 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'President');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Principal','fr-FR','Principal'              ,16 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Principal');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'SUP'      ,'fr-FR','Superviseur'            ,20 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'SUP');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'TC'       ,'fr-FR','Consultant en formation'    ,19 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'TC');

DELETE FROM CFGEmployeeTypeDescriptions WHERE UICultureName = 'fr-FR';
--.....................................................................................v(30)
INSERT INTO CFGEmployeeTypeDescriptions (Type,UICultureName,Label) SELECT 'E','fr-FR','Employé'  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTypeData WHERE Type = 'E');
INSERT INTO CFGEmployeeTypeDescriptions (Type,UICultureName,Label) SELECT 'P','fr-FR','Principal' WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTypeData WHERE Type = 'P');

DELETE FROM CFGEMRegistrationDescriptions WHERE UICultureName = 'fr-FR';
--...................................................................................................v(70)
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'AIA','fr-FR','American Institute of Architects',2 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'AIA');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'EIT','fr-FR','Ingénieur en formation'            ,5 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'EIT');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'PE' ,'fr-FR','Ingénieur professionnel'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'PE');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'PLS','fr-FR','Arpenteur-géomètre professionnel'      ,4 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'PLS');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'RLS','fr-FR','Arpenteur-géomètre agréé'        ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'RLS');

DELETE FROM CFGEMSkillDescriptions WHERE UICultureName = 'fr-FR';
--.............................................................................................v(70)
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '01'  ,'fr-FR','Administratif'                          ,1  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '01SF','fr-FR','Ingénieur acoustique'                     ,2  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '02'  ,'fr-FR','Architectes'                              ,3  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '02');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '03SF','fr-FR','Photographe aérien'                     ,4  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '03SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '04SF','fr-FR','Ingénieur aéronautique'                   ,5  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '04SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '05'  ,'fr-FR','Dessinateurs'                               ,6  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '05');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '05SF','fr-FR','Archéologue'                            ,7  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '05SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '07'  ,'fr-FR','Inspecteurs en construction'                 ,8  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '07');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '07SF','fr-FR','Biologiste'                               ,9  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '07SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '08'  ,'fr-FR','Estimateurs'                              ,10 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '08');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '08SF','fr-FR','Technicien CADD'                         ,11 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '08SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '09SF','fr-FR','Cartographe'                            ,12 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '09SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '10'  ,'fr-FR','Ingénieurs sanitaires'                      ,13 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '10');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '11'  ,'fr-FR','Économistes'                              ,14 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '11');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '11SF','fr-FR','Chimiste'                                 ,15 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '11SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '12'  ,'fr-FR','Écologistes'                              ,16 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '12');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '13SF','fr-FR','Ingénieur des communications'                 ,17 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '13SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '14'  ,'fr-FR','Ingénieurs chimistes'                      ,18 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '14');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '14SF','fr-FR','Programmeur'                     ,19 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '14SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '15'  ,'fr-FR','Ingénieurs civils'                         ,20 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '15');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '16SF','fr-FR','Directeur de travaux'                    ,21 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '16SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '17SF','fr-FR','Ingénieur de corrosion'                      ,22 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '17SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '18SF','fr-FR','Ingénieur/estimateur de coûts'                 ,23 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '18SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '19'  ,'fr-FR','Ingénieurs électriciens'                    ,24 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '19');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '21'  ,'fr-FR','Arpenteurs'                               ,25 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '21');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '22SF','fr-FR','Ingénieur en électronique'                    ,26 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '22SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '23SF','fr-FR','Ingénieur environnement'                  ,27 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '23SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '24SF','fr-FR','Scientifique de l''environnement'                 ,28 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '24SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '25SF','fr-FR','Ingénieur en sécurité incendie'                ,29 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '25SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '26SF','fr-FR','Ingénieur légiste'                       ,30 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '26SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '27'  ,'fr-FR','Ingénieurs mécaniciens'                    ,31 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '27');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '27SF','fr-FR','Ingénieur en fondation/géotechnique'        ,32 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '27SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '28'  ,'fr-FR','Ingénieurs miniers'                        ,33 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '28');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '28SF','fr-FR','Arpenteur géodésique'                       ,34 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '28SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '29SF','fr-FR','Spécialiste des systèmes d''information géographique',35 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '29SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '30'  ,'fr-FR','Ingénieurs des sols'                         ,36 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '30');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '31'  ,'fr-FR','Rédacteurs de cahier des charges'                  ,37 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '31');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '31SF','fr-FR','Planificateur des établissements de santé'                 ,38 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '31SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '32'  ,'fr-FR','Ingénieurs structurels'                    ,39 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '32');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '32SF','fr-FR','Ingénieur hydraulique'                      ,40 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '32SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '33'  ,'fr-FR','Ingénieurs en transport'                ,41 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '33SF','fr-FR','Arpenteur hydrographique'                   ,42 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '35SF','fr-FR','Ingénieur industriel'                     ,43 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '36'  ,'fr-FR','Géologue'                               ,44 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '36SF','fr-FR','Hygiéniste industriel'                    ,45 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '38SF','fr-FR','Arpenteur foncier'                           ,46 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '40'  ,'fr-FR','Hydrologues'                            ,47 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '40SF','fr-FR','Ingénieur matériaux'                      ,48 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '41SF','fr-FR','Ingénieur en manutention de matériaux'             ,49 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '42'  ,'fr-FR','Architectes d''intérieur'                      ,50 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '43'  ,'fr-FR','Architectes paysagistes'                    ,51 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '44'  ,'fr-FR','Océanographes'                          ,52 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '45'  ,'fr-FR','Planificateurs : urbain/régional'                ,53 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '45SF','fr-FR','Interprète photo'                       ,54 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '46SF','fr-FR','Photogrammétriste'                        ,55 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '48SF','fr-FR','Chef de projet'                         ,56 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '49SF','fr-FR','Spécialiste de la télédétection'               ,57 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '50SF','fr-FR','Évaluateur des risques'                           ,58 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '51SF','fr-FR','Ingénieur en sécurité/santé au travail'     ,59 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '53SF','fr-FR','Planificateur'                               ,60 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '54SF','fr-FR','Spécialiste de la sécurité'                     ,61 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '58SF','fr-FR','Technicien/analyste'                      ,62 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '59SF','fr-FR','Toxicologue'                            ,63 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '61SF','fr-FR','Ingénieur valeur'                          ,64 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '62SF','fr-FR','Ingénieur en ressources hydrauliques'                ,65 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');

DELETE FROM CFGEMSkillLevelDescriptions WHERE UICultureName = 'fr-FR';
--.............................................................................................v(50)
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 1,'fr-FR','Élémentaire'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 1);
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 2,'fr-FR','Avancé',2 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 2);
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 3,'fr-FR','Expert'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 3);

DELETE FROM CFGEMSkillUsageDescriptions WHERE UICultureName = 'fr-FR';
--.............................................................................................v(255)
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 1,'fr-FR','Saisie'        ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 1);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 2,'fr-FR','1-2 ans'    ,2 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 2);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 3,'fr-FR','3-5 ans'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 3);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 4,'fr-FR','6-10 ans'   ,4 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 4);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 5,'fr-FR','Plus de 10 ans',5 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 5);

DELETE FROM CFGLeadRatingDescriptions WHERE UICultureName = 'fr-FR';
--..............................................................................................v(50)
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-FR','Chaud' ,1 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '01');
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-FR','Tiède',2 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '02');
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-FR','Froid',3 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '03');

DELETE FROM CFGOpportunityClosedReasonDescriptions WHERE UICultureName = 'fr-FR';
--...........................................................................................................v(50)
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-FR','Prix'    ,1 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '01');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-FR','Services' ,2 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '02');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-FR','Ressources',3 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '03');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-FR','Calendrier'   ,4 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '04');

DELETE FROM CFGOpportunitySourceDescriptions WHERE UICultureName = 'fr-FR';
--...........................................................................................................v(50)
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'CRef'    ,'fr-FR','Référence du client'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'CRef');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Event'   ,'fr-FR','Événement'                     ,2 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Event');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'List'    ,'fr-FR','Liste'                      ,5 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'List');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'PersCont','fr-FR','Contact personnel'          ,6 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'PersCont');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Social'  ,'fr-FR','Médias sociaux'              ,7 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Social');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'TLead'   ,'fr-FR','Leadership éclairé'        ,8 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'TLead');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Web'     ,'fr-FR','Web'                       ,9 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Web');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'WIQ'     ,'fr-FR','GovWin IQ'                 ,3 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'WIQ');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'WOM'     ,'fr-FR','Gestionnaire d''occasions GovWin',4 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'WOM');

DELETE FROM CFGPrefixDescriptions WHERE UICultureName = 'fr-FR';
--........................................................................................v(10)
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Dr.'  ,'fr-FR','Dr'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Dr.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Miss' ,'fr-FR','Mlle' ,2 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Miss');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Mr.'  ,'fr-FR','M.'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Mr.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Mrs.' ,'fr-FR','Mme' ,4 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Mrs.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Ms.'  ,'fr-FR','Mme'  ,5 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Ms.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Prof.','fr-FR','Prof.',6 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Prof.');

DELETE FROM CFGProbabilityDescriptions WHERE UICultureName = 'fr-FR';
--.....................................................................................................v(50)
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 5  ,'fr-FR','05' ,1  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 5);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 10 ,'fr-FR','10' ,2  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 10);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 20 ,'fr-FR','20' ,3  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 20);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 30 ,'fr-FR','30' ,4  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 30);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 40 ,'fr-FR','40' ,5  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 40);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 50 ,'fr-FR','50' ,6  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 50);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 60 ,'fr-FR','60' ,7  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 60);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 70 ,'fr-FR','70' ,8  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 70);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 80 ,'fr-FR','80' ,9  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 80);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 90 ,'fr-FR','90' ,10 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 90);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 95 ,'fr-FR','95' ,11 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 95);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 100,'fr-FR','100',12 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 100);

DELETE FROM CFGProjectCodeDescriptions WHERE UICultureName = 'fr-FR';
--.......................................................................................................v(100)
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '001','fr-FR','Acoustique ; Réduction du bruit'                                                                    ,1   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '001');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '002','fr-FR','Photographie aérienne ; Collecte et analyse d''images et de données aéroportées'                         ,2   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '002');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '003','fr-FR','Développement agricole ; Stockage de grains ; Mécanisation agricole'                                   ,3   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '003');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '004','fr-FR','Contrôle de la pollution atmosphérique'                                                                         ,4   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '004');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '005','fr-FR','Aéroports ; Aides ; Éclairage aéroportuaire ; Carburant pour aéronefs'                                         ,5   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '005');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '006','fr-FR','Aéroports ; Terminau ; Hangars ; Manutention frêt'                                              ,6   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '006');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '007','fr-FR','Installations arctiques'                                                                             ,7   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '007');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '008','fr-FR','Auditoriums et théâtres'                                                                        ,8   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '008');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '009','fr-FR','Automatisation ; Contrôles ; Instrumentation'                                                         ,9   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '009');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '010','fr-FR','Caserne ; Dortoirs'                                                                         ,10  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '010');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '011','fr-FR','Ponts'                                                                                       ,11  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '011');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '012','fr-FR','Cimetières (planification et réinstallation)'                                                            ,12  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '012');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '013','fr-FR','Traitement chimique et stockage'                                                                 ,13  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '013');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '014','fr-FR','Églises ; Chapelles'                                                                             ,14  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '014');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '015','fr-FR','Codes ; Normes ; Ordonnances'                                                                  ,15  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '015');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '016','fr-FR','Entreposage frigorifique ; Réfrigération ; Congélation rapide'                                                      ,16  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '016');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '017','fr-FR','Bâtiment commercial ; (faible élévation) ; Centres commerciaux'                                             ,17  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '017');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '018','fr-FR','Systèmes de communication ; TV ; Micro-ondes'                                                         ,18  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '018');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '019','fr-FR','Installations informatiques ; Service informatique'                                                         ,19  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '019');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '020','fr-FR','Conservation et gestion des ressources'                                                          ,20  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '020');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '021','fr-FR','Gestion de la construction'                                                                       ,21  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '021');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '022','fr-FR','Contrôle de corrosion ; Protection cathodique ; Électrolyse'                                          ,22  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '022')
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '023','fr-FR','Estimation des coûts; Ingénierie et analyse des coûts ; Coûts paramétriques ; Prévision'               ,23  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '023');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '024','fr-FR','Barrages (béton ; Arc)'                                                                         ,24  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '024');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '025','fr-FR','Barrages (Terre ; Roche) ; Digues ; Digues'                                                             ,25  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '025');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '026','fr-FR','Désalinisation (processus et installations)'                                                         ,26  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '026');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '027','fr-FR','Salles à manger; Clubs ; Restaurants'                                                              ,27  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '027');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '028','fr-FR','Enquêtes écologiques et archéologiques'                                                     ,28  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '028');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '029','fr-FR','Établissements d''enseignement ; Classes'                                                            ,29  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '029');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '030','fr-FR','Électronique'                                                                                   ,30  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '030');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '031','fr-FR','Ascenseurs ; Escaliers mécaniques ; Navettes automatisées'                                                          ,31  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '031');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '032','fr-FR','Conservation de l''énergie ; Nouvelles sources d''énergie'                                                       ,32  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '032');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '033','fr-FR','Études, évaluations ou déclarations sur l''impact environnemental'                                       ,33  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '033');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '034','fr-FR','Abris antiatomiques ; Conception résistante aux explosions'                                                      ,34  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '034');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '035','fr-FR','Pavilons ; Gymnases ; Stades'                                                            ,35  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '035');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '036','fr-FR','Protection contre l''incendie'                                                                               ,36  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '036');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '037','fr-FR','Pêches ; Échelles à poissons'                                                                       ,37  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '037');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '038','fr-FR','Foresterie et produits forestiers'                                                                    ,38  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '038');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '039','fr-FR','Garages ; Installations d''entretien de véhicules ; Ponts de stationnement'                                       ,39  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '039');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '040','fr-FR','Systèmes à gaz (propane, naturel, etc.)'                                                          ,40  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '040');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '041','fr-FR','Conception graphique'                                                                                ,41  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '041');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '042','fr-FR','Ports ; jetées ; quais ; Installations terminales de navires'                                             ,42  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '042');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '043','fr-FR','Chauffage, ventilation, climatisation'                                                        ,43  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '043');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '044','fr-FR','Planification des systèmes de santé'                                                                       ,44  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '044');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '045','fr-FR','Tours d''habitation ; bâtiments de type Air-Rights'                                                           ,45  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '045');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '046','fr-FR','Autoroutes ; Rues ; Pavage d''aérodrome ; Stationnement'                                              ,46  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '046');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '047','fr-FR','Préservation historique'                                                                       ,47  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '047');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '048','fr-FR','Hôpitaux et installations médicales'                                                                ,48  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '048');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '049','fr-FR','Hôtels ; Motels'                                                                                ,49  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '049');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '050','fr-FR','Logement (résidentiel, multifamilial, appartements, condominiums)'                                   ,50  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '050');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '051','fr-FR','Hydraulique et pneumatique'                                                                       ,51  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '051');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '052','fr-FR','Bâtiments industriels ; Usines de fabrication'                                                    ,52  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '052');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '053','fr-FR','Procédés industriels ; Contrôle qualité'                                                         ,53  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '053');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '054','fr-FR','Traitement des déchets industriels'                                                                     ,54  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '054');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '055','fr-FR','Architecture d''intérieur ; Planification de l''espace'                                                               ,55  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '055');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '056','fr-FR','Irrigation ; Drainage'                                                                          ,56  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '056');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '057','fr-FR','Installations judiciaires et d''audience'                                                             ,57  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '057');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '058','fr-FR','Laboratoires ; Installations de recherche médicale'                                                      ,58  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '058');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '059','fr-FR','Architecture du paysage'                                                                        ,59  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '059');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '060','fr-FR','Bibliothèques ; Musées ; Galeries'                                                                 ,60  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '060');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '061','fr-FR','Éclairage (Intérieur ; Affiches ; Théâtres ; etc.)'                                                 ,61  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '061');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '062','fr-FR','Éclairage (Extérieur ; Rue ; Mémoriaux ; Terrains de sport)'                                      ,62  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '062');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '063','fr-FR','Systèmes de manutention des matériaux ; Convoyeurs ; Trieuses'                                                ,63  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '063');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '064','fr-FR','Métallurgie'                                                                                    ,64  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '064');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '065','fr-FR','Microclimatologie ; Génie tropical'                                                        ,65  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '065');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '066','fr-FR','Normes de conception militaire'                                                                     ,66  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '066');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '067','fr-FR','Mines et minéralogie'                                                                         ,67  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '067');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '068','fr-FR','Installations de missiles (Silos ; Combustibles ; Transport)'                                                  ,68  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '068');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '069','fr-FR','Conception de systèmes modulaires ; Structures ou composants pour modèles préfabriqués'                                      ,69  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '069');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '070','fr-FR','Architecture navale ; Plateformes off-shore'                                                       ,70  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '070');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '071','fr-FR','Installations nucléaires ; Blindage nucléaire'                                                         ,71  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '071');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '072','fr-FR','Immeuble de bureaux ; Parcs industriels'                                                             ,72  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '072');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '073','fr-FR','Génie océanographique'                                                                     ,73  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '073');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '074','fr-FR','Ordonnances ; Munitions ; Armes spéciales'                                                       ,74  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '074');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '075','fr-FR','Exploration pétrolière ; Raffinage'                                                               ,75  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '075');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '076','fr-FR','Pétrole et carburant (stockage et distribution)'                                                 ,76  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '076');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '077','fr-FR','Pipelines (Cross-Country--liquide et gaz)'                                                       ,77  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '077');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '078','fr-FR','Planification (Communauté ; Régional ; Zone et État)'                                              ,78  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '078');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '079','fr-FR','Planification (site, installation et projet)'                                                     ,79  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '079');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '080','fr-FR','Plomberie et conception des tuyaux'                                                                        ,80  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '080');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '081','fr-FR','Structures pneumatiques ; Bâtiments de soutien aérien'                                                   ,81  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '081');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '082','fr-FR','Installations postales'                                                                             ,82  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '082');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '083','fr-FR','Production, transmission, distribution d''électricité'                                                  ,83  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '083');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '084','fr-FR','Prisons et établissements correctionnels'                                                             ,84  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '084');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '085','fr-FR','Conception de produit, de machine et d''équipement'                                                           ,85  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '085');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '086','fr-FR','Radar ; Sonar ; Télescopes radio et radar'                                                        ,86  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '086');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '087','fr-FR','Chemin de fer et transit rapide'                                                                    ,87  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '087');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '088','fr-FR','Installations récréatives (Parcs ; Marinas ; ect.)'                                                ,88  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '088');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '089','fr-FR','Réhabilitation (bâtiments ; Structures ; Installations)'                                            ,89  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '089');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '090','fr-FR','Récupération des ressources ; Recyclage'                                                                 ,90  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '090');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '091','fr-FR','Systèmes de radiofréquences et blindages'                                                          ,91  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '091');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '092','fr-FR','Canaux des rivières ; Navigables ; Contrôle des inondations'                                                       ,92  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '092');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '093','fr-FR','Ingénierie de sécurité ; Études sur les accidents ; Études OSHA'                                            ,93  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '093');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '094','fr-FR','Systèmes de sécurité ; Intrusion et détection de fumée'                                                  ,94  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '094');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '095','fr-FR','Conception et études sismiques'                                                                      ,95  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '095');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '096','fr-FR','Collecte, traitement et élimination des eaux usées'                                                       ,96  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '096');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '097','fr-FR','Sols et études géologiques ; Fondations'                                                         ,97  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '097');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '098','fr-FR','Utilisation de l''énergie solaire'                                                                      ,98  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '098');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '099','fr-FR','Déchets solides ; Incinération ; Décharge'                                                          ,99  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '099');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '100','fr-FR','Environnements spéciaux ; Salles blanches ; Etc.'                                                       ,100 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '100');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '101','fr-FR','Conception structurale ; Structures spéciales'                                                         ,101 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '101');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '102','fr-FR','Arpentage ; Parcelles ; Cartographie ; Études sur les plaines inondables'                                             ,102 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '102');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '103','fr-FR','Piscines'                                                                                ,103 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '103');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '104','fr-FR','Manutention et installations des eaux pluviales'                                                              ,104 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '104');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '105','fr-FR','Systèmes téléphoniques (ruraux ; Mobile ; Intercom ; etc.)'                                             ,105 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '105');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '106','fr-FR','Services d''essai et d''inspection'                                                                 ,106 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '106');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '107','fr-FR','Transport et ingénierie des transports'                                                          ,107 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '107');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '108','fr-FR','Tours (Systèmes autonomes et haubanés)'                                                      ,108 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '108');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '109','fr-FR','Tunnels et métros'                                                                             ,109 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '109');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '110','fr-FR','Renouvellements urbains ; Développement communautaire'                                                         ,110 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '110');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '111','fr-FR','Services publics (gaz et vapeur)'                                                                     ,111 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '111');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '112','fr-FR','Analyse de la valeur ; Coût du cycle de vie'                                                            ,112 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '112');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '113','fr-FR','Entrepôts et dépôts'                                                                           ,113 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '113');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '114','fr-FR','Ressources en eau ; Hydrologie ; Eaux souterraines'                                                      ,114 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '114');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '115','fr-FR','Approvisionnement en eau ; Traitement et distribution'                                                      ,115 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '115');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '116','fr-FR','Souffleries ; Conception des installations de recherche/essais'                                              ,116 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '116');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '117','fr-FR','Zonage ; Études sur l''utilisation des terres'                                                                      ,117 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '117');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'A08','fr-FR','Animaleries'                                                                             ,118 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'A08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'A09','fr-FR','Anti-terrorisme/protection de la force'                                                               ,119 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'A09');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'A10','fr-FR','Réduction de l''amiante'                                                                            ,120 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'A10');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C01','fr-FR','Cartographie'                                                                                   ,121 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C01');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C03','fr-FR','Cartographie : nautique et aéronautique'                                                           ,122 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C05','fr-FR','Services de garde d''enfants/aménagement'                                                             ,123 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C07','fr-FR','Ingénierie côtière'                                                                           ,124 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C07');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C11','fr-FR','Installations communautaires'                                                                          ,125 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C11');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C16','fr-FR','Arpentage de la construction'                                                                        ,126 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C16');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C19','fr-FR','Installations cryogéniques'                                                                          ,127 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C19');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D04','fr-FR','Conception-construction - Préparation des demandes de propositions'                                          ,128 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D05','fr-FR','Développement de modèles numériques d''élévation et de terrain'                                               ,129 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D06','fr-FR','Orthophotographie numérique'                                                                      ,130 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D06');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D08','fr-FR','Études et conception de dragage'                                                                   ,131 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E03','fr-FR','Études électriques et conception'                                                                 ,132 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E06','fr-FR','Ambassades et chancelleries'                                                                      ,133 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E06');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E08','fr-FR','Economie d''ingénierie'                                                                         ,134 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E10','fr-FR','Cartographie de l''environnement et des ressources naturelles'                                                    ,135 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E10');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E11','fr-FR','Planification environnementale'                                                                        ,136 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E11');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E12','fr-FR','Assainissement environnemental'                                                                     ,137 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E12');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E13','fr-FR','Essais et analyses environnementales'                                                            ,138 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E13');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'F05','fr-FR','Ingénierie judiciaire'                                                                          ,139 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'F05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'G03','fr-FR','Arpentage géodésique : terrestre et aéroporté'                                                       ,140 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'G03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'G04','fr-FR','Services du système d''information géographique : développement, analyse et collecte de données'            ,141 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'G04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'G05','fr-FR','Conversion de données géospatiales : balayage, numérisation, compilation, attribution, inscription, rédaction',142 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'G05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'H02','fr-FR','Manutention et stockage des matières dangereuses'                                                      ,143 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'H02');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'H03','fr-FR','Assainissement des déchets dangereux, toxiques et radioactifs'                                               ,144 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'H03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'H13','fr-FR','Arpentage hydrographique'                                                                        ,145 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'H13');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'I04','fr-FR','Systèmes de transport intelligents'                                                            ,146 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'I04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'L02','fr-FR','Arpentage des terres'                                                                                ,147 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'L02');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'M01','fr-FR','Cartographie des systèmes de localisation/adressage'                                                           ,148 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'M01');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'N02','fr-FR','Structures de navigation ; Serrures'                                                                  ,149 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'N02');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'P03','fr-FR','Photogrammétrie'                                                                                ,150 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'P03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'P13','fr-FR','Installations de sécurité publique'                                                                      ,151 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'P13');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R05','fr-FR','Installations/systèmes de réfrigération'                                                                  ,152 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R07','fr-FR','Télédétection'                                                                                ,153 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R07');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R08','fr-FR','Installations de recherche'                                                                           ,154 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R10','fr-FR','Analyse des risques'                                                                                 ,155 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R10');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R12','fr-FR','Toiture'                                                                                       ,156 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R12');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'S11','fr-FR','Conception durable'                                                                            ,157 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'S11');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'T04','fr-FR','Arpentage et cartographie topographiques'                                                             ,158 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'T04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'U01','fr-FR','Assainissement des munitions non explosées'                                                               ,159 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'U01');

DELETE FROM CFGProjectMilestoneDescriptions WHERE UICultureName = 'fr-FR';
--...........................................................................................................................v(255)
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysEstStart',           'fr-FR','Début estimé',                   1 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysEstStart')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysEstCompletion',      'fr-FR','Achèvement estimé',              2 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysEstCompletion')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysContract',           'fr-FR','Contrat attribué',                  3 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysContract')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysBidSubmitted',       'fr-FR','Offre envoyée',                     4 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysBidSubmitted')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysStart',              'fr-FR','Début effectif',                      5 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysStart')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysActualCompl',        'fr-FR','Achèvement réel',                 6 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysActualCompl')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysProfServicesCompl',  'fr-FR','Achèvement des services professionnels',  7 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysProfServicesCompl')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysConstCompl',         'fr-FR','Achèvement de la construction',           8 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysConstCompl')


DELETE FROM CFGProposalSourceDescriptions WHERE UICultureName = 'fr-FR';
--..................................................................................................v(50)
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-FR','Référence du client'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '01');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-FR','Événement'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '02');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-FR','Liste'              ,3 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '03');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-FR','Contact personnel'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '04');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '05','fr-FR','Médias sociaux'      ,5 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '05');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '06','fr-FR','Leadership éclairé',6 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '06');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '07','fr-FR','Site Web'           ,7 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '07');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '08','fr-FR','GovWinIQ'          ,8 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '08');

DELETE FROM CFGProposalStatusDescriptions WHERE UICultureName = 'fr-FR';
--..................................................................................................v(50)
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-FR','En cours',1 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '01');
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-FR','En cours de révision'  ,2 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '02');
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-FR','Envoyé'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '03');

DELETE FROM CFGPRResponsibilityDescriptions WHERE UICultureName = 'fr-FR';
--....................................................................................................v(80)
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'C' ,'fr-FR','Consultant'           ,3 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'C');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'IE','fr-FR','Expérience individuelle',4 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'IE');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'JV','fr-FR','Coentreprise'        ,2 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'JV');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'P' ,'fr-FR','Premier'                ,1 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'P');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'S' ,'fr-FR','Sous-traitant'        ,5 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'S');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'U' ,'fr-FR','Non déterminé'         ,6 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'U');

DELETE FROM CFGPYAccrualsDescriptions WHERE UICultureName = 'fr-FR';
--............................................................................................................................v(40)
INSERT INTO CFGPYAccrualsDescriptions (Code,Company,UICultureName,Description) SELECT 'Sick Lv.',CFGMainData.Company,'fr-FR','Congé de maladie' FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGPYAccrualsData WHERE Code = 'Sick Lv.' AND Company = CFGMainData.Company)
INSERT INTO CFGPYAccrualsDescriptions (Code,Company,UICultureName,Description) SELECT 'Vacation',CFGMainData.Company,'fr-FR','Vacances'   FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGPYAccrualsData WHERE Code = 'Vacation' AND Company = CFGMainData.Company)

DELETE FROM CFGRGMethodsDescriptions WHERE UICultureName = 'fr-FR';
--..........................................................................................v(40)
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'B','fr-FR','Facturations PAJ'                   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'B');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'M','fr-FR','(JTD DL*Mult) + Frais remb. PAJ'  WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'M');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'N','fr-FR','Aucune reconnaissance des revenus'         WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'N');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'P','fr-FR','(Pourcentage de frais de rémun.*) + Frais remb. PAJ' WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'P');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'R','fr-FR','Reçus PAJ'                   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'R');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'W','fr-FR','Facturations PAJ + Travaux en cours selon Facturation'   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'W');

DELETE FROM CFGServiceProfileDescriptions WHERE UICultureName = 'fr-FR';
--..................................................................................................................v(40)
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.18' ,'fr-FR','Plan d''utilisation et de développement des terres','Area in hectares'               WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.18');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.19' ,'fr-FR','Aménagement du territoire/plan de construction','Area in hectares'                          WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.19');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.23' ,'fr-FR','Plan du paysage','Area in hectares'                                  WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.23');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.24' ,'fr-FR','Plan d''espace ouvert/vert','Area in hectares'                           WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.24');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.25' ,'fr-FR','Plan de structure de paysage','Area in hectares'                        WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.25');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.26' ,'fr-FR','Plan de conservation du paysage','Area in hectares'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.26');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.27' ,'fr-FR','Plan d''entretien et de développement','Area in hectares'                WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.27');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.34' ,'fr-FR','Bâtiments et intérieurs','Chargeable costs in Euro'                 WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.34');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.39' ,'fr-FR','Installations en plein air','Chargeable costs in Euro'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.39');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.43' ,'fr-FR','Structures de génie civil','Chargeable costs in Euro'            WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.43');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.47' ,'fr-FR','Installations de transport','Chargeable costs in Euro'               WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.47');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.51' ,'fr-FR','Planification structurelle','Chargeable costs in Euro'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.51');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.55' ,'fr-FR','Équipements techniques','Chargeable costs in Euro'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.55');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A01','fr-FR','Études sur l''impact environnemental','Area in hectares'                    WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A01');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A02','fr-FR','Isolation thermique et équilibrage d''énergie','Chargeable costs in Euro' WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A02');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A03','fr-FR','Acoustique de bâtiment','Chargeable costs in Euro'                      WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A03');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A04','fr-FR','Acoustique de la pièce','Chargeable costs in Euro'                          WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A04');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A05','fr-FR','Géotechnique','Chargeable costs in Euro'                             WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A05');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A06','fr-FR','Opérations de mesure/arpentage','Accounting units'                  WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A06');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A07','fr-FR','Étude d''ingénierie','Chargeable costs in Euro'                   WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A07');

DELETE FROM CFGSuffixDescriptions WHERE UICultureName = 'fr-FR';
--......................................................................................v(150)
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'I'  ,'fr-FR','I'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'I');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'II' ,'fr-FR','II' ,4 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'II');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'III','fr-FR','III',5 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'III');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'Jr.','fr-FR','Jr.',1 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'Jr.');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'Sr.','fr-FR','Sr.',2 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'Sr.');

DELETE FROM CFGUnitTypeDescriptions WHERE UICultureName = 'fr-FR';
--...................................................................................................v(50)
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Equipment','fr-FR','Équipement',1 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Equipment');
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Expenses ','fr-FR','Frais' ,2 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Expenses');
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Labor'    ,'fr-FR','Main-d''œuvre'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Labor');

DELETE FROM CFGVendorTypeDescriptions WHERE UICultureName = 'fr-FR';
--...................................................................................v(30)
INSERT INTO CFGVendorTypeDescriptions (Type,UICultureName,Label) SELECT 'C','fr-FR','Consultant' WHERE EXISTS (SELECT 'x' FROM CFGVendorTypeData WHERE Type = 'C');
INSERT INTO CFGVendorTypeDescriptions (Type,UICultureName,Label) SELECT 'T','fr-FR','Commerce'      WHERE EXISTS (SELECT 'x' FROM CFGVendorTypeData WHERE Type = 'T');

DELETE FROM FW_CFGAttachmentCategoryDesc WHERE UICultureName = 'fr-FR';
--................................................................................................................................................v(255)
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Notes'          ,'Activities'         ,'fr-FR','Remarques'                   ,1  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Notes' and Application ='Activities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'NDA'            ,'Contacts'           ,'fr-FR','Accord de non-divulgation',2  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'NDA' and Application ='Contacts');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Notes'          ,'Contacts'           ,'fr-FR','Remarques'                   ,3  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Notes' and Application ='Contacts');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'CompanyOverview','TextLibrary'        ,'fr-FR','Vue d''ensemble de la société'        ,4  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'CompanyOverview' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Case Study'     ,'TextLibrary'        ,'fr-FR','Étude de cas'              ,5  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Case Study' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Reference'      ,'TextLibrary'        ,'fr-FR','Référence'               ,6  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Reference' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Resume'         ,'Employees'          ,'fr-FR','Reprendre'                  ,7  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Resume' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Award'          ,'Employees'          ,'fr-FR','Prix'                   ,8  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Award' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Leadership'     ,'Employees'          ,'fr-FR','Leadership éclairé'      ,9  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Leadership' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Firms'              ,'fr-FR','Contrat de service principal',10 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Firms'              ,'fr-FR','Énoncé des travaux'       ,11 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'NDA'            ,'Firms'              ,'fr-FR','Accord de non-divulgation',12 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'NDA' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'ClientACH'      ,'Firms'              ,'fr-FR','Client ACH'              ,13 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'ClientACH' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'VendorACH'      ,'Firms'              ,'fr-FR','Fournisseur ACH'              ,14 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'VendorACH' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'DigitalArtifact','Marketing Campaigns','fr-FR','Artefact numérique'        ,15 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'DigitalArtifact' and Application ='Marketing Campaigns');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Estimate'       ,'Opportunities'      ,'fr-FR','Estimation'                ,16 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Estimate' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Proposal'       ,'Opportunities'      ,'fr-FR','Proposition'                ,17 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Proposal' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Opportunities'      ,'fr-FR','Énoncé des travaux'       ,18 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Opportunities'      ,'fr-FR','Contrat de service principal',19 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Estimate'       ,'Projects'           ,'fr-FR','Estimation'                ,20 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Estimate' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Proposal'       ,'Projects'           ,'fr-FR','Proposition'                ,21 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Proposal' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Projects'           ,'fr-FR','Énoncé des travaux'       ,22 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Projects'           ,'fr-FR','Contrat de service principal',23 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Projects');

DELETE FROM FW_CFGLabelData WHERE UICultureName = 'fr-FR';
--............................................................................................................................................................................v(100)
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','accountLabel'                            ,'Compte'                       ,'[Account]'                    ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','accountLabelPlural'                      ,'Comptes'                      ,'[Accounts]'                   ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','clientLabel'                             ,'Client'                        ,'[Client]'                     ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','clientLabelPlural'                       ,'Clients'                       ,'[Clients]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','contactLabel'                            ,'Contact'                       ,'[Contact]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','contactLabelPlural'                      ,'Contacts'                      ,'[Contacts]'                   ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','employeeLabel'                           ,'Employé'                      ,'[Employee]'                   ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','employeeLabelPlural'                     ,'Employés'                     ,'[Employees]'                  ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','labcd1Label'                             ,'Code de main d''œuvre de niveau 1'            ,'[Labcd Level 1]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','labcd1LabelPlural'                       ,'Code de main d''œuvre de niveau 1s'           ,'[Labcd Level 1s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','labcd2Label'                             ,'Code de main d''œuvre de niveau 2'            ,'[Labcd Level 2]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','labcd2LabelPlural'                       ,'Code de main d''œuvre de niveau 2s'           ,'[Labcd Level 2s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','labcd3Label'                             ,'Code de main d''œuvre de niveau 3'            ,'[Labcd Level 3]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','labcd3LabelPlural'                       ,'Code de main d''œuvre de niveau 3s'           ,'[Labcd Level 3s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','labcd4Label'                             ,'Code de main d''œuvre de niveau 4'            ,'[Labcd Level 4]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','labcd4LabelPlural'                       ,'Code de main d''œuvre de niveau 4s'           ,'[Labcd Level 4s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','labcd5Label'                             ,'Code de main d''œuvre de niveau 5'            ,'[Labcd Level 5]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','labcd5LabelPlural'                       ,'Code de main d''œuvre de niveau 5s'           ,'[Labcd Level 5s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','labcdLabel'                              ,'Code de main d''œuvre'                    ,'[Labor Code]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','labcdLabelPlural'                        ,'Codes de main d''œuvre'                   ,'[Labor Codes]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','firmLabel'                               ,'Entreprise'                          ,'[Firm]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','firmLabelPlural'                         ,'Entreprises'                         ,'[Firms]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','mktLabel'                                ,'Campagne de marketing'            ,'[Marketing Campaign]'         ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','mktLabelPlural'                          ,'Campagnes de marketing'           ,'[Marketing Campaigns]'        ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','org1Label'                               ,'Organisation 1'                ,'[Org Level 1]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','org1LabelPlural'                         ,'Organisation 1s'               ,'[Org Level 1s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','org2Label'                               ,'Organisation 2'                ,'[Org Level 2]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','org2LabelPlural'                         ,'Organisation 2s'               ,'[Org Level 2s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','org3Label'                               ,'Organisation 3'                ,'[Org Level 3]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','org3LabelPlural'                         ,'Organisation 3S'               ,'[Org Level 3s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','org4Label'                               ,'Organisation 4'                ,'[Org Level 4]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','org4LabelPlural'                         ,'Organisation 4s'               ,'[Org Level 4s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','org5Label'                               ,'Organisation 5'                ,'[Org Level 5]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','org5LabelPlural'                         ,'Organisation 5s'               ,'[Org Level 5s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','orgLabel'                                ,'Organisation'                  ,'[Organization]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','orgLabelPlural'                          ,'Organisations'                 ,'[Organizations]'              ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','proposalLabel'                           ,'Proposition'                      ,'[Proposal]'                   ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','proposalLabelPlural'                     ,'Propositions'                     ,'[Proposals]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','textLibLabel'                            ,'Expression standard'                   ,'[Text Library]'               ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','textLibLabelPlural'                      ,'Expressions standard'                  ,'[Text Libraries]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','unitLabel'                               ,'Unité'                          ,'[Unit]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','unitLabelPlural'                         ,'Unités'                         ,'[Units]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','vendorLabel'                             ,'Fournisseur'                        ,'[Vendor]'                     ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','vendorLabelPlural'                       ,'Fournisseurs'                       ,'[Vendors]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','wbs1Label'                               ,'Projet'                       ,'[WBS1]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','wbs1LabelPlural'                         ,'Projets'                      ,'[WBS1s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','wbs2Label'                               ,'Phase'                         ,'[WBS2]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','wbs2LabelPlural'                         ,'Phases'                        ,'[WBS2s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','wbs3Label'                               ,'Tâche'                          ,'[WBS3]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','wbs3LabelPlural'                         ,'Tâches'                         ,'[WBS3s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','sysPM'                                   ,'Chef de projet'               ,'[Project Manager]'            ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','sysPR'                                   ,'Principal'                     ,'[Principal]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','sysSP'                                   ,'Superviseur'                    ,'[Supervisor]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','equipmentLabel'                          ,'Équipement'                     ,'[Equipment]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','equipmentLabelPlural'                    ,'Équipement'                     ,'[Equipments]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','jobToDateLabel'                          ,'projet-à-ce jour'                   ,'[JobToDate]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','JTDLabel'                                ,'PAJ'                           ,'[JTD]'                        ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','estimateToCompleteLabel'                 ,'Estimation-à-achever'          ,'[EstimateToComplete]'         ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','ETCLabel'                                ,'CEPA'                           ,'[ETC]'                        ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','estimateAtCompletionLabel'               ,'Estimation-à-l''achèvement'        ,'[EstimateAtCompletion]'       ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','EACLabel'                                ,'Les frais annuels équivalents'                           ,'[EAC]'                        ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','sysPRM'                                  ,'Responsable des propositions'              ,'[Proposal Manager]'           ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','sysMC'                                   ,'Coordinateur marketing'         ,'[Marketing Coordinator]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','sysBDL'                                  ,'Responsable du développement d''activité'     ,'[Business Development Lead]'  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','consultantLabel'                         ,'Consultant'                    ,'[Consultant]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','consultantLabelPlural'                   ,'Consultants'                   ,'[Consultants]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','compensationLabel'                       ,'Rémunération'                  ,'[Compensation]'               ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','revType1Label'                           ,'Main-d''œuvre'                         ,'[Revenue Method Type 1]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','revType2Label'                           ,'Consultant'                    ,'[Revenue Method Type 2]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','revType3Label'                           ,'Frais remb. de '                         ,'[Revenue Method Type 3]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','revType4Label'                           ,'Méthode de revenu 4'              ,'[Revenue Method Type 4]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','revType5Label'                           ,'Méthode de revenu 5'              ,'[Revenue Method Type 5]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','feeEstimatingCostGroupLabel'             ,'Groupe de coûts'                    ,'[Cost Group]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','feeEstimatingCostGroupLabelPlural'       ,'Groupes de coûts'                   ,'[Cost Groups]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','feeEstimatingFunctionalGroupLabel'       ,'Groupe fonctionnel'              ,'[Functional Group]'           ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','feeEstimatingFunctionalGroupLabelPlural' ,'Groupes fonctionnels'             ,'[Functional Groups]'          ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','feeEstimatingServiceProfileLabel'        ,'Profil de service'               ,'[Service Profile]'            ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-FR','feeEstimatingServiceProfileLabelPlural'  ,'Profils de service'              ,'[Service Profiles]'           ,'N','N',NULL);

COMMIT TRANSACTION

END
GO
