SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPConvertOppUDIC2PR]
AS

BEGIN -- Procedure stRPConvertOppUDIC2PR

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
--
-- The purpose of this stored procedure is to move UDIC data from Opportunities to Projects.
--
-- 1. Move only Custom Tabs from Opportunity to Project.
--    1.1 FW_InfoCenterTabsData
--    1.2 FW_InfoCenterTabHeadings
--
-- 2. Move Custom Grids from Opportunity to Project. A Custom Grid could be on a Standard or a Custom Tab.
--    2.1 FW_CustomGridsData
--    2.2 FW_CustomGridCaptions
--    2.3 CFGScreenDesignerData --> GridID = 'X', ComponentID = 'Projects_' + @strNewGridID
--    2.4 CFGScreenDesignerLabels
--    2.5 Physical Project Grid Tables --> 'Projects_' + @strNewGridID, 'Projects_' + @strNewGridID + 'Template'
--
-- 3. Move Custom Columns from Opportunity to Project. A Custom Column could be on a Custom Grid on a Form Field (GridID = 'X').
--    3.1 FW_CustomColumnsData
--    3.2 FW_CustomColumnCaptions
--    3.3 FW_CustomColumnValuesData
--    3.4 CFGScreenDesignerData
--    3.5 CFGScreenDesignerLabels
--    3.6 ProjectCustomTabFields
--
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @strOldTabID nvarchar(125)
  DECLARE @strNewTabID nvarchar(125)
  DECLARE @strOldGridID nvarchar(125)
  DECLARE @strNewGridID nvarchar(125)
  DECLARE @strOldColName nvarchar(125)
  DECLARE @strNewColName nvarchar(125)
  DECLARE @strOppGrdTblName nvarchar(128)
  DECLARE @strPRGrdTblName nvarchar(128)
  DECLARE @strPRTemplateGrdTblName nvarchar(128)
  DECLARE @strDefaultValue nvarchar(MAX)
  DECLARE @strDataType nvarchar(256)
  DECLARE @strOpportunityID varchar(32)
  DECLARE @strWBS1 nvarchar(30)
  DECLARE @strRecordExists varchar(1)
  DECLARE @strCol nvarchar(MAX)
  DECLARE @strVal nvarchar(MAX)
  DECLARE @strSQL nvarchar(MAX)
  DECLARE @strUserName nvarchar(32) = 'DPS20OPPUDIC2PR'
  DECLARE @strOppPrefix varchar(10) = '[OPP] '
  DECLARE @strRole nvarchar(20)
  DECLARE @strAccessAllTabs varchar(1)
  DECLARE @strCustomDataType varchar(32)

  DECLARE @iCount int
  DECLARE @siPrec smallint

  DECLARE @tabCustomTabs TABLE(
    OldTabID nvarchar(125) COLLATE database_default,
    NewTabID nvarchar(125) COLLATE database_default
    UNIQUE(OldTabID, NewTabID)
  )

  DECLARE @tabCustomGrids TABLE (
    RowID int IDENTITY(1,1),
    OldTabID nvarchar(125) COLLATE database_default,
    NewTabID nvarchar(125) COLLATE database_default,
    OldGridID nvarchar(125) COLLATE database_default,
    NewGridID nvarchar(125) COLLATE database_default,
    OppGrdTblName nvarchar(128) COLLATE database_default,
    PRGrdTblName nvarchar(128) COLLATE database_default,
    PRTemplateGrdTblName nvarchar(128) COLLATE database_default
    UNIQUE(RowID, OldGridID, NewGridID, OppGrdTblName, PRGrdTblName, PRTemplateGrdTblName)
  )

  DECLARE @tabCustomColumns TABLE (
    GridID nvarchar(125) COLLATE database_default,
    OldColName nvarchar(125) COLLATE database_default,
    NewColName nvarchar(125) COLLATE database_default
    UNIQUE(GridID, OldColName, NewColName)
  )

  DECLARE csr20180521_CustomTab CURSOR LOCAL FAST_FORWARD FOR
    SELECT TabID FROM FW_InfoCenterTabsData WHERE InfoCenterArea = 'Opportunities' AND TabType = 'Custom' ORDER BY Seq

  DECLARE csr20180521_CustomGrid CURSOR LOCAL FAST_FORWARD FOR
    SELECT
      COALESCE(SDD.TabID, CG.TabID, CGT.OldTabID) AS OldTabID, 
      COALESCE(CST.NewTabID, CGT.NewTabID, 'overview') AS NewTabID,
      CG.GridID, 
      CG.TableName 
      FROM FW_CustomGridsData AS CG
        LEFT JOIN @tabCustomTabs AS CGT ON CG.TabID = CGT.OldTabID
        LEFT JOIN CFGScreenDesignerData AS SDD ON CG.InfoCenterArea = SDD.InfoCenterArea AND CG.Tablename = SDD.ComponentID AND SDD.ComponentType = 'grid'
        LEFT JOIN @tabCustomTabs AS CST ON SDD.TabID = CST.OldTabID
      WHERE CG.InfoCenterArea = 'Opportunities' 
      ORDER BY CG.GridID

  DECLARE csr20180521_CustomCol CURSOR LOCAL FAST_FORWARD FOR
    SELECT 
      X.GridID AS GridID, 
      X.Name AS Name, 
      X.DefaultValue AS DefaultValue, 
      X.DataType as DataType,
      X.OppGrdTblName AS OppGrdTblName,
      X.PRGrdTblName AS PRGrdTblName,
      X.PRTemplateGrdTblName AS PRTemplateGrdTblName
      FROM (
        SELECT DISTINCT
          CCD.GridID AS GridID, 
          CCD.Name AS Name, 
          CCD.DefaultValue AS DefaultValue, 
          CCD.DataType as DataType,
          CG.OppGrdTblName AS OppGrdTblName,
          CG.PRGrdTblName AS PRGrdTblName,
          CG.PRTemplateGrdTblName AS PRTemplateGrdTblName
          FROM FW_CustomColumnsData AS CCD
            INNER JOIN @tabCustomGrids AS CG ON CCD.GridID = CG.OldGridID
          WHERE CCD.InfoCenterArea = 'Opportunities' AND CCD.GridID <> 'X'
        UNION
        SELECT DISTINCT
          CCD.GridID AS GridID, 
          CCD.Name AS Name, 
          CCD.DefaultValue AS DefaultValue, 
          CCD.DataType as DataType,
          NULL AS OppGrdTblName,
          NULL AS PRGrdTblName,
          NULL AS PRTemplateGrdTblName
          FROM FW_CustomColumnsData AS CCD
          WHERE CCD.InfoCenterArea = 'Opportunities' AND CCD.GridID = 'X'
      ) X

  DECLARE csr20180521_OppCustFields CURSOR LOCAL FAST_FORWARD FOR
    SELECT O.OpportunityID, O.PRWBS1 FROM Opportunity AS O INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' ORDER BY O.OpportunityID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (OBJECT_ID('dbo.[OpportunityCustomTabFields]', 'U') IS NOT NULL) 
    BEGIN

      -- Convert Custom Tabs from Opportunity to Project
      -- Select only Tabs with TabType = 'Custom'

      IF (OBJECT_ID('dbo.[FW_InfoCenterTabsData]', 'U') IS NOT NULL)
        BEGIN

          OPEN csr20180521_CustomTab
          FETCH NEXT FROM csr20180521_CustomTab INTO @strNewTabID

          WHILE (@@FETCH_STATUS = 0)
            BEGIN
                
              SET @strOldTabID = @strNewTabID
              SET @iCount = 0

              -- If TabID already exists for Projects, append a number to the end of it to make it unique
              WHILE (EXISTS(SELECT TabID FROM FW_InfoCenterTabsData WHERE InfoCenterArea = 'Projects' AND TabID = @strNewTabID))
                BEGIN
                  SET @iCount = @iCount + 1
                  -- Truncate the TabID to avoid exceeding max length
                  SET @strNewTabID = LEFT(@strOldTabID, 125 - LEN(@iCount)) + CONVERT(VARCHAR, @iCount)
                END

              -- Keep track of old and new TabID to be used in subsequent steps
              INSERT INTO @tabCustomTabs (OldTabID, NewTabID) VALUES (@strOldTabID, @strNewTabID)

              --Insert custom tab for Projects
              INSERT INTO FW_InfoCenterTabsData (InfoCenterArea, TabID, TabType, Seq, Installed, Platform, PageID) 
              SELECT 'Projects', @strNewTabID, TabType, Seq, Installed, 'WebUI' AS Platform, 'ProjectDetailsPage' AS PageID FROM FW_InfoCenterTabsData WHERE InfoCenterArea = 'Opportunities' AND TabID = @strOldTabID
      
              INSERT INTO FW_InfoCenterTabHeadings (InfocenterArea, TabID, UICultureName, TabHeading, SysTabHeading)
              SELECT 'Projects', @strNewTabID, UICultureName, @strOppPrefix + TabHeading, SysTabHeading FROM FW_InfoCenterTabHeadings WHERE InfoCenterArea = 'Opportunities' AND TabID = @strOldTabID

              FETCH NEXT FROM csr20180521_CustomTab INTO @strNewTabID
            
            END /* END WHILE (@@FETCH_STATUS = 0) */

          CLOSE csr20180521_CustomTab
          DEALLOCATE csr20180521_CustomTab

          -- Update Seq of entries so that they will be arranged alphabetically

          UPDATE FW_InfoCenterTabsData SET
            Seq = X.Seq
            FROM FW_InfoCenterTabsData AS ITD
              INNER JOIN ( /* X */
                SELECT 
                  TD.TabID AS TabID, 
                  ROW_NUMBER() OVER (ORDER BY TD.TabType DESC, CASE WHEN TD.TabType = 'Standard' THEN TD.Seq ELSE 999 END, H.TabHeading) AS Seq
                  FROM FW_InfoCenterTabsData TD
                    INNER JOIN FW_InfoCenterTabHeadings H ON H.InfocenterArea = TD.InfoCenterArea AND H.TabID = TD.TabID AND H.UICultureName = 'en-US'
                  WHERE TD.InfoCenterArea = 'Projects'
              ) X ON X.TabID = ITD.TabID
            WHERE ITD.InfoCenterArea = 'Projects'

        END /* END IF (OBJECT_ID('dbo.[FW_InfoCenterTabsData]', 'U') IS NOT NULL) */

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- Convert Custom Grids from Opportunity to Project

      IF OBJECT_ID('dbo.[FW_CustomGridsData]', 'U') IS NOT NULL
      BEGIN

        OPEN csr20180521_CustomGrid
        FETCH NEXT FROM csr20180521_CustomGrid INTO @strOldTabID, @strNewTabID, @strNewGridID, @strOppGrdTblName

        WHILE (@@FETCH_STATUS = 0)
          BEGIN

            SET @strOldGridID = @strNewGridID
            SET @iCount = 0

            -- If GridID already exists for Projects, append a number to the end of it to make it unique
            
            WHILE (EXISTS(SELECT GridID FROM FW_CustomGridsData WHERE InfoCenterArea = 'Projects' AND GridID = @strNewGridID))
              BEGIN

                SET @iCount = @iCount + 1

                --Truncate the GridID to avoid exceeding max length
                SET @strNewGridID = LEFT(@strOldGridID, 125 - LEN(@iCount)) + CONVERT(VARCHAR, @iCount)

              END /* WHILE */

              -- Set Table Names for Project and Project Template Custom Grids

              IF (EXISTS(SELECT Table_Name FROM INFORMATION_SCHEMA.TABLES WHERE Table_Name = ('Projects_' + REPLACE(@strOppGrdTblName, 'Opportunities_', ''))))
                BEGIN
                  SET @strPRGrdTblName = 'Projects_' + REPLACE(@strOppGrdTblName, 'Opportunities_', '') + '1'
                  SET @strPRTemplateGrdTblName = @strPRGrdTblName + 'Template'
                END
              ELSE
                BEGIN
                  SET @strPRGrdTblName = 'Projects_' + REPLACE(@strOppGrdTblName, 'Opportunities_', '')
                  SET @strPRTemplateGrdTblName = @strPRGrdTblName + 'Template'
                END

            -- Save data into @tabCustomGrids to be used subsequent steps

            INSERT @tabCustomGrids(
              OldTabID,
              NewTabID,
              OldGridID, 
              NewGridID, 
              OppGrdTblName, 
              PRGrdTblName, 
              PRTemplateGrdTblName
            ) 
              SELECT
                @strOldTabID,
                @strNewTabID,
                @strOldGridID, 
                @strNewGridID, 
                @strOppGrdTblName, 
                @strPRGrdTblName, 
                @strPRTemplateGrdTblName

            -- Insert custom grids for Projects.

            INSERT FW_CustomGridsData(
              InfoCenterArea, 
              GridID, 
              TableName, 
              TabID, 
              GridRows, 
              Seq, 
              GridType, 
              PropertyBag
            ) 
              SELECT 
                'Projects', 
                @strNewGridID, 
                @strPRGrdTblName, 
                COALESCE(CST.NewTabID, 'overview') AS TabID, 
                GridRows, 
                Seq, 
                GridType, 
                CGD.PropertyBag
                FROM FW_CustomGridsData AS CGD
                  LEFT JOIN CFGScreenDesignerData AS SDD ON CGD.InfoCenterArea = SDD.InfoCenterArea AND SDD.ComponentID = @strOppGrdTblName AND SDD.ComponentType = 'grid'
                  LEFT JOIN @tabCustomTabs AS CST ON SDD.TabID = CST.OldTabID
                WHERE CGD.InfoCenterArea = 'Opportunities' AND CGD.GridID = @strOldGridID
      
            INSERT INTO FW_CustomGridCaptions (InfocenterArea, GridID, UICultureName, Caption)
            SELECT 'Projects', @strNewGridID, UICultureName, @strOppPrefix + Caption
            FROM FW_CustomGridCaptions
            WHERE InfoCenterArea = 'Opportunities' AND GridID = @strOldGridID

            -- Add each custom grid to CFGScreenDesignerData

            INSERT INTO CFGScreenDesignerData(
              InfocenterArea,
              TabID,
              GridID,
              ComponentID,
              ButtonType,
              ComponentType,
              DesignerCreated,
              GenericPropValue,
              HiddenFor,
              LockedFor,
              RequiredFor,
              RecordLimit,
              ColPos,
              RowPos,
              ColWidth,
              RowHeight,
              CreateDate,
              CreateUser,
              ModDate,
              ModUser
            )
              SELECT 
                'Projects' AS InfoCenterArea,
                COALESCE(CST.NewTabID, 'overview') AS TabID,
                'X' AS GridID,
                @strPRGrdTblName AS ComponentID,
                0 AS ButtonType,
                'grid' AS ComponentType,
                'Y' AS DesignerCreated,
                SDD.GenericPropValue,
                SDD.HiddenFor,
                SDD.LockedFor,
                NULL AS RequiredFor,
                ISNULL(SDD.RecordLimit,0),
                  CASE 
                      WHEN COALESCE(CST.NewTabID, 'overview') = 'overview' THEN -1   /* if it is going on the overview tab, reset the ColPos */
                      ELSE SDD.ColPos
                  END AS ColPos, /* ColPos */
                  CASE 
                      WHEN COALESCE(CST.NewTabID, 'overview') = 'overview' THEN -1  /* if it is going on the overview tab, reset the RowPos */
                      ELSE SDD.RowPos
                  END AS RowPos, /* RowPos */
                  CASE 
                      WHEN COALESCE(CST.NewTabID, 'overview') = 'overview' THEN 1  /* if it is going on the overview tab, reset the ColWidth */
                      ELSE SDD.ColWidth
                  END AS ColWidth, /* ColWidth */
                  CASE 
                      WHEN COALESCE(CST.NewTabID, 'overview') = 'overview' THEN 1  /* if it is going on the overview tab, reset the RowHeight */
                      ELSE SDD.RowHeight
                  END AS RowHeight, /* RowHeight */
                GETUTCDATE() AS CreateDate, 
                @strUserName AS CreateUser,
                GETUTCDATE() AS  ModDate,
                @strUserName AS ModUser
                FROM FW_CustomGridsData CG
                  LEFT JOIN CFGScreenDesignerData AS SDD ON CG.InfoCenterArea = SDD.InfoCenterArea  AND SDD.ComponentID = CG.TableName  AND SDD.ComponentType = 'grid'
                  LEFT JOIN @tabCustomTabs AS CST ON SDD.TabID = CST.OldTabID
                WHERE CG.InfoCenterArea = 'Opportunities' AND CG.GridID = @strOldGridID

            -- Add each custom grid to CFGScreenDesignerLabels

            INSERT INTO CFGScreenDesignerLabels(
              InfocenterArea,
              TabID,
              GridID,
              ComponentID,
              UICultureName,
              Label,
              ToolTip
            )
              SELECT 
                InfoCenterArea AS InfoCenterArea,
                TabID AS TabID, 
                GridID AS GridID,
                ComponentID AS ComponentID,
                UICultureName AS UICultureName,
                @strOppPrefix + Label AS Label,
                ToolTip AS ToolTip
                FROM ( /* X */
                  SELECT 
                    'Projects' AS InfoCenterArea,
                    COALESCE(CST.NewTabID, 'overview') AS TabID, 
                    'X' AS GridID,
                    @strPRGrdTblName AS ComponentID,
                    COALESCE(SDL.UICultureName, 'en-US') AS UICultureName,
                    SDL.Label AS Label,
                    SDL.ToolTip
                    FROM FW_CustomGridsData CG
                      LEFT JOIN CFGScreenDesignerLabels AS SDL ON CG.InfoCenterArea = SDL.InfoCenterArea 
                        AND (CG.TableName = SDL.ComponentID AND SDL.GridID = 'X')
                      LEFT JOIN @tabCustomTabs AS CST ON SDL.TabID = CST.OldTabID
                   WHERE CG.InfoCenterArea = 'Opportunities' AND CG.GridID = @strOldGridID
                ) AS X

            -- Must clone 2 custom grid tables, one for Project, and the other one for Project Template, from Opportunity grid table.
            -- For the Project Template side, only clone structure and not rows.

            EXECUTE ('SELECT * INTO ' + 'dbo.' + @strPRGrdTblName + ' FROM ' + 'dbo.' + @strOppGrdTblName)
            EXECUTE ('SELECT Top 0 * INTO ' + 'dbo.' + @strPRTemplateGrdTblName + ' FROM ' + 'dbo.' + @strOppGrdTblName)
     
            -- Fix up primary key in the Custom Grid's physical tables...

            -- Add columns WBS1, WBS2, WBS3 to new Grid tables

            EXECUTE ('ALTER TABLE ' + @strPRGrdTblName + ' WITH NOCHECK ADD ' + 'WBS1 nvarchar(30) NOT NULL DEFAULT '' ''')
            EXECUTE ('ALTER TABLE ' + @strPRGrdTblName + ' WITH NOCHECK ADD ' + 'WBS2 nvarchar(30) NOT NULL DEFAULT '' ''')
            EXECUTE ('ALTER TABLE ' + @strPRGrdTblName + ' WITH NOCHECK ADD ' + 'WBS3 nvarchar(30) NOT NULL DEFAULT '' ''')

            EXECUTE ('ALTER TABLE ' + @strPRTemplateGrdTblName + ' WITH NOCHECK ADD ' + 'WBS1 nvarchar(30) NOT NULL DEFAULT '' ''')
            EXECUTE ('ALTER TABLE ' + @strPRTemplateGrdTblName + ' WITH NOCHECK ADD ' + 'WBS2 nvarchar(30) NOT NULL DEFAULT '' ''')
            EXECUTE ('ALTER TABLE ' + @strPRTemplateGrdTblName + ' WITH NOCHECK ADD ' + 'WBS3 nvarchar(30) NOT NULL DEFAULT '' ''')

            -- Update WBS1 column from Opportunity

            SET @strSQL = 'UPDATE X SET ' + 
              'WBS1 = O.PRWBS1, ' +
              'WBS2 = '' '', ' +
              'WBS3 = '' '' ' +
              'FROM ' + @strPRGrdTblName + ' AS X ' +
              'INNER JOIN Opportunity AS O ON X.OpportunityID = O.OpportunityID WHERE O.PRWBS1 IS NOT NULL '
            EXEC (@strSQL)

            -- Remove orphan rows

            SET @strSQL = 'DELETE FROM ' + @strPRGrdTblName + ' WHERE ISNULL(WBS1,'' '') = '' '''
            EXEC (@strSQL)

            -- Fix PK

            SET @strSQL = 'ALTER TABLE ' + @strPRGrdTblName + ' WITH NOCHECK ADD CONSTRAINT ' + @strPRGrdTblName + 'PK PRIMARY KEY NONCLUSTERED (WBS1, WBS2, WBS3, Seq)'
            EXEC (@strSQL)

            SET @strSQL = 'ALTER TABLE ' + @strPRTemplateGrdTblName + ' WITH NOCHECK ADD CONSTRAINT ' + @strPRTemplateGrdTblName + 'PK PRIMARY KEY NONCLUSTERED (WBS1, WBS2, WBS3, Seq)'
            EXEC (@strSQL)

            -- drop opportunityID column
            EXECUTE ('ALTER TABLE ' + @strPRGrdTblName + '  DROP Column OpportunityID')
            EXECUTE ('ALTER TABLE ' + @strPRTemplateGrdTblName + '  DROP Column OpportunityID')

            -- Recreate Triggers

            EXECUTE CreateCustomTabTriggers @strPRGrdTblName, 'Projects'
            EXECUTE CreateCustomTabTriggers @strPRTemplateGrdTblName, 'Projects'

            -- Drop Opportunity Grid Table

            --SET @strSQL = 'DROP TABLE ' + @strOppGrdTblName + ' '
            --EXEC (@strSQL)

            FETCH NEXT FROM csr20180521_CustomGrid INTO @strOldTabID, @strNewTabID, @strNewGridID, @strOppGrdTblName

          END

        CLOSE csr20180521_CustomGrid
        DEALLOCATE csr20180521_CustomGrid

        -- Update Seq of entries so that they will be arranged alphabetically

        UPDATE FW_CustomGridsData SET
          Seq = X.Seq
          FROM FW_CustomGridsData
          INNER JOIN ( /* X */
            SELECT 
              XG.GridID, 
              ROW_NUMBER() OVER (ORDER BY XT.TabHeading, XC.Caption) AS Seq
              FROM FW_CustomGridsData AS XG
                INNER JOIN FW_CustomGridCaptions AS XC ON XC.InfocenterArea = XG.InfoCenterArea AND XC.GridID = XG.GridID AND XC.UICultureName = 'en-US'
                  LEFT JOIN FW_InfoCenterTabHeadings AS XT ON XT.InfocenterArea = XG.InfoCenterArea AND XT.TabID = XG.TabID AND XT.UICultureName = 'en-US'
              WHERE XG.InfoCenterArea = 'Projects'
          ) X ON X.GridID = FW_CustomGridsData.GridID
          WHERE FW_CustomGridsData.InfoCenterArea = 'Projects'
  
      END

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- Convert Custom Columns from Opportunity to Project

      IF (OBJECT_ID('dbo.[FW_CustomColumnsData]', 'U') IS NOT NULL)
        BEGIN

          DECLARE @CustomFieldExists CHAR(1)
          SET @CustomFieldExists = 'N'

          OPEN csr20180521_CustomCol
          FETCH NEXT FROM csr20180521_CustomCol INTO @strOldGridID, @strNewColName, @strDefaultValue, @strCustomDataType, @strOppGrdTblName, @strPRGrdTblName, @strPRTemplateGrdTblName

          WHILE (@@FETCH_STATUS = 0)
            BEGIN

              SET @strOldColName = @strNewColName
              SET @iCount = 0

              -- Column does not belong to a grid. 
              -- We have to move the custom field from OpportunityCustomTabFields to ProjectCustomTabFields.

              IF (@strOldGridID = 'X')
                BEGIN

                  -- If Name already exists for Projects, append a number to the end of it to make it unique

                  WHILE (EXISTS(SELECT Name FROM FW_CustomColumnsData WHERE InfoCenterArea = 'Projects' AND GridID = @strOldGridID AND Name = @strNewColName))
                    BEGIN
                      SET @iCount = @iCount + 1
                      -- Truncate the Name to avoid exceeding max length
                      SET @strNewColName = LEFT(@strOldColName, 125 - LEN(@iCount)) + CONVERT(VARCHAR, @iCount)
                    END /* END WHILE */

                  -- Get the column datatype and length

                  SELECT @strDataType = c.name, @siPrec = case when c.name in ('nchar','nvarchar', 'ntext') and b.max_length > 0 then b.max_length/2 when c.name in ('money','bit','decimal','numeric','float','real','money','smallmoney') or c.name like '%int%' or c.name like '%date%' or c.name like '%time%'  then b.precision else b.max_length end FROM sys.objects a INNER JOIN sys.columns b ON a.object_id = b.object_id AND a.name = 'OpportunityCustomTabFields' AND b.name = @strOldColName INNER JOIN sys.types c ON b.user_type_id = c.user_type_id AND c.name <> 'sysname'

                  IF (@strDataType = 'datetime' OR @strDataType = 'timestamp')
                    BEGIN
                      SET @strSQL = 'DATETIME'
                      IF (@strDefaultValue IS NOT NULL)
                        BEGIN
                          IF @strDefaultValue = '[TODAY]' SET @strSQL = @strSQL + ' DEFAULT (GETDATE())'
                          ELSE SET @strSQL = @strSQL + ' DEFAULT ((CONVERT([datetime], REPLACE(''' + @strDefaultValue + ''', ''T'' ,'' ''), (121))))'
                        END
                      SET @strSQL = @strSQL + ' NULL'
                    END
                  ELSE IF (@strDataType IN ('varchar', 'nvarchar', 'string', 'text', 'checkbox', 'memo')) 
                    BEGIN

                      IF (@strDataType = 'text') 
                        SET @strSQL = @strDataType
                      ELSE 
                        BEGIN
                          IF (@siPrec = -1) 
                            SET @strSQL = @strDataType + '(max)'
                          ELSE 
                            SET @strSQL = @strDataType + '(' + CONVERT(VARCHAR, @siPrec) + ')'
                        END

                      IF (@strDefaultValue IS NOT NULL) SET @strSQL = @strSQL + ' DEFAULT (''' + REPLACE(@strDefaultValue, '''', '''''') + ''')'

                      SET @strSQL = @strSQL + ' NULL'

                    END /* END ELSE IF */
                  ELSE 
                    BEGIN
                      IF (@strDefaultValue IS NULL) SET @strDefaultValue = '0'
                      SET @strSQL = 'DECIMAL(19,5) DEFAULT (' + @strDefaultValue + ') NOT NULL'
                    END /* END ELSE */

                  -- Add Field to ProjectCustomTabFields and ProjectCustomTabFieldsTemplate

                  EXECUTE ('ALTER TABLE ProjectCustomTabFields WITH NOCHECK ADD ' + @strNewColName + ' ' + @strSQL)
                  EXECUTE ('ALTER TABLE ProjectCustomTabFieldsTemplate WITH NOCHECK ADD ' + @strNewColName + ' ' + @strSQL)

                  -- Add phone format columns
                  IF (@strCustomDataType = 'phone')
                  BEGIN
                    EXECUTE ('ALTER TABLE ProjectCustomTabFields WITH NOCHECK ADD ' + @strNewColName + 'Format NVARCHAR(24) NULL')
                    EXECUTE ('ALTER TABLE ProjectCustomTabFieldsTemplate WITH NOCHECK ADD ' + @strNewColName + 'Format NVARCHAR(24) NULL')
                  END
                  -- Track changed column names from custom grids or all custom fields that do not belong to a grid for mapping of data later

                  IF (@strOldColName <> @strNewColName OR @strOldGridID = 'X')
                    BEGIN
                      INSERT INTO @tabCustomColumns (GridID, OldColName, NewColName) VALUES (@strOldGridID, @strOldColName, @strNewColName)
                       IF (@strCustomDataType = 'phone')
                       BEGIN
                         INSERT INTO @tabCustomColumns (GridID, OldColName, NewColName) VALUES (@strOldGridID, @strOldColName + 'Format', @strNewColName + 'Format')
                       END
                    END

                  SET @CustomFieldExists = 'Y'

                END /* END IF (@strOldGridID = 'X') */

              ELSE /* Handle the case column belongs to Custom Grid Table */
                BEGIN

                  -- Need to determine the DataType of a column in Opportunity Grid Table so that Default can be set approriately.

                  SELECT 
                    @strDataType = ST.Name
                    FROM sys.objects AS SO 
                    INNER JOIN sys.columns AS SC ON SO.object_id = SC.object_id AND SO.Name = @strOppGrdTblName AND SC.Name = @strOldColName 
                    INNER JOIN sys.types AS ST ON SC.system_type_id = ST.system_type_id AND ST.Name <> 'sysname'

                  SET @strDefaultValue = 
                    CASE
                      WHEN ((@strDataType IN ('datetime', 'timestamp', 'date')) AND (@strDefaultValue IS NOT NULL) AND @strDefaultValue = '[TODAY]') THEN 'GETDATE()'
                      WHEN ((@strDataType IN ('datetime', 'timestamp', 'date')) AND (@strDefaultValue IS NOT NULL) AND @strDefaultValue <> '[TODAY]') THEN 'CONVERT([datetime], REPLACE(''' + @strDefaultValue + ''', ''T'' ,'' ''), (121))'
                      WHEN ((@strDataType IN ('varchar', 'nvarchar', 'string', 'text', 'checkbox', 'memo')) AND (@strDefaultValue IS NOT NULL)) THEN '''' + REPLACE(@strDefaultValue, '''', '''''') + ''''
                      WHEN ((@strDataType IN ('decimal', 'float', 'bigint', 'int', 'smallint', 'tinyint')) AND (@strDefaultValue IS NOT NULL)) THEN '0'
                      ELSE @strDefaultValue
                    END

                  -- For Grid Table, the column names were cloned columns from Opportunity, therefore, must use @strOldColName.
                  -- Add Default for column.

                  IF (@strDefaultValue IS NOT NULL)
                    BEGIN
                      EXECUTE ('ALTER TABLE ' + @strPRGrdTblName + ' ADD DEFAULT ((' + @strDefaultValue + ')) FOR ' + @strOldColName)
                      EXECUTE ('ALTER TABLE ' + @strPRTemplateGrdTblName + ' ADD DEFAULT ((' + @strDefaultValue + ')) FOR ' + @strOldColName)
                    END /* IF (@strDefaultValue IS NOT NULL) */

                END /* END ELSE IF (@strOldGridID = 'X') */

              -- Insert custom columns for Projects

              INSERT INTO FW_CustomColumnsData (
                InfoCenterArea, 
                GridID, 
                Name, 
                TabID, 
                DataType, 
                DisplayWidth, 
                Lines, 
                Sort, 
                LimitToList, 
                Total, 
                Decimals, 
                MinValue, 
                MaxValue, 
                Required, 
                ReqWBSLevel, 
                Seq, 
                CurrencyCode, 
                AvailableForCubes, 
                ColumnID, 
                FieldType, 
                PropertyBag, 
                SearchBy, 
                SearchResults, 
                DefaultValue
              ) 
                SELECT 
                  'Projects' AS InfoCenterArea, 
                  CASE WHEN CG.NewGridID IS NOT NULL THEN CG.NewGridID ELSE CCD.GridID END AS GridID, 
                  @strNewColName AS Name, 
                  COALESCE(CST.NewTabID, CCT.NewTabID, CG.NewTabID, 'overview') AS TabID, 
                  CCD.DataType, 
                  CCD.DisplayWidth, 
                  Lines, 
                  CCD.Sort, 
                  CCD.LimitToList, 
                  CCD.Total, 
                  CCD.Decimals, 
                  CCD.MinValue, 
                  CCD.MaxValue, 
                  CCD.Required, 
                  CCD.ReqWBSLevel, 
                  CCD.Seq, 
                  CCD.CurrencyCode, 
                  CCD.AvailableForCubes, 
                  CCD.ColumnID, 
                  CCD.FieldType, 
                  CCD.PropertyBag, 
                  CCD.SearchBy, 
                  CCD.SearchResults, 
                  CCD.DefaultValue
                  FROM FW_CustomColumnsData AS CCD
                    LEFT JOIN @tabCustomTabs AS CCT ON CCT.OldTabID = CCD.TabID
                    LEFT JOIN @tabCustomGrids AS CG ON CG.OldGridID = CCD.GridID
                    LEFT JOIN CFGScreenDesignerData AS SDD ON CCD.InfoCenterArea = SDD.InfoCenterArea 
                      AND CCD.Name = REPLACE(SDD.ComponentID, 'Opportunity.', '') AND CCD.GridID = SDD.GridID
                    LEFT JOIN @tabCustomTabs AS CST ON SDD.TabID = CST.OldTabID
                  WHERE CCD.InfoCenterArea = 'Opportunities' AND CCD.GridID = @strOldGridID AND CCD.Name = @strOldColName
      
              INSERT INTO FW_CustomColumnCaptions (InfocenterArea, GridID, UICultureName, Name, Label)
              SELECT 'Projects', CASE WHEN CG.NewGridID IS NOT NULL THEN CG.NewGridID ELSE GridID END, UICultureName, @strNewColName, LEFT(CASE WHEN GridID = 'X' THEN @strOppPrefix ELSE '' END + Label,100)
              FROM FW_CustomColumnCaptions
              LEFT JOIN @tabCustomGrids AS CG ON CG.OldGridID = FW_CustomColumnCaptions.GridID
              WHERE InfoCenterArea = 'Opportunities' AND GridID = @strOldGridID AND Name = @strOldColName

              INSERT INTO FW_CustomColumnValuesData (InfoCenterArea, GridID, ColName, DataValue, Seq, Code, UICultureName, CreateUser, CreateDate, ModUser, ModDate)
              SELECT 'Projects', CASE WHEN CG.NewGridID IS NOT NULL THEN CG.NewGridID ELSE GridID END, @strNewColName, DataValue, Seq, Code, UICultureName, @strUserName, GETUTCDATE(), @strUserName, GETUTCDATE()
              FROM FW_CustomColumnValuesData
              LEFT JOIN @tabCustomGrids AS CG ON CG.OldGridID = FW_CustomColumnValuesData.GridID
              WHERE InfoCenterArea = 'Opportunities' AND GridID = @strOldGridID AND ColName = @strOldColName

              -- Copy user defined field attributes to new screen designer metadata

              INSERT INTO CFGScreenDesignerData(
                InfocenterArea,
                TabID,
                GridID,
                ComponentID,
                ButtonType,
                ComponentType,
                DesignerCreated,
                GenericPropValue,
                HiddenFor,
                LockedFor,
                RequiredFor,
                RecordLimit,
                ColPos,
                RowPos,
                ColWidth,
                RowHeight,
                LabelPosition,
                CreateDate,
                CreateUser,
                ModDate,
                ModUser
              )
                SELECT 
                  'Projects' AS InfoCenterArea, 
                  COALESCE(CST.NewTabID, CG.NewTabID, 'overview') AS TabID, 
                  CASE 
                    WHEN CG.NewGridID IS NOT NULL THEN CG.NewGridID 
                    ELSE CCD.GridID 
                  END AS GridID,
                  CASE 
                    WHEN CCD.GridID = 'X' THEN 'PR.' + @strNewColName
                    ELSE @strNewColName 
                  END AS ComponentID,
                  0 AS ButtonType, 
                  CASE CCD.DataType   /* All curency types are stored as currency in the CFGScreenDesignerData table */
                    WHEN 'projectCurrency' THEN 'currency'
                    WHEN 'billingCurrency' THEN 'currency'
                    WHEN 'costCurrency' THEN 'currency'
                    WHEN 'specificCurrency' THEN 'currency'
                    ELSE CCD.DataType
                  END AS ComponentType, 
                  'Y' AS DesignerCreated, 
                  SDD.GenericPropValue, 
                  SDD.HiddenFor, 
                  SDD.LockedFor, 
                  SDD.RequiredFor,
                  0 AS RecordLimit, 
                  CASE 
                      WHEN COALESCE(CST.NewTabID, CG.NewTabID, 'overview') = 'overview' THEN -1   /* if it is going on the overview tab, reset the ColPos */
                      ELSE SDD.ColPos
                  END AS ColPos, /* ColPos */
                  CASE 
                      WHEN COALESCE(CST.NewTabID, CG.NewTabID, 'overview') = 'overview' THEN -1  /* if it is going on the overview tab, reset the RowPos */
                      ELSE SDD.RowPos
                  END AS RowPos, /* RowPos */
                  CASE 
                      WHEN COALESCE(CST.NewTabID, CG.NewTabID, 'overview') = 'overview' THEN 1  /* if it is going on the overview tab, reset the ColWidth */
                      ELSE SDD.ColWidth
                  END AS ColWidth, /* ColWidth */
                  CASE 
                      WHEN COALESCE(CST.NewTabID, CG.NewTabID, 'overview') = 'overview' THEN 1  /* if it is going on the overview tab, reset the RowHeight */
                      ELSE SDD.RowHeight
                  END AS RowHeight, /* RowHeight */
                   CASE 
                      WHEN COALESCE(CST.NewTabID, CG.NewTabID, 'overview') = 'overview' THEN 'T'  /* if it is going on the overview tab, reset the LabelPosition  */
                      ELSE SDD.LabelPosition
                  END AS LabelPosition, /* Label Position */
                  GETUTCDATE() AS CreateDate, 
                  @strUserName AS CreateUser,
                  GETUTCDATE() AS  ModDate,
                  @strUserName AS ModUser
                  FROM FW_CustomColumnsData CCD 
                    LEFT JOIN @tabCustomGrids AS CG ON CCD.GridID = CG.OldGridID
                    LEFT JOIN CFGScreenDesignerData AS SDD ON CCD.InfoCenterArea = SDD.InfoCenterArea 
                      AND CCD.Name = REPLACE(SDD.ComponentID, 'Opportunity.', '') AND CCD.GridID = SDD.GridID
                    LEFT JOIN @tabCustomTabs AS CST ON SDD.TabID = CST.OldTabID
                   WHERE CCD.InfoCenterArea = 'Opportunities' AND  CCD.GridID = @strOldGridID AND  CCD.Name = @strOldColName
  
              INSERT INTO CFGScreenDesignerLabels(
                InfocenterArea,
                TabID,
                GridID,
                ComponentID,
                UICultureName,
                Label,
                ToolTip
              )
                SELECT 
                  InfoCenterArea AS InfoCenterArea,
                  TabID AS TabID, 
                  GridID AS GridID,
                  ComponentID AS ComponentID,
                  UICultureName AS UICultureName,
                  CASE
                    WHEN GridID = 'X' 
                    THEN @strOppPrefix + Label
                    ELSE Label
                  END AS Label,
                  ToolTip AS ToolTip
                  FROM ( /* X */
                    SELECT 
                      'Projects' AS InfoCenterArea,
                      COALESCE(CST.NewTabID, CG.NewTabID, 'overview') AS TabID, 
                      CASE 
                        WHEN CG.NewGridID IS NOT NULL THEN CG.NewGridID 
                        ELSE CCD.GridID 
                      END AS GridID,
                      CASE 
                        WHEN CCD.GridID = 'X' THEN 'PR.' + @strNewColName
                        ELSE @strNewColName 
                      END AS ComponentID,
                      CCL.UICultureName AS UICultureName,
                      COALESCE(SDL.Label, CU2.Caption1, CU.Caption1, CCL.Label) AS Label,
                      SDL.ToolTip
                      FROM FW_CustomColumnCaptions CCL 
                        LEFT JOIN FW_CustomColumnsData CCD ON CCL.InfoCenterArea = CCD.InfoCenterArea AND CCL.GridID = CCD.GridID AND CCL.Name = CCD.Name
                        LEFT JOIN FW_CFGUserSettingsLabels CU 
                          ON CCD.InfocenterArea = CU.infocenterarea AND CCD.GridID = CU.GridID AND CCD.Name = CU.ElementID AND CU.UICultureName= CCL.UICultureName
                        LEFT JOIN FW_CFGUserSettingsLabels CU2 
                          ON CCD.InfocenterArea = CU2.infocenterarea AND CCD.GridID = CU2.GridID AND CCD.Name + 'lbl' = CU2.ElementID AND CU2.UICultureName= CCL.UICultureName
                        LEFT JOIN @tabCustomGrids AS CG ON CCL.GridID = CG.OldGridID
                        LEFT JOIN CFGScreenDesignerData AS SDD ON CCD.InfoCenterArea = SDD.InfoCenterArea
                          AND CCD.Name = REPLACE(SDD.ComponentID, 'Opportunity.', '') AND CCD.GridID = SDD.GridID
                        LEFT JOIN CFGScreenDesignerLabels AS SDL ON SDD.InfoCenterArea = SDL.InfoCenterArea AND SDD.TabID = SDL.TabID 
                          AND SDD.GridID = SDL.GridID AND SDD.ComponentID = SDL.ComponentID AND CCL.UICultureName = SDL.UICultureName
                        LEFT JOIN @tabCustomTabs AS CST ON SDD.TabID = CST.OldTabID
                      WHERE CCL.InfoCenterArea = 'Opportunities' AND CCL.GridID = @strOldGridID AND  CCL.Name = @strOldColName
                  ) AS X
                  WHERE X.Label IS NOT NULL

             -- Fetch Next row

            FETCH NEXT FROM csr20180521_CustomCol INTO @strOldGridID, @strNewColName, @strDefaultValue, @strCustomDataType, @strOppGrdTblName, @strPRGrdTblName, @strPRTemplateGrdTblName

            END /* END WHILE (@@FETCH_STATUS = 0) */

          CLOSE csr20180521_CustomCol
          DEALLOCATE csr20180521_CustomCol

          IF (@CustomFieldExists = 'Y')
            BEGIN

              -- Recreate Triggers

              EXECUTE CreateCustomTabTriggers 'ProjectCustomTabFields', 'Projects'
              EXECUTE CreateCustomTabTriggers 'ProjectCustomTabFieldsTemplate', 'Projects'

              --Update Seq of entries so that they will be arranged alphabetically

              UPDATE FW_CustomColumnsData SET
                Seq = X.Seq
                FROM FW_CustomColumnsData
                  INNER JOIN (
                    SELECT 
                      XCD.GridID, 
                      XCD.Name, 
                      ROW_NUMBER() OVER (PARTITION BY XCD.GridID ORDER BY XT.TabHeading, XC.Label) AS Seq
                      FROM FW_CustomColumnsData AS XCD
                        INNER JOIN FW_CustomColumnCaptions AS XC ON XC.InfoCenterArea = XCD.InfoCenterArea AND XC.GridID = XCD.GridID AND XC.Name = XCD.Name AND XC.UICultureName = 'en-US'
                          LEFT JOIN FW_InfoCenterTabHeadings AS XT ON XT.InfocenterArea = XCD.InfoCenterArea AND XT.TabID = XCD.TabID AND XT.UICultureName = 'en-US'
                      WHERE XCD.InfoCenterArea = 'Projects'
                  ) X ON X.GridID = FW_CustomColumnsData.GridID AND X.Name = FW_CustomColumnsData.Name
                WHERE FW_CustomColumnsData.InfoCenterArea = 'Projects'

            END /* END IF (@CustomFieldExists = 'Y') */

        END /* END IF (OBJECT_ID('dbo.[FW_CustomColumnsData]', 'U') IS NOT NULL) */

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- Move data from OpportunityCustomTabFields into ProjectCustomTabFields

      IF (OBJECT_ID('dbo.[FW_CustomColumnsData]', 'U') IS NOT NULL)
        BEGIN

          OPEN csr20180521_OppCustFields
          FETCH NEXT FROM csr20180521_OppCustFields INTO @strOpportunityID, @strWBS1

          WHILE (@@FETCH_STATUS = 0)
            BEGIN

              -- Map Opportunity data for UDFs to Project record

              IF EXISTS(SELECT 'X' FROM @tabCustomColumns WHERE GridID = 'X') 
                BEGIN

                  IF (EXISTS(SELECT WBS1 FROM ProjectCustomTabFields WHERE WBS1 = @strWBS1 AND WBS2 = ' ' AND WBS3 = ' '))
                    SET @strRecordExists = 'Y'
                  ELSE
                    SET @strRecordExists = 'N'

                  SET @strSQL = ''
                  SET @strCol = 'WBS1, WBS2, WBS3'
                  SET @strVal = '''' + REPLACE(@strWBS1, '''', '''''') + ''','' '','' '''

                  -- Moving column data from OpportunityCustomTabFields to ProjectCustomTabFields

                  DECLARE csr20180521_OppCustCol CURSOR LOCAL FAST_FORWARD FOR
                    SELECT OldColName, NewColName FROM @tabCustomColumns WHERE GridID = 'X'

                  OPEN csr20180521_OppCustCol
                  FETCH NEXT FROM csr20180521_OppCustCol INTO @strOldColName, @strNewColName

                  WHILE (@@FETCH_STATUS = 0)
                    BEGIN
                      IF (@strRecordExists = 'Y')
                        BEGIN
                          IF (@strSQL <> '') SET @strSQL = @strSQL + ', '
                          SET @strSQL = @strSQL + 'P.' + @strNewColName + ' = O.' + @strOldColName
                        END
                      ELSE 
                        BEGIN
                          SET @strCol = @strCol + ', ' + @strNewColName
                          SET @strVal = @strVal + ', ' + @strOldColName
                        END

                      FETCH NEXT FROM csr20180521_OppCustCol INTO @strOldColName, @strNewColName
                    END /* END WHILE */

                  CLOSE csr20180521_OppCustCol
                  DEALLOCATE csr20180521_OppCustCol

                  IF (@strRecordExists = 'Y')
                    BEGIN

                      IF (DATALENGTH(@strSQL) > 0)
                        BEGIN

                          -- Set ModDate and ModUser

                          SET @strSQL = @strSQL + ', ModUser = ''' + @strUserName + ''', ModDate = GETUTCDATE() '

                          SET @strSQL = 'UPDATE P SET ' + @strSQL + ' FROM ProjectCustomTabFields AS P LEFT JOIN OpportunityCustomTabFields AS O ON O.OpportunityID = ''' + @strOpportunityID + ''' WHERE P.WBS1 = ''' + REPLACE(@strWBS1,'''','''''') + ''' AND P.WBS2 = '' '' AND P.WBS3 = '' '''

                        END /* END IF (DATALENGTH(@strSQL) > 0) */

                    END /* END IF (@strRecordExists = 'Y') */
                  ELSE
                    BEGIN

                      IF (DATALENGTH(@strCol) > 0)
                        BEGIN

                          -- Set ModDate and ModUser

                          SET @strCol = @strCol + ', ModUser, ModDate' 
                          SET @strVal = @strVal + ', ''' + @strUserName + ''', GETUTCDATE()'

                          SET @strSQL = 'INSERT INTO ProjectCustomTabFields (' + @strCol + ') SELECT ' + @strVal + ' FROM OpportunityCustomTabFields AS O WHERE OpportunityID = ''' + @strOpportunityID + ''''

                        END /* END IF (DATALENGTH(@strCol) > 0) */

                    END /* END ELSE */

                  EXEC (@strSQL)

                END

              FETCH NEXT FROM csr20180521_OppCustFields INTO @strOpportunityID, @strWBS1

            END /* END WHILE (@@FETCH_STATUS = 0) */

          CLOSE csr20180521_OppCustFields
          DEALLOCATE csr20180521_OppCustFields

        END /* END IF (OBJECT_ID('dbo.[FW_CustomColumnsData]', 'U') IS NOT NULL) */

        ---Convert custom fields of type opportunity to projects
        DECLARE @TableName		VARCHAR(100),
                @ColName		VARCHAR(125)

	    DECLARE OppUDFCursor CURSOR FOR
	    SELECT 
		    CASE C.GridID
			    WHEN 'X' THEN
				    CASE C.InfoCenterArea
					    WHEN 'Activity' THEN 'ActivityCustomTabFields'
					    WHEN 'ChartOfAccounts' THEN 'AccountCustomTabFields'
					    WHEN 'Clients' THEN 'ClientCustomTabFields'
					    WHEN 'Contacts' THEN 'ContactCustomTabFields'
					    WHEN 'Employees' THEN 'EmployeeCustomTabFields'
					    WHEN 'Equipment' THEN 'EquipmentCustomTabFields'
					    WHEN 'Leads' THEN 'LeadCustomTabFields'
					    WHEN 'MktCampaigns' THEN 'MktCampaignCustomTabFields'
					    WHEN 'Opportunities' THEN 'OpportunityCustomTabFields'
					    WHEN 'ProjectPlan' THEN 'PlanningCustomTabFields'
					    WHEN 'Projects' THEN 'ProjectCustomTabFields'
					    WHEN 'TextLibrary' THEN 'TextLibraryCustomTabFields'
					    WHEN 'Vendors' THEN 'VendorCustomTabFields'
					    ELSE C.InfoCenterArea
				    END
			    ELSE G.TableName
		    END AS TableName,
		    C.Name As ColName
	    FROM FW_CustomColumnsData C
	    LEFT JOIN FW_CustomGridsData G ON G.InfoCenterArea = C.InfoCenterArea AND G.GridID = C.GridID
	    WHERE C.DataType = 'opportunity'
	    ORDER BY C.InfoCenterArea, C.GridID
	    BEGIN
		    OPEN OppUDFCursor

		    FETCH NEXT FROM OppUDFCursor INTO  @TableName, @ColName

		    WHILE @@FETCH_STATUS = 0
		    BEGIN
			    IF EXISTS (SELECT 'x' FROM sys.columns a, sys.objects b WHERE a.object_id = b.object_id AND a.name = @ColName and b.name = @TableName) 
                BEGIN
				    SET @strSQL = 'UPDATE ' + @TableName + ' SET ' + @ColName + ' =  O.PRWBS1 FROM ' + @TableName + ' as Tbl ' +
                                ' INNER JOIN Opportunity AS O ON Tbl.' + @ColName +' =O.OpportunityID   INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND PR.WBS2 = '' '' AND PR.WBS3 = '' ''  '
                    EXEC(@strSQL)            

				    IF @TableName = 'ActivityCustomTabFields' BEGIN
					    IF EXISTS (SELECT 'x' FROM sys.columns a, sys.objects b WHERE a.object_id = b.object_id AND a.name = @ColName and b.name = 'WorkflowActivityCustomTabFields')
                            BEGIN
                                SET @strSQL = 'UPDATE WorkflowActivityCustomTabFields SET ' + @ColName + ' =  O.PRWBS1 FROM WorkflowActivityCustomTabFields AS WFA '+ 
                                    ' INNER JOIN Opportunity AS O ON WFA.' + @ColName +' =O.OpportunityID  INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND PR.WBS2 = '' '' AND PR.WBS3 = '' '' '
                                EXEC(@strSQL)     
                            END
				    END
				    ELSE IF @TableName = 'ProjectCustomTabFields' BEGIN
					    IF EXISTS (SELECT 'x' FROM sys.columns a, sys.objects b WHERE a.object_id = b.object_id AND a.name = @ColName and b.name = 'ProjectCustomTabFieldsTemplate')
                            BEGIN
                                SET @strSQL = 'UPDATE ProjectCustomTabFieldsTemplate SET ' + @ColName + ' =  O.PRWBS1 FROM ProjectCustomTabFieldsTemplate as PCT '+ 
                                    ' INNER JOIN Opportunity AS O ON PCT.' + @ColName +' =O.OpportunityID INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND PR.WBS2 = '' '' AND PR.WBS3 = '' '' '
                                EXEC(@strSQL)     
                            END
				    END
			    END

			    FETCH NEXT FROM OppUDFCursor INTO  @TableName, @ColName
		    END
	    END

	    CLOSE OppUDFCursor
	    DEALLOCATE OppUDFCursor

	    UPDATE FW_CustomColumnsData SET DataType = 'wbs1' WHERE DataType = 'opportunity'
        UPDATE CFGScreenDesignerData set ComponentType = 'wbs1' WHERE ComponentType = 'opportunity'


        /* select the roles that have access to projects */
        DECLARE rolecursor CURSOR FOR 
            SELECT role, AccessAllTabs FROM se where se.AccessAllNavNodes='y'  
            or exists (select 'x' from senavtree where senavtree.role=se.role and senavtree.nodeid='projects')
        BEGIN 
            OPEN rolecursor 

            FETCH next FROM rolecursor INTO @strRole, @strAccessAllTabs
            WHILE @@FETCH_STATUS = 0 
                BEGIN 

                /* Custom Grid Component */
                INSERT INTO sefield (role, infocenterarea, gridid, componentid, readonly, createuser, createdate, moduser, moddate) 
                SELECT DISTINCT @strRole, 'Projects', 'X', CG.PRGrdTblName,  
                        CASE WHEN Charindex(',_ALL_,', ',' + Isnull(lockedfor, '') + ',') > 0  OR Charindex(',' + @strRole + ',', ',' + lockedfor + ',') > 0 THEN 'Y' ELSE 'N' END, 
                        'Conversion', Getdate(), 'Conversion', Getdate() 
                FROM  @tabCustomGrids  CG
                        INNER JOIN cfgscreendesignerdata sd ON sd.infocenterarea ='Projects' AND sd.gridid ='X' AND  sd.componentid =CG.PRGrdTblName
                WHERE  Charindex(',_ALL_,', ',' + Isnull(hiddenfor, '') + ',') = 0
                AND  Charindex(',' + @strRole + ',', ',' + Isnull(hiddenfor, '') + ',') = 0 
                AND (sd.tabid ='Overview' or @strAccessAllTabs ='Y')
 
                /* Custom Grid Seq column */
                INSERT INTO sefield (role, infocenterarea, gridid, componentid, tablename, columnname, readonly, createuser, createdate, moduser, moddate) 
                SELECT DISTINCT @strRole, 'Projects', CG.NewGridID, 'Seq', CG.PRGrdTblName, 'Seq',
                        CASE WHEN Charindex(',_ALL_,', ',' + Isnull(lockedfor, '') + ',') > 0  OR Charindex(',' + @strRole + ',', ',' + lockedfor + ',') > 0 THEN 'Y' ELSE 'N' END, 
                        'Conversion', Getdate(), 'Conversion', Getdate() 
                FROM  @tabCustomGrids CG
                        INNER JOIN cfgscreendesignerdata sd ON sd.infocenterarea ='Projects' AND sd.gridid ='X' AND  sd.componentid =CG.PRGrdTblName
                WHERE  Charindex(',_ALL_,', ',' + Isnull(hiddenfor, '') + ',') = 0
                AND  Charindex(',' + @strRole + ',', ',' + Isnull(hiddenfor, '') + ',') = 0 
                AND (sd.tabid ='Overview' or @strAccessAllTabs ='Y')

                /* Custom Grid WBS1 column */
                INSERT INTO sefield (role, infocenterarea, gridid, componentid, tablename, columnname, readonly, createuser, createdate, moduser, moddate) 
                SELECT DISTINCT @strRole, 'Projects', CG.NewGridID, 'WBS1', CG.PRGrdTblName, 'WBS1',
                        CASE WHEN Charindex(',_ALL_,', ',' + Isnull(lockedfor, '') + ',') > 0  OR Charindex(',' + @strRole + ',', ',' + lockedfor + ',') > 0 THEN 'Y' ELSE 'N' END, 
                        'Conversion', Getdate(), 'Conversion', Getdate() 
                FROM  @tabCustomGrids CG
                        INNER JOIN cfgscreendesignerdata sd ON sd.infocenterarea ='Projects' AND sd.gridid ='X' AND  sd.componentid =CG.PRGrdTblName
                WHERE  Charindex(',_ALL_,', ',' + Isnull(hiddenfor, '') + ',') = 0
                AND  Charindex(',' + @strRole + ',', ',' + Isnull(hiddenfor, '') + ',') = 0 
                AND (sd.tabid ='Overview' or @strAccessAllTabs ='Y')

                /* Custom Grid WBS2 column */
                INSERT INTO sefield (role, infocenterarea, gridid, componentid, tablename, columnname, readonly, createuser, createdate, moduser, moddate) 
                SELECT DISTINCT @strRole, 'Projects', CG.NewGridID, 'WBS2', CG.PRGrdTblName, 'WBS2',
                        CASE WHEN Charindex(',_ALL_,', ',' + Isnull(lockedfor, '') + ',') > 0  OR Charindex(',' + @strRole + ',', ',' + lockedfor + ',') > 0 THEN 'Y' ELSE 'N' END, 
                        'Conversion', Getdate(), 'Conversion', Getdate() 
                FROM  @tabCustomGrids CG
                        INNER JOIN cfgscreendesignerdata sd ON sd.infocenterarea ='Projects' AND sd.gridid ='X' AND  sd.componentid =CG.PRGrdTblName
                WHERE  Charindex(',_ALL_,', ',' + Isnull(hiddenfor, '') + ',') = 0
                AND  Charindex(',' + @strRole + ',', ',' + Isnull(hiddenfor, '') + ',') = 0 
                AND (sd.tabid ='Overview' or @strAccessAllTabs ='Y')

                /* Custom Grid WBS3 column */
                INSERT INTO sefield (role, infocenterarea, gridid, componentid, tablename, columnname, readonly, createuser, createdate, moduser, moddate) 
                SELECT DISTINCT @strRole, 'Projects', CG.NewGridID, 'WBS3', CG.PRGrdTblName, 'WBS3',
                        CASE WHEN Charindex(',_ALL_,', ',' + Isnull(lockedfor, '') + ',') > 0  OR Charindex(',' + @strRole + ',', ',' + lockedfor + ',') > 0 THEN 'Y' ELSE 'N' END, 
                        'Conversion', Getdate(), 'Conversion', Getdate() 
                FROM  @tabCustomGrids CG
                        INNER JOIN cfgscreendesignerdata sd ON sd.infocenterarea ='Projects' AND sd.gridid ='X' AND  sd.componentid =CG.PRGrdTblName
                WHERE   Charindex(',_ALL_,', ',' + Isnull(hiddenfor, '') + ',') = 0
                AND  Charindex(',' + @strRole + ',', ',' + Isnull(hiddenfor, '') + ',') = 0 
                AND (sd.tabid ='Overview' or @strAccessAllTabs ='Y')

                /* Custom Grid columns */
                INSERT INTO sefield (role, infocenterarea, gridid, componentid, tablename, columnname, readonly, createuser, createdate, moduser, moddate) 
                SELECT DISTINCT @strRole, 'Projects', CG.NewGridID, FW_CustomColumnsData.Name, CG.PRGrdTblName, FW_CustomColumnsData.Name,
                        CASE WHEN Charindex(',_ALL_,', ',' + Isnull(sd.lockedfor, '') + ',') > 0  OR Charindex(',' + @strRole + ',', ',' + sd.lockedfor + ',') > 0 THEN 'Y' ELSE 'N' END, 
                        'Conversion', Getdate(), 'Conversion', Getdate() 
                FROM  FW_CustomColumnsData 
                        INNER JOIN @tabCustomGrids CG ON FW_CustomColumnsData.GridID = CG.NewGridID 
                        INNER JOIN cfgscreendesignerdata sd ON sd.infocenterarea ='Projects' AND sd.gridid = FW_CustomColumnsData.GridID AND  sd.componentid = FW_CustomColumnsData.name
                        INNER JOIN cfgscreendesignerdata sdg ON sdg.infocenterarea ='Projects' AND sdg.gridid = 'X' AND  sdg.componentid = CG.PRGrdTblName
                WHERE  Charindex(',_ALL_,', ',' + Isnull(sd.hiddenfor, '') + ',') = 0
                AND  Charindex(',' + @strRole + ',', ',' + Isnull(sd.hiddenfor, '') + ',') = 0 
                AND FW_CustomColumnsData.InfocenterArea='Projects'
                AND (sdg.tabid ='Overview' or @strAccessAllTabs ='Y')

                /* Special Custom Grid description columns */
                INSERT INTO sefield (role, infocenterarea, gridid, componentid, tablename, columnname, readonly, createuser, createdate, moduser, moddate) 
                SELECT DISTINCT @strRole, 'Projects', CG.NewGridID, 'desc_'+ FW_CustomColumnsData.Name, CG.PRGrdTblName, 'desc_'+ FW_CustomColumnsData.Name,
                        CASE WHEN Charindex(',_ALL_,', ',' + Isnull(sd.lockedfor, '') + ',') > 0  OR Charindex(',' + @strRole + ',', ',' + sd.lockedfor + ',') > 0 THEN 'Y' ELSE 'N' END, 
                        'Conversion', Getdate(), 'Conversion', Getdate() 
                FROM  FW_CustomColumnsData 
                        INNER JOIN @tabCustomGrids CG ON FW_CustomColumnsData.GridID = CG.NewGridID 
                        INNER JOIN cfgscreendesignerdata sd ON sd.infocenterarea ='Projects' AND sd.gridid = FW_CustomColumnsData.GridID AND  sd.componentid = FW_CustomColumnsData.name
                        INNER JOIN cfgscreendesignerdata sdg ON sdg.infocenterarea ='Projects' AND sdg.gridid = 'X' AND  sdg.componentid = CG.PRGrdTblName
                WHERE Charindex(',_ALL_,', ',' + Isnull(sd.hiddenfor, '') + ',') = 0
                AND  Charindex(',' + @strRole + ',', ',' + Isnull(sd.hiddenfor, '') + ',') = 0 
                AND FW_CustomColumnsData.InfocenterArea='Projects'
                AND (DataType like 'udic%' or datatype in ('firm','contact','employee','wbs1','equipment','mkt','textlibrary','account','org'))
                AND (sdg.tabid ='Overview' or @strAccessAllTabs ='Y')

                /* Special Custom Grid phone format columns columns */
                INSERT INTO sefield (role, infocenterarea, gridid, componentid, tablename, columnname, readonly, createuser, createdate, moduser, moddate) 
                SELECT DISTINCT @strRole, 'Projects', CG.NewGridID, FW_CustomColumnsData.name +'Format', CG.PRGrdTblName, FW_CustomColumnsData.name +'Format',
                        CASE WHEN Charindex(',_ALL_,', ',' + Isnull(sd.lockedfor, '') + ',') > 0 OR Charindex(',' + @strRole + ',', ',' + sd.lockedfor + ',') > 0 THEN 'Y' ELSE 'N' END, 
                        'Conversion', Getdate(), 'Conversion', Getdate() 
                FROM  FW_CustomColumnsData 
                        INNER JOIN @tabCustomGrids CG ON FW_CustomColumnsData.GridID = CG.NewGridID 
                        INNER JOIN cfgscreendesignerdata sd ON sd.infocenterarea ='Projects' AND sd.gridid = FW_CustomColumnsData.GridID AND  sd.componentid = FW_CustomColumnsData.name
                        INNER JOIN cfgscreendesignerdata sdg ON sdg.infocenterarea ='Projects' AND sdg.gridid = 'X' AND  sdg.componentid = CG.PRGrdTblName
                WHERE Charindex(',_ALL_,', ',' + Isnull(sd.hiddenfor, '') + ',') = 0
                AND  Charindex(',' + @strRole + ',', ',' + Isnull(sd.hiddenfor, '') + ',') = 0 
                AND FW_CustomColumnsData.InfocenterArea='Projects'
                AND DataType='Phone'
                AND (sdg.tabid ='Overview' or @strAccessAllTabs ='Y')

                 /* Non grid custom columns */
                INSERT INTO sefield (role, infocenterarea, gridid, componentid, tablename, columnname, readonly, createuser, createdate, moduser, moddate) 
                SELECT DISTINCT @strRole, 'Projects', 'X', 'PR.' + FW_CustomColumnsData.Name, 'ProjectCustomTabFields', FW_CustomColumnsData.Name,
                        CASE WHEN Charindex(',_ALL_,', ',' + Isnull(lockedfor, '') + ',') > 0 OR Charindex(',' + @strRole + ',', ',' + lockedfor + ',') > 0 THEN 'Y' ELSE 'N' END, 
                        'Conversion', Getdate(), 'Conversion', Getdate() 
                FROM  FW_CustomColumnsData 
                        INNER JOIN @tabCustomColumns CC ON FW_CustomColumnsData.GridID ='X' and FW_CustomColumnsData.Name = CC.NewColName
                        INNER JOIN cfgscreendesignerdata sd ON sd.infocenterarea ='Projects' AND sd.gridid = FW_CustomColumnsData.GridID AND sd.componentid ='PR.' + FW_CustomColumnsData.Name
                WHERE Charindex(',_ALL_,', ',' + Isnull(hiddenfor, '') + ',') = 0
                AND  Charindex(',' + @strRole + ',', ',' + Isnull(hiddenfor, '') + ',') = 0 
                AND FW_CustomColumnsData.InfocenterArea='Projects'
                AND (sd.tabid ='Overview' or @strAccessAllTabs ='Y')

                 /* Special non grid custom description columns */
                INSERT INTO sefield (role, infocenterarea, gridid, componentid, tablename, columnname, readonly, createuser, createdate, moduser, moddate) 
                SELECT DISTINCT @strRole, 'Projects', 'X', 'PR.desc_'+ FW_CustomColumnsData.Name, 'ProjectCustomTabFields', 'desc_' + FW_CustomColumnsData.Name,
                        CASE WHEN Charindex(',_ALL_,', ',' + Isnull(lockedfor, '') + ',') > 0 OR Charindex(',' + @strRole + ',', ',' + lockedfor + ',') > 0 THEN 'Y' ELSE 'N' END, 
                        'Conversion', Getdate(), 'Conversion', Getdate() 
                FROM  FW_CustomColumnsData 
                        INNER JOIN @tabCustomColumns CC ON FW_CustomColumnsData.GridID ='X' and FW_CustomColumnsData.Name = CC.NewColName
                        INNER JOIN cfgscreendesignerdata sd ON sd.infocenterarea ='Projects' AND sd.gridid = FW_CustomColumnsData.GridID AND sd.componentid ='PR.' +  FW_CustomColumnsData.Name
                WHERE  Charindex(',_ALL_,', ',' + Isnull(hiddenfor, '') + ',') = 0
                AND  Charindex(',' + @strRole + ',', ',' + Isnull(hiddenfor, '') + ',') = 0 
                AND FW_CustomColumnsData.InfocenterArea='Projects'
                AND (DataType like 'udic%' or datatype in ('firm','contact','employee','wbs1','equipment','mkt','textlibrary','account','org'))
                AND (sd.tabid ='Overview' or @strAccessAllTabs ='Y')

                 /* Special non grid custom phone format columns columns */
                INSERT INTO sefield (role, infocenterarea, gridid, componentid, tablename, columnname, readonly, createuser, createdate, moduser, moddate) 
                SELECT DISTINCT @strRole, 'Projects', 'X', 'PR.' + FW_CustomColumnsData.Name  +'Format', 'ProjectCustomTabFields', FW_CustomColumnsData.Name  +'Format',
                        CASE WHEN Charindex(',_ALL_,', ',' + Isnull(lockedfor, '') + ',') > 0 OR Charindex(',' + @strRole + ',', ',' + lockedfor + ',') > 0 THEN 'Y' ELSE 'N' END, 
                        'Conversion', Getdate(), 'Conversion', Getdate() 
                FROM  FW_CustomColumnsData 
                        INNER JOIN @tabCustomColumns CC ON FW_CustomColumnsData.GridID ='X' and FW_CustomColumnsData.Name = CC.NewColName
                        INNER JOIN cfgscreendesignerdata sd ON sd.infocenterarea ='Projects' AND sd.gridid = FW_CustomColumnsData.GridID AND sd.componentid ='PR.' +  FW_CustomColumnsData.Name
                WHERE  Charindex(',_ALL_,', ',' + Isnull(hiddenfor, '') + ',') = 0
                AND  Charindex(',' + @strRole + ',', ',' + Isnull(hiddenfor, '') + ',') = 0 
                AND FW_CustomColumnsData.InfocenterArea='Projects'
                AND DataType='Phone'
                AND (sd.tabid ='Overview' or @strAccessAllTabs ='Y')

                /* Add tabs if role has access to all tabs */
                IF @strAccessAllTabs='Y'
                BEGIN           
                    INSERT INTO SETAB (role, infocenterarea, tabid, createuser, createdate, moduser, moddate) 
                    SELECT @strRole, 'Projects', CT.NewTabID, 'Conversion', Getdate(), 'Conversion', Getdate()
                    FROM  @tabCustomTabs CT
                END


                FETCH next FROM rolecursor INTO @strRole, @strAccessAllTabs 
            END 
        END 

        CLOSE rolecursor 
        DEALLOCATE rolecursor 

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- Removing obsolete rows from FW_CFGLabelData, FW_InfoCenterTabHeadings, FW_InfoCenterTabsData

      --DELETE FROM FW_InfoCenterTabsData WHERE InfoCenterArea = 'Opportunities'
      --DELETE FROM FW_InfoCenterTabHeadings WHERE InfoCenterArea = 'Opportunities'
      --DELETE FROM FW_CFGUserSettingsData WHERE InfoCenterArea = 'Opportunities'
      --DELETE FROM FW_CFGUserSettingsLabels WHERE InfoCenterArea = 'Opportunities'

      -- Removing obsolete rows from FW_CustomColumnsData, FW_CustomColumnCaptions, FW_CustomGridsData, FW_CustomGridCaptions, FW_CustomColumnValuesData

      --DELETE FROM FW_CustomColumnsData WHERE InfoCenterArea = 'Opportunities'
      --DELETE FROM FW_CustomColumnCaptions WHERE InfoCenterArea = 'Opportunities'
      --DELETE FROM FW_CustomGridsData WHERE InfoCenterArea = 'Opportunities'
      --DELETE FROM FW_CustomGridCaptions WHERE InfoCenterArea = 'Opportunities'
      --DELETE FROM FW_CustomColumnValuesData WHERE InfoCenterArea = 'Opportunities'

      -- Removing obsolete rows CFGScreenDesignerData, CFGScreenDesignerLabels 

      --DELETE FROM CFGScreenDesignerData WHERE InfoCenterArea = 'Opportunities'
      --DELETE FROM CFGScreenDesignerLabels WHERE InfoCenterArea = 'Opportunities'

      -- Removing obsolete Opportunity tables 

      --DROP TABLE OpportunityCustomTabFields

    END /* END IF */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF

END -- stRPConvertOppUDIC2PR
GO
