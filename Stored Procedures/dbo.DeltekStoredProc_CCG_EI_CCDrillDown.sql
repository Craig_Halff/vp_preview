SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_CCDrillDown] ( @SP nvarchar(max), @Params nvarchar(max))
             AS EXEC spCCG_EI_CCDrillDown @SP,@Params
GO
