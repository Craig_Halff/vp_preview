SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[pmGetUntTPD]
  @strPlanID VARCHAR(32),
  @strETCDate VARCHAR(10), -- Date must be in format: 'yyyy-mm-dd'
  @strRetrievalMode VARCHAR(1) = '3',
  @strGetPlanned VARCHAR(1) = 'Y',
  @strGetBaseline VARCHAR(1) = 'Y',
  @strGetJTD VARCHAR(1) = 'Y'
AS

BEGIN -- Procedure pmGetUntTPD

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

  DECLARE @startTime datetime
  DECLARE @totalTime int
  
  SET @startTime = GetDate()
  
  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
  
  DECLARE @intQtyDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intECURevDecimals int
  
  DECLARE @intBaselineUntCount int
  DECLARE @intPlannedUntCount int
  DECLARE @intJTDUntCount int
  
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strUserName Nvarchar(32)

  DECLARE @strOutlineNumber varchar(255)

  DECLARE @strTaskID varchar(32)
  DECLARE @strUnitID varchar(32)
  
	DECLARE @strHasJTDUnt varchar(1)
  
  -- Declare Temp tables.
           
  DECLARE @tabTopTask 
    TABLE (TaskID varchar(32) COLLATE database_default,
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(7) COLLATE database_default,
           WBS3 Nvarchar(7) COLLATE database_default
           PRIMARY KEY(TaskID))

  DECLARE @tabJTDCalendar 
    TABLE (StartDate datetime,
           EndDate datetime
           PRIMARY KEY(StartDate)) 

  DECLARE @tabLedger 
    TABLE (RowID int IDENTITY,
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(7) COLLATE database_default,
           WBS3 Nvarchar(7) COLLATE database_default,
           Account Nvarchar(13) COLLATE database_default,
           Vendor Nvarchar(30) COLLATE database_default,
           Unit Nvarchar(30) COLLATE database_default,
           UnitTable Nvarchar(30) COLLATE database_default,
           StartDate datetime,
           EndDate datetime,
           PeriodQty decimal(19,4), 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4),
           PostedFlg smallint
           PRIMARY KEY(RowID, WBS1, WBS2, WBS3, StartDate))

  DECLARE @tabJTDUnt
    TABLE (RowID int IDENTITY,
           PlanID varchar(32) COLLATE database_default,
           TaskID varchar(32) COLLATE database_default,
           UnitID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodQty decimal(19,4), 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PostedFlg smallint 
           PRIMARY KEY(RowID, PlanID, TaskID, StartDate, EndDate))
          
  -- Save Active Company string for use later.
  
  SET @strCompany = dbo.GetActiveCompany()
  SET @strUserName = dbo.GetVisionAuditUserName()
          
  -- Set JTD/ETC Dates.
  
  SET @dtETCDate = CONVERT(datetime, REPLACE(@strETCDate, '-', ''))
  SET @dtJTDDate = DATEADD(d, -1, @dtETCDate) 

  -- Get decimal settings.
  
  SELECT @intQtyDecimals = QtyDecimals,
         @intAmtCostDecimals = AmtCostDecimals,
         @intAmtBillDecimals = AmtBillDecimals,
         @intECURevDecimals = ECURevDecimals
    FROM dbo.RP$tabDecimals(@strPlanID)
         
  -- Initialize counting variables.
  
  SELECT 
    @intBaselineUntCount = 0,
    @intPlannedUntCount = 0,
    @intJTDUntCount = 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strGetJTD = 'Y' AND @strRetrievalMode != '1')
    BEGIN
 
      INSERT @tabTopTask(TaskID, WBS1, WBS2, WBS3)
        SELECT DISTINCT TaskID, WBS1, WBS2, WBS3
          FROM RPTask AS T
          INNER JOIN (SELECT MIN(OutlineLevel) AS OutlineLevel
              FROM RPTask AS XT INNER JOIN RPWBSLevelFormat AS F ON XT.PlanID = F.PlanID
              AND XT.WBSType = F.WBSType AND F.WBSMatch = 'Y'
              WHERE XT.PlanID = @strPlanID) AS ZT ON T.OutlineLevel = ZT.OutlineLevel
          WHERE T.PlanID = @strPlanID AND T.WBS1 IS NOT NULL AND T.WBS1 != '<none>'
      
	    IF (@@ROWCOUNT > 0)
	      BEGIN
	      
	        -- Compute data for JTD Calendar table with overflow periods before and after the regular calendar.

	        INSERT @tabJTDCalendar(StartDate, EndDate)
		        SELECT StartDate, EndDate FROM
			        (SELECT StartDate AS StartDate, EndDate AS EndDate
			           FROM RPCalendarInterval WHERE PlanID = @strPlanID
			         UNION ALL
			         SELECT CAST('19300101' AS datetime) AS StartDate,
			           DATEADD(d, -1, MIN(StartDate)) AS EndDate
			           FROM RPCalendarInterval WHERE PlanID = @strPlanID
			           GROUP BY PlanID
			         UNION ALL
			         SELECT DATEADD(d, 1, MAX(EndDate)) AS StartDate,
			           DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate))) AS EndDate
			           FROM RPCalendarInterval WHERE PlanID = @strPlanID
			        ) AS CI
            
          -- Initialize control variables to indicate whether we have JTD data of certain type.

	        SET @strHasJTDUnt = 'N'
    
          --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

          -- Save Unit JTD for this Plan into temp table.
          
  	        INSERT @tabLedger
              (WBS1,
               WBS2,
               WBS3,
               Account,
               Vendor,
               Unit,
               UnitTable,
               StartDate,
               EndDate,
               PeriodQty, 
               PeriodCost, 
               PeriodBill,
               PostedFlg)
		          SELECT
			          Ledger.WBS1 AS WBS1,
			          Ledger.WBS2 AS WBS2,
			          Ledger.WBS3 AS WBS3,
			          Ledger.Account AS Account,
			          Ledger.Vendor AS Vendor,
			          Ledger.Unit AS Unit,
			          Ledger.UnitTable AS UnitTable,
			          CI.StartDate AS StartDate,
			          CI.EndDate AS EndDate,
			          SUM(UnitQuantity) AS PeriodQty,
                SUM(AmountProjectCurrency) AS PeriodCost,  
                SUM(BillExt) AS PeriodBill, 
			          1 AS PostedFlg
                  FROM LedgerMISC AS Ledger
                    INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                      AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                      AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                      AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                      AND Ledger.TransType = 'UN'
                    INNER JOIN @tabJTDCalendar AS CI ON Ledger.TransDate BETWEEN CI.StartDate AND CI.EndDate 
			            GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, Ledger.Account, Ledger.Vendor, 
			              Ledger.Unit, Ledger.UnitTable, CI.StartDate, CI.EndDate
			          
	        IF (@@ROWCOUNT > 0) SET @strHasJTDUnt = 'Y'

          IF (@strHasJTDUnt = 'Y')
            BEGIN
 
              INSERT @tabJTDUnt
                (PlanID,
                 TaskID,  
                 UnitID,  
                 StartDate,  
                 EndDate,
                 PeriodQty,  
                 PeriodCost,  
                 PeriodBill,  
                 PostedFlg)
                SELECT
                  @strPlanID AS PlanID,
                  TaskID,  
                  UnitID,  
                  StartDate,  
                  EndDate,  
                  SUM(ISNULL(PeriodQty, 0)) AS PeriodQty,  
                  SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,  
                  SUM(ISNULL(X.PeriodBill, 0)) AS PeriodBill,  
                  SIGN(MIN(PostedFlg)) AS PostedFlg
                  FROM
                    (SELECT U.UnitID,  
                       U.TaskID,  
                       Ledger.StartDate,  
                       Ledger.EndDate,  
                       SUM(PeriodQty) AS PeriodQty,  
                       SUM(PeriodCost) AS PeriodCost,  
                       SUM(PeriodBill) AS PeriodBill,  
                       PostedFlg
                       FROM @tabLedger AS Ledger
                         INNER JOIN RPUnit AS U ON Ledger.WBS1 = U.WBS1
                           AND Ledger.WBS2 LIKE (ISNULL(U.WBS2, '%'))  
                           AND Ledger.WBS3 LIKE (ISNULL(U.WBS3, '%'))
                           AND Ledger.Unit = U.Unit 
                           AND Ledger.UnitTable = U.UnitTable 
                           AND Ledger.Account = U.Account 
						 INNER JOIN RPTask AS T ON Ledger.WBS1 = T.WBS1 AND T.TaskID = U.TaskID
                           AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                           AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))
          
                         INNER JOIN RPWBSLevelFormat AS F ON T.PlanID = F.PlanID AND T.WBSType = F.WBSType
                           AND F.FmtLevel != 0 AND F.WBSMatch = 'Y' AND F.WBSType != 'LBCD'

                         INNER JOIN (SELECT MIN(UnitID) AS UnitID FROM RPUnit  
                           WHERE PlanID = @strPlanID  
                           GROUP BY TaskID, Unit, Account) AS U1 ON U.UnitID = U1.UnitID
                       WHERE U.PlanID = @strPlanID  
                       GROUP BY U.UnitID, U.TaskID, Ledger.StartDate, Ledger.EndDate, PostedFlg
                     UNION ALL
                     SELECT NULL AS UnitID,  
                       T.TaskID,  
                       Ledger.StartDate,  
                       Ledger.EndDate,  
                       SUM(PeriodQty) AS PeriodQty,  
                       SUM(PeriodCost) AS PeriodCost,  
                       SUM(PeriodBill) AS PeriodBill,  
                       PostedFlg
                       FROM @tabLedger AS Ledger
                         INNER JOIN RPTask AS T ON Ledger.WBS1 = T.WBS1
                           AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                           AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                         INNER JOIN RPWBSLevelFormat AS F ON T.PlanID = F.PlanID AND T.WBSType = F.WBSType
                           AND F.FmtLevel != 0 AND F.WBSMatch = 'Y' AND F.WBSType != 'LBCD'
                       WHERE T.PlanID = @strPlanID
                       GROUP BY T.TaskID, Ledger.StartDate, Ledger.EndDate, PostedFlg) AS X
                  GROUP BY TaskID, UnitID, StartDate, EndDate
                  HAVING SUM(ISNULL(PeriodQty, 0)) != 0 OR SUM(ISNULL(PeriodCost, 0)) != 0 OR SUM(ISNULL(PeriodBill, 0)) != 0
                                                  
            END -- If-Then (@strHasJTDUnt = 'Y')

 	      END -- If-Then (@@ROWCOUNT > 0)
    
    END -- If-Then (@strGetJTD = 'Y' AND @strRetrievalMode != '1')
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Check to see if there is any time-phased data.
  -- We don't want to do any work if the plan is empty.
  
  IF (@strRetrievalMode != '1')
    BEGIN
      IF (@strGetJTD = 'Y') SET @intJTDUntCount = CASE WHEN EXISTS (SELECT 'X' FROM @tabJTDUnt) THEN 1 ELSE 0 END
    END

  IF (@strRetrievalMode = '3')
    BEGIN
      IF (@strGetBaseline = 'Y') SET @intBaselineUntCount = CASE WHEN EXISTS (SELECT 'X' FROM RPBaselineUnit WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
    END
    
  IF (@strGetPlanned = 'Y') SET @intPlannedUntCount = CASE WHEN EXISTS (SELECT 'X' FROM RPPlannedUnit WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -->>> Planned Unit TPD.
  
  IF (@intPlannedUntCount > 0)
    BEGIN
    
      SELECT DISTINCT 1 AS Tag, NULL AS Parent,
        U.UnitID AS [PU!1!P],
        NULL AS [R!2!I],
        NULL AS [R!2!SD],
        NULL AS [R!2!ED],
        NULL AS [R!2!!Element]
        FROM RPUnit AS U -- Need INNER JOIN to weed out Units w/o TPD...
          INNER JOIN RPPlannedUnit AS TPD ON U.PlanID = TPD.PlanID AND U.TaskID = TPD.TaskID AND U.UnitID = TPD.UnitID
        WHERE U.PlanID = @strPlanID
      UNION ALL
      SELECT 2 AS Tag, 1 AS Parent,
        TPD.UnitID AS [PU!1!P],
        TimePhaseID AS [R!2!I],
        CONVERT(INT, TPD.StartDate) AS [R!2!SD],
        CONVERT(INT, TPD.EndDate) AS [R!2!ED],
        LTRIM(ISNULL(STR(TPD.PeriodQty, 19, @intQtyDecimals), '')) AS [R!2!!Element]
        FROM RPPlannedUnit AS TPD
        WHERE TPD.PlanID = @strPlanID AND TPD.UnitID IS NOT NULL
      UNION ALL -- ONLY Leaf Task Units TPD.
      SELECT DISTINCT 1 AS Tag, NULL AS Parent,
        T.TaskID AS [PU!1!P],
        NULL AS [R!2!I],
        NULL AS [R!2!SD],
        NULL AS [R!2!ED],
        NULL AS [R!2!!Element]
        FROM RPTask AS T -- Need INNER JOIN to weed out Tasks w/o TPD...
          INNER JOIN RPPlannedUnit AS TPD ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID AND TPD.UnitID IS NULL
        WHERE T.PlanID = @strPlanID AND T.ChildrenCount = 0 AND T.UntParentState = 'N'
      UNION ALL
      SELECT 2 AS Tag, 1 AS Parent,
        T.TaskID AS [PU!1!P], 
        TimePhaseID AS [R!2!I],
        CONVERT(INT, TPD.StartDate) AS [R!2!SD],
        CONVERT(INT, TPD.EndDate) AS [R!2!ED],
        LTRIM(ISNULL(STR(TPD.PeriodQty, 19, @intQtyDecimals), '')) AS [R!2!!Element]
        FROM  RPTask AS T
          INNER JOIN RPPlannedUnit AS TPD ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID AND TPD.UnitID IS NULL
        WHERE T.PlanID = @strPlanID AND T.ChildrenCount = 0 AND T.UntParentState = 'N'
        ORDER BY [PU!1!P], Tag, [R!2!SD]
        FOR XML EXPLICIT
    
    END --IF (@intPlannedUntCount > 0)
    
  -->>> Get Actual Units TPD (only when Retrieval Mode is not "Planned Only").
  
  IF (@strRetrievalMode != '1' AND @intJTDUntCount > 0)
    BEGIN
    
      SELECT DISTINCT 1 AS Tag, NULL AS Parent,
        ISNULL(TPD.UnitID, T.TaskID) AS [JU!1!P],
        NULL AS [R!2!SD],
        NULL AS [R!2!!Element]
        FROM RPTask AS T -- Need INNER JOIN to weed out Tasks w/o TPD...
          INNER JOIN @tabJTDUnt AS TPD ON T.TaskID = TPD.TaskID
        WHERE T.PlanID = @strPlanID
      UNION ALL
      SELECT 2 AS Tag, 1 AS Parent,
        ISNULL(TPD.UnitID, TPD.TaskID) AS [JU!1!P], 
        CONVERT(INT, TPD.StartDate) AS [R!2!SD], 
        LTRIM(ISNULL(STR(TPD.PeriodQty, 19, 2), '')) + '|' +
        LTRIM(ISNULL(STR(TPD.PeriodCost, 19, 2), '')) + '|' +  
        LTRIM(ISNULL(STR(TPD.PeriodBill, 19, 2), '')) AS [R!2!!Element]
        FROM  @tabJTDUnt AS TPD
        ORDER BY [JU!1!P], Tag, [R!2!SD]
        FOR XML EXPLICIT

    END --IF (@strRetrievalMode != '1' AND @intJTDUntCount > 0)
    
  -->>> Get Baseline Units TPD (only when needed).

  IF (@strRetrievalMode = '3' AND @intBaselineUntCount > 0)
    BEGIN

      SELECT DISTINCT 1 AS Tag, NULL AS Parent,
        ISNULL(TPD.UnitID, T.TaskID) AS [BU!1!P],
        NULL AS [R!2!SD],
        NULL AS [R!2!!Element]
        FROM RPTask AS T -- Need INNER JOIN to weed out Tasks w/o TPD...
          INNER JOIN RPBaselineUnit AS TPD ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID
        WHERE T.PlanID = @strPlanID
      UNION ALL
      SELECT 2 AS Tag, 1 AS Parent,
        ISNULL(TPD.UnitID, TPD.TaskID) AS [BU!1!P], 
        CONVERT(INT, TPD.StartDate) AS [R!2!SD], 
        LTRIM(ISNULL(STR(TPD.PeriodQty, 19, @intQtyDecimals), '')) + '|' +
        LTRIM(ISNULL(STR(TPD.PeriodCost, 19, @intAmtCostDecimals), '')) + '|' +  
        LTRIM(ISNULL(STR(TPD.PeriodBill, 19, @intAmtBillDecimals), '')) AS [R!2!!Element]
        FROM  RPBaselineUnit AS TPD
        WHERE TPD.PlanID = @strPlanID
        ORDER BY [BU!1!P], Tag, [R!2!SD]
        FOR XML EXPLICIT
        
    END --IF (@strRetrievalMode = '3' AND @intBaselineUntCount > 0)
        
  -- Calculate and return execution time.
  
  SET @totalTime = DATEDIFF(ms, @startTime, GetDate())

  SELECT 1 Tag, NULL Parent, @totalTime [time!1] FOR XML EXPLICIT

  SET NOCOUNT OFF

END -- pmGetUntTPD
GO
