SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Pack_Get_Fields]
	@visionRev		varchar(10) = 7,--remove
	@infoCenterArea	varchar(100),
	@dataType		varchar(100)
AS
BEGIN
	/*
	[DeltekStoredProc_CCG_EI_Pack_Get_Fields]
		@visionRev = '7',
		@infoCenterArea = 'Client',
		@dataType = 'client'
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL				varchar(max);
	DECLARE @safeSql			int;
	DECLARE @dataTypeCondition	varchar(100);

	-- Ensure that our inputs are safe (prevent SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @infoCenterArea);
	SET @safeSql = @safeSql | (dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @dataType) * 2);
	IF @safeSql < 3 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS varchar(4));
		RETURN;
	END;

	IF @dataType = 'employee' OR @dataType = 'client' SET @dataTypeCondition = '= ''' + @dataType + '''';
	ELSE SET @dataTypeCondition = 'LIKE ''%' + @dataType + '%''';

	SET @sSQL = '
		SELECT name FROM FW_CustomColumnsData
			WHERE InfoCenterArea = ''' + @infoCenterArea + ''' and GridID = ''X'' and DataType ' + @dataTypeCondition;


	EXEC(@sSQL);
END;
GO
