SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[spCCG_ProjectTeamSync]
AS
/*
Copyright 2020 Central Consulting Group.   All rights reserved.
07/14/2020	David Springer
			Sync Collections team with Project team
			Call this from Project scheduled workflow
*/
BEGIN
SET NOCOUNT ON

	Update c
	Set c.CustPrincipal = p.Principal
	From UDIC_Collection c, PR p
	Where c.CustProject = p.WBS1
	  and p.WBS2 = ' '
	  and c.CustPrincipal <> p.Principal

	Update c
	Set c.CustProjMgr = p.ProjMgr
	From UDIC_Collection c, PR p
	Where c.CustProject = p.WBS1
	  and p.WBS2 = ' '
	  and c.CustProjMgr <> p.ProjMgr

	Update c
	Set c.CustBiller= p.Biller
	From UDIC_Collection c, PR p
	Where c.CustProject = p.WBS1
	  and p.WBS2 = ' '
	  and c.CustBiller <> p.Biller

	Update c
	Set c.CustOperationsManager = p.CustOperationsManager
	From UDIC_Collection c, ProjectCustomTabFields p
	Where c.CustProject = p.WBS1
	  and p.WBS2 = ' '
	  and c.CustOperationsManager <> p.CustOperationsManager

	Update c
	Set c.CustManagementLeader = p.CustManagementLeader
	From UDIC_Collection c, ProjectCustomTabFields p
	Where c.CustProject = p.WBS1
	  and p.WBS2 = ' '
	  and c.CustManagementLeader <> p.CustManagementLeader

	Update c
	Set c.CustDirector = p.CustDirector
	From UDIC_Collection c, ProjectCustomTabFields p
	Where c.CustProject = p.WBS1
	  and p.WBS2 = ' '
	  and c.CustDirector <> p.CustDirector

END
GO
