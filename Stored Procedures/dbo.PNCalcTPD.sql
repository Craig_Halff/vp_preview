SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PNCalcTPD]
  @strPlanID VARCHAR(32),
  @strCalcLab VARCHAR(1) = 'Y',
  @strCalcExp VARCHAR(1) = 'Y',
  @strCalcCon VARCHAR(1) = 'Y'
AS

BEGIN -- Procedure PNCalcTPD

  SET NOCOUNT ON
  
  DECLARE @dcLabRevMult decimal(19,4)
  DECLARE @dcPlannedMultiplier decimal(19,4)
  DECLARE @dcPlannedRatio decimal(19,4)
  DECLARE @dcLabBillMult decimal(19,4)

  DECLARE @dcPlannedLaborHrs decimal(19,4)
  DECLARE @dcPlannedLabCost decimal(19,4)
  DECLARE @dcPlannedLabBill decimal(19,4)
  DECLARE @dcPlannedExpCost decimal(19,4)
  DECLARE @dcPlannedExpBill decimal(19,4)
  DECLARE @dcPlannedConCost decimal(19,4)
  DECLARE @dcPlannedConBill decimal(19,4)
  DECLARE @dcLabRevenue decimal(19,4)
  DECLARE @dcExpRevenue decimal(19,4)
  DECLARE @dcConRevenue decimal(19,4)
  DECLARE @dcPlannedDirExpCost decimal(19,4)
  DECLARE @dcPlannedDirExpBill decimal(19,4)
  DECLARE @dcPlannedDirConCost decimal(19,4)
  DECLARE @dcPlannedDirConBill decimal(19,4)
  
  DECLARE @dcCompCost decimal(19,4)
  DECLARE @dcCompBill decimal(19,4)
  DECLARE @dcConsCost decimal(19,4)
  DECLARE @dcConsBill decimal(19,4)
  DECLARE @dcReimCost decimal(19,4)
  DECLARE @dcReimBill decimal(19,4)

  DECLARE @strLabRevBasis char
  DECLARE @strCalcExpBill char
  DECLARE @strCalcConBill char
  DECLARE @strReimbMethod char
  
  DECLARE @intHrDecimals int
  DECLARE @intQtyDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intLabRevDecimals int
  DECLARE @intECURevDecimals int

  DECLARE @intPlannedLabCount int
  DECLARE @intPlannedExpCount int
  DECLARE @intPlannedConCount int

  DECLARE @intLabMultType smallint
     
  SET @dcPlannedMultiplier = 0
  SET @dcPlannedRatio = 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Let's clean up all of  records which have "zero" hour/amount.
  -- These should be no time-phased data in the DB with "zero" hour/amount

  DELETE PNPlannedLabor WHERE PeriodHrs = 0 AND PlanID = @strPlanID
  DELETE PNPlannedExpenses WHERE PeriodCost = 0 AND PeriodBill = 0 AND PlanID = @strPlanID
  DELETE PNPlannedConsultant WHERE PeriodCost = 0 AND PeriodBill = 0 AND PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  
  -- Get decimal settings.

  SELECT @intHrDecimals = 2,
         @intAmtCostDecimals = 4,
         @intAmtBillDecimals = 4,
         @intLabRevDecimals = 4, 
         @intECURevDecimals = 4

/*
  
  SELECT @intHrDecimals = HrDecimals,
         @intQtyDecimals = QtyDecimals,
         @intAmtCostDecimals = AmtCostDecimals,
         @intAmtBillDecimals = AmtBillDecimals,
         @intLabRevDecimals = LabRevDecimals, 
         @intECURevDecimals = ECURevDecimals
    FROM dbo.RP$tabDecimals(@strPlanID)

*/

  -- Check to see if there is any time-phased data
  
  IF (@strCalcLab = 'Y') SET @intPlannedLabCount = CASE WHEN EXISTS (SELECT 'X' FROM PNPlannedLabor WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
  IF (@strCalcExp = 'Y') SET @intPlannedExpCount = CASE WHEN EXISTS (SELECT 'X' FROM PNPlannedExpenses WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
  IF (@strCalcCon = 'Y') SET @intPlannedConCount = CASE WHEN EXISTS (SELECT 'X' FROM PNPlannedConsultant WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END

  -- Get parameters from Plan to be used throughout this procedure.
  -- Also, save off Planned numbers to be used in calculation later
  -- in case a particular data type (e.g. Labor/Expense/Consultant/CCR)
  -- does not have to be calculated.
  
  SELECT 
    @intLabMultType = LabMultType,
    @dcLabRevMult = Multiplier,
    @strLabRevBasis = CASE WHEN LabMultType < 2 THEN 'C' ELSE 'B' END,
    @strCalcExpBill = CalcExpBillAmtFlg, 
    @strCalcConBill = CalcConBillAmtFlg,
    @strReimbMethod = ReimbMethod,
    @dcLabBillMult = LabBillMultiplier,
    @dcPlannedLaborHrs = PlannedLaborHrs, 
    @dcPlannedLabCost = PlannedLabCost,
    @dcPlannedLabBill = PlannedLabBill,
    @dcLabRevenue = LabRevenue,
    @dcPlannedExpCost = PlannedExpCost,
    @dcPlannedExpBill = PlannedExpBill,
    @dcPlannedDirExpCost = PlannedDirExpCost,
    @dcPlannedDirExpBill = PlannedDirExpBill,
    @dcExpRevenue = ExpRevenue,
    @dcPlannedConCost = PlannedConCost,
    @dcPlannedConBill = PlannedConBill,
    @dcPlannedDirConCost = PlannedDirConCost,
    @dcPlannedDirConBill = PlannedDirConBill,
    @dcConRevenue = ConRevenue,
    @dcCompCost = PR.Fee,
    @dcCompBill = PR.FeeBillingCurrency,
    @dcConsCost = PR.ConsultFee,
    @dcConsBill = PR.ConsultFeeBillingCurrency,
    @dcReimCost = PR.ReimbAllow,
    @dcReimBill = PR.ReimbAllowBillingCurrency
    FROM PNPlan AS PL 
      LEFT JOIN PR ON PL.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
    WHERE PL.PlanID = @strPlanID

  -- >>> rpDeleteSummaryTPD should have been called before getting to this point.
           
  -- >>> At this point, the remaining time-phased data belong to either childless-Task or 
  -- >>> Assignment/Expense/Consultant.
  
  -- Update PNPlannedLabor.

  -- Need to defer the updating of Planned Labor Revenue just in case the Labor Revenue
  -- Method uses Planned Multiplier/Ratio. In this case, Planned Multiplier/Ratio can only
  -- be calculated after all of the Planned Labor/Expense/Cost numbers have been updated.

  IF (@strCalcLab = 'Y')
    BEGIN
    
      IF (@intPlannedLabCount > 0)
        BEGIN
        
          UPDATE PNPlannedLabor
            SET PeriodCost = ROUND((PeriodHrs * CostRate), @intAmtCostDecimals),
                PeriodBill = ROUND((PeriodHrs * BillingRate * @dcLabBillMult), @intAmtBillDecimals)     
            FROM PNPlannedLabor AS TPD
            WHERE TPD.PlanID = @strPlanID
              AND (PeriodCost <> ROUND((PeriodHrs * CostRate), @intAmtCostDecimals) OR
                  PeriodBill <> ROUND((PeriodHrs * BillingRate * @dcLabBillMult), @intAmtBillDecimals))
                  
          SELECT @dcPlannedLaborHrs = ROUND(ISNULL(SUM(PeriodHrs), 0), @intHrDecimals),
                 @dcPlannedLabCost = ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals),
                 @dcPlannedLabBill = ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals)
            FROM PNPlannedLabor WHERE PlanID = @strPlanID
            
        END
        
      ELSE
        BEGIN 
          SET @dcPlannedLaborHrs = 0
          SET @dcPlannedLabCost = 0
          SET @dcPlannedLabBill = 0
        END
        
    END -- IF (@strCalcLab = 'Y')

  -- Update PNPlannedExpenses
  -- Note: for V3, Direct Bill Expense is not always zero.
  --       Each Expense row already had the appropriate Multiplier calculated upon returned from Lookup.
  -- All TPD Expenses under Task rows (that do not have any children) are considered "Direct" Expenses.
    
  IF (@strCalcExp = 'Y')
    BEGIN

      IF (@intPlannedExpCount > 0)
        BEGIN
            
          UPDATE PNPlannedExpenses
            SET PeriodRev = 
              CASE 
                WHEN E.DirectAcctFlg = 'N'
                THEN (CASE WHEN @strReimbMethod = 'C' THEN PeriodCost ELSE PeriodBill END)
                ELSE 0.0 
              END
            FROM PNPlannedExpenses AS TPD 
              LEFT JOIN PNExpense AS E ON TPD.ExpenseID = E.ExpenseID
              LEFT JOIN PNTask AS T ON TPD.TaskID = T.TaskID
            WHERE TPD.PlanID = @strPlanID
              AND PeriodRev <> 
                CASE 
                  WHEN E.DirectAcctFlg = 'N'
                  THEN (CASE WHEN @strReimbMethod = 'C' THEN PeriodCost ELSE PeriodBill END)
                  ELSE 0.0 
                END
                          
          SELECT @dcPlannedExpCost = ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals),
                 @dcPlannedExpBill = ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals),
                 @dcExpRevenue = ROUND(ISNULL(SUM(PeriodRev), 0), CASE WHEN @strReimbMethod = 'C' 
                                                                         THEN @intAmtCostDecimals
                                                                         ELSE @intAmtBillDecimals END),
                 @dcPlannedDirExpCost = 
                   ROUND(ISNULL(SUM(CASE WHEN (E.DirectAcctFlg = 'Y' OR E.DirectAcctFlg IS NULL)
                                         THEN PeriodCost ELSE 0.0 END), 0), @intAmtCostDecimals),
                 @dcPlannedDirExpBill = 
                   ROUND(ISNULL(SUM(CASE WHEN (E.DirectAcctFlg = 'Y' OR E.DirectAcctFlg IS NULL)
                                         THEN PeriodBill ELSE 0.0 END), 0), @intAmtBillDecimals)
            FROM PNPlannedExpenses AS TPD LEFT JOIN PNExpense AS E 
              ON TPD.PlanID = E.PlanID AND TPD.TaskID = E.TaskID AND TPD.ExpenseID = E.ExpenseID
            WHERE TPD.PlanID = @strPlanID 

        END
        
      ELSE
        BEGIN
          SET @dcPlannedExpCost = 0
          SET @dcPlannedExpBill = 0
          SET @dcExpRevenue = 0
          SET @dcPlannedDirExpCost = 0
          SET @dcPlannedDirExpBill = 0
        END
        
    END -- If-Then
    
  -- Update PNPlannedConsultant
  -- Note: for V3, Direct Bill Consultant is not always zero.
  --       Each Expense row already had the appropriate Multiplier calculated upon returned from Lookup.
  -- All TPD Consultants under Task rows (that do not have any children) are considered "Direct" Consultants.
  
  IF (@strCalcCon = 'Y')
    BEGIN
    
      IF (@intPlannedConCount > 0)
        BEGIN
            
          UPDATE PNPlannedConsultant
            SET PeriodRev = 
              CASE 
                WHEN C.DirectAcctFlg = 'N'
                THEN (CASE WHEN @strReimbMethod = 'C' THEN PeriodCost ELSE PeriodBill END)
                ELSE 0.0 
              END
            FROM PNPlannedConsultant AS TPD 
              LEFT JOIN PNConsultant AS C ON TPD.ConsultantID = C.ConsultantID
              LEFT JOIN PNTask AS T ON TPD.TaskID = T.TaskID
            WHERE TPD.PlanID = @strPlanID
              AND PeriodRev <> 
                CASE 
                  WHEN C.DirectAcctFlg = 'N'
                  THEN (CASE WHEN @strReimbMethod = 'C' THEN PeriodCost ELSE PeriodBill END)
                  ELSE 0.0 
                END
                          
          SELECT @dcPlannedConCost = ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals),
                 @dcPlannedConBill = ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals),
                 @dcConRevenue = ROUND(ISNULL(SUM(PeriodRev), 0), CASE WHEN @strReimbMethod = 'C' 
                                                                         THEN @intAmtCostDecimals
                                                                         ELSE @intAmtBillDecimals END),
                 @dcPlannedDirConCost = 
                   ROUND(ISNULL(SUM(CASE WHEN (C.DirectAcctFlg = 'Y' OR C.DirectAcctFlg IS NULL)
                                         THEN PeriodCost ELSE 0.0 END), 0), @intAmtCostDecimals),
                 @dcPlannedDirConBill = 
                   ROUND(ISNULL(SUM(CASE WHEN (C.DirectAcctFlg = 'Y' OR C.DirectAcctFlg IS NULL)
                                         THEN PeriodBill ELSE 0.0 END), 0), @intAmtBillDecimals)
            FROM PNPlannedConsultant AS TPD LEFT JOIN PNConsultant AS C 
              ON TPD.PlanID = C.PlanID AND TPD.TaskID = C.TaskID AND TPD.ConsultantID = C.ConsultantID
            WHERE TPD.PlanID = @strPlanID 

        END
        
      ELSE
        BEGIN
          SET @dcPlannedConCost = 0
          SET @dcPlannedConBill = 0
          SET @dcConRevenue = 0
          SET @dcPlannedDirConCost = 0
          SET @dcPlannedDirConBill = 0
        END
        
    END -- If-Then

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- >>> Now we can calculate Planned Multiplier and Planned Ratio 
  -- >>> before calculating PeriodRev for the Labor time-phased data.
  
  IF (@dcPlannedLabCost = 0)
    SET @dcPlannedMultiplier = 0.00
  ELSE
    SET @dcPlannedMultiplier = ROUND((((@dcCompCost + @dcConsCost + @dcReimCost) -
						(@dcPlannedExpCost + @dcPlannedConCost)) / @dcPlannedLabCost), 2)

  IF (@dcPlannedLabBill = 0)
    SET @dcPlannedRatio = 0.00
  ELSE
    SET @dcPlannedRatio = ROUND((((@dcCompBill + @dcConsBill + @dcReimBill) -
						(@dcPlannedExpBill + @dcPlannedConBill)) / @dcPlannedLabBill), 2)
						
  SET @dcLabRevMult = CASE WHEN @intLabMultType = 1 THEN @dcPlannedMultiplier
                           WHEN @intLabMultType = 3 THEN @dcPlannedRatio
                           ELSE @dcLabRevMult END				

  -- Update Labor Revenue time-phased data.
  -- Since @dcLabRevMult could be changed because Planned Multiplier or Planned Ratio was changed,
  -- we will need to recalculate Labor Revenue regardless of whether @strCalcLab is 'Y' or not.

  IF (@strCalcLab = 'Y')
    BEGIN
    
      IF (@intPlannedLabCount > 0)
        BEGIN
        
          UPDATE PNPlannedLabor
            SET PeriodRev = CASE WHEN @strLabRevBasis = 'C' THEN ROUND((PeriodCost * @dcLabRevMult), @intAmtCostDecimals)
                                 ELSE ROUND((PeriodBill * @dcLabRevMult), @intAmtBillDecimals) END
            FROM PNPlannedLabor 
            WHERE PlanID = @strPlanID
              AND PeriodRev <> CASE WHEN @strLabRevBasis = 'C' THEN ROUND((PeriodCost * @dcLabRevMult), @intAmtCostDecimals)
                                    ELSE ROUND((PeriodBill * @dcLabRevMult), @intAmtBillDecimals) END

          SELECT @dcLabRevenue = ROUND(SUM(PeriodRev), CASE WHEN @strLabRevBasis = 'C' 
                                                              THEN @intAmtCostDecimals
                                                              ELSE @intAmtBillDecimals END)
            FROM PNPlannedLabor WHERE PlanID = @strPlanID
      
        END
      ELSE SET @dcLabRevenue = 0
        
    END -- IF (@strCalcLab = 'Y')

  -- Update Summary data of the Plan
  -- I do this update here because I already have the data so might as well use them.
  -- Also, this will allow me to cross check the calculations later.
  
  UPDATE PNPlan
    SET Multiplier = ISNULL(@dcLabRevMult, 0),
        ProjectedMultiplier = ISNULL(@dcPlannedMultiplier, 0), 
        ProjectedRatio = ISNULL(@dcPlannedRatio, 0), 
        PlannedLaborHrs = ISNULL(@dcPlannedLaborHrs, 0), 
        PlannedLabCost = ISNULL(@dcPlannedLabCost, 0), 
        PlannedLabBill = ISNULL(@dcPlannedLabBill, 0), 
        PlannedExpCost = ISNULL(@dcPlannedExpCost, 0), 
        PlannedExpBill = ISNULL(@dcPlannedExpBill, 0), 
        PlannedConCost = ISNULL(@dcPlannedConCost, 0), 
        PlannedConBill = ISNULL(@dcPlannedConBill, 0), 
        LabRevenue = ISNULL(@dcLabRevenue, 0), 
        ExpRevenue = ISNULL(@dcExpRevenue, 0), 
        ConRevenue = ISNULL(@dcConRevenue, 0), 
        PlannedDirExpCost = ISNULL(@dcPlannedDirExpCost, 0), 
        PlannedDirExpBill = ISNULL(@dcPlannedDirExpBill, 0), 
        PlannedDirConCost = ISNULL(@dcPlannedDirConCost, 0), 
        PlannedDirConBill = ISNULL(@dcPlannedDirConBill, 0)
    FROM PNPlan
    WHERE PlanID = @strPlanID
        
  -- >>> I need to calculate the Direct amounts here
  -- >>> for the bottom-most rows so that they can be used later in summing up
  -- >>> for the summary rows.
  
  -- Update bottom-most Tasks.
  
  UPDATE PNTask
    SET PlannedLaborHrs = TPD.PlannedLaborHrs,
        PlannedLabCost = TPD.PlannedLabCost,
        PlannedLabBill = TPD.PlannedLabBill,
        LabRevenue = TPD.LabRevenue,
        PlannedExpCost = TPD.PlannedExpCost,
        PlannedExpBill = TPD.PlannedExpBill,
        ExpRevenue = TPD.ExpRevenue,
        PlannedConCost = TPD.PlannedConCost,
        PlannedConBill = TPD.PlannedConBill,
        ConRevenue = TPD.ConRevenue,
        PlannedDirExpCost = TPD.PlannedDirExpCost,
        PlannedDirExpBill = TPD.PlannedDirExpBill,
        PlannedDirConCost = TPD.PlannedDirConCost,
        PlannedDirConBill = TPD.PlannedDirConBill
    FROM PNTask AS T INNER JOIN
      (SELECT XT.PlanID AS PlanID, XT.TaskID AS TaskID,
              SUM(ISNULL(ACount, 0)) AS ACount,
              SUM(ISNULL(ECount, 0)) AS ECount,
              SUM(ISNULL(CCount, 0)) AS CCount,
              SUM(ISNULL(UCount, 0)) AS UCount,
              ROUND(SUM(ISNULL(LabPeriodHrs, 0)), @intHrDecimals) AS PlannedLaborHrs,
              ROUND(SUM(ISNULL(LabPeriodCost, 0)), @intAmtCostDecimals) AS PlannedLabCost,
              ROUND(SUM(ISNULL(LabPeriodBill, 0)), @intAmtBillDecimals)AS PlannedLabBill,
              ROUND(SUM(ISNULL(LabPeriodRev, 0)), CASE WHEN P.LabMultType < 2 
                                                       THEN @intAmtCostDecimals 
                                                       ELSE @intAmtBillDecimals END) AS LabRevenue,
              ROUND(SUM(ISNULL(ExpPeriodCost, 0)), @intAmtCostDecimals) AS PlannedExpCost,
              ROUND(SUM(ISNULL(ExpPeriodBill, 0)), @intAmtBillDecimals) AS PlannedExpBill,
              ROUND(SUM(ISNULL(ExpPeriodRev, 0)), CASE WHEN P.ReimbMethod = 'C' 
                                                       THEN @intAmtCostDecimals
                                                       ELSE @intAmtBillDecimals END) AS ExpRevenue,
              ROUND(SUM(ISNULL(ConPeriodCost, 0)), @intAmtCostDecimals) AS PlannedConCost,
              ROUND(SUM(ISNULL(ConPeriodBill, 0)), @intAmtBillDecimals) AS PlannedConBill,
              ROUND(SUM(ISNULL(ConPeriodRev, 0)), CASE WHEN P.ReimbMethod = 'C' 
                                                       THEN @intAmtCostDecimals
                                                       ELSE @intAmtBillDecimals END) AS ConRevenue,
              ROUND(SUM(ISNULL(ExpDirCost, 0)), @intAmtCostDecimals) AS PlannedDirExpCost,
              ROUND(SUM(ISNULL(ExpDirBill, 0)), @intAmtBillDecimals) AS PlannedDirExpBill,
              ROUND(SUM(ISNULL(ConDirCost, 0)), @intAmtCostDecimals) AS PlannedDirConCost,
              ROUND(SUM(ISNULL(ConDirBill, 0)), @intAmtBillDecimals) AS PlannedDirConBill
         FROM PNTask AS XT LEFT JOIN PNPlan AS P ON P.PlanID = XT.PlanID LEFT JOIN
           (SELECT LT.PlanID AS PlanID, LT.TaskID AS TaskID, 
                   COUNT(XA.AssignmentID) AS ACount, 0 AS ECount, 0 AS CCount, 0 AS UCount,
                   SUM(ISNULL(PeriodHrs, 0)) AS LabPeriodHrs, 
                   SUM(ISNULL(PeriodCost, 0)) AS LabPeriodCost, 
                   SUM(ISNULL(PeriodBill, 0)) AS LabPeriodBill, 
                   SUM(ISNULL(PeriodRev, 0)) AS LabPeriodRev,
                   0 AS ExpPeriodCost, 0 AS ExpPeriodBill, 0 AS ExpPeriodRev,
                   0 AS ConPeriodCost, 0 AS ConPeriodBill, 0 AS ConPeriodRev,
                   0 AS ExpDirCost, 0 AS ExpDirBill, 0 AS ConDirCost, 0 AS ConDirBill
              FROM PNTask AS LT 
                LEFT JOIN PNAssignment AS XA ON LT.PlanID = XA.PlanID AND LT.TaskID = XA.TaskID
                LEFT JOIN PNPlannedLabor AS LTPD ON LT.PlanID = LTPD.PlanID AND LT.TaskID = LTPD.TaskID AND ISNULL(LTPD.AssignmentID, 1) = ISNULL(XA.AssignmentID, 1)
              WHERE LT.PlanID = @strPlanID AND LT.ChildrenCount = 0
              GROUP BY  LT.PlanID, LT.TaskID, XA.AssignmentID
            UNION ALL --> PNPlannedExpenses
            SELECT ET.PlanID AS PlanID, ET.TaskID AS TaskID, 
                   0 AS ACount, COUNT(XE.ExpenseID) AS ECount, 0 AS CCount, 0 AS UCount,
                   0 AS LabPeriodHrs, 0 AS LabPeriodCost, 0 AS LabPeriodBill, 0 AS LabPeriodRev,
                   SUM(ISNULL(PeriodCost, 0)) AS ExpPeriodCost, 
                   SUM(ISNULL(PeriodBill, 0)) AS ExpPeriodBill, 
                   SUM(ISNULL(PeriodRev, 0)) AS ExpPeriodRev,
                   0 AS ConPeriodCost, 0 AS ConPeriodBill, 0 AS ConPeriodRev,
                   ROUND(ISNULL(SUM(CASE WHEN (XE.DirectAcctFlg = 'Y' OR XE.DirectAcctFlg IS NULL)
                                         THEN PeriodCost ELSE 0.0 END), 0), @intAmtCostDecimals) AS ExpDirCost,
                   ROUND(ISNULL(SUM(CASE WHEN (XE.DirectAcctFlg = 'Y' OR XE.DirectAcctFlg IS NULL)
                                         THEN PeriodBill ELSE 0.0 END), 0), @intAmtBillDecimals) AS ExpDirBill,
                   0 AS ConDirCost, 0 AS ConDirBill
              FROM PNTask AS ET 
                LEFT JOIN PNExpense AS XE ON ET.PlanID = XE.PlanID AND ET.TaskID = XE.TaskID
                LEFT JOIN PNPlannedExpenses AS ETPD ON ET.PlanID = ETPD.PlanID AND ET.TaskID = ETPD.TaskID AND ISNULL(ETPD.ExpenseID, 1) = ISNULL(XE.ExpenseID, 1)
              WHERE ET.PlanID = @strPlanID AND ET.ChildrenCount = 0
              GROUP BY ET.PlanID, ET.TaskID, XE.ExpenseID
            UNION ALL --> PNPlannedConsultant
            SELECT CT.PlanID AS PlanID, CT.TaskID AS TaskID, 
                   0 AS ACount, 0 AS ECount, COUNT(XC.ConsultantID) AS CCount, 0 AS UCount,
                   0 AS LabPeriodHrs, 0 AS LabPeriodCost, 0 AS LabPeriodBill, 0 AS LabPeriodRev,
                   0 AS ExpPeriodCost, 0 AS ExpPeriodBill, 0 AS ExpPeriodRev,
                   SUM(ISNULL(PeriodCost, 0)) AS ConPeriodCost, 
                   SUM(ISNULL(PeriodBill, 0)) AS ConPeriodBill, 
                   SUM(ISNULL(PeriodRev, 0)) AS ConPeriodRev,
                  0 AS ExpDirCost, 0 AS ExpDirBill,
                   ROUND(ISNULL(SUM(CASE WHEN (XC.DirectAcctFlg = 'Y' OR XC.DirectAcctFlg IS NULL)
                                         THEN PeriodCost ELSE 0.0 END), 0), @intAmtCostDecimals) AS ConDirCost,
                   ROUND(ISNULL(SUM(CASE WHEN (XC.DirectAcctFlg = 'Y' OR XC.DirectAcctFlg IS NULL)
                                         THEN PeriodBill ELSE 0.0 END), 0), @intAmtBillDecimals) AS ConDirBill
               FROM PNTask AS CT 
                 LEFT JOIN PNConsultant AS XC ON CT.PlanID = XC.PlanID AND CT.TaskID = XC.TaskID
                 LEFT JOIN PNPlannedConsultant AS CTPD ON CT.PlanID = CTPD.PlanID AND CT.TaskID = CTPD.TaskID AND ISNULL(CTPD.ConsultantID, 1) = ISNULL(XC.ConsultantID, 1)
               WHERE CT.PlanID = @strPlanID AND CT.ChildrenCount = 0
               GROUP BY CT.PlanID, CT.TaskID, XC.ConsultantID
           ) AS XTPD
           ON XT.PlanID = XTPD.PlanID AND XT.TaskID = XTPD.TaskID
           WHERE XT.PlanID = @strPlanID AND XT.ChildrenCount = 0
         GROUP BY XT.PlanID, XT.TaskID, P.LabMultType, P.ReimbMethod) AS TPD 
      ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID
      LEFT JOIN PNPlan AS XP ON T.PlanID = XP.PlanID
 
  IF (@strCalcLab = 'Y')
    BEGIN

      -- Update Assignment.
      
      UPDATE PNAssignment
        SET PlannedLaborHrs = TPD.PlannedLaborHrs, 
            PlannedLabCost = TPD.PlannedLabCost,
            PlannedLabBill = TPD.PlannedLabBill, 
            LabRevenue = TPD.LabRevenue,
            StartDate = T.StartDate,
            EndDate = T.EndDate
        FROM PNAssignment AS A INNER JOIN
          (SELECT XA.PlanID, XA.TaskID, XA.AssignmentID, 
                  ROUND(SUM(ISNULL(PeriodHrs, 0)), @intHrDecimals) AS PlannedLaborHrs,
                  ROUND(SUM(ISNULL(PeriodCost, 0)), @intAmtCostDecimals) AS PlannedLabCost,
                  ROUND(SUM(ISNULL(PeriodBill, 0)), @intAmtBillDecimals) AS PlannedLabBill,
                  ROUND(SUM(ISNULL(PeriodRev, 0)), CASE WHEN P.LabMultType < 2 
                                                        THEN @intAmtCostDecimals 
                                                        ELSE @intAmtBillDecimals END) AS LabRevenue
            FROM PNAssignment AS XA LEFT JOIN PNPlannedLabor AS TPD
              ON XA.PlanID = TPD.PlanID AND XA.TaskID = TPD.TaskID AND XA.AssignmentID = TPD.AssignmentID
              LEFT JOIN PNPlan AS P ON P.PlanID = XA.PlanID
            WHERE XA.PlanID = @strPlanID
            GROUP BY XA.AssignmentID, XA.PlanID, XA.TaskID, P.LabMultType) AS TPD
              ON A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID
          INNER JOIN PNTask AS T ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
        WHERE
          A.PlannedLaborHrs <> TPD.PlannedLaborHrs OR 
          A.PlannedLabCost <> TPD.PlannedLabCost OR
          A.PlannedLabBill <> TPD.PlannedLabBill OR 
          A.LabRevenue <> TPD.LabRevenue OR
          A.StartDate <> T.StartDate OR
          A.EndDate <> T.EndDate

    END

  IF (@strCalcExp = 'Y')
    BEGIN

      -- Update Expense.
      
      UPDATE PNExpense
        SET PlannedExpCost = TPD.PlannedExpCost, 
            PlannedExpBill = TPD.PlannedExpBill,
            ExpRevenue = TPD.ExpRevenue,
            PlannedDirExpCost = TPD.PlannedDirExpCost,
            PlannedDirExpBill = TPD.PlannedDirExpBill,
            StartDate = T.StartDate,
            EndDate = T.EndDate
        FROM PNExpense AS E INNER JOIN
          (SELECT XE.PlanID, XE.TaskID, XE.ExpenseID,
                  ROUND(SUM(ISNULL(PeriodCost, 0)), @intAmtCostDecimals) AS PlannedExpCost,
                  ROUND(SUM(ISNULL(PeriodBill, 0)), @intAmtBillDecimals)AS PlannedExpBill,
                  ROUND(SUM(ISNULL(PeriodRev, 0)), CASE WHEN P.ReimbMethod = 'C' 
                                                        THEN @intAmtCostDecimals
                                                        ELSE @intAmtBillDecimals END) AS ExpRevenue,
                  ROUND(ISNULL(SUM(CASE WHEN (XE.DirectAcctFlg = 'Y' OR XE.DirectAcctFlg IS NULL)
                                        THEN PeriodCost ELSE 0.0 END), 0), @intAmtCostDecimals) AS PlannedDirExpCost,
                  ROUND(ISNULL(SUM(CASE WHEN (XE.DirectAcctFlg = 'Y' OR XE.DirectAcctFlg IS NULL)
                                        THEN PeriodBill ELSE 0.0 END), 0), @intAmtBillDecimals) AS PlannedDirExpBill
            FROM PNExpense AS XE LEFT JOIN PNPlannedExpenses AS XTPD
              ON XE.PlanID = XTPD.PlanID AND XE.TaskID = XTPD.TaskID AND XE.ExpenseID = XTPD.ExpenseID
              LEFT JOIN PNPlan AS P ON P.PlanID = XE.PlanID
            WHERE XE.PlanID = @strPlanID
            GROUP BY XE.ExpenseID, XE.PlanID, XE.TaskID, XE.DirectAcctFlg,
              XE.PctCompleteExpCost, XE.PctCompleteExpBill, P.ReimbMethod) AS TPD
            ON E.PlanID = TPD.PlanID AND E.TaskID = TPD.TaskID AND E.ExpenseID = TPD.ExpenseID
          INNER JOIN PNTask AS T ON E.PlanID = T.PlanID AND E.TaskID = T.TaskID
        WHERE
          E.PlannedExpCost <> TPD.PlannedExpCost OR 
          E.PlannedExpBill <> TPD.PlannedExpBill OR
          E.ExpRevenue <> TPD.ExpRevenue OR
          E.PlannedDirExpCost <> TPD.PlannedDirExpCost OR
          E.PlannedDirExpBill <> TPD.PlannedDirExpBill OR
          E.StartDate <> T.StartDate OR
          E.EndDate <> T.EndDate
        
    END
    
  IF (@strCalcCon = 'Y')
    BEGIN

      -- Update Consultant.
      
      UPDATE PNConsultant
        SET PlannedConCost = TPD.PlannedConCost, 
            PlannedConBill = TPD.PlannedConBill,
            ConRevenue = TPD.ConRevenue,
            PlannedDirConCost = TPD.PlannedDirConCost,
            PlannedDirConBill = TPD.PlannedDirConBill,
            StartDate = T.StartDate,
            EndDate = T.EndDate
        FROM PNConsultant AS C INNER JOIN
          (SELECT XC.PlanID, XC.TaskID, XC.ConsultantID,
                  ROUND(SUM(ISNULL(PeriodCost, 0)), @intAmtCostDecimals) AS PlannedConCost,
                  ROUND(SUM(ISNULL(PeriodBill, 0)), @intAmtBillDecimals)AS PlannedConBill,
                  ROUND(SUM(ISNULL(PeriodRev, 0)), CASE WHEN P.ReimbMethod = 'C' 
                                                        THEN @intAmtCostDecimals
                                                        ELSE @intAmtBillDecimals END) AS ConRevenue,
                  ROUND(ISNULL(SUM(CASE WHEN (XC.DirectAcctFlg = 'Y' OR XC.DirectAcctFlg IS NULL)
                                        THEN PeriodCost ELSE 0.0 END), 0), @intAmtCostDecimals) AS PlannedDirConCost,
                  ROUND(ISNULL(SUM(CASE WHEN (XC.DirectAcctFlg = 'Y' OR XC.DirectAcctFlg IS NULL)
                                        THEN PeriodBill ELSE 0.0 END), 0), @intAmtBillDecimals) AS PlannedDirConBill
            FROM PNConsultant AS XC LEFT JOIN PNPlannedConsultant AS XTPD
              ON XC.PlanID = XTPD.PlanID AND XC.TaskID = XTPD.TaskID AND XC.ConsultantID = XTPD.ConsultantID
              LEFT JOIN PNPlan AS P ON P.PlanID = XC.PlanID
            WHERE XC.PlanID = @strPlanID
            GROUP BY XC.ConsultantID, XC.PlanID, XC.TaskID, XC.DirectAcctFlg,
              XC.PctCompleteConCost, XC.PctCompleteConBill, P.ReimbMethod) AS TPD
        ON C.PlanID = TPD.PlanID AND C.TaskID = TPD.TaskID AND C.ConsultantID = TPD.ConsultantID
          INNER JOIN PNTask AS T ON C.PlanID = T.PlanID AND C.TaskID = T.TaskID
        WHERE 
          C.PlannedConCost <> TPD.PlannedConCost OR 
          C.PlannedConBill <> TPD.PlannedConBill OR
          C.ConRevenue <> TPD.ConRevenue OR
          C.PlannedDirConCost <> TPD.PlannedDirConCost OR
          C.PlannedDirConBill <> TPD.PlannedDirConBill OR
          C.StartDate <> T.StartDate OR
          C.EndDate <> T.EndDate

    END
        
  SET NOCOUNT OFF

END -- PNCalcTPD
GO
