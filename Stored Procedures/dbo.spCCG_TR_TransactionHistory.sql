SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_TR_TransactionHistory](@TableName varchar(32), @Period int, @PostSeq int, @PKey varchar(32))
AS BEGIN

	set nocount on
	select DateAdd(minute, DateDiff(minute, getutcdate(), getdate()), ActionDate) as ActionDate, EM.LastName + ', ' + IsNull(EM.FirstName,'') as EmpName, 'Modified' as ActionTaken, 
		Case When ActionTaken='T' Then 'Transfered' When ActionTaken='B' Then 'Bill Status Change' 
		When ActionTaken='M' and ActionDetail='TransDate' Then 'Trans Date Change'
		When ActionTaken='M' and ActionDetail='Comment'   Then 'Comment Change'
		When ActionTaken='M' and ActionDetail='Desc1'     Then 'Desc 1 Change'
		When ActionTaken='M' and ActionDetail='Desc2'     Then 'Desc 2 Change'
		When ActionTaken='M' and ActionDetail='Cost'      Then 'Partial Cost'
		When ActionTaken='M' and ActionDetail='RegHrs'    Then 'Partial Reg Hours'
		When ActionTaken='M' and ActionDetail='OvtHrs'    Then 'Partial Ovt Hours'
		When ActionTaken='M' and ActionDetail='SpecialOvtHrs' Then 'Partial Ovt2 Hours'
		When ActionTaken='I' and ActionDetail='NonBillable'   Then 'Include Non-Billable'
		When ActionTaken='I' and ActionDetail='Future'        Then 'Include Future'
		When ActionTaken='X' Then 'Cleared Unapproved' End as ActionType,
		Case When ActionTaken='Tfails' Then 'From ' + dbo.fnCCG_TR_GetWBS(h.OriginalTable,h.OriginalPeriod,h.OriginalPostSeq,h.OriginalPKey) 
			Else 'From "' + OldValue + '" to "' + NewValue + '"'
		End as Details, ModificationComment, ApprovalComment
	from CCG_TR_History h
	left join EM on EM.Employee=h.ActionTakenBy
	where OriginalTable=@TableName and OriginalPeriod=@Period and OriginalPostSeq=@PostSeq and OriginalPKey=@PKey and h.ActionStatus='S'
	UNION ALL
	select DateAdd(minute, DateDiff(minute, getutcdate(), getdate()), SubmittedDate) as ActionDate, EM.LastName + ', ' + IsNull(EM.FirstName,'') as EmpName, 'Submitted' as ActionTaken, '' as ActionType,
		'' as Details, '' as ModificationComment, '' as ApprovalComment
	from CCG_TR_ApprovalsHistory h
	left join EM on EM.Employee=h.SubmittedBy
	where TableName=@TableName and Period=@Period and PostSeq=@PostSeq and PKey=@PKey and h.SubmittedBy is not null
	UNION ALL
	select DateAdd(minute, DateDiff(minute, getutcdate(), getdate()), ApprovedDate) as ActionDate, EM.LastName + ', ' + IsNull(EM.FirstName,'') as EmpName, 'Approved' as ActionTaken, '' as ActionType,
		'' as Details, '' as ModificationComment, ApprovalComment as ApprovalComment
	from CCG_TR_ApprovalsHistory h 
	left join EM on EM.Employee=h.ApprovedBy
	where TableName=@TableName and Period=@Period and PostSeq=@PostSeq and PKey=@PKey and h.ApprovedBy is not null

	order by ActionDate
END
GO
