SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FW_DropColumn] @tname nvarchar(100), @cname nvarchar(100)
AS
  DECLARE @dname varchar(100),
	  @sqlstr nvarchar(max)
 
  set @sqlstr = 'DECLARE DropConstCursor cursor for select d.name from sys.tables t join sys.default_constraints d on d.parent_object_id = t.object_id join sys.columns c on c.object_id = t.object_id and c.column_id = d.parent_column_id where t.name = ''' + @tname + ''' and c.name = ''' + @cname + '''' 
 
  exec (@sqlstr)
  open DropConstCursor
  fetch DropConstCursor into @dname
  while @@fetch_status = 0
  begin
	set @sqlstr = 'alter table ' + @tname + ' drop constraint ' + @dname
	exec (@sqlstr) 
 
	fetch DropConstCursor into @sqlstr
  end
  close DropConstCursor
  deallocate DropConstCursor
 
  set @sqlstr = 'if exists (select ''x'' from sysobjects a, syscolumns b where a.id = b.id and a.name = ''' + @tname + ''' and b.name = ''' + @cname + ''')  alter table ' + @tname + ' drop column ' + @cname + ''
  exec (@sqlstr)
GO
