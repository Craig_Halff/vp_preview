SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   PROCEDURE [dbo].[HAI_QADefaults]
AS
    BEGIN
        /* 
		Copyright (c) 2021 Halff Associates, Inc.  All rights reserved.
		2/16/2021	
	
		Set QA/QC fileds to defaults.

	*/
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
        SET NOCOUNT ON;

        UPDATE px
        SET px.CustQAReviewRequired = 'Y'
          , px.CustQAManager = qa.CustQAManager
          , px.CustQAReview1Goal = qa.CustReview1Goal
          , px.CustQAReview2Goal = qa.CustReview2Goal
          , px.CustQAReview3Goal = qa.CustReview3Goal
        FROM PR p
           , ProjectCustomTabFields px
           , UDIC_QAQCParameters qa
        WHERE
            p.WBS1 = px.WBS1
            AND p.WBS2 = px.WBS2
            AND p.WBS2 = ''
            AND px.CustPracticeArea = qa.CustPracticeArea
            AND px.CustQAReviewRequired <> 'Y'
            AND p.Fee + p.ConsultFee + p.ReimbAllow >= qa.CustTotalCompensationThreshold
            AND p.ChargeType = 'R'
            AND p.ReadyForProcessing = 'Y'
            AND LEFT(p.Org, 2) <> '02'
            AND EXISTS
        (
            SELECT 'x' FROM PR WHERE WBS1 = p.WBS1 AND WBS2 = ' ' AND Status = 'A'
        );
    END;

GO
