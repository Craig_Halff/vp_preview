SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Effort of Completion Script - Real time update>
			--<modified by Conrad Harrison on 6/18/2018>
-- =============================================
CREATE PROCEDURE [dbo].[Halff_UpdateEAC_TEST]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Update the EAC from the Backlog application
	DECLARE @tempTable TABLE (
	  WBS1 varchar(30) COLLATE database_default,
	  WBS2 varchar(7) COLLATE database_default,
	  Writeoff decimal(19,5),
	  EAC decimal(19,5),
	  active_level varchar(255) COLLATE database_default
	  PRIMARY KEY (WBS1, WBS2)
	)
	
	INSERT INTO @tempTable
	SELECT [prj_code] AS [WBS1]
	,[phase_code] AS [WBS2]
	,ISNULL([writeoff_effort], 0) AS [Writeoff]
	,ISNULL([eac], 0) AS EAC
	,ISNULL([active_level], '') AS [InputLevel]
	FROM [Backlog].[dbo].[EstimatedEffort_RevenueCalc]
	ORDER BY [prj_code], [phase_code]

	-- Update the project custom fields for EAC, Active level, and Writeoff
	Update p
	Set p.CustEffortatCompletion = FORMAT(b.EAC, '#,0;(#,0)'),
		p.CustEACInputLevel = b.active_level,
		p.CustWriteoffAdjmt = FORMAT(b.Writeoff, '#,0;(#,0)')
	From [ProjectCustomTabFields] p, @tempTable b
	Where p.WBS1 = b.WBS1
		and p.WBS2 = b.WBS2
		and p.WBS3 = ' '

END
GO
