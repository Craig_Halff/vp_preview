SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_UltraGridConfig_InitRow]
	@RouteType		varchar(5)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);

	If @RouteType = 'PR'
		SELECT Name, Label
            FROM FW_CustomColumns
            WHERE InfoCenterArea = 'Projects' and DataType = 'Employee' and GridID = 'X'
            ORDER BY 2
	ELSE IF @RouteType = 'VE'
		SELECT Name, Label
            FROM FW_CustomColumns
            WHERE InfoCenterArea = 'Vendors' and DataType = 'Employee' and GridID = 'X'
            ORDER BY 2
	ELSE IF @RouteType = 'SP'
		SELECT name, name
            FROM sys.objects
            WHERE name like '%spCCG_PAT_%Rout%' AND type in (N'P', N'PC')
            ORDER BY 1
END;

GO
