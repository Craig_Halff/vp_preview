SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyConvertWBS2]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg Nvarchar(1000) output,
@UserName Nvarchar(32) = 'VisionKeyConvert' -- Add by DP 4.0
,@Period integer = 0
,@EntityType Nvarchar(20)
as
begin -- Procedure
	SET ANSI_WARNINGS OFF
	set nocount on
	declare @OldWBS1 			Nvarchar(30)
	declare @OldWBS2 			Nvarchar(30)
	declare @OldWBS3 			Nvarchar(30)
	declare @NewWBS1 			Nvarchar(30)
	declare @NewWBS2 			Nvarchar(30)
	declare @NewWBS3 			Nvarchar(30)
	DECLARE @OldAllocMethod varchar(1)
	DECLARE @NewAllocMethod varchar(1)
	DECLARE @OldStoredCurrencySetting Nvarchar(100)
	DECLARE @NewStoredCurrencySetting Nvarchar(100)
	DECLARE @OldStoredCurrencyCode Nvarchar(3)
	DECLARE @NewStoredCurrencyCode Nvarchar(3)
	Declare @Existing 		integer
	declare @Entity 			Nvarchar(20)
	declare @length 			integer
	declare @CvtType 			integer
	declare @KeysFetch 		integer
	declare @RetVal 			integer
	declare @Errmsg2 			Nvarchar(1000),
			@custlabel		Nvarchar(100),
			@CustLabelProj 	Nvarchar(100),
			@custLabelProjPlural	Nvarchar(100),
			@Custlabelplural 	Nvarchar(100)
	declare @NewChargeType 	Nvarchar(1)
	declare @OldChargeType 	Nvarchar(1)
	declare @newSubLevel 	Nvarchar(1)
	declare @oldSubLevel 	Nvarchar(1)
	declare @badProject	Nvarchar(1)
	declare @diag integer
	declare @message nvarchar(max)
	set @diag = 0
	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'WBS2'
	set @length = 30
	set @Existing = 0
	set @KeysFetch = 0

	execute InsertKeyCvtDriver @Entity -- populate driver table

	declare @VPlanSourceExist as varchar(1)
	declare @VPlanTargetExist as varchar(1)
	declare @NPlanSourceExist as varchar(1)
	declare @NPlanTargetExist as varchar(1)
	declare @OldPlanID varchar(32)
	declare @NewPlanID varchar(32)
	declare @CheckedOutID as varchar(32)

-- Add by DP 4.0
	delete from keyconvertDriver where entity = N'WBS2' and TableName = N'PR' and ColumnName = N'WBS2'
	delete from keyconvertDriver where entity = N'WBS2' and TableName = N'BT' and ColumnName = N'WBS2'
	DECLARE @OldName Nvarchar(40)
	DECLARE @NewName Nvarchar(40)
	DECLARE @OldName2 Nvarchar(40)
	DECLARE @NewName2 Nvarchar(40)
	DECLARE @PKey Nvarchar(50)
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0
--
	select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'wbs2Label'
	select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'wbs2LabelPlural'
	select @custLabelProj = LabelValue from FW_CFGLabels where LabelName = 'wbs1Label'
	select @custLabelProjPlural = LabelValue from FW_CFGLabels where LabelName = 'wbs1LabelPlural'

	-- Add Storm 1.1
	declare @RPTaskFetch integer
	declare @RPOldPlanID varchar(32)
	declare @RPOldTaskID varchar(32)
	declare @RPNewTaskID varchar(32)
	declare @RPOldWBS1 	Nvarchar(30)
	declare @RPOldWBS2 	Nvarchar(30)
	declare @RPOldWBS3 	Nvarchar(30)
	declare @RPOldLaborCode 	Nvarchar(14)
	declare @RPOldChildrenCount int 
	declare @RPOldParentOutLineNumber varchar(255)
	declare @RPOldOutlineNumber varchar(255)
	declare @RPOldOutlineLevel int	
	declare @RPTableName varchar(30)

	declare KeysCursor cursor for
		select OldKey,
			OldKey2,
			OldKey3,
			NewKey,
			NewKey2,
			NewKey3, PKey from KeyConvertWork where Entity = @EntityType
	open KeysCursor
	fetch next from KeysCursor into 
		@OldWBS1,
		@OldWBS2,
		@OldWBS3,
		@NewWBS1,
		@NewWBS2,
		@NewWBS3,
		@PKey

	set @KeysFetch = @@Fetch_Status

While (@KeysFetch = 0)
	begin
		begin tran

		Set @VPlanSourceExist = 'N'
		Set @VPlanTargetExist = 'N'
		Set @NPlanSourceExist = 'N'
		Set @NPlanTargetExist = 'N'
		Set @OldPlanID = ''
		Set @NewPlanID = ''
		Set @CheckedOutID = REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')
		declare @TempFetch integer
		declare @TempFetch2 integer
		declare @OldTaskID		varchar(32)
		declare @NewTaskID		varchar(32)
		declare @ResourceID		Nvarchar(20),
				@GenericResourceID		Nvarchar(20),
				@OldAssignmentID	Nvarchar(32),
				@NewAssignmentID	Nvarchar(32)
		declare @WBS1	Nvarchar(30),
				@WBS2	Nvarchar(30),
				@WBS3	Nvarchar(30)
		declare @Account		Nvarchar(13),
				@Vendor		Nvarchar(20),
				@OldExpenseID	Nvarchar(32),
				@NewExpenseID	Nvarchar(32)
		declare @OldConsultantID	Nvarchar(32),
				@NewConsultantID	Nvarchar(32),
				@OldUnitID	Nvarchar(32),
				@NewUnitID	Nvarchar(32)
		declare @LaborCode	Nvarchar(14),
				@WBSType	Nvarchar(4)

-- For checking of Navigator tables
		if exists (select 'x' from PNTask where WBS1 = @OldWBS1)
		Begin
			Set @NPlanSourceExist = 'Y'
			select @OldPlanID=PlanID from PNPlan where wbs1=@OldWBS1
		End
		if exists (select 'x' from PNTask where WBS1 = @NewWBS1)
		Begin
			Set @NPlanTargetExist = 'Y'
			select @NewPlanID=PlanID from PNPlan where wbs1=@NewWBS1
		End
-- For checking of Planning tables
		if exists (select 'x' from RPTask where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and RPTask.PlanID <> @OldPlanID)
			Set @VPlanSourceExist = 'Y'
		if exists (select 'x' from RPTask where WBS1 = @NewWBS1 and WBS2 = @NewWBS2 and RPTask.PlanID <> @NewPlanID)
			Set @VPlanTargetExist = 'Y'

		select @OldName = isnull(PR.Name, N''), @OldAllocMethod = AllocMethod, @OldStoredCurrencySetting = RPPlan.StoredCurrency, @OldStoredCurrencyCode = case when RPPlan.StoredCurrency = 'B' then PR.BillingCurrencyCode Else PR.ProjectCurrencyCode End 
		From PR Left Join RPPlan on PR.WBS1=RPPlan.WBS1 Where PR.WBS1 = @OldWBS1 and PR.WBS2=N' ' and PR.WBS3=N' '
		select @NewName = isnull(PR.Name, N''), @NewAllocMethod = AllocMethod, @NewStoredCurrencySetting = RPPlan.StoredCurrency, @NewStoredCurrencyCode = case when RPPlan.StoredCurrency = 'B' then PR.BillingCurrencyCode Else PR.ProjectCurrencyCode End 
		From PR Left Join RPPlan on PR.WBS1=RPPlan.WBS1 Where PR.WBS1 = @NewWBS1 and PR.WBS2=N' ' and PR.WBS3=N' '
		if (@OldStoredCurrencySetting = 'B')
			Set @OldStoredCurrencySetting = 'Billing'
		else
			Set @OldStoredCurrencySetting = @CustLabelProj
		if (@NewStoredCurrencySetting = 'B')
			Set @NewStoredCurrencySetting = 'Billing'
		else
			Set @NewStoredCurrencySetting = @CustLabelProj

-- Add by DP 4.0
		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany Nvarchar(14)
			DECLARE @RecordCount integer

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				set @RecordCount = (select count(*) from KeyConvertWork where Entity = @EntityType)

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),N'KC','Y',  @custlabel + ' Key Convert ('+rtrim(convert(Nvarchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, left(@custlabel + ' Key Convert', 40), @custlabel);
			end

			Set @PostControlFlag = 1
		end
		select @OldName = isnull(Name, N'') from PR where WBS1 = @OldWBS1 and WBS2=N' ' and WBS3 = N' '
		select @OldName2 = isnull(Name, N'') from PR where WBS1 = @OldWBS1 and WBS2=@OldWBS2 and WBS3 = N' '
		select @NewName = isnull(Name, N'') from PR where WBS1 = @NewWBS1 and WBS2=N' ' and WBS3 = N' '
		select @NewName2 = isnull(Name, N'') from PR where WBS1 = @NewWBS1 and WBS2=@NewWBS2 and WBS3 = N' '
--
		set @Existing = 0
		select @Existing =  1 from PR where WBS1 = @NewWBS1
			and WBS2 = @NewWBS2
			and WBS3 = N' '

		if not exists (select 'x' from pr where WBS1 = @NewWBS1 and WBS2 = N' ' and WBS3 = N' ')
			begin
				--Project: # Phase: #
			SET @message = dbo.GetMessage('MsgInvalidDestNumDoesNotExist',@CustLabelProj,@NewWBS1,@custlabelProj,'','','','','','')
			RAISERROR(@message,16,3)
				close keysCursor
				deallocate keysCursor
				if (@@Trancount > 0)
				   ROLLBACK TRANSACTION
				return(50005) -- user defined error
			end		

		if ((@OldWBS2='' and @NewWBS2='') or (@OldWBS2='' and exists (select 'x' from PR where WBS1 = @OldWBS1 and WBS2 > ' ')))
			begin
				--Project: # Phase: #
			SET @message = dbo.GetMessage('MsgUseDifferentWBSKeyConvertWBS',@CustLabelProj,@OldWBS1,'','','','','','','')
			RAISERROR(@message,16,3)
				close keysCursor
				deallocate keysCursor
				if (@@Trancount > 0)
				   ROLLBACK TRANSACTION
				return(50005) -- user defined error
			end		

-- For checking of Vision/Navigator Planning tables
		declare @RPHardBooking as varchar(1)
		declare @UseBookingForEmpHours as varchar(1)
		declare @UseBookingForGenHours as varchar(1)

		Select @RPHardBooking=SE.RPHardBooking, @UseBookingForEmpHours=UseBookingForEmpHours, @UseBookingForGenHours=UseBookingForGenHours 
		from CFGRMSettings, SE Inner join SEUser on SE.Role=SEUser.Role and SEUser.Username=@UserName

		declare @VPlanSourceCheckedOut as varchar(1)
		declare @NPlanSourceCheckedOut as varchar(1)
		declare @NPlanTargetCheckedOut as varchar(1)
		Set @VPlanSourceCheckedOut = 'N'
		Set @NPlanSourceCheckedOut = 'N'
		Set @NPlanTargetCheckedOut = 'N'

		declare @UnpublishedAllMsg as varchar(max)
		set @UnpublishedAllMsg = ''
		declare @publishedFlag as bit
		declare @CheckedOutUserAllMsg as varchar(max)
		set @CheckedOutUserAllMsg = ''
		declare @PlanName as varchar(255)
		declare @CheckedOutUser as varchar(255)

-- Vision Plan Source
		if (@VPlanSourceExist = 'Y')
			begin
				if exists (select 'x' from RPTask left join RPPlan on RPTask.PlanID = RPPlan.PlanID where RPTask.WBS1 = @OldWBS1 and RPTask.PlanID <> @OldPlanID and RPPlan.CheckedOutUser > '' and RPPlan.CheckedOutUser <> @UserName)
					begin
						set @VPlanSourceCheckedOut = 'Y'

						DECLARE db_cursor CURSOR FOR 
						select distinct isnull(RPPlan.PlanNumber+' : ','')+isnull(RPPlan.PlanName,''), isnull(isnull(EM.FirstName+' ', '')+EM.LastName, RPPlan.CheckedOutUser) as CheckedOutUser
						from RPTask left join RPPlan on RPTask.PlanID = RPPlan.PlanID 
						Left Join SEUser on SEUser.UserName=RPPlan.CheckedOutUser
						Left Join EM on SEUser.Employee=EM.Employee
						where RPTask.WBS1 = @OldWBS1 and RPPlan.CheckedOutUser > '' and RPPlan.CheckedOutUser <> @UserName

						OPEN db_cursor  
						FETCH NEXT FROM db_cursor INTO @PlanName, @CheckedOutUser  

						WHILE @@FETCH_STATUS = 0  
						BEGIN  
							if (@CheckedOutUserAllMsg > '')
								SET @CheckedOutUserAllMsg = @CheckedOutUserAllMsg + CHAR(13)+CHAR(10)						
							SET @CheckedOutUserAllMsg = @CheckedOutUserAllMsg + dbo.GetMessage('MsgPlanCheckedOutBy',@PlanName,@CheckedOutUser,'','','','','','','') 
								
							FETCH NEXT FROM db_cursor INTO @PlanName, @CheckedOutUser  
						END  

						CLOSE db_cursor  
						DEALLOCATE db_cursor
					end
			end

-- Navigator Plan Source
		if (@NPlanSourceExist = 'Y')
			begin
				if exists (select 'x' from RPTask left join RPPlan on RPTask.PlanID = RPPlan.PlanID where RPPlan.PlanID = @OldPlanID and RPPlan.CheckedOutUser > '' and RPPlan.CheckedOutUser <> @UserName)
					begin
						set @NPlanSourceCheckedOut = 'Y'

						select distinct @PlanName = isnull(RPPlan.PlanNumber+' : ','')+isnull(RPPlan.PlanName,''), @CheckedOutUser = isnull(isnull(EM.FirstName+' ', '')+EM.LastName, RPPlan.CheckedOutUser)
						from RPTask left join RPPlan on RPTask.PlanID = RPPlan.PlanID 
						Left Join SEUser on SEUser.UserName=RPPlan.CheckedOutUser
						Left Join EM on SEUser.Employee=EM.Employee
						where RPPlan.PlanID = @OldPlanID and RPPlan.CheckedOutUser > '' and RPPlan.CheckedOutUser <> @UserName

						if (@CheckedOutUserAllMsg > '')
							SET @CheckedOutUserAllMsg = @CheckedOutUserAllMsg + CHAR(13)+CHAR(10)						
						SET @CheckedOutUserAllMsg = @CheckedOutUserAllMsg + dbo.GetMessage('MsgNavigatorPlanCheckedOutBy',@PlanName,@CheckedOutUser,'','','','','','','')
					end

				select @publishedFlag = [dbo].[stRP$DiffPlanned](@OldPlanID), @PlanName = isnull(RPPlan.PlanNumber+' : ','')+isnull(RPPlan.PlanName,'') from RPPlan where RPPlan.PlanID = @OldPlanID
				if (@publishedFlag = 1)
					begin
						if (@UnpublishedAllMsg > '')
							SET @UnpublishedAllMsg = @UnpublishedAllMsg + CHAR(13)+CHAR(10)						
						SET @UnpublishedAllMsg = @UnpublishedAllMsg + dbo.GetMessage('MsgPlanUnpublished',@PlanName,'','','','','','','','')
					end
			end

-- Navigator Plan Target
		if (@OldWBS1 <> @NewWBS1 and @NPlanTargetExist = 'Y')
			begin
				if exists (select 'x' from RPTask left join RPPlan on RPTask.PlanID = RPPlan.PlanID where RPPlan.PlanID = @NewPlanID and RPPlan.CheckedOutUser > '' and RPPlan.CheckedOutUser <> @UserName)
					begin
						set @NPlanTargetCheckedOut = 'Y'

						select distinct @PlanName = isnull(RPPlan.PlanNumber+' : ','')+isnull(RPPlan.PlanName,''), @CheckedOutUser = isnull(isnull(EM.FirstName+' ', '')+EM.LastName, RPPlan.CheckedOutUser)
						from RPTask left join RPPlan on RPTask.PlanID = RPPlan.PlanID 
						Left Join SEUser on SEUser.UserName=RPPlan.CheckedOutUser
						Left Join EM on SEUser.Employee=EM.Employee
						where RPPlan.PlanID = @NewPlanID and RPPlan.CheckedOutUser > '' and RPPlan.CheckedOutUser <> @UserName

						if (@CheckedOutUserAllMsg > '')
							SET @CheckedOutUserAllMsg = @CheckedOutUserAllMsg + CHAR(13)+CHAR(10)						
						SET @CheckedOutUserAllMsg = @CheckedOutUserAllMsg + dbo.GetMessage('MsgNavigatorPlanCheckedOutBy',@PlanName,@CheckedOutUser,'','','','','','','')
					end

				select @publishedFlag = [dbo].[stRP$DiffPlanned](@NewPlanID), @PlanName = isnull(RPPlan.PlanNumber+' : ','')+isnull(RPPlan.PlanName,'') from RPPlan where RPPlan.PlanID = @NewPlanID
				if (@publishedFlag = 1)
					begin
						if (@UnpublishedAllMsg > '')
							SET @UnpublishedAllMsg = @UnpublishedAllMsg + CHAR(13)+CHAR(10)						
						SET @UnpublishedAllMsg = @UnpublishedAllMsg + dbo.GetMessage('MsgPlanUnpublished',@PlanName,'','','','','','','','')
					end
			end

		declare @PlanningModule as varchar(1)
		if exists (select ModuleID from CFGModuleConfiguration where ModuleID='Planning' and isnull(Password,'') > '')
			set @PlanningModule = 'Y'
		else
			set @PlanningModule = 'N'

-- Checking for Checked out
		if (@PlanningModule = 'Y' and @CheckedOutUserAllMsg > '')
			begin
				SET @message = dbo.GetMessage('MsgFollowingPlansAreCheckedOut',@custlabel,CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10)+@CheckedOutUserAllMsg+CHAR(13)+CHAR(10),@Custlabelplural,'','','','','','')
				RAISERROR(@message,16,3)
				close keysCursor
				deallocate keysCursor
				if (@@Trancount > 0)
				   ROLLBACK TRANSACTION
				return(50005) -- user defined error
			end

-- Checking for Unpublished Plan
		if (@PlanningModule = 'Y' and @UnpublishedAllMsg > '')
			begin
				SET @message = dbo.GetMessage('MsgFollowingPlansAreUnpublished',@custlabel,CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10)+@UnpublishedAllMsg+CHAR(13)+CHAR(10),@Custlabelplural,'','','','','','')
				RAISERROR(@message,16,3)
				close keysCursor
				deallocate keysCursor
				if (@@Trancount > 0)
					ROLLBACK TRANSACTION
				return(50005) -- user defined error
			end

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------Storm 1.1 - WBS3 children records Only
		Select PlanID, TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, ChildrenCount, OutlineLevel, OutlineNumber, ParentOutlineNumber, 'RPTask' as TableName Into #tempRPPN From RPTask 
		Where RPTask.WBS1 = @OldWBS1 and isnull(RPTask.WBS2,'') = @OldWBS2 and (ISNULL(RPTask.WBS3,' ')<>' ' And OutlineLevel>=2)
		Union All Select PlanID, TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, ChildrenCount, OutlineLevel, OutlineNumber, ParentOutlineNumber, 'PNTask' as TableName From PNTask
		Where PNTask.WBS1 = @OldWBS1 and isnull(PNTask.WBS2,'') = @OldWBS2 and (ISNULL(PNTask.WBS3,' ')<>' ' And OutlineLevel>=2)
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		declare @MulticompanyEnabled Nvarchar(1)
		declare @MulticurrencyEnabled Nvarchar(1)
		declare @NewFunctionalCurrencyCode Nvarchar(3)
		declare @OldFunctionalCurrencyCode Nvarchar(3)
		declare @oldCompany Nvarchar(14)
		declare @newCompany Nvarchar(14)
		select @MulticompanyEnabled = MulticompanyEnabled,
			   	@MulticurrencyEnabled = MulticurrencyEnabled from FW_CFGSystem
				
		if (@MulticurrencyEnabled = 'Y')
			declare @oldCurrency Nvarchar(14)
			declare @newCurrency Nvarchar(14)
			begin
				set @oldCurrency = ''
				set @newCurrency = ''
				select 
					@oldCurrency = old.ProjectCurrencyCode+';'+old.BillingCurrencyCode,
					@newCurrency = new.ProjectCurrencyCode+';'+new.BillingCurrencyCode
				from pr new inner join pr Old on (new.wbs2 = old.wbs2 and new.wbs3 = old.wbs3)
				where new.wbs1 = @NewWBS1 and old.wbs1 = @OldWBS1
				and (old.ProjectCurrencyCode+old.BillingCurrencyCode <> new.ProjectCurrencyCode+new.BillingCurrencyCode)
				order by new.wbs1,
				new.wbs2,
				new.wbs3

				if (@oldCurrency <> @newCurrency)
					begin
					SET @message = dbo.GetMessage('MsgWithMultiCmpYouCantMergeDiffCurr',@custlabelPlural,@CustLabelProj,@OldWBS1,@oldCurrency,@CustLabelProj,@NewWBS1,@newCurrency,'','')
					RAISERROR(@message,16,3)
						close keysCursor
						deallocate keysCursor
						if (@@Trancount > 0)
							ROLLBACK TRANSACTION
						return(50003) -- user defined error
					end
			end

	if (@MulticurrencyEnabled='Y' and @oldStoredCurrencyCode <> @newStoredCurrencyCode and @OldAllocMethod = 'r' and @NewAllocMethod = 'r')
		begin
		SET @message = dbo.GetMessage('MsgWithMultiCmpYouCantMergeDiffStoredCurr',LOWER(@custLabelProjPlural),@custLabelProj,@OldWBS1,LOWER(@OldStoredCurrencySetting),@oldStoredCurrencyCode,LOWER(@custLabelProj),@NewWBS1,LOWER(@NewStoredCurrencySetting),@newStoredCurrencyCode)
		RAISERROR(@message,16,3)
			close keysCursor
			deallocate keysCursor
			if (@@Trancount > 0)
				ROLLBACK TRANSACTION
			return(50003) -- user defined error
		end
		
	If (@Existing = 1)
		begin
			  if (@DeleteExisting = 0)
				begin
					--Project: # Phase: #
				SET @message = dbo.GetMessage('MsgAnExistConversionWillNotCont',@CustLabelProj,@NewWBS1,@custlabel,@NewWBS2,@CustLabel,@custLabelPlural,'','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end
				if (@NewWBS1 = @OldWBS1 and @NewWBS2 = @OldWbs2)
					begin
					SET @message = dbo.GetMessage('MsgAreTheSameNumConvNotCont',@custLabelProjPlural,@custlabelPlural,'','','','','','','')
					RAISERROR(@message,16,3)
						close keysCursor
						deallocate keysCursor
						if (@@Trancount > 0)
						   ROLLBACK TRANSACTION
						return(50002) -- user defined error
					end

				if (@MulticompanyEnabled = 'Y')
					begin
						set @oldCompany = ''
						set @newCompany = ''
						select 
							@oldCompany = substring(old.org,1,Org1Length),
							@newCompany = substring(new.org,1,Org1Length)
						from pr new inner join pr Old on (new.wbs3 = old.wbs3),cfgformat
						where new.WBS1 = @NewWBS1 and 
						old.WBS1 = @OldWBS1 and 
						new.WBS2 = @newWBS2 and 
						old.wbs2 = @oldWBS2 and
						(substring(old.org,1,Org1Length) <> substring(new.org,1,Org1Length))
						order by new.wbs1,
						new.wbs2,
						new.wbs3

						if (@oldCompany <> @newCompany)
							begin
							SET @message = dbo.GetMessage('MsgWithMultiCmpYouCannotMerge',@custlabelPlural,@CustLabelProj,@OldWBS1,@oldCompany,@CustLabelProj,@NewWBS1,@newCompany,'','')
							RAISERROR(@message,16,3)
								close keysCursor
								deallocate keysCursor
								if (@@Trancount > 0)
								   ROLLBACK TRANSACTION
								return(50003) -- user defined error
							end
					end

			--Test for inconsistent chargeTypes and task structures
			-- The following are valid transfers
			-- Proj/Phase -> Proj/Phase both have tasks
			-- Proj/Phase -> Proj/Phase both don't have tasks
			-- Proj(no Phases) -> Proj/Phase(no tasks)
			-- Proj/Phase(no tasks) -> Proj(noPhases)
				set @NewChargeType = ''
				set @OldChargeType = ''
				set @newSubLevel = ''
				set @oldSubLevel = ''
				set @badProject = 'N'
				select 
					@badProject = case when new.wbs2 = N' ' and new.subLevel = 'Y' then 'Y' else 'N' end,
					@NewChargeType = new.chargeType,
					@newSubLevel = new.subLevel,
					@OldChargeType = old.chargeType,
					@oldSubLevel = old.subLevel 
				from pr new inner join pr Old on (new.wbs3 = old.wbs3)
					where new.WBS1 = @NewWBS1 and 
						old.WBS1 = @OldWBS1 and 
						new.WBS2 = @newWBS2 and 
						old.wbs2 = @oldWBS2 and
					 (case old.ChargeType when 'P' then 'H' else old.ChargeType end
						<> case new.ChargeType when 'P' then 'H' else new.ChargeType end or 
						old.subLevel <> new.subLevel or
						(new.wbs2 = N' ' and new.subLevel = 'Y'))
				order by new.WBS1,
				new.wbs2,
				new.wbs3
			 if (@NewChargeType <> '') --somthing was returned from the above query = error
				begin
					if (@oldSubLevel <> @NewSublevel)
						begin
						SET @message = dbo.GetMessage('MsgHasDiffWorkBreakDown',@custlabel,@NewWBS2,@custlabel,@oldWBS2,'','','','','')
						RAISERROR(@message,16,3)
							close keysCursor
							deallocate keysCursor
							if (@@Trancount > 0)
							   ROLLBACK TRANSACTION
							return(50003) -- user defined error
						end
					if (@oldChargeType <> @NewChargeType)
						begin
						SET @message = dbo.GetMessage('MsgHasDiffChargeType',@CustLabelProj,@NewWBS1,@custlabel,@NewWBS2,@CustLabelProj,@oldWBS1,@custlabel,@oldWBS2,'')
						RAISERROR(@message,16,3)					
							close keysCursor
							deallocate keysCursor
							if (@@Trancount > 0)
							   ROLLBACK TRANSACTION
							return(50003) -- user defined error
						end
					if (@badProject = 'Y')
						begin
						SET @message = dbo.GetMessage('MsgInvalidDestinationANum',@CustLabelProj,@NewWBS1,@custlabel,@NewWBS2,@CustLabel,@custLabelProjPlural,@custLabelPlural,'','')
						RAISERROR(@message,16,3)			
							close keysCursor
							deallocate keysCursor
							if (@@Trancount > 0)
							   ROLLBACK TRANSACTION
							return(50003) -- user defined error
						end 
		
				end	--End Test for inconsistent chargeTypes and task structures


-- Checking for Expense and Consultant WBS Level
			if (@NPlanSourceExist='Y' and @NPlanTargetExist='Y' and @OldWBS1 <> @NewWBS1) 
				Begin
					declare @ExpTab as varchar(1)
					declare @ConTab as varchar(1)
					if (@MulticompanyEnabled = 'Y')
						select @ExpTab=ExpTab, @ConTab=ConTab from CFGResourcePlanning left join 
						PR on PR.WBS1=@OldWBS1 and left(PR.Org, (select Org1Length from cfgformat)) = CFGResourcePlanning.Company
					else
						select @ExpTab=ExpTab, @ConTab=ConTab from CFGResourcePlanning

					if (@ExpTab = 'Y' or @ConTab = 'Y')
						Begin
							declare @ExpWBSLevelSource as smallint
							declare @ExpWBSLevelTarget as smallint
							declare @ConWBSLevelSource as smallint
							declare @ConWBSLevelTarget as smallint
							select @ExpWBSLevelSource=isnull(ExpWBSLevel,0), @ConWBSLevelSource=isnull(ConWBSLevel,0) from PNPlan Where WBS1=@OldWBS1
							select @ExpWBSLevelTarget=isnull(ExpWBSLevel,0), @ConWBSLevelTarget=isnull(ConWBSLevel,0) from PNPlan Where WBS1=@NewWBS1
							if ((@ExpTab = 'Y' and @ExpWBSLevelSource <> @ExpWBSLevelTarget) and (@ConTab = 'Y' and @ConWBSLevelSource <> @ConWBSLevelTarget))
								begin
									SET @message = dbo.GetMessage('MsgExpConLevelNotMatched',@Custlabelplural,'','','','','','','','')
									RAISERROR(@message,16,3)
									close keysCursor
									deallocate keysCursor
									if (@@Trancount > 0)
									   ROLLBACK TRANSACTION
									return(50005) -- user defined error
								end
							if ((@ExpTab = 'Y' and @ExpWBSLevelSource <> @ExpWBSLevelTarget) and (@ConTab = 'N' or @ConWBSLevelSource = @ConWBSLevelTarget))							
								begin
									SET @message = dbo.GetMessage('MsgExpLevelNotMatched',@Custlabelplural,'','','','','','','','')
									RAISERROR(@message,16,3)
									close keysCursor
									deallocate keysCursor
									if (@@Trancount > 0)
									   ROLLBACK TRANSACTION
									return(50005) -- user defined error
								end
							if ((@ExpTab = 'N' or @ExpWBSLevelSource = @ExpWBSLevelTarget) and( @ConTab = 'Y' and @ConWBSLevelSource <> @ConWBSLevelTarget))
								begin
									SET @message = dbo.GetMessage('MsgConLevelNotMatched',@Custlabelplural,'','','','','','','','')
									RAISERROR(@message,16,3)
									close keysCursor
									deallocate keysCursor
									if (@@Trancount > 0)
									   ROLLBACK TRANSACTION
									return(50005) -- user defined error
								end
						End			
				End
			
/* Removed by DP 4.0
			  exec @RetVal = KeyCvt @Entity = @Entity,
											@CvtType = @CvtType,
											@Length = 20,
											@ErrMsg = @ErrMsg2 output,
											@Diag = @Diag,
											@ConstrFlag = 3 -- UpdateTables
*/
							--Project -> Project
			if (@diag <> 0)
				print 'Delete from PR'
/*new for 3.0*/
/*PRChargeCompanies
WBS1	PRChargeCompanies
WBS2	PRChargeCompanies
WBS3	PRChargeCompanies
Company*/	
	        Delete FROM PRChargeCompanies WHERE PRChargeCompanies.WBS1 = @OldWBS1 and PRChargeCompanies.WBS2 = @OldWBS2 and
				exists (select 'x' from PRChargeCompanies new where 
					new.WBS1 = @NewWBS1 
					and new.wbs2 = @NewWBS2 
					and new.wbs3 = PRChargeCompanies.wbs3
					and new.Company = PRChargeCompanies.Company)	
/*end 3.0*/
/*new for 2.0*/
/*
EMProjectAssocTemplate   WBS1
EMProjectAssocTemplate   WBS2
EMProjectAssocTemplate   WBS3
EMProjectAssocTemplate   Employee
EMProjectAssocTemplate   Role*/
	        Delete FROM EMProjectAssocTemplate WHERE EMProjectAssocTemplate.WBS1 = @OldWBS1 and EMProjectAssocTemplate.WBS2 = @OldWBS2 and
				exists (select 'x' from EMProjectAssocTemplate new where 
					new.WBS1 = @NewWBS1 
					and new.wbs2 = @NewWBS2 
					and new.wbs3 = EMProjectAssocTemplate.wbs3
					and new.Employee = EMProjectAssocTemplate.Employee
					and new.Role = EMProjectAssocTemplate.Role)
/*
PRAwardsTemplate         WBS1
PRAwardsTemplate         WBS2
PRAwardsTemplate         WBS3
PRAwardsTemplate         RecordID
*/
	        Delete FROM PRAwardsTemplate WHERE PRAwardsTemplate.WBS1 = @OldWBS1 and PRAwardsTemplate.WBS2 = @OldWBS2 and
				exists (select 'x' from PRAwardsTemplate new where 
					new.WBS1 = @NewWBS1 
					and new.wbs2 = @NewWBS2 
					and new.wbs3 = PRAwardsTemplate.wbs3
					and new.RecordID = PRAwardsTemplate.RecordID)
/*
PRDescriptionsTemplate   WBS1
PRDescriptionsTemplate   WBS2
PRDescriptionsTemplate   WBS3
PRDescriptionsTemplate   DescCategory
*/
	        Delete FROM PRDescriptionsTemplate WHERE PRDescriptionsTemplate.WBS1 = @OldWBS1 and PRDescriptionsTemplate.WBS2 = @OldWBS2 and
				exists (select 'x' from PRDescriptionsTemplate new where 
					new.WBS1 = @NewWBS1 
					and new.wbs2 = @NewWBS2 
					and new.wbs3 = PRDescriptionsTemplate.wbs3
					and new.DescCategory = PRDescriptionsTemplate.DescCategory)
/*
PRProjectCodesTemplate   WBS1
PRProjectCodesTemplate   WBS2
PRProjectCodesTemplate   WBS3
PRProjectCodesTemplate   ProjectCode

*/
	        Delete FROM PRProjectCodesTemplate WHERE PRProjectCodesTemplate.WBS1 = @OldWBS1 and PRProjectCodesTemplate.WBS2 = @OldWBS2 and
				exists (select 'x' from PRProjectCodesTemplate new where 
					new.WBS1 = @NewWBS1 
					and new.wbs2 = @NewWBS2 
					and new.wbs3 = PRProjectCodesTemplate.wbs3
					and new.ProjectCode = PRProjectCodesTemplate.ProjectCode)
/*
PRTemplate               WBS1
PRTemplate               WBS2
PRTemplate               WBS3
*/
	        Delete FROM PRTemplate WHERE PRTemplate.WBS1 = @OldWBS1 and PRTemplate.WBS2 = @OldWBS2 and
				exists (select 'x' from PRTemplate new where 
					new.WBS1 = @NewWBS1 
					and new.wbs2 = @NewWBS2 
					and new.wbs3 = PRTemplate.wbs3)
/*
ClendorProjectAssoc           Vendor
ClendorProjectAssoc           WBS1
ClendorProjectAssoc           WBS2
ClendorProjectAssoc           WBS3
*/
	        Delete FROM ClendorProjectAssoc WHERE ClendorProjectAssoc.WBS1 = @OldWBS1 and ClendorProjectAssoc.WBS2 = @OldWBS2 and
				exists (select 'x' from ClendorProjectAssoc new where 
					new.WBS1 = @NewWBS1 
					and new.wbs2 = @NewWBS2 
					and new.wbs3 = ClendorProjectAssoc.wbs3
					and new.ClientID = ClendorProjectAssoc.ClientID)
/*
ClendorProjectAssocTemplate   Vendor
ClendorProjectAssocTemplate   WBS1
ClendorProjectAssocTemplate   WBS2
ClendorProjectAssocTemplate   WBS3*/
	        Delete FROM ClendorProjectAssocTemplate WHERE ClendorProjectAssocTemplate.WBS1 = @OldWBS1 and ClendorProjectAssocTemplate.WBS2 = @OldWBS2 and
				exists (select 'x' from ClendorProjectAssocTemplate new where 
					new.WBS1 = @NewWBS1 
					and new.wbs2 = @NewWBS2 
					and new.wbs3 = ClendorProjectAssocTemplate.wbs3
					and new.ClientID = ClendorProjectAssocTemplate.ClientID)


	        Delete FROM PRCompetitionAssoc WHERE PRCompetitionAssoc.WBS1 = @OldWBS1 and PRCompetitionAssoc.WBS2 = @OldWBS2 and
				exists (select 'x' from PRCompetitionAssoc new where 
					new.WBS1 = @NewWBS1 
					and new.wbs2 = @NewWBS2 
					and new.wbs3 = PRCompetitionAssoc.wbs3
					and new.ClientID = PRCompetitionAssoc.ClientID)

	        Delete FROM PRCompetitionAssocTemplate WHERE PRCompetitionAssocTemplate.WBS1 = @OldWBS1 and PRCompetitionAssocTemplate.WBS2 = @OldWBS2 and
				exists (select 'x' from PRCompetitionAssocTemplate new where 
					new.WBS1 = @NewWBS1 
					and new.wbs2 = @NewWBS2 
					and new.wbs3 = PRCompetitionAssocTemplate.wbs3
					and new.ClientID = PRCompetitionAssocTemplate.ClientID)


				delete from MktCampaignProjectAssoc where MktCampaignProjectAssoc.WBS1 = @OldWBS1 and MktCampaignProjectAssoc.WBS2 = @OldWBS2 and
				exists (select 'x' from MktCampaignProjectAssoc new where new.wbs1 = @NewWBS1 
									and new.WBS2 = @NewWBS2 
									and new.WBS3 = MktCampaignProjectAssoc.WBS3
									and new.CampaignID = MktCampaignProjectAssoc.CampaignID)

				delete from MktCampaignProjectAssocTemplate where MktCampaignProjectAssocTemplate.WBS1 = @OldWBS1 and MktCampaignProjectAssocTemplate.WBS2 = @OldWBS2 and
				exists (select 'x' from MktCampaignProjectAssocTemplate new where new.wbs1 = @NewWBS1 
									and new.WBS2 = @NewWBS2 
									and new.WBS3 = MktCampaignProjectAssocTemplate.WBS3
									and new.CampaignID = MktCampaignProjectAssocTemplate.CampaignID)
/*end 2.0*/
-- Removed by DP
--	        Delete FROM PR WHERE PR.WBS1 = @OldWBS1 and PR.WBS2 = @OldWBS2 and
--				exists (select 'x' from pr new where new.WBS1 = @NewWBS1 and new.wbs2 = @NewWBS2 and new.wbs3 = pr.wbs3)
-- Added by DP
				Delete from PRSummaryWBSList where WBS1 = @OldWBS1 and WBS2 = @OldWBS2
--
				delete from reportWBSList where wbs1 = @oldwbs1 and wbs2 = @oldwbs2

				delete from EMProjectAssoc where EMProjectAssoc.WBS1 = @OldWBS1 and EMProjectAssoc.WBS2 = @OldWBS2 and
				exists (select 'x' from EMProjectAssoc new where new.WBS1 = @NewWBS1 and new.wbs2 = @NewWBS2 and new.wbs3 = EMProjectAssoc.wbs3 and new.Employee = EMProjectAssoc.Employee and new.Role = EMProjectAssoc.Role)

				delete from PRAwards where PRAwards.WBS1 = @OldWBS1 and PRAwards.WBS2 = @OldWBS2 and
				exists (select 'x' from PRAwards new where new.WBS1 = @NewWBS1 and new.wbs2 = @NewWBS2 and new.wbs3 = PRAwards.wbs3 and new.RecordID = PRAwards.RecordID)

				IF EXISTS (SELECT 'x' FROM sys.objects WHERE name = 'ProjectCustomTabFields' AND type = 'U')
					begin	
						delete from ProjectCustomTabFields where ProjectCustomTabFields.WBS1 = @OldWBS1 and ProjectCustomTabFields.WBS2 = @OldWBS2 and
						exists (select 'x' from ProjectCustomTabFields new where new.WBS1 = @NewWBS1 and new.wbs2 = @NewWBS2 and new.wbs3 = ProjectCustomTabFields.wbs3)
					end
				delete from PRDescriptions where PRDescriptions.WBS1 = @OldWBS1 and PRDescriptions.WBS2 = @OldWBS2 and
				exists (select 'x' from PRDescriptions new where new.WBS1 = @NewWBS1 and new.wbs2 = @NewWBS2 and new.wbs3 = PRDescriptions.wbs3 and new.DescCategory = PRDescriptions.DescCategory)

				delete from PRFileLinks where PRFileLinks.WBS1 = @OldWBS1 and PRFileLinks.WBS2 = @OldWBS2 and
				exists (select 'x' from PRFileLinks new where new.WBS1 = @NewWBS1 and new.wbs2 = @NewWBS2 and new.wbs3 = PRFileLinks.wbs3 and new.LinkID = PRFileLinks.LinkID)

				delete from PRProjectCodes where PRProjectCodes.WBS1 = @OldWBS1 and PRProjectCodes.WBS2 = @OldWBS2 and
				exists (select 'x' from PRProjectCodes new where new.WBS1 = @NewWBS1 and new.wbs2 = @NewWBS2 and new.wbs3 = PRProjectCodes.wbs3 and new.ProjectCode = PRProjectCodes.ProjectCode)
/*
ALTER TABLE PRSubscr WITH NOCHECK 
    ADD CONSTRAINT PRSubscrPK PRIMARY KEY NONCLUSTERED 
	(WBS1, WBS2, WBS3, UserName)   
    WITH FILLFACTOR = 90
go*/
				delete from PRSubscr where PRSubscr.WBS1 = @oldWBS1 and PRSubscr.WBS2 = @oldWBS2 and
				exists (select 'x' from PRSubscr new where new.WBS1 = @NewWBS1 and new.wbs2 = @NewWBS2 and new.wbs3 = PRSubscr.wbs3  and PRSubscr.userName = new.username)
			
				delete from inMaster where inMaster.WBS1 = @OldWBS1 and inMaster.WBS2 = @OldWBS2 and
				exists (select 'x' from inMaster new where new.WBS1 = @NewWBS1 and new.wbs2 = @NewWBS2 and new.wbs3 = inMaster.wbs3 and new.Batch = inMaster.Batch and new.Invoice = inMaster.Invoice)

				
			update EB
				set eb.AmtBud =  eb.AmtBud + old.AmtBud,
				eb.BillBud = eb.BillBud + old.BillBud,
				eb.EtcAmt =  eb.EtcAmt + old.EtcAmt,
				eb.EacAmt = eb.EacAmt + old.EacAmt
				from eb old inner join eb on (eb.WBS3 = old.WBS3 and 	eb.Vendor = old.Vendor and	eb.Account = old.Account)
				where
					eb.WBS1 = @NewWBS1 and
					old.WBS1 = @OldWBS1 and
					eb.WBS2 = @NewWBS2 and
					old.WBS2 = @OldWBS2

				Delete from EB where WBS1 = @OldWBS1 and WBS2 = @OldWBS2
					and exists (select 'x' from EB new where  new.WBS1 = @newWBS1 and
																			new.WBS2 = @newWBS2 and
																			EB.WBS3 = new.WBS3 and 
																			eb.Vendor = new.Vendor and
																			EB.Account = new.Account)

				update LB 
				set LB.BillRate = LB.BillRate + old.BillRate,
					LB.HrsBud = LB.HrsBud + old.HrsBud,
					LB.AmtBud = LB.AmtBud + old.AmtBud,
					LB.BillBud = LB.BillBud + old.BillBud,
					LB.EtcHrs = LB.EtcHrs + old.EtcHrs,
					LB.EtcAmt = LB.EtcAmt + old.EtcAmt,
					LB.EacHrs = LB.EacHrs + old.EacHrs,
					LB.EacAmt = LB.EacAmt + old.EacAmt
				from LB old inner join LB on (LB.WBS3 = old.WBS3 and LB.LaborCode = old.LaborCode)
				where
					LB.WBS1 = @NewWBS1 and
					old.WBS1 = @OldWBS1 and
					LB.WBS2 = @NewWBS2 and
					old.WBS2 = @OldWBS2

				Delete from LB where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 
					and exists (select 'x' from LB new where  new.WBS1 = @newWBS1 and
																			new.WBS2 = @newWBS2 and
																			LB.WBS3 = new.WBS3 and 
																			LB.LaborCode = new.LaborCode)
        --Delete from LB where lbWBS2 = @OldWBS2
        --Delete from EB where ebWBS2 = @OldWBS2

			Update PRF 
			set PRF.RegOH = PRF.RegOH + old.regoh,
				 PRF.GandAOH = PRF.GandAOH + old.GandAOH,
				 PRF.RegOHProjectCurrency =  PRF.RegOHProjectCurrency + old.RegOHProjectCurrency,
				 PRF.GandAOHProjectCurrency =  PRF.GandAOHProjectCurrency + old.GandAOHProjectCurrency
			from PRF old inner join PRF on (PRF.WBS3 = old.WBS3 and PRF.Period = old.Period)
			where PRF.WBS1 = @NewWBS1 and 
					old.WBS1 = @OldWbs1 and 
					PRF.WBS2 = @NewWBS2 and 
					old.WBS2 = @OldWBS2

        Delete FROM PRF  
                WHERE PRF.WBS1 = @OldWBS1 and PRF.WBS2 = @OldWBS2 and 
					exists (select 'x' from PRF new where new.WBS1 = @NewWBS1 and
																	  new.WBS2 = @NewWBS2 and
																	  new.WBS3 = PRF.WBS3 and
																	  new.Period = PRF.Period)


			update PRSummarySub set
				PRSummarySub.Revenue = PRSummarySub.Revenue + old.Revenue,
				PRSummarySub.DirectConsCost = PRSummarySub.DirectConsCost + old.DirectConsCost,
				PRSummarySub.DirectOtherCost = PRSummarySub.DirectOtherCost + old.DirectOtherCost,
				PRSummarySub.DirectConsBilling = PRSummarySub.DirectConsBilling + old.DirectConsBilling,
				PRSummarySub.DirectOtherBilling = PRSummarySub.DirectOtherBilling + old.DirectOtherBilling,
				PRSummarySub.InDirectCost = PRSummarySub.InDirectCost + old.InDirectCost,
				PRSummarySub.InDirectBilling = PRSummarySub.InDirectBilling + old.InDirectBilling,
				PRSummarySub.ReimbConsCost = PRSummarySub.ReimbConsCost + old.ReimbConsCost,
				PRSummarySub.ReimbOtherCost = PRSummarySub.ReimbOtherCost + old.ReimbOtherCost,
				PRSummarySub.ReimbConsBilling = PRSummarySub.ReimbConsBilling + old.ReimbConsBilling,
				PRSummarySub.ReimbOtherBilling = PRSummarySub.ReimbOtherBilling + old.ReimbOtherBilling,
				PRSummarySub.Hours = PRSummarySub.Hours + old.Hours,
				PRSummarySub.LaborCost = PRSummarySub.LaborCost + old.LaborCost,
				PRSummarySub.LaborBilling = PRSummarySub.LaborBilling + old.LaborBilling,
				PRSummarySub.Billed = PRSummarySub.Billed + old.Billed,
				PRSummarySub.BilledLab = PRSummarySub.BilledLab + old.BilledLab,
				PRSummarySub.BilledCons = PRSummarySub.BilledCons + old.BilledCons,
				PRSummarySub.BilledExp = PRSummarySub.BilledExp + old.BilledExp,
				PRSummarySub.BilledFee = PRSummarySub.BilledFee + old.BilledFee,
				PRSummarySub.BilledUnit = PRSummarySub.BilledUnit + old.BilledUnit,
				PRSummarySub.BilledAddOn = PRSummarySub.BilledAddOn + old.BilledAddOn,
				PRSummarySub.BilledTaxes = PRSummarySub.BilledTaxes + old.BilledTaxes,
				PRSummarySub.BilledInterest = PRSummarySub.BilledInterest + old.BilledInterest,
				PRSummarySub.BilledOther = PRSummarySub.BilledOther + old.BilledOther,
				PRSummarySub.AR = PRSummarySub.AR + old.AR,
				PRSummarySub.Received = PRSummarySub.Received + old.Received,
				PRSummarySub.Overhead = PRSummarySub.Overhead + old.Overhead,
				PRSummarySub.RevenueBillingCurrency = PRSummarySub.RevenueBillingCurrency + old.RevenueBillingCurrency,
				PRSummarySub.RevenueProjectCurrency = PRSummarySub.RevenueProjectCurrency + old.RevenueProjectCurrency,
				PRSummarySub.DirectConsCostProjectCurrency = PRSummarySub.DirectConsCostProjectCurrency + old.DirectConsCostProjectCurrency,
				PRSummarySub.DirectOtherCostProjectCurrency = PRSummarySub.DirectOtherCostProjectCurrency + old.DirectOtherCostProjectCurrency,
				PRSummarySub.ReimbConsCostProjectCurrency = PRSummarySub.ReimbConsCostProjectCurrency + old.ReimbConsCostProjectCurrency,
				PRSummarySub.ReimbOtherCostProjectCurrency = PRSummarySub.ReimbOtherCostProjectCurrency + old.ReimbOtherCostProjectCurrency,
				PRSummarySub.InDirectCostProjectCurrency = PRSummarySub.InDirectCostProjectCurrency + old.InDirectCostProjectCurrency,
				PRSummarySub.LaborCostProjectCurrency = PRSummarySub.LaborCostProjectCurrency + old.LaborCostProjectCurrency,
				PRSummarySub.BilledBillingCurrency = PRSummarySub.BilledBillingCurrency + old.BilledBillingCurrency,
				PRSummarySub.BilledLabBillingCurrency = PRSummarySub.BilledLabBillingCurrency + old.BilledLabBillingCurrency,
				PRSummarySub.BilledConsBillingCurrency = PRSummarySub.BilledConsBillingCurrency + old.BilledConsBillingCurrency,
				PRSummarySub.BilledExpBillingCurrency = PRSummarySub.BilledExpBillingCurrency + old.BilledExpBillingCurrency,
				PRSummarySub.BilledFeeBillingCurrency = PRSummarySub.BilledFeeBillingCurrency + old.BilledFeeBillingCurrency,
				PRSummarySub.BilledUnitBillingCurrency = PRSummarySub.BilledUnitBillingCurrency + old.BilledUnitBillingCurrency,
				PRSummarySub.BilledAddOnBillingCurrency = PRSummarySub.BilledAddOnBillingCurrency + old.BilledAddOnBillingCurrency,
				PRSummarySub.BilledTaxesBillingCurrency = PRSummarySub.BilledTaxesBillingCurrency + old.BilledTaxesBillingCurrency,
				PRSummarySub.BilledInterestBillingCurrency = PRSummarySub.BilledInterestBillingCurrency + old.BilledInterestBillingCurrency,
				PRSummarySub.BilledOtherBillingCurrency = PRSummarySub.BilledOtherBillingCurrency + old.BilledOtherBillingCurrency,
				PRSummarySub.BilledProjectCurrency = PRSummarySub.BilledProjectCurrency + old.BilledProjectCurrency,
				PRSummarySub.BilledLabProjectCurrency = PRSummarySub.BilledLabProjectCurrency + old.BilledLabProjectCurrency,
				PRSummarySub.BilledConsProjectCurrency = PRSummarySub.BilledConsProjectCurrency + old.BilledConsProjectCurrency,
				PRSummarySub.BilledExpProjectCurrency = PRSummarySub.BilledExpProjectCurrency + old.BilledExpProjectCurrency,
				PRSummarySub.BilledFeeProjectCurrency = PRSummarySub.BilledFeeProjectCurrency + old.BilledFeeProjectCurrency,
				PRSummarySub.BilledUnitProjectCurrency = PRSummarySub.BilledUnitProjectCurrency + old.BilledUnitProjectCurrency,
				PRSummarySub.BilledAddOnProjectCurrency = PRSummarySub.BilledAddOnProjectCurrency + old.BilledAddOnProjectCurrency,
				PRSummarySub.BilledTaxesProjectCurrency = PRSummarySub.BilledTaxesProjectCurrency + old.BilledTaxesProjectCurrency,
				PRSummarySub.BilledInterestProjectCurrency = PRSummarySub.BilledInterestProjectCurrency + old.BilledInterestProjectCurrency,
				PRSummarySub.BilledOtherProjectCurrency = PRSummarySub.BilledOtherProjectCurrency + old.BilledOtherProjectCurrency,
				PRSummarySub.ARProjectCurrency = PRSummarySub.ARProjectCurrency + old.ARProjectCurrency,
				PRSummarySub.ReceivedProjectCurrency = PRSummarySub.ReceivedProjectCurrency + old.ReceivedProjectCurrency,
				PRSummarySub.ReceivedBillingCurrency = PRSummarySub.ReceivedBillingCurrency + old.ReceivedBillingCurrency,
				PRSummarySub.OverheadProjectCurrency = PRSummarySub.OverheadProjectCurrency + old.OverheadProjectCurrency,
				PRSummarySub.LaborCostBillingCurrency = PRSummarySub.LaborCostBillingCurrency + old.LaborCostBillingCurrency,
				PRSummarySub.DirectConsCostBillingCurrency = PRSummarySub.DirectConsCostBillingCurrency + old.DirectConsCostBillingCurrency,
				PRSummarySub.DirectOtherCostBillingCurrency = PRSummarySub.DirectOtherCostBillingCurrency + old.DirectOtherCostBillingCurrency,
				PRSummarySub.ReimbConsCostBillingCurrency = PRSummarySub.ReimbConsCostBillingCurrency + old.ReimbConsCostBillingCurrency,
				PRSummarySub.ReimbOtherCostBillingCurrency = PRSummarySub.ReimbOtherCostBillingCurrency + old.ReimbOtherCostBillingCurrency,
				PRSummarySub.InDirectCostBillingCurrency = PRSummarySub.InDirectCostBillingCurrency + old.InDirectCostBillingCurrency,
				PRSummarySub.RegHours = PRSummarySub.RegHours + old.RegHours,
				PRSummarySub.OvtHours = PRSummarySub.OvtHours + old.OvtHours,
				PRSummarySub.SpecialOvtHours = PRSummarySub.SpecialOvtHours + old.SpecialOvtHours,
				PRSummarySub.LaborRegAmt = PRSummarySub.LaborRegAmt + old.LaborRegAmt,
				PRSummarySub.LaborOvtAmt = PRSummarySub.LaborOvtAmt + old.LaborOvtAmt,
				PRSummarySub.LaborSpecialOvtAmt = PRSummarySub.LaborSpecialOvtAmt + old.LaborSpecialOvtAmt,
				PRSummarySub.LaborRegAmtProjectCurrency = PRSummarySub.LaborRegAmtProjectCurrency + old.LaborRegAmtProjectCurrency,
				PRSummarySub.LaborOvtAmtProjectCurrency = PRSummarySub.LaborOvtAmtProjectCurrency + old.LaborOvtAmtProjectCurrency,
				PRSummarySub.LaborSpecialOvtAmtProjectCurrency = PRSummarySub.LaborSpecialOvtAmtProjectCurrency + old.LaborSpecialOvtAmtProjectCurrency,
				PRSummarySub.LaborRegAmtBillingCurrency = PRSummarySub.LaborRegAmtBillingCurrency + old.LaborRegAmtBillingCurrency,
				PRSummarySub.LaborOvtAmtBillingCurrency = PRSummarySub.LaborOvtAmtBillingCurrency + old.LaborOvtAmtBillingCurrency,
				PRSummarySub.LaborSpecialOvtAmtBillingCurrency = PRSummarySub.LaborSpecialOvtAmtBillingCurrency + old.LaborSpecialOvtAmtBillingCurrency
			from PRSummarySub old inner join PRSummarySub on 
				(PRSummarySub.WBS3 = old.WBS3 and PRSummarySub.Period = old.Period)
			where
				PRSummarySub.WBS1 = @NewWBS1 and
				old.WBS1 = @OldWBS1 and
				PRSummarySub.WBS2 = @NewWBS2 and
				old.WBS2 = @OldWBS2

				Delete from PRSummarySub where WBS1 = @OldWBS1 and WBS2 = @OldWBS2
					and exists (select 'x' from PRSummarySub new where  new.WBS1 = @newWBS1 and new.WBS2 = @NewWBS2 and new.WBS3 = PRSummarySub.WBS3 and new.Period = PRSummarySub.Period)


			update PRSummaryMain set
				PRSummaryMain.Revenue = PRSummaryMain.Revenue + old.Revenue,
				PRSummaryMain.Billed = PRSummaryMain.Billed + old.Billed,
				PRSummaryMain.BilledLab = PRSummaryMain.BilledLab + old.BilledLab,
				PRSummaryMain.BilledCons = PRSummaryMain.BilledCons + old.BilledCons,
				PRSummaryMain.BilledExp = PRSummaryMain.BilledExp + old.BilledExp,
				PRSummaryMain.BilledFee = PRSummaryMain.BilledFee + old.BilledFee,
				PRSummaryMain.BilledUnit = PRSummaryMain.BilledUnit + old.BilledUnit,
				PRSummaryMain.BilledAddOn = PRSummaryMain.BilledAddOn + old.BilledAddOn,
				PRSummaryMain.BilledTaxes = PRSummaryMain.BilledTaxes + old.BilledTaxes,
				PRSummaryMain.BilledInterest = PRSummaryMain.BilledInterest + old.BilledInterest,
				PRSummaryMain.BilledOther = PRSummaryMain.BilledOther + old.BilledOther,
				PRSummaryMain.AR = PRSummaryMain.AR + old.AR,
				PRSummaryMain.Received = PRSummaryMain.Received + old.Received,
				PRSummaryMain.LaborCost = PRSummaryMain.LaborCost + old.LaborCost,
				PRSummaryMain.LaborBilling = PRSummaryMain.LaborBilling + old.LaborBilling,
				PRSummaryMain.Unbilled = PRSummaryMain.Unbilled + old.Unbilled,
				PRSummaryMain.SpentCostLessOH = PRSummaryMain.SpentCostLessOH + old.SpentCostLessOH,
				PRSummaryMain.SpentBilling = PRSummaryMain.SpentBilling + old.SpentBilling,
				PRSummaryMain.GrossMargin = PRSummaryMain.GrossMargin + old.GrossMargin,
				PRSummaryMain.NetRevenueCost = PRSummaryMain.NetRevenueCost + old.NetRevenueCost,
				PRSummaryMain.NetRevenueBilling = PRSummaryMain.NetRevenueBilling + old.NetRevenueBilling,
				PRSummaryMain.ProfitCostLessOH = PRSummaryMain.ProfitCostLessOH + old.ProfitCostLessOH,
				PRSummaryMain.ProfitBilling = PRSummaryMain.ProfitBilling + old.ProfitBilling,
				PRSummaryMain.Overhead = PRSummaryMain.Overhead + old.Overhead,
				PRSummaryMain.RevenueBillingCurrency = PRSummaryMain.RevenueBillingCurrency + old.RevenueBillingCurrency,
				PRSummaryMain.RevenueProjectCurrency = PRSummaryMain.RevenueProjectCurrency + old.RevenueProjectCurrency,
				PRSummaryMain.ARProjectCurrency = PRSummaryMain.ARProjectCurrency + old.ARProjectCurrency,
				PRSummaryMain.ARBillingCurrency = PRSummaryMain.ARBillingCurrency + old.ARBillingCurrency,
				PRSummaryMain.ReceivedProjectCurrency = PRSummaryMain.ReceivedProjectCurrency + old.ReceivedProjectCurrency,
				PRSummaryMain.ReceivedBillingCurrency = PRSummaryMain.ReceivedBillingCurrency + old.ReceivedBillingCurrency,
				PRSummaryMain.LaborCostProjectCurrency = PRSummaryMain.LaborCostProjectCurrency + old.LaborCostProjectCurrency,
				PRSummaryMain.LaborCostBillingCurrency = PRSummaryMain.LaborCostBillingCurrency + old.LaborCostBillingCurrency,
				PRSummaryMain.SpentCostLessOHProjectCurrency = PRSummaryMain.SpentCostLessOHProjectCurrency + old.SpentCostLessOHProjectCurrency,
				PRSummaryMain.SpentCostLessOHBillingCurrency = PRSummaryMain.SpentCostLessOHBillingCurrency + old.SpentCostLessOHBillingCurrency,
				PRSummaryMain.GrossMarginProjectCurrency = PRSummaryMain.GrossMarginProjectCurrency + old.GrossMarginProjectCurrency,
				PRSummaryMain.GrossMarginBillingCurrency = PRSummaryMain.GrossMarginBillingCurrency + old.GrossMarginBillingCurrency,
				PRSummaryMain.NetRevenueCostProjectCurrency = PRSummaryMain.NetRevenueCostProjectCurrency + old.NetRevenueCostProjectCurrency,
				PRSummaryMain.NetRevenueCostBillingCurrency = PRSummaryMain.NetRevenueCostBillingCurrency + old.NetRevenueCostBillingCurrency,
				PRSummaryMain.ProfitCostLessOHProjectCurrency = PRSummaryMain.ProfitCostLessOHProjectCurrency + old.ProfitCostLessOHProjectCurrency,
				PRSummaryMain.ProfitCostLessOHBillingCurrency = PRSummaryMain.ProfitCostLessOHBillingCurrency + old.ProfitCostLessOHBillingCurrency,
				PRSummaryMain.OverheadProjectCurrency = PRSummaryMain.OverheadProjectCurrency + old.OverheadProjectCurrency

			from PRSummaryMain old inner join PRSummaryMain on 
				(PRSummaryMain.WBS3 = old.WBS3 and PRSummaryMain.Period = old.Period)
			where
				PRSummaryMain.WBS1 = @NewWBS1 and
				old.WBS1 = @OldWBS1 and
				PRSummaryMain.WBS2 = @NewWBS2 and
				old.WBS2 = @OldWBS2

				Delete from PRSummaryMain where WBS1 = @OldWBS1 and WBS2 = @OldWBS2
					and exists (select 'x' from PRSummaryMain new where  new.WBS1 = @newWBS1 and new.WBS2 = @NewWBS2 and new.WBS3 = PRSummaryMain.WBS3 and new.Period = PRSummaryMain.Period)

		Delete from billInvMaster where BillWBS1 = @OldWBS1 and BillWBS2 = @OldWBS2 
			and exists (select 'x' from billInvMaster new where  new.BillWBS1 = @newWBS1 and new.BillWBS2 = @NewWBS2 and new.BillWBS3 = billInvMaster.BillWBS3 and new.Invoice = billInvMaster.Invoice)

         Delete from BTA where WBS1 = @OldWBS1 and WBS2 = @OldWBS2
         Delete from BTF where WBS1 = @OldWBS1 and WBS2 = @OldWBS2
         Delete from BTSchedule where WBS1 = @OldWBS1 and WBS2 = @OldWBS2
         Delete from BTTaxCodes where WBS1 = @OldWBS1 and WBS2 = @OldWBS2
         Delete from BT where WBS1 = @OldWBS1 and WBS2 = @OldWBS2

			delete from billINMaster where billINMaster.WBS1 = @OldWBS1 and billINMaster.WBS2 = @OldWBS2 and
			exists (select 'x' from billINMaster new where new.mainWBS1 = billINMaster.mainWBS1 and new.WBS1 = @NewWBS1 and new.wbs2 = @NewWBS2 and new.wbs3 = billINMaster.wbs3 and new.Invoice = billINMaster.Invoice)

			delete from billINDetail where billINDetail.WBS1 = @OldWBS1 and billINDetail.WBS2 = @OldWBS2 and
			exists (select 'x' from billINDetail new where new.mainWBS1 = billINDetail.mainWBS1 and new.WBS1 = @NewWBS1 and new.wbs2 = @NewWBS2 and new.wbs3 = billINDetail.wbs3 and new.Invoice = billINDetail.Invoice and new.PKey = billINDetail.pKey)

/*new for 6.2*/
		Delete FROM PRAdditionalData WHERE PRAdditionalData.WBS1 = @OldWBS1 and PRAdditionalData.WBS2 = @OldWBS2 and
			exists (select 'x' from PRAdditionalData new where new.wbs1 = @NewWBS1 
				and new.wbs2 = @NewWBS2 
				and new.wbs3 = PRAdditionalData.wbs3)
/*end 6.2*/

/*new for 7.4*/
		Delete from ARPreInvoice where ARPreInvoice.WBS1 = @OldWBS1 and ARPreInvoice.WBS2 = @OldWBS2 and 
			exists (select 'x' from ARPreInvoice new 
				where new.WBS1 = @NewWBS1 and new.WBS2 =@NewWBS2 and new.WBS3 = ARPreInvoice.WBS3 
				and new.PreInvoice = ARPreInvoice.PreInvoice)
		Delete from ARPreInvoiceDetail where ARPreInvoiceDetail.WBS1 = @OldWBS1 and ARPreInvoiceDetail.WBS2 = @OldWBS2 and 
			exists (select 'x' from ARPreInvoiceDetail new 
				where new.WBS1 = @NewWBS1 and new.WBS2 =@NewWBS2 and new.WBS3 = ARPreInvoiceDetail.WBS3 
				and new.PreInvoice = ARPreInvoiceDetail.PreInvoice and new.Seq = ARPreInvoiceDetail.Seq)
		Delete from BTFPhaseGroupData where BTFPhaseGroupData.WBS1 = @OldWBS1 and BTFPhaseGroupData.WBS2 = @OldWBS2 and
			exists (select 'x' from BTFPhaseGroupData new 
				where new.WBS1 = @NewWBS1 and new.WBS2 =@NewWBS2 and new.WBS3 = BTFPhaseGroupData.WBS3 
				and new.Seq = BTFPhaseGroupData.Seq and new.PhaseGroup = BTFPhaseGroupData.PhaseGroup)
		Delete from BTFPhaseGroupDescriptions where BTFPhaseGroupDescriptions.WBS1 = @OldWBS1 and BTFPhaseGroupDescriptions.WBS2 = @OldWBS2 and 
			exists (select 'x' from BTFPhaseGroupDescriptions new 
				where new.WBS1 = @NewWBS1 and new.WBS2 =@NewWBS2 and new.WBS3 = BTFPhaseGroupDescriptions.WBS3 
				and new.Seq = BTFPhaseGroupDescriptions.Seq and new.PhaseGroup = BTFPhaseGroupDescriptions.PhaseGroup
				and new.UICultureName = BTFPhaseGroupDescriptions.UICultureName)
/*end 7.4*/


--Updating paid period to most recent
			Update AR
				set PaidPeriod = old.PaidPeriod
			from AR old
			where
				WBS1 = @NewWBS1 and
				old.WBS1 = @OldWBS1 and
				WBS2 = @NewWBS2 and
				old.WBS2 = @OldWBS2 and				
				WBS3 = old.WBS3 and
   			Invoice = old.Invoice and
				old.PaidPeriod > PaidPeriod

			Delete from AR where AR.WBS1 = @OldWBS1 and AR.WBS2 = @OldWBS2 and 
				exists (select 'x' from AR new 
					where new.WBS1 = @NewWBS1 and new.WBS2 =@NewWBS2 and new.WBS3 = AR.WBS3 and new.Invoice = AR.invoice)

-- Add by DP Europa
			--declare @SyncProjToContractFees Nvarchar(1)
			--select @SyncProjToContractFees = SyncProjToContractFees from FW_CFGSystem
			--if (@SyncProjToContractFees = 'Y') 
			--	Begin
					update PR 
					set PR.Fee = PR.Fee + old.Fee,
						PR.ReimbAllow = PR.ReimbAllow + old.ReimbAllow,
						PR.ConsultFee = PR.ConsultFee + old.ConsultFee,
						PR.FeeBillingCurrency = PR.FeeBillingCurrency + old.FeeBillingCurrency,
						PR.ReimbAllowBillingCurrency = PR.ReimbAllowBillingCurrency + old.ReimbAllowBillingCurrency,
						PR.ConsultFeeBillingCurrency = PR.ConsultFeeBillingCurrency + old.ConsultFeeBillingCurrency,
						PR.FeeFunctionalCurrency = PR.FeeFunctionalCurrency + old.FeeFunctionalCurrency,
						PR.ReimbAllowFunctionalCurrency = PR.ReimbAllowFunctionalCurrency + old.ReimbAllowFunctionalCurrency,
						PR.ConsultFeeFunctionalCurrency = PR.ConsultFeeFunctionalCurrency + old.ConsultFeeFunctionalCurrency,
						PR.FeeDirLab = PR.FeeDirLab + old.FeeDirLab,
						PR.FeeDirExp = PR.FeeDirExp + old.FeeDirExp,
						PR.ReimbAllowExp = PR.ReimbAllowExp + old.ReimbAllowExp,
						PR.ReimbAllowCons = PR.ReimbAllowCons + old.ReimbAllowCons,
						PR.FeeDirLabBillingCurrency = PR.FeeDirLabBillingCurrency + old.FeeDirLabBillingCurrency,
						PR.FeeDirExpBillingCurrency = PR.FeeDirExpBillingCurrency + old.FeeDirExpBillingCurrency,
						PR.ReimbAllowExpBillingCurrency = PR.ReimbAllowExpBillingCurrency + old.ReimbAllowExpBillingCurrency,
						PR.ReimbAllowConsBillingCurrency = PR.ReimbAllowConsBillingCurrency + old.ReimbAllowConsBillingCurrency,
						PR.FeeDirLabFunctionalCurrency = PR.FeeDirLabFunctionalCurrency + old.FeeDirLabFunctionalCurrency,
						PR.FeeDirExpFunctionalCurrency = PR.FeeDirExpFunctionalCurrency + old.FeeDirExpFunctionalCurrency,
						PR.ReimbAllowExpFunctionalCurrency = PR.ReimbAllowExpFunctionalCurrency + old.ReimbAllowExpFunctionalCurrency,
						PR.ReimbAllowConsFunctionalCurrency = PR.ReimbAllowConsFunctionalCurrency + old.ReimbAllowConsFunctionalCurrency
					from PR old inner join PR on (PR.WBS3 = old.WBS3)
					where
						PR.WBS1 = @NewWBS1 and
						old.wbs1 = @OldWBS1 and
						(PR.WBS2 = @NewWBS2 or PR.WBS2=' ') and
						old.WBS2 = @OldWBS2
		
					update PR 
					set PR.Fee = PR.Fee - old.Fee,
						PR.ReimbAllow = PR.ReimbAllow - old.ReimbAllow,
						PR.ConsultFee = PR.ConsultFee - old.ConsultFee,
						PR.FeeBillingCurrency = PR.FeeBillingCurrency - old.FeeBillingCurrency,
						PR.ReimbAllowBillingCurrency = PR.ReimbAllowBillingCurrency - old.ReimbAllowBillingCurrency,
						PR.ConsultFeeBillingCurrency = PR.ConsultFeeBillingCurrency - old.ConsultFeeBillingCurrency,
						PR.FeeFunctionalCurrency = PR.FeeFunctionalCurrency - old.FeeFunctionalCurrency,
						PR.ReimbAllowFunctionalCurrency = PR.ReimbAllowFunctionalCurrency - old.ReimbAllowFunctionalCurrency,
						PR.ConsultFeeFunctionalCurrency = PR.ConsultFeeFunctionalCurrency - old.ConsultFeeFunctionalCurrency,
						PR.FeeDirLab = PR.FeeDirLab - old.FeeDirLab,
						PR.FeeDirExp = PR.FeeDirExp - old.FeeDirExp,
						PR.ReimbAllowExp = PR.ReimbAllowExp - old.ReimbAllowExp,
						PR.ReimbAllowCons = PR.ReimbAllowCons - old.ReimbAllowCons,
						PR.FeeDirLabBillingCurrency = PR.FeeDirLabBillingCurrency - old.FeeDirLabBillingCurrency,
						PR.FeeDirExpBillingCurrency = PR.FeeDirExpBillingCurrency - old.FeeDirExpBillingCurrency,
						PR.ReimbAllowExpBillingCurrency = PR.ReimbAllowExpBillingCurrency - old.ReimbAllowExpBillingCurrency,
						PR.ReimbAllowConsBillingCurrency = PR.ReimbAllowConsBillingCurrency - old.ReimbAllowConsBillingCurrency,
						PR.FeeDirLabFunctionalCurrency = PR.FeeDirLabFunctionalCurrency - old.FeeDirLabFunctionalCurrency,
						PR.FeeDirExpFunctionalCurrency = PR.FeeDirExpFunctionalCurrency - old.FeeDirExpFunctionalCurrency,
						PR.ReimbAllowExpFunctionalCurrency = PR.ReimbAllowExpFunctionalCurrency - old.ReimbAllowExpFunctionalCurrency,
						PR.ReimbAllowConsFunctionalCurrency = PR.ReimbAllowConsFunctionalCurrency - old.ReimbAllowConsFunctionalCurrency
					from PR old inner join PR on 1=1
					where
						PR.WBS1 = @OldWBS1 and
						old.wbs1 = @OldWBS1 and
						(PR.WBS2 = @OldWBS2 or PR.WBS2=' ') and
						old.WBS2 = @OldWBS2 and
						PR.WBS3 = ' ' and old.WBS3 = ' '
				--End

			Update PNTask
			set PNTask.CompensationFee = PNTask.CompensationFee + old.CompensationFee,
				PNTask.ConsultantFee = PNTask.ConsultantFee + old.ConsultantFee,
				PNTask.ReimbAllowance = PNTask.ReimbAllowance + old.ReimbAllowance, 
				PNTask.CompensationFeeBill = PNTask.CompensationFeeBill + old.CompensationFeeBill, 
				PNTask.ConsultantFeeBill = PNTask.ConsultantFeeBill + old.ConsultantFeeBill,
				PNTask.ReimbAllowanceBill = PNTask.ReimbAllowanceBill + old.ReimbAllowanceBill,
				PNTask.CompensationFeeDirLab = PNTask.CompensationFeeDirLab + old.CompensationFeeDirLab, 
				PNTask.CompensationFeeDirExp = PNTask.CompensationFeeDirExp + old.CompensationFeeDirExp, 
				PNTask.ReimbAllowanceExp = PNTask.ReimbAllowanceExp + old.ReimbAllowanceExp,
				PNTask.ReimbAllowanceCon = PNTask.ReimbAllowanceCon + old.ReimbAllowanceCon,
				PNTask.CompensationFeeDirLabBill = PNTask.CompensationFeeDirLabBill + old.CompensationFeeDirLabBill, 
				PNTask.CompensationFeeDirExpBill = PNTask.CompensationFeeDirExpBill + old.CompensationFeeDirExpBill,
				PNTask.ReimbAllowanceExpBill = PNTask.ReimbAllowanceExpBill + old.ReimbAllowanceExpBill,
				PNTask.ReimbAllowanceConBill = PNTask.ReimbAllowanceConBill + old.ReimbAllowanceConBill
			from PNTask old inner join PNTask on (isnull(PNTask.WBS3, ' ') = isnull(old.WBS3, ' '))
			where
				PNTask.WBS1 = @NewWBS1 and
				old.wbs1 = @OldWBS1 and
				(PNTask.WBS2 = @NewWBS2 or isnull(PNTask.WBS2, ' ')=' ') and
				old.WBS2 = @OldWBS2

			Update RPTask
			set RPTask.CompensationFee = RPTask.CompensationFee + old.CompensationFee,
				RPTask.ConsultantFee = RPTask.ConsultantFee + old.ConsultantFee,
				RPTask.ReimbAllowance = RPTask.ReimbAllowance + old.ReimbAllowance, 
				RPTask.CompensationFeeBill = RPTask.CompensationFeeBill + old.CompensationFeeBill, 
				RPTask.ConsultantFeeBill = RPTask.ConsultantFeeBill + old.ConsultantFeeBill,
				RPTask.ReimbAllowanceBill = RPTask.ReimbAllowanceBill + old.ReimbAllowanceBill,
				RPTask.CompensationFeeDirLab = RPTask.CompensationFeeDirLab + old.CompensationFeeDirLab, 
				RPTask.CompensationFeeDirExp = RPTask.CompensationFeeDirExp + old.CompensationFeeDirExp, 
				RPTask.ReimbAllowanceExp = RPTask.ReimbAllowanceExp + old.ReimbAllowanceExp,
				RPTask.ReimbAllowanceCon = RPTask.ReimbAllowanceCon + old.ReimbAllowanceCon,
				RPTask.CompensationFeeDirLabBill = RPTask.CompensationFeeDirLabBill + old.CompensationFeeDirLabBill, 
				RPTask.CompensationFeeDirExpBill = RPTask.CompensationFeeDirExpBill + old.CompensationFeeDirExpBill,
				RPTask.ReimbAllowanceExpBill = RPTask.ReimbAllowanceExpBill + old.ReimbAllowanceExpBill,
				RPTask.ReimbAllowanceConBill = RPTask.ReimbAllowanceConBill + old.ReimbAllowanceConBill
			from RPTask old inner join RPTask on (isnull(RPTask.WBS3, ' ') = isnull(old.WBS3, ' '))
			where
				RPTask.WBS1 = @NewWBS1 and
				old.wbs1 = @OldWBS1 and
				(RPTask.WBS2 = @NewWBS2 or isnull(RPTask.WBS2, ' ')=' ') and
				old.WBS2 = @OldWBS2

-- Add by DP 4.0
			select * into #TempKeyConvert2 from PR where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and 
			not exists (select 'x' from pr new where new.WBS1 = @NewWBS1 and new.wbs2 = @NewWBS2 and new.wbs3 = pr.wbs3)
			Update #TempKeyConvert2 set WBS1=@NewWBS1, WBS2=@NewWBS2
			Insert PR select * from #TempKeyConvert2
			Drop Table #TempKeyConvert2

			select * into #TempKeyConvertBT2 from BT where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and 
			not exists (select 'x' from BT new where new.WBS1 = @NewWBS1 and new.wbs2 = @NewWBS2 and new.wbs3 = BT.wbs3)
			Update #TempKeyConvertBT2 set WBS1=@NewWBS1, WBS2=@NewWBS2
			Insert BT select * from #TempKeyConvertBT2
			Drop Table #TempKeyConvertBT2
			

--Reset Contracts.FeeIncludeInd to 'N'
			Update Contracts set FeeIncludeInd='N'
			from Contracts C 
			Inner join ContractDetails CD on CD.WBS1=C.WBS1 and CD.ContractNumber=C.ContractNumber 
			Where CD.WBS1=@OldWBS1 and CD.WBS2=@OldWBS2 and C.FeeIncludeInd='Y'

			Update Contracts set FeeIncludeInd='N'
			from Contracts C 
			Inner join ContractDetails CD on CD.WBS1=C.WBS1 and CD.ContractNumber=C.ContractNumber 
			Where CD.WBS1=@NewWBS1 and CD.WBS2=@NewWBS2 and C.FeeIncludeInd='Y'
--
-- RonK 07/15/09: Added Contracts for Rubik--update for merged WBS1s.		
			-- ContractDetails Records:   ********************************
			-- First update non-duplicate ContractDetails to the target WBS2.
			Update ContractDetails Set WBS1 = @NewWBS1, WBS2 = @NewWBS2 
				where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and not exists 
				(select 'x' from ContractDetails new where new.wbs1 = @NewWBS1 and new.WBS2 = @NewWBS2 
					and new.ContractNumber = ContractDetails.ContractNumber and new.wbs3 = ContractDetails.wbs3)
													  
			-- Next update ContractDetails records duplicated in the Target WBS2.
			Update ContractDetails
			set ContractDetails.Fee = ContractDetails.Fee + old.Fee,
				ContractDetails.ReimbAllow = ContractDetails.ReimbAllow + old.ReimbAllow,
				ContractDetails.ConsultFee = ContractDetails.ConsultFee + old.ConsultFee, 
				ContractDetails.Total = ContractDetails.Total + old.Total, 
				ContractDetails.FeeBillingCurrency = ContractDetails.FeeBillingCurrency + old.FeeBillingCurrency,
				ContractDetails.ReimbAllowBillingCurrency = ContractDetails.ReimbAllowBillingCurrency + old.ReimbAllowBillingCurrency,
				ContractDetails.ConsultFeeBillingCurrency = ContractDetails.ConsultFeeBillingCurrency + old.ConsultFeeBillingCurrency, 
				ContractDetails.TotalBillingCurrency = ContractDetails.TotalBillingCurrency + old.TotalBillingCurrency, 
				ContractDetails.FeeFunctionalCurrency = ContractDetails.FeeFunctionalCurrency + old.FeeFunctionalCurrency,
				ContractDetails.ReimbAllowFunctionalCurrency = ContractDetails.ReimbAllowFunctionalCurrency + old.ReimbAllowFunctionalCurrency,
				ContractDetails.ConsultFeeFunctionalCurrency = ContractDetails.ConsultFeeFunctionalCurrency + old.ConsultFeeFunctionalCurrency, 
				ContractDetails.TotalFunctionalCurrency = ContractDetails.TotalFunctionalCurrency + old.TotalFunctionalCurrency,
				ContractDetails.FeeDirLab = ContractDetails.FeeDirLab + old.FeeDirLab,
				ContractDetails.FeeDirExp = ContractDetails.FeeDirExp + old.FeeDirExp,
				ContractDetails.ReimbAllowExp = ContractDetails.ReimbAllowExp + old.ReimbAllowExp,
				ContractDetails.ReimbAllowCons = ContractDetails.ReimbAllowCons + old.ReimbAllowCons,
				ContractDetails.FeeDirLabBillingCurrency = ContractDetails.FeeDirLabBillingCurrency + old.FeeDirLabBillingCurrency,
				ContractDetails.FeeDirExpBillingCurrency = ContractDetails.FeeDirExpBillingCurrency + old.FeeDirExpBillingCurrency,
				ContractDetails.ReimbAllowExpBillingCurrency = ContractDetails.ReimbAllowExpBillingCurrency + old.ReimbAllowExpBillingCurrency,
				ContractDetails.ReimbAllowConsBillingCurrency = ContractDetails.ReimbAllowConsBillingCurrency + old.ReimbAllowConsBillingCurrency,
				ContractDetails.FeeDirLabFunctionalCurrency = ContractDetails.FeeDirLabFunctionalCurrency + old.FeeDirLabFunctionalCurrency,
				ContractDetails.FeeDirExpFunctionalCurrency = ContractDetails.FeeDirExpFunctionalCurrency + old.FeeDirExpFunctionalCurrency,
				ContractDetails.ReimbAllowExpFunctionalCurrency = ContractDetails.ReimbAllowExpFunctionalCurrency + old.ReimbAllowExpFunctionalCurrency,
				ContractDetails.ReimbAllowConsFunctionalCurrency = ContractDetails.ReimbAllowConsFunctionalCurrency + old.ReimbAllowConsFunctionalCurrency
			from ContractDetails old inner join ContractDetails on (ContractDetails.ContractNumber = old.ContractNumber 
																		and ContractDetails.WBS3 = old.WBS3)
			where ContractDetails.WBS1 = @NewWBS1 and (ContractDetails.WBS2 = @NewWBS2 or isnull(ContractDetails.WBS2, ' ')=' ') 
						  and old.WBS1 = @OldWBS1 and old.WBS2 = @OldWBS2 

			Update ContractDetails
			set ContractDetails.Fee = ContractDetails.Fee - old.Fee,
				ContractDetails.ReimbAllow = ContractDetails.ReimbAllow - old.ReimbAllow,
				ContractDetails.ConsultFee = ContractDetails.ConsultFee - old.ConsultFee, 
				ContractDetails.Total = ContractDetails.Total - old.Total, 
				ContractDetails.FeeBillingCurrency = ContractDetails.FeeBillingCurrency - old.FeeBillingCurrency,
				ContractDetails.ReimbAllowBillingCurrency = ContractDetails.ReimbAllowBillingCurrency - old.ReimbAllowBillingCurrency,
				ContractDetails.ConsultFeeBillingCurrency = ContractDetails.ConsultFeeBillingCurrency - old.ConsultFeeBillingCurrency, 
				ContractDetails.TotalBillingCurrency = ContractDetails.TotalBillingCurrency - old.TotalBillingCurrency, 
				ContractDetails.FeeFunctionalCurrency = ContractDetails.FeeFunctionalCurrency - old.FeeFunctionalCurrency,
				ContractDetails.ReimbAllowFunctionalCurrency = ContractDetails.ReimbAllowFunctionalCurrency - old.ReimbAllowFunctionalCurrency,
				ContractDetails.ConsultFeeFunctionalCurrency = ContractDetails.ConsultFeeFunctionalCurrency - old.ConsultFeeFunctionalCurrency, 
				ContractDetails.TotalFunctionalCurrency = ContractDetails.TotalFunctionalCurrency - old.TotalFunctionalCurrency,
				ContractDetails.FeeDirLab = ContractDetails.FeeDirLab - old.FeeDirLab,
				ContractDetails.FeeDirExp = ContractDetails.FeeDirExp - old.FeeDirExp,
				ContractDetails.ReimbAllowExp = ContractDetails.ReimbAllowExp - old.ReimbAllowExp,
				ContractDetails.ReimbAllowCons = ContractDetails.ReimbAllowCons - old.ReimbAllowCons,
				ContractDetails.FeeDirLabBillingCurrency = ContractDetails.FeeDirLabBillingCurrency - old.FeeDirLabBillingCurrency,
				ContractDetails.FeeDirExpBillingCurrency = ContractDetails.FeeDirExpBillingCurrency - old.FeeDirExpBillingCurrency,
				ContractDetails.ReimbAllowExpBillingCurrency = ContractDetails.ReimbAllowExpBillingCurrency - old.ReimbAllowExpBillingCurrency,
				ContractDetails.ReimbAllowConsBillingCurrency = ContractDetails.ReimbAllowConsBillingCurrency - old.ReimbAllowConsBillingCurrency,
				ContractDetails.FeeDirLabFunctionalCurrency = ContractDetails.FeeDirLabFunctionalCurrency - old.FeeDirLabFunctionalCurrency,
				ContractDetails.FeeDirExpFunctionalCurrency = ContractDetails.FeeDirExpFunctionalCurrency - old.FeeDirExpFunctionalCurrency,
				ContractDetails.ReimbAllowExpFunctionalCurrency = ContractDetails.ReimbAllowExpFunctionalCurrency - old.ReimbAllowExpFunctionalCurrency,
				ContractDetails.ReimbAllowConsFunctionalCurrency = ContractDetails.ReimbAllowConsFunctionalCurrency - old.ReimbAllowConsFunctionalCurrency
			from ContractDetails old inner join ContractDetails on ContractDetails.ContractNumber = old.ContractNumber 
			where ContractDetails.WBS1 = @OldWBS1 and (ContractDetails.WBS2 = @OldWBS2 or isnull(ContractDetails.WBS2, ' ')=' ') 
						and (ContractDetails.WBS3 = ' ' or isnull(ContractDetails.WBS3, ' ')=' ')
						and old.WBS1 = @OldWBS1 and old.WBS2 = @OldWBS2 
			
			-- Next remove old ContractDetail records now merged. (Shouldn't be any others left on the old WBS1/2 IDs)
        	Delete FROM ContractDetails WHERE WBS1 = @OldWBS1 and WBS2 = @OldWBS2 
									  				  
-- RonK 07/15/09: End Mod
				
--
		END --Existing
	else 
		Begin
-- Add by DP 4.0
			select * into #TempKeyConvert from PR where WBS1 = @OldWBS1 and WBS2 = @OldWBS2
			Update #TempKeyConvert set WBS1=@NewWBS1, WBS2=@NewWBS2
			Insert PR select * from #TempKeyConvert
			Drop Table #TempKeyConvert
			set @NewName2 = @OldName2

			select * into #TempKeyConvertBT from BT where WBS1 = @OldWBS1 and WBS2 = @OldWBS2
			Update #TempKeyConvertBT set WBS1=@NewWBS1, WBS2=@NewWBS2
			Insert BT select * from #TempKeyConvertBT
			Drop Table #TempKeyConvertBT
			
--
		end



-- Remove the Vision plan of the Nav plan
		if (@OldPlanID > '' and @NPlanSourceExist='Y')
		begin
if (@diag <> 0)	print 'Execute dbo.PMDelPlan @OldPlanID'					 
			Execute dbo.PMDelPlan @OldPlanID
		end
		if (@NewPlanID > '' and @NPlanTargetExist='Y')
		begin
if (@diag <> 0)	print 'Execute dbo.PMDelPlan @NewPlanID'					 
			Execute dbo.PMDelPlan @NewPlanID
		end

-- Check out plans
		if (@VPlanSourceExist = 'Y')
			Update RPPlan Set CheckedOutDate=GETDATE(), CheckedOutUser=@UserName, CheckedOutID=@CheckedOutID
			From RPPlan Left Join RPTask on RPPlan.PlanID=RPTask.PlanID
			where RPTask.WBS1 = @OldWBS1 and RPTask.WBS2 = @OldWBS2

		if (@NPlanSourceExist = 'Y' or @NPlanTargetExist = 'Y')
			Update PNPlan Set CheckedOutDate=GETDATE(), CheckedOutUser=@UserName, CheckedOutID=@CheckedOutID
			From PNPlan Left Join PNTask on PNPlan.PlanID=PNTask.PlanID
			where (PNTask.WBS1 = @OldWBS1 and PNTask.WBS2 = @OldWBS2) or (PNTask.WBS1 = @NewWBS1 and PNTask.WBS2 = @NewWBS2)

-- Combine Navigator Plans Start
		if (@NPlanSourceExist = 'Y' and @NPlanTargetExist = 'Y')
			Begin
-- combine duplicate rsources in PNAssignment, PNExpense, PNConsultant
-- Move Planned and Basedline for the duplicate PNAssignment, Delete duplicate PNAssignment
				declare AssignmentCursor cursor for
					select TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, isnull(ResourceID,'') as ResourceID, isnull(GenericResourceID,'') as GenericResourceID, MIN(AssignmentID) as AssignmentID  
					from PNAssignment where PlanID=@OldPlanID and WBS1=@OldWBS1 and WBS2=@OldWBS2 and
					exists (select 'x' from PNAssignment new where PlanID=@NewPlanID and new.wbs1 = @NewWBS1 and new.wbs2 = @NewWBS2 
						and isnull(new.wbs3,'') = isnull(PNAssignment.wbs3,'')
						and isnull(new.LaborCode,'') = isnull(PNAssignment.LaborCode,'')
						and isnull(new.ResourceID,'') = isnull(PNAssignment.ResourceID,'')
						and isnull(new.GenericResourceID,'') = isnull(PNAssignment.GenericResourceID,''))
					group by TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(LaborCode,''), isnull(ResourceID,''), isnull(GenericResourceID,'')
				open AssignmentCursor
				fetch next from AssignmentCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @LaborCode, @ResourceID, @GenericResourceID, @OldAssignmentID
				set @TempFetch = @@Fetch_Status
				While (@TempFetch = 0)
				begin
					select @NewTaskID=TaskID, @NewAssignmentID=AssignmentID from PNAssignment 
						where PlanID=@NewPlanID and WBS1=@NewWBS1 and WBS2 = @NewWBS2 
						and isnull(WBS3,'')=@WBS3 
						and isnull(LaborCode,'')=@LaborCode 
						and isnull(ResourceID,'')=@ResourceID 
						and isnull(GenericResourceID,'')=@GenericResourceID 

					if (@UseBookingForEmpHours = 'Y')
						Update PNPlannedLabor Set HardBooked=@RPHardBooking 
						From PNPlannedLabor Left Join PNAssignment on PNPlannedLabor.AssignmentID=PNAssignment.AssignmentID
						Where PNPlannedLabor.AssignmentID=@OldAssignmentID and PNAssignment.ResourceID > '' and PNPlannedLabor.HardBooked<>@RPHardBooking 
					if (@UseBookingForGenHours = 'Y')
						Update PNPlannedLabor Set HardBooked=@RPHardBooking 
						From PNPlannedLabor Left Join PNAssignment on PNPlannedLabor.AssignmentID=PNAssignment.AssignmentID
						Where PNPlannedLabor.AssignmentID=@OldAssignmentID and PNAssignment.GenericResourceID > '' and PNPlannedLabor.HardBooked<>@RPHardBooking 

					Update PNPlannedLabor Set PlanID=@NewPlanID, TaskID=@NewTaskID, AssignmentID=@NewAssignmentID where AssignmentID=@OldAssignmentID
					Update PNBaselineLabor Set PlanID=@NewPlanID, TaskID=@NewTaskID, AssignmentID=@NewAssignmentID where AssignmentID=@OldAssignmentID


					delete PNAssignment Where TaskID=@OldTaskID and WBS1=@WBS1 and WBS2=@WBS2 
						and isnull(WBS3,'')=@WBS3 
						and isnull(LaborCode,'')=@LaborCode 
						and isnull(ResourceID,'')=@ResourceID
						and isnull(GenericResourceID,'')=@GenericResourceID 

					fetch next from AssignmentCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @LaborCode, @ResourceID, @GenericResourceID, @OldAssignmentID
					set @TempFetch = @@Fetch_Status
				end
				close AssignmentCursor
				deallocate AssignmentCursor

-- Move Planned and Basedline for the duplicate PNExpense, Delete duplicate PNExpense
				declare ExpenseCursor cursor for
					select TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Account,'') as Account, isnull(Vendor,'') as Vendor, MIN(ExpenseID) as ExpenseID  
					from PNExpense where PlanID=@OldPlanID and WBS1=@OldWBS1 and WBS2=@OldWBS2 and
					exists (select 'x' from PNExpense new where PlanID=@NewPlanID and new.wbs1 = @NewWBS1 and new.wbs2 = @NewWBS2 
						and isnull(new.wbs3,'') = isnull(PNExpense.wbs3,'')
						and isnull(new.Account,'') = isnull(PNExpense.Account,'')
						and isnull(new.Vendor,'') = isnull(PNExpense.Vendor,''))
					group by TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
				open ExpenseCursor
				fetch next from ExpenseCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldExpenseID
				set @TempFetch = @@Fetch_Status
				While (@TempFetch = 0)
				begin
					select @NewTaskID=TaskID, @NewExpenseID=ExpenseID from PNExpense 
						where PlanID=@NewPlanID and WBS1=@NewWBS1 and WBS2 = @NewWBS2 
						and isnull(WBS3,'')=@WBS3 
						and isnull(Account,'')=@Account 
						and isnull(Vendor,'')=@Vendor 

					Update PNPlannedExpenses Set PlanID=@NewPlanID, TaskID=@NewTaskID, ExpenseID=@NewExpenseID where ExpenseID=@OldExpenseID
					Update PNBaselineExpenses Set PlanID=@NewPlanID, TaskID=@NewTaskID, ExpenseID=@NewExpenseID where ExpenseID=@OldExpenseID

					delete PNExpense Where TaskID=@OldTaskID and WBS1=@WBS1 and WBS2=@WBS2 
						and isnull(WBS3,'')=@WBS3 
						and isnull(Account,'')=@Account
						and isnull(Vendor,'')=@Vendor 

					fetch next from ExpenseCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldExpenseID
					set @TempFetch = @@Fetch_Status
				end
				close ExpenseCursor
				deallocate ExpenseCursor

-- Move Planned and Basedline for the duplicate PNConsultant, Delete duplicate PNConsultant
				declare ConsultantCursor cursor for
					select TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Account,'') as Account, isnull(Vendor,'') as Vendor, MIN(ConsultantID) as ConsultantID  
					from PNConsultant where PlanID=@OldPlanID and WBS1=@OldWBS1 and WBS2=@OldWBS2 and
					exists (select 'x' from PNConsultant new where PlanID=@NewPlanID and new.wbs1 = @NewWBS1 and new.wbs2 = @NewWBS2 
						and isnull(new.wbs3,'') = isnull(PNConsultant.wbs3,'')
						and isnull(new.Account,'') = isnull(PNConsultant.Account,'')
						and isnull(new.Vendor,'') = isnull(PNConsultant.Vendor,''))
					group by TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
				open ConsultantCursor
				fetch next from ConsultantCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldConsultantID
				set @TempFetch = @@Fetch_Status
				While (@TempFetch = 0)
				begin
					select @NewTaskID=TaskID, @NewConsultantID=ConsultantID from PNConsultant 
						where PlanID=@NewPlanID and WBS1=@NewWBS1 and WBS2 = @NewWBS2 
						and isnull(WBS3,'')=@WBS3 
						and isnull(Account,'')=@Account 
						and isnull(Vendor,'')=@Vendor 

					Update PNPlannedConsultant Set PlanID=@NewPlanID, TaskID=@NewTaskID, ConsultantID=@NewConsultantID where ConsultantID=@OldConsultantID
					Update PNBaselineConsultant Set PlanID=@NewPlanID, TaskID=@NewTaskID, ConsultantID=@NewConsultantID where ConsultantID=@OldConsultantID

					delete PNConsultant Where TaskID=@OldTaskID and WBS1=@WBS1 and WBS2=@WBS2 
						and isnull(WBS3,'')=@WBS3 
						and isnull(Account,'')=@Account
						and isnull(Vendor,'')=@Vendor 

					fetch next from ConsultantCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldConsultantID
					set @TempFetch = @@Fetch_Status
				end
				close ConsultantCursor
				deallocate ConsultantCursor

-- Move Everything related for the duplicate PNTask
				declare TaskCursor cursor for
					select TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, isnull(WBSType,'') as WBSType 
					from PNTask where PlanID=@OldPlanID and WBS1=@OldWBS1 and WBS2=@OldWBS2 and
					exists (select 'x' from PNTask new where PlanID=@NewPlanID and new.wbs1 = @NewWBS1 and new.wbs2 = @NewWBS2 
						and isnull(new.wbs3,'') = isnull(PNTask.wbs3,'')
						and isnull(new.LaborCode,'') = isnull(PNTask.LaborCode,'')
						and isnull(new.WBSType,'') = isnull(PNTask.WBSType,''))
					group by TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(LaborCode,''), isnull(WBSType,'')
				open TaskCursor
				fetch next from TaskCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @LaborCode, @WBSType
				set @TempFetch = @@Fetch_Status
				While (@TempFetch = 0)
				begin
if (@diag = 1) Print 'Update all related pntask tables ' + @NewPlanID +'/'+ @NewWBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+@WBSType
					select @NewTaskID = TaskID from PNTask 
						where PlanID=@NewPlanID and WBS1=@NewWBS1 and WBS2=@NewWBS2 
						and isnull(WBS3,'')=@WBS3 
						and isnull(LaborCode,'')=@LaborCode 
						and isnull(WBSType,'')=@WBSType 

if (@diag = 1) Print 'Update PNPlannedLabor and PNBaselineLabor ' + @NewPlanID +'/'+ @NewWBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @ResourceID +'/'+@GenericResourceID
					if (@UseBookingForEmpHours = 'Y')
						Update PNPlannedLabor Set HardBooked=@RPHardBooking 
						From PNPlannedLabor Left Join PNAssignment on PNPlannedLabor.AssignmentID=PNAssignment.AssignmentID
						Where PNPlannedLabor.TaskID=@OldTaskID and PNAssignment.ResourceID > '' and PNPlannedLabor.HardBooked<>@RPHardBooking 
					if (@UseBookingForGenHours = 'Y')
						Update PNPlannedLabor Set HardBooked=@RPHardBooking 
						From PNPlannedLabor Left Join PNAssignment on PNPlannedLabor.AssignmentID=PNAssignment.AssignmentID
						Where PNPlannedLabor.TaskID=@OldTaskID and PNAssignment.GenericResourceID > '' and PNPlannedLabor.HardBooked<>@RPHardBooking 

					Update PNPlannedLabor Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
					Update PNPlannedExpenses Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
					Update PNPlannedConsultant Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID

					Update PNBaselineLabor Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
					Update PNBaselineExpenses Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
					Update PNBaselineConsultant Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID

					Update PNAssignment Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
					Update PNExpense Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
					Update PNConsultant Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID

					if (@OldAllocMethod = 'r' and @NewAllocMethod <> 'r')
						begin	--delete Old
							Delete PNPlannedRevenueLabor where TaskID=@OldTaskID
							Delete PNBaselineRevenueLabor where TaskID=@OldTaskID
							Delete RPPlannedRevenueLabor where TaskID=@OldTaskID
							Delete RPBaselineRevenueLabor where TaskID=@OldTaskID
						end
					if (@OldAllocMethod <> @NewAllocMethod)
						begin
							Update PR Set AllocMethod = @NewAllocMethod where WBS1 = @OldWBS1 and WBS2 = @OldWBS2
							Update PR Set AllocMethod = @NewAllocMethod where WBS1 = @NewWBS1 and WBS2 = @NewWBS2
						end
					if (@OldAllocMethod = 'r' and @NewAllocMethod = 'r')
						begin	--merge Old into new
							Update PNPlannedRevenueLabor Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
							Update PNBaselineRevenueLabor Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
							Update RPPlannedRevenueLabor Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
							Update RPBaselineRevenueLabor Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
						end

if (@diag = 1) Print 'delete PNTask Where TaskID=@OldTaskID and WBS1=@WBS1 ' + @OldTaskID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+@WBSType
					delete PNTask Where TaskID=@OldTaskID

					fetch next from TaskCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @LaborCode, @WBSType
					set @TempFetch = @@Fetch_Status
				end
				close TaskCursor
				deallocate TaskCursor

-- merge PNTask, PNAssignment, PNExpense, PNConsultant
				Update PNPlannedLabor Set PlanID=@NewPlanID from PNPlannedLabor 
				inner join PNAssignment on PNPlannedLabor.AssignmentID = PNAssignment.AssignmentID 
				Where PNPlannedLabor.PlanID=@OldPlanID and PNAssignment.WBS1 = @OldWBS1 and WBS2 = @OldWBS2
				Update PNPlannedExpenses Set PlanID=@NewPlanID from PNPlannedExpenses 
				inner join PNExpense on PNPlannedExpenses.ExpenseID = PNExpense.ExpenseID 
				Where PNPlannedExpenses.PlanID=@OldPlanID and PNExpense.WBS1 = @OldWBS1 and WBS2 = @OldWBS2
				Update PNPlannedConsultant Set PlanID=@NewPlanID from PNPlannedConsultant 
				inner join PNConsultant on PNPlannedConsultant.ConsultantID = PNConsultant.ConsultantID 
				Where PNPlannedConsultant.PlanID=@OldPlanID and PNConsultant.WBS1 = @OldWBS1 and WBS2 = @OldWBS2

				Update PNBaselineLabor Set PlanID=@NewPlanID from PNBaselineLabor 
				inner join PNAssignment on PNBaselineLabor.AssignmentID = PNAssignment.AssignmentID 
				Where PNBaselineLabor.PlanID=@OldPlanID and PNAssignment.WBS1 = @OldWBS1 and WBS2 = @OldWBS2
				Update PNBaselineExpenses Set PlanID=@NewPlanID from PNBaselineExpenses 
				inner join PNExpense on PNBaselineExpenses.ExpenseID = PNExpense.ExpenseID 
				Where PNBaselineExpenses.PlanID=@OldPlanID and PNExpense.WBS1 = @OldWBS1 and WBS2 = @OldWBS2
				Update PNBaselineConsultant Set PlanID=@NewPlanID from PNBaselineConsultant 
				inner join PNConsultant on PNBaselineConsultant.ConsultantID = PNConsultant.ConsultantID 
				Where PNBaselineConsultant.PlanID=@OldPlanID and PNConsultant.WBS1 = @OldWBS1 and WBS2 = @OldWBS2

				if (@OldAllocMethod = 'r' and @NewAllocMethod <> 'r')
					begin	--delete Old
						Delete PNPlannedRevenueLabor From PNPlannedRevenueLabor Inner Join PNTask on PNPlannedRevenueLabor.TaskID=PNTask.TaskID 
						where PNTask.PlanID=@OldPlanID and PNTask.WBS1 = @OldWBS1 and isnull(PNTask.WBS2,'')=@OldWBS2
						Delete PNBaselineRevenueLabor From PNBaselineRevenueLabor Inner Join PNTask on PNBaselineRevenueLabor.TaskID=PNTask.TaskID 
						where PNTask.PlanID=@OldPlanID and PNTask.WBS1 = @OldWBS1 and isnull(PNTask.WBS2,'')=@OldWBS2
						Delete RPPlannedRevenueLabor From RPPlannedRevenueLabor Inner Join PNTask on RPPlannedRevenueLabor.TaskID=PNTask.TaskID 
						where PNTask.PlanID=@OldPlanID and PNTask.WBS1 = @OldWBS1 and isnull(PNTask.WBS2,'')=@OldWBS2
						Delete RPBaselineRevenueLabor From RPBaselineRevenueLabor Inner Join PNTask on RPBaselineRevenueLabor.TaskID=PNTask.TaskID 
						where PNTask.PlanID=@OldPlanID and PNTask.WBS1 = @OldWBS1 and isnull(PNTask.WBS2,'')=@OldWBS2
					end
				if (@OldAllocMethod <> @NewAllocMethod)
					begin
						Update PR Set AllocMethod = @NewAllocMethod where WBS1 = @OldWBS1 and WBS2 = @OldWBS2
						Update PR Set AllocMethod = @NewAllocMethod where WBS1 = @NewWBS1 and WBS2 = @NewWBS2
					end
				if (@OldAllocMethod = 'r' and @NewAllocMethod = 'r')
					begin	--merge Old into new
						Update PNPlannedRevenueLabor Set PlanID=@NewPlanID From PNPlannedRevenueLabor Inner Join PNTask on PNPlannedRevenueLabor.TaskID=PNTask.TaskID 
						where PNTask.PlanID=@OldPlanID and PNTask.WBS1 = @OldWBS1 and isnull(PNTask.WBS2,'')=@OldWBS2
						Update PNBaselineRevenueLabor Set PlanID=@NewPlanID From PNBaselineRevenueLabor Inner Join PNTask on PNBaselineRevenueLabor.TaskID=PNTask.TaskID 
						where PNTask.PlanID=@OldPlanID and PNTask.WBS1 = @OldWBS1 and isnull(PNTask.WBS2,'')=@OldWBS2
						Update RPPlannedRevenueLabor Set PlanID=@NewPlanID From RPPlannedRevenueLabor Inner Join PNTask on RPPlannedRevenueLabor.TaskID=PNTask.TaskID 
						where PNTask.PlanID=@OldPlanID and PNTask.WBS1 = @OldWBS1 and isnull(PNTask.WBS2,'')=@OldWBS2
						Update RPBaselineRevenueLabor Set PlanID=@NewPlanID From RPBaselineRevenueLabor Inner Join PNTask on RPBaselineRevenueLabor.TaskID=PNTask.TaskID 
						where PNTask.PlanID=@OldPlanID and PNTask.WBS1 = @OldWBS1 and isnull(PNTask.WBS2,'')=@OldWBS2
					end

				Update PNTask Set PlanID=@NewPlanID, WBS1=@NewWBS1, WBS2=@NewWBS2 Where PlanID=@OldPlanID and PNTask.WBS1 = @OldWBS1 and WBS2 = @OldWBS2
				Update PNAssignment Set PlanID=@NewPlanID, WBS1=@NewWBS1, WBS2=@NewWBS2 Where PlanID=@OldPlanID and PNAssignment.WBS1 = @OldWBS1 and WBS2 = @OldWBS2
				Update PNExpense Set PlanID=@NewPlanID, WBS1=@NewWBS1, WBS2=@NewWBS2 Where PlanID=@OldPlanID and PNExpense.WBS1 = @OldWBS1 and WBS2 = @OldWBS2
				Update PNConsultant Set PlanID=@NewPlanID, WBS1=@NewWBS1, WBS2=@NewWBS2 Where PlanID=@OldPlanID and PNConsultant.WBS1 = @OldWBS1 and WBS2 = @OldWBS2

			End
-- Combine Navigator Plans End


-- Delete Navigator Plans mapping
		if (@NPlanSourceExist = 'Y' and @NPlanTargetExist = 'N')
			Begin
				Delete PNPlannedLabor From PNPlannedLabor Left Join PNAssignment on PNPlannedLabor.AssignmentID=PNAssignment.AssignmentID
				Where PNAssignment.WBS1=@OldWBS1 and PNAssignment.WBS2=@OldWBS2
				Delete PNBaselineLabor From PNBaselineLabor Left Join PNAssignment on PNBaselineLabor.AssignmentID=PNAssignment.AssignmentID
				Where PNAssignment.WBS1=@OldWBS1 and PNAssignment.WBS2=@OldWBS2

				Delete PNPlannedExpenses From PNPlannedExpenses Left Join PNExpense on PNPlannedExpenses.ExpenseID=PNExpense.ExpenseID
				Where PNExpense.WBS1=@OldWBS1 and PNExpense.WBS2=@OldWBS2
				Delete PNBaselineExpenses From PNBaselineExpenses Left Join PNExpense on PNBaselineExpenses.ExpenseID=PNExpense.ExpenseID
				Where PNExpense.WBS1=@OldWBS1 and PNExpense.WBS2=@OldWBS2

				Delete PNPlannedConsultant From PNPlannedConsultant Left Join PNConsultant on PNPlannedConsultant.ConsultantID=PNConsultant.ConsultantID
				Where PNConsultant.WBS1=@OldWBS1 and PNConsultant.WBS2=@OldWBS2
				Delete PNBaselineConsultant From PNBaselineConsultant Left Join PNConsultant on PNBaselineConsultant.ConsultantID=PNConsultant.ConsultantID
				Where PNConsultant.WBS1=@OldWBS1 and PNConsultant.WBS2=@OldWBS2

				Delete PNAssignment Where PNAssignment.WBS1=@OldWBS1 and PNAssignment.WBS2=@OldWBS2
				Delete PNExpense Where PNExpense.WBS1=@OldWBS1 and PNExpense.WBS2=@OldWBS2
				Delete PNConsultant Where PNConsultant.WBS1=@OldWBS1 and PNConsultant.WBS2=@OldWBS2
				Delete PNTask Where PNTask.WBS1=@OldWBS1 and PNTask.WBS2=@OldWBS2
			End

-- If Moving or Combining Phase for different Projects or no Target Vision Plan, unmap Vision plans
		if (@VPlanSourceExist = 'Y' and ((@Existing = 1 and @VPlanTargetExist = 'N') or @OldWBS1 <> @NewWBS1))
			Begin
if (@diag <> 0)	print 'If Moving or Combining Phase for different Projects or no Target Vision Plan, unmap Vision plans'

if exists (select * from RPTask where WBS1=@OldWBS1 and WBS2=@OldWBS2)
	if (@diag <> 0)	print 'Existing .....'
else
	if (@diag <> 0)	print 'Not Existing .....'

				Update RPTask set WBS2='<none>' where WBS1=@OldWBS1 and WBS2=@OldWBS2
				Update RPAssignment set WBS2='<none>' where WBS1=@OldWBS1 and WBS2=@OldWBS2
				Update RPExpense set WBS2='<none>' where WBS1=@OldWBS1 and WBS2=@OldWBS2
				Update RPConsultant set WBS2='<none>' where WBS1=@OldWBS1 and WBS2=@OldWBS2
				Update RPUnit set WBS2='<none>' where WBS1=@OldWBS1 and WBS2=@OldWBS2
			End

-- If From and To Vision Plans exists and Projects are the same, Merge Vision Plans
		if (@VPlanSourceExist = 'Y' and @VPlanTargetExist = 'Y' and @OldWBS1 = @NewWBS1)
			Begin
if (@diag <> 0)	print 'If From and To Vision Plans exists and Projects are the same, Merge Vision Plans'
if (@diag <> 0)	print '@VPlanSourceExist = ''Y'' and @VPlanTargetExist = ''Y'' and @OldWBS1 = @NewWBS1'

				declare @PlanID varchar(32)
				declare @PlanFetch 	integer
				declare PlanCursor cursor for
					select distinct PlanID from RPTask where wbs1=@oldWbs1 and WBSType = 'WBS1'
				open PlanCursor
				fetch next from PlanCursor into @PlanID
				set @PlanFetch = @@Fetch_Status
				While (@PlanFetch = 0)
				begin
	-- combine duplicate rsources in RPAssignment, RPExpense, RPConsultant
	-- Move Planned and Basedline for the duplicate RPAssignment, Delete duplicate RPAssignment
					declare AssignmentCursor cursor for
						select TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, isnull(ResourceID,'') as ResourceID, isnull(GenericResourceID,'') as GenericResourceID, MIN(AssignmentID) as AssignmentID  
						from RPAssignment where PlanID=@PlanID and WBS1=@OldWBS1 and WBS2=@OldWBS2 and
						exists (select 'x' from RPAssignment new where PlanID=@PlanID and new.wbs1 = @NewWBS1 and new.wbs2 = @NewWBS2 
							and isnull(new.wbs3,'') = isnull(RPAssignment.wbs3,'')
							and isnull(new.LaborCode,'') = isnull(RPAssignment.LaborCode,'')
							and isnull(new.ResourceID,'') = isnull(RPAssignment.ResourceID,'')
							and isnull(new.GenericResourceID,'') = isnull(RPAssignment.GenericResourceID,''))
						group by TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(LaborCode,''), isnull(ResourceID,''), isnull(GenericResourceID,'')
					open AssignmentCursor
					fetch next from AssignmentCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @LaborCode, @ResourceID, @GenericResourceID, @OldAssignmentID
					set @TempFetch = @@Fetch_Status
					While (@TempFetch = 0)
					begin
						select @NewTaskID=TaskID, @NewAssignmentID=AssignmentID from RPAssignment 
							where PlanID=@PlanID and WBS1=@NewWBS1 and WBS2 = @NewWBS2 
							and isnull(WBS3,'')=@WBS3 
							and isnull(LaborCode,'')=@LaborCode 
							and isnull(ResourceID,'')=@ResourceID 
							and isnull(GenericResourceID,'')=@GenericResourceID 

						if (@UseBookingForEmpHours = 'Y')
							Update RPPlannedLabor Set HardBooked=@RPHardBooking 
							From RPPlannedLabor Left Join RPAssignment on RPPlannedLabor.AssignmentID=RPAssignment.AssignmentID
							Where RPPlannedLabor.AssignmentID=@OldAssignmentID and RPAssignment.ResourceID > '' and RPPlannedLabor.HardBooked<>@RPHardBooking 
						if (@UseBookingForGenHours = 'Y')
							Update RPPlannedLabor Set HardBooked=@RPHardBooking 
							From RPPlannedLabor Left Join RPAssignment on RPPlannedLabor.AssignmentID=RPAssignment.AssignmentID
							Where RPPlannedLabor.AssignmentID=@OldAssignmentID and RPAssignment.GenericResourceID > '' and RPPlannedLabor.HardBooked<>@RPHardBooking 

						Update RPPlannedLabor Set PlanID=@PlanID, TaskID=@NewTaskID, AssignmentID=@NewAssignmentID where AssignmentID=@OldAssignmentID
						Update RPBaselineLabor Set PlanID=@PlanID, TaskID=@NewTaskID, AssignmentID=@NewAssignmentID where AssignmentID=@OldAssignmentID

						delete RPAssignment Where TaskID=@OldTaskID and WBS1=@WBS1 and WBS2=@WBS2 
							and isnull(WBS3,'')=@WBS3 
							and isnull(LaborCode,'')=@LaborCode 
							and isnull(ResourceID,'')=@ResourceID
							and isnull(GenericResourceID,'')=@GenericResourceID 

						fetch next from AssignmentCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @LaborCode, @ResourceID, @GenericResourceID, @OldAssignmentID
						set @TempFetch = @@Fetch_Status
					end
					close AssignmentCursor
					deallocate AssignmentCursor

	-- Move Planned and Basedline for the duplicate RPExpense, Delete duplicate RPExpense
					declare ExpenseCursor cursor for
						select TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Account,'') as Account, isnull(Vendor,'') as Vendor, MIN(ExpenseID) as ExpenseID  
						from RPExpense where PlanID=@PlanID and WBS1=@OldWBS1 and WBS2=@OldWBS2 and
						exists (select 'x' from RPExpense new where PlanID=@PlanID and new.wbs1 = @NewWBS1 and new.wbs2 = @NewWBS2 
							and isnull(new.wbs3,'') = isnull(RPExpense.wbs3,'')
							and isnull(new.Account,'') = isnull(RPExpense.Account,'')
							and isnull(new.Vendor,'') = isnull(RPExpense.Vendor,''))
						group by TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
					open ExpenseCursor
					fetch next from ExpenseCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldExpenseID
					set @TempFetch = @@Fetch_Status
					While (@TempFetch = 0)
					begin
						select @NewTaskID=TaskID, @NewExpenseID=ExpenseID from RPExpense 
							where PlanID=@PlanID and WBS1=@NewWBS1 and WBS2 = @NewWBS2 
							and isnull(WBS3,'')=@WBS3 
							and isnull(Account,'')=@Account 
							and isnull(Vendor,'')=@Vendor 

						Update RPPlannedExpenses Set PlanID=@PlanID, TaskID=@NewTaskID, ExpenseID=@NewExpenseID where ExpenseID=@OldExpenseID
						Update RPBaselineExpenses Set PlanID=@PlanID, TaskID=@NewTaskID, ExpenseID=@NewExpenseID where ExpenseID=@OldExpenseID

						delete RPExpense Where TaskID=@OldTaskID and WBS1=@WBS1 and WBS2=@WBS2 
							and isnull(WBS3,'')=@WBS3 
							and isnull(Account,'')=@Account
							and isnull(Vendor,'')=@Vendor 

						fetch next from ExpenseCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldExpenseID
						set @TempFetch = @@Fetch_Status
					end
					close ExpenseCursor
					deallocate ExpenseCursor

	-- Move Planned and Basedline for the duplicate RPConsultant, Delete duplicate RPConsultant
					declare ConsultantCursor cursor for
						select TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Account,'') as Account, isnull(Vendor,'') as Vendor, MIN(ConsultantID) as ConsultantID  
						from RPConsultant where PlanID=@PlanID and WBS1=@OldWBS1 and WBS2=@OldWBS2 and
						exists (select 'x' from RPConsultant new where PlanID=@PlanID and new.wbs1 = @NewWBS1 and new.wbs2 = @NewWBS2 
							and isnull(new.wbs3,'') = isnull(RPConsultant.wbs3,'')
							and isnull(new.Account,'') = isnull(RPConsultant.Account,'')
							and isnull(new.Vendor,'') = isnull(RPConsultant.Vendor,''))
						group by TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
					open ConsultantCursor
					fetch next from ConsultantCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldConsultantID
					set @TempFetch = @@Fetch_Status
					While (@TempFetch = 0)
					begin
						select @NewTaskID=TaskID, @NewConsultantID=ConsultantID from RPConsultant 
							where PlanID=@PlanID and WBS1=@NewWBS1 and WBS2 = @NewWBS2 
							and isnull(WBS3,'')=@WBS3 
							and isnull(Account,'')=@Account 
							and isnull(Vendor,'')=@Vendor 

						Update RPPlannedConsultant Set PlanID=@PlanID, TaskID=@NewTaskID, ConsultantID=@NewConsultantID where ConsultantID=@OldConsultantID
						Update RPBaselineConsultant Set PlanID=@PlanID, TaskID=@NewTaskID, ConsultantID=@NewConsultantID where ConsultantID=@OldConsultantID

						delete RPConsultant Where TaskID=@OldTaskID and WBS1=@WBS1 and WBS2=@WBS2 
							and isnull(WBS3,'')=@WBS3 
							and isnull(Account,'')=@Account
							and isnull(Vendor,'')=@Vendor 

						fetch next from ConsultantCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldConsultantID
						set @TempFetch = @@Fetch_Status
					end
					close ConsultantCursor
					deallocate ConsultantCursor

	-- Move Planned and Basedline for the duplicate RPUnit, Delete duplicate RPUnit
					declare UnitCursor cursor for
						select TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Account,'') as Account, MIN(UnitID) as UnitID  
						from RPUnit where PlanID=@PlanID and WBS1=@OldWBS1 and WBS2=@OldWBS2 and
						exists (select 'x' from RPUnit new where PlanID=@PlanID and new.wbs1 = @NewWBS1 and new.wbs2 = @NewWBS2 
							and isnull(new.wbs3,'') = isnull(RPUnit.wbs3,'')
							and isnull(new.Account,'') = isnull(RPUnit.Account,''))
						group by TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(Account,'')
					open UnitCursor
					fetch next from UnitCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @OldUnitID
					set @TempFetch = @@Fetch_Status
					While (@TempFetch = 0)
					begin
						select @NewTaskID=TaskID, @NewUnitID=UnitID from RPUnit 
							where PlanID=@PlanID and WBS1=@NewWBS1 and WBS2 = @NewWBS2 
							and isnull(WBS3,'')=@WBS3 
							and isnull(Account,'')=@Account 

						Update RPPlannedUnit Set PlanID=@PlanID, TaskID=@NewTaskID, UnitID=@NewUnitID where UnitID=@OldUnitID
						Update RPBaselineUnit Set PlanID=@PlanID, TaskID=@NewTaskID, UnitID=@NewUnitID where UnitID=@OldUnitID

						delete RPUnit Where TaskID=@OldTaskID and WBS1=@WBS1 and WBS2=@WBS2 
							and isnull(WBS3,'')=@WBS3 
							and isnull(Account,'')=@Account

						fetch next from UnitCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @OldUnitID
						set @TempFetch = @@Fetch_Status
					end
					close UnitCursor
					deallocate UnitCursor


-- Move Everything related for the duplicate RPTask
					declare TaskCursor cursor for
						select TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, isnull(WBSType,'') as WBSType 
						from RPTask where PlanID=@PlanID and WBS1=@OldWBS1 and WBS2=@OldWBS2 and
						exists (select 'x' from RPTask new where PlanID=@PlanID and new.wbs1 = @NewWBS1 and new.wbs2 = @NewWBS2 
							and isnull(new.wbs3,'') = isnull(RPTask.wbs3,'')
							and isnull(new.LaborCode,'') = isnull(RPTask.LaborCode,'')
							and isnull(new.WBSType,'') = isnull(RPTask.WBSType,''))
						group by TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(LaborCode,''), isnull(WBSType,'')
					open TaskCursor
					fetch next from TaskCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @LaborCode, @WBSType
					set @TempFetch = @@Fetch_Status
					While (@TempFetch = 0)
					begin
	if (@diag = 1) Print 'Update all related rptask tables ' + @PlanID +'/'+ @NewWBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+@WBSType
						select @NewTaskID = TaskID from RPTask 
							where PlanID=@PlanID and WBS1=@NewWBS1 and WBS2=@NewWBS2 
							and isnull(WBS3,'')=@WBS3 
							and isnull(LaborCode,'')=@LaborCode 
							and isnull(WBSType,'')=@WBSType 

	if (@diag = 1) Print 'Update RPPlannedLabor and RPBaselineLabor ' + @PlanID +'/'+ @NewWBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @ResourceID +'/'+@GenericResourceID
						if (@UseBookingForEmpHours = 'Y')
							Update RPPlannedLabor Set HardBooked=@RPHardBooking 
							From RPPlannedLabor Left Join RPAssignment on RPPlannedLabor.AssignmentID=RPAssignment.AssignmentID
							Where RPPlannedLabor.TaskID=@OldTaskID and RPAssignment.ResourceID > '' and RPPlannedLabor.HardBooked<>@RPHardBooking 
						if (@UseBookingForGenHours = 'Y')
							Update RPPlannedLabor Set HardBooked=@RPHardBooking 
							From RPPlannedLabor Left Join RPAssignment on RPPlannedLabor.AssignmentID=RPAssignment.AssignmentID
							Where RPPlannedLabor.TaskID=@OldTaskID and RPAssignment.GenericResourceID > '' and RPPlannedLabor.HardBooked<>@RPHardBooking 

						Update RPPlannedLabor Set PlanID=@PlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
						Update RPPlannedExpenses Set PlanID=@PlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
						Update RPPlannedConsultant Set PlanID=@PlanID, TaskID=@NewTaskID where TaskID=@OldTaskID

						Update RPBaselineLabor Set PlanID=@PlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
						Update RPBaselineExpenses Set PlanID=@PlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
						Update RPBaselineConsultant Set PlanID=@PlanID, TaskID=@NewTaskID where TaskID=@OldTaskID

						Update RPAssignment Set PlanID=@PlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
						Update RPExpense Set PlanID=@PlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
						Update RPConsultant Set PlanID=@PlanID, TaskID=@NewTaskID where TaskID=@OldTaskID

						Update RPPlannedRevenueLabor Set PlanID=@PlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
						Update RPBaselineRevenueLabor Set PlanID=@PlanID, TaskID=@NewTaskID where TaskID=@OldTaskID

	if (@diag = 1) Print 'delete RPTask Where TaskID=@OldTaskID and WBS1=@WBS1 ' + @OldTaskID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+@WBSType
						delete RPTask Where TaskID=@OldTaskID
						delete RPDependency Where TaskID=@OldTaskID

						fetch next from TaskCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @LaborCode, @WBSType
						set @TempFetch = @@Fetch_Status
					end
					close TaskCursor
					deallocate TaskCursor

	-- merge RPTask, RPAssignment, RPExpense, RPConsultant
					Update RPTask Set WBS1=@NewWBS1, WBS2=@NewWBS2 Where PlanID=@PlanID and RPTask.WBS1 = @OldWBS1 and WBS2 = @OldWBS2
					Update RPAssignment Set WBS1=@NewWBS1, WBS2=@NewWBS2 Where PlanID=@PlanID and RPAssignment.WBS1 = @OldWBS1 and WBS2 = @OldWBS2
					Update RPExpense Set WBS1=@NewWBS1, WBS2=@NewWBS2 Where PlanID=@PlanID and RPExpense.WBS1 = @OldWBS1 and WBS2 = @OldWBS2
					Update RPConsultant Set WBS1=@NewWBS1, WBS2=@NewWBS2 Where PlanID=@PlanID and RPConsultant.WBS1 = @OldWBS1 and WBS2 = @OldWBS2
					Update RPUnit Set WBS1=@NewWBS1, WBS2=@NewWBS2 Where PlanID=@PlanID and RPUnit.WBS1 = @OldWBS1 and WBS2 = @OldWBS2

					fetch next from PlanCursor into @PlanID
					set @PlanFetch = @@Fetch_Status
				end
				close PlanCursor
				deallocate PlanCursor
			End


		if (@OldWBS2 = ' ') -- converting a project without phases/tasks
			begin
/*
				update ARC set WBS1=@NewWBS1 Where WBS1 = @OldWBS1
				
				update BTBG set MainWBS1=@NewWBS1 Where MainWBS1 = @OldWBS1 and not exists (select 'x' from BTBG new Where new.MainWBS1 =@NewWBS1)
				Delete BTBG Where MainWBS1 = @OldWBS1
				
				update BTBGSubs set MainWBS1=@NewWBS1 Where MainWBS1 = @OldWBS1 and not exists (select 'x' from BTBGSubs new Where new.MainWBS1 =@NewWBS1 and new.SubWBS1 = BTBGSubs.subWBS1)
				Delete BTBGSubs Where MainWBS1 = @OldWBS1
				update BTBGSubs set SubWBS1=@NewWBS1 Where SubWBS1 = @OldWBS1 and not exists (select 'x' from BTBGSubs new Where new.SubWBS1 =@NewWBS1 and new.MainWBS1 = BTBGSubs.MainWBS1)
				Delete BTBGSubs Where SubWBS1 = @OldWBS1
				
				update CFGTimeAnalysis set StartWBS1=@NewWBS1 Where StartWBS1 = @OldWBS1 and not exists (select 'x' from CFGTimeAnalysis new Where new.StartWBS1 =@NewWBS1)
				Delete CFGTimeAnalysis Where StartWBS1 = @OldWBS1
				update CFGTimeAnalysis set EndWBS1=@NewWBS1 Where EndWBS1 = @OldWBS1 and not exists (select 'x' from CFGTimeAnalysis new Where new.EndWBS1 =@NewWBS1)
				Delete CFGTimeAnalysis Where EndWBS1 = @OldWBS1*/
			     Delete from BTA where WBS1 = @OldWBS1
			     Delete from BTF where WBS1 = @OldWBS1
		         Delete from BTSchedule where WBS1 = @OldWBS1
		         Delete from BTTaxCodes where WBS1 = @OldWBS1
			     Delete from BT where WBS1 = @OldWBS1
		        Delete from BTBG where MainWBS1 = @OldWBS1
		        Delete from BTBGSubs where MainWBS1 = @OldWBS1
		        Delete from BTBGSubs where SubWBS1 = @OldWBS1
		        Delete from billAddDetail where MainWBS1 = @OldWBS1
		        Delete from billARDetail where MainWBS1 = @OldWBS1
		        Delete from billBTDDetail where MainWBS1 = @OldWBS1
		        Delete from billConDetail where MainWBS1 = @OldWBS1
		        Delete from billExpDetail where MainWBS1 = @OldWBS1
		        Delete from billFeeDetail where MainWBS1 = @OldWBS1
		        Delete from billIntDetail where MainWBS1 = @OldWBS1
		        Delete from billINDetail where MainWBS1 = @OldWBS1
		        Delete from billINMaster where MainWBS1 = @OldWBS1
		        Delete from billINDetail where WBS1 = @OldWBS1
		        Delete from billINMaster where WBS1 = @OldWBS1
		        Delete from billInvMaster where MainWBS1 = @OldWBS1
		        Delete from billInvSums where MainWBS1 = @OldWBS1
		        Delete from billLabDetail where MainWBS1 = @OldWBS1
		        Delete from billLimDetail where MainWBS1 = @OldWBS1
		        Delete from billRetDetail where MainWBS1 = @OldWBS1
		        Delete from billTaxDetail where MainWBS1 = @OldWBS1
		        Delete from billUnitDetail where MainWBS1 = @OldWBS1				

				Delete from billFeeAllocation where MainWBS1 = @OldWBS1
				Delete from billExpWriteOffDelete where MainWBS1 = @OldWBS1
				Delete from billLabWriteOffDelete where MainWBS1 = @OldWBS1
				Delete from billUnitWriteOffDelete where MainWBS1 = @OldWBS1

/*added 3.0 sp5*/
		        Delete from ICBillInvMaster where MainWBS1 = @OldWBS1
		        Delete from ICBillLabDetail where MainWBS1 = @OldWBS1
		        Delete from ICBillExpDetail where MainWBS1 = @OldWBS1
		        Delete from ICBillTaxDetail where MainWBS1 = @OldWBS1
		        Delete from ICBillInvSums where MainWBS1 = @OldWBS1

				update PRClientAssoc set WBS1=@NewWBS1 Where WBS1 = @OldWBS1 and not exists (select 'x' from PRClientAssoc new Where new.WBS1 =@NewWBS1 and new.ClientID = PRClientAssoc.ClientID)
				Delete PRClientAssoc Where WBS1 = @OldWBS1
				
				update PRContactAssoc set WBS1=@NewWBS1 Where WBS1 = @OldWBS1 and not exists (select 'x' from PRContactAssoc new Where new.WBS1 =@NewWBS1 and new.ContactID = PRContactAssoc.ContactID)
				Delete PRContactAssoc Where WBS1 = @OldWBS1
/*removed at 2.0 handled above				
				update SF254Projects set WBS1=@NewWBS1 Where WBS1 = @OldWBS1
				update SF255Projects set WBS1=@NewWBS1 Where WBS1 = @OldWBS1
				
				update VEProjectAssoc set WBS1=@NewWBS1 Where WBS1 = @OldWBS1 and not exists (select 'x' from VEProjectAssoc new Where new.WBS1 =@NewWBS1 and new.Vendor =ClendorProjectAssoc.Vendor)
				Delete ClendorProjectAssoc Where WBS1 = @OldWBS1
*/
			end

/*handle inconsistent billing tables*/
	update btf set PostWBS2 = @NewWBS2
	where isNull(PostWBS1,WBS1) = @oldWBS1 and
		PostWBS2 = @oldWbs2
/*
	update pr set RevUpsetWBS2 = @NewWBS2
	where exists (select 'x' from pr tmp where
		tmp.WBS1 = @oldWBS1 and
		tmp.WBS2 = @oldWbs2)
	delete from keyconvertdriver where columnName = 'RevUpsetWBS2'
*/
/**/
		exec @RetVal = KeyCvt @Entity = @Entity,
					 @CvtType = @CvtType,
					 @Length = 20,
					 @ErrMsg = @ErrMsg2 output,
					 @Diag = @Diag,
					 @NewValue = @NewWBS1,
					 @NewValue2 = @NewWBS2,
					 @OldValue = @OldWBS1,
					 @OldValue2 = @OldWBS2,
					 @ConstrFlag = 3 -- UpdateTables
		
--
		if (@RetVal = 0)
			begin
				Delete FROM BT WHERE BT.WBS1 = @OldWBS1 and BT.WBS2 = @OldWBS2 and
				exists (select 'x' from bt new where new.WBS1 = @NewWBS1 and new.wbs2 = @NewWBS2 and new.wbs3 = bt.wbs3)
				Delete FROM PR WHERE PR.WBS1 = @OldWBS1 and PR.WBS2 = @OldWBS2 and
				exists (select 'x' from pr new where new.WBS1 = @NewWBS1 and new.wbs2 = @NewWBS2 and new.wbs3 = pr.wbs3)

				Update PR Set SubLevel='N' WHERE PR.WBS1 = @OldWBS1 and PR.WBS2 = ' ' and SubLevel='Y' and 
				not exists (select 'x' from pr old where old.WBS1 = @OldWBS1 and old.wbs2 <> ' ')
				Update PR Set SubLevel='Y' WHERE PR.WBS1 = @NewWBS1 and PR.WBS2 = ' ' and SubLevel='N' and 
				exists (select 'x' from pr new where new.WBS1 = @NewWBS1 and new.wbs2 <> ' ')

				Update BT Set BT.WBS1 = @NewWBS1, BT.WBS2ToPost = @NewWBS2 Where BT.WBS1 = @OldWBS1 and BT.WBS2ToPost = @OldWBS2
				and not exists (select 'x' from BT new where new.WBS1 = @NewWBS1 and new.WBS2 = BT.WBS2 and new.WBS3 = BT.WBS3)
				Update BT Set BT.WBS1 = @NewWBS1, BT.IntercompanyTaxWBS2ToPost = @NewWBS2 Where BT.WBS1 = @OldWBS1 and BT.IntercompanyTaxWBS2ToPost = @OldWBS2
				and not exists (select 'x' from BT new where new.WBS1 = @NewWBS1 and new.WBS2 = BT.WBS2 and new.WBS3 = BT.WBS3)
				Update BT Set BT.WBS1 = @NewWBS1, BT.TaxWBS2ToPost = @NewWBS2 Where BT.WBS1 = @OldWBS1 and BT.TaxWBS2ToPost = @OldWBS2
				and not exists (select 'x' from BT new where new.WBS1 = @NewWBS1 and new.WBS2 = BT.WBS2 and new.WBS3 = BT.WBS3)

				if exists (select 'x' from FW_CFGSystem where AutoSumComp='Y')
				begin
					exec UpdatePRFees @OldWBS1, 'N'
					exec UpdatePRFees @OldWBS1, 'Y'
					if @OldWBS1 <> @NewWBS1
					begin
						exec UpdatePRFees @NewWBS1, 'N'
						exec UpdatePRFees @NewWBS1, 'Y'
					end
				end

-- Add by DP 12/11/2009
				INSERT INTO EMProjectAssoc (RecordID,WBS1,WBS2,WBS3,Employee,Role,RoleDescription,TeamStatus,CreateUser) 
				SELECT replace(convert(varchar(50),newid()),'-',''),EMProjectAssoc.WBS1,' ',' ',EMProjectAssoc.Employee,' ',NULL,'Active','KeyConvert' 
				from EMProjectAssoc 
				left join (select distinct WBS1,Employee from EMProjectAssoc Where WBS2= N' ') A on EMProjectAssoc.Employee=A.Employee and EMProjectAssoc.WBS1=A.WBS1 
				inner join (select distinct WBS1 from PR Where WBS2= N' ') B on EMProjectAssoc.WBS1=B.WBS1 
				where EMProjectAssoc.WBS2<>N' ' and A.WBS1 is null and EMProjectAssoc.WBS1=@NewWBS1 
				group by EMProjectAssoc.WBS1, EMProjectAssoc.Employee 

				INSERT INTO ClendorProjectAssoc (WBS1,WBS2,WBS3,ClientID,Role,RoleDescription,TeamStatus,Address,PrimaryInd,ClientConfidential,ClientInd,VendorInd,CreateUser) 
				SELECT ClendorProjectAssoc.WBS1,' ',' ',ClendorProjectAssoc.ClientID,max(Role),max(convert(Nvarchar,RoleDescription)),max(TeamStatus),max(Address),max(PrimaryInd),max(ClientConfidential),max(ClientInd),max(VendorInd),'KeyConvert' 
				from ClendorProjectAssoc 
				left join (select distinct WBS1,ClientID from ClendorProjectAssoc Where WBS2= N' ') A on ClendorProjectAssoc.ClientID=A.ClientID and ClendorProjectAssoc.WBS1=A.WBS1 
				inner join (select distinct WBS1 from PR Where WBS2= N' ') B on ClendorProjectAssoc.WBS1=B.WBS1 
				where ClendorProjectAssoc.WBS2<>N' ' and A.WBS1 is null and ClendorProjectAssoc.WBS1=@NewWBS1 
				group by ClendorProjectAssoc.WBS1, ClendorProjectAssoc.ClientID 
--

-- Add by DP 4.0
				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq, OldKey2, OldKey3, NewKey2, NewKey3, OldName2, OldName3, NewName2, NewName3, WBSName)
					select PKey, Entity, OldKey, NewKey, @OldName, @NewName, @Period, @LastPostSeq, @OldWBS2, N'', @NewWBS2, N'', @OldName2, N'', @NewName2, N'', WBSName from keyConvertWork where Pkey = @PKey
				end
--
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--- Storm 1.1 Update 1st level under WBS3 with ParentOutlineNumber				
				declare RPTaskCursor cursor for 
						select PlanID, TaskID, WBS1, WBS2, WBS3, LaborCode, ChildrenCount, OutlineLevel, OutlineNumber, ParentOutlineNumber, TableName from #TempRPPN 
				open RpTaskCursor
				fetch next from RPTaskCursor into 
					@RPOldPlanID,
					@RPOldTaskID,
					@RPOldWBS1,
					@RPOldWBS2,
					@RPOldWBS3,
					@RPOldLaborCode,
					@RPOldChildrenCount,
					@RPOldOutLineLevel,
					@RPOldOutLineNumber,
					@RPOldParentOutlineNumber,
					@RPTableName
				set @RPTaskfetch = @@FETCH_STATUS

				while  (@RPTaskFetch = 0)
				begin
					if (@RPTableName = 'PNTask') 
						begin 							
							Update PNTask Set PlanID=@NewPlanID, WBS1=@NewWBS1, WBS2=@NewWBS2 From PNTask
							Where PNTask.PlanID=@RPOldPlanID and PNTask.WBS1 = @RPOldWBS1 and isnull(PNTask.WBS2,'') = @RPOldWBS2 And isnull(PNTask.WBS3,'')=@RPOldWBS3 
							And isnull(PNTask.LaborCode,'')=@RPOldLaborCode and PNTask.OutlineLevel >= @RPOldOutlineLevel 

							Update PNAssignment Set PlanID=@NewPlanID, WBS1=@newWBS1, WBS2=@Newwbs2 from PNAssignment
							where PNAssignment.PlanID=@RPOldPlanID and PNAssignment.TaskID=@RPOldTaskID and PNAssignment.WBS1=@RPOldWBS1 and PNAssignment.WBS2=@RPOldWBS2 and PNAssignment.WBS3=@RPOldWBS3

							Update PNExpense Set PlanID=@NewPlanID, WBS1=@NewWBS1, WBS2=@NewWBS2 from PNExpense
							where PNExpense.PlanID=@RPOldPlanID and PNExpense.TaskID=@RPOldTaskID and PNExpense.WBS1=@RPOldWBS1 and PNExpense.WBS2=@RPOldWBS2 and PNExpense.WBS3=@RPOldWBS3

							Update PNConsultant Set PlanID=@NewPlanID, WBS1=@NewWBS1, WBS2=@NewWBS2 from PNConsultant
							where PNConsultant.PlanID=@RPOldPlanID and PNConsultant.TaskID=@RPOldTaskID and PNConsultant.WBS1=@RPOldWBS1 and PNConsultant.WBS2=@RPOldWBS2 and PNConsultant.WBS3=@RPOldWBS3
						end

					if (@RPTableName = 'RPTask') 
						begin
							Update RPTask Set PlanID=@NewPlanID, WBS1=@NewWBS1, WBS2=@NewWBS2 from RPTask
							Where RPTask.PlanID=@RPOldPlanID and RPTask.WBS1 = @RPOldWBS1 and RPTask.WBS2 = @RPOldWBS2 And RPTask.WBS3=@RPOldWBS3 and RPTask.OutlineLevel >= @RPOldOutlineLevel 

							Update RPAssignment Set PlanID=@NewPlanID, WBS1=@newWBS1, WBS2=@Newwbs2 from RPAssignment
							where RPAssignment.PlanID=@RPOldPlanID and RPAssignment.TaskID=@RPOldTaskID and RPAssignment.WBS1=@RPOldWBS1 and RPAssignment.WBS2=@RPOldWBS2 and RPAssignment.WBS3=@RPOldWBS3

							Update RPExpense Set PlanID=@NewPlanID, WBS1=@newWBS1, WBS2=@Newwbs2 from RPExpense
							where RPExpense.PlanID=@RPOldPlanID and RPExpense.TaskID=@RPOldTaskID and RPExpense.WBS1=@RPOldWBS1 and RPExpense.WBS2=@RPOldWBS2 and RPExpense.WBS3=@RPOldWBS3

							Update RPConsultant Set PlanID=@NewPlanID, WBS1=@NewWBS1, WBS2=@NewWBS2 from RPConsultant
							where RPConsultant.PlanID=@RPOldPlanID and RPConsultant.TaskID=@RPOldTaskID and RPConsultant.WBS1=@RPOldWBS1 and RPConsultant.WBS2=@RPOldWBS2 and RPConsultant.WBS3=@RPOldWBS3

							Update RPUnit Set PlanID=@NewPlanID, WBS1=@NewWBS1, WBS2=@NewWBS2 from RPUnit
							where RPUnit.PlanID=@RPOldPlanID and RPUnit.TaskID=@RPOldTaskID and RPUnit.WBS1=@RPOldWBS1 and RPUnit.WBS2=@RPOldWBS2 and RPUnit.WBS3=@RPOldWBS3
						End

					fetch next from RPTaskCursor into 
					@RPOldPlanID,
					@RPOldTaskID,
					@RPOldWBS1,
					@RPOldWBS2,
					@RPOldWBS3,
					@RPOldLaborCode,
					@RPOldChildrenCount,
					@RPOldOutLineLevel,
					@RPOldOutLineNumber,
					@RPOldParentOutlineNumber,
					@RPTableName
					set @RPTaskfetch = @@FETCH_STATUS
				End
				close RPTaskCursor
				deallocate RPTaskCursor

				-- Update/Delete Duplicate outlinelevel >= 3 Records
				declare RPPNDupTaskCursor cursor for 
					select PlanID, Min(TaskID) As OrigTaskID, Max(TaskID) As MergeTaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, OutlineLevel, OutlineNumber, ParentOutlineNumber, 'RPTask' tableName from RPTask
					Where OutlineLevel >= 3 And WBS1 = @NewWBS1 and WBS2 = @NewWBS2 
					group by PlanID, WBS1, WBS2, WBS3, LaborCode, OutlineLevel, ParentOutlineNumber, OutlineNumber
					having count(*) > 1
					union 
					select PlanID, Min(TaskID) As OrigTaskID, Max(TaskID) As MergeTaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, OutlineLevel, OutlineNumber, ParentOutlineNumber, 'PNTask' tableName from PNTask
					Where OutlineLevel >= 3 And WBS1 = @NewWBS1 and WBS2 = @NewWBS2 
					group by PlanID, WBS1, WBS2, WBS3, LaborCode, OutlineLevel, ParentOutlineNumber, OutlineNumber
					having count(*) > 1

				open RPPNDupTaskCursor
				fetch next from RPPNDupTaskCursor into 
					@RPOldPlanID,
					@RPOldTaskID,
					@RPNewTaskID, 
					@RPOldWBS1,
					@RPOldWBS2,
					@RPOldWBS3,
					@RPOldLaborCode,
					@RPOldOutLineLevel,
					@RPOldOutLineNumber,
					@RPOldParentOutlineNumber, 
					@RPTableName
				set @RPTaskfetch = @@FETCH_STATUS
				while  (@RPTaskFetch = 0)
				begin
					if (@RPTableName = 'PNTask') 
						begin 							
							Update PNTask
							set PNTask.CompensationFee = PNTask.CompensationFee + old.CompensationFee,
								PNTask.ConsultantFee = PNTask.ConsultantFee + old.ConsultantFee,
								PNTask.ReimbAllowance = PNTask.ReimbAllowance + old.ReimbAllowance, 
								PNTask.CompensationFeeBill = PNTask.CompensationFeeBill + old.CompensationFeeBill, 
								PNTask.ConsultantFeeBill = PNTask.ConsultantFeeBill + old.ConsultantFeeBill,
								PNTask.ReimbAllowanceBill = PNTask.ReimbAllowanceBill + old.ReimbAllowanceBill,
								PNTask.CompensationFeeDirLab = PNTask.CompensationFeeDirLab + old.CompensationFeeDirLab, 
								PNTask.CompensationFeeDirExp = PNTask.CompensationFeeDirExp + old.CompensationFeeDirExp, 
								PNTask.ReimbAllowanceExp = PNTask.ReimbAllowanceExp + old.ReimbAllowanceExp,
								PNTask.ReimbAllowanceCon = PNTask.ReimbAllowanceCon + old.ReimbAllowanceCon,
								PNTask.CompensationFeeDirLabBill = PNTask.CompensationFeeDirLabBill + old.CompensationFeeDirLabBill, 
								PNTask.CompensationFeeDirExpBill = PNTask.CompensationFeeDirExpBill + old.CompensationFeeDirExpBill,
								PNTask.ReimbAllowanceExpBill = PNTask.ReimbAllowanceExpBill + old.ReimbAllowanceExpBill,
								PNTask.ReimbAllowanceConBill = PNTask.ReimbAllowanceConBill + old.ReimbAllowanceConBill
							from PNTask old inner join PNTask on 1=1
							where
								PNTask.WBS1 = @RPOldWBS1 and old.wbs1 = @RPOldWBS1 
								and isnull(PNTask.WBS2,'') = @RPOldWBS2 and isnull(old.WBS2,'') = @RPOldWBS2 
								and isnull(PNTask.WBS3,'') = @RPOldWBS3 and isnull(old.WBS3,'') = @RPOldwbs3
								and isnull(PNTask.LaborCode,'') = @RPOldLaborCode and isnull(old.LaborCode,'') = @RPOldLaborCode 
								and PNTask.ParentOutlineNumber = @RPOldParentOutLineNumber and old.ParentOutlineNumber = @RPOldParentOutLineNumber
								and PNTask.OutlineLevel = @RPOldOutlineLevel and old.OutlineLevel = @RPOldOutlineLevel
								and PNTask.OutlineNumber = @RPOldOutlineNumber and old.OutlineNumber = @RPOldOutlineNumber
								and PNTask.PlanID = @RPOldPlanID and old.PlanID = @RPOldPlanID
								and PNTask.TaskID = @RPNewTaskID and old.TaskID = @RPOldTaskID

							Update PNAssignment Set TaskID = @RPNewTaskID 
							where PNAssignment.PlanID=@RPOldPlanID and PNAssignment.TaskID=@RPOldTaskID and PNAssignment.WBS1=@RPOldWBS1 and PNAssignment.WBS2=@RPOldWBS2 and PNAssignment.WBS3=@RPOldWBS3

							Update PNExpense Set TaskID = @RPNewTaskID 
							where PNExpense.PlanID=@RPOldPlanID and PNExpense.TaskID=@RPOldTaskID and PNExpense.WBS1=@RPOldWBS1 and PNExpense.WBS2=@RPOldWBS2 and PNExpense.WBS3=@RPOldWBS3

							Update PNConsultant Set TaskID = @RPNewTaskID 
							where PNConsultant.PlanID=@RPOldPlanID and PNConsultant.TaskID=@RPOldTaskID and PNConsultant.WBS1=@RPOldWBS1 and PNConsultant.WBS2=@RPOldWBS2 and PNConsultant.WBS3=@RPOldWBS3

							Delete From PNTask 
							Where PNTask.PlanID=@RPOldPlanID and PNTask.TaskID = @RPOldTaskID and PNTask.WBS1 = @RPOldWBS1 and PNTask.WBS2 = @RPOldWBS2 And PNTask.WBS3=@RPOldWBS3 
								And PNTask.OutlineLevel = @RPOldOutlineLevel And PNTask.OutlineNumber = @RPOldOutlineNumber and PNTask.ParentOutlineNumber = @RPOldParentOutLineNumber
						end

					if (@RPTableName = 'RPTask') 
						begin
							Update RPTask
							set RPTask.CompensationFee = RPTask.CompensationFee + old.CompensationFee,
								RPTask.ConsultantFee = RPTask.ConsultantFee + old.ConsultantFee,
								RPTask.ReimbAllowance = RPTask.ReimbAllowance + old.ReimbAllowance, 
								RPTask.CompensationFeeBill = RPTask.CompensationFeeBill + old.CompensationFeeBill, 
								RPTask.ConsultantFeeBill = RPTask.ConsultantFeeBill + old.ConsultantFeeBill,
								RPTask.ReimbAllowanceBill = RPTask.ReimbAllowanceBill + old.ReimbAllowanceBill,
								RPTask.CompensationFeeDirLab = RPTask.CompensationFeeDirLab + old.CompensationFeeDirLab, 
								RPTask.CompensationFeeDirExp = RPTask.CompensationFeeDirExp + old.CompensationFeeDirExp, 
								RPTask.ReimbAllowanceExp = RPTask.ReimbAllowanceExp + old.ReimbAllowanceExp,
								RPTask.ReimbAllowanceCon = RPTask.ReimbAllowanceCon + old.ReimbAllowanceCon,
								RPTask.CompensationFeeDirLabBill = RPTask.CompensationFeeDirLabBill + old.CompensationFeeDirLabBill, 
								RPTask.CompensationFeeDirExpBill = RPTask.CompensationFeeDirExpBill + old.CompensationFeeDirExpBill,
								RPTask.ReimbAllowanceExpBill = RPTask.ReimbAllowanceExpBill + old.ReimbAllowanceExpBill,
								RPTask.ReimbAllowanceConBill = RPTask.ReimbAllowanceConBill + old.ReimbAllowanceConBill
							from RPTask old inner join RPTask on 1=1
							where
								RPTask.WBS1 = @OldWBS1 and old.wbs1 = @OldWBS1 
								and isnull(RPTask.WBS2,'') = @OldWBS2 and isnull(old.WBS2,'') = @OldWBS2 
								and isnull(RPTask.WBS3,'') = @OldWBS3 and isnull(old.WBS3,'') = @Oldwbs3
								and isnull(RPTask.LaborCode,'') = @RPOldLaborCode and isnull(old.LaborCode,'') = @RPOldLaborCode 
								and RPTask.ParentOutlineNumber = @RPOldParentOutLineNumber and old.ParentOutlineNumber = @RPOldParentOutLineNumber
								and RPTask.OutlineLevel = @RPOldOutlineLevel and old.OutlineLevel = @RPOldOutlineLevel
								and RPTask.OutlineNumber = @RPOldOutlineNumber and old.OutlineNumber = @RPOldOutlineNumber
								and RPTask.PlanID = @RPOldPlanID and old.PlanID = @RPOldPlanID
								and RPTask.TaskID = @RPNewTaskID and old.TaskID = @RPOldTaskID								

							Update RPAssignment Set TaskID = @RPNewTaskID
							where RPAssignment.PlanID=@RPOldPlanID and RPAssignment.TaskID=@RPOldTaskID and RPAssignment.WBS1=@RPOldWBS1 and RPAssignment.WBS2=@RPOldWBS2 and RPAssignment.WBS3=@RPOldWBS3

							Update RPExpense Set TaskID = @RPNewTaskID
							where RPExpense.PlanID=@RPOldPlanID and RPExpense.TaskID=@RPOldTaskID and RPExpense.WBS1=@RPOldWBS1 and RPExpense.WBS2=@RPOldWBS2 and RPExpense.WBS3=@RPOldWBS3

							Update RPConsultant Set TaskID = @RPNewTaskID
							where RPConsultant.PlanID=@RPOldPlanID and RPConsultant.TaskID=@RPOldTaskID and RPConsultant.WBS1=@RPOldWBS1 and RPConsultant.WBS2=@RPOldWBS2 and RPConsultant.WBS3=@RPOldWBS3

							Update RPUnit Set TaskID = @RPNewTaskID
							where RPUnit.PlanID=@RPOldPlanID and RPUnit.TaskID=@RPOldTaskID and RPUnit.WBS1=@RPOldWBS1 and RPUnit.WBS2=@RPOldWBS2 and RPUnit.WBS3=@RPOldWBS3

							delete From RPTask 
							Where RPTask.PlanID=@RPOldPlanID and RPTask.TaskID = @RPOldTaskID and RPTask.WBS1 = @RPOldWBS1 and RPTask.WBS2 = @RPOldWBS2 And RPTask.WBS3=@RPOldWBS3 
								And RPTask.OutlineLevel = @RPOldOutlineLevel And RPTask.OutlineNumber = @RPOldOutlineNumber and RPTask.ParentOutlineNumber = @RPOldParentOutLineNumber
						End
					
					fetch next from RPPNDupTaskCursor into 
					@RPOldPlanID,
					@RPOldTaskID,
					@RPNewTaskID, 
					@RPOldWBS1,
					@RPOldWBS2,
					@RPOldWBS3,
					@RPOldLaborCode,
					@RPOldOutLineLevel,
					@RPOldOutLineNumber,
					@RPOldParentOutlineNumber, 
					@RPTableName
					set @RPTaskfetch = @@FETCH_STATUS
				End
				close RPPNDupTaskCursor
				deallocate RPPNDupTaskCursor

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				if (@OldPlanID > '')
				begin
					PRINT '>>> Start Processing PlanID = "' + @OldPlanID + '", WBS1 = "' + @OldWBS1 + '"'
					Update RPTask Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@OldPlanID 
					Update RPAssignment Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@OldPlanID 
					Update RPExpense Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@OldPlanID 
					Update RPConsultant Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@OldPlanID 
					Update RPUnit Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@OldPlanID 
					Update PNTask Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@OldPlanID 
					Update PNAssignment Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@OldPlanID 
					Update PNExpense Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@OldPlanID 
					Update PNConsultant Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@OldPlanID 
					EXECUTE dbo.stRPSyncWBSStructure @OldPlanID
					PRINT '<<< End Processing PlanID = "' + @OldPlanID + '", WBS1 = "' + @OldWBS1 + '"'
				end
				if (@NewPlanID > '' and @OldPlanID <> @NewPlanID)
				begin
					PRINT '>>> Start Processing PlanID = "' + @NewPlanID + '", WBS1 = "' + @NewWBS1 + '"'
					Update RPTask Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
					Update RPAssignment Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
					Update RPExpense Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
					Update RPConsultant Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
					Update RPUnit Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
					Update PNTask Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
					Update PNAssignment Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
					Update PNExpense Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
					Update PNConsultant Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
					EXECUTE dbo.stRPSyncWBSStructure @NewPlanID
					PRINT '<<< End Processing PlanID = "' + @NewPlanID + '", WBS1 = "' + @NewWBS1 + '"'
				end

				if (@OldAllocMethod = 'r' and @NewAllocMethod = 'r')
					begin	--Update RevenueStartDate, RevenueEndDate for @NewPlanID
						Update PNTask Set RevenueStartDate = case when isnull(MinStartDate1, MinStartDate2) <= isnull(MinStartDate2, MinStartDate1) then isnull(MinStartDate1, MinStartDate2) Else isnull(MinStartDate2, MinStartDate1) End
						From PNTask Inner Join 
						(	select PlanID, TaskID, outlinenumber,
							(select Min(StartDate) from PNPlannedRevenueLabor where PlanID=PNTask.PlanID and TaskID=PNTask.TaskID) as MinStartDate1
							, (	select Min(StartDate) from PNPlannedRevenueLabor L 
								inner join ( select PlanID, TaskID from PNTask TT where TT.PlanID=PNTask.PlanID and TT.OutlineNumber like PNTask.OutlineNumber+'.%' ) T 
								on T.PlanID=L.PlanID and T.TaskID=L.TaskID
								) as MinStartDate2
							from PNTask  Where PNTask.PlanID=@NewPlanID 
						) T on PNTask.PlanID=T.PlanID and PNTask.TaskID=T.TaskID
						Where PNTask.PlanID=@NewPlanID and RevenueStartDate > case when isnull(MinStartDate1, MinStartDate2) <= isnull(MinStartDate2, MinStartDate1) then isnull(MinStartDate1, MinStartDate2) Else isnull(MinStartDate2, MinStartDate1) End

						Update PNTask Set RevenueEndDate = case when isnull(MaxEndDate1, MaxEndDate2) >= isnull(MaxEndDate2, MaxEndDate1) Then isnull(MaxEndDate1, MaxEndDate2) Else isnull(MaxEndDate2, MaxEndDate1) End
						From PNTask Inner Join 
						(	select PlanID, TaskID, outlinenumber,
							(select Max(EndDate) from PNPlannedRevenueLabor where PlanID=PNTask.PlanID and TaskID=PNTask.TaskID) as MaxEndDate1
							, (	select Max(EndDate) from PNPlannedRevenueLabor L 
								inner join ( select PlanID, TaskID from PNTask TT where TT.PlanID=PNTask.PlanID and TT.OutlineNumber like PNTask.OutlineNumber+'.%' ) T 
								on T.PlanID=L.PlanID and T.TaskID=L.TaskID
								) as MaxEndDate2
							from PNTask  Where PNTask.PlanID=@NewPlanID 
						) T on PNTask.PlanID=T.PlanID and PNTask.TaskID=T.TaskID
						Where PNTask.PlanID=@NewPlanID and RevenueEndDate < case when isnull(MaxEndDate1, MaxEndDate2) >= isnull(MaxEndDate2, MaxEndDate1) Then isnull(MaxEndDate1, MaxEndDate2) Else isnull(MaxEndDate2, MaxEndDate1) End

						Update RPTask Set RevenueStartDate = case when isnull(MinStartDate1, MinStartDate2) <= isnull(MinStartDate2, MinStartDate1) then isnull(MinStartDate1, MinStartDate2) Else isnull(MinStartDate2, MinStartDate1) End
						From RPTask Inner Join 
						(	select PlanID, TaskID, outlinenumber,
							(select Min(StartDate) from RPPlannedRevenueLabor where PlanID=RPTask.PlanID and TaskID=RPTask.TaskID) as MinStartDate1
							, (	select Min(StartDate) from RPPlannedRevenueLabor L 
								inner join ( select PlanID, TaskID from RPTask TT where TT.PlanID=RPTask.PlanID and TT.OutlineNumber like RPTask.OutlineNumber+'.%' ) T 
								on T.PlanID=L.PlanID and T.TaskID=L.TaskID
								) as MinStartDate2
							from RPTask  Where RPTask.PlanID=@NewPlanID 
						) T on RPTask.PlanID=T.PlanID and RPTask.TaskID=T.TaskID
						Where RPTask.PlanID=@NewPlanID and RevenueStartDate > case when isnull(MinStartDate1, MinStartDate2) <= isnull(MinStartDate2, MinStartDate1) then isnull(MinStartDate1, MinStartDate2) Else isnull(MinStartDate2, MinStartDate1) End

						Update RPTask Set RevenueEndDate = case when isnull(MaxEndDate1, MaxEndDate2) >= isnull(MaxEndDate2, MaxEndDate1) Then isnull(MaxEndDate1, MaxEndDate2) Else isnull(MaxEndDate2, MaxEndDate1) End
						From RPTask Inner Join 
						(	select PlanID, TaskID, outlinenumber,
							(select Max(EndDate) from RPPlannedRevenueLabor where PlanID=RPTask.PlanID and TaskID=RPTask.TaskID) as MaxEndDate1
							, (	select Max(EndDate) from RPPlannedRevenueLabor L 
								inner join ( select PlanID, TaskID from RPTask TT where TT.PlanID=RPTask.PlanID and TT.OutlineNumber like RPTask.OutlineNumber+'.%' ) T 
								on T.PlanID=L.PlanID and T.TaskID=L.TaskID
								) as MaxEndDate2
							from RPTask  Where RPTask.PlanID=@NewPlanID 
						) T on RPTask.PlanID=T.PlanID and RPTask.TaskID=T.TaskID
						Where RPTask.PlanID=@NewPlanID and RevenueEndDate < case when isnull(MaxEndDate1, MaxEndDate2) >= isnull(MaxEndDate2, MaxEndDate1) Then isnull(MaxEndDate1, MaxEndDate2) Else isnull(MaxEndDate2, MaxEndDate1) End
					end

				if (@OldAllocMethod <> 'r' and @NewAllocMethod = 'r')
					begin	--Update RevenueStartDate, RevenueEndDate for @NewPlanID
						Update PNTask Set RevenueStartDate=parentTask.RevenueStartDate, RevenueEndDate=parentTask.RevenueEndDate
						from pntask 
						left join pntask parentTask on parentTask.PlanID=@NewPlanID and parentTask.WBSType='WBS1' and parentTask.WBS1=@NewWBs1
						where pntask.PlanID=@NewPlanID and pntask.wbs1=@NewWBs1 and pntask.WBS2=@NewWBS2

						Update RPTask Set RevenueStartDate=parentTask.RevenueStartDate, RevenueEndDate=parentTask.RevenueEndDate
						from RPTask 
						left join RPTask parentTask on parentTask.PlanID=@NewPlanID and parentTask.WBSType='WBS1' and parentTask.WBS1=@NewWBs1
						where RPTask.PlanID=@NewPlanID and RPTask.wbs1=@NewWBs1 and RPTask.WBS2=@NewWBS2
					end
				if (@OldAllocMethod = 'r' and @NewAllocMethod <> 'r')
					begin	--Blank RevenueStartDate, RevenueEndDate for @NewPlanID
						Update PNTask Set RevenueStartDate=null, RevenueEndDate=null
						from pntask 
						where pntask.PlanID=@NewPlanID and pntask.wbs1=@NewWBs1 and pntask.WBS2=@NewWBS2

						Update RPTask Set RevenueStartDate=null, RevenueEndDate=null
						from RPTask 
						where RPTask.PlanID=@NewPlanID and RPTask.wbs1=@NewWBs1 and RPTask.WBS2=@NewWBS2
					end

--To Publish a Nav plan
				if (@OldPlanID > '' and @NPlanSourceExist='Y')
				begin
if (@diag <> 0)	print 'Execute dbo.PMDelPlan @OldPlanID'					 
					Execute dbo.PMDelPlan @OldPlanID
					Execute dbo.stRPPublish @OldPlanID
				end
				if (@NewPlanID > '' and @NPlanTargetExist='Y')
				begin
if (@diag <> 0)	print 'Execute dbo.PMDelPlan @NewPlanID'					 
					Execute dbo.PMDelPlan @NewPlanID
					Execute dbo.stRPPublish @NewPlanID
				end

--Undo Check out
if (@diag <> 0)	print 'Undo Check out'
				if (@VPlanSourceExist = 'Y')
					Update RPPlan Set CheckedOutDate=NULL, CheckedOutUser=NULL, CheckedOutID=NULL
					From RPPlan 
					where CheckedOutID=@CheckedOutID
				
				if (@NPlanSourceExist = 'Y' or @NPlanTargetExist = 'Y')
					Update PNPlan Set CheckedOutDate=NULL, CheckedOutUser=NULL, CheckedOutID=NULL
					From PNPlan 
					where CheckedOutID=@CheckedOutID

				delete from keyConvertWork where current of keysCursor
				commit Transaction
			end
		else
			begin
				set @ErrMsg = @ErrMsg + @ErrMsg2
				rollback transaction
			end

	Drop Table #tempRPPN

	fetch next from KeysCursor into 
		@OldWBS1,
		@OldWBS2,
		@OldWBS3,
		@NewWBS1,
		@NewWBS2,
		@NewWBS3,
		@PKey
	set @KeysFetch = @@Fetch_Status
	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	SET ANSI_WARNINGS ON
	return(0)
End -- Procedure
GO
