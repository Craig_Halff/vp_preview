SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[DeltekStoredProc_HAI_ProjectTypeUnits_GetAll]
AS
BEGIN
  ------------------------------------------------------------------------
  SET NOCOUNT ON;

  ------------------------------------------------------------------------
  SELECT
	UDIC_UID
	, CustUnitOfMeasure AS Unit
  FROM
    dbo.UDIC_ProjectType_CustUnitsOfMeasure
  ORDER BY
	UDIC_UID
	, Unit
  ------------------------------------------------------------------------
END
GO
