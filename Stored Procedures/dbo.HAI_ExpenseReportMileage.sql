SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[HAI_ExpenseReportMileage]
AS
/*
    Copyright (c) 2018 Halff Associates Inc. All rights reserved.
    Sets the mileage rate for un-posted transactions in the previous year to the appropriate rate.

    08/26/2018	Joseph Sagel
                Set mileage reimbursement rate per IRS rates
    01/04/2020	Craig H. Anderson
                Modify for 2020  Rates
                Simplify to run-once because our NetAmount should always equal PaymentAmount as we do not reimburse at a rate greater than the IRS rate.
*/
    DECLARE
        @Mileage FLOAT;

    BEGIN
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;
        BEGIN TRANSACTION
            -- SET @Mileage = 0.5450;  -- 2018
            -- SET @Mileage = 0.580;   -- 2019
            -- SET @Mileage = 0.575;   -- 2020 
            SET @Mileage = 0.5750; --2021
    
            --
            UPDATE d
            SET d.AmountPerMile = @Mileage
              , d.Amount        = @Mileage * d.Miles
              , d.NetAmount     = @Mileage * d.Miles
              , d.PaymentAmount = @Mileage * d.Miles
            FROM ekMaster m
               , ekDetail d
               , PR p
            WHERE m.Employee = d.Employee
              AND m.ReportDate = d.ReportDate
              AND m.ReportName = d.ReportName
              AND m.STATUS <> 'P'
              AND d.WBS1 = p.WBS1
              AND d.WBS2 = p.WBS2
              AND d.WBS3 = p.WBS3
              AND d.TransDate > '12/31/2019'
              AND d.TransDate <= '12/31/2020'
              AND d.Miles <> 0.00; -- Ensure it's an actual mileage entry
        COMMIT TRANSACTION;
    END;
GO
