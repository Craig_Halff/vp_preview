SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[HAI_QAErrorReviewRequired]
    @WBS1 VARCHAR(30)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
 
    DECLARE @Error1 NVARCHAR(255) = N'Unable to Change QA Review to “No” because project Total Compensation exceeds review threshold for practice.                                                                 ';
    DECLARE @SummedAmount DECIMAL(19,4);
    DECLARE @ThresholdAmount DECIMAL(19,4);
    DECLARE @WBS VARCHAR(30);
 
    SELECT DISTINCT 
	    @WBS = p.WBS1, 
        @SummedAmount = p.Fee + p.ConsultFee + p.ReimbAllow, 
        @ThresholdAmount = qaqc.CustTotalCompensationThreshold
     FROM dbo.PR p
          INNER JOIN dbo.ProjectCustomTabFields ctf ON ctf.WBS1 = p.WBS1
          INNER JOIN dbo.UDIC_QAQCParameters qaqc ON qaqc.CustPracticeArea = ctf.CustPracticeArea
    WHERE p.WBS1 = @WBS1
      AND p.WBS2 = '';

    IF @SummedAmount >= @ThresholdAmount
        RAISERROR(@Error1, 16, 1);
 
END
GO
