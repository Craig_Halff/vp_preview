SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AccountInTables] @Account Nvarchar(20),@TableName Nvarchar(50) output,@Company Nvarchar(50) output
as
declare
	@BaseStmt		Nvarchar(1000),
	@SqlStmt		Nvarchar(max),
	@WhereClause		Nvarchar(100),
	@CompanyWhere		Nvarchar(100),
	@BaseStmtNoMC		Nvarchar(1000),
	@BaseStmtRP		Nvarchar(1000),
	@WhereClauseNoMC	Nvarchar(100),
	@MulticompanyEnabled 	varchar(1),
	@Org1Start 		int,
	@Org1Length 		int
	
begin 

	SELECT @MulticompanyEnabled = MulticompanyEnabled FROM FW_CFGSystem
	SELECT @Org1Start = Org1Start, @Org1Length = Org1Length FROM CFGFormat	

	set @WhereClauseNoMC = ' (?Account = N''' + @Account + ''') '
	set @BaseStmtNoMC = 'SELECT TOP 1 ''?Table'' as TableName, '' '' AS Company FROM ?Table WHERE ' + @WhereClauseNoMC
	set @BaseStmtRP = 'SELECT TOP 1 ''?Table'' as TableName, '' '' AS Company FROM ?Table WHERE ' + @WhereClauseNoMC
	set @TableName = ''
	set @CompanyWhere = ''

	if rtrim(@Company) <> ''
	begin	
		if len(@Company) > @Org1Length
			set @Company = SUBSTRING (@Company, @Org1Start, @Org1Length)
	end

	if rtrim(@Account) <> '' 
	begin
		-- These tables have a Company column
		if @MulticompanyEnabled = 'N'
		begin
			set @BaseStmt = @BaseStmtNoMC
			set @WhereClause = @WhereClauseNoMC
		end
		else
		begin
			if rtrim(@Company) <> ''	
				set @CompanyWhere = ' AND Company = N''' + @Company + ''''
			set @WhereClause = ' (?Account = N''' + @Account + '''' + @CompanyWhere + ') '
			set @BaseStmt = 'SELECT TOP 1 ''?Table'' as TableName, Company FROM ?Table WHERE ' + @WhereClause
		end

		set @SqlStmt = 'declare ChkCursor cursor for SELECT top 1 TableName, Company FROM ('
		set @SqlStmt = @SqlStmt + replace(replace(@BaseStmt,'?Table','CFGAPDiscount'),'?Account','Account')
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','CFGARMap'),'?Account','Account')
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt+'OR'+replace(@WhereClause,'?Account','UnbilledServices') +'OR'+replace(@WhereClause,'?Account','AcctsReceivable') +'OR'+replace(@WhereClause,'?Account','EmplAdvCreditAccount') +'OR'+replace(@WhereClause,'?Account','RetainedEarnPrevYrs') +'OR'+replace(@WhereClause,'?Account','RetainedEarnCurrent'),'?Table','CFGAutoPosting'),'?Account','UninvoicedRevenue')
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt+'OR'+replace(@WhereClause,'?Account','AcctsPayableIntercompany') +'OR'+replace(@WhereClause,'?Account','SuspenseIntercompany') +'OR'+replace(@WhereClause,'?Account','ForeignEMTypeDirectAccount') +'OR'+replace(@WhereClause,'?Account','ForeignEMTypeIndirectAccount') +'OR'+replace(@WhereClause,'?Account','RealizedGainsAccount')
			+'OR'+replace(@WhereClause,'?Account','UninvoicedRevenue1')+'OR'+replace(@WhereClause,'?Account','UninvoicedRevenue2')+'OR'+replace(@WhereClause,'?Account','UninvoicedRevenue3')+'OR'+replace(@WhereClause,'?Account','UninvoicedRevenue4')+'OR'+replace(@WhereClause,'?Account','UninvoicedRevenue5')
			+'OR'+replace(@WhereClause,'?Account','UnbilledServices1')+'OR'+replace(@WhereClause,'?Account','UnbilledServices2')+'OR'+replace(@WhereClause,'?Account','UnbilledServices3')+'OR'+replace(@WhereClause,'?Account','UnbilledServices4')+'OR'+replace(@WhereClause,'?Account','UnbilledServices5')
			+'OR'+replace(@WhereClause,'?Account','RetainedEarnCurrent') +'OR'+replace(@WhereClause,'?Account','AcctsReceivableIntercompany') +'OR'+replace(@WhereClause,'?Account','ICBillingLabRevenueAccount') +'OR'+replace(@WhereClause,'?Account','ForeignEMTypeDirectAccount') 
			+'OR'+replace(@WhereClause,'?Account','UnbilledRealizedGainsAccount')+'OR'+replace(@WhereClause,'?Account','UnbilledUnrealizedGainsAccount') +'OR'+replace(@WhereClause,'?Account','UnbilledRealizedLossesAccount') +'OR'+replace(@WhereClause,'?Account','UnbilledUnrealizedLossesAccount')
			+'OR'+replace(@WhereClause,'?Account','FeeBilledAccount1')+'OR'+replace(@WhereClause,'?Account','FeeBilledAccount2')+'OR'+replace(@WhereClause,'?Account','FeeBilledAccount3')+'OR'+replace(@WhereClause,'?Account','FeeBilledAccount4')+'OR'+replace(@WhereClause,'?Account','FeeBilledAccount5')
		,'?Table','CFGAutoPosting'),'?Account','UnrealizedGainsAccount')
                set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt+'OR'+replace(@WhereClause,'?Account','ICCashBasisSuspenseAccount')+'OR'+replace(@WhereClause,'?Account','RealizedLossesAccount'),'?Table','CFGAutoPosting'),'?Account','UnrealizedLossesAccount')
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt+'OR'+replace(@WhereClause,'?Account','MiscCreditAccount') +'OR'+replace(@WhereClause,'?Account','PrintsCreditAccount') +'OR'+replace(@WhereClause,'?Account','ConsAccrAccount') +'OR'+replace(@WhereClause,'?Account','EmplExpCreditAccount'),'?Table','CFGAutoPosting'),'?Account','LaborCreditAccount')
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt+'OR'+replace(@WhereClause,'?Account','ConAccount') +'OR'+replace(@WhereClause,'?Account','ExpAccount') +'OR'+replace(@WhereClause,'?Account','UnitAccount') +'OR'+replace(@WhereClause,'?Account','FeeAccount') +'OR'+replace(@WhereClause,'?Account','AddOnAccount') +'OR'+replace(@WhereClause,'?Account','IntAccount'),'?Table','CFGBillMainData'),'?Account','LabAccount')
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt+'OR'+replace(@WhereClause,'?Account','DebitAccount')+'OR'+replace(@WhereClause,'?Account','ICBillingARAcct'),'?Table','CFGBillTaxesData'),'?Account','CreditAccount')
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt+'OR'+replace(@WhereClause,'?Account','CompanyPaidAccount') +'OR'+replace(@WhereClause,'?Account','IndirectAccount') +'OR'+replace(@WhereClause,'?Account','ReimbAccount'),'?Table','CFGEKCategory'),'?Account','DirectAccount')
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','CFGEKMain'),'?Account','CompanyPaidAccount')
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt+'OR'+replace(@WhereClause,'?Account','IndirectAccount'),'?Table','CFGEmployeeTypeAccounts'),'?Account','DirectAccount')
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt+'OR'+replace(@WhereClause,'?Account','ARAccount') +'OR'+replace(@WhereClause,'?Account','RetainageAccount'),'?Table','CFGInvMap'),'?Account','Account')
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','CFGOHAccounts'),'?Account','Account')
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','CFGTimeAnalysis'),'?Account','Account')
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt+'OR'+replace(@WhereClause,'?Account','FICAAccount') +'OR'+replace(@WhereClause,'?Account','BonusCostAccount') +'OR'+replace(@WhereClause,'?Account','OtherPayCostAccount'),'?Table','CFGPYMain'),'?Account','PayrollCreditAccount')
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','CFGPYWHCodes'),'?Account','Account')
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt+'OR'+replace(@WhereClause,'?Account','XChargeRegDebitAccount') +'OR'+replace(@WhereClause,'?Account','XChargeOHCreditAccount') +'OR'+replace(@WhereClause,'?Account','XChargeOHDebitAccount'),'?Table','CFGXCharge'),'?Account','XChargeRegCreditAccount')
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt+'OR'+replace(@WhereClause,'?Account','RegAccount') +'OR'+replace(@WhereClause,'?Account','ReimbAccount'),'?Table','VEAccounting'),'?Account','OHAccount')

/*7.5 changes*/
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','CFGAssetMain'),'?Account','FAAccount')
		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','CFGFAAssetTypeData'),'?Account','AssetAccount') +'OR'+replace(@WhereClause,'?Account','AccuDepAccount') +'OR'+replace(@WhereClause,'?Account','DepExpAccount')
/*end 7.5*/

		set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt+'OR'+replace(@WhereClause,'?Account','RegAccount') +'OR'+replace(@WhereClause,'?Account','OHAccount') +'OR'+replace(@WhereClause,'?Account','CreditAccount'),'?Table','UN'),'?Account','PostAccount')
		set @SqlStmt = @SqlStmt + ') as TableCheck'

		execute (@SqlStmt)
		open ChkCursor
		fetch ChkCursor into @TableName, @Company
		close ChkCursor
		deallocate ChkCursor

		-- These tables have an Org column
		if rtrim(@TableName) = ''
		begin
			if @MulticompanyEnabled = 'N'
			begin
				set @BaseStmt = @BaseStmtNoMC
				set @WhereClause = @WhereClauseNoMC
			end
			else
			begin
				if rtrim(@Company) <> ''	
					set @CompanyWhere = ' AND Substring(?Table.Org, ' + CONVERT(Nvarchar,@Org1Start) + ',' + CONVERT(Nvarchar,@Org1Length) + ') = N''' + @Company + ''''
				set @WhereClause = ' (?Account = N''' + @Account + '''' + @CompanyWhere + ') '
				set @BaseStmt = 'SELECT TOP 1 ''?Table'' as TableName, Substring(?Table.Org, ' 
				set @BaseStmt = @BaseStmt + CONVERT(Nvarchar,@Org1Start) + ',' + CONVERT(Nvarchar,@Org1Length) + ') As Company '
				set @BaseStmt = @BaseStmt + ' FROM ?Table WHERE ' + @WhereClause
			end

			set @SqlStmt = 'declare ChkCursor cursor for SELECT top 1 TableName, Company FROM ('
			set @SqlStmt = @SqlStmt + replace(replace(@BaseStmt,'?Table','apppChecks'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','CFGAPLiability'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','CFGBanks'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ') as TableCheck'
			execute (@SqlStmt)
			open ChkCursor
			fetch ChkCursor into @TableName, @Company
			close ChkCursor
			deallocate ChkCursor
		end

		-- These tables need to be joined back to PR to figure out the Company column in Multicompany enabled environment
		if rtrim(@TableName) = ''
		begin
  			if @MulticompanyEnabled = 'N'
			begin
				set @BaseStmt = @BaseStmtNoMC
				set @WhereClause = @WhereClauseNoMC
			end
			else
			begin
				if rtrim(@Company) <> ''	
					set @CompanyWhere = ' AND Substring(PR.Org, ' + CONVERT(Nvarchar,@Org1Start) + ',' + CONVERT(Nvarchar,@Org1Length) + ') = N''' + @Company + ''''
				set @WhereClause = ' (?Account = N''' + @Account + '''' + @CompanyWhere + ') '
				set @BaseStmt = 'SELECT TOP 1 ''?Table'' as TableName, Substring(PR.Org, ' 
				set @BaseStmt = @BaseStmt + CONVERT(Nvarchar,@Org1Start) + ',' + CONVERT(Nvarchar,@Org1Length) + ') As Company '
				set @BaseStmt = @BaseStmt + ' FROM ?Table LEFT JOIN PR ON ?Table.WBS1 = PR.WBS1 AND ?Table.WBS2 = PR.WBS2 AND ?Table.WBS3 = PR.WBS3 WHERE ' + @WhereClause
			end

			set @SqlStmt = 'declare ChkCursor cursor for SELECT top 1 TableName, Company FROM ('
			set @SqlStmt = @SqlStmt + replace(replace(@BaseStmt,'?Table','apDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','billINDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','billUnitDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','cdDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','crDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','cvDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','ekDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','exDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','inDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','jeDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','miDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','prDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','unDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt+'OR'+replace(@WhereClause,'?Account','ConAccount') +'OR'+replace(@WhereClause,'?Account','ExpAccount') +'OR'+replace(@WhereClause,'?Account','UnitAccount') +'OR'+replace(@WhereClause,'?Account','FeeAccount') +'OR'+replace(@WhereClause,'?Account','AddOnAccount') +'OR'+replace(@WhereClause,'?Account','IntAccount'),'?Table','BT'),'?Account','LabAccount')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','BTA'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmtRP,'?Table','RPConsultant'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmtRP,'?Table','RPExpense'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmtRP,'?Table','PNConsultant'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmtRP,'?Table','PNExpense'),'?Account','Account')

			-- CABDetail is associated with a master row in CAB, which has an Org column for Multicompany scenerio
  			if @MulticompanyEnabled = 'N'
			begin
				set @BaseStmt = @BaseStmtNoMC
				set @WhereClause = @WhereClauseNoMC
			end
			else
			begin
				if rtrim(@Company) <> ''
				begin	
					if len(@Company) > @Org1Length
						set @Company = SUBSTRING (@Company, @Org1Start, @Org1Length)
					set @CompanyWhere = ' AND Substring(CAB.Org, ' + CONVERT(Nvarchar,@Org1Start) + ',' + CONVERT(Nvarchar,@Org1Length) + ') = N''' + @Company + ''''
				end

				set @WhereClause = ' (?Account = N''' + @Account + '''' + @CompanyWhere + ') '
				set @BaseStmt = 'SELECT TOP 1 ''?Table'' as TableName, Substring(CAB.Org, ' 
				set @BaseStmt = @BaseStmt + CONVERT(Nvarchar,@Org1Start) + ',' + CONVERT(Nvarchar,@Org1Length) + ') As Company '
				set @BaseStmt = @BaseStmt + ' FROM ?Table INNER JOIN CAB ON ?Table.BudgetName = CAB.BudgetName WHERE ' + @WhereClause
			end
			set @SqlStmt = @SqlStmt + ' union ' +  replace(replace(@BaseStmt,'?Table','CABDetail'),'?Account','Account')


			if rtrim(@Company) = ''
			begin
				-- The following are global level accounts.
				set @BaseStmt = 'SELECT TOP 1 ''?Table'' as TableName, '' '' AS Company FROM ?Table WHERE ?Account = N''' + @Account + ''''  
				set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','CA'),'?Account','CashBasisAccount')
				set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','CA'),'?Account','UnrealizedLossAccount')
				set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','CA'),'?Account','UnrealizedGainAccount')


				-- Change the following lines FOR MULTICOMPANY SCENARIO (if needed) after Dave K. has decided the company context for these tables
				set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','BTEAAccts'),'?Account','Account')
				set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','BTECAccts'),'?Account','Account')

			end

			set @SqlStmt = @SqlStmt + ') as TableCheck'
			execute (@SqlStmt)
			open ChkCursor
			fetch ChkCursor into @TableName, @Company
			close ChkCursor
			deallocate ChkCursor
		end

		-- JOIN back to PR to figure out the Company column; make this a secondary cursor only if the previous cursors return nothing!
		if rtrim(@TableName) = ''
		begin
			if @MulticompanyEnabled = 'N'
			begin
				set @BaseStmt = @BaseStmtNoMC
				set @WhereClause = @WhereClauseNoMC
			end
			else
			begin
				if rtrim(@Company) <> ''	
					set @CompanyWhere = ' AND Substring(PR.Org, ' + CONVERT(Nvarchar,@Org1Start) + ',' + CONVERT(Nvarchar,@Org1Length) + ') = N''' + @Company + ''''
				set @WhereClause = ' (?Account = N''' + @Account + '''' + @CompanyWhere + ') '
				set @BaseStmt = 'SELECT TOP 1 ''?Table'' as TableName, Substring(PR.Org, ' 
				set @BaseStmt = @BaseStmt + CONVERT(Nvarchar,@Org1Start) + ',' + CONVERT(Nvarchar,@Org1Length) + ') As Company '
				set @BaseStmt = @BaseStmt + ' FROM ?Table LEFT JOIN PR ON ?Table.WBS1 = PR.WBS1 AND ?Table.WBS2 = PR.WBS2 AND ?Table.WBS3 = PR.WBS3 WHERE ' + @WhereClause
			end

			set @SqlStmt = 'declare ChkCursor cursor for SELECT top 1 TableName, Company FROM ('
			set @SqlStmt = @SqlStmt + replace(replace(@BaseStmt,'?Table','EB'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','BIED'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerAP'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerAR'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerEX'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerMisc'),'?Account','Account')
  			set @SqlStmt = @SqlStmt + ') as TableCheck'

			execute (@SqlStmt)
			open ChkCursor
			fetch ChkCursor into @TableName, @Company
			close ChkCursor
			deallocate ChkCursor
		end
	end
end -- AccountInTables  
GO
