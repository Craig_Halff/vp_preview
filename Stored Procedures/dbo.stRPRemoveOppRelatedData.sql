SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPRemoveOppRelatedData]
AS

BEGIN -- Procedure stRPRemoveOppRelatedData

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- The purpose of this stored procedure is to remove the opportunity related data.
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  SET NOCOUNT ON

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
select * from sys.objects where object_id in (
select object_id From sys.columns where name =  'InfocenterArea') order by name
  BEGIN TRANSACTION

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  
  --Remove data related to the table that has InfoCenterArea
	delete from  BulkUpdateHistory where bulkupdateid in (select bulkupdateid from BulkUpdate where InfoCenterArea = 'Opportunities')
    delete  from BulkUpdate where InfoCenterArea = 'Opportunities'
	delete  from CFGGridViewFields where InfoCenterArea = 'Opportunities'
	delete  from CFGMobileCRMSections where InfoCenterArea = 'Opportunities'
	--delete  from CFGMobileFieldsData where InfoCenterArea = 'Opportunities'
	--delete  from CFGMobileFieldsDescriptions where InfoCenterArea = 'Opportunities'
	delete  from CFGNaturalKeys where InfoCenterArea = 'Opportunities'
	delete  from CFGNavCRMTabHeadings where InfoCenterArea = 'Opportunities'
	delete  from CFGNavCRMTabsData where InfoCenterArea = 'Opportunities'
	delete  from CustomProposalOrder where InfoCenterArea = 'Opportunities'
	--delete  from FW_CFGInfoCenterFieldsData where InfoCenterArea = 'Opportunities'
	--delete  from FW_CFGInfoCenterFieldsDesc where InfoCenterArea = 'Opportunities'
	delete  from FW_CFGUserSettingsData where InfoCenterArea = 'Opportunities'
	delete  from FW_CFGUserSettingsLabels where InfoCenterArea = 'Opportunities'
	delete  from FW_CustomColumnCaptions where InfoCenterArea = 'Opportunities'
	delete  from FW_CustomColumnsData where InfoCenterArea = 'Opportunities'
	delete  from FW_CustomColumnValues where InfoCenterArea = 'Opportunities'
	delete  from FW_CustomColumnValuesData where InfoCenterArea = 'Opportunities'
	
	delete  from FW_InfoCenterTabHeadings where InfoCenterArea = 'Opportunities'
	delete  from FW_InfoCenterTabsData where InfoCenterArea = 'Opportunities'
	delete  from MergeFavorites where InfoCenterArea = 'Opportunities'
	delete  from MergeTemplates where InfoCenterArea = 'Opportunities'
	delete  from SEInfoCenter where InfoCenterArea = 'Opportunities'
	delete  from SEMergeTemplate where InfoCenterArea = 'Opportunities'
	delete  from SETab where InfoCenterArea = 'Opportunities'
	delete  from TLSyncFields where InfoCenterArea = 'Opportunities'

	--delete lookup cache 
	delete from   LookupXmlCache where LookupType = 'Opportunity'
	--delete report related data
	delete from   FW_ReportFavorites where ReportType = 'Opportunity'
	delete from   FW_ReportOptions where Type like 'Opportunity%'
	delete from   ReportSubscr where ReportType = 'Opportunity'

	--delete activity type
	delete CFGActivityTypeData WHERE Code = 'Milestone'
	delete CFGActivityTypeDescriptions WHERE Code = 'Milestone'
	delete from Activity Where Type = 'Milestone'

	--delete security data
	delete from   SENavTree where NodeID in ('OppEstService','Opportunities','OpportunityRM','OpportunitySettings','rptOpportunity')
	delete from   SEReport where Folder = 'Opportunity'
	delete from   SEReportColumns where ReportFolder = 'Opportunity'
	delete from   SEReportGroups where GroupType = 'Opportunity'
    delete from   SESearches where OptionType = 'Opportunity' 

	--delete user saved options
	delete from FW_SEUserOptions where   OptionName like 'Opportunity%'

	--workflow data related to Opportunity
	DELETE FROM WorkflowConditions WHERE ID in 
	(SELECT EventID from WorkflowEvents Where ApplicationName = 'OpportunityICBO') 
	--Delete workflow action condtion related to Opportunity
	DELETE FROM WorkflowConditions
	WHERE ID in 
	(SELECT ActionID FROM WorkflowActions WHERE EventID in
	(SELECT EventID from WorkflowEvents Where ApplicationName = 'OpportunityICBO')
	)
	--Delete WorkflowActionColumnUpdate related to Opportunity
	DELETE FROM WorkflowActionColumnUpdate
	WHERE ActionID in 
	(SELECT ActionID FROM WorkflowActions WHERE EventID in
	(SELECT EventID from WorkflowEvents Where ApplicationName = 'OpportunityICBO')
	)

	--Delete WorkflowActionWebService related to Opportunity
	DELETE FROM WorkflowActionWebService
	WHERE ActionID in 
	(SELECT ActionID FROM WorkflowActions WHERE EventID in
	(SELECT EventID from WorkflowEvents Where ApplicationName = 'OpportunityICBO')
	)
	--Delete WorkflowActionWebServiceArgs related to Opportunity
	DELETE FROM WorkflowActionWebServiceArgs
	WHERE ActionID in 
	(SELECT ActionID FROM WorkflowActions WHERE EventID in
	(SELECT EventID from WorkflowEvents Where ApplicationName = 'OpportunityICBO')
	)
	--Delete WorkflowActionMethod related to Opportunity
	DELETE FROM WorkflowActionMethod
	WHERE ActionID in 
	(SELECT ActionID FROM WorkflowActions WHERE EventID in
	(SELECT EventID from WorkflowEvents Where ApplicationName = 'OpportunityICBO')
	)
	--Delete WorkflowActionMethodArgs related to Opportunity
	DELETE FROM WorkflowActionMethodArgs
	WHERE ActionID in 
	(SELECT ActionID FROM WorkflowActions WHERE EventID in
	(SELECT EventID from WorkflowEvents Where ApplicationName = 'OpportunityICBO')
	)
	--Delete DELETE FROM WorkflowActionSproc related to Opportunity
	DELETE FROM WorkflowActionSproc 
	WHERE ActionID in 
	(SELECT ActionID FROM WorkflowActions WHERE EventID in
	(SELECT EventID from WorkflowEvents Where ApplicationName = 'OpportunityICBO')
	)
	--Delete DELETE FROM WorkflowActionSprocArgs related to Opportunity
	DELETE FROM WorkflowActionSprocArgs 
	WHERE ActionID in 
	(SELECT ActionID FROM WorkflowActions WHERE EventID in
	(SELECT EventID from WorkflowEvents Where ApplicationName = 'OpportunityICBO')
	)

	----
    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	-- Rename the opportunity related tables or views
		exec sp_Rename        'CFGOppEstClause','CFGOppEstClause_Backup'
		exec sp_Rename        'CFGOppEstService', 'CFGOppEstService_Backup'
		--DROP TABLE        CFGOpportunityClosedReasonData
		--DROP TABLE        CFGOpportunityClosedReasonDescriptions
		--DROP TABLE        CFGOpportunitySourceData
		--DROP TABLE        CFGOpportunitySourceDescriptions
		exec sp_Rename        'CFGOpportunityStageData','CFGOpportunityStageData_Backup'
		exec sp_Rename        'CFGOpportunityStageDescriptions','CFGOpportunityStageDescriptions_Backup'
		exec sp_Rename        'CFGOpportunityStatusData', 'CFGOpportunityStatusData_Backup'
		exec sp_Rename        'CFGOpportunityStatusDescriptions','CFGOpportunityStatusDescriptions_Backup'
		exec sp_Rename        'CFGOppRevenueDefaults','CFGOppRevenueDefaults_Backup'
		exec sp_Rename        'CFGOppRevenueTemplateDetail','CFGOppRevenueTemplateDetail_Backup'
		exec sp_Rename        'CFGOppRevenueTemplateMaster','CFGOppRevenueTemplateMaster_Backup'
		exec sp_Rename        'CustomProposalOpportunity','CustomProposalOpportunity_Backup'
		exec sp_Rename        'CustomProposalOpportunityGraphics','CustomProposalOpportunityGraphics_Backup'
		exec sp_Rename        'DocumentsOpportunity','DocumentsOpportunity_Backup'
		exec sp_Rename        'MktCampaignOppAssoc','MktCampaignOppAssoc_Backup'
		exec sp_Rename        'OppConsultant','OppConsultant_Backup'
		exec sp_Rename        'OppExpense','OppExpense_Backup'
		exec sp_Rename        'OppFEWBS','OppFEWBS_Backup'
		exec sp_Rename        'OppLabor','OppLabor_Backup'
		exec sp_Rename        'Opportunity','Opportunity_Backup'
		exec sp_Rename        'OpportunityClientAssoc','OpportunityClientAssoc_Backup'
		exec sp_Rename        'OpportunityCompetitionAssoc','OpportunityCompetitionAssoc_Backup'
		exec sp_Rename        'OpportunityContactAssoc','OpportunityContactAssoc_Backup'
		exec sp_Rename        'OpportunityCustomTabFields','OpportunityCustomTabFields_Backup'
		exec sp_Rename        'OpportunityEMAssoc','OpportunityEMAssoc_Backup'
		exec sp_Rename        'OpportunityFECostGroups','OpportunityFECostGroups_Backup'
		exec sp_Rename        'OpportunityFEFuncGroups', 'OpportunityFEFuncGroups_Backup'
		exec sp_Rename        'OpportunityFEPhases', 'OpportunityFEPhases_Backup'
		exec sp_Rename        'OpportunityFESpecialServices','OpportunityFESpecialServices_Backup'
		exec sp_Rename        'OpportunityFileLinks','OpportunityFileLinks_Backup'
		exec sp_Rename        'OpportunityFromContacts','OpportunityFromContacts_Backup'
		exec sp_Rename        'OpportunityPhoto','OpportunityPhoto_Backup'
		exec sp_Rename        'OpportunityProjectCodes','OpportunityProjectCodes_Backup'
		exec sp_Rename        'OpportunityProposals', 'OpportunityProposals_Backup'
		exec sp_Rename        'OpportunityRevenue', 'OpportunityRevenue_Backup'
		exec sp_Rename        'OpportunitySubscr', 'OpportunitySubscr_Backup'
		exec sp_Rename        'OppUnit', 'OppUnit_Backup'
-- Not removing because the current 2.0 code still use it for project --VIEW     CFGOpportunityClosedReason
-- Not removing because the current 2.0 code still use it for GovIQ--VIEW     CFGOpportunitySource
		IF OBJECT_ID('dbo.CFGOpportunityStage', 'V') IS NOT NULL exec sp_Rename 'CFGOpportunityStage','CFGOpportunityStage_Backup'
		IF OBJECT_ID('dbo.CFGOpportunityStatus', 'V') IS NOT NULL exec sp_Rename 'CFGOpportunityStatus', 'CFGOpportunityStatus_Backup'
		IF OBJECT_ID('dbo.OpportunityVEAssoc', 'V') IS NOT NULL exec sp_Rename 'OpportunityVEAssoc','OpportunityVEAssoc_Backup'


	--remove the custom opportunity grid table
	DECLARE @gridTableName VARCHAR(100)
	DECLARE @sqlCommand varchar(1000)
	DECLARE gridCursor CURSOR FOR 
	select tableName from FW_CustomGrids where InfocenterArea = 'Opportunities'
 
	BEGIN
	OPEN gridCursor
	FETCH NEXT FROM gridCursor INTO @gridTableName
	WHILE @@FETCH_STATUS = 0 
	BEGIN
	set @sqlCommand = 'exec sp_Rename ''' + @gridTableName + ''','''  + @gridTableName + '_Backup'''
	print @sqlCommand
	EXEC (@sqlCommand)
	FETCH NEXT FROM gridCursor INTO @gridTableName
	END
	END
	CLOSE gridCursor
	DEALLOCATE gridCursor

	--now we can remove the opp custom grid data
	delete  from FW_CustomGridCaptions where InfoCenterArea = 'Opportunities'
	delete  from FW_CustomGrids where InfoCenterArea = 'Opportunities'
	delete  from FW_CustomGridsData where InfoCenterArea = 'Opportunities'
 
	--regenerate Audit triggers to remove Opportunity references 
	declare @tablename    nvarchar(100),
			@infocenter   nvarchar(32)
	declare TrigCursor cursor for 
		select distinct object_name(trg.parent_id) as TabName, ctu.infocenterarea
		from sys.sql_modules m, sys.triggers trg, CustomTablesWithUDIC ctu
		where trg.object_id = m.object_id and trg.type = 'TR' 
		and m.Definition like '%LEFT JOIN Opportunity%'
		and trg.Name like 'VisionAudit%' 
		and object_name(trg.parent_id) = ctu.TableName
	   order by 1
	begin
	   open TrigCursor
	   fetch TrigCursor into @tablename, @infocenter
	   while @@fetch_status = 0
	   begin
			 exec CreateCustomTabTriggers @tablename, @infocenter
			 fetch TrigCursor into @tablename, @infocenter
	   end
	   close TrigCursor
	   deallocate TrigCursor
	end

  COMMIT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF

END -- stRPRemoveOppRelatedData
GO
