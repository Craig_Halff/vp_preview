SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PNSpreadConTPD]
  @strTaskID VARCHAR(32)
AS

BEGIN -- Procedure PNSpreadConTPD

  SET NOCOUNT ON
  
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strPlanID varchar(32)
  
  -- Declare Temp tables.
  
  DECLARE @tabCalendarInterval
    TABLE(PlanID varchar(32) COLLATE database_default,
          StartDate datetime,
          EndDate datetime,
          PeriodScale varchar(1) COLLATE database_default
          PRIMARY KEY(PlanID, StartDate, EndDate))
          
	DECLARE @tabPConTPD
	  TABLE (RowID varchar(32) COLLATE database_default,
	         TimePhaseID varchar(32) COLLATE database_default,
           CIStartDate datetime, 
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         ConsultantID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PeriodCount smallint, 
           PeriodScale varchar(1) COLLATE database_default
           PRIMARY KEY(RowID, TimePhaseID, PlanID, EndDate)) 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get settings from PNPlan and CFGResourcePlanning.
  
  SELECT
    @strPlanID = P.PlanID,
    @strCompany = P.Company,
    @intAmtCostDecimals = 4,
    @intAmtBillDecimals = 4
    FROM PNTask AS T
      INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID
      INNER JOIN CFGResourcePlanning AS CRP ON P.Company = CRP.Company
    WHERE T.TaskID = @strTaskID

/*

  SELECT 
    @strCompany = P.Company,
    @strUnpostedFlg = P.UnpostedFlg,
    @sintGRMethod = P.GRMethod,
    @intGenResTableNo = P.GenResTableNo,
    @intGRBillTableNo = P.GRBillTableNo,
    @sintCostRtMethod = P.CostRtMethod,
    @sintBillingRtMethod = P.BillingRtMethod,
    @intCostRtTableNo = P.CostRtTableNo,
    @intBillingRtTableNo = P.BillingRtTableNo,
    @strExpTab = CRP.ExpTab,
    @strConTab = CRP.ConTab,
    @intHrDecimals = HrDecimals,
    @intAmtCostDecimals = CASE WHEN AmtDecimals = -1 THEN CC.DecimalPlaces ELSE AmtDecimals END, 
    @intAmtCostDecimals = CASE WHEN AmtDecimals = -1 THEN BC.DecimalPlaces ELSE AmtDecimals END, 
    @intRtCostDecimals = 4, 
    @intRtBillDecimals = 4, 
    @intLabRevDecimals = CASE WHEN P.LabMultType < 2 
                              THEN CASE WHEN AmtDecimals = -1 THEN CC.DecimalPlaces ELSE AmtDecimals END 
                              ELSE CASE WHEN AmtDecimals = -1 THEN BC.DecimalPlaces ELSE AmtDecimals END 
                         END, 
    @intECURevDecimals = CASE WHEN P.ReimbMethod = 'C' 
                              THEN CASE WHEN AmtDecimals = -1 THEN CC.DecimalPlaces ELSE AmtDecimals END 
                              ELSE CASE WHEN AmtDecimals = -1 THEN BC.DecimalPlaces ELSE AmtDecimals END 
                         END
    FROM PNPlan AS P, CFGResourcePlanning AS CRP, CFGCurrency AS CC, CFGCurrency AS BC 
    WHERE P.PlanID = @strPlanID AND CRP.Company = P.Company AND
          CC.Code = P.CostCurrencyCode AND BC.Code = P.BillingCurrencyCode 

*/

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Delete all time-phased data for the given task.
  
  DELETE PNPlannedConsultant WHERE TaskID = @strTaskID AND PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save Calendar Intervals into a temp table and add an overflow Calendar Interval.
  
  INSERT @tabCalendarInterval(PlanID, StartDate, EndDate, PeriodScale)
  SELECT PlanID, StartDate, EndDate, PeriodScale FROM
    (SELECT PlanID AS PlanID, CAST('19900101' AS datetime) AS StartDate,
			 DATEADD(d, -1, MIN(StartDate)) AS EndDate,
			 'o' AS PeriodScale
			 FROM PNCalendarInterval WHERE PlanID = @strPlanID
			 GROUP BY PlanID
		 UNION ALL
     SELECT PlanID AS PlanID, StartDate AS StartDate, EndDate AS EndDate, PeriodScale AS PeriodScale
       FROM PNCalendarInterval WHERE PlanID = @strPlanID
     UNION ALL
     SELECT PlanID AS PlanID, DATEADD(d, 1, MAX(EndDate)) AS StartDate,
       DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate))) AS EndDate,
       'o' AS PeriodScale
       FROM PNCalendarInterval WHERE PlanID = @strPlanID
       GROUP BY PlanID) AS CI

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
              
  -- Save Planned Consultant time-phased data rows.

  INSERT PNPlannedConsultant (
    TimePhaseID,
    PlanID, 
    TaskID,
    ConsultantID,
    StartDate, 
    EndDate, 
    PeriodCost,
    PeriodBill,
    PeriodCount, 
    PeriodScale
  )
    SELECT 
      REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
      PlanID AS PlanID, 
      TaskID AS TaskID,
      ConsultantID AS ConsultantID,
      StartDate AS StartDate, 
      EndDate AS EndDate, 
      ROUND(ISNULL(PeriodCost, 0), @intAmtCostDecimals) AS PeriodCost, 
      ROUND(ISNULL(PeriodBill, 0), @intAmtBillDecimals) AS PeriodBill, 
      PeriodCount AS PeriodCount, 
      PeriodScale AS PeriodScale
      FROM (
        SELECT 
          CI.StartDate AS CIStartDate, 
          C.PlanID AS PlanID, 
          C.TaskID AS TaskID,
          C.ConsultantID AS ConsultantID, 
          CASE WHEN T.StartDate > CI.StartDate THEN T.StartDate ELSE CI.StartDate END AS StartDate, 
          CASE WHEN T.EndDate < CI.EndDate THEN T.EndDate ELSE CI.EndDate END AS EndDate,
          C.PlannedConCost * 
            CASE 
              WHEN T.StartDate = T.EndDate THEN 1
              ELSE
                dbo.DLTK$ProrateRatio(
                  CASE WHEN T.StartDate > CI.StartDate THEN T.StartDate ELSE CI.StartDate END, 
                  CASE WHEN T.EndDate < CI.EndDate THEN T.EndDate ELSE CI.EndDate END, 
                  T.StartDate, T.EndDate,
                  @strCompany)
            END
            AS PeriodCost,
          C.PlannedConBill * 
            CASE 
              WHEN T.StartDate = T.EndDate THEN 1
              ELSE
                dbo.DLTK$ProrateRatio(
                  CASE WHEN T.StartDate > CI.StartDate THEN T.StartDate ELSE CI.StartDate END, 
                  CASE WHEN T.EndDate < CI.EndDate THEN T.EndDate ELSE CI.EndDate END, 
                  T.StartDate, T.EndDate,
                  @strCompany)
            END
            AS PeriodBill,
          1 AS PeriodCount, 
          CI.PeriodScale AS PeriodScale
          FROM @tabCalendarInterval AS CI 
            LEFT JOIN PNConsultant AS C ON CI.PlanID = C.PlanID AND C.TaskID = @strTaskID  AND 
              (C.PlannedConCost != 0 OR C.PlannedConBill != 0)
            LEFT JOIN PNTask AS T ON CI.PlanID = T.PlanID AND C.TaskID = T.TaskID
              AND T.StartDate <= CI.EndDate AND T.EndDate >= CI.StartDate
        ) AS X
        WHERE ((PeriodCost IS NOT NULL AND PeriodCost != 0) OR (PeriodBill IS NOT NULL AND PeriodBill != 0))
            
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Adjust Planned Consultant time-phased data to compensate for rounding errors.
     
  UPDATE PNPlannedConsultant SET 
    PeriodCost = TPD.PeriodCost + D.DeltaCost,
    PeriodBill = TPD.PeriodBill + D.DeltaBill
    FROM PNPlannedConsultant AS TPD INNER JOIN 
      (SELECT 
         C.ConsultantID AS ConsultantID,
         MAX(XTPD.EndDate) AS MaxEndDate,
         (C.PlannedConCost - SUM(ISNULL(XTPD.PeriodCost, 0))) AS DeltaCost,
         (C.PlannedConBill - SUM(ISNULL(XTPD.PeriodBill, 0))) AS DeltaBill
         FROM PNPlannedConsultant AS XTPD 
           INNER JOIN PNConsultant AS C ON XTPD.ConsultantID = C.ConsultantID AND XTPD.TaskID = @strTaskID AND C.TaskID = XTPD.TaskID
         GROUP BY C.ConsultantID, C.PlannedConCost, C.PlannedConBill) AS D
      ON TPD.ConsultantID = D.ConsultantID AND TPD.EndDate = D.MaxEndDate
    WHERE TPD.TaskID = @strTaskID
           
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF

END -- PNSpreadConTPD
GO
