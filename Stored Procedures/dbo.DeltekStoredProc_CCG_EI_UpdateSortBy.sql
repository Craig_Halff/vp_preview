SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_UpdateSortBy] ( @FILEID varchar(50))
             AS EXEC spCCG_EI_UpdateSortBy @FILEID
GO
