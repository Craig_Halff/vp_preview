SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[spCCG_ProjectReadyForProcessingPromo] @WBS1  varchar (32)
AS
/*
Copyright 2017 Central Consulting Group.   All rights reserved.
05/03/2017 David Springer
           Call this from a Opportunity CHANGE workflow when promo project is created.
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

BEGIN
      Update PR 
	  Set ReadyForProcessing = 'Y'
      Where WBS1 = @WBS1
END
GO
