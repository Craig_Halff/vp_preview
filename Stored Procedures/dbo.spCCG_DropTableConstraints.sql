SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_DropTableConstraints](@tableName nvarchar(255), @columnName nvarchar(255))
AS BEGIN
	declare @sql nvarchar(255)

	select @sql = 'ALTER TABLE dbo.' + @tableName + ' drop constraint ' + d.name
	from sys.tables t
	join sys.default_constraints d on d.parent_object_id = t.object_id
	join sys.columns c on c.object_id = t.object_id and c.column_id = d.parent_column_id
	where t.name = @tableName and c.name = @columnName	and t.schema_id = schema_id('dbo')

	--print @sql
	execute (@sql)
END
GO
