SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_NCProjectStatusSync]
AS
BEGIN
/****************************************************************************
    Copyright (c) 2021 Halff Associates Inc. All rights reserved.
    We like to match NC project location's status with employees org.  If an
	employee is active their corresponding NC project org locations should
	be active as well.  likewise if there are no active employees on a
	particular org the corresponding NC project locations should be Inactive.

    01/13/2021	Craig H. Anderson - Created
*****************************************************************************/
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    BEGIN TRANSACTION;
    UPDATE  p
    SET p.Status = 'I'
    FROM    dbo.PR                                                      p
    LEFT JOIN (SELECT   DISTINCT Org FROM   dbo.EM WHERE Status <> 'T') e
        ON p.Org = e.Org
    WHERE   p.ChargeType = 'H'
            AND p.Status = 'A'
            AND e.Org IS NULL
            AND RIGHT(p.Org, 3) <> '000'
            AND p.WBS1 NOT LIKE 'ZZ%';
    COMMIT TRANSACTION;

    BEGIN TRANSACTION;
    UPDATE  p
    SET p.Status = 'A'
    FROM    dbo.PR                                                      p
    LEFT JOIN (SELECT   DISTINCT Org FROM   dbo.EM WHERE Status <> 'T') e
        ON p.Org = e.Org
    WHERE   p.ChargeType = 'H'
            AND p.Status <> 'A'
            AND RIGHT(p.Org, 3) <> '000'
            AND p.WBS1 NOT LIKE 'ZZ%';
    COMMIT TRANSACTION;
END;
GO
