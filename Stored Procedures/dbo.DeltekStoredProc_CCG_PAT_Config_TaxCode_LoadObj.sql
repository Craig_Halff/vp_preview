SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_TaxCode_LoadObj] ( @VISION_LANGUAGE varchar(10))
             AS EXEC spCCG_PAT_Config_TaxCode_LoadObj @VISION_LANGUAGE
GO
