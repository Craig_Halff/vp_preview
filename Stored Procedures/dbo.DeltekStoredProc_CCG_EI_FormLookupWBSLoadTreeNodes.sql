SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_FormLookupWBSLoadTreeNodes] ( @exactMatch bit, @filter nvarchar(max), @statusFilter varchar(max), @projectFilter nvarchar(max))
             AS EXEC spCCG_EI_FormLookupWBSLoadTreeNodes @exactMatch,@filter,@statusFilter,@projectFilter
GO
