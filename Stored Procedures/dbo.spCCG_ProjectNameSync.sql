SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_ProjectNameSync] @WBS1 varchar (30)
AS
/*
Copyright (c) 2021 Central Consulting Group. All rights reserved.
09/09/2021	David Springer
			Sync the promo project names with the regular project names
*/
BEGIN
	Update p
	Set p.Name = r.Name, 
		p.LongName = r.LongName,
		p.ReadyForProcessing = 'Y'
	From PR p, PR r
	Where r.WBS1 = @WBS1
	  and r.WBS2 = ' '
	  and r.ChargeType = 'R'
	  and r.SiblingWBS1 = p.WBS1
	  and p.WBS2 = ' '
	  and p.ChargeType = 'P'

	Update px
	Set px.CustMarketingName = rx.CustMarketingName
	From PR p, ProjectCustomTabFields px, PR r, ProjectCustomTabFields rx
	Where r.WBS1 = @WBS1
	  and r.WBS2 = ' '
	  and r.WBS1 = rx.WBS1
	  and r.WBS2 = rx.WBS2
	  and r.ChargeType = 'R'
	  and r.SiblingWBS1 = p.WBS1
	  and p.WBS2 = ' '
	  and p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.ChargeType = 'P'

END;

GO
