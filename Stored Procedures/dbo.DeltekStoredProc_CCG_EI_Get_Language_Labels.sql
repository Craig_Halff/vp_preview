SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Get_Language_Labels]
	@VISION_LANGUAGE		varchar(10) = NULL
AS
BEGIN
	-- DeltekStoredProc_CCG_EI_Get_Language_Labels @VISION_LANGUAGE = 'en-US'
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT Group1, ID, Product, UICultureName, Label
		FROM CCG_Language_Labels
		WHERE Product in ('', 'EI')
			AND UICultureName = ISNULL(@VISION_LANGUAGE, UICultureName)
END;

GO
