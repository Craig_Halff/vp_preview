SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_AddlNotifications] (
	@Employee				Nvarchar(20),	/* Employee from employee-based scheduled workflow				*/
	@NotifyId				int,				/* Notifications will be uniquely considered per this id		*/
	@EmployeePICField		varchar(100),		/* The Project Info Center user-defined field to use			*/
	@RepeatInterval			int,				/* 0 = Don't repeat; 1 = repeat every 1 day, etc				*/
	@Subject				Nvarchar(120),	/* Email subject												*/
	@Body					Nvarchar(512),	/* Email body													*/
	@OverrideSenderEmail	varchar(100),		/* Can set to an email address to override default				*/
	@StatusToInclude		varchar(10),		/* Can be empty to include all project status's, or e.g. 'A,I' to exclude dormant */
	@MsgPriority			varchar(10) = NULL,	/* Optional: starts with L for Low or H for High else normal	*/
	@IncludeSupervisorAge	int = -1			/* -1=never, 0=always, X=if action is X days old (5.0)			*/
)
AS BEGIN
/*
	Copyright (c) 2017 Central Consulting Group. All rights reserved.

	02/06/2014 SPS Initial version
	03/14/2015 SPS Modifications/corrections for Paul

	select * from ccg_ei where wbs1='2002010.00'
	select emaillink from ccg_ei_config
	delete from CCG_Email_Messages
	select count(*) from CCG_Email_Messages
	select count(*) from CCG_EI_History
	exec spCCG_EI_AddlNotifications '00003', 2, 'Principal', 3, 'Delinquent invoicing - [:EM.Name]/[:PR.WBS1]','[:EM.Name] has not completed invoicing project [:PR.WBS1] after [:N] days','', 'A', null, -1
	select * from CCG_Email_Messages
	select count(*) from CCG_Email_Messages
	select count(*) from CCG_EI_History
	select * from CCG_Email_Messages
	select * from CCG_EI_History where wbs1='04114.02'

	select * from ccg_email_history
	select DATEDIFF(d, '2013-11-21 15:56:32.210', GETUTCDATE())
	update CCG_EI set DateLastNotificationSent='11/1/13' where wbs1='2002005.00'
	delete from CCG_Email_Messages
*/
	set nocount on
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	BEGIN TRY
		declare @Username					Nvarchar(32)
		declare @NotificationIntervalHours	int				-- Hours
		declare @EmailLink					varchar(1024)	-- 5.0 sps
		declare @EncTimeout					int				-- 5.0 sps
		declare @Email						Nvarchar(255)
		declare @Field						nvarchar(100)
		declare @WHERE						nvarchar(1000)
		declare @PR_JOIN					nvarchar(255) = ''
		declare @STATUS_WHERE				nvarchar(255) = ''
		declare @SupEmail					Nvarchar(255)	-- 5.0 sps
		declare @isPRField					bit = 0

		select @EmailLink=EmailLink, @EncTimeout=EncTimeout from CCG_EI_Config
		set @NotificationIntervalHours=@RepeatInterval*24
		--select @NotificationIntervalHours=(MAX(isnull(Renotify,0))*24) from CCG_EI_ConfigRights
		--print 'Max Interval: ' + Convert(varchar(10),@NotificationIntervalHours)

		if IsNull(@EmailLink,'') <> ''
		begin
			select @Username=IsNull(Min(Username),N'') from SEUser where Employee=@Employee and DisableLogin='N'
			-- Determine if this is a Cloud deployment or non-Cloud (look for database connection info)
			if CHARINDEX('database',@EmailLink) > 0 or CHARINDEX('dbup',@EmailLink) > 0						-- not Cloud
				set @EmailLink = REPLACE(@EmailLink, '[:USERENC]', 'userenc=' + dbo.fnCCG_EI_GetE(@Username))
			else
				set @EmailLink = REPLACE(@EmailLink, '[:USERENC]', 'user=' + dbo.fnCCG_HexScrambleCode(@Username))
			set @EmailLink = REPLACE(@EmailLink, '[:EDENC]',   'edenc='   + dbo.fnCCG_EI_GetE(Convert(varchar(20), DATEADD(d, @EncTimeout, getdate()),101)))
			--print @EmailLink
		end

		select @Email=Email from EM where Employee=@Employee
		select @SupEmail=Sup.Email from EM inner join EM Sup on Sup.Employee=EM.Supervisor where EM.Employee=@Employee
		--print 'Email: ' + IsNull(@Email,'') + ', Sup Email: ' + IsNull(@SupEmail,'')
		if Len(isnull(@Email,N'')) < 1 return					-- Employee has no email address

		-- Cursor to get the Vision fields for EI employee roles that receive emails:
		declare FieldCursor cursor fast_forward for
			select distinct VisionField
			from CCG_EI_ConfigRoles roles
			inner join CCG_EI_ConfigRights rights on rights.Role = roles.Role
			--?where isnull(rights.Renotify,0) > 0
		open FieldCursor
		fetch next from FieldCursor into @Field
		set @WHERE=N''
		while @@fetch_status=0
		begin
			if @WHERE=N''
				set @WHERE=N' and ('
			else
				set @WHERE=@WHERE + N' or '

			set @isPRField = case when left(@Field, 3) = 'PR.' or @Field in ('Biller', 'ProjMgr') then 1 else 0 end
			if @isPRField = 1 and @PR_JOIN = '' set @PR_JOIN = N' inner join PR on PR.WBS1=CCG_EI.WBS1 and PR.WBS2=N'' '' and PR.WBS3=N'' '' '

			set @WHERE = @WHERE + N'	(VisionField=''' + @Field + N''' and ' +
				(case when @isPRField = 1 then '' else 'PCTF.' end) + @Field + N'=N''' + @Employee + N''')'

			fetch next from FieldCursor into @Field
		end
		if @WHERE<>'' set @WHERE = @WHERE + N')'
		--print @WHERE
		close FieldCursor
		deallocate FieldCursor

		if @StatusToInclude <> ''
		begin
			set @PR_JOIN = N' inner join PR on PR.WBS1=CCG_EI.WBS1 and PR.WBS2=N'' '' and PR.WBS3=N'' '' '
			set @STATUS_WHERE = N' and PR.Status in (''' + Replace(Replace(@StatusToInclude,' ',''),',',''',''') + N''')'
		end

		declare @GUID uniqueidentifier, @nowutc datetime, @sql nvarchar(3000), @sqlCC nvarchar(500)
		set @nowutc=GETUTCDATE()
		set @GUID = NEWID()
		--print @GUID
		--print '@EmployeePICField=' + @EmployeePICField + ', @NotifyId=' + cast(@NotifyId as varchar) + ', @RepeatInterval=' + Cast(@RepeatInterval as varchar)
		set @sql = N'insert into CCG_EI_Renotify (ID,InsertDate,Employee,WBS1,InvoiceStage,DateChanged,ChangedBy,' +
			N'		DateLastNotificationSent,EmailSubjectBatch,EmailSubject,EmailMessage,CCSender,MessageDelay,NotifyId,EmployeePICField) ' +
			N' select ''' + CAST(@GUID as varchar(36)) + ''', ''' + Cast(@nowutc as varchar) + ''', ' +
			N' N''' + @Employee + ''', ' +
			N' CCG_EI.WBS1, CCG_EI.InvoiceStage, CCG_EI.DateChanged, CCG_EI.ChangedBy, r.DateLastNotificationSent, ' +
			N' N''' + @Subject + ''',N''' + @Subject + ''',N''' + Replace(@Body,'[:N]',CAST(@RepeatInterval as varchar)) + ''',' +
			N' rights.CCSender, 1 as MessageDelay, ' + CAST(@NotifyId as varchar) + ',''' + @EmployeePICField + ''' ' +
			N' from CCG_EI ' +
			@PR_JOIN +
			N' inner join ProjectCustomTabFields PCTF on PCTF.WBS1=CCG_EI.WBS1 and PCTF.WBS2='' '' and PCTF.WBS3='' '' ' +
			N' inner join CCG_EI_ConfigRights rights on rights.StageFlow=PCTF.CustInvoiceStageFlow and rights.Stage=CCG_EI.InvoiceStage ' +
			N' inner join CCG_EI_ConfigRoles roles on roles.Role=rights.Role and roles.VisionField=''' + @EmployeePICField + '''' +
			N' left join ( ' +
			N'   select WBS1,Employee,Max(DateLastNotificationSent) as DateLastNotificationSent ' +
			N'   from CCG_EI_Renotify ' +
			N'   where NotifyId=' + Cast(@NotifyId as varchar) + ' and Employee=N''' + @Employee + ''' and EmployeePICField=''' + @EmployeePICField + ''' ' +
			N'   group by WBS1,Employee ' +
			N' ) r on r.WBS1=CCG_EI.WBS1 and r.Employee=N''' + @Employee + ''' ' +
			N' where isnull(rights.Renotify,0) > 0 AND rights.CanModify <> ''Y''' +
			@STATUS_WHERE + @WHERE +
			N' group by ' +
			N' CCG_EI.WBS1, CCG_EI.InvoiceStage, CCG_EI.DateChanged, CCG_EI.ChangedBy, r.DateLastNotificationSent, rights.CCSender ' +
			/* The stage itself has to be more than X days old: */
			N' having DateDiff(hh, CCG_EI.DateChanged, getutcdate()) >= ' + CAST(@RepeatInterval*24 as varchar) +
			/* And the last renotification has to be more than X days old (or not yet notified): */
			N'    and DateDiff(hh, IsNull(Max(r.DateLastNotificationSent),''1/1/2010''), getutcdate()) >= ' + CAST(@RepeatInterval*24 as varchar)
		--print @sql
		exec sp_executesql @sql

		if @IncludeSupervisorAge > -1
			set @sqlCC = 'case when CCSender = ''Y'' then Sender.Email + '';'' else '''' end'
		else
			set @sqlCC = 'case when CCSender = ''Y'' then Sender.Email else '''' end'

		/* Check if we're to include the employee's supervisor on the email: */
		if IsNull(@SupEmail,'') <> '' and @IncludeSupervisorAge = 0
			set @sqlCC = @sqlCC + ' + N''' + @SupEmail + ''''
		else if IsNull(@SupEmail,N'') <> N'' and @IncludeSupervisorAge > 0
			set @sqlCC = @sqlCC + ' + case When DATEDIFF(d, DateChanged, GETUTCDATE()) >= ' + Cast(@IncludeSupervisorAge as varchar) +
				' Then N''' + @SupEmail + ''' Else N'''' End'
		-- print @sqlCC
		set @sql = N'insert into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, BCCList, GroupSubject, [Subject], Body,MaxDelay,Priority) ' +
			N' select distinct ''Electronic Invoicing.'', case When Len(''' + @OverrideSenderEmail + ''')>1 Then ''' + @OverrideSenderEmail + ''' Else Sender.Email End, ' +
			N' ToEM.Email, N' + @sqlCC + ', null, ' +
			N' Replace(Replace(Replace(EmailSubjectBatch, ''[:PR.WBS1]'', CCG_EI_Renotify.WBS1), ''[:PR.Name]'', PR.Name), ''[:EM.Name]'', AboutEM.Lastname+'', '' +AboutEM.Firstname), ' +
			N' Replace(Replace(Replace(EmailSubject,      ''[:PR.WBS1]'', CCG_EI_Renotify.WBS1), ''[:PR.Name]'', PR.Name), ''[:EM.Name]'', AboutEM.Lastname+'', '' +AboutEM.Firstname), ' +
			N' ''Project #: '' + PR.WBS1 + '', Name: '' + PR.Name + ''<br>'' + AboutEM.FirstName + '' '' + AboutEM.LastName + N'' '' + EmailMessage+''' + ' ['' + ' +
			N'	Cast(DATEDIFF(d, case When IsNull(DateLastNotificationSent,DateChanged) < DateChanged Then DateChanged Else IsNull(DateLastNotificationSent,DateChanged) End, getutcdate()) as varchar) + '' days outstanding]'', ' +
			N' MessageDelay,case when LEFT(UPPER(''' + isnull(@MsgPriority,'NULL') + '''),1)=''N'' then null else ''' + isnull(@MsgPriority,'NULL') + ''' end ' +
			N' from CCG_EI_Renotify ' +
			N' left join PR on PR.WBS1=CCG_EI_Renotify.WBS1 and PR.WBS2 = '' '' and PR.WBS3='' ''' +
			case When Upper(Left(@EmployeePICField,4))='CUST' Then N'left join ProjectCustomTabFields PCTF on PCTF.WBS1=CCG_EI_Renotify.WBS1 and PCTF.WBS2='' '' and PCTF.WBS3='' '' ' Else '' End +
			N' left join EM Sender on Sender.Employee=CCG_EI_Renotify.ChangedBy ' +
			N' left join EM AboutEM on AboutEM.Employee=CCG_EI_Renotify.Employee ' +
			N' inner join EM ToEM on ToEM.Employee=' + case When Upper(Left(@EmployeePICField,4))='CUST' Then N'PCTF.' Else N'PR.' End + @EmployeePICField +
			N' where ID=''' + CAST(@GUID as varchar(36)) + ''''
		--print @sql
		exec sp_executesql @sql

		-- Reset CCG_EI_Renotify.DateLastNotificationDate:
		set @sql = 'update CCG_EI_Renotify set DateLastNotificationSent=getutcdate() ' +
			N' where ID=''' + CAST(@GUID as varchar(36)) + ''''
		-- print @sql
		exec sp_executesql @sql

		-- Email service only logs in CCG_EMAIL_History when email is sent, CCG_EI_History entries needed for reporting
		-- Add history entry:
		set @sql = N'insert into CCG_EI_History (ActionTaken, ActionDate, ActionTakenBy, ActionRecipient, WBS1, InvoiceStage, PriorInvoiceStage) ' +
				N' select ''Email Reminder Sent'',''' + Cast(@nowutc as varchar) + ''', ' +
				N' ''Re-Notification Script'', N''' + @Employee + ''', WBS1, InvoiceStage, null ' +
				N' from CCG_EI_Renotify ' +
				N' where ID=''' + CAST(@GUID as varchar(36)) + ''''
		--print @sql
		exec sp_executesql @sql

		return 0
	END TRY
	BEGIN CATCH
		declare @res int
		set @res = ERROR_NUMBER()
		print Cursor_Status('variable', 'FieldCursor')
		if Cursor_Status('variable', 'FieldCursor') > -1
			close FieldCursor
		if Cursor_Status('variable', 'FieldCursor') > -2
			deallocate FieldCursor
		return @res
	END CATCH
END
GO
