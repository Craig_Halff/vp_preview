SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AddIndustryDefaultsAEdeDE]
AS
BEGIN

PRINT ' '
PRINT 'Adding AE Industry Defaults for de-DE'

BEGIN TRANSACTION

DELETE FROM BTLaborCatsDescriptions WHERE UICultureName = 'de-DE';
--..........................................................................................v(50)
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 1, 'de-DE','Hauptgeschäftsführer'         WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 1);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 2, 'de-DE','Projektmanager'   WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 2);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 3, 'de-DE','Leitender Berater' WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 3);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 4, 'de-DE','Leitender Architekt'  WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 4);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 5, 'de-DE','Oberingenieur'   WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 5);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 6, 'de-DE','Ingenieur'          WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 6);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 7, 'de-DE','Architekt'         WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 7);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 8, 'de-DE','Designer'          WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 8);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 9, 'de-DE','Technischer Zeichner'      WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 9);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 10,'de-DE','Vermesser'          WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 10);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 11,'de-DE','Verwaltung'    WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 11);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 12,'de-DE','Marketing'         WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 12);

DELETE FROM CFGActivitySubjectData WHERE UICultureName = 'de-DE';
--..................................................................................v(50)
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Unangemeldeter Anruf'               ,'de-DE',3 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Erste Besprechung'        ,'de-DE',1 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Vorstellungs-E-Mail'      ,'de-DE',4 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Qualifizierungsanruf'      ,'de-DE',2 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Social Media-Erwähnung'    ,'de-DE',6 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Vordenker-E-Mail','de-DE',5 ,'N');

DELETE FROM CFGActivityTypeDescriptions WHERE UICultureName = 'de-DE';
--.........................................................................................................v(15)
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'EMail'     ,'de-DE','E-Mail'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'EMail');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Event'     ,'de-DE','Ereignis'          ,4 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Event');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Mailing'   ,'de-DE','Mailing'        ,2 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Mailing');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Meeting'   ,'de-DE','Besprechung'        ,3 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Meeting');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Merge'     ,'de-DE','Dokument erstellen',5 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Merge');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Milestone' ,'de-DE','Meilenstein'      ,6 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Milestone');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Phone Call','de-DE','Telefonanruf'     ,7 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Phone Call');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Task'      ,'de-DE','Aufgabe'           ,8 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Task');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Touchpoint','de-DE','Berührungspunkt'     ,9 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Touchpoint');

DELETE FROM CFGARLedgerHeadingsDescriptions WHERE UICultureName = 'de-DE';
--...............................................................................................v(12)
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 1,'de-DE','Honorare'       WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 1);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 2,'de-DE','Erstattungsf.'     WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 2);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 3,'de-DE','Berater' WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 3);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 4,'de-DE','Vorauszahlung'   WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 4);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 5,'de-DE','Steuern'      WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 5);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 6,'de-DE','Zins'   WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 6);

DELETE FROM CFGAwardTypeDescriptions WHERE UICultureName = 'de-DE';
--............................................................................................v(255)
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '1','de-DE','IDIQ - GWAC'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 1);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '2','de-DE','IDIQ - Agenturspezifisch',2 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 2);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '4','de-DE','BPA'                   ,3 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 4);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '5','de-DE','Sonstige'                 ,4 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 5);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '6','de-DE','Aufgabe/Lieferauftrag' ,5 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 6);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '7','de-DE','Unbestimmt'          ,6 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 7);

DELETE FROM CFGBillMainDescriptions WHERE UICultureName = 'de-DE';
--..............................................................................................................................................................................................................v(15)..v(15)....v(15).........v(15)......v(15)...v(15).....v(15)..v(15)
INSERT INTO CFGBillMainDescriptions (Company,UICultureName,FooterMessage,FeeLabel,LabLabel,ConLabel,ExpLabel,UnitLabel,AddOnLabel,TaxLabel,InterestLabel,OvtIndicator) SELECT CFGMainData.Company,'de-DE',NULL,'Honorar' ,'Aufwand' ,'Berater' ,'Ausgaben' ,'Einheit' ,'Zusatz' ,'Steuer' ,'Zins' ,'Ovt' FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGBillMainData WHERE Company = CFGMainData.Company);

DELETE FROM CFGCampaignActionDescriptions WHERE UICultureName = 'de-DE';
--..................................................................................................v(50)
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '01','de-DE','Inhaltsentwicklung' ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '01');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '02','de-DE','Einladung senden' ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '02');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '03','de-DE','E-Mail-Blast senden',3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '03');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '04','de-DE','Folgeaktion'       ,4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '04');

DELETE FROM CFGCampaignAudienceDescriptions WHERE UICultureName = 'de-DE';
--....................................................................................................v(50)
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '01','de-DE','Leitende Angestellte'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '01');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '02','de-DE','Finanzwesen'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '02');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '03','de-DE','Marketing'           ,3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '03');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '04','de-DE','Geschäftsentwicklung',4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '04');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '05','de-DE','Projektmanager'    ,5 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '05');

DELETE FROM CFGCampaignObjectiveDescriptions WHERE UICultureName = 'de-DE';
--.....................................................................................................v(50)
INSERT INTO CFGCampaignObjectiveDescriptions (Code,UICultureName,Description,Seq) SELECT '01','de-DE','Generierung neuer Geschäftsabschlüsse' ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignObjectiveData WHERE Code = '01');
INSERT INTO CFGCampaignObjectiveDescriptions (Code,UICultureName,Description,Seq) SELECT '02','de-DE','Erweiterung bestehender Geschäfte',2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignObjectiveData WHERE Code = '02');

DELETE FROM CFGCampaignStatusDescriptions WHERE UICultureName = 'de-DE';
--..................................................................................................v(50)
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','de-DE','Aktiv'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '01');
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','de-DE','Planung' ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '02');
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','de-DE','Abgeschlossen',3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '03');

DELETE FROM CFGCampaignTypeDescriptions WHERE UICultureName = 'de-DE';
--................................................................................................v(50)
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','de-DE','E-Mail'      ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '01');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','de-DE','Direct-Mail',2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '02');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','de-DE','Webinar'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '03');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','de-DE','Fachmesse'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '04');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '05','de-DE','Meetup'     ,5 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '05');

DELETE FROM CFGClientCurrentStatusDescriptions WHERE UICultureName = 'de-DE';
--...............................................................................................................v(50)
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Existing','de-DE','Bestehend',1 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Existing');
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Former'  ,'de-DE','Früherer'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Former');
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Prospect','de-DE','Potenzieller Kunde',2 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Prospect');

DELETE FROM CFGClientRelationshipDescriptions WHERE UICultureName = 'de-DE';
--......................................................................................................v(50)
INSERT INTO CFGClientRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01','de-DE','Wettbewerber',1 WHERE EXISTS (SELECT 'x' FROM CFGClientRelationshipData WHERE Code = '01');

DELETE FROM CFGClientRoleDescriptions WHERE UICultureName = 'de-DE';
--....................................................................................................v(50)
INSERT INTO CFGClientRoleDescriptions (Code,UICultureName,Description,Seq) SELECT '01'      ,'de-DE','Subauftragnehmer',2 WHERE EXISTS (SELECT 'x' FROM CFGClientRoleData WHERE Code = '01');
INSERT INTO CFGClientRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'sysOwner','de-DE','Kunde'       ,1 WHERE EXISTS (SELECT 'x' FROM CFGClientRoleData WHERE Code = 'sysOwner');

DELETE FROM CFGClientTypeDescriptions WHERE UICultureName = 'de-DE';
--..............................................................................................v(50)
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','de-DE','Ausbildung'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '01');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','de-DE','Unternehmen'          ,2 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '02');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','de-DE','Finanz- und Bankwesen' ,3 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '03');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','de-DE','Kranken- und Gesundheitsversorgung' ,4 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '04');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '05','de-DE','Gastgewerbe'         ,5 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '05');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '06','de-DE','Informationsservices',6 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '06');

DELETE FROM CFGCompetitionTypeDescriptions WHERE UICultureName = 'de-DE';
--...................................................................................................v(255)
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '37','de-DE','8(a) Rücklage'                                       ,2  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '37')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '38','de-DE','SDB Rücklage'                                        ,3  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '38')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '39','de-DE','Kleinunternehmen-Rücklage'                                  ,4  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '39')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '40','de-DE','Voll und offen / Unbeschränkt'                         ,5  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '40')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '44','de-DE','Hubzone'                                              ,6  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '44')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '45','de-DE','Dienst im Eigentum eines Kriegsversehrten'        ,7  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '45')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '47','de-DE','Unbestimmt'                                         ,1  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '47')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '48','de-DE','Sonstige'                                                ,8  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '48')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '64','de-DE','Einzige Quelle'                                          ,9  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '64')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '65','de-DE','Rücklage für von Frauen geführtem Kleinunternehmen'                 ,10 WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '65')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '66','de-DE','Von Veteran geführter Kleinbetrieb'                         ,11 WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '66')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '67','de-DE','Von benachteiligter Frau geführtes Kleinunternehmen',12 WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '67')

DELETE FROM CFGContactRelationshipDescriptions WHERE UICultureName = 'de-DE';
--.......................................................................................................v(50)
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01','de-DE','Früherer Kunde'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '01');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '02','de-DE','Früherer Kollege' ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '02');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '03','de-DE','Branchenkollege'   ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '03');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '04','de-DE','Persönlicher Kontakt',4 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '04');

DELETE FROM CFGContactRoleDescriptions WHERE UICultureName = 'de-DE';
--.......................................................................................................v(50)
INSERT INTO CFGContactRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Contractor','de-DE','Subauftragnehmer',1 WHERE EXISTS (SELECT 'x' FROM CFGContactRoleData WHERE Code = 'Contractor');
INSERT INTO CFGContactRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'sysOwner'  ,'de-DE','Eigentümer'        ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactRoleData WHERE Code = 'sysOwner');

DELETE FROM CFGContactSourceDescriptions WHERE UICultureName = 'de-DE';
--...................................................................................................v(50)
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '01','de-DE','Kundenreferenz'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '01');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '02','de-DE','Ereignis'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '02');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '03','de-DE','Liste'              ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '03');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '04','de-DE','Persönlicher Kontakt'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '04');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '05','de-DE','Social Media'      ,5 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '05');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '06','de-DE','Vordenker',6 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '06');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '07','de-DE','Website'           ,7 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '07');

DELETE FROM CFGContactTitleDescriptions WHERE UICultureName = 'de-DE';
--.................................................................................................v(50)
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Director' ,'de-DE','Direktor' ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Director');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Executive','de-DE','Leitender Angestellter',1 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Executive');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Finance'  ,'de-DE','Finanzwesen'  ,5 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Finance');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Marketing','de-DE','Marketing',6 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Marketing');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Sales'    ,'de-DE','Vertrieb'    ,4 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Sales');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'VP'       ,'de-DE','VP'       ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'VP');

DELETE FROM CFGContactTypeDescriptions WHERE UICultureName = 'de-DE';
--..........................................................................................v(100)
INSERT INTO CFGContactTypeDescriptions (Code,UICultureName,Description) SELECT 'C','de-DE','Kunde' WHERE EXISTS (SELECT 'x' FROM CFGContactTypeData WHERE Code = 'C');

DELETE FROM CFGContractStatusDescriptions WHERE UICultureName = 'de-DE';
--..................................................................................................v(50)
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','de-DE','Interne Prüfung'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '01');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','de-DE','An Kunden gesendet'   ,2 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '02');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','de-DE','Verhandlung'      ,3 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '03');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '04','de-DE','Rechtliche Überprüfung'     ,4 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '04');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '05','de-DE','Unterschrieben und ausgeführt',5 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '05');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '06','de-DE','Verloren/Kein Abschluss'     ,6 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '06');

DELETE FROM CFGContractTypeDescriptions WHERE UICultureName = 'de-DE';
--................................................................................................v(50)
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','de-DE','Original'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '01');
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','de-DE','Änderungsauftrag'       ,2 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '02');
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','de-DE','Zusätzliche Dienstleistungen',3 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '03');
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','de-DE','Einverständniserklärung',4 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '04');

DELETE FROM CFGCubeTranslationDescriptions WHERE UICultureName = 'de-DE';
--..............................................................................................................................................v(200)
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'U'                       ,'de-DE','Unbekannt');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'UM'                      ,'de-DE','Unbekannter Monat');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'H'                       ,'de-DE','Verlauf');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('YesNo'          ,'Y'                       ,'de-DE','Ja');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('YesNo'          ,'N'                       ,'de-DE','Nein');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('Status'         ,'A'                       ,'de-DE','Aktiv');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('Status'         ,'I'                       ,'de-DE','Inaktiv');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'1-Asset'                 ,'de-DE','Aktivposten');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'2-Liability'             ,'de-DE','Verbindlichkeit');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'3-NetWorth'              ,'de-DE','Nettowert');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'4-Revenue'               ,'de-DE','Umsatz');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'5-Reimbursable'          ,'de-DE','Erstattungsfähig');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'6-ReimbursableConsultant','de-DE','Erstattungsfähiger Berater');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'7-Direct'                ,'de-DE','Direkt');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'8-DirectConsultant'      ,'de-DE','Direkter Berater');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'9-Indirect'              ,'de-DE','Indirekt');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'10-OtherCharges'         ,'de-DE','Sonstige Gebühren');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'U-Unknown'               ,'de-DE','Unbekannt');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ExpenseType'    ,'5-ReimbursableOther'     ,'de-DE','Sonstige erstattungsfähige');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ExpenseType'    ,'7-DirectOther'           ,'de-DE','Sonstige direkte');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ReportType'     ,'BalanceSheet'            ,'de-DE','Bilanz');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ReportType'     ,'IncomeStatement'         ,'de-DE','Gewinn- und Verlustrechnung');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'1-Asset','de-DE'         ,'Other Assets');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'2-Liability'             ,'de-DE','Andere Verbindlichkeiten');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'3-NetWorth'              ,'de-DE','Anderer Nettowert');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'4-Revenue'               ,'de-DE','Sonstiger Umsatz');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'5-Reimbursable'          ,'de-DE','Sonstige erstattungsfähige');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'6-Reimbursable'          ,'de-DE','Sonstige erstattungsfähige');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'7-Direct'                ,'de-DE','Sonstige direkte');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'8-Direct'                ,'de-DE','Sonstige direkte');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'9-Indirect'              ,'de-DE','Sonstige indirekte');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'10-OtherCharges'         ,'de-DE','Sonstige Gebühren');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'B'                       ,'de-DE','Abrechnungsfähig');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'H'                       ,'de-DE','Zurückgestellt');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'W'                       ,'de-DE','Abzuschreiben');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'X'                       ,'de-DE','Abgeschrieben');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'D'                       ,'de-DE','Zum Löschen vorgemerkt');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'N'                       ,'de-DE','Nicht abrechnungsfähig');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'F'                       ,'de-DE','Endgültig in Rechnung gestellt');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'T'                       ,'de-DE','Umgebucht');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'R'                       ,'de-DE','Teilweise zurückgestellt/freigegeben');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'M'                       ,'de-DE','Geändert');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'O'                       ,'de-DE','Gelöscht');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AP'                      ,'de-DE','Kreditorenbelege');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BA'                      ,'de-DE','Verarbeitung der Leistungsstunden');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BE'                      ,'de-DE','Abrechnungskostentransfers');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BL'                      ,'de-DE','Abrechnung von Arbeitstransfers');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BU'                      ,'de-DE','Umbuchungen von Abrechnungs[Unit]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CD'                      ,'de-DE','Barausgaben');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CP'                      ,'de-DE','[WBS1] [Organization] konvertieren');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CR'                      ,'de-DE','Bareinnahmen');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CV'                      ,'de-DE','Kreditorenaufwendungen');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EP'                      ,'de-DE','[Employee]-Zahlungsverarbeitung');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','ER'                      ,'de-DE','[Employee]-Rückzahlungen');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EX'                      ,'de-DE','[Employee]-Ausgaben');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IN'                      ,'de-DE','Rechnungen');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','JE'                      ,'de-DE','Protokolleinträge');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','KC'                      ,'de-DE','Schlüsselkonvertierung');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','LA'                      ,'de-DE','Arbeitsanpassungen');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','LG'                      ,'de-DE','Erträge/Verluste und Aufwertungen');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','MI'                      ,'de-DE','Verschiedene Ausgaben');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PP'                      ,'de-DE','Kreditorenzahlungsverarbeitung');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PR'                      ,'de-DE','Ausdrucke und Reproduktionen');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PY'                      ,'de-DE','Lohn- und Gehaltsabrechnung');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','RG'                      ,'de-DE','Umsatzgenerierung');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','TS'                      ,'de-DE','Stundenabrechnungen');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','UN'                      ,'de-DE','[Units]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','UP'                      ,'de-DE','[Units] pro [WBS1]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AL'                      ,'de-DE','Schlüsselkonvertierungsbuchung, Eröffnung einer neuen Periode /W2 Quartal / Leistungsabgrenzungsjahr, Abschluss einer Periode');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AX'                      ,'de-DE','Eingelöster AP-Gutschein');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CN'                      ,'de-DE','Konvertierte Beraterausgaben(en)');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CT'                      ,'de-DE','Transaktion, die angibt, wann die Umstellung von einem früheren Release durchgeführt wurde');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EC'                      ,'de-DE','Konvertierte Zeitanalyse Anpassung(en)');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IH'                      ,'de-DE','Konvertierte Rechnung');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IX'                      ,'de-DE','Konvertierte Rechnung(en)');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','JX'                      ,'de-DE','MCFMS-Konvertierung');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PX'                      ,'de-DE','Konvertierte AP-Zahlungsabwicklung');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','RX'                      ,'de-DE','MCFMS-Konvertierung');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','XD'                      ,'de-DE','Konvertierte Kontodaten');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','XE'                      ,'de-DE','Konvertierte [WBS1]ausgaben');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HA'                      ,'de-DE','[Account]Guthabenverlauf');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HB'                      ,'de-DE','[Employee] – Verlauf der Leistungsabgrenzung');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HE'                      ,'de-DE','[WBS1] – Verlauf der Arbeits- und Ausgabenentwicklung');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HL'                      ,'de-DE','[WBS1] – Verlauf der Arbeits- und Ausgabenentwicklung');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HU'                      ,'de-DE','Nicht in Rechnung gestellter Umsatz');

DELETE FROM CFGEMDegreeDescriptions WHERE UICultureName = 'de-DE';
--............................................................................................v(50)
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','de-DE','Bachelor of Science',1 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '01');
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','de-DE','Bachelor of Arts'   ,2 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '02');
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','de-DE','Masters Degree'            ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '03');

DELETE FROM CFGEmployeeRelationshipDescriptions WHERE UICultureName = 'de-DE';
--..............................................................................................................v(50)
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01'      ,'de-DE','Früherer Arbeitgeber'            ,3 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '01');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '02'      ,'de-DE','Früherer Kollege'            ,2 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '02');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '03'      ,'de-DE','Persönlicher Kontakt'           ,5 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '03');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '04'      ,'de-DE','Allgemeine Branchenverbindung',4 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '04');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT 'SysOwner','de-DE','Eigentümer'                      ,1 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = 'SysOwner');

DELETE FROM CFGEmployeeRoleDescriptions WHERE UICultureName = 'de-DE';
--........................................................................................................v(50)
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Admin'     ,'de-DE','Verwaltung'         ,7  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Admin');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'AirQuality','de-DE','Luftqualitätsspezialist' ,8  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'AirQuality');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Architect' ,'de-DE','Architekt'              ,9  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Architect');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Associate' ,'de-DE','Mitarbeiter'              ,10 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Associate');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Biologist' ,'de-DE','Biologe'              ,11 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Biologist');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'CADDTechi' ,'de-DE','CADD-Techniker'        ,12 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'CADDTechi');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'CivilEngr' ,'de-DE','Bauingenieur'         ,13 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'CivilEngr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ConstIns'  ,'de-DE','Bauinspekteur' ,14 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ConstIns');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ConstMgr'  ,'de-DE','Bauleiter'   ,15 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ConstMgr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ElectEngr' ,'de-DE','Elektrotechnikingenieur'    ,16 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ElectEngr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'EntitleSpl','de-DE','Spezialist für Leistungsansprüche' ,17 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'EntitleSpl');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'EnvirPlnr' ,'de-DE','Umweltschutzplaner'  ,18 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'EnvirPlnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Estimator' ,'de-DE','Schätzer'              ,19 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Estimator');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'GISSpecial','de-DE','GIS-Spezialist'         ,20 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'GISSpecial');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Hydrolgst' ,'de-DE','Hydrologe'            ,21 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Hydrolgst');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'InfoTech'  ,'de-DE','Informationstechnologie' ,22 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'InfoTech');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'InteDsnr'  ,'de-DE','Inneneinrichter'      ,23 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'InteDsnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'LandArch'  ,'de-DE','Landschaftsarchitekt'    ,24 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'LandArch');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'MechEng'   ,'de-DE','Maschinenbauingenieur'    ,25 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'MechEng');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'MEPCoordin','de-DE','MEP-Koordinator'        ,26 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'MEPCoordin');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Planner'   ,'de-DE','Planer'                ,27 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Planner');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjAdmin' ,'de-DE','Projektverwalter'  ,28 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjAdmin');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjArch'  ,'de-DE','Projektarchitekt'      ,29 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjArch');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjDsnr'  ,'de-DE','Projektdesigner'       ,30 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjDsnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjEng'   ,'de-DE','Projektingenieur'       ,31 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjEng');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjPlnr'  ,'de-DE','Projektplaner'        ,32 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjPlnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjSvor'  ,'de-DE','Projektvermessungstechniker'       ,33 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjSvor');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'QAQCMgr'   ,'de-DE','QA/QC-Manager'          ,34 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'QAQCMgr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'SpecWriter','de-DE','Spezifikationsverfasser'  ,35 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'SpecWriter');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'StructEng' ,'de-DE','Hochbauingenieur'    ,36 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'StructEng');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Surveyor'  ,'de-DE','Vermesser'               ,37 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Surveyor');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TechEditor','de-DE','Technischer Editor'       ,38 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TechEditor');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TrafficSpe','de-DE','Spezialist für Verkehr'     ,39 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TrafficSpe');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TransEngr' ,'de-DE','Transportingenieur',40 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TransEngr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TransPlnr' ,'de-DE','Transportplaner' ,41 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TransPlnr');

DELETE FROM CFGEmployeeTitleDescriptions WHERE UICultureName = 'de-DE';
--..................................................................................................v(50)
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Architect','de-DE','Architekt'              ,1  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Architect');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CFO'      ,'de-DE','Finanzvorstand',2  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CFO');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CivilEng' ,'de-DE','Bauingenieur'         ,3  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CivilEng');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Designer' ,'de-DE','Designer'               ,4  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Designer');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Director' ,'de-DE','Direktor'               ,5  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Director');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'EE'       ,'de-DE','Umweltingenieur' ,8  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'EE');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'EIT'      ,'de-DE','Ingenieur in der Ausbildung'   ,7  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'EIT');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Engineer' ,'de-DE','Ingenieur'               ,6  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Engineer');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'GE'       ,'de-DE','Geotechnischer Ingenieur'  ,10 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'GE');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'GM'       ,'de-DE','Geschäftsführer'        ,9  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'GM');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'ID'       ,'de-DE','Inneneinrichter'      ,12 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'ID');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'LA'       ,'de-DE','Landschaftsarchitekt'    ,13 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'LA');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'PIC'      ,'de-DE','Hauptverantwortlicher'    ,17 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'PIC');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Planner'  ,'de-DE','Planer'                ,14 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Planner');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'PM'       ,'de-DE','Projektmanager'        ,18 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'PM');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'President','de-DE','Präsident'              ,15 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'President');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Principal','de-DE','Hauptgeschäftsführer'              ,16 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Principal');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'SUP'      ,'de-DE','Vorgesetzter'            ,20 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'SUP');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'TC'       ,'de-DE','Schulungsberater'    ,19 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'TC');

DELETE FROM CFGEmployeeTypeDescriptions WHERE UICultureName = 'de-DE';
--.....................................................................................v(30)
INSERT INTO CFGEmployeeTypeDescriptions (Type,UICultureName,Label) SELECT 'E','de-DE','Mitarbeiter'  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTypeData WHERE Type = 'E');
INSERT INTO CFGEmployeeTypeDescriptions (Type,UICultureName,Label) SELECT 'P','de-DE','Hauptgeschäftsführer' WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTypeData WHERE Type = 'P');

DELETE FROM CFGEMRegistrationDescriptions WHERE UICultureName = 'de-DE';
--...................................................................................................v(70)
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'AIA','de-DE','Amerikanisches Institut für Architekten',2 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'AIA');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'EIT','de-DE','Ingenieur in der Ausbildung'            ,5 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'EIT');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'PE' ,'de-DE','Professioneller Ingenieur'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'PE');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'PLS','de-DE','Professioneller Landvermesser'      ,4 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'PLS');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'RLS','de-DE','Registrierter Landvermesser'        ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'RLS');

DELETE FROM CFGEMSkillDescriptions WHERE UICultureName = 'de-DE';
--.............................................................................................v(70)
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '01'  ,'de-DE','Verwaltung'                          ,1  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '01SF','de-DE','Akustikingenieur'                     ,2  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '02'  ,'de-DE','Architekten'                              ,3  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '02');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '03SF','de-DE','Luftbildfotograf'                     ,4  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '03SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '04SF','de-DE','Luftfahrtingenieur'                   ,5  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '04SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '05'  ,'de-DE','Technische Zeichner'                               ,6  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '05');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '05SF','de-DE','Archäologe'                            ,7  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '05SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '07'  ,'de-DE','Bauinspektoren'                 ,8  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '07');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '07SF','de-DE','Biologe'                               ,9  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '07SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '08'  ,'de-DE','Schätzer'                              ,10 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '08');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '08SF','de-DE','CADD-Techniker'                         ,11 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '08SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '09SF','de-DE','Kartograph'                            ,12 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '09SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '10'  ,'de-DE','Sanitärtechniker'                      ,13 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '10');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '11'  ,'de-DE','Ökonomen'                              ,14 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '11');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '11SF','de-DE','Chemiker'                                 ,15 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '11SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '12'  ,'de-DE','Ökologe'                              ,16 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '12');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '13SF','de-DE','Kommunikationstechniker'                 ,17 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '13SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '14'  ,'de-DE','Chemieingenieure'                      ,18 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '14');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '14SF','de-DE','Computerprogrammierer'                     ,19 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '14SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '15'  ,'de-DE','Bauingenieure'                         ,20 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '15');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '16SF','de-DE','Bauleiter'                    ,21 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '16SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '17SF','de-DE','Korrosionsingenieur'                      ,22 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '17SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '18SF','de-DE','Kosteningenieur/Schätzer'                 ,23 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '18SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '19'  ,'de-DE','Elektroingenieure'                    ,24 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '19');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '21'  ,'de-DE','Vermesser'                               ,25 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '21');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '22SF','de-DE','Elektronik-Ingenieur'                    ,26 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '22SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '23SF','de-DE','Umweltingenieur'                  ,27 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '23SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '24SF','de-DE','Umweltwissenschaftler'                 ,28 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '24SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '25SF','de-DE','Brandschutzingenieur'                ,29 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '25SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '26SF','de-DE','Kriminaltechniker'                       ,30 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '26SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '27'  ,'de-DE','Maschinenbauingenieure'                    ,31 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '27');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '27SF','de-DE','Fundament/Geotechnischer Ingenieur'        ,32 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '27SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '28'  ,'de-DE','Bergbauingenieure'                        ,33 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '28');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '28SF','de-DE','Geodätischer Vermesser'                       ,34 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '28SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '29SF','de-DE','Spezialist für geografische Informationssysteme',35 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '29SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '30'  ,'de-DE','Bodeningenieure'                         ,36 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '30');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '31'  ,'de-DE','Spezifikationsverfasser'                  ,37 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '31');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '31SF','de-DE','Planer von Gesundheitseinrichtungen'                 ,38 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '31SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '32'  ,'de-DE','Bauingenieure'                    ,39 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '32');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '32SF','de-DE','Hydraulik-Ingenieur'                      ,40 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '32SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '33'  ,'de-DE','Transportingenieure'                ,41 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '33SF','de-DE','Hydrographischer Vermesser'                   ,42 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '35SF','de-DE','Wirtschaftsingenieur'                     ,43 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '36'  ,'de-DE','Geologe'                               ,44 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '36SF','de-DE','Arbeitshygieniker/in'                    ,45 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '38SF','de-DE','Landvermesser'                           ,46 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '40'  ,'de-DE','Hydrologen'                            ,47 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '40SF','de-DE','Werkstoffingenieur'                      ,48 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '41SF','de-DE','Ingenieur für Materialflusstechnik'             ,49 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '42'  ,'de-DE','Inneneinrichter'                      ,50 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '43'  ,'de-DE','Landschaftsarchitekt'                    ,51 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '44'  ,'de-DE','Ozeanographen'                          ,52 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '45'  ,'de-DE','Planer: Städtisch/Regional'                ,53 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '45SF','de-DE','Bildübersetzer/in'                       ,54 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '46SF','de-DE','Fotogrammetriker'                        ,55 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '48SF','de-DE','Projektmanager'                         ,56 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '49SF','de-DE','Spezialist für Fernerkundung'               ,57 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '50SF','de-DE','Risikoanalyst'                           ,58 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '51SF','de-DE','Sicherheits-/Arbeitsschutzingenieur'     ,59 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '53SF','de-DE','Planer'                               ,60 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '54SF','de-DE','Sicherheitstechniker'                     ,61 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '58SF','de-DE','Techniker/Analyst'                      ,62 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '59SF','de-DE','Toxikologe'                            ,63 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '61SF','de-DE','Wertschöpfungsingenieur'                          ,64 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '62SF','de-DE','Wasserwirtschaftlicher Ingenieur'                ,65 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');

DELETE FROM CFGEMSkillLevelDescriptions WHERE UICultureName = 'de-DE';
--.............................................................................................v(50)
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 1,'de-DE','Grundlagen'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 1);
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 2,'de-DE','Fortgeschritten',2 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 2);
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 3,'de-DE','Experte'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 3);

DELETE FROM CFGEMSkillUsageDescriptions WHERE UICultureName = 'de-DE';
--.............................................................................................v(255)
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 1,'de-DE','Einstiegsebene'        ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 1);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 2,'de-DE','1-2 Jahre'    ,2 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 2);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 3,'de-DE','3-5 Jahre'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 3);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 4,'de-DE','6-10 Jahre'   ,4 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 4);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 5,'de-DE','Mehr als 10 Jahre',5 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 5);

DELETE FROM CFGLeadRatingDescriptions WHERE UICultureName = 'de-DE';
--..............................................................................................v(50)
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '01','de-DE','Heiß' ,1 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '01');
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '02','de-DE','Warm',2 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '02');
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '03','de-DE','Kalt',3 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '03');

DELETE FROM CFGOpportunityClosedReasonDescriptions WHERE UICultureName = 'de-DE';
--...........................................................................................................v(50)
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '01','de-DE','Preis'    ,1 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '01');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '02','de-DE','Services' ,2 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '02');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '03','de-DE','Ressourcen',3 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '03');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '04','de-DE','Zeitplanung'   ,4 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '04');

DELETE FROM CFGOpportunitySourceDescriptions WHERE UICultureName = 'de-DE';
--...........................................................................................................v(50)
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'CRef'    ,'de-DE','Kundenreferenz'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'CRef');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Event'   ,'de-DE','Ereignis'                     ,2 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Event');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'List'    ,'de-DE','Liste'                      ,5 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'List');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'PersCont','de-DE','Persönlicher Kontakt'          ,6 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'PersCont');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Social'  ,'de-DE','Social Media'              ,7 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Social');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'TLead'   ,'de-DE','Vordenker'        ,8 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'TLead');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Web'     ,'de-DE','Web'                       ,9 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Web');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'WIQ'     ,'de-DE','GovWin IQ'                 ,3 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'WIQ');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'WOM'     ,'de-DE','GovWin Opportunity-Manager',4 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'WOM');

DELETE FROM CFGPrefixDescriptions WHERE UICultureName = 'de-DE';
--........................................................................................v(10)
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Dr.'  ,'de-DE','Dr.'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Dr.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Miss' ,'de-DE','Frl.' ,2 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Miss');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Mr.'  ,'de-DE','Herr'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Mr.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Mrs.' ,'de-DE','Frau' ,4 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Mrs.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Ms.'  ,'de-DE','Frau'  ,5 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Ms.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Prof.','de-DE','Prof.',6 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Prof.');

DELETE FROM CFGProbabilityDescriptions WHERE UICultureName = 'de-DE';
--.....................................................................................................v(50)
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 5  ,'de-DE','05' ,1  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 5);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 10 ,'de-DE','10' ,2  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 10);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 20 ,'de-DE','20' ,3  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 20);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 30 ,'de-DE','30' ,4  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 30);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 40 ,'de-DE','40' ,5  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 40);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 50 ,'de-DE','50' ,6  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 50);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 60 ,'de-DE','60' ,7  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 60);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 70 ,'de-DE','70' ,8  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 70);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 80 ,'de-DE','80' ,9  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 80);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 90 ,'de-DE','90' ,10 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 90);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 95 ,'de-DE','95' ,11 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 95);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 100,'de-DE','100',12 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 100);

DELETE FROM CFGProjectCodeDescriptions WHERE UICultureName = 'de-DE';
--.......................................................................................................v(100)
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '001','de-DE','Akustik; Lärmminderung'                                                                    ,1   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '001');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '002','de-DE','Luftbildaufnahmen; Sammlung und Analyse von Daten und Bildern aus der Luft'                         ,2   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '002');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '003','de-DE','Landwirtschaftliche Entwicklung; Getreidelagerung; Mechanisierung von landwirtschaftlichen Betrieben'                                   ,3   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '003');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '004','de-DE','Immissionsschutz und Luftreinhaltung'                                                                         ,4   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '004');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '005','de-DE','Flughäfen; Navigationssysteme; Flughafenbeleuchtung; Flugzeugbetankung'                                         ,5   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '005');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '006','de-DE','Flughäfen; Terminals & Hangers; Frachtabfertigung'                                              ,6   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '006');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '007','de-DE','Arktische Einrichtungen'                                                                             ,7   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '007');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '008','de-DE','Auditorien & Theater'                                                                        ,8   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '008');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '009','de-DE','Automatisierung; Steuerungen; Instrumentierung'                                                         ,9   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '009');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '010','de-DE','Kasernen; Schlafsäle'                                                                         ,10  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '010');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '011','de-DE','Brücken'                                                                                       ,11  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '011');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '012','de-DE','Friedhöfe (Planung & Verlegung)'                                                            ,12  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '012');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '013','de-DE','Chemische Prozesse und Lagerung'                                                                 ,13  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '013');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '014','de-DE','Kirchen; Kapellen'                                                                             ,14  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '014');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '015','de-DE','Codes; Normen; Verordnungen'                                                                  ,15  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '015');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '016','de-DE','Kühlhaus; Kältekammern; Schnellgefrieren'                                                      ,16  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '016');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '017','de-DE','Geschäftshaus; (geringe Bauhöhe); Einkaufszentren'                                             ,17  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '017');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '018','de-DE','Kommunikationssysteme; TV; Mikrowelle'                                                         ,18  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '018');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '019','de-DE','Computeranlagen; Computerservice'                                                         ,19  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '019');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '020','de-DE','Naturschutz und Ressourcenmanagement'                                                          ,20  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '020');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '021','de-DE','Bauleitung und -management'                                                                       ,21  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '021');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '022','de-DE','Korrosionsschutz; Kathodischer Schutz; Galvanisierung; Elektrolyse'                                          ,22  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '022')
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '023','de-DE','Kostenschätzung; Kostenplanung und -analyse; Parametrische Kalkulation; Prognose'               ,23  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '023');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '024','de-DE','Dämme (Beton; Bogen)'                                                                         ,24  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '024');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '025','de-DE','Dämme (Erde; Fels); Deiche; Wehre'                                                             ,25  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '025');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '026','de-DE','Entsalzung (Prozess & Anlagen)'                                                         ,26  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '026');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '027','de-DE','Esssäle, Klubs, Restaurants'                                                              ,27  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '027');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '028','de-DE','Ökologische & archäologische Untersuchungen'                                                     ,28  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '028');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '029','de-DE','Bildungseinrichtungen; Klassenzimmer'                                                            ,29  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '029');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '030','de-DE','Elektronik'                                                                                   ,30  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '030');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '031','de-DE','Aufzüge; Rolltreppen; Personenbeförderer'                                                          ,31  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '031');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '032','de-DE','Energieeinsparung; neue Energiequellen'                                                       ,32  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '032');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '033','de-DE','Umweltverträglichkeitsstudien, -bewertungen oder -erklärungen'                                       ,33  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '033');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '034','de-DE','Schutzbunker; Sprengwirkungshemmendes Design'                                                      ,34  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '034');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '035','de-DE','Sporthallen; Turnhallen; Stadien'                                                            ,35  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '035');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '036','de-DE','Brandschutz'                                                                               ,36  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '036');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '037','de-DE','Fischzuchten; Fischtreppen'                                                                       ,37  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '037');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '038','de-DE','Forstwirtschaft & Forstprodukte'                                                                    ,38  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '038');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '039','de-DE','Garagen; Fahrzeugwerkstätten; Parkdecks'                                       ,39  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '039');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '040','de-DE','Gassysteme (Propan, Erdgas etc.)'                                                          ,40  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '040');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '041','de-DE','Grafikdesign'                                                                                ,41  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '041');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '042','de-DE','Häfen; Anlegestellen; Piers; Schiffsterminalanlagen'                                             ,42  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '042');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '043','de-DE','Heizung, Lüftung, Klimaanlagen'                                                        ,43  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '043');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '044','de-DE','Gesundheitssystemplanung'                                                                       ,44  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '044');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '045','de-DE','Hochhaus; luftrechtstypische Gebäude'                                                           ,45  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '045');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '046','de-DE','Autobahnen; Straßen; Rollfelder & Landebahnen; Parkplätze'                                              ,46  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '046');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '047','de-DE','Denkmalpflege'                                                                       ,47  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '047');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '048','de-DE','Krankenhäuser & medizinische Einrichtungen'                                                                ,48  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '048');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '049','de-DE','Hotels; Motels'                                                                                ,49  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '049');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '050','de-DE','Wohnen (Einfamilienhäuser, Mehrfamilienhäuser, Wohnungen, Eigentumswohnungen)'                                   ,50  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '050');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '051','de-DE','Hydraulik & Pneumatik'                                                                       ,51  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '051');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '052','de-DE','Industriegebäude; Produktionsstätten'                                                    ,52  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '052');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '053','de-DE','Industrielle Prozesse; Qualitätskontrolle'                                                         ,53  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '053');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '054','de-DE','Industrieabfallbehandlung'                                                                     ,54  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '054');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '055','de-DE','Innendesign; Raumplanung'                                                               ,55  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '055');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '056','de-DE','Bewässerung; Entwässerung'                                                                          ,56  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '056');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '057','de-DE','Gerichts- und Gerichtssaaleinrichtungen'                                                             ,57  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '057');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '058','de-DE','Laboratorien; Medizinische Forschungseinrichtungen'                                                      ,58  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '058');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '059','de-DE','Landschaftsarchitektur'                                                                        ,59  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '059');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '060','de-DE','Bibliotheken; Museen; Galerien'                                                                 ,60  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '060');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '061','de-DE','Beleuchtung (Innen; Display; Theater etc.)'                                                 ,61  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '061');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '062','de-DE','Beleuchtung (Außen; Straße; Denkmäler, Sportstadien)'                                      ,62  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '062');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '063','de-DE','Materialflusssysteme; Förderer; Sortieranlagen; Transportbänder'                                                ,63  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '063');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '064','de-DE','Metallurgie'                                                                                    ,64  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '064');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '065','de-DE','Mikroklimatologie; Tropentechnik'                                                        ,65  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '065');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '066','de-DE','Militärische Entwicklungsstandards'                                                                     ,66  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '066');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '067','de-DE','Bergbau und Mineralogie'                                                                         ,67  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '067');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '068','de-DE','Raketenanlagen (Silos; Kraftstoffe; Transport)'                                                  ,68  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '068');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '069','de-DE','Modularer Systemaufbau; Fertigteilbauten oder -komponenten'                                      ,69  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '069');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '070','de-DE','Marinearchitektur; Offshore-Plattformen'                                                       ,70  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '070');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '071','de-DE','Nukleare Einrichtungen; Nukleare Abschirmung'                                                         ,71  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '071');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '072','de-DE','Bürogebäude; Industrie-Parks'                                                             ,72  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '072');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '073','de-DE','Ozeanographische Technik'                                                                     ,73  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '073');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '074','de-DE','Ordonnanzen; Munition; Spezialwaffen'                                                       ,74  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '074');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '075','de-DE','Erdölexploration; Raffination'                                                               ,75  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '075');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '076','de-DE','Erdöl und Brennstoffe (Lagerung und Verteilung)'                                                 ,76  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '076');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '077','de-DE','Rohrleitungen (Überland-Flüssigkeit und Gas)'                                                       ,77  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '077');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '078','de-DE','Planung (Siedlung; Regional; Flächendeckend & Staat)'                                              ,78  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '078');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '079','de-DE','Planung (Standort, Installation und Projekt)'                                                     ,79  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '079');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '080','de-DE','Sanitär- und Rohrleitungsplanung'                                                                        ,80  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '080');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '081','de-DE','Pneumatikanlagen; Luftunterstützungsgebäude'                                                   ,81  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '081');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '082','de-DE','Posteinrichtungen'                                                                             ,82  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '082');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '083','de-DE','Energieerzeugung, -übertragung, -verteilung'                                                  ,83  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '083');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '084','de-DE','Gefängnisse & Justizvollzugsanstalten'                                                             ,84  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '084');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '085','de-DE','Produkt-, Maschinen- und Anlagenplanung'                                                           ,85  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '085');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '086','de-DE','Radar; Sonar; Radio & Radarteleskope'                                                        ,86  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '086');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '087','de-DE','Eisenbahn und Schnellverkehr'                                                                    ,87  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '087');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '088','de-DE','Freizeiteinrichtungen (Parks; Sporthäfen etc.)'                                                ,88  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '088');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '089','de-DE','Sanierung (Gebäude; Bauten; Einrichtungen)'                                            ,89  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '089');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '090','de-DE','Ressourcenrückgewinnung; Recycling'                                                                 ,90  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '090');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '091','de-DE','Hochfrequenzsysteme & Abschirmungen'                                                          ,91  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '091');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '092','de-DE','Flusskanäle; Wasserstraßen; Hochwasserschutz'                                                       ,92  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '092');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '093','de-DE','Sicherheitstechnik; Unfallstudien; OSHA-Studien'                                            ,93  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '093');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '094','de-DE','Sicherheitssysteme; Einbruch- und Rauchmeldeanlagen'                                                  ,94  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '094');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '095','de-DE','Erdbebensicherheitsdesign & Studien'                                                                      ,95  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '095');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '096','de-DE','Abwassersammlung, -behandlung und -entsorgung'                                                       ,96  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '096');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '097','de-DE','Böden & Geologische Studien; Fundamente'                                                         ,97  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '097');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '098','de-DE','Solarenergienutzung'                                                                      ,98  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '098');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '099','de-DE','Feste Abfälle; Verbrennung; Deponierung'                                                          ,99  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '099');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '100','de-DE','Spezielle Umgebungen; Reinräume etc.'                                                       ,100 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '100');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '101','de-DE','Baukonstruktionen; Spezielle Konstruktionen'                                                         ,101 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '101');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '102','de-DE','Vermessung; Planung; Kartierung; Überschwemmungsgebiet-Studien'                                             ,102 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '102');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '103','de-DE','Schwimmbäder'                                                                                ,103 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '103');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '104','de-DE','Regenwasserbehandlung & -einrichtungen'                                                              ,104 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '104');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '105','de-DE','Telefonsysteme (ländlich; mobil; Gegensprechanlage etc.)'                                             ,105 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '105');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '106','de-DE','Prüf- und Inspektionsservices'                                                                 ,106 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '106');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '107','de-DE','Verkehrs- und Transporttechnik'                                                          ,107 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '107');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '108','de-DE','Türme (Selbsttragende & Abspann-Systeme)'                                                      ,108 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '108');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '109','de-DE','Tunnels & Unterführungen'                                                                             ,109 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '109');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '110','de-DE','Stadterneuerung; Gemeindeentwicklung'                                                         ,110 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '110');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '111','de-DE','Versorgungsunternehmen (Gas und Dampf)'                                                                     ,111 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '111');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '112','de-DE','Wertanalyse; Lebenszykluskostenrechnung'                                                            ,112 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '112');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '113','de-DE','Lagerhallen & Depots'                                                                           ,113 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '113');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '114','de-DE','Wasserressourcen; Hydrologie; Grundwasser'                                                      ,114 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '114');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '115','de-DE','Wasserversorgung; Aufbereitung und Verteilung'                                                      ,115 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '115');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '116','de-DE','Windkanäle; Entwurf von Forschungs-/Prüfeinrichtungen'                                              ,116 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '116');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '117','de-DE','Zoneneinteilung; Landnutzungsstudien'                                                                      ,117 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '117');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'A08','de-DE','Tierische Einrichtungen'                                                                             ,118 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'A08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'A09','de-DE','Anti-Terrorismus / Gewaltschutz'                                                               ,119 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'A09');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'A10','de-DE','Asbestsanierung'                                                                            ,120 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'A10');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C01','de-DE','Kartographie'                                                                                   ,121 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C01');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C03','de-DE','Kartierung: Nautisch und Aeronautisch'                                                           ,122 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C05','de-DE','Kinderbetreuung/Entwicklungseinrichtungen'                                                             ,123 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C07','de-DE','Küsteningenieurwesen'                                                                           ,124 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C07');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C11','de-DE','Gemeinschaftseinrichtungen'                                                                          ,125 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C11');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C16','de-DE','Bauvermessung'                                                                        ,126 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C16');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C19','de-DE','Kryogene Anlagen'                                                                          ,127 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C19');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D04','de-DE','Design-Build - Vorbereitung von Ausschreibungen für Angebote'                                          ,128 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D05','de-DE','Entwicklung digitaler Höhen- und Geländemodelle'                                               ,129 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D06','de-DE','Digitale Orthophotographie'                                                                      ,130 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D06');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D08','de-DE','Ausbaggerungsstudien und -design'                                                                   ,131 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E03','de-DE','Elektrische Studien und Design'                                                                 ,132 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E06','de-DE','Botschaften und Kanzleien'                                                                      ,133 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E06');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E08','de-DE','Ingenieurwesen und Wirtschaftswissenschaften'                                                                         ,134 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E10','de-DE','Kartierung von Umwelt- und Naturressourcen'                                                    ,135 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E10');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E11','de-DE','Umweltplanung'                                                                        ,136 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E11');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E12','de-DE','Umweltsanierung'                                                                     ,137 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E12');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E13','de-DE','Umweltprüfungen und -analysen'                                                            ,138 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E13');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'F05','de-DE','Forensische Technik'                                                                          ,139 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'F05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'G03','de-DE','Geodätische Vermessung: boden- und luftgestützt'                                                       ,140 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'G03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'G04','de-DE','Geografische Informationssystemdienste: Entwicklung, Analyse und Datenerfassung'            ,141 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'G04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'G05','de-DE','Geodatenkonvertierung: Scannen, Digitalisieren, Kompilieren, Attributieren, Schreiben, Zeichnen',142 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'G05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'H02','de-DE','Handhabung und Lagerung von Gefahrstoffen'                                                      ,143 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'H02');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'H03','de-DE','Sanierung gefährlicher, giftiger, radioaktiver Abfälle'                                               ,144 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'H03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'H13','de-DE','Hydrographische Vermessung'                                                                        ,145 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'H13');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'I04','de-DE','Intelligente Transportsysteme'                                                            ,146 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'I04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'L02','de-DE','Landvermessung'                                                                                ,147 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'L02');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'M01','de-DE','Zuordnung von Standort/Adressiersystemen'                                                           ,148 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'M01');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'N02','de-DE','Navigationsstrukturen; Schleusen'                                                                  ,149 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'N02');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'P03','de-DE','Photogrammetrie'                                                                                ,150 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'P03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'P13','de-DE','Öffentliche Sicherheitseinrichtungen'                                                                      ,151 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'P13');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R05','de-DE','Kühlanlagen/-systeme'                                                                  ,152 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R07','de-DE','Fernerkundung'                                                                                ,153 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R07');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R08','de-DE','Forschungseinrichtungen'                                                                           ,154 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R10','de-DE','Risikoanalyse'                                                                                 ,155 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R10');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R12','de-DE','Bedachung'                                                                                       ,156 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R12');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'S11','de-DE','Nachhaltiges Design'                                                                            ,157 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'S11');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'T04','de-DE','Topografische Vermessung und Kartierung'                                                             ,158 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'T04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'U01','de-DE','Beseitigung nicht explodierter Kampfmittel'                                                               ,159 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'U01');

DELETE FROM CFGProjectMilestoneDescriptions WHERE UICultureName = 'de-DE';
--...........................................................................................................................v(255)
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysEstStart',           'de-DE','Veranschlagter Beginn',                   1 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysEstStart')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysEstCompletion',      'de-DE','Veranschlagter Abschluss',              2 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysEstCompletion')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysContract',           'de-DE','Vergebener Vertrag',                  3 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysContract')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysBidSubmitted',       'de-DE','Gebot eingereicht',                     4 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysBidSubmitted')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysStart',              'de-DE','Tatsächlicher Start',                      5 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysStart')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysActualCompl',        'de-DE','Tatsächlicher Abschluss',                 6 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysActualCompl')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysProfServicesCompl',  'de-DE','Abschluss der professionellen Services',  7 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysProfServicesCompl')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysConstCompl',         'de-DE','Abschlussdatum für Bau',           8 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysConstCompl')


DELETE FROM CFGProposalSourceDescriptions WHERE UICultureName = 'de-DE';
--..................................................................................................v(50)
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '01','de-DE','Kundenreferenz'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '01');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '02','de-DE','Ereignis'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '02');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '03','de-DE','Liste'              ,3 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '03');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '04','de-DE','Persönlicher Kontakt'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '04');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '05','de-DE','Social Media'      ,5 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '05');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '06','de-DE','Vordenker',6 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '06');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '07','de-DE','Website'           ,7 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '07');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '08','de-DE','GovWinIQ'          ,8 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '08');

DELETE FROM CFGProposalStatusDescriptions WHERE UICultureName = 'de-DE';
--..................................................................................................v(50)
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','de-DE','In Bearbeitung',1 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '01');
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','de-DE','Wird überprüft'  ,2 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '02');
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','de-DE','Eingereicht'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '03');

DELETE FROM CFGPRResponsibilityDescriptions WHERE UICultureName = 'de-DE';
--....................................................................................................v(80)
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'C' ,'de-DE','Berater'           ,3 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'C');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'IE','de-DE','Individuelle Erfahrung',4 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'IE');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'JV','de-DE','Joint Venture'        ,2 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'JV');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'P' ,'de-DE','Prime'                ,1 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'P');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'S' ,'de-DE','Subauftragnehmer'        ,5 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'S');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'U' ,'de-DE','Unbestimmt'         ,6 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'U');

DELETE FROM CFGPYAccrualsDescriptions WHERE UICultureName = 'de-DE';
--............................................................................................................................v(40)
INSERT INTO CFGPYAccrualsDescriptions (Code,Company,UICultureName,Description) SELECT 'Sick Lv.',CFGMainData.Company,'de-DE','Krankenstand' FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGPYAccrualsData WHERE Code = 'Sick Lv.' AND Company = CFGMainData.Company)
INSERT INTO CFGPYAccrualsDescriptions (Code,Company,UICultureName,Description) SELECT 'Vacation',CFGMainData.Company,'de-DE','Urlaub'   FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGPYAccrualsData WHERE Code = 'Vacation' AND Company = CFGMainData.Company)

DELETE FROM CFGRGMethodsDescriptions WHERE UICultureName = 'de-DE';
--..........................................................................................v(40)
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'B','de-DE','Abrechnung von Aufträgen bis dato'                   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'B');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'M','de-DE','(Auftrag bis dato DL*Mult) + Erstattungsausgaben Auftrag bis dato'  WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'M');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'N','de-DE','Keine Umsatzrealisierung'         WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'N');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'P','de-DE','(% Komp*Honorar) + Erstattungsausgaben Auftrag bis dato' WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'P');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'R','de-DE','Einnahmen, Auftrag bis dato'                   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'R');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'W','de-DE','Abrechnungen Auftrag bis dato + WIP bei Abrechnung'   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'W');

DELETE FROM CFGServiceProfileDescriptions WHERE UICultureName = 'de-DE';
--..................................................................................................................v(40)
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.18' ,'de-DE','Landnutzung/Entwicklungsplan','Area in hectares'               WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.18');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.19' ,'de-DE','Landnutzung/Bauplan','Area in hectares'                          WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.19');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.23' ,'de-DE','Landschaftsplan','Area in hectares'                                  WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.23');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.24' ,'de-DE','Plan für Freiflächen/Grünflächen','Area in hectares'                           WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.24');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.25' ,'de-DE','Struktureller Landschaftplan','Area in hectares'                        WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.25');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.26' ,'de-DE','Landschaftserhaltungsplan','Area in hectares'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.26');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.27' ,'de-DE','Erhaltungs- und Entwicklungsplan','Area in hectares'                WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.27');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.34' ,'de-DE','Gebäude und Innenräume','Chargeable costs in Euro'                 WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.34');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.39' ,'de-DE','Freilufteinrichtungen','Chargeable costs in Euro'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.39');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.43' ,'de-DE','Hochbauten','Chargeable costs in Euro'            WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.43');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.47' ,'de-DE','Transporteinrichtungn','Chargeable costs in Euro'               WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.47');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.51' ,'de-DE','Bautechnische Planung','Chargeable costs in Euro'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.51');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.55' ,'de-DE','Technische Ausrüstung','Chargeable costs in Euro'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.55');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A01','de-DE','Umwelteinflussstudien','Area in hectares'                    WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A01');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A02','de-DE','Wärmedämmung und Energiebalance','Chargeable costs in Euro' WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A02');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A03','de-DE','Gebäudeakkustik','Chargeable costs in Euro'                      WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A03');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A04','de-DE','Raumakkustik','Chargeable costs in Euro'                          WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A04');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A05','de-DE','Geotechnik','Chargeable costs in Euro'                             WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A05');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A06','de-DE','Mess- und Vermessungsarbeiten','Accounting units'                  WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A06');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A07','de-DE','Ingenieurvermessung','Chargeable costs in Euro'                   WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A07');

DELETE FROM CFGSuffixDescriptions WHERE UICultureName = 'de-DE';
--......................................................................................v(150)
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'I'  ,'de-DE','I'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'I');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'II' ,'de-DE','II' ,4 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'II');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'III','de-DE','III',5 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'III');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'Jr.','de-DE','Jr.',1 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'Jr.');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'Sr.','de-DE','Herr',2 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'Sr.');

DELETE FROM CFGUnitTypeDescriptions WHERE UICultureName = 'de-DE';
--...................................................................................................v(50)
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Equipment','de-DE','Ausrüstung',1 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Equipment');
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Expenses ','de-DE','Ausgaben' ,2 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Expenses');
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Labor'    ,'de-DE','Aufwand'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Labor');

DELETE FROM CFGVendorTypeDescriptions WHERE UICultureName = 'de-DE';
--...................................................................................v(30)
INSERT INTO CFGVendorTypeDescriptions (Type,UICultureName,Label) SELECT 'C','de-DE','Berater' WHERE EXISTS (SELECT 'x' FROM CFGVendorTypeData WHERE Type = 'C');
INSERT INTO CFGVendorTypeDescriptions (Type,UICultureName,Label) SELECT 'T','de-DE','Handel'      WHERE EXISTS (SELECT 'x' FROM CFGVendorTypeData WHERE Type = 'T');

DELETE FROM FW_CFGAttachmentCategoryDesc WHERE UICultureName = 'de-DE';
--................................................................................................................................................v(255)
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Notes'          ,'Activities'         ,'de-DE','Anmerkungen'                   ,1  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Notes' and Application ='Activities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'NDA'            ,'Contacts'           ,'de-DE','Vertraulichkeitsvereinbarung',2  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'NDA' and Application ='Contacts');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Notes'          ,'Contacts'           ,'de-DE','Anmerkungen'                   ,3  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Notes' and Application ='Contacts');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'CompanyOverview','TextLibrary'        ,'de-DE','Überblick über Unternehmen'        ,4  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'CompanyOverview' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Case Study'     ,'TextLibrary'        ,'de-DE','Fallstudie'              ,5  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Case Study' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Reference'      ,'TextLibrary'        ,'de-DE','Referenz'               ,6  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Reference' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Resume'         ,'Employees'          ,'de-DE','Lebenslauf'                  ,7  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Resume' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Award'          ,'Employees'          ,'de-DE','Auszeichnung'                   ,8  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Award' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Leadership'     ,'Employees'          ,'de-DE','Vordenker'      ,9  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Leadership' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Firms'              ,'de-DE','Dienstleistungsrahmenvertrag',10 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Firms'              ,'de-DE','Leistungsbeschreibung'       ,11 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'NDA'            ,'Firms'              ,'de-DE','Vertraulichkeitsvereinbarung',12 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'NDA' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'ClientACH'      ,'Firms'              ,'de-DE','Kunden-ACH'              ,13 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'ClientACH' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'VendorACH'      ,'Firms'              ,'de-DE','Anbieter-ACH'              ,14 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'VendorACH' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'DigitalArtifact','Marketing Campaigns','de-DE','Digitaler Artefakt'        ,15 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'DigitalArtifact' and Application ='Marketing Campaigns');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Estimate'       ,'Opportunities'      ,'de-DE','Kostenvoranschlag'                ,16 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Estimate' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Proposal'       ,'Opportunities'      ,'de-DE','Angebot'                ,17 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Proposal' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Opportunities'      ,'de-DE','Leistungsbeschreibung'       ,18 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Opportunities'      ,'de-DE','Dienstleistungsrahmenvertrag',19 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Estimate'       ,'Projects'           ,'de-DE','Kostenvoranschlag'                ,20 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Estimate' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Proposal'       ,'Projects'           ,'de-DE','Angebot'                ,21 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Proposal' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Projects'           ,'de-DE','Leistungsbeschreibung'       ,22 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Projects'           ,'de-DE','Dienstleistungsrahmenvertrag',23 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Projects');

DELETE FROM FW_CFGLabelData WHERE UICultureName = 'de-DE';
--............................................................................................................................................................................v(100)
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','accountLabel'                            ,'Konto'                       ,'[Account]'                    ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','accountLabelPlural'                      ,'Konten'                      ,'[Accounts]'                   ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','clientLabel'                             ,'Kunde'                        ,'[Client]'                     ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','clientLabelPlural'                       ,'Kunden'                       ,'[Clients]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','contactLabel'                            ,'Kontakt'                       ,'[Contact]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','contactLabelPlural'                      ,'Kontakte'                      ,'[Contacts]'                   ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','employeeLabel'                           ,'Mitarbeiter'                      ,'[Employee]'                   ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','employeeLabelPlural'                     ,'Mitarbeiter'                     ,'[Employees]'                  ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','labcd1Label'                             ,'Aufwandsschlüssel Ebene 1'            ,'[Labcd Level 1]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','labcd1LabelPlural'                       ,'Aufwandsschlüssel Ebene 1s'           ,'[Labcd Level 1s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','labcd2Label'                             ,'Aufwandsschlüssel Ebene 2'            ,'[Labcd Level 2]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','labcd2LabelPlural'                       ,'Aufwandsschlüssel Ebene 2s'           ,'[Labcd Level 2s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','labcd3Label'                             ,'Aufwandsschlüssel Ebene 3'            ,'[Labcd Level 3]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','labcd3LabelPlural'                       ,'Aufwandsschlüssel Ebene 3s'           ,'[Labcd Level 3s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','labcd4Label'                             ,'Aufwandsschlüssel Ebene 4'            ,'[Labcd Level 4]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','labcd4LabelPlural'                       ,'Aufwandsschlüssel Ebene 4s'           ,'[Labcd Level 4s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','labcd5Label'                             ,'Aufwandsschlüssel Ebene 5'            ,'[Labcd Level 5]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','labcd5LabelPlural'                       ,'Aufwandsschlüssel Ebene 5s'           ,'[Labcd Level 5s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','labcdLabel'                              ,'Aufwandsschlüssel'                    ,'[Labor Code]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','labcdLabelPlural'                        ,'Aufwandsschlüssel'                   ,'[Labor Codes]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','firmLabel'                               ,'Unternehmen'                          ,'[Firm]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','firmLabelPlural'                         ,'Unternehmen'                         ,'[Firms]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','mktLabel'                                ,'Marketingkampagne'            ,'[Marketing Campaign]'         ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','mktLabelPlural'                          ,'Marketingkampagnen'           ,'[Marketing Campaigns]'        ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','org1Label'                               ,'Unternehmen 1'                ,'[Org Level 1]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','org1LabelPlural'                         ,'Unternehmensebene 1s'               ,'[Org Level 1s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','org2Label'                               ,'Unternehmen 2'                ,'[Org Level 2]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','org2LabelPlural'                         ,'Unternehmen 2a'               ,'[Org Level 2s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','org3Label'                               ,'Organisation 3'                ,'[Org Level 3]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','org3LabelPlural'                         ,'Organisation 3s'               ,'[Org Level 3s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','org4Label'                               ,'Organisation 4'                ,'[Org Level 4]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','org4LabelPlural'                         ,'Organisation 4s'               ,'[Org Level 4s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','org5Label'                               ,'Organisation 5'                ,'[Org Level 5]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','org5LabelPlural'                         ,'Organisation 5s'               ,'[Org Level 5s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','orgLabel'                                ,'Organisation'                  ,'[Organization]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','orgLabelPlural'                          ,'Organisationen'                 ,'[Organizations]'              ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','proposalLabel'                           ,'Angebot'                      ,'[Proposal]'                   ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','proposalLabelPlural'                     ,'Angebote'                     ,'[Proposals]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','textLibLabel'                            ,'Standardfassung'                   ,'[Text Library]'               ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','textLibLabelPlural'                      ,'Standardfassungen'                  ,'[Text Libraries]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','unitLabel'                               ,'Einheit'                          ,'[Unit]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','unitLabelPlural'                         ,'Einheiten'                         ,'[Units]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','vendorLabel'                             ,'Anbieter'                        ,'[Vendor]'                     ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','vendorLabelPlural'                       ,'Anbieter'                       ,'[Vendors]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','wbs1Label'                               ,'Projekt'                       ,'[WBS1]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','wbs1LabelPlural'                         ,'Projekte'                      ,'[WBS1s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','wbs2Label'                               ,'Phase'                         ,'[WBS2]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','wbs2LabelPlural'                         ,'Phasen'                        ,'[WBS2s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','wbs3Label'                               ,'Aufgabe'                          ,'[WBS3]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','wbs3LabelPlural'                         ,'Aufgaben'                         ,'[WBS3s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','sysPM'                                   ,'Projektmanager'               ,'[Project Manager]'            ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','sysPR'                                   ,'Hauptgeschäftsführer'                     ,'[Principal]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','sysSP'                                   ,'Vorgesetzter'                    ,'[Supervisor]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','equipmentLabel'                          ,'Ausrüstung'                     ,'[Equipment]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','equipmentLabelPlural'                    ,'Ausrüstung'                     ,'[Equipments]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','jobToDateLabel'                          ,'Auftrag bis dato'                   ,'[JobToDate]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','JTDLabel'                                ,'Auftrag bis dato'                           ,'[JTD]'                        ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','estimateToCompleteLabel'                 ,'Kostenvoranschlag bis Abschluss'          ,'[EstimateToComplete]'         ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','ETCLabel'                                ,'ETC'                           ,'[ETC]'                        ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','estimateAtCompletionLabel'               ,'Kostenvoranschlag bei Abschluss'        ,'[EstimateAtCompletion]'       ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','EACLabel'                                ,'EAC'                           ,'[EAC]'                        ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','sysPRM'                                  ,'Angebotsmanager'              ,'[Proposal Manager]'           ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','sysMC'                                   ,'Marketingkoordinator'         ,'[Marketing Coordinator]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','sysBDL'                                  ,'Geschäftsentwicklungsleiter'     ,'[Business Development Lead]'  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','consultantLabel'                         ,'Berater'                    ,'[Consultant]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','consultantLabelPlural'                   ,'Berater'                   ,'[Consultants]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','compensationLabel'                       ,'Honorar'                  ,'[Compensation]'               ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','revType1Label'                           ,'Aufwand'                         ,'[Revenue Method Type 1]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','revType2Label'                           ,'Berater'                    ,'[Revenue Method Type 2]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','revType3Label'                           ,'Erstattung'                         ,'[Revenue Method Type 3]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','revType4Label'                           ,'Umsatzmethode 4'              ,'[Revenue Method Type 4]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','revType5Label'                           ,'Umsatzmethode 5'              ,'[Revenue Method Type 5]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','feeEstimatingCostGroupLabel'             ,'Kostengruppe'                    ,'[Cost Group]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','feeEstimatingCostGroupLabelPlural'       ,'Kostengruppen'                   ,'[Cost Groups]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','feeEstimatingFunctionalGroupLabel'       ,'Funktionsgruppe'              ,'[Functional Group]'           ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','feeEstimatingFunctionalGroupLabelPlural' ,'Funktionsgruppen'             ,'[Functional Groups]'          ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','feeEstimatingServiceProfileLabel'        ,'Serviceprofil'               ,'[Service Profile]'            ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('de-DE','feeEstimatingServiceProfileLabelPlural'  ,'Serviceprofile'              ,'[Service Profiles]'           ,'N','N',NULL);

COMMIT TRANSACTION

END
GO
