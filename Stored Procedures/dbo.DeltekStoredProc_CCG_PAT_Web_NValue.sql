SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Web_NValue]
	@FN				varchar(500),
	@PayableSeqE	varchar(max),
	@PayableSeq		varchar(15),
	@WBS1E			varchar(max),
	@WBS1			varchar(37),
	@WBS2			varchar(15),
	@WBS3			varchar(15),
	@NumValues		int,
	@DbParams		varchar(max)
AS
BEGIN
	/*
	[DeltekStoredProc_CCG_PAT_Web_NValue]
		@FN='fnCCG_PAT_JTDInvoicedFromVendor_Details',
		@PayableSeqE = null, @PayableSeq=5112,
		@WBS1E=null, @WBS1=' ',
		@WBS2=' ',
		@WBS3=' ',
		@NumValues=0,
		@DbParams = '-1'
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		varchar(max);
	SET @DbParams = LTRIM(ISNULL(@DbParams,'-1'))
	IF SUBSTRING(@DbParams, 1, 1) = ',' SET @DbParams = SUBSTRING(@DbParams, 2, LEN(@DbParams)-1)		-- Allow for @DbParams to start with a comma (backward compatibility)

	IF @WBS1 <> 'null' SET @WBS1 = ''''+@WBS1+''''
	IF @WBS2 <> 'null' SET @WBS2 = ''''+@WBS2+''''
	IF @WBS3 <> 'null' SET @WBS3 = ''''+@WBS3+''''

	SET @sSQL = rtrim((CASE WHEN ISNULL(@PayableSeqE,'')<>'' THEN dbo.fnCCG_PAT_GetD(@PayableSeqE)+', ' ELSE '' END) +
				(CASE WHEN ISNULL(@PayableSeq,'-1')<>'-1' THEN @PayableSeq+', ' ELSE '' END) +
				(CASE WHEN ISNULL(@WBS1E,'')<>'' THEN ''''+dbo.fnCCG_PAT_GetD(@WBS1E)+''', ' ELSE '' END) +
				(CASE WHEN ISNULL(@WBS1,'')<>'' THEN @WBS1+', ' ELSE '' END) +
				(CASE WHEN ISNULL(@WBS2,'')<>'' THEN @WBS2+', ' ELSE '' END) +
				(CASE WHEN ISNULL(@WBS3,'')<>'' THEN @WBS3+', ' ELSE '' END) +
				(CASE WHEN LTRIM(ISNULL(@DbParams,'-1'))<>'-1' THEN @DbParams+', ' ELSE '' END))

	SET @sSQL = '
		SELECT *
			FROM dbo.' + @FN + '(' + SUBSTRING(@sSQL, 1, LEN(@sSQL)-1) + ')
			ORDER BY TopOrder, Descr '

	If @NumValues > 0 SET @sSQL = @sSQL + ', Value1'

	PRINT @sSQL;
	EXEC (@sSQL);
END;
GO
