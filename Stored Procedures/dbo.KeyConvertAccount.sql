SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyConvertAccount]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg Nvarchar(1000) output,
@NeedTransaction Nvarchar(1) = 'Y',
@CheckAccountType Nvarchar(1) = 'Y',
@UserName Nvarchar(32) = 'VisionKeyConvert' -- Add by DP 4.0
,@Period integer = 0,
@EntityType Nvarchar(20)
as
begin -- Procedure
	set nocount on
	declare @OldValue Nvarchar(20)
	declare @NewValue Nvarchar(20)
	Declare @Existing integer
	declare @Entity Nvarchar(20)
	declare @length integer
	declare @CvtType integer
	declare @KeysFetch integer
	declare @RetVal integer
	declare @diag smallint
	declare @Errmsg2 Nvarchar(1000),
			@custlabel	Nvarchar(40),
			@Custlabelplural Nvarchar(40)
	declare @TableName 		Nvarchar(100)
	declare @Sql 				Nvarchar(1000)
	declare @NewAccountType		int
	declare @OldAccountType		int
	declare @message nvarchar(max)

	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'Account'
	set @length = 20
	set @Existing = 0
	set @KeysFetch = 0

	set @diag = 0
	execute InsertKeyCvtDriver @Entity -- populate driver table
-- Add by DP 4.0
	delete from keyconvertDriver where entity = N'Account' and TableName = N'CA' and ColumnName = N'Account'
	DECLARE @OldName Nvarchar(40)
	DECLARE @NewName Nvarchar(40)
	DECLARE @PKey Nvarchar(50)
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0
--
	select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'accountLabel'
	select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'accountLabelPlural'

	declare KeysCursor cursor for
		select OldKey, NewKey, Pkey from KeyConvertWork where Entity = @EntityType
	open KeysCursor
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue, 
		@PKey

	set @KeysFetch = @@Fetch_Status

While (@KeysFetch = 0)
	begin
		if (@NeedTransaction = 'Y')
			begin tran

			if exists (select 'x' from CFGAutoPosting where UninvoicedRevenue1 = @OldValue or UninvoicedRevenue2 = @OldValue or UninvoicedRevenue3 = @OldValue or UninvoicedRevenue4 = @OldValue or UninvoicedRevenue5 = @OldValue)
			begin
			SET @message = dbo.GetMessage('MsgNumSpecInRevenueGenerationSetup',@custlabel,@OldValue,@custlabel,'','','','','','')
			RAISERROR(@message,16,3)
				close keysCursor
				deallocate keysCursor
				if (@@Trancount > 0)
				   ROLLBACK TRANSACTION
				return(50002) -- user defined error
			end
	
-- Add by DP 4.0
		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany Nvarchar(14)
			DECLARE @RecordCount integer

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				set @RecordCount = (select count(*) from KeyConvertWork where Entity = @EntityType)

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),'KC','Y',  @custlabel + ' Key Convert ('+rtrim(convert(Nvarchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, left(@custlabel + ' Key Convert', 40), @custlabel);
			end

			Set @PostControlFlag = 1
		end
		set @Existing = 0
		select @Existing =  1, @NewName = Name, @NewAccountType = Type from CA where Account = @NewValue
		select @OldName = Name, @OldAccountType = Type from CA where Account = @OldValue
--

		if (@CheckAccountType = 'Y' and @Existing = 1)
			begin
			if (@NewAccountType <> @OldAccountType)
				begin
				SET @message = dbo.GetMessage('MsgCvtAcctDiffTypesConvNotCont',@custlabelPlural,'','','','','','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0 and @NeedTransaction = 'Y')
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end	
			end
	If (@Existing = 1)
		begin
			  if (@DeleteExisting = 0)
				begin
				SET @message = dbo.GetMessage('MsgNumAnExistingConvWillNotCont',@custlabel,@NewValue,@custlabel,@custLabelPlural,'','','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0 and @NeedTransaction = 'Y')
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end
				if (@NewValue = @OldValue)
					begin
					SET @message = dbo.GetMessage('MsgAreSameNumConvNotCont',@custlabelPlural,'','','','','','','','')
					RAISERROR(@message,16,3)
						close keysCursor
						deallocate keysCursor
						if (@@Trancount > 0 and @NeedTransaction = 'Y')
						   ROLLBACK TRANSACTION
						return(50002) -- user defined error
					end

				declare @MulticompanyEnabled Nvarchar(1)
				declare @MulticurrencyEnabled Nvarchar(1)
				select @MulticompanyEnabled = MulticompanyEnabled,
			   		 @MulticurrencyEnabled = MulticurrencyEnabled from FW_CFGSystem
				if (@MulticurrencyEnabled = 'Y')
					declare @oldCurrency Nvarchar(14)
					declare @newCurrency Nvarchar(14)
					begin
						set @oldCurrency = ''
						set @newCurrency = ''
						select 
							@oldCurrency = old.AccountCurrencyCode,
							@newCurrency = new.AccountCurrencyCode
						from ca new inner join ca Old on (old.Account = @OldValue)
						where new.Account = @NewValue 
						and (old.AccountCurrencyCode <> new.AccountCurrencyCode)
						order by new.Account

						if (@oldCurrency <> @newCurrency)
							begin
							SET @message = dbo.GetMessage('MsgWithMultiCmpYouCantMergeDiffCurr',@custlabelPlural,@custlabel,@OldValue,@oldCurrency,@custlabel,@NewValue,@newCurrency,'','')
							RAISERROR(@message,16,3)
								close keysCursor
								deallocate keysCursor
								if (@@Trancount > 0)
								   ROLLBACK TRANSACTION
								return(50003) -- user defined error
							end
					end

/* Removed by DP 4.0
			  exec @RetVal = KeyCvt @Entity = @Entity,
											@CvtType = @CvtType,
											@Length = 20,
											@ErrMsg = @ErrMsg2 output,
											@Diag = @Diag,
											@ConstrFlag = 3, -- UpdateTables
											@NeedTransaction = @NeedTransaction
*/
/*new for 3.0*/
--CACompanyMapping	Account,Company
	Delete from CACompanyMapping where Account = @OldValue 
		and exists (select 'x' from CACompanyMapping new 
			where Account = @newValue and new.Company = CACompanyMapping.Company)
/*end 3.0*/

/*new for 2.0*/
/*
BudgetMilestoneSeq AlertID 
BudgetMilestoneSeq PlanID  
BudgetMilestoneSeq Account 
BudgetMilestoneSeq Vendor  
*/

				Delete from BudgetMilestoneSeq where Account = @OldValue 
					and exists (select 'x' from BudgetMilestoneSeq new where  BudgetMilestoneSeq.AlertID = new.AlertID and
																			BudgetMilestoneSeq.PlanID = new.PlanID and
																			BudgetMilestoneSeq.Vendor = new.Vendor and
																			BudgetMilestoneSeq.Account = @newValue)
/*end 2.0*/
-- Removed by DP
--				Delete from CA where Account = @OldValue


				update EB
				set eb.AmtBud =  eb.AmtBud + old.AmtBud,
				eb.BillBud = eb.BillBud + old.BillBud,
				eb.EtcAmt =  eb.EtcAmt + old.EtcAmt,
				eb.EacAmt = eb.EacAmt + old.EacAmt
				from eb old inner join eb on (eb.WBS1 = old.WBS1 and eb.WBS2 = old.WBS2 and	eb.WBS3 = old.WBS3 and 	eb.Vendor = old.Vendor)
				where
					eb.Account = @newValue and
					old.Account = @OldValue

				Delete from EB where Account = @OldValue 
					and exists (select 'x' from EB new where  EB.WBS1 = new.WBS1 and
																			EB.WBS2 = new.WBS2 and
																			EB.WBS3 = new.WBS3 and 
																			eb.Vendor = new.Vendor and
																			new.Account = @newValue)

				IF EXISTS (SELECT 'x' FROM sys.objects WHERE name = 'AccountCustomTabFields' AND type = 'U')
					begin
						delete from AccountCustomTabFields where Account = @OldValue 
							and exists (select 'x' from AccountCustomTabFields where Account = @NewValue)
					end

				update cabDetail
				set
					cabDetail.Annual =  cabDetail.Annual + old.Annual,
					cabDetail.Amount1 = cabDetail.Amount1 + old.Amount1,
					cabDetail.Amount2 = cabDetail.Amount2 + old.Amount2,
					cabDetail.Amount3 = cabDetail.Amount3 + old.Amount3,
					cabDetail.Amount4 = cabDetail.Amount4 + old.Amount4,
					cabDetail.Amount5 = cabDetail.Amount5 + old.Amount5,
					cabDetail.Amount6 = cabDetail.Amount6 + old.Amount6,
					cabDetail.Amount7 = cabDetail.Amount7 + old.Amount7,
					cabDetail.Amount8 = cabDetail.Amount8 + old.Amount8,
					cabDetail.Amount9 = cabDetail.Amount9 + old.Amount9,
					cabDetail.Amount10 = cabDetail.Amount10 + old.Amount10,
					cabDetail.Amount11 = cabDetail.Amount11 + old.Amount11,
					cabDetail.Amount12 = cabDetail.Amount12 + old.Amount12,
					cabDetail.Amount13 = cabDetail.Amount13 + old.Amount13
				from cabDetail old inner join cabDetail on (cabDetail.BudgetName = old.BudgetName )
				where	cabDetail.Account = @NewValue and
				old.Account = @oldValue

				Delete from CABDetail 
					where Account = @OldValue And exists  
					(select 'x'  from CABDetail new where new.BudgetName = cabdetail.budgetName and new.Account = @NewValue)      
				Delete from CFGInvMap 
					where Account = @OldValue 
					and exists (select 'x' from CFGInvMap where Account = @NewValue)
				Delete from CFGARMap 
					where Account = @OldValue 
					and exists (select 'x' from CFGARMap where Account = @NewValue)
				Delete from CFGOHAccounts 
					where Account = @OldValue 
					and exists (select 'x' from CFGOHAccounts where Account = @NewValue)
				Delete from BTEAAccts 
					where Account = @OldValue And TableNo in 
					(select tableNo from BTEAAccts where Account = @NewValue)
				Delete from BTECAccts 
					where Account =  @OldValue And exists
					(select 'x' from BTECAccts new where new.tableNo = BTECAccts.TableNo and new.Account = @NewValue)

				Delete from Consolidations 
					where Account = @OldValue And exists  
					(select 'x'  from Consolidations new where new.ReportingGroup = Consolidations.ReportingGroup and new.Company = Consolidations.Company and new.Period = Consolidations.Period and new.Account = @NewValue)      
				Delete from ConsolidationBudgetsDetail 
					where Account = @OldValue And exists  
					(select 'x'  from ConsolidationBudgetsDetail new where new.BudgetName = ConsolidationBudgetsDetail.BudgetName and new.Account = @NewValue)      
				Delete from CFGConsolidationEliminations 
					where Account = @OldValue And exists  
					(select 'x'  from CFGConsolidationEliminations new where new.ReportingGroup = CFGConsolidationEliminations.ReportingGroup and new.Account = @NewValue)      
				Delete from CFGConsolidationTranslationMethodOverrides 
					where Account = @OldValue And exists  
					(select 'x'  from CFGConsolidationTranslationMethodOverrides new where new.ReportingGroup = CFGConsolidationTranslationMethodOverrides.ReportingGroup and new.Account = @NewValue)      
				Delete from Eliminations 
					where Account = @OldValue And exists  
					(select 'x'  from Eliminations new where new.ReportingGroup = Eliminations.ReportingGroup and new.Company = Eliminations.Company and new.Period = Eliminations.Period and new.Account = @NewValue)      


				/*handle duplicate custom tab records, possible when copying one project to another*/

				declare custTable cursor for
				select tableName from FW_CustomGrids where infocenterarea = N'Accounts'
				open custTable
				fetch next from custTable into @TableName
				while (@@FETCH_STATUS = 0)
				begin
					set @Sql = 'Delete FROM ' + @TableName + ' WHERE ' + @TableName + '.Account =N''' +  @OldValue + ''' and ' +
								  'exists (select ''x'' from ' + @TableName + ' new where new.Account = N''' + @NewValue + ''' and new.seq = ' + @TableName + '.seq)'
					if (@Diag = 1)
						print @Sql
					execute(@Sql)
					fetch next from custTable into @TableName
				end
				close custTable
				deallocate custTable					


			declare @PlanID varchar(32)
			declare @WBS1	Nvarchar(30),
					@WBS2	Nvarchar(30),
					@WBS3	Nvarchar(30)
			declare @Account		Nvarchar(13),
					@Vendor		Nvarchar(20),
					@OldExpenseID	Nvarchar(32),
					@NewExpenseID	Nvarchar(32),
					@OldConsultantID	Nvarchar(32),
					@NewConsultantID	Nvarchar(32)
			declare @OldTaskID		varchar(32)
			declare @NewTaskID		varchar(32)
			declare @TempFetch integer

-- Move Planned and Basedline for the duplicate RPExpense, Delete duplicate RPExpense
			declare ExpenseCurso cursor for
				select PlanID, TaskID, isnull(WBS1,'') as WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Account,'') as Account, isnull(Vendor,'') as Vendor, MIN(ExpenseID) as ExpenseID  
				from RPExpense where Account=@OldValue and
				exists (select 'x' from RPExpense new where new.PlanID=RPExpense.PlanID 
					and isnull(new.wbs1,'') = isnull(RPExpense.wbs1,'') 
					and isnull(new.wbs2,'') = isnull(RPExpense.wbs2,'')
					and isnull(new.wbs3,'') = isnull(RPExpense.wbs3,'')
					and isnull(new.Account,'') = @NewValue
					and isnull(new.Vendor,'') = isnull(RPExpense.Vendor,''))
				group by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
				Order by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
			open ExpenseCurso
			fetch next from ExpenseCurso into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldExpenseID
			set @TempFetch = @@Fetch_Status
			While (@TempFetch = 0)
			begin
				select @NewTaskID=TaskID, @NewExpenseID=ExpenseID from RPExpense 
					where PlanID=@PlanID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(Account,'')=@NewValue 
					and isnull(Vendor,'')=isnull(@Vendor,'') 

				Update RPPlannedExpenses Set PlanID=@PlanID, TaskID=@NewTaskID, ExpenseID=@NewExpenseID where ExpenseID=@OldExpenseID
				Update RPBaselineExpenses Set PlanID=@PlanID, TaskID=@NewTaskID, ExpenseID=@NewExpenseID where ExpenseID=@OldExpenseID

				UPDATE A SET 
					StartDate = CASE WHEN XTPD.StartDate < A.StartDate THEN XTPD.StartDate ELSE A.StartDate END, 
					EndDate = CASE WHEN XTPD.EndDate > A.EndDate THEN XTPD.EndDate ELSE A.EndDate END
					FROM RPExpense AS A
					  INNER JOIN (
						SELECT PlanID, TaskID, ExpenseID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
						  FROM RPPlannedExpenses
						  WHERE ExpenseID=@NewExpenseID
						  GROUP BY PlanID, TaskID, ExpenseID
					  ) AS XTPD 
						ON A.PlanID = XTPD.PlanID AND A.TaskID = XTPD.TaskID AND A.ExpenseID = XTPD.ExpenseID

				delete RPExpense Where PlanID=@PlanID and TaskID=@OldTaskID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(Account,'')=@OldValue
					and isnull(Vendor,'')=isnull(@Vendor,'')

				fetch next from ExpenseCurso into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldExpenseID
				set @TempFetch = @@Fetch_Status
			end
			close ExpenseCurso
			deallocate ExpenseCurso

-- Move Planned and Basedline for the duplicate RPConsultant, Delete duplicate RPConsultant
			declare ConsultantCurso cursor for
				select PlanID, TaskID, isnull(WBS1,'') as WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Account,'') as Account, isnull(Vendor,'') as Vendor, MIN(ConsultantID) as ConsultantID  
				from RPConsultant where Account=@OldValue and
				exists (select 'x' from RPConsultant new where new.PlanID=RPConsultant.PlanID 
					and isnull(new.wbs1,'') = isnull(RPConsultant.wbs1,'') 
					and isnull(new.wbs2,'') = isnull(RPConsultant.wbs2,'')
					and isnull(new.wbs3,'') = isnull(RPConsultant.wbs3,'')
					and isnull(new.Account,'') = @NewValue
					and isnull(new.Vendor,'') = isnull(RPConsultant.Vendor,''))
				group by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
				Order by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
			open ConsultantCurso
			fetch next from ConsultantCurso into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldConsultantID
			set @TempFetch = @@Fetch_Status
			While (@TempFetch = 0)
			begin
				select @NewTaskID=TaskID, @NewConsultantID=ConsultantID from RPConsultant 
					where PlanID=@PlanID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(Account,'')=@NewValue 
					and isnull(Vendor,'')=isnull(@Vendor,'') 

				Update RPPlannedConsultant Set PlanID=@PlanID, TaskID=@NewTaskID, ConsultantID=@NewConsultantID where ConsultantID=@OldConsultantID
				Update RPBaselineConsultant Set PlanID=@PlanID, TaskID=@NewTaskID, ConsultantID=@NewConsultantID where ConsultantID=@OldConsultantID

				UPDATE A SET 
					StartDate = CASE WHEN XTPD.StartDate < A.StartDate THEN XTPD.StartDate ELSE A.StartDate END, 
					EndDate = CASE WHEN XTPD.EndDate > A.EndDate THEN XTPD.EndDate ELSE A.EndDate END
					FROM RPConsultant AS A
					  INNER JOIN (
						SELECT PlanID, TaskID, ConsultantID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
						  FROM RPPlannedConsultant
						  WHERE ConsultantID=@NewConsultantID
						  GROUP BY PlanID, TaskID, ConsultantID
					  ) AS XTPD 
						ON A.PlanID = XTPD.PlanID AND A.TaskID = XTPD.TaskID AND A.ConsultantID = XTPD.ConsultantID

				delete RPConsultant Where PlanID=@PlanID and TaskID=@OldTaskID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(Account,'')=@OldValue
					and isnull(Vendor,'')=isnull(@Vendor,'')

				fetch next from ConsultantCurso into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldConsultantID
				set @TempFetch = @@Fetch_Status
			end
			close ConsultantCurso
			deallocate ConsultantCurso

-- Move Planned and Basedline for the duplicate PNExpense, Delete duplicate PNExpense
			declare ExpenseCurso cursor for
				select PlanID, TaskID, isnull(WBS1,'') as WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Account,'') as Account, isnull(Vendor,'') as Vendor, MIN(ExpenseID) as ExpenseID  
				from PNExpense where Account=@OldValue and
				exists (select 'x' from PNExpense new where new.PlanID=PNExpense.PlanID 
					and isnull(new.wbs1,'') = isnull(PNExpense.wbs1,'') 
					and isnull(new.wbs2,'') = isnull(PNExpense.wbs2,'')
					and isnull(new.wbs3,'') = isnull(PNExpense.wbs3,'')
					and isnull(new.Account,'') = @NewValue
					and isnull(new.Vendor,'') = isnull(PNExpense.Vendor,''))
				group by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
				Order by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
			open ExpenseCurso
			fetch next from ExpenseCurso into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldExpenseID
			set @TempFetch = @@Fetch_Status
			While (@TempFetch = 0)
			begin
				select @NewTaskID=TaskID, @NewExpenseID=ExpenseID from PNExpense 
					where PlanID=@PlanID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(Account,'')=@NewValue 
					and isnull(Vendor,'')=isnull(@Vendor,'') 

				Update PNPlannedExpenses Set PlanID=@PlanID, TaskID=@NewTaskID, ExpenseID=@NewExpenseID where ExpenseID=@OldExpenseID
				Update PNBaselineExpenses Set PlanID=@PlanID, TaskID=@NewTaskID, ExpenseID=@NewExpenseID where ExpenseID=@OldExpenseID

				UPDATE A SET 
					StartDate = CASE WHEN XTPD.StartDate < A.StartDate THEN XTPD.StartDate ELSE A.StartDate END, 
					EndDate = CASE WHEN XTPD.EndDate > A.EndDate THEN XTPD.EndDate ELSE A.EndDate END
					FROM PNExpense AS A
					  INNER JOIN (
						SELECT PlanID, TaskID, ExpenseID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
						  FROM PNPlannedExpenses
						  WHERE ExpenseID=@NewExpenseID
						  GROUP BY PlanID, TaskID, ExpenseID
					  ) AS XTPD 
						ON A.PlanID = XTPD.PlanID AND A.TaskID = XTPD.TaskID AND A.ExpenseID = XTPD.ExpenseID

				delete PNExpense Where PlanID=@PlanID and TaskID=@OldTaskID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(Account,'')=@OldValue
					and isnull(Vendor,'')=isnull(@Vendor,'')

				fetch next from ExpenseCurso into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldExpenseID
				set @TempFetch = @@Fetch_Status
			end
			close ExpenseCurso
			deallocate ExpenseCurso

-- Move Planned and Basedline for the duplicate PNConsultant, Delete duplicate PNConsultant
			declare ConsultantCurso cursor for
				select PlanID, TaskID, isnull(WBS1,'') as WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Account,'') as Account, isnull(Vendor,'') as Vendor, MIN(ConsultantID) as ConsultantID  
				from PNConsultant where Account=@OldValue and
				exists (select 'x' from PNConsultant new where new.PlanID=PNConsultant.PlanID 
					and isnull(new.wbs1,'') = isnull(PNConsultant.wbs1,'') 
					and isnull(new.wbs2,'') = isnull(PNConsultant.wbs2,'')
					and isnull(new.wbs3,'') = isnull(PNConsultant.wbs3,'')
					and isnull(new.Account,'') = @NewValue
					and isnull(new.Vendor,'') = isnull(PNConsultant.Vendor,''))
				group by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
				Order by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
			open ConsultantCurso
			fetch next from ConsultantCurso into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldConsultantID
			set @TempFetch = @@Fetch_Status
			While (@TempFetch = 0)
			begin
				select @NewTaskID=TaskID, @NewConsultantID=ConsultantID from PNConsultant 
					where PlanID=@PlanID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(Account,'')=@NewValue 
					and isnull(Vendor,'')=isnull(@Vendor,'') 

				Update PNPlannedConsultant Set PlanID=@PlanID, TaskID=@NewTaskID, ConsultantID=@NewConsultantID where ConsultantID=@OldConsultantID
				Update PNBaselineConsultant Set PlanID=@PlanID, TaskID=@NewTaskID, ConsultantID=@NewConsultantID where ConsultantID=@OldConsultantID

				UPDATE A SET 
					StartDate = CASE WHEN XTPD.StartDate < A.StartDate THEN XTPD.StartDate ELSE A.StartDate END, 
					EndDate = CASE WHEN XTPD.EndDate > A.EndDate THEN XTPD.EndDate ELSE A.EndDate END
					FROM PNConsultant AS A
					  INNER JOIN (
						SELECT PlanID, TaskID, ConsultantID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
						  FROM PNPlannedConsultant
						  WHERE ConsultantID=@NewConsultantID
						  GROUP BY PlanID, TaskID, ConsultantID
					  ) AS XTPD 
						ON A.PlanID = XTPD.PlanID AND A.TaskID = XTPD.TaskID AND A.ConsultantID = XTPD.ConsultantID

				delete PNConsultant Where PlanID=@PlanID and TaskID=@OldTaskID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(Account,'')=@OldValue
					and isnull(Vendor,'')=isnull(@Vendor,'')

				fetch next from ConsultantCurso into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldConsultantID
				set @TempFetch = @@Fetch_Status
			end
			close ConsultantCurso
			deallocate ConsultantCurso

	    END --Existing

-- Add by DP 4.0
		Else	-- Not Existing
		begin
			select * into #TempKeyConvert from CA where Account = @OldValue
			Update #TempKeyConvert set Account=@NewValue
			Insert CA select * from #TempKeyConvert
			Drop Table #TempKeyConvert
			set @NewName = @OldName
		end

		exec @RetVal = KeyCvt @Entity = @Entity,
					 @CvtType = @CvtType,
					 @Length = 20,
					 @ErrMsg = @ErrMsg2 output,
					 @Diag = @Diag,
					 @NewValue = @NewValue,
					 @OldValue = @OldValue,
					 @NeedTransaction = @NeedTransaction,
					 @ConstrFlag = 3 -- UpdateTables

		Delete from CA where Account = @OldValue
--

		if (@RetVal = 0)
			begin
-- Add by DP 4.0
				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq)
					select PKey, Entity, OldKey, NewKey, @OldName, @NewName, @Period, @LastPostSeq from keyConvertWork where Pkey = @PKey
				end
--
				delete from keyConvertWork where current of keysCursor
				if (@NeedTransaction = 'Y')
					commit Transaction
			end
		else
			begin
				set @ErrMsg = @ErrMsg + @ErrMsg2
   			    if (@NeedTransaction = 'Y')
					rollback transaction
			end
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue, 
		@PKey
	set @KeysFetch = @@Fetch_Status
	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	return(0)
End -- Procedure
GO
