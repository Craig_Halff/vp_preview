SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_Load_Settings] ( @isEIUser bit, @user nvarchar(32))
             AS EXEC spCCG_PAT_Config_Load_Settings @isEIUser,@user
GO
