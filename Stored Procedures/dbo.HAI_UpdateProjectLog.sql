SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_UpdateProjectLog] @targetPeriod AS INT
AS
    BEGIN
        /* 
		Copyright (c) 2021 Halff Associates, Inc.  All rights reserved.
		5/29/2021	Craig H. Anderson
	
		Write results of last Revenue Recognitiuon calculations to the Project History log for future reference.
	*/
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
        SET NOCOUNT ON;
        DECLARE @now DATETIME = GETDATE();
        DECLARE @user VARCHAR(25) = 'HAI_UpdateProjectLog';
        INSERT INTO dbo.Projects_ProjectLog (
            WBS1
          , WBS2
          , WBS3
          , Seq
          , CreateUser
          , CreateDate
          , ModUser
          , ModDate
          , CustTotalCompensation
          , CustEAC
          , CustJTDBilling
          , CustETC
          , CustWriteOffAdj
          , CustJTDRevenue
          , CustFeeTypeRollup
          , CustJTDBilled
          , CustJTDUnbilled
          , CustJTDCost
          , CustJTDProfit
          , CustPeriod
          , CustJTDLaborRevenue
          , CustJTDExpRevenue
          , CustJTDConRevenue
        )
            SELECT r.WBS1
                 , r.WBS2
                 , r.WBS3
                 , REPLACE(NEWID(), '-', '')                                                       AS Seq
                 , @user                                                                           AS CreatUser
                 , @now                                                                            AS CreateDate
                 , @user                                                                           AS ModUser
                 , @now                                                                            AS ModDate
                 , (p.FeeDirLab + p.FeeDirExp + p.ConsultFee + p.ReimbAllowCons + p.ReimbAllowExp) AS CustTotalCompensation
                 , r.EAC                                                                           AS CustEAC
                 , r.TotalSpent
                 , r.ETC                                                                           AS ETC
                 , r.CustWriteoffAdjmt                                                             AS WriteOffAdj
                 , r.TotalRevenue
                 , r.CustFeeTypeRollup
                 , r.JTDBilled
                 , r.JTDUnbilled
                 , r.JTDCost
                 , ISNULL(r.TotalRevenue, 0) - ISNULL(r.JTDCost, 0)                                AS CustJTDProfit
                 , @targetPeriod                                                                   AS CustPeriod
                 , r.CustRevenueLabor
                 , r.CustRevenueExpense
                 , r.CustRevenueConsultant
                FROM dbo._Revenue AS r WITH (NOLOCK)
                INNER JOIN dbo.PR AS p
                    ON r.WBS1 = p.WBS1
                        AND p.WBS2 = r.WBS2
                        AND p.WBS3 = r.WBS3
                WHERE NOT EXISTS (
                    SELECT 'x'
                        FROM dbo.Projects_ProjectLog WITH (NOLOCK)
                        WHERE
                        WBS1 = r.WBS1
                            AND WBS2 = r.WBS2
                            AND WBS3 = r.WBS3
                            AND CustPeriod = @targetPeriod
                );

        UPDATE h
            SET h.ModUser = @user
              , h.ModDate = @now
              , h.CustTotalCompensation = d.CustTotalCompensation
              , h.CustEAC = d.CustEAC
              , h.CustJTDBilling = d.TotalSpent
              , h.CustETC = d.ETC
              , h.CustWriteOffAdj = d.WriteOffAdj
              , h.CustJTDRevenue = d.TotalRevenue
              , h.CustFeeTypeRollup = d.CustFeeTypeRollup
              , h.CustJTDBilled = d.JTDBilled
              , h.CustJTDUnbilled = d.JTDUnbilled
              , h.CustJTDCost = d.JTDCost
              , h.CustJTDProfit = d.CustJTDProfit
              , h.CustPeriod = @targetPeriod
              , h.CustJTDLaborRevenue = d.CustRevenueLabor
              , h.CustJTDExpRevenue = d.CustRevenueExpense
              , h.CustJTDConRevenue = d.CustRevenueConsultant
            FROM dbo.Projects_ProjectLog h
            INNER JOIN (
                SELECT r.WBS1
                     , r.WBS2
                     , r.WBS3
                     , @user                                                                           AS ModUser
                     , @now                                                                            AS ModDate
                     , (p.FeeDirLab + p.FeeDirExp + p.ConsultFee + p.ReimbAllowCons + p.ReimbAllowExp) AS CustTotalCompensation
                     , r.EAC                                                                           AS CustEAC
                     , r.TotalSpent
                     , r.ETC                                                                           AS ETC
                     , r.CustWriteoffAdjmt                                                             AS WriteOffAdj
                     , r.TotalRevenue
                     , r.CustFeeTypeRollup
                     , r.JTDBilled
                     , r.JTDUnbilled
                     , r.JTDCost
                     , ISNULL(r.TotalRevenue, 0) - ISNULL(r.JTDCost, 0)                                AS CustJTDProfit
                     , r.CustRevenueLabor
                     , r.CustRevenueExpense
                     , r.CustRevenueConsultant
                    FROM dbo._Revenue AS r WITH (NOLOCK)
                    INNER JOIN dbo.PR AS p WITH (NOLOCK)
                        ON r.WBS1 = p.WBS1
                            AND p.WBS2 = r.WBS2
                            AND p.WBS3 = r.WBS3
            )                            d
                ON h.WBS1 = d.WBS1
                    AND h.WBS2 = d.WBS2
                    AND h.WBS3 = d.WBS3
            WHERE h.CustPeriod = @targetPeriod;
    END;
GO
