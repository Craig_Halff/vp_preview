SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_PAT_LoadCompany] (@requiredCompany Nvarchar(16) = N' ')
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

	SELECT Company, FirmName + case when Company <> '' then N' (' + Company + N')' else '' end as DisplayName
        FROM CFGMainData c
			left join CFGOrgCodesData oc on oc.Code = c.Company and oc.OrgLevel = 1
		WHERE (c.Company = @requiredCompany or isnull(oc.Status, '') <> 'D')
		ORDER BY 2
END
GO
