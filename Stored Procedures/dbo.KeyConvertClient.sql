SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyConvertClient]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg Nvarchar(1000) output,
@UserName Nvarchar(32) = 'VisionKeyConvert', -- Add by DP 4.0
@Period integer = 0,
@EntityType Nvarchar(20)
as
begin -- Procedure
	set nocount on
	declare @OldValue Nvarchar(32)
	declare @NewValue Nvarchar(32)
	Declare @Existing integer
	declare @Entity Nvarchar(20)
	declare @length integer
	declare @CvtType integer
	declare @KeysFetch integer
	declare @RetVal integer
	declare @diag smallint
	declare @Errmsg2 Nvarchar(1000),
			@custlabel	Nvarchar(40),
			@Custlabelplural Nvarchar(40)
	declare @TableName 		Nvarchar(100)
	declare @Sql 				Nvarchar(1000)
	declare @message nvarchar(max)

	declare @OldVendor Nvarchar(20)
	declare @NewVendor Nvarchar(20)
	DECLARE @OldVendorName Nvarchar(125)
	DECLARE @NewVendorName Nvarchar(125)
	Declare @ExistingClientOld integer
	Declare @ExistingClientNew integer
	Declare @ExistingVendorOld integer
	Declare @ExistingVendorNew integer
	declare @custlabelClient	Nvarchar(40),
			@CustlabelpluralClient Nvarchar(40)
	declare @custlabelVendor	Nvarchar(40),
			@CustlabelpluralVendor Nvarchar(40)
	declare @NewAddress Nvarchar(20)
	declare @OldAddress Nvarchar(20)
	declare @AddressFetch integer
	declare @AppendInt	integer

	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'Client'
	set @length = 32
	set @Existing = 0
	set @KeysFetch = 0

	set @diag = 0
	execute InsertKeyCvtDriver 'Client' -- populate driver table
	execute InsertKeyCvtDriver 'Vendor' -- populate driver table

	delete from keyConvertDriver where entity = N'Client' and TableName = N'Clendor' and columnname = N'Client'
	delete from keyconvertDriver where entity = N'Client' and TableName = N'Clendor' and ColumnName = N'ClientID'
	delete from keyconvertDriver where entity = N'Vendor' and TableName = N'Clendor' and columnname = N'Vendor'

	DECLARE @OldName Nvarchar(125)
	DECLARE @NewName Nvarchar(125)
	DECLARE @PKey Nvarchar(50)
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0

	select @custlabel = LabelValue from FW_CFGLabels where LabelName = N'firmLabel'
	select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = N'firmLabelPlural'
	select @custlabelClient = LabelValue from FW_CFGLabels where LabelName = N'ClientLabel'
	select @custlabelpluralClient = LabelValue from FW_CFGLabels where LabelName = N'ClientLabelPlural'
	select @custlabelVendor = LabelValue from FW_CFGLabels where LabelName = N'VendorLabel'
	select @custlabelpluralVendor = LabelValue from FW_CFGLabels where LabelName = N'VendorLabelPlural'

	declare KeysCursor cursor for
		select OldKey, NewKey, Pkey from KeyConvertWork where Entity = @Entity
	open KeysCursor
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue, 
		@PKey

	set @KeysFetch = @@Fetch_Status

	While (@KeysFetch = 0)
	begin
		begin tran

		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany Nvarchar(14)
			DECLARE @RecordCount integer
			set @LastPostSeq = 0

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				set @RecordCount = (select count(*) from KeyConvertWork where Entity = @Entity)

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),N'KC','Y',  @custlabel + ' Key Convert ('+rtrim(convert(Nvarchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, left(@custlabel + ' Key Convert', 40), @custlabel);
			end

			Set @PostControlFlag = 1
		end

		Declare @ClientIndOld varchar(1), @VendorIndOld varchar(1), @GovernmentAgencyOld varchar(1), @CompetitorOld varchar(1), @ReadyForProcessingOld varchar(1), @AvailableForCRMOld varchar(1), @ReadyForApprovalOld varchar(1)
		Declare @ClientIndNew varchar(1), @VendorIndNew varchar(1), @GovernmentAgencyNew varchar(1), @CompetitorNew varchar(1), @ReadyForProcessingNew varchar(1), @AvailableForCRMNew varchar(1), @ReadyForApprovalNew varchar(1)
		Declare @OldClient Nvarchar(20), @NewClient Nvarchar(20)
		Select @ClientIndOld = 'N', @VendorIndOld = 'N', @GovernmentAgencyOld = 'N', @CompetitorOld = 'N', @ReadyForProcessingOld = 'N', @AvailableForCRMOld = 'N', @ReadyForApprovalOld = 'N'
			, @ClientIndNew = 'N', @VendorIndNew = 'N', @GovernmentAgencyNew = 'N', @CompetitorNew = 'N', @ReadyForProcessingNew = 'N', @AvailableForCRMNew = 'N', @ReadyForApprovalNew = 'N'
			, @OldClient = '', @NewClient = ''

		set @ExistingClientOld = 0
		select @ExistingClientOld =  case when Clendor.ClientID > '' then 1 else 0 end, @OldClient = Client, @OldName = isnull(Client + ' - ','') + Name,  
		@ClientIndOld = ClientInd, @VendorIndOld = VendorInd, @GovernmentAgencyOld = GovernmentAgency, @CompetitorOld = Competitor
		, @ReadyForProcessingOld = ReadyForProcessing, @AvailableForCRMOld = AvailableForCRM, @ReadyForApprovalOld = ReadyForApproval from Clendor where ClientID = @OldValue
		set @ExistingClientNew = 0
		select @ExistingClientNew =  case when Clendor.ClientID > '' then 1 else 0 end, @NewClient = Client, @NewName = isnull(Client + ' - ','') + Name, 
		@ClientIndNew = ClientInd, @VendorIndNew = VendorInd, @GovernmentAgencyNew = GovernmentAgency, @CompetitorNew = Competitor
		, @ReadyForProcessingNew = ReadyForProcessing, @AvailableForCRMNew = AvailableForCRM, @ReadyForApprovalNew = ReadyForApproval from Clendor where ClientID = @NewValue

		set @ExistingVendorOld = 0
		select @ExistingVendorOld =  case when Clendor.VendorInd = 'Y' then 1 else 0 end, @OldVendor = Vendor, @OldVendorName = Vendor + ' - ' + Name from Clendor where ClientID = @OldValue
		set @ExistingVendorNew = 0
		select @ExistingVendorNew =  case when Clendor.VendorInd = 'Y' then 1 else 0 end, @NewVendor = Vendor, @NewVendorName = Vendor + ' - ' + Name from Clendor where ClientID = @NewValue

		if (@diag = 1) 
			begin
				print '@OldClient - ' + @OldClient + ', ' + '@ClientIndOld - ' + @ClientIndOld + ', ' + '@VendorIndOld - ' + @VendorIndOld + ', ' + '@GovernmentAgencyOld - ' + @GovernmentAgencyOld + ', ' + '@CompetitorOld - ' + @CompetitorOld
				print '@NewClient - ' + @NewClient + ', ' + '@ClientIndNew - ' + @ClientIndNew + ', ' + '@VendorIndNew - ' + @VendorIndNew + ', ' + '@GovernmentAgencyNew - ' + @GovernmentAgencyNew + ', ' + '@CompetitorNew - ' + @CompetitorNew
				print '@OldVendor - ' + @OldVendor + ', ' + '@NewVendor - ' + @NewVendor
			end

--1 Old CLient exist, Combining Client records

		If (@ExistingClientOld = 1 or @ExistingVendorOld = 1)
			begin
				if (@diag=1) print '1 Old CLient exist, Combining Client records'

				if (@DeleteExisting = 0)
					begin
						SET @message = dbo.GetMessage('MsgNumAnExistingConvWillNotCont',@custlabelClient,@NewValue,@custlabelClient,@CustlabelpluralClient,'','','','','')
						RAISERROR(@message,16,3)
							close keysCursor
							deallocate keysCursor
							if (@@Trancount > 0)
							   ROLLBACK TRANSACTION
							return(50002) -- user defined error
					end
				if (@NewValue = @OldValue)
					begin
						SET @message = dbo.GetMessage('MsgAreSameNumConvNotCont',@CustlabelpluralClient,'','','','','','','','')
						RAISERROR(@message,16,3)
							close keysCursor
							deallocate keysCursor
							if (@@Trancount > 0)
								ROLLBACK TRANSACTION
							return(50002) -- user defined error
					end

--ARC (ClientID, CommentDate, Username)
				Delete from ARC where ClientID = @OldValue
					and exists (select 'x' from ARC new where new.ClientID = @newValue and
																			ARC.CommentDate = new.CommentDate and
																			ARC.Username = new.userName)

				Delete from ClientPhoto where ClientID = @OldValue
					and exists (select 'x' from ClientPhoto new where new.ClientID = @newValue)

--CLAddress(ClientID, Address) Preserver duplicate address codes by renumbering them
				set @AppendInt = 0
				declare AddressCursor cursor for select Address from CLAddress where ClientID = @OldValue 
									and exists (select 'x' from CLAddress new where new.ClientID = @newValue and
																								CLAddress.Address = new.Address)
				open addressCursor
				fetch next from AddressCursor into @OldAddress
				set @AddressFetch = @@FETCH_STATUS
				while (@AddressFetch = 0)
					begin
						SET @AppendInt = @AppendInt + 1
						Set @NewAddress = left(@OldAddress,20 - len(convert(Nvarchar,@AppendInt))) + convert(Nvarchar,@AppendInt)

						if not exists (select 'x' from CLAddress where (clientID = @oldValue or clientID = @newValue) and Address = @NewAddress)
							begin
								update CLAddress set Address = @NewAddress where ClientID = @OldValue and Address = @OldAddress
								update Contacts set CLAddress = @NewAddress where ClientID = @OldValue and CLAddress = @OldAddress
								update PR set CLAddress = @NewAddress where ClientID = @OldValue and CLAddress = @OldAddress
								update PR set CLBillingAddr = @NewAddress where ClientID = @OldValue and CLBillingAddr = @OldAddress

								update PRDefaults set CLAddress = @NewAddress where ClientID = @OldValue and CLAddress = @OldAddress
								update PRDefaults set CLBillingAddr = @NewAddress where ClientID = @OldValue and CLBillingAddr = @OldAddress
								update PRTemplate set CLAddress = @NewAddress where ClientID = @OldValue and CLAddress = @OldAddress
								update PRTemplate set CLBillingAddr = @NewAddress where ClientID = @OldValue and CLBillingAddr = @OldAddress

								fetch next from AddressCursor into @OldAddress
								set @AddressFetch = @@FETCH_STATUS
								SET @AppendInt = 0
							end
					end
				close addressCursor
				deallocate addressCursor

				update CLAddress set PrimaryInd ='N', Billing = 'N', Payment= case when exists(select payment from claddress new where new.ClientID=@NewValue and new.Payment='Y') then 'N' else Payment end  where ClientID = @OldValue
--this should never delete anything

				Delete from CLAddress where ClientID = @OldValue 
					and exists (select 'x' from CLAddress new where new.ClientID = @newValue and
																				CLAddress.Address = new.Address)

--ClientCustomTabFields		(ClientID)
				Delete from ClientCustomTabFields where ClientID = @OldValue
					and exists (select 'x' from ClientCustomTabFields new where new.ClientID = @newValue)

--ClientFileLinks	(LinkID, ClientID)
				Delete from ClientFileLinks where ClientID = @OldValue 
					and exists (select 'x' from ClientFileLinks new where new.ClientID = @newValue and
																				ClientFileLinks.LinkID = new.LinkID)
--ClientToClientAssoc (FromClientID, ToClientID)
				Delete from ClientToClientAssoc where FromClientID = @OldValue
								and exists (select 'x' from ClientToClientAssoc new where new.FromClientID = @newValue and
									(ClientToClientAssoc.ToClientID = new.ToClientID or new.ToClientID = @OldValue))

				Delete from ClientToClientAssoc where ToClientID = @OldValue
								and exists (select 'x' from ClientToClientAssoc new where new.ToClientID = @newValue and
									(ClientToClientAssoc.FromClientID = new.FromClientID or new.FromClientID = @OldValue))

--CLSubscr							(ClientID, UserName)
				Delete from CLSubscr where ClientID = @OldValue 
					and exists (select 'x' from CLSubscr new where new.ClientID = @newValue and
																				CLSubscr.UserName = new.UserName)
--CustomProposalClient		(CustomPropID, SectionID, ClientID)

				Delete from CustomProposalClient where ClientID = @OldValue
					and exists (select 'x' from CustomProposalClient new where new.ClientID = @newValue and
																			CustomProposalClient.CustomPropID = new.CustomPropID and
																			CustomProposalClient.SectionID = new.SectionID)

--CustomProposalClientGraphics		(CustomPropID, SectionID, ClientID, LinkID)
				Delete from CustomProposalClientGraphics where ClientID = @OldValue
					and exists (select 'x' from CustomProposalClientGraphics new where new.ClientID = @newValue and
																			CustomProposalClientGraphics.CustomPropID = new.CustomPropID and
																			CustomProposalClientGraphics.SectionID = new.SectionID and
																			CustomProposalClientGraphics.LinkID = new.LinkID)

--EMClientAssoc		(Employee, ClientID)
				Delete from EMClientAssoc where ClientID = @OldValue 
					and exists (select 'x' from EMClientAssoc new where new.ClientID = @newValue and
																				EMClientAssoc.Employee = new.Employee)

				Delete from WorkflowCLActivity where ClientID = @OldValue 
					and exists (select 'x' from WorkflowCLActivity new where new.ClientID = @newValue and
																				WorkflowCLActivity.ActivityID = new.ActivityID)

	/*
	ClendorProjectAssoc ClientID  
	ClendorProjectAssoc WBS1    
	ClendorProjectAssoc WBS2    
	ClendorProjectAssoc WBS3    
	*/

				Delete from ClendorProjectAssoc where ClientID = @OldValue
					and exists (select 'x' from ClendorProjectAssoc new where  ClendorProjectAssoc.WBS1 = new.WBS1 and
																			ClendorProjectAssoc.WBS2 = new.WBS2 and
																			ClendorProjectAssoc.WBS3 = new.WBS3 and 
																			new.ClientID =  @NewValue)
				Delete from ClendorProjectAssocTemplate where ClientID = @OldValue
					and exists (select 'x' from ClendorProjectAssocTemplate new where  ClendorProjectAssocTemplate.WBS1 = new.WBS1 and
																			ClendorProjectAssocTemplate.WBS2 = new.WBS2 and
																			ClendorProjectAssocTemplate.WBS3 = new.WBS3 and 
																			new.ClientID =  @NewValue)

				Delete from PRCompetitionAssoc where ClientID = @OldValue
					and exists (select 'x' from PRCompetitionAssoc new where  PRCompetitionAssoc.WBS1 = new.WBS1 and
																			PRCompetitionAssoc.WBS2 = new.WBS2 and
																			PRCompetitionAssoc.WBS3 = new.WBS3 and 
																			new.ClientID =  @NewValue)
				Delete from PRCompetitionAssocTemplate where ClientID = @OldValue
					and exists (select 'x' from PRCompetitionAssocTemplate new where  PRCompetitionAssocTemplate.WBS1 = new.WBS1 and
																			PRCompetitionAssocTemplate.WBS2 = new.WBS2 and
																			PRCompetitionAssocTemplate.WBS3 = new.WBS3 and 
																			new.ClientID =  @NewValue)


				delete from ClientFromContacts where ClientID = @OldValue 
				and exists (select 'x' from ClientFromContacts new where new.ClientID = @newValue and ClientFromContacts.ContactID = new.ContactID)

			/*handle duplicate custom tab records, possible when copying one project to another*/
				declare custTable cursor for
				select tableName from FW_CustomGrids where infocenterarea = N'Clients'
				open custTable
				fetch next from custTable into @TableName
				while (@@FETCH_STATUS = 0)
				begin
					set @Sql = 'Delete FROM ' + @TableName + ' WHERE ' + @TableName + '.ClientID =''' +  @OldValue + ''' and ' +
									'exists (select ''x'' from ' + @TableName + ' new where new.ClientID = ''' + @NewValue + ''' and new.seq = ' + @TableName + '.seq)'
					if (@Diag = 1)
						print @Sql
					execute(@Sql)
					fetch next from custTable into @TableName
				end
				close custTable
				deallocate custTable					

				exec @RetVal = KeyCvt @Entity = 'Client',
						@CvtType = @CvtType,
						@Length = 32,
						@ErrMsg = @ErrMsg2 output,
						@Diag = @Diag,
						@NewValue = @NewValue,
						@OldValue = @OldValue,
						@ConstrFlag = 3 -- UpdateTables

			END --Combining Clients


--2 Old and New Vendor exist, Combining Vendor records

		If (@ExistingVendorOld = 1 and @ExistingVendorNew = 1)
			begin
				if (@diag=1) print '2 Old and New Vendor exist, Combining Vendor records'
				if (@DeleteExisting = 0)
					begin
						SET @message = dbo.GetMessage('MsgNumAnExistingConvWillNotCont',@custlabelVendor,@NewVendor,@custlabelVendor,@custLabelPluralVendor,'','','','','')
						RAISERROR(@message,16,3)
						close keysCursor
						deallocate keysCursor
						if (@@Trancount > 0)
						   ROLLBACK TRANSACTION
						return(50002) -- user defined error
					end
				if (@NewVendor = @OldVendor)
					begin
						SET @message = dbo.GetMessage('MsgAreSameNumConvNotCont',@custlabelPluralVendor,'','','','','','','','')
						RAISERROR(@message,16,3)
						close keysCursor
						deallocate keysCursor
						if (@@Trancount > 0)
							ROLLBACK TRANSACTION
						return(50002) -- user defined error
					end

				Update VEAccounting
					Set VEAccounting.ThisYear1099 = VEAccounting.ThisYear1099 + old.ThisYear1099,  
					VEAccounting.LastYear1099 = VEAccounting.LastYear1099 + old.LastYear1099
					from VEAccounting old inner join VEAccounting on (old.Company = VEAccounting.Company)
					Where VEAccounting.Vendor = @newVendor And old.Vendor = @OldVendor
	
	--VEAccounting	Vendor
	--VEAccounting	Company
				delete from VEAccounting where Vendor = @OldVendor 
					and exists (select 'x' from VEAccounting new 
					where new.Vendor = @newVendor and new.Company = VEAccounting.Company)

				delete from VEFileLinks where Vendor = @OldVendor 
					and exists (select 'x' from VEFileLinks new 
					where new.Vendor = @newVendor and new.LinkID = VEFileLinks.LinkID)

				delete from form1099data where vendor  = @OldVendor
				and Exists (select 'x' from form1099data as tmp 
					where tmp.Year = form1099data.year 
					and tmp.Company = form1099data.company 
					and tmp.Vendor = @newVendor)


	--VendorItem	Item
	--VendorItem	Vendor
	--TotalQty and TotalAmt, and use Max(LastPriceDate) 
				Update VendorItem
					Set vendorItem.TotalQty = vendorItem.TotalQty + old.TotalQty,  
					vendorItem.TotalAmt = vendorItem.TotalAmt + old.TotalAmt
					from VendorItem old inner join vendorItem on (old.Item = vendorItem.Item)
					Where vendorItem.Vendor = @newVendor
					  And old.Vendor = @OldVendor

				delete from VendorItem where Vendor = @OldVendor 
					and exists (select 'x' from VendorItem new 
					where new.Vendor = @newVendor and new.Item = VendorItem.Item)
	/*end for 3.0*/
	/* New for 2.0
	BudgetMilestoneSeq     AlertID 
	BudgetMilestoneSeq     PlanID  
	BudgetMilestoneSeq     Account 
	BudgetMilestoneSeq     Vendor  */

				Delete from BudgetMilestoneSeq where Vendor = @OldVendor 
					and exists (select 'x' from BudgetMilestoneSeq new where  BudgetMilestoneSeq.AlertID = new.AlertID and
																			BudgetMilestoneSeq.PlanID = new.PlanID and
																			new.Vendor =  @newVendor and
																			BudgetMilestoneSeq.Account = new.Account)
	/*
	VEProjectAssocTemplate Vendor  
	VEProjectAssocTemplate WBS1    
	VEProjectAssocTemplate WBS2    
	VEProjectAssocTemplate WBS3    
	*/

				Delete from VEProjectAssocTemplate where Vendor = @OldVendor 
					and exists (select 'x' from VEProjectAssocTemplate new where  VEProjectAssocTemplate.WBS1 = new.WBS1 and
																			VEProjectAssocTemplate.WBS2 = new.WBS2 and
																			VEProjectAssocTemplate.WBS3 = new.WBS3 and 
																			new.Vendor =  @newVendor)

				Delete from CustomProposalVendor where Vendor = @OldVendor 
					and exists (select 'x' from CustomProposalVendor new where  CustomProposalVendor.CustomPropID = new.CustomPropID and
																			CustomProposalVendor.SectionID = new.SectionID and
																			new.Vendor =  @newVendor)


	/*End 2.0*/


				update EB
					set eb.AmtBud =  eb.AmtBud + old.AmtBud,
					eb.BillBud = eb.BillBud + old.BillBud,
					eb.EtcAmt =  eb.EtcAmt + old.EtcAmt,
					eb.EacAmt = eb.EacAmt + old.EacAmt
					from eb old inner join eb on (eb.WBS1 = old.WBS1 and eb.WBS2 = old.WBS2 and eb.WBS3 = old.WBS3 and eb.Account = old.Account)
					where
						eb.Vendor = @newVendor and
						old.Vendor = @OldVendor

				Delete from EB where Vendor = @OldVendor 
					and exists (select 'x' from EB new where  EB.WBS1 = new.WBS1 and
																			EB.WBS2 = new.WBS2 and
																			EB.WBS3 = new.WBS3 and 
																			new.Vendor =  @newVendor and
																			eb.Account = new.Account)

				declare @testVal Nvarchar(1)
				set @TestVal = 'N'
				Select  @testVal = 'Y' From VO Inner Join VO As VOtmp On VO.Voucher = VOTmp.Voucher
				Where VO.Vendor = @oldVendor And VOtmp.Vendor = @newVendor
				if (@testVal = 'Y')
					begin
					SET @message = dbo.GetMessage('MsgDupVoucherNumExistsNotContinue',@custlabelpluralVendor,@custlabelVendor,@OldVendor,@custlabelVendor,@NewVendor,'','','','')
					RAISERROR(@message,16,3)
						if (@@Trancount > 0)
						   ROLLBACK TRANSACTION
						return(50002) -- user defined error
					end

				set @TestVal = 'N'
				Select  @testVal = 'Y' From VO Inner Join VO As VOtmp On VO.Invoice = VOTmp.Invoice and VO.InvoiceDate = VOTmp.InvoiceDate and VO.Company = VOTmp.Company 
				Where VO.Vendor = @oldVendor And VOtmp.Vendor = @newVendor
				if (@testVal = 'Y')
					begin
			   		RAISERROR ('Duplicate invoice numbers/invoice date/company exist for these %s. Conversion will not continue.  Old %s:%s New %s:%s',
					  16, 3,  @custlabelpluralVendor,@custlabelVendor,@OldVendor,@custlabelVendor,@NewVendor)
						if (@@Trancount > 0)
						   ROLLBACK TRANSACTION
						return(50002) -- user defined error
					end

	--VEAddress(Vendor, Address) Preserver duplicate address codes by renumbering them
				set @AppendInt = 0
				declare AddressCursor cursor for select Address from VEAddress where Vendor = @OldVendor 
					and exists (select 'x' from VEAddress new where new.Vendor = @newVendor and VEAddress.Address = new.Address)
				open addressCursor
				fetch next from AddressCursor into @OldAddress
				set @AddressFetch = @@FETCH_STATUS
				while (@AddressFetch = 0)
				begin
					SET @AppendInt = @AppendInt + 1
					Set @NewAddress = left(@OldAddress,20 - len(convert(Nvarchar,@AppendInt))) + convert(Nvarchar,@AppendInt)

					if not exists (select 'x' from VEAddress where (Vendor = @oldVendor or Vendor = @newVendor) and Address = @NewAddress)
					begin
						update VEAddress set Address = @NewAddress where Vendor = @OldVendor and Address = @OldAddress
						update VEProjectAssoc set VEAddress = @NewAddress where Vendor = @OldVendor and VEAddress = @OldAddress
						update Contacts set VEAddress = @NewAddress where Vendor = @OldVendor and VEAddress = @OldAddress

						update VEProjectAssocTemplate set VEAddress = @NewAddress where Vendor = @OldVendor and VEAddress = @OldAddress
						update apMaster set Address = @NewAddress where Vendor = @OldVendor and Address = @OldAddress
						update apppChecks set Address = @NewAddress where Vendor = @OldVendor and Address = @OldAddress
						update cvMaster set Address = @NewAddress where Vendor = @OldVendor and Address = @OldAddress
						update ItemRequestVendorDetail set Address = @NewAddress where Vendor = @OldVendor and Address = @OldAddress
						update POMaster set Address = @NewAddress where Vendor = @OldVendor and Address = @OldAddress
						update POPQVendorDetail set Address = @NewAddress where Vendor = @OldVendor and Address = @OldAddress
						update POPRVendorDetail set Address = @NewAddress where Vendor = @OldVendor and Address = @OldAddress
						update POVoucherMaster set Address = @NewAddress where Vendor = @OldVendor and Address = @OldAddress
						update VO set Address = @NewAddress where Vendor = @OldVendor and Address = @OldAddress

						fetch next from AddressCursor into @OldAddress
						set @AddressFetch = @@FETCH_STATUS
						SET @AppendInt = 0
					end
				end
				close addressCursor
				deallocate addressCursor

				update VEAddress set PrimaryInd ='N' where Vendor = @OldVendor

				Delete from VEAddress where Vendor = @OldVendor 
					and exists (select 'x' from VEAddress new where new.Vendor = @newVendor and
					VEAddress.Address = new.Address)
	--

			
				Delete From BTEVVends Where Vendor = @OldVendor 
					  And TableNo In (Select TableNo From BTEVVends Where Vendor = @newVendor)

				delete from VEProjectAssoc where VEProjectAssoc.Vendor = @OldVendor  and
				exists (select 'x' from VEProjectAssoc new where new.Vendor = @newVendor and VEProjectAssoc.WBS1 = new.WBS1)

				delete from VESubscr where VESubscr.Vendor = @OldVendor and
				exists (select 'x' from VESubscr new where new.Vendor = @newVendor and VESubscr.username = new.username)

				/*handle duplicate custom tab records, possible when copying one project to another*/
				declare custTable cursor for
				select tableName from FW_CustomGrids where infocenterarea = N'Vendors'
				open custTable
				fetch next from custTable into @TableName
				while (@@FETCH_STATUS = 0)
				begin
					set @Sql = 'Delete FROM ' + @TableName + ' WHERE ' + @TableName + '.Vendor =N''' +  @OldVendor + ''' and ' +
									'exists (select ''x'' from ' + @TableName + ' new where new.Vendor = N''' + @NewVendor + ''' and new.seq = ' + @TableName + '.seq)'
					if (@Diag = 1)
						print @Sql
					execute(@Sql)
					fetch next from custTable into @TableName
				end
				close custTable
				deallocate custTable				


if (@diag=1) print 'Merge RPExpense, RPConsultant, PNExpense, PNConsultant tables....'


			declare @PlanID varchar(32)
			declare @WBS1	Nvarchar(30),
					@WBS2	Nvarchar(30),
					@WBS3	Nvarchar(30)
			declare @Account		Nvarchar(13),
					@Vendor		Nvarchar(20),
					@OldExpenseID	Nvarchar(32),
					@NewExpenseID	Nvarchar(32),
					@OldConsultantID	Nvarchar(32),
					@NewConsultantID	Nvarchar(32)
			declare @OldTaskID		varchar(32)
			declare @NewTaskID		varchar(32)
			declare @TempFetch integer

-- Move Planned and Basedline for the duplicate RPExpense, Delete duplicate RPExpense
			declare ExpenseCurso cursor for
				select PlanID, TaskID, isnull(WBS1,'') as WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Account,'') as Account, isnull(Vendor,'') as Vendor, MIN(ExpenseID) as ExpenseID  
				from RPExpense where Vendor=@OldVendor and
				exists (select 'x' from RPExpense new where new.PlanID=RPExpense.PlanID 
					and isnull(new.wbs1,'') = isnull(RPExpense.wbs1,'') 
					and isnull(new.wbs2,'') = isnull(RPExpense.wbs2,'')
					and isnull(new.wbs3,'') = isnull(RPExpense.wbs3,'')
					and isnull(new.Vendor,'') = @newVendor
					and isnull(new.Account,'') = isnull(RPExpense.Account,''))
				group by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
				Order by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
			open ExpenseCurso
			fetch next from ExpenseCurso into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldExpenseID
			set @TempFetch = @@Fetch_Status
			While (@TempFetch = 0)
			begin
				select @NewTaskID=TaskID, @NewExpenseID=ExpenseID from RPExpense 
					where PlanID=@PlanID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(Vendor,'')=@newVendor 
					and isnull(Account,'')=isnull(@Account,'') 

				Update RPPlannedExpenses Set PlanID=@PlanID, TaskID=@NewTaskID, ExpenseID=@NewExpenseID where ExpenseID=@OldExpenseID
				Update RPBaselineExpenses Set PlanID=@PlanID, TaskID=@NewTaskID, ExpenseID=@NewExpenseID where ExpenseID=@OldExpenseID

				UPDATE A SET 
					StartDate = CASE WHEN XTPD.StartDate < A.StartDate THEN XTPD.StartDate ELSE A.StartDate END, 
					EndDate = CASE WHEN XTPD.EndDate > A.EndDate THEN XTPD.EndDate ELSE A.EndDate END
					FROM RPExpense AS A
					  INNER JOIN (
						SELECT PlanID, TaskID, ExpenseID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
						  FROM RPPlannedExpenses
						  WHERE ExpenseID=@NewExpenseID
						  GROUP BY PlanID, TaskID, ExpenseID
					  ) AS XTPD 
						ON A.PlanID = XTPD.PlanID AND A.TaskID = XTPD.TaskID AND A.ExpenseID = XTPD.ExpenseID

				delete RPExpense Where PlanID=@PlanID and TaskID=@OldTaskID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(Vendor,'')=@OldVendor
					and isnull(Account,'')=isnull(@Account,'')

				fetch next from ExpenseCurso into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldExpenseID
				set @TempFetch = @@Fetch_Status
			end
			close ExpenseCurso
			deallocate ExpenseCurso

-- Move Planned and Basedline for the duplicate RPConsultant, Delete duplicate RPConsultant
			declare ConsultantCurso cursor for
				select PlanID, TaskID, isnull(WBS1,'') as WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Account,'') as Account, isnull(Vendor,'') as Vendor, MIN(ConsultantID) as ConsultantID  
				from RPConsultant where Vendor=@OldVendor and
				exists (select 'x' from RPConsultant new where new.PlanID=RPConsultant.PlanID 
					and isnull(new.wbs1,'') = isnull(RPConsultant.wbs1,'') 
					and isnull(new.wbs2,'') = isnull(RPConsultant.wbs2,'')
					and isnull(new.wbs3,'') = isnull(RPConsultant.wbs3,'')
					and isnull(new.Vendor,'') = @newVendor
					and isnull(new.Account,'') = isnull(RPConsultant.Account,''))
				group by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
				Order by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
			open ConsultantCurso
			fetch next from ConsultantCurso into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldConsultantID
			set @TempFetch = @@Fetch_Status
			While (@TempFetch = 0)
			begin
				select @NewTaskID=TaskID, @NewConsultantID=ConsultantID from RPConsultant 
					where PlanID=@PlanID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(Vendor,'')=@newVendor 
					and isnull(Account,'')=isnull(@Account,'') 

				Update RPPlannedConsultant Set PlanID=@PlanID, TaskID=@NewTaskID, ConsultantID=@NewConsultantID where ConsultantID=@OldConsultantID
				Update RPBaselineConsultant Set PlanID=@PlanID, TaskID=@NewTaskID, ConsultantID=@NewConsultantID where ConsultantID=@OldConsultantID

				UPDATE A SET 
					StartDate = CASE WHEN XTPD.StartDate < A.StartDate THEN XTPD.StartDate ELSE A.StartDate END, 
					EndDate = CASE WHEN XTPD.EndDate > A.EndDate THEN XTPD.EndDate ELSE A.EndDate END
					FROM RPConsultant AS A
					  INNER JOIN (
						SELECT PlanID, TaskID, ConsultantID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
						  FROM RPPlannedConsultant
						  WHERE ConsultantID=@NewConsultantID
						  GROUP BY PlanID, TaskID, ConsultantID
					  ) AS XTPD 
						ON A.PlanID = XTPD.PlanID AND A.TaskID = XTPD.TaskID AND A.ConsultantID = XTPD.ConsultantID

				delete RPConsultant Where PlanID=@PlanID and TaskID=@OldTaskID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(Vendor,'')=@OldVendor
					and isnull(Account,'')=isnull(@Account,'')

				fetch next from ConsultantCurso into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldConsultantID
				set @TempFetch = @@Fetch_Status
			end
			close ConsultantCurso
			deallocate ConsultantCurso

-- Move Planned and Basedline for the duplicate PNExpense, Delete duplicate PNExpense
if (@diag=1) print 'Start Merge PNExpense.... @OldVendor = '  + @OldVendor + ',  @newVendor = ' + @newVendor
			declare ExpenseCurso cursor for
				select PlanID, TaskID, isnull(WBS1,'') as WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Account,'') as Account, isnull(Vendor,'') as Vendor, MIN(ExpenseID) as ExpenseID  
				from PNExpense where Vendor=@OldVendor and
				exists (select 'x' from PNExpense new where new.PlanID=PNExpense.PlanID 
					and isnull(new.wbs1,'') = isnull(PNExpense.wbs1,'') 
					and isnull(new.wbs2,'') = isnull(PNExpense.wbs2,'')
					and isnull(new.wbs3,'') = isnull(PNExpense.wbs3,'')
					and isnull(new.Vendor,'') = @newVendor
					and isnull(new.Account,'') = isnull(PNExpense.Account,''))
				group by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
				Order by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
			open ExpenseCurso
			fetch next from ExpenseCurso into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldExpenseID
			set @TempFetch = @@Fetch_Status
			While (@TempFetch = 0)
			begin
				select @NewTaskID=TaskID, @NewExpenseID=ExpenseID from PNExpense 
					where PlanID=@PlanID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(Vendor,'')=@newVendor 
					and isnull(Account,'')=isnull(@Account,'') 

if (@diag=1) print 'Start Updating PNPlannedExpenses.... @NewTaskID = '  + @NewTaskID + ',  @NewExpenseID = ' + @NewExpenseID

				Update PNPlannedExpenses Set PlanID=@PlanID, TaskID=@NewTaskID, ExpenseID=@NewExpenseID where ExpenseID=@OldExpenseID
				Update PNBaselineExpenses Set PlanID=@PlanID, TaskID=@NewTaskID, ExpenseID=@NewExpenseID where ExpenseID=@OldExpenseID

if (@diag=1) print 'Start Updating Start End Date.... @NewTaskID = '  + @NewTaskID + ',  @NewExpenseID = ' + @NewExpenseID

				UPDATE A SET 
					StartDate = CASE WHEN XTPD.StartDate < A.StartDate THEN XTPD.StartDate ELSE A.StartDate END, 
					EndDate = CASE WHEN XTPD.EndDate > A.EndDate THEN XTPD.EndDate ELSE A.EndDate END
					FROM PNExpense AS A
					  INNER JOIN (
						SELECT PlanID, TaskID, ExpenseID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
						  FROM PNPlannedExpenses
						  WHERE ExpenseID=@NewExpenseID
						  GROUP BY PlanID, TaskID, ExpenseID
					  ) AS XTPD 
						ON A.PlanID = XTPD.PlanID AND A.TaskID = XTPD.TaskID AND A.ExpenseID = XTPD.ExpenseID

if (@diag=1) print 'Start deleting .... @OldVendor= '  + @OldVendor
				delete PNExpense Where PlanID=@PlanID and TaskID=@OldTaskID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(Vendor,'')=@OldVendor
					and isnull(Account,'')=isnull(@Account,'')

				fetch next from ExpenseCurso into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldExpenseID
				set @TempFetch = @@Fetch_Status
			end
			close ExpenseCurso
			deallocate ExpenseCurso

-- Move Planned and Basedline for the duplicate PNConsultant, Delete duplicate PNConsultant
if (@diag=1) print 'Start Merge PNConsultant.... @OldVendor = '  + @OldVendor + ',  @newVendor = ' + @newVendor
			declare ConsultantCurso cursor for
				select PlanID, TaskID, isnull(WBS1,'') as WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Account,'') as Account, isnull(Vendor,'') as Vendor, MIN(ConsultantID) as ConsultantID  
				from PNConsultant where Vendor=@OldVendor and
				exists (select 'x' from PNConsultant new where new.PlanID=PNConsultant.PlanID 
					and isnull(new.wbs1,'') = isnull(PNConsultant.wbs1,'') 
					and isnull(new.wbs2,'') = isnull(PNConsultant.wbs2,'')
					and isnull(new.wbs3,'') = isnull(PNConsultant.wbs3,'')
					and isnull(new.Vendor,'') = @newVendor
					and isnull(new.Account,'') = isnull(PNConsultant.Account,''))
				group by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
				Order by PlanID, TaskID, isnull(WBS1,''), isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
			open ConsultantCurso
			fetch next from ConsultantCurso into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldConsultantID
			set @TempFetch = @@Fetch_Status
			While (@TempFetch = 0)
			begin
				select @NewTaskID=TaskID, @NewConsultantID=ConsultantID from PNConsultant 
					where PlanID=@PlanID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(Vendor,'')=@newVendor 
					and isnull(Account,'')=isnull(@Account,'') 

if (@diag=1) print 'Start Updating PNPlannedConsultant.... @NewTaskID = '  + @NewTaskID + ',  @NewExpenseID = ' + @NewExpenseID

				Update PNPlannedConsultant Set PlanID=@PlanID, TaskID=@NewTaskID, ConsultantID=@NewConsultantID where ConsultantID=@OldConsultantID
				Update PNBaselineConsultant Set PlanID=@PlanID, TaskID=@NewTaskID, ConsultantID=@NewConsultantID where ConsultantID=@OldConsultantID

if (@diag=1) print 'Start Updating Start End Date.... @NewTaskID = '  + @NewTaskID + ',  @NewExpenseID = ' + @NewExpenseID
				UPDATE A SET 
					StartDate = CASE WHEN XTPD.StartDate < A.StartDate THEN XTPD.StartDate ELSE A.StartDate END, 
					EndDate = CASE WHEN XTPD.EndDate > A.EndDate THEN XTPD.EndDate ELSE A.EndDate END
					FROM PNConsultant AS A
					  INNER JOIN (
						SELECT PlanID, TaskID, ConsultantID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
						  FROM PNPlannedConsultant
						  WHERE ConsultantID=@NewConsultantID
						  GROUP BY PlanID, TaskID, ConsultantID
					  ) AS XTPD 
						ON A.PlanID = XTPD.PlanID AND A.TaskID = XTPD.TaskID AND A.ConsultantID = XTPD.ConsultantID

if (@diag=1) print 'Start deleting .... @OldVendor= '  + @OldVendor
				delete PNConsultant Where PlanID=@PlanID and TaskID=@OldTaskID 
					and isnull(WBS1,'')=isnull(@WBS1,'')
					and isnull(WBS2,'')=isnull(@WBS2,'') 
					and isnull(WBS3,'')=isnull(@WBS3,'') 
					and isnull(Vendor,'')=@OldVendor
					and isnull(Account,'')=isnull(@Account,'')

				fetch next from ConsultantCurso into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldConsultantID
				set @TempFetch = @@Fetch_Status
			end
			close ConsultantCurso
			deallocate ConsultantCurso


				exec @RetVal = KeyCvt @Entity = 'Vendor',
						 @CvtType = @CvtType,
						 @Length = 32,
						 @ErrMsg = @ErrMsg2 output,
						 @Diag = @Diag,
						 @NewValue = @NewVendor,
						 @OldValue = @OldVendor,
						 @ConstrFlag = 3 -- UpdateTables

			END --Combining Vendors


	--When merging clendor records, update the ClientInd, Client, VendorInd, Vendor, GovernmentAgency, Competitor fields on the new record
		if (@ExistingClientOld = 1 and @ExistingClientNew = 1) 
			Begin
				if (@diag=1) print '@ExistingClientOld = 1 and @ExistingClientNew = 1, update ClientInd, Client, VendorInd, Vendor, GovernmentAgency, Competitor, ReadyForProcessing, AvailableForCRM, ReadyForApproval for New record : ' + @NewValue
				if (@ClientIndOld = 'Y' and @ClientIndNew = 'N') 
					Update Clendor Set ClientInd = 'Y' Where ClientID = @newValue
				if (isnull(@OldClient, '') > '' and isnull(@NewClient, '') = '') 
					Update Clendor Set Client = @OldClient Where ClientID = @newValue
				if (@VendorIndOld = 'Y' and @VendorIndNew = 'N') 
					Update Clendor Set VendorInd = 'Y' Where ClientID = @newValue
				if (isnull(@OldVendor, '') > '' and isnull(@NewVendor, '') = '') 
					Update Clendor Set Vendor = @OldVendor 
					, Type=VE.Type
					, SpecialtyType=VE.SpecialtyType
					, Status=VE.Status
					, WebSite=VE.WebSite
					, CustomCurrencyCode=VE.CustomCurrencyCode
					, PriorWork=VE.PriorWork
					, Recommend=VE.Recommend
					, DisadvBusiness=VE.DisadvBusiness
					, DisabledVetOwnedSmallBusiness=VE.DisabledVetOwnedSmallBusiness
					, HBCU=VE.HBCU
					, MinorityBusiness=VE.MinorityBusiness
					, SmallBusiness=VE.SmallBusiness
					, VetOwnedSmallBusiness=VE.VetOwnedSmallBusiness
					, WomanOwned=VE.WomanOwned
					, AlaskaNative=VE.AlaskaNative
					, Specialty=VE.Specialty
					, Employees=VE.Employees
					, AnnualRevenue=VE.AnnualRevenue
					, EightA=VE.EightA
					, Hubzone=VE.Hubzone
					, AjeraSync=VE.AjeraSync
					, SortName=VE.SortName
					, Category=VE.Category
					, FedID=VE.FedID
					, Org=VE.Org
					, Market=VE.Market
					, TypeOfTIN=VE.TypeOfTIN
					from Clendor left join Clendor VE on VE.Vendor=@OldVendor
					Where Clendor.ClientID = @newValue

				if (@GovernmentAgencyOld = 'Y' and @GovernmentAgencyNew = 'N') 
					Update Clendor Set GovernmentAgency = 'Y' Where ClientID = @newValue
				if (@CompetitorOld = 'Y' and @CompetitorNew = 'N') 
					Update Clendor Set Competitor = 'Y' Where ClientID = @newValue
				if (@ReadyForProcessingOld = 'Y' and @ReadyForProcessingNew = 'N') 
					Update Clendor Set ReadyForProcessing = 'Y' Where ClientID = @newValue
				if (@AvailableForCRMOld = 'Y' and @AvailableForCRMNew = 'N') 
					Update Clendor Set AvailableForCRM = 'Y' Where ClientID = @newValue
				if (@ReadyForApprovalOld = 'Y' and @ReadyForApprovalNew = 'N') 
					Update Clendor Set ReadyForApproval = 'Y' Where ClientID = @newValue

				Delete from Clendor where ClientID = @OldValue
				--Update ModUser/ModDate
				UPDATE Clendor SET ModUser = @UserName, ModDate = GETUTCDATE() WHERE ClientID = @NewValue
				Set @RetVal = 0
			END 


		if (@RetVal = 0)
			begin
				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq)
					select PKey, Entity, OldKey, NewKey, @OldName, @NewName, @Period, @LastPostSeq from keyConvertWork where Pkey = @PKey
				end
	--
				delete from keyConvertWork where current of keysCursor
				commit Transaction
			end
		else
			begin
				set @ErrMsg = @ErrMsg + @ErrMsg2
				rollback transaction
			end


		fetch next from KeysCursor into 
			@OldValue,
			@NewValue, 
			@PKey
		set @KeysFetch = @@Fetch_Status

	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	return(0)
End -- Procedure
GO
