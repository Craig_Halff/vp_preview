SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--SET QUOTED_IDENTIFIER ON|OFF
--SET ANSI_NULLS ON|OFF
--GO
CREATE PROCEDURE [dbo].[HAI_DM_InsertTest]
AS
    BEGIN
        BEGIN TRANSACTION; -- #CHANGE: Put the insert inside of a transaction with a commit to try to ensure it writes before moving on.
        INSERT INTO DataMart.dbo.HAI_EACandETCUpdaterQueue (
            Projects
          , Status
          , SubmittedDate
          , StartedDate
          , CompletedDate
          , ElapsedTime
          , Details
        )
        VALUES
        (   CAST(' ' AS NTEXT) -- Projects - ntext
          , 'submitted' -- Status - nvarchar(50)
          , GETDATE() -- SubmittedDate - datetime2(7)
          , NULL, NULL, NULL, NULL);
        COMMIT TRANSACTION;
    END;
GO
