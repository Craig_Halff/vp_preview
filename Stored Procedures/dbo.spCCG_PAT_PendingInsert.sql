SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_PendingInsert] (
	@EmployeeID			Nvarchar(32),
	@PayableSeq			int,
	@Route				varchar(128)=null,
	@Description		Nvarchar(128)=null,
	@Parallel			char(1) = 'N',
	@SortOrder			int = 0

) AS
BEGIN
	SET NOCOUNT ON;

	declare @UseSortOrder int
	if ISNULL(@SortOrder,0) <= 0
		select @UseSortOrder = ISNULL(max(SortOrder),0)+1 from CCG_PAT_Pending where PayableSeq = @PayableSeq
	ELSE
		set @UseSortOrder = @SortOrder
	INSERT INTO CCG_PAT_Pending (PayableSeq, [Route], [Description], SortOrder, Employee, Parallel,CreateDateTime) VALUES
		(@PayableSeq, @Route, @Description, @UseSortOrder, @EmployeeID,@Parallel, GETUTCDATE());
END;

GO
