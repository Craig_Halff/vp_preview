SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_CheckNotifications] ( @Employee nvarchar(20), @OverrideSubject nvarchar(120), @OverrideSenderEmail nvarchar(100), @MsgPriority varchar(10)= NULL, @IncludeSupervisorAge int= -1, @InclWeekends char(1)= 'Y', @RoundingHoursCutoff int= 22)
             AS EXEC spCCG_PAT_CheckNotifications @Employee,@OverrideSubject,@OverrideSenderEmail,@MsgPriority,@IncludeSupervisorAge,@InclWeekends,@RoundingHoursCutoff
GO
