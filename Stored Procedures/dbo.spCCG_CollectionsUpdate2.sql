SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_CollectionsUpdate2]
AS

/* 
Copyright (c) 2020 Central Consulting Group.  All rights reserved.
06/25/2020	David Springer
			Update UDIC_Collections 
			Notify Project Manager
			Call this from a Collections scheduled workflow 
			Run every Monday (2nd Monday is critical to coding below)
07/16/2020  Craig Anderson
            Changed to CustHistoryResponse ='Verified Invoice Received' in the update section.  Was CustHistoryAction.
            CSS modifications.
07/20/2020  Craig Anderson
            Added c.CustRequestedActionStatus = 'Incomplete' to the update.
10/12/2020	Craig H. Anderson
			Modified the Per-Project query to use LEFT OUTER join to account for NULL BillingContacts
10/17/2020	Craig H. Anderson
			Modified to use CustCollectionsResponsibility instead of CustPAResponsibleForCollections
02/01/2021	Craig H. Anderson
			Fixed, making Jami Kubik the Bcc on this notification.
04/08/2021	Craig H. Anderson
			Removed date restrictions and added more null tolerances.
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;
DECLARE @Subject             VARCHAR(255)
      , @Intro               VARCHAR(MAX)
      , @RAOEmpno            VARCHAR(32)
      , @RAOName             VARCHAR(255)
      , @RAOEmail            VARCHAR(255)
      , @PAEmail             VARCHAR(255)
      , @TLEmail             VARCHAR(255)
      , @WBS1                VARCHAR(32)
      , @Project             VARCHAR(2000)
      , @UDIC_UID            VARCHAR(32)
      , @InvoiceNo           VARCHAR(255)
      , @InvoiceDate         DATE
      , @Balance             FLOAT
      , @DaysOut             INTEGER
      , @RequestedAction     VARCHAR(255)
      , @RequestedActionDate DATE
      , @CSS                 VARCHAR(MAX)
      , @Body                VARCHAR(MAX)
      , @Loop1               INTEGER
      , @Loop2               INTEGER
      , @Loop3               INTEGER
      , @i                   INTEGER = 0;

    BEGIN

        BEGIN
            UPDATE c
            SET c.CustRequestedAction = IIF(ISNULL(px.CustCollectionsResponsibility, 'PA') = 'PA', 'PA to Follow Up', 'PM to Follow Up')
              , c.CustRequestedActionStatus = 'Incomplete'
              , c.CustRequestedActionStart = CONVERT(DATE, GETDATE())
              , c.CustRequestedActionOwner = IIF(ISNULL(px.CustCollectionsResponsibility, 'PA') = 'PA', c.CustBiller, c.CustProjMgr)
              , c.CustResponseNotes = NULL
            FROM dbo.UDIC_Collection c
               , dbo.ProjectCustomTabFields px
            WHERE
                c.CustCollectionStatus = 'Open'
                AND ISNULL(c.CustRequestedAction, 'PM to Follow Up') IN ( 'PA to Follow Up', 'PM to Follow Up' )
                AND DATEDIFF(DAY, c.CustInvoiceDate, GETDATE()) > 60
                AND ISNULL(c.CustRequestedActionStart, CAST(GETDATE() AS DATE)) <= CAST(GETDATE() AS DATE)
                AND c.CustBalance > 0.0
                AND c.CustProject = px.WBS1
                AND px.WBS2 = ' '
                AND px.WBS3 = ' '
                AND EXISTS
                (
                    SELECT 'x'
                    FROM dbo.UDIC_Collection_History
                    WHERE
                        UDIC_UID = c.UDIC_UID
                        AND ISNULL(CustHistoryResponse, '') IN ( 'Verified Invoice Received', 'Expect Pay within 30 days', 'Expect Pay within 60 days'
                                                               , 'Expect Pay within 90 days'
                                                               )
                );

            SET @Subject = 'Monthly Collections Notification';
            SET @Intro
                = '<p>XXXXX,</p><p>The invoices below are over 60 days old and I have already verified that they have been received by our clients. Please follow up with your client(s) regarding payment status.</p>';
        END;

        --	Must be below update
        DECLARE curPM INSENSITIVE CURSOR FOR
        SELECT DISTINCT
               rao.Employee
             , ISNULL(rao.PreferredName, rao.FirstName)
             , rao.EMail
             , ISNULL(pa.EMail, '')
             , ISNULL(tl.EMail, '')
        FROM dbo.UDIC_Collection c
           , dbo.EM rao
           , dbo.EM pa
           , dbo.EM tl
        WHERE
            c.CustCollectionStatus = 'Open'
            AND c.CustRequestedActionStatus = 'Incomplete'
            AND c.CustBalance > 0.0
            AND ISNULL(c.CustRequestedActionStart, CAST(GETDATE() AS DATE)) <= CAST(GETDATE() AS DATE)
            AND ISNULL(c.CustRequestedAction, '') IN ( 'PA to Follow Up', 'PM to Follow Up' )
            AND c.CustRequestedActionOwner = rao.Employee
            AND c.CustBiller = pa.Employee
            AND c.CustPrincipal = tl.Employee
        ORDER BY 1;

        -- CSS style rules
        SET @CSS
            = '
        <style type="text/css">
            table {width:100%;border-collapse: collapse; font-size:10pt;}
	        p {padding-top:10pt;font-family: Calibri, Arial, sans-serif; font-size:10pt;}
	        td, th {border: 1pt solid black;padding: 5px;font-family:Calibri, Arial, sans-serif; font-size:10pt;}
	        th {background-color: rbg(68,114,96);color: white; text-align:center;font-size:10.5pt;padding-left:5px;padding-right:5px;}
	        .aLeft {text-align:left;}
	        .aCenter {text-align:center;}
	        .aRight {text-align:right;}
	        .Table_Header tr th {background-color: #4472c4;color: white;}
	        .Project_Header {background-color:#CCCCCC; color:black;}
	        tfoot tr td {border:none;font-weight:bold;}
            tfoot tr td.aRight {border-bottom: 3pt double;black;}
            td.Response {background-color:#ffffcc;}
        </style>
'       ;
        --	############  Project Manager  ##############
        OPEN curPM;
        FETCH NEXT FROM curPM
        INTO @RAOEmpno
           , @RAOName
           , @RAOEmail
           , @PAEmail
           , @TLEmail;
        SET @Loop1 = @@FETCH_STATUS;
        WHILE @Loop1 = 0
            BEGIN
                --	Open the table with headers

                SET @Body
                    = REPLACE(@Intro, 'XXXXX', @RAOName)
                      + '<table cellpadding="5" cellspacing="0" style="border:1px solid black;border-collapse:collapse;">
	<thead class="Table_Header">
		<tr>
			<th rowspan="2" style="width:3em;">Invoice<br>Number</th>
			<th rowspan="2" style="width:3em;" class="aCenter">Invoice<br>Date</th>
			<th rowspan="2" style="width:4em;" class="aRight">Invoice<br>Balance</th>
			<th rowspan="2" style="width:2em;" class="aRight">Days<br>Out</th>
			<th rowspan="2" style="width:3.5em;" class="aCenter">Requested<br>Action<br>Date</th>
			<th colspan="4" class="aCenter">New Collections Actions</th>
		</tr>
		<tr>
			<th class="aCenter" style="width:3em;">Date<br>Contacted</th>
			<th class="aCenter" style="width:5em;">Client Contact</th>
			<th class="aCenter" style="width:6em;">Client Response<br>Classification</th>
			<th class="aCenter">Additional<br>Collection Notes</th>
		</tr>
	</thead>
	<tbody>
'               ;

                DECLARE curProject INSENSITIVE CURSOR FOR
                SELECT DISTINCT
                       p.WBS1
                     , CASE
                           WHEN bcc.ContactID IS NOT NULL
                               THEN
                               bc.Name + ' (' + ISNULL(bcc.PreferredName, bcc.FirstName) + ' ' + bcc.LastName + '): ' + p.WBS1 + ' - ' + p.Name
                           ELSE
                               bc.Name + ' ( - ): ' + p.WBS1 + ' - ' + p.Name
                       END AS Project
                FROM dbo.UDIC_Collection         c
                    INNER JOIN dbo.PR            p
                        ON c.CustProject = p.WBS1
                           AND p.WBS2 = ' '
                    INNER JOIN dbo.CL            bc
                        ON c.CustBillingClient = bc.ClientID
                    LEFT OUTER JOIN dbo.Contacts bcc
                        ON c.CustBillingContact = bcc.ContactID
                WHERE
                    c.CustCollectionStatus = 'Open'
                    AND c.CustRequestedActionStatus = 'Incomplete'
                    AND c.CustBalance > 0.0
                    AND ISNULL(c.CustRequestedActionStart, CAST(GETDATE() AS DATE)) <= CAST(GETDATE() AS DATE)
                    AND ISNULL(c.CustRequestedAction, '') IN ( 'PA to Follow Up', 'PM to Follow Up' )
                    AND c.CustRequestedActionOwner = @RAOEmpno
                ORDER BY 2;

                OPEN curProject;
                FETCH NEXT FROM curProject
                INTO @WBS1
                   , @Project;
                SET @Loop3 = @@FETCH_STATUS;
                WHILE @Loop3 = 0
                    BEGIN
                        DECLARE curInvoice INSENSITIVE CURSOR FOR
                        SELECT DISTINCT
                               c.UDIC_UID
                             , c.CustInvoiceNumber
                             , c.CustInvoiceDate
                             , c.CustBalance
                             , c.CustDaysOutstanding
                             , c.CustRequestedActionStart
                        FROM dbo.UDIC_Collection c
                        WHERE
                            c.CustCollectionStatus = 'Open'
                            AND c.CustBalance > 0.0
                            AND ISNULL(c.CustRequestedActionStart, CAST(GETDATE() AS DATE)) <= CAST(GETDATE() AS DATE)
                            AND ISNULL(c.CustRequestedAction, '') IN ( 'PA to Follow Up', 'PM to Follow Up' )
                            AND c.CustProject = @WBS1
                        ORDER BY 1;
                        SET @Body = @Body + '<tr>
					<td class="Project_Header" colspan="9">' + @Project + '</td>
				</tr>'  ;
                        OPEN curInvoice;
                        FETCH NEXT FROM curInvoice
                        INTO @UDIC_UID
                           , @InvoiceNo
                           , @InvoiceDate
                           , @Balance
                           , @DaysOut
                           , @RequestedActionDate;
                        WHILE @@FETCH_STATUS = 0
                            BEGIN
                                SET @Body
                                    = @Body + '<tr>
						<td>'               + @InvoiceNo + '</td>
						<td class="aCenter">' + CONVERT(VARCHAR(10), @InvoiceDate, 101) + '</td>
						<td class="aRight">' + CONVERT(VARCHAR, CAST(@Balance AS MONEY), 1) + '</td>
						<td class="aRight">' + CONVERT(VARCHAR, CAST(@DaysOut AS INTEGER), 1) + '</td>
						<td class="aCenter">' + CONVERT(VARCHAR(10), @RequestedActionDate, 101)
                                      + '</td>
						<td class="Response"></td>
						<td class="Response"></td>
						<td class="Response"></td>
						<td class="Response"></td>
					</tr>'      ;

                                INSERT INTO dbo.UDIC_Collection_History
                                    (
                                        UDIC_UID
                                      , Seq
                                      , CustHistoryActionStatus
                                      , CustHistoryAction
                                      , CustHistoryActionStart
                                    )
                                VALUES
                                (@UDIC_UID, REPLACE(NEWID(), '-', ''), 'Sent', 'PM ' + @Subject, GETDATE());

                                --	End Invoice

                                -- SET @i = @i + 1;
                                --  PRINT RIGHT('0000' + CAST(@i AS VARCHAR), 5) + ' ' + @InvoiceNo + ' ' + @RAOEmail + ' ' + @TLEmail + ' ' + @PAEmail;

                                FETCH NEXT FROM curInvoice
                                INTO @UDIC_UID
                                   , @InvoiceNo
                                   , @InvoiceDate
                                   , @Balance
                                   , @DaysOut
                                   , @RequestedActionDate;
                            END;
                        CLOSE curInvoice;
                        DEALLOCATE curInvoice;

                        --	End Project
                        FETCH NEXT FROM curProject
                        INTO @WBS1
                           , @Project;
                        SET @Loop3 = @@FETCH_STATUS;
                    END;
                CLOSE curProject;
                DEALLOCATE curProject;



                SET @Body
                    = @Body
                      + '</tbody>
			</table>
			<p>Please fill in actions taken in the yellow fields above and respond within the next 7 days.  I will update the collections records in Deltek.  I am happy to help with the client follow up, just let me know how I can be of assistance.</p>
			<p>Please limit client response classifications to only the following:</p>
			<div style="column-count:2;clear:both;vertical-align:top;height:5em;">
                <ul>
			        <li>Expect Pay within 30 days</li>
			        <li>Expect Pay within 60 days</li>
			        <li>Expect Pay within 90 days</li>
			        <li>Invoice Correction Needed</li>
			        <li>In Dispute</li>   
                    <li>No Client Response</li>
			        <li>Legal Involvement</li>
			        <li>Write-off</li>
			        <li>Change to PA Collections Lead</li>
                </ul>
            </div>';
               
        INSERT INTO dbo.CCG_Email_Messages
        (
            SourceApplication
          , Sender
          , ToList
          , CCList
		  , BCCList
          , Subject
          , Body
          , CreateDateTime
          , MaxDelay
        )
        VALUES
       ('Collections Notification', @PAEmail, @RAOEmail, @TLEmail,'jKubik@halff.com', @Subject, @CSS + @Body, GetDate(), 0);
	   --('Collections Notification', 'canderson@halff.com', 'canderson@halff.com', '','', @Subject, @CSS + @Body, GetDate(), 0);

                --	End Project Accountant
                FETCH NEXT FROM curPM
                INTO @RAOEmpno
                   , @RAOName
                   , @RAOEmail
                   , @PAEmail
                   , @TLEmail;
                SET @Loop1 = @@FETCH_STATUS;
            END;
        CLOSE curPM;
        DEALLOCATE curPM;

    END;
GO
