SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPRevert]
  @strPlanID varchar(32)
AS

BEGIN -- Procedure stRPRevert

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to delete a plan from Navigator (unpublished data).
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON

  DECLARE @tiHasFragmentedTPD tinyint

  DECLARE @strCompany nvarchar(14)
  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled 
    FROM FW_CFGSystem

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION
  
  DELETE PNCalendarInterval WHERE PlanID = @strPlanID
  DELETE PNAccordionFormat WHERE PlanID = @strPlanID
  DELETE PNWBSLevelFormat WHERE PlanID = @strPlanID
  DELETE PNPlannedLabor WHERE PlanID = @strPlanID
  DELETE PNPlannedExpenses WHERE PlanID = @strPlanID
  DELETE PNPlannedConsultant WHERE PlanID = @strPlanID
  DELETE PNBaselineLabor WHERE PlanID = @strPlanID
  DELETE PNBaselineExpenses WHERE PlanID = @strPlanID
  DELETE PNBaselineConsultant WHERE PlanID = @strPlanID
  DELETE PNAssignment WHERE PlanID = @strPlanID
  DELETE PNExpense WHERE PlanID = @strPlanID
  DELETE PNConsultant WHERE PlanID = @strPlanID
  DELETE PNTask WHERE PlanID = @strPlanID
  DELETE PNPlan WHERE PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Update RPPlan with fields on PR table

  UPDATE RPPlan SET
    PlanName = PR.Name,
    ClientID = PR.ClientID,
    ProjMgr = PR.ProjMgr,
    Principal = PR.Principal,
    Supervisor = PR.Supervisor,
    Org = PR.Org,
    Status = PR.Status,
    Probability = ISNULL(PR.Probability, 0),
    ModDate = CONVERT(VARCHAR, GETUTCDATE(), 121)
  FROM RPPlan
    INNER JOIN PR ON RPPlan.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
  WHERE RPPlan.PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   
  INSERT PNPlan (
    PlanID,
    PlanName,
    PlanNumber,
    ClientID,
    ProjMgr,
    Principal,
    Supervisor,
    Org,
    StartDate,
    EndDate,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    BaselineExpCost,
    BaselineExpBill,
    BaselineConCost,
    BaselineConBill,
    BaselineRevenue,
    BaselineStart,
    BaselineFinish,
    PlannedLaborHrs,
    PlannedLabCost,
    PlannedLabBill,
    PlannedExpCost,
    PlannedExpBill,
    PlannedConCost,
    PlannedConBill,
    LabRevenue,
    PlannedDirExpCost,
    PlannedDirExpBill,
    PlannedDirConCost,
    PlannedDirConBill,
    CostRtMethod,
    BillingRtMethod,
    CostRtTableNo,
    BillingRtTableNo,
    ContingencyPct,
    ContingencyAmt,
    OverheadPct,
    AvailableFlg,
    GenResTableNo,
    UnPostedFlg,
    BudgetType,
    PctCompleteFormula,
    PctComplete,
    PctCompleteBill,
    PctCompleteLabCost,
    PctCompleteLabBill,
    PctCompleteExpCost,
    PctCompleteExpBill,
    PctCompleteConCost,
    PctCompleteConBill,
    WeightedLabCost,
    WeightedLabBill,
    WeightedExpCost,
    WeightedExpBill,
    WeightedConCost,
    WeightedConBill,
    UtilizationIncludeFlg,
    WBS1,
    Probability,
    LabMultType,
    Multiplier,
    ProjectedMultiplier,
    ProjectedRatio,
    CalcExpBillAmtFlg,
    CalcConBillAmtFlg,
    ExpBillRtMethod,
    ExpBillRtTableNo,
    ConBillRtMethod,
    ConBillRtTableNo,
    GRBillTableNo,
    ExpBillMultiplier,
    ConBillMultiplier,
    ReimbMethod,
    ExpRevenue,
    ConRevenue,
    TargetMultCost,
    TargetMultBill,
    BaselineUntQty,
    BaselineUntCost,
    BaselineUntBill,
    PlannedUntQty,
    PlannedUntCost,
    PlannedUntBill,
    PctCompleteUntCost,
    PctCompleteUntBill,
    PlannedDirUntCost,
    PlannedDirUntBill,
    WeightedUntCost,
    WeightedUntBill,
    UntRevenue,
    Status,
    PctComplByPeriodFlg,
    RevenueMethod,
    AnalysisBasis,
    GRMethod,
    CostGRRtMethod,
    BillGRRtMethod,
    Company,
    StartingDayOfWeek,
    CostCurrencyCode,
    BillingCurrencyCode,

    CompensationFee,
    ConsultantFee,
    ReimbAllowance,
    CompensationFeeBill,
    ConsultantFeeBill,
    ReimbAllowanceBill,

    CompensationFeeDirLab,
    CompensationFeeDirExp,
    ReimbAllowanceExp,
    ReimbAllowanceCon,
    CompensationFeeDirLabBill,
    CompensationFeeDirExpBill,
    ReimbAllowanceExpBill,
    ReimbAllowanceConBill,

    EVFormula,
    LabBillMultiplier,
    UntBillMultiplier,
    BaselineDirExpCost,
    BaselineDirExpBill,
    BaselineDirConCost,
    BaselineDirConBill,
    BaselineDirUntCost,
    BaselineDirUntBill,
    CommitmentFlg,
    TopdownFee,
    ExpWBSLevel,
    ConWBSLevel,
    NeedsRecalc,
    LastPlanAction,
	LCLevel1Enabled,
	LCLevel2Enabled,
	LCLevel3Enabled,
	LCLevel4Enabled,
	LCLevel5Enabled,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      PlanID,
      PlanName,
      PlanNumber,
      ClientID,
      ProjMgr,
      Principal,
      Supervisor,
      Org,
      StartDate,
      EndDate,
      BaselineLaborHrs,
      BaselineLabCost,
      BaselineLabBill,
      BaselineExpCost,
      BaselineExpBill,
      BaselineConCost,
      BaselineConBill,
      BaselineRevenue,
      BaselineStart,
      BaselineFinish,
      PlannedLaborHrs,
      PlannedLabCost,
      PlannedLabBill,
      PlannedExpCost,
      PlannedExpBill,
      PlannedConCost,
      PlannedConBill,
      LabRevenue,
      PlannedDirExpCost,
      PlannedDirExpBill,
      PlannedDirConCost,
      PlannedDirConBill,
      CostRtMethod,
      BillingRtMethod,
      CostRtTableNo,
      BillingRtTableNo,
      ContingencyPct,
      ContingencyAmt,
      OverheadPct,
      AvailableFlg,
      GenResTableNo,
      UnPostedFlg,
      BudgetType,
      PctCompleteFormula,
      PctComplete,
      PctCompleteBill,
      PctCompleteLabCost,
      PctCompleteLabBill,
      PctCompleteExpCost,
      PctCompleteExpBill,
      PctCompleteConCost,
      PctCompleteConBill,
      WeightedLabCost,
      WeightedLabBill,
      WeightedExpCost,
      WeightedExpBill,
      WeightedConCost,
      WeightedConBill,
      UtilizationIncludeFlg,
      WBS1,
      Probability,
      LabMultType,
      Multiplier,
      ProjectedMultiplier,
      ProjectedRatio,
      CalcExpBillAmtFlg,
      CalcConBillAmtFlg,
      ExpBillRtMethod,
      ExpBillRtTableNo,
      ConBillRtMethod,
      ConBillRtTableNo,
      GRBillTableNo,
      ExpBillMultiplier,
      ConBillMultiplier,
      ReimbMethod,
      ExpRevenue,
      ConRevenue,
      TargetMultCost,
      TargetMultBill,
      BaselineUntQty,
      BaselineUntCost,
      BaselineUntBill,
      PlannedUntQty,
      PlannedUntCost,
      PlannedUntBill,
      PctCompleteUntCost,
      PctCompleteUntBill,
      PlannedDirUntCost,
      PlannedDirUntBill,
      WeightedUntCost,
      WeightedUntBill,
      UntRevenue,
      Status,
      PctComplByPeriodFlg,
      RevenueMethod,
      AnalysisBasis,
      GRMethod,
      CostGRRtMethod,
      BillGRRtMethod,
      Company,
      StartingDayOfWeek,
      CostCurrencyCode,
      BillingCurrencyCode,

      CompensationFee,
      ConsultantFee,
      ReimbAllowance,
      CompensationFeeBill,
      ConsultantFeeBill,
      ReimbAllowanceBill,

      CompensationFeeDirLab,
      CompensationFeeDirExp,
      ReimbAllowanceExp,
      ReimbAllowanceCon,
      CompensationFeeDirLabBill,
      CompensationFeeDirExpBill,
      ReimbAllowanceExpBill,
      ReimbAllowanceConBill,

      EVFormula,
      LabBillMultiplier,
      UntBillMultiplier,
      BaselineDirExpCost,
      BaselineDirExpBill,
      BaselineDirConCost,
      BaselineDirConBill,
      BaselineDirUntCost,
      BaselineDirUntBill,
      CommitmentFlg,
      TopdownFee,
      ExpWBSLevel,
      ConWBSLevel,
      NeedsRecalc,
      'REVERTED' AS LastPlanAction,
	  LCLevel1Enabled,
	  LCLevel2Enabled,
	  LCLevel3Enabled,
	  LCLevel4Enabled,
	  LCLevel5Enabled,

      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPPlan AS P WHERE P.PlanID = @strPlanID

  INSERT PNTask (
    TaskID,
    PlanID,
    Name,
    WBS1,
    WBS2,
    WBS3,
    LaborCode,
    WBSType,
    ParentOutlineNumber,
    OutlineNumber,
    ChildrenCount,
    OutlineLevel,
    Priority,
    PctComplete,
    PctCompleteLabCost,
    PctCompleteLabBill,
    PctCompleteExpCost,
    PctCompleteExpBill,
    PctCompleteConCost,
    PctCompleteConBill,
    OverheadPct,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    BaselineExpCost,
    BaselineExpBill,
    BaselineConCost,
    BaselineConBill,
    BaselineRevenue,
    PlannedLaborHrs,
    PlannedLabCost,
    PlannedLabBill,
    PlannedExpCost,
    PlannedExpBill,
    PlannedConCost,
    PlannedConBill,
    LabRevenue,
    PlannedDirExpCost,
    PlannedDirExpBill,
    PlannedDirConCost,
    PlannedDirConBill,
    WeightedLabCost,
    WeightedLabBill,
    WeightedExpCost,
    WeightedExpBill,
    WeightedConCost,
    WeightedConBill,
    Duration,
    ActualDuration,
    BaselineDuration,
    RemainingDuration,
    ScheduleConstraint,
    StartDate,
    ActualStart,
    BaselineStart,
    EarlyStart,
    LateStart,
    ScheduledStart,
    EndDate,
    ActualFinish,
    BaselineFinish,
    EarlyFinish,
    LateFinish,
    ScheduledFinish,
    CostRate,
    BillingRate,
    ExpBillRate,
    ConBillRate,
    ExpRevenue,
    ConRevenue,
    UntCostRate,
    UntBillRate,
    BaselineUntQty,
    BaselineUntCost,
    BaselineUntBill,
    PlannedUntQty,
    PlannedUntCost,
    PlannedUntBill,
    PctCompleteUntCost,
    PctCompleteUntBill,
    PlannedDirUntCost,
    PlannedDirUntBill,
    WeightedUntCost,
    WeightedUntBill,
    UntRevenue,
    BaselineDirExpCost,
    BaselineDirExpBill,
    BaselineDirConCost,
    BaselineDirConBill,
    BaselineDirUntCost,
    BaselineDirUntBill,
    CompensationFee,
    ConsultantFee,
    ReimbAllowance,
    CompensationFeeBill,
    ConsultantFeeBill,
    ReimbAllowanceBill,
    CompensationFeeDirLab,
    CompensationFeeDirExp,
    ReimbAllowanceExp,
    ReimbAllowanceCon,
    CompensationFeeDirLabBill,
    CompensationFeeDirExpBill,
    ReimbAllowanceExpBill,
    ReimbAllowanceConBill,
    UnmatchedFlg,
    Status,
	RevenueStartDate,
	RevenueEndDate,
    ClientID,
    ProjMgr,
    ProjectType,
    ChargeType,
    Org,
    Notes,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      TaskID,
      PlanID,
      Name,
      WBS1,
      WBS2,
      WBS3,
      LaborCode,
      WBSType,
      ParentOutlineNumber,
      OutlineNumber,
      ChildrenCount,
      OutlineLevel,
      Priority,
      PctComplete,
      PctCompleteLabCost,
      PctCompleteLabBill,
      PctCompleteExpCost,
      PctCompleteExpBill,
      PctCompleteConCost,
      PctCompleteConBill,
      OverheadPct,
      BaselineLaborHrs,
      BaselineLabCost,
      BaselineLabBill,
      BaselineExpCost,
      BaselineExpBill,
      BaselineConCost,
      BaselineConBill,
      BaselineRevenue,
      PlannedLaborHrs,
      PlannedLabCost,
      PlannedLabBill,
      PlannedExpCost,
      PlannedExpBill,
      PlannedConCost,
      PlannedConBill,
      LabRevenue,
      PlannedDirExpCost,
      PlannedDirExpBill,
      PlannedDirConCost,
      PlannedDirConBill,
      WeightedLabCost,
      WeightedLabBill,
      WeightedExpCost,
      WeightedExpBill,
      WeightedConCost,
      WeightedConBill,
      Duration,
      ActualDuration,
      BaselineDuration,
      RemainingDuration,
      ScheduleConstraint,
      StartDate,
      ActualStart,
      BaselineStart,
      EarlyStart,
      LateStart,
      ScheduledStart,
      EndDate,
      ActualFinish,
      BaselineFinish,
      EarlyFinish,
      LateFinish,
      ScheduledFinish,
      CostRate,
      BillingRate,
      ExpBillRate,
      ConBillRate,
      ExpRevenue,
      ConRevenue,
      UntCostRate,
      UntBillRate,
      BaselineUntQty,
      BaselineUntCost,
      BaselineUntBill,
      PlannedUntQty,
      PlannedUntCost,
      PlannedUntBill,
      PctCompleteUntCost,
      PctCompleteUntBill,
      PlannedDirUntCost,
      PlannedDirUntBill,
      WeightedUntCost,
      WeightedUntBill,
      UntRevenue,
      BaselineDirExpCost,
      BaselineDirExpBill,
      BaselineDirConCost,
      BaselineDirConBill,
      BaselineDirUntCost,
      BaselineDirUntBill,
      CompensationFee,
      ConsultantFee,
      ReimbAllowance,
      CompensationFeeBill,
      ConsultantFeeBill,
      ReimbAllowanceBill,
      CompensationFeeDirLab,
      CompensationFeeDirExp,
      ReimbAllowanceExp,
      ReimbAllowanceCon,
      CompensationFeeDirLabBill,
      CompensationFeeDirExpBill,
      ReimbAllowanceExpBill,
      ReimbAllowanceConBill,
      UnmatchedFlg,
      Status,
      RevenueStartDate,
	  RevenueEndDate,
      ClientID,
      ProjMgr,
      ProjectType,
      ChargeType,
      Org,
      Notes,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPTask AS T WHERE T.PlanID = @strPlanID
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT PNAssignment (
    AssignmentID,
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    LaborCode,
    ResourceID,
    GenericResourceID,
    Category,
    GRLBCD,
    ActivityID,
    CostRate,
    BillingRate,
    PctCompleteLabCost,
    PctCompleteLabBill,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    BaselineRevenue,
    BaselineStart,
    BaselineFinish,
    PlannedLaborHrs,
    PlannedLabCost,
    PlannedLabBill,
    LabRevenue,
    WeightedLabCost,
    WeightedLabBill,
    StartDate,
    EndDate,
    SortSeq,
    HardBooked,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      AssignmentID,
      PlanID,
      TaskID,
      WBS1,
      WBS2,
      WBS3,
      LaborCode,
      ResourceID,
      GenericResourceID,
      Category,
      GRLBCD,
      ActivityID,
      CostRate,
      BillingRate,
      PctCompleteLabCost,
      PctCompleteLabBill,
      BaselineLaborHrs,
      BaselineLabCost,
      BaselineLabBill,
      BaselineRevenue,
      BaselineStart,
      BaselineFinish,
      PlannedLaborHrs,
      PlannedLabCost,
      PlannedLabBill,
      LabRevenue,
      WeightedLabCost,
      WeightedLabBill,
      StartDate,
      EndDate,
      SortSeq,
      HardBooked,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPAssignment AS A WHERE A.PlanID = @strPlanID

  INSERT PNPlannedLabor (
    TimePhaseID,
    TaskID,
    PlanID,
    AssignmentID,
    StartDate,
    EndDate,
    PeriodHrs,
    PeriodCost,
    PeriodBill,
    CostRate,
    BillingRate,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      TimePhaseID,
      TaskID,
      PlanID,
      AssignmentID,
      StartDate,
      EndDate,
      PeriodHrs,
      PeriodCost,
      PeriodBill,
      CostRate,
      BillingRate,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPPlannedLabor AS PL WHERE PL.PlanID = @strPlanID
  
  INSERT PNBaselineLabor (
    TimePhaseID,
    TaskID,
    PlanID,
    AssignmentID,
    StartDate,
    EndDate,
    PeriodHrs,
    PeriodCost,
    PeriodBill,
    CostRate,
    BillingRate,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      TimePhaseID,
      TaskID,
      PlanID,
      AssignmentID,
      StartDate,
      EndDate,
      PeriodHrs,
      PeriodCost,
      PeriodBill,
      CostRate,
      BillingRate,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPBaselineLabor AS BL WHERE BL.PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Expenses

  INSERT PNExpense (
    ExpenseID,
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    Account,
    Vendor,
    StartDate,
    EndDate,
    DirectAcctFlg,
    ExpBillRate,
    PlannedExpCost,
    PlannedExpBill,
    PlannedDirExpCost,
    PlannedDirExpBill,
    BaselineStart,
    BaselineFinish,
    BaselineExpCost,
    BaselineExpBill,
    BaselineDirExpCost,
    BaselineDirExpBill,
    ExpRevenue,
    SortSeq,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      ExpenseID,
      PlanID,
      TaskID,
      WBS1,
      WBS2,
      WBS3,
      Account,
      Vendor,
      StartDate,
      EndDate,
      DirectAcctFlg,
      ExpBillRate,
      PlannedExpCost,
      PlannedExpBill,
      PlannedDirExpCost,
      PlannedDirExpBill,
      BaselineStart,
      BaselineFinish,
      BaselineExpCost,
      BaselineExpBill,
      BaselineDirExpCost,
      BaselineDirExpBill,
      ExpRevenue,
      SortSeq,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPExpense AS E WHERE E.PlanID = @strPlanID

  INSERT PNPlannedExpenses (
    TimePhaseID,
    TaskID,
    PlanID,
    ExpenseID,
    StartDate,
    EndDate,
    PeriodCost,
    PeriodBill,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      TimePhaseID,
      TaskID,
      PlanID,
      ExpenseID,
      StartDate,
      EndDate,
      PeriodCost,
      PeriodBill,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPPlannedExpenses AS TPD WHERE TPD.PlanID = @strPlanID

  INSERT PNBaselineExpenses (
    TimePhaseID,
    TaskID,
    PlanID,
    ExpenseID,
    StartDate,
    EndDate,
    PeriodCost,
    PeriodBill,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      TimePhaseID,
      TaskID,
      PlanID,
      ExpenseID,
      StartDate,
      EndDate,
      PeriodCost,
      PeriodBill,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPBaselineExpenses AS TPD WHERE TPD.PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Consultants

  INSERT PNConsultant (
    ConsultantID,
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    Account,
    Vendor,
    StartDate,
    EndDate,
    DirectAcctFlg,
    ConBillRate,
    PlannedConCost,
    PlannedConBill,
    PlannedDirConCost,
    PlannedDirConBill,
    BaselineStart,
    BaselineFinish,
    BaselineConCost,
    BaselineConBill,
    BaselineDirConCost,
    BaselineDirConBill,
    ConRevenue,
    SortSeq,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      ConsultantID,
      PlanID,
      TaskID,
      WBS1,
      WBS2,
      WBS3,
      Account,
      Vendor,
      StartDate,
      EndDate,
      DirectAcctFlg,
      ConBillRate,
      PlannedConCost,
      PlannedConBill,
      PlannedDirConCost,
      PlannedDirConBill,
      BaselineStart,
      BaselineFinish,
      BaselineConCost,
      BaselineConBill,
      BaselineDirConCost,
      BaselineDirConBill,
      ConRevenue,
      SortSeq,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPConsultant AS C WHERE C.PlanID = @strPlanID

  INSERT PNPlannedConsultant (
    TimePhaseID,
    TaskID,
    PlanID,
    ConsultantID,
    StartDate,
    EndDate,
    PeriodCost,
    PeriodBill,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      TimePhaseID,
      TaskID,
      PlanID,
      ConsultantID,
      StartDate,
      EndDate,
      PeriodCost,
      PeriodBill,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPPlannedConsultant AS TPD WHERE TPD.PlanID = @strPlanID

  INSERT PNBaselineConsultant (
    TimePhaseID,
    TaskID,
    PlanID,
    ConsultantID,
    StartDate,
    EndDate,
    PeriodCost,
    PeriodBill,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      TimePhaseID,
      TaskID,
      PlanID,
      ConsultantID,
      StartDate,
      EndDate,
      PeriodCost,
      PeriodBill,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPBaselineConsultant AS TPD WHERE TPD.PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT PNWBSLevelFormat (
    WBSFormatID,
    PlanID,
    FmtLevel,
    WBSType,
    WBSMatch,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT 
      WBSFormatID,
      PlanID,
      FmtLevel,
      WBSType,
      WBSMatch,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPWBSLevelFormat WHERE PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- If the given Plan has fragmented Time-Phased Data (TPD) as a result of someone ran Resource Management (RM) with smaller scale
  -- (e.g. Weekly) than the Plan's scale (e.g. Monthly), then we need to realign the TPD in PN tables after reverting the Plan.

  SET @tiHasFragmentedTPD = dbo.RM$HasFragmentedTPD(@strPlanID)

  IF (@tiHasFragmentedTPD = 1)
    BEGIN

      -- Realign Time-Phased Data in PN tables.

      EXECUTE dbo.PNAlignTPD @strPlanID

      -- Recalculate Time-Phased Data in PN tables.

      EXECUTE dbo.PNDeleteSummaryTPD @strPlanID, 'Y', 'Y', 'Y'
      EXECUTE dbo.PNCalcTPD @strPlanID, 'Y', 'Y', 'Y'
      EXECUTE dbo.PNSumUpTPD @strPlanID, 'Y', 'Y', 'Y'

    END -- END IF (@tiHasFragmentedTPD = 1)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Update VersionID for both PNPlan and RPPlan.

  EXECUTE dbo.stRPUpdateVersionID @strPlanID, 1 

 --Update the LastPlanAction Field to REVERTED status.

  UPDATE PNPlan SET LastPlanAction = 'REVERTED'  WHERE PlanID = @strPlanID

  COMMIT

  SET NOCOUNT OFF

END -- stRPRevert
GO
