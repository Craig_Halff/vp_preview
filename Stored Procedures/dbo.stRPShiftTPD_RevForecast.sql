SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPShiftTPD_RevForecast]
  @strTimePhaseID varchar(32),
  @strFromStartDate varchar(8),
  @strFromEndDate varchar(8),
  @strToStartDate varchar(8),
  @strToEndDate varchar(8),
  @bitShiftAll bit, /* 0 = Shift using the "From" range, 1 = Shift the entire Assignment Row */
  @bitFixStartDate bit /* 1 = Hold "To" StartDate fixed, 0 = Hold "To" EndDate fixed */
AS

BEGIN -- Procedure stRPShiftAssigment

  SET NOCOUNT ON
  
  DECLARE @dtFromStartDate datetime
  DECLARE @dtFromEndDate datetime
  DECLARE @dtMinTPD datetime
  DECLARE @dtMaxTPD datetime
  DECLARE @dtBeforeDate datetime
  DECLARE @dtAfterDate datetime

  DECLARE @dtToOrgStartDate datetime
  DECLARE @dtToOrgEndDate datetime

  DECLARE @dtToTPDStartDate datetime
  DECLARE @dtToTPDEndDate datetime

  DECLARE @dtASGStartDate datetime
  DECLARE @dtASGEndDate datetime
  DECLARE @dtToASGStartDate datetime
  DECLARE @dtToASGEndDate datetime

  DECLARE @strCompany nvarchar(14)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strResourceID nvarchar(20)
  DECLARE @strGenericResourceID nvarchar(20)
  DECLARE @strUserName nvarchar(32)

  DECLARE @siGRMethod smallint
  DECLARE @siCostRtMethod smallint
  DECLARE @siBillingRtMethod smallint
  
  DECLARE @intCostRtTableNo int
  DECLARE @intBillingRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int

  DECLARE @siHrDecimals smallint
  
  DECLARE @decBeforeWD decimal(19,4) = 0
  DECLARE @decAfterWD decimal(19,4) = 0
  DECLARE @decASGOffsetWD decimal(19,4) = 0
  DECLARE @decASGDurationWD decimal(19,4) = 0

  DECLARE @intMinRowSeq int = 0
  DECLARE @intMaxRowSeq int = 0
  DECLARE @intTPDDurationWD int = 0

  -- Declare Temp tables.
  
  DECLARE @tabRevTPD TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    TimePhaseID varchar(32) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PeriodBill decimal(19,4),
    PeriodCost decimal(19,4),
    AT_OutlineNumber varchar(255) COLLATE database_default
    UNIQUE(PlanID, TaskID, TimePhaseID)
  )


-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

  SELECT 
    @siHrDecimals = HrDecimals
    FROM CFGRMSettings

  -- Set Dates

  SET @dtFromStartDate = CONVERT(datetime, @strFromStartDate)
  SET @dtFromEndDate = CONVERT(datetime, @strFromEndDate)
  SET @dtBeforeDate = DATEADD(DAY, -1, @dtFromStartDate)
  SET @dtAfterDate = DATEADD(DAY, 1, @dtFromEndDate)

  SET @dtToOrgStartDate = CONVERT(datetime, @strToStartDate)
  SET @dtToOrgEndDate = CONVERT(datetime, @strToEndDate)
  SET @dtToTPDStartDate = CONVERT(datetime, @strToStartDate)
  SET @dtToTPDEndDate = CONVERT(datetime, @strToEndDate)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabRevTPD(
    PlanID,
    TaskID,
    TimePhaseID,
    StartDate,
    EndDate,
    PeriodBill,
    PeriodCost,
    AT_OutlineNumber
  )
    SELECT DISTINCT
      A.PlanID,
      A.TaskID,
      A.TimePhaseID,
      A.StartDate,
      A.EndDate,
	  A.PeriodBill,
      A.PeriodCost,
      AT.OutlineNumber AS AT_OutlineNumber
      FROM PNPlannedRevenueLabor AS A
        INNER JOIN PNTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
      WHERE
        A.TimePhaseID = @strTimePhaseID AND
        A.StartDate <= @dtFromEndDate AND A.EndDate >= @dtFromStartDate

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get various parameters from Plan, Task

  SELECT 
    @strPlanID = T.PlanID,
    @strTaskID = A.TaskID,
    @strCompany = P.Company,
    @dtASGStartDate = A.StartDate,
    @dtASGEndDate = A.EndDate
    FROM @tabRevTPD AS A
      INNER JOIN PNTask AS T ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
      INNER JOIN PNPlan AS P ON A.PlanID = P.PlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@bitShiftAll = 1)
    BEGIN

      -- Calculating WD for Assignment Duration and Offset.

      SELECT @decASGDurationWD = dbo.DLTK$NumWorkingDays(@dtASGStartDate, @dtASGEndDate, @strCompany) 

      SELECT @decASGOffsetWD = 
        CASE 
          WHEN @bitFixStartDate = 1
          THEN
            CASE 
              WHEN @dtFromStartDate = @dtASGStartDate 
              THEN 0
              ELSE dbo.DLTK$NumWorkingDays(@dtFromStartDate, @dtASGStartDate, @strCompany) - 1
            END
          ELSE
            CASE
              WHEN @dtASGEndDate = @dtFromEndDate
              THEN 0
              ELSE dbo.DLTK$NumWorkingDays(@dtASGEndDate, @dtFromEndDate, @strCompany) - 1
            END
        END
                  
      -- Need to calculate @dtToASGStartDate or @dtToASGEndDate because of @bitFixStartDate setiting
                  
      SELECT
        @dtToASGStartDate = 
          CASE 
            WHEN @bitFixStartDate = 0
            THEN
              CASE
                WHEN @decASGDurationWD = 0
                THEN @dtToOrgEndDate
                ELSE
                  dbo.DLTK$AddBusinessDays(
                    @dtToOrgEndDate, 
                    -1 * ((@decASGDurationWD - 1) + @decASGOffsetWD), 
                    @strCompany
                  )
              END
            ELSE 
              dbo.DLTK$AddBusinessDays(
                @dtToOrgStartDate, 
                @decASGOffsetWD, 
                @strCompany
              )
          END,
        @dtToASGEndDate = 
          CASE 
            WHEN @bitFixStartDate = 1
            THEN
              dbo.DLTK$AddBusinessDays(
                @dtToOrgStartDate, 
                CASE
                  WHEN @decASGDurationWD <> 0
                  THEN ((@decASGDurationWD - 1) + @decASGOffsetWD)
                  ELSE (@decASGOffsetWD) 
                END, 
                @strCompany
              )
            ELSE 
              dbo.DLTK$AddBusinessDays(
                @dtToOrgEndDate, 
                -1 * (@decASGOffsetWD), 
                @strCompany
              )
          END

      UPDATE PNPlannedRevenueLabor SET
        StartDate = 
          CASE
            WHEN ISNULL(@strFromStartDate, '') = '' THEN StartDate
            ELSE @dtToASGStartDate               
            END,
        EndDate = 
          CASE
            WHEN ISNULL(@strFromEndDate, '') = '' THEN EndDate             
            ELSE @dtToASGEndDate
          END
        WHERE PlanID = @strPlanID AND TaskID = @strTaskID AND TimePhaseID = @strTimePhaseID

    END /* END IF (@bitShiftAll = 1) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     
  SET NOCOUNT OFF

END -- stRPShiftTPD_RevForecast
GO
