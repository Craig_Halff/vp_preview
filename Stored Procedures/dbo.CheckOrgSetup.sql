SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CheckOrgSetup]
@transStr varchar(2),
@ExpenseEnabled varchar(1),
@RetVal varchar(max) output
as
begin -- procedure CheckOrgSetup

	DECLARE
	@Company as varchar(14),
	@PayrollAccount as varchar(13),
	@PayrollAcctType as integer,
	@FICAAccount as varchar(13),
	@FICAAcctType as integer,
	@BalanceSheet as varchar(1),
	@OrgLevels as int,
	@LabDist as varchar(1),
	@Units as varchar(1),
	@Misc as varchar(1),
	@Prints as varchar(1),
	@Cons as varchar(1),
	@EmplExp as varchar(1),
	@Payroll as varchar(1),
	@FICA as varchar(1),
	@AcctsReceivable as varchar(13),
	@LaborCreditAccount as varchar(13),
	@LaborCreditAcctType as integer,
	@MiscCreditAccount as varchar(13),
	@MiscCreditAcctType as integer,
	@PrintsCreditAccount as varchar(13),
	@PrintsCreditAcctType as integer,
	@ConsAccrAccount as varchar(13),
	@ConsAccrAcctType as integer,
	@EmplExpCreditAccount as varchar(13),
	@EmplExpCreditAcctType as integer,
	@EmplAdvCreditAccount as varchar(13),
	@EmplAdvCreditAcctType as integer,
	@RealizedGainsAccount as varchar(13),
	@RealizedGainsAcctType as integer,
	@UnrealizedGainsAccount as varchar(13),
	@UnrealizedGainsAcctType as integer, 
	@RealizedLossesAccount as varchar(13),
	@RealizedLossesAcctType as integer,
	@UnrealizedLossesAccount as varchar(13),
	@UnrealizedLossesAcctType  as integer
	
	declare
	@Org as varchar(14),
	@LabDistWBS1 as varchar(30),
	@LabDistWBS2 as varchar(7),
	@LabDistWBS3 as varchar(7),
	@LabDistSubLevel as varchar(1), 
	@LabDistChargeType as varchar(1),
	@UnitsBSWBS1 as varchar(30),
	@UnitsBSWBS2 as varchar(7),
	@UnitsBSWBS3 as varchar(7),
	@UnitsBSSubLevel as varchar(1), 
	@UnitsBSChargeType as varchar(1),
	@UnitsIndWBS1 as varchar(30),
	@UnitsIndWBS2 as varchar(7),
	@UnitsIndWBS3 as varchar(7),
	@UnitsIndSubLevel as varchar(1), 
	@UnitsIndChargeType as varchar(1),
	@UnitsOtherWBS1 as varchar(30),
	@UnitsOtherWBS2 as varchar(7),
	@UnitsOtherWBS3 as varchar(7),
	@UnitsOtherSubLevel as varchar(1), 
	@UnitsOtherChargeType as varchar(1),	
	@MiscWBS1 as varchar(30),
	@MiscWBS2 as varchar(7),
	@MiscWBS3 as varchar(7),
	@MiscSubLevel as varchar(1), 
	@MiscChargeType as varchar(1),	
	@PrintsWBS1 as varchar(30),
	@PrintsWBS2 as varchar(7),
	@PrintsWBS3 as varchar(7),
	@PrintsSubLevel as varchar(1), 
	@PrintsChargeType as varchar(1),	
	@ConsAccrWBS1 as varchar(30),
	@ConsAccrWBS2 as varchar(7),
	@ConsAccrWBS3 as varchar(7),
	@ConsAccrSubLevel as varchar(1), 
	@ConsAccrChargeType as varchar(1),	
	@EmplExpWBS1 as varchar(30),
	@EmplExpWBS2 as varchar(7),
	@EmplExpWBS3 as varchar(7),
	@EmplExpSubLevel as varchar(1), 
	@EmplExpChargeType as varchar(1),	
	@EmplAdvWBS1 as varchar(30),
	@EmplAdvWBS2 as varchar(7),
	@EmplAdvWBS3 as varchar(7),
	@EmplAdvSubLevel as varchar(1), 
	@EmplAdvChargeType as varchar(1),	
	@PayrollWBS1 as varchar(30),
	@PayrollWBS2 as varchar(7),
	@PayrollWBS3 as varchar(7),
	@PayrollSubLevel as varchar(1), 
	@PayrollChargeType as varchar(1),
	@FICAWBS1 as varchar(30),
	@FICAWBS2 as varchar(7),
	@FICAWBS3 as varchar(7),
	@FICASubLevel as varchar(1), 
	@FICAChargeType as varchar(1),
	@RealizedGainsWBS1 as varchar(30), 
	@RealizedGainsWBS2 as varchar(7), 
	@RealizedGainsWBS3 as varchar(7),
	@RealizedGainsSubLevel as varchar(1), 
	@RealizedGainsChargeType as varchar(1),
	@RealizedLossesWBS1 as varchar(30),
	@RealizedLossesWBS2 as varchar(7),
	@RealizedLossesWBS3 as varchar(7),
	@RealizedLossesSubLevel as varchar(1), 
	@RealizedLossesChargeType as varchar(1),
	@UnrealizedGainsWBS1 as varchar(30), 
	@UnrealizedGainsWBS2 as varchar(7), 
	@UnrealizedGainsWBS3 as varchar(7),
	@UnrealizedGainsSubLevel as varchar(1),
	@UnrealizedGainsChargeType as varchar(1),
	@UnrealizedLossesWBS1 as varchar(30), 
	@UnrealizedLossesWBS2 as varchar(7), 
	@UnrealizedLossesWBS3 as varchar(7),
	@UnrealizedLossesSubLevel as varchar(1),
	@UnrealizedLossesChargeType as varchar(1)
	
	DECLARE	@MulticurrencyEnabled as varchar(1)
	DECLARE @MulticompanyEnabled as varchar(1)
	DECLARE @count int
	
	SET @RetVal = ''

	SELECT @BalanceSheet = BalanceSheet, @MulticurrencyEnabled = MulticurrencyEnabled, @OrgLevels = OrgLevels, @MulticompanyEnabled = MulticompanyEnabled FROM FW_CFGSystem CFGSystem, CFGFormat

	SELECT	@FICAAccount = isNull(FICAAccount,''),
			@PayrollAccount = isNull(Case When PayrollPostToBank = 'Y' Then CFGBanks.Account Else PayrollCreditAccount End,''), 
			@FICAAcctType = IsNull(CAFICA.Type, 0),
			@PayrollAcctType = ISNULL(CAPayroll.Type, 0) 
			From CFGPYMain Left Join CFGBanks On CFGPYMain.PayrollBankCode = CFGBanks.Code 
			Left Join CA CAFICA ON CFGPYMain.FICAAccount = CAFICA.Account 
			Left Join CA CAPayroll ON (Case When PayrollPostToBank = 'Y' Then CFGBanks.Account Else PayrollCreditAccount End) = CAPayroll.Account
			Where CFGPYMain.Company = dbo.GetActiveCompany()
		
	Select @count = count (*) from Organization
	if @count=0
		begin
			SET @RetVal= '1' -- ErrorAccessingOrganizationTable
		end
	
	if @MulticompanyEnabled = 'Y'
		begin	
			DECLARE preOrgSetupCursor CURSOR FOR 
			Select Left(O.Org,CFGFormat.Org1Length) As Company
			From Organization O
			Left Join CFGFormat ON 1=1
			Where Left(O.Org,CFGFormat.Org1Length) NOT IN(Select Company From CFGAutoPosting)
			Group By Left(O.Org,CFGFormat.Org1Length)
			begin
				OPEN preOrgSetupCursor
				FETCH NEXT FROM preOrgSetupCursor INTO @Company
				WHILE @@FETCH_STATUS = 0
					begin
						if @RetVal > '' SET @RetVal = @RetVal + '|'
						SET @RetVal = @RetVal + '2~' + @Company -- ErrorSetupInformationMissing
						FETCH NEXT FROM preOrgSetupCursor INTO @Company
					end	   
					CLOSE preOrgSetupCursor
					DEALLOCATE preOrgSetupCursor
			end
		end
				
	DECLARE orgSetupCursor CURSOR FOR
	Select 
	CFGOrgSetup.Company, 
	CFGOrgSetup.LabDist,
	CFGOrgSetup.Units,
	CFGOrgSetup.Misc,
	CFGOrgSetup.Prints,
	CFGOrgSetup.Cons,
	CFGOrgSetup.EmplExp,
	CFGOrgSetup.Payroll,
	CFGOrgSetup.FICA,
	v_CFGAutoPosting.AcctsReceivable,
	v_CFGAutoPosting.LaborCreditAccount,
	v_CFGAutoPosting.LaborCreditAcctType,
	v_CFGAutoPosting.MiscCreditAccount,
	v_CFGAutoPosting.MiscCreditAcctType,
	v_CFGAutoPosting.PrintsCreditAccount,
	v_CFGAutoPosting.PrintsCreditAcctType,
	v_CFGAutoPosting.ConsAccrAccount,
	v_CFGAutoPosting.ConsAccrAcctType,
	v_CFGAutoPosting.EmplExpCreditAccount,
	v_CFGAutoPosting.EmplExpCreditAcctType,
	v_CFGAutoPosting.EmplAdvCreditAccount,
	v_CFGAutoPosting.EmplAdvCreditAcctType,
	v_CFGAutoPosting.RealizedGainsAccount,
	v_CFGAutoPosting.RealizedGainsAcctType,
	v_CFGAutoPosting.UnrealizedGainsAccount,
	v_CFGAutoPosting.UnrealizedGainsAcctType,   
	v_CFGAutoPosting.RealizedLossesAccount,
	v_CFGAutoPosting.RealizedLossesAcctType,
	v_CFGAutoPosting.UnrealizedLossesAccount,
	v_CFGAutoPosting.UnrealizedLossesAcctType

	From CFGMainData CFGMain
	Left Join  CFGOrgSetup ON CFGMain.Company = CFGOrgSetup.Company
	Left Join 
	(select cfgautoposting.*, IsNull(CALaborCredit.Type, 0) As LaborCreditAcctType, 
	IsNull(CAMiscCredit.Type, 0) As MiscCreditAcctType, 
	IsNull(CAPrintsCredit.Type, 0) AS PrintsCreditAcctType, 
	isNull(CAConsAccr.Type, 0) AS ConsAccrAcctType,
	IsNull(CAEmplExpCredit.Type, 0) As EmplExpCreditAcctType,
	IsNull(CAEmplAdvCredit.Type, 0) As EmplAdvCreditAcctType,
	IsNull(CARealizedGains.Type, 0) As RealizedGainsAcctType, 
	IsNull(CAUnrealizedGains.Type, 0) AS UnrealizedGainsAcctType, 
	IsNull(CARealizedLosses.Type, 0) As RealizedLossesAcctType, 
	IsNull(CAUnrealizedLosses.Type, 0) AS UnrealizedLossesAcctType 
	from cfgautoposting 
	Left Join CA AS CALaborCredit ON cfgautoposting.LaborCreditAccount = CALaborCredit.Account 
	Left Join CA AS CAMiscCredit ON cfgautoposting.MiscCreditAccount = CAMiscCredit.Account 
	Left Join CA AS CAPrintsCredit ON cfgautoposting.PrintsCreditAccount = CAPrintsCredit.Account 
	Left Join CA AS CAConsAccr ON cfgautoposting.ConsAccrAccount = CAConsAccr.Account 
	Left Join CA AS CAEmplExpCredit ON cfgautoposting.EmplExpCreditAccount = CAEmplExpCredit.Account 
	Left Join CA AS CAEmplAdvCredit ON cfgautoposting.EmplAdvCreditAccount = CAEmplAdvCredit.Account 
	Left Join CA AS CARealizedGains ON cfgautoposting.RealizedGainsAccount = CARealizedGains.Account 
	Left Join CA AS CAUnrealizedGains ON cfgautoposting.UnrealizedGainsAccount = CAUnrealizedGains.Account 
	Left Join CA AS CARealizedLosses ON cfgautoposting.RealizedLossesAccount = CARealizedLosses.Account 
	Left Join CA AS CAUnrealizedLosses ON cfgautoposting.UnrealizedLossesAccount = CAUnrealizedLosses.Account 
	) as v_cfgautoposting 
	ON CFGMain.Company = v_cfgautoposting.Company
	Left Join CFGFormat ON 1=1
	Order By CFGMain.Company

	BEGIN
		OPEN orgSetupCursor
		FETCH NEXT FROM orgSetupCursor INTO 
	   @Company,
	   @LabDist,
	   @Units,
	   @Misc,
	   @Prints,
	   @Cons,
	   @EmplExp,
	   @Payroll,
	   @FICA,
	   @AcctsReceivable,
	   @LaborCreditAccount,
	   @LaborCreditAcctType,
	   @MiscCreditAccount,
	   @MiscCreditAcctType,
	   @PrintsCreditAccount,
	   @PrintsCreditAcctType,
	   @ConsAccrAccount,
	   @ConsAccrAcctType,
	   @EmplExpCreditAccount,
	   @EmplExpCreditAcctType,
	   @EmplAdvCreditAccount,
	   @EmplAdvCreditAcctType,
	   @RealizedGainsAccount,
	   @RealizedGainsAcctType,
	   @UnrealizedGainsAccount,
	   @UnrealizedGainsAcctType,   
	   @RealizedLossesAccount,
	   @RealizedLossesAcctType,
	   @UnrealizedLossesAccount,
	   @UnrealizedLossesAcctType
 
	  
		if (@MulticurrencyEnabled <> 'Y' or @BalanceSheet <> 'Y') and (@transStr = 'IN' or @transStr = 'CR' or @transStr = 'AP' or @transStr = 'PP')
			GOTO Return_One; -- skip when multicurrency is not enabled.  return with no error
	   
	   WHILE @@FETCH_STATUS = 0
		BEGIN
 	
 			if @MulticompanyEnabled = 'Y'
				begin
					DECLARE orgCursor CURSOR FOR
					Select O.Org,
					isNull(LabDistWBS1,''), isNull(LabDistWBS2,''), isNull(LabDistWBS3,''),isNull(prLabDist.SubLevel,'') LabDistSubLevel,isNull(prLabDist.ChargeType,'') LabDistChargeType,
					isNull(UnitsBSWBS1,''), isNull(UnitsBSWBS2,''), isNull(UnitsBSWBS3,''),isNull(prUnitsBS.SubLevel,'') UnitsBSSubLevel,isNull(prUnitsBS.ChargeType,'') UnitsBSChargeType,
					isNull(UnitsIndWBS1,''), isNull(UnitsIndWBS2,''), isNull(UnitsIndWBS3,''),isNull(prUnitsInd.SubLevel,'') UnitsIndSubLevel,isNull(prUnitsInd.ChargeType,'') UnitsIndChargeType,
					isNull(UnitsOtherWBS1,''), isNull(UnitsOtherWBS2,''), isNull(UnitsOtherWBS3,''),isNull(prUnitsOther.SubLevel,'') UnitsOtherSubLevel,isNull(prUnitsOther.ChargeType,'') UnitsOtherChargeType,
					isNull(MiscWBS1,''), isNull(MiscWBS2,''), isNull(MiscWBS3,''),isNull(prMisc.SubLevel,'') MiscSubLevel,isNull(prMisc.ChargeType,'') MiscChargeType,
					isNull(PrintsWBS1,''), isNull(PrintsWBS2,''), isNull(PrintsWBS3,''),isNull(prPrints.SubLevel,'') PrintsSubLevel,isNull(prPrints.ChargeType,'') PrintsChargeType,
					isNull(ConsAccrWBS1,''), isNull(ConsAccrWBS2,''), isNull(ConsAccrWBS3,''),isNull(prConsAccr.SubLevel,'') ConsAccrSubLevel,isNull(prConsAccr.ChargeType,'') ConsAccrChargeType,
					isNull(EmplExpWBS1,''), isNull(EmplExpWBS2,''), isNull(EmplExpWBS3,''),isNull(prEmplExp.SubLevel,'') EmplExpSubLevel,isNull(prEmplExp.ChargeType,'') EmplExpChargeType,
					isNull(EmplAdvWBS1,''), isNull(EmplAdvWBS2,''), isNull(EmplAdvWBS3,''),isNull(prEmplAdv.SubLevel,'') EmplAdvSubLevel,isNull(prEmplAdv.ChargeType,'') EmplAdvChargeType,
					isNull(PayrollWBS1,''), isNull(PayrollWBS2,''), isNull(PayrollWBS3,''),isNull(prPayroll.SubLevel,'') PayrollSubLevel,isNull(prPayroll.ChargeType,'') PayrollChargeType,
					isNull(FICAWBS1,''), isNull(FICAWBS2,''), isNull(FICAWBS3,''),isNull(prFICA.SubLevel,'') FICASubLevel,isNull(prFICA.ChargeType,'') FICAChargeType,
					isNull(RealizedGainsWBS1,''), isNull(RealizedGainsWBS2,''), isNull(RealizedGainsWBS3,''),isNull(prRealizedGains.SubLevel,'') RealizedGainsSubLevel,isNull(prRealizedGains.ChargeType,'') RealizedGainsChargeType,
					isNull(RealizedLossesWBS1,''),isNull(RealizedLossesWBS2,''),isNull(RealizedLossesWBS3,''),isNull(prRealizedLosses.SubLevel,'') RealizedLossesSubLevel,isNull(prRealizedLosses.ChargeType,'') RealizedLossesChargeType,
					isNull(UnrealizedGainsWBS1,''), isNull(UnrealizedGainsWBS2,''), isNull(UnrealizedGainsWBS3,''),isNull(prUnrealizedGains.SubLevel,'') UnrealizedGainsSubLevel,isNull(prUnrealizedGains.ChargeType,'') UnrealizedGainsChargeType,
					isNull(UnrealizedLossesWBS1,''), isNull(UnrealizedLossesWBS2,''), isNull(UnrealizedLossesWBS3,''),isNull(prUnrealizedLosses.SubLevel,'') UnrealizedLossesSubLevel,isNull(prUnrealizedLosses.ChargeType,'') UnrealizedLossesChargeType
					From Organization O
					Left Join PR prLabDist ON prLabDist.WBS1=LabDistWBS1 AND prLabDist.WBS2=LabDistWBS2 AND prLabDist.WBS3=LabDistWBS3 
					Left Join PR prUnitsBS ON prUnitsBS.WBS1=UnitsBSWBS1 AND prUnitsBS.WBS2=UnitsBSWBS2 AND prUnitsBS.WBS3=UnitsBSWBS3 
					Left Join PR prUnitsInd ON prUnitsInd.WBS1=UnitsIndWBS1 AND prUnitsInd.WBS2=UnitsIndWBS2 AND prUnitsInd.WBS3=UnitsIndWBS3 
					Left Join PR prUnitsOther ON prUnitsOther.WBS1=UnitsOtherWBS1 AND prUnitsOther.WBS2=UnitsOtherWBS2 AND prUnitsOther.WBS3=UnitsOtherWBS3 
					Left Join PR prMisc ON prMisc.WBS1=MiscWBS1 AND prMisc.WBS2=MiscWBS2 AND prMisc.WBS3=MiscWBS3 
					Left Join PR prPrints ON prPrints.WBS1=PrintsWBS1 AND prPrints.WBS2=PrintsWBS2 AND prPrints.WBS3=PrintsWBS3 
					Left Join PR prConsAccr ON prConsAccr.WBS1=ConsAccrWBS1 AND prConsAccr.WBS2=ConsAccrWBS2 AND prConsAccr.WBS3=ConsAccrWBS3 
					Left Join PR prEmplExp ON prEmplExp.WBS1=EmplExpWBS1 AND prEmplExp.WBS2=EmplExpWBS2 AND prEmplExp.WBS3=EmplExpWBS3 
					Left Join PR prEmplAdv ON prEmplAdv.WBS1=EmplAdvWBS1 AND prEmplAdv.WBS2=EmplAdvWBS2 AND prEmplAdv.WBS3=EmplAdvWBS3 
					Left Join PR prPayroll ON prPayroll.WBS1=PayrollWBS1 AND prPayroll.WBS2=PayrollWBS2 AND prPayroll.WBS3=PayrollWBS3 
					Left Join PR prFICA ON prFICA.WBS1=FICAWBS1 AND prFICA.WBS2=FICAWBS2 AND prFICA.WBS3=FICAWBS3 
					Left Join PR prRealizedGains ON prRealizedGains.WBS1=RealizedGainsWBS1 AND prRealizedGains.WBS2=RealizedGainsWBS2 AND prRealizedGains.WBS3=RealizedGainsWBS3 
					Left Join PR prRealizedLosses ON prRealizedLosses.WBS1=RealizedLossesWBS1 AND prRealizedLosses.WBS2=RealizedLossesWBS2 AND prRealizedLosses.WBS3=RealizedLossesWBS3 
					Left Join PR prUnrealizedGains ON prUnrealizedGains.WBS1=UnrealizedGainsWBS1 AND prUnrealizedGains.WBS2=UnrealizedGainsWBS2 AND prUnrealizedGains.WBS3=UnrealizedGainsWBS3 
					Left Join PR prUnrealizedLosses ON prUnrealizedLosses.WBS1=UnrealizedLossesWBS1 AND prUnrealizedLosses.WBS2=UnrealizedLossesWBS2 AND prUnrealizedLosses.WBS3=UnrealizedLossesWBS3 
					Left Join CFGFormat ON 1=1
					Where Left(O.Org,CFGFormat.Org1Length) = @Company
				end
			else
				begin
					DECLARE orgCursor CURSOR FOR
					Select O.Org,
					isNull(LabDistWBS1,''), isNull(LabDistWBS2,''), isNull(LabDistWBS3,''),isNull(prLabDist.SubLevel,'') LabDistSubLevel,isNull(prLabDist.ChargeType,'') LabDistChargeType,
					isNull(UnitsBSWBS1,''), isNull(UnitsBSWBS2,''), isNull(UnitsBSWBS3,''),isNull(prUnitsBS.SubLevel,'') UnitsBSSubLevel,isNull(prUnitsBS.ChargeType,'') UnitsBSChargeType,
					isNull(UnitsIndWBS1,''), isNull(UnitsIndWBS2,''), isNull(UnitsIndWBS3,''),isNull(prUnitsInd.SubLevel,'') UnitsIndSubLevel,isNull(prUnitsInd.ChargeType,'') UnitsIndChargeType,
					isNull(UnitsOtherWBS1,''), isNull(UnitsOtherWBS2,''), isNull(UnitsOtherWBS3,''),isNull(prUnitsOther.SubLevel,'') UnitsOtherSubLevel,isNull(prUnitsOther.ChargeType,'') UnitsOtherChargeType,
					isNull(MiscWBS1,''), isNull(MiscWBS2,''), isNull(MiscWBS3,''),isNull(prMisc.SubLevel,'') MiscSubLevel,isNull(prMisc.ChargeType,'') MiscChargeType,
					isNull(PrintsWBS1,''), isNull(PrintsWBS2,''), isNull(PrintsWBS3,''),isNull(prPrints.SubLevel,'') PrintsSubLevel,isNull(prPrints.ChargeType,'') PrintsChargeType,
					isNull(ConsAccrWBS1,''), isNull(ConsAccrWBS2,''), isNull(ConsAccrWBS3,''),isNull(prConsAccr.SubLevel,'') ConsAccrSubLevel,isNull(prConsAccr.ChargeType,'') ConsAccrChargeType,
					isNull(EmplExpWBS1,''), isNull(EmplExpWBS2,''), isNull(EmplExpWBS3,''),isNull(prEmplExp.SubLevel,'') EmplExpSubLevel,isNull(prEmplExp.ChargeType,'') EmplExpChargeType,
					isNull(EmplAdvWBS1,''), isNull(EmplAdvWBS2,''), isNull(EmplAdvWBS3,''),isNull(prEmplAdv.SubLevel,'') EmplAdvSubLevel,isNull(prEmplAdv.ChargeType,'') EmplAdvChargeType,
					isNull(PayrollWBS1,''), isNull(PayrollWBS2,''), isNull(PayrollWBS3,''),isNull(prPayroll.SubLevel,'') PayrollSubLevel,isNull(prPayroll.ChargeType,'') PayrollChargeType,
					isNull(FICAWBS1,''), isNull(FICAWBS2,''), isNull(FICAWBS3,''),isNull(prFICA.SubLevel,'') FICASubLevel,isNull(prFICA.ChargeType,'') FICAChargeType,
					isNull(RealizedGainsWBS1,''), isNull(RealizedGainsWBS2,''), isNull(RealizedGainsWBS3,''),isNull(prRealizedGains.SubLevel,'') RealizedGainsSubLevel,isNull(prRealizedGains.ChargeType,'') RealizedGainsChargeType,
					isNull(RealizedLossesWBS1,''),isNull(RealizedLossesWBS2,''),isNull(RealizedLossesWBS3,''),isNull(prRealizedLosses.SubLevel,'') RealizedLossesSubLevel,isNull(prRealizedLosses.ChargeType,'') RealizedLossesChargeType,
					isNull(UnrealizedGainsWBS1,''), isNull(UnrealizedGainsWBS2,''), isNull(UnrealizedGainsWBS3,''),isNull(prUnrealizedGains.SubLevel,'') UnrealizedGainsSubLevel,isNull(prUnrealizedGains.ChargeType,'') UnrealizedGainsChargeType,
					isNull(UnrealizedLossesWBS1,''), isNull(UnrealizedLossesWBS2,''), isNull(UnrealizedLossesWBS3,''),isNull(prUnrealizedLosses.SubLevel,'') UnrealizedLossesSubLevel,isNull(prUnrealizedLosses.ChargeType,'') UnrealizedLossesChargeType
					From Organization O
					Left Join PR prLabDist ON prLabDist.WBS1=LabDistWBS1 AND prLabDist.WBS2=LabDistWBS2 AND prLabDist.WBS3=LabDistWBS3 
					Left Join PR prUnitsBS ON prUnitsBS.WBS1=UnitsBSWBS1 AND prUnitsBS.WBS2=UnitsBSWBS2 AND prUnitsBS.WBS3=UnitsBSWBS3 
					Left Join PR prUnitsInd ON prUnitsInd.WBS1=UnitsIndWBS1 AND prUnitsInd.WBS2=UnitsIndWBS2 AND prUnitsInd.WBS3=UnitsIndWBS3 
					Left Join PR prUnitsOther ON prUnitsOther.WBS1=UnitsOtherWBS1 AND prUnitsOther.WBS2=UnitsOtherWBS2 AND prUnitsOther.WBS3=UnitsOtherWBS3 
					Left Join PR prMisc ON prMisc.WBS1=MiscWBS1 AND prMisc.WBS2=MiscWBS2 AND prMisc.WBS3=MiscWBS3 
					Left Join PR prPrints ON prPrints.WBS1=PrintsWBS1 AND prPrints.WBS2=PrintsWBS2 AND prPrints.WBS3=PrintsWBS3 
					Left Join PR prConsAccr ON prConsAccr.WBS1=ConsAccrWBS1 AND prConsAccr.WBS2=ConsAccrWBS2 AND prConsAccr.WBS3=ConsAccrWBS3 
					Left Join PR prEmplExp ON prEmplExp.WBS1=EmplExpWBS1 AND prEmplExp.WBS2=EmplExpWBS2 AND prEmplExp.WBS3=EmplExpWBS3 
					Left Join PR prEmplAdv ON prEmplAdv.WBS1=EmplAdvWBS1 AND prEmplAdv.WBS2=EmplAdvWBS2 AND prEmplAdv.WBS3=EmplAdvWBS3 
					Left Join PR prPayroll ON prPayroll.WBS1=PayrollWBS1 AND prPayroll.WBS2=PayrollWBS2 AND prPayroll.WBS3=PayrollWBS3 
					Left Join PR prFICA ON prFICA.WBS1=FICAWBS1 AND prFICA.WBS2=FICAWBS2 AND prFICA.WBS3=FICAWBS3 
					Left Join PR prRealizedGains ON prRealizedGains.WBS1=RealizedGainsWBS1 AND prRealizedGains.WBS2=RealizedGainsWBS2 AND prRealizedGains.WBS3=RealizedGainsWBS3 
					Left Join PR prRealizedLosses ON prRealizedLosses.WBS1=RealizedLossesWBS1 AND prRealizedLosses.WBS2=RealizedLossesWBS2 AND prRealizedLosses.WBS3=RealizedLossesWBS3 
					Left Join PR prUnrealizedGains ON prUnrealizedGains.WBS1=UnrealizedGainsWBS1 AND prUnrealizedGains.WBS2=UnrealizedGainsWBS2 AND prUnrealizedGains.WBS3=UnrealizedGainsWBS3 
					Left Join PR prUnrealizedLosses ON prUnrealizedLosses.WBS1=UnrealizedLossesWBS1 AND prUnrealizedLosses.WBS2=UnrealizedLossesWBS2 AND prUnrealizedLosses.WBS3=UnrealizedLossesWBS3 
					Left Join CFGFormat ON 1=1
				end
			
			BEGIN
				OPEN orgCursor
				FETCH NEXT FROM orgCursor INTO @Org, 
				@LabDistWBS1, @LabDistWBS2, @LabDistWBS3, @LabDistSubLevel, @LabDistChargeType,
				@UnitsBSWBS1, @UnitsBSWBS2, @UnitsBSWBS3, @UnitsBSSubLevel, @UnitsBSChargeType,
				@UnitsIndWBS1, @UnitsIndWBS2, @UnitsIndWBS3, @UnitsIndSubLevel, @UnitsIndChargeType,
				@UnitsOtherWBS1, @UnitsOtherWBS2, @UnitsOtherWBS3, @UnitsOtherSubLevel, @UnitsOtherChargeType,
				@MiscWBS1, @MiscWBS2, @MiscWBS3, @MiscSubLevel, @MiscChargeType,
				@PrintsWBS1, @PrintsWBS2, @PrintsWBS3, @PrintsSubLevel, @PrintsChargeType,
				@ConsAccrWBS1, @ConsAccrWBS2, @ConsAccrWBS3, @ConsAccrSubLevel, @ConsAccrChargeType,
				@EmplExpWBS1, @EmplExpWBS2, @EmplExpWBS3, @EmplExpSubLevel, @EmplExpChargeType,
				@EmplAdvWBS1, @EmplAdvWBS2, @EmplAdvWBS3, @EmplAdvSubLevel, @EmplAdvChargeType,
				@PayrollWBS1, @PayrollWBS2, @PayrollWBS3, @PayrollSubLevel, @PayrollChargeType,
				@FICAWBS1, @FICAWBS2, @FICAWBS3, @FICASubLevel, @FICAChargeType,
				@RealizedGainsWBS1, @RealizedGainsWBS2, @RealizedGainsWBS3, @RealizedGainsSubLevel, @RealizedGainsChargeType,
				@RealizedLossesWBS1,@RealizedLossesWBS2,@RealizedLossesWBS3, @RealizedLossesSubLevel, @RealizedLossesChargeType,
				@UnrealizedGainsWBS1, @UnrealizedGainsWBS2, @UnrealizedGainsWBS3,@UnrealizedGainsSubLevel,@UnrealizedGainsChargeType,
				@UnrealizedLossesWBS1, @UnrealizedLossesWBS2, @UnrealizedLossesWBS3,@UnrealizedLossesSubLevel,@UnrealizedLossesChargeType
				
					BEGIN
					WHILE @@FETCH_STATUS = 0
						BEGIN
							IF @transStr = 'TS' Or @transStr = 'LA' Or @transStr = 'PY' Or @transStr = ''
								IF (@LabDist = 'Y') And ((@Balancesheet = 'Y') Or @LaborCreditAccount = '' Or 
                                (@LaborCreditAcctType > 3 And @LaborCreditAcctType < 10))
									begin
										-- Labor distribution project
										if (DATALENGTH(@LabDistWBS3)=0) or (DATALENGTH(@LabDistWBS2)=0)	or (@LabDistSubLevel <> 'N')
											begin
												if @RetVal > '' SET @RetVal = @RetVal + '|'
													SET @RetVal = @RetVal + 'A~' + @Org -- ErrorInvalidLaborDistributionWBS1ForOrg
											end
										else -- ValidProjectAccount
											if dbo.ValidProjectAccount(@LaborCreditAccount,@Company,@LaborCreditAcctType,@AcctsReceivable,@LabDistWBS1,@LabDistChargeType,@OrgLevels,@BalanceSheet) = 'N'
												begin
													if @RetVal > '' SET @RetVal = @RetVal + '|'
														SET @RetVal = @RetVal + 'B~' + @Org -- ErrorIncompatableLaborDistributionWBS1Account
												end
									end
						    If @transStr = 'UN' Or @transStr = 'UP' Or @transStr = ''
								begin
									IF (@Units = 'Y') And @Balancesheet = 'Y'
										begin
											if (DATALENGTH(@UnitsBSWBS3)=0) or (DATALENGTH(@UnitsBSWBS2)=0)	or (@UnitsBSSubLevel <> 'N')
												begin
													if @RetVal > '' SET @RetVal = @RetVal + '|'
														SET @RetVal = @RetVal + 'C~' + @Org -- ErrorInvalidUnitBalancesheetWBS1
												end
											else -- ValidProjectAccount
												if dbo.ValidProjectAccount('',@Company,2,@AcctsReceivable,@UnitsBSWBS1,@UnitsBSChargeType,@OrgLevels,@BalanceSheet) = 'N'
													begin
														if @RetVal > '' SET @RetVal = @RetVal + '|'
															SET @RetVal = @RetVal + 'D~' + @Org -- ErrorIncompatableUnitBalancesheetWBS1Account
													end
										end
									--Units indirect project
									If @Units = 'Y' 
										begin
											if (DATALENGTH(@UnitsIndWBS3)=0) or (DATALENGTH(@UnitsIndWBS2)=0)	or (@UnitsIndSubLevel <> 'N')
												begin
													if @RetVal > '' SET @RetVal = @RetVal + '|'
														SET @RetVal = @RetVal + 'E~' + @Org -- ErrorInvalidUnitIndirectWBS1
												end
											else -- ValidProjectAccount
												if dbo.ValidProjectAccount('',@Company,9,@AcctsReceivable,@UnitsIndWBS1,@UnitsIndChargeType,@OrgLevels,@BalanceSheet) = 'N'
													begin
														if @RetVal > '' SET @RetVal = @RetVal + '|'
															SET @RetVal = @RetVal + 'F~' + @Org -- ErrorIncompatableUnitIndirectWBS1Account
													end
										end
									--Units other project
									If (@Units = 'Y') And @Balancesheet = 'Y'
										begin
											if (DATALENGTH(@UnitsOtherWBS3)=0) or (DATALENGTH(@UnitsOtherWBS2)=0) or (@UnitsOtherSubLevel <> 'N')
												begin
													if @RetVal > '' SET @RetVal = @RetVal + '|'
														SET @RetVal = @RetVal + 'G~' + @Org -- ErrorInvalidUnitOtherChargesWBS1
												end
											else -- ValidProjectAccount
												if dbo.ValidProjectAccount('',@Company,10,@AcctsReceivable,@UnitsOtherWBS1,@UnitsOtherChargeType,@OrgLevels,@BalanceSheet) = 'N'
													begin
														if @RetVal > '' SET @RetVal = @RetVal + '|'
															SET @RetVal = @RetVal + 'H~' + @Org -- ErrorIncompatableUnitOtherChargesWBS1Account
													end
										end
								end -- end of @transStr = 'UN' Or @transStr = 'UP' Or @transStr = ''
							
							IF @transStr = 'MI' Or @transStr = '' 
								if @Misc = 'Y' --Miscellaneous expense project
									begin					
											if (DATALENGTH(@MiscWBS3)=0) or (DATALENGTH(@MiscWBS2)=0) or (@MiscSubLevel <> 'N')
												begin
													if @RetVal > '' SET @RetVal = @RetVal + '|'
														SET @RetVal = @RetVal + 'I~' + @Org -- ErrorInvalidMiscellaneousExpenseWBS1
												end
											else -- ValidProjectAccount
												if dbo.ValidProjectAccount(@MiscCreditAccount,@Company,@MiscCreditAcctType,@AcctsReceivable,@MiscWBS1,@MiscChargeType,@OrgLevels,@BalanceSheet) = 'N'
													begin
														if @RetVal > '' SET @RetVal = @RetVal + '|'
															SET @RetVal = @RetVal + 'J~' + @Org -- ErrorIncompatableMiscExpenseWBS1Account
													end					

									end -- End 'MI', '', Misc = 'Y'
                        	
							If @transStr = 'PR' Or @transStr = ''
								if @Prints = 'Y'
									begin
										 --Prints and Reproductions project
										if (DATALENGTH(@PrintsWBS3)=0) or (DATALENGTH(@PrintsWBS2)=0) or (@PrintsSubLevel <> 'N')
											begin
												if @RetVal > '' SET @RetVal = @RetVal + '|'
													SET @RetVal = @RetVal + 'K~' + @Org -- ErrorInvalidPrintsWBS1
											end
										else -- ValidProjectAccount
											if dbo.ValidProjectAccount(@PrintsCreditAccount,@Company,@PrintsCreditAcctType,@AcctsReceivable,@PrintsWBS1,@PrintsChargeType,@OrgLevels,@BalanceSheet) = 'N'
												begin
													if @RetVal > '' SET @RetVal = @RetVal + '|'
														SET @RetVal = @RetVal + 'L~' + @Org -- ErrorIncompatablePrintsWBS1Account
												end	
									end -- end 'PR, '' Prints & Reproduction
	                    If @transStr = 'CN' Or @transStr = ''
							if @Cons = 'Y' And @BalanceSheet = 'Y'
								begin --Consultant Accrual expense project
									if (DATALENGTH(@ConsAccrWBS3)=0) or (DATALENGTH(@ConsAccrWBS2)=0) or (@ConsAccrSubLevel <> 'N')
											begin
												if @RetVal > '' SET @RetVal = @RetVal + '|'
													SET @RetVal = @RetVal + 'M~' + @Org -- ErrorInvalidConsultantAccrualWBS1
											end
										else -- ValidProjectAccount
											if dbo.ValidProjectAccount(@ConsAccrAccount,@Company,@ConsAccrAcctType,@AcctsReceivable,@ConsAccrWBS1,@ConsAccrChargeType,@OrgLevels,@BalanceSheet) = 'N'
												begin
													if @RetVal > '' SET @RetVal = @RetVal + '|'
														SET @RetVal = @RetVal + 'N~' + @Org -- ErrorImcompatableConsultantAccrualWBS1Accoun
												end	

							end -- end CN & '' & Cons = 'Y' an dBalancesheet = 'Y'

                        If (@ExpenseEnabled = 'Y' And @transStr = '') Or @transStr = 'EX' Or @transStr = 'EP' Or @transStr = 'ER'
							if @EmplExp = 'Y'
								begin
			                        --Employee expense/advance project
									if (DATALENGTH(@EmplExpWBS3)=0) or (DATALENGTH(@EmplExpWBS2)=0) or (@EmplExpSubLevel <> 'N')
											begin
												if @RetVal > '' SET @RetVal = @RetVal + '|'
													SET @RetVal = @RetVal + 'O~' + @Org -- ErrorInvalidEmployeeExpenseWBS1
											end
										else -- ValidProjectAccount
											if dbo.ValidProjectAccount(@EmplExpCreditAccount,@Company,@EmplExpCreditAcctType,@AcctsReceivable,@EmplExpWBS1,@EmplExpChargeType,@OrgLevels,@BalanceSheet) = 'N'
												begin
													if @RetVal > '' SET @RetVal = @RetVal + '|'
														SET @RetVal = @RetVal + 'P~' + @Org -- ErrorIncompatableEmployeeExpenseWBS1Account
												end									
									if (DATALENGTH(@EmplAdvWBS3)=0) or (DATALENGTH(@EmplAdvWBS2)=0) or (@EmplAdvSubLevel <> 'N')
											begin
												if @RetVal > '' SET @RetVal = @RetVal + '|'
													SET @RetVal = @RetVal + 'Q~' + @Org -- ErrorInvalidEmployeeAdvanceWBS1
											end
										else -- ValidProjectAccount
											if dbo.ValidProjectAccount(@EmplAdvCreditAccount,@Company,@EmplAdvCreditAcctType,@AcctsReceivable,@EmplAdvWBS1,@EmplAdvChargeType,@OrgLevels,@BalanceSheet) = 'N'
												begin
													if @RetVal > '' SET @RetVal = @RetVal + '|'
														SET @RetVal = @RetVal + 'R~' + @Org -- ErrorIncompatableEmployeeAdvanceWBS1Account
												end		
								end -- end 'EX', ''
							IF @Company = dbo.getactivecompany() And (@transstr = 'PY' or @transstr = '')
								begin
									if @Payroll = 'Y' And @BalanceSheet = 'Y'
										begin
											if (DATALENGTH(@PayrollWBS3)=0) or (DATALENGTH(@PayrollWBS2)=0) or (@PayrollSubLevel <> 'N')
												begin
													if @RetVal > '' SET @RetVal = @RetVal + '|'
														SET @RetVal = @RetVal + 'S' + @Org -- ErrorInvalidPayrollLiabilityWBS1
												end
											else -- ValidProjectAccount
												if dbo.ValidProjectAccount(@PayrollAccount,@Company,@PayrollAcctType,@AcctsReceivable,@PayrollWBS1,@PayrollChargeType,@OrgLevels,@BalanceSheet) = 'N'
													begin
														if @RetVal > '' SET @RetVal = @RetVal + '|'
															SET @RetVal = @RetVal + 'T' + @Org -- ErrorIncompatablePayrollLiabilityWBS1Account
													end	
										end -- @Payroll
									if @FICA = 'Y' And (@Balancesheet = 'Y' Or @FICAAccount = '' Or (@FICAAcctType >= 5 And @FICAAcctType < 10))
										begin
											if (DATALENGTH(@FICAWBS3)=0) or (DATALENGTH(@FICAWBS2)=0) or (@FICASubLevel <> 'N')
												begin
													if @RetVal > '' SET @RetVal = @RetVal + '|'
														SET @RetVal = @RetVal + 'U' + @Org -- ErrorInvalidFICAExpenseWBS1
												end
											else -- ValidProjectAccount
												if dbo.ValidProjectAccount(@FICAAccount,@Company,@FICAAcctType,@AcctsReceivable,@FICAWBS1,@FICAChargeType,@OrgLevels,@BalanceSheet) = 'N'
													begin
														if @RetVal > '' SET @RetVal = @RetVal + '|'
															SET @RetVal = @RetVal + 'V' + @Org -- ErrorIncompatableFICAExpenseWBS1Account
													end	
										end -- @FICA
								end -- PayrollEnabled
									
							IF @MulticurrencyEnabled = 'Y' And @BalanceSheet = 'Y' And (@transStr = 'IN' Or @transStr = 'CR' Or @transStr = 'AP' Or @transStr = 'PP' Or @transStr = '')
								begin
									--Gains and losses 
									if (DATALENGTH(@RealizedGainsWBS3)=0) or (DATALENGTH(@RealizedGainsWBS2)=0)	or (@RealizedGainsSubLevel <> 'N')
										begin
											if @RetVal > '' SET @RetVal = @RetVal + '|'
											SET @RetVal = @RetVal + '3~' + @Org -- ErrorInvalidRealizedGainsWBS1ForOrg
										end
									else -- ValidProjectAccount
										if dbo.ValidProjectAccount(@RealizedGainsAccount,@Company,@RealizedGainsAcctType,@AcctsReceivable,@RealizedGainsWBS1,@RealizedGainsChargeType,@OrgLevels,@BalanceSheet) = 'N'
											begin
												if @RetVal > '' SET @RetVal = @RetVal + '|'
												SET @RetVal = @RetVal + '4~' + @Org -- ErrorIncompatableRealizedGainsWBS1Acct
											end
											
									if DATALENGTH(@RealizedLossesWBS3)=0 or DATALENGTH(@RealizedLossesWBS2)=0 or @RealizedLossesSubLevel <> 'N'
										begin
											if @RetVal > '' SET @RetVal = @RetVal + '|'
											SET @RetVal = @RetVal + '5~' + @Org -- WarningInvalidRealizedLossesWBS1ForOrg
										end
									else -- ValidProjectAccount
										if dbo.ValidProjectAccount(@RealizedLossesAccount,@Company,@RealizedLossesAcctType,@AcctsReceivable,@RealizedLossesWBS1,@RealizedLossesChargeType,@OrgLevels,@BalanceSheet) = 'N'
											begin
												if @RetVal > '' SET @RetVal = @RetVal + '|'
												SET @RetVal = @RetVal + '6~' + @Org -- WarningRealizedLossesWBS1AccountForOrg
											end

									if DATALENGTH(@UnrealizedGainsWBS3)=0 or DATALENGTH(@UnrealizedGainsWBS2)=0 or @UnrealizedGainsSubLevel <> 'N'
										begin
											if @RetVal > '' SET @RetVal = @RetVal + '|'
											SET @RetVal = @RetVal + '7~' + @Org -- ErrorInvalidUnrealizedGainsWBS1ForOrg
										end
									else -- ValidProjectAccount
										if dbo.ValidProjectAccount(@UnrealizedGainsAccount,@Company,@UnrealizedGainsAcctType,@AcctsReceivable,@UnrealizedGainsWBS1,@UnrealizedGainsChargeType,@OrgLevels,@BalanceSheet) = 'N'
											begin
												if @RetVal > '' SET @RetVal = @RetVal + '|'
												SET @RetVal = @RetVal + '8~' + @Org -- ErrorIncompatableUnrealizedGainsWBS1AcctForOrg
											end		
											
									if DATALENGTH(@UnrealizedLossesWBS3)=0 or DATALENGTH(@UnrealizedLossesWBS2)=0 or @UnrealizedLossesSubLevel <> 'N'
										begin
											if @RetVal > '' SET @RetVal = @RetVal + '|'
											SET @RetVal = @RetVal + '9~' + @Org -- WarningInvalidUnrealizedLossesWBS1ForOrg
										end
									else -- ValidProjectAccount
										if dbo.ValidProjectAccount(@UnrealizedLossesAccount,@Company,@UnrealizedLossesAcctType,@AcctsReceivable,@UnrealizedLossesWBS1,@UnrealizedLossesChargeType,@OrgLevels,@BalanceSheet) = 'N'
											begin
												if @RetVal > '' SET @RetVal = @RetVal + '|'
												SET @RetVal = @RetVal + '0~' + @Org -- WarningUnrealizedLossesWBS1AccountForOrg
											end																						
								end -- end @MulticurrencyEnabled = 'Y' And @BalanceSheet = 'Y' And 'IN' Or 'CR' Or 'AP' Or 'PP' Or  ''
											
								FETCH NEXT FROM orgCursor INTO @Org, 
								@LabDistWBS1, @LabDistWBS2, @LabDistWBS3, @LabDistSubLevel, @LabDistChargeType,
								@UnitsBSWBS1, @UnitsBSWBS2, @UnitsBSWBS3, @UnitsBSSubLevel, @UnitsBSChargeType,
								@UnitsIndWBS1, @UnitsIndWBS2, @UnitsIndWBS3, @UnitsIndSubLevel, @UnitsIndChargeType,
								@UnitsOtherWBS1, @UnitsOtherWBS2, @UnitsOtherWBS3, @UnitsOtherSubLevel, @UnitsOtherChargeType,
								@MiscWBS1, @MiscWBS2, @MiscWBS3, @MiscSubLevel, @MiscChargeType,
								@PrintsWBS1, @PrintsWBS2, @PrintsWBS3, @PrintsSubLevel, @PrintsChargeType,
								@ConsAccrWBS1, @ConsAccrWBS2, @ConsAccrWBS3, @ConsAccrSubLevel, @ConsAccrChargeType,
								@EmplExpWBS1, @EmplExpWBS2, @EmplExpWBS3, @EmplExpSubLevel, @EmplExpChargeType,
								@EmplAdvWBS1, @EmplAdvWBS2, @EmplAdvWBS3, @EmplAdvSubLevel, @EmplAdvChargeType,
								@PayrollWBS1, @PayrollWBS2, @PayrollWBS3, @PayrollSubLevel, @PayrollChargeType,
								@FICAWBS1, @FICAWBS2, @FICAWBS3, @FICASubLevel, @FICAChargeType,
								@RealizedGainsWBS1, @RealizedGainsWBS2, @RealizedGainsWBS3, @RealizedGainsSubLevel, @RealizedGainsChargeType,
								@RealizedLossesWBS1,@RealizedLossesWBS2,@RealizedLossesWBS3, @RealizedLossesSubLevel, @RealizedLossesChargeType,
								@UnrealizedGainsWBS1, @UnrealizedGainsWBS2, @UnrealizedGainsWBS3,@UnrealizedGainsSubLevel,@UnrealizedGainsChargeType,
								@UnrealizedLossesWBS1, @UnrealizedLossesWBS2, @UnrealizedLossesWBS3,@UnrealizedLossesSubLevel,@UnrealizedLossesChargeType
						END
					END
				Close OrgCursor
				Deallocate OrgCursor
			END
			
			FETCH NEXT FROM orgSetupCursor INTO 
			@Company,
			@LabDist,
			@Units,
			@Misc,
			@Prints,
			@Cons,
			@EmplExp,
			@Payroll,
			@FICA,
			@AcctsReceivable,
			@LaborCreditAccount,
			@LaborCreditAcctType,
			@MiscCreditAccount,
			@MiscCreditAcctType,
			@PrintsCreditAccount,
			@PrintsCreditAcctType,
			@ConsAccrAccount,
			@ConsAccrAcctType,
			@EmplExpCreditAccount,
			@EmplExpCreditAcctType,
			@EmplAdvCreditAccount,
			@EmplAdvCreditAcctType,
			@RealizedGainsAccount,
			@RealizedGainsAcctType,
			@UnrealizedGainsAccount,
			@UnrealizedGainsAcctType,   
			@RealizedLossesAccount,
			@RealizedLossesAcctType,
			@UnrealizedLossesAccount,
			@UnrealizedLossesAcctType
		End
		Close orgSetupCursor
		DEALLOCATE orgSetupCursor
	END  
	 
	Return 
	
	
	Return_One:
				Close orgSetupCursor
				DEALLOCATE orgSetupCursor
				RETURN 

end --- end procedure
GO
