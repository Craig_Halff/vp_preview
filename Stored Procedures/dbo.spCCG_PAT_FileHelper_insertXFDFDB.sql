SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[spCCG_PAT_FileHelper_insertXFDFDB]
      @User Nvarchar(32),
      @payableSeq int,
      @XFDFFile Nvarchar(max)
AS
BEGIN
      SET NOCOUNT ON;
      update  CCG_PAT_XFDF set RevisionSeq = RevisionSeq +1 where PayableSeq = @payableSeq;

      insert into CCG_PAT_XFDF (CreateDate,CreateUser,PayableSeq,XFDFFILE)
      values (GETUTCDATE(),@User, @payableSeq, @XFDFFile) ;
END;


GO
