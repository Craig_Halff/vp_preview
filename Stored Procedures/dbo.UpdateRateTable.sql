SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[UpdateRateTable]
    @table Nvarchar(20),
    @tableno Nvarchar(10)
AS
BEGIN
DECLARE @intTableno int = convert(int,@tableno)
	IF @table = 'BTRCTCats' OR @table = '%'
		BEGIN
			update btrctcats
			set startdate = isnull(effectivedate, '1/1/1900'),
			enddate = (select
			IsNull(eDateNull, '1/1/9999') as EndDate  
			FROM (SELECT r1.TableNo, r1.Category, 
			(SELECT TOP 1 DateAdd(day, -1, r2.EffectiveDate) FROM BTRCTCats r2 
			WHERE r1.TableNo = r2.TableNo AND r1.Category = r2.Category 
			AND r2.EffectiveDate > IsNull(r1.EffectiveDate, '1/1/1900') 
			AND r2.tableno = @intTableno
			ORDER BY r2.EffectiveDate) AS eDateNull FROM BTRCTCats r1
			where r1.tableno = btrctcats.tableno and r1.category = btrctcats.category and r1.rateid = btrctcats.rateid
			AND r1.tableno = @intTableno) 
			AS RatesSub)
			WHERE btrctcats.tableno = @intTableno
		END

	IF @table = 'BTRLTCodes' OR @table = '%'
		BEGIN
			update btrltcodes
			set startdate = isnull(effectivedate, '1/1/1900'),
			enddate = (select
			IsNull(eDateNull, '1/1/9999') as EndDate  
			FROM (SELECT r1.TableNo, r1.LaborCodeMask, 
			(SELECT TOP 1 DateAdd(day, -1, r2.EffectiveDate) FROM BTRLTCodes r2 
			WHERE r1.TableNo = r2.TableNo AND r1.LaborCodeMask = r2.LaborCodeMask 
			AND r2.EffectiveDate > IsNull(r1.EffectiveDate, '1/1/1900') 
			AND r2.tableno = @intTableno
			ORDER BY r2.EffectiveDate) AS eDateNull FROM BTRLTCodes r1 
			where r1.tableno = btrltcodes.tableno and r1.LaborCodeMask = btrltcodes.LaborCodeMask and r1.rateid = btrltcodes.rateid
			AND r1.tableno = @intTableno)
			AS RatesSub)
			WHERE btrltcodes.tableno = @intTableno
		END			
		
	IF @table = 'BTROTEmpls' OR @table = '%'
		BEGIN
			update btrotempls
			set startdate = isnull(effectivedate, '1/1/1900'),
			enddate = (select
			IsNull(eDateNull, '1/1/9999') as EndDate  
			FROM (SELECT r1.TableNo, r1.Employee, 
			(SELECT TOP 1 DateAdd(day, -1, r2.EffectiveDate) FROM BTROTEmpls r2 
			WHERE r1.TableNo = r2.TableNo AND r1.Employee = r2.Employee 
			AND r2.EffectiveDate > IsNull(r1.EffectiveDate, '1/1/1900')
			AND r2.tableno = @intTableno
			ORDER BY r2.EffectiveDate) AS eDateNull FROM BTROTEmpls r1
			where r1.tableno = btrotempls.tableno and r1.Employee = btrotempls.Employee and r1.rateid = btrotempls.rateid
			AND r1.tableno = @intTableno)
			AS RatesSub)
			WHERE btrotempls.tableno = @intTableno
		END			

	IF @table = 'BTROTCats' OR @table = '%'
		BEGIN
			update btrotcats
			set startdate = isnull(effectivedate, '1/1/1900'),
			enddate = (select
			IsNull(eDateNull, '1/1/9999') as EndDate  
			FROM (SELECT r1.TableNo, r1.Category, 
			(SELECT TOP 1 DateAdd(day, -1, r2.EffectiveDate) FROM BTROTCats r2 
			WHERE r1.TableNo = r2.TableNo AND r1.Category = r2.Category 
			AND r2.EffectiveDate > IsNull(r1.EffectiveDate, '1/1/1900')
			AND r2.tableno = @intTableno
			ORDER BY r2.EffectiveDate) AS eDateNull FROM BTROTCats r1
			where r1.tableno = btrotcats.tableno and r1.Category = btrotcats.Category and r1.rateid = btrotcats.rateid
			AND r1.tableno = @intTableno)
			AS RatesSub)
			WHERE btrotcats.tableno = @intTableno
		END			

	IF @table = 'BTRRTEmpls' OR @table = '%'
		BEGIN
			update btrrtempls
			set startdate = isnull(effectivedate, '1/1/1900'),
			enddate = (select
			IsNull(eDateNull, '1/1/9999') as EndDate  
			FROM (SELECT r1.TableNo, r1.Employee,  
			(SELECT TOP 1 DateAdd(day, -1, r2.EffectiveDate) FROM BTRRTEmpls r2 
			WHERE r1.TableNo = r2.TableNo AND r1.Employee = r2.Employee 
			AND r2.EffectiveDate > IsNull(r1.EffectiveDate, '1/1/1900')
			AND  r2.tableno = @intTableno
			ORDER BY r2.EffectiveDate) AS eDateNull FROM BTRRTEmpls r1
			where r1.tableno = btrrtempls.tableno and r1.Employee = btrrtempls.Employee and r1.rateid = btrrtempls.rateid
			AND r1.tableno = @intTableno)
			AS RatesSub)
			WHERE btrrtempls.tableno = @intTableno
		END			

	IF @table = 'CostCTCats' OR @table = '%'
		BEGIN
			update costctcats
			set startdate = isnull(effectivedate, '1/1/1900'),
			enddate = (select
			IsNull(eDateNull, '1/1/9999') as EndDate  
			FROM (SELECT r1.TableNo, r1.Category, 
			(SELECT TOP 1 DateAdd(day, -1, r2.EffectiveDate) FROM costctcats r2 
			WHERE r1.TableNo = r2.TableNo AND r1.Category = r2.Category 
			AND r2.EffectiveDate > IsNull(r1.EffectiveDate, '1/1/1900')
			AND  r2.tableno = @intTableno
			ORDER BY r2.EffectiveDate) AS eDateNull FROM costctcats r1
			where r1.tableno = costctcats.tableno and r1.category = costctcats.category and r1.rateid = costctcats.rateid
			AND r1.tableno = @intTableno) 
			AS RatesSub)
			WHERE costctcats.tableno = @intTableno
		END			

	IF @table = 'CostLTCodes' OR @table = '%'
		BEGIN
			update costltcodes
			set startdate = isnull(effectivedate, '1/1/1900'),
			enddate = (select
			IsNull(eDateNull, '1/1/9999') as EndDate  
			FROM (SELECT r1.TableNo, r1.LaborCodeMask, 
			(SELECT TOP 1 DateAdd(day, -1, r2.EffectiveDate) FROM costltcodes r2 
			WHERE r1.TableNo = r2.TableNo AND r1.LaborCodeMask = r2.LaborCodeMask 
			AND r2.EffectiveDate > IsNull(r1.EffectiveDate, '1/1/1900') 
			AND r2.tableno = @intTableno
			ORDER BY r2.EffectiveDate) AS eDateNull FROM costltcodes r1 
			where r1.tableno = costltcodes.tableno and r1.LaborCodeMask = costltcodes.LaborCodeMask and r1.rateid = costltcodes.rateid
			AND  r1.tableno = @intTableno)
			AS RatesSub)
			WHERE  costltcodes.tableno = @intTableno
		END			

	IF @table = 'CostRTEmpls' OR @table = '%'
		BEGIN
			update costrtempls
			set startdate = isnull(effectivedate, '1/1/1900'),
			enddate = (select
			IsNull(eDateNull, '1/1/9999') as EndDate  
			FROM (SELECT r1.TableNo, r1.Employee,  
			(SELECT TOP 1 DateAdd(day, -1, r2.EffectiveDate) FROM costrtempls r2 
			WHERE r1.TableNo = r2.TableNo AND r1.Employee = r2.Employee 
			AND r2.EffectiveDate > IsNull(r1.EffectiveDate, '1/1/1900')
			AND  r2.tableno = @intTableno
			ORDER BY r2.EffectiveDate) AS eDateNull FROM costrtempls r1
			where r1.tableno = costrtempls.tableno and r1.Employee = costrtempls.Employee and r1.rateid = costrtempls.rateid
			AND  r1.tableno = @intTableno)
			AS RatesSub)
			WHERE costrtempls.tableno = @intTableno
		END	
	IF @table = 'CFGCurrencyExchange'
		BEGIN
			update CFGCurrencyExchange
			set startdate = isnull(effectivedate, '1/1/1900'),
			enddate = (select
			IsNull(eDateNull, '1/1/9999') as EndDate  
			FROM (SELECT r1.FromCode, r1.ToCode,  
			(SELECT TOP 1 DateAdd(day, -1, r2.EffectiveDate) FROM CFGCurrencyExchange r2 
			WHERE r1.FromCode = r2.FromCode AND r1.ToCode = r2.ToCode 
			AND r2.EffectiveDate > IsNull(r1.EffectiveDate, '1/1/1900') 
			ORDER BY r2.EffectiveDate) AS eDateNull FROM CFGCurrencyExchange r1
			where r1.FromCode = CFGCurrencyExchange.FromCode and r1.ToCode = CFGCurrencyExchange.ToCode and r1.RateID = CFGCurrencyExchange.RateID)
			AS RatesSub)
		END		
END	
GO
