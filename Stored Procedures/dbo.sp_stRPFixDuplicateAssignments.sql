SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_stRPFixDuplicateAssignments]
AS

BEGIN -- Procedure sp_stRPFixDuplicateAssignments
  
  SET NOCOUNT ON

 DECLARE @tabRPAssignment TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    MinAssignmentID varchar(32) COLLATE database_default
    UNIQUE(PlanID, TaskID, AssignmentID)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabRPAssignment(
    PlanID,
    TaskID,
    AssignmentID,
    MinAssignmentID
  )
    SELECT DISTINCT 
      PlanID,
      TaskID,
      AssignmentID,
      MinAssignmentID
      FROM (
        SELECT 
          PlanID,
          TaskID,
          AssignmentID,
          ResourceID, 
          GenericResourceID,
          MIN(AssignmentID) OVER (PARTITION BY PlanID, TaskID, ResourceID, GenericResourceID) AS MinAssignmentID,
          COUNT(*) OVER (PARTITION BY PlanID, TaskID, ResourceID, GenericResourceID) AS CountRows  
          FROM RPAssignment
      ) AS XA
      WHERE CountRows > 1 AND AssignmentID <> MinAssignmentID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  UPDATE TPD SET
    AssignmentID = YA.MinAssignmentID
    FROM RPPlannedLabor AS TPD
      INNER JOIN @tabRPAssignment AS YA
        ON TPD.PlanID = YA.PlanID AND TPD.TaskID = YA.TaskID AND TPD.AssignmentID = YA.AssignmentID

  UPDATE TPD SET
    AssignmentID = YA.MinAssignmentID
    FROM RPBaselineLabor AS TPD
      INNER JOIN @tabRPAssignment AS YA
        ON TPD.PlanID = YA.PlanID AND TPD.TaskID = YA.TaskID AND TPD.AssignmentID = YA.AssignmentID

  UPDATE AC SET
    AssignmentID = YA.MinAssignmentID
    FROM Activity AS AC
      INNER JOIN @tabRPAssignment AS YA ON AC.AssignmentID = YA.AssignmentID

  DELETE RPAssignment
    WHERE AssignmentID IN (SELECT AssignmentID FROM @tabRPAssignment)

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  UPDATE A SET 
    StartDate = CASE WHEN XTPD.StartDate < A.StartDate THEN XTPD.StartDate ELSE A.StartDate END, 
    EndDate = CASE WHEN XTPD.EndDate > A.EndDate THEN XTPD.EndDate ELSE A.EndDate END
    FROM RPAssignment AS A
      INNER JOIN (
          SELECT DISTINCT PlanID, TaskID, MinAssignmentID AS AssignmentID FROM @tabRPAssignment
      ) AS XA
        ON A.PlanID = XA.PlanID AND A.TaskID = XA.TaskID AND A.AssignmentID = XA.AssignmentID
      INNER JOIN (
        SELECT PlanID, TaskID, AssignmentID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
          FROM RPPlannedLabor
          WHERE AssignmentID IN (SELECT DISTINCT MinAssignmentID FROM @tabRPAssignment)
          GROUP BY PlanID, TaskID, AssignmentID
      ) AS XTPD 
        ON XA.PlanID = XTPD.PlanID AND XA.TaskID = XTPD.TaskID AND XA.AssignmentID = XTPD.AssignmentID

  SET NOCOUNT OFF
     
END -- sp_stRPFixDuplicateAssignments
GO
