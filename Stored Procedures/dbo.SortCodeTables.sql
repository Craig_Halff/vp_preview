SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SortCodeTables]
  @UICultureName AS varchar(10) = ''
AS
BEGIN

SET NOCOUNT ON

EXECUTE SetSeqColumn 'CFGActivitySubjectData', 'Subject', 'Subject', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGActivityTypeDescriptions', 'ActivityType', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGBankAccountIDTypeDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGBankIDTypeDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGCampaignActionDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGCampaignAudienceDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGCampaignObjectiveDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGCampaignStatusDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGCampaignTypeDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGClientCurrentStatusDescriptions', 'CurrentStatus', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGClientRelationshipDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGClientRoleDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGClientTypeDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGContactInfoCatDescriptions', 'Category', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGContactRelationshipDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGContactRoleDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGContactSourceDescriptions', 'ContactSource', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGContactTitleDescriptions', 'Title', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGContractStatusDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGContractTypeDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'FW_CFGCountryDesc', 'Country', 'ISOCountryCode', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGCountyDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGDescriptionCategoryDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGEMDegreeDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGEmployeeRelationshipDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGEmployeeRoleDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGEmployeeTitleDescriptions', 'Title', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGEMRegistrationDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGEMSkillDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGEMSkillLevelDescriptions', 'Description', 'Code', 'UICultureName', '', @UICultureName
--EXECUTE SetSeqColumn 'CFGIndustryDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGLeadRatingDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
--EXECUTE SetSeqColumn 'CFGLeadSourceDescriptions', 'LeadSource', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGLeadStatusDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGLeadStatusReasonDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGOpportunitySourceDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
--EXECUTE SetSeqColumn 'CFGOpportunityStageDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
--EXECUTE SetSeqColumn 'CFGOpportunityTypeDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'FW_CFGPhoneFormatDesc','Description', 'PKey', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGPrefixDescriptions','Prefix', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGPrimarySpecialtyDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGProbabilityDescriptions','Probability', 'Probability', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGProjectCodeDescriptions','Description', 'ProjectCode', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGProjectTypeDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGProposalSourceDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGProposalStatusDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGProposalTypeDescriptions', 'Type', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGPRResponsibilityDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGResumeCategoryDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGStatesDescriptions','Description', 'ISOCountryCode', 'Code', 'UICultureName', @UICultureName
EXECUTE SetSeqColumn 'CFGSuffixDescriptions','Suffix', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGUnitTypeDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGVendorRoleDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGCitizenshipDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGEMLocationDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGEMRegistrationStatusDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName
EXECUTE SetSeqColumn 'CFGProjectTemplateDescriptions','Description', 'Code', 'UICultureName', '', @UICultureName

END
GO
