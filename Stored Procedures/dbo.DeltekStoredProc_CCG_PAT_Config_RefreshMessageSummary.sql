SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_RefreshMessageSummary]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT ToList, MIN(CreateDateTime), COUNT(*)
        FROM CCG_Email_Messages
        WHERE SourceApplication = 'Payables Approval Tracking'
        GROUP BY ToList
        ORDER BY 1
END;

GO
