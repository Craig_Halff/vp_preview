SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectBillingTermsLimitAboveCM]
   @WBS1        varchar(32), 
   @WBS2        varchar(7), 
   @WBS3        varchar(7),
   @LimitAtLevelAbove varchar (1)
AS
/*
Copyright (c) 2017 Central Consulting Group. All rights reserved.
07/12/2017 David Springer
           Update billing terms
		   Added to spCCG_ProjectBillingTermsUpdateCM2
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
BEGIN
-- Phase level
   If @WBS2 <> ' ' and @WBS3 = ' '
      Begin
      Update ProjectCustomTabFields
	  Set CustLimitatLevelAbove = @LimitAtLevelAbove
	  Where WBS1 = @WBS1
		and WBS2 <> @WBS2
	    and CustBillingType like 'Cost Plus Max%'

      If @LimitAtLevelAbove = 'Y'
	     Begin
      -- Update project level billing terms if Limit at Level Above is checked
         Update b
         Set b.LimitMeth = '3',
	         b.LimitsBySubLevel = 'N',  -- limit by phase
	         b.LabLimit = IsNull (f.Labor, 0),
             b.ConLimit = IsNull (f.Cons, 0),
             b.ExpLimit = IsNull (f.Exp, 0)
         From BT b
			Left Join
			 (Select d.WBS1, 
					 Sum (d.FeeDirLab + d.FeeDirExp + d.ConsultFee) Labor,
					 Sum (d.ReimbAllowCons) Cons,
					 Sum (d.ReimbAllowExp) Exp
			  From Contracts c, ContractDetails d, ProjectCustomTabFields px
			  Where c.WBS1 = d.WBS1
				and c.ContractNumber = d.ContractNumber
				and c.FeeIncludeInd = 'Y'
				and d.WBS1 = px.WBS1
				and d.WBS2 = px.WBS2
				and d.WBS3 = px.WBS3
				and px.CustBillingType like 'Cost Plus Max%'
				and px.CustLimitatLevelAbove = 'Y'
			 Group by d.WBS1) f on b.WBS1 = f.WBS1
         Where b.WBS1 = @WBS1
           and b.WBS2 = ' ' -- project level

      -- Update phase level billing terms if Limit at Level Above is checked
         Update b
         Set b.LimitMeth = '0',
	         b.LimitsBySubLevel = 'Y',  -- limit by phase
	         b.LabLimit = 0,
             b.ConLimit = 0,
             b.ExpLimit = 0
         From BT b, ProjectCustomTabFields px
		 Where b.WBS1 = @WBS1
           and b.WBS2 <> ' ' -- phase level
           and b.WBS3 = ' '
           and b.WBS1 = px.WBS1
		   and b.WBS2 = px.WBS2
		   and b.WBS3 = px.WBS3
		   and px.CustBillingType like 'Cost Plus Max%'
		   and px.CustLimitatLevelAbove = 'Y'

         End

      If @LimitAtLevelAbove = 'N'
	     Begin
         Update b
         Set b.LimitMeth = '0',
	         b.LimitsBySubLevel = 'Y',
	         b.LabLimit = 0,
             b.ConLimit = 0,
             b.ExpLimit = 0
         From BT b
         Where b.WBS1 = @WBS1
           and b.WBS2 = ' ' -- project level

         Update b
         Set b.LimitMeth = '1',
	         b.LimitsBySubLevel = 'N',
	         b.LabLimit = IsNull (f.Labor, 0),
             b.ConLimit = IsNull (f.Cons, 0),
             b.ExpLimit = IsNull (f.Exp, 0)
         From BT b
			Left Join
			 (Select d.WBS1, d.WBS2,
					 Sum (d.FeeDirLab + d.FeeDirExp + d.ConsultFee) Labor,
					 Sum (d.ReimbAllowCons) Cons,
					 Sum (d.ReimbAllowExp) Exp
			  From Contracts c, ContractDetails d, ProjectCustomTabFields px
			  Where c.WBS1 = d.WBS1
				and c.ContractNumber = d.ContractNumber
				and c.FeeIncludeInd = 'Y'
				and d.WBS1 = px.WBS1
				and d.WBS2 = px.WBS2
				and d.WBS3 = px.WBS3
				and px.CustBillingType like 'Cost Plus Max%'
				and px.CustLimitatLevelAbove = 'N'
			 Group by d.WBS1, d.WBS2) f on b.WBS1 = f.WBS1 and b.WBS2 = f.WBS2
         Where b.WBS1 = @WBS1
           and b.WBS2 <> ' '  -- phase level
           and b.WBS3 = ' '
        End
      End -- Phase level

-- Task level
   If @WBS3 <> ' '
      Begin
      Update ProjectCustomTabFields
	  Set CustLimitatLevelAbove = @LimitAtLevelAbove
	  Where WBS1 = @WBS1
		and WBS2 = @WBS2
		and WBS3 <> @WBS3
	    and CustBillingType like 'Cost Plus Max%'

      If @LimitAtLevelAbove = 'Y'
	     Begin
      -- Update phase level billing terms if Limit at Level Above is checked
         Update b
         Set b.LimitMeth = '3',
	         b.LimitsBySubLevel = 'N',  -- limit by task
	         b.LabLimit = IsNull (f.Labor, 0),
             b.ConLimit = IsNull (f.Cons, 0),
             b.ExpLimit = IsNull (f.Exp, 0)
         From BT b
			Left Join
			 (Select d.WBS1, d.WBS2, 
					 Sum (d.FeeDirLab + d.FeeDirExp + d.ConsultFee) Labor,
					 Sum (d.ReimbAllowCons) Cons,
					 Sum (d.ReimbAllowExp) Exp
			  From Contracts c, ContractDetails d, ProjectCustomTabFields px
			  Where c.WBS1 = d.WBS1
				and c.ContractNumber = d.ContractNumber
				and c.FeeIncludeInd = 'Y'
				and d.WBS1 = px.WBS1
				and d.WBS2 = px.WBS2
				and d.WBS3 = px.WBS3
				and px.CustBillingType like 'Cost Plus Max%'
				and px.CustLimitatLevelAbove = 'Y'
			 Group by d.WBS1, d.WBS2) f on b.WBS1 = f.WBS1 and b.WBS2 = f.WBS2
         Where b.WBS1 = @WBS1
           and b.WBS2 = @WBS2
           and b.WBS3 = ' ' -- phaselevel

      -- Update task level billing terms if Limit at Level Above is checked
         Update b
         Set b.LimitMeth = '0',
	         b.LimitsBySubLevel = 'Y',  -- limit by phase
	         b.LabLimit = 0,
             b.ConLimit = 0,
             b.ExpLimit = 0
         From BT b, ProjectCustomTabFields px
		 Where b.WBS1 = @WBS1
		   and b.WBS2 = @WBS2
           and b.WBS3 <> ' ' -- task level
           and b.WBS1 = px.WBS1
		   and b.WBS2 = px.WBS2
		   and b.WBS3 = px.WBS3
		   and px.CustBillingType like 'Cost Plus Max%'
		   and px.CustLimitatLevelAbove = 'Y'

         End

      If @LimitAtLevelAbove = 'N'
	     Begin
         Update b
         Set b.LimitMeth = '0',
	         b.LimitsBySubLevel = 'Y',
	         b.LabLimit = 0,
             b.ConLimit = 0,
             b.ExpLimit = 0
         From BT b
         Where b.WBS1 = @WBS1
           and b.WBS2 = @WBS2
           and b.WBS3 = ' ' -- phase level
         End

         Update b
         Set b.LimitMeth = '1',
	         b.LimitsBySubLevel = 'N',
	         b.LabLimit = IsNull (f.Labor, 0),
             b.ConLimit = IsNull (f.Cons, 0),
             b.ExpLimit = IsNull (f.Exp, 0)
         From BT b
			Left Join
			 (Select d.WBS1, d.WBS2, d.WBS3,
					 Sum (d.FeeDirLab + d.FeeDirExp + d.ConsultFee) Labor,
					 Sum (d.ReimbAllowCons) Cons,
					 Sum (d.ReimbAllowExp) Exp
			  From Contracts c, ContractDetails d, ProjectCustomTabFields px
			  Where c.WBS1 = d.WBS1
				and c.ContractNumber = d.ContractNumber
				and c.FeeIncludeInd = 'Y'
				and d.WBS1 = px.WBS1
				and d.WBS2 = px.WBS2
				and d.WBS3 = px.WBS3
				and px.CustBillingType like 'Cost Plus Max%'
				and px.CustLimitatLevelAbove = 'N'
			 Group by d.WBS1, d.WBS2, d.WBS3) f on b.WBS1 = f.WBS1 and b.WBS2 = f.WBS2 and b.WBS3 = f.WBS3
         Where b.WBS1 = @WBS1
		   and b.WBS2 = @WBS2
           and b.WBS3 <> ' '  -- task level

      End -- Task level
END
GO
