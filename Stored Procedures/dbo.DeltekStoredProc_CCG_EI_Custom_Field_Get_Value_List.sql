SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Custom_Field_Get_Value_List]
	@visionMajorVersion		int = 7,--remove
	@VISION_LANGUAGE		varchar(10),
	@DatabaseField			varchar(500)
AS
BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_EI_Custom_Field_Get_Value_List] 7, 'en-US', 'CustReasonNotInvoiced'
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @sSQL varchar(max);
	DECLARE @safeSql	bit;

	-- Ensure that our inputs are safe (prevent SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @VISION_LANGUAGE);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @DatabaseField);
	IF @safeSql = 0 BEGIN
		SELECT 'ERROR: Unsafe parameters used! Execution cancelled.';
		RETURN;
	END;


	SELECT Code, DataValue
		FROM FW_CustomColumnValuesData
		WHERE InfoCenterArea = 'Projects'
			AND ColName = @DatabaseField
			AND GridID = 'X'
			AND UICultureName =  @VISION_LANGUAGE
		ORDER BY Seq

END;
GO
