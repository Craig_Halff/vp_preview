SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_GetLatestInvoiceForWBS1] ( @WBS1 nvarchar(30))
             AS EXEC spCCG_EI_GetLatestInvoiceForWBS1 @WBS1
GO
