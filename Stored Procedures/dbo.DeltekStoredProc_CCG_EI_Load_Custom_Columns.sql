SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Load_Custom_Columns] ( @VISION_LANGUAGE varchar(10))
             AS EXEC spCCG_EI_Load_Custom_Columns @VISION_LANGUAGE
GO
