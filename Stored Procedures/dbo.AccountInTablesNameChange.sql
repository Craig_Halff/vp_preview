SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AccountInTablesNameChange] @Account Nvarchar(20),@TableName Nvarchar(50) output,@Company Nvarchar(50) output
as
declare
	@BaseStmt		Nvarchar(1000),
	@SqlStmt		Nvarchar(max),
	@WhereClause		Nvarchar(1000),
	@CompanyWhere		Nvarchar(100),
	@BaseStmtNoMC		Nvarchar(1000),
	@WhereClauseNoMC	Nvarchar(1000),
	@MulticompanyEnabled 	varchar(1),
	@Org1Start 		int,
	@Org1Length 		int,
	@DisallowAcctNameChangeWhere		Nvarchar(200)
	
begin 

	SELECT @MulticompanyEnabled = MulticompanyEnabled FROM FW_CFGSystem
	SELECT @Org1Start = Org1Start, @Org1Length = Org1Length FROM CFGFormat	

	set @WhereClauseNoMC = ' (?Account = N''' + @Account + ''') '
	set @BaseStmtNoMC = 'SELECT TOP 1 ''?Table'' as TableName, '' '' AS Company FROM ?Table WHERE ' + @WhereClauseNoMC
	set @TableName = ''
	set @CompanyWhere = ''
	set @DisallowAcctNameChangeWhere = ''

	if rtrim(@Company) <> ''
	begin	
		if len(@Company) > @Org1Length
			set @Company = SUBSTRING (@Company, @Org1Start, @Org1Length)
	end

	if rtrim(@Account) <> '' 
	begin
		-- These tables need to be joined back to PR to figure out the Company column in Multicompany enabled environment
		if rtrim(@TableName) = ''
		begin
  			if @MulticompanyEnabled = 'N'
			begin
				set @BaseStmt = @BaseStmtNoMC
				set @WhereClause = @WhereClauseNoMC
			end
			else
			begin
				if rtrim(@Company) <> ''	
					set @CompanyWhere = ' AND Substring(PR.Org, ' + CONVERT(Nvarchar,@Org1Start) + ',' + CONVERT(Nvarchar,@Org1Length) + ') = N''' + @Company + ''''

				set @DisallowAcctNameChangeWhere = ' AND Substring(PR.Org, ' + CONVERT(Nvarchar,@Org1Start) + ',' + CONVERT(Nvarchar,@Org1Length) + ') IN (SELECT Company FROM CFGMainData WHERE DisallowAccountNameChange = ''Y'') '

				set @WhereClause = ' (?Account = N''' + @Account + '''' + @CompanyWhere + @DisallowAcctNameChangeWhere + ') '
				set @BaseStmt = 'SELECT TOP 1 ''?Table'' as TableName, Substring(PR.Org, ' 
				set @BaseStmt = @BaseStmt + CONVERT(Nvarchar,@Org1Start) + ',' + CONVERT(Nvarchar,@Org1Length) + ') As Company '
				set @BaseStmt = @BaseStmt + ' FROM ?Table LEFT JOIN PR ON ?Table.WBS1 = PR.WBS1 AND ?Table.WBS2 = PR.WBS2 AND ?Table.WBS3 = PR.WBS3 WHERE ' + @WhereClause
			end

			set @SqlStmt = 'declare ChkCursor cursor for SELECT top 1 TableName, Company FROM ('
			set @SqlStmt = @SqlStmt + replace(replace(@BaseStmt,'?Table','apDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','billINDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','billUnitDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','cdDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','crDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','cvDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','ekDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','exDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','inDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','jeDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','miDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','prDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','unDetail'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','upDetail'),'?Account','Account')

			set @SqlStmt = @SqlStmt + ') as TableCheck'

			execute (@SqlStmt)
			open ChkCursor
			fetch ChkCursor into @TableName, @Company
			close ChkCursor
			deallocate ChkCursor
		end

		-- JOIN back to PR to figure out the Company column; make this a secondary cursor only if the previous cursors return nothing!
		if rtrim(@TableName) = ''
		begin
			if @MulticompanyEnabled = 'N'
			begin
				set @BaseStmt = @BaseStmtNoMC
				set @WhereClause = @WhereClauseNoMC
			end
			else
			begin
				if rtrim(@Company) <> ''	
					set @CompanyWhere = ' AND Substring(PR.Org, ' + CONVERT(Nvarchar,@Org1Start) + ',' + CONVERT(Nvarchar,@Org1Length) + ') = N''' + @Company + ''''

				set @DisallowAcctNameChangeWhere = ' AND Substring(PR.Org, ' + CONVERT(Nvarchar,@Org1Start) + ',' + CONVERT(Nvarchar,@Org1Length) + ') IN (SELECT Company FROM CFGMainData WHERE DisallowAccountNameChange = ''Y'') '

				set @WhereClause = ' (?Account = N''' + @Account + '''' + @CompanyWhere + @DisallowAcctNameChangeWhere + ') '
				set @BaseStmt = 'SELECT TOP 1 ''?Table'' as TableName, Substring(PR.Org, ' 
				set @BaseStmt = @BaseStmt + CONVERT(Nvarchar,@Org1Start) + ',' + CONVERT(Nvarchar,@Org1Length) + ') As Company '
				set @BaseStmt = @BaseStmt + ' FROM ?Table LEFT JOIN PR ON ?Table.WBS1 = PR.WBS1 AND ?Table.WBS2 = PR.WBS2 AND ?Table.WBS3 = PR.WBS3 WHERE ' + @WhereClause
			end

			set @SqlStmt = 'declare ChkCursor cursor for SELECT top 1 TableName, Company FROM ('
			set @SqlStmt = @SqlStmt + replace(replace(@BaseStmt,'?Table','BIED'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerAP'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerAR'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerEX'),'?Account','Account')
			set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerMisc'),'?Account','Account')
  			set @SqlStmt = @SqlStmt + ') as TableCheck'

			execute (@SqlStmt)
			open ChkCursor
			fetch ChkCursor into @TableName, @Company
			close ChkCursor
			deallocate ChkCursor
		end
	end
end -- AccountInTablesNameChange  
/* sp_AddCarriageReturns.SQL */
/* Stored procedure fixes varchar fields by adding CHAR(13) whenever a CHAR(10) is found in */
/* all varchar fields greater than or equal to 50 characters */
/* now check for nvarchar or varchar fields. */
/* Last Modified : DPB 06/125/08 */

Print ' '
Print 'Drop AddCarriageReturns procedure (if it exists)...'
GO
