SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_SaveStage] ( @wbs1 nvarchar(32), @oldStage nvarchar(50), @newStage nvarchar(50), @newStageVerified varchar(50), @employee nvarchar(32), @delegateFor nvarchar(32), @role varchar(32), @stageReRoutedFrom nvarchar(50)= null)
             AS EXEC spCCG_EI_SaveStage @wbs1,@oldStage,@newStage,@newStageVerified,@employee,@delegateFor,@role,@stageReRoutedFrom
GO
