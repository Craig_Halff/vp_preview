SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[spCCG_OpportunitySalesValidation] @OpportunityID varchar (32)
AS
/*
Copyright 2017 Central Consulting Group.   All rights reserved.
03/14/2017 David Springer
           Validate that the Sales grid matches the Total Revenue.
           Call this from an Opportunity CHANGE workflow when Create Regular Project is checked.
*/
SET NOCOUNT ON
BEGIN
   If exists (
   Select 'x'
   From Opportunity o
        Left Join
	   (Select OpportunityID, Sum (CustSalesAmount) Sales
	    From Opportunities_Sales
		Group by OpportunityID) s on s.OpportunityID = o.OpportunityID
   Where o.OpportunityID = @OpportunityID
     and o.Revenue <> IsNull (s.Sales, 0)
	         )
			 RAISERROR ('Sales grid Amount must equal Total Revenue.                        ', 16, 1)
END
GO
