SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/* 
	Copyright (c) 2021 Halff Associates, Inc.  All rights reserved.
	3/3/2021	Craig H. Anderson

	Send ESOP Welcome notifications to newly eligible ESOP participants.

	
	- Run list by Susie each month before sending.
*/

CREATE   PROCEDURE [dbo].[HAI_ESOP_Notice] @TrialRun VARCHAR(1) = 'Y'
AS
    BEGIN
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
        SET NOCOUNT ON;
        DECLARE @CSS                     VARCHAR(MAX)
              , @bodyHTML                VARCHAR(MAX)
              , @employee                VARCHAR(10)
              , @firstName               VARCHAR(25)
              , @empEmail                VARCHAR(50)
              , @empLegacysource         VARCHAR(25)
              , @thisYear                VARCHAR(4)
              , @lastYear                VARCHAR(4)
              , @previousYear            VARCHAR(4) -- Two years ago
              , @nextYear                VARCHAR(4)
              , @followingYear           VARCHAR(4) -- Two years from now
              , @previousYearStockPrice  VARCHAR(8)
              , @previousYearPctIncrease VARCHAR(6)
              , @thisQuarter             VARCHAR(7)
              , @myTrustee               VARCHAR(30)
              , @firstOfThisMonth        DATE;
        SET @thisYear = CAST(DATEPART(YEAR, GETDATE()) AS VARCHAR(4));
        SET @lastYear = CAST(CAST(@thisYear AS INT) - 1 AS VARCHAR(4));
        SET @previousYear = CAST(CAST(@lastYear - 1 AS INT) AS VARCHAR(4));
        SET @nextYear = CAST(CAST(@thisYear AS INT) + 1 AS VARCHAR(4));
        SET @followingYear = CAST(CAST(@thisYear AS INT) + 2 AS VARCHAR(4));
        SET @previousYearStockPrice = CAST((
                                          SELECT CustStockPrice FROM dbo.UDIC_Metadata_StockPrices WHERE CustYear = @previousYear
                                      ) AS VARCHAR(10));
        SET @previousYearStockPrice = LEFT(@previousYearStockPrice, PATINDEX('%.%', @previousYearStockPrice) + 2);
        SET @previousYearPctIncrease = CAST((
                                           SELECT CustPctChangeOverPrevious FROM dbo.UDIC_Metadata_StockPrices WHERE CustYear = @previousYear
                                       ) AS VARCHAR(10));
        SET @previousYearPctIncrease = LEFT(@previousYearPctIncrease, PATINDEX('%.%', @previousYearPctIncrease) + 1);
        SET @thisQuarter = CASE
                               WHEN DATEPART(MONTH, GETDATE()) IN ( 1, 2, 3 )
                                   THEN
                                   CAST(DATEPART(YEAR, GETDATE()) AS VARCHAR(4)) + ' Q1'
                               WHEN DATEPART(MONTH, GETDATE()) IN ( 4, 5, 6 )
                                   THEN
                                   CAST(DATEPART(YEAR, GETDATE()) AS VARCHAR(4)) + ' Q2'
                               WHEN DATEPART(MONTH, GETDATE()) IN ( 7, 8, 9 )
                                   THEN
                                   CAST(DATEPART(YEAR, GETDATE()) AS VARCHAR(4)) + ' Q3'
                               ELSE
                                   CAST(DATEPART(YEAR, GETDATE()) AS VARCHAR(4)) + ' Q4'
                           END;
        SET @firstOfThisMonth = CAST(CONCAT(CAST(DATEPART(YEAR, GETDATE()) AS VARCHAR(4)) + '-' + CAST(DATEPART(MONTH, GETDATE()) AS VARCHAR(2)), '-01') AS DATE);
        PRINT @firstOfThisMonth;
        DECLARE @myRand INT;
        DECLARE @trustees TABLE (
            myRand INT
          , myEmail VARCHAR(30)
        );
        INSERT INTO @trustees (myRand, myEmail) VALUES (0, 'tMurray@Halff.com');
        INSERT INTO @trustees (myRand, myEmail) VALUES (1, 'tJackson@Halff.com');
        INSERT INTO @trustees (myRand, myEmail) VALUES (2, 'RKillen@Halff.com');
        INSERT INTO @trustees (myRand, myEmail) VALUES (3, 'tMurray@Halff.com');


        -- ##############################################################################
        -- Calculate the Eligibility Year - First year they worked more than 1,000 hours
        -- ##############################################################################
        IF OBJECT_ID('tempdb..#ESOP_Eligibility', 'U') IS NOT NULL
            BEGIN
                DROP TABLE #ESOP_Eligibility;
            END;
        SELECT e.Employee
             , e2.HoursWorkedThisYear
             , e1.HoursWorkedFirstTwelveMonths
             , e.HireDate
            INTO #ESOP_Eligibility
            FROM dbo.EM AS e
            LEFT JOIN (
                SELECT ly.Employee
                     , ly.HoursWorkedFirstTwelveMonths
                    FROM (
                        SELECT l.Employee
                             , SUM(l.RegHrs) + SUM(l.OvtHrs) + SUM(l.SpecialOvtHrs) AS HoursWorkedFirstTwelveMonths
                            FROM dbo.LD       AS l WITH (NOLOCK)
                            INNER JOIN dbo.EM AS e3 WITH (NOLOCK)
                                ON l.Employee = e3.Employee
                                    AND e3.Status = 'A'
                                    AND e3.Type <> 'C'
                            WHERE
                            l.Employee IS NOT NULL
                                AND l.TransDate BETWEEN e3.HireDate AND DATEADD(YEAR, 1, e3.HireDate)
                            GROUP BY l.Employee
                    ) ly
            )           e1
                ON e.Employee = e1.Employee
            LEFT JOIN (
                SELECT ly.Employee
                     , ly.HoursWorkedThisYear
                    FROM (
                        SELECT l.Employee
                             , SUM(l.RegHrs) + SUM(l.OvtHrs) + SUM(l.SpecialOvtHrs) AS HoursWorkedThisYear
                            FROM dbo.LD       AS l WITH (NOLOCK)
                            INNER JOIN dbo.EM AS e3 WITH (NOLOCK)
                                ON l.Employee = e3.Employee
                                    AND e3.Status = 'A'
                                    AND e3.Type <> 'C'
                            WHERE
                            l.Employee IS NOT NULL
                                AND DATEPART(YEAR, l.TransDate) = CAST(@thisYear AS INT)
                            GROUP BY l.Employee
                    ) ly
            )           e2
                ON e.Employee = e2.Employee
            WHERE
            e.Status = 'A'
                AND e.Type <> 'C'
                AND e.HomeCompany = '01';
        /*##############################################################################
		  Identify Eligible employees based on
			â€¢	Halff Employees Only
			â€¢	Not a Contract employee type
			â€¢	Currently Active employee (do not send to termed employees)
			â€¢	Not an ESOP participant in prior year
			â€¢	Worked 1,000 hours in their first year of employment (within 1 year from hire date)
				or worked 1,000 hours in the current calendar year
			â€¢	Reached 1 year of service on or prior the first of current month.
			â€¢	First 5 emails will come from Trey Murray, next 10 emails will be split between Todd and Russell.
				Repeat pattern until all emails are sent for current month.
			â€¢	BCC only the ESOP trustee sending the email
			â€¢	Current Quarter > anniversary Quarter
		###############################################################################*/
        IF OBJECT_ID('tempdb..#EmployeeList', 'U') IS NOT NULL
            BEGIN
                DROP TABLE #EmployeeList;
            END;

        SELECT s.Employee
             , ISNULL(s.PreferredName, s.FirstName) AS Name
             , s.EMail
             , s.SimpleName
             , s.HireDate
             , s.CustLegacySource
             , ee.HoursWorkedFirstTwelveMonths
             , ee.HoursWorkedThisYear
             , eq.eligibleQuarterDate
            INTO #EmployeeList
            FROM DataMart.dbo.Staff           AS s
            LEFT OUTER JOIN #ESOP_Eligibility ee
                ON s.Employee = ee.Employee
            LEFT OUTER JOIN (
                SELECT e.Employee
                     , CASE
                           WHEN DATENAME(QUARTER, DATEADD(YEAR, 1, e.HireDate)) = '1'
                               THEN
                               CAST(CAST(DATEPART(YEAR, DATEADD(YEAR, 1, e.HireDate)) AS VARCHAR(4)) + '0401' AS DATE)
                           WHEN DATENAME(QUARTER, DATEADD(YEAR, 1, e.HireDate)) = '2'
                               THEN
                               CAST(CAST(DATEPART(YEAR, DATEADD(YEAR, 1, e.HireDate)) AS VARCHAR(4)) + '0701' AS DATE)
                           WHEN DATENAME(QUARTER, DATEADD(YEAR, 1, e.HireDate)) = '3'
                               THEN
                               CAST(CAST(DATEPART(YEAR, DATEADD(YEAR, 1, e.HireDate)) AS VARCHAR(4)) + '1001' AS DATE)
                           WHEN DATENAME(QUARTER, DATEADD(YEAR, 1, e.HireDate)) = '4'
                               THEN
                               CAST(CAST(DATEPART(YEAR, DATEADD(YEAR, 1, e.HireDate)) AS VARCHAR(4)) + '0101' AS DATE)
                           ELSE
                               NULL
                       END AS eligibleQuarterDate
                    FROM dbo.EM AS e WITH (NOLOCK)
                    WHERE e.Status = 'A'
            )                                 eq
                ON s.Employee = eq.Employee
            WHERE
            s.HomeCompany = '01'
                AND s.Type <> 'C'
                AND s.Status = 'A'
                AND NOT EXISTS (
                SELECT 1
                    FROM dbo.Employees_RoleLog
                    WHERE
                    Employee = s.Employee
                        AND (
                            CustRole = 'ESOP Participant'
                                OR CustAction = 'Notice Sent'
                        )
            )
                AND DATEADD(YEAR, 1, s.HireDate) < @firstOfThisMonth
                AND (
                    ee.HoursWorkedFirstTwelveMonths >= 1000
                        OR ee.HoursWorkedThisYear >= 1000
                )
                AND GETDATE() >= eq.eligibleQuarterDate;


        DECLARE @eligibleStaff INT = (
                    SELECT COUNT(*)FROM #EmployeeList AS el
                );
        IF ISNULL(@eligibleStaff, 0) > 0
            BEGIN
                SET @CSS =
                    '</p><br>
<style type="text/css">
	@page{size:letter portrait;}
	body p:first-child {display:none;}
    table {width:450px;border-collapse: collapse;margin-left:.25in;}
	#envelope {font-family: Arial, sans-serif;width:700px; margin:0 0 0 0;}
	p {padding-bottom:10pt;font-family: Arial, sans-serif;padding-top:0;margin-top:0;mso-line-height-rule:exactly; line-height;1.8;}
	td {padding-left: 5px;padding-right:5px;font-family:Arial, sans-serif; border:1pt solid black;}
	thead tr th {font-weight:400;text-align:left;background-color:#365F91; color:#ffffff;border:1pt solid black;}
	.aLeft {text-align:left;}
	.aCenter {text-align:center;}
	.aRight {text-align:right;}
	.Table_Header tr th {background-color: #4472c4;color: white;}
	.Project_Header {background-color:#CCCCCC; color:black;}
    caption {color:white;font-size: 1.25em;}
	h3 {margin-bottom:0;padding-bottom:0;font-size: 1.1em; font-family:Arial, sans-serif;}
	#esopDiagram {width:100%;clear:both;}
</style>
'               ;

                /* declare variables */
                DECLARE @variable INT;

                DECLARE emp_cursor CURSOR FAST_FORWARD READ_ONLY FOR
                    SELECT el.Employee, el.Name, el.EMail, el.CustLegacySource FROM #EmployeeList AS el;
                --WHERE el.Name LIKE 'Craig';

                OPEN emp_cursor;

                FETCH NEXT FROM emp_cursor
                INTO @employee
                   , @firstName
                   , @empEmail
                   , @empLegacysource;

                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        --------------------------------------------------------------------------



                        SET @bodyHTML = '<div id="envelope">
		<!--[if mso]><div style="width:7in;"><![endif]-->
		<p>Hi, ' +      @firstName + '.</p>';

                        SET @bodyHTML =
                            @bodyHTML
                            + '<p>Congratulations on reaching your one-year service anniversary at Halff! That is an
					exciting milestone, and it means you will automatically enter Halffâ€™s Employee Stock Ownership Plan (ESOP) on ' + DATENAME(MONTH, CAST(@firstOfThisMonth AS DATE)) + ' '
                            + DATENAME(DAY, CAST(@firstOfThisMonth AS DATE)) + ', ' + DATENAME(YEAR, CAST(@firstOfThisMonth AS DATE)) + '.<p>';

                        SET @bodyHTML =
                            @bodyHTML
                            + '	<h3>WHAT IS AN ESOP?</h3>
					<p>An employee stock ownership plan (ESOP) is a benefit plan that enables employees to own part of Halff Associates.
					Employees need to meet certain eligibility requirements to become an ESOP participant. Once in the ESOP, participants
					own shares of company stock that are valued every year and accumulate in their ESOP retirement account. When
					employees separate from the company, they sell their shares back to the ESOP.</p>';

                        SET @bodyHTML = @bodyHTML + '<h3>ELIGIBILITY</h3>';
                        SET @bodyHTML =
                            @bodyHTML
                            + '<p>Halff employees are eligible to enter the plan after working 1,000 hours in their first year of service.
			  They will enter at the beginning of the quarter following their anniversary date.  For example, if the hire
			  date was Jan. 4, '        + @thisYear + ', the employee entered the plan April 1, ' + @nextYear + '. If their hire date
			  is April 1, '             + @thisYear + ', the employee also entered the plan April 1, ' + @nextYear
                            + '. 
			  Entering the ESOP means an employee is now eligible to receive company stock through the ESOP based on
			  established schedules for stock valuation and ESOP distributions. </p>';
                        SET @bodyHTML =
                            @bodyHTML
                            + '
				<table id="enddates">
					<thead>
						<tr><th colspan="2">Quarterly Entry Dates</th></tr>
					</thead>
					<tbody>
						<tr><td>Q1 - Jan. 1</td><td>Q3 - July 1</td></tr>
						<tr><td>Q2 - April 1</td><td>Q4 - Oct. 1</td></tr>
					<tbody>
				</table><br>
				'       ;

                        SET @bodyHTML = @bodyHTML + '<h3>TIMELINE OF STATEMENT PREPARATION</h3>';
                        SET @bodyHTML =
                            @bodyHTML
                            + '<p>The monetary value afforded by the ESOP is often under-recognized by participants
					due to the length of time between entering the ESOP and the schedule for evaluating
					Halffâ€™s annual stock price, subsequent update to individual ESOP account information and
					the distribution of ESOP statements. Please note the typical ESOP timeline below relates
					to accounts for the previous calendar year.</p>';


                        SET @bodyHTML =
                            @bodyHTML
                            + '<table id="timeline"><thead><tr><th colspan="2">Typical ESOP Timeline<br><span style="font-size:0.875em;">(Activities to Determine Previous Yearâ€™s Value and Distribution)</span></th></tr></thead><tbody>';
                        SET @bodyHTML = @bodyHTML + '<tr><td>February - May</td><td>Reconciliation of ESOP accounts</td></tr>';
                        SET @bodyHTML = @bodyHTML + '<tr><td>End of May</td><td>Stock price determined</td></tr>';
                        SET @bodyHTML = @bodyHTML + '<tr><td>July</td><td>Preparation of statements</td></tr>';
                        SET @bodyHTML = @bodyHTML + '<tr><td>August</td><td>Distribution of statements</td></tr>';
                        SET @bodyHTML = @bodyHTML + '<tbody></table>';

                        SET @bodyHTML =
                            @bodyHTML
                            + '<br><p>There is often misunderstanding about this waiting period, so it may help to reference the example given previously for
			  a person starting work at Halff on Jan. 4, ' + @thisYear + '.</p>
			  <ul>
			  <li>This employee enters the ESOP plan on April 1, ' + @nextYear
                            + '.</li>
			  <li>Since the ESOP contribution occurs only once per year on the last day of the year, the effective date of the first contribution
			  for this employee would not occur until Dec. 31, ' + @nextYear
                            + '.</li>
			  <li>However, this contribution cannot be finalized until the final account reconciliation occurs and the stock price is determined in mid-' + @followingYear
                            + '.</li>
			  <li>The new ESOP member will ultimately receive this information in August ' + @followingYear
                            + '.</li>
			  </ul>
			  <p>The process takes some timeâ€”especially in the first yearâ€”but the plan provides great assistance in working with your
			  401(k) and personal investments to prepare for retirement.</p>';

                        SET @bodyHTML = @bodyHTML + '<h3>VESTING</h3>';
                        SET @bodyHTML =
                            @bodyHTML + '<p>Vesting is calculated based the number of hours worked in a calendar year.  For example, if an employee was hired on April 1, ' + @thisYear
                            + ', and works 1,000 hours by
			  Dec. 31, '                + @thisYear + ', that employee earned one year of vesting for ' + @thisYear + '. In ' + @nextYear
                            + ', if 1,000 additional hours were worked, they have two years of vesting. Prior years of experience with companies Halff has acquired count toward an employeeâ€™s ESOP vesting.';


                        SET @bodyHTML =
                            @bodyHTML
                            + ' The vesting schedule is as follows:</p>
			<ul>
				<li>One year = 20 percent vested</li>
				<li>Two years = 40 percent vested</li>
				<li>Three years = 60 percent vested</li>
				<li>Four years = 80 percent vested </li>
				<li>Five years = 100 percent vested</li>
			</ul>'      ;

                        SET @bodyHTML = @bodyHTML + '<h3>HISTORY OF HALFF STOCK PRICE</h3>';
                        SET @bodyHTML =
                            @bodyHTML + '<p>Halffâ€™s ESOP participants have directly benefited from Halffâ€™s continued growth. The stock valuation of ' + @previousYearStockPrice
                            + ' per share for the year ending ' + @previousYear + ' was a ' + @previousYearPctIncrease
                            + ' percent increase from the previous yearâ€™s valuation. Below is the stock price for the past three years.</p>';


                        SET @bodyHTML = @bodyHTML + '<table id="enddates"><thead><tr><th colspan="2">Three-Year History of Halffâ€™s Stock Price</th></tr></thead><tbody>';
                        /* declare variables */
                        DECLARE @stockYear        VARCHAR(4)
                              , @esopYear         VARCHAR(4)
                              , @stockPrice       MONEY
                              , @esopContribution VARCHAR(8);

                        -- ####################################################
                        -- Stock Prices cursor for stock price table
                        -- ####################################################

                        DECLARE stock_cursor CURSOR FAST_FORWARD READ_ONLY FOR
                            SELECT a.CustYear
                                 , a.CustStockPrice
                                FROM (
                                    SELECT TOP (3)
                                           msp.CustYear
                                         , msp.CustStockPrice
                                        FROM dbo.UDIC_Metadata_StockPrices AS msp
                                        ORDER BY msp.CustYear DESC
                                ) a
                                ORDER BY a.CustYear;
                        OPEN stock_cursor;

                        FETCH NEXT FROM stock_cursor
                        INTO @stockYear
                           , @stockPrice;
                        WHILE @@FETCH_STATUS = 0
                            BEGIN
                                SET @bodyHTML = @bodyHTML + '<tr><td>Dec. 31, ' + @stockYear + '</td><td>$' + CAST(@stockPrice AS VARCHAR(12)) + '</td></tr>';
                                FETCH NEXT FROM stock_cursor
                                INTO @stockYear
                                   , @stockPrice;
                            END;
                        CLOSE stock_cursor;
                        DEALLOCATE stock_cursor;
                        SET @bodyHTML = @bodyHTML + '</tbody></table><br>';

                        SET @bodyHTML = @bodyHTML + '<h3>HISTORY OF COMPANY CONTRIBUTIONS</h3>';
                        SET @bodyHTML =
                            @bodyHTML
                            + '<p>You will receive a company contribution if you have worked 1,000 hours during the calendar
					year and are employed on the last day of the plan year (Dec. 31). Exceptions to this are disability,
					death or normal retirement age of 65. If you have reached age 65 and have been employed for at least
					five years, you do not have to be employed on the last day of the plan year to receive a company
					contribution. Individual contributions are based on each ESOP memberâ€™s total compensation. Total
					compensation is comprised of all forms of earnings received through payroll. This can include, but
					is not limited to, regular pay, overtime, salary, holidays, paid time off, awards and bonuses.<p>';

                        -- ####################################################
                        -- ESOP Contribution Table
                        -- ####################################################
                        SET @bodyHTML = @bodyHTML + '<table id="contributions"><thead><tr><th colspan="2">Three-Year History of Halffâ€™s Contribution to ESOP Accounts</th></tr></thead><tbody>';
                        DECLARE esop_cursor CURSOR FAST_FORWARD READ_ONLY FOR
                            SELECT a.CustYear
                                 , a.CustPctESOPContribution
                                FROM (
                                    SELECT TOP (3)
                                           me.CustYear
                                         , me.CustPctESOPContribution
                                        FROM dbo.UDIC_Metadata_ESOP AS me
                                        ORDER BY me.CustYear DESC
                                ) a
                                ORDER BY a.CustYear;
                        OPEN esop_cursor;

                        FETCH NEXT FROM esop_cursor
                        INTO @esopYear
                           , @esopContribution;
                        WHILE @@FETCH_STATUS = 0
                            BEGIN
                                SET @bodyHTML = @bodyHTML + '<tr><td>Dec. 31, ' + @esopYear + '</td><td>' + @esopContribution + ' of total compensation<sup>*</sup></td></tr>';
                                FETCH NEXT FROM esop_cursor
                                INTO @esopYear
                                   , @esopContribution;
                            END;
                        CLOSE esop_cursor;
                        DEALLOCATE esop_cursor;

                        SET @bodyHTML = @bodyHTML + '</tbody>';
                        SET @bodyHTML = @bodyHTML + '</table>';
                        SET @bodyHTML =
                            @bodyHTML
                            + '<div style="margin-left:.25in; font-style:italic;font-size:9pt;clear:both;">* Subject to IRS total compensation limits for evaluating
			  company contributions.</div>';

                        SET @bodyHTML = @bodyHTML + '<h3>ESOP TRUSTEES</h3>';
                        SET @bodyHTML =
                            @bodyHTML
                            + '<p>The ESOP program includes three trustees who have the fiduciary responsibility to oversee and protect ESOP assets for the benefit
			  of participants. The trustees are Chief Operating Officer Todd Jackson, Executive Vice President Russell Killen, and Senior Vice President
			  and McAllen Operations Manager Trey Murray.</p>';

                        SET @bodyHTML = @bodyHTML + '<p>Please reach out to our Benefits Department at benefits@halff.com if you have any questions.</p>';
                        SET @bodyHTML = @bodyHTML + '<!--[if mso]><div style="width:7in;"><![endif]--></div>';
                        SET @bodyHTML = @bodyHTML + '<p>Sincerely,<p>';

                        -- If this is a trial run, send only to Craig
                        IF @TrialRun = 'N'
                            BEGIN
                                SET @myTrustee = (
                                    SELECT TOP 1 t.myEmail FROM @trustees AS t WHERE t.myRand = ROUND(RAND() * 3, 0)
                                );
                            END;
                        IF @TrialRun = 'Y'
                            BEGIN
                                SET @myTrustee = 'cAnderson@Halff.com';
                                SET @empEmail = @myTrustee + ';jbaummer@halff.com';

                            END;

                        INSERT INTO dbo.CCG_Email_Messages (
                            SourceApplication
                          , Sender
                          , ToList
                          , CCList
                          , BCCList
                          , Subject
                          , GroupSubject
                          , Body
                          , MaxDelay
                          , Priority
                          , CreateDateTime
                        )
                        VALUES
                        (   'Halff Associates ESOP Trustees' -- SourceApplication - varchar(40)
                          , @myTrustee -- Sender - varchar(80)
                          , @empEmail -- ToList - varchar(2000)
                          , '' -- CCList - varchar(2000)
                          , @myTrustee -- BCCList - varchar(2000)
                          , 'Welcome to Halff''s Employee Stock Ownership Plan (ESOP)' -- Subject - varchar(120)
                          , '' -- GroupSubject - varchar(120)
                          , @CSS + @bodyHTML -- Body - varchar(max)
                          , 0 -- MaxDelay - int
                          , '' -- Priority - varchar(12)
                          , GETDATE() -- CreateDateTime - datetime
                            );
                        IF @TrialRun = 'N'
                            BEGIN
                                UPDATE dbo.EmployeeCustomTabFields SET CustRoleESOPParticipant = 'Y' WHERE Employee = @employee;
                                INSERT INTO dbo.Employees_RoleLog (
                                    Employee
                                  , Seq
                                  , CreateUser
                                  , CreateDate
                                  , ModUser
                                  , ModDate
                                  , CustRole
                                  , CustAction
                                  , CustDate
                                )
                                VALUES
                                (   @employee -- Employee - varchar(20)
                                  , REPLACE(NEWID(), '-', '') -- Seq - varchar(32)
                                  , 'ESOP Notice' -- CreateUser - varchar(20)
                                  , GETDATE() -- CreateDate - datetime
                                  , 'ESOP Notice' -- ModUser - varchar(20)
                                  , GETDATE() -- ModDate - datetime
                                  , 'ESOP Participant' -- CustRole - varchar(255)
                                  , 'Notice Sent' -- CustAction - varchar(255)
                                  , GETDATE() -- CustDate - datetime
                                    );
                            END;
                        FETCH NEXT FROM emp_cursor
                        INTO @employee
                           , @firstName
                           , @empEmail
                           , @empLegacysource;
                    END;

                CLOSE emp_cursor;
                DEALLOCATE emp_cursor;
            END;
        ELSE
            BEGIN
                PRINT 'No new eligible employees were found today.';
                INSERT INTO dbo.CCG_Email_Messages (
                    SourceApplication
                  , Sender
                  , ToList
                  , CCList
                  , BCCList
                  , Subject
                  , GroupSubject
                  , Body
                  , MaxDelay
                  , Priority
                  , CreateDateTime
                )
                VALUES
                (   'Halff Associates ESOP Trustees' -- SourceApplication - varchar(40)
                  , 'cAnderson@Halff.com' -- Sender - varchar(80)
                  , 'cAnderson@Halff.com' -- ToList - varchar(2000)
                  , '' -- CCList - varchar(2000)
                  , '' -- BCCList - varchar(2000)
                  , 'ESOP - No newly eligible employees found.' -- Subject - varchar(120)
                  , '' -- GroupSubject - varchar(120)
                  , '<br><p>ESOP - No newly eligible employees found.</p>' -- Body - varchar(max)
                  , 0 -- MaxDelay - int
                  , '' -- Priority - varchar(12)
                  , GETDATE() -- CreateDateTime - datetime
                    );
            END;
    END;

GO
