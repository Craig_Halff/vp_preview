SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_CallSPWithoutParams] (
	@SPName		varchar(200)
) AS
BEGIN
	-- [DeltekStoredProc_CCG_PAT_CallSPWithoutParams] 'DeltekStoredProc_CCG_PAT_Route_Load'
	SET NOCOUNT ON;

	EXEC (@SPName);
END;

GO
