SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RPFixMappings]
AS

BEGIN -- Procedure RPFixMappings

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to fix invalid {WBS1, WBS2, WBS3, LaborCode} mappings in Vision Plans. 
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @ErrorMessage nvarchar(MAX);
  DECLARE @ErrorSeverity int;
  DECLARE @ErrorState int;

  DECLARE @tabUpdated TABLE(
    RowID int IDENTITY(1,1),
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBSType varchar(4) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int,
    Old_WBS1 nvarchar(30) COLLATE database_default,
    Old_WBS2 nvarchar(30) COLLATE database_default,
    Old_WBS3 nvarchar(30) COLLATE database_default,
    Old_LaborCode nvarchar(14) COLLATE database_default,
    New_WBS1 nvarchar(30) COLLATE database_default,
    New_WBS2 nvarchar(30) COLLATE database_default,
    New_WBS3 nvarchar(30) COLLATE database_default,
    New_LaborCode nvarchar(14) COLLATE database_default
    UNIQUE (RowID, PlanID, TaskID, WBSType, OutlineNumber, OutlineLevel)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRY 

    BEGIN TRANSACTION FixWBS1WBS2WBS3LaborCode;

    --######################################################################################################################################################################################################
    
      -- Fix RPTask [intermediate] rows which are ancestors above my branch where I am a WBS1 row.
      -- These RPTask rows must have all NULL {WBS1, WBS2, WBS3, LaborCode}.
      -- No other WBS Mapping setting is valid for this kind of rows

      UPDATE BT SET
        WBS1 = NULL,
        WBS2 = NULL,
        WBS3 = NULL,
        LaborCode = NULL
        OUTPUT
          INSERTED.PlanID,
          INSERTED.TaskID,
          INSERTED.WBSType,
          INSERTED.OutlineNumber,
          INSERTED.OutlineLevel,
          DELETED.WBS1,
          DELETED.WBS2,
          DELETED.WBS3,
          DELETED.LaborCode,
          INSERTED.WBS1,
          INSERTED.WBS2,
          INSERTED.WBS3,
          INSERTED.LaborCode
          INTO @tabUpdated(
            PlanID,
            TaskID,
            WBSType,
            OutlineNumber,
            OutlineLevel,
            Old_WBS1,
            Old_WBS2,
            Old_WBS3,
            Old_LaborCode,
            New_WBS1,
            New_WBS2,
            New_WBS3,
            New_LaborCode
          )
        FROM RPTask AS BT
          INNER JOIN RPTask AS CT
            ON BT.PlanID = CT.PlanID AND CT.OutlineNumber LIKE BT.OutlineNumber + '%' AND BT.WBSType IS NULL
          LEFT JOIN PNPlan AS PN ON BT.PlanID = PN.PlanID /* Select ONLY Vision Plans */
        WHERE BT.TaskID <> CT.TaskID
          AND PN.PlanID IS NULL /* Select ONLY Vision Plans */
          AND CT.WBSType = 'WBS1'
          AND (NOT(BT.WBS1 IS NULL AND BT.WBS2 IS NULL AND BT.WBS3 IS NULL AND BT.LaborCode IS NULL))

      -- Fix RPTask [intermediate] rows in a Plan where there was no WBSType in the entire Plan.
      -- These RPTask rows must have all NULL {WBS1, WBS2, WBS3, LaborCode}.
      -- No other WBS Mapping setting is valid for this kind of rows

      UPDATE BT SET
        WBS1 = NULL,
        WBS2 = NULL,
        WBS3 = NULL,
        LaborCode = NULL
        OUTPUT
          INSERTED.PlanID,
          INSERTED.TaskID,
          INSERTED.WBSType,
          INSERTED.OutlineNumber,
          INSERTED.OutlineLevel,
          DELETED.WBS1,
          DELETED.WBS2,
          DELETED.WBS3,
          DELETED.LaborCode,
          INSERTED.WBS1,
          INSERTED.WBS2,
          INSERTED.WBS3,
          INSERTED.LaborCode
          INTO @tabUpdated(
            PlanID,
            TaskID,
            WBSType,
            OutlineNumber,
            OutlineLevel,
            Old_WBS1,
            Old_WBS2,
            Old_WBS3,
            Old_LaborCode,
            New_WBS1,
            New_WBS2,
            New_WBS3,
            New_LaborCode
          )
        FROM RPTask AS BT
          INNER JOIN RPTask AS CT
            ON BT.PlanID = CT.PlanID AND CT.OutlineNumber LIKE BT.OutlineNumber + '%' AND BT.WBSType IS NULL
          LEFT JOIN PNPlan AS PN ON BT.PlanID = PN.PlanID /* Select ONLY Vision Plans */
        WHERE 
          NOT EXISTS(SELECT 'X' FROM RPTask AS XT WHERE XT.WBSType IS NOT NULL AND XT.PlanID = BT.PlanID)
          AND PN.PlanID IS NULL /* Select ONLY Vision Plans */
          AND (NOT(BT.WBS1 IS NULL AND BT.WBS2 IS NULL AND BT.WBS3 IS NULL AND BT.LaborCode IS NULL))

    --######################################################################################################################################################################################################

      -- Fix RPTask WBS1 rows to ensure that mapping fields are correct.

      UPDATE T SET 
        WBS1 = 
          CASE 
            WHEN (DATALENGTH(LTRIM(RTRIM(ISNULL(T.WBS1, '')))) = 0)
            THEN '<none>'
            ELSE T.WBS1
          END,
        WBS2 = NULL,
        WBS3 = NULL,
        LaborCode = NULL
        OUTPUT
          INSERTED.PlanID,
          INSERTED.TaskID,
          INSERTED.WBSType,
          INSERTED.OutlineNumber,
          INSERTED.OutlineLevel,
          DELETED.WBS1,
          DELETED.WBS2,
          DELETED.WBS3,
          DELETED.LaborCode,
          INSERTED.WBS1,
          INSERTED.WBS2,
          INSERTED.WBS3,
          INSERTED.LaborCode
          INTO @tabUpdated(
            PlanID,
            TaskID,
            WBSType,
            OutlineNumber,
            OutlineLevel,
            Old_WBS1,
            Old_WBS2,
            Old_WBS3,
            Old_LaborCode,
            New_WBS1,
            New_WBS2,
            New_WBS3,
            New_LaborCode
          )
        FROM RPTask AS T
          LEFT JOIN PNPlan AS PN ON T.PlanID = PN.PlanID /* Select ONLY Vision Plans */
        WHERE T.WBSType = 'WBS1'
          AND PN.PlanID IS NULL /* Select ONLY Vision Plans */
          AND (
            (DATALENGTH(LTRIM(RTRIM(ISNULL(T.WBS1, '')))) = 0) OR
            (NOT(T.WBS2 IS NULL AND T.WBS3 IS NULL AND T.LaborCode IS NULL))
          )

      -- Fix RPTask WBS2 rows to ensure that mapping fields are correct.

      UPDATE T SET 
        WBS1 = 
          CASE 
            WHEN (DATALENGTH(LTRIM(RTRIM(ISNULL(T.WBS1, '')))) = 0)
            THEN '<none>'
            ELSE T.WBS1
          END,
        WBS2 = 
          CASE 
            WHEN (DATALENGTH(LTRIM(RTRIM(ISNULL(T.WBS2, '')))) = 0)
            THEN '<none>'
            ELSE T.WBS2
          END,
        WBS3 = NULL,
        LaborCode = NULL
        OUTPUT
          INSERTED.PlanID,
          INSERTED.TaskID,
          INSERTED.WBSType,
          INSERTED.OutlineNumber,
          INSERTED.OutlineLevel,
          DELETED.WBS1,
          DELETED.WBS2,
          DELETED.WBS3,
          DELETED.LaborCode,
          INSERTED.WBS1,
          INSERTED.WBS2,
          INSERTED.WBS3,
          INSERTED.LaborCode
          INTO @tabUpdated(
            PlanID,
            TaskID,
            WBSType,
            OutlineNumber,
            OutlineLevel,
            Old_WBS1,
            Old_WBS2,
            Old_WBS3,
            Old_LaborCode,
            New_WBS1,
            New_WBS2,
            New_WBS3,
            New_LaborCode
          )
        FROM RPTask AS T
          LEFT JOIN PNPlan AS PN ON T.PlanID = PN.PlanID /* Select ONLY Vision Plans */
        WHERE T.WBSType = 'WBS2'
          AND PN.PlanID IS NULL /* Select ONLY Vision Plans */
          AND (
            (DATALENGTH(LTRIM(RTRIM(ISNULL(T.WBS1, '')))) = 0) OR
            (DATALENGTH(LTRIM(RTRIM(ISNULL(T.WBS2, '')))) = 0) OR
            (NOT(T.WBS3 IS NULL AND T.LaborCode IS NULL))
          )

      -- Fix RPTask WBS3 rows to ensure that mapping fields are correct.

      UPDATE T SET 
        WBS1 = 
          CASE 
            WHEN (DATALENGTH(LTRIM(RTRIM(ISNULL(T.WBS1, '')))) = 0)
            THEN '<none>'
            ELSE T.WBS1
          END,
        WBS2 = 
          CASE 
            WHEN (DATALENGTH(LTRIM(RTRIM(ISNULL(T.WBS2, '')))) = 0)
            THEN '<none>'
            ELSE T.WBS2
          END,
        WBS3 = 
          CASE
            WHEN (DATALENGTH(LTRIM(RTRIM(ISNULL(T.WBS3, '')))) = 0) 
            THEN '<none>'
            ELSE T.WBS3
          END,
        LaborCode = NULL
        OUTPUT
          INSERTED.PlanID,
          INSERTED.TaskID,
          INSERTED.WBSType,
          INSERTED.OutlineNumber,
          INSERTED.OutlineLevel,
          DELETED.WBS1,
          DELETED.WBS2,
          DELETED.WBS3,
          DELETED.LaborCode,
          INSERTED.WBS1,
          INSERTED.WBS2,
          INSERTED.WBS3,
          INSERTED.LaborCode
          INTO @tabUpdated(
            PlanID,
            TaskID,
            WBSType,
            OutlineNumber,
            OutlineLevel,
            Old_WBS1,
            Old_WBS2,
            Old_WBS3,
            Old_LaborCode,
            New_WBS1,
            New_WBS2,
            New_WBS3,
            New_LaborCode
          )
        FROM RPTask AS T
          LEFT JOIN PNPlan AS PN ON T.PlanID = PN.PlanID /* Select ONLY Vision Plans */
        WHERE T.WBSType = 'WBS3'
          AND PN.PlanID IS NULL /* Select ONLY Vision Plans */
          AND (
            (DATALENGTH(LTRIM(RTRIM(ISNULL(T.WBS1, '')))) = 0) OR
            (DATALENGTH(LTRIM(RTRIM(ISNULL(T.WBS2, '')))) = 0) OR
            (DATALENGTH(LTRIM(RTRIM(ISNULL(T.WBS3, '')))) = 0) OR
            (NOT(T.LaborCode IS NULL))
          )

    --######################################################################################################################################################################################################

      -- Fix RPTask LaborCode rows with blank(s) or NULL LaborCode

      UPDATE T SET 
        LaborCode = '<none>'
        OUTPUT
          INSERTED.PlanID,
          INSERTED.TaskID,
          INSERTED.WBSType,
          INSERTED.OutlineNumber,
          INSERTED.OutlineLevel,
          DELETED.WBS1,
          DELETED.WBS2,
          DELETED.WBS3,
          DELETED.LaborCode,
          INSERTED.WBS1,
          INSERTED.WBS2,
          INSERTED.WBS3,
          INSERTED.LaborCode
          INTO @tabUpdated(
            PlanID,
            TaskID,
            WBSType,
            OutlineNumber,
            OutlineLevel,
            Old_WBS1,
            Old_WBS2,
            Old_WBS3,
            Old_LaborCode,
            New_WBS1,
            New_WBS2,
            New_WBS3,
            New_LaborCode
          )
        FROM RPTask AS T
          LEFT JOIN PNPlan AS PN ON T.PlanID = PN.PlanID /* Select ONLY Vision Plans */
        WHERE T.WBSType = 'LBCD'
          AND PN.PlanID IS NULL /* Select ONLY Vision Plans */
          AND (DATALENGTH(LTRIM(RTRIM(ISNULL(T.LaborCode, '')))) = 0)

      -- Make RPTask LaborCode rows inherit {WBS1, WBS2, WBS3} of the Branch Parent

      UPDATE CT SET
        WBS1 = BT.WBS1,
        WBS2 = BT.WBS2,
        WBS3 = BT.WBS3
        OUTPUT
          INSERTED.PlanID,
          INSERTED.TaskID,
          INSERTED.WBSType,
          INSERTED.OutlineNumber,
          INSERTED.OutlineLevel,
          DELETED.WBS1,
          DELETED.WBS2,
          DELETED.WBS3,
          DELETED.LaborCode,
          INSERTED.WBS1,
          INSERTED.WBS2,
          INSERTED.WBS3,
          INSERTED.LaborCode
          INTO @tabUpdated(
            PlanID,
            TaskID,
            WBSType,
            OutlineNumber,
            OutlineLevel,
            Old_WBS1,
            Old_WBS2,
            Old_WBS3,
            Old_LaborCode,
            New_WBS1,
            New_WBS2,
            New_WBS3,
            New_LaborCode
          )
        FROM RPTask AS CT
          LEFT JOIN PNPlan AS PN ON CT.PlanID = PN.PlanID /* Select ONLY Vision Plans */
          CROSS APPLY(
            SELECT DISTINCT
              YBT.WBS1,
              YBT.WBS2,
              YBT.WBS3,
              YBT.OutlineNumber
              FROM ( /* YBT */
                SELECT DISTINCT
                  XBT.OutlineNumber,
                  XBT.WBS1,
                  XBT.WBS2,
                  XBT.WBS3,
                  MAX(XBT.OutlineNumber) OVER(PARTITION BY XCT.OutlineNumber ORDER BY XCT.OutlineNumber) AS MAXBranch_OutlineNumber
                  FROM RPTask AS XCT
                    INNER JOIN RPTask AS XBT 
                      ON XCT.PlanID = XBT.PlanID AND XCT.OutlineNumber LIKE XBT.OutlineNumber + '%' AND XBT.WBSType IN ('WBS1', 'WBS2', 'WBS3')
                  WHERE XCT.PlanID = CT.PlanID AND XCT.TaskID = CT.TaskID
                    AND XCT.TaskID <> XBT.TaskID
                    AND XCT.WBSType = 'LBCD'
              ) AS YBT
                WHERE YBT.OutlineNumber = YBT.MAXBranch_OutlineNumber
          ) AS BT
        WHERE CT.WBSType = 'LBCD'
          AND PN.PlanID IS NULL /* Select ONLY Vision Plans */
          AND (
            ISNULL(CT.WBS1, '|') <> ISNULL(BT.WBS1, '|') OR
            ISNULL(CT.WBS2, '|') <> ISNULL(BT.WBS2, '|') OR
            ISNULL(CT.WBS3, '|') <> ISNULL(BT.WBS3, '|')
          )
          
    --######################################################################################################################################################################################################

      -- Fix RPTask rows with NULL WBSType.
      -- Must do this in sequence starting from the top and rolling downward.

      DECLARE @intMAXOutlineLevel int
      DECLARE @intOutlineLevel int = 1

      -- Get Maximum OutlineLevel.

      SELECT @intMAXOutlineLevel = ISNULL(MAX(OutlineLevel), -1) FROM RPTask

      -- Loop through all Task rows starting from second level (OutlineLevel = 1) and work downward.

      WHILE ((@intMAXOutlineLevel - @intOutlineLevel) >= 0)
        BEGIN

          -- Make Child RPTask rows inherit {WBS1, WBS2, WBS3, LaborCode} of the Parent RPTask row.

          UPDATE CT SET
            WBS1 = PT.WBS1,
            WBS2 = PT.WBS2,
            WBS3 = PT.WBS3,
            LaborCode = PT.LaborCode
            OUTPUT
              INSERTED.PlanID,
              INSERTED.TaskID,
              INSERTED.WBSType,
              INSERTED.OutlineNumber,
              INSERTED.OutlineLevel,
              DELETED.WBS1,
              DELETED.WBS2,
              DELETED.WBS3,
              DELETED.LaborCode,
              INSERTED.WBS1,
              INSERTED.WBS2,
              INSERTED.WBS3,
              INSERTED.LaborCode
              INTO @tabUpdated(
                PlanID,
                TaskID,
                WBSType,
                OutlineNumber,
                OutlineLevel,
                Old_WBS1,
                Old_WBS2,
                Old_WBS3,
                Old_LaborCode,
                New_WBS1,
                New_WBS2,
                New_WBS3,
                New_LaborCode
              )
            FROM RPTask AS CT
              INNER JOIN RPTask AS PT ON CT.PlanID = PT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber
              LEFT JOIN PNPlan AS PN ON CT.PlanID = PN.PlanID /* Select ONLY Vision Plans */
            WHERE CT.WBSType IS NULL
              AND CT.OutlineLevel = @intOutlineLevel
              AND PN.PlanID IS NULL /* Select ONLY Vision Plans */
              AND (
                CT.WBS1 <> PT.WBS1 OR
                CT.WBS2 <> PT.WBS2 OR
                CT.WBS3 <> PT.WBS3
              )

          -- Ready to move down one level.

          SET @intOutlineLevel = @intOutlineLevel + 1
    
        END /* END WHILE */

    --######################################################################################################################################################################################################
    --######################################################################################################################################################################################################

      -- Fix RPAssignment rows to inherit WBS1, WBS2, WBS3 of the immediate parent RPTask.

      UPDATE RPAssignment SET
        WBS1 = PT.WBS1,
        WBS2 = PT.WBS2,
        WBS3 = PT.WBS3,
        LaborCode = PT.LaborCode
        FROM RPAssignment AS A
          INNER JOIN RPTask AS PT ON A.PlanID = PT.PlanID AND A.TaskID = PT.TaskID
        WHERE A.TaskID IN (SELECT DISTINCT TaskID FROM @tabUpdated)

      -- Fix RPExpense rows to inherit WBS1, WBS2, WBS3 of the immediate parent RPTask.

      UPDATE RPExpense SET
        WBS1 = PT.WBS1,
        WBS2 = PT.WBS2,
        WBS3 = PT.WBS3
        FROM RPExpense AS E
          INNER JOIN RPTask AS PT ON E.PlanID = PT.PlanID AND E.TaskID = PT.TaskID
        WHERE E.TaskID IN (SELECT DISTINCT TaskID FROM @tabUpdated)

      -- Fix RPConsultant rows to inherit WBS1, WBS2, WBS3 of the immediate parent RPTask.

      UPDATE RPConsultant SET
        WBS1 = PT.WBS1,
        WBS2 = PT.WBS2,
        WBS3 = PT.WBS3
        FROM RPConsultant AS C
          INNER JOIN RPTask AS PT ON C.PlanID = PT.PlanID AND C.TaskID = PT.TaskID
        WHERE C.TaskID IN (SELECT DISTINCT TaskID FROM @tabUpdated)

      -- Fix RPUnit rows to inherit WBS1, WBS2, WBS3 of the immediate parent RPTask.

      UPDATE RPUnit SET
        WBS1 = PT.WBS1,
        WBS2 = PT.WBS2,
        WBS3 = PT.WBS3
        FROM RPUnit AS U
          INNER JOIN RPTask AS PT ON U.PlanID = PT.PlanID AND U.TaskID = PT.TaskID
        WHERE U.TaskID IN (SELECT DISTINCT TaskID FROM @tabUpdated) 

    --######################################################################################################################################################################################################

    COMMIT TRANSACTION FixWBS1WBS2WBS3LaborCode;

  END TRY

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  BEGIN CATCH 

    SELECT 
      @ErrorMessage = ERROR_MESSAGE(),
      @ErrorSeverity = ERROR_SEVERITY(),
      @ErrorState = ERROR_STATE();

    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION FixWBS1WBS2WBS3LaborCode;

    RAISERROR (
      @ErrorMessage, -- Message text.
      @ErrorSeverity, -- Severity.
      @ErrorState -- State.
    );

    RETURN;

  END CATCH;

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

END -- RPFixMappings
GO
