SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_KeyConvert] ( @What varchar(32), @OldValue nvarchar(32), @NewValue nvarchar(32), @WBS1 nvarchar(30), @WBS2 nvarchar(7))
             AS EXEC spCCG_EI_KeyConvert @What,@OldValue,@NewValue,@WBS1,@WBS2
GO
