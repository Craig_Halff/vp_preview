SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PNCreatePlanFromProj]
  @strWBS1 Nvarchar(30),
  @strUserName Nvarchar(32),
  @strPlanID varchar(32) OUTPUT
AS

BEGIN -- Procedure PNCreatePlanFromProj

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- The purpose of this stored procedure is to create unblished Navigator Plan from a given Project.
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

  DECLARE @intPlanCount int

  -- If the given Project already have an associated Plan, then exit this stored procedure.

  SET @intPlanCount = 
    CASE WHEN EXISTS 
      (SELECT P.PlanID FROM PNPlan AS P INNER JOIN PNTask AS T ON P.PlanID = T.PlanID WHERE T.WBS1 = @strWBS1 AND T.WBSType = 'WBS1'
       UNION
       SELECT P.PlanID FROM RPPlan AS P INNER JOIN RPTask AS T ON P.PlanID = T.PlanID WHERE T.WBS1 = @strWBS1 AND T.WBSType = 'WBS1' AND P.UtilizationIncludeFlg = 'Y'
      ) 
      THEN 1 ELSE 0 END

  IF (@intPlanCount > 0) RETURN

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  DECLARE @strCompany Nvarchar(14)
  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)
  DECLARE @strAccordionFormatID varchar(32)
  DECLARE @strNavigatorCalendarScale varchar(1)
  DECLARE @strEMCostCurrencyCode Nvarchar(3)
  DECLARE @strEMBillCurrencyCode Nvarchar(3)
  DECLARE @strGRCostCurrencyCode Nvarchar(3)
  DECLARE @strGRBillCurrencyCode Nvarchar(3)
  DECLARE @strCalcExpBillAmtFlg varchar(1)
  DECLARE @strCalcConBillAmtFlg varchar(1)
  DECLARE @strExpTab varchar(1)
  DECLARE @strConTab varchar(1)
  DECLARE @strBudgetType varchar(1)
  DECLARE @strReimbMethod varchar(1)

  DECLARE @tiDefExpWBSLevel tinyint
  DECLARE @tiDefConWBSLevel tinyint

  DECLARE @siWBS2Length smallint
  DECLARE @siWBS3Length smallint
  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint
  DECLARE @siLCLevels smallint
  DECLARE @siLCFmtLevel smallint

  DECLARE @siGRMethod smallint
  DECLARE @siCostRtMethod smallint
  DECLARE @siBillRtMethod smallint
  DECLARE @siLabMultType smallint

  DECLARE @intCostRtTableNo int
  DECLARE @intBillRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int
  DECLARE @intExpBillRtMethod int 
  DECLARE @intExpBillRtTableNo int 
  DECLARE @intConBillRtMethod int 
  DECLARE @intConBillRtTableNo int 

  DECLARE @dtMinDate datetime
  DECLARE @dtMaxDate datetime
  DECLARE @dtToday datetime 
  DECLARE @dtTomorrow datetime

  DECLARE @decLabBillMultiplier decimal(19, 4)
  DECLARE @decOverheadPct decimal(19, 4)
  DECLARE @decExpBillMultiplier decimal(19, 4)
  DECLARE @decConBillMultiplier decimal(19, 4)

  DECLARE @tabExpWBSTree TABLE 
    (WBS1 Nvarchar(30) COLLATE database_default,
     WBS2 Nvarchar(7) COLLATE database_default,
     WBS3 Nvarchar(7) COLLATE database_default,
     WBSLevel tinyint,
     IsLeaf bit
     UNIQUE(WBS1, WBS2, WBS3, WBSLevel, IsLeaf)
    )

  DECLARE @tabConWBSTree TABLE 
    (WBS1 Nvarchar(30) COLLATE database_default,
     WBS2 Nvarchar(7) COLLATE database_default,
     WBS3 Nvarchar(7) COLLATE database_default,
     WBSLevel tinyint,
     IsLeaf bit
     UNIQUE(WBS1, WBS2, WBS3, WBSLevel, IsLeaf)
    )

  DECLARE @tabLab TABLE 
    (AssignmentID varchar(32) COLLATE database_default,
     WBS1 Nvarchar(30) COLLATE database_default,
     WBS2 Nvarchar(7) COLLATE database_default,
     WBS3 Nvarchar(7) COLLATE database_default,
     ResourceID Nvarchar(20) COLLATE database_default,
     StartDate datetime,
     EndDate datetime
     UNIQUE(AssignmentID, WBS1, WBS2, WBS3, ResourceID))

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled 
    FROM FW_CFGSystem

  -- Get the current date in current local and not the UTC date.
  
  SET @dtToday = DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)
  SET @dtTomorrow = DATEADD(dd, 1, @dtToday)  
  
  -- Get Org Format

  SELECT
    @siWBS2Length = WBS2Length,
    @siWBS3Length = WBS3Length,
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length,
    @siLCLevels = LCLevels
    FROM CFGFormat

  -- Get Project data.

  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN ISNULL(SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length), ' ') ELSE ' ' END,
    @dtMinDate = COALESCE(StartDate, EstCompletionDate, @dtTomorrow),
    @dtMaxDate = CASE WHEN (EstCompletionDate IS NOT NULL AND EstCompletionDate >= @dtMinDate) THEN EstCompletionDate ELSE @dtMinDate END
    FROM PR WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '

  -- Get Default Plan Settings from CFGResourcePlanning

  SELECT 
    @strBudgetType = BudgetType,
    @siLabMultType =
      CASE BudgetType
        WHEN 'C' THEN 1 /* Planned Multiplier */
        WHEN 'B' THEN 3 /* Planned Ratio */
        WHEN 'A' THEN
          CASE 
            WHEN LabMultType IN (0, 1) THEN 1 /* Planned Multiplier */
            WHEN LabMultType IN (2, 3) THEN 3 /* Planned Ratio */
          END
      END,
    @strReimbMethod = 
      CASE BudgetType
        WHEN 'C' THEN 'C' /* Cost */
        WHEN 'B' THEN 'B' /* Bill */
        WHEN 'A' THEN ReimbMethod
      END,    
    @siGRMethod = GRMethod,
    @intGenResTableNo = GenResTableNo,
    @intGRBillTableNo = GRBillTableNo,
    @siCostRtMethod = CostRtMethod,
    @siBillRtMethod = BillingRtMethod,
    @intCostRtTableNo = CostRtTableNo,
    @intBillRtTableNo = BillingRtTableNo,
    @strCalcExpBillAmtFlg = 'Y',
    @intExpBillRtMethod = ExpBillRtMethod, 
    @intExpBillRtTableNo = ExpBillRtTableNo,
    @decExpBillMultiplier = ExpBillMultiplier,
    @strCalcConBillAmtFlg = 'Y',
    @intConBillRtMethod = ConBillRtMethod, 
    @intConBillRtTableNo = ConBillRtTableNo, 
    @decConBillMultiplier = ConBillMultiplier,
    @decLabBillMultiplier = LabBillMultiplier,
    @decOverheadPct = OverheadPct,
    @strExpTab = ExpTab,
    @strConTab = ConTab,
    @strNavigatorCalendarScale = NavigatorCalendarScale,
    @tiDefExpWBSLevel = ExpWBSLevel,
    @tiDefConWBSLevel = ConWBSLevel
    FROM CFGResourcePlanning WHERE Company = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN @strCompany ELSE ' ' END

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Determining the Currency Code of Rate Tables based on Rate Methods

  SELECT @strEMCostCurrencyCode = 
    CASE 
      WHEN @siCostRtMethod = 2 THEN (SELECT CurrencyCode FROM BTRRT WHERE TableNo = @intCostRtTableNo)
      WHEN @siCostRtMethod = 3 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intCostRtTableNo)
      WHEN @siCostRtMethod = 4 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intCostRtTableNo)
    END

  SELECT @strEMBillCurrencyCode = 
    CASE 
      WHEN @siBillRtMethod = 2 THEN (SELECT CurrencyCode FROM BTRRT WHERE TableNo = @intBillRtTableNo)
      WHEN @siBillRtMethod = 3 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intBillRtTableNo)
      WHEN @siBillRtMethod = 4 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intBillRtTableNo)
    END

  SELECT @strGRCostCurrencyCode = 
    CASE 
      WHEN @siGRMethod = 0 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intGenResTableNo)
      WHEN @siGRMethod = 1 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intGenResTableNo)
    END

  SELECT @strGRBillCurrencyCode = 
    CASE 
      WHEN @siGRMethod = 0 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intGRBillTableNo)
      WHEN @siGRMethod = 1 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intGRBillTableNo)
    END

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  /*
    Need to select either:
      1. Rows at same level as the Default setting (e.g. Level 2).
      2. Rows at level lesser as the Default setting (e.g. Level 1) but are leaf nodes.
  */

  INSERT @tabExpWBSTree (
    WBS1,
    WBS2,
    WBS3,
    WBSLevel,
    IsLeaf
  )
    SELECT
      WBS1,
      WBS2,
      WBS3,
      WBSLevel,
      IsLeaf
      FROM
        (SELECT
           WBS1, WBS2, WBS3,
           CASE 
             WHEN WBS2 = ' ' AND WBS3 = ' ' THEN 1
             WHEN WBS2 != ' ' AND WBS3 = ' ' THEN 2
             WHEN WBS2 != ' ' AND WBS3 != ' ' THEN 3
           END AS WBSLevel,
           CASE WHEN SubLevel = 'N' THEN 1 ELSE 0 END AS IsLeaf
           FROM PR
           WHERE WBS1 = @strWBS1
        ) AS X
      WHERE (WBSLevel = @tiDefExpWBSLevel OR (WBSLevel < @tiDefExpWBSLevel AND IsLeaf = 1))

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  INSERT @tabConWBSTree (
    WBS1,
    WBS2,
    WBS3,
    WBSLevel,
    IsLeaf
  )
    SELECT
      WBS1,
      WBS2,
      WBS3,
      WBSLevel,
      IsLeaf
      FROM
        (SELECT
           WBS1, WBS2, WBS3,
           CASE 
             WHEN WBS2 = ' ' AND WBS3 = ' ' THEN 1
             WHEN WBS2 != ' ' AND WBS3 = ' ' THEN 2
             WHEN WBS2 != ' ' AND WBS3 != ' ' THEN 3
           END AS WBSLevel,
           CASE WHEN SubLevel = 'N' THEN 1 ELSE 0 END AS IsLeaf
           FROM PR
           WHERE WBS1 = @strWBS1
        ) AS X
      WHERE (WBSLevel = @tiDefConWBSLevel OR (WBSLevel < @tiDefConWBSLevel AND IsLeaf = 1))

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Save away the list of Employees who charged to the Project (from the Ledger tables)

  INSERT @tabLab
    (AssignmentID,
     WBS1,
     WBS2,
     WBS3,
     ResourceID,
     StartDate,
     EndDate)
    SELECT DISTINCT
      REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS AssignmentID,
      WBS1,
      WBS2,
      WBS3,
      Employee AS ResourceID,
      @dtMinDate AS StartDate,
      @dtMaxDate AS EndDate
      FROM
        (SELECT DISTINCT
           LD.WBS1,
           LD.WBS2,
           LD.WBS3,
           LD.Employee
           FROM LD -- LD table has entries at only the leaf level.
             INNER JOIN EM ON LD.Employee = EM.Employee -- Only pick up records in LD which has legitimate EM.
           WHERE LD.WBS1 = @strWBS1 AND LD.ProjectCost = 'Y' 
           GROUP BY LD.WBS1, LD.WBS2, LD.WBS3, LD.Employee
         UNION SELECT DISTINCT
           TD.WBS1,
           TD.WBS2,
           TD.WBS3,
           TD.Employee
           FROM tkDetail AS TD -- TD table has entries at only the leaf level.
             INNER JOIN EM ON TD.Employee = EM.Employee  
             INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
           WHERE TD.WBS1 = @strWBS1 AND
             (RegHrs > 0 OR OvtHrs > 0 OR SpecialOvtHrs > 0)
           GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.Employee
         UNION SELECT DISTINCT
           TD.WBS1,
           TD.WBS2,
           TD.WBS3,
           TD.Employee
           FROM tsDetail AS TD -- TD table has entries at only the leaf level.
             INNER JOIN EM ON TD.Employee = EM.Employee  
             INNER JOIN tsControl AS TC ON (TC.Posted = 'N' AND TD.Batch = TC.Batch)
           WHERE TD.WBS1 = @strWBS1 AND
             (RegHrs > 0 OR OvtHrs > 0 OR SpecialOvtHrs > 0)
           GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.Employee
        ) AS X
        GROUP BY WBS1, WBS2, WBS3, Employee

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
  BEGIN TRANSACTION

    -- Insert PNPlan record.

    SET @strPlanID = REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')
  
    INSERT PNPlan 
      (
       PlanID,
       PlanName,
       PlanNumber,
       ProjMgr,
       Principal,
       Supervisor,
       OpportunityID,
       ClientID,
       Org,
       WBS1,
       StartDate,
       EndDate,

       Probability,

       PctCompleteFormula,
       LabMultType,
       ReimbMethod,
       AvailableFlg,
       UnPostedFlg,
       CommitmentFlg,
       UtilizationIncludeFlg,

       Status,
       Company,
       StartingDayOfWeek,
       CostCurrencyCode,
       BillingCurrencyCode,

       BudgetType,
       CostRtMethod,
       BillingRtMethod,
       CostRtTableNo,
       BillingRtTableNo,
       LabBillMultiplier,
       GRMethod,
       GenResTableNo,
       GRBillTableNo,

       CalcExpBillAmtFlg,
       ExpBillRtMethod, 
       ExpBillRtTableNo,
       ExpBillMultiplier,

       CalcConBillAmtFlg,
       ConBillRtMethod, 
       ConBillRtTableNo,
       ConBillMultiplier,

       CompensationFee,
       ConsultantFee,
       ReimbAllowance,
       CompensationFeeBill,
       ConsultantFeeBill,
       ReimbAllowanceBill,

	     CompensationFeeDirLab,
	     CompensationFeeDirExp,
	     ReimbAllowanceExp,
	     ReimbAllowanceCon,
	     CompensationFeeDirLabBill,
	     CompensationFeeDirExpBill,
	     ReimbAllowanceExpBill,
	     ReimbAllowanceConBill,

       OverheadPct,
       ExpWBSLevel,
       ConWBSLevel,

       CreateUser,
       CreateDate,
       ModUser,
       ModDate
      )
      SELECT
        @strPlanID AS PlanID,
        PR.Name AS PlanName,
        PR.WBS1 AS PlanNumber,
        PR.ProjMgr AS ProjMgr,
        PR.Principal AS Principal,
        PR.Supervisor AS Supervisor,
        PR.OpportunityID AS OpportunityID,
        PR.ClientID AS ClientID,
        PR.Org AS Org,
        PR.WBS1 AS WBS1,
        @dtMinDate AS StartDate,
        @dtMaxDate AS EndDate,

        ISNULL(O.Probability, 0) AS Probability,

        2 AS PctCompleteFormula, -- JTD / (JTD + ETC)
        @siLabMultType AS LabMultType, 
        @strReimbMethod AS ReimbMethod,
        'Y' AS AvailableFlg,
        'Y' AS UnPostedFlg,
        'Y' AS CommitmentFlg,
        'Y' AS UtilizationIncludeFlg,

        'A' AS Status, -- Active
        @strCompany Company,
        2 AS StartingDayOfWeek, -- Always set to Monday
        PR.ProjectCurrencyCode AS CostCurrencyCode,
        CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.BillingCurrencyCode ELSE PR.ProjectCurrencyCode END AS BillingCurrencyCode,

        @strBudgetType AS BudgetType, -- From CFGResourcePlanning
 
        CASE WHEN @siCostRtMethod = 4 THEN 2 ELSE @siCostRtMethod END AS CostRtMethod,
        CASE WHEN @siBillRtMethod = 4 THEN 2 ELSE @siBillRtMethod END AS BillingRtMethod,

        CASE WHEN @siCostRtMethod = 4 THEN 0
        ELSE
          CASE WHEN @strMultiCurrencyEnabled <> 'Y' THEN @intCostRtTableNo
          ELSE
            CASE WHEN @strEMCostCurrencyCode = PR.ProjectCurrencyCode THEN @intCostRtTableNo ELSE 0 END
          END
        END AS CostRtTableNo,

        CASE WHEN @siBillRtMethod = 4 THEN 0
        ELSE
          CASE WHEN @strMultiCurrencyEnabled <> 'Y' THEN @intBillRtTableNo
          ELSE
            CASE WHEN @strEMBillCurrencyCode = CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.BillingCurrencyCode ELSE PR.ProjectCurrencyCode END
                 THEN @intBillRtTableNo
                 ELSE 0
            END
          END
        END AS BillingRtTableNo,
 
        @decLabBillMultiplier AS LabBillMultiplier,
 
        @siGRMethod AS GRMethod,

        CASE WHEN @strMultiCurrencyEnabled <> 'Y' THEN @intGenResTableNo
        ELSE
          CASE WHEN @strGRCostCurrencyCode = PR.ProjectCurrencyCode THEN @intGenResTableNo ELSE 0 END 
        END AS GenResTableNo,

        CASE WHEN @strMultiCurrencyEnabled <> 'Y' THEN @intGRBillTableNo
        ELSE
          CASE WHEN @strGRBillCurrencyCode = CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.BillingCurrencyCode ELSE PR.ProjectCurrencyCode END
               THEN @intGRBillTableNo
               ELSE 0
          END 
        END AS GRBillTableNo,

        @strCalcExpBillAmtFlg AS CalcExpBillAmtFlg,
        @intExpBillRtMethod AS ExpBillRtMethod, 
        @intExpBillRtTableNo AS ExpBillRtTableNo,
        @decExpBillMultiplier AS ExpBillMultiplier,
 
        @strCalcConBillAmtFlg AS CalcConBillAmtFlg,
        @intConBillRtMethod AS ConBillRtMethod, 
        @intConBillRtTableNo AS ConBillRtTableNo,
        @decConBillMultiplier AS ConBillMultiplier,

        PR.Fee AS CompensationFee,
        PR.ConsultFee AS ConsultantFee,
        PR.ReimbAllow AS ReimbAllowance,

        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeBillingCurrency ELSE PR.Fee END) AS CompensationFeeBill,
        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ConsultFeeBillingCurrency ELSE PR.ConsultFee END) AS ConsultantFeeBill,
        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowBillingCurrency ELSE PR.ReimbAllow END) AS ReimbAllowanceBill,

	      PR.FeeDirLab AS CompensationFeeDirLab,
	      PR.FeeDirExp AS CompensationFeeDirExp,
	      PR.ReimbAllowExp AS ReimbAllowanceExp,
	      PR.ReimbAllowCons AS ReimbAllowanceCon,

        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirLabBillingCurrency ELSE PR.FeeDirLab END) AS CompensationFeeDirLabBill,
        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirExpBillingCurrency ELSE PR.FeeDirExp END) AS CompensationFeeDirExpBill,
        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowExpBillingCurrency ELSE PR.ReimbAllowExp END) AS ReimbAllowanceExpBill,
        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowConsBillingCurrency ELSE PR.ReimbAllowCons END) AS ReimbAllowanceConBill,

        @decOverheadPct AS OverheadPct,
        @tiDefExpWBSLevel AS ExpWBSLevel,
        @tiDefConWBSLevel AS ConWBSLevel,

        @strUserName AS CreateUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
        @strUserName AS ModUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate

        FROM PR
          LEFT JOIN Opportunity AS O ON PR.OpportunityID = O.OpportunityID
        WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND
          (NOT EXISTS 
             (SELECT P.PlanID FROM PNPlan AS P INNER JOIN PNTask AS T ON P.PlanID = T.PlanID WHERE T.WBS1 = @strWBS1 AND T.WBSType = 'WBS1'
              UNION
              SELECT P.PlanID FROM RPPlan AS P INNER JOIN RPTask AS T ON P.PlanID = T.PlanID WHERE T.WBS1 = @strWBS1 AND T.WBSType = 'WBS1' AND P.UtilizationIncludeFlg = 'Y'
             )
          )

   -- If the given Project already have an associated Plan, then exit this stored procedure.
 
    IF (@@ROWCOUNT = 0)
      BEGIN
        SET @strPlanID = NULL
        ROLLBACK
        RETURN
      END -- END IF (@@ROWCOUNT > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    -- Insert WBS1 PNTask record.

    INSERT PNTask
      (
       TaskID,
       PlanID,
       Name,
       WBS1,
       WBS2,
       WBS3,
       WBSType,
       ParentOutlineNumber,
       OutlineNumber,
       ChildrenCount,
       OutlineLevel,

       StartDate,
       EndDate,

       LabParentState,
       ExpParentState,
       ConParentState,
       UntParentState,
       LabVState,
       ExpVState,
       ConVState,
       UntVState,

       CreateUser,
       CreateDate,
       ModUser,
       ModDate
      )
      SELECT
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TaskID,
        @strPlanID AS PlanID,
        PR.Name AS Name,
        PR.WBS1 AS WBS1,
        NULL AS WBS2,
        NULL AS WBS3,
        'WBS1' AS WBSType,
        NULL AS ParentOutlineNumber,
        '001' AS OutlineNumber,
        ISNULL(X1.WBS2Count, 0) AS ChildrenCount,
        0 AS OutlineLevel,

        @dtMinDate AS StartDate,
        @dtMaxDate  AS EndDate,

        CASE WHEN ((ISNULL(X1.WBS2Count, 0) + ISNULL(X2.ACount, 0)) > 0) THEN 'O' ELSE 'N' END AS LabParentState,
        'N' AS ExpParentState,
        'N' AS ConParentState,
        'N' AS UntParentState,
        'Y' AS LabVState,
        'Y' AS ExpVState,
        'Y' AS ConVState,
        'Y' AS UntVState,

        @strUserName AS CreateUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
        @strUserName AS ModUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate

        FROM PR
          LEFT JOIN (SELECT COUNT(*) AS WBS2Count, WBS1 FROM PR WHERE WBS1 = @strWBS1 AND WBS2 <> ' ' AND WBS3 = ' ' GROUP BY WBS1) AS X1 ON X1.WBS1 = PR.WBS1
          LEFT JOIN (SELECT COUNT(*) AS ACount, MIN(StartDate) AS MinDate, MAX(EndDate) AS MaxDate, WBS1 FROM @tabLab AS A WHERE A.WBS1 = @strWBS1 GROUP BY WBS1) AS X2 ON X2.WBS1 = PR.WBS1
        WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    -- Insert WBS2 PNTask record, if applicable.

    IF(@siWBS2Length > 0)
      BEGIN

        INSERT PNTask
          (
           TaskID,
           PlanID,
           Name,
           WBS1,
           WBS2,
           WBS3,
           WBSType,
           ParentOutlineNumber,
           OutlineNumber,
           ChildrenCount,
           OutlineLevel,

           StartDate,
           EndDate,

           LabParentState,
           ExpParentState,
           ConParentState,
           UntParentState,
           LabVState,
           ExpVState,
           ConVState,
           UntVState,

           CreateUser,
           CreateDate,
           ModUser,
           ModDate
          )
          SELECT
            TaskID,
            PlanID,
            Name,
            WBS1,
            WBS2,
            WBS3,
            WBSType,
            ParentOutlineNumber,
            ParentOutlineNumber + '.' + RIGHT('000' + CONVERT(VARCHAR(3), RowID), 3) AS OutlineNumber,
            ChildrenCount,
            OutlineLevel,
            StartDate,
            EndDate,
            LabParentState,
            ExpParentState,
            ConParentState,
            UntParentState,
            LabVState,
            ExpVState,
            ConVState,
            UntVState,
            CreateUser,
            CreateDate,
            ModUser,
            ModDate
            FROM
              (SELECT
                 REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TaskID,
                 @strPlanID AS PlanID,
                 PR.Name AS Name,
                 PR.WBS1 AS WBS1,
                 PR.WBS2 AS WBS2,
                 NULL AS WBS3,
                 'WBS2' AS WBSType,
                 '001' AS ParentOutlineNumber,
                 ISNULL(X1.WBS3Count, 0) AS ChildrenCount,
                 1 AS OutlineLevel,
                 COALESCE(X2.MinDate, @dtMinDate) AS StartDate,
                 COALESCE(X2.MaxDate, @dtMaxDate)  AS EndDate,
                 CASE WHEN ((ISNULL(X1.WBS3Count, 0) + ISNULL(X2.ACount, 0)) > 0) THEN 'O' ELSE 'N' END AS LabParentState,
                 'N' AS ExpParentState,
                 'N' AS ConParentState,
                 'N' AS UntParentState,
                 'Y' AS LabVState,
                 'Y' AS ExpVState,
                 'Y' AS ConVState,
                 'Y' AS UntVState,
                 @strUserName AS CreateUser,
                 CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
                 @strUserName AS ModUser,
                 CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate,
                 ROW_NUMBER() OVER (PARTITION BY PR.WBS1 ORDER BY PR.WBS2) AS RowID
                 FROM PR
                   LEFT JOIN (SELECT COUNT(*) AS WBS3Count, WBS1, WBS2 FROM PR WHERE WBS1 = @strWBS1 AND WBS2 <> ' ' AND WBS3 <> ' ' GROUP BY WBS1, WBS2) AS X1 ON X1.WBS1 = PR.WBS1 AND X1.WBS2 = PR.WBS2
                   LEFT JOIN (SELECT COUNT(*) AS ACount, MIN(StartDate) AS MinDate, MAX(EndDate) AS MaxDate, WBS1, WBS2 FROM @tabLab AS A WHERE A.WBS1 = @strWBS1 AND WBS2 <> ' ' GROUP BY WBS1, WBS2) AS X2 ON X2.WBS1 = PR.WBS1 AND X2.WBS2 = PR.WBS2
                 WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 <> ' ' AND PR.WBS3 = ' '
              ) AS Y

      END -- END IF(@siWBS2Length > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    -- Insert WBS3 PNTask record, if applicable.

    IF(@siWBS3Length > 0)
      BEGIN

        INSERT PNTask
          (
           TaskID,
           PlanID,
           Name,
           WBS1,
           WBS2,
           WBS3,
           WBSType,
           ParentOutlineNumber,
           OutlineNumber,
           ChildrenCount,
           OutlineLevel,

           StartDate,
           EndDate,

           LabParentState,
           ExpParentState,
           ConParentState,
           UntParentState,
           LabVState,
           ExpVState,
           ConVState,
           UntVState,

           CreateUser,
           CreateDate,
           ModUser,
           ModDate
          )
          SELECT
            TaskID,
            PlanID,
            Name,
            WBS1,
            WBS2,
            WBS3,
            WBSType,
            ParentOutlineNumber,
            ParentOutlineNumber + '.' + RIGHT('000' + CONVERT(VARCHAR(3), RowID), 3) AS OutlineNumber,
            ChildrenCount,
            OutlineLevel,
            StartDate,
            EndDate,
            LabParentState,
            ExpParentState,
            ConParentState,
            UntParentState,
            LabVState,
            ExpVState,
            ConVState,
            UntVState,
            CreateUser,
            CreateDate,
            ModUser,
            ModDate
            FROM
              (SELECT
                 REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TaskID,
                 @strPlanID AS PlanID,
                 PR.Name AS Name,
                 PR.WBS1 AS WBS1,
                 PR.WBS2 AS WBS2,
                 PR.WBS3 AS WBS3,
                 'WBS3' AS WBSType,
                 PT.OutlineNumber AS ParentOutlineNumber,
                 0 AS ChildrenCount,
                 2 AS OutlineLevel,
                 COALESCE(X2.MinDate, @dtMinDate) AS StartDate,
                 COALESCE(X2.MaxDate, @dtMaxDate)  AS EndDate,
                 CASE WHEN (ISNULL(X2.ACount, 0) > 0) THEN 'O' ELSE 'N' END AS LabParentState,
                 'N' AS ExpParentState,
                 'N' AS ConParentState,
                 'N' AS UntParentState,
                 'Y' AS LabVState,
                 'Y' AS ExpVState,
                 'Y' AS ConVState,
                 'Y' AS UntVState,
                 @strUserName AS CreateUser,
                 CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
                 @strUserName AS ModUser,
                 CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate,
                 ROW_NUMBER() OVER (PARTITION BY PR.WBS1, PR.WBS2 ORDER BY PR.WBS3) AS RowID
                 FROM PR
                   INNER JOIN PNTask AS PT ON PR.WBS1 = PT.WBS1 AND PR.WBS2 = PT.WBS2
                   LEFT JOIN (SELECT COUNT(*) AS ACount, MIN(StartDate) AS MinDate, MAX(EndDate) AS MaxDate, WBS1, WBS2, WBS3 FROM @tabLab AS A WHERE A.WBS1 = @strWBS1 AND WBS2 <> ' ' AND WBS3 <> ' ' GROUP BY WBS1, WBS2, WBS3) AS X2 ON X2.WBS1 = PR.WBS1 AND X2.WBS2 = PR.WBS2 AND X2.WBS3 = PR.WBS3
                 WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 <> ' ' AND PR.WBS3 <> ' '
              ) AS Y

      END -- END IF(@siWBS3Length > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    -- Create Accordion Calendar.

    SET @strAccordionFormatID = REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')

    INSERT PNAccordionFormat
      (
       AccordionFormatID,
	     PlanID,
	     StartDate,
	     EndDate,
	     MajorScale,
	     MinorScale,
	     CreateUser,
	     CreateDate,
	     ModUser,
	     ModDate
      )
      SELECT
        @strAccordionFormatID AS AccordionFormatID,
        @strPlanID AS PlanID,
        P.StartDate AS StartDate,
        P.EndDate AS EndDate,
        CASE WHEN @strNavigatorCalendarScale = 'w' THEN 'm'
             WHEN @strNavigatorCalendarScale = 'b' THEN 'm'
             WHEN @strNavigatorCalendarScale = 's' THEN 'm'
             WHEN @strNavigatorCalendarScale = 'm' THEN 'q'
             WHEN @strNavigatorCalendarScale = 'q' THEN 'y'
             WHEN @strNavigatorCalendarScale = 'y' THEN 'y'
             END AS MajorScale,
        ISNULL(@strNavigatorCalendarScale, 'm') AS MinorScale,
        @strUserName AS CreateUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
        @strUserName AS ModUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate
        FROM PNPlan AS P
        WHERE P.PlanID = @strPlanID 

    -- Make Calendar Intervals.

    EXEC dbo.PNMakeCI @strAccordionFormatID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    -- Create WBS Level Format.

    INSERT PNWBSLevelFormat
      (
       WBSFormatID,
	     PlanID,
	     FmtLevel,
	     WBSType,
	     CreateUser,
	     CreateDate,
	     ModUser,
	     ModDate
      )
      SELECT 
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS WBSFormatID,
	      @strPlanID AS PlanID,
	      1 AS FmtLevel,
	      'WBS1' AS WBSType,
        @strUserName AS CreateUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
        @strUserName AS ModUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate

    IF(@siWBS2Length > 0)
      BEGIN

        INSERT PNWBSLevelFormat
          (
           WBSFormatID,
	         PlanID,
	         FmtLevel,
	         WBSType,
	         CreateUser,
	         CreateDate,
	         ModUser,
	         ModDate
          )
          SELECT 
            REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS WBSFormatID,
	          @strPlanID AS PlanID,
	          2 AS FmtLevel,
	          'WBS2' AS WBSType,
            @strUserName AS CreateUser,
            CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
            @strUserName AS ModUser,
            CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate

      END -- END IF(@siWBS2Length > 0)

    IF(@siWBS3Length > 0)
      BEGIN

        INSERT PNWBSLevelFormat
          (
           WBSFormatID,
	         PlanID,
	         FmtLevel,
	         WBSType,
	         CreateUser,
	         CreateDate,
	         ModUser,
	         ModDate
          )
          SELECT 
            REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS WBSFormatID,
	          @strPlanID AS PlanID,
	          3 AS FmtLevel,
	          'WBS3' AS WBSType,
            @strUserName AS CreateUser,
            CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
            @strUserName AS ModUser,
            CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate

      END -- END IF(@siWBS3Length > 0)

    IF(@siLCLevels > 0)
      BEGIN

        SELECT @siLCFmtLevel =
          CASE 
            WHEN @siWBS3Length > 0 THEN 4
            WHEN @siWBS3Length = 0 AND @siWBS2Length > 0 THEN 3
            WHEN @siWBS2Length = 0 THEN 2
            ELSE 0
          END

        INSERT PNWBSLevelFormat
          (
           WBSFormatID,
	         PlanID,
	         FmtLevel,
	         WBSType,
	         CreateUser,
	         CreateDate,
	         ModUser,
	         ModDate
          )
          SELECT 
            REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS WBSFormatID,
	          @strPlanID AS PlanID,
	          @siLCFmtLevel AS FmtLevel,
	          'LBCD' AS WBSType,
            @strUserName AS CreateUser,
            CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
            @strUserName AS ModUser,
            CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate

      END -- END IF(@siLCLevels > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Publish the Navigator Plan.

  EXECUTE dbo.PNPublish @strPlanID

  COMMIT

  SET NOCOUNT OFF

END -- PNCreatePlanFromProj
GO
