SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectLowestLevelStatusValidation] @Project varchar (32)
AS
/*
Copyright 2019 (c) Central Consulting Group.   All rights reserved.
08/02/2019  David Springer
            Validate lowest level is not Dormant
*/
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
BEGIN

   If exists (Select 'x' From UDIC_ProjectLowestLevel Where UDIC_UID = @Project and CustStatus = 'D')
      RAISERROR ('This project number is Closed and cannot be used.           ', 16, 1)
   
   IF exists (Select 'x' From UDIC_ProjectLowestLevel Where UDIC_UID = @Project and CustStatus = 'I')
      RAISERROR ('This project number is Inactive and cannot be used.           ', 16, 1)
END
GO
