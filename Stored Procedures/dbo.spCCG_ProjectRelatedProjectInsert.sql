SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectRelatedProjectInsert] @WBS1 varchar(32), @ModUser varchar (255)
AS
/*
Copyright (c) 2019 Central Consulting Group. All rights reserved.
08/15/2019	David Springer
			Write suffix Project into parent Project Related Projects grid.
			Call from Project INSERT workflow.
			Called from spCCG_ProjectRelatedProjectUpdate procedure.
04/09/2020  David Springer
            Added Master Contract Projects
09/17/2020	Craig H. Anderson
			Parent's CustMasterContractType must not be 'Individual Project/Task Order'
*/ 
DECLARE @ParentWBS1 varchar (32)
BEGIN
SET NOCOUNT ON

	Select @ParentWBS1 = p.WBS1
	From PR c, PR p, ProjectCustomTabFields px
	Where c.WBS1 = @WBS1
	  and c.WBS2 = ' '
	  and c.WBS1 not like '%000' 
	-- Child left of the period matches Parent left of the period
	  and Left (c.WBS1, charindex ('.', c.WBS1)) = Left (p.WBS1, charindex ('.', p.WBS1))
	  and p.WBS1 like '%.000'
	  and p.WBS2 = ' '
	  and p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and px.CustMasterContractType is not NULL
	  AND px.CustMasterContractType <> 'Individual Project/Task Order'
	  and not exists (Select 'x' From Projects_RelatedProjects Where WBS1 = p.WBS1 and WBS2 = ' ' and CustRelatedProject = c.WBS1)

If @ParentWBS1 is not null
	Begin
	Insert Into Projects_RelatedProjects
	(WBS1, WBS2, WBS3, Seq, CreateUser, CreateDate, CustRelatedProjectNo, CustRelatedProject, 
	 CustRelatedProjectFeeDirLab, CustRelatedProjectFeeDirExp, CustRelatedProjectConsultFee, 
	 CustRelatedProjectReimbAllowExp, CustRelatedProjectReimbAllowCons, CustRelatedProjectFeeTotal, CustRelatedProjectStatus)
	Select p.WBS1, ' ', ' ', Replace (NewID(), '-', '') Seq, @ModUser, getDate(), c.WBS1, c.WBS1, 
		c.FeeDirLab, c.FeeDirExp, c.ConsultFee, c.ReimbAllowExp, c.ReimbAllowCons, c.FeeDirLab + c.FeeDirExp + c.ConsultFee + c.ReimbAllowExp + c.ReimbAllowCons, c.Status
	From PR c, ProjectCustomTabFields cx, PR p
	Where c.WBS1 = @WBS1
	  and c.WBS2 = ' '  -- project level
	  and c.WBS1 = cx.WBS1
	  and c.WBS2 = cx.WBS2
	  and c.WBS1 not like '%.000'
	-- Child left of the period matches Parent left of the period
	  and Left (c.WBS1, charindex ('.', c.WBS1)) = Left (p.WBS1, charindex ('.', p.WBS1))
	  and p.WBS1 like '%.000' 
	  and p.WBS2 = ' '
	  and not exists (Select 'x' From Projects_RelatedProjects Where WBS1 = p.WBS1 and WBS2 = ' ' and CustRelatedProject = c.WBS1)

    UPDATE dbo.ProjectCustomTabFields
    SET CustMasterContractProject = @ParentWBS1
    WHERE WBS1 = @WBS1
      AND WBS2 = ' '

	exec spCCG_ProjectRelatedProjectFeeAdjustment @ParentWBS1
	End
END
GO
