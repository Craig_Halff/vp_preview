SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteVendor] @Vendor Nvarchar(32)
AS
    DELETE FROM CustomProposalVendor WHERE Vendor = @Vendor
    DELETE FROM CustomProposalVendorGraphics WHERE Vendor = @Vendor
    --DELETE FROM OpportunityVEAssoc WHERE Vendor = @Vendor
    DELETE FROM VEAccounting WHERE Vendor = @Vendor
    DELETE FROM VEAddress WHERE Vendor = @Vendor
    DELETE FROM VEFileLinks WHERE Vendor = @Vendor
    DELETE FROM VendorCustomTabFields WHERE Vendor = @Vendor
    DELETE FROM VEProjectAssoc WHERE Vendor = @Vendor
    DELETE FROM VEProjectAssocTemplate WHERE Vendor = @Vendor
    DELETE FROM VESubscr WHERE Vendor = @Vendor

    EXEC DeleteCustomGridTabData 'Vendors',@Vendor, '', ''

    UPDATE Contacts SET Vendor = null WHERE Vendor = @Vendor
    UPDATE SF255Consultant SET Consultant = null WHERE Consultant = @Vendor

    UPDATE RPExpense SET Vendor = null WHERE Vendor = @Vendor
    UPDATE PNExpense SET Vendor = null WHERE Vendor = @Vendor
    UPDATE RPConsultant SET Vendor = null WHERE Vendor = @Vendor
    UPDATE PNConsultant SET Vendor = null WHERE Vendor = @Vendor
GO
