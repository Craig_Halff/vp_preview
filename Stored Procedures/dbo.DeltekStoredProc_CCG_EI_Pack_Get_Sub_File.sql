SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Pack_Get_Sub_File]
	@PAT				BIT,
	@ShowJTD			BIT,
	@ShowDirect			BIT,
	@SuppressbillOnly	BIT,
	@FilterPrintWithInvoice BIT,
	@FW_FILES			BIT,
	@Order				VARCHAR(300),
	@Wbs1				NVARCHAR(30),
	@Invoice			NVARCHAR(20),
	@Minimum			VARCHAR(50),
	@XML_String			VARCHAR(500)
AS
BEGIN
	/*
	[DeltekStoredProc_CCG_EI_Pack_Get_Sub_File]
		@PAT = '1',
		@ShowJTD = '0',
		@ShowDirect = '0',
		@SuppressbillOnly = '1',
		@FW_FILES = '1',
		@Order = 'WBS',
		@Wbs1 = '2003005.00',
		@Invoice = '0000958',
		@Minimum = '0.00'
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max) =N'';
	DECLARE @safeSql	int;

	-- Ensure that our inputs are safe (prevent SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @Wbs1);
	SET @safeSql = @safeSql | (dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @Invoice) * 2);
	SET @safeSql = @safeSql | (dbo.fnCCG_EI_RegexMatch('<NUMBER>|<FIELD>', @Minimum) * 4);
	SET @safeSql = @safeSql | (dbo.fnCCG_EI_RegexMatch('<SQL PHRASE>|', @XML_String) * 8);
	IF @safeSql < 15 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS varchar(4));
		RETURN;
	END;

	SET @Order = LOWER(ISNULL(@Order, ''));

	IF @PAT = 1 BEGIN
		SET @sSQL = N'
			SELECT ''PAT'' as FileSource, BillConDetail.*, PAT.PayableFileName as FileID, '''' as PrintWithInvoice,
					LedgerAP.Account, IsNull(CAap.Type, CAbied.Type) as Type, LedgerAP.BillStatus, LedgerAP.Voucher
				FROM BillConDetail
					LEFT JOIN LedgerAP on BillConDetail.OriginalPeriod = LedgerAP.Period
						and BillConDetail.OriginalPostSeq = LedgerAP.PostSeq
						and BillConDetail.OriginalPKey = LedgerAP.PKey
						and BillConDetail.OriginalTable = ''LedgerAP''
					LEFT JOIN BIED on BillConDetail.OriginalPeriod = BIED.Period
						and BillConDetail.OriginalPostSeq = BIED.PostSeq
						and BillConDetail.OriginalPKey = BIED.PKey
						and BillConDetail.OriginalTable = ''BIED''
					LEFT JOIN CA CAap on LedgerAP.Account = CAap.Account
					LEFT JOIN CA CAbied on BIED.Account = CAbied.Account
					LEFT JOIN CCG_PAT_Payable PAT on LedgerAP.Vendor = PAT.Vendor
						and LedgerAP.Voucher = PAT.Voucher ';
		IF CHARINDEX('vendorname', @Order) > 0 SET @sSQL += N'
					LEFT JOIN VE on BillConDetail.Vendor = VE.Vendor ';

		SET @sSQL += N'
				WHERE BillConDetail.MainWBS1 = ''' + @Wbs1 + N''' ';
		IF @ShowJTD = 0 SET @sSQL += N'and BillConDetail.Invoice = ''' + @Invoice + N''' ';
		SET @sSQL += N'
					and IsNull(CAap.Type, CAbied.Type) in ' +
					(CASE WHEN @ShowDirect = 1 THEN N'(5,6,7,8)' ELSE N'(5,6)' END); --reimb only default

		IF @SuppressbillOnly = 1 SET @sSQL += N'
					and IsNull(LedgerAP.SuppressBill, IsNull(BIED.SuppressBill, ''N'')) = ''N'' ';
		IF ISNULL(@Minimum, '') <> '' SET @sSQL += N'
					and BillConDetail.Amount >= ' + @Minimum;
	END;

	IF @FW_FILES = 1 BEGIN
		IF @sSQL <> '' SET @sSQL += N'
			UNION ALL ';
		SET @sSQL += N'
			SELECT ''FW_FILES'' as FileSource, BillConDetail.*,
					convert(varchar(255), LedgerDocuments.FileID) as FileID, LedgerDocuments.PrintWithInvoice,
					LedgerAP.Account, IsNull(CAap.Type, CAbied.Type) as Type, LedgerAP.BillStatus, LedgerAP.Voucher
				FROM BillConDetail
					LEFT JOIN LedgerAP on billConDetail.OriginalPeriod = LedgerAP.Period
						and billConDetail.OriginalPostSeq = LedgerAP.PostSeq
						and billConDetail.OriginalPKey = LedgerAP.PKey
						and billConDetail.OriginalTable = ''LedgerAP''
                    LEFT JOIN BIED on billConDetail.OriginalPeriod = BIED.Period
						and billConDetail.OriginalPostSeq = BIED.PostSeq
						and billConDetail.OriginalPKey = BIED.PKey
						and billConDetail.OriginalTable = ''BIED''
                    LEFT JOIN CA CAap on LedgerAP.Account = CAap.Account
                    LEFT JOIN CA CAbied on BIED.Account = CAbied.Account
                    LEFT JOIN LedgerDocuments on LedgerDocuments.Period = billConDetail.OriginalPeriod
						and LedgerDocuments.PostSeq = billConDetail.OriginalPostSeq
						and LedgerDocuments.PKey = billConDetail.OriginalPKey
						and LedgerDocuments.TableName = billConDetail.OriginalTable ';
		IF CHARINDEX('vendorname', @Order) > 0 SET @sSQL += N'
					LEFT JOIN VE on BillConDetail.Vendor = VE.Vendor ';

		SET @sSQL += N'
				WHERE BillConDetail.MainWBS1 = ''' + @Wbs1 + N''' ';
		IF @ShowJTD = 0 SET @sSQL += N'and BillConDetail.Invoice = ''' + @Invoice + N''' ';
		SET @sSQL += N'
					and IsNull(CAap.Type, CAbied.Type) in ' +
					(CASE WHEN @ShowDirect = 1 THEN N'(5,6,7,8)' ELSE N'(5,6)' END); --reimb only default

		IF @SuppressbillOnly = 1 SET @sSQL += N'
					and IsNull(LedgerAP.SuppressBill, IsNull(BIED.SuppressBill, ''N'')) = ''N'' ';
		IF @FilterPrintWithInvoice = 1 SET @sSQL += N'
					and LedgerDocuments.PrintWithInvoice = ''Y'' ';
		IF ISNULL(@Minimum, '') <> '' SET @sSQL += N'
					and BillConDetail.Amount >= ' + @Minimum;
	END;

	IF @sSQL <> '' BEGIN
		-- Keep ordering in sync with subconsultants and expenses
		IF @Order = '' OR @Order = 'default' OR @Order = 'invoice' SET @sSQL += N'
			ORDER BY BillConDetail.BillWBS1, BillConDetail.BillWBS2, BillConDetail.BillWBS3,
				BillConDetail.SortKey, BillConDetail.SortKey2 ';
		ELSE IF CHARINDEX('wbs', @Order) > 0 SET @sSQL += N'
			ORDER BY BillConDetail.BillWBS1, BillConDetail.BillWBS2, BillConDetail.BillWBS3,
				BillConDetail.TransDate ';
		ELSE IF CHARINDEX('date', @Order) > 0 SET @sSQL += N'
			ORDER BY BillConDetail.TransDate ';
		ELSE IF CHARINDEX('vendorname', @Order) > 0 SET @sSQL += N'
			ORDER BY VE.Name, BillConDetail.TransDate ';
		ELSE IF CHARINDEX('vendor', @Order) > 0 SET @sSQL += N'
			ORDER BY BillConDetail.Vendor, BillConDetail.TransDate ';
		ELSE IF CHARINDEX('amountd', REPLACE(@Order, ' ', '')) = 1 SET @sSQL += N'
			ORDER BY BillConDetail.Amount Desc';
		ELSE IF CHARINDEX('amount', @Order) > 0 SET @sSQL += N'
			ORDER BY BillConDetail.Amount ';
		-- Same as first since default
		ELSE SET @sSQL += N'
			ORDER BY BillConDetail.BillWBS1, BillConDetail.BillWBS2, BillConDetail.BillWBS3,
				BillConDetail.SortKey, BillConDetail.SortKey2 ';
	END;
	SET @sSQL += ' ' + @XML_String;

	--PRINT @sSQL;
	EXEC (@sSQL);
END;
GO
