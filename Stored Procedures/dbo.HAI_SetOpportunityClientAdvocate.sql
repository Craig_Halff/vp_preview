SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_SetOpportunityClientAdvocate] @OpportunityID varchar(32)
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;
/*
    Copyright (c) 2020 Halff Associates Inc. All rights reserved.
    Sets Client Advocate for Opportunities based on Client change

    20201112	Craig H. Anderson
*/

BEGIN
    BEGIN TRANSACTION;
    UPDATE  ox
    SET ox.CustClientAdvocate = cx.CustClientAdvocate
    FROM    dbo.OpportunityCustomTabFields AS ox
    INNER JOIN dbo.Opportunity             AS o
        ON ox.OpportunityID = o.OpportunityID
    INNER JOIN dbo.CL                      AS c
        ON o.ClientID = c.ClientID
    INNER JOIN dbo.ClientCustomTabFields   AS cx
        ON o.ClientID = cx.ClientID
    WHERE   ox.OpportunityID = @OpportunityID;
    COMMIT TRANSACTION;
END;
GO
