SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_LoadLiabCode] (
	@visionMultiCompany		bit,
	@hasVendor				bit,
	@vendor					Nvarchar(20),
	@company				Nvarchar(14)
) AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_LoadContracts] 1, '', 1
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max)

	SET @sql = N'
		SELECT l.code, min(l.Description ' +
			(CASE WHEN @visionMultiCompany = 1 THEN ' + '' ('' + ISNULL(l.Org, l.company) +'')''' ELSE '' END) +
			'), ' +
			CASE
				WHEN @hasVendor = 1 THEN ' max(case when ve.category is null then null else c.liabcode end)'
				ELSE ''''' as vedef'
			END + '
			FROM CFGAPLiability l ' +
			CASE
				WHEN @hasVendor = 1 THEN '
				LEFT JOIN VE on ve.Vendor = N''' + @vendor + '''
				LEFT JOIN CFGvendorTypeAPLiability c on ve.Category = c.Type AND l.Company = c.Company '
				ELSE ''
			END;

    IF @visionMultiCompany = 1 AND ISNULL(@company,'') <> ''
        SET @sql = @sql + '
			WHERE l.Company = N''' + @company + ''' ';

    SET @sql = @sql + '
			GROUP BY l.code
			ORDER BY 2';

	PRINT @sql
	EXEC(@sql);
END;

GO
