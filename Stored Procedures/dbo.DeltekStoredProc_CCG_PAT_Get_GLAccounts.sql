SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Get_GLAccounts] ( @GLStatusActiveOnly bit, @AllAccountTypes bit, @Company nvarchar(20)= N' ', @RequiredAccounts nvarchar(max)= '')
             AS EXEC spCCG_PAT_Get_GLAccounts @GLStatusActiveOnly,@AllAccountTypes,@Company,@RequiredAccounts
GO
