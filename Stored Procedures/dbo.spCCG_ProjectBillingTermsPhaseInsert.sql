SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectBillingTermsPhaseInsert] @WBS1 varchar(32), @WBS2 varchar(7)
AS
/*
Copyright (c) 2017 Central Consulting Group. All rights reserved.
04/03/2017 David Springer
           Add to the billing terms Fee grid (BTF) when a new phase is checked for Lump Sum Phase.
           Call from Project CHANGE workflow when Lump Sum Phase is checked.
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
DECLARE @BTWBS2 varchar (7)
BEGIN
   Select @BTWBS2 = Min (WBS2) From BT Where WBS1 = @WBS1 and FeeMeth = '3'
If @BTWBS2 is not null
   Begin
   -- Delete BTF phases that are no longer checked as Lump Sum Phase
      Delete b
	  From ProjectCustomTabFields p, BTF b
	  Where b.WBS1 = @WBS1
	    and b.WBS2 = @BTWBS2
		and b.WBS1 = p.WBS1
		and p.CustLumpSumPhase = 'N'
		and p.WBS2 = b.PostWBS2
		and p.WBS3 = ' ' -- phase level checkbox

   -- Set AP Default for Lump Sum Phase checkbox
      Update p
	  Set BillByDefaultConsultants = 'N',
          BillByDefaultOtherExp = 'N',
          BillByDefault = 'N'
      From PR p, ProjectCustomTabFields px
      Where p.WBS1 = @WBS1
        and p.WBS2 = @WBS2
        and p.WBS3 = ' '
        and p.WBS1 = px.WBS1
        and p.WBS2 = px.WBS2
        and p.WBS3 = px.WBS3
		and px.CustLumpSumPhase = 'Y'

   -- Set AP Default for Lump Sum Phase checkbox at lowest level below level of Billing Type
      Update p
      Set BillByDefaultConsultants = 'N',
          BillByDefaultOtherExp = 'N',
          BillByDefault = 'N'
      From PR p, ProjectCustomTabFields px
      Where p.WBS1 = @WBS1
        and p.Sublevel = 'N'
        and p.WBS1 = px.WBS1
        and p.WBS2 = px.WBS2
        and px.WBS3 = ' '  -- phase level for checkbox
		and px.CustLumpSumPhase = 'Y'

	  Update ProjectCustomTabFields
	  Set CustFeeType = 'Lump Sum'
      Where WBS1 = @WBS1
        and WBS2 = @WBS2
		and CustLumpSumPhase = 'Y'

   -- Propagate to lower levels
	  Update p3
	  Set p3.CustFeeType = 'Lump Sum'
	  From ProjectCustomTabFields p2, ProjectCustomTabFields p3
      Where p2.WBS1 = @WBS1
        and p2.WBS2 = CASE When @WBS2 = ' ' Then p2.WBS2 Else @WBS2 END
        and p2.WBS3 = ' ' -- phase level for checkbox
		and p2.CustLumpSumPhase = 'Y'
		and p2.WBS1 = p3.WBS1
		and p2.WBS2 = p3.WBS2
		and p3.WBS3 <> ' '  -- task level for sublevel

   -- Insert new Billing Phase records (Fee);  FeeMeth = 3-Percent Complete by Phase as Fixed Amount
      Insert Into BTF (WBS1, WBS2, WBS3, Seq, Phase, Name, Fee, PostWBS1, PostWBS2, PostWBS3)
      Select p.WBS1, @BTWBS2, ' ', 
        (Select IsNull (Max (Seq), 0) From BTF Where WBS1 = p.WBS1) + row_number () over (order by p.WBS2) Seq, 
        (Select IsNull (Max (Seq), 0) From BTF Where WBS1 = p.WBS1) + row_number () over (order by p.WBS2) Phase, 
         Left (p.WBS2 + ' - ' + p.Name, 40), p.FeeDirLab + p.FeeDirExp + p.ConsultFee, 
         p.WBS1 PostWBS1, 
         p.WBS2 PostWBS2,
         t.WBS3 PostWBS3
      From PR p, ProjectCustomTabFields px
	       Left Join
	       (Select WBS1, WBS2, Min (WBS3) WBS3 From PR Where WBS3 <> ' ' Group by WBS1, WBS2) t on t.WBS1 = px.WBS1 and t.WBS2 = px.WBS2
      Where p.WBS1 = @WBS1
	    and p.WBS2 = @WBS2
		and p.WBS3 = ' ' -- Phase level
        and p.WBS1 = px.WBS1
		and p.WBS2 = px.WBS2
		and p.WBS3 = px.WBS3
        and px.CustLumpSumPhase = 'Y'
        and not exists (Select * From BTF 
                        Where WBS1 = p.WBS1 and IsNull (PostWBS2, ' ') = p.WBS2)

		Update b
		Set b.FeeToDate = i.Amount,
			b.BilledJTD = i.Amount,
			b.PctComplete = 
			CASE When b.Fee = 0 Then 0
				Else i.Amount / b.Fee * 100
			END
		From BTF b, 
			(Select WBS1, WBS2, - sum (Amount) Amount
				From LedgerAR
				Where TransType = 'IN'
				and RefNo <> 'Auto'
				and IsNull (SubType, '') <> 'R'
				and InvoiceSection <> 'T'  -- Tax
				Group by WBS1, WBS2) i
		Where b.WBS1 = @WBS1
		and b.WBS2 = @BTWBS2
		and b.WBS1 = i.WBS1
		and b.PostWBS2 = @WBS2
		and b.PostWBS2 = i.WBS2
   End
END
GO
