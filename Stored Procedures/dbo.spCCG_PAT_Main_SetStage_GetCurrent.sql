SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_Main_SetStage_GetCurrent]
	@seq			int,
	@pendingseq		int = 0,
	@modDate		datetime,
	@oldStage		varchar(200)		-- not currently used
AS
BEGIN
	-- EXEC [spCCG_PAT_Main_SetStage_GetCurrent] 1,null
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	if ISNULL(@pendingseq, 0) = 0
	BEGIN
		SELECT case when ISNULL(nextRoute.Stage,'') <> '' then nextRoute.Stage else p.Stage end as VisibleStage, 
				p.Stage, convert(Nvarchar(25), ModDate, 25) as ModDate, ModUser
			FROM CCG_PAT_Payable p
				LEFT JOIN (
					SELECT PayableSeq, MIN(SortOrder) as nextRouteOrder
						FROM CCG_PAT_Pending
						WHERE PayableSeq = @seq
						GROUP BY PayableSeq
				) summary on p.Seq = summary.PayableSeq
				LEFT JOIN CCG_PAT_Pending nextRoute
					on summary.PayableSeq = nextRoute.PayableSeq and summary.nextRouteOrder = nextRoute.SortOrder
			WHERE p.Seq = @seq and isnull(p.ModDate, '1900-01-01') > @modDate				-- we only care if the mod dates don't match
	END
	ELSE
	BEGIN
		-- New 4.0 : need to know the specific pending record especially for parallel, but should be ok for all stages changes that are to update the pending list
		SELECT case when ISNULL(nextRoute.Stage,'') <> '' then nextRoute.Stage when nextRoute.Seq IS NULL then p.Stage else nextStage.Stage end as VisibleStage, 
				p.Stage, convert(Nvarchar(25), ModDate, 25) as ModDate, ModUser
			FROM CCG_PAT_Payable p
			LEFT JOIN (select top 1 stage from CCG_PAT_ConfigStages where status = 'A' and Type = 'NEXT' order by SortOrder) nextStage on nextStage.Stage is not null
			LEFT JOIN CCG_PAT_Pending nextRoute
					on p.Seq = nextRoute.PayableSeq and nextRoute.Seq = @pendingseq
			WHERE p.Seq = @seq and isnull(p.ModDate, '1900-01-01') > @modDate				-- we only care if the mod dates don't match
	END
END
GO
