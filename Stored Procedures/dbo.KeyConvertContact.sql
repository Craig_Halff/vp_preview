SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyConvertContact]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg Nvarchar(1000) output,
@UserName Nvarchar(32) = 'VisionKeyConvert', -- Add by DP 4.0
@Period integer = 0,
@EntityType Nvarchar(20)
as
begin -- Procedure
	set nocount on
	declare @OldValue Nvarchar(32)
	declare @NewValue Nvarchar(32)
	Declare @Existing integer
	declare @Entity Nvarchar(20)
	declare @length integer
	declare @CvtType integer
	declare @KeysFetch integer
	declare @RetVal integer
	declare @diag smallint
	declare @Errmsg2 Nvarchar(1000),
			@custlabel	Nvarchar(40),
			@Custlabelplural Nvarchar(40)
	declare @TableName 		Nvarchar(100)
	declare @Sql 				Nvarchar(1000)
	declare @message nvarchar(max)

	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'Contact'
	set @length = 32
	set @Existing = 0
	set @KeysFetch = 0

	set @diag = 0
	execute InsertKeyCvtDriver @Entity -- populate driver table
	delete from keyConvertDriver where entity = N'Contact' and columnname = N'Contact'
-- Add by DP 4.0
	delete from keyconvertDriver where entity = N'Contact' and TableName = N'Contacts'
	DECLARE @OldName Nvarchar(125)
	DECLARE @NewName Nvarchar(125)
	DECLARE @PKey Nvarchar(50)
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0
--
	select @custlabel = LabelValue from FW_CFGLabels where LabelName = N'contactLabel'
	select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = N'contactLabelPlural'

	declare KeysCursor cursor for
		select OldKey, NewKey, Pkey from KeyConvertWork where Entity = @Entity
	open KeysCursor
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue,
		@PKey

	set @KeysFetch = @@Fetch_Status

While (@KeysFetch = 0)
	begin
		begin tran
-- Add by DP 4.0
		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany Nvarchar(14)
			DECLARE @RecordCount integer

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				set @RecordCount = (select count(*) from KeyConvertWork where Entity = @Entity)

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),N'KC','Y',  @custlabel + ' Key Convert ('+rtrim(convert(Nvarchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, left(@custlabel + ' Key Convert', 40), @custlabel);
			end

			Set @PostControlFlag = 1
		end
		set @Existing = 0
		select @Existing =  1, @NewName = IsNull(LastName, N'') + IsNull(', ' + FirstName, N'') from Contacts where ContactID = @NewValue
		select @OldName = IsNull(LastName, N'') + IsNull(', ' + FirstName, N'') from Contacts where ContactID = @OldValue
--

	If (@Existing = 1)
		begin
			  if (@DeleteExisting = 0)
				begin
				SET @message = dbo.GetMessage('MsgNumAnExistingConvWillNotCont',@custlabel,@NewValue,@custlabel,@custLabelPlural,'','','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end
				if (@NewValue = @OldValue)
					begin
					SET @message = dbo.GetMessage('MsgAreSameNumConvNotCont',@custlabelPlural,'','','','','','','','')
					RAISERROR(@message,16,3)
						close keysCursor
						deallocate keysCursor
						if (@@Trancount > 0)
						   ROLLBACK TRANSACTION
						return(50002) -- user defined error
					end

/* Removed by DP 4.0
			  exec @RetVal = KeyCvt @Entity = @Entity,
											@CvtType = @CvtType,
											@Length = 32,
											@ErrMsg = @ErrMsg2 output,
											@Diag = @Diag,
											@ConstrFlag = 3 -- UpdateTables

*/
/*new for 2.0*/
/*
ContactActivity        ActivityID
ContactActivity        ContactID
*/

				delete from ContactActivity where contactID = @OldValue 
							and exists (select 'x' from ContactActivity new where 
									ContactActivity.ActivityID = new.ActivityID and	
									new.ContactID = @newValue)
/*
ContactsFromLeads      LeadID
ContactsFromLeads      ContactID
*/
				delete from ContactsFromLeads where contactID = @OldValue 
							and exists (select 'x' from ContactsFromLeads new where 
									ContactsFromLeads.LeadID = new.LeadID and	
									new.ContactID = @newValue)
/*
PRContactAssocTemplate WBS1
PRContactAssocTemplate ContactID
*/
				delete from PRContactAssocTemplate where contactID = @OldValue 
							and exists (select 'x' from PRContactAssocTemplate new where 
									PRContactAssocTemplate.WBS1 = new.WBS1 and	
									new.ContactID = @newValue)
/*end 2.0*/
				
				delete from ContactCustomTabFields where ContactID = @OldValue and exists 
					(select 'x' from ContactCustomTabFields where  ContactID = @newValue)

				delete from ContactFileLinks where contactID = @OldValue 
							and exists (select 'x' from ContactFileLinks new where ContactFileLinks.LinkID = new.LinkID and	
																											new.ContactID = @newValue)

				Delete from ContactMoreInfo where ContactID = @OldValue 
					and exists (select 'x' from ContactMoreInfo new where new.ContactID = @newValue and
																			 ContactMoreInfo.Category = new.Category)

-- Removed by DP
--				delete from Contacts where ContactID = @OldValue

				Delete from ContactSubscr where ContactID = @OldValue 
					and exists (select 'x' from ContactSubscr new where new.ContactID = @newValue and
																			 ContactSubscr.UserName = new.UserName)
				Delete from ContactToContactAssoc where FromContactID = @OldValue 
					and exists (select 'x' from ContactToContactAssoc new where new.FromContactID = @newValue and
																			 ContactToContactAssoc.ToContactID = new.ToContactID)

				Delete from ContactToContactAssoc where ToContactID = @OldValue 
					and exists (select 'x' from ContactToContactAssoc new where new.ToContactID = @newValue and
																			 ContactToContactAssoc.FromContactID = new.FromContactID)

				Delete from CustomProposalContact where ContactID = @OldValue 
					and exists (select 'x' from CustomProposalContact new where new.ContactID = @newValue and
																			 CustomProposalContact.CustomPropID = new.CustomPropID and
																			 CustomProposalContact.SectionID = new.SectionID)


--DeletedContacts
--ContactID
				Delete from EMContactAssoc where ContactID = @OldValue 
					and exists (select 'x' from EMContactAssoc new where new.ContactID = @newValue and
																			 EMContactAssoc.Employee = new.Employee)
				Delete from MktCampaignContactAssoc where ContactID = @OldValue 
					and exists (select 'x' from MktCampaignContactAssoc new where new.ContactID = @newValue and
																			 MktCampaignContactAssoc.CampaignID = new.CampaignID)
				Delete from PRContactAssoc where ContactID = @OldValue 
					and exists (select 'x' from PRContactAssoc new where new.ContactID = @newValue and
																			 PRContactAssoc.WBS1 = new.WBS1)
			   Delete from ContactPhoto where ContactID = @OldValue
					and exists (select 'x' from ContactPhoto new where new.ContactID = @newValue)

			   Delete from WorkflowContactActivity where ContactID = @OldValue
					and exists (select 'x' from WorkflowContactActivity new where new.ContactID = @newValue and
																			 WorkflowContactActivity.ActivityID = new.ActivityID)

					/*handle duplicate custom tab records, possible when copying one project to another*/
				declare custTable cursor for
				select tableName from FW_CustomGrids where infocenterarea = N'Contacts'
				
				open custTable
				fetch next from custTable into @TableName
				while (@@FETCH_STATUS = 0)
				begin
					set @Sql = 'Delete FROM ' + @TableName + ' WHERE ' + @TableName + '.ContactID =N''' +  @OldValue + ''' and ' +
								  'exists (select ''x'' from ' + @TableName + ' new where new.ContactID = N''' + @NewValue + ''' and new.seq = ' + @TableName + '.seq)'
					if (@Diag = 1)
						print @Sql
					execute(@Sql)
					fetch next from custTable into @TableName
				end
				close custTable
				deallocate custTable		
	    END --Existing
-- Add by DP 4.0
		Else	-- Not Existing
		begin
			select * into #TempKeyConvert from Contacts where ContactID = @OldValue
			Update #TempKeyConvert set ContactID=@NewValue
			Insert Contacts select * from #TempKeyConvert
			Drop Table #TempKeyConvert
			set @NewName = @OldName
		end

		exec @RetVal = KeyCvt @Entity = @Entity,
					 @CvtType = @CvtType,
					 @Length = 32,
					 @ErrMsg = @ErrMsg2 output,
					 @Diag = @Diag,
					 @NewValue = @NewValue,
					 @OldValue = @OldValue,
					 @ConstrFlag = 3 -- UpdateTables
		delete from Contacts where ContactID = @OldValue
--
		if (@RetVal = 0)
			begin
-- Add by DP 4.0
				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq)
					select PKey, Entity, OldKey, NewKey, @OldName, @NewName, @Period, @LastPostSeq from keyConvertWork where Pkey = @PKey
				end
--
				delete from keyConvertWork where current of keysCursor
				commit Transaction
			end
		else
			begin
				set @ErrMsg = @ErrMsg + @ErrMsg2
				rollback transaction
			end
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue,
		@PKey
	set @KeysFetch = @@Fetch_Status
	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	return(0)
End -- Procedure
GO
