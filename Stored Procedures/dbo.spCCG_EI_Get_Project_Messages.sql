SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_Get_Project_Messages]
	@prnamelen		int,
	@wbs1List		Nvarchar(max)
AS
BEGIN
	-- DEBUG TEST: EXEC [dbo].[spCCG_EI_Get_Project_Messages] 10, '''2002002.00'',''2003005.00''', 0
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT Seq, WBS1, Name, Employee, EnteredBy, DateEntered, ShortMessage, Role, Message, DaysOld
		FROM (
			SELECT M.Seq, M.WBS1, LEFT(PR.Name, @prnamelen) as Name, EM.Employee as Employee,
				IsNull(EM.LastName, '') + ', ' +
					Case
						When Len(PreferredName) > 0 Then PreferredName
						Else IsNull(EM.FirstName, '')
					End as EnteredBy,
				DateEntered,
				CASE
					WHEN LEN(convert(VARCHAR(100), Message)) > 65 THEN Left(convert(VARCHAR(100), Message), 62) + '...'
					ELSE Message
				END as ShortMessage,
				Role, Message, Datediff(day, DateEntered, Current_timestamp) as DaysOld
			FROM CCG_EI_Messages M
				LEFT JOIN PR on PR.WBS1 = M.WBS1 and PR.WBS2 = N' ' and PR.WBS3 = N' ' 
				LEFT JOIN EM on EM.Employee = M.EnteredBy
			where M.WBS1 = @wbs1List
		) ShowMessages
	ORDER BY DateEntered desc
END
GO
