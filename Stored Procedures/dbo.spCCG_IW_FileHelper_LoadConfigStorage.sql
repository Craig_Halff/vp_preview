SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--exec [spCCG_IW_FileHelper_LoadConfigStorage]
CREATE PROCEDURE [dbo].[spCCG_IW_FileHelper_LoadConfigStorage]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	
	declare @EI varchar(1) = 'N'
	if exists(SELECT 'x' as result FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CCG_EI_ConfigStorage]') AND type in (N'U'))
		set @EI = 'Y'

	declare @ARM varchar(1) = 'N'
	if exists(SELECT 'x' as result FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CCG_ARM_ConfigStorage]') AND type in (N'U'))
		set @ARM = 'Y'

	declare @sql nvarchar(1000) = ''


	if @EI = 'Y'
		set @sql = N'SELECT ''EI''+Convert(varchar(20),[Seq]) as SourceSeq,[Seq]
      ,[Description]
      ,[UseType]
      ,[StorageType]
      ,[Server]
      ,[Catalog]
      ,[Root]
      ,[UserName]
      ,[Password]
      ,[AllowDelete]
      ,[RevisionType]
      ,[MarkupType]
      ,[Status]
      ,[DateChanged]
      ,[ChangedBy]
      ,[Detail] FROM CCG_EI_ConfigStorage '
	if @EI = 'Y' and @ARM = 'Y'
		set @sql = @sql + N' 
		UNION ALL '
	if @ARM = 'Y'
		set @sql = @sql + N'SELECT ''ARM''+Convert(varchar(20),[Seq]) as SourceSeq,[Seq]
      ,[Description]
      ,[UseType]
      ,[StorageType]
      ,[Server]
      ,[Catalog]
      ,[Root]
      ,[UserName]
      ,[Password]
      ,[AllowDelete]
      ,[RevisionType]
      ,[MarkupType]
      ,[Status]
      ,[DateChanged]
      ,[ChangedBy]
      ,[Detail] FROM CCG_ARM_ConfigStorage '

	if LEN(@sql) > 10
	BEGIN
		set @sql = @sql + N' order by 1'
		EXEC(@sql)
	END
END;

GO
