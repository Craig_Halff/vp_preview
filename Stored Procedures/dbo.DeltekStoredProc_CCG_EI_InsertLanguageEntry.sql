SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_InsertLanguageEntry] ( @Group1 varchar(255), @Id varchar(255), @Product varchar(255), @UICultureName varchar(20), @Value nvarchar(255))
             AS EXEC spCCG_EI_InsertLanguageEntry @Group1,@Id,@Product,@UICultureName,@Value
GO
