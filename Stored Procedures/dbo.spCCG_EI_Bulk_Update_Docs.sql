SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_Bulk_Update_Docs]
	@deleteFileIds		varchar(max),
	@insertValuesSql	Nvarchar(max) = '',
	@insertPkgValuesSql	Nvarchar(max) = '',
	@updateValuesSql	Nvarchar(max) = ''
AS
BEGIN
	/*
	TEST CASE:
	spCCG_EI_Bulk_Update_Docs
		@insertValuesSql = '',
		@insertPkgValuesSql = '',
		@updateValuesSql = '(''CCG Electronic Invoicing 4.0 Release Notes.pdf'', ''15d459ac-c86e-4c6e-9118-80ce75620d4d''), (''Vision APIs Insight 2015bb.pdf'', ''2af59d48-9789-44f9-9f3f-b1c9761c5b27''), (''CCG Electronic Invoicing 4.2 - PDF Toolbar.pdf'', ''b0904588-7fe1-4817-84b1-bba6f92cbb4f''), (''SampleContract.pdf'', ''b4bdc8f0-6374-487c-9c51-8b71523787d4''), (''PS-35-NgAlexander-TechTitansMastering VisionAPIs-FINAL.pdf'', ''f3e3cf05-428b-4ec5-95d8-9e7c04fc4b7c'')',
		@deleteFileIds = ''
	*/
	SET NOCOUNT ON
	DECLARE @i			int
	DECLARE @newI		int
	DECLARE @fileId		varchar(50)
	DECLARE @execString varchar(max)

	BEGIN TRANSACTION

	-- Do document inserts
	IF @insertValuesSql <> '' AND @insertValuesSql IS NOT NULL BEGIN
		SET @execString = N'
			INSERT INTO CCG_DOCS_PR
				(FILEID, WBS1, WBS2, WBS3, FilePath, FileDescription, Filesize, StorageSeq, StoragePathOrID, CreateUser, CreateDate)
				VALUES ' + @insertValuesSql
		EXEC (@execString)
	END

	-- Do document packaging inserts
	IF @insertPkgValuesSql <> '' AND @insertPkgValuesSql IS NOT NULL BEGIN
		declare @VP3 bit = (case when EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Projects_CustFinalInvPkgUserDocuments]')) then 1 else 0 end)
		SET @execString = N'
			INSERT INTO Projects_'+(case when @VP3=1 then 'Cust' else '' end)+'FinalInvPkgUserDocuments
				(WBS1, WBS2, WBS3, Seq, CreateUser, CreateDate, ModUser, ModDate, CustOrder, CustDocType, CustDocument)
				VALUES ' + (case when @VP3=1 
					then replace(@insertPkgValuesSql, 'Projects_FinalInvPkgUserDocuments', 'Projects_CustFinalInvPkgUserDocuments') 
					else replace(@insertPkgValuesSql, 'Projects_CustFinalInvPkgUserDocuments', 'Projects_FinalInvPkgUserDocuments') end)
		EXEC (@execString)
	END

	-- Do document updates
	IF @updateValuesSql <> '' AND @updateValuesSql IS NOT NULL BEGIN			
		SET @execString = N'
			UPDATE docs
				SET FileDescription = temp.FileDescription
				FROM CCG_DOCS_PR docs
				JOIN (
					VALUES ' + @updateValuesSql + N'
				) temp (FileDescription, FileId) ON temp.FileId = docs.FileId'
		EXEC (@execString)
		--PRINT @execString;
	END

	-- Do document deletes
	SET @i = 0
	BEGIN TRY
		WHILE @i >= 0 And @i < Len(@deleteFileIds) BEGIN
			SET @newI = CHARINDEX('|', @deleteFileIds, @i + 1)
			IF @newI <= 0 SET @newI = Len(@deleteFileIds) + 1
			SET @fileId = SUBSTRING(@deleteFileIds, @i, @newI - @i)

			--PRINT @fileId
			DELETE FROM CCG_DOCS_PR WHERE FILEID = @fileId

			SET @i = @newI + 1
		END
	END TRY
	BEGIN CATCH
		SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_MESSAGE() AS ErrorMessage
	END CATCH

	COMMIT TRANSACTION
END
GO
