SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_ClearMessages]
	@clearType		varchar(100),
	@detail			Nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON

	IF @clearType = 'Selected'
		DELETE FROM CCG_EMAIL_MESSAGES
			WHERE SourceApplication = 'Payables Approval Tracking' and ToList = @detail;
	ELSE IF @clearType = 'NotVisionEmployee'
		DELETE FROM CCG_EMAIL_MESSAGES
			WHERE SourceApplication = 'Payables Approval Tracking'
				and ToList not in (SELECT distinct email from EM WHERE not email is null)
	ELSE IF @clearType = 'Before'
		DELETE FROM CCG_EMAIL_MESSAGES
			WHERE SourceApplication = 'Payables Approval Tracking' and CreateDateTime <= @detail
	ELSE IF @clearType = 'After'
		DELETE FROM CCG_EMAIL_MESSAGES
			WHERE SourceApplication = 'Payables Approval Tracking' and CreateDateTime >= @detail
	ELSE IF @clearType = 'All'
		DELETE FROM CCG_EMAIL_MESSAGES
			WHERE SourceApplication = 'Payables Approval Tracking'

	SELECT @@ROWCOUNT as RowsAffected;
END;

GO
