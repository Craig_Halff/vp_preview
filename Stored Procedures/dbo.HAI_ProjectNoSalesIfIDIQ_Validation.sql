SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_ProjectNoSalesIfIDIQ_Validation]
    @WBS1 VARCHAR(32)
  , @ContractType VARCHAR(32)
AS
/* =======================================================================================
    Halff Associates, Inc.   All rights reserved.
    20200624 - Craig H. Anderson
            Validate that the Sales grid is empty for IDIQ/On Call/MSA/GSA type contracts.
   ======================================================================================= */
SET NOCOUNT ON;
    BEGIN
        IF EXISTS
            (
                SELECT 'x'
                FROM dbo.PR p
                    INNER JOIN
                        (
                            SELECT ps.WBS1
                                 , SUM(ps.CustSalesAmount) AS Sales
                            FROM dbo.Projects_Sales ps WITH ( NOLOCK )
                            GROUP BY ps.WBS1
                        )   s
                        ON p.WBS1 = s.WBS1
                WHERE p.WBS1 = @WBS1
                      AND @ContractType <> 'Individual Project/Task Order'
                      AND ISNULL(s.Sales, 0) <> 0
            )
            RAISERROR('Only "Individual Project/Task Order" type contracts may have Sales data.', 16, 1);
    END;
GO
