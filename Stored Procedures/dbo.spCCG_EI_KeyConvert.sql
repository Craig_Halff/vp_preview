SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_KeyConvert](@What varchar(32), @OldValue Nvarchar(32), @NewValue Nvarchar(32), @WBS1 Nvarchar(30), @WBS2 Nvarchar(7))
AS BEGIN
/*
	Copyright (c) 2017 Central Consulting Group. All rights reserved.

	@What = WBS1, WBS2, WBS3, Stage, Stage Flow

	select * from CCG_TR where OriginalWBS1 in ('2003005.00', 'xyz')
	exec spCCG_EI_KeyConvert 'WBS1', 'xyz','2003005.00', '', ''
	select * from CCG_TR where OriginalWBS1 in ('2003005.00', 'xyz')

	select InvoiceStage,count(*) from CCG_EI group by InvoiceStage order by 1
	select Stage,count(*) from CCG_EI_ConfigStages group by Stage order by 1
	select Stage,count(*) from CCG_EI_ConfigRights group by Stage order by 1
	select Stage,count(*) from CCG_EI_ConfigStageFlows group by Stage order by 1
	spCCG_EI_KeyConvert 'Stage Flow', 'Default', 'Approval Required'
*/
	set nocount on
	declare @NumRowsAffected int
	set @NumRowsAffected=0
	print @What
	BEGIN TRY
		BEGIN TRAN
		if @What = 'WBS1'
		begin
			update CCG_EI_Messages				set WBS1=@NewValue where WBS1=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_History				set WBS1=@NewValue where WBS1=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI						set WBS1=@NewValue where WBS1=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_CustomColumns			set WBS1=@NewValue where WBS1=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_HistoryUpdateColumns	set WBS1=@NewValue where WBS1=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_Pending				set WBS1=@NewValue where WBS1=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT			
			update CCG_EI_Delegation			set ForWBS1=@NewValue where ForWBS1=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_FeePctCpl				set WBS1=@NewValue where WBS1=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT			
			update CCG_EI_HistoryDelegation		set ForWBS1=@NewValue where ForWBS1=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT			
			update CCG_EI_Renotify				set WBS1=@NewValue where WBS1=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT			
			update CCG_EI_XFDF					set WBS1=@NewValue where WBS1=@OldValue
		end
		else if @What = 'WBS2'
		begin
			update CCG_EI_CustomColumns			set WBS2=@NewValue where WBS1=@WBS1 and WBS2=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_HistoryUpdateColumns	set WBS2=@NewValue where WBS1=@WBS1 and WBS2=@OldValue
			
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_FeePctCpl				set WBS2=@NewValue where WBS1=@WBS1 and WBS2=@OldValue
		end
		else if @What = 'WBS3'
		begin
			update CCG_EI_CustomColumns			set WBS3=@NewValue where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_HistoryUpdateColumns	set WBS3=@NewValue where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@OldValue			
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_FeePctCpl				set WBS3=@NewValue where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@OldValue
		end
		else if @What = 'Stage'
		begin
			update CCG_EI					set InvoiceStage=@NewValue where InvoiceStage=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_History			set InvoiceStage=@NewValue where InvoiceStage=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_ConfigRights		set Stage=@NewValue where Stage=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_ConfigStageFlows	set Stage=@NewValue where Stage=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_ConfigStages		set Stage=@NewValue, StageLabel=@NewValue, StageDescription=@NewValue
				where Stage=@OldValue
				
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_Renotify			set InvoiceStage=@NewValue where InvoiceStage=@OldValue
		end
		else if @What = 'Stage Flow'
		begin
			declare @Version varchar(3)
			select @Version=Left(Version,3) from CFGSystem

			if Left(@Version,1) >= '7'
			begin
				update FW_CustomColumnValuesData set Code=@NewValue, DataValue=@NewValue
				where InfoCenterArea='Projects' and ColName='CustInvoiceStageFlow' and GridID='X' and Code=@OldValue
				set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			end
			else if Left(@Version,1) = '6'
			begin
				update CustomColumnValuesData set Code=@NewValue, DataValue=@NewValue
				where InfoCenterArea='Projects' and ColName='CustInvoiceStageFlow' and GridID='[FORM]' and Code=@OldValue
				set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			end
			else if Left(@Version,1) = '5'
			begin
				update CustomColumnValues set DataValue=@NewValue
				where InfoCenterArea='Projects' and ColName='CustInvoiceStageFlow' and GridID='[FORM]' and DataValue=@OldValue
				set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			end
			update ProjectCustomTabFields set CustInvoiceStageFlow = @NewValue where WBS2=' ' and CustInvoiceStageFlow=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT

			update CCG_EI_ConfigRights		set StageFlow=@NewValue where StageFlow=@OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_ConfigStageFlows	set StageFlow=@NewValue where StageFlow=@OldValue
		end
		else if @What = 'Employee'
		begin
			update CCG_EI_History 				set ActionTakenBy = @NewValue		where ActionTakenBy = @OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_History				set ActionRecipient = @NewValue		where ActionRecipient = @OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_History				set DelegateFor = @NewValue			where DelegateFor = @OldValue			
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_HistoryUpdateColumns	set ActionTakenBy = @NewValue		where ActionTakenBy = @OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_HistoryDelegation		set Employee = @NewValue			where Employee = @OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_HistoryDelegation		set Delegate = @NewValue			where Delegate = @OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_HistoryDelegation		set ActionTakenBy = @NewValue		where ActionTakenBy = @OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI						set ChangedBy = @NewValue			where ChangedBy = @OldValue		-- May be changed to USER eventually [JAM]
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_Pending				set Employee = @NewValue			where Employee = @OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_Renotify				set Employee = @NewValue			where Employee = @OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_Renotify				set ChangedBy = @NewValue			where ChangedBy = @OldValue		-- May be changed to USER eventually [JAM]
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_Delegation			set Employee = @NewValue			where Employee = @OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_Delegation 			set Delegate = @NewValue			where Delegate = @OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT
			update CCG_EI_Delegation 			set ApprovedBy = @NewValue			where ApprovedBy = @OldValue
			set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT			
			update CCG_EI_Messages				set EnteredBy = @NewValue			where EnteredBy = @OldValue
		end

		set @NumRowsAffected=@NumRowsAffected+@@ROWCOUNT

		declare @sql nvarchar(512), @parmDefinition nvarchar(500)
		if exists (select 'x' from sys.objects where object_id = OBJECT_ID(N'[dbo].[spCCG_TR_KeyConvert]') and type in (N'P', N'PC'))
		begin
			declare @WhatParm varchar(32)=@What, @OldValueParm Nvarchar(32)=@OldValue, @NewValueParm Nvarchar(32)=@NewValue, @WBS1Parm Nvarchar(30)=@WBS1, @WBS2Parm Nvarchar(7)=@WBS2, @NumRecsParm int=0
			set @sql = N'exec spCCG_TR_KeyConvert ' + '@What, @OldValue, @NewValue, @WBS1, @WBS2, @NumRecs OUTPUT'
			set @parmDefinition = N'@What varchar(32), @OldValue Nvarchar(32), @NewValue Nvarchar(32), @WBS1 Nvarchar(30), @WBS2 Nvarchar(7), @NumRecs int OUTPUT'
			execute sp_executesql @sql, @parmDefinition, @What=@WhatParm, @OldValue=@OldValueParm, @NewValue=@NewValueParm, @WBS1=@WBS1Parm, @WBS2=@WBS2Parm, @NumRecs=@NumRecsParm OUTPUT
			set @NumRowsAffected = @NumRowsAffected + @NumRecsParm
		end

		if exists (select 'x' from sys.objects where object_id = OBJECT_ID(N'[dbo].[spEVA_EvPay_KeyConvert]') and type in (N'P', N'PC'))
		begin
			declare @WhatParam varchar(32)=@What, @OldValueParam Nvarchar(32)=@OldValue, @NewValueParam Nvarchar(32)=@NewValue, @WBS1Param Nvarchar(30)=@WBS1, @NumRecsParam int=0
			set @sql = N'exec spEVA_EvPay_KeyConvert ' + '@What, @OldValue, @NewValue, @WBS1, @WBS2, @NumRecs OUTPUT'
			set @parmDefinition = N'@What varchar(32), @OldValue Nvarchar(32), @NewValue Nvarchar(32), @WBS1 Nvarchar(30), @NumRecs int OUTPUT'
			execute sp_executesql @sql, @parmDefinition, @What=@WhatParam, @OldValue=@OldValueParam, @NewValue=@NewValueParam, @WBS1=@WBS1Param, @NumRecs=@NumRecsParam OUTPUT
			set @NumRowsAffected = @NumRowsAffected + @NumRecsParm
		end

		COMMIT TRAN
		select @NumRowsAffected as ReturnValue, '' as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		select -1 as  ReturnValue, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage
	END CATCH
END
GO
