SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[stRPMoveAssignment]
	@strPlanID nvarchar(32),
	@strParentTaskID varchar(32)	
AS

BEGIN
	DECLARE @strChildTaskID varchar(32)
	DECLARE @dtETCDate datetime
	DECLARE @dtJTDDate datetime
	DECLARE @strCompany nvarchar(14)
	DECLARE @strLaborCode nvarchar(14)
	DECLARE @strUserName nvarchar(42)
	DECLARE @assignmentCount as int
	DECLARE @WBS1 nvarchar(30) 
	DECLARE @WBS2 nvarchar(30) 
	DECLARE @WBS3 nvarchar(30)

	DECLARE @tabAssignment TABLE (
		PlanID varchar(32) COLLATE database_default,
		TaskID varchar(32) COLLATE database_default,
		AssignmentID varchar(32) COLLATE database_default
		UNIQUE(PlanID, TaskID, AssignmentID)
	  )

	DECLARE @tabAssignmentHasJTD TABLE (
		PlanID varchar(32) COLLATE database_default,
		TaskID varchar(32) COLLATE database_default,
		AssignmentID varchar(32) COLLATE database_default
		UNIQUE(PlanID, TaskID, AssignmentID)
	)

	DECLARE @tabAssignmentHasETC TABLE (
		PlanID varchar(32) COLLATE database_default,
		TaskID varchar(32) COLLATE database_default,
		AssignmentID varchar(32) COLLATE database_default
		UNIQUE(PlanID, TaskID, AssignmentID)
	)

	DECLARE @tabJTDLD TABLE (
		RowID int IDENTITY(1,1),
		WBS1 nvarchar(30) COLLATE database_default,
		WBS2 nvarchar(30) COLLATE database_default,
		WBS3 nvarchar(30) COLLATE database_default,
		Employee nvarchar(20) COLLATE database_default,
		PeriodHrs decimal(19,4)
		UNIQUE (RowID, WBS1, WBS2, WBS3, Employee)
	)

	SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

	--retrieve the first child labor code task under the parent task
	SELECT Top 1 @strchildtaskID = Child.TaskID,
				 @WBS1 = Child.WBS1,
				 @WBS2 = ISNULL(Child.WBS2,' '),
				 @WBS3 = ISNULL(Child.WBS3,' '),
				 @strLaborCode = Child.LaborCode
		From PNTask Child 
		INNER JOIN PNTask Parent ON Child.PlanID = Parent.PlanID 
			AND Child.ParentOutlineNumber = Parent.OutlineNumber
		WHERE Parent.TaskID = @strParentTaskID AND  Parent.PlanID = @strPlanID
		ORDER BY Child.OutlineNumber			

	IF @strchildtaskID IS NULL Return 

	--Move all basline data into the child level
	UPDATE PNBaselineLabor Set TaskID = @strChildTaskID, ModDate = GETUTCDATE(),ModUser =  @strUserName
		WHERE TaskID = @strParentTaskID AND PlanID = @strPlanID
	UPDATE RPBaselineLabor SET TaskID = @strChildTaskID, ModDate = GETUTCDATE(),ModUser =  @strUserName
	WHERE TaskID = @strParentTaskID AND PlanID = @strPlanID
	
	-- if no assignment  for the parent task, don't do anything
	SELECT @assignmentCount = Count(*) FROM PNAssignment WHERE TaskID = @strParentTaskID
	IF @assignmentCount = 0   Return

	--set JTD and ETC Date    
	SELECT @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE())))
    FROM CFGRMSettings

	SELECT @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)

-- Put all JTD records for the employee into the temparary table
	INSERT @tabJTDLD(
      WBS1,
      WBS2,
      WBS3,
      Employee,	  
      PeriodHrs
    )      
      SELECT
        WBS1,
        WBS2,
        WBS3,
        Employee,
        SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs
      FROM LD
      WHERE LD.TransDate <= @dtJTDDate AND WBS1 = @WBS1 AND WBS2 = @WBS2 AND WBS3 = @WBS3
      GROUP BY WBS1, WBS2, WBS3, Employee    

      UNION ALL
      SELECT
        WBS1,
        WBS2,
        WBS3,
        TD.Employee AS Employee,
        SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs
      FROM tkDetail AS TD 
      INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate AND TD.EmployeeCompany = TM.EmployeeCompany)
      WHERE   TD.TransDate <= @dtJTDDate AND WBS1 = @WBS1 AND WBS2 = @WBS2 AND WBS3 = @WBS3
      GROUP BY WBS1, WBS2, WBS3, TD.Employee    
  
      UNION ALL
       SELECT
        WBS1,
        WBS2,
        WBS3, 
        TD.Employee AS Employee,
        SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs
      FROM tsDetail AS TD
      INNER JOIN tsControl AS TC ON (TD.Batch = TC.Batch AND TC.Posted = 'N')  
      WHERE   TD.TransDate <= @dtJTDDate AND WBS1 = @WBS1 AND WBS2 = @WBS2 AND WBS3 = @WBS3
      GROUP BY WBS1, WBS2, WBS3, TD.Employee     

      ---Retrieve all Assginment which has JTD hours 
	  INSERT @tabAssignmentHasJTD(
      PlanID,
      TaskID,
      AssignmentID
    )      
	  SELECT DISTINCT ALLAssignments.PlanID AS PlanID,
	  ALLAssignments.TaskID AS TaskID,
	  ALLAssignments.AssignmentID AS AssignmentID FROM
	  (SELECT PlanID, TaskID, WBS1,WBS2,WBS3,AssignmentID, ResourceID FROM PNAssignment 
	     Where PlanID = @strPlanID and TaskID = @strParentTaskID) AS ALLAssignments
	  INNER JOIN @tabJTDLD JTD ON ALLAssignmentS.WBS1 = JTD.WBS1 AND
	  ISNULL(ALLAssignments.WBS2, ' ') =  JTD.WBS2 
	  AND  ISNULL(ALLAssignments.WBS3, ' ') =  JTD.WBS3 
	  AND ALLAssignmentS.ResourceID = JTD.Employee 

	  ---Retrieve all Assginment which has ETC hours 
	  INSERT @tabAssignmentHasETC(
      PlanID,
      TaskID,
      AssignmentID
    )      
	  SELECT DISTINCT ALLAssignments.PlanID AS PlanID,
	  ALLAssignments.TaskID AS TaskID,
	  ALLAssignments.AssignmentID AS AssignmentID FROM
		  (SELECT PlanID, TaskID, AssignmentID FROM PNAssignment 
			 Where PlanID = @strPlanID and TaskID = @strParentTaskID) 
	  AS ALLAssignments
	  INNER JOIN  
		(SELECT PlanID, TaskID, AssignmentID,
	    ROUND(ISNULL(
        CASE 
          WHEN TPD.StartDate >= @dtETCDate 
          THEN TPD.PeriodHrs
          ELSE TPD.PeriodHrs * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) 
        END 
        , 0), 2) AS PeriodHrs FROM PNPlannedLabor TPD
	  WHERE PlanID = @strPlanID and TaskID = @strParentTaskID) 
	  AS PlannedAssginments 
	  ON PlannedAssginments.PlanID = ALLAssignments.PlanID 
	  AND PlannedAssginments.TaskID = ALLAssignments.TaskID 
	  AND PlannedAssginments.AssignmentID = ALLAssignments.AssignmentID 
	  Where PeriodHrs > 0

	  --Only Move those assignments with JTD hours but no ETC hours
	UPDATE PNAssignment SET TaskID = @strChildTaskID, LaborCode = @strLaborCode, 
	  ModDate = GETUTCDATE(), ModUser =  @strUserName  	 
	  Where PNAssignment.PlanID = @strPlanID AND PNAssignment.TaskID = @strParentTaskID 	   
	  AND PNAssignment.AssignmentID  NOT IN -- the assignment not to have JTD hours but has no ETC hours
		(SELECT AssignmentID  FROM @tabAssignmentHasJTD JTD 
		 WHERE AssignmentID NOT IN 
		  (SELECT AssignmentID FROM @tabAssignmentHasETC))

	UPDATE RPAssignment SET TaskID = @strChildTaskID, LaborCode = @strLaborCode, 
	  ModDate = GETUTCDATE(), ModUser =  @strUserName   
	  Where RPAssignment.PlanID = @strPlanID AND RPAssignment.TaskID = @strParentTaskID 	   
	  AND RPAssignment.AssignmentID  NOT IN -- the assignment not to have JTD hours but has no ETC hours
		(SELECT AssignmentID  FROM @tabAssignmentHasJTD JTD 
		 WHERE AssignmentID NOT IN 
		  (SELECT AssignmentID FROM @tabAssignmentHasETC))

	UPDATE PNPlannedLabor SET TaskID = @strChildTaskID, 
	  ModDate = GETUTCDATE(), ModUser =  @strUserName 
	  Where PlanID = @strPlanID AND TaskID = @strParentTaskID 
	  AND AssignmentID  NOT IN 
		(SELECT AssignmentID  FROM @tabAssignmentHasJTD JTD 
		 WHERE AssignmentID NOT IN 
		  (SELECT AssignmentID FROM @tabAssignmentHasETC))

	UPDATE RPPlannedLabor SET TaskID = @strChildTaskID, 
	  ModDate = GETUTCDATE(), ModUser =  @strUserName 
	  Where PlanID = @strPlanID AND TaskID = @strParentTaskID 
	  AND AssignmentID  NOT IN 
		(SELECT AssignmentID  FROM @tabAssignmentHasJTD JTD 
		 WHERE AssignmentID NOT IN 
		  (SELECT AssignmentID FROM @tabAssignmentHasETC))		  
END

GO
