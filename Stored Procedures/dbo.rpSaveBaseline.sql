SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpSaveBaseline]
  @strPlanID VARCHAR(32)
AS

BEGIN -- Procedure rpSaveBaseline

  -->>> This procedure must be called after all of the recalculation is done.

  SET NOCOUNT ON
  
  -- Delete all Baseline time-phased data.
  
  DELETE RPBaselineLabor WHERE PlanID = @strPlanID
  DELETE RPBaselineExpenses WHERE PlanID = @strPlanID
  DELETE RPBaselineConsultant WHERE PlanID = @strPlanID
  DELETE RPBaselineUnit WHERE PlanID = @strPlanID
  
  -- Copy Planned time-phased to make new Baseline time-phased data.
  
 INSERT RPBaselineLabor
         (TimePhaseID, 
          PlanID, 
          TaskID,
          AssignmentID,
          StartDate, 
          EndDate, 
          CostRate,
          BillingRate, 
          PeriodHrs, 
          PeriodCost, 
          PeriodBill, 
          PeriodRev, 
          PeriodCount, 
          PeriodScale,
          CreateDate,
          ModDate)
    SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
           PlanID, 
           TaskID,
           AssignmentID,
           StartDate, 
           EndDate, 
           CostRate,
           BillingRate, 
           PeriodHrs, 
           PeriodCost, 
           PeriodBill, 
           PeriodRev, 
           PeriodCount, 
           PeriodScale,
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
    FROM RPPlannedLabor
    WHERE PlanID = @strPlanID
  
  INSERT RPBaselineExpenses
         (TimePhaseID, 
          PlanID, 
          TaskID,
          ExpenseID,
          StartDate, 
          EndDate, 
          PeriodCost, 
          PeriodBill, 
          PeriodRev, 
          PeriodCount, 
          PeriodScale,
          CreateDate,
          ModDate)
    SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
           PlanID, 
           TaskID,
           ExpenseID,
           StartDate, 
           EndDate, 
           PeriodCost, 
           PeriodBill, 
           PeriodRev, 
           PeriodCount, 
           PeriodScale,
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
    FROM RPPlannedExpenses
    WHERE PlanID = @strPlanID
  
  INSERT RPBaselineConsultant
         (TimePhaseID, 
          PlanID, 
          TaskID,
          ConsultantID,
          StartDate, 
          EndDate, 
          PeriodCost, 
          PeriodBill, 
          PeriodRev, 
          PeriodCount, 
          PeriodScale,
          CreateDate,
          ModDate)
    SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
           PlanID, 
           TaskID,
           ConsultantID,
           StartDate, 
           EndDate, 
           PeriodCost, 
           PeriodBill, 
           PeriodRev, 
           PeriodCount, 
           PeriodScale,
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
    FROM RPPlannedConsultant
    WHERE PlanID = @strPlanID
    
  INSERT RPBaselineUnit
         (TimePhaseID, 
          PlanID, 
          TaskID,
          UnitID,
          StartDate, 
          EndDate, 
          PeriodQty, 
          PeriodCost, 
          PeriodBill, 
          PeriodRev, 
          PeriodCount, 
          PeriodScale,
          CreateDate,
          ModDate)
    SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
           PlanID, 
           TaskID,
           UnitID,
           StartDate, 
           EndDate, 
           PeriodQty, 
           PeriodCost, 
           PeriodBill, 
           PeriodRev, 
           PeriodCount, 
           PeriodScale,
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate,
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
    FROM RPPlannedUnit
    WHERE PlanID = @strPlanID

  -- Update Plan.
  
  UPDATE RPPlan
    SET BaselineLaborHrs = PlannedLaborHrs,
        BaselineLabCost = PlannedLabCost,
        BaselineLabBill = PlannedLabBill,
        BaselineExpCost = PlannedExpCost,
        BaselineExpBill = PlannedExpBill,
        BaselineConCost = PlannedConCost,
        BaselineConBill = PlannedConBill,
        BaselineUntQty = PlannedUntQty,
        BaselineUntCost = PlannedUntCost,
        BaselineUntBill = PlannedUntBill,
        BaselineDirExpCost = PlannedDirExpCost,
        BaselineDirExpBill = PlannedDirExpBill,
        BaselineDirConCost = PlannedDirConCost,
        BaselineDirConBill = PlannedDirConBill,
        BaselineDirUntCost = PlannedDirUntCost,
        BaselineDirUntBill = PlannedDirUntBill,
        BaselineStart = StartDate,
        BaselineFinish = EndDate
    WHERE PlanID = @strPlanID

  -- Update Tasks.
  
  UPDATE RPTask
    SET BaselineLaborHrs = PlannedLaborHrs,
        BaselineLabCost = PlannedLabCost,
        BaselineLabBill = PlannedLabBill,
        BaselineExpCost = PlannedExpCost,
        BaselineExpBill = PlannedExpBill,
        BaselineConCost = PlannedConCost,
        BaselineConBill = PlannedConBill,
        BaselineUntQty = PlannedUntQty,
        BaselineUntCost = PlannedUntCost,
        BaselineUntBill = PlannedUntBill,
        BaselineDirExpCost = PlannedDirExpCost,
        BaselineDirExpBill = PlannedDirExpBill,
        BaselineDirConCost = PlannedDirConCost,
        BaselineDirConBill = PlannedDirConBill,
        BaselineDirUntCost = PlannedDirUntCost,
        BaselineDirUntBill = PlannedDirUntBill,
        BaselineStart = StartDate,
        BaselineFinish = EndDate
    WHERE PlanID = @strPlanID
  
  -- Update Assignments.
  
  UPDATE RPAssignment
    SET BaselineLaborHrs = PlannedLaborHrs,
        BaselineLabCost = PlannedLabCost,
        BaselineLabBill = PlannedLabBill,
        BaselineStart = StartDate,
        BaselineFinish = EndDate
    WHERE PlanID = @strPlanID
    
  -- Update Expenses.
  
  UPDATE RPExpense
    SET BaselineExpCost = PlannedExpCost,
        BaselineExpBill = PlannedExpBill,
        BaselineDirExpCost = PlannedDirExpCost,
        BaselineDirExpBill = PlannedDirExpBill,
        BaselineStart = StartDate,
        BaselineFinish = EndDate
    WHERE PlanID = @strPlanID
    
  -- Update Consultants.
  
  UPDATE RPConsultant
    SET BaselineConCost = PlannedConCost,
        BaselineConBill = PlannedConBill,
        BaselineDirConCost = PlannedDirConCost,
        BaselineDirConBill = PlannedDirConBill,
        BaselineStart = StartDate,
        BaselineFinish = EndDate
    WHERE PlanID = @strPlanID
  
  -- Update Units.
  
  UPDATE RPUnit
    SET BaselineUntQty = PlannedUntQty,
        BaselineUntCost = PlannedUntCost,
        BaselineUntBill = PlannedUntBill,
        BaselineDirUntCost = PlannedDirUntCost,
        BaselineDirUntBill = PlannedDirUntBill,
        BaselineStart = StartDate,
        BaselineFinish = EndDate
    WHERE PlanID = @strPlanID
  
  SET NOCOUNT OFF

END -- rpSaveBaseline
GO
