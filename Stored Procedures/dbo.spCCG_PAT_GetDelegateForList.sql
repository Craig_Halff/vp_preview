SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_GetDelegateForList] (
	@Employee			Nvarchar(20),
	@IncludeSelf		bit = 1
)
AS
BEGIN
/*
	Copyright (c) 2020 EleVia Software. All rights reserved.

	SELECT * FROM CCG_PAT_Delegation d
	EXEC [spCCG_PAT_GetDelegateForList] '00003', 1
*/
	SET NOCOUNT ON
	declare @ApprovalRequired char(1)
	SELECT @ApprovalRequired = c.DelegationApproval FROM CCG_PAT_Config c

	SELECT d.Employee as [Employee]
		FROM CCG_PAT_Delegation d
		WHERE d.Delegate = @Employee
			AND (ISNULL(d.ApprovedBy,N'') <> N'' OR @ApprovalRequired <> 'Y')
			AND GETDATE() BETWEEN d.FromDate AND d.ToDate
	UNION
	SELECT @Employee WHERE @IncludeSelf = 1
END
GO
