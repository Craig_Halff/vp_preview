SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Check_Entry_Column]
	@spName			Nvarchar(100),
	@wbs1			Nvarchar(30),
	@wbs2			Nvarchar(7),
	@wbs3			Nvarchar(7),
	@isValueString	bit,
	@newValue		Nvarchar(max)
AS
BEGIN
	/*
	DeltekStoredProc_CCG_EI_Check_Entry_Column
		@spName = 'spCCG_EI_HoldUntilDate_CHECK',
		@WBS1 = '2002005.00',
		@WBS2 = ' ',
		@WBS3 = ' ',
		@NewValue = '1/6/2016 12:00:00 AM',
		@isValueString = '1'
 	*/
	SET NOCOUNT ON;
	DECLARE @returnVal		int;
	DECLARE @returnMessage	Nvarchar(max);
	DECLARE @safeSql		int;

	-- Prevent SQL injection using the @newValue parameter
	IF @isValueString = 1 SET @newValue = REPLACE(@newValue, '''', '''''');

	-- Ensure that our inputs are safe (prevent SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @wbs1);
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @wbs2) * 2;
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @wbs3) * 4;
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<FIELD>', @spName) * 8;
	IF @isValueString = 0 SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<NUMBER>', @newValue) * 16;
	ELSE SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @newValue) * 16;

	IF @safeSql < 31 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + cast(@safeSQL As varchar(10));
		RETURN;
	END;

	IF ISNULL(@spName, '') = '' BEGIN
		RETURN;
	END
	ELSE BEGIN
		-- Determine if this SP has a ReturnValue output parameter
		IF EXISTS(
			SELECT 'x'
				FROM [sys].[parameters] p
				JOIN [sys].[objects] o ON o.[object_id] = p.[object_id]
				WHERE o.name = @spName And Lower(p.name) = '@returnvalue'
		) BEGIN
			SELECT 'ERROR: Old version SP with output parameters not supported!';
		END
		ELSE BEGIN
			IF @isValueString = 0
				EXEC (@spName + N' ''' + @wbs1 + N''', ''' + @wbs2 + N''', ''' + @wbs3 + N''', ' + @newValue);
			ELSE
				EXEC (@spName + N' ''' + @wbs1 + N''', ''' + @wbs2 + N''', ''' + @wbs3 + N''', ''' + @newValue + N'''');
		END;
	END;
END;
GO
