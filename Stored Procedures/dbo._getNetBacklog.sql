SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Net-Backlog Assignments

CREATE   PROCEDURE [dbo].[_getNetBacklog] @ViewingTeam VARCHAR(3) = '101'
AS
    BEGIN
        SELECT psp.WBS1
             , p.ProjTeamNum                              AS PrimeTeamNum
             , psp.TeamNum                                AS SubTeamNum
             , IIF(p.ProjTeamNum = psp.TeamNum, 'Y', 'N') AS SelfWorkInd
             , psp.PlanYear
             , psp.PlanYear * 100 + psp.PlanMonth         AS PlanPeriod
             , CASE
                   WHEN psp.WeekendDate BETWEEN DATEADD(WEEK, -8, s.Sunday) AND DATEADD(WEEK, -5, s.Sunday) THEN 'p5-8'
                   WHEN psp.WeekendDate BETWEEN DATEADD(WEEK, -4, s.Sunday) AND DATEADD(WEEK, -1, s.Sunday) THEN 'p1-4'
                   WHEN psp.WeekendDate BETWEEN s.Sunday AND DATEADD(WEEK, 4, s.Sunday) THEN 'f1-4'
                   WHEN psp.WeekendDate BETWEEN DATEADD(WEEK, 5, s.Sunday) AND DATEADD(WEEK, 8, s.Sunday) THEN 'f5-8'
                   WHEN psp.WeekendDate BETWEEN DATEADD(WEEK, 9, s.Sunday) AND DATEADD(WEEK, 12, s.Sunday) THEN 'f9-12'
                   ELSE NULL
               END                                        AS bucket
             , psp.WeekendDate
             , psp.TotalSpent
             , psp.PlannedSpend
             , p.ProjectLabel
        FROM DataMart.dbo.HAI_ProjectStatusPlanning AS psp WITH (NOLOCK)
            INNER JOIN DataMart.dbo.ProjectJTD      AS p WITH (NOLOCK)
                ON psp.WBS1 = p.WBS1
            INNER JOIN DataMart.dbo.Sundays         AS s
                ON GETDATE() BETWEEN s.Monday AND s.Sunday
            INNER JOIN DataMart.dbo.Teams           AS tp WITH (NOLOCK)
                ON p.ProjTeamNum = tp.TeamNum
            INNER JOIN DataMart.dbo.Teams           AS ts WITH (NOLOCK)
                ON psp.TeamNum = ts.TeamNum
        WHERE
            psp.WBS2 = ' '
            AND psp.WeekendDate BETWEEN DATEADD(WEEK, -8, GETDATE()) AND DATEADD(WEEK, 12, GETDATE())
            AND psp.GroupingType = 'Labor'
            AND p.ProjCompanyID = '01';
    END;
GO
