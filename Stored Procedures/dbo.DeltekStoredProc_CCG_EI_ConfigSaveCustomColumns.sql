SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_ConfigSaveCustomColumns] ( @fieldsSQL varchar(max), @updateValuesSql varchar(max), @insertValuesSql varchar(max), @deleteSeqs varchar(max), @VISION_LANGUAGE varchar(10), @VerifyCCParams varchar(1))
             AS EXEC spCCG_EI_ConfigSaveCustomColumns @fieldsSQL,@updateValuesSql,@insertValuesSql,@deleteSeqs,@VISION_LANGUAGE,@VerifyCCParams
GO
