SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectBillingTermsUpdate] (
   @WBS1         varchar (32), 
   @WBS2         varchar (32),
   @WBS3         varchar (32),
   @BillingType varchar (225))
AS
/*
Copyright (c) 2017 Central Consulting Group. All rights reserved.
03/01/2017 David Springer
           Update Billing Terms fee based upon Billing Type.
           Call from Project workflow when fees have changed.
03/09/2017 David Springer
           Changed Lump Sum to FeeMeth = 3
04/03/2017 David Springer
           Added Limit at Level Above; added Fee Type as a sub type to Billing Type
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
DECLARE @Fee        float,
        @FeePercent float,
		@LumpSumPhase varchar (1),
		@LimitAtLevelAbove varchar (1)
BEGIN
   If @WBS3 <> ' ' and IsNull (@BillingType, '') = ''
      Begin
   -- Get phase Billing Type
      Select @BillingType = custBillingType, @LumpSumPhase = CustLumpSumPhase
	  From ProjectCustomTabFields Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = ' '
      If @BillingType <> 'Lump Sum by Phase'
	     Begin
         Set @WBS3 = ' ' -- operate at phase level billing terms
		 End
      If @LumpSumPhase = 'Y'
	     Begin
		 Set @BillingType = 'Lump Sum by Phase'
		 If 1 < (Select Count (*) From ProjectCustomTabFields Where WBS1 = @WBS1 and CustBillingType = 'Lump Sum by Phase' and WBS3 = ' ')
			RAISERROR ('Lump Sum by Phase billing type has been used on more than one phase.                                                            ', 16, 1)
         Set @WBS2 = (Select WBS2 From ProjectCustomTabFields Where WBS1 = @WBS1 and CustBillingType = 'Lump Sum by Phase' and WBS3 = ' ')
		 End
      End

   If @WBS2 <> ' ' and IsNull (@BillingType, '') = ''
      Begin
   -- Get phase Lump Sum Phase checkbox
      Select @LumpSumPhase = CustLumpSumPhase From ProjectCustomTabFields Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = ' '
      If @LumpSumPhase = 'Y'
	     Begin
		 Set @BillingType = 'Lump Sum by Phase'
         Set @WBS2 = (Select WBS2 From ProjectCustomTabFields Where WBS1 = @WBS1 and CustBillingType = 'Lump Sum by Phase' and WBS3 = ' ')
		 End
      If @LumpSumPhase = 'N'
	     Begin
	   -- Get project Billing Type
		  Select @BillingType = custBillingType From ProjectCustomTabFields Where WBS1 = @WBS1 and WBS2 = ' '
		  If @BillingType is not null
			 Begin
			 Set @WBS2 = ' ' -- operate at project level billing terms
			 End
         End
      End

   If @BillingType like 'Cost Plus Max%'
      Begin
      Select @LimitAtLevelAbove = CustLimitAtLevelAbove From ProjectCustomTabFields Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
      exec spCCG_ProjectBillingTermsLimitAbove @WBS1, @WBS2, @WBS3, @LimitAtLevelAbove
      End

   If @BillingType = 'Lump Sum'
      Begin
		  If @WBS2 <> ' ' and @WBS3 = ' ' -- Phase level billing terms
			 Begin
		  Update b
		  Set b.Fee = p.Fee,
			  b.PctComplete = CASE When p.Fee = 0 Then 0 Else Round (b.FeeToDate / p.Fee * 100, 2) END
		  From BTF b, 
			  (Select WBS1, WBS2, Sum (FeeDirLab + FeeDirExp + ConsultFee) Fee
			   From PR
			-- must be lowest level, because phase fee will not be updated when task fee is changed
			   Where Sublevel = 'N'
			   Group by WBS1, WBS2) p
		  Where b.WBS1 = @WBS1
			and b.WBS2 = @WBS2
			and b.WBS3 = @WBS3
			and b.WBS1 = p.WBS1
			and IsNull (b.PostWBS2, ' ') = p.WBS2
	       End -- Task

		  If @WBS3 <> ' ' -- Task level billing terms
			 Begin
		  Update b
		  Set b.Fee = p.Fee,
			  b.PctComplete = CASE When p.Fee = 0 Then 0 Else Round (b.FeeToDate / p.Fee * 100, 2) END
		  From BTF b, 
			  (Select WBS1, WBS2, WBS3, Sum (FeeDirLab + FeeDirExp + ConsultFee) Fee
			   From PR
			-- must be lowest level, because phase fee will not be updated when task fee is changed
			   Where Sublevel = 'N'
			   Group by WBS1, WBS2, WBS3) p
		  Where b.WBS1 = @WBS1
			and b.WBS2 = @WBS2
			and b.WBS3 = @WBS3
			and b.WBS1 = p.WBS1
			and IsNull (b.PostWBS2, ' ') = p.WBS2
			and IsNull (b.PostWBS3, ' ') = p.WBS3
	       End -- Task

      End  -- End Lump Sum without "Allocate Billed Revenue on Fee Invoices Based on Category"

   If @BillingType = 'Lump Sum by Phase'  -- FeeMeth = 3 Percent Complete by Phase as Fixed Amount
      Begin
      Update b
      Set b.Fee = p.Fee,
          b.PctComplete = CASE When p.Fee = 0 Then 0 Else Round (b.FeeToDate / p.Fee * 100, 2) END
      From BTF b, 
	      (Select WBS1, WBS2, Sum (FeeDirLab + FeeDirExp + ConsultFee) Fee
		   From PR
        -- must be lowest level, because phase fee will not be updated when task fee is changed
		   Where Sublevel = 'N'
		   Group by WBS1, WBS2) p
      Where b.WBS1 = @WBS1
	    and b.WBS2 = @WBS2
        and b.WBS1 = p.WBS1
        and b.PostWBS2 = p.WBS2
		-- Don't restrict to WBS3, since billling terms are on Task 1

   -- Add Missing Phases
   -- Phase level
      If @WBS2 <> ' ' and @WBS3 = ' ' -- Phase level billing terms (pull only tasks)
         and exists (Select 'x' From BT Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = ' ')
         Begin
	   -- Insert new Billing Phase records (Fee);  FeeMeth = 3-Percent Complete by Phase as Fixed Amount
		  Insert Into BTF (WBS1, WBS2, WBS3, Seq, Phase, Name, Fee, PostWBS1, PostWBS2, PostWBS3)
		  Select p.WBS1, @WBS2, ' ', 
			(Select IsNull (Max (Seq), 0) From BTF Where WBS1 = p.WBS1) + row_number () over (order by p.WBS2) Seq, 
			(Select IsNull (Max (Seq), 0) From BTF Where WBS1 = p.WBS1) + row_number () over (order by p.WBS2) Phase, 
			 p.Name, p.FeeDirLab + p.FeeDirExp + p.ConsultFee, 
			 p.WBS1 PostWBS1, 
			 p.WBS2 PostWBS2,
			 t.WBS3 PostWBS3
		  From PR p, ProjectCustomTabFields px
			   Left Join
			   (Select WBS1, WBS2, Min (WBS3) WBS3 From PR Where WBS3 <> ' ' Group by WBS1, WBS2) t on t.WBS1 = px.WBS1 and t.WBS2 = px.WBS2
		  Where p.WBS1 = @WBS1
			and p.WBS1 = px.WBS1
			and p.WBS2 = px.WBS2
			and p.WBS3 = px.WBS3
			and p.WBS2 <> ' '
			and p.WBS3 = ' ' -- Phase level
			and px.CustLumpSumPhase = 'Y'
			and not exists (Select * From BTF b
							Where WBS1 = p.WBS1 and IsNull (PostWBS2, ' ') = p.WBS2
							  and IsNull (PostWBS3, '') = IsNull ((Select Min (WBS3) From PR Where WBS1 = b.WBS1 and WBS2 = b.postWBS2 and WBS3 <> ' '), ''))
         End

      End -- Lump Sum by Phase

   If @BillingType = 'Lump Sum by Task' and
      @WBS2 <> ' ' -- Phase level billing terms (pull only tasks)
      and exists (Select 'x' From BT Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = ' ')
      Begin
      Update b
      Set b.Fee = p.FeeDirLab + p.FeeDirExp + p.ConsultFee, 
          b.PctComplete = CASE When p.FeeDirLab + p.FeeDirExp + p.ConsultFee = 0 Then 0 Else Round (b.FeeToDate / (p.FeeDirLab + p.FeeDirExp + p.ConsultFee) * 100, 2) END
      From BTF b, PR p
      Where b.WBS1 = @WBS1
	    and b.WBS2 = @WBS2
        and b.WBS1 = p.WBS1
        and b.PostWBS2 = p.WBS2
        and IsNull (b.PostWBS3, ' ') = p.WBS3

	-- Insert new Billing Phase records (Fee);  FeeMeth = 3-Percent Complete by Phase as Fixed Amount
		Insert Into BTF (WBS1, WBS2, WBS3, Seq, Phase, Name, Fee, PostWBS1, PostWBS2, PostWBS3)
		Select p.WBS1, @WBS2, ' ', 
		(Select IsNull (Max (Seq), 0) From BTF Where WBS1 = p.WBS1) + row_number () over (order by p.WBS2) Seq, 
		(Select IsNull (Max (Seq), 0) From BTF Where WBS1 = p.WBS1) + row_number () over (order by p.WBS2) Phase, 
			p.Name, p.FeeDirLab + p.FeeDirExp + p.ConsultFee, 
			p.WBS1 PostWBS1, 
			p.WBS2 PostWBS2,
            p.WBS3 PostWBS3
		From PR p
		Where p.WBS1 = @WBS1
		  and p.WBS2 = @WBS2
		  and p.WBS3 <> ' ' -- Task level
		  and not exists (Select * From BTF 
			  			  Where WBS1 = p.WBS1 and IsNull (PostWBS2, ' ') = p.WBS2 and IsNull (PostWBS3, ' ') = p.WBS3)

      End -- Lump Sum by Task

   If @BillingType = 'Cost Plus Fixed Fee'
      Begin
      Update BT
      Set Mult1 = p.MultAmt,
          FeeFactor1 = FeeDirExp
	  From BT b, PR p
      Where b.WBS1 = @WBS1
        and b.WBS2 = @WBS2
        and b.WBS3 = @WBS3
        and b.WBS1 = p.WBS1
		and b.WBS2 = p.WBS2
		and b.WBS3 = p.WBS3
      End

   If @BillingType = 'Percent of Construction'  -- Any level
      Begin
      Select @Fee = CustConstructionFeeCost
      From ProjectCustomTabFields
      Where WBS1 = @WBS1
        and WBS2 = ' '  -- Project level construction contract

      Select @FeePercent = custConstructionFeePercent
      From ProjectCustomTabFields
      Where WBS1 = @WBS1
        and WBS2 = @WBS2
        and WBS3 = @WBS3  -- Fee Pct comes from any level

      Update BT
      Set FeeFactor1 = @Fee, -- Construction Cost using Fee variable
          FeeFactor2 = @FeePercent  -- Fee Percent using Pct Complete variable
      Where WBS1 = @WBS1
        and WBS2 = @WBS2
        and WBS3 = @WBS3
      End

END
GO
