SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_GetDelegateForList] (@Employee Nvarchar(20), @IncludeSelf int = 1)
AS BEGIN
/*
	Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved.

	select * from CCG_EI_Delegation d
	exec [spCCG_EI_GetDelegateForList] '00003', 1
*/
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	declare @ApprovalRequired varchar(1)
	select @ApprovalRequired = DelegationApproval from CCG_EI_Config

	select d.Employee
		from CCG_EI_Delegation d
		where d.Delegate = @Employee
			and (isnull(d.ApprovedBy,N'') <> N'' or @ApprovalRequired <> 'Y')
			and GETDATE() between d.FromDate and dateadd(d, 1, d.ToDate)
			and ForClientId='' and ForWBS1=N''			-- We only want 'global' delegates in this case
	UNION
	select @Employee where @IncludeSelf = 1
END
GO
