SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPShiftTasks]
  @strRowID nvarchar(255),
  @strFromStartDate varchar(8),
  @strFromEndDate varchar(8),
  @strToStartDate varchar(8),
  @strToEndDate varchar(8),
  @bitShiftAll bit, /* This flag is applicable only for ShiftMode = {1, 3}. 0 = Shift using the "From" range, 1 = Shift the entire Assignment Row */
  @bitFixStartDate bit /* This flag is applicable only for ShiftMode = {1, 3}. 1 = Hold "To" StartDate fixed, 0 = Hold "To" EndDate fixed */
AS

BEGIN -- Procedure stRPShiftTasks

  SET NOCOUNT ON
  
  DECLARE @dtFromStartDate datetime
  DECLARE @dtFromEndDate datetime

  DECLARE @dtToStartDate datetime
  DECLARE @dtToEndDate datetime

  DECLARE @dtMinASGDate datetime
  DECLARE @dtMaxASGDate datetime
  DECLARE @dtTSKStartDate datetime
  DECLARE @dtTSKEndDate datetime
  DECLARE @dtOvrdFromStartDate datetime
  DECLARE @dtOvrdFromEndDate datetime
   
  DECLARE @strCompany nvarchar(14)
  DECLARE @strPlanID varchar(32) = NULL
  DECLARE @strTaskID varchar(32) = NULL
  DECLARE @strAssignmentID varchar(32) = NULL
  DECLARE @strNextAssignmentID varchar(32) = NULL
  DECLARE @strResourceID nvarchar(20) = NULL
  DECLARE @strGenericResourceID nvarchar(20) = NULL
  DECLARE @strUserName nvarchar(32)
  DECLARE @strAECFlg varchar(1) = ''
  DECLARE @strRescheduleFlg varchar(1) = 'P' /* 'P' = Reschedule Plan Date, 'A' = Reschedule Assignment Date */

  DECLARE @strOvrdFromStartDate varchar(8)
  DECLARE @strOvrdFromEndDate varchar(8)

  DECLARE @decSumHrs decimal(19,4) = 0

  DECLARE @intFromDurationWD int = 0

  -- Declare Temp tables.
  
  DECLARE @tabAssignment TABLE (
    AssignmentID varchar(32) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    MinASGDate datetime,
    MaxASGDate datetime
    UNIQUE(AssignmentID)
  )

  DECLARE @tabTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    ToStartDate datetime,
    ToEndDate datetime,
    SumHrs decimal(19,4),
    MinTPDDate datetime,
    MaxTPDDate datetime
    UNIQUE(PlanID, TaskID)
  )

  DECLARE @tabTPD TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    SumHrs decimal(19,4),
    MinTPDDate datetime,
    MaxTPDDate datetime
    UNIQUE(PlanID, OutlineNumber)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

  -- Set Dates

  SET @dtFromStartDate = 
    CASE 
      WHEN DATALENGTH(@strFromStartDate) = 0 
      THEN NULL
      ELSE CONVERT(datetime, @strFromStartDate)
    END

  SET @dtFromEndDate = 
    CASE
      WHEN DATALENGTH(@strFromEndDate) = 0
      THEN NULL
      ELSE CONVERT(datetime, @strFromEndDate)
    END

  SET @dtToStartDate = 
    CASE
      WHEN DATALENGTH(@strToStartDate) = 0
      THEN NULL
      ELSE CONVERT(datetime, @strToStartDate)
    END

  SET @dtToEndDate = 
    CASE
    WHEN DATALENGTH(@strToEndDate) = 0
    THEN NULL
    ELSE CONVERT(datetime, @strToEndDate)
  END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Parse @strRowID
  -- dbo.stRPShiftTasks could be called from 3 areas: Project Planing (PP), RM Project View (PV), and RM Resource View (RV).
  -- @strRowID is a string with the following format
  --   1. For an Assignment row, @strRowID = 'E~<PNAssignment.ResourceID>|<PNTask.TaskID>'
  --   2. For an Assignment row, @strRowID = 'G~<PNAssignment.GenericResourceID>|<PNTask.TaskID>'
  --   3. For an Expense row, @strRowID = '<PNExpense.Account>➔<PNExpense.Vendor>|<PNTask.TaskID>'
  --   4. For an Expense row, @strRowID = '<PNExpense.Account>➔|<PNTask.TaskID>'
  --   5. For a Consultant row, @strRowID = '<PNConsultant.Account>➔<PNConsultant.Vendor>|<PNTask.TaskID>'
  --   6. For a Consultant row, @strRowID = '<PNConsultant.Account>➔|<PNTask.TaskID>'
  --   7. For a WBS row {PP, PV}, @strRowID = '|<PNTask.TaskID>'
  --   8. For a WBS row {RV}, @strRowID = same as cases # {1, 2, 3, 4, 5, 6} above. 
  --      <PNTask.TaskID> could be TaskID of WBS row at any levels and is the Ancestor of the Resource row (not just the Parent).
  
  SELECT
    @strPlanID = PlanID,
    @strTaskID = TaskID,
    @strAssignmentID = AssignmentID,
    @strResourceID = ResourceID,
    @strGenericResourceID = GenericResourceID,
    @strAECFlg = AECFlg
    FROM dbo.stRP$tabParseRowID(@strRowID)

  -- 1. When dbo.stRPShiftTasks is called from Project Planing (PP)
  --    1.1 @strAECFlg = {'A', 'E', 'C', ''}
  --    1.2 User can change [Plan] Start or End Date on a WBS row (@strAECFlg = '')
  --    1.3 User can change [Assignment] Start or End Date on a Resource row (@strAECFlg = {'A', 'E', 'C'})
  -- 2. When dbo.stRPShiftTasks is called from RM Project View (PV)
  --    2.1 @strAECFlg = {'A', ''}
  --    2.2 User can change [Plan] Start or End Date on a WBS row (@strAECFlg = '')
  --    2.3 User can change [Assignment] Start or End Date on a Resource row (@strAECFlg = 'A')
  -- 3. When dbo.stRPShiftTasks is called from RM Resource View (RV)
  --    3.1 @strAECFlg = {'A'}
  --    3.3 User can change [Assignment] Start or End Date on a WBS row *at any level*

  -- Set @strRescheduleFlg
  -- 'P' = Reschedule Plan Date
  -- 'A' = Reschedule Assignment Date

  SET @strRescheduleFlg = 
    CASE
      WHEN (DATALENGTH(@strAECFlg) = 0) THEN 'P'
      WHEN (@strAECFlg IN ('A', 'E', 'C')) THEN 'A'
      ELSE '' 
    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get various parameters.

  SELECT 
    @strCompany = P.Company,
    @dtTSKStartDate = T.StartDate,
    @dtTSKEndDate = T.EndDate
    FROM PNTask AS T
      INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID
    WHERE T.TaskID = @strTaskID

  -- Keep track of original Duration of the "From" range.

  SET @intFromDurationWD = dbo.DLTK$NumWorkingDays(@dtFromStartDate, @dtFromEndDate, @strCompany)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabAssignment(
    AssignmentID,
    StartDate,
    EndDate,
    MinASGDate,
    MaxASGDate
  )
    SELECT DISTINCT
      A.AssignmentID, 
      A.StartDate,
      A.EndDate,
      MIN(A.StartDate) OVER () AS MinASGDate,
      MAX(A.EndDate) OVER () AS MaxASGDate
      FROM PNAssignment AS A
        INNER JOIN PNTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
        INNER JOIN PNTask AS PT ON A.PlanID = PT.PlanID 
      WHERE
        A.PlanID = @strPlanID AND
        PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
        AT.OutlineNumber LIKE PT.OutlineNumber + '%' AND
        ((ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') 
          AND ISNULL(A.GenericResourceID, '|') = ISNULL( @strGenericResourceID, '|'))
          OR (@strResourceID IS NULL AND @strGenericResourceID IS NULL))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- I must use a temp table here otherwise the cursor will get confused when Assignment Start/End dates are changed.

  DECLARE csr20160203_tabAssignment CURSOR LOCAL FAST_FORWARD FOR
    SELECT DISTINCT
      A.AssignmentID, 
      A.MinASGDate,
      A.MaxASGDate
      FROM @tabAssignment AS A

  OPEN csr20160203_tabAssignment
  FETCH NEXT FROM csr20160203_tabAssignment INTO @strNextAssignmentID, @dtMinASGDate, @dtMaxASGDate

  WHILE (@@FETCH_STATUS = 0)
    BEGIN

      -- Adjusting "From" Date for Assignment row.
      -- When "Shift All", the caller does not pass in "From" Date, and the "From" Date is derived from either the current Task or Assignment.

      SELECT
        @strOvrdFromStartDate = 
          CASE
            WHEN @bitShiftAll = 1 
            THEN 
              CASE 
                WHEN @strRescheduleFlg = 'P' 
                THEN CONVERT(VARCHAR(8), @dtTSKStartDate, 112)
                ELSE CONVERT(VARCHAR(8), @dtMinASGDate, 112)
              END
            ELSE @strFromStartDate
          END,
        @strOvrdFromEndDate = 
          CASE
            WHEN @bitShiftAll = 1 
            THEN 
              CASE 
                WHEN @strRescheduleFlg = 'P' 
                THEN CONVERT(VARCHAR(8), @dtTSKEndDate, 112)
                ELSE CONVERT(VARCHAR(8), @dtMaxASGDate, 112)
              END
            ELSE @strFromEndDate
          END

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      EXECUTE dbo.stRPShiftAssigment @strNextAssignmentID, @strOvrdFromStartDate, @strOvrdFromEndDate, @strToStartDate, @strToEndDate, @bitShiftAll, @bitFixStartDate

      FETCH NEXT FROM csr20160203_tabAssignment INTO @strNextAssignmentID, @dtMinASGDate, @dtMaxASGDate

    END -- While

  CLOSE csr20160203_tabAssignment
  DEALLOCATE csr20160203_tabAssignment

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Change Task {StartDate, EndDate} only when user is changing "Plan Dates" at WBS Level and not at Assignment Level.
  
  IF (@strRescheduleFlg = 'P')
    BEGIN

      -- Adjusting "From" Date for WBS row.
      -- When "Shift All", the caller does not pass in "From" Date, and the "From" Date is derived from the current Task.

      SELECT
        @dtOvrdFromStartDate = 
          CASE
            WHEN @bitShiftAll = 1 AND @strRescheduleFlg = 'P'
            THEN @dtTSKStartDate
            ELSE @dtFromStartDate
          END,
        @dtOvrdFromEndDate = 
          CASE
            WHEN @bitShiftAll = 1 AND @strRescheduleFlg = 'P'
            THEN @dtTSKEndDate
            ELSE @dtFromEndDate
          END

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabTPD(
        PlanID,
        OutlineNumber,
        SumHrs,
        MinTPDDate,
        MaxTPDDate
      )
        SELECT
          TPD.PlanID AS PlanID,
          AT.OutlineNumber AS OutlineNumber,
          SUM(TPD.PeriodHrs) AS SumHrs,
          MIN(TPD.StartDate) AS MinTPDDate,
          MAX(TPD.EndDate) AS MaxTPDDate
          FROM PNPlannedLabor AS TPD
            INNER JOIN PNTask AS AT ON TPD.PlanID = AT.PlanID AND TPD.TaskID = AT.TaskID AND TPD.AssignmentID IS NOT NULL
            INNER JOIN PNTask AS PT ON AT.PlanID = PT.PlanID
          WHERE TPD.PlanID = @strPlanID AND
            PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
            AT.OutlineNumber LIKE PT.OutlineNumber + '%' 
          GROUP BY TPD.PlanID, TPD.TaskID, AT.OutlineNumber

      INSERT @tabTask(
        PlanID,
        TaskID,
        StartDate,
        EndDate,
        ToStartDate,
        ToEndDate,
        SumHrs,
        MinTPDDate,
        MaxTPDDate
      )
        SELECT
          XT.PlanID AS PlanID,
          XT.TaskID AS TaskID,
          XT.StartDate AS StartDate,
          XT.EndDate AS EndDate,
          XT.ToStartDate AS ToStartDate,
          XT.ToEndDate AS ToEndDate,
          ISNULL(SUM(TPD.SumHrs), 0) AS SumHrs,
          MIN(TPD.MinTPDDate) AS MinTPDDate,
          MAX(TPD.MinTPDDate) AS MinTPDDate
          FROM (
            SELECT 
              YT.PlanID AS PlanID,
              YT.TaskID AS TaskID,
              YT.OutlineNumber AS OutlineNumber,
              YT.StartDate AS StartDate,
              YT.EndDate AS EndDate,
              CASE 
                WHEN @bitFixStartDate = 0
                THEN
                  CASE
                    WHEN YT.DurationWD = 0
                    THEN @dtToEndDate
                    ELSE
                      dbo.DLTK$AddBusinessDays(
                        @dtToEndDate, 
                        -1 * ((YT.DurationWD - 1) + YT.OffsetWD), 
                        @strCompany
                      )
                  END
                ELSE 
                  dbo.DLTK$AddBusinessDays(
                    @dtToStartDate, 
                    YT.OffsetWD, 
                    @strCompany
                  )
              END AS ToStartDate,
              CASE 
                WHEN @bitFixStartDate = 1
                THEN
                  dbo.DLTK$AddBusinessDays(
                    @dtToStartDate,
                    CASE
                      WHEN YT.DurationWD <> 0
                      THEN ((YT.DurationWD - 1) + YT.OffsetWD)
                      ELSE (YT.OffsetWD) 
                    END, 
                    @strCompany
                  )
                ELSE 
                  dbo.DLTK$AddBusinessDays(
                    @dtToEndDate, 
                    -1 * (YT.OffsetWD), 
                    @strCompany
                  )
              END AS ToEndDate
              FROM (
                SELECT
                  CT.PlanID AS PlanID,
                  CT.TaskID AS TaskID,
                  CT.OutlineNumber AS OutlineNumber,
                  CT.StartDate AS StartDate,
                  CT.EndDate AS EndDate,
                  CASE
                    WHEN @bitShiftAll = 1
                    THEN dbo.DLTK$NumWorkingDays(CT.StartDate, CT.EndDate, @strCompany)
                    ELSE @intFromDurationWD
                  END AS DurationWD,
                  CASE 
                    WHEN @bitFixStartDate = 1
                    THEN
                      CASE 
                        WHEN @dtOvrdFromStartDate = CT.StartDate 
                        THEN 0
                        ELSE dbo.DLTK$NumWorkingDays(@dtOvrdFromStartDate, CT.StartDate, @strCompany) - 1
                      END
                    ELSE
                      /* When approaching from the rear @bitFixStartDate = 0, need to reverse the sign for OffsetWD. */
                      /* OffsetWD is the distance from OvrdFromEndDate to EndDate of WBS row. */
                      CASE
                        WHEN @dtOvrdFromEndDate = CT.EndDate
                        THEN 0
                        ELSE dbo.DLTK$NumWorkingDays(CT.EndDate, @dtOvrdFromEndDate, @strCompany) - 1
                      END
                  END AS OffsetWD
                  FROM 
                    PNTask AS PT
                      INNER JOIN PNTask AS CT ON PT.PlanID = CT.PlanID
                    WHERE
                      PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
                      CT.OutlineNumber LIKE PT.OutlineNumber + '%'   
              ) AS YT
          ) AS XT
            LEFT JOIN @tabTPD AS TPD
              ON XT.PlanID = TPD.PlanID AND TPD.OutlineNumber LIKE XT.OutlineNumber + '%'
          GROUP BY XT.PlanID, XT.TaskID, XT.StartDate, XT.EndDate, XT.ToStartDate, XT.ToEndDate

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      UPDATE ZT SET
        StartDate = 
          CASE 
            WHEN @bitShiftAll = 1 THEN
              CASE
                WHEN ToStartDate IS NULL
                THEN
                  CASE
                    WHEN ZT.StartDate <= ToEndDate
                    THEN ZT.StartDate
                    ELSE ToEndDate
                  END
                WHEN XT.SumHrs = 0 THEN ToStartDate
                WHEN XT.MinTPDDate IS NULL THEN ToStartDate
                ELSE
                  CASE
                    WHEN ToStartDate < XT.MinTPDDate
                    THEN ToStartDate
                    ELSE XT.MinTPDDate
                  END
              END
            WHEN @bitShiftAll = 0 THEN
              CASE
                WHEN XT.SumHrs = 0 THEN ZT.StartDate
                ELSE
                  CASE
                    WHEN ZT.StartDate <= XT.MinTPDDate
                    THEN ZT.StartDate
                    ELSE XT.MinTPDDate
                  END
              END
          END,
        EndDate = 
          CASE 
            WHEN @bitShiftAll = 1 THEN
              CASE
                WHEN ToEndDate IS NULL
                THEN
                  CASE
                    WHEN ZT.EndDate >= ToStartDate
                    THEN ZT.EndDate
                    ELSE ToStartDate
                  END
                WHEN XT.SumHrs = 0 THEN ToEndDate
                WHEN XT.MaxTPDDate IS NULL THEN ToEndDate
                ELSE
                  CASE
                    WHEN ToEndDate > XT.MaxTPDDate
                    THEN ToEndDate
                    ELSE XT.MaxTPDDate
                  END
              END
            WHEN @bitShiftAll = 0 THEN
              CASE
                WHEN XT.SumHrs = 0 THEN ZT.EndDate
                ELSE
                  CASE
                    WHEN ZT.EndDate >= XT.MaxTPDDate
                    THEN ZT.EndDate
                    ELSE XT.MaxTPDDate
                  END
              END
          END,
		ModDate=LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19),
		ModUser=@strUserName
        FROM PNTask AS ZT
          INNER JOIN @tabTask AS XT ON ZT.PlanID = XT.PlanID AND ZT.TaskID = XT.TaskID
    
    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- Shifting Expense and Consultant data.
      -- This only needs to be done when the shifting occured at a WBS row.
      -- User cannot shift date on an Expense or Consultant row.

      -- Shifting Expense data.

      UPDATE TPD SET
        StartDate = ET.StartDate,
        EndDate = ET.EndDate
        FROM PNPlannedExpenses AS TPD
          INNER JOIN PNTask AS ET ON TPD.PlanID = ET.PlanID AND TPD.TaskID = ET.TaskID AND TPD.ExpenseID IS NOT NULL
          INNER JOIN PNTask AS PT ON ET.PlanID = PT.PlanID AND ET.OutlineNumber LIKE PT.OutlineNumber + '%'
        WHERE TPD.PlanID = @strPlanID AND PT.TaskID = @strTaskID 

      UPDATE E SET
        StartDate = ET.StartDate,
        EndDate = ET.EndDate
        FROM PNExpense AS E
          INNER JOIN PNTask AS ET ON E.PlanID = ET.PlanID AND E.TaskID = ET.TaskID
          INNER JOIN PNTask AS PT ON ET.PlanID = PT.PlanID AND ET.OutlineNumber LIKE PT.OutlineNumber + '%'
        WHERE E.PlanID = @strPlanID AND PT.TaskID = @strTaskID

      -- Shifting Consultant data.

      UPDATE TPD SET
        StartDate = CT.StartDate,
        EndDate = CT.EndDate
        FROM PNPlannedConsultant AS TPD
          INNER JOIN PNTask AS CT ON TPD.PlanID = CT.PlanID AND TPD.TaskID = CT.TaskID AND TPD.ConsultantID IS NOT NULL
          INNER JOIN PNTask AS PT ON CT.PlanID = PT.PlanID AND CT.OutlineNumber LIKE PT.OutlineNumber + '%'
        WHERE TPD.PlanID = @strPlanID AND PT.TaskID = @strTaskID

      UPDATE C SET
        StartDate = CT.StartDate,
        EndDate = CT.EndDate
        FROM PNConsultant AS C
          INNER JOIN PNTask AS CT ON C.PlanID = CT.PlanID AND C.TaskID = CT.TaskID
          INNER JOIN PNTask AS PT ON CT.PlanID = PT.PlanID AND CT.OutlineNumber LIKE PT.OutlineNumber + '%'
        WHERE C.PlanID = @strPlanID AND PT.TaskID = @strTaskID

    END /* END IF (@strRescheduleFlg =  'P') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     
  SET NOCOUNT OFF

END -- stRPShiftTasks
GO
