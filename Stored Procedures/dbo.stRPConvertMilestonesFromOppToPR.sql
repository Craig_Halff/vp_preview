SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPConvertMilestonesFromOppToPR]
AS

BEGIN -- Procedure stRPConvertMilestonesFromOppToPR

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- The purpose of this stored procedure is to convert Opportunity Milestones in Activity table to Project Milestones in table PRMilestone.
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

  DECLARE @siNext_M smallint
  DECLARE @strUserName nvarchar(32) = 'DPS20OPPMStone2PR'

  DECLARE @tabActivitySubjects TABLE (
    RowID int IDENTITY(1,1),
    Subject nvarchar(255) COLLATE database_default,
    UNIQUE (RowID, Subject)
  )

  DECLARE @tabCFGProjectMilestone TABLE (
    RowID int IDENTITY(1,1),
    Code nvarchar(20) COLLATE database_default,
    Description nvarchar(255) COLLATE database_default
    UNIQUE (RowID, Code, Description)
  )

  DECLARE @tabNewCFGProjectMilestone TABLE (
    RowID int IDENTITY(1,1),
    Code nvarchar(20) COLLATE database_default,
    Description nvarchar(255) COLLATE database_default
    UNIQUE (RowID, Code, Description)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get DISTINCT Activity Milestone Subjects

  INSERT @tabActivitySubjects(
    Subject
  )
    SELECT DISTINCT
      A.Subject AS Subject
      FROM Activity AS A
      WHERE A.Type = 'Milestone' AND A.OpportunityID IS NOT NULL AND A.Subject IS NOT NULL
      ORDER BY A.Subject

  -- Get Project Stages. Use UICultureName = 'en-US' to get Descriptions.
  -- Collect both System and Non-System Milestone Code

  INSERT @tabCFGProjectMilestone(
    Code,
    Description
  )
    SELECT DISTINCT
      PSD.Code,
      PSD.Description
      FROM CFGProjectMilestoneDescriptions AS PSD
        INNER JOIN CFGProjectMilestoneData AS PSC ON PSD.Code = PSC.Code
      WHERE PSD.UICultureName = 'en-US'

  -- Determine the next sequence number for Non-System Milestones currently in CFGProjectMilestoneData

  SELECT @siNext_M = COUNT(*) + 1 FROM CFGProjectMilestoneData WHERE SystemInd = 'N'

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save those Codes/Descriptions from Opportunity Stage table that have no corresponding entry in Project Stage table yet.

  INSERT @tabNewCFGProjectMilestone(
    Code,
    Description
  )
    SELECT DISTINCT
      '~AO' +
      RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(@siNext_M, 36)), 3) +
      RIGHT('00000' + CONVERT(VARCHAR(5), dbo.DLTK$DecToBase36(RowNo, 36)), 5) AS Code,
      Description AS Description
      FROM ( /* Y */
        SELECT DISTINCT
          X.Description AS Description,
          ROW_NUMBER() OVER (ORDER BY X.Description) AS RowNo
          FROM ( /* X */
            SELECT DISTINCT
              S.Subject AS Description
              FROM @tabActivitySubjects AS S
                LEFT JOIN @tabCFGProjectMilestone AS M ON S.Subject = M.Description 
              WHERE M.Code IS NULL
          ) AS X
      ) AS Y

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    INSERT CFGProjectMilestoneData(
      Code,
      SystemInd
    )
      SELECT
        Code,
        'N' AS SystemInd
        FROM @tabNewCFGProjectMilestone
        
    INSERT CFGProjectMilestoneDescriptions(
      Code,
      UICultureName,
      Description,
      Seq
    )
      SELECT
        Code AS Code,
        'en-US' AS UICultureName,
        Description AS Description,
        RowID AS Seq
        FROM @tabNewCFGProjectMilestone

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move Opportunity Milestones from Activity table to PRMilestone table.

    INSERT PRMilestone(
	    RecordID,
	    WBS1,
	    WBS2,
	    WBS3,
	    EndDate,
	    Code,
	    CreateUser,
	    CreateDate,
	    ModUser,
	    ModDate
    )
      SELECT
	      REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RecordID,
	      PR.WBS1 AS WBS1,
	      PR.WBS2 AS WBS2,
	      PR.WBS3 AS WBS3,
	      OA.EndDate AS EndDate,
	      MDS.Code AS Code,
	      @strUserName CreateUser,
	      GETUTCDATE() AS CreateDate,
	      @strUserName ModUser,
	      GETUTCDATE() AS ModDate
        FROM Activity AS OA
          INNER JOIN Opportunity AS O ON OA.OpportunityID = O.OpportunityID AND OA.Type = 'Milestone'
          INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          LEFT JOIN CFGProjectMilestoneDescriptions AS MDS ON OA.Subject = MDS.Description AND MDS.UICultureName = 'en-US'
          LEFT JOIN CFGProjectMilestoneData AS MDC ON MDS.Code = MDC.Code
        WHERE MDS.Code IS NOT NULL

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	-- Create Milestones of PR date fields from Opportunity that are not from Activity table
	INSERT PRMilestone(
		RecordID, 
		WBS1, 
		WBS2, 
		WBS3, 
		EndDate, 
		Code, 
		CreateUser, 
		CreateDate, 
		ModUser, 
		ModDate
	)
	  SELECT 
      REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RecordID, 
      X.WBS1 AS WBS1, 
      X.WBS2 AS WBS2, 
      X.WBS3 AS WBS3, 
      X.PR_Date AS EndDate,
      X.M_Code AS Code,
      @strUserName CreateUser,
      GETUTCDATE() AS CreateDate,
      @strUserName ModUser,
      GETUTCDATE() AS ModDate 
	  FROM ( /* X */
	    SELECT DISTINCT 'SysEstCompletion' AS M_Code, PR.WBS1 AS WBS1, PR.WBS2 AS WBS2, PR.WBS3 AS WBS3, PR.EstCompletionDate AS PR_Date
        FROM PR LEFT JOIN PRMilestone AS MEC ON PR.WBS1 = MEC.WBS1 AND PR.WBS2 = MEC.WBS2 AND PR.WBS3 = MEC.WBS3 AND MEC.Code = 'SysEstCompletion' 
        WHERE MEC.RecordID IS NULL AND PR.EstCompletionDate IS NOT NULL AND PR.OpportunityID IS NOT NULL
	    UNION
	    SELECT DISTINCT 'SysEstStart' AS M_Code, PR.WBS1 AS WBS1, PR.WBS2 AS WBS2, PR.WBS3 AS WBS3, PR.EstStartDate AS PR_Date 
        FROM PR LEFT JOIN PRMilestone AS MES ON PR.WBS1 = MES.WBS1 AND PR.WBS2 = MES.WBS2 AND PR.WBS3 = MES.WBS3 AND MES.Code = 'SysEstStart' 
        WHERE MES.RecordID IS NULL AND PR.EstStartDate IS NOT NULL AND PR.OpportunityID IS NOT NULL
    ) AS X

	--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
  COMMIT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF

END -- stRPConvertMilestonesFromOppToPR
GO
