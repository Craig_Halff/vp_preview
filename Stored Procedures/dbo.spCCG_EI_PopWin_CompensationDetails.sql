SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_PopWin_CompensationDetails] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7), @UserName varchar(32))
AS BEGIN
/*
	Copyright (c) 2020 Central Consulting Group.  All rights reserved.
	[spCCG_EI_PopWin_CompensationDetails] '1999004.00',' ',' ', ''
*/
	declare @T TABLE (TopOrder	int, Row_Detail varchar(255), Descr	varchar(255), Amount money, ID varchar(30))

	insert into @T (TopOrder, Row_Detail, Descr, Amount, ID)
		select 1 as TopOrder, '', 'Fee', Sum(Fee), ''
		from PR 
		where WBS1=@WBS1 and (@WBS2=' ' or WBS2=@WBS2) and (@WBS3=' ' or WBS3=@WBS3)
		UNION ALL
		select 2 as TopOrder, '', 'Consultant', Sum(ConsultFee + ReimbAllowCons), ''
		from PR
		where WBS1=@WBS1 and (@WBS2=' ' or WBS2=@WBS2) and (@WBS3=' ' or WBS3=@WBS3) 
		UNION ALL
		select 5 as TopOrder, '', 'Expenses', Sum(ReimbAllowExp + FeeDirExp), ''
		from PR
		where WBS1=@WBS1 and (@WBS2=' ' or WBS2=@WBS2) and (@WBS3=' ' or WBS3=@WBS3)

	-- Add grand total line:
	insert into @T (TopOrder, Row_Detail, Descr, Amount)
		select 90 as TopOrder, 'BackgroundColor=#888888;ForegroundColor=#ffffff;Bold=Y', 'GRAND TOTAL', (select sum(Amount) from @T)
	
	--delete from @T where Amount=0 or isnull(rtrim(Descr), '') = ''

	-- Final recordsets
	select '', '', 'C0;NS', '',   'Total Compensation: '+ @WBS1 + case when @WBS2 <> '' then +' - ' + @WBS2 else '' end + case when @WBS3 <> '' then +' - ' + @WBS3 else '' end 
	select Descr as [Compensation], Row_Detail, Amount, case when Row_Detail = '' then '' else '' end Amount_Detail from @T

	select 'width=500
		height=400' as AdvancedOptions
END
GO
