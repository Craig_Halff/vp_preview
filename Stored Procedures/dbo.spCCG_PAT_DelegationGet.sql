SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_DelegationGet] (
	@Username					Nvarchar(32),
	@BeginDate					datetime,
	@EndDate					datetime,
	@UserEmployeeId				Nvarchar(20),
	@IsSupervisorApprovalOnly	bit = 0,
	@ShowAllDelegations			bit = 1,
	@CanSelfDelegate			bit = 1
)
AS BEGIN
/*
	Copyright (c) 2020 EleVia Software. All rights reserved.

	select * from CCG_PAT_Delegation
	exec spCCG_PAT_DelegationGet 'ADMIN','','', '00001', 1, 1, 1
	exec spCCG_PAT_DelegationGet 'ADMIN','','', '00001', 1, 0, 1
	exec spCCG_PAT_DelegationGet 'ADMIN','2/21/2017','2/28/2017', '00001', 0, 1, 1
	exec spCCG_PAT_DelegationGet 'ADMIN','2/21/2017','3/06/2017', '00001', 0, 1, 1
	exec spCCG_PAT_DelegationGet 'ADMIN','2/21/2017','3/26/2017', '00001', 0, 1, 1
	exec spCCG_PAT_DelegationGet 'ADMIN','3/05/2017','3/6/2017', '00001', 0, 1, 1
	exec spCCG_PAT_DelegationGet 'ADMIN','3/05/2017','3/26/2017', '00001', 0, 1, 1
	exec spCCG_PAT_DelegationGet 'ADMIN','3/25/2017','3/26/2017', '00001', 0, 1, 1
*/
	SET NOCOUNT ON
	IF ISNULL(@BeginDate,'') <> '' AND ISNULL(@EndDate,'') <> ''
		SELECT d.*, 'N' /*CASE WHEN pa.Seq IS NULL THEN 'N' ELSE 'Y' END*/ AS Permanent,
				--pa.ApprovedBy as PermanentApprovedBy, pa.ApprovedOn as PermanentApprovedOn
				null as PermanentApprovedBy, null as PermanentApprovedOn
			FROM CCG_PAT_Delegation d
				-- LEFT JOIN CCG_PAT_DelegationPermanentApprovals pa ON pa.Employee = d.Employee AND pa.Delegate = d.Delegate
				LEFT JOIN EM ON EM.Employee = d.Employee
			WHERE FromDate <= @EndDate and ToDate >= @BeginDate
				AND (@ShowAllDelegations = 1
					OR (@IsSupervisorApprovalOnly = 1 AND EM.Supervisor = @UserEmployeeId)
					OR (d.Employee = @UserEmployeeId OR d.Delegate = @UserEmployeeId)
				)
	ELSE
		SELECT d.*, 'N' /*CASE WHEN pa.Seq IS NULL THEN 'N' ELSE 'Y' END*/ AS Permanent,
				--pa.ApprovedBy as PermanentApprovedBy, pa.ApprovedOn as PermanentApprovedOn
				null as PermanentApprovedBy, null as PermanentApprovedOn
			FROM CCG_PAT_Delegation d
				-- LEFT JOIN CCG_PAT_DelegationPermanentApprovals pa ON pa.Employee = d.Employee AND pa.Delegate = d.Delegate
				LEFT JOIN EM ON EM.Employee = d.Employee
			WHERE (@ShowAllDelegations = 1
					OR (@IsSupervisorApprovalOnly = 1 AND EM.Supervisor = @UserEmployeeId)
					OR (d.Employee = @UserEmployeeId OR d.Delegate = @UserEmployeeId)
				)
END
GO
