SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_PopWin_JTDUnbilledBillingDetailsByEmp] ( @WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7), @Key varchar(32), @EmpName varchar(200), @UserName varchar(32))
             AS EXEC spCCG_EI_PopWin_JTDUnbilledBillingDetailsByEmp @WBS1,@WBS2,@WBS3,@Key,@EmpName,@UserName
GO
