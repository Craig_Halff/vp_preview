SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_UMF_SaveData] ( @product varchar(20), @cacheField varchar(100), @updateTable varchar(200), @updateField varchar(100), @emp varchar(32), @wbs1 nvarchar(30), @wbs2 nvarchar(7), @wbs3 nvarchar(7), @newValue varchar(max), @payableSeq int= 0)
             AS EXEC spCCG_PAT_UMF_SaveData @product,@cacheField,@updateTable,@updateField,@emp,@wbs1,@wbs2,@wbs3,@newValue,@payableSeq
GO
