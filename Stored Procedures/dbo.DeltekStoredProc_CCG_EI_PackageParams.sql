SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_PackageParams] ( @WBS1 nvarchar(30), @Invoice nvarchar(12)= NULL)
             AS EXEC spCCG_EI_PackageParams @WBS1,@Invoice
GO
