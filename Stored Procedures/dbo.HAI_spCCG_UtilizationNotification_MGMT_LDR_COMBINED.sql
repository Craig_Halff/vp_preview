SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Conrad Harrison
-- Create date: 2018-07-09
-- Modified on: 2018-11-13 by Conrad Harrison
-- Description:	Stored Procedure that emails each Management Leader the Management Leader, All Practices, and All Offices stored procedure emails as one email
-- =============================================
CREATE PROCEDURE [dbo].[HAI_spCCG_UtilizationNotification_MGMT_LDR_COMBINED] 
	-- Add the parameters for the stored procedure here
	--**COMMENTED OUT MANAGEMENT LEADER BELOW
	--@ManagementLeaderEmail						VARCHAR(100)

AS

SET NOCOUNT ON
DECLARE 

--PASSED IN MANAGEMENT LEADER EMAIL
@ManagementLeaderEmail						VARCHAR(100),
@CombinedBody								varchar (max),
@CombinedCSS            					varchar (max),
@HalffEmailPass								VARCHAR(100),
@MgmtLeaderSP								varchar (max),
@AllPracticesSP								varchar (max),
@AllOfficesSP								varchar (max),
@MgmtLeaderHeader							VARCHAR(100),
@AllPracticesHeader							VARCHAR(100),
@AllOfficesHeader							VARCHAR(100),
@RowCount									integer,
--**ADDING IN LOOP
@Loop1										integer,

--**TESTING EMAILS
@EmailSentTime								Datetime,
@TestEmail1									VARCHAR(100),
@TestEmail2									VARCHAR(100),
@TestEmail3									VARCHAR(100),
@TestEmail4									VARCHAR(100),
@TestEmail5									VARCHAR(100),
@TestEmail6									VARCHAR(100),
@TestEmail7									VARCHAR(100),
@TestEmailDestination						VARCHAR(100)

Set @HalffEmailPass	= 'halff.com'
Set @MgmtLeaderHeader = 'Management Leader Utilization'
Set @AllPracticesHeader = 'All Practices Utilization'
Set @AllOfficesHeader = 'All Offices Utilization'

--**TESTING EMAILS
SET @TestEmail1	= 'jsagel@halff.com'							
SET @TestEmail2	= 'jsagel@halff.com'								
SET @TestEmail3	= 'jsagel@halff.com'					
SET @TestEmail4	= 'jsagel@halff.com'						
SET @TestEmail5	= 'jsagel@halff.com'								
SET @TestEmail6	= 'jsagel@halff.com'							
SET @TestEmail7	= 'jsagel@halff.com'			

--SET @TestEmail1	= 'charrison@halff.com'							
--SET @TestEmail2	= 'charrison@halff.com'								
--SET @TestEmail3	= 'charrison@halff.com'					
--SET @TestEmail4	= 'charrison@halff.com'						
--SET @TestEmail5	= 'charrison@halff.com'								
--SET @TestEmail6	= 'charrison@halff.com'							
--SET @TestEmail7	= 'charrison@halff.com'					

SET @RowCount = 0

--**TRIAL LOOP CODE HERE
DECLARE curMgmtLeaderUtilization insensitive cursor for

select Distinct 
em.Email
FROM [HalffImport].[dbo].[EMMain] em, [HalffImport].[dbo].[UDIC_OrganizationStructure] uoc
where em.Employee = uoc.CustManagementLeader

--ADDING IN LOOP

OPEN curMgmtLeaderUtilization

FETCH NEXT FROM curMgmtLeaderUtilization INTO @ManagementLeaderEmail

While @@Fetch_Status = 0

--**TRIAL LOOK CODE ABOVE

BEGIN

	SET @RowCount = @RowCount + 1

	EXECUTE [dbo].[HAI_spCCG_UtilizationNotification_Mgmt_Leader] @ManagementLeaderEmail, @MgmtLeaderSP Output
	EXEC [dbo].[HAI_spCCG_UtilizationNotification_All_Practices] @AllPracticesSP Output
	EXEC [dbo].[HAI_spCCG_UtilizationNotification_All_Offices] @AllOfficesSP Output

-- CSS style rules
	SET @CombinedCSS = 
'
<style type="text/css">
	--p {padding-top:15px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;}
	th {border: 2px solid black;padding: 5px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;font-size:10.5pt;}
	th {background-color: #808080;color: white;text-align:center;}
	.team {text-align:left; border-left: 2px solid Black; background-color: white; border-bottom: 1px solid #6E6E6E;}
	.teamB {font-weight: bold; text-align:left; border-left: 2px solid Black; background-color: white; border-bottom: 1px solid #6E6E6E;}
	.LW {color: black; background-color: white; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
	.L4W {color: black; background-color: #e6e6e6; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;} 
	.L12W {background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
	.L12Wr {color: black; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-right: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
	.LWF {color: black; font-weight: bold; background-color: white; text-align:right; border-left: 2px solid Black; border-top: 2px solid Black;}
	.LWFLeft {font-weight: bold; background-color: white; text-align:left; border-left: 2px solid Black; border-top: 2px solid Black;}
	.L4WF {color: black; font-weight: bold; background-color: #e6e6e6; text-align:right; border-left: 2px solid Black; border-top: 2px solid Black;} 
	.L12WF {font-weight: bold; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-top: 2px solid Black;}
	.L12WrF {color: black; font-weight: bold; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-right: 2px solid Black; border-top: 2px solid Black;}
	.redText {color:#FF0000;}
	</style>
'

--**CALL OTHER STORED PROCEDURES HERE
	SET @CombinedBody = @MgmtLeaderHeader + @MgmtLeaderSP + @AllPracticesHeader + @AllPracticesSP +	@AllOfficesHeader + @AllOfficesSP

	SET @EmailSentTime = GETDATE() 

	--** TEST EMAIL CASE LOGIC
	Set @TestEmailDestination =
		CASE WHEN @RowCount = 1 THEN @TestEmail1
			 WHEN @RowCount = 2 THEN @TestEmail2
			 WHEN @RowCount = 3 THEN @TestEmail3
			 WHEN @RowCount = 4 THEN @TestEmail4
			 WHEN @RowCount = 5 THEN @TestEmail5
			 WHEN @RowCount = 6 THEN @TestEmail6
			 WHEN @RowCount = 7 THEN @TestEmail7
			 Else 'jsagel@halff.com'
			 --Else 'charrison@halff.com'
		End
	
	--**ADJUST EMAIL RECIPIENT BELOW, CURRENTLY SET TO THE PROXY RECIPIENTS

	Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
	Values ('Utilization Summary', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Utilization Summary For: ' + CAST(@ManagementLeaderEmail as Varchar(100)) + ' On: ' + CAST(@EmailSentTime as Varchar(100)), @CombinedCSS + 
	@CombinedBody, getDate(), 1)

	--*COMMENTED OUT @ManagementLeaderEmail to TEST

	--Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
	--Values ('Utilization Summary', @ManagementLeaderEmail, @ManagementLeaderEmail, @ManagementLeaderEmail, 'Utilization Summary ' + CAST(@EmailSentTime as Varchar(100)), @CombinedCSS + 
	--@CombinedBody, getDate(), 1)
	
	FETCH NEXT FROM curMgmtLeaderUtilization INTO @ManagementLeaderEmail

END

--ADDING IN LOOP INFO
	CLOSE curMgmtLeaderUtilization
	DEALLOCATE curMgmtLeaderUtilization
GO
