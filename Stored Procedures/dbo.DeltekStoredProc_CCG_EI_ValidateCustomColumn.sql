SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_ValidateCustomColumn] ( @objectName varchar(100), @hasParamOrCol varchar(20), @lookForErrors int, @ValidateOnly int, @oName varchar(10), @oType varchar(10), @HasParamCol varchar(1), @HasWBS1 varchar(1), @HasWBS2 varchar(1), @HasWBS3 varchar(1), @HasUserNameParam varchar(1), @HasEmployeeParam varchar(1), @InitError varchar(max))
             AS EXEC spCCG_EI_ValidateCustomColumn @objectName,@hasParamOrCol,@lookForErrors,@ValidateOnly,@oName,@oType,@HasParamCol,@HasWBS1,@HasWBS2,@HasWBS3,@HasUserNameParam,@HasEmployeeParam,@InitError
GO
