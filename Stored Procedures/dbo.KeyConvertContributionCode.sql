SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyConvertContributionCode]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg Nvarchar(1000) output,
@UserName Nvarchar(32) = 'VisionKeyConvert' -- Add by DP 4.0
,@Period integer = 0,
@EntityType Nvarchar(20)
as
begin -- Procedure
	set nocount on
	declare @OldValue Nvarchar(20)
	declare @NewValue Nvarchar(20)
	Declare @Existing integer
	declare @Entity Nvarchar(20)
	declare @length integer
	declare @CvtType integer
	declare @KeysFetch integer
	declare @RetVal integer
	declare @diag smallint
	declare @Errmsg2 Nvarchar(1000),
			@custlabel	Nvarchar(40),
			@Custlabelplural Nvarchar(40)
	declare @message nvarchar(max)
	set @diag = 0
	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'ContributionCode'
	set @length = 10
	set @Existing = 0
	set @KeysFetch = 0

	execute InsertKeyCvtDriver @Entity -- populate driver table

-- Add by DP 4.0
	delete from keyconvertDriver where entity = N'ContributionCode' and TableName = N'CFGPYContribution'
	DECLARE @OldName Nvarchar(40)
	DECLARE @NewName Nvarchar(40)
	DECLARE @PKey Nvarchar(50)
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0
--
	select @custlabel = 'Contribution Code',
			 @Custlabelplural = 'Contribution Codes'
	declare KeysCursor cursor for
		select OldKey, NewKey, Pkey from KeyConvertWork where Entity = @EntityType
	open KeysCursor
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue, 
		@PKey

	set @KeysFetch = @@Fetch_Status

While (@KeysFetch = 0)
	begin
		begin tran
-- Add by DP 4.0
		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany Nvarchar(14)
			DECLARE @RecordCount integer

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)
			  	
				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				set @RecordCount = (select count(*) from KeyConvertWork where Entity = @EntityType)

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),N'KC','Y',  @custlabel + ' Key Convert ('+rtrim(convert(Nvarchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, @custlabel + ' Key Convert', @custlabel);
			end

			Set @PostControlFlag = 1
		end

		set @Existing = 0
		select @Existing =  1, @NewName = Description from CFGPYContribution where Code = @NewValue
		select @OldName = Description from CFGPYContribution where Code = @OldValue
--

	If (@Existing = 1)
		begin
		  if (@DeleteExisting = 0)
			begin
			SET @message = dbo.GetMessage('MsgNumAnExistingConvWillNotCont',@custlabel,@NewValue,@custlabel,@custLabelPlural,'','','','','')
			RAISERROR(@message,16,3)
				close keysCursor
				deallocate keysCursor
				if (@@Trancount > 0)
				   ROLLBACK TRANSACTION
				return(50002) -- user defined error
			end
			if (@NewValue = @OldValue)
				begin
				SET @message = dbo.GetMessage('MsgAreSameNumConvNotCont',@custlabelPlural,'','','','','','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end
				declare @MulticompanyEnabled Nvarchar(1)
				declare @MulticurrencyEnabled Nvarchar(1)
				declare @NewFunctionalCurrencyCode Nvarchar(3)
				declare @OldFunctionalCurrencyCode Nvarchar(3)
				declare @oldCompay Nvarchar(14)
				declare @newCompay Nvarchar(14)
				select @MulticompanyEnabled = MulticompanyEnabled,
			   		 @MulticurrencyEnabled = MulticurrencyEnabled from FW_CFGSystem
				
				if (@MulticompanyEnabled = 'Y')
					begin
						select @oldCompay = old.Company,
								 @newCompay = new.Company from CFGPYContribution old,CFGPYContribution new, cfgformat 
								where old.Code = @OldValue and new.Code = @NewValue
						if (@oldCompay <> @newCompay)
							begin
							SET @message = dbo.GetMessage('MsgWithMultiCmpYouCannotMerge',@custlabelPlural,@custlabel,@OldValue,@oldCompay,@custlabel,@NewValue,@newCompay,'','')
							RAISERROR(@message,16,3)
								close keysCursor
								deallocate keysCursor
								if (@@Trancount > 0)
								   ROLLBACK TRANSACTION
								return(50003) -- user defined error
							end
					end

/* Removed by DP 4.0
			  exec @RetVal = KeyCvt @Entity = @Entity,
											@CvtType = @CvtType,
											@Length = @Length,
											@ErrMsg = @ErrMsg2 output,
											@Diag = @Diag,
											@ConstrFlag = 3 -- UpdateTables
*/

		Delete From EMPayrollContribution Where EMPayrollContribution.Code = @OldValue 
			And Exists (Select 'x' From EMPayrollContribution As Tmp Where Tmp.Code = @NewValue 
			And Tmp.Employee = EMPayrollContribution.Employee and EMPayrollContribution.EmployeeCompany = Tmp.EmployeeCompany)

	        Update EMPayrollContributionDetail 
			Set EMPayrollContributionDetail.GrossPayBasis = EMPayrollContributionDetail.GrossPayBasis + Tmp.GrossPayBasis, 
			EMPayrollContributionDetail.Amount = EMPayrollContributionDetail.Amount + Tmp.Amount,
			EMPayrollContributionDetail.TaxablePayBasis = EMPayrollContributionDetail.TaxablePayBasis + tmp.TaxablePayBasis,
			EMPayrollContributionDetail.AdjustedGrossPayBasis = EMPayrollContributionDetail.AdjustedGrossPayBasis + tmp.AdjustedGrossPayBasis,
			EMPayrollContributionDetail.Amt401K = EMPayrollContributionDetail.Amt401K + tmp.Amt401K,
			EMPayrollContributionDetail.OtherPay1 = EMPayrollContributionDetail.OtherPay1 + tmp.OtherPay1,
			EMPayrollContributionDetail.OtherPay2 = EMPayrollContributionDetail.OtherPay2 + tmp.OtherPay2,
			EMPayrollContributionDetail.OtherPay3 = EMPayrollContributionDetail.OtherPay3 + tmp.OtherPay3,
			EMPayrollContributionDetail.OtherPay4 = EMPayrollContributionDetail.OtherPay4 + tmp.OtherPay4,
			EMPayrollContributionDetail.OtherPay5 = EMPayrollContributionDetail.OtherPay5 + tmp.OtherPay5,
			EMPayrollContributionDetail.Amt125 = EMPayrollContributionDetail.Amt125 + tmp.Amt125
			from EMPayrollContributionDetail Inner Join EMPayrollContributionDetail As Tmp On 
			EMPayrollContributionDetail.Employee = Tmp.Employee 
			And EMPayrollContributionDetail.Period = Tmp.Period 
			And EMPayrollContributionDetail.PostSeq = Tmp.PostSeq 
			Where EMPayrollContributionDetail.Code = @NewValue 
			And Tmp.Code = @OldValue

		if (@@RowCount > 0)
			Delete FROM EMPayrollContributionDetail Where EMPayrollContributionDetail.code = @OldValue 
				And Exists (Select 'x' from EMPayrollContributionDetail AS Tmp Where Tmp.code = @NewValue 
				And Tmp.Employee = EMPayrollContributionDetail.Employee 
				And Tmp.Period = EMPayrollContributionDetail.Period 
				And Tmp.PostSeq = EMPayrollContributionDetail.PostSeq)

	        Update EMPayrollContributionDetailWage 
			Set EMPayrollContributionDetailWage.Amount = EMPayrollContributionDetailWage.Amount + Tmp.Amount
			from EMPayrollContributionDetailWage Inner Join EMPayrollContributionDetailWage As Tmp On 
			EMPayrollContributionDetailWage.Employee = Tmp.Employee 
			And EMPayrollContributionDetailWage.Period = Tmp.Period 
			And EMPayrollContributionDetailWage.PostSeq = Tmp.PostSeq 
			And EMPayrollContributionDetailWage.CodeWage = Tmp.CodeWage 
			Where EMPayrollContributionDetailWage.Code = @NewValue 
			And Tmp.Code = @OldValue

		if (@@RowCount > 0)
			Delete FROM EMPayrollContributionDetailWage Where EMPayrollContributionDetailWage.code = @OldValue 
				And Exists (Select 'x' from EMPayrollContributionDetailWage AS Tmp Where Tmp.code = @NewValue 
				And Tmp.Employee = EMPayrollContributionDetailWage.Employee 
				And Tmp.Period = EMPayrollContributionDetailWage.Period 
				And Tmp.CodeWage = EMPayrollContributionDetailWage.CodeWage 
				And Tmp.PostSeq = EMPayrollContributionDetailWage.PostSeq)

		Delete FROM PYChecksC Where PYChecksC.code = @OldValue
			And Exists (Select 'x' from PYChecksC AS Tmp Where Tmp.code = @NewValue 
			And Tmp.Employee = PYChecksC.Employee 
			And Tmp.Period = PYChecksC.Period 
			And Tmp.PostSeq = PYChecksC.PostSeq)

		Delete From CFGPYContributionWage Where CFGPYContributionWage.Code = @OldValue 
			And Exists (Select 'x' From CFGPYContributionWage As Tmp Where Tmp.Code = @NewValue 
			And Tmp.Company = CFGPYContributionWage.Company
			And Tmp.CodeWage = CFGPYContributionWage.CodeWage)

		Delete From EMPayrollContributionWage Where EMPayrollContributionWage.Code = @OldValue 
			And Exists (Select 'x' From EMPayrollContributionWage As Tmp Where Tmp.Code = @NewValue 
			And Tmp.Employee = EMPayrollContributionWage.Employee and EMPayrollContributionWage.EmployeeCompany = Tmp.EmployeeCompany
			And (Tmp.CodeWage = EMPayrollContributionWage.CodeWage))
-- Removed by DP
--			Delete from CFGPYContribution where Code = @OldValue
			
	    END --Existing
-- Add by DP 4.0
		Else	-- Not Existing
		begin
			select * into #TempKeyConvert from CFGPYContribution where Code = @OldValue
			Update #TempKeyConvert set Code=@NewValue
			Insert CFGPYContribution select * from #TempKeyConvert
			Drop Table #TempKeyConvert
			set @NewName = @OldName
		end

		exec @RetVal = KeyCvt @Entity = @Entity,
					 @CvtType = @CvtType,
					 @Length = @Length,
					 @ErrMsg = @ErrMsg2 output,
					 @Diag = @Diag,
					 @NewValue = @NewValue,
					 @OldValue = @OldValue,
					 @ConstrFlag = 3 -- UpdateTables
		Delete from CFGPYContribution where Code = @OldValue
--
		if (@RetVal = 0)
			begin
-- Add by DP 4.0
				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq)
					select PKey, Entity, OldKey, NewKey, @OldName, @NewName, @Period, @LastPostSeq from keyConvertWork where Pkey = @PKey
				end
--
				delete from keyConvertWork where current of keysCursor
				commit Transaction
			end
		else
			begin
				set @ErrMsg = @ErrMsg + @ErrMsg2
				rollback transaction
			end
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue,
		@PKey
	set @KeysFetch = @@Fetch_Status
	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	return(0)
End -- Procedure
GO
