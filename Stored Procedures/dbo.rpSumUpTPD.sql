SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpSumUpTPD]
  @strPlanID VARCHAR(32),
  @strCalcLab VARCHAR(1) = 'Y',
  @strCalcExp VARCHAR(1) = 'Y',
  @strCalcCon VARCHAR(1) = 'Y',
  @strCalcUnt VARCHAR(1) = 'Y',
  @strCalcCompF VARCHAR(1) = 'N',
  @strCalcConsF VARCHAR(1) = 'N',
  @strCalcReimA VARCHAR(1) = 'N'
AS

BEGIN -- Procedure rpSumUpTPD

  SET NOCOUNT ON

  DECLARE @intOutlineLevel integer

  DECLARE @intHrDecimals int
  DECLARE @intQtyDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intLabRevDecimals int
  DECLARE @intECURevDecimals int
  
  DECLARE @intPlannedLabCount int
  DECLARE @intPlannedExpCount int
  DECLARE @intPlannedConCount int
  DECLARE @intPlannedUntCount int
     
  DECLARE @intCompCount int
  DECLARE @intConsCount int
  DECLARE @intReimCount int
  
  DECLARE @tabCalendarInterval
    TABLE(PlanID varchar(32) COLLATE database_default,
          StartDate datetime,
          EndDate datetime,
          PeriodScale varchar(1) COLLATE database_default
          PRIMARY KEY(PlanID, StartDate, EndDate, PeriodScale))

  -- Get decimal settings.
  
  SELECT @intHrDecimals = HrDecimals,
         @intQtyDecimals = QtyDecimals,
         @intAmtCostDecimals = AmtCostDecimals,
         @intAmtBillDecimals = AmtBillDecimals,
         @intLabRevDecimals = LabRevDecimals, 
         @intECURevDecimals = ECURevDecimals
    FROM dbo.RP$tabDecimals(@strPlanID)
  
  -- Check to see if there is any time-phased data
  
  IF (@strCalcLab = 'Y') SELECT @intPlannedLabCount = COUNT(*) FROM RPPlannedLabor WHERE PlanID = @strPlanID
  IF (@strCalcExp = 'Y') SELECT @intPlannedExpCount = COUNT(*) FROM RPPlannedExpenses WHERE PlanID = @strPlanID
  IF (@strCalcCon = 'Y') SELECT @intPlannedConCount = COUNT(*) FROM RPPlannedConsultant WHERE PlanID = @strPlanID
  IF (@strCalcUnt = 'Y') SELECT @intPlannedUntCount = COUNT(*) FROM RPPlannedUnit WHERE PlanID = @strPlanID

  IF (@strCalcCompF = 'Y') SELECT @intCompCount = COUNT(*) FROM RPCompensationFee WHERE PlanID = @strPlanID
  IF (@strCalcConsF = 'Y') SELECT @intConsCount = COUNT(*) FROM RPConsultantFee WHERE PlanID = @strPlanID
  IF (@strCalcReimA = 'Y') SELECT @intReimCount = COUNT(*) FROM RPReimbAllowance WHERE PlanID = @strPlanID

  -- Save Calendar Intervals into a temp table and add an overflow Calendar Interval.
  
  INSERT @tabCalendarInterval(PlanID, StartDate, EndDate, PeriodScale)
  SELECT PlanID, StartDate, EndDate, PeriodScale
  FROM
    (SELECT PlanID AS PlanID, StartDate AS StartDate, EndDate AS EndDate, PeriodScale AS PeriodScale
       FROM RPCalendarInterval WHERE PlanID = @strPlanID
     UNION ALL
     SELECT PlanID AS PlanID, DATEADD(d, 1, MAX(EndDate)) AS StartDate,
       DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate))) AS EndDate,
       'o' AS PeriodScale
       FROM RPCalendarInterval WHERE PlanID = @strPlanID
       GROUP BY PlanID) AS CI
       
  -- Get Maximum OutlineLevel.

  SELECT @intOutlineLevel = ISNULL(MAX(OutlineLevel), -1) FROM RPTask
    WHERE RPTask.PlanID = @strPlanID

  -- Loop through all Task rows starting from the highest OutlineLevel
  -- to insert summed-up time-phased data rows.

  WHILE (@intOutlineLevel >= 0)
    BEGIN

      -- Insert Labor time-phased data rows.

      IF (@strCalcLab = 'Y' AND @intPlannedLabCount > 0)
        INSERT INTO RPPlannedLabor(TimePhaseID, PlanID, TaskID,
          StartDate, EndDate, PeriodHrs, PeriodCost, PeriodBill, PeriodRev,
          PeriodCount, PeriodScale, CreateDate, ModDate)
        (SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, PlanID AS PlanID, TaskID AS TaskID, 
           MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate, 
           ROUND(ISNULL(SUM(PeriodHrs), 0), @intHrDecimals) AS PeriodHrs, 
           ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals) AS PeriodCost,
           ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals) AS PeriodBill, 
           ROUND(ISNULL(SUM(PeriodRev), 0), @intLabRevDecimals) AS PeriodRev,
           PeriodCount AS PeriodCount, PeriodScale AS PeriodScale, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
           FROM (SELECT CI.StartDate AS CIStartDate, T.PlanID AS PlanID, T.TaskID AS TaskID, 
                   MIN(TPD.StartDate) AS StartDate, MAX(TPD.EndDate) AS EndDate, 
                   ISNULL(SUM(PeriodHrs), 0) AS PeriodHrs, ISNULL(SUM(PeriodCost), 0) AS PeriodCost,
                   ISNULL(SUM(PeriodBill), 0) AS PeriodBill, ISNULL(SUM(PeriodRev), 0) AS PeriodRev,
                   1 AS PeriodCount, CI.PeriodScale AS PeriodScale
                   FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                   INNER JOIN RPPlannedLabor AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                     AND TPD.StartDate >= CI.StartDate AND TPD.EndDate <= CI.EndDate
                     AND TPD.AssignmentID IS NOT NULL
                   WHERE TPD.TimePhaseID IS NOT NULL 
                     AND T.OutlineLevel = @intOutlineLevel
                   GROUP BY CI.StartDate, CI.PeriodScale, T.TaskID, T.PlanID
                 UNION ALL
                 SELECT CI.StartDate AS CIStartDate, T.PlanID AS PlanID, T.TaskID AS TaskID, 
                   MIN(TPD.StartDate) AS StartDate, MAX(TPD.EndDate) AS EndDate, 
                   ISNULL(SUM(PeriodHrs), 0) AS PeriodHrs, ISNULL(SUM(PeriodCost), 0) AS PeriodCost,
                   ISNULL(SUM(PeriodBill), 0) AS PeriodBill, ISNULL(SUM(PeriodRev), 0) AS PeriodRev,
                   1 AS PeriodCount, CI.PeriodScale AS PeriodScale
                   FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                   INNER JOIN RPTask AS CT ON T.PlanID = CT.PlanID AND T.OutlineNumber = CT.ParentOutlineNumber
                   INNER JOIN RPPlannedLabor AS TPD ON TPD.PlanID = CT.PlanID AND TPD.TaskID = CT.TaskID 
                     AND TPD.StartDate >= CI.StartDate AND TPD.EndDate <= CI.EndDate
                     AND TPD.AssignmentID IS NULL
                   WHERE TPD.TimePhaseID IS NOT NULL 
                     AND T.ChildrenCount <> 0  AND T.OutlineLevel = @intOutlineLevel
                   GROUP BY T.Name, CI.StartDate, CI.PeriodScale, T.TaskID, T.PlanID) AS X
           GROUP BY PlanID, TaskID, CIStartDate, PeriodCount, PeriodScale)
        
      -- Insert Expense time-phased data rows.

      IF (@strCalcExp = 'Y' AND @intPlannedExpCount > 0)
        INSERT INTO RPPlannedExpenses(TimePhaseID, PlanID, TaskID,
          StartDate, EndDate, PeriodCost, PeriodBill, PeriodRev,
          PeriodCount, PeriodScale, CreateDate, ModDate)
        (SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, PlanID AS PlanID, TaskID AS TaskID, 
           MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate, 
           ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals) AS PeriodCost, 
           ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals) AS PeriodBill, 
           ROUND(ISNULL(SUM(PeriodRev), 0), @intECURevDecimals) AS PeriodRev,
           PeriodCount AS PeriodCount, PeriodScale AS PeriodScale, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
           FROM (SELECT CI.StartDate AS CIStartDate, T.PlanID AS PlanID, T.TaskID AS TaskID, 
                   MIN(TPD.StartDate) AS StartDate, MAX(TPD.EndDate) AS EndDate, 
                   ISNULL(SUM(PeriodCost), 0) AS PeriodCost, ISNULL(SUM(PeriodBill), 0) AS PeriodBill, ISNULL(SUM(PeriodRev), 0) AS PeriodRev,
                   1 AS PeriodCount, CI.PeriodScale AS PeriodScale
                   FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                   INNER JOIN RPPlannedExpenses AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                     AND TPD.StartDate >= CI.StartDate AND TPD.EndDate <= CI.EndDate
                     AND TPD.ExpenseID IS NOT NULL
                   WHERE TPD.TimePhaseID IS NOT NULL 
                     AND T.OutlineLevel = @intOutlineLevel
                   GROUP BY CI.StartDate, CI.PeriodScale, T.TaskID, T.PlanID
                 UNION ALL
                 SELECT CI.StartDate AS CIStartDate, T.PlanID AS PlanID, T.TaskID AS TaskID, 
                   MIN(TPD.StartDate) AS StartDate, MAX(TPD.EndDate) AS EndDate, 
                   ISNULL(SUM(PeriodCost), 0) AS PeriodCost, ISNULL(SUM(PeriodBill), 0) AS PeriodBill, ISNULL(SUM(PeriodRev), 0) AS PeriodRev,
                   1 AS PeriodCount, CI.PeriodScale AS PeriodScale
                   FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                   INNER JOIN RPTask AS CT ON T.PlanID = CT.PlanID AND T.OutlineNumber = CT.ParentOutlineNumber
                   INNER JOIN RPPlannedExpenses AS TPD ON TPD.PlanID = CT.PlanID AND TPD.TaskID = CT.TaskID 
                     AND TPD.StartDate >= CI.StartDate AND TPD.EndDate <= CI.EndDate
                     AND TPD.ExpenseID IS NULL
                   WHERE TPD.TimePhaseID IS NOT NULL 
                     AND T.ChildrenCount <> 0  AND T.OutlineLevel = @intOutlineLevel
                   GROUP BY T.Name, CI.StartDate, CI.PeriodScale, T.TaskID, T.PlanID) AS X
           GROUP BY PlanID, TaskID, CIStartDate, PeriodCount, PeriodScale)
        
      -- Insert Consultant time-phased data rows.

      IF (@strCalcCon = 'Y' AND @intPlannedConCount > 0)
        INSERT INTO RPPlannedConsultant(TimePhaseID, PlanID, TaskID,
          StartDate, EndDate, PeriodCost, PeriodBill, PeriodRev,
          PeriodCount, PeriodScale, CreateDate, ModDate)
        (SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, PlanID AS PlanID, TaskID AS TaskID, 
           MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate, 
           ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals) AS PeriodCost, 
           ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals) AS PeriodBill, 
           ROUND(ISNULL(SUM(PeriodRev), 0), @intECURevDecimals) AS PeriodRev,
           PeriodCount AS PeriodCount, PeriodScale AS PeriodScale, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
           FROM (SELECT CI.StartDate AS CIStartDate, T.PlanID AS PlanID, T.TaskID AS TaskID, 
                   MIN(TPD.StartDate) AS StartDate, MAX(TPD.EndDate) AS EndDate, 
                   ISNULL(SUM(PeriodCost), 0) AS PeriodCost, ISNULL(SUM(PeriodBill), 0) AS PeriodBill, ISNULL(SUM(PeriodRev), 0) AS PeriodRev,
                   1 AS PeriodCount, CI.PeriodScale AS PeriodScale
                   FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                   INNER JOIN RPPlannedConsultant AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                     AND TPD.StartDate >= CI.StartDate AND TPD.EndDate <= CI.EndDate
                     AND TPD.ConsultantID IS NOT NULL
                   WHERE TPD.TimePhaseID IS NOT NULL 
                     AND T.OutlineLevel = @intOutlineLevel
                   GROUP BY CI.StartDate, CI.PeriodScale, T.TaskID, T.PlanID
                 UNION ALL
                 SELECT CI.StartDate AS CIStartDate, T.PlanID AS PlanID, T.TaskID AS TaskID, 
                   MIN(TPD.StartDate) AS StartDate, MAX(TPD.EndDate) AS EndDate, 
                   ISNULL(SUM(PeriodCost), 0) AS PeriodCost, ISNULL(SUM(PeriodBill), 0) AS PeriodBill, ISNULL(SUM(PeriodRev), 0) AS PeriodRev,
                   1 AS PeriodCount, CI.PeriodScale AS PeriodScale
                   FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                   INNER JOIN RPTask AS CT ON T.PlanID = CT.PlanID AND T.OutlineNumber = CT.ParentOutlineNumber
                   INNER JOIN RPPlannedConsultant AS TPD ON TPD.PlanID = CT.PlanID AND TPD.TaskID = CT.TaskID 
                     AND TPD.StartDate >= CI.StartDate AND TPD.EndDate <= CI.EndDate
                     AND TPD.ConsultantID IS NULL
                   WHERE TPD.TimePhaseID IS NOT NULL 
                     AND T.ChildrenCount <> 0  AND T.OutlineLevel = @intOutlineLevel
                   GROUP BY T.Name, CI.StartDate, CI.PeriodScale, T.TaskID, T.PlanID) AS X
           GROUP BY PlanID, TaskID, CIStartDate, PeriodCount, PeriodScale)
        
      -- Insert Unit time-phased data rows.

      IF (@strCalcUnt = 'Y' AND @intPlannedUntCount > 0)
        INSERT INTO RPPlannedUnit(TimePhaseID, PlanID, TaskID,
          StartDate, EndDate, PeriodQty, PeriodCost, PeriodBill, PeriodRev,
          PeriodCount, PeriodScale, CreateDate, ModDate)
        (SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, PlanID AS PlanID, TaskID AS TaskID, 
           MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate, 
           ROUND(ISNULL(SUM(PeriodQty), 0), @intQtyDecimals) AS PeriodQty, 
           ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals) AS PeriodCost, 
           ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals) AS PeriodBill, 
           ROUND(ISNULL(SUM(PeriodRev), 0), @intECURevDecimals) AS PeriodRev,
           PeriodCount AS PeriodCount, PeriodScale AS PeriodScale, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
           FROM (SELECT CI.StartDate AS CIStartDate, T.PlanID AS PlanID, T.TaskID AS TaskID, 
                   MIN(TPD.StartDate) AS StartDate, MAX(TPD.EndDate) AS EndDate, 
                   ISNULL(SUM(PeriodQty), 0) AS PeriodQty, ISNULL(SUM(PeriodCost), 0) AS PeriodCost,
                   ISNULL(SUM(PeriodBill), 0) AS PeriodBill, ISNULL(SUM(PeriodRev), 0) AS PeriodRev,
                   1 AS PeriodCount, CI.PeriodScale AS PeriodScale
                   FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                   INNER JOIN RPPlannedUnit AS TPD ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID 
                     AND TPD.StartDate >= CI.StartDate AND TPD.EndDate <= CI.EndDate
                     AND TPD.UnitID IS NOT NULL
                   WHERE TPD.TimePhaseID IS NOT NULL 
                     AND T.OutlineLevel = @intOutlineLevel
                   GROUP BY CI.StartDate, CI.PeriodScale, T.TaskID, T.PlanID
                 UNION ALL
                 SELECT CI.StartDate AS CIStartDate, T.PlanID AS PlanID, T.TaskID AS TaskID, 
                   MIN(TPD.StartDate) AS StartDate, MAX(TPD.EndDate) AS EndDate, 
                   ISNULL(SUM(PeriodQty), 0) AS PeriodQty, ISNULL(SUM(PeriodCost), 0) AS PeriodCost,
                   ISNULL(SUM(PeriodBill), 0) AS PeriodBill, ISNULL(SUM(PeriodRev), 0) AS PeriodRev,
                   1 AS PeriodCount, CI.PeriodScale AS PeriodScale
                   FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                   INNER JOIN RPTask AS CT ON T.PlanID = CT.PlanID AND T.OutlineNumber = CT.ParentOutlineNumber
                   INNER JOIN RPPlannedUnit AS TPD ON TPD.PlanID = CT.PlanID AND TPD.TaskID = CT.TaskID 
                     AND TPD.StartDate >= CI.StartDate AND TPD.EndDate <= CI.EndDate
                     AND TPD.UnitID IS NULL
                   WHERE TPD.TimePhaseID IS NOT NULL 
                     AND T.ChildrenCount <> 0  AND T.OutlineLevel = @intOutlineLevel
                   GROUP BY T.Name, CI.StartDate, CI.PeriodScale, T.TaskID, T.PlanID) AS X
           GROUP BY PlanID, TaskID, CIStartDate, PeriodCount, PeriodScale)
          
      -- Insert Compensation Fee time-phased data rows.

      IF (@strCalcCompF = 'Y' AND @intCompCount > 0)
        INSERT INTO RPCompensationFee(TimePhaseID, PlanID, TaskID,
          StartDate, EndDate, PeriodCost, PeriodBill, CreateDate, ModDate,PeriodDirLabCost,PeriodDirExpCost,PeriodDirLabBill,PeriodDirExpBill)
        (SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, PlanID AS PlanID, TaskID AS TaskID, 
           MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate, 
           ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals) AS PeriodCost, 
           ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals) AS PeriodBill, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate,

		   /* Compensation breakout */
           ROUND(ISNULL(SUM(PeriodDirLabCost), 0), @intAmtCostDecimals) AS PeriodDirLabCost, 
           ROUND(ISNULL(SUM(PeriodDirExpCost), 0), @intAmtCostDecimals) AS PeriodDirExpCost, 
           ROUND(ISNULL(SUM(PeriodDirLabBill), 0), @intAmtBillDecimals) AS PeriodDirLabBill, 
           ROUND(ISNULL(SUM(PeriodDirExpBill), 0), @intAmtBillDecimals) AS PeriodDirExpBill

           FROM (SELECT CI.StartDate AS CIStartDate, T.PlanID AS PlanID, T.TaskID AS TaskID, 
                   MIN(TPD.StartDate) AS StartDate, MAX(TPD.EndDate) AS EndDate, 
                   ISNULL(SUM(PeriodCost), 0) AS PeriodCost, 
                   ISNULL(SUM(PeriodBill), 0) AS PeriodBill,

				   /* Compensation breakout */
                   ISNULL(SUM(PeriodDirLabCost), 0) AS PeriodDirLabCost, 
                   ISNULL(SUM(PeriodDirExpCost), 0) AS PeriodDirExpCost, 
                   ISNULL(SUM(PeriodDirLabBill), 0) AS PeriodDirLabBill,
                   ISNULL(SUM(PeriodDirExpBill), 0) AS PeriodDirExpBill

                   FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                   INNER JOIN RPTask AS CT ON T.PlanID = CT.PlanID AND T.OutlineNumber = CT.ParentOutlineNumber
                   INNER JOIN RPCompensationFee AS TPD ON TPD.PlanID = CT.PlanID AND TPD.TaskID = CT.TaskID 
                     AND TPD.StartDate >= CI.StartDate AND TPD.EndDate <= CI.EndDate
                   WHERE TPD.TimePhaseID IS NOT NULL 
                     AND T.ChildrenCount <> 0  AND T.OutlineLevel = @intOutlineLevel
                   GROUP BY T.Name, CI.StartDate, T.TaskID, T.PlanID) AS X
           GROUP BY PlanID, TaskID, CIStartDate)
             
      -- Insert Consultant Fee time-phased data rows.

      IF (@strCalcConsF = 'Y' AND @intConsCount > 0)
        INSERT INTO RPConsultantFee(TimePhaseID, PlanID, TaskID,
          StartDate, EndDate, PeriodCost, PeriodBill, CreateDate, ModDate)
        (SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, PlanID AS PlanID, TaskID AS TaskID, 
           MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate, 
           ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals) AS PeriodCost, 
           ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals) AS PeriodBill, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
           FROM (SELECT CI.StartDate AS CIStartDate, T.PlanID AS PlanID, T.TaskID AS TaskID, 
                   MIN(TPD.StartDate) AS StartDate, MAX(TPD.EndDate) AS EndDate, 
                   ISNULL(SUM(PeriodCost), 0) AS PeriodCost, 
                   ISNULL(SUM(PeriodBill), 0) AS PeriodBill
                   FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                   INNER JOIN RPTask AS CT ON T.PlanID = CT.PlanID AND T.OutlineNumber = CT.ParentOutlineNumber
                   INNER JOIN RPConsultantFee AS TPD ON TPD.PlanID = CT.PlanID AND TPD.TaskID = CT.TaskID 
                     AND TPD.StartDate >= CI.StartDate AND TPD.EndDate <= CI.EndDate
                   WHERE TPD.TimePhaseID IS NOT NULL 
                     AND T.ChildrenCount <> 0  AND T.OutlineLevel = @intOutlineLevel
                   GROUP BY T.Name, CI.StartDate, T.TaskID, T.PlanID) AS X
           GROUP BY PlanID, TaskID, CIStartDate)
            
      -- Insert Reimbursable Allowance time-phased data rows.

      IF (@strCalcReimA = 'Y' AND @intReimCount > 0)
        INSERT INTO RPReimbAllowance(TimePhaseID, PlanID, TaskID,
          StartDate, EndDate, PeriodCost, PeriodBill, CreateDate, ModDate,PeriodExpCost,PeriodConCost,PeriodExpBill,PeriodConBill)
        (SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, PlanID AS PlanID, TaskID AS TaskID, 
           MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate, 
           ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals) AS PeriodCost, 
           ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals) AS PeriodBill, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate, 
           LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate,

		   /* Compensation breakout */
           ROUND(ISNULL(SUM(PeriodExpCost), 0), @intAmtCostDecimals) AS PeriodExpCost, 
           ROUND(ISNULL(SUM(PeriodConCost), 0), @intAmtCostDecimals) AS PeriodConCost, 
           ROUND(ISNULL(SUM(PeriodExpBill), 0), @intAmtBillDecimals) AS PeriodExpBill, 
           ROUND(ISNULL(SUM(PeriodConBill), 0), @intAmtBillDecimals) AS PeriodConBill 

           FROM (SELECT CI.StartDate AS CIStartDate, T.PlanID AS PlanID, T.TaskID AS TaskID, 
                   MIN(TPD.StartDate) AS StartDate, MAX(TPD.EndDate) AS EndDate, 
                   ISNULL(SUM(PeriodCost), 0) AS PeriodCost, 
                   ISNULL(SUM(PeriodBill), 0) AS PeriodBill,

				   /* Compensation breakout */
                   ISNULL(SUM(PeriodExpCost), 0) AS PeriodExpCost, 
                   ISNULL(SUM(PeriodConCost), 0) AS PeriodConCost, 
                   ISNULL(SUM(PeriodExpBill), 0) AS PeriodExpBill,
                   ISNULL(SUM(PeriodConBill), 0) AS PeriodConBill

                   FROM @tabCalendarInterval AS CI LEFT JOIN RPTask AS T ON CI.PlanID = T.PlanID
                   INNER JOIN RPTask AS CT ON T.PlanID = CT.PlanID AND T.OutlineNumber = CT.ParentOutlineNumber
                   INNER JOIN RPReimbAllowance AS TPD ON TPD.PlanID = CT.PlanID AND TPD.TaskID = CT.TaskID 
                     AND TPD.StartDate >= CI.StartDate AND TPD.EndDate <= CI.EndDate
                   WHERE TPD.TimePhaseID IS NOT NULL 
                     AND T.ChildrenCount <> 0  AND T.OutlineLevel = @intOutlineLevel
                   GROUP BY T.Name, CI.StartDate, T.TaskID, T.PlanID) AS X
           GROUP BY PlanID, TaskID, CIStartDate)

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++   
           
      -- Update summary Task rows.
    
      UPDATE RPTask
        SET PlannedLaborHrs = TPD.PlannedLaborHrs,
            PlannedLabCost = TPD.PlannedLabCost,
            PlannedLabBill = TPD.PlannedLabBill,
            LabRevenue = TPD.LabRevenue,
            PlannedExpCost = TPD.PlannedExpCost,
            PlannedExpBill = TPD.PlannedExpBill,
            ExpRevenue = TPD.ExpRevenue,
            PlannedConCost = TPD.PlannedConCost,
            PlannedConBill = TPD.PlannedConBill,
            ConRevenue = TPD.ConRevenue,
            PlannedUntQty = TPD.PlannedUntQty,
            PlannedUntCost = TPD.PlannedUntCost,
            PlannedUntBill = TPD.PlannedUntBill,
            UntRevenue = TPD.UntRevenue,
            CompensationFee = TPD.CompCost,
            CompensationFeeBill = TPD.CompBill,
            ConsultantFee = TPD.ConsCost,
            ConsultantFeeBill = TPD.ConsBill,
            ReimbAllowance = TPD.ReimCost,
            ReimbAllowanceBill = TPD.ReimBill,
            WeightedLabCost = TPD.WeightedLabCost,
            WeightedLabBill = TPD.WeightedLabBill,
            WeightedExpCost = TPD.WeightedExpCost,
            WeightedExpBill = TPD.WeightedExpBill,
            WeightedConCost = TPD.WeightedConCost,
            WeightedConBill = TPD.WeightedConBill,
            WeightedUntCost = TPD.WeightedUntCost,
            WeightedUntBill = TPD.WeightedUntBill,
            PlannedDirExpCost = TPD.PlannedDirExpCost,
            PlannedDirExpBill = TPD.PlannedDirExpBill,
            PlannedDirConCost = TPD.PlannedDirConCost,
            PlannedDirConBill = TPD.PlannedDirConBill,
            PlannedDirUntCost = TPD.PlannedDirUntCost,
            PlannedDirUntBill = TPD.PlannedDirUntBill,
            PctCompleteLabCost = CASE WHEN XP.PctCompleteFormula = 0
                                      THEN CASE WHEN TPD.PlannedLabCost <> 0 
                                                THEN ROUND((100.0 * TPD.WeightedLabCost / TPD.PlannedLabCost), 2)
                                                ELSE 0 END
                                      ELSE T.PctCompleteLabCost END,
            PctCompleteLabBill = CASE WHEN XP.PctCompleteFormula = 0
                                      THEN CASE WHEN TPD.PlannedLabBill <> 0 
                                                THEN ROUND((100.0 * TPD.WeightedLabBill / TPD.PlannedLabBill), 2)
                                                ELSE 0 END
                                      ELSE T.PctCompleteLabBill END,
            PctCompleteExpCost = CASE WHEN XP.PctCompleteFormula = 0
                                      THEN CASE WHEN TPD.PlannedExpCost <> 0 
                                                THEN ROUND((100.0 * TPD.WeightedExpCost / TPD.PlannedExpCost), 2)
                                                ELSE 0 END
                                      ELSE T.PctCompleteExpCost END,
            PctCompleteExpBill = CASE WHEN XP.PctCompleteFormula = 0
                                      THEN CASE WHEN TPD.PlannedExpBill <> 0 
                                                THEN ROUND((100.0 * TPD.WeightedExpBill / TPD.PlannedExpBill), 2)
                                                ELSE 0 END
                                      ELSE T.PctCompleteExpBill END,
            PctCompleteConCost = CASE WHEN XP.PctCompleteFormula = 0
                                      THEN CASE WHEN TPD.PlannedConCost <> 0 
                                                THEN ROUND((100.0 * TPD.WeightedConCost / TPD.PlannedConCost), 2)
                                                ELSE 0 END
                                      ELSE T.PctCompleteConCost END,
            PctCompleteConBill = CASE WHEN XP.PctCompleteFormula = 0
                                      THEN CASE WHEN TPD.PlannedConBill <> 0 
                                                THEN ROUND((100.0 * TPD.WeightedConBill / TPD.PlannedConBill), 2)
                                                ELSE 0 END
                                      ELSE T.PctCompleteConBill END,
            PctCompleteUntCost = CASE WHEN XP.PctCompleteFormula = 0
                                      THEN CASE WHEN TPD.PlannedUntCost <> 0 
                                                THEN ROUND((100.0 * TPD.WeightedUntCost / TPD.PlannedUntCost), 2)
                                                ELSE 0 END
                                      ELSE T.PctCompleteUntCost END,
            PctCompleteUntBill = CASE WHEN XP.PctCompleteFormula = 0
                                      THEN CASE WHEN TPD.PlannedUntBill <> 0 
                                                THEN ROUND((100.0 * TPD.WeightedUntBill / TPD.PlannedUntBill), 2)
                                                ELSE 0 END
                                      ELSE T.PctCompleteUntBill END,

		/* Compensation breakout */
		CompensationFeeDirLab = TPD.DirLabCost,
		CompensationFeeDirExp = TPD.DirExpCost, 
		CompensationFeeDirLabBill = TPD.DirLabBill, 
		CompensationFeeDirExpBill = TPD.DirExpBill,
		ReimbAllowanceExp = TPD.ReimExpCost, 
		ReimbAllowanceCon = TPD.ReimConsCost, 
		ReimbAllowanceExpBill = TPD.ReimExpBill, 
		ReimbAllowanceConBill = TPD.ReimConsBill

        FROM RPTask AS T INNER JOIN
          (SELECT XT.PlanID, XT.TaskID,
                  ROUND(SUM(ISNULL(LabHrs, 0)), @intHrDecimals) AS PlannedLaborHrs,
                  ROUND(SUM(ISNULL(LabCost, 0)), @intAmtCostDecimals) AS PlannedLabCost,
                  ROUND(SUM(ISNULL(LabBill, 0)), @intAmtBillDecimals)AS PlannedLabBill,
                  ROUND(SUM(ISNULL(LabRev, 0)), @intLabRevDecimals) AS LabRevenue,
                  ROUND(SUM(ISNULL(ExpCost, 0)), @intAmtCostDecimals) AS PlannedExpCost,
                  ROUND(SUM(ISNULL(ExpBill, 0)), @intAmtBillDecimals) AS PlannedExpBill,
                  ROUND(SUM(ISNULL(ExpRev, 0)), @intECURevDecimals) AS ExpRevenue,
                  ROUND(SUM(ISNULL(ConCost, 0)), @intAmtCostDecimals) AS PlannedConCost,
                  ROUND(SUM(ISNULL(ConBill, 0)), @intAmtBillDecimals) AS PlannedConBill,
                  ROUND(SUM(ISNULL(ConRev, 0)), @intECURevDecimals) AS ConRevenue,
                  ROUND(SUM(ISNULL(UntQty, 0)), @intQtyDecimals) AS PlannedUntQty,
                  ROUND(SUM(ISNULL(UntCost, 0)), @intAmtCostDecimals) AS PlannedUntCost,
                  ROUND(SUM(ISNULL(UntBill, 0)), @intAmtBillDecimals) AS PlannedUntBill,
                  ROUND(SUM(ISNULL(UntRev, 0)), @intECURevDecimals) AS UntRevenue,
                  ROUND(SUM(ISNULL(CompCost, 0)), @intAmtCostDecimals) AS CompCost,
                  ROUND(SUM(ISNULL(CompBill, 0)), @intAmtBillDecimals) AS CompBill,
                  ROUND(SUM(ISNULL(ConsCost, 0)), @intAmtCostDecimals) AS ConsCost,
                  ROUND(SUM(ISNULL(ConsBill, 0)), @intAmtBillDecimals) AS ConsBill,
                  ROUND(SUM(ISNULL(ReimCost, 0)), @intAmtCostDecimals) AS ReimCost,
                  ROUND(SUM(ISNULL(ReimBill, 0)), @intAmtBillDecimals) AS ReimBill,
                  SUM(ISNULL(LabWtCost, 0)) AS WeightedLabCost,
                  SUM(ISNULL(LabWtBill, 0)) AS WeightedLabBill,
                  SUM(ISNULL(ExpWtCost, 0)) AS WeightedExpCost,
                  SUM(ISNULL(ExpWtBill, 0)) AS WeightedExpBill,
                  SUM(ISNULL(ConWtCost, 0)) AS WeightedConCost,
                  SUM(ISNULL(ConWtBill, 0)) AS WeightedConBill,
                  SUM(ISNULL(UntWtCost, 0)) AS WeightedUntCost,
                  SUM(ISNULL(UntWtBill, 0)) AS WeightedUntBill,
                  ROUND(SUM(ISNULL(ExpDirCost, 0)), @intAmtCostDecimals) AS PlannedDirExpCost,
                  ROUND(SUM(ISNULL(ExpDirBill, 0)), @intAmtBillDecimals) AS PlannedDirExpBill,
                  ROUND(SUM(ISNULL(ConDirCost, 0)), @intAmtCostDecimals) AS PlannedDirConCost,
                  ROUND(SUM(ISNULL(ConDirBill, 0)), @intAmtBillDecimals) AS PlannedDirConBill,
                  ROUND(SUM(ISNULL(UntDirCost, 0)), @intAmtCostDecimals) AS PlannedDirUntCost,
                  ROUND(SUM(ISNULL(UntDirBill, 0)), @intAmtBillDecimals) AS PlannedDirUntBill,

				  /* Compensation breakout */
                  ROUND(SUM(ISNULL(DirLabCost, 0)), @intAmtCostDecimals) AS DirLabCost,
                  ROUND(SUM(ISNULL(DirExpCost, 0)), @intAmtCostDecimals) AS DirExpCost,
                  ROUND(SUM(ISNULL(DirLabBill, 0)), @intAmtBillDecimals) AS DirLabBill,
                  ROUND(SUM(ISNULL(DirExpBill, 0)), @intAmtBillDecimals) AS DirExpBill,
                  ROUND(SUM(ISNULL(ReimExpCost, 0)), @intAmtCostDecimals) AS ReimExpCost,
                  ROUND(SUM(ISNULL(ReimConsCost, 0)), @intAmtCostDecimals) AS ReimConsCost,
                  ROUND(SUM(ISNULL(ReimExpBill, 0)), @intAmtBillDecimals) AS ReimExpBill,
                  ROUND(SUM(ISNULL(ReimConsBill, 0)), @intAmtBillDecimals) AS ReimConsBill

             FROM RPTask AS XT LEFT JOIN
               (SELECT LT.PlanID AS PlanID, LT.TaskID AS TaskID,
                       SUM(ISNULL(XA.PlannedLaborHrs, 0)) AS LabHrs, 
                       SUM(ISNULL(XA.PlannedLabCost, 0)) AS LabCost, 
                       SUM(ISNULL(XA.PlannedLabBill, 0)) AS LabBill, 
                       SUM(ISNULL(XA.LabRevenue, 0)) AS LabRev,
                       0 AS ExpCost, 0 AS ExpBill, 0 AS ExpRev,
                       0 AS ConCost, 0 AS ConBill, 0 AS ConRev,
                       0 AS UntQty, 0 AS UntCost, 0 AS UntBill, 0 AS UntRev,
                       0 AS CompCost, 0 AS CompBill, 0 AS ConsCost, 0 AS ConsBill, 0 AS ReimCost, 0 AS ReimBill,
                       SUM(ISNULL(XA.WeightedLabCost, 0)) AS LabWtCost, 
                       SUM(ISNULL(XA.WeightedLabBill, 0)) AS LabWtBill,
                       0 AS ExpWtCost, 0 AS ExpWtBill, 0 AS ConWtCost, 0 AS ConWtBill, 0 AS UntWtCost, 0 AS UntWtBill,
                       0 AS ExpDirCost, 0 AS ExpDirBill, 0 AS ConDirCost, 0 AS ConDirBill, 0 AS UntDirCost, 0 AS UntDirBill,

					   /* Compensation breakout */
                       0 AS DirLabCost, 0 AS DirExpCost, 0 AS DirLabBill, 0 AS DirExpBill, 0 AS ReimExpCost, 0 AS ReimConsCost, 0 AS ReimExpBill, 0 AS ReimConsBill

                  FROM RPTask AS LT LEFT JOIN RPAssignment AS XA 
                    ON LT.PlanID = XA.PlanID AND LT.TaskID = XA.TaskID
                  WHERE LT.PlanID = @strPlanID AND LT.OutlineLevel = @intOutlineLevel AND LT.ChildrenCount > 0
                  GROUP BY LT.PlanID, LT.TaskID
                UNION ALL
                SELECT ET.PlanID AS PlanID, ET.TaskID AS TaskID,
                       0 AS LabHrs, 0 AS LabCost, 0 AS LabBill, 0 AS LabRev,
                       SUM(ISNULL(XE.PlannedExpCost, 0)) AS ExpCost, 
                       SUM(ISNULL(XE.PlannedExpBill, 0)) AS ExpBill, 
                       SUM(ISNULL(XE.ExpRevenue, 0)) AS ExpRev,
                       0 AS ConCost, 0 AS ConBill, 0 AS ConRev,
                       0 AS UntQty, 0 AS UntCost, 0 AS UntBill, 0 AS UntRev,
                       0 AS CompCost, 0 AS CompBill, 0 AS ConsCost, 0 AS ConsBill, 0 AS ReimCost, 0 AS ReimBill,
                       0 AS LabWtCost, 0 AS LabWtBill,
                       SUM(ISNULL(XE.WeightedExpCost, 0)) AS ExpWtCost, 
                       SUM(ISNULL(XE.WeightedExpBill, 0)) AS ExpWtBill,
                       0 AS ConWtCost, 0 AS ConWtBill,
                       0 AS UntWtCost, 0 AS UntWtBill,
                       SUM(ISNULL(XE.PlannedDirExpCost, 0)) AS ExpDirCost,
                       SUM(ISNULL(XE.PlannedDirExpBill, 0)) AS ExpDirBill,
                       0 AS ConDirCost, 0 AS ConDirBill, 
                       0 AS UntDirCost, 0 AS UntDirBill,

					   /* Compensation breakout */
                       0 AS DirLabCost, 0 AS DirExpCost, 0 AS DirLabBill, 0 AS DirExpBill, 0 AS ReimExpCost, 0 AS ReimConsCost, 0 AS ReimExpBill, 0 AS ReimConsBill

                  FROM RPTask AS ET LEFT JOIN RPExpense AS XE 
                    ON ET.PlanID = XE.PlanID AND ET.TaskID = XE.TaskID
                  WHERE ET.PlanID = @strPlanID AND ET.OutlineLevel = @intOutlineLevel AND ET.ChildrenCount > 0
                  GROUP BY ET.PlanID, ET.TaskID
                UNION ALL
                SELECT CT.PlanID AS PlanID, CT.TaskID AS TaskID,
                       0 AS LabHrs, 0 AS LabCost, 0 AS LabBill, 0 AS LabRev,
                       0 AS ExpCost, 0 AS ExpBill, 0 AS ExpRev,
                       SUM(ISNULL(XC.PlannedConCost, 0)) AS ConCost, 
                       SUM(ISNULL(XC.PlannedConBill, 0)) AS ConBill, 
                       SUM(ISNULL(XC.ConRevenue, 0)) AS ConRev,
                       0 AS UntQty, 0 AS UntCost, 0 AS UntBill, 0 AS UntRev,
                       0 AS CompCost, 0 AS CompBill, 0 AS ConsCost, 0 AS ConsBill, 0 AS ReimCost, 0 AS ReimBill,
                       0 AS LabWtCost, 0 AS LabWtBill, 0 AS ExpWtCost, 0 AS ExpWtBill,
                       SUM(ISNULL(XC.WeightedConCost, 0)) AS ConWtCost, 
                       SUM(ISNULL(XC.WeightedConBill, 0)) AS ConWtBill,
                       0 AS UntWtCost, 0 AS UntWtBill,
                       0 AS ExpDirCost, 0 AS ExpDirBill,
                       SUM(ISNULL(XC.PlannedDirConCost, 0)) AS ConDirCost,
                       SUM(ISNULL(XC.PlannedDirConBill, 0)) AS ConDirBill,
                       0 AS UntDirCost, 0 AS UntDirBill,

					   /* Compensation breakout */
                       0 AS DirLabCost, 0 AS DirExpCost, 0 AS DirLabBill, 0 AS DirExpBill, 0 AS ReimExpCost, 0 AS ReimConsCost, 0 AS ReimExpBill, 0 AS ReimConsBill

                   FROM RPTask AS CT LEFT JOIN RPConsultant AS XC 
                     ON CT.PlanID = XC.PlanID AND CT.TaskID = XC.TaskID
                   WHERE CT.PlanID = @strPlanID AND CT.OutlineLevel = @intOutlineLevel AND CT.ChildrenCount > 0
                   GROUP BY CT.PlanID, CT.TaskID
                UNION ALL
                SELECT CT.PlanID AS PlanID, CT.TaskID AS TaskID,
                       0 AS LabHrs, 0 AS LabCost, 0 AS LabBill, 0 AS LabRev,
                       0 AS ExpCost, 0 AS ExpBill, 0 AS ExpRev,
                       0 AS ConCost, 0 AS ConBill, 0 AS ConRev,
                       SUM(ISNULL(XU.PlannedUntQty, 0)) AS UntQty, 
                       SUM(ISNULL(XU.PlannedUntCost, 0)) AS UntCost, 
                       SUM(ISNULL(XU.PlannedUntBill, 0)) AS UntBill, 
                       SUM(ISNULL(XU.UntRevenue, 0)) AS UntRev,
                       0 AS CompCost, 0 AS CompBill, 0 AS ConsCost, 0 AS ConsBill, 0 AS ReimCost, 0 AS ReimBill,
                       0 AS LabWtCost, 0 AS LabWtBill, 
                       0 AS ExpWtCost, 0 AS ExpWtBill,
                       0 AS ConWtCost, 0 AS ConWtBill,
                       SUM(ISNULL(XU.WeightedUntCost, 0)) AS UntWtCost, 
                       SUM(ISNULL(XU.WeightedUntBill, 0)) AS UntWtBill,
                       0 AS ExpDirCost, 0 AS ExpDirBill,
                       0 AS ConDirCost, 0 AS ConDirBill, 
                       SUM(ISNULL(XU.PlannedDirUntCost, 0)) AS UntDirCost,
                       SUM(ISNULL(XU.PlannedDirUntBill, 0)) AS UntDirBill,

					   /* Compensation breakout */
                       0 AS DirLabCost, 0 AS DirExpCost, 0 AS DirLabBill, 0 AS DirExpBill, 0 AS ReimExpCost, 0 AS ReimConsCost, 0 AS ReimExpBill, 0 AS ReimConsBill

                   FROM RPTask AS CT LEFT JOIN RPUnit AS XU 
                     ON CT.PlanID = XU.PlanID AND CT.TaskID = XU.TaskID
                   WHERE CT.PlanID = @strPlanID AND CT.OutlineLevel = @intOutlineLevel AND CT.ChildrenCount > 0
                   GROUP BY CT.PlanID, CT.TaskID
                UNION ALL
                SELECT PT.PlanID, PT.TaskID,
                       SUM(ISNULL(KT.PlannedLaborHrs, 0)) AS LabHrs, 
                       SUM(ISNULL(KT.PlannedLabCost, 0)) AS LabCost, 
                       SUM(ISNULL(KT.PlannedLabBill, 0)) AS LabBill, 
                       SUM(ISNULL(KT.LabRevenue, 0)) AS LabRev,
                       SUM(ISNULL(KT.PlannedExpCost, 0)) AS ExpCost, 
                       SUM(ISNULL(KT.PlannedExpBill, 0)) AS ExpBill, 
                       SUM(ISNULL(KT.ExpRevenue, 0)) AS ExpRev,
                       SUM(ISNULL(KT.PlannedConCost, 0)) AS ConCost, 
                       SUM(ISNULL(KT.PlannedConBill, 0)) AS ConBill, 
                       SUM(ISNULL(KT.ConRevenue, 0)) AS ConRev,
                       SUM(ISNULL(KT.PlannedUntQty, 0)) AS UntQty, 
                       SUM(ISNULL(KT.PlannedUntCost, 0)) AS UntCost, 
                       SUM(ISNULL(KT.PlannedUntBill, 0)) AS UntBill, 
                       SUM(ISNULL(KT.UntRevenue, 0)) AS UntRev,
                       SUM(ISNULL(KT.CompensationFee, 0)) AS CompCost,
                       SUM(ISNULL(KT.CompensationFeeBill, 0)) AS CompBill,
                       SUM(ISNULL(KT.ConsultantFee, 0)) AS ConsCost,
                       SUM(ISNULL(KT.ConsultantFeeBill, 0)) AS ConsBill,
                       SUM(ISNULL(KT.ReimbAllowance, 0)) AS ReimCost,
                       SUM(ISNULL(KT.ReimbAllowanceBill, 0)) AS ReimBill,
                       SUM(ISNULL(KT.WeightedLabCost, 0)) AS LabWtCost, 
                       SUM(ISNULL(KT.WeightedLabBill, 0)) AS LabWtBill,
                       SUM(ISNULL(KT.WeightedExpCost, 0)) AS ExpWtCost, 
                       SUM(ISNULL(KT.WeightedExpBill, 0)) AS ExpWtBill,
                       SUM(ISNULL(KT.WeightedConCost, 0)) AS ConWtCost, 
                       SUM(ISNULL(KT.WeightedConBill, 0)) AS ConWtBill,
                       SUM(ISNULL(KT.WeightedUntCost, 0)) AS UntWtCost, 
                       SUM(ISNULL(KT.WeightedUntBill, 0)) AS UntWtBill,
                       SUM(ISNULL(KT.PlannedDirExpCost, 0)) AS ExpDirCost,
                       SUM(ISNULL(KT.PlannedDirExpBill, 0)) AS ExpDirBill,
                       SUM(ISNULL(KT.PlannedDirConCost, 0)) AS ConDirCost,
                       SUM(ISNULL(KT.PlannedDirConBill, 0)) AS ConDirBill,
                       SUM(ISNULL(KT.PlannedDirUntCost, 0)) AS UntDirCost,
                       SUM(ISNULL(KT.PlannedDirUntBill, 0)) AS UntDirBill,

					   /* Compensation breakout */
                       SUM(ISNULL(KT.CompensationFeeDirLab, 0)) AS DirLabCost,
                       SUM(ISNULL(KT.CompensationFeeDirExp, 0)) AS DirExpCost,
                       SUM(ISNULL(KT.CompensationFeeDirLabBill, 0)) AS DirLabBill,
                       SUM(ISNULL(KT.CompensationFeeDirExpBill, 0)) AS DirExpBill,
                       SUM(ISNULL(KT.ReimbAllowanceExp, 0)) AS ReimExpCost,
                       SUM(ISNULL(KT.ReimbAllowanceCon, 0)) AS ReimConsCost,
                       SUM(ISNULL(KT.ReimbAllowanceExpBill, 0)) AS ReimExpBill,
                       SUM(ISNULL(KT.ReimbAllowanceConBill, 0)) AS ReimConsBill

                  FROM RPTask AS PT INNER JOIN RPTask AS KT
                    ON PT.PlanID = KT.PlanID AND PT.OutlineNumber = KT.ParentOutlineNumber
                  WHERE PT.PlanID = @strPlanID AND PT.OutlineLevel = @intOutlineLevel AND PT.ChildrenCount > 0
                  GROUP BY PT.PlanID, PT.TaskID) AS XTPD
               ON XT.PlanID = XTPD.PlanID AND XT.TaskID = XTPD.TaskID
               WHERE XT.PlanID = @strPlanID AND XT.ChildrenCount > 0
             GROUP BY XT.PlanID, XT.TaskID) AS TPD 
          ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID
          LEFT JOIN RPPlan AS XP ON T.PlanID = XP.PlanID
        WHERE T.OutlineLevel = @intOutlineLevel AND T.ChildrenCount > 0

      -- Ready to move up one level.

      SET @intOutlineLevel = @intOutlineLevel - 1
    
    END -- While
    
  -->>> We need to update the Plan record with the total weighted average amounts
  -->>> and compute the summary percent complete numbers, provided that the
  -->>> Pct Complete Formula is "User-Entered".
  
  UPDATE RPPlan
    SET WeightedLabCost = ISNULL(T.WeightedLabCost, 0),
        WeightedLabBill = ISNULL(T.WeightedLabBill, 0),
        WeightedExpCost = ISNULL(T.WeightedExpCost, 0),
        WeightedExpBill = ISNULL(T.WeightedExpBill, 0),
        WeightedConCost = ISNULL(T.WeightedConCost, 0),
        WeightedConBill = ISNULL(T.WeightedConBill, 0),
        WeightedUntCost = ISNULL(T.WeightedUntCost, 0),
        WeightedUntBill = ISNULL(T.WeightedUntBill, 0),
        PctCompleteLabCost = CASE WHEN P.PlannedLabCost <> 0 
                                  THEN ROUND((100.0 * T.WeightedLabCost / P.PlannedLabCost), 2)
                                  ELSE 0 END,
        PctCompleteLabBill = CASE WHEN P.PlannedLabBill <> 0 
                                  THEN ROUND((100.0 * T.WeightedLabBill / P.PlannedLabBill), 2)
                                  ELSE 0 END,
        PctCompleteExpCost = CASE WHEN P.PlannedExpCost <> 0 
                                  THEN ROUND((100.0 * T.WeightedExpCost / P.PlannedExpCost), 2)
                                  ELSE 0 END,
        PctCompleteExpBill = CASE WHEN P.PlannedExpBill <> 0 
                                  THEN ROUND((100.0 * T.WeightedExpBill / P.PlannedExpBill), 2)
                                  ELSE 0 END,
        PctCompleteConCost = CASE WHEN P.PlannedConCost <> 0 
                                  THEN ROUND((100.0 * T.WeightedConCost / P.PlannedConCost), 2)
                                  ELSE 0 END,
        PctCompleteConBill = CASE WHEN P.PlannedConBill <> 0 
                                  THEN ROUND((100.0 * T.WeightedConBill / P.PlannedConBill), 2)
                                  ELSE 0 END,
        PctCompleteUntCost = CASE WHEN P.PlannedUntCost <> 0 
                                  THEN ROUND((100.0 * T.WeightedUntCost / P.PlannedUntCost), 2)
                                  ELSE 0 END,
        PctCompleteUntBill = CASE WHEN P.PlannedUntBill <> 0 
                                  THEN ROUND((100.0 * T.WeightedUntBill / P.PlannedUntBill), 2)
                                  ELSE 0 END,
        PctComplete = CASE WHEN (P.PlannedLabCost + P.PlannedExpCost + P.PlannedConCost + P.PlannedUntCost) <> 0 
                           THEN ROUND((100.0 * (T.WeightedLabCost + T.WeightedExpCost + T.WeightedConCost + T.WeightedUntCost) / 
                                       (P.PlannedLabCost + P.PlannedExpCost + P.PlannedConCost + P.PlannedUntCost)), 2)
                           ELSE 0 END,
        PctCompleteBill = CASE WHEN (P.PlannedLabBill + P.PlannedExpBill + P.PlannedConBill + P.PlannedUntBill) <> 0 
                               THEN ROUND((100.0 * (T.WeightedLabBill + T.WeightedExpBill + T.WeightedConBill + T.WeightedUntBill) / 
                                           (P.PlannedLabBill + P.PlannedExpBill + P.PlannedConBill + P.PlannedUntBill)), 2)
                               ELSE 0 END
    FROM RPPlan AS P LEFT JOIN                            
      (SELECT XT.PlanID, 
              SUM(ISNULL(WeightedLabCost, 0)) AS WeightedLabCost,
              SUM(ISNULL(WeightedLabBill, 0)) AS WeightedLabBill,
              SUM(ISNULL(WeightedExpCost, 0)) AS WeightedExpCost,
              SUM(ISNULL(WeightedExpBill, 0)) AS WeightedExpBill,
              SUM(ISNULL(WeightedConCost, 0)) AS WeightedConCost,
              SUM(ISNULL(WeightedConBill, 0)) AS WeightedConBill,
              SUM(ISNULL(WeightedUntCost, 0)) AS WeightedUntCost,
              SUM(ISNULL(WeightedUntBill, 0)) AS WeightedUntBill
         FROM RPTask AS XT
         WHERE XT.PlanID = @strPlanID AND XT.OutlineLevel = 0
         GROUP BY XT.PlanID) AS T
      ON P.PlanID = T.PlanID
    WHERE P.PlanID = @strPlanID AND P.PctCompleteFormula = 0

  SET NOCOUNT OFF

END -- rpSumUpTPD
GO
