SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_PAT_SPRouteHalff] (@PayableSeq int,@Route varchar(50),@NextOrder int,@Option1 varchar(50),@Option2 varchar(50))
AS
BEGIN

	set nocount on

	BEGIN TRY
		--exec spCCG_PAT_SPRouteHalff 74,'Halff',1,' ',' '
		--select * from CCG_PAT_Payable where vendor = '00008338' --3
		--BEGIN TRANSACTION
		--select * from CCG_PAT_Payable where Stage = 'Pending' and PayableType = 'I'
		--select top 20 * from CCG_PAT_Pending where PayableSeq = 5392
		--delete from CCG_PAT_Pending where PayableSeq = 5392 and Seq > 368
		--select * from CCG_PAT_ConfigRoutes
		--select * from CCG_PAT_ConfigRouteDetail
		--select * from CCG_PAT_ProjectAmount where PayableSeq = 74
		--declare @PayableSeq int
		--declare @Route varchar(20)
		--declare @NextOrder int
		--set @Route = 'DRoute'
		--set @NextOrder = 1
		--set @PayableSeq = 	5392	
		declare @MaxPendingSeq int
		select @MaxPendingSeq = MAX(seq) from CCG_PAT_Pending where PayableSeq = @PayableSeq
		declare @threshold decimal(19,5)
		declare @totalAmount decimal(19,5)
		declare @WBS1 varchar(30)
		declare @AccountType varchar(1)
		declare @ChargeType varchar(1)--future
		declare @VEPATApprover1 varchar(30), @VEPATApprover2 varchar(30), @VEPATApprover3 varchar(30), @patApprover varchar(30)
		declare @VEPATThreshold1 decimal(19,5), @VEPATThreshold2 decimal(19,5)
		declare @GLPATApprover1 varchar(30),@GLPATApprover2 varchar(30), @GLPATApprover3 varchar(30)
		declare @GLPATThreshold1 decimal(19,5), @GLPATThreshold2 decimal(19,5)
		declare @PM varchar(30), @PRPATBiller varchar(30), @TL varchar(30)
		declare @bflag varchar(1)
		declare @dt datetime = GETUTCDATE()
	
		declare @process varchar(30)
		declare @Supervisor varchar(30)
		declare @vetype varchar(1)
		declare @end varchar(1)
		set @end = 'N'
		declare @PO varchar(1), @PONumer varchar(20)
		set @PO = 'N'
		select @PO = 'Y', @PONumer = pa.PONumber 
			from CCG_PAT_Payable pa
			where pa.seq = @payableseq and isnull(pa.PONumber,' ') <> ' '

		select @wbs1 = min(pa.wbs1) from CCG_PAT_ProjectAmount pa where pa.PayableSeq = @PayableSeq

---------Get Consultant Type and VEPatApprover and process-------
		select @vetype = category, @VEPATApprover1 = vct.CustPATApprover1, @VEPATApprover2 = vct.CustPATApprover2, @VEPATApprover3 = vct.CustPATApprover3,
		 @VEPATThreshold1 = CustPATThreshold1,@VEPATThreshold2 = CustPATThreshold2, @process = VCT.CustPATRouteType, @totalAmount = ControlAmount
			from ccg_pat_payable pa
		    left join VE on pa.Vendor = ve.Vendor
			left join VendorCustomTabFields vct on vct.Vendor = ve.Vendor
			where pa.seq = @PayableSeq
			--print @vepatapprover1
		 --    PRINT @process

----------GET GL PATApprover -----
		if @process = 'GeneralLedger'
		Begin
			select @GLPATApprover1 = custpatapprover1, @GLPATApprover2 = custpatapprover2,@GLPATApprover3 = custpatapprover3, 
					@GLPATThreshold1 = CustPATThreshold1, @GLPATThreshold2 = CustPATThreshold2
				from CCG_PAT_ProjectAmount pa
				inner join AccountCustomTabFields ACT on ACT.account = pa.GLAccount
				where pa.PayableSeq = @PayableSeq
		end

---------Get PM,  project PATApprover, Charge type-----------
		if RTRIM(ISNULL(@WBS1,' ')) <> ' '-- and @process = 'Manager'
		BEGIN
			select @PM = ProjMgr, @PRPATBiller = pr.Biller, @ChargeType = ChargeType, @TL = pr.Principal
				from CCG_PAT_ProjectAmount pa
					inner join PR on pr.wbs1 = pa.wbs1 and pr.wbs2 = ' '
					inner join ProjectCustomTabFields PCT on PCT.wbs1=PR.WBS1 and PCT.WBS2 = ' '
				where  pa.PayableSeq = @PayableSeq		
		END
		
		select @bflag = CustPATBillerReview
			from EmployeeCustomTabFields ECT where employee = @PRPATBiller
		
		if @chargetype = 'R'
		begin
			if @bflag = 'Y'
				insert into CCG_PAT_Pending (PayableSeq,Route,Description,SortOrder,Employee,CreateDateTime) 
					values ( @PayableSeq, @Route,'Biller',@NextOrder, @PRPATBiller, @dt)

			insert into CCG_PAT_Pending (PayableSeq,Route,Description,SortOrder,Employee,CreateDateTime) 
				select distinct @PayableSeq, @Route, 'Manager', @NextOrder+1, PR.ProjMgr, @dt
					from CCG_PAT_ProjectAmount pa 
						inner join PR on pr.wbs1 = pa.wbs1 and pr.wbs2 = pa.wbs2 and pr.wbs3 = pa.wbs3
					where pa.PayableSeq = @PayableSeq and PR.ProjMgr is not null

			set @end = 'Y'
		end
		else if @ChargeType = 'P'
		begin
			insert into CCG_PAT_Pending (PayableSeq,Route,Description,SortOrder,Employee,CreateDateTime) 
				select distinct @PayableSeq, @Route, 'Manager', @NextOrder, isnull(isnull(PR.ProjMgr, PR.Principal),UDIC_OrganizationStructure.CustOperationsManager), @dt
					from CCG_PAT_ProjectAmount pa 
						inner join PR on pr.wbs1 = pa.wbs1 and pr.wbs2 = pa.wbs2 and pr.wbs3 = pa.wbs3
						left join UDIC_OrganizationStructure on pr.Org = UDIC_OrganizationStructure.CustOrganization
					where pa.PayableSeq = @PayableSeq and (PR.ProjMgr is not null or PR.Principal is not null or UDIC_OrganizationStructure.CustOperationsManager is not null)

			set @end = 'Y'
		end
		
	--	--------Get PAT Approver--------
	--print @vepatapprover1
	--Print @process	
		print @totalamount			
		if @process = 'Vendor' and @end = 'N'
			begin
				insert into CCG_PAT_Pending (PayableSeq,Route,Description,SortOrder,Employee,CreateDateTime) 
					values ( @PayableSeq, @Route,'Vendor',@NextOrder, @VEPATApprover1,GETUTCDATE() )
				
			    if @totalamount >= isnull(@VEPATThreshold1,999999) and @VEPATThreshold1 <> 0
					insert into CCG_PAT_Pending (PayableSeq,Route,Description,SortOrder,Employee,CreateDateTime) 
						values ( @PayableSeq, @Route,'Vendor',@NextOrder, @VEPATApprover2,GETUTCDATE() )
					
				if @totalamount >= isnull(@VEPATThreshold2,999999) and @VEPATThreshold2 <> 0
					insert into CCG_PAT_Pending (PayableSeq,Route,Description,SortOrder,Employee,CreateDateTime) 
						values ( @PayableSeq, @Route,'Vendor',@NextOrder, @VEPATApprover3,GETUTCDATE() )
				set @end = 'Y'
			end
			
		if @process = 'GeneralLedger' and @end = 'N'
			begin
				insert into CCG_PAT_Pending (PayableSeq,Route,Description,SortOrder,Employee,CreateDateTime) 
					select distinct @PayableSeq, @Route, 'GL', @NextOrder, ACT.CustPATApprover1, @dt
						from CCG_PAT_ProjectAmount pa 
							inner join AccountCustomTabFields ACT on ACT.account = pa.GLAccount
						where pa.PayableSeq = @PayableSeq and ACT.CustPATApprover1 is not null	

			    if @totalamount >= isnull(@GLPATThreshold1,999999) and @GLPATThreshold1 <> 0						
					insert into CCG_PAT_Pending (PayableSeq,Route,Description,SortOrder,Employee,CreateDateTime) 
						select distinct @PayableSeq, @Route, 'GL', @NextOrder + 1, ACT.CustPATApprover2, @dt
							from CCG_PAT_ProjectAmount pa 
								inner join AccountCustomTabFields ACT on ACT.account = pa.GLAccount
							where pa.PayableSeq = @PayableSeq and ACT.CustPATApprover2 is not null	
															
				if @totalamount >= isnull(@GLPATThreshold2,999999) and @GLPATThreshold2 <> 0
					insert into CCG_PAT_Pending (PayableSeq,Route,Description,SortOrder,Employee,CreateDateTime) 
						select distinct @PayableSeq, @Route, 'GL', @NextOrder + 2, ACT.CustPATApprover3, @dt
							from CCG_PAT_ProjectAmount pa 
								inner join AccountCustomTabFields ACT on ACT.account = pa.GLAccount
							where pa.PayableSeq = @PayableSeq and ACT.CustPATApprover3 is not null
							
				set @end = 'Y'
			end

		if @process = 'Manager' and @end = 'N'
			begin			  
				if @chargetype <> 'R' and @end = 'N'					
					insert into CCG_PAT_Pending (PayableSeq,Route,Description,SortOrder,Employee,CreateDateTime) 
						select distinct @PayableSeq, @Route, 'Manager', @NextOrder, isnull(isnull(isnull(PR.ProjMgr, PR.Principal), UDIC_OrganizationStructure.CustTeamLeader), UDIC_OrganizationStructure.CustOperationsManager), @dt
							from CCG_PAT_ProjectAmount pa 
								inner join PR on pr.wbs1 = pa.wbs1 and pr.wbs2 = pa.wbs2 and pr.wbs3 = pa.wbs3
								left join UDIC_OrganizationStructure on pr.Org = UDIC_OrganizationStructure.CustOrganization
							where pa.PayableSeq = @PayableSeq and (PR.ProjMgr is not null or PR.Principal is not null or UDIC_OrganizationStructure.CustTeamLeader is not null or UDIC_OrganizationStructure.CustOperationsManager is not null)

				set @end = 'Y'
			end
	
		--return the records added
		select * from CCG_PAT_Pending where PayableSeq = @PayableSeq and (@MaxPendingSeq is null or Seq > @MaxPendingSeq)

	END TRY
	BEGIN CATCH
		
		select -1 as Result, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage

	END CATCH

END
GO
