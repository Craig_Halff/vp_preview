SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpRptAnalysis]
  @dtETCDate datetime,
  @strUnposted varchar(1),
  @strCommitment varchar(1),
  @strExpTab varchar(1),
  @strConTab varchar(1),
  @strUnitTab varchar(1),
  @rpLaborDetail varchar(1),
  @rpExpenseDetail varchar(1),
  @rpConsultantDetail varchar(1),
  @rpUnitDetail varchar(1),
  @ReportAtBillingInBillingCurr varchar(1) = 'N'
AS

BEGIN --procedure rpRptAnalysis start

  declare @strSelect as varchar(300)

  declare @strPlanID varchar(32)
  declare @sintGRMethod smallint

--  set @strSelect = 'DECLARE planInfo  CURSOR FOR Select PlanID, GRMethod from rpPlan LEFT JOIN Organization AS ORG on ORG.Org = rpPlan.Org where 1=1 AND '
--  execute(@strSelect + @strWhere1+ @strWhere2 + @strWhere3 + @strWhere4 + @strWhere5)

  DECLARE planInfo  CURSOR FOR Select PlanID, GRMethod from #rptPlanList

  BEGIN --open planInfo
     OPEN planInfo
     FETCH from planInfo INTO @strPlanID, @sintGRMethod
     WHILE @@FETCH_STATUS = 0 
       begin --while @@FETCH_STATUS = 0 start

       insert #rptTaskLaborJTD
       select PlanID,
	   TaskID,
       MAX(JTDCostCurrencyCode),
       COUNT( distinct JTDCostCurrencyCode),
       MAX(JTDBillCurrencyCode),
       COUNT( distinct JTDBillCurrencyCode),
	   SUM(PeriodHrs) AS JTDHrs,    
	   SUM(PeriodCost) AS JTDCost,
	   SUM(PeriodBill) AS JTDBill
       from dbo.RP$rptTaskJTDLabor (@strPlanID, @dtETCDate, @strUnposted, 'Y',@sintGRMethod, @ReportAtBillingInBillingCurr)  	
       GROUP BY PlanID, TaskID 

       if @rpLaborDetail = 'Y'
         begin
           insert #rptAssignLaborJTD
           select PlanID,
	       TaskID,
	       AssignmentID,
           MAX(JTDCostCurrencyCode),
           COUNT( distinct JTDCostCurrencyCode),
           MAX(JTDBillCurrencyCode),
           COUNT( distinct JTDBillCurrencyCode),
      	   SUM(PeriodHrs) AS JTDHrs,    
	       SUM(PeriodCost) AS JTDCost,
	       SUM(PeriodBill) AS JTDBill
           from dbo.RP$rptAssignJTD(@strPlanID,@dtETCDate,NULL,@strUnposted,@sintGRMethod,'N',@ReportAtBillingInBillingCurr)  	
           GROUP BY PlanID, TaskID, AssignmentID
        end		

       if @strExpTab = 'Y'
         begin
           insert #rptTaskExpJTD
           select PlanID,
	       TaskID,
           MAX(JTDCostCurrencyCode),
           COUNT( distinct JTDCostCurrencyCode),
           MAX(JTDBillCurrencyCode),
           COUNT( distinct JTDBillCurrencyCode),
      	   SUM(PeriodCost) AS JTDCost,
	       SUM(PeriodBill) AS JTDBill
           from dbo.RP$rptTaskJTDExpCon(@strPlanID,@dtETCDate,@strCommitment,@strUnitTab,'Y','E',@ReportAtBillingInBillingCurr)  	
           GROUP BY PlanID, TaskID

           if @rpExpenseDetail = 'Y'
             begin
               insert #rptExpJTD
               select PlanID,
	           TaskID,
	           RowID as ExpenseID, 	
               MAX(JTDCostCurrencyCode),
               COUNT( distinct JTDCostCurrencyCode),
               MAX(JTDBillCurrencyCode),
               COUNT( distinct JTDBillCurrencyCode),
	           SUM(PeriodCost) AS JTDCost,
	           SUM(PeriodBill) AS JTDBill
               from dbo.RP$rptExpConJTD(@strPlanID,@dtETCDate,NULL,@strCommitment,'N',@strUnitTab,'E',@ReportAtBillingInBillingCurr)
               GROUP BY PlanID, TaskID, RowID
             end
       end --if @strExpTab = 'Y'

       if @strConTab = 'Y'
         begin
           insert #rptTaskConJTD
           select PlanID,
	       TaskID,
           MAX(JTDCostCurrencyCode),
           COUNT( distinct JTDCostCurrencyCode),
           MAX(JTDBillCurrencyCode),
           COUNT( distinct JTDBillCurrencyCode),
	       SUM(PeriodCost) AS JTDCost,
	       SUM(PeriodBill) AS JTDBill
           from dbo.RP$rptTaskJTDExpCon(@strPlanID,@dtETCDate,@strCommitment,@strUnitTab,'Y','C',@ReportAtBillingInBillingCurr)  	
           GROUP BY PlanID, TaskID


           if @rpConsultantDetail = 'Y'
             begin
               insert #rptConJTD
               select PlanID,
	   TaskID,
	   RowID as ConsultantID, 	
       MAX(JTDCostCurrencyCode),
       COUNT( distinct JTDCostCurrencyCode),
       MAX(JTDBillCurrencyCode),
       COUNT( distinct JTDBillCurrencyCode),
	   SUM(PeriodCost) AS JTDCost,
	   SUM(PeriodBill) AS JTDBill
               from dbo.RP$rptExpConJTD(@strPlanID,@dtETCDate,NULL,@strCommitment,'N',@strUnitTab,'C',@ReportAtBillingInBillingCurr)
               GROUP BY PlanID, TaskID, RowID
             end
         end --if @strConTab = 'Y'

       if @strUnitTab = 'Y'
         begin
           insert #rptTaskUnitJTD
           select PlanID,
	   TaskID,
       MAX(JTDCostCurrencyCode),
       COUNT( distinct JTDCostCurrencyCode),
       MAX(JTDBillCurrencyCode),
       COUNT( distinct JTDBillCurrencyCode),
	   SUM(PeriodQty) AS JTDQty,
	   SUM(PeriodCost) AS JTDCost,
	   SUM(PeriodBill) AS JTDBill
           from dbo.RP$rptTaskJTDUnit(@strPlanID,'Y',@dtETCDate,@ReportAtBillingInBillingCurr)  	
           GROUP BY PlanID, TaskID 

          if @rpUnitDetail = 'Y'
            begin
              insert #rptUnitJTD
              select PlanID,
	   TaskID,
	   UnitID,
       MAX(JTDCostCurrencyCode),
       COUNT( distinct JTDCostCurrencyCode),
       MAX(JTDBillCurrencyCode),
       COUNT( distinct JTDBillCurrencyCode),
	   SUM(PeriodQty) AS JTDQty,
	   SUM(PeriodCost) AS JTDCost,
	   SUM(PeriodBill) AS JTDBill
              from dbo.RP$rptUnitJTD(@strPlanID,@dtETCDate,NULL,@ReportAtBillingInBillingCurr)
              GROUP BY PlanID, TaskID, UnitID
            end
        end -- if @strUnitTab = 'Y'

       insert #rptPlanRevJTD
       select PlanID,
       MAX(JTDRevenueCurrencyCode),
       COUNT ( distinct JTDRevenueCurrencyCode),
	   SUM(ActualRevenue) as ActualRevenue
       from dbo.RP$rptPlanJTDRevenue(@strPlanID,@dtETCDate)     
	   GROUP BY PlanID	

	   insert #rptTaskETC
	   select PlanID,
	   TaskID,
	   ETCLabHrs,
       ETCLabCost,
       ETCLabBill,
       ETCExpCost,
       ETCExpBill,
       ETCDirExpCost,
       ETCDirExpBill,
       ETCReimExpCost,
       ETCReimExpBill,
       ETCConCost,
       ETCConBill,
       ETCDirConCost,
       ETCDirConBill,
       ETCReimConCost,
       ETCReimConBill,
       ETCUntQty,
       ETCUntCost,
       ETCUntBill   
	   from dbo.RP$rptTaskETC(@strPlanID,dateadd(d,1,@dtETCDate))

       FETCH NEXT FROM planInfo INTO @strPlanID, @sintGRMethod
       end  --while @@FETCH_STATUS = 0 end    
     CLOSE planInfo
     DEALLOCATE planInfo  
  END --open planInfo

END -- procedure rpRptAnalysis end
GO
