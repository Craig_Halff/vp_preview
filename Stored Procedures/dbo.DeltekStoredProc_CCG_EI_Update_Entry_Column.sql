SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Update_Entry_Column]
	@spName			varchar(100),
	@wbs1			Nvarchar(30),
	@wbs2			Nvarchar(7),
	@wbs3			Nvarchar(7)
AS
BEGIN
	/*
	DeltekStoredProc_CCG_EI_Update_Entry_Column
		@spName = 'spCCG_EI_HoldUntilDate',
		@WBS1 = '1999015.00',
		@WBS2 = ' ',
		@WBS3 = ' '
	*/
	SET NOCOUNT ON;
	DECLARE @returnVal		int;
	DECLARE @returnMessage	varchar(max);
	DECLARE @safeSql	bit;

	-- Ensure that our inputs are safe (prevent SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @wbs1);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @wbs2);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @wbs3);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<FIELD>', @spName);
	IF @safeSql = 0 BEGIN
		SELECT 'ERROR: Unsafe parameters used! Execution cancelled.';
		RETURN;
	END;

	-- Verify that the parameters are safe, single 'words' (ALTERNATIVE/PREVIOUS METHOD)
	--SELECT @spName = spname FROM (SELECT @spName As spname) T WHERE spname not like '%[^0-9A-Za-z_]%';
	--SELECT @wbs1 = parm FROM (SELECT @spName As parm) T WHERE parm not like '%[^0-9A-Za-z_.]%';
	--SELECT @wbs2 = parm FROM (SELECT @spName As parm) T WHERE parm not like '%[^0-9A-Za-z_.]%';
	--SELECT @wbs3 = parm FROM (SELECT @spName As parm) T WHERE parm not like '%[^0-9A-Za-z_.]%';

	IF ISNULL(@spName, '') = '' BEGIN
		RETURN;
	END
	ELSE BEGIN
		SET @wbs1 = REPLACE(@wbs1, N'''', N'''''');		-- Help prevent SQL injection
		SET @wbs2 = REPLACE(@wbs2, N'''', N'''''');		-- Help prevent SQL injection
		SET @wbs3 = REPLACE(@wbs3, N'''', N'''''');		-- Help prevent SQL injection

		-- Determine if this SP has a ReturnValue output parameter
		IF EXISTS(
			SELECT 'x'
				FROM [sys].[parameters] p
				JOIN [sys].[objects] o ON o.[object_id] = p.[object_id]
				WHERE o.name = @spName And Lower(p.name) = '@returnvalue'
		) BEGIN
			DECLARE @SQLExec nvarchar(500);
			SET @SQLExec = N'EXEC ' + @spName + ' @pWBS1, @pWBS2, @pWBS3, @pReturnValue OUTPUT, @pReturnMessage OUTPUT';

			--PRINT @sSQL;
			EXECUTE sp_executesql
				@SQLExec,
				N'@pWBS1 varchar(30), @pWBS2 varchar(7), @pWBS3 varchar(7), @pReturnValue int OUTPUT, @pReturnMessage varchar(255) OUTPUT',
				@pWBS1 = @wbs1,
				@pWBS2 = @wbs2,
				@pWBS3 = @wbs3,
				@pReturnValue = @returnVal OUTPUT,
				@pReturnMessage = @returnMessage OUTPUT;

			SELECT @returnVal as ReturnValue, @returnMessage as ReturnMessage;
		END
		ELSE BEGIN
			EXEC (@spName + N' ''' + @wbs1 + N''', ''' + @wbs2 + N''', ''' + @wbs3 + N'''');
		END;
	END;
END;
GO
