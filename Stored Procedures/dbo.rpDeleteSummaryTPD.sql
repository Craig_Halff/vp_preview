SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpDeleteSummaryTPD]
  @strPlanID VARCHAR(32),
  @strCalcLab VARCHAR(1) = 'Y',
  @strCalcExp VARCHAR(1) = 'Y',
  @strCalcCon VARCHAR(1) = 'Y',
  @strCalcUnt VARCHAR(1) = 'Y',
  @strCalcCompF VARCHAR(1) = 'N',
  @strCalcConsF VARCHAR(1) = 'N',
  @strCalcReimA VARCHAR(1) = 'N'
AS

BEGIN -- Procedure rpDeleteSummaryTPD

  SET NOCOUNT ON
  
  -- Delete existing time-phased data for Task rows that have either:
  --   * Other child Task rows underneath.
  --   * Assignment/Expense/Consultant/Unit rows underneath.

  -- Delete Summary Labor time-phased records.
  
  IF (@strCalcLab = 'Y')
    DELETE TPD FROM RPPlannedLabor AS TPD WHERE TPD.AssignmentID IS NULL AND TPD.TaskID IN
      (SELECT PT.TaskID AS TaskID
         FROM RPTask AS PT INNER JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
         WHERE PT.PlanID = @strPlanID
       UNION
       SELECT PT.TaskID AS TaskID 
         FROM RPTask AS PT INNER JOIN RPAssignment AS A ON PT.PlanID = A.PlanID AND PT.TaskID = A.TaskID
         WHERE PT.PlanID = @strPlanID)
      

  -- Delete Summary Expense time-phased records.
  
  IF (@strCalcExp = 'Y')
    DELETE TPD FROM RPPlannedExpenses AS TPD WHERE TPD.ExpenseID IS NULL AND TPD.TaskID IN
      (SELECT PT.TaskID AS TaskID
         FROM RPTask AS PT INNER JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
         WHERE PT.PlanID = @strPlanID
       UNION
       SELECT PT.TaskID AS TaskID 
         FROM RPTask AS PT INNER JOIN RPExpense AS E ON PT.PlanID = E.PlanID AND PT.TaskID = E.TaskID
         WHERE PT.PlanID = @strPlanID)

  -- Delete Summary Consultant time-phased records.
  
  IF (@strCalcCon = 'Y')
    DELETE TPD FROM RPPlannedConsultant AS TPD WHERE TPD.ConsultantID IS NULL AND TPD.TaskID IN
      (SELECT PT.TaskID AS TaskID
         FROM RPTask AS PT INNER JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
         WHERE PT.PlanID = @strPlanID
       UNION
       SELECT PT.TaskID AS TaskID 
         FROM RPTask AS PT INNER JOIN RPConsultant AS C ON PT.PlanID = C.PlanID AND PT.TaskID = C.TaskID
         WHERE PT.PlanID = @strPlanID)

  -- Delete Summary Unit time-phased records.
  
  IF (@strCalcUnt = 'Y')
    DELETE TPD FROM RPPlannedUnit AS TPD WHERE TPD.UnitID IS NULL AND TPD.TaskID IN
      (SELECT PT.TaskID AS TaskID
         FROM RPTask AS PT INNER JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
         WHERE PT.PlanID = @strPlanID
       UNION
       SELECT PT.TaskID AS TaskID 
         FROM RPTask AS PT INNER JOIN RPUnit AS U ON PT.PlanID = U.PlanID AND PT.TaskID = U.TaskID
         WHERE PT.PlanID = @strPlanID)
        
  -- Delete Summary Compensation Fee time-phased records.
      
  IF (@strCalcCompF = 'Y')
    DELETE TPD FROM RPCompensationFee AS TPD WHERE TPD.TaskID IN
      (SELECT PT.TaskID AS TaskID
         FROM RPTask AS PT INNER JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
         WHERE PT.PlanID = @strPlanID)

  -- Delete Summary Consultant Fee time-phased records.
  
  IF (@strCalcConsF = 'Y')
    DELETE TPD FROM RPConsultantFee AS TPD WHERE TPD.TaskID IN
      (SELECT PT.TaskID AS TaskID
         FROM RPTask AS PT INNER JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
         WHERE PT.PlanID = @strPlanID)

  -- Delete Summary Reimbursable Allowance time-phased records.

  IF (@strCalcReimA = 'Y')
    DELETE TPD FROM RPReimbAllowance AS TPD WHERE TPD.TaskID IN
      (SELECT PT.TaskID AS TaskID
         FROM RPTask AS PT INNER JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
         WHERE PT.PlanID = @strPlanID)
        
  -- >>> At this point, the remaining time-phased data belong to either childless-Task or 
  -- >>> Assignment/Expense/Consultant/Unit.
  
  SET NOCOUNT OFF

END -- rpDeleteSummaryTPD
GO
