SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPAddJTDAssignment]
  @strRowID nvarchar(255),
  @bitCalledFromRM bit = 0
AS

BEGIN -- Procedure stRPAddJTDAssignment

  SET NOCOUNT ON

  DECLARE @siLCLevels smallint = 0

  DECLARE @strLCLevel1Enabled nvarchar(14) = 'Y'
  DECLARE @strLCLevel2Enabled nvarchar(14) = 'Y'
  DECLARE @strLCLevel3Enabled nvarchar(14) = 'Y'
  DECLARE @strLCLevel4Enabled nvarchar(14) = 'Y'
  DECLARE @strLCLevel5Enabled nvarchar(14) = 'Y'

  DECLARE @useLaborCode bit = 0

   
  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime

  DECLARE @intOutlineLevel int
   
  DECLARE @strCompany nvarchar(14)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strTopTaskID varchar(32)
  DECLARE @strWBS1 nvarchar(30)
  DECLARE @strUserName nvarchar(42)

  DECLARE @intLBCDInserted int = 0
  DECLARE @intASGNInserted int = 0

  -- Declare Temp tables.
  
  DECLARE @tabWBS TABLE (
    RowID int IDENTITY(1,1),
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    NewTaskID varchar(32) COLLATE database_default,
    Name nvarchar(255) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default,
    WBSType varchar(4) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int,
    ChildrenCount int,
    StartDate datetime,
    EndDate datetime,
    ProjMgr nvarchar(20) COLLATE database_default,
    ChargeType varchar(1) COLLATE database_default,
    ProjectType nvarchar(10) COLLATE database_default,
    ClientID varchar(32) COLLATE database_default,
    Org nvarchar(30) COLLATE database_default,
    Status varchar(1) COLLATE database_default
    UNIQUE (RowID, WBS1, WBS2, WBS3, LaborCode, ParentOutlineNumber, OutlineNumber)
  )

  DECLARE @tabLD TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default,
    Employee nvarchar(20) COLLATE database_default
    UNIQUE (RowID, WBS1, WBS2, WBS3, LaborCode, Employee)
  )
   DECLARE @tabLDTask TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    LaborCode nvarchar(14) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    Employee nvarchar(20) COLLATE database_default
    UNIQUE (RowID, WBS1, WBS2, WBS3, LaborCode, OutlineNumber, Employee)
  )

  DECLARE @tabASGNInserted TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID VARCHAR(32) COLLATE database_default
    UNIQUE (PlanID, TaskID, AssignmentID)
  )

  DECLARE @tabASGNNonLeaf TABLE(
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default
    UNIQUE (PlanID, TaskID, AssignmentID)
  )

  DECLARE @tabAssignmentHasJTD TABLE (
		PlanID varchar(32) COLLATE database_default,
		TaskID varchar(32) COLLATE database_default,
		AssignmentID varchar(32) COLLATE database_default
		UNIQUE(PlanID, TaskID, AssignmentID)
	)

  DECLARE @tabAssignmentHasETC TABLE (
		PlanID varchar(32) COLLATE database_default,
		TaskID varchar(32) COLLATE database_default,
		AssignmentID varchar(32) COLLATE database_default
		UNIQUE(PlanID, TaskID, AssignmentID)
	)
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))
  SELECT @strUserName = CASE WHEN DATALENGTH(@strUserName) = 0 THEN N'stRPAddJTDAssignment' ELSE N'LJTD_' + @strUserName END

  SELECT 
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE())))
    FROM CFGRMSettings

  -- Set Dates

  SET @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. For an Assignment row, @strRowID = 'E~<PNAssignment.ResourceID>|<PNTask.TaskID>
  --   2. For an Assignment row, @strRowID = 'G~<PNAssignment.GenericResourceID>|<PNTask.TaskID>'
  --   3. For a WBS row, @strRowID = '|<PNTask.TaskID>'

  -- Parsing for TaskID.

  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

  -- Setting various Plan parameter.

  SELECT 
    @strCompany = P.Company,
    @strPlanID = PT.PlanID,
    @strWBS1 = PT.WBS1,
    @strTopTaskID = TT.TaskID
    FROM PNTask AS PT 
      LEFT JOIN PNTask AS TT ON PT.PlanID = TT.PlanID AND TT.OutlineLevel = 0
      INNER JOIN PNPlan AS P ON PT.PlanID = P.PlanID
    WHERE PT.TaskID = @strTaskID

  -- If @strRowID is not for the top-most Task row then leave the SP.
  -- We only need to execute this SP for the top-most Task row.
  -- If there is no data in LD table for the given WBS1 then leave the SP.

  IF((@strTaskID <> @strTopTaskID) OR  (
   (NOT EXISTS(SELECT 'X' FROM LD WHERE WBS1 = @strWBS1)) AND (NOT EXISTS(SELECT 'X' FROM tkDetail WHERE WBS1 = @strWBS1)) 
    AND (NOT EXISTS(SELECT 'X' FROM tsDetail WHERE WBS1 = @strWBS1)))) RETURN

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
--  Retrieve Labor code level and labor code setup for the plan
SELECT
    @strLCLevel1Enabled = LCLevel1Enabled,
    @strLCLevel2Enabled = LCLevel2Enabled,
    @strLCLevel3Enabled = LCLevel3Enabled,
    @strLCLevel4Enabled = LCLevel4Enabled,
    @strLCLevel5Enabled = LCLevel5Enabled
    FROM PNPlan WHERE PlanID = @strPlanID

	SELECT
    @siLCLevels = LCLevels
    FROM CFGFormat

	IF @siLCLevels > 0 AND (@strLCLevel1Enabled = 'Y' OR @strLCLevel2Enabled = 'Y' OR @strLCLevel3Enabled = 'Y' OR @strLCLevel4Enabled = 'Y' OR @strLCLevel5Enabled = 'Y' ) set @useLaborCode = 1
  -- Inserting existing rows from PNTask into @tabWBS

  INSERT @tabWBS(
    PlanID,
    TaskID,
    NewTaskID,
    Name,
    WBS1,
    WBS2,
    WBS3,
    LaborCode,
    WBSType,
    ParentOutlineNumber,
    OutlineNumber,
    OutlineLevel,
    ChildrenCount,
    StartDate,
    EndDate,
    ProjMgr,
    ChargeType,
    ProjectType,
    ClientID,
    Org,
    Status
  )
    SELECT
      T.PlanID AS PlanID,
      T.TaskID AS TaskID,
      NULL AS NewTaskID,
      T.Name AS Name,
      T.WBS1 AS WBS1,
      ISNULL(T.WBS2, ' ') AS WBS2,
      ISNULL(T.WBS3, ' ') AS WBS3,
      T.LaborCode AS LaborCode,
      T.WBSType AS WBSType,
      T.ParentOutlineNumber AS ParentOutlineNumber,
      T.OutlineNumber AS OutlineNumber,
      T.OutlineLevel AS OutlineLevel,
      T.ChildrenCount AS ChildrenCount,
      T.StartDate AS StartDate,
      T.EndDate AS EndDate,
      T.ProjMgr AS ProjMgr,
      T.ChargeType AS ChargeType,
      T.ProjectType AS ProjectType,
      T.ClientID AS ClientID,
      T.Org AS Org,
      T.Status AS Status
      FROM PNTask AS T
      WHERE T.PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
  -- Save {WBS1, WBS2, WBS3, LaborCode, Employee} from LD table into @tabLD.
  -- Only select combination of {WBS1, WBS2, WBS3, LaborCode, Employee} with non-zero JTDHrs.

  INSERT @tabLD(
    WBS1,
    WBS2,
    WBS3,
    LaborCode,
    Employee
  ) 
    SELECT DISTINCT
      Y.WBS1 AS WBS1,
      Y.WBS2 AS WBS2,
      Y.WBS3 AS WBS3,
      Y.LaborCode AS LaborCode,
      Y.Employee AS Employee
      FROM ( /* Y */
        SELECT
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.LaborCode AS LaborCode,
          SUM(X.JTDHours) AS JTDHours,
          X.Employee AS Employee
          FROM ( /* X */
            SELECT  
              LD.WBS1,
              LD.WBS2,
              LD.WBS3,
              LD.LaborCode,
              SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDHours,
              LD.Employee
              FROM LD
              WHERE LD.WBS1 = @strWBS1 AND LD.Employee IS NOT NULL AND
                LD.ProjectCost = 'Y' AND LD.TransDate <= @dtJTDDate
              GROUP BY LD.WBS1, LD.WBS2, LD.WBS3, LD.LaborCode, LD.Employee
              HAVING SUM(RegHrs + OvtHrs + SpecialOvtHrs) <> 0
            UNION
            SELECT  
              TD.WBS1,
              TD.WBS2,
              TD.WBS3,
              TD.LaborCode,
              SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDHours,
              TD.Employee
              FROM tkDetail AS TD -- TD table has entries at only the leaf level.
                INNER JOIN EM ON TD.Employee = EM.Employee  
                INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
              WHERE TD.WBS1 = @strWBS1 AND TD.Employee IS NOT NULL AND TD.TransDate <= @dtJTDDate  
              GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.LaborCode, TD.Employee   
              HAVING SUM(RegHrs + OvtHrs + SpecialOvtHrs) <> 0
            UNION
            SELECT  
              TD.WBS1,
              TD.WBS2,
              TD.WBS3,
              TD.LaborCode,
              SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS JTDHours,
              TD.Employee
              FROM tsDetail AS TD INNER JOIN tsControl AS TC ON TD.Batch = TC.Batch AND TC.Posted = 'N'
              WHERE TD.WBS1 = @strWBS1 AND TD.Employee IS NOT NULL AND TD.TransDate <= @dtJTDDate  
              GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.LaborCode, TD.Employee    
              HAVING SUM(RegHrs + OvtHrs + SpecialOvtHrs) <> 0
          ) AS X
          GROUP BY X.WBS1, X.WBS2, X.WBS3, X.LaborCode, X.Employee
          HAVING SUM(X.JTDHours) <> 0
      ) AS Y

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>



  -- Inserting Labor Code rows that are in LD but not yet in PNTask into @tabWBS
  -- Need to ensure that the added Labor Code rows have valid Sub-Code.
  -- dbo.stRP$tabLaborCodeSubcodes was used to weed out invalid Sub-Code and at the same time computed "Description" of Sub-Code.

  IF @useLaborCode = 1
  BEGIN
	  INSERT @tabWBS(
		PlanID,
		NewTaskID,
		Name,
		WBS1,
		WBS2,
		WBS3,
		LaborCode,
		WBSType,
		ParentOutlineNumber,
		OutlineNumber,
		OutlineLevel,
		ChildrenCount,
		StartDate,
		EndDate,
		ProjMgr,
		ChargeType,
		ProjectType,
		ClientID,
		Org,
		Status
	  )
	  SELECT 
	   @strPlanID AS PlanID,
		  REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS NewTaskID,
		  Z.Name AS Name,
		  Z.WBS1 AS WBS1,
		  ISNULL(Z.WBS2, ' ') AS WBS2,
		  ISNULL(Z.WBS3, ' ') AS WBS3,
		  Z.LaborCode AS LaborCode,
		  'LBCD' AS WBSType,
		  Z.ParentOutlineNumber AS ParentOutlineNumber,
		  1, --will readjust the outline number later
		  Z.OutlineLevel AS OutlineLevel,
		  0 AS ChildrenCount,
		  Z.StartDate AS StartDate,
		  Z.EndDate AS EndDate,
		  Z.ProjMgr AS ProjMgr,
		  Z.ChargeType AS ChargeType,
		  Z.ProjectType AS ProjectType,
		  Z.ClientID AS ClientID,
		  Z.Org AS Org,
		  Z.Status AS Status
	  FROM (/* Z */
	   SELECT DISTINCT 
		  @strPlanID AS PlanID,
		  --REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS NewTaskID,
		  Y.Name AS Name,
		  Y.WBS1 AS WBS1,
		  ISNULL(Y.WBS2, ' ') AS WBS2,
		  ISNULL(Y.WBS3, ' ') AS WBS3,
		  Y.LaborCode AS LaborCode,
		  'LBCD' AS WBSType,
		  Y.ParentOutlineNumber AS ParentOutlineNumber,
		  --Y.OutlineNumber AS OutlineNumber,
		  Y.OutlineLevel AS OutlineLevel,
		  0 AS ChildrenCount,
		  Y.StartDate AS StartDate,
		  Y.EndDate AS EndDate,
		  Y.ProjMgr AS ProjMgr,
		  Y.ChargeType AS ChargeType,
		  Y.ProjectType AS ProjectType,
		  Y.ClientID AS ClientID,
		  Y.Org AS Org,
		  Y.Status AS Status
      
		  FROM  
		   ( /* Y */ 
			SELECT
			  XL.LaborCodeName AS Name,
			  X.WBS1 AS WBS1,
			  ISNULL(X.WBS2, ' ') AS WBS2,
			  ISNULL(X.WBS3, ' ') AS WBS3,
			  XL.LaborCode AS LaborCode,
			  X.OutlineNumber AS ParentOutlineNumber,
			  X.OutlineNumber + '.Z' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
			  X.OutlineLevel + 1 AS OutlineLevel,
			  X.StartDate AS StartDate,
			  X.EndDate AS EndDate,
			  X.ProjMgr AS ProjMgr,
			  X.ChargeType AS ChargeType,
			  X.ProjectType AS ProjectType,
			  X.ClientID AS ClientID,
			  X.Org AS Org,
			  X.Status AS Status
			  FROM ( /* X */
				SELECT  
				  LD.WBS1 AS WBS1,
				  CASE WHEN LD.WBS2 = ' ' Then NULL ELSE LD.WBS2 END AS WBS2,
				  CASE WHEN LD.WBS3 = ' ' Then NULL ELSE LD.WBS3 END AS WBS3,
				  LD.LaborCode AS LaborCode,
				  PT.OutlineNumber AS OutlineNumber,
				  PT.OutlineLevel AS OutlineLevel,
				  PT.StartDate AS StartDate,
				  PT.EndDate AS EndDate,
				  PT.ProjMgr AS ProjMgr,
				  PT.ChargeType AS ChargeType,
				  PT.ProjectType AS ProjectType,
				  PT.ClientID AS ClientID,
				  PT.Org AS Org,
				  PT.Status AS Status,
				  ROW_NUMBER() OVER (PARTITION BY LD.WBS1, LD.WBS2, LD.WBS3 ORDER BY LD.WBS1, LD.WBS2, LD.WBS3, LD.LaborCode) AS RowID
				  FROM ( /* LD */
					SELECT DISTINCT
					  WBS1 AS WBS1,
					  WBS2 AS WBS2,
					  WBS3 AS WBS3,
					  LaborCode AS LaborCode
					  FROM @tabLD
				  ) AS LD
					INNER JOIN @tabWBS AS PT ON LD.WBS1 = PT.WBS1 AND LD.WBS2 = ISNULL(PT.WBS2, ' ') AND LD.WBS3 = ISNULL(PT.WBS3, ' ') 
					LEFT JOIN  @tabWBS AS CT ON LD.WBS1 = CT.WBS1 AND LD.WBS2 = ISNULL(CT.WBS2, ' ') AND LD.WBS3 = ISNULL(CT.WBS3, ' ') 
					  AND ISNULL(LD.LaborCode, '%') LIKE CT.LaborCode AND CT.ParentOutlineNumber = PT.OutlineNumber
				  WHERE CT.TaskID IS NULL 
					AND PT.WBSType <> 'LBCD'
					AND EXISTS(SELECT 'X' FROM PNTask AS LaborCodeTask WHERE LaborCodeTask.PlanID = @strPlanID 
							   AND LaborCodeTask.WBS1 = LD.WBS1 
							   AND ISNULL(LaborCodeTask.WBS2, ' ') = LD.WBS2 
							   AND ISNULL(LaborCodeTask.WBS3, ' ') = LD.WBS3 
							   AND LaborCodeTask.WBSType = 'LBCD'  )			 
							    ) AS X
			  OUTER APPLY dbo.stRP$tabLaborCode(@strPlanID, X.LaborCode) AS XL
			  WHERE XL.LaborCode IS NOT NULL ) as Y ) AS Z
	END -- IF @useLaborCode = 1
  -- Remember whether any new Labor Code row was inserted into @tabWBS.

  SET @intLBCDInserted = @@ROWCOUNT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabLDTask(
    WBS1,
    WBS2,
    WBS3,
    LaborCode,
    OutlineNumber,
    Employee
  )  
    SELECT DISTINCT
      ZLD.WBS1 AS WBS1,
      ZLD.WBS2 AS WBS2,
      ZLD.WBS3 AS WBS3,
      W.LaborCode AS LaborCode, /* Need to use a Wildcard Labor Code here to consolidate Employee */
      W.OutlineNumber AS OutlineNumber,
      ZLD.Employee AS Employee
      FROM  @tabLD AS ZLD
        INNER JOIN @tabWBS AS W 
          ON ZLD.WBS1 = W.WBS1 AND ZLD.WBS2 = W.WBS2 AND ZLD.WBS3 = W.WBS3
            AND ISNULL(ZLD.LaborCode, '%') LIKE ISNULL(W.LaborCode, '%')  

			   
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION 

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Find and insert Un-Planned Labor Code (with JTD) into PNTask RPTask tables.

    --IF (@intLBCDInserted > 0)
    --  BEGIN

        INSERT PNTask(
          PlanID,
          TaskID,
          Name,
          WBS1,
          WBS2,
          WBS3,
          LaborCode,
          WBSType,
          ParentOutlineNumber,
          OutlineNumber,
          OutlineLevel,
          ChildrenCount,
          StartDate,
          EndDate,
          ProjMgr,
          ChargeType,
          ProjectType,
          ClientID,
          Org,
          Status,
          CreateUser,
          ModUser,
          CreateDate,
          ModDate
        )
          SELECT
            T.PlanID AS PlanID,
            T.NewTaskID AS TaskID,
            T.Name AS Name,
            T.WBS1 AS WBS1,
            CASE WHEN T.WBS2 = ' ' Then NULL ELSE T.WBS2 END AS WBS2,
            CASE WHEN T.WBS3 = ' ' Then NULL ELSE T.WBS3 END AS WBS3,
            T.LaborCode AS LaborCode,
            T.WBSType AS WBSType,
            T.ParentOutlineNumber AS ParentOutlineNumber,
            T.OutlineNumber AS OutlineNumber,
            T.OutlineLevel AS OutlineLevel,
            T.ChildrenCount AS ChildrenCount,
            T.StartDate AS StartDate,
            T.EndDate AS EndDate,
            T.ProjMgr AS ProjMgr,
            T.ChargeType AS ChargeType,
            T.ProjectType AS ProjectType,
            T.ClientID AS ClientID,
            T.Org AS Org,
            T.Status AS Status,
            @strUserName AS CreateUser,
            @strUserName AS ModUser,
            GETUTCDATE() AS CreateDate,
            GETUTCDATE() AS ModDate
            FROM @tabWBS AS T
            WHERE T.NewTaskID IS NOT NULL

        INSERT RPTask(
          PlanID,
          TaskID,
          Name,
          WBS1,
          WBS2,
          WBS3,
          LaborCode,
          WBSType,
          ParentOutlineNumber,
          OutlineNumber,
          OutlineLevel,
          ChildrenCount,
          StartDate,
          EndDate,
          ProjMgr,
          ChargeType,
          ProjectType,
          ClientID,
          Org,
          Status,
          CreateUser,
          ModUser,
          CreateDate,
          ModDate
        )
          SELECT
            T.PlanID AS PlanID,
            T.NewTaskID AS TaskID,
            T.Name AS Name,
            T.WBS1 AS WBS1,
            CASE WHEN T.WBS2 = ' ' Then NULL ELSE T.WBS2 END AS WBS2,
            CASE WHEN T.WBS3 = ' ' Then NULL ELSE T.WBS3 END AS WBS3,
            T.LaborCode AS LaborCode,
            T.WBSType AS WBSType,
            T.ParentOutlineNumber AS ParentOutlineNumber,
            T.OutlineNumber AS OutlineNumber,
            T.OutlineLevel AS OutlineLevel,
            T.ChildrenCount AS ChildrenCount,
            T.StartDate AS StartDate,
            T.EndDate AS EndDate,
            T.ProjMgr AS ProjMgr,
            T.ChargeType AS ChargeType,
            T.ProjectType AS ProjectType,
            T.ClientID AS ClientID,
            T.Org AS Org,
            T.Status AS Status,
            @strUserName AS CreateUser,
            @strUserName AS ModUser,
            GETUTCDATE() AS CreateDate,
            GETUTCDATE() AS ModDate
            FROM @tabWBS AS T
            WHERE T.NewTaskID IS NOT NULL

      --END /* IF (@intLBCDInserted > 0) */

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Must reorder OutlineNumber and update ChildrenCount at this point
    -- so that OutlineNumber and ChildrenCount can be used further downstream.

    IF (@intLBCDInserted > 0)
      BEGIN

        -- Reorder OutlineNumber.

        EXECUTE dbo.stRPReorderOutlineNumber @strPlanID, 1 
 
        -- Update ChildrenCount for the whole plan, if needed.

        UPDATE T SET
          ChildrenCount = (SELECT COUNT(*) FROM PNTask WHERE PlanID = @strPlanID AND ParentOutlineNumber = T.OutlineNUmber)
          FROM PNTask AS T 
          WHERE T.PlanID = @strPlanID 
            AND T.ChildrenCount <> (SELECT COUNT(*) FROM PNTask WHERE PlanID = @strPlanID AND ParentOutlineNumber = T.OutlineNUmber)

        UPDATE T SET
          ChildrenCount = (SELECT COUNT(*) FROM RPTask WHERE PlanID = @strPlanID AND ParentOutlineNumber = T.OutlineNUmber)
          FROM RPTask AS T 
          WHERE T.PlanID = @strPlanID 
            AND T.ChildrenCount <> (SELECT COUNT(*) FROM RPTask WHERE PlanID = @strPlanID AND ParentOutlineNumber = T.OutlineNUmber)
	  
      END /* END IF (@intASGNInserted > 0 OR @intLBCDInserted > 0) */

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Find and move non-leaf Assignments down to the first leaf PNTask/RPTask.

    -- Collect Assignment rows that are at non-leaf level so that they can be moved later.

    INSERT @tabASGNNonLeaf(
          PlanID,
          TaskID,
          AssignmentID
        )
          SELECT
            A.PlanID AS PlanID,
            A.TaskID AS TaskID,
            A.AssignmentID AS AssignmentID
          FROM PNAssignment AS A
            INNER JOIN PNTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID AND AT.ChildrenCount > 0
            WHERE A.PlanID = @strPlanID

	--Collect The Assignment that Has JTD at the non-leaf level
    INSERT @tabAssignmentHasJTD(
		  PlanID,
		  TaskID,
		  AssignmentID
    )      
	SELECT DISTINCT ALLAssignments.PlanID AS PlanID,
	ALLAssignments.TaskID AS TaskID,
	ALLAssignments.AssignmentID AS AssignmentID FROM
	  (SELECT A.PlanID AS PlanID, A.TaskID AS TaskID, A.WBS1 AS WBS1,A.WBS2 AS WBS2,A.WBS3 WBS3,AssignmentID, ResourceID FROM PNAssignment A
		 INNER JOIN PNTask T ON T.PlanID = A.PlanID AND  T.TaskID = A.TaskID
	     Where A.PlanID = @strPlanID and ChildrenCount > 0) AS ALLAssignments
	INNER JOIN @tabLDTask JTD ON ALLAssignmentS.WBS1 = JTD.WBS1 AND
	ISNULL(ALLAssignments.WBS2, ' ') =  JTD.WBS2 
	AND  ISNULL(ALLAssignments.WBS3, ' ') =  JTD.WBS3 
	AND ALLAssignmentS.ResourceID = JTD.Employee 

	--Collect The Assignment that Has ETC Hours at the non-leaf level
	INSERT @tabAssignmentHasETC(
      PlanID,
      TaskID,
      AssignmentID
    )      
	  SELECT DISTINCT ALLAssignments.PlanID AS PlanID,
	  ALLAssignments.TaskID AS TaskID,
	  ALLAssignments.AssignmentID AS AssignmentID FROM
		  (SELECT A.PlanID AS PlanID, A.TaskID AS TaskID, A.WBS1 AS WBS1,A.WBS2 AS WBS2,A.WBS3 WBS3,AssignmentID, ResourceID FROM PNAssignment A
		 INNER JOIN PNTask T ON T.PlanID = A.PlanID AND  T.TaskID = A.TaskID
	     Where A.PlanID = @strPlanID and ChildrenCount > 0) AS ALLAssignments	  
	  INNER JOIN (  
		(SELECT PlanID, TaskID, AssignmentID,
	    ROUND(ISNULL(
        CASE 
          WHEN TPD.StartDate >= @dtETCDate 
          THEN TPD.PeriodHrs
          ELSE TPD.PeriodHrs * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany) 
        END 
        , 0), 2) AS PeriodHrs FROM PNPlannedLabor TPD  WHERE PlanID = @strPlanID )) AS PlannedAssginments 
	  ON PlannedAssginments.PlanID = ALLAssignments.PlanID 
	  AND PlannedAssginments.TaskID = ALLAssignments.TaskID 
	  AND PlannedAssginments.AssignmentID = ALLAssignments.AssignmentID 
	  WHERE PeriodHrs > 0
	  
	    
	  -- Now we will move all non leaf assignments but not those JTD without ETC hours to the first child labor code level
        IF EXISTS(SELECT 1 FROM @tabASGNNonLeaf)
          BEGIN

            UPDATE A SET
              TaskID = XT.CT_TaskID,
              WBS1 = XT.CT_WBS1,
              WBS2 = XT.CT_WBS2,
              WBS3 = XT.CT_WBS3,
              LaborCode = XT.CT_LaborCode,
              ModUser = @strUserName,
              ModDate = GETUTCDATE()
              FROM PNAssignment AS A
                INNER JOIN @tabASGNNonLeaf AS LA ON A.PlanID = LA.PlanID AND A.TaskID = LA.TaskID AND A.AssignmentID = LA.AssignmentID
                INNER JOIN ( /* XT */
                  SELECT 
                    PT.PlanID AS PlanID,
                    PT.TaskID AS PT_TaskID,
                    CT.TaskID AS CT_TaskID,
                    CT.WBS1 AS CT_WBS1,
                    CT.WBS2 AS CT_WBS2,
                    CT.WBS3 AS CT_WBS3,
                    CT.LaborCode AS CT_LaborCode,
                    ROW_NUMBER() OVER(PARTITION BY PT.PlanID, PT.TaskID, PT.OutlineNumber ORDER BY CT.OutlineNumber) AS RowNum
                    FROM PNTask AS PT 
                      LEFT JOIN PNTask AS CT 
                        ON PT.PlanID = CT.PlanID AND CT.OutlineNumber LIKE PT.OutlineNumber + '%' 
                          AND CT.ChildrenCount = 0 AND PT.TaskID <> CT.TaskID 
                    WHERE CT.PlanID IS NOT NULL AND PT.PlanID = @strPlanID AND CT.WBSType = 'LBCD'
                ) XT ON LA.PlanID = XT.PlanID AND LA.TaskID = XT.PT_TaskID WHERE XT.RowNum =  1 
				AND  A.AssignmentID NOT IN (
					SELECT AssignmentID  FROM @tabAssignmentHasJTD JTD 
					WHERE AssignmentID NOT IN 
				   (SELECT AssignmentID FROM @tabAssignmentHasETC))
			
			--RPAssignment	    
			UPDATE A SET
              TaskID = XT.CT_TaskID,
              WBS1 = XT.CT_WBS1,
              WBS2 = XT.CT_WBS2,
              WBS3 = XT.CT_WBS3,
              LaborCode = XT.CT_LaborCode,
              ModUser = @strUserName,
              ModDate = GETUTCDATE()
              FROM RPAssignment AS A
                INNER JOIN @tabASGNNonLeaf AS LA ON A.PlanID = LA.PlanID AND A.TaskID = LA.TaskID AND A.AssignmentID = LA.AssignmentID
                INNER JOIN ( /* XT */
                  SELECT 
                    PT.PlanID AS PlanID,
                    PT.TaskID AS PT_TaskID,
                    CT.TaskID AS CT_TaskID,
                    CT.WBS1 AS CT_WBS1,
                    CT.WBS2 AS CT_WBS2,
                    CT.WBS3 AS CT_WBS3,
                    CT.LaborCode AS CT_LaborCode,
                    ROW_NUMBER() OVER(PARTITION BY PT.PlanID, PT.TaskID, PT.OutlineNumber ORDER BY CT.OutlineNumber) AS RowNum
                    FROM RPTask AS PT 
                      LEFT JOIN RPTask AS CT 
                        ON PT.PlanID = CT.PlanID AND CT.OutlineNumber LIKE PT.OutlineNumber + '%' 
                          AND CT.ChildrenCount = 0 AND PT.TaskID <> CT.TaskID 
                    WHERE CT.PlanID IS NOT NULL AND PT.PlanID = @strPlanID AND CT.WBSType = 'LBCD'
                ) XT ON LA.PlanID = XT.PlanID AND LA.TaskID = XT.PT_TaskID WHERE XT.RowNum =  1 
				AND  A.AssignmentID NOT IN (
					SELECT AssignmentID  FROM @tabAssignmentHasJTD JTD 
					WHERE AssignmentID NOT IN 
				   (SELECT AssignmentID FROM @tabAssignmentHasETC))

			--Need to move those assigments with JTD but no ETC to laborcode level
			UPDATE A SET
					TaskID = T.TaskID,
					LaborCode = T.LaborCode,
					ModUser = @strUserName,
					ModDate = GETUTCDATE()    			 
			FROM PNAssignment AS A INNER JOIN
			  (
				SELECT DISTINCT
				WBS1 AS WBS1,
				WBS2 AS WBS2,
				WBS3 AS WBS3,
				LaborCode AS LaborCode,
				OutlineNumber AS OutlineNumber,
				Employee AS Employee
				FROM @tabLDTask AS XLD
			) AS LD
				  ON LD.WBS1 = A.WBS1 AND LD.WBS2 = ISNULL(A.WBS2, ' ') AND LD.WBS3 = ISNULL(A.WBS3, ' ') 
				AND LD.Employee = A.ResourceID           
				AND A.ResourceID IS NOT NULL AND A.PlanID = @strPlanID 
				INNER JOIN PNTask AS T ON LD.WBS1 = T.WBS1 AND LD.WBS2 = ISNULL(T.WBS2, ' ') 
				AND LD.WBS3 = ISNULL(T.WBS3, ' ')  AND LD.LaborCode =  T.LaborCode
			WHERE A.AssignmentID IN (
			SELECT AssignmentID  FROM @tabAssignmentHasJTD JTD 
						WHERE AssignmentID NOT IN 
						(SELECT AssignmentID FROM @tabAssignmentHasETC))

			--For RPAssignment
			UPDATE A SET
					TaskID = T.TaskID,
					LaborCode = T.LaborCode,
					ModUser = @strUserName,
					ModDate = GETUTCDATE()              
			FROM (
				SELECT DISTINCT
				WBS1 AS WBS1,
				WBS2 AS WBS2,
				WBS3 AS WBS3,
				LaborCode AS LaborCode,
				OutlineNumber AS OutlineNumber,
				Employee AS Employee
				FROM @tabLDTask AS XLD
			) AS LD
				INNER JOIN RPAssignment AS A ON LD.WBS1 = A.WBS1 AND LD.WBS2 = ISNULL(A.WBS2, ' ') AND LD.WBS3 = ISNULL(A.WBS3, ' ') 
				AND LD.Employee = A.ResourceID           
				AND A.ResourceID IS NOT NULL AND A.PlanID = @strPlanID 
				INNER JOIN RPTask AS T ON LD.WBS1 = T.WBS1 AND LD.WBS2 = ISNULL(T.WBS2, ' ') 
				AND LD.WBS3 = ISNULL(T.WBS3, ' ')  AND LD.LaborCode =  T.LaborCode
			WHERE A.AssignmentID IN (
			SELECT AssignmentID  FROM @tabAssignmentHasJTD JTD 
						WHERE AssignmentID NOT IN 
						(SELECT AssignmentID FROM @tabAssignmentHasETC))				 			
          END /* END IF EXISTS(SELECT 1 FROM @tabASGNNonLeaf) */

      
  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Find and insert Un-Planned resources (with JTD) into PNAssignment table.
	 		 
    INSERT PNAssignment(
      PlanID,
      TaskID,
      AssignmentID,
      WBS1,
      WBS2,
      WBS3,
      LaborCode,
      ResourceID,
      GenericResourceID,
      StartDate,
      EndDate,
      CreateUser,
      ModUser,
      CreateDate,
      ModDate
    )
      OUTPUT
        INSERTED.PlanID,
        INSERTED.TaskID,
        INSERTED.AssignmentID
        INTO @tabASGNInserted(
          PlanID,
          TaskID,
          AssignmentID
        )
      SELECT  
        @strPlanID AS PlanID,
        ISNULL(T.TaskID, T.NewTaskID) AS TaskID,
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS AssignmentID,
        LD.WBS1 AS WBS1,
        CASE WHEN LD.WBS2 = ' ' Then NULL ELSE LD.WBS2 END AS WBS2,
        CASE WHEN LD.WBS3 = ' ' Then NULL ELSE LD.WBS3 END AS WBS3,
        LD.LaborCode AS LaborCode,
        LD.Employee AS ResourceID,
        NULL AS GenericResourceID,
        T.StartDate AS StartDate,
        T.EndDate AS EndDate,
        @strUserName AS CreateUser,
        @strUserName AS ModUser,
        GETUTCDATE() AS CreateDate,
        GETUTCDATE() AS ModDate
        FROM (
          SELECT DISTINCT
            WBS1 AS WBS1,
            WBS2 AS WBS2,
            WBS3 AS WBS3,
            LaborCode AS LaborCode,
            OutlineNumber AS OutlineNumber,
            Employee AS Employee
            FROM @tabLDTask AS XLD
        ) AS LD
          LEFT JOIN PNAssignment AS A ON LD.WBS1 = A.WBS1 AND LD.WBS2 = ISNULL(A.WBS2, ' ') AND LD.WBS3 = ISNULL(A.WBS3, ' ') 
            AND ISNULL(LD.LaborCode, '%') LIKE ISNULL(A.LaborCode,'%')
            AND LD.Employee = A.ResourceID 
            AND A.ResourceID IS NOT NULL AND A.PlanID = @strPlanID 
          INNER JOIN @tabWBS AS T ON LD.WBS1 = T.WBS1 AND LD.WBS2 = T.WBS2 AND LD.WBS3 = T.WBS3 
		AND (1 = (CASE WHEN @useLaborCode = 0 AND ISNULL(LD.LaborCode, '%') like  ISNULL(T.LaborCode, '%') THEN 1 
				   WHEN @useLaborCode = 1 AND (
											   (LD.LaborCode = T.LaborCode) 
												OR NOT EXISTS(SELECT 'X' FROM PNTask AS LaborCodeTask WHERE LaborCodeTask.PlanID = @strPlanID 
												AND ISNULL(LaborCodeTask.WBS1, ' ') = LD.WBS1 
												AND ISNULL(LaborCodeTask.WBS2, ' ') = LD.WBS2 
												AND ISNULL(LaborCodeTask.WBS3, ' ') = LD.WBS3
												AND LaborCodeTask.WBSType = 'LBCD'  )
												 ) THEN 1 ELSE 0 END ))
        WHERE A.AssignmentID IS NULL  
 
    SET @intASGNInserted = @@ROWCOUNT	

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    IF (@intASGNInserted > 0)
      BEGIN
        -- Find and insert Un-Planned resources (with JTD) into RPAssignment table.
        INSERT RPAssignment(
          PlanID,
          TaskID,
          AssignmentID,
          WBS1,
          WBS2,
          WBS3,
          LaborCode,
          ResourceID,
          GenericResourceID,
          StartDate,
          EndDate,
          CreateUser,
          ModUser,
          CreateDate,
          ModDate
        )
          SELECT  
            PA.PlanID AS PlanID,
            PA.TaskID AS TaskID,
            PA.AssignmentID AS AssignmentID,
            PA.WBS1 AS WBS1,
            PA.WBS2 AS WBS2,
            PA.WBS3 AS WBS3,
            PA.LaborCode AS LaborCode,
            PA.ResourceID AS ResourceID,
            PA.GenericResourceID AS GenericResourceID,
            PA.StartDate AS StartDate,
            PA.EndDate AS EndDate,
            @strUserName AS CreateUser,
            @strUserName AS ModUser,
            GETUTCDATE() AS CreateDate,
            GETUTCDATE() AS ModDate
            FROM PNAssignment AS PA
              INNER JOIN @tabASGNInserted AS I
                ON PA.PlanID = I.PlanID AND PA.TaskID = I.TaskID AND PA.AssignmentID = I.AssignmentID

      END /* END IF (@intASGNInserted > 0) */

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    IF (@intASGNInserted > 0 OR @intLBCDInserted > 0)
      BEGIN

        -- Update VesionID.

        EXECUTE dbo.stRPUpdateVersionID @strPlanID, @bitCalledFromRM,0 

        -- Set PNPlan.LastPlanAction = 'SAVED'

        UPDATE PNPlan SET LastPlanAction = 'SAVED' WHERE PlanID = @strPlanID

      END /* END IF (@intASGNInserted > 0 OR @intLBCDInserted > 0) */
         
  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  COMMIT    
 
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF

END -- stRPAddJTDAssignment
GO
