SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_Cust_Load_Support_Docs_For_Wbs]
	@wbs1		Nvarchar(30)
AS
BEGIN
	-- EXEC [dbo].[spCCG_EI_Cust_Load_Support_Docs_For_Wbs] '2003005.00'
	SET NOCOUNT ON;

	IF EXISTS(
		SELECT * FROM sys.objects b, sys.columns a
		WHERE b.object_id = OBJECT_ID(N'[dbo].[ProjectCustomTabFields]') AND type in (N'U')
			AND a.object_id = b.object_id AND LOWER(a.name) = 'custinvoicesuppdocs')
	BEGIN
		EXEC (N'SELECT custInvoiceSuppDocs, custInvoiceFolder FROM ProjectCustomTabFields WHERE wbs1 = ''' + @wbs1 + N''' and wbs2 = '' '';');
	END
	ELSE SELECT '' as custInvoiceSuppDocs, '' as custInvoiceFolder;
END;
GO
