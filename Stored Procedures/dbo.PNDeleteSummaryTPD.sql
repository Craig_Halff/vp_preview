SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PNDeleteSummaryTPD]
  @strPlanID VARCHAR(32),
  @strCalcLab VARCHAR(1) = 'Y',
  @strCalcExp VARCHAR(1) = 'Y',
  @strCalcCon VARCHAR(1) = 'Y'
AS

BEGIN -- Procedure PNDeleteSummaryTPD

  SET NOCOUNT ON
  
  -- Delete existing time-phased data for Task rows.
  -- In Navigator, Time-Phased data cannot be set by user for a Task row.
  -- All Time-Phased data for Task rows are summed up from Assignment rows.

  -- Delete Summary Labor time-phased records.
  
  IF (@strCalcLab = 'Y')
    DELETE TPD FROM PNPlannedLabor AS TPD WHERE TPD.PlanID = @strPlanID AND TPD.AssignmentID IS NULL

  -- Delete Summary Expense time-phased records.
  
  IF (@strCalcExp = 'Y')
    DELETE TPD FROM PNPlannedExpenses AS TPD WHERE TPD.PlanID = @strPlanID AND TPD.ExpenseID IS NULL

  -- Delete Summary Consultant time-phased records.
  
  IF (@strCalcCon = 'Y')
    DELETE TPD FROM PNPlannedConsultant AS TPD WHERE TPD.PlanID = @strPlanID AND TPD.ConsultantID IS NULL
        
  -- >>> At this point, the remaining time-phased data belong to either childless-Task or 
  -- >>> Assignment/Expense/Consultant/Unit.
  
  SET NOCOUNT OFF

END -- PNDeleteSummaryTPD
GO
