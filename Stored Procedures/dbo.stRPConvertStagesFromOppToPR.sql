SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPConvertStagesFromOppToPR]
AS

BEGIN -- Procedure stRPConvertStagesFromOppToPR

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- The purpose of this stored procedure is to move Stage Configuration from Opportunity to Project.
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

  DECLARE @siNext_D smallint
  DECLARE @siNext_I smallint
  DECLARE @siNext_L smallint
  DECLARE @siNext_W smallint

  DECLARE @tabStagesLookup TABLE (
    RowID int IDENTITY(1,1),
    Description nvarchar(50) COLLATE database_default,
    StepDesc nvarchar(50) COLLATE database_default,
    StepCode varchar(1) COLLATE database_default
    UNIQUE (RowID, Description, StepDesc, StepCode)
  )

  DECLARE @tabOppStagesCodeDesc TABLE (
    Code nvarchar(10) COLLATE database_default,
    Description nvarchar(50) COLLATE database_default,
    Closed varchar(1) COLLATE database_default
    UNIQUE (Code, Description, Closed)
  )

  DECLARE @tabOppStages TABLE (
    Code nvarchar(10) COLLATE database_default,
    Description nvarchar(50) COLLATE database_default,
    Step varchar(1) COLLATE database_default
    UNIQUE (Code, Description, Step)
  )

  DECLARE @tabProjStages TABLE (
    Code nvarchar(10) COLLATE database_default,
    Description nvarchar(50) COLLATE database_default,
    Step varchar(1) COLLATE database_default
    UNIQUE (Code, Description, Step)
  )

  DECLARE @tabNewProjStages TABLE (
    RowID int IDENTITY(1,1),
    Code nvarchar(10) COLLATE database_default,
    Description nvarchar(50) COLLATE database_default,
    Step varchar(1) COLLATE database_default
    UNIQUE (RowID, Code, Description, Step)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Set up Lookup table to determine "StepCode" given "Description"

  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Canceled', 'Do Not Pursue', 'D')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Cancelled', 'Do Not Pursue', 'D')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Closed', 'Do Not Pursue', 'D')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Do Not Pursue', 'Do Not Pursue', 'D')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('No Go', 'Do Not Pursue', 'D')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('No Winner', 'Do Not Pursue', 'D')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Not Funded', 'Do Not Pursue', 'D')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Not Qualified', 'Do Not Pursue', 'D')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Unqualified', 'Do Not Pursue', 'D')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Awaiting Funding', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Awaiting Go/No Go', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Awaiting RFP Release', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Awaiting Signature', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Contract in Progress', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Go', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Identified', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('In Pursuit', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Interview', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Lead', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Negotiation ', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Pending', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Pre-qualified', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Presentation', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Proposal', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Qualified', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Research', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('RFP', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('RFQ', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Shortlist', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('SOQ', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Verbal Confirmation', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Did Not Win', 'Lost', 'L')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Loss', 'Lost', 'L')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Lost', 'Lost', 'L')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Not Awarded', 'Lost', 'L')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Not Won', 'Lost', 'L')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Shortlisted, Lost', 'Lost', 'L')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Awarded', 'Won', 'W')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Shortlisted, Awarded', 'Won', 'W')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Shortlisted, Won', 'Won', 'W')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Win', 'Won', 'W')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Won', 'Won', 'W')

  -- 2018-04-25 - Added the following stages to the associated spreadsheet: DNS, Did Not Submit, Dormant, Hold, Issued, Not Selected, Prospect, Selected, and Withdrawn.

  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('DNS', 'Do Not Pursue', 'D')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Did Not Submit',  'Do Not Pursue', 'D')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Dormant', 'Do Not Pursue', 'D')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Withdrawn', 'Do Not Pursue', 'D')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Hold', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Issued', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Prospect', 'In Pursuit', 'I')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Not Selected', 'Lost', 'L')
  INSERT @tabStagesLookup(Description, StepDesc, StepCode) VALUES('Selected', 'Won', 'W')

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get Opportunity Stages Code and Description. Look for UICultureName = 'en-US' first to get Descriptions.
  -- IF 'en-US' is not enabled then check 'en-GB' and if neither is enabled use the first enabled language from FW_CFGLabelData

  DECLARE @UICultureName varchar(10)
  SET @UICultureName = null
  IF EXISTS (SELECT 'X' FROM FW_CFGLabelData WHERE UICultureName = 'en-US') SET @UICultureName = 'en-US'
  IF @UICultureName IS NULL AND EXISTS (SELECT 'X' FROM FW_CFGLabelData WHERE UICultureName = 'en-GB') SET @UICultureName = 'en-GB' 
  IF @UICultureName IS NULL SET @UICultureName = (SELECT MIN(UICultureName) FROM FW_CFGLabelData)

  INSERT @tabOppStagesCodeDesc(
    Code,
    Description,
    Closed
  )
    SELECT DISTINCT
      OSD.Code AS Code,
      OSD.Description AS Description,
      OSC.Closed AS Closed
      FROM CFGOpportunityStageDescriptions AS OSD
        INNER JOIN CFGOpportunityStageData AS OSC ON OSD.Code = OSC.Code
      WHERE OSD.UICultureName = @UICultureName
 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine appropriate Steps for Opportunity Stages.
  -- Need to control the priority of matching to Stages Lookup.

  INSERT @tabOppStages(
    Code,
    Description,
    Step
  )
    SELECT DISTINCT
      OSCD.Code AS Code,
      OSCD.Description AS Description,
      SL.StepCode AS Step
      FROM @tabOppStagesCodeDesc AS OSCD
        /* Find match on Description in the Lookup table where SL.StepDesc = 'Won' */
        INNER JOIN @tabStagesLookup AS SL 
          ON SL.StepDesc = 'Won' AND
            OSCD.Description NOT LIKE '%NO%' AND
            /* Need to get the LIKE matching on whole words, and not on part of a word */
            '~' + OSCD.Description + '~' LIKE '%[^a-z]' + SL.Description + '[^a-z]%'

  INSERT @tabOppStages(
    Code,
    Description,
    Step
  )
    SELECT DISTINCT
      OSCD.Code AS Code,
      OSCD.Description AS Description,
      MAX(SL.StepCode) AS Step
      FROM @tabOppStagesCodeDesc AS OSCD
        LEFT JOIN @tabOppStages AS OS ON OSCD.Code = OS.Code AND OSCD.Description = OS.Description
        /* Find match on Description in the Lookup table where SL.StepDesc = 'In Pursuit' */
        INNER JOIN @tabStagesLookup AS SL 
          ON SL.StepDesc = 'In Pursuit' AND
            OSCD.Description NOT LIKE '%NO%' AND
            /* Need to get the LIKE matching on whole words, and not on part of a word */
            '~' + OSCD.Description + '~' LIKE '%[^a-z]' + SL.Description + '[^a-z]%'
      WHERE OS.Step IS NULL
      GROUP BY OSCD.Code, OSCD.Description

  INSERT @tabOppStages(
    Code,
    Description,
    Step
  )
    SELECT DISTINCT
      OSCD.Code AS Code,
      OSCD.Description AS Description,
      MAX(SL.StepCode) AS Step
      FROM @tabOppStagesCodeDesc AS OSCD
        LEFT JOIN @tabOppStages AS OS ON OSCD.Code = OS.Code AND OSCD.Description = OS.Description
        /* Find match on Description in the Lookup table where SL.StepDesc = 'Lost' */
        INNER JOIN @tabStagesLookup AS SL 
          ON SL.StepDesc = 'Lost' AND
            /* Need to get the LIKE matching on whole words, and not on part of a word */
            '~' + OSCD.Description + '~' LIKE '%[^a-z]' + SL.Description + '[^a-z]%'
      WHERE OS.Step IS NULL
      GROUP BY OSCD.Code, OSCD.Description

  INSERT @tabOppStages(
    Code,
    Description,
    Step
  )
    SELECT DISTINCT
      OSCD.Code AS Code,
      OSCD.Description AS Description,
      MAX(SL.StepCode) AS Step
      FROM @tabOppStagesCodeDesc AS OSCD
        LEFT JOIN @tabOppStages AS OS ON OSCD.Code = OS.Code AND OSCD.Description = OS.Description
        /* Find match on Description in the Lookup table where SL.StepDesc = 'Do Not Pursue' */
        INNER JOIN @tabStagesLookup AS SL 
          ON SL.StepDesc = 'Do Not Pursue' AND
            /* Need to get the LIKE matching on whole words, and not on part of a word */
            '~' + OSCD.Description + '~' LIKE '%[^a-z]' + SL.Description + '[^a-z]%'
      WHERE OS.Step IS NULL
      GROUP BY OSCD.Code, OSCD.Description

  INSERT @tabOppStages(
    Code,
    Description,
    Step
  )
    SELECT DISTINCT
      OSCD.Code AS Code,
      OSCD.Description AS Description,
      MAX(
        CASE
          WHEN SL.StepCode IS NULL
          THEN
            CASE
              WHEN OSCD.Closed = 'Y' THEN 'D'
              ELSE 'I'
            END
          ELSE 
            CASE
              WHEN OSCD.Description LIKE '%NO%' THEN 'D'
              ELSE SL.StepCode
            END
        END
      ) AS Step
      FROM @tabOppStagesCodeDesc AS OSCD
        LEFT JOIN @tabOppStages AS OS ON OSCD.Code = OS.Code AND OSCD.Description = OS.Description
        /* Find reasonble match on Description in the Lookup table (from the opposite end of the LIKE operation). */
        LEFT JOIN @tabStagesLookup AS SL 
          ON SL.Description <> OSCD.Description AND 
            /* Need to get the LIKE matching on whole words, and not on part of a word */
            '~' + OSCD.Description + '~' LIKE '%[^a-z]' + SL.Description + '[^a-z]%'
      WHERE OS.Step IS NULL
      GROUP BY OSCD.Code, OSCD.Description

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get Project Stages. 

  INSERT @tabProjStages(
    Code,
    Description,
    Step
  )
    SELECT DISTINCT
      PSD.Code,
      PSD.Description,
      PSC.Step
      FROM CFGProjectStageDescriptions AS PSD
        INNER JOIN CFGProjectStageData AS PSC ON PSD.Code = PSC.Code
      WHERE PSD.UICultureName = @UICultureName

  -- Determine the next sequence number for each kinds of step currently in CFGProjectStageData

  SELECT @siNext_D = COUNT(*) + 1 FROM CFGProjectStageData WHERE Step = 'D'
  SELECT @siNext_I = COUNT(*) + 1 FROM CFGProjectStageData WHERE Step = 'I'
  SELECT @siNext_L = COUNT(*) + 1 FROM CFGProjectStageData WHERE Step = 'L'
  SELECT @siNext_W = COUNT(*) + 1 FROM CFGProjectStageData WHERE Step = 'W'

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save those Codes/Descriptions from Opportunity Stage table that have no corresponding entry in Project Stage table yet.

  INSERT @tabNewProjStages(
    Code,
    Description,
    Step
  )
    SELECT DISTINCT
      OS.Code AS Code,
      OS.Description AS Description,
      OS.Step AS Step
      FROM @tabOppStages AS OS
        LEFT JOIN @tabProjStages AS PS ON PS.Code = OS.Code
      WHERE PS.Code IS NULL

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    INSERT CFGProjectStageData(
      Code,
      Step
    )
      SELECT
        Code,
        Step
        FROM @tabNewProjStages
        
    INSERT CFGProjectStageDescriptions(
      Code,
      UICultureName,
      Description,
      Seq
    )
      SELECT
        Code AS Code,
        @UICultureName AS UICultureName,
        Description AS Description,
        RowID AS Seq
        FROM @tabNewProjStages

    -- If there are rows for other languages, insert them as well
    INSERT CFGProjectStageDescriptions(Code, UICultureName, Description, Seq)
    SELECT Code, UICultureName, Description, Seq FROM CFGOpportunityStageDescriptions
     WHERE Code IN (SELECT Code FROM @tabNewProjStages) AND UICultureName NOT IN (SELECT UICultureName FROM CFGProjectStageDescriptions)

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- If after everything has been inserted and there is still no Stage where Step = 'W'
    -- then add a row for the required "Won" stage.

    IF(NOT EXISTS(SELECT 'X' FROM CFGProjectStageData WHERE Step = 'W'))
      BEGIN

        DECLARE @strStageCodeDefault AS nvarchar(10) = N'~WDEF~'
        DECLARE @strStageDescDefault AS nvarchar(50) = N'Won'
        DECLARE @strStageDescDefaultDE AS nvarchar(50) = N'Gewonnen'
        DECLARE @strStageDescDefaultES AS nvarchar(50) = N'Ganado'
        DECLARE @strStageDescDefaultFR AS nvarchar(50) = N'Gagné'
        DECLARE @strStageDescDefaultNL AS nvarchar(50) = N'Gewonnen'
        DECLARE @strStageDescDefaultBR AS nvarchar(50) = N'Ganho'

        DECLARE @strStageStepDefault AS varchar(1) = 'W'

        INSERT CFGProjectStageData(Code,Step)
        SELECT @strStageCodeDefault AS Code,@strStageStepDefault AS Step

        IF EXISTS (SELECT 'X' FROM FW_CFGLabelData WHERE UICultureName = 'en-US')
          INSERT CFGProjectStageDescriptions(Code,UICultureName,Description,Seq) 
          SELECT @strStageCodeDefault AS Code,'en-US' AS UICultureName,@strStageDescDefault AS Description,0 AS Seq

        IF EXISTS (SELECT 'X' FROM FW_CFGLabelData WHERE UICultureName = 'en-GB')
          INSERT CFGProjectStageDescriptions(Code,UICultureName,Description,Seq) 
          SELECT @strStageCodeDefault AS Code,'en-GB' AS UICultureName,@strStageDescDefault AS Description,0 AS Seq

        IF EXISTS (SELECT 'X' FROM FW_CFGLabelData WHERE UICultureName = 'de-DE')
          INSERT CFGProjectStageDescriptions(Code,UICultureName,Description,Seq) 
          SELECT @strStageCodeDefault AS Code,'de-DE' AS UICultureName,@strStageDescDefaultDE AS Description,0 AS Seq

        IF EXISTS (SELECT 'X' FROM FW_CFGLabelData WHERE UICultureName = 'es-ES')
          INSERT CFGProjectStageDescriptions(Code,UICultureName,Description,Seq) 
          SELECT @strStageCodeDefault AS Code,'es-ES' AS UICultureName,@strStageDescDefaultES AS Description,0 AS Seq

        IF EXISTS (SELECT 'X' FROM FW_CFGLabelData WHERE UICultureName = 'fr-CA')
          INSERT CFGProjectStageDescriptions(Code,UICultureName,Description,Seq) 
          SELECT @strStageCodeDefault AS Code,'fr-CA' AS UICultureName,@strStageDescDefaultFR AS Description,0 AS Seq

        IF EXISTS (SELECT 'X' FROM FW_CFGLabelData WHERE UICultureName = 'fr-FR')
          INSERT CFGProjectStageDescriptions(Code,UICultureName,Description,Seq) 
          SELECT @strStageCodeDefault AS Code,'fr-FR' AS UICultureName,@strStageDescDefaultFR AS Description,0 AS Seq

        IF EXISTS (SELECT 'X' FROM FW_CFGLabelData WHERE UICultureName = 'nl-NL')
          INSERT CFGProjectStageDescriptions(Code,UICultureName,Description,Seq) 
          SELECT @strStageCodeDefault AS Code,'nl-NL' AS UICultureName,@strStageDescDefaultNL AS Description,0 AS Seq

        IF EXISTS (SELECT 'X' FROM FW_CFGLabelData WHERE UICultureName = 'pt-BR')
          INSERT CFGProjectStageDescriptions(Code,UICultureName,Description,Seq) 
          SELECT @strStageCodeDefault AS Code,'pt-BR' AS UICultureName,@strStageDescDefaultBR AS Description,0 AS Seq

      END /* END IF(NOT EXISTS(SELECT * FROM CFGProjectStageData WHERE Step = 'W')) */

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  COMMIT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF

END -- stRPConvertStagesFromOppToPR
GO
