SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_DelegationDel] ( @Id varchar(32), @Username nvarchar(32))
             AS EXEC spCCG_PAT_DelegationDel @Id,@Username
GO
