SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FormEdit_LoadBankCode]
	@mc					bit,
	@def				Nvarchar(max),
	@company			Nvarchar(max)
AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_FormEdit_LoadBankCode] 1, '', ''
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max);

	SET @sql = N'
		SELECT code, Description' +
				(CASE WHEN @mc = 1 THEN '+ '' ('' + Org + '')''' ELSE '' END) + ',
				case when code = N''' + ISNULL(@def,'') + ''' then code else N'''' end
            FROM CFGBanks ' +
                (CASE WHEN @mc = 1 THEN '
			WHERE Company = N''' + ISNULL(@company,'') + ''' ' ELSE '' END) + '
            ORDER BY 2';

	EXEC (@sql);
END;

GO
