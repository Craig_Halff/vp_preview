SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FormRouting_LoadGrid] ( @ROLE_ACCT nvarchar(200), @seq nvarchar(20), @history bit, @showAll bit, @POSeq nvarchar(20), @VISION_LANGUAGE nvarchar(10)= 'en-US')
             AS EXEC spCCG_PAT_FormRouting_LoadGrid @ROLE_ACCT,@seq,@history,@showAll,@POSeq,@VISION_LANGUAGE
GO
