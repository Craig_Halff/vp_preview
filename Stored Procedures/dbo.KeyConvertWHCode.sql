SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyConvertWHCode]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg varchar(1000) output,
@UserName nvarchar(32) = 'VisionKeyConvert' -- Add by DP 4.0
,@Period integer = 0,
@EntityType Nvarchar(20)
as
begin -- Procedure
	set nocount on
	declare @OldValue varchar(20)
	declare @NewValue varchar(20)
	Declare @Existing integer
	declare @Entity varchar(20)
	declare @length integer
	declare @CvtType integer
	declare @KeysFetch integer
	declare @RetVal integer
	declare @diag smallint
	declare @Errmsg2 varchar(1000),
			@custlabel	varchar(40),
			@Custlabelplural varchar(40)
	set @diag = 0
	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'WHCode'
	set @length = 10
	set @Existing = 0
	set @KeysFetch = 0

	execute InsertKeyCvtDriver @Entity -- populate driver table

-- Add by DP 4.0
	delete from keyconvertDriver where entity = N'WHCode' and TableName = N'cfgPYWHCodes' and ColumnName = N'Code'
	DECLARE @OldName varchar(40)
	DECLARE @NewName varchar(40)
	DECLARE @PKey varchar(50)
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0
--
	select @custlabel = 'Withholding Code',
			 @Custlabelplural = 'Withholding Codes'
	declare KeysCursor cursor for
		select OldKey, NewKey, Pkey from KeyConvertWork where Entity = @EntityType
	open KeysCursor
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue, 
		@PKey

	set @KeysFetch = @@Fetch_Status

While (@KeysFetch = 0)
	begin
		begin tran
-- Add by DP 4.0
		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany varchar(14)
			DECLARE @RecordCount integer

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				set @RecordCount = (select count(*) from KeyConvertWork where Entity = @EntityType)

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),'KC','Y',  @custlabel + ' Key Convert ('+rtrim(convert(varchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, @custlabel + ' Key Convert', @custlabel);
			end

			Set @PostControlFlag = 1
		end

		set @Existing = 0
		select @Existing =  1, @NewName = Description from CFGPYWHCodes where Code = @NewValue
		select @OldName = Description from CFGPYWHCodes where Code = @OldValue
--

	If (@Existing = 1)
		begin
		  if (@DeleteExisting = 0)
			begin
		   	RAISERROR ('%s Number: %s is an existing %s.  Conversion will not continue.  To combine existing %s select the combine existing option.',
		      16, 3,  @custlabel,@NewValue,@custlabel,@custLabelPlural)
				close keysCursor
				deallocate keysCursor
				if (@@Trancount > 0)
				   ROLLBACK TRANSACTION
				return(50002) -- user defined error
			end
			if (@NewValue = @OldValue)
				begin
			   	RAISERROR ('%s are the same number.  Conversion will not continue.',
			      16, 3,  @custlabelPlural)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end
			if (exists (select 'x' from cfgPyWHCodes new ,cfgpyWHcodes old where new.Code = @newValue and old.Code = @OldValue and new.Type <> old.Type))
				begin
			   	RAISERROR ('%s are not of the same type.  Conversion will not continue.',
			      16, 3,  @custlabelPlural)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error					
				end
				declare @MulticompanyEnabled varchar(1)
				declare @MulticurrencyEnabled varchar(1)
				declare @NewFunctionalCurrencyCode varchar(3)
				declare @OldFunctionalCurrencyCode varchar(3)
				declare @oldCompay varchar(14)
				declare @newCompay varchar(14)
				select @MulticompanyEnabled = MulticompanyEnabled,
			   		 @MulticurrencyEnabled = MulticurrencyEnabled from FW_CFGSystem
				
				if (@MulticompanyEnabled = 'Y')
					begin
						select @oldCompay = old.Company,
								 @newCompay = new.Company from cfgPyWHCodes old,cfgPYWHCodes new, cfgformat 
								where old.Code = @OldValue and new.Code = @NewValue
						if (@oldCompay <> @newCompay)
							begin
						   	RAISERROR ('With multi-company enabled you cannot merge %s of different companies. Old %s: %s company: %s New %s: %s company: %s.  The conversion will not continue.',
						      16, 3,  @custlabelPlural, @custlabel, @OldValue, @oldCompay,@custlabel, @NewValue, @newCompay)
								close keysCursor
								deallocate keysCursor
								if (@@Trancount > 0)
								   ROLLBACK TRANSACTION
								return(50003) -- user defined error
							end
					end

/* Removed by DP 4.0
			  exec @RetVal = KeyCvt @Entity = @Entity,
											@CvtType = @CvtType,
											@Length = @Length,
											@ErrMsg = @ErrMsg2 output,
											@Diag = @Diag,
											@ConstrFlag = 3 -- UpdateTables
*/
		Delete From EMPayrollWithholding Where EMPayrollWithholding.Code = @OldValue 
			And Exists (Select 'x' From EMPayrollWithholding As Tmp Where Tmp.Code = @NewValue 
			And Tmp.Employee = EMPayrollWithholding.Employee and EMPayrollWithholding.EmployeeCompany = Tmp.EmployeeCompany)

		Update EMPayrollWithholdingDetail 
			Set EMPayrollWithholdingDetail.GrossPayBasis = EMPayrollWithholdingDetail.GrossPayBasis + Tmp.GrossPayBasis, 
			EMPayrollWithholdingDetail.Amount = EMPayrollWithholdingDetail.Amount + Tmp.Amount,
			EMPayrollWithholdingDetail.TaxablePayBasis = EMPayrollWithholdingDetail.TaxablePayBasis + tmp.TaxablePayBasis,
			EMPayrollWithholdingDetail.AdjustedGrossPayBasis = EMPayrollWithholdingDetail.AdjustedGrossPayBasis + tmp.AdjustedGrossPayBasis,
			EMPayrollWithholdingDetail.Amt401K = EMPayrollWithholdingDetail.Amt401K + tmp.Amt401K,
			EMPayrollWithholdingDetail.OtherPay1 = EMPayrollWithholdingDetail.OtherPay1 + tmp.OtherPay1,
			EMPayrollWithholdingDetail.OtherPay2 = EMPayrollWithholdingDetail.OtherPay2 + tmp.OtherPay2,
			EMPayrollWithholdingDetail.OtherPay3 = EMPayrollWithholdingDetail.OtherPay3 + tmp.OtherPay3,
			EMPayrollWithholdingDetail.OtherPay4 = EMPayrollWithholdingDetail.OtherPay4 + tmp.OtherPay4,
			EMPayrollWithholdingDetail.OtherPay5 = EMPayrollWithholdingDetail.OtherPay5 + tmp.OtherPay5,
			EMPayrollWithholdingDetail.Amt125 = EMPayrollWithholdingDetail.Amt125 + tmp.Amt125
			from EMPayrollWithholdingDetail Inner Join EMPayrollWithholdingDetail As Tmp On 
			EMPayrollWithholdingDetail.Employee = Tmp.Employee 
			And EMPayrollWithholdingDetail.Period = Tmp.Period 
			And EMPayrollWithholdingDetail.PostSeq = Tmp.PostSeq 
			Where EMPayrollWithholdingDetail.Code = @NewValue 
			And Tmp.Code = @OldValue
		if (@@RowCount > 0)
			Delete FROM EMPayrollWithholdingDetail Where EMPayrollWithholdingDetail.code = @OldValue 
				And Exists (Select 'x' from EMPayrollWithholdingDetail AS Tmp Where Tmp.code = @NewValue 
				And Tmp.Employee = EMPayrollWithholdingDetail.Employee And Tmp.Period = EMPayrollWithholdingDetail.Period 
				And Tmp.PostSeq = EMPayrollWithholdingDetail.PostSeq)
/* 4.1 */
	        Update EMPayrollWithholdingDetailWage 
			Set EMPayrollWithholdingDetailWage.Amount = EMPayrollWithholdingDetailWage.Amount + Tmp.Amount
			from EMPayrollWithholdingDetailWage Inner Join EMPayrollWithholdingDetailWage As Tmp On 
			EMPayrollWithholdingDetailWage.Employee = Tmp.Employee 
			And EMPayrollWithholdingDetailWage.Period = Tmp.Period 
			And EMPayrollWithholdingDetailWage.PostSeq = Tmp.PostSeq 
			And EMPayrollWithholdingDetailWage.CodeWage = Tmp.CodeWage 
			Where EMPayrollWithholdingDetailWage.Code = @NewValue 
			And Tmp.Code = @OldValue

		if (@@RowCount > 0)
			Delete FROM EMPayrollWithholdingDetailWage Where EMPayrollWithholdingDetailWage.code = @OldValue 
				And Exists (Select 'x' from EMPayrollWithholdingDetailWage AS Tmp Where Tmp.code = @NewValue 
				And Tmp.Employee = EMPayrollWithholdingDetailWage.Employee 
				And Tmp.Period = EMPayrollWithholdingDetailWage.Period 
				And Tmp.CodeWage = EMPayrollWithholdingDetailWage.CodeWage 
				And Tmp.PostSeq = EMPayrollWithholdingDetailWage.PostSeq)

		Delete From CFGPYWHCodesWage Where CFGPYWHCodesWage.Code = @OldValue 
			And Exists (Select 'x' From CFGPYWHCodesWage As Tmp Where Tmp.Code = @NewValue 
			And Tmp.Company = CFGPYWHCodesWage.Company
			And (Tmp.CodeWage = CFGPYWHCodesWage.CodeWage or (Tmp.CodeWage = @OldValue and CFGPYWHCodesWage.CodeWage = @NewValue)))

		Delete From EMPayrollWithholdingWage Where EMPayrollWithholdingWage.Code = @OldValue 
			And Exists (Select 'x' From EMPayrollWithholdingWage As Tmp Where Tmp.Code = @NewValue 
			And Tmp.Employee = EMPayrollWithholdingWage.Employee and EMPayrollWithholdingWage.EmployeeCompany = Tmp.EmployeeCompany
			And (Tmp.CodeWage = EMPayrollWithholdingWage.CodeWage or (Tmp.CodeWage = @OldValue and EMPayrollWithholdingWage.CodeWage = @NewValue)))

/* end 4.1 */


            Delete FROM PYChecksW Where PYChecksW.code = @OldValue
                   And Exists (Select 'x' from PYChecksW AS Tmp Where Tmp.code = @NewValue 
                   And Tmp.Employee = PYChecksW.Employee 
						 And Tmp.Period = PYChecksW.Period 
                   And Tmp.PostSeq = PYChecksW.PostSeq)

		Delete From CFGPYContributionWage Where CFGPYContributionWage.CodeWage = @OldValue 
			And Exists (Select 'x' From CFGPYContributionWage As Tmp Where Tmp.CodeWage = @NewValue 
			And Tmp.Company = CFGPYContributionWage.Company
			And Tmp.Code = CFGPYContributionWage.Code)


-- Removed by DP
--			Delete from cfgPYWHCodes where Code = @OldValue
			
	    END --Existing
-- Add by DP 4.0
		Else	-- Not Existing
		begin
			if (not exists (select 'x' from cfgpyWHcodes old where old.Code = @OldValue))
				begin
			   		RAISERROR ('Existing code does not exist.  Conversion will not continue.', 16, 3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error					
				end

			select * into #TempKeyConvert from CFGPYWHCodes where Code = @OldValue
			Update #TempKeyConvert set Code=@NewValue
			Insert CFGPYWHCodes select * from #TempKeyConvert
			Drop Table #TempKeyConvert
			set @NewName = @OldName
		end

		exec @RetVal = KeyCvt @Entity = @Entity,
					 @CvtType = @CvtType,
					 @Length = @Length,
					 @ErrMsg = @ErrMsg2 output,
					 @Diag = @Diag,
					 @NewValue = @NewValue,
					 @OldValue = @OldValue,
					 @ConstrFlag = 3 -- UpdateTables
		Delete from cfgPYWHCodes where Code = @OldValue
--
		if (@RetVal = 0)
			begin
-- Add by DP 4.0
				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq)
					select PKey, Entity, OldKey, NewKey, @OldName, @NewName, @Period, @LastPostSeq from keyConvertWork where Pkey = @PKey
				end
--
				delete from keyConvertWork where current of keysCursor
				commit Transaction
			end
		else
			begin
				set @ErrMsg = @ErrMsg + @ErrMsg2
				rollback transaction
			end
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue,
		@PKey
	set @KeysFetch = @@Fetch_Status
	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	return(0)
End -- Procedure
GO
