SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpChecksum]
  @strPlanID VARCHAR(32)

AS

BEGIN -- Procedure rpChecksum

  SET NOCOUNT ON
  SET ANSI_WARNINGS OFF
  
  DECLARE @CS_Plan int
  DECLARE @CS_Task int
  DECLARE @CS_TPD int
  
  DECLARE @strPlanName Nvarchar(255)
    
  SELECT @CS_TPD = 
    BINARY_CHECKSUM(SUM(ISNULL(PlannedLaborHrs, 0)),
                    SUM(ISNULL(PlannedLabCost, 0)),
                    SUM(ISNULL(PlannedLabBill, 0)),
                    SUM(ISNULL(LabRevenue, 0)),
                    SUM(ISNULL(PlannedExpCost, 0)),
                    SUM(ISNULL(PlannedExpBill, 0)),
                    SUM(ISNULL(ExpRevenue, 0)),
                    SUM(ISNULL(PlannedDirExpCost, 0)),
                    SUM(ISNULL(PlannedDirExpBill, 0)),
                    SUM(ISNULL(PlannedConCost, 0)),
                    SUM(ISNULL(PlannedConBill, 0)),
                    SUM(ISNULL(ConRevenue, 0)),
                    SUM(ISNULL(PlannedDirConCost, 0)),
                    SUM(ISNULL(PlannedDirConBill, 0)))
    FROM
      (SELECT 
         SUM(ISNULL(PeriodHrs, 0)) AS PlannedLaborHrs,
         SUM(ISNULL(PeriodCost, 0)) AS PlannedLabCost,
         SUM(ISNULL(PeriodBill, 0)) AS PlannedLabBill,
         SUM(ISNULL(PeriodRev, 0)) AS LabRevenue,
         0 AS PlannedExpCost,
         0 AS PlannedExpBill,
         0 AS ExpRevenue,
         0 AS PlannedDirExpCost,
         0 AS PlannedDirExpBill,
         0 AS PlannedConCost,
         0 AS PlannedConBill,
         0 AS ConRevenue,
         0 AS PlannedDirConCost,
         0 AS PlannedDirConBill 
         FROM RPPlannedLabor AS TPD INNER JOIN RPAssignment AS A
           ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID AND TPD.AssignmentID = A.AssignmentID
         WHERE TPD.PlanID = @strPlanID
       UNION ALL
       SELECT 
         SUM(ISNULL(PeriodHrs, 0)) AS PlannedLaborHrs,
         SUM(ISNULL(PeriodCost, 0)) AS PlannedLabCost,
         SUM(ISNULL(PeriodBill, 0)) AS PlannedLabBill,
         SUM(ISNULL(PeriodRev, 0)) AS LabRevenue,
         0 AS PlannedExpCost,
         0 AS PlannedExpBill,
         0 AS ExpRevenue,
         0 AS PlannedDirExpCost,
         0 AS PlannedDirExpBill,
         0 AS PlannedConCost,
         0 AS PlannedConBill,
         0 AS ConRevenue,
         0 AS PlannedDirConCost,
         0 AS PlannedDirConBill 
         FROM RPPlannedLabor AS TPD INNER JOIN
           (SELECT UT.TaskID, SUM(UT.ChildrenCount) AS ChildrenCount, 
                   UT.CostRate AS CostRate, UT.BillingRate AS BillingRate FROM
              (SELECT PT.TaskID AS TaskID, COUNT(CT.TaskID) AS ChildrenCount, 
                      PT.CostRate AS CostRate, PT.BillingRate AS BillingRate
                 FROM RPTask AS PT LEFT JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
                 WHERE PT.PlanID = @strPlanID GROUP BY PT.TaskID, PT.CostRate, PT.BillingRate
               UNION ALL
               SELECT PT.TaskID AS TaskID, COUNT(A.AssignmentID) AS ChildrenCount, 
                      PT.CostRate AS CostRate, PT.BillingRate AS BillingRate 
                 FROM RPTask AS PT LEFT JOIN RPAssignment AS A ON PT.PlanID = A.PlanID AND PT.TaskID = A.TaskID
                 WHERE PT.PlanID = @strPlanID GROUP BY PT.TaskID, PT.CostRate, PT.BillingRate) AS UT
            GROUP BY UT.TaskID, UT.CostRate, UT.BillingRate) AS T
           ON TPD.TaskID = T.TaskID AND T.ChildrenCount = 0
         WHERE TPD.PlanID = @strPlanID AND TPD.AssignmentID IS NULL
       UNION ALL
       SELECT 
         0 AS PlannedLaborHrs,
         0 AS PlannedLabCost,
         0 AS PlannedLabBill,
         0 AS LabRevenue,
         SUM(ISNULL(PeriodCost, 0))  AS PlannedExpCost,
         SUM(ISNULL(PeriodBill, 0))  AS PlannedExpBill,
         SUM(ISNULL(PeriodRev, 0))  AS ExpRevenue,
         SUM(ISNULL(CASE WHEN E.DirectAcctFlg = 'Y'
                         THEN PeriodCost ELSE 0.0 END, 0))  AS PlannedDirExpCost,
         SUM(ISNULL(CASE WHEN E.DirectAcctFlg = 'Y'
                         THEN PeriodBill ELSE 0.0 END, 0))  AS PlannedDirExpBill,
         0 AS PlannedConCost,
         0 AS PlannedConBill,
         0 AS ConRevenue,
         0 AS PlannedDirConCost,
         0 AS PlannedDirConBill 
         FROM RPPlannedExpenses AS TPD INNER JOIN RPExpense AS E
           ON TPD.PlanID = E.PlanID AND TPD.TaskID = E.TaskID AND TPD.ExpenseID = E.ExpenseID
         WHERE TPD.PlanID = @strPlanID
       UNION ALL
       SELECT 
         0 AS PlannedLaborHrs,
         0 AS PlannedLabCost,
         0 AS PlannedLabBill,
         0 AS LabRevenue,
         SUM(ISNULL(PeriodCost, 0))  AS PlannedExpCost,
         SUM(ISNULL(PeriodBill, 0))  AS PlannedExpBill,
         SUM(ISNULL(PeriodRev, 0))  AS ExpRevenue,
         SUM(ISNULL(PeriodCost, 0))  AS PlannedDirExpCost,
         SUM(ISNULL(PeriodBill, 0))  AS PlannedDirExpBill,
         0 AS PlannedConCost,
         0 AS PlannedConBill,
         0 AS ConRevenue,
         0 AS PlannedDirConCost,
         0 AS PlannedDirConBill 
         FROM RPPlannedExpenses AS TPD INNER JOIN
           (SELECT UT.TaskID, SUM(UT.ChildrenCount) AS ChildrenCount FROM
              (SELECT PT.TaskID AS TaskID, COUNT(CT.TaskID) AS ChildrenCount
                 FROM RPTask AS PT LEFT JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
                 WHERE PT.PlanID = @strPlanID GROUP BY PT.TaskID
               UNION
               SELECT PT.TaskID AS TaskID, COUNT(E.ExpenseID) AS ChildrenCount
                 FROM RPTask AS PT LEFT JOIN RPExpense AS E ON PT.PlanID = E.PlanID AND PT.TaskID = E.TaskID
                 WHERE PT.PlanID = @strPlanID GROUP BY PT.TaskID) AS UT
            GROUP BY UT.TaskID) AS T
           ON TPD.TaskID = T.TaskID AND T.ChildrenCount = 0
         WHERE TPD.PlanID = @strPlanID AND TPD.ExpenseID IS NULL
       UNION ALL
       SELECT 
         0 AS PlannedLaborHrs,
         0 AS PlannedLabCost,
         0 AS PlannedLabBill,
         0 AS LabRevenue,
         0 AS PlannedExpCost,
         0 AS PlannedExpBill,
         0 AS ExpRevenue,
         0 AS PlannedDirExpCost,
         0 AS PlannedDirExpBill,
         SUM(ISNULL(PeriodCost, 0))  AS PlannedConCost,
         SUM(ISNULL(PeriodBill, 0))  AS PlannedConBill,
         SUM(ISNULL(PeriodRev, 0))  AS ConRevenue,
         SUM(ISNULL(CASE WHEN C.DirectAcctFlg = 'Y'
                         THEN PeriodCost ELSE 0.0 END, 0))  AS PlannedDirConCost,
         SUM(ISNULL(CASE WHEN C.DirectAcctFlg = 'Y'
                         THEN PeriodBill ELSE 0.0 END, 0))  AS PlannedDirConBill
         FROM RPPlannedConsultant AS TPD INNER JOIN RPConsultant AS C
           ON TPD.PlanID = C.PlanID AND TPD.TaskID = C.TaskID AND TPD.ConsultantID = C.ConsultantID
         WHERE TPD.PlanID = @strPlanID
       UNION ALL
       SELECT 
         0 AS PlannedLaborHrs,
         0 AS PlannedLabCost,
         0 AS PlannedLabBill,
         0 AS LabRevenue,
         0 AS PlannedExpCost,
         0 AS PlannedExpBill,
         0 AS ExpRevenue,
         0 AS PlannedDirExpCost,
         0 AS PlannedDirExpBill,
         SUM(ISNULL(PeriodCost, 0))  AS PlannedConCost,
         SUM(ISNULL(PeriodBill, 0))  AS PlannedConBill,
         SUM(ISNULL(PeriodRev, 0))  AS ConRevenue,
         SUM(ISNULL(PeriodCost, 0))  AS PlannedDirConCost,
         SUM(ISNULL(PeriodBill, 0))  AS PlannedDirConBill
         FROM RPPlannedConsultant AS TPD INNER JOIN
           (SELECT UT.TaskID, SUM(UT.ChildrenCount) AS ChildrenCount FROM
              (SELECT PT.TaskID AS TaskID, COUNT(CT.TaskID) AS ChildrenCount
                 FROM RPTask AS PT LEFT JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
                 WHERE PT.PlanID = @strPlanID GROUP BY PT.TaskID
               UNION
               SELECT PT.TaskID AS TaskID, COUNT(C.ConsultantID) AS ChildrenCount
                 FROM RPTask AS PT LEFT JOIN RPConsultant AS C ON PT.PlanID = C.PlanID AND PT.TaskID = C.TaskID
                 WHERE PT.PlanID = @strPlanID GROUP BY PT.TaskID) AS UT
            GROUP BY UT.TaskID) AS T
           ON TPD.TaskID = T.TaskID AND T.ChildrenCount = 0
         WHERE TPD.PlanID = @strPlanID AND TPD.ConsultantID IS NULL) AS CK
         
  -- Calculate Checksum of Top-most level Tasks.
  
  SELECT @CS_Task = 
    BINARY_CHECKSUM(SUM(ISNULL(PlannedLaborHrs, 0)),
                    SUM(ISNULL(PlannedLabCost, 0)),
                    SUM(ISNULL(PlannedLabBill, 0)),
                    SUM(ISNULL(LabRevenue, 0)),
                    SUM(ISNULL(PlannedExpCost, 0)),
                    SUM(ISNULL(PlannedExpBill, 0)),
                    SUM(ISNULL(ExpRevenue, 0)),
                    SUM(ISNULL(PlannedDirExpCost, 0)),
                    SUM(ISNULL(PlannedDirExpBill, 0)),
                    SUM(ISNULL(PlannedConCost, 0)),
                    SUM(ISNULL(PlannedConBill, 0)),
                    SUM(ISNULL(ConRevenue, 0)),
                    SUM(ISNULL(PlannedDirConCost, 0)),
                    SUM(ISNULL(PlannedDirConBill, 0)))
    FROM RPTask AS T
    WHERE T.PlanID = @strPlanID AND T.OutlineLevel = 0
      
  -- Calculate Checksum of Plan.
  
  SELECT @CS_Plan = 
    BINARY_CHECKSUM(PlannedLaborHrs, 
                    PlannedLabCost, 
                    PlannedLabBill, 
                    LabRevenue, 
                    PlannedExpCost, 
                    PlannedExpBill, 
                    ExpRevenue, 
                    PlannedDirExpCost, 
                    PlannedDirExpBill, 
                    PlannedConCost, 
                    PlannedConBill, 
                    ConRevenue, 
                    PlannedDirConCost, 
                    PlannedDirConBill),
         @strPlanName = PlanName
    FROM RPPlan
    WHERE PlanID = @strPlanID
    
    IF (@CS_Plan <> @CS_TPD OR @CS_Plan <> @CS_Task OR @CS_TPD <> @CS_Task)
      BEGIN
        PRINT '>>> Checksums do not match for PlanID ' + @strPlanID
        PRINT '..... Checksum for Plan = ' + STR(@CS_Plan)
        PRINT '..... Checksum for Task = ' + STR(@CS_Task)
        PRINT '..... Checksum for TPD = ' + STR(@CS_TPD)
      END -- End If
      
  SET ANSI_WARNINGS ON  
  SET NOCOUNT OFF

END -- rpChecksum
GO
