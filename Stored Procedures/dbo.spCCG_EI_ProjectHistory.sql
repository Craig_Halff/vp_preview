SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_ProjectHistory] (@WBS1 Nvarchar(30),@Language varchar(10) = 'en-US',@Username Nvarchar(32) = null)
AS BEGIN
/*
	Copyright (c) 2013 Central Consulting Group.  All rights reserved.

	spCCG_EI_ProjectHistory '2003005.00'
	spCCG_EI_ProjectHistory '2003005.00','en-GB'
	select distinct actionTaken from CCG_EI_History
*/

	select h.WBS1, case When EM.Employee is null Then h.ActionTakenBy Else EM.LastName + N', ' + IsNull(EM.FirstName,N'') End as EmpName,
		Min(isnull(ActionLabels.Label, ActionTaken)) + case when ActionTaken = 'Stage Change-RD' then '**' when ActionTaken = 'Stage Change-P' then '*' else '' end  as ActionTaken,ActionTaken as ActionTakenKey,
		-- DateAdd(minute, DateDiff(minute, getutcdate(), getdate()), ActionDate) as ActionDate,
		convert(varchar(25), ActionDate, 120) as ActionDate,
		-- DateAdd(minute, DateDiff(minute, getutcdate(), getdate()), dbo.fnCCG_EI_GetPriorActionDate(h.Seq)) AS PriorActionDate,
		convert(varchar(25), dbo.fnCCG_EI_GetPriorActionDate(h.Seq), 120) as PriorActionDate,
		EMRecipient.LastName + N', ' + IsNull(EMRecipient.FirstName,N'') as ActionRecipient,
		isnull(lang.StageLabel,h.InvoiceStage) AS InvoiceStage,
		isnull(PriorStage.StageLabel,h.PriorInvoiceStage) AS PriorInvoiceStage,
		'' AS Message,N' ' as WBS2,N' ' as WBS3, ' ' as BTFPhase, null as ColumnLabel, null as DataType,
		case When DelegateFor is null Then null Else EMDel.LastName + N', ' + IsNull(EMDel.FirstName,N'') End as DelegateFor,
		h.Invoice
	from CCG_EI_History h
		inner join CCG_EI_ConfigStages on CCG_EI_ConfigStages.Stage = h.InvoiceStage
	left join CCG_EI_ConfigStagesDescriptions lang on CCG_EI_ConfigStages.Stage=lang.Stage and lang.UICultureName =@Language
		left join CCG_EI_ConfigStagesDescriptions PriorStage on PriorStage.Stage = h.PriorInvoiceStage and PriorStage.UICultureName =@Language
		left join EM on EM.Employee=h.ActionTakenBy
		left join EM EMRecipient on EMRecipient.Employee=h.ActionRecipient
		left join EM EMDel on EMDel.Employee = h.DelegateFor
		left join PR on PR.WBS1=h.WBS1 and PR.WBS2=N' ' and PR.WBS3=N' '
	left join BTBGSubs on BTBGSubs.MainWBS1 = PR.WBS1 and BTBGSubs.SubWBS1=BTBGSubs.MainWBS1
	left join (select id,Min(Label) as Label from CCG_Language_Labels where
		ID in('Email Reminder Sent','Email Sent','Pkg Email Created','Pkg File Created','Stage Change')
		and UICultureName = @Language and Product in ('','EI')
			group by id ) ActionLabels on LEFT(h.ActionTaken,LEN(ActionLabels.Id)) = ActionLabels.Id
	where h.WBS1=@WBS1
	group by h.WBS1, case When EM.Employee is null Then h.ActionTakenBy Else EM.LastName + N', ' + IsNull(EM.FirstName,N'') End,
		ActionTaken, ActionDate, EMRecipient.LastName + N', ' + IsNull(EMRecipient.FirstName,N''),
		InvoiceStage, PriorInvoiceStage, h.Seq, h.Invoice, lang.StageLabel, PriorStage.StageLabel,
		DelegateFor, EMDel.LastName, EMDel.FirstName

	UNION ALL
	select msg.WBS1,
		case When EM.Employee is null Then msg.EnteredBy Else EM.LastName + N', ' + IsNull(EM.FirstName,N'') End as EmpName,
		'Message Entered' AS ActionTaken,--TODO lang
		'Message Entered' AS ActionTakenKey,
		-- DateAdd(minute, DateDiff(minute, getutcdate(), getdate()), msg.DateEntered) AS ActionDate,
		convert(varchar(25), msg.DateEntered, 120) as ActionDate,
		NULL AS PriorActionDate, NULL AS ActionRecipient, NULL AS InvoiceStage, NULL AS PriorInvoiceStage,
		CAST(IsNull(msg.Message,'') AS varchar(8000)) AS Message,N' ' as WBS2,N' ' as WBS3, ' ' as BTFPhase, null as ColumnLabel, null as DataType,
		null as DelegateFor, null as Invoice
	from CCG_EI_Messages msg
		left join EM on EM.Employee=msg.EnteredBy
		left join PR on PR.WBS1=msg.WBS1 and PR.WBS2=N' ' and PR.WBS3=N' '
	left join BTBGSubs on BTBGSubs.MainWBS1 = PR.WBS1 and BTBGSubs.SubWBS1=BTBGSubs.MainWBS1
	where msg.WBS1=@WBS1
	group by msg.WBS1, case When EM.Employee is null Then msg.EnteredBy Else EM.LastName + N', ' + IsNull(EM.FirstName,N'') End,
		msg.DateEntered, CAST(IsNull(msg.Message,'') AS varchar(8000))

	UNION ALL
	select huc.WBS1,
		case When EM.Employee is null Then huc.ActionTakenBy Else EM.LastName + N', ' + IsNull(EM.FirstName,N'') End as EmpName,
		'Column ' + huc.ActionTaken AS ActionTaken,--TODO Lang
		'Column ' + huc.ActionTaken AS ActionTakenKey,
		-- DateAdd(minute, DateDiff(minute, getutcdate(), getdate()), huc.ActionDate) AS ActionDate,
		convert(varchar(25), huc.ActionDate, 120) as ActionDate,
		NULL AS PriorActionDate,
		NULL AS ActionRecipient,
		CAST(CASE when NewValue IS NULL then NULL
				when DataType = 'P' then CAST(CAST(NewValue AS decimal(19,4)) as varchar(20))
				when DataType = 'D' then convert(varchar(10), CAST(NewValue as DATETIME), 120)
				when DataType = 'T' then convert(varchar(19), CAST(NewValue as DATETIME), 120)
				when DataType = 'N' then CAST(CAST(NewValue AS decimal(19,2)) as varchar(20))
				else CAST(NewValue as varchar(8000)) END AS varchar(8000))
			AS InvoiceStage,
		CAST(CASE when OldValue IS NULL then NULL
				when DataType = 'P' then CAST(CAST(OldValue AS decimal(19,4)) as varchar(20))
				when DataType = 'D' then convert(varchar(10), CAST(OldValue as DATETIME), 120)
				when DataType = 'T' then convert(varchar(19), CAST(OldValue as DATETIME), 120)
				when DataType = 'N' then CAST(CAST(OldValue AS decimal(19,2)) as varchar(20))
				else CAST(OldValue as varchar(8000)) END as varchar(8000))
			AS PriorInvoiceStage,
		'' AS Message,huc.WBS2,huc.WBS3, Convert(varchar(100),BTF.Phase) as BTFPhase,
		case when ColumnLabel = 'CompDate' then ' Completion Date' else Replace(ColumnLabel,'\n',' ') END as ColumnLabel, DataType,
		null as DelegateFor, null as Invoice
	from CCG_EI_HistoryUpdateColumns huc
		left join EM on EM.Employee=huc.ActionTakenBy
		left join PR on PR.WBS1 = huc.WBS1 and PR.WBS2 = N' ' and PR.WBS3 = N' '
		left join BTF on huc.WBS1 = BTF.WBS1 and huc.WBS2 = BTF.WBS2 and huc.WBS3 = BTF.WBS3 and huc.BTFSeq = BTF.Seq
	left join BTBGSubs on BTBGSubs.MainWBS1 = PR.WBS1 and BTBGSubs.SubWBS1 = BTBGSubs.MainWBS1
	where huc.WBS1 = @WBS1
	group by huc.WBS1, huc.WBS2, huc.WBS3, BTF.Phase,
		case When EM.Employee is null Then huc.ActionTakenBy Else EM.LastName + N', ' + IsNull(EM.FirstName,N'') End,
		huc.ActionTaken, huc.ActionDate, NewValue, OldValue, DataType, ColumnLabel

	order by ActionDate desc
END
GO
