SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_MC_GeneralSelects] (
	@type		varchar(50),
	@param1		Nvarchar(max) = N''
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF @type = 'GetLanguageLabels'
		SELECT Group1, ID, Product, UICultureName, Label, 'Preexisting' as RowStatus
			FROM CCG_Language_Labels
			WHERE Product IN ('','PAT') and (UICultureName = @param1 OR ISNULL(@param1,N'') = N'')
	ELSE IF @type = 'GetLanguageLabels_General'
		SELECT Id, MAX(Label) as Label, CASE WHEN UICultureName = @param1 THEN 1 ELSE 0 END as Specificity
	        FROM CCG_Language_Labels
	        WHERE Product IN ('','PAT') and UICultureName like LEFT(@param1, 2)+'%'
	        GROUP BY Id, (CASE WHEN UICultureName = @param1 THEN 1 ELSE 0 END)
	        ORDER BY Id, Specificity Desc
	ELSE IF @type = 'MaxCheckLength'
		SELECT MAX(len(checkno))
            FROM (
                SELECT TOP 100 checkNo FROM cvMaster
                    WHERE Posted = 'Y'
                    ORDER BY TransDate desc
            ) recentCV
	ELSE IF @type = 'Config_CheckUDFs'
		SELECT sysobjects.Name
            FROM sysobjects
                INNER JOIN syscolumns on syscolumns.id = sysobjects.id
            WHERE sysobjects.Name in (N'VendorCustomTabFields', N'ProjectCustomTabFields') and sysobjects.xtype in ('U','V')
                and syscolumns.name = N'CustPayablesFolder'
            ORDER BY syscolumns.name
END

GO
