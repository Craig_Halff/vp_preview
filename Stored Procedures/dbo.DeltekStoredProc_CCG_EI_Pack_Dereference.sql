SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Pack_Dereference]
	@getAddress			bit,
	@fieldsql			varchar(500),
	@id					Nvarchar(500),
	@param				varchar(max),
	@descriptionField	varchar(50),
	@table1				varchar(50),
	@table2				varchar(50),
	@table3				varchar(50),
	@idField			varchar(50),
	@hasAddress4		bit = 1,
	@XML_String			VARCHAR(500) = ''
AS
BEGIN
	/*
	[DeltekStoredProc_CCG_EI_Pack_Dereference] 1, 'EM.Employee', 'Test',
		'', '''Address''', 'EM', 'EmployeeCustomTabFields', 'EM', 'Employee', 0, ''
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	int;

	-- Ensure that our inputs are safe (prevent SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<SQL PHRASE>', @fieldsql);
	SET @safeSql = @safeSql | (dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @id) * 2);
	SET @safeSql = @safeSql | (dbo.fnCCG_EI_RegexMatch('<FIELD>|', @param) * 4);
	SET @safeSql = @safeSql | (dbo.fnCCG_EI_RegexMatch('<FIELD>|', @table1) * 8);
	SET @safeSql = @safeSql | (dbo.fnCCG_EI_RegexMatch('<FIELD>|', @table2) * 16);
	SET @safeSql = @safeSql | (dbo.fnCCG_EI_RegexMatch('<FIELD>|', @table3) * 32);
	SET @safeSql = @safeSql | (dbo.fnCCG_EI_RegexMatch('<SQL PHRASE>|', @descriptionField) * 64);
	SET @safeSql = @safeSql | (dbo.fnCCG_EI_RegexMatch('<FIELD>', @idField) * 128);
	SET @safeSql = @safeSql | (dbo.fnCCG_EI_RegexMatch('<SQL PHRASE>|', @XML_String) * 256);
	IF @safeSql < 511 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS varchar(4));
		RETURN;
	END;

	IF @getAddress = 0 SET @sSQL = '
		SELECT ' + @fieldsql + ' FROM ' + @table1 + '
			INNER JOIN ' + @table2 + ' C on ' + @table1 + '.' + @idField + ' = C.' + @idField + '
			WHERE ' + @table1 + '.' + @idField + ' = ''' + @id + '''';
	ELSE SET @sSQL = '
		SELECT top 1 ' + @descriptionField + ' as Description, Address1, Address2, Address3, ' +
			(Case When @hasAddress4 = 1 THEN 'Address4, ' ELSE '' END) + '
			City, State, ZIP, Country
			FROM ' + @table3 + '
			WHERE ' + @idField + ' = ''' + @id + ''' and ' +
				(CASE WHEN @param = '' THEN '''Y''' WHEN CHARINDEX('bill', @param) > 0 THEN 'Billing' ELSE 'PrimaryInd' END) + ' = ''Y''
			ORDER BY CreateDate DESC';
	SET @sSQL += ' ' + @XML_String;

	--PRINT @sSQL
	EXEC(@sSQL);
END;
GO
