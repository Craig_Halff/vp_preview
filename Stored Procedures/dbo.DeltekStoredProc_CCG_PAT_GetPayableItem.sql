SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_GetPayableItem]
	@seq		int
AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_GetPayableItem] 1
	SET NOCOUNT ON;
	SELECT *
		FROM CCG_PAT_Payable
		WHERE Seq = @seq
END;

GO
