SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[~spCCG_ProjectLaborBillerNotification_20180502] 
AS
/*
Copyright 2017 (c) Central Consulting Group.   All rights reserved.
08/24/2017 David Springer
           Write Biller into EI CCG_Email table with regular projects with BillExt = 0.

- REVISED 1/5/2018 Jackson Gray
*/

SET NOCOUNT ON
DECLARE @Biller			varchar (8000),
        @Email			varchar (100),
		@LastName		varchar (50),
		@FirstName		varchar (25),
		@WBS1			varchar (32),
		@WBS2			varchar (7),
		@WBS3			varchar (7),
		@TransDate		datetime,
		@Employee		varchar (255),
		@Category		varchar (8000),
		@Hours			decimal (19,4),
		@Multiplier		decimal (19,4),
		@BilledInvoice	varchar (12),
        @Body			varchar (max),
		@CSS            varchar (max),
        @Loop1			integer

BEGIN

	-- CSS style rules
	Set @CSS = 
'
<style type="text/css">
	p {padding-top:15px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;}
	td, th {border: 1px solid black;padding: 5px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;font-size:10.5pt;}
	th {background-color: #6E6E6E;color: white;text-align:center;}
	.c1, .c2,. c3, .c4, .c5, .c6, .c9 {text-align:left;}
	.c7, .c8 {text-align:right;}
</style>
'

	DECLARE curBiller insensitive cursor for
		Select Distinct IsNull (p.Biller, '001975'), e.EMail, e.LastName, e.FirstName
		From PR p, LD l, EMMain e
		Where p.WBS1 = l.WBS1
		  and p.WBS2 = ' '  --- project level for Biller
		  and p.WBS1 not like 'Z%'
		  and p.ChargeType = 'R'
		  and l.LaborCode = '01'
		  and l.BillStatus = 'B'
		  and l.TransDate > '04-24-2017'
		  and p.Biller = e.Employee
		  and l.BillExt = 0

	-- Get each biller
	OPEN curBiller
	FETCH NEXT FROM curBiller INTO @Biller, @Email, @LastName, @FirstName
	Set @Loop1 = @@FETCH_STATUS
	While @Loop1 = 0
	BEGIN

		Declare curProjects insensitive cursor for
		SELECT * 
		FROM 
		(
			   SELECT 
			   IsNull(WBS1, '') As WBS1
			   , IsNull(WBS2, '') As WBS2
			   , IsNull(WBS3, '') As WBS3
			   , IsNull(Name, '') As Name
			   , TransDate
			   , IsNull([Labor Category], '') AS [Labor Category]
			   , IsNull([Hours], 0) As [Hours]
			   , [BillExt]
			   , Round([BillExt] / [Amount], 2) AS [Multiplier]
			   , IsNull(BilledInvoice, '') As BilledInvoice
			   FROM 
			   (
					Select distinct p.Biller
					, x.CustFeeType
					, p.org Org
					, l.WBS1
					, l.WBS2
					, l.WBS3
					, l.Name
					, l.TransDate
					, ltrim (str (l.Category)) + ' - ' + c.Description 'Labor Category'
					, l.RegHrs + l.OvtHrs + l.SpecialOvtHrs 'Hours'
					, l.RegAmt + l.OvtAmt + l.SpecialOvtAmt 'Amount'
					, l.BillExt
					, l.BillStatus
					, l.BilledInvoice
					From PR p, ProjectCustomTabFields x, LD l
							Left Join BTLaborCats c on c.Category = l.Category
					Where p.WBS1 = l.WBS1
						and x.WBS1 = l.WBS1
						and x.WBS2 = l.WBS2
						and x.WBS3 = l.WBS3
						and p.WBS2 = ' '  --- project level for Biller
						and p.WBS1 not like 'Z%'
						and p.ChargeType = 'R'
						and l.LaborCode = '01'
						and l.BillStatus <> 'T'
						and l.TransDate > '2017-04-24'
					) AS A
			   ) AS B
		WHERE B.Multiplier = 0.00 or B.Multiplier = 1.00
		ORDER BY WBS1, WBS2, WBS3, Name, TransDate ASC

		-- Open the table with headers
		Set @Body = 
'<p><strong>' + IsNull(@LastName, '') + ', ' + IsNull(@FirstName, '') + '</strong></p> 
<table cellpadding=''5'' cellspacing=''0'' style=''border:1px solid black;border-collapse:collapse;''>
	<thead>
		<tr>
			<th>Project</th>
			<th>Phase</th>
			<th>Task</th>
			<th>Employee</th>
			<th>Date</th>
			<th>Category</th>
			<th>Hours</th>
			<th>Multiplier</th>
			<th>Billed<br/>Invoice</th>
		</tr>
	</thead>
	<tbody>
'

		Open curProjects
		Fetch next from curProjects Into @WBS1, @WBS2, @WBS3, @Employee, @TransDate, @Category, @Hours, @Multiplier, @BilledInvoice 
		While @@FETCH_STATUS = 0
		Begin

			Set @Body = @Body + 
'		<tr>
			<td class="c1"> ' + @WBS1 + '</td>
			<td class="c2"> ' + @WBS2 + '</td>
			<td class="c3"> ' + @WBS3 + '</td>
			<td class="c4"> ' + @Employee + '</td>
			<td class="c5"> ' + Convert(varchar(50), @TransDate, 101) + '</td>
			<td class="c6"> ' + @Category + '</td>
			<td class="c7"> ' + Cast(Convert(decimal(19,2), @Hours) As varchar(50)) + '</td>
			<td class="c8"> ' + Cast(Convert(decimal(19,2), @Multiplier) As varchar(50)) + '</td>
			<td class="c9"> ' + @BilledInvoice + '</td>
		</tr>
'

			Fetch next from curProjects Into @WBS1, @WBS2, @WBS3, @Employee, @TransDate, @Category, @Hours, @Multiplier, @BilledInvoice 
		End
		Close curProjects
		Deallocate curProjects

		-- Close the table
		Set @Body = @Body + 
'	</tbody>
</table>
'

	Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
	Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
	
	FETCH NEXT FROM curBiller INTO @Biller, @Email, @LastName, @FirstName
	Set @Loop1 = @@FETCH_STATUS

	END
	CLOSE curBiller
	DEALLOCATE curBiller

END






-- previous version


/*
SET NOCOUNT ON
DECLARE @Biller     varchar (8000),
        @Email      varchar (100),
		@WBS1		varchar (32),
		@WBS2		varchar (7),
		@WBS3		varchar (7),
		@Period		varchar (6),
		@Employee	varchar (255),
		@Category	varchar (8000),
        @Body       varchar (8000),
        @Loop1      integer

DECLARE curBiller insensitive cursor for
		Select distinct IsNull (p.Biller, '001975'), e.EMail
		From PR p, LD l, EMMain e
		Where p.WBS1 = l.WBS1
		  and p.WBS2 = ' '  --- project level for Biller
		  and p.WBS1 not like 'Z%'
		  and p.ChargeType = 'R'
		  and l.LaborCode = '01'
		  and l.BillStatus = 'B'
		  and l.TransDate > '04-24-2017'
		  and p.Biller = e.Employee
		  and l.BillExt = 0

BEGIN
-- Get each biller
	OPEN curBiller
	FETCH NEXT FROM curBiller INTO @Biller, @Email
	Set @Loop1 = @@FETCH_STATUS
	While @Loop1 = 0
	BEGIN
		Declare curProjects insensitive cursor for
		Select distinct l.WBS1, l.WBS2, l.WBS3, l.Name, l.Period, ltrim (str (l.Category)) + ' - ' + c.Description
		From PR p, LD l
			Left Join BTLaborCats c on c.Category = l.Category
		Where p.WBS1 = l.WBS1
		  and p.WBS2 = ' '  --- project level for Biller
		  and p.WBS1 not like 'Z%'
		  and p.ChargeType = 'R'
		  and l.LaborCode = '01'
		  and l.BillStatus = 'B'
		  and l.TransDate > '04-24-2017'
		  and p.Biller = @Biller
		  and l.BillExt = 0

	-- Open the table with headers
		Set @Body = '<table><th>Project</th><th>Phase</th><th>Task</th><th>Employee</th><th>Period</th><th>Category</th>'
		Open curProjects
		Fetch next from curProjects Into @WBS1, @WBS2, @WBS3, @Employee, @Period, @Category
		While @@FETCH_STATUS = 0
		Begin
			Set @Body = @Body + '<tr><td>' + @WBS1 + '</td><td>  ' + @WBS2 + '</td><td>  ' + @WBS3 + '</td><td>  ' +  @Employee + '</td><td>  ' +  @Period + '</td><td>  ' +  @Category+ '</td></tr>'
			Fetch next from curProjects Into @WBS1, @WBS2, @WBS3, @Employee, @Period, @Category
		End
		Close curProjects
		Deallocate curProjects

	-- Close the table
		Set @Body = @Body + '</table>'

		Insert Into CCG_Email_Messages
		(SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
		Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @Body, getDate(), 1)

	FETCH NEXT FROM curBiller INTO @Biller, @Email
	Set @Loop1 = @@FETCH_STATUS

	END
	CLOSE curBiller
	DEALLOCATE curBiller

END
*/
GO
