SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Load_Settings]
	@isEIUser	bit = 1,
	@user		Nvarchar(32),
	@visionRev	varchar(10) = '7.5',
	@vision7	bit = 1
AS
BEGIN
	/*
	DeltekStoredProc_CCG_EI_Config_Load_Settings
		@isEIUser = '1',
		@user = 'ADMIN',
		@visionRev = '7.4.700',
		@vision7 = '1'

	DeltekStoredProc_CCG_EI_Config_Load_Settings

		@user = 'JEREMY.KRUBE'
	*/
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @sSQL		varchar(max);
	DECLARE @safeSql	bit;

	SELECT MIN(FirmName) as FirmName, COUNT(*) as NumFirms FROM CFGMainData;

	SELECT ClientNumber FROM FW_CFGSysConfig

	SELECT WBS2Length, WBS3Length FROM CFGFormat;

	SELECT * FROM CCG_EI_Config;

	SELECT * FROM CCG_Email_Config;

	if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[CCG_TR_Config]') AND type in (N'U'))
		exec('select * from CCG_TR_Config')
	else select null

    IF ISNULL(@user,'') <> '' BEGIN

		-- Ensure that our inputs are safe (prevent SQL injection)
		SET @safeSql = dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @user);
		IF @safeSql = 0 BEGIN
			SELECT 'ERROR: Unsafe parameters used! Execution cancelled.';
			RETURN;
		END;
		SELECT * FROM CCG_EI_User WHERE username = @user;


		SELECT Role, SEUser.Employee, EM.EMail,
			IsNull(EM.LastName, '') + ', ' + IsNull(EM.FirstName, '') as EMName, DisableLogin, EM.Language AS Language, SEUser.Domain, case when Password = 'Integrated' then 1 else 0 end as [Integrated]

			FROM SEUser
				LEFT JOIN EM on EM.Employee = SEUser.Employee
			WHERE username  = @user


		SELECT * FROM CCG_EI_ConfigSecurity
			WHERE [Role] = (SELECT [Role] FROM SEUser WHERE Username = @user);
	END;

END;
GO
