SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPDelPlan]
  @strPlanID varchar(32),
  @bPreserveStructure varchar(1) = 'N'
AS

BEGIN -- Procedure stRPDelPlan

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to delete a Navigator Plan.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

  UPDATE PR SET PlanID = NULL WHERE WBS1 IN (
     SELECT WBS1 FROM PR WITH (READUNCOMMITTED) WHERE PlanID=@strPlanID GROUP BY WBS1) 
  
  UPDATE PRDefaults WITH (ROWLOCK) SET PRDefaults.PlanID = NULL
    FROM PRDefaults AS PR 
    WHERE PR.PlanID = @strPlanID
  
  UPDATE PRTemplate WITH (ROWLOCK) SET PRTemplate.PlanID = NULL
    FROM PRTemplate AS PR 
    WHERE PR.PlanID = @strPlanID
   
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DELETE PNCalendarInterval WHERE PlanID = @strPlanID
  DELETE PNAccordionFormat WHERE PlanID = @strPlanID
  DELETE PNWBSLevelFormat WHERE PlanID = @strPlanID
  DELETE PNPlannedLabor WHERE PlanID = @strPlanID
  DELETE PNPlannedExpenses WHERE PlanID = @strPlanID
  DELETE PNPlannedConsultant WHERE PlanID = @strPlanID
  DELETE PNBaselineLabor WHERE PlanID = @strPlanID
  DELETE PNBaselineExpenses WHERE PlanID = @strPlanID
  DELETE PNBaselineConsultant WHERE PlanID = @strPlanID
  DELETE PNAssignment WHERE PlanID = @strPlanID
  DELETE PNExpense WHERE PlanID = @strPlanID
  DELETE PNConsultant WHERE PlanID = @strPlanID
  DELETE PNPlannedRevenueLabor WHERE PlanID = @strPlanID

  IF(@bPreserveStructure = 'N') 
   BEGIN    
    DELETE PNTask WHERE PlanID = @strPlanID
    DELETE PNPlan WHERE PlanID = @strPlanID	 
   END
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DELETE PlanningCustomTabFields WHERE PlanID = @strPlanID

  DELETE EMActivity
    FROM EMActivity
    INNER JOIN Activity ON EMActivity.ActivityID = Activity.ActivityID
    INNER JOIN RPAssignment ON Activity.AssignmentID = RPAssignment.AssignmentID
    WHERE RPAssignment.PlanID = @strPlanID
  
  DELETE ActivityCustomTabFields 
    FROM ActivityCustomTabFields
    INNER JOIN Activity ON ActivityCustomTabFields.ActivityID = Activity.ActivityID
    INNER JOIN RPAssignment ON Activity.AssignmentID = RPAssignment.AssignmentID
    WHERE RPAssignment.PlanID = @strPlanID

  DELETE Activity 
    FROM Activity
    INNER JOIN RPAssignment ON Activity.AssignmentID = RPAssignment.AssignmentID
    WHERE RPAssignment.PlanID = @strPlanID
  
  DELETE RPCalendarInterval WHERE PlanID = @strPlanID
  DELETE RPAccordionFormat WHERE PlanID = @strPlanID
  DELETE RPWBSLevelFormat WHERE PlanID = @strPlanID
  DELETE RPPlannedLabor WHERE PlanID = @strPlanID
  DELETE RPPlannedExpenses WHERE PlanID = @strPlanID
  DELETE RPPlannedConsultant WHERE PlanID = @strPlanID
  DELETE RPBaselineLabor WHERE PlanID = @strPlanID
  DELETE RPBaselineExpenses WHERE PlanID = @strPlanID
  DELETE RPBaselineConsultant WHERE PlanID = @strPlanID
  DELETE RPAssignment WHERE PlanID = @strPlanID
  DELETE RPExpense WHERE PlanID = @strPlanID
  DELETE RPConsultant WHERE PlanID = @strPlanID
  DELETE RPPlannedRevenueLabor WHERE PlanID = @strPlanID 

  IF(@bPreserveStructure = 'N') 
   BEGIN
    DELETE RPTask WHERE PlanID = @strPlanID
    DELETE RPPlan WHERE PlanID = @strPlanID 
   END

  SET NOCOUNT OFF

END -- stRPDelPlan
GO
