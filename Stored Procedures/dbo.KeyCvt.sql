SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyCvt]
	@Entity 			Nvarchar(40),
	@CvtType 		integer, -- 0 oldVal -> NewVal; 1 --Length Change; 2 --Remove leading zeros; 3 --length Change no zeros
	@Length  		integer,
	@ErrMsg 			Nvarchar(1000) output,
	@ChangeSide    Nvarchar(5) = 'Left',
	@Delim1			Nvarchar(1) = '',
	@DelimPos1  	integer = 0,
	@Delim2			Nvarchar(1) = '',
	@DelimPos2  	integer = 0,
	@Diag				integer = 0,
	@NewValue 		Nvarchar(32) = '',
	@NewValue2		Nvarchar(7) = null,
	@NewValue3		Nvarchar(7) = null,
	@OldValue 		Nvarchar(32) = '',
	@OldValue2		Nvarchar(7) = null,
	@OldValue3		Nvarchar(7) = null,
	@ConstrFlag		integer = 0, -- 0 ALL, 1=Drop Constraints, 2=EnableConstraints, 3=UpdateTables
	@NeedTransaction Nvarchar(1) = 'Y',
	@UserName Nvarchar(32) = 'VisionKeyFormat' -- Add by DP 4.0
	,@Period integer = 0

as
begin

set nocount on
declare 	@TableName  	Nvarchar(100),
			@ColumnName 	Nvarchar(100),
			@Val 				Nvarchar(20),
			@UpdateValue 	Nvarchar(500),
			@ErrorNum 		integer,
			@SqlStr			Nvarchar(max),
			@SqlStr2		Nvarchar(max),
			@SqlStr3		Nvarchar(max),
			@test 			integer,
			@custlabel		Nvarchar(40),
			@Custlabelplural Nvarchar(40),
			@OldDelim1		Nvarchar(1),
			@OldDelimPos1  integer,
			@OldDelim2		Nvarchar(1),
			@OldDelimPos2  integer,
			@LastColumnName Nvarchar(100),
			@CvtFetch		integer,
			@AddlWhere		Nvarchar(500),
			@WBSWhere   	Nvarchar(500),
			@saveNewValue2 Nvarchar(7),
			@saveNewValue3 Nvarchar(7),
			@tmpUpdateValue 	Nvarchar(500)

	declare @message nvarchar(max)
	set @saveNewValue2 = @NewValue2
	set @saveNewValue3 = @NewValue3
	set @LastColumnName = ''
	set @AddlWhere = ''
	select @OldDelim1 = '',
			@OldDelimPos1= 0,
			@OldDelim2 = '',
			@OldDelimPos2 = 0
	if (@Diag <> 0)
		print 'Beginning KeyCvt Procedure: ' + @Entity
	if (@Diag <> 0)
		print 'Checking for Record loss ' + @Entity
	if (@NeedTransaction = 'Y')
		begin transaction
	if (@cvtType <> 0)  --duplicate record check
		begin
		execute InsertKeyCvtDriver @Entity -- populate driver table
		if (@Entity = 'Employee')
			begin
				select @OldDelim1 = isNull(EmployeeDelimiter,N''),
				  		 @OldDelimPos1 = isNull(EmployeeDelimiterPosition,0) from cfgformat
				update cfgformat
					set EmployeeDelimiter = @Delim1,
					EmployeeDelimiterPosition = @DelimPos1,
					EmployeeLeadZeros = case when @CvtType = 1 then 'Y' else 'N' end,
					EmployeeLength = @Length

				exec KeyNewVal @ColumnName='Employee',
									@cvtType=@cvtType,
									@Length = @Length,
									@ChangeSide = @ChangeSide,
									@RetValue= @UpdateValue output,
									@OldDelim1=@OldDelim1,
									@OldDelimPos1 = @OldDelimPos1,
									@delim1=@delim1,
									@DelimPos1=@DelimPos1

				select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'employeeLabel'
				select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'employeeLabelPlural'

				if (@Diag <> 0)
					print 'declare countCursor cursor for select 1 from em group by ' + @UpdateValue + ' having count(*) > 1'
				execute('declare countCursor cursor for select 1 from em group by ' + @UpdateValue + ' having count(*) > 1')
			end -- Employee
		else if (@Entity = 'Client')
			begin
				delete from keyconvertDriver where entity = N'Client' and columnName <> N'Client'
				select @OldDelim1 = isNull(ClientDelimiter,N''),
				  		 @OldDelimPos1 = isNull(ClientDelimiterPosition,0) from cfgformat
				update cfgformat
					set ClientDelimiter = @Delim1,
					ClientDelimiterPosition = @DelimPos1,
					ClientLeadZeros = case when @CvtType = 1 then 'Y' else 'N' end,
					ClientLength = @Length
	if (@Diag <> 0)
		print 'Client oldDelim ' + @oldDelim1 + ' new delim ' + @Delim1

/*
				update cfgformat
					set ClientDelimiter = @Delim1,
					ClientDelimiterPosition = @DelimPos1,
					ClientLeadZeros = case when @CvtType = 1 then 'Y' else 'N' end,
					ClientLength = @Length

				exec KeyNewVal @ColumnName='Client',
									@cvtType=@cvtType,
									@Length = @Length, 
									@ChangeSide = @ChangeSide,
									@RetValue= @UpdateValue output,
									@OldDelim1=@OldDelim1,
									@OldDelimPos1 = @OldDelimPos1,
									@delim1=@delim1,
									@DelimPos1=@DelimPos1
*/
				select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'clientLabel'
				select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'clientLabelPlural'

				if (@Diag <> 0) --Remove duplicate number check for clients, duplicates are allowed.
					print 'declare countCursor cursor for select 1 from Cl where 0=1'
				execute('declare countCursor cursor for select 1 from Cl  where 0=1')
			end -- Client
		else if (@Entity = 'Account')
			begin
				select @OldDelim1 = isNull(AccountDelimiter,N''),
				  		 @OldDelimPos1 = isNull(AccountDelimiterPosition,0) from cfgformat
				update cfgformat
					set AccountDelimiter = @Delim1,
					AccountDelimiterPosition = @DelimPos1,
					AccountLeadZeros = case when @CvtType = 1 then 'Y' else 'N' end,
					AccountLength = @Length

				exec KeyNewVal @ColumnName='Account',
									@cvtType=@cvtType,
									@Length = @Length, 
									@ChangeSide = @ChangeSide,
									@RetValue= @UpdateValue output,
									@OldDelim1=@OldDelim1,
									@OldDelimPos1 = @OldDelimPos1,
									@delim1=@delim1,
									@DelimPos1=@DelimPos1

				select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'accountLabel'
				select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'accountLabelPlural'

				if (@Diag <> 0)
					print 'declare countCursor cursor for select 1 from ca group by ' + @UpdateValue + ' having count(*) > 1'
				execute('declare countCursor cursor for select 1 from ca group by ' + @UpdateValue + ' having count(*) > 1')
			end -- Account
		else if (@Entity = 'Vendor')
			begin
				select @OldDelim1 = isNull(VendorDelimiter,N''),
				  		 @OldDelimPos1 = isNull(VendorDelimiterPosition,0) from cfgformat
				update cfgformat
					set VendorDelimiter = @Delim1,
					VendorDelimiterPosition = @DelimPos1,
					VendorLeadZeros = case when @CvtType = 1 then 'Y' else 'N' end,
					VendorLength = @Length

				exec KeyNewVal @ColumnName='Vendor',
									@cvtType=@cvtType,
									@Length = @Length, 
									@ChangeSide = @ChangeSide,
									@RetValue= @UpdateValue output,
									@OldDelim1=@OldDelim1,
									@OldDelimPos1 = @OldDelimPos1,
									@delim1=@delim1,
									@DelimPos1=@DelimPos1

				select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'vendorLabel'
				select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'vendorLabelPlural'

				if (@Diag <> 0)
					print 'declare countCursor cursor for select 1 from ve where not vendor is null group by ' + @UpdateValue + ' having count(*) > 1'
				execute('declare countCursor cursor for select 1 from ve where not vendor is null group by ' + @UpdateValue + ' having count(*) > 1')
			end -- vendor
		else if (@Entity = 'WBS1')
			begin
				select @OldDelim1 = isNull(WBS1Delimiter1,N''),
				  		 @OldDelimPos1 = isNull(WBS1Delimiter1Position,0),
						 @OldDelim2 = isNull(WBS1Delimiter2,N''),
				  		 @OldDelimPos2 = isNull(WBS1Delimiter2Position,0) from cfgformat
				update cfgformat
					set WBS1Delimiter1 = @Delim1,
					WBS1Delimiter1Position = @DelimPos1,
					WBS1Delimiter2 = @Delim2,
					WBS1Delimiter2Position = @DelimPos2,
					WBS1LeadZeros = case when @CvtType = 1 then 'Y' else 'N' end,
					WBS1Length = @Length

				exec KeyNewVal @ColumnName='WBS1',
									@cvtType=@cvtType,
									@Length = @Length, 
									@ChangeSide = @ChangeSide,
									@RetValue= @UpdateValue output,
									@OldDelim1=@OldDelim1,
									@OldDelimPos1 = @OldDelimPos1,
									@oldDelim2 = @oldDelim2,
									@oldDelimPos2 = @OldDelimPos2,
									@delim1=@delim1,
									@DelimPos1=@DelimPos1,
									@delim2 = @Delim2,
									@delimPos2 = @delimPos2

				select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'wbs1Label'
				select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'wbs1LabelPlural'

				if (@Diag <> 0)
					print 'declare countCursor cursor for select 1 from PR group by ' + @UpdateValue + ',wbs2,wbs3 having count(*) > 1'
				execute('declare countCursor cursor for select 1 from PR group by ' + @UpdateValue + ',wbs2,wbs3 having count(*) > 1')
			end -- WBS1
		else if (@Entity = 'WBS2')
			begin
				select @OldDelim1 = isNull(WBS2Delimiter,N''),
				  		 @OldDelimPos1 = isNull(WBS2DelimiterPosition,0) from cfgformat
				update cfgformat
					set WBS2Delimiter = @Delim1,
					WBS2DelimiterPosition = @DelimPos1,
					WBS2LeadZeros = case when @CvtType = 1 then 'Y' else 'N' end,
					WBS2Length = @Length

				exec KeyNewVal @ColumnName='WBS2',
									@cvtType=@cvtType,
									@Length = @Length, 
									@ChangeSide = @ChangeSide,
									@RetValue= @UpdateValue output,
									@OldDelim1=@OldDelim1,
									@OldDelimPos1 = @OldDelimPos1,
									@delim1=@delim1,
									@DelimPos1=@DelimPos1

				select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'wbs2Label'
				select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'wbs2LabelPlural'

				if (@Diag <> 0)
					print 'declare countCursor cursor for select 1 from PR where wbs2 <> N'' '' group by WBS1,' + @UpdateValue + ',wbs3 having count(*) > 1'
				execute('declare countCursor cursor for select 1 from PR where wbs2 <> N'' '' group by WBS1,' + @UpdateValue + ',wbs3 having count(*) > 1')
			end -- WBS2
		else if (@Entity = 'WBS3')
			begin
				select @OldDelim1 = isNull(WBS3Delimiter,N''),
				  		 @OldDelimPos1 = isNull(WBS3DelimiterPosition,0) from cfgformat
				update cfgformat
					set WBS3Delimiter = @Delim1,
					WBS3DelimiterPosition = @DelimPos1,
					WBS3LeadZeros = case when @CvtType = 1 then 'Y' else 'N' end,
					WBS3Length = @Length

				exec KeyNewVal @ColumnName='WBS3',
									@cvtType=@cvtType,
									@Length = @Length, 
									@ChangeSide = @ChangeSide,
									@RetValue= @UpdateValue output,
									@OldDelim1=@OldDelim1,
									@OldDelimPos1 = @OldDelimPos1,
									@delim1=@delim1,
									@DelimPos1=@DelimPos1

				select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'wbs3Label'
				select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'wbs3LabelPlural'

				if (@Diag <> 0)
					print 'declare countCursor cursor for select 1 from PR where wbs3 <> N'' '' group by WBS1,wbs2,' + @UpdateValue + ' having count(*) > 1'
				execute('declare countCursor cursor for select 1 from PR where wbs3 <> N'' '' group by WBS1,wbs2,' + @UpdateValue + ' having count(*) > 1')
			end -- WBS3
		else if (@Entity = 'Unit')
			begin
				select @OldDelim1 = isNull(UnitDelimiter,N''),
				  		 @OldDelimPos1 = isNull(UnitDelimiterPosition,0) from cfgformat
				update cfgformat
					set UnitDelimiter = @Delim1,
					UnitDelimiterPosition = @DelimPos1,
					UnitLeadZeros = case when @CvtType = 1 then 'Y' else 'N' end,
					UnitLength = @Length

				exec KeyNewVal @ColumnName='Unit',
									@cvtType=@cvtType,
									@Length = @Length, 
									@ChangeSide = @ChangeSide,
									@RetValue= @UpdateValue output,
									@OldDelim1=@OldDelim1,
									@OldDelimPos1 = @OldDelimPos1,
									@delim1=@delim1,
									@DelimPos1=@DelimPos1

				select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'unitLabel'
				select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'unitLabelPlural'

				if (@Diag <> 0)
					print 'declare countCursor cursor for select 1 from UN group by ' + @UpdateValue + ', unitTable having count(*) > 1'
				execute('declare countCursor cursor for select 1 from UN group by ' + @UpdateValue + ', unitTable having count(*) > 1')
			end -- Unit
		else if (@Entity = 'RefNo')
			begin
				update cfgformat
					set RefNoLength = @Length

				exec KeyNewVal @ColumnName='[Invoice]',
									@cvtType=@cvtType,
									@Length = @Length, 
									@ChangeSide = @ChangeSide,
									@RetValue= @UpdateValue output,
									@OldDelim1=@OldDelim1,
									@OldDelimPos1 = @OldDelimPos1,
									@delim1=@delim1,
									@DelimPos1=@DelimPos1

				select @custlabel = 'Reference Number',
						 @Custlabelplural = 'Reference Numbers' 
				declare @query Nvarchar(max)
				set @query = ' declare countCursor cursor for select 1 from AR group by WBS1 + ''|'' + WBS2 + ''|'' + WBS3 + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from billAddDetail group by MainWBS1 + ''|'' + str(RecdSeq) + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from billARDetail group by MainWBS1 + ''|'' + str(RecdSeq) + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from billBTDDetail group by MainWBS1 + ''|'' + str(RecdSeq) + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from billConDetail group by MainWBS1 + ''|'' + OriginalTable + ''|'' +  str(OriginalPostSeq) + ''|'' + OriginalPKey + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from billExpDetail group by MainWBS1 + ''|'' + OriginalTable + ''|'' +  str(OriginalPostSeq) + ''|'' + OriginalPKey + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from billFeeDetail group by MainWBS1 + ''|'' + str(RecdSeq) + ''|'' + PreInvoice + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from billINDetail group by MainWBS1 + ''|'' + WBS1 + ''|'' + WBS2 + ''|'' + WBS3  + ''|'' + PKey + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from billINMaster group by MainWBS1 + ''|'' + WBS1 + ''|'' + WBS2 + ''|'' + WBS3 + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from billIntDetail group by MainWBS1 + ''|'' + str(RecdSeq) + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from billPreInvoice group by WBS1 + ''|'' + ' + replace(@UpdateValue,'[Invoice]','PreInvoice') + ' + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from billInvMaster group by MainWBS1 + ''|'' + str(RecdSeq) + ''|'' + PreInvoice + ''|'' + ' + @UpdateValue + ' having count(*) > 1 '
				set @query = @query + 'union  select 1 from billInvSums group by MainWBS1 + ''|'' + str(RecdSeq) + ''|'' + ArrayType + ''|'' + PreInvoice + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from billLabDetail group by MainWBS1 + ''|'' + OriginalTable + ''|'' +  str(OriginalPostSeq) + ''|'' + OriginalPKey + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from billLimDetail group by MainWBS1 + ''|'' + str(RecdSeq) + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from billRetDetail group by MainWBS1 + ''|'' + str(RecdSeq) + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from billTaxDetail group by MainWBS1 + ''|'' + str(RecdSeq) + ''|'' + PreInvoice + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from billUnitDetail group by MainWBS1 + ''|'' + OriginalTable + ''|'' +  str(OriginalPostSeq) + ''|'' + OriginalPKey + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from ICBillInvMaster group by Company + ''|'' + str(Period) + ''|'' + str(RunSeq) + ''|'' + MainWBS1 + ''|'' + str(RecdSeq) + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from ICBillLabDetail group by Company + ''|'' + str(Period) + ''|'' + str(RunSeq) + ''|'' + MainWBS1 + ''|'' + OriginalTable + ''|'' +  str(OriginalPostSeq) + ''|'' + OriginalPKey + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from ICBillExpDetail group by Company + ''|'' + str(Period) + ''|'' + str(RunSeq) + ''|'' + MainWBS1 + ''|'' + OriginalTable + ''|'' +  str(OriginalPostSeq) + ''|'' + OriginalPKey + ''|'' + ' + @UpdateValue + ' having count(*) > 1 '
				set @query = @query + 'union  select 1 from ICBillTaxDetail group by Company + ''|'' + str(Period) + ''|'' + str(RunSeq) + ''|'' + MainWBS1 + ''|'' + str(RecdSeq) + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from ICBillInvSums group by Company + ''|'' + str(Period) + ''|'' + str(RunSeq) + ''|'' + MainWBS1 + ''|'' + str(RecdSeq) + ''|'' + ArrayType + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from cdDetail group by Batch + ''|'' + PKey + ''|'' + ' + replace(@UpdateValue,'[Invoice]','CheckNo') + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from cdMaster group by Batch  + ''|'' + ' + replace(@UpdateValue,'[Invoice]','CheckNo') + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from crDetail group by Batch + ''|'' + PKey + ''|'' + ' + replace(@UpdateValue,'[Invoice]','RefNo') + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from crMaster group by Batch + ''|'' + ' + replace(@UpdateValue,'[Invoice]','RefNo') + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from cvDetail group by Batch + ''|'' + PKey + ''|'' + ' + replace(@UpdateValue,'[Invoice]','CheckNo') + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from cvMaster group by Batch + ''|'' + ' + replace(@UpdateValue,'[Invoice]','CheckNo') + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from erDetail group by Batch + ''|'' + PKey + ''|'' + ' + replace(@UpdateValue,'[Invoice]','RefNo') + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from erMaster group by Batch + ''|'' + ' + replace(@UpdateValue,'[Invoice]','RefNo') + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from EX group by Employee + ''|'' + ' + replace(@UpdateValue,'[Invoice]','Voucher') + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from inDetail group by Batch + ''|'' + WBS1 + ''|'' + WBS2 + ''|'' + WBS3 + ''|'' + PKey + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from inMaster group by Batch + ''|'' + WBS1 + ''|'' + WBS2 + ''|'' + WBS3 + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from jeDetail group by Batch + ''|'' + PKey + ''|'' + ' + replace(@UpdateValue,'[Invoice]','RefNo') + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from jeMaster group by Batch + ''|'' + ' + replace(@UpdateValue,'[Invoice]','RefNo') + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from miDetail group by Batch + ''|'' + PKey + ''|'' + ' + replace(@UpdateValue,'[Invoice]','RefNo') + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from miMaster group by Batch + ''|'' + ' + replace(@UpdateValue,'[Invoice]','RefNo') + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from prDetail group by Batch + ''|'' + PKey + ''|'' + ' + replace(@UpdateValue,'[Invoice]','RefNo') + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from prMaster group by Batch + ''|'' + ' + replace(@UpdateValue,'[Invoice]','RefNo') + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from SavedInvoices group by WBS1 + ''|'' + str(Seq ) + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from VO group by Vendor + ''|'' + ' + replace(@UpdateValue,'[Invoice]','Voucher') + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from ARPreInvoice group by WBS1 + ''|'' + WBS2 + ''|'' + WBS3 + ''|'' + ' + replace(@UpdateValue,'[Invoice]','PreInvoice') + ' having count(*) > 1 ' 
				set @query = @query + 'union  select 1 from billPriorInvoices group by MainWBS1 + ''|'' + str(RecdSeq) + ''|'' + ' + @UpdateValue + ' having count(*) > 1 ' 

				if (@Diag <> 0)
					print @query
				execute(@query)
			end -- RefNo
		open countCursor
		fetch next from countCursor into @test
		close countCursor
		deallocate countCursor
		if (@@FETCH_STATUS = 0) --duplicate records
			begin
				SET @message = dbo.GetMessage('MsgSomeWillLostIfNumShortConvNot',@custlabelplural,@custlabel,'','','','','','','')
				RAISERROR(@message,16,3)
					if (@@Trancount > 0 and @NeedTransaction = 'Y')
					   ROLLBACK TRANSACTION
					return(50001) -- user defined error
			end
	end --duplicate record check


		declare 	@Ent1Start                 smallint,
				@Ent1Length                smallint,
				@Ent2Start                 smallint,
				@Ent2Length                smallint,
				@Ent3Start                 smallint,
				@Ent3Length                smallint,
				@Ent4Start                 smallint,
				@Ent4Length                smallint,
				@Ent5Start                 smallint,
				@Ent5Length                smallint,
				@Entlevels					  smallint,
				@EntDelimiter				  Nvarchar(1),
				@retWhere					  Nvarchar(200),
				@RetCompStr 				  Nvarchar(200),
				@level						  smallint

	if (@Entity = 'labcd')
		begin
	
				select 	@Ent1Start 	= 	LC1Start,
							@Ent1Length =	LC1Length,
							@Ent2Start 	=	LC2Start,
							@Ent2Length =	LC2Length,
							@Ent3Start 	=	LC3Start,
							@Ent3Length =	LC3Length,
							@Ent4Start 	=	LC4Start,
							@Ent4Length =	LC4Length,
							@Ent5Start =	LC5Start,
							@Ent5Length =	LC5Length,
							@EntLevels = 	LCLevels,
							@EntDelimiter = LCDelimiter from CFGFormat
			set @level = convert(integer, @oldvalue2)
		end
	if (@Entity = 'orgSubcode')
		begin
	
			select 	@Ent1Start 	= 	Org1Start,
						@Ent1Length =	Org1Length,
						@Ent2Start 	=	Org2Start,
						@Ent2Length =	Org2Length,
						@Ent3Start 	=	Org3Start,
						@Ent3Length =	Org3Length,
						@Ent4Start 	=	Org4Start,
						@Ent4Length =	Org4Length,
						@Ent5Start =	Org5Start,
						@Ent5Length =	Org5Length,
						@EntLevels = 	OrgLevels,
						@EntDelimiter = OrgDelimiter from CFGFormat

			set @level = convert(integer, @oldvalue2)
			if (@level = 1)
			  begin
				declare cvtCursor cursor scroll static for
				select TableName, ColumnName from keyConvertDriver where Entity in (@Entity,N'Org')
					and case @cvtType when 0 then skipValue else 'N' end = 'N' --skiping some tables/columns on value change
					order by ColumnName,TableName
				end
			else
				begin
					declare cvtCursor cursor scroll static for
					select TableName, ColumnName from keyConvertDriver where Entity = N'Org' 
						and case @cvtType when 0 then skipValue else 'N' end = 'N' --skiping some tables/columns on value change
						order by ColumnName,TableName
				end			
		end
	else
		begin
			declare cvtCursor cursor scroll static for
			select TableName, ColumnName from keyConvertDriver where Entity = @Entity 
				and case @cvtType when 0 then skipValue else 'N' end = 'N' --skiping some tables/columns on value change
				order by ColumnName,TableName
		end
	open cvtCursor

	--fetch next from cvtCursor  into
	--@TableName,
	--@ColumnName


	if (@ConstrFlag = 0 or @ConstrFlag = 1)
		begin
			if (@Diag <> 0)
			  Print 'Removing Constraints'
			fetch first  from cvtCursor  into
			@TableName,
			@ColumnName
			While (@@Fetch_Status = 0)
			begin
				execute('alter table ' + @TableName + ' nocheck constraint all')
				fetch next from cvtCursor into
				@TableName,
				@ColumnName
				--set @CvtFetch = @@Fetch_Status			
			end
		end
	if (@ConstrFlag = 0 or @ConstrFlag = 3)
		begin
			fetch first  from cvtCursor  into
			@TableName,
			@ColumnName
			While (@@FETCH_STATUS = 0 )
			begin
				if (@Diag <> 0)
					Print 'Converting '  + @TableName
		/*@ColumnName 	varchar(50),
		@cvtType			integer, -- 0 oldVal -> NewVal; 1 --Length Change; 2 --Remove leading zeros
		@Length 			integer,
		@RetValue 		varchar(200) output,
		@Delim1			varchar(1) = '',
		@DelimPos1  	integer = 0,
		@Delim2			varchar(1) = '',
		@DelimPos2  	integer = 0,
		@OldDelim1		varchar(1) = '',
		@OldDelimPos1  integer = 0,
		@OldDelim2		varchar(1) = '',
		@OldDelimPos2  integer = 0,
		@OldValue		varchar(20) = '',
		@NewValue		varchar(20) = ''*/
				if (@ColumnName <> @LastColumnName)
					begin
						if (@Diag <> 0)
							begin
								print '@NewValue = ' + isNull(@NewValue,'') + ' @NewValue2= ' + isNull(@NewValue2,'') + ' @NewValue3= ' + isNull(@NewValue3,'')
								print '@OldValue = ' + isNull(@OldValue,'') + ' @OldValue2= ' + isNull(@OldValue2,'') + ' @OldValue3= ' + isNull(@OldValue3,'')
								print '@Length = ' + convert(Nvarchar,@length) + '@ColumnName =' + @ColumnName + '@DelimPos1=' + convert(Nvarchar,@DelimPos1)
							end
						if (@Entity = 'labcd' or @Entity='orgSubCode')
							begin
								exec KeyNewValLabcd
									@ColumnName 	=@ColumnName,
									@RetValue 		=@UpdateValue output,
									@retWhere 		=@retWhere output,
									@level 			=@level,
									@NewValue 		=@NewValue,
									@OldValue 		=@OldValue,
									@LC1Start    	=@Ent1Start,
									@LC1Length   	=@Ent1Length,
									@LC2Start    	=@Ent2Start,
									@LC2Length   	=@Ent2Length,
									@LC3Start    	=@Ent3Start,
									@LC3Length   	=@Ent3Length,
									@LC4Start    	=@Ent4Start,
									@LC4Length   	=@Ent4Length,
									@LC5Start    	=@Ent5Start,
									@LC5Length   	=@Ent5Length,
									@LClevels	 	=@EntLevels,
									@Delimiter 		=@EntDelimiter
							end
						else
							begin
								exec KeyNewVal @ColumnName=@ColumnName,
													@cvtType=@cvtType,
													@Length =@Length,
													@ChangeSide = @ChangeSide,
													@RetValue = @UpdateValue output,
													@OldValue=@OldValue,
													@OldValue2=@OldValue2,
													@OldValue3=@OldValue3,											
													@NewValue=@NewValue,
													@NewValue2=@NewValue2,
													@NewValue3=@NewValue3,
													@Delim1=@Delim1,
													@DelimPos1=@DelimPos1,
													@Delim2=@Delim2,
													@DelimPos2=@DelimPos2,
													@OldDelim1=@OldDelim1,
													@OldDelimPos1=@OldDelimPos1,
													@OldDelim2=@OldDelim2,
													@OldDelimPos2=@OldDelimPos2
							end
						set @LastColumnName = @ColumnName
					end

				set @newValue2 = @saveNewValue2
				set @newValue3 = @saveNewValue3
				set @tmpUpdateValue = @UpdateValue
				--exception for disable phase/task and resource planning, since it stores strange and unusual values in what are phase/task fields
				if (@TableName in ('RPTask', 'RPAssignment', 'RPExpense', 'RPConsultant', 'RPUnit') and (@OldValue2 = '.task.' or @OldValue3 = '.task.'))
				  begin
					if (@OldValue2 = '.task.' )
						set @NewValue2 = '<none>'
					if (@OldValue3 = '.task.')
						set @NewValue3 = '<none>'
						exec KeyNewVal @ColumnName=@ColumnName,
											@cvtType=@cvtType,
											@Length =@Length,
											@ChangeSide = @ChangeSide,
											@RetValue = @UpdateValue output,
											@OldValue=@OldValue,
											@OldValue2=@OldValue2,
											@OldValue3=@OldValue3,											
											@NewValue=@NewValue,
											@NewValue2=@NewValue2,
											@NewValue3=@NewValue3,
											@Delim1=@Delim1,
											@DelimPos1=@DelimPos1,
											@Delim2=@Delim2,
											@DelimPos2=@DelimPos2,
											@OldDelim1=@OldDelim1,
											@OldDelimPos1=@OldDelimPos1,
											@OldDelim2=@OldDelim2,
											@OldDelimPos2=@OldDelimPos2
				  end --RP exception

				set @SQLStr = 'Update ' + @TableName + ' Set ' + @ColumnName + ' = ' + @UpdateValue

				--JYC 7.6 Employee expense report approval, run this only for key format
				if @CvtType <> 0 and (@entity = 'Employee' and @TableName = 'ApprovalItem' and @ColumnName = 'ApplicationKey') 
					AND EXISTS(SELECT 'x' FROM ApprovalItem WHERE ApplicationID = 'TIME')
					begin
							--Master records
							exec KeyNewVal @ColumnName='(SUBSTRING(ApplicationKey,1,CHARINDEX(''|'',ApplicationKey,1)-1))',
											@cvtType=@cvtType,
											@Length = @Length,
											@ChangeSide = @ChangeSide,
											@RetValue= @UpdateValue output,
											@OldDelim1=@OldDelim1,
											@OldDelimPos1 = @OldDelimPos1,
											@delim1=@delim1,
											@DelimPos1=@DelimPos1

							set @SQLStr = 'UPDATE ApprovalItem '
							+ 'SET ApplicationKey = REPLACE(ApplicationKey,SUBSTRING(ApplicationKey,1,CHARINDEX(''|'',ApplicationKey,1)-1)+''|'','+@UpdateValue+'+''|'') '
							+ 'WHERE ApplicationID = ''TIME'' AND ApprovalLevel = ''Master'' '

							--Time Detail records
							exec KeyNewVal @ColumnName='(SUBSTRING(ApplicationKey,17,CHARINDEX(''|'',ApplicationKey,17)-17))',
											@cvtType=@cvtType,
											@Length = @Length,
											@ChangeSide = @ChangeSide,
											@RetValue= @UpdateValue output,
											@OldDelim1=@OldDelim1,
											@OldDelimPos1 = @OldDelimPos1,
											@delim1=@delim1,
											@DelimPos1=@DelimPos1

							set @SQLStr3 = 'UPDATE ApprovalItem '
							+ 'SET ApplicationKey = REPLACE(ApplicationKey,SUBSTRING(ApplicationKey,17,CHARINDEX(''|'',ApplicationKey,17)-17)+''|'','+@UpdateValue+'+''|'') '
							+ 'WHERE ApplicationID = ''TIME'' AND ApprovalLevel = ''Detail'''

					end

				--This is for Key Format - Employee utility.
				--JYC update ModUser/ModDate as well for  Project and Employee
				if (@entity = 'Employee' and @TableName = 'EMMain' and @ColumnName = 'Employee') 
					or (@entity = 'WBS1' and @TableName = 'PR' and @ColumnName = 'WBS1')
					or (@Entity = 'Client' and @TableName = 'Clendor' and @ColumnName = 'Client')
					or (@Entity = 'Vendor' and @TableName = 'Clendor' and @ColumnName = 'Vendor')
					begin
							set @SQLStr = @SQLStr + ', ModUser = ''' + @UserName + ''', ModDate = GETUTCDATE() '
					end
				
				if @entity = 'Employee' and @TableName = 'EMMain' and @ColumnName = 'Employee'
					begin
						--If Talent Management Web Service is enabled, update TalentModDate to track change to Employee field.
						if (Select EnableWebService FROM CFGIntegrationWS WHERE Type = 'Talent') = 'Y'
							set @SQLStr = @SQLStr + ', TalentModDate = GETUTCDATE() '
						--If TrafficLIVE Web Service is enabled, update TLSyncModDate to track change to Employee field.
						if (Select EnableWebService FROM CFGIntegrationWS WHERE Type = 'TL') = 'Y'
							set @SQLStr = @SQLStr + ', TLSyncModDate = GETUTCDATE() '
					end

				if @entity = 'WBS1' and @TableName = 'PR' and @ColumnName = 'WBS1'
					begin
						--If TrafficLIVE Web Service is enabled, update TLSyncModDate to track change to PR field.
						if (Select EnableWebService FROM CFGIntegrationWS WHERE Type = 'TL') = 'Y'
							set @SQLStr = @SQLStr + ', TLSyncModDate = GETUTCDATE() '
					end

				set @UpdateValue = @tmpUpdateValue
				set @AddlWhere = ' AND ' + @ColumnName + ' <> N'' '''

				if (@OldValue2 = '.task.' or @OldValue3 = '.task.') --This is for WBSDisable
				  begin
 				  	set @SQLStr = replace(@SQLStr,'= ''.task.''','<> '' ''')
 				  	set @SQLStr = replace(@SQLStr,'= N''.task.''','<> N'' ''')
				  end 
			
				if (@CvtType <> 0)
					begin
						if (@entity = 'Employee' and @TableName = 'ApprovalItem' and @ColumnName = 'ApplicationKey') 
							--JYC 7.6 Employee expense report approval, run this only for key format
							print ''
						else
							set @SQLStr = @SQLStr + ' Where ' + @ColumnName + ' is Not null' + ' ' + @AddlWhere

						if (@entity = 'WBS1' or @entity = 'WBS2' or @entity = 'WBS3')
							set @SQLStr = @SQLStr + ' AND ' + @ColumnName +  ' not in (N''<none>'',N''~'',N''<D>'',N''<R>'',N''<O>'')'
                        if (@entity = 'refno')
                            set @SQLStr = @SQLStr + ' AND ' + @ColumnName +  ' not in (''<Draft>'', ''<State>'', ''Auto'')'
--						if (@entity = 'refno')
--							set @SQLStr = @SQLStr + ' AND IsNumeric(' + @ColumnName + ') = 1'							
					end
				if (@entity = 'labcd' or @entity = 'orgSubcode')
						set @SqlStr = @SqlStr + ' where ' + @RetWhere + ' and ' + @ColumnName + ' is Not null' + ' AND ' + @ColumnName + ' not in (N''<none>'')' + @addlWhere

				if (@entity = 'orgSubcode' and (@columnName = 'Company' or @columnName = 'LinkCompany' or @columnName = 'ICBillingInvoiceCompany' or @columnName = 'ICBillingVoucherCompany' or @columnName = 'HomeCompany' or @columnName = 'EmployeeCompany' or @columnName = 'DefaultCompany'))
					set @SQLStr = 'Update ' + @TableName + ' Set ' + @ColumnName + ' = N''' + @newValue + ''' where ' + @ColumnName + ' = N''' + @oldValue + ''''

				if (@CvtType = 0)
				begin
					--If Talent Management Web Service is enabled, update TalentModDate to track changes relevant to Talent Management Integration.
					if (@TableName = 'EMMain' and @ColumnName = 'HomeCompany') or (@TableName = 'EMCompany' and (@ColumnName = 'Supervisor' or @ColumnName = 'Org'))
						if (Select EnableWebService From CFGIntegrationWS Where Type = 'Talent') = 'Y'
							--We update EM view since we are only tracking changes to home company records.
							if @Entity = 'orgSubcode' and @ColumnName = 'Org'
								set @SqlStr = 'Update EM Set TalentModDate = GETUTCDATE() Where ' + @RetWhere + ' And ' + @ColumnName + ' Is Not Null ' + @addlWhere + '; ' + @SqlStr
							else
								set @SqlStr = @SqlStr + '; Update EM Set TalentModDate = GETUTCDATE() Where ' + @ColumnName + ' = N''' + @newValue + ''''
				end

				/*Start FW_Attachments*/
				if (@entity = 'WBS1' or @entity = 'WBS2' or @entity = 'WBS3') And (@TableName = 'FW_Attachments')
					set @SQLStr = @SQLStr + ' AND Application =''Projects'' '
				if (@entity = 'Contact') And (@TableName = 'FW_Attachments')
					set @SQLStr = @SQLStr + ' AND Application =''Contacts'' '
				if (@entity = 'Client') And (@TableName = 'FW_Attachments')
					set @SQLStr = @SQLStr + ' AND Application =''Firms'' '
				if (@entity = 'Employee') And (@TableName = 'FW_Attachments')
					set @SQLStr = @SQLStr + ' AND Application =''Employees'' '
				if (@entity = 'Vendor') And (@TableName = 'FW_Attachments')
					set @SQLStr = @SQLStr + ' AND Application =''Vendors'' '
				/*End FW_Attachments*/

				if (@entity = 'Employee') And (@TableName = 'AnalysisCubesKPIOverride')
					set @SQLStr = @SQLStr + ' AND exists(select AnalysisCubesKPI.PKey from AnalysisCubesKPI where AnalysisCubesKPI.PKey=AnalysisCubesKPIOverride.PKey and AnalysisCubesKPI.Dimension in (''PRPrincipal'',''PRProjectManager'',''PRSupervisor'',''PlanPrincipal'',''PlanProjectManager'',''EMEmployee'',''OppPrincipal'',''OPPProjectManager'')) '

				--JYC 7.6 Employee expense report approval, run this only for key format
				if @CvtType <> 0 and (@entity = 'Employee' and @TableName = 'ApprovalItem' and @ColumnName = 'ApplicationKey') 
					AND EXISTS(SELECT 'x' FROM ApprovalItem WHERE ApplicationID = 'TIME')
					begin
						--Master records
						if (@Diag <> 0)
							print  @SQLStr
						execute(@SQLStr)
						set @ErrorNum = @@Error
						if (@ErrorNum <> 0)
							begin
								if (@Diag <> 0)
									print 'Error :' + convert(Nvarchar,@ErrorNum)
								if (@NeedTransaction = 'Y')
									Rollback Transaction
								close cvtCursor
								deallocate cvtCursor
								select @ErrMsg = ' SQL:' + @SqlStr
								set nocount off
								return(@ErrorNum)
							end

						--Expnese Detail records
						if (@Diag <> 0)
							print  @SQLStr2
						execute(@SQLStr2)
						set @ErrorNum = @@Error
						if (@ErrorNum <> 0)
							begin
								if (@Diag <> 0)
									print 'Error :' + convert(Nvarchar,@ErrorNum)
								if (@NeedTransaction = 'Y')
									Rollback Transaction
								close cvtCursor
								deallocate cvtCursor
								select @ErrMsg = ' SQL:' + @SqlStr2
								set nocount off
								return(@ErrorNum)
							end

						--Time Detail records
						if (@Diag <> 0)
							print  @SQLStr3
						execute(@SQLStr3)
						set @ErrorNum = @@Error
						if (@ErrorNum <> 0)
							begin
								if (@Diag <> 0)
									print 'Error :' + convert(Nvarchar,@ErrorNum)
								if (@NeedTransaction = 'Y')
									Rollback Transaction
								close cvtCursor
								deallocate cvtCursor
								select @ErrMsg = ' SQL:' + @SqlStr3
								set nocount off
								return(@ErrorNum)
							end

					end
				else
					begin

				if (@Diag <> 0)
					print  @SQLStr
				execute(@SQLStr)
				set @ErrorNum = @@Error
				if (@ErrorNum <> 0)
					begin
						if (@Diag <> 0)
							print 'Error :' + convert(Nvarchar,@ErrorNum)
						if (@NeedTransaction = 'Y')
							Rollback Transaction
						close cvtCursor
						deallocate cvtCursor
						select @ErrMsg = ' SQL:' + @SqlStr
						set nocount off
						return(@ErrorNum)
					end

					end	

				fetch next from cvtCursor into
				@TableName,
				@ColumnName
			end --While
		end --If Update
	if (@ConstrFlag = 0 or @ConstrFlag = 2)
		begin
			if (@Diag <> 0)
			  Print 'Applying Constraints '
			fetch first  from cvtCursor  into
			@TableName,
			@ColumnName
			While (@@FETCH_STATUS = 0)
			begin
				execute('alter table ' + @TableName + ' check constraint all')
				fetch next from cvtCursor into
				@TableName,
				@ColumnName
			end
		end
	if (@Diag <> 0)
		print 'Transactions: ' + convert(Nvarchar,@@Trancount)

-- Add by DP 4.0
	if (@cvtType <> 0) 
	begin
		DECLARE @LastPostSeq integer
		DECLARE @strCompany Nvarchar(14)
		DECLARE @strComment Nvarchar(255)
	
		if (@Period = 0)
			set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
		if (@Period > 0)
		begin
			set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
			if (IsNull(@LastPostSeq, 0) = 0)
			  Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

			set @strCompany = dbo.GetActiveCompany()
			if (@strCompany = '')
				set @strCompany = (select MIN(company) from CFGMainData)
	
			set @strComment = @custlabel + ' Key Convert Format (Length = ' + convert(Nvarchar, @Length) + ', Add/Remove from ' + @ChangeSide
	
			if (@Entity <> 'RefNo')
				set @strComment =  @strComment + ', LeadZeros = ' + case when @CvtType = 1 then 'Y' else 'N' end
			if (@DelimPos1 > 0)
				set @strComment =  @strComment + ', Delimiter' + case when @Entity = 'WBS1' then ' 1' else '' end + ' = ' + @Delim1 + ', Delimiter' + case when @Entity = 'WBS1' then ' 1' else '' end + ' Position = ' + convert(Nvarchar, @DelimPos1)

			if (@DelimPos2 > 0)
				set @strComment =  @strComment + ', Delimiter 2 = ' + @Delim2 + ', Delimiter 2 Position = ' + convert(Nvarchar, @DelimPos2)
			set @strComment =  @strComment + ')'
	
			Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
			Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, QueueName, PaymentProcessRun) 
				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),'KF','Y',  @strComment, @strCompany, @Entity, @custlabel + ' Key Convert Format', @custlabel);
	
		end
	end
--
    if @CvtType <> 0 and (@entity = 'RefNo')
        begin
           UPDATE FW_CFGSystem SET LastVoucherAP = (Select IsNull(Max(Voucher),0) from VO where IsNumeric(Voucher) = 1)
           Where RequireVoucherNumbers = 'N'
        end



	if (@NeedTransaction = 'Y')
		commit Transaction
	if (@Diag <> 0)
		print 'Transactions: ' + convert(Nvarchar,@@Trancount)
	close cvtCursor
	deallocate cvtCursor
	set nocount off
	return (0) -- no error
end
GO
