SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Save_Stage_Flows]
	@fieldsSQL			Nvarchar(max),
	@newValuesSql		Nvarchar(max),
	@VISION_LANGUAGE	varchar(10)
AS
BEGIN
	/*
	DeltekStoredProc_CCG_EI_Config_Save_Stage_Flows
		@fieldsSQL = 'StageFlow, StageFlowDescription, Status, Stage, StageOrder',
		@newValuesSql = '(''Approval Required'', ''eApproval Required'', ''A'', ''Accounting'', 1), (''Approval Required'', ''eApproval Required'', ''A'', ''Review'', 2), (''Approval Required'', ''eApproval Required'', ''A'', ''Re-Review'', 3), (''Approval Required'', ''eApproval Required'', ''A'', ''Invoice as is'', 4), (''Approval Required'', ''eApproval Required'', ''A'', ''Invoice as noted'', 5), (''Approval Required'', ''eApproval Required'', ''A'', ''Do not bill this month'', 6), (''Approval Required'', ''eApproval Required'', ''A'', ''Revise and return'', 7), (''Approval Required'', ''eApproval Required'', ''A'', ''Approved'', 8), (''Approval Required'', ''eApproval Required'', ''A'', ''Approved - DNI'', 9), (''Approval Required'', ''eApproval Required'', ''A'', ''Write-off approval'', 10), (''Approval Required'', ''eApproval Required'', ''A'', ''Write-off approved'', 11), (''Approval Required'', ''eApproval Required'', ''A'', ''Final'', 12), (''DLR Group'', ''DLR Group'', ''A'', ''Accounting'', 1), (''DLR Group'', ''DLR Group'', ''A'', ''PM Input'', 2), (''DLR Group'', ''DLR Group'', ''A'', ''Invoice as is'', 3), (''DLR Group'', ''DLR Group'', ''A'', ''Invoice as noted'', 4), (''DLR Group'', ''DLR Group'', ''A'', ''Do not bill this month'', 5), (''DLR Group'', ''DLR Group'', ''A'', ''Send Draft'', 6), (''DLR Group'', ''DLR Group'', ''A'', ''Ready For Final'', 7), (''DLR Group'', ''DLR Group'', ''A'', ''Final'', 8), (''No Approval Required'', ''eNo Approval Required'', ''A'', ''Accounting'', 1), (''No Approval Required'', ''eNo Approval Required'', ''A'', ''Review'', 2), (''No Approval Required'', ''eNo Approval Required'', ''A'', ''Re-Review'', 3), (''No Approval Required'', ''eNo Approval Required'', ''A'', ''Invoice as is'', 4), (''No Approval Required'', ''eNo Approval Required'', ''A'', ''Invoice as noted'', 5), (''No Approval Required'', ''eNo Approval Required'', ''A'', ''Do not bill this month'', 6), (''No Approval Required'', ''eNo Approval Required'', ''A'', ''Revise and return'', 7), (''No Approval Required'', ''eNo Approval Required'', ''A'', ''Write-off approval'', 8), (''No Approval Required'', ''eNo Approval Required'', ''A'', ''Write-off approved'', 9), (''No Approval Required'', ''eNo Approval Required'', ''A'', ''Final'', 10)',
		@VISION_LANGUAGE = 'en-US'
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		varchar(max);
	DECLARE @safeSql	int;			-- INT value indicates which parameter failed the security check

	-- Ensure that our inputs are safe (preventing SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<CSV>', @fieldsSQL);
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<INSERT VALUES>', @newValuesSql) * 2;
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @VISION_LANGUAGE) * 4;
	IF @safeSql < 7 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS varchar(4));
		RETURN;
	END;

	BEGIN TRANSACTION;

	SET @sSQL = N'
		MERGE CCG_EI_ConfigStageFlows WITH (HOLDLOCK) AS Target
		USING (VALUES ' + @newValuesSql + N'
			) As Source (' + @fieldsSQL + N')
			ON Target.StageFlow = Source.StageFlow
				AND Target.Stage = Source.Stage
		WHEN MATCHED THEN
			UPDATE SET
				[StageOrder]			= Source.[StageOrder],
				[StageFlowDescription]	= Source.[StageFlowDescription],
				[Status]				= Source.[Status]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (' + @fieldsSQL + N')
			VALUES (Source.' + REPLACE(@fieldsSQL, N', ', N', Source.') + N')
		WHEN NOT MATCHED BY SOURCE THEN DELETE;';
	--PRINT (@sSQL);
	EXEC (@sSQL);

	-- Update the multilingual descriptions table
	SET @sSQL = N'
		MERGE CCG_EI_ConfigStageFlowsDescriptions WITH (HOLDLOCK) AS Target
		USING CCG_EI_ConfigStageFlows AS Source
			ON Target.StageFlow = Source.StageFlow
				AND Target.Stage = Source.Stage
		WHEN MATCHED AND Target.UICultureName = '''+@VISION_LANGUAGE+N''' THEN
			UPDATE SET Target.StageFlowDescription = Source.StageFlowDescription
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (StageFlow, Stage, UICultureName, StageFlowDescription)
			VALUES (Source.StageFlow, Source.Stage, '''+@VISION_LANGUAGE+N''', Source.StageFlowDescription)
		WHEN NOT MATCHED BY SOURCE THEN DELETE;';
	EXEC (@sSQL);

	COMMIT TRANSACTION;
END;
GO
