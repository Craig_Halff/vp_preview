SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ngRPShiftTasks]
  @strRowID Nvarchar(255),
  @strFromStartDate varchar(8),
  @strFromEndDate varchar(8),
  @strToStartDate varchar(8),
  @strToEndDate varchar(8),
  @bitShiftAll bit, /* This flag is applicable only for ShiftMode = {1, 3}. 0 = Shift using the "From" range, 1 = Shift the entire Assignment Row */
  @bitFixStartDate bit /* This flag is applicable only for ShiftMode = {1, 3}. 1 = Hold "To" StartDate fixed, 0 = Hold "To" EndDate fixed */
AS

BEGIN -- Procedure ngRPShiftTasks

  SET NOCOUNT ON
  
  DECLARE @dtFromStartDate datetime
  DECLARE @dtFromEndDate datetime

  DECLARE @dtToStartDate datetime
  DECLARE @dtToEndDate datetime

  DECLARE @dtASGStartDate datetime
  DECLARE @dtASGEndDate datetime
  DECLARE @dtTSKStartDate datetime
  DECLARE @dtTSKEndDate datetime
  DECLARE @dtOvrdFromStartDate datetime
  DECLARE @dtOvrdFromEndDate datetime
   
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strAssignmentID varchar(32)
  DECLARE @strResourceID Nvarchar(20)
  DECLARE @strGenericResourceID Nvarchar(20)
  DECLARE @strResourceType varchar(1)
  DECLARE @strIDPrefix Nvarchar(255)
  DECLARE @strUserName Nvarchar(32)

  DECLARE @strOvrdFromStartDate varchar(8)
  DECLARE @strOvrdFromEndDate varchar(8)

  DECLARE @decSumHrs decimal(19,4) = 0

  -- Declare Temp tables.
  
  DECLARE @tabAssignment TABLE (
    AssignmentID varchar(32) COLLATE database_default,
    StartDate datetime,
    EndDate datetime
    UNIQUE(AssignmentID)
  )

  DECLARE @tabTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    ToStartDate datetime,
    ToEndDate datetime,
    SumHrs decimal(19,4),
    MinTPDDate datetime,
    MaxTPDDate datetime
    UNIQUE(PlanID, TaskID)
  )

  DECLARE @tabTPD TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    SumHrs decimal(19,4),
    MinTPDDate datetime,
    MaxTPDDate datetime
    UNIQUE(PlanID, OutlineNumber)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

  -- Set Dates

  SET @dtFromStartDate = 
    CASE 
      WHEN DATALENGTH(@strFromStartDate) = 0 
      THEN NULL
      ELSE CONVERT(datetime, @strFromStartDate)
    END

  SET @dtFromEndDate = 
    CASE
      WHEN DATALENGTH(@strFromEndDate) = 0
      THEN NULL
      ELSE CONVERT(datetime, @strFromEndDate)
    END

  SET @dtToStartDate = 
    CASE
      WHEN DATALENGTH(@strToStartDate) = 0
      THEN NULL
      ELSE CONVERT(datetime, @strToStartDate)
    END

  SET @dtToEndDate = 
    CASE
    WHEN DATALENGTH(@strToEndDate) = 0
    THEN NULL
    ELSE CONVERT(datetime, @strToEndDate)
  END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. E~<RPAssignment.ResourceID>|<RPTask.TaskID>
  --   2. G~<RPAssignment.GenericResourceID>|<RPTask.TaskID>

  SET @strIDPrefix = LEFT(@strRowID, CHARINDEX('|', @strRowID) - 1)
  SET @strResourceType = SUBSTRING(@strIDPrefix, 1, 1)

  IF (@strResourceType = 'E')
    BEGIN
      SET @strResourceID = REPLACE(@strIDPrefix, 'E~', '')
      SET @strGenericResourceID = NULL
    END
  ELSE IF (@strResourceType = 'G')
    BEGIN
      SET @strGenericResourceID = REPLACE(@strIDPrefix, 'G~', '')
      SET @strResourceID = NULL
    END
  ELSE
    BEGIN
      SET @strResourceID = NULL
      SET @strGenericResourceID = NULL
    END

  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

  -- Get various parameters.

  SELECT 
    @strCompany = P.Company,
    @strPlanID = T.PlanID,
    @dtTSKStartDate = T.StartDate,
    @dtTSKEndDate = T.EndDate
    FROM RPTask AS T
      INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID
    WHERE T.TaskID = @strTaskID

  -- Determine Assignment Start/End Dates.
  
  IF (DATALENGTH(@strIDPrefix) > 0)
    BEGIN

      SELECT
        @dtASGStartDate = A.StartDate,
        @dtASGEndDate = A.EndDate
        FROM RPAssignment AS A
          INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
          INNER JOIN RPTask AS PT ON A.PlanID = PT.PlanID 
        WHERE
          A.PlanID = @strPlanID AND
          PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
          AT.OutlineNumber LIKE PT.OutlineNumber + '%' AND
          (ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') 
	          AND ISNULL(A.GenericResourceID, '|') = ISNULL( @strGenericResourceID, '|'))
    
    END /* END IF (DATALENGTH(@strIDPrefix) > 0) */

  -- Adjusting "From" Dates.
  -- When "Shift All", the caller does not pass in "From" Dates 
  -- and the "From" Dates are derived from either the current Task or Assignment.

  SELECT
    @strOvrdFromStartDate = 
      CASE
        WHEN @bitShiftAll = 1 
        THEN 
          CASE 
            WHEN DATALENGTH(@strIDPrefix) = 0
            THEN CONVERT(VARCHAR(8), @dtTSKStartDate, 112)
            ELSE CONVERT(VARCHAR(8), @dtASGStartDate, 112)
          END
        ELSE @strFromStartDate
      END,
    @strOvrdFromEndDate = 
      CASE
        WHEN @bitShiftAll = 1 
        THEN 
          CASE 
            WHEN DATALENGTH(@strIDPrefix) = 0
            THEN CONVERT(VARCHAR(8), @dtTSKEndDate, 112)
            ELSE CONVERT(VARCHAR(8), @dtASGEndDate, 112)
          END
        ELSE @strFromEndDate
      END

  SELECT
    @dtOvrdFromStartDate = CONVERT(VARCHAR(8), @strOvrdFromStartDate, 112),
    @dtOvrdFromEndDate = CONVERT(VARCHAR(8), @strOvrdFromEndDate, 112)
      
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- I must use a temp table here otherwise the cursor will get confused when Assignment Start/End dates are changed.

  INSERT @tabAssignment(
    AssignmentID,
    StartDate,
    EndDate
  )
    SELECT DISTINCT
      A.AssignmentID, 
      A.StartDate,
      A.EndDate
    FROM RPAssignment AS A
      INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
      INNER JOIN RPTask AS PT ON A.PlanID = PT.PlanID 
    WHERE
      A.PlanID = @strPlanID AND
      PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
      AT.OutlineNumber LIKE PT.OutlineNumber + '%' AND
      ((ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') 
				AND ISNULL(A.GenericResourceID, '|') = ISNULL( @strGenericResourceID, '|'))
				OR (@strResourceID IS NULL AND @strGenericResourceID IS NULL))

  DECLARE csr20160203_tabAssignment CURSOR LOCAL FAST_FORWARD FOR
    SELECT DISTINCT
      A.AssignmentID, 
      A.StartDate,
      A.EndDate
    FROM @tabAssignment AS A

  OPEN csr20160203_tabAssignment
  FETCH NEXT FROM csr20160203_tabAssignment INTO @strAssignmentID, @dtASGStartDate, @dtASGEndDate

  WHILE (@@FETCH_STATUS = 0)
    BEGIN

      EXECUTE dbo.ngRPShiftAssigment @strAssignmentID, @strOvrdFromStartDate, @strOvrdFromEndDate, @strToStartDate, @strToEndDate, @bitShiftAll, @bitFixStartDate

      FETCH NEXT FROM csr20160203_tabAssignment INTO @strAssignmentID, @dtASGStartDate, @dtASGEndDate

    END -- While

  CLOSE csr20160203_tabAssignment
  DEALLOCATE csr20160203_tabAssignment

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Change Task {StartDate, EndDate} only when user is changing "Plan Dates" at WBS Level and not at Assignment Level.
  
  IF (DATALENGTH(@strIDPrefix) = 0)
    BEGIN

      INSERT @tabTPD(
        PlanID,
        OutlineNumber,
        SumHrs,
        MinTPDDate,
        MaxTPDDate
      )
        SELECT
          TPD.PlanID AS PlanID,
          AT.OutlineNumber AS OutlineNumber,
          SUM(TPD.PeriodHrs) AS SumHrs,
          MIN(TPD.StartDate) AS MinTPDDate,
          MAX(TPD.EndDate) AS MaxTPDDate
          FROM RPPlannedLabor AS TPD
            INNER JOIN RPTask AS AT ON TPD.PlanID = AT.PlanID AND TPD.TaskID = AT.TaskID AND TPD.AssignmentID IS NOT NULL
            INNER JOIN RPTask AS PT ON AT.PlanID = PT.PlanID
          WHERE TPD.PlanID = @strPlanID AND
            PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
            AT.OutlineNumber LIKE PT.OutlineNumber + '%' 
          GROUP BY TPD.PlanID, TPD.TaskID, AT.OutlineNumber

      INSERT @tabTask(
        PlanID,
        TaskID,
        StartDate,
        EndDate,
        ToStartDate,
        ToEndDate,
        SumHrs,
        MinTPDDate,
        MaxTPDDate
      )
        SELECT
          XT.PlanID AS PlanID,
          XT.TaskID AS TaskID,
          XT.StartDate AS StartDate,
          XT.EndDate AS EndDate,
          XT.ToStartDate AS ToStartDate,
          XT.ToEndDate AS ToEndDate,
          ISNULL(SUM(TPD.SumHrs), 0) AS SumHrs,
          MIN(TPD.MinTPDDate) AS MinTPDDate,
          MAX(TPD.MinTPDDate) AS MinTPDDate
          FROM (
            SELECT 
              YT.PlanID AS PlanID,
              YT.TaskID AS TaskID,
              YT.OutlineNumber AS OutlineNumber,
              YT.StartDate AS StartDate,
              YT.EndDate AS EndDate,
              CASE 
                WHEN @bitFixStartDate = 0
                THEN
                  dbo.DLTK$AddBusinessDays(
                    @dtToEndDate, 
                    -1 * (YT.DurationWD + YT.OffsetWD + 1), 
                    @strCompany
                  )
                ELSE 
                  dbo.DLTK$AddBusinessDays(
                    @dtToStartDate, 
                    (YT.OffsetWD - 1), 
                    @strCompany
                  )
              END AS ToStartDate,
              CASE 
                WHEN @bitFixStartDate = 1
                THEN
                  dbo.DLTK$AddBusinessDays(
                    @dtToStartDate, 
                    ((YT.DurationWD - 1) + (YT.OffsetWD - 1)), 
                    @strCompany
                  )
                ELSE 
                  dbo.DLTK$AddBusinessDays(
                    @dtToEndDate, 
                    -1 * (YT.OffsetWD + 1), 
                    @strCompany
                  )
              END AS ToEndDate
              FROM (
                SELECT
                  CT.PlanID AS PlanID,
                  CT.TaskID AS TaskID,
                  CT.OutlineNumber AS OutlineNumber,
                  CT.StartDate AS StartDate,
                  CT.EndDate AS EndDate,
                  dbo.DLTK$NumWorkingDays(CT.StartDate, CT.EndDate, @strCompany) AS DurationWD,
                  CASE 
                    WHEN @bitFixStartDate = 1
                    THEN dbo.DLTK$NumWorkingDays(@dtOvrdFromStartDate, CT.StartDate, @strCompany)
                    ELSE dbo.DLTK$NumWorkingDays(@dtOvrdFromEndDate, CT.EndDate, @strCompany)
                  END AS OffsetWD
                  FROM 
                    RPTask AS PT
                      INNER JOIN RPTask AS CT ON PT.PlanID = CT.PlanID
                    WHERE
                      PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
                      CT.OutlineNumber LIKE PT.OutlineNumber + '%'   
              ) AS YT
          ) AS XT
            LEFT JOIN @tabTPD AS TPD
              ON XT.PlanID = TPD.PlanID AND TPD.OutlineNumber LIKE XT.OutlineNumber + '%'
          GROUP BY XT.PlanID, XT.TaskID, XT.StartDate, XT.EndDate, XT.ToStartDate, XT.ToEndDate

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      UPDATE ZT SET
        StartDate = 
          CASE
            WHEN ToStartDate IS NULL
            THEN
              CASE
                WHEN ZT.StartDate <= ToEndDate
                THEN ZT.StartDate
                ELSE ToEndDate
              END
            WHEN XT.SumHrs = 0 THEN ToStartDate
            WHEN XT.MinTPDDate IS NULL THEN ToStartDate
            ELSE
              CASE
                WHEN ToStartDate < XT.MinTPDDate
                THEN ToStartDate
                ELSE XT.MinTPDDate
              END
          END,
        EndDate = 
          CASE
            WHEN ToEndDate IS NULL
            THEN
              CASE
                WHEN ZT.EndDate >= ToStartDate
                THEN ZT.EndDate
                ELSE ToStartDate
              END
            WHEN XT.SumHrs = 0 THEN ToEndDate
            WHEN XT.MaxTPDDate IS NULL THEN ToEndDate
            ELSE
              CASE
                WHEN ToEndDate > XT.MaxTPDDate
                THEN ToEndDate
                ELSE XT.MaxTPDDate
              END
          END
        FROM RPTask AS ZT
          INNER JOIN @tabTask AS XT ON ZT.PlanID = XT.PlanID AND ZT.TaskID = XT.TaskID
    
    END /* END IF (DATALENGTH(@strIDPrefix) = 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     
  SET NOCOUNT OFF

END -- ngRPShiftTasks
GO
