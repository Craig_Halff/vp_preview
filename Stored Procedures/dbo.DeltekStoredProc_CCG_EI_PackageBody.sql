SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_PackageBody] ( @WBS1 nvarchar(30)= NULL, @Invoice nvarchar(12)= '')
             AS EXEC spCCG_EI_PackageBody @WBS1,@Invoice
GO
