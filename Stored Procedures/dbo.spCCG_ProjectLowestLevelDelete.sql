SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[spCCG_ProjectLowestLevelDelete] @WBS1 varchar (32), @WBS2 varchar (7), @WBS3 varchar (7)
AS
/*
Copyright 2019 (c) Central Consulting Group.   All rights reserved.
07/29/2019	David Springer
			Delete project levels from UDIC Project Lowest Levels
			Call from project DELETE workflows
*/
DECLARE @Status varchar (1)
BEGIN
SET NOCOUNT ON

   If @WBS2 = ' ' -- Project level
      Delete From UDIC_ProjectLowestLevel Where CustWBS1 = @WBS1

   If @WBS2 <> ' ' and @WBS3 = ' ' -- Phase level
      Delete From UDIC_ProjectLowestLevel Where CustWBS1 = @WBS1 and CustWBS2 = @WBS2

   If @WBS3 <> ' ' -- Task level
      Delete From UDIC_ProjectLowestLevel Where CustWBS1 = @WBS1 and CustWBS2 = @WBS2 and CustWBS3 = @WBS3


END
GO
