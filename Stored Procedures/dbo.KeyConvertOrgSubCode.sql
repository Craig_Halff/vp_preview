SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[KeyConvertOrgSubCode]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg Nvarchar(1000) output,
@UserName Nvarchar(32) = 'VisionKeyConvert', -- Add by DP 4.0
@Period integer = 0,
@EntityType Nvarchar(20)
as
begin -- Procedure
	set nocount on 
	declare @OldValue Nvarchar(20),
			  @OldValue2 Nvarchar(20)
	declare @NewValue Nvarchar(20)
	declare @OrgLevel integer
	Declare @Existing integer
	declare @Entity Nvarchar(20)
	declare @length integer
	declare @CvtType integer
	declare @KeysFetch integer
	declare @RetVal integer
	declare @diag smallint
	declare @sqlstr Nvarchar(1000)
	declare @multiCompanyEnabled Nvarchar(1)
	declare @Errmsg2 Nvarchar(1000),
			@custlabel	Nvarchar(40),
			@Custlabelplural Nvarchar(40)
	declare @message nvarchar(max) 
	set @diag = 0
	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'orgSubCode'
	set @length = 14
	set @Existing = 0
	set @KeysFetch = 0
	execute InsertKeyCvtDriver 'Org' -- populate driver table
	execute InsertKeyCvtDriver @Entity -- populate driver table

-- Add by DP 4.0
	delete from keyconvertDriver where entity = N'orgSubCode' and TableName = N'CFGOrgCodesData'
	delete from keyconvertDriver where entity = N'orgSubCode' and TableName = N'CFGOrgCodesDescriptions'
	DECLARE @OldName Nvarchar(40)
	DECLARE @NewName Nvarchar(40)
	DECLARE @PKey Nvarchar(50)
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0
--
	select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'orgLabel'
	select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'orgLabelPlural'

	declare KeysCursor cursor for
		select OldKey,OldKey2,convert(integer, OldKey2) as OrgLevel, NewKey, Pkey from KeyConvertWork where Entity = @EntityType
	open KeysCursor
	fetch next from KeysCursor into 
		@OldValue,
		@OldValue2,
		@OrgLevel,
		@NewValue, 
		@PKey

	set @KeysFetch = @@Fetch_Status
	select @multiCompanyEnabled = multiCompanyEnabled from FW_CFGSystem
While (@KeysFetch = 0)
	begin
		begin tran
-- Add by DP 4.0
		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany Nvarchar(14)
			DECLARE @RecordCount integer

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				set @RecordCount = (select count(*) from KeyConvertWork where Entity = @EntityType)

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),'KC','Y',  @custlabel + ' Sub-code Key Convert ('+rtrim(convert(Nvarchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, left(@custlabel + ' Sub-code Key Convert', 40), @custlabel + ' Sub-code');
			end

			Set @PostControlFlag = 1
		end
		set @Existing = 0
		select @Existing =  1, @NewName = Label from CFGOrgCodes where Code = @NewValue and OrgLevel = @OrgLevel
		select @OldName = Label from CFGOrgCodes where Code = @OldValue and OrgLevel = @OrgLevel
--
	
	If (@Existing = 1)
		begin
			  if (@DeleteExisting = 0)
				begin
				SET @message = dbo.GetMessage('MsgNumLevelExistConvNotContinue',@custlabel,@NewValue,@OldValue2,@custlabel,@custlabelPlural,'','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end
			  if (@orgLevel = 1 and @multiCompanyEnabled = 'Y')
				begin
				SET @message = dbo.GetMessage('MsgCvtOrgSubCodeNumExistMergComp',@custlabel,@NewValue,@OldValue2,@custlabel,@custlabelPlural,'','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end
			if (@NewValue = @OldValue)
				begin
				SET @message = dbo.GetMessage('MsgCvtLabcdAreTheSamevalue',@custlabelPlural,'','','','','','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end

				declare 	@Org1Start                 smallint,
							@Org1Length                smallint,
							@Org2Start                 smallint,
							@Org2Length                smallint,
							@Org3Start                 smallint,
							@Org3Length                smallint,
							@Org4Start                 smallint,
							@Org4Length                smallint,
							@Org5Start                 smallint,
							@Org5Length                smallint,
							@OrgLevels					  smallint,
							@retWhere					  Nvarchar(200),
							@level						  smallint,
							@UpdateValue				  Nvarchar(500),
							@UpdateValue2				  Nvarchar(500),
							@OrgDelimiter 				  Nvarchar(1)

				select 	@Org1Start 	= 	Org1Start,
							@Org1Length =	Org1Length,
							@Org2Start 	=	Org2Start,
							@Org2Length =	Org2Length,
							@Org3Start 	=	Org3Start,
							@Org3Length =	Org3Length,
							@Org4Start 	=	Org4Start,
							@Org4Length =	Org4Length,
							@Org5Start =	Org5Start,
							@Org5Length =	Org5Length,
							@OrgLevels = 	OrgLevels,
							@OrgDelimiter = OrgDelimiter
													from CFGFormat

							set @level = convert(integer, @oldvalue2)

							exec KeyNewValLabcd
								@ColumnName 	= 'Organization.Org',
								@RetValue 		= @UpdateValue output,
								@retWhere 		= @retWhere output,
								@level 			= @level,
								@NewValue 		= @NewValue,
								@OldValue 		= @OldValue,
								@LC1Start    	= @Org1Start,
								@LC1Length   	= @Org1Length,
								@LC2Start    	= @Org2Start,
								@LC2Length   	= @Org2Length,
								@LC3Start    	= @Org3Start,
								@LC3Length   	= @Org3Length,
								@LC4Start    	= @Org4Start,
								@LC4Length   	= @Org4Length,
								@LC5Start    	= @Org5Start,
								@LC5Length   	= @Org5Length,
								@LCLevels	 	= @OrgLevels,	
								@Delimiter  	= @OrgDelimiter
								

			set @updateValue2 = replace(@updateValue,'''' + @newValue + '''' ,'''' + @OldValue + '''')
   		set @SqlStr = 'update Organization set ' +
				'Organization.PlugAmtRev = Organization.PlugAmtRev + old.PlugAmtRev, ' +
				'Organization.PlugAmtDirLab = Organization.PlugAmtDirLab + old.PlugAmtDirLab, ' +
				'Organization.PlugAmtDirExp = Organization.PlugAmtDirExp + old.PlugAmtDirExp, ' +
				'Organization.PlugAmtIndLab = Organization.PlugAmtIndLab + old.PlugAmtIndLab, ' +
				'Organization.PlugAmtIndExp = Organization.PlugAmtIndExp + old.PlugAmtIndExp, ' +
				'Organization.Status =   old.Status ' +
			'from Organization old, Organization ' +
			'where Organization.org = ' + @updateValue +
					'and old.org = ' + @updateValue2

				if (@Diag <> 0)
					print @SqlStr
				execute(@Sqlstr)
   				set @SqlStr = 'Delete From Organization Where ' + @retWhere +
					' and exists (select org from Organization tmp where tmp.org = ' + @updateValue + ')'
				if (@Diag <> 0)
					print @SqlStr
				execute(@Sqlstr)

-- Added by DP
   		set @SqlStr = 'delete FirmOrgAssociations from FirmOrgAssociations Organization where org = ' + @updateValue2 +
			' and exists (select org from FirmOrgAssociations tmp where tmp.firmID = Organization.firmID and tmp.org = ' + @updateValue + ')'
		if (@Diag <> 0)
			print @SqlStr
		execute(@Sqlstr)
--

		END --Existing
	else 
		Begin
-- Add by DP 4.0
			select * into #TempKeyConvert from CFGOrgCodesData where Code = @OldValue and OrgLevel = @OrgLevel
			Update #TempKeyConvert set Code=@NewValue
			Insert CFGOrgCodesData select * from #TempKeyConvert
			Drop Table #TempKeyConvert

			select * into #TempKeyConvert1 from CFGOrgCodesDescriptions where Code = @OldValue and OrgLevel = @OrgLevel
			Update #TempKeyConvert1 set Code=@NewValue
			Insert CFGOrgCodesDescriptions select * from #TempKeyConvert1
			Drop Table #TempKeyConvert1

			set @NewName = @OldName
		end

		exec @RetVal = KeyCvt @Entity = @Entity,
					 @CvtType = @CvtType,
					 @Length = 14,
					 @ErrMsg = @ErrMsg2 output,
					 @Diag = @Diag,
					 @NewValue = @NewValue,
					 @OldValue = @OldValue,
					 @Oldvalue2 = @OldValue2,
					 @ConstrFlag = 3 -- UpdateTables
					 
-- Changed to use the tables instead of the cfgOrgCodes vew DUG 8/21/08
		delete from CFGOrgCodesData where code = @oldValue and OrgLevel = @OrgLevel
		delete from CFGOrgCodesDescriptions where code = @oldValue and OrgLevel = @OrgLevel

--
		if (@RetVal = 0)
			begin
-- Add by DP 4.0
				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq, OldKey2)
					select PKey, Entity, OldKey, NewKey, @OldName, @NewName, @Period, @LastPostSeq, OldKey2 from keyConvertWork where Pkey = @PKey
				end
--
				delete from keyConvertWork where current of keysCursor
				commit Transaction
			end
		else
			begin
				set @ErrMsg = @ErrMsg + @ErrMsg2
				rollback transaction
			end
	fetch next from KeysCursor into 
		@OldValue,
		@OldValue2,
		@OrgLevel,
		@NewValue, 
		@PKey

	set @KeysFetch = @@Fetch_Status
	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	return(0)
End -- Procedure


GO
