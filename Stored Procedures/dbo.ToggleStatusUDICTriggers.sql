SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ToggleStatusUDICTriggers]
AS

BEGIN -- Procedure ToggleStatusUDICTriggers

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to enable or disable Audit Triggers for UDIC tables 
-- based on AuditingEnabled flags in FW_UDICData.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON
  
  BEGIN TRANSACTION
    
  DECLARE @strSQL VARCHAR(MAX)
  DECLARE @strSysAuditingEnabled AS VARCHAR(1)
  
  SELECT @strSysAuditingEnabled = AuditingEnabled FROM FW_CFGSystem
  
  -- Constructing a SQL to Enable/Disable Triggers based on AuditingEnabled flags in FW_UDICData.
  
  SET @strSQL = NULL
  
  SELECT @strSQL = COALESCE(@strSQL + '; ', '') + 
    CASE WHEN @strSysAuditingEnabled = 'Y' AND AuditingEnabled = 'Y' 
         THEN 'ENABLE TRIGGER ' 
         ELSE 'DISABLE TRIGGER ' END +
    SchemaName + '.' + TriggerName +
    ' ON ' + TableName
    FROM
      (SELECT  
         TR.name AS TriggerName,  
         TB.Name AS TableName,  
         S.Name AS SchemaName,  
         FU.AuditingEnabled AS AuditingEnabled  
         FROM sys.Triggers AS TR  
           INNER JOIN sys.objects AS TB ON TR.Parent_ID = TB.OBJECT_ID AND TR.Name LIKE 'VisionAudit[_]%' AND TR.Type = 'TR' AND TB.Name LIKE '%UDIC_%'
		   INNER JOIN sys.schemas AS S on S.schema_id=TB.schema_id
           INNER JOIN FW_UDICData AS FU ON TB.Name collate database_default = FU.UDIC_ID) AS X

  -- Execute SQL.
  
  EXECUTE (@strSQL)
 
  COMMIT

  SET NOCOUNT OFF

END -- ToggleStatusUDICTriggers
GO
