SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_PopWin_ARDetails] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7), @UserName varchar(32))
AS BEGIN
/*
	Copyright (c) 2020 Central Consulting Group.  All rights reserved.

	[spCCG_EI_PopWin_AROver60Details] '1999015.00',' ',' ', ''
*/
	declare @T TABLE (TopOrder	int, Row_Detail varchar(255), Descr	varchar(255), InvDate date, Inv_0 money, Inv_31_61 money, Inv_61_91 money, Inv_91_121 money, Inv_121 money, ID varchar(30))
	declare @today datetime, @MainWBS1 varchar(30)
    set @today=getutcdate()

	select @MainWBS1=MainWBS1 from BTBGSubs where SubWBS1=@WBS1
	if @MainWBS1 is not null 
		set @WBS1=@MainWBS1
		
	insert into @T (TopOrder, Row_Detail, Descr, InvDate, Inv_0, Inv_31_61, Inv_61_91, Inv_91_121, Inv_121, ID)
	select row_number() over(order by InvoiceDate desc, Invoice desc), '', Invoice, InvoiceDate,
		Sum(Case When					DaysOld < 31 Then InvoiceBalance Else 0 End) as Inv_0,
		Sum(Case When DaysOld >= 31 and DaysOld < 61 Then InvoiceBalance Else 0 End) as Inv_31_61,
		Sum(Case When DaysOld >= 61 and DaysOld < 91 Then InvoiceBalance Else 0 End) as Inv_61_91,
		Sum(Case When DaysOld >= 91 and DaysOld < 121 Then InvoiceBalance Else 0 End) as Inv_91_121,	
		Sum(Case When DaysOld >= 121 Then InvoiceBalance Else 0 End) as Inv_121,
		''
	from ( 
		select IsNull(LedgerAR.Invoice, '') As Invoice, AR.InvoiceDate, IsNull(PR.WBS1, '') As WBS1,
			Min(case when AR.InvoiceDate IS NULL then 0 else datediff(day,AR.InvoiceDate,GETDATE()) end) AS DaysOld, 
			Sum(case when LedgerAR.TransType='IN' then -Amount else case when LedgerAR.TransType='CR' and LedgerAR.SubType='T' then -Amount else Amount end end ) AS InvoiceBalance
		from LedgerAR LEFT JOIN BTBGSubs on LedgerAR.WBS1 = BTBGSubs.SubWBS1 
		left join PR as MAINPR on BTBGSubs.MainWBS1=MAINPR.WBS1 AND MAINPR.WBS2=' ' AND MAINPR.WBS3=' '
		, AR , PR 
		where
			LedgerAR.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' 
			 AND LedgerAR.WBS1 = AR.WBS1 AND LedgerAR.WBS2 = AR.WBS2 AND LedgerAR.WBS3 = AR.WBS3 AND LedgerAR.Invoice = AR.Invoice 
			AND LedgerAR.AutoEntry = 'N' 
			AND ((LedgerAR.TransType = 'IN' AND (LedgerAR.SubType <> 'X' OR LedgerAR.SubType Is Null)) 
			OR (LedgerAR.TransType = 'CR' AND LedgerAR.SubType IN ('R','T'))) 
			 AND ((PR.WBS1 = @WBS1) OR (PR.WBS1 = BTBGSubs.subwbs1 AND (BTBGSubs.MAINWBS1 = @WBS1) ))
		group by IsNull(LedgerAR.Invoice, ''), IsNull(PR.WBS1, ''), AR.InvoiceDate,
			case when LedgerAR.TransType='CR' AND LedgerAR.SubType='R' then 'R' 
			 when LedgerAR.SubType='R' then 'E' 
			 when LedgerAR.TransType ='CR' AND LedgerAR.SubType='T' then 'T' else ' ' end
	) ARAgingQuery
	group by Invoice, InvoiceDate

	-- Add grand total line:
	insert into @T (TopOrder, Row_Detail, Descr, Inv_0, Inv_31_61, Inv_61_91, Inv_91_121, Inv_121)
		select 900 as TopOrder, 'BackgroundColor=#888888;ForegroundColor=#ffffff;Bold=Y', 'GRAND TOTAL', (select sum(Inv_0) from @T), (select sum(Inv_31_61) from @T), (select sum(Inv_61_91) from @T), (select sum(Inv_91_121) from @T), (select sum(Inv_121) from @T)
	
	delete from @T where Inv_0=0 and Inv_31_61=0 and Inv_61_91=0 and Inv_91_121=0 and Inv_121=0 

	-- Final recordsets
	select 'T;Align=Center', '', 'D;Align=Center', 'C2;NS', 'C2;NS', 'C2;NS', 'C2;NS', 'C2;NS',    'Outstanding AR: ' + @WBS1 + case when @WBS2 <> '' then +' - ' + @WBS2 else '' end + case when @WBS3 <> '' then +' - ' + @WBS3 else '' end 
	select Descr as [Invoice], Row_Detail, InvDate as [Date], Inv_0 as [0-30], Inv_31_61 as [31-60], Inv_61_91 as [61-90], Inv_91_121 as [91-120], Inv_121 as [121+] from @T 

	select 'width=800
		height=600' as AdvancedOptions
END
GO
