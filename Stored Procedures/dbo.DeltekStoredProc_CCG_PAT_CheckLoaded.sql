SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_CheckLoaded] (
	@sourcetype		Nvarchar(50),
	@sourcepkey		Nvarchar(512)
) AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_CheckLoaded] '', ''
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SET NOCOUNT ON;
	SELECT seq
        FROM CCG_PAT_Payable
        WHERE sourcetype = @sourcetype and sourcepkey = @sourcepkey;
END;

GO
