SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Ctrl_DoEntryColumnClear] (
	@EMPLOYEEID				Nvarchar(32),
	@dataType				Nvarchar(20),
	@ConfigCustomColumn		int,
	@ColumnLabel			Nvarchar(500),
	@DatabaseField			Nvarchar(100),
	@decValue				bit,
	@payableList			Nvarchar(max),
	@DatabaseModEmpField	Nvarchar(100)
) AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max);

	SET @sql = N'
	INSERT INTO CCG_PAT_HistoryUpdateColumns (ActionTaken, ActionDate, ActionTakenBy, PayableSeq, DataType,
			ConfigCustomColumn, ColumnLabel, OldValue, NewValue,
			OldValueDecimal, NewValueDecimal)
		SELECT N''Clear'', getutcdate(), N''' + @EMPLOYEEID + ''', PayableSeq, ''' + @dataType + ''',
				' + @ConfigCustomColumn + ', N''' + REPLACE(@ColumnLabel, '''', '''''') + ''', MAX(' + @DatabaseField + '), NULL,
				' + (CASE WHEN @decValue = 1 THEN 'MAX(' + @DatabaseField + ')' ELSE 'NULL' END) + ', NULL
			FROM CCG_PAT_CustomColumns
			WHERE PayableSeq in (' + @payableList + ') and NOT ' + @DatabaseField + ' IS NULL
			GROUP BY PayableSeq';
	EXEC(@sql);

	SET @sql = N'
		UPDATE CCG_PAT_CustomColumns SET ' +
            @DatabaseField + ' = NULL, ' +
            @DatabaseModEmpField + ' = NULL
			WHERE PayableSeq in (' + @payableList + ') and NOT ' + @DatabaseField + ' IS NULL';

	EXEC(@sql);
	SELECT @@ROWCOUNT as RowsAffected;
END;

GO
