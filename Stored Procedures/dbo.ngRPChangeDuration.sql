SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ngRPChangeDuration]
  @strRowID Nvarchar(255),
  @strNewStartDate varchar(8),
  @strNewEndDate varchar(8),
  @intShiftMode int /* 1 = Maintain Pattern, 2 = Distribute Evenly, 3 = No Redistribute */
AS

BEGIN -- Procedure ngRPChangeDuration

/*

  The objective of this SP is to change the durations of WBS rows as well as Assignment rows in the branch that is pointed to by @strRowID.

  1. Which WBS rows and which Assignment rows will be changed are determined by the {StartDate, EndDate} range of those rows.
  2. The new Start or End Date can be reduced down to one working day for all WBS, Assigment, and TPD rows within the selection criteria.
  3. If a row has Start Date later than NewStartDate and End Date earlier than NewEndDate then the row and its decendants will nor be affected. 

*/

  SET NOCOUNT ON
  
  DECLARE @dtNewStartDate datetime
  DECLARE @dtNewEndDate datetime

  DECLARE @dtMinNewDate datetime
  DECLARE @dtMaxNewDate datetime
  DECLARE @dt_MINDATE datetime = CONVERT(datetime, '')
  DECLARE @dt_MAXDATE datetime = CONVERT(datetime, '99991231')
   
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strAssignmentID varchar(32)
  DECLARE @strResourceID Nvarchar(20)
  DECLARE @strGenericResourceID Nvarchar(20)
  DECLARE @strResourceType varchar(1)
  DECLARE @strIDPrefix Nvarchar(255)
  DECLARE @strUserName Nvarchar(32)

  DECLARE @siGRMethod smallint
  DECLARE @siCostRtMethod smallint
  DECLARE @siBillingRtMethod smallint
  
  DECLARE @intCostRtTableNo int
  DECLARE @intBillingRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int

  DECLARE @siLabRateDecimals smallint = 4

  DECLARE @tabAssignment TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ResourceID Nvarchar(20) COLLATE database_default,
    GenericResourceID Nvarchar(20) COLLATE database_default,
    Category smallint,
    GRLBCD Nvarchar(14) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    CostRate decimal(19,4),
    BillingRate decimal(19,4),
    AT_OutlineNumber varchar(255) COLLATE database_default
    UNIQUE(PlanID, TaskID, AssignmentID)
  )

  DECLARE @tabASGTPD TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ASGResourceID Nvarchar(20) COLLATE database_default,
    ASGCategory smallint,
    ASGGRLBCD Nvarchar(14) COLLATE database_default,
    ASGCostRate decimal(19,4),
    ASGBillingRate decimal(19,4),
    MinTPDDate datetime,
    MaxTPDDate datetime,
    SumHrs decimal(19,4)
    UNIQUE(PlanID, TaskID, AssignmentID, MinTPDDate, MaxTPDDate, SumHrs)
  )
  
  DECLARE @tabTSKTPD TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    MinTPDDate datetime,
    MaxTPDDate datetime,
    SumHrs decimal(19,4)
    UNIQUE(PlanID, TaskID, MinTPDDate, MaxTPDDate, SumHrs)
  )
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. E~<RPAssignment.ResourceID>|<RPTask.TaskID>
  --   2. G~<RPAssignment.GenericResourceID>|<RPTask.TaskID>

  SET @strIDPrefix = LEFT(@strRowID, CHARINDEX('|', @strRowID) - 1)
  SET @strResourceType = SUBSTRING(@strRowID, 1, 1)

  IF (@strResourceType = 'E')
    BEGIN
      SET @strResourceID = REPLACE(@strIDPrefix, 'E~', '')
      SET @strGenericResourceID = NULL
    END
  ELSE IF (@strResourceType = 'G')
    BEGIN
      SET @strGenericResourceID = REPLACE(@strIDPrefix, 'G~', '')
      SET @strResourceID = NULL
    END
  ELSE
    BEGIN
      SET @strResourceID = NULL
      SET @strGenericResourceID = NULL
    END

  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

  -- Get various parameters.

  SELECT 
    @strCompany = P.Company,
    @strPlanID = T.PlanID,
    @dtMinNewDate = T.StartDate,
    @dtMaxNewDate = T.EndDate,
    @siGRMethod = P.GRMethod,
    @intGenResTableNo = P.GenResTableNo,
    @intGRBillTableNo = P.GRBillTableNo,
    @siCostRtMethod = P.CostRtMethod,
    @siBillingRtMethod = P.BillingRtMethod,
    @intCostRtTableNo = P.CostRtTableNo,
    @intBillingRtTableNo = P.BillingRtTableNo
    FROM RPTask AS T
      INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID
    WHERE T.TaskID = @strTaskID

  -- Set Date limits.
  -- Only the selected duration to be shrunk to one calendar day either at the EndDate or StartDate of the selected row.  

  SET @dtNewStartDate = 
    CASE
      WHEN DATALENGTH(@strNewStartDate) = 0 THEN @dtMinNewDate
      ELSE CONVERT(datetime, @strNewStartDate)
    END

  SET @dtNewEndDate = 
    CASE
      WHEN DATALENGTH(@strNewEndDate) = 0 THEN @dtMaxNewDate
      ELSE CONVERT(datetime, @strNewEndDate)
    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save off Assignment rows that are in the selected branch

  INSERT @tabAssignment(
    PlanID,
    TaskID,
    AssignmentID,
    ResourceID,
    GenericResourceID,
    Category,
    GRLBCD,
    StartDate,
    EndDate,
    CostRate,
    BillingRate,
    AT_OutlineNumber
  )
    SELECT DISTINCT
      A.PlanID,
      A.TaskID,
      A.AssignmentID,
      A.ResourceID,
      A.GenericResourceID,
      A.Category,
      A.GRLBCD,
      A.StartDate,
      A.EndDate,
      A.CostRate,
      A.BillingRate,
      AT.OutlineNumber AS AT_OutlineNumber
      FROM RPAssignment AS A
        INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
        INNER JOIN RPTask AS PT ON A.PlanID = PT.PlanID 
      WHERE
        A.PlanID = @strPlanID AND
        PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
        AT.OutlineNumber LIKE PT.OutlineNumber + '%' AND
        ((ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') 
				  AND ISNULL(A.GenericResourceID, '|') = ISNULL( @strGenericResourceID, '|'))
					OR (@strResourceID IS NULL AND @strGenericResourceID IS NULL)) AND
        (A.StartDate <= @dtNewEndDate AND A.EndDate >= @dtNewStartDate)

      -- Remember AssignmentID of the row that has RowID, if any.
      
      SELECT 
        @strAssignmentID = A.AssignmentID
        FROM @tabAssignment AS A
        WHERE ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') 
				  AND ISNULL(A.GenericResourceID, '|') = ISNULL( @strGenericResourceID, '|')
      
      -- Group TPD by Assignment.

      INSERT @tabASGTPD(
        PlanID,
        TaskID,
        AssignmentID,
        ASGResourceID,
        ASGCategory,
        ASGGRLBCD,
        ASGCostRate,
        ASGBillingRate,
        MinTPDDate,
        MaxTPDDate,
        SumHrs
      )
        SELECT DISTINCT
          A.PlanID AS PlanID,
          A.TaskID AS TaskID,
          A.AssignmentID AS AssignmentID,
          A.ResourceID AS ASGResourceID,
          A.Category AS ASGCategory,
          A.GRLBCD AS ASGGRLBCD,
          A.CostRate AS ASGCostRate,
          A.BillingRate AS ASGBillingRate,
          MIN(TPD.StartDate) AS MinTPDDate,
          MAX(TPD.EndDate) AS MaxTPDDate,
          SUM(TPD.PeriodHrs) AS SumHrs
          FROM RPPlannedLabor AS TPD
            INNER JOIN @tabAssignment AS A
              ON A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID AND TPD.AssignmentID IS NOT NULL
          WHERE TPD.PlanID = @strPlanID
          GROUP BY A.PlanID, A.TaskID, A.AssignmentID, A.ResourceID, A.Category, A.GRLBCD, A.CostRate, A.BillingRate

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Redistribute TPD evenly only when Shift Mode is 2.

  IF (@intShiftMode = 2)
    BEGIN

      -- Delete existing TPD under Assigments collected earlier in @tabAssignment.

      DELETE RPPlannedLabor
        WHERE PlanID = @strPlanID AND 
        AssignmentID IN (SELECT DISTINCT AssignmentID FROM @tabAssignment)

      -- Insert new TPD, one for each AssignmentID in @tabASGTPD and @tabAssignment

      INSERT RPPlannedLabor(
        TimePhaseID,
        PlanID, 
        TaskID,
        AssignmentID,
        StartDate, 
        EndDate, 
        PeriodHrs,
        CostRate,
        BillingRate,
        CreateUser,
        ModUser,
        CreateDate,
        ModDate
      )
        SELECT
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
        TPD.PlanID AS PlanID, 
        TPD.TaskID AS TaskID,
        TPD.AssignmentID AS AssignmentID,
        CASE
          WHEN (@dtNewStartDate > TPD.MinTPDDate) THEN @dtNewStartDate
          WHEN (@dtNewEndDate < TPD.MinTPDDate) THEN @dtNewEndDate
          ELSE TPD.MinTPDDate
        END AS StartDate,
        CASE
          WHEN (@dtNewEndDate < TPD.MaxTPDDate) THEN @dtNewEndDate
          WHEN (@dtNewStartDate > TPD.MaxTPDDate) THEN @dtNewStartDate
          ELSE TPD.MaxTPDDate
        END AS EndDate,
        TPD.SumHrs AS PeriodHrs,
        dbo.RP$LabRate(
          TPD.ASGResourceID,
          ISNULL(TPD.ASGCategory, 0),
          TPD.ASGGRLBCD,
          NULL,
          @siCostRtMethod,
          @intCostRtTableNo,
          @siGRMethod,
          @intGenResTableNo,
          CASE
            WHEN (@dtNewStartDate > TPD.MinTPDDate) THEN @dtNewStartDate
            WHEN (@dtNewEndDate < TPD.MinTPDDate) THEN @dtNewEndDate
            ELSE TPD.MinTPDDate
          END,
          CASE
            WHEN (@dtNewEndDate < TPD.MaxTPDDate) THEN @dtNewEndDate
            WHEN (@dtNewStartDate > TPD.MaxTPDDate) THEN @dtNewStartDate
            ELSE TPD.MaxTPDDate
          END,
          @siLabRateDecimals,
          0,
          ISNULL(TPD.ASGCostRate, 0)
        ) AS CostRate,
        dbo.RP$LabRate(
          TPD.ASGResourceID,
          ISNULL(TPD.ASGCategory, 0),
          TPD.ASGGRLBCD,
          NULL,
          @siBillingRtMethod,
          @intBillingRtTableNo,
          @siGRMethod,
          @intGRBillTableNo,
          CASE
            WHEN (@dtNewStartDate > TPD.MinTPDDate) THEN @dtNewStartDate
            WHEN (@dtNewEndDate < TPD.MinTPDDate) THEN @dtNewEndDate
            ELSE TPD.MinTPDDate
          END,
          CASE
            WHEN (@dtNewEndDate < TPD.MaxTPDDate) THEN @dtNewEndDate
            WHEN (@dtNewStartDate > TPD.MaxTPDDate) THEN @dtNewStartDate
            ELSE TPD.MaxTPDDate
          END,
          @siLabRateDecimals,
          0,
          ISNULL(TPD.ASGBillingRate, 0)
        ) AS BillingRate,
        CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stRPShiftDates' ELSE 'SDT_' + @strUserName END AS CreateUser,
        CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stRPShiftDates' ELSE 'SDT_' + @strUserName END AS ModUser,
        LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As CreateDate,
        LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As ModDate 
      FROM @tabASGTPD AS TPD

    END /* END IF (@intShiftMode = 2) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  UPDATE A SET
    StartDate = 
      CASE @intShiftMode
        WHEN 2 THEN
          CASE
            WHEN (DATALENGTH(@strNewStartDate) = 0 AND A.StartDate <= @dtNewEndDate) THEN A.StartDate
            WHEN (DATALENGTH(@strNewStartDate) = 0 AND A.StartDate > @dtNewEndDate) THEN @dtNewEndDate
            WHEN (DATALENGTH(@strNewStartDate) <> 0 AND A.AssignmentID = ISNULL(@strAssignmentID, '|')) THEN @dtNewStartDate
            WHEN (DATALENGTH(@strNewStartDate) <> 0 AND @dtNewStartDate > A.StartDate) THEN @dtNewStartDate
            WHEN (DATALENGTH(@strNewStartDate) <> 0 AND @dtNewStartDate <= A.StartDate) THEN A.StartDate
          END        
        WHEN 3 THEN
          CASE
            WHEN (DATALENGTH(@strNewStartDate) = 0 AND ISNULL(TPD.SumHrs, 0) = 0 AND A.StartDate <= @dtNewEndDate) THEN A.StartDate
            WHEN (DATALENGTH(@strNewStartDate) = 0 AND ISNULL(TPD.SumHrs, 0) = 0 AND A.StartDate > @dtNewEndDate) THEN @dtNewEndDate
            WHEN (DATALENGTH(@strNewStartDate) <> 0 AND ISNULL(TPD.SumHrs, 0) = 0) THEN @dtNewStartDate
            WHEN (DATALENGTH(@strNewStartDate) <> 0 AND TPD.MinTPDDate IS NULL) THEN @dtNewStartDate
            ELSE
              CASE
                WHEN @dtNewStartDate < TPD.MinTPDDate
                THEN @dtNewStartDate
                ELSE TPD.MinTPDDate
              END
          END        
      END,

    EndDate = 
      CASE @intShiftMode
        WHEN 2 THEN
          CASE
            WHEN (DATALENGTH(@strNewEndDate) = 0 AND A.EndDate >= @dtNewStartDate) THEN A.EndDate
            WHEN (DATALENGTH(@strNewEndDate) = 0 AND A.EndDate < @dtNewStartDate) THEN @dtNewStartDate
            WHEN (DATALENGTH(@strNewEndDate)<> 0 AND A.AssignmentID = ISNULL(@strAssignmentID, '|')) THEN @dtNewEndDate
            WHEN (DATALENGTH(@strNewEndDate)<> 0 AND @dtNewEndDate < A.EndDate) THEN @dtNewEndDate
            WHEN (DATALENGTH(@strNewEndDate)<> 0 AND @dtNewEndDate >= A.EndDate) THEN A.EndDate
          END
        WHEN 3 THEN
          CASE
            WHEN (DATALENGTH(@strNewEndDate) = 0 AND ISNULL(TPD.SumHrs, 0) = 0 AND A.EndDate >= @dtNewStartDate) THEN A.EndDate
            WHEN (DATALENGTH(@strNewEndDate) = 0 AND ISNULL(TPD.SumHrs, 0) = 0 AND A.EndDate < @dtNewStartDate) THEN @dtNewStartDate
            WHEN (DATALENGTH(@strNewEndDate) <> 0 AND ISNULL(TPD.SumHrs, 0) = 0) THEN @dtNewEndDate
            WHEN (DATALENGTH(@strNewEndDate) <> 0 AND TPD.MaxTPDDate IS NULL) THEN @dtNewEndDate
            ELSE
              CASE
                WHEN @dtNewEndDate > TPD.MaxTPDDate
                THEN @dtNewEndDate
                ELSE TPD.MaxTPDDate
              END
          END
      END          
         
    FROM RPAssignment AS A
      INNER JOIN @tabAssignment AS XA
        ON A.PlanID = XA.PlanID AND A.TaskID = XA.TaskID AND A.AssignmentID = XA.AssignmentID
      LEFT JOIN @tabASGTPD AS TPD
        ON XA.PlanID = TPD.PlanID AND XA.TaskID = TPD.TaskID AND XA.AssignmentID = TPD.AssignmentID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- If RowID is pointing to an Assignment row, then do not change Task {StartDate, EndDate}.

  IF (DATALENGTH(@strIDPrefix) = 0)
    BEGIN

      -- Group TPD by Task.
      -- Must include all TPD under a Task and cannot use @tabASGTPD (because it only has Assignments that need to be changed).
      -- Must perform this step here after TPD rows have been updated with the new Start/End Dates.

      INSERT @tabTSKTPD(
        PlanID,
        TaskID,
        MinTPDDate,
        MaxTPDDate,
        SumHrs
      )
        SELECT DISTINCT
          TPD.PlanID AS PlanID,
          TPD.TaskID AS TaskID,
          MIN(TPD.StartDate) AS MinTPDDate,
          MAX(TPD.EndDate) AS MaxTPDDate,
          SUM(TPD.PeriodHrs) AS SumHrs
          FROM RPPlannedLabor AS TPD
            INNER JOIN RPTask AS AT ON TPD.PlanID = AT.PlanID AND TPD.TaskID = AT.TaskID
            INNER JOIN RPTask AS PT ON AT.PlanID = PT.PlanID 
          WHERE
            TPD.PlanID = @strPlanID AND
            PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
            AT.OutlineNumber LIKE PT.OutlineNumber + '%'
          GROUP BY TPD.PlanID, TPD.TaskID

      UPDATE CT SET
        StartDate = 
          CASE @intShiftMode
            WHEN 2 THEN
              CASE
                WHEN (DATALENGTH(@strNewStartDate) = 0 AND CT.StartDate <= @dtNewEndDate) THEN CT.StartDate
                WHEN (DATALENGTH(@strNewStartDate) = 0 AND CT.StartDate > @dtNewEndDate) THEN @dtNewEndDate
                WHEN (DATALENGTH(@strNewStartDate) <> 0 AND CT.TaskID = ISNULL(@strTaskID, '|')) THEN @dtNewStartDate
                WHEN (DATALENGTH(@strNewStartDate) <> 0 AND @dtNewStartDate > CT.StartDate) THEN @dtNewStartDate
                WHEN (DATALENGTH(@strNewStartDate) <> 0 AND @dtNewStartDate <= CT.StartDate) THEN CT.StartDate
              END
            WHEN 3 THEN
              CASE
                WHEN (DATALENGTH(@strNewStartDate) = 0 AND ISNULL(TPD.SumHrs, 0) = 0 AND CT.StartDate <= @dtNewEndDate) THEN CT.StartDate
                WHEN (DATALENGTH(@strNewStartDate) = 0 AND ISNULL(TPD.SumHrs, 0) = 0 AND CT.StartDate > @dtNewEndDate) THEN @dtNewEndDate
                WHEN (DATALENGTH(@strNewStartDate) <> 0 AND ISNULL(TPD.SumHrs, 0) = 0) THEN @dtNewStartDate
                WHEN (DATALENGTH(@strNewStartDate) <> 0 AND TPD.MinTPDDate IS NULL) THEN @dtNewStartDate
                ELSE
                  CASE
                    WHEN @dtNewStartDate < TPD.MinTPDDate
                    THEN @dtNewStartDate
                    ELSE TPD.MinTPDDate
                  END
              END        
          END,

        EndDate = 
          CASE @intShiftMode
            WHEN 2 THEN
              CASE
                WHEN (DATALENGTH(@strNewEndDate) = 0 AND CT.EndDate >= @dtNewStartDate) THEN CT.EndDate
                WHEN (DATALENGTH(@strNewEndDate) = 0 AND CT.EndDate < @dtNewStartDate) THEN @dtNewStartDate
                WHEN (DATALENGTH(@strNewEndDate)<> 0 AND CT.TaskID = ISNULL(@strTaskID, '|')) THEN @dtNewEndDate
                WHEN (DATALENGTH(@strNewEndDate)<> 0 AND @dtNewEndDate < CT.EndDate) THEN @dtNewEndDate
                WHEN (DATALENGTH(@strNewEndDate)<> 0 AND @dtNewEndDate >= CT.EndDate) THEN CT.EndDate
              END
            WHEN 3 THEN
              CASE
                WHEN (DATALENGTH(@strNewEndDate) = 0 AND ISNULL(TPD.SumHrs, 0) = 0 AND CT.EndDate >= @dtNewStartDate) THEN CT.EndDate
                WHEN (DATALENGTH(@strNewEndDate) = 0 AND ISNULL(TPD.SumHrs, 0) = 0 AND CT.EndDate < @dtNewStartDate) THEN @dtNewStartDate
                WHEN (DATALENGTH(@strNewEndDate) <> 0 AND ISNULL(TPD.SumHrs, 0) = 0) THEN @dtNewEndDate
                WHEN (DATALENGTH(@strNewEndDate) <> 0 AND TPD.MaxTPDDate IS NULL) THEN @dtNewEndDate
                ELSE
                  CASE
                    WHEN @dtNewEndDate > TPD.MaxTPDDate
                    THEN @dtNewEndDate
                    ELSE TPD.MaxTPDDate
                  END
              END              
          END

        FROM RPTask AS PT
          INNER JOIN RPTask AS CT ON PT.PlanID = CT.PlanID
          LEFT JOIN @tabTSKTPD AS TPD
            ON CT.PlanID = TPD.PlanID AND CT.TaskID = TPD.TaskID

        WHERE
          PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
          CT.OutlineNumber LIKE PT.OutlineNumber + '%' AND
          (
           ((DATALENGTH(@strNewStartDate) > 0 AND CT.StartDate <= @dtNewEndDate) 
            AND 
            (DATALENGTH(@strNewEndDate) > 0 AND CT.EndDate >= @dtNewStartDate)
           )
           OR CT.TaskID = @strTaskID
          )

    END /* END IF (DATALENGTH(@strIDPrefix) = 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
  SET NOCOUNT OFF

END -- ngRPChangeDuration
GO
