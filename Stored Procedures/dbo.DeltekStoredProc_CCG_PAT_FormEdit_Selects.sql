SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FormEdit_Selects] ( @selectType varchar(100), @param nvarchar(max)= '', @param2 nvarchar(max)= '', @boolCase bit= 0)
             AS EXEC spCCG_PAT_FormEdit_Selects @selectType,@param,@param2,@boolCase
GO
