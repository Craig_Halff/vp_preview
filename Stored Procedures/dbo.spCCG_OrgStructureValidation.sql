SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_OrgStructureValidation] @UDIC_UID varchar (32), @Org varchar (255), @PrimaryTeamLocation varchar (1)
AS
/*
Copyright (c) 2018 Central Consulting Group. All rights reserved.
04/09/2018 David Springer
           Validate Org is unique.
		   Validate Primary Team Location is only used once.
           Call this from an Org Structure INSERT & CHANGE workflow when Org or Primary Team Location has changed.
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
DECLARE @OtherPrimaryOrg	varchar (255),
		@ErrorMessage		varchar (8000)
BEGIN

	If exists (Select 'x' From UDIC_OrganizationStructure Where UDIC_UID <> @UDIC_UID and CustOrganization = @Org)
		RAISERROR ('This Organization already exists.                                         ', 16, 1)

	If @PrimaryTeamLocation = 'Y'
		Begin
		Select @OtherPrimaryOrg = max (CustOrganization) 
		From UDIC_OrganizationStructure 
		Where UDIC_UID <> @UDIC_UID
		  and Left (CustOrganization, (Select Org1Length From CFGFormat)) + Right (CustOrganization, (Select Org3Length From CFGFormat)) 
			= Left (@Org, (Select Org1Length From CFGFormat)) + Right (@Org, (Select Org3Length From CFGFormat))
		  and CustPrimaryTeamLocation = 'Y'

		If @OtherPrimaryOrg is not null
			Begin
			Set @ErrorMessage = @OtherPrimaryOrg + ' is already set as the Primary Team Location.                            '
			RAISERROR (@ErrorMessage, 16, 1)
			End
		End

END
GO
