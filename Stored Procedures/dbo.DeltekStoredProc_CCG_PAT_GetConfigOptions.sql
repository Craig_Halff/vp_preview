SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_GetConfigOptions] ( @user nvarchar(32), @type varchar(50))
             AS EXEC spCCG_PAT_GetConfigOptions @user,@type
GO
