SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[pmDelConsultant]
  @strPlanID varchar(32),
  @strConsultantID varchar(32)
AS

BEGIN -- Procedure pmDelConsultant

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to delete a Consultant row and its associated rows.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    
  DELETE RPPlannedConsultant WHERE PlanID = @strPlanID AND ConsultantID = @strConsultantID
  DELETE RPBaselineConsultant WHERE PlanID = @strPlanID AND ConsultantID = @strConsultantID
  DELETE RPConsultant WHERE PlanID = @strPlanID AND ConsultantID = @strConsultantID

  SET NOCOUNT OFF

END -- pmDelConsultant
GO
