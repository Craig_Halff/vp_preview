SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_GetProjectData] ( @sRole varchar(32), @invGroupFilter varchar(max), @invGroupFilter2 varchar(max), @ARMExists int, @docsFunction int, @sInvoiceGroup varchar(255), @visionLanguage varchar(10), @user nvarchar(32), @employee nvarchar(20), @inclCustomCols int, @hideDelegatedProjects int= 0, @quickLoad int= 0)
             AS EXEC spCCG_EI_GetProjectData @sRole,@invGroupFilter,@invGroupFilter2,@ARMExists,@docsFunction,@sInvoiceGroup,@visionLanguage,@user,@employee,@inclCustomCols,@hideDelegatedProjects,@quickLoad
GO
