SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_HAI_Staff_GetByUsername]
(
	@Username		NVARCHAR(6)
)
AS
BEGIN
  ------------------------------------------------------------------------
  SET NOCOUNT ON;

  ------------------------------------------------------------------------
  SELECT TOP 1
	Employee
	, LastName
	, FirstName
	, MiddleName
	, Suffix
	, ProfessionalSuffix
	, PreferredName
	, DisplayName
	, SimpleName
	, Initials
	, Title
	, Salutation
	, TeamID
	, Username
	, EMail
	, WorkPhone
	, WorkPhoneFormat
	, MobilePhone
	, MobilePhoneFormat
	, City
	, [State]
	,ZIP
	, SecurityRole
  FROM
    DataMart.dbo.Staff
  WHERE
	Username = @Username
  ------------------------------------------------------------------------
END
GO
