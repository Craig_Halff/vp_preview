SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_DelegationGetMyProjectsFilter] (
	@employee	Nvarchar(20)
)
AS
BEGIN
	/* Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved. */
	-- [spCCG_EI_DelegationGetMyProjectsFilter] '00001'
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

	declare @sql nvarchar(max)
	select @sql = IsNull(STUFF((
		SELECT N' or '+(case when left(VisionField, 4) = 'Cust' then 'PCTF.' when left(VisionField, 3) <> 'PR.' then 'PR.' else '' end)+VisionField+N' = N''' + @employee + N''''
			from CCG_EI_ConfigRoles
			where VisionField <> ''
			order by VisionField
			FOR XML PATH(''), TYPE
		).value('.', 'nvarchar(MAX)'),1,4,''),'')

	select @sql
END
GO
