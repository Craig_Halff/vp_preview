SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AddIndustryDefaultsConsultnlNL]
AS
BEGIN

PRINT ' '
PRINT 'Adding Consulting Industry Defaults for nl-NL'

BEGIN TRANSACTION

DELETE FROM BTLaborCatsDescriptions WHERE UICultureName = 'nl-NL';
--..........................................................................................v(50)
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 1, 'nl-NL','Leidinggevende'             WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 1);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 2, 'nl-NL','Oefenlead'         WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 2);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 3, 'nl-NL','Leidinggevende klant'      WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 3);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 4, 'nl-NL','Engagementmanager'    WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 4);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 5, 'nl-NL','Adviseur'            WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 5);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 6, 'nl-NL','Zakelijk analyst'      WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 6);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 7, 'nl-NL','Architect'             WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 7);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 8, 'nl-NL','Ontwerper'              WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 8);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 9, 'nl-NL','Offertemanager'      WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 9);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 10,'nl-NL','Controller'            WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 10);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 11,'nl-NL','Marketing'             WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 11);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 12,'nl-NL','Personeelszaken'       WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 12);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 13,'nl-NL','Bedrijfsontwikkeling'  WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 13);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 14,'nl-NL','Beheerder'                 WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 14);

DELETE FROM CFGActivitySubjectData WHERE UICultureName = 'nl-NL';
--..................................................................................v(50)
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Cold Call'                ,'nl-NL',3 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Eerste vergadering'         ,'nl-NL',1 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Eerste e-mail'       ,'nl-NL',4 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Kwalificatieoproep'       ,'nl-NL',2 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Vermelding op social media'     ,'nl-NL',6 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('E-mail met inzicht' ,'nl-NL',5 ,'N');

DELETE FROM CFGActivityTypeDescriptions WHERE UICultureName = 'nl-NL';
--.........................................................................................................v(15)
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'EMail'     ,'nl-NL','E-mail'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'EMail');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Event'     ,'nl-NL','Gebeurtenis'          ,6 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Event');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Mailing'   ,'nl-NL','Mailing'        ,5 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Mailing');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Meeting'   ,'nl-NL','Vergadering'        ,3 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Meeting');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Merge'     ,'nl-NL','Document maken',4 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Merge');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Milestone' ,'nl-NL','Mijlpaal'      ,8 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Milestone');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Phone Call','nl-NL','Telefoongesprek'     ,2 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Phone Call');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Task'      ,'nl-NL','Taak'           ,9 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Task');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Touchpoint','nl-NL','Contactpunt'     ,7 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Touchpoint');

DELETE FROM CFGARLedgerHeadingsDescriptions WHERE UICultureName = 'nl-NL';
--...............................................................................................v(12)
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 1,'nl-NL','Honoraria'       WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 1);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 2,'nl-NL','Vergoeding'     WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 2);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 3,'nl-NL','Adviseur' WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 3);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 4,'nl-NL','Voorschot'   WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 4);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 5,'nl-NL','Belastingen'      WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 5);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 6,'nl-NL','Rente'   WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 6);

DELETE FROM CFGBillMainDescriptions WHERE UICultureName = 'nl-NL';
--..............................................................................................................................................................................................................v(15)..v(15)....v(15)............v(15)......v(15)...v(15).....v(15)..v(15)
INSERT INTO CFGBillMainDescriptions (Company,UICultureName,FooterMessage,FeeLabel,LabLabel,ConLabel,ExpLabel,UnitLabel,AddOnLabel,TaxLabel,InterestLabel,OvtIndicator) SELECT CFGMainData.Company,'nl-NL',NULL,'Honorarium' ,'Arbeid' ,'Onderaannemer' ,'Onkosten-' ,'Eenheid' ,'Bijkomende kosten' ,'Belasting' ,'Rente' ,'Ovt'  FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGBillMainData WHERE Company = CFGMainData.Company);

DELETE FROM CFGCampaignActionDescriptions WHERE UICultureName = 'nl-NL';
--..................................................................................................v(50)
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '01','nl-NL','Inhoud ontwikkelen' ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '01');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '02','nl-NL','Uitnodiging versturen' ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '02');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '03','nl-NL','E-mailblast verzenden',3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '03');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '04','nl-NL','Opvolging'       ,4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '04');

DELETE FROM CFGCampaignAudienceDescriptions WHERE UICultureName = 'nl-NL';
--....................................................................................................v(50)
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '01','nl-NL','Leidinggevenden'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '01');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '02','nl-NL','Finance'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '02');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '03','nl-NL','Marketing'           ,3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '03');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '04','nl-NL','Bedrijfsontwikkeling',4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '04');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '05','nl-NL','Engagementmanagers' ,5 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '05');

DELETE FROM CFGCampaignObjectiveDescriptions WHERE UICultureName = 'nl-NL';
--.....................................................................................................v(50)
INSERT INTO CFGCampaignObjectiveDescriptions (Code,UICultureName,Description,Seq) SELECT '01','nl-NL','Generatie van nieuwe business' ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignObjectiveData WHERE Code = '01');
INSERT INTO CFGCampaignObjectiveDescriptions (Code,UICultureName,Description,Seq) SELECT '02','nl-NL','Uitbreiding van bestaande business',2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignObjectiveData WHERE Code = '02');

DELETE FROM CFGCampaignStatusDescriptions WHERE UICultureName = 'nl-NL';
--..................................................................................................v(50)
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','nl-NL','Actief'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '01');
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','nl-NL','Planning' ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '02');
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','nl-NL','Voltooid',3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '03');

DELETE FROM CFGCampaignTypeDescriptions WHERE UICultureName = 'nl-NL';
--................................................................................................v(50)
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','nl-NL','E-mail'      ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '01');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','nl-NL','Directe mail',2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '02');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','nl-NL','Webinar'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '03');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','nl-NL','Beurs'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '04');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '05','nl-NL','Bijeenkomst'     ,5 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '05');

DELETE FROM CFGClientCurrentStatusDescriptions WHERE UICultureName = 'nl-NL';
--...............................................................................................................v(50)
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Existing','nl-NL','Bestaand',1 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Existing');
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Former'  ,'nl-NL','Voormalig'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Former');
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Prospect','nl-NL','vooruitzicht',2 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Prospect');

DELETE FROM CFGClientRelationshipDescriptions WHERE UICultureName = 'nl-NL';
--......................................................................................................v(50)
INSERT INTO CFGClientRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01','nl-NL','Concurrent',1 WHERE EXISTS (SELECT 'x' FROM CFGClientRelationshipData WHERE Code = '01');

DELETE FROM CFGClientRoleDescriptions WHERE UICultureName = 'nl-NL';
--....................................................................................................v(50)
INSERT INTO CFGClientRoleDescriptions (Code,UICultureName,Description,Seq) SELECT '01'      ,'nl-NL','Onderaannemer',2 WHERE EXISTS (SELECT 'x' FROM CFGClientRoleData WHERE Code = '01');
INSERT INTO CFGClientRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'sysOwner','nl-NL','Klant'      ,1 WHERE EXISTS (SELECT 'x' FROM CFGClientRoleData WHERE Code = 'sysOwner');

DELETE FROM CFGClientTypeDescriptions WHERE UICultureName = 'nl-NL';
--..............................................................................................v(50)
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','nl-NL','Opleiding'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '01');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','nl-NL','Onderneming'          ,2 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '02');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','nl-NL','Financiën en bankieren' ,3 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '03');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','nl-NL','Gezondheid en welzijn' ,4 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '04');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '05','nl-NL','Horeca'         ,5 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '05');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '06','nl-NL','Informatieservices',6 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '06');

DELETE FROM CFGContactRelationshipDescriptions WHERE UICultureName = 'nl-NL';
--.......................................................................................................v(50)
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01','nl-NL','Voormalige klant'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '01');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '02','nl-NL','Voormalige collega' ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '02');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '03','nl-NL','Branchegenoot'   ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '03');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '04','nl-NL','Persoonlijk contact',4 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '04');

DELETE FROM CFGContactRoleDescriptions WHERE UICultureName = 'nl-NL';
--.....................................................................................................v(50)
INSERT INTO CFGContactRoleDescriptions (Code,UICultureName,Description,Seq) SELECT '01'      ,'nl-NL','Onderaannemer',2 WHERE EXISTS (SELECT 'x' FROM CFGContactRoleData WHERE Code = '01');
INSERT INTO CFGContactRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'sysOwner','nl-NL','Klant'      ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactRoleData WHERE Code = 'sysOwner');

DELETE FROM CFGContactSourceDescriptions WHERE UICultureName = 'nl-NL';
--...................................................................................................v(50)
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '01','nl-NL','Klantreferentie'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '01');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '02','nl-NL','Gebeurtenis'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '02');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '03','nl-NL','Lijst'              ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '03');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '04','nl-NL','Persoonlijk contact'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '04');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '05','nl-NL','Social media'      ,5 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '05');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '06','nl-NL','Intellectueel leiderschap',6 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '06');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '07','nl-NL','Website'           ,7 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '07');

DELETE FROM CFGContactTitleDescriptions WHERE UICultureName = 'nl-NL';
--.................................................................................................v(50)
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Director' ,'nl-NL','Directeur' ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Director');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Executive','nl-NL','Leidinggevende',1 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Executive');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Finance'  ,'nl-NL','Finance'  ,5 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Finance');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Marketing','nl-NL','Marketing',6 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Marketing');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Sales'    ,'nl-NL','Verkoop'    ,4 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Sales');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'VP'       ,'nl-NL','VP'       ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'VP');

DELETE FROM CFGContractStatusDescriptions WHERE UICultureName = 'nl-NL';
--..................................................................................................v(50)
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','nl-NL','Interne controle'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '01');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','nl-NL','Verzonden naar klant'   ,2 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '02');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','nl-NL','Onderhandeling'      ,3 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '03');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '04','nl-NL','Juridische controle'     ,4 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '04');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '05','nl-NL','Ondertekend en uitgevoerd',5 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '05');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '06','nl-NL','Verloren/geen deal'     ,6 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '06');

DELETE FROM CFGContractTypeDescriptions WHERE UICultureName = 'nl-NL';
--................................................................................................v(50)
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','nl-NL','Werkverklaring',1 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '01');
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','nl-NL','Aanpassing'        ,2 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '02');

DELETE FROM CFGCubeTranslationDescriptions WHERE UICultureName = 'nl-NL';
--..............................................................................................................................................v(200)
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'U'                       ,'nl-NL','Onbekend');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'UM'                      ,'nl-NL','Onbekende maand');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'H'                       ,'nl-NL','Historie');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('YesNo'          ,'Y'                       ,'nl-NL','Ja');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('YesNo'          ,'N'                       ,'nl-NL','Nee');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('Status'         ,'A'                       ,'nl-NL','Actief');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('Status'         ,'I'                       ,'nl-NL','Inactief');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'1-Asset'                 ,'nl-NL','Activum');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'2-Liability'             ,'nl-NL','Passiefpost');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'3-NetWorth'              ,'nl-NL','Nettowaarde');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'4-Revenue'               ,'nl-NL','Opbrengst');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'5-Reimbursable'          ,'nl-NL','Vergoedbare');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'6-ReimbursableConsultant','nl-NL','Vergoeding consultant');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'7-Direct'                ,'nl-NL','Direct');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'8-DirectConsultant'      ,'nl-NL','Directe kosten consultant');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'9-Indirect'              ,'nl-NL','Indirect');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'10-OtherCharges'         ,'nl-NL','Overige kosten');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'U-Unknown'               ,'nl-NL','Onbekend');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ExpenseType'    ,'5-ReimbursableOther'     ,'nl-NL','Overige vergoedingen');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ExpenseType'    ,'7-DirectOther'           ,'nl-NL','Direct overig');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ReportType'     ,'BalanceSheet'            ,'nl-NL','Balans');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ReportType'     ,'IncomeStatement'         ,'nl-NL','Winst- en verliesrekening');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'1-Asset','nl-NL'         ,'Other Assets');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'2-Liability'             ,'nl-NL','Andere passiva');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'3-NetWorth'              ,'nl-NL','Andere nettowaarde');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'4-Revenue'               ,'nl-NL','Andere opbrengsten');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'5-Reimbursable'          ,'nl-NL','Ander invorderbaar');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'6-Reimbursable'          ,'nl-NL','Ander invorderbaar');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'7-Direct'                ,'nl-NL','Ander direct');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'8-Direct'                ,'nl-NL','Ander direct');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'9-Indirect'              ,'nl-NL','Ander indirect');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'10-OtherCharges'         ,'nl-NL','Overige kosten');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'B'                       ,'nl-NL','Factureerbaar');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'H'                       ,'nl-NL','Geblokkeerd');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'W'                       ,'nl-NL','Af te schrijven');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'X'                       ,'nl-NL','Afgeboekt');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'D'                       ,'nl-NL','Aangeduid voor verwijdering');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'N'                       ,'nl-NL','Niet-factureerbaar');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'F'                       ,'nl-NL','Eindtotaal gefactureerd');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'T'                       ,'nl-NL','Overgeboekt');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'R'                       ,'nl-NL','Deels ingehouden/vrijgegeven');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'M'                       ,'nl-NL','Gewijzigd');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'O'                       ,'nl-NL','Verwijderd');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AP'                      ,'nl-NL','Crediteurenboekingsstukken');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BA'                      ,'nl-NL','Benefitsuren verwerken');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BE'                      ,'nl-NL','Facturatie kostenoverboekingen');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BL'                      ,'nl-NL','Overboekingen facturatie-arbeid');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BU'                      ,'nl-NL','Overboekingen facturatie-[Unit]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CD'                      ,'nl-NL','Kasbetalingen');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CP'                      ,'nl-NL','[WBS1] [Organization] converteren');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CR'                      ,'nl-NL','Kasontvangsten');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CV'                      ,'nl-NL','Crediteurenbetalingen');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EP'                      ,'nl-NL','[Employee]-betaling verwerken');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','ER'                      ,'nl-NL','Terugbetalingen [Employee]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EX'                      ,'nl-NL','Onkosten [Employee]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IN'                      ,'nl-NL','Facturen');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','JE'                      ,'nl-NL','Journaalposten');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','KC'                      ,'nl-NL','Sleutelconversie');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','LA'                      ,'nl-NL','Arbeidscorrecties');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','LG'                      ,'nl-NL','Winsten/verliezen en herwaarderingen');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','MI'                      ,'nl-NL','Overige onkosten');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PP'                      ,'nl-NL','Verwerking crediteurenbetalingen');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PR'                      ,'nl-NL','Afdrukken en reproducties');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PY'                      ,'nl-NL','Verwerking loonlijst');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','RG'                      ,'nl-NL','Opbrengsten genereren');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','TS'                      ,'nl-NL','Urenstaten');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','UN'                      ,'nl-NL','[Units]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','UP'                      ,'nl-NL','[Units] per [WBS1]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AL'                      ,'nl-NL','Sleutelconversiepost, openen van een nieuwe periode/kwartaal W-2/opbouwjaar voor benefits, sluiten van een periode');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AX'                      ,'nl-NL','Geconverteerd crediteurenboekingsstuk');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CN'                      ,'nl-NL','Geconverteerde consultantonkost(en)');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CT'                      ,'nl-NL','Transactie geeft aan wanneer de conversie van een voorgaande vrijgave werd uitgevoerd');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EC'                      ,'nl-NL','Geconverteerde tijdanalyseaanpassing(en)');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IH'                      ,'nl-NL','Geconverteerde factuur');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IX'                      ,'nl-NL','Geconverteerde factuur/facturen');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','JX'                      ,'nl-NL','MCFMS-conversie');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PX'                      ,'nl-NL','Geconverteerde verwerking van crediteurenbetaling');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','RX'                      ,'nl-NL','MCFMS-conversie');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','XD'                      ,'nl-NL','Geconverteerde rekeninggegevens');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','XE'                      ,'nl-NL','Geconverteerde [WBS1]-onkosten');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HA'                      ,'nl-NL','Historie [Account]saldi');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HB'                      ,'nl-NL','Historie opgebouwde benefits [Employee]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HE'                      ,'nl-NL','Historie arbeid en onkosten [WBS1]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HL'                      ,'nl-NL','Historie arbeid en onkosten [WBS1]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HU'                      ,'nl-NL','Niet-gefactureerde opbrengsten');

DELETE FROM CFGEMDegreeDescriptions WHERE UICultureName = 'nl-NL';
--............................................................................................v(50)
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','nl-NL','Bachelor of Science',1 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '01');
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','nl-NL','Bachelor of Arts'   ,2 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '02');
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','nl-NL','Masters'            ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '03');

DELETE FROM CFGEmployeeRelationshipDescriptions WHERE UICultureName = 'nl-NL';
--..............................................................................................................v(50)
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01'      ,'nl-NL','Voormalige werkgever'            ,3 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '01');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '02'      ,'nl-NL','Voormalige collega'            ,2 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '02');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '03'      ,'nl-NL','Persoonlijk contact'           ,5 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '03');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '04'      ,'nl-NL','Algemene brancheconnectie',4 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '04');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT 'SysOwner','nl-NL','Eigenaar'                      ,1 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = 'SysOwner');

DELETE FROM CFGEmployeeRoleDescriptions WHERE UICultureName = 'nl-NL';
--........................................................................................................v(50)
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Admin'     ,'nl-NL','Beheerder'           ,1  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Admin');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Controller','nl-NL','Controller'      ,4  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Controller');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'DelivTeam' ,'nl-NL','Leverteam'   ,5  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'DelivTeam');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Executive' ,'nl-NL','Leidinggevende'       ,7  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Executive');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'HumanRes'  ,'nl-NL','Personeelszaken' ,8  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'HumanRes');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ResMgr'    ,'nl-NL','Bedrijfsmiddelen Manager',12 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ResMgr');

DELETE FROM CFGEmployeeTitleDescriptions WHERE UICultureName = 'nl-NL';
--...................................................................................................v(50)
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Acct'      ,'nl-NL','Accountant'           ,18 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Acct');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Arch'      ,'nl-NL','Architect'            ,22 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Arch');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'BA'        ,'nl-NL','Zakelijk analyst'     ,23 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'BA');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CE'        ,'nl-NL','Leidinggevende klant'     ,9  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CE');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CEO'       ,'nl-NL','Directeur'                  ,1  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CEO');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CFO'       ,'nl-NL','financieel directeur'                  ,2  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CFO');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Consultant','nl-NL','Adviseur'           ,16 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Consultant');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Controller','nl-NL','Controller'           ,17 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Controller');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'COO'       ,'nl-NL','COO'                  ,3  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'COO');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CP'        ,'nl-NL','Partnerklant'       ,10 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CP');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Dev'       ,'nl-NL','Ontwikkelaar'            ,20 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Dev');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'DirHR'     ,'nl-NL','directeur personeelszaken'       ,7  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'DirHR');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'DirMkting' ,'nl-NL','Marketingdirecteur',8  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'DirMkting');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Eng'       ,'nl-NL','Ingenieur'             ,21 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Eng');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'EngageMgr' ,'nl-NL','Engagementmanager'   ,11 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'EngageMgr');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Marketing' ,'nl-NL','Marketingassistent'  ,25 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Marketing');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'ProdMgr'   ,'nl-NL','Productmanager'      ,12 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'ProdMgr');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'ProjMgr'   ,'nl-NL','Projectmanager'      ,13 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'ProjMgr');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'PropMgr'   ,'nl-NL','Offertemanager'     ,15 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'PropMgr');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Recruiter' ,'nl-NL','Recruiter'            ,19 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Recruiter');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'RM'        ,'nl-NL','Bedrijfsmiddelen Manager'     ,14 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'RM');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'UX'        ,'nl-NL','UX'                   ,24 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'UX');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'VPEng'     ,'nl-NL','VP Engineering'       ,6  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'VPEng');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'VPSales'   ,'nl-NL','VP Verkoop'          ,4  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'VPSales');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'VPStrat'   ,'nl-NL','VP Strategie'       ,5  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'VPStrat');

DELETE FROM CFGEmployeeTypeDescriptions WHERE UICultureName = 'nl-NL';
--.....................................................................................v(30)
INSERT INTO CFGEmployeeTypeDescriptions (Type,UICultureName,Label) SELECT 'E','nl-NL','Werknemer'   WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTypeData WHERE Type = 'E');
INSERT INTO CFGEmployeeTypeDescriptions (Type,UICultureName,Label) SELECT 'M','nl-NL','Management' WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTypeData WHERE Type = 'M');

DELETE FROM CFGEMRegistrationDescriptions WHERE UICultureName = 'nl-NL';
--.......................................................................................................v(70)
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'ACP'    ,'nl-NL','PMI-ACP'          ,2 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'ACP');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'AWS'    ,'nl-NL','AWS-certificering',5 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'AWS');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'CCNA'   ,'nl-NL','CCNA'             ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'CCNA');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'CompTIA','nl-NL','CompTIA'          ,4 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'CompTIA');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'PMP'    ,'nl-NL','PMP'              ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'PMP');

DELETE FROM CFGEMSkillDescriptions WHERE UICultureName = 'nl-NL';
--..........................................................................................v(70)
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '1','nl-NL','Strategie'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '1');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '2','nl-NL','Executive Coaching',2 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '2');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '3','nl-NL','Gegevensanalyse'     ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '3');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '4','nl-NL','Financiële analyse',4 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '4');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '5','nl-NL','Ideevalidatie'   ,5 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '5');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '6','nl-NL','Engineering'       ,6 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '6');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '7','nl-NL','Risicobeoordeling'   ,7 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '7');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '8','nl-NL','Naar markt'      ,8 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '8');

DELETE FROM CFGEMSkillLevelDescriptions WHERE UICultureName = 'nl-NL';
--.............................................................................................v(50)
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 1,'nl-NL','Basis'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 1);
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 2,'nl-NL','Geavanceerd',2 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 2);
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 3,'nl-NL','Expert'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 3);

DELETE FROM CFGEMSkillUsageDescriptions WHERE UICultureName = 'nl-NL';
--.............................................................................................v(255)
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 1,'nl-NL','Beginner'        ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 1);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 2,'nl-NL','1-2 jaar'    ,2 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 2);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 3,'nl-NL','3-5 jaar'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 3);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 4,'nl-NL','6-10 jaar'   ,4 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 4);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 5,'nl-NL','Meer dan 10 jaar',5 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 5);

DELETE FROM CFGLeadRatingDescriptions WHERE UICultureName = 'nl-NL';
--..............................................................................................v(50)
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '01','nl-NL','Koud',1 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '01');
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '02','nl-NL','Warm',2 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '02');
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '03','nl-NL','Heet' ,3 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '03');

DELETE FROM CFGOpportunityClosedReasonDescriptions WHERE UICultureName = 'nl-NL';
--...........................................................................................................v(50)
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '01','nl-NL','Prijs'    ,1 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '01');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '02','nl-NL','Diensten' ,2 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '02');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '03','nl-NL','Bedrijfsmiddelen',3 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '03');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '04','nl-NL','Timing'   ,4 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '04');

DELETE FROM CFGOpportunitySourceDescriptions WHERE UICultureName = 'nl-NL';
--...........................................................................................................v(50)
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'CRef'    ,'nl-NL','Klantreferentie'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'CRef');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Event'   ,'nl-NL','Gebeurtenis'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Event');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'List'    ,'nl-NL','Lijst'              ,5 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'List');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'PersCont','nl-NL','Persoonlijk contact'  ,6 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'PersCont');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Social'  ,'nl-NL','Social media'      ,7 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Social');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'TLead'   ,'nl-NL','Intellectueel leiderschap',8 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'TLead');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Web'     ,'nl-NL','Web'               ,9 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Web');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'WIQ'     ,'nl-NL','GovWin IQ'         ,3 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'WIQ');

DELETE FROM CFGPrefixDescriptions WHERE UICultureName = 'nl-NL';
--........................................................................................v(10)
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Dr.'  ,'nl-NL','Dr.'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Dr.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Miss' ,'nl-NL','Mevr.' ,2 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Miss');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Mr.'  ,'nl-NL','Dhr.'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Mr.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Mrs.' ,'nl-NL','Mevr.' ,4 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Mrs.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Ms.'  ,'nl-NL','Mevr.'  ,5 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Ms.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Prof.','nl-NL','Prof.',6 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Prof.');

DELETE FROM CFGProbabilityDescriptions WHERE UICultureName = 'nl-NL';
--.....................................................................................................v(50)
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 5,  'nl-NL','05' ,1 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 5);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 10, 'nl-NL','10' ,2 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 10);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 20, 'nl-NL','20' ,3 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 20);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 30, 'nl-NL','30' ,4 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 30);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 40, 'nl-NL','40' ,5 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 40);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 50, 'nl-NL','50' ,6 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 50);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 60, 'nl-NL','60' ,7 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 60);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 70, 'nl-NL','70' ,8 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 70);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 80, 'nl-NL','80' ,9 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 80);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 90, 'nl-NL','90' ,10 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 90);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 95, 'nl-NL','95' ,11 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 95);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 100,'nl-NL','100',12 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 100);

DELETE FROM CFGProjectMilestoneDescriptions WHERE UICultureName = 'nl-NL';
--.......................................................................................................................v(255)
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysEstStart',       'nl-NL','Geschatte start',       1 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysEstStart')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysEstCompletion',  'nl-NL','Geschatte voltooiing',  2 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysEstCompletion')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysContract',       'nl-NL','Toegekend contract',      3 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysContract')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysBidSubmitted',   'nl-NL','Voorstel ingediend',    4 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysBidSubmitted')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysStart',          'nl-NL','Actuele start',          5 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysStart')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysActualCompl',    'nl-NL','Actuele voltooiing',     6 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysActualCompl')


DELETE FROM CFGProjectTypeDescriptions WHERE UICultureName = 'nl-NL';
--...............................................................................................v(50)
INSERT INTO CFGProjectTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','nl-NL','Executive Coaching' ,1 WHERE EXISTS (SELECT 'x' FROM CFGProjectTypeData WHERE Code = '01');
INSERT INTO CFGProjectTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','nl-NL','Productontwikkeling',2 WHERE EXISTS (SELECT 'x' FROM CFGProjectTypeData WHERE Code = '02');
INSERT INTO CFGProjectTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','nl-NL','Risicobeoordeling'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGProjectTypeData WHERE Code = '03');
INSERT INTO CFGProjectTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','nl-NL','Strategie'           ,4 WHERE EXISTS (SELECT 'x' FROM CFGProjectTypeData WHERE Code = '04');
INSERT INTO CFGProjectTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '05','nl-NL','Workshop'           ,5 WHERE EXISTS (SELECT 'x' FROM CFGProjectTypeData WHERE Code = '05');

DELETE FROM CFGProposalSourceDescriptions WHERE UICultureName = 'nl-NL';
--..................................................................................................v(50)
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '01','nl-NL','Klantreferentie'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '01');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '02','nl-NL','Gebeurtenis'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '02');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '03','nl-NL','Lijst'              ,3 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '03');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '04','nl-NL','Persoonlijk contact'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '04');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '05','nl-NL','Social media'      ,5 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '05');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '06','nl-NL','Intellectueel leiderschap',6 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '06');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '07','nl-NL','Website'           ,7 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '07');

DELETE FROM CFGProposalStatusDescriptions WHERE UICultureName = 'nl-NL';
--..................................................................................................v(50)
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','nl-NL','In behandeling',1 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '01');
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','nl-NL','Wordt gecontroleerd'  ,2 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '02');
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','nl-NL','Ingediend'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '03');

DELETE FROM CFGProposalTypeDescriptions WHERE UICultureName = 'nl-NL';
--.........................................................................................v(30)
INSERT INTO CFGProposalTypeDescriptions (Code,UICultureName,Type,Seq) SELECT '01','nl-NL','Snelle offerte'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGProposalTypeData WHERE Code = '01');
INSERT INTO CFGProposalTypeDescriptions (Code,UICultureName,Type,Seq) SELECT '02','nl-NL','Volledig voorstel',2 WHERE EXISTS (SELECT 'x' FROM CFGProposalTypeData WHERE Code = '02');

DELETE FROM CFGPRResponsibilityDescriptions WHERE UICultureName = 'nl-NL';
--....................................................................................................v(80)
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'J' ,'nl-NL','Samenwerking',2 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'J');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'SO','nl-NL','Alleen' ,1 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'SO');

DELETE FROM CFGPYAccrualsDescriptions WHERE UICultureName = 'nl-NL';
--............................................................................................................................v(40)
INSERT INTO CFGPYAccrualsDescriptions (Code,Company,UICultureName,Description) SELECT 'Sick Lv.',CFGMainData.Company,'nl-NL','Ziekteverlof' FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGPYAccrualsData WHERE Code = 'Sick Lv.' AND Company = CFGMainData.Company)
INSERT INTO CFGPYAccrualsDescriptions (Code,Company,UICultureName,Description) SELECT 'Vacation',CFGMainData.Company,'nl-NL','Vakantie'   FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGPYAccrualsData WHERE Code = 'Vacation' AND Company = CFGMainData.Company)

DELETE FROM CFGRGMethodsDescriptions WHERE UICultureName = 'nl-NL';
--..........................................................................................v(40)
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'B','nl-NL','Facturatie'                   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'B');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'N','nl-NL','Geen verantwoording van opbrengsten'     WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'N');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'P','nl-NL','(Pct voltooid*honorarium) + onkostenvergoeding' WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'P');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'W','nl-NL','Facturatie + werk-in-uitvoering @ facturatie'   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'W');

DELETE FROM CFGSuffixDescriptions WHERE UICultureName = 'nl-NL';
--......................................................................................v(150)
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'I'  ,'nl-NL','I'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'I');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'II' ,'nl-NL','II' ,4 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'II');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'III','nl-NL','III',5 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'III');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'Jr.','nl-NL','Jr.',1 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'Jr.');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'Sr.','nl-NL','Sr.',2 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'Sr.');

DELETE FROM CFGUnitTypeDescriptions WHERE UICultureName = 'nl-NL';
--...................................................................................................v(50)
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Equipment','nl-NL','Uitrusting',1 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Equipment');
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Expenses' ,'nl-NL','Onkosten' ,2 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Expenses');
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Labor'    ,'nl-NL','Arbeid'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Labor');

DELETE FROM CFGVendorRoleDescriptions WHERE UICultureName = 'nl-NL';
--..............................................................................................v(50)
INSERT INTO CFGVendorRoleDescriptions (Code,UICultureName,Description,Seq) SELECT '01','nl-NL','Loonlijst'        ,1 WHERE EXISTS (SELECT 'x' FROM CFGVendorRoleData WHERE Code = '01');
INSERT INTO CFGVendorRoleDescriptions (Code,UICultureName,Description,Seq) SELECT '02','nl-NL','Personeelszaken',2 WHERE EXISTS (SELECT 'x' FROM CFGVendorRoleData WHERE Code = '02');
INSERT INTO CFGVendorRoleDescriptions (Code,UICultureName,Description,Seq) SELECT '03','nl-NL','Reisagent'   ,3 WHERE EXISTS (SELECT 'x' FROM CFGVendorRoleData WHERE Code = '03');

DELETE FROM CFGVendorTypeDescriptions WHERE UICultureName = 'nl-NL';
--...................................................................................v(30)
INSERT INTO CFGVendorTypeDescriptions (Type,UICultureName,Label) SELECT 'G','nl-NL','Algemeen'       WHERE EXISTS (SELECT 'x' FROM CFGVendorTypeData WHERE Type = 'G');
INSERT INTO CFGVendorTypeDescriptions (Type,UICultureName,Label) SELECT 'S','nl-NL','Onderaannemer' WHERE EXISTS (SELECT 'x' FROM CFGVendorTypeData WHERE Type = 'S');

DELETE FROM FW_CFGAttachmentCategoryDesc WHERE UICultureName = 'nl-NL';
--................................................................................................................................................v(255)
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Notes'          ,'Activities'         ,'nl-NL','Opmerkingen'                   ,1  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Notes' and Application ='Activities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'NDA'            ,'Contacts'           ,'nl-NL','Geheimhoudingsovereenkomst',2  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'NDA' and Application ='Contacts');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Notes'          ,'Contacts'           ,'nl-NL','Opmerkingen'                   ,3  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Notes' and Application ='Contacts');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'CompanyOverview','TextLibrary'        ,'nl-NL','Bedrijfsoverzicht'        ,4  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'CompanyOverview' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Case Study'     ,'TextLibrary'        ,'nl-NL','Casestudy'              ,5  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Case Study' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Reference'      ,'TextLibrary'        ,'nl-NL','Referentie'               ,6  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Reference' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Resume'         ,'Employees'          ,'nl-NL','CV'                  ,7  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Resume' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Award'          ,'Employees'          ,'nl-NL','Gunning'                   ,8  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Award' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Leadership'     ,'Employees'          ,'nl-NL','Intellectueel leiderschap'      ,9  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Leadership' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Firms'              ,'nl-NL','Masterserviceovereenkomst',10 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Firms'              ,'nl-NL','Werkverklaring'       ,11 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'NDA'            ,'Firms'              ,'nl-NL','Geheimhoudingsovereenkomst',12 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'NDA' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'ClientACH'      ,'Firms'              ,'nl-NL','Klant-ACH'              ,13 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'ClientACH' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'VendorACH'      ,'Firms'              ,'nl-NL','Leverancier-ACH'              ,14 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'VendorACH' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'DigitalArtifact','Marketing Campaigns','nl-NL','Digitaal artefact'        ,15 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'DigitalArtifact' and Application ='Marketing Campaigns');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Estimate'       ,'Opportunities'      ,'nl-NL','Schatting'                ,16 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Estimate' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Proposal'       ,'Opportunities'      ,'nl-NL','Offerte'                ,17 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Proposal' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Opportunities'      ,'nl-NL','Werkverklaring'       ,18 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Opportunities'      ,'nl-NL','Masterserviceovereenkomst',19 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Estimate'       ,'Projects'           ,'nl-NL','Schatting'                ,20 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Estimate' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Proposal'       ,'Projects'           ,'nl-NL','Offerte'                ,21 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Proposal' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Projects'           ,'nl-NL','Werkverklaring'       ,22 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Projects'           ,'nl-NL','Masterserviceovereenkomst',23 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Projects');

DELETE FROM FW_CFGLabelData WHERE UICultureName = 'nl-NL';
--............................................................................................................................................................................v(100)
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','accountLabel'                            ,'Rekening'                       ,'[Account]'                    ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','accountLabelPlural'                      ,'Rekeningen'                      ,'[Accounts]'                   ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','clientLabel'                             ,'Klant'                        ,'[Client]'                     ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','clientLabelPlural'                       ,'Klanten'                       ,'[Clients]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','contactLabel'                            ,'Contactpersoon'                       ,'[Contact]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','contactLabelPlural'                      ,'Contactpersonen'                      ,'[Contacts]'                   ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','employeeLabel'                           ,'Werknemer'                      ,'[Employee]'                   ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','employeeLabelPlural'                     ,'Werknemers'                     ,'[Employees]'                  ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','labcd1Label'                             ,'Arbeidscodeniveau 1'            ,'[Labcd Level 1]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','labcd1LabelPlural'                       ,'Arbeidscodeniveau 1s'           ,'[Labcd Level 1s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','labcd2Label'                             ,'Arbeidscodeniveau 2'            ,'[Labcd Level 2]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','labcd2LabelPlural'                       ,'Arbeidscodeniveau 2s'           ,'[Labcd Level 2s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','labcd3Label'                             ,'Arbeidscodeniveau 3'            ,'[Labcd Level 3]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','labcd3LabelPlural'                       ,'Arbeidscodeniveau 3s'           ,'[Labcd Level 3s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','labcd4Label'                             ,'Arbeidscodeniveau 4'            ,'[Labcd Level 4]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','labcd4LabelPlural'                       ,'Arbeidscodeniveau 4s'           ,'[Labcd Level 4s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','labcd5Label'                             ,'Arbeidscodeniveau 5'            ,'[Labcd Level 5]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','labcd5LabelPlural'                       ,'Arbeidscodeniveau 5s'           ,'[Labcd Level 5s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','labcdLabel'                              ,'Arbeidscode'                    ,'[Labor Code]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','labcdLabelPlural'                        ,'Arbeidscodes'                   ,'[Labor Codes]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','firmLabel'                               ,'Bedrijf'                          ,'[Firm]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','firmLabelPlural'                         ,'Bedrijven'                         ,'[Firms]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','mktLabel'                                ,'Marketinginspanning'              ,'[Marketing Campaign]'         ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','mktLabelPlural'                          ,'Marketinginspanningen'             ,'[Marketing Campaigns]'        ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','org1Label'                               ,'Locatie'                      ,'[Org Level 1]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','org1LabelPlural'                         ,'Locaties'                     ,'[Org Level 1s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','org2Label'                               ,'Afdeling'                    ,'[Org Level 2]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','org2LabelPlural'                         ,'Afdelingen'                   ,'[Org Level 2s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','org3Label'                               ,'Organisatie 3'                ,'[Org Level 3]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','org3LabelPlural'                         ,'Organisatie 3s'               ,'[Org Level 3s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','org4Label'                               ,'Organisatie 4'                ,'[Org Level 4]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','org4LabelPlural'                         ,'Organisatie 4s'               ,'[Org Level 4s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','org5Label'                               ,'Organisatie 5'                ,'[Org Level 5]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','org5LabelPlural'                         ,'Organisatie 5s'               ,'[Org Level 5s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','orgLabel'                                ,'Organisatie'                  ,'[Organization]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','orgLabelPlural'                          ,'Organisaties'                 ,'[Organizations]'              ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','proposalLabel'                           ,'Offerte'                      ,'[Proposal]'                   ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','proposalLabelPlural'                     ,'Offertes'                     ,'[Proposals]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','textLibLabel'                            ,'Boilerplate'                   ,'[Text Library]'               ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','textLibLabelPlural'                      ,'Boilerplates'                  ,'[Text Libraries]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','unitLabel'                               ,'Eenheid'                          ,'[Unit]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','unitLabelPlural'                         ,'Eenheden'                         ,'[Units]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','vendorLabel'                             ,'Leverancier'                        ,'[Vendor]'                     ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','vendorLabelPlural'                       ,'Leveranciers'                       ,'[Vendors]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','wbs1Label'                               ,'Engagement'                    ,'[WBS1]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','wbs1LabelPlural'                         ,'Engagements'                   ,'[WBS1s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','wbs2Label'                               ,'Fase'                         ,'[WBS2]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','wbs2LabelPlural'                         ,'Fasen'                        ,'[WBS2s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','wbs3Label'                               ,'Taak'                          ,'[WBS3]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','wbs3LabelPlural'                         ,'Taken'                         ,'[WBS3s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','sysPM'                                   ,'Engagementmanager'            ,'[Project Manager]'            ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','sysPR'                                   ,'Leidinggevende klant'              ,'[Principal]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','sysSP'                                   ,'Oefenlead'                 ,'[Supervisor]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','equipmentLabel'                          ,'Uitrusting'                     ,'[Equipment]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','equipmentLabelPlural'                    ,'Uitrusting'                     ,'[Equipments]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','jobToDateLabel'                          ,'Engagement-tot-nu'            ,'[JobToDate]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','JTDLabel'                                ,'ETD'                           ,'[JTD]'                        ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','estimateToCompleteLabel'                 ,'Inschatting-tot-voltooiing'          ,'[EstimateToComplete]'         ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','ETCLabel'                                ,'ETC'                           ,'[ETC]'                        ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','estimateAtCompletionLabel'               ,'Inschatting-bij-voltooiing'        ,'[EstimateAtCompletion]'       ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','EACLabel'                                ,'EAC'                           ,'[EAC]'                        ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','sysPRM'                                  ,'Offertemanager'              ,'[Proposal Manager]'           ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','sysMC'                                   ,'Marketing Coördinator'         ,'[Marketing Coordinator]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','sysBDL'                                  ,'Lead voor zakelijke ontwikkeling'     ,'[Business Development Lead]'  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','consultantLabel'                         ,'Adviseur'                    ,'[Consultant]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','consultantLabelPlural'                   ,'Adviseurs'                   ,'[Consultants]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','compensationLabel'                       ,'Compensatie'                  ,'[Compensation]'               ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','revType1Label'                           ,'Arbeid'                         ,'[Revenue Method Type 1]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','revType2Label'                           ,'Adviseur'                    ,'[Revenue Method Type 2]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','revType3Label'                           ,'Vergoeding'                         ,'[Revenue Method Type 3]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','revType4Label'                           ,'Inkomstenmethode 4'              ,'[Revenue Method Type 4]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','revType5Label'                           ,'Inkomstenmethode 5'              ,'[Revenue Method Type 5]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','feeEstimatingCostGroupLabel'             ,'Kostengroep'                    ,'[Cost Group]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','feeEstimatingCostGroupLabelPlural'       ,'Kostengroepen'                   ,'[Cost Groups]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','feeEstimatingFunctionalGroupLabel'       ,'Functionele groep'              ,'[Functional Group]'           ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','feeEstimatingFunctionalGroupLabelPlural' ,'Functionele groepen'             ,'[Functional Groups]'          ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','feeEstimatingServiceProfileLabel'        ,'Serviceprofiel'               ,'[Service Profile]'            ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('nl-NL','feeEstimatingServiceProfileLabelPlural'  ,'Serviceprofielen'              ,'[Service Profiles]'           ,'N','N',NULL);

COMMIT TRANSACTION
    
END
GO
