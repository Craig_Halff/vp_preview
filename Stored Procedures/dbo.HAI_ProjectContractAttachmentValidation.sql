SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[HAI_ProjectContractAttachmentValidation] @WBS1 VARCHAR(15)
AS
    BEGIN
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
        SET NOCOUNT ON;
        /****************************************************************************
			Copyright (c) 2021 Halff Associates Inc. All rights reserved.
			Check to ensure that there is a contract attached, the Phase-Team
			Grid has been populated and Collections Responsibility has been
			assigned before Project Structure can be created.

			08/18/2021	Jeremy Baummer - Created
			08/19/1021	Craig H. Anderson - Added type 003
			08/24/2021	Craig H. Anderson - Added exclusion foir Administrative Qualifications
			10/15/2021  Jeremy Baummer - Changed wording in error to reflect 
										 contract being required in Create AVO stage.
	*****************************************************************************/

        -- Look for contract 002 or mgt approval 008, or 003 Executed Contracts attachment types
        IF NOT EXISTS (
            SELECT 1
            FROM dbo.FW_Attachments WITH (NOLOCK)
            WHERE
                Key1 = @WBS1
                AND CategoryCode IN ( '002', '008', '003' )
        )
            AND NOT EXISTS (
            SELECT 1
            FROM dbo.ProjectCustomTabFields AS px
            WHERE
                px.WBS1 = @WBS1
                AND px.CustContractType = 'Administrative Qualification'
        )
            RAISERROR('<h3>A contract is required before converting to an AVO. Return to Selected/Negotiating 
			stage, upload your contract or management team work authorization, save and return to Create AVO.</h3>', 16, 1);
    END;

GO
