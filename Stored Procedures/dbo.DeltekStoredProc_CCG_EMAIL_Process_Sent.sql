SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EMAIL_Process_Sent]
	@msgSeqList		varchar(max)
AS
BEGIN
	/*
	select * from CCG_EMAIL_Messages
	select * from CCG_EMAIL_HISTORY order by seq desc
	DeltekStoredProc_CCG_EMAIL_PROCESS_Sent
		@msgSeqList	= '12,13'
 	*/
	SET NOCOUNT ON;
	
	--Use Replace to handle single quotes passed in to reduce SQL injection risk

	DECLARE @sSQL		varchar(max);

	set @sSQL = 'delete from CCG_Email_Messages where Seq in (' + Replace(@msgSeqList,'''','') + ');select @@ROWCOUNT as msgrowcount; delete from CCG_Email_Attachments where  MessageSeq in (' + Replace(@msgSeqList,'''','') + ') and KeepAfterSent <> ''Y'';select @@ROWCOUNT as attachrowcount; '
	exec(@sSQL)

	BEGIN TRY
		IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CCG_Email_Log]') AND type in (N'U'))
		BEGIN
		 exec('insert into CCG_Email_Log (Action,DETAIL) values (''PROCESS_SENT'',''' + @msgSeqList + ''')')
		END
	END TRY
	BEGIN CATCH
		PRINT 'TRY LOG ERR:' + ERROR_MESSAGE()
	END CATCH
--print ISNULL(@sSQL,'NULL')
END

GO
