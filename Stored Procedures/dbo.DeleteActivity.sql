SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteActivity] @ActivityID Nvarchar(32)
AS
BEGIN
	 DELETE FROM EMActivity where ActivityID = @ActivityID
	 DELETE FROM CLActivity where ActivityID = @ActivityID
	 DELETE FROM EMActivityOccurrence where ActivityID = @ActivityID
	 DELETE FROM ActivitySubscr where ActivityID = @ActivityID
	 DELETE FROM ContactActivity where ActivityID = @ActivityID
	 DELETE FROM RecurrActivityException where ActivityID = @ActivityID
	 Update RPAssignment Set ActivityID=null  where ActivityID = @ActivityID
	 DELETE FROM ActivityCustomTabFields where ActivityID = @ActivityID
	 	 
	 --Delete UDIC associations
	 SELECT UDIC_ID INTO #UDICs FROM FW_UDIC, FW_CustomGrids WHERE UDIC_ID = InfoCenterArea AND GridType = 'A'
	 DECLARE @UDIC_ID  Nvarchar(32)
	 WHILE EXISTS (select UDIC_ID from #UDICs)
	 BEGIN
		 SELECT TOP 1 @UDIC_ID = UDIC_ID FROM #UDICs ORDER BY UDIC_ID

			 EXECUTE('DELETE FROM ' + @UDIC_ID + 'Activity WHERE ActivityID = ''' + @ActivityID + '''')

		 DELETE #UDICs WHERE UDIC_ID = @UDIC_ID
	 END

	 DROP TABLE #UDICs

	 EXEC DeleteCustomGridTabData 'Activity', @ActivityID, '', '' --Delete UDIC associations
--Check for  Parent Activity 
	 DECLARE @ParentActivityID AS Varchar(32),	 @MaxOccurrence Int;

	 SELECT @ParentActivityID = Parent.ActivityID , @MaxOccurrence = Parent.MaxOccurences 
	 FROM Activity Child INNER JOIN Activity Parent ON Child.RecurrID=Parent.ActivityID
	 WHERE Child.ActivityID=@ActivityID AND Parent.RecurrenceInd='Y' AND Parent.RecurrEndType = 'A'
	 
	 If @ParentActivityID IS NOT NULL
	 BEGIN
		IF @MaxOccurrence > 1 
		BEGIN 
			UPDATE Activity Set MaxOccurences = MaxOccurences -1 WHERE ActivityID = @ParentActivityID 
		END
		ELSE
		BEGIN /* Delete Parent Activity Since no child exists */
			EXEC DeleteActivity @ActivityID = @ParentActivityID ;

			DELETE ACTIVITY WHERE ActivityID = @ParentActivityID
		END
	 END

	 -- Update Child Activity 
	 UPDATE Activity SET RecurrID = NULL , RecurrType = NULL WHERE RecurrID = @ActivityID
END
GO
