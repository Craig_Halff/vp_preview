SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpReCalc]
  @strPlanID VARCHAR(32)
AS

BEGIN -- Procedure rpReCalc

  SET NOCOUNT ON

  DECLARE @strNextPlanID varchar(32)
  DECLARE @intTaskCount int
  DECLARE @CS_Plan_Original int
  DECLARE @CS_Plan_ReCalc int
  
  DECLARE @strPlanName Nvarchar(255)
  
  DECLARE @strProjectedMultiplier varchar(20)
  DECLARE @strProjectedRatio varchar(20) 
  DECLARE @strPlannedLaborHrs varchar(20) 
  DECLARE @strPlannedLabCost varchar(20) 
  DECLARE @strPlannedLabBill varchar(20) 
  DECLARE @strPlannedExpCost varchar(20) 
  DECLARE @strPlannedExpBill varchar(20) 
  DECLARE @strPlannedConCost varchar(20) 
  DECLARE @strPlannedConBill varchar(20) 
  DECLARE @strLabRevenue varchar(20)
  DECLARE @strExpRevenue varchar(20) 
  DECLARE @strConRevenue varchar(20) 
  DECLARE @strPlannedDirExpCost varchar(20) 
  DECLARE @strPlannedDirExpBill varchar(20) 
  DECLARE @strPlannedDirConCost varchar(20) 
  DECLARE @strPlannedDirConBill varchar(20)
  
  DECLARE @strProjectedMultiplier_R varchar(20)
  DECLARE @strProjectedRatio_R varchar(20) 
  DECLARE @strPlannedLaborHrs_R varchar(20) 
  DECLARE @strPlannedLabCost_R varchar(20) 
  DECLARE @strPlannedLabBill_R varchar(20) 
  DECLARE @strPlannedExpCost_R varchar(20) 
  DECLARE @strPlannedExpBill_R varchar(20) 
  DECLARE @strPlannedConCost_R varchar(20) 
  DECLARE @strPlannedConBill_R varchar(20) 
  DECLARE @strLabRevenue_R varchar(20)
  DECLARE @strExpRevenue_R varchar(20) 
  DECLARE @strConRevenue_R varchar(20) 
  DECLARE @strPlannedDirExpCost_R varchar(20) 
  DECLARE @strPlannedDirExpBill_R varchar(20) 
  DECLARE @strPlannedDirConCost_R varchar(20) 
  DECLARE @strPlannedDirConBill_R varchar(20)
  
  DECLARE csrRPPlan CURSOR FOR
    SELECT PlanID FROM RPPlan WHERE PlanID LIKE @strPlanID

  OPEN csrRPPlan

  FETCH NEXT FROM csrRPPlan INTO @strNextPlanID

  WHILE (@@FETCH_STATUS = 0)
    BEGIN
    
      SELECT @intTaskCount = COUNT(*) FROM RPTask
        WHERE PlanID  = @strNextPlanID
        
      IF (@intTaskCount > 0)
        BEGIN
        
          -- Calculate Checksum of original Plan.
  
          SELECT @CS_Plan_Original = 
            BINARY_CHECKSUM(PlannedLaborHrs, 
                            PlannedLabCost, 
                            PlannedLabBill, 
                            LabRevenue, 
                            PlannedExpCost, 
                            PlannedExpBill, 
                            ExpRevenue, 
                            PlannedDirExpCost, 
                            PlannedDirExpBill, 
                            PlannedConCost, 
                            PlannedConBill, 
                            ConRevenue, 
                            PlannedDirConCost, 
                            PlannedDirConBill,
                            ProjectedMultiplier,
                            ProjectedRatio),
            @strProjectedMultiplier = ProjectedMultiplier,
            @strProjectedRatio = ProjectedRatio, 
            @strPlannedLaborHrs = PlannedLaborHrs, 
            @strPlannedLabCost = PlannedLabCost, 
            @strPlannedLabBill = PlannedLabBill, 
            @strPlannedExpCost = PlannedExpCost, 
            @strPlannedExpBill = PlannedExpBill, 
            @strPlannedConCost = PlannedConCost, 
            @strPlannedConBill = PlannedConBill, 
            @strLabRevenue = LabRevenue,
            @strExpRevenue = ExpRevenue, 
            @strConRevenue = ConRevenue, 
            @strPlannedDirExpCost = PlannedDirExpCost, 
            @strPlannedDirExpBill = PlannedDirExpBill, 
            @strPlannedDirConCost = PlannedDirConCost, 
            @strPlannedDirConBill = PlannedDirConBill
 
            FROM RPPlan
            WHERE PlanID = @strNextPlanID
 
          EXEC rpDeleteSummaryTPD @strNextPlanID, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'
          EXEC rpFixDateTPD @strNextPlanID
          EXEC rpConsolidateOverflowTPD @strNextPlanID
          EXEC rpCalcTPD @strNextPlanID, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'
          EXEC rpSumUpTPD @strNextPlanID, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'
          EXEC rpChecksum @strNextPlanID
          
          -- Calculate Checksum of Plan after recalculation.

          SELECT @CS_Plan_ReCalc = 
            BINARY_CHECKSUM(PlannedLaborHrs, 
                            PlannedLabCost, 
                            PlannedLabBill, 
                            LabRevenue, 
                            PlannedExpCost, 
                            PlannedExpBill, 
                            ExpRevenue, 
                            PlannedDirExpCost, 
                            PlannedDirExpBill, 
                            PlannedConCost, 
                            PlannedConBill, 
                            ConRevenue, 
                            PlannedDirConCost, 
                            PlannedDirConBill,
                            ProjectedMultiplier,
                            ProjectedRatio),
            @strPlanName = PlanName,
            @strProjectedMultiplier_R = ProjectedMultiplier,
            @strProjectedRatio_R = ProjectedRatio, 
            @strPlannedLaborHrs_R = PlannedLaborHrs, 
            @strPlannedLabCost_R = PlannedLabCost, 
            @strPlannedLabBill_R = PlannedLabBill, 
            @strPlannedExpCost_R = PlannedExpCost, 
            @strPlannedExpBill_R = PlannedExpBill, 
            @strPlannedConCost_R = PlannedConCost, 
            @strPlannedConBill_R = PlannedConBill, 
            @strLabRevenue_R = LabRevenue,
            @strExpRevenue_R = ExpRevenue, 
            @strConRevenue_R = ConRevenue, 
            @strPlannedDirExpCost_R = PlannedDirExpCost, 
            @strPlannedDirExpBill_R = PlannedDirExpBill, 
            @strPlannedDirConCost_R = PlannedDirConCost, 
            @strPlannedDirConBill_R = PlannedDirConBill
            FROM RPPlan
            WHERE PlanID = @strNextPlanID
          
          IF (@CS_Plan_ReCalc <> @CS_Plan_Original)
            BEGIN
            
              PRINT '*** Plan "' + @strPlanName + '" was changed during recalculation.'
              PRINT '      PlanID = ' + @strNextPlanID
              
              PRINT CASE WHEN @strProjectedMultiplier = @strProjectedMultiplier_R 
                         THEN '-----' 
                         ELSE '-->>>' 
                         END + ' Projected Multiplier (Old, New) = ' + @strProjectedMultiplier + ', ' + @strProjectedMultiplier_R 
              PRINT CASE WHEN @strProjectedRatio = @strProjectedRatio_R 
                         THEN '-----' 
                         ELSE '-->>>' 
                         END + ' Projected Ratio (Old, New) = ' + @strProjectedRatio + ', ' + @strProjectedRatio_R 
              PRINT CASE WHEN @strPlannedLaborHrs = @strPlannedLaborHrs_R
                         THEN '-----' 
                         ELSE '-->>>' 
                         END + ' Planned Hrs (Old, New) = ' + @strPlannedLaborHrs + ', ' + @strPlannedLaborHrs_R 
              PRINT CASE WHEN @strPlannedLabCost = @strPlannedLabCost_R
                         THEN '-----' 
                         ELSE '-->>>' 
                         END + ' Planned Labor Cost (Old, New) = ' + @strPlannedLabCost + ', ' + @strPlannedLabCost_R 
              PRINT CASE WHEN @strPlannedLabBill = @strPlannedLabBill_R
                         THEN '-----' 
                         ELSE '-->>>' 
                         END + ' Planned Labor Bill (Old, New) = ' + @strPlannedLabBill + ', ' + @strPlannedLabBill_R 
              PRINT CASE WHEN @strPlannedExpCost = @strPlannedExpCost_R 
                         THEN '-----' 
                         ELSE '-->>>' 
                         END + ' Planned Expense Cost (Old, New) = ' + @strPlannedExpCost + ', ' + @strPlannedExpCost_R 
              PRINT CASE WHEN @strPlannedExpBill = @strPlannedExpBill_R
                         THEN '-----' 
                         ELSE '-->>>' 
                         END + ' Planned Expense Bill (Old, New) = ' + @strPlannedExpBill + ', ' + @strPlannedExpBill_R 
              PRINT CASE WHEN @strPlannedConCost = @strPlannedConCost_R
                         THEN '-----' 
                         ELSE '-->>>' 
                         END + ' Planned Consultant Cost (Old, New) = ' + @strPlannedConCost + ', ' + @strPlannedConCost_R 
              PRINT CASE WHEN @strPlannedConBill = @strPlannedConBill_R 
                         THEN '-----' 
                         ELSE '-->>>' 
                         END + ' PlanConsultant Bill (Old, New) = ' + @strPlannedConBill + ', ' + @strPlannedConBill_R 
              PRINT CASE WHEN @strLabRevenue = @strLabRevenue_R
                         THEN '-----' 
                         ELSE '-->>>' 
                         END + ' Planned Labor Revenue (Old, New) = ' + @strLabRevenue + ', ' + @strLabRevenue_R
              PRINT CASE WHEN @strExpRevenue = @strExpRevenue_R
                         THEN '-----' 
                         ELSE '-->>>' 
                         END + ' Planned Expense Revenue (Old, New) = ' + @strExpRevenue + ', ' + @strExpRevenue_R 
              PRINT CASE WHEN @strConRevenue = @strConRevenue_R
                         THEN '-----' 
                         ELSE '-->>>' 
                         END + ' Planned Consultant Revenue (Old, New) = ' + @strConRevenue + ', ' + @strConRevenue_R 
              PRINT CASE WHEN @strPlannedDirExpCost = @strPlannedDirExpCost_R
                         THEN '-----' 
                         ELSE '-->>>' 
                         END + ' Planned Direct Expense Cost (Old, New) = ' + @strPlannedDirExpCost + ', ' + @strPlannedDirExpCost_R 
              PRINT CASE WHEN @strPlannedDirExpBill = @strPlannedDirExpBill_R 
                         THEN '-----' 
                         ELSE '-->>>' 
                         END + ' Planned Direct Expense Bill (Old, New) = ' + @strPlannedDirExpBill + ', ' + @strPlannedDirExpBill_R 
              PRINT CASE WHEN @strPlannedDirConCost = @strPlannedDirConCost_R 
                         THEN '-----' 
                         ELSE '-->>>' 
                         END + ' Planned Direct Consultant Cost (Old, New) = ' + @strPlannedDirConCost + ', ' + @strPlannedDirConCost_R 
              PRINT CASE WHEN @strPlannedDirConBill= @strPlannedDirConBill_R
                         THEN '-----' 
                         ELSE '-->>>' 
                         END + ' Planned Direct Consultant Bill (Old, New) = ' + @strPlannedDirConBill + ', ' + @strPlannedDirConBill_R
                
            END -- End If   
          
        END -- If
      
      FETCH NEXT FROM csrRPPlan INTO @strNextPlanID

    END -- While

  CLOSE csrRPPlan
  DEALLOCATE csrRPPlan
 
  SET NOCOUNT OFF

END -- rpReCalc
GO
