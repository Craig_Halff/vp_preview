SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectBillingTermsMiscUpdate]
   @WBS1        varchar(32), 
   @WBS2        varchar(7), 
   @WBS3        varchar(7),
   @TaxCodeOld  varchar (225)
AS
/*
Copyright (c) 2019 Central Consulting Group. All rights reserved.
09/23/2019	David Springer
			Update billing terms when custom fields have changed.
10/14/2019	David Springer
			Incorporated BillingTermsShowTaskBreakdown & BillingTermsMultipliers
03/10/2020	David Springer
			Removed mulitpliers to dedicated procedures
*/
BEGIN
SET NOCOUNT ON
--	Update Tax Code
	Delete From BTTaxCodes
	Where WBS1 = @WBS1
	  and WBS2 = @WBS2
	  and WBS3 = @WBS3
	  and TaxCode = @TaxCodeOld

 -- Add Tax Code
	Insert Into BTTaxCodes
	(WBS1, WBS2, WBS3, Seq, TaxCode)
	Select WBS1, WBS2, WBS3, 1, CustTaxCode
	From ProjectCustomTabFields p
	Where WBS1 = @WBS1
	  and WBS2 = @WBS2
	  and WBS3 = @WBS3
	  and custTaxCode is not null
	  and not exists (Select 'x' From BTTaxCodes 
					Where WBS1 = p.WBS1
					  and WBS2 = p.WBS2
					  and WBS3 = p.WBS3
					  and TaxCode = p.CustTaxCode)

   If @WBS2 <> ' ' and @WBS3 = ' '
      Update BT
	  Set TaxBySubLevel = 'Y'
	  Where WBS1 = @WBS1
        and WBS2 = ' ' -- project level

   If @WBS3 <> ' '
      Update BT
	  Set TaxBySubLevel = 'Y'
	  Where WBS1 = @WBS1
	    and WBS2 = @WBS2
        and WBS3 = ' ' -- phase level

	Update b
	Set b.Notes = dbo.CCG_StripHTMLTag(px.CustBillingTermNotes,'')
	From ProjectCustomTabFields px, BT b
   Where b.WBS1 = @WBS1
     and b.WBS2 = @WBS2
     and b.WBS3 = @WBS3
     and b.WBS1 = px.WBS1
     and b.WBS2 = px.WBS2
     and b.WBS3 = px.WBS3
	 and px.CustBillingTermNotes is not null

     Update px
	 Set px.CustBillingTermNotes = Replace(dbo.fnCCG_ConvertHtml_LeaveNewLines(replace(px.CustBillingTermNotes, CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10), CHAR(13)+CHAR(10))),
			CHAR(13)+CHAR(10), '<br/>')
	From ProjectCustomTabFields px, BT b
   Where b.WBS1 = @WBS1
     and b.WBS2 = @WBS2
     and b.WBS3 = @WBS3
     and b.WBS1 = px.WBS1
     and b.WBS2 = px.WBS2
     and b.WBS3 = px.WBS3
	 and px.CustBillingTermNotes is not null

	Update b
	Set b.Description = Replace(dbo.fnCCG_ConvertHtml_LeaveNewLines(replace(px.CustBillingTermDescription, CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10), CHAR(13)+CHAR(10))),
			CHAR(13)+CHAR(10), '<br/>')
	From ProjectCustomTabFields px, BT b
   Where b.WBS1 = @WBS1
     and b.WBS2 = @WBS2
     and b.WBS3 = @WBS3
     and b.WBS1 = px.WBS1
     and b.WBS2 = px.WBS2
     and b.WBS3 = px.WBS3
	 and px.CustBillingTermDescription is not null

	 Update px
	 Set px.CustBillingTermDescription = Replace(dbo.fnCCG_ConvertHtml_LeaveNewLines(replace(px.CustBillingTermDescription, CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10), CHAR(13)+CHAR(10))),
			CHAR(13)+CHAR(10), '<br/>')
	From ProjectCustomTabFields px, BT b
   Where b.WBS1 = @WBS1
     and b.WBS2 = @WBS2
     and b.WBS3 = @WBS3
     and b.WBS1 = px.WBS1
     and b.WBS2 = px.WBS2
     and b.WBS3 = px.WBS3
	 and px.CustBillingTermDescription is not null

-- Show Task Breakdown
   If @WBS2 = ' '
	Begin
      Update b
      Set b.ConsolidateWBS3 = CASE When p.CustShowTaskBreakdown = 'Y' Then 'N' Else 'Y' END
      From BT b, ProjectCustomTabFields p
      Where b.WBS1 = @WBS1
        and b.WBS2 = @WBS2
        and b.WBS3 = @WBS3
        and b.WBS1 = p.WBS1
        and b.WBS2 = p.WBS2
        and b.WBS3 = p.WBS3

   -- Change phase & task levels to match
      Update p2
      Set p2.CustShowTaskBreakdown = p.CustShowTaskBreakdown
      From ProjectCustomTabFields p2, ProjectCustomTabFields p
      Where p.WBS1 = @WBS1
        and p.WBS2 = @WBS2
		and p.WBS1 = p2.WBS1
		and p2.WBS2 <> @WBS2
		and p2.CustBillingType is not null -- only check the box where there are billing terms

      Update b
      Set b.ConsolidateWBS3 = CASE When p.CustShowTaskBreakdown = 'Y' Then 'N' Else 'Y' END
      From BT b, ProjectCustomTabFields p
      Where p.WBS1 = @WBS1
        and p.WBS2 = @WBS2
		and p.WBS1 = b.WBS1
	End

   If @WBS2 <> ' '
	Begin
      Update b
      Set b.ConsolidatePrinting = CASE When p.CustShowTaskBreakdown = 'Y' Then 'N' Else 'Y' END
      From BT b, ProjectCustomTabFields p
      Where b.WBS1 = @WBS1
        and b.WBS2 = @WBS2
        and b.WBS3 = @WBS3
        and b.WBS1 = p.WBS1
        and b.WBS2 = p.WBS2
        and b.WBS3 = p.WBS3

   -- Change task levels to match
      Update p2
      Set p2.CustShowTaskBreakdown = p.CustShowTaskBreakdown
      From ProjectCustomTabFields p2, ProjectCustomTabFields p
      Where p.WBS1 = @WBS1
        and p.WBS2 = @WBS2
		and p.WBS3 = ' '
		and p.WBS1 = p2.WBS1
		and p.WBS2 = p2.WBS2
		and p2.WBS3 <> ' '
		and p2.CustBillingType is not null -- only check the box where there are billing terms

      Update b
      Set b.ConsolidatePrinting = CASE When p.CustShowTaskBreakdown = 'Y' Then 'N' Else 'Y' END
      From BT b, ProjectCustomTabFields p
      Where p.WBS1 = @WBS1
	    and p.WBS2 = @WBS2
		and p.WBS3 = ' '
		and p.WBS1 = b.WBS1
		and p.WBS2 = b.WBS2
	End

END
GO
