SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spCCG_PAT_FileHelper_getXFDFDB]
      @payableSeq int,
      @revisionSeq int
AS
BEGIN
      SET NOCOUNT ON;
      select * from CCG_PAT_XFDF where PayableSeq = @payableSeq and RevisionSeq = @revisionSeq;
END;

GO
