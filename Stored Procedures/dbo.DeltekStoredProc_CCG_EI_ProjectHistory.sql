SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_ProjectHistory] ( @WBS1 nvarchar(30), @Language varchar(10)= 'en-US', @Username nvarchar(32)= null)
             AS EXEC spCCG_EI_ProjectHistory @WBS1,@Language,@Username
GO
