SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[LoadIndustryDefaultsAE]
AS
BEGIN

PRINT ' '
PRINT 'Loading AE Industry Defaults'

-- variables used at the bottom to load each enabled language
DECLARE 
	@enUSEnabled varchar(1) = 'N',
	@enGBEnabled varchar(1) = 'N',
	@deDEEnabled varchar(1) = 'N',
	@esESEnabled varchar(1) = 'N',
	@frCAEnabled varchar(1) = 'N',
	@frFREnabled varchar(1) = 'N',
	@nlNLEnabled varchar(1) = 'N',
	@ptBREnabled varchar(1) = 'N'

IF EXISTS (SELECT 'x' FROM FW_CFGLabelData WHERE UICultureName = 'en-US') SET @enUSEnabled = 'Y'
IF EXISTS (SELECT 'x' FROM FW_CFGLabelData WHERE UICultureName = 'en-GB') SET @enGBEnabled = 'Y'
IF EXISTS (SELECT 'x' FROM FW_CFGLabelData WHERE UICultureName = 'de-DE') SET @deDEEnabled = 'Y'
IF EXISTS (SELECT 'x' FROM FW_CFGLabelData WHERE UICultureName = 'es-ES') SET @esESEnabled = 'Y'
IF EXISTS (SELECT 'x' FROM FW_CFGLabelData WHERE UICultureName = 'fr-CA') SET @frCAEnabled = 'Y'
IF EXISTS (SELECT 'x' FROM FW_CFGLabelData WHERE UICultureName = 'fr-FR') SET @frFREnabled = 'Y'
IF EXISTS (SELECT 'x' FROM FW_CFGLabelData WHERE UICultureName = 'nl-NL') SET @nlNLEnabled = 'Y'
IF EXISTS (SELECT 'x' FROM FW_CFGLabelData WHERE UICultureName = 'pt-BR') SET @ptBREnabled = 'Y'

BEGIN TRANSACTION

DELETE FROM BTDefaults;
INSERT INTO BTDefaults (DefaultType,Company,UseClientAddress,Address1,Address2,Address3,Address4,City,State,ZIP,Country,Description,Notes,Template,NextInvoice,DraftPrint,LabMeth,Mult1,Mult2,Mult3,SeparateOvt,OvtMult,RateTable,ORTable,LabDetail,ShowComment,ShowRate,ShowDate,ShowMults,ShowOvtMult,FlagOverrides,SortMeth1,SortTable1,SortMeth2,SortTable2,ExpMeth,ExpMult,ExpTable,ExpShowMult,ExpDetail,ConMeth,ConMult,ConTable,ConShowMult,ConDetail,UnitMeth,UnitMult,UnitShowMult,UnitDetail,FeeMeth,FeeBasis,FeeFactor1,FeeFactor2,FeePctCpl,FeeLabel,PriorFee,AddOns,PrintWBS1AR,Interest,GracePd,PrintWBS1BTD,LimitMeth,LabLimit,ConLimit,ExpLimit,UnitLimit,AddOnLimit,FeeLimit,Retainage,RetPct,RetLimit,RetLab,RetExp,RetCon,RetFee,RetUnit,RetAddOn,ConsolidatePrinting,ConsolidateWBS3,SubLevelTerms,ConsolidatePosting,WBS2ToPost,WBS3ToPost,SubLevelRollup,WBS3Rollup,LimitsBySubLevel,PrintARBreakout,PrintARWBS2Totals,PrintBTDBreakout,PrintBTDWBS2Totals,PrintWBS2AR,PrintWBS2BTD,InterestBySubLevel,TaxBySubLevel,RetainageBySubLevel,BackupReport,BackupComments,BackupVendorInvoice,LabAccount,ConAccount,ExpAccount,UnitAccount,FeeAccount,AddOnAccount,IntAccount,Footer,LabDefault,ExpDefault,ConDefault,UnitDefault,PostFeesByPhase,SpecialOvtMult,BackupLaborCode,BackupEKDescription,PrintBTDReceivedTotal,PrintBTDARBalanceTotal,IntercompanyTax,IntercompanyTaxWBS2ToPost,IntercompanyTaxWBS3ToPost,printLaborForFixedFee,BackupLabor,BackupLaborCategory,BackupEmployeeNumber,BackupLaborSort,BackupLaborTable,BackupExpense,BackupAPDescription,BackupMileage,BackupTransType,BackupExpenseSort,BackupExpenseTable,BackupEXDescription,BackupLabDefault,DraftInvoice,ScheduledBilling,ConsolidateTaxPosting,TaxWBS2ToPost,TaxWBS3ToPost,TaxRetainage,FeeByDetailEnabled,OverUnderAccount,FeeToDate,Fee1,FeePctCpl1,PriorFee1,FeeToDate1,Fee2,FeePctCpl2,PriorFee2,FeeToDate2,Fee3,FeePctCpl3,PriorFee3,FeeToDate3,Fee4,FeePctCpl4,PriorFee4,FeeToDate4,Fee5,FeePctCpl5,PriorFee5,FeeToDate5,CreditTemplate,InvoiceApprovalEnabled,InvoiceApprovalCode,SupportDocuments,SupportDocumentsEX,SupportDocumentsAP,SupportDocumentsBkupRpt,DaysBeforeDue,PayTerms,ShowDueDate,PreInvoice,PreInvTemplate,CancelTemplate,PreInvNote,PreInvWBS2ToPost,PreInvWBS3ToPost,ExpPODetail,ConPODetail,SupportDocumentsUN,EmpTitle,EmpTitleTable,BackupEmpTitle,BackupEmpTitleTable,BackupExpDefault,BackupFees,BackupAddOns)VALUES('<D>',' ','N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.0000,'N',0,0.0000,0.0000,0.0000,0,0.0000,0,0,'Y','N','N','N','Y','Y','N',0,0,0,0,0,1.0000,0,'Y','N',0,1.0000,0,'Y','N',0,1.0000,'Y','N',0,NULL,0.0000,0.0000,0.0000,NULL,0.0000,'N','Y',0.0000,0.0000,'N','0',0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,'N',0.0000,0.0000,'Y','Y','Y','Y','Y','Y','N','N','N','N',NULL,NULL,0,0,'N','N','N','N','N','N','N','N','N','N','N','N','N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N','N','N','N','N',0.0000,'N','N','N','N','N',NULL,NULL,'B','Y','N','Y',0,0,'Y','Y','N','Y',0,0,'Y','N',NULL,'N','N',NULL,NULL,'W','N',NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,'N',NULL,'N','N','N','Y',0,'I','N','N',NULL,NULL,NULL,NULL,NULL,'N','N','N',0,0,0,0,'N','N','N');
INSERT INTO BTDefaults (DefaultType,Company,UseClientAddress,Address1,Address2,Address3,Address4,City,State,ZIP,Country,Description,Notes,Template,NextInvoice,DraftPrint,LabMeth,Mult1,Mult2,Mult3,SeparateOvt,OvtMult,RateTable,ORTable,LabDetail,ShowComment,ShowRate,ShowDate,ShowMults,ShowOvtMult,FlagOverrides,SortMeth1,SortTable1,SortMeth2,SortTable2,ExpMeth,ExpMult,ExpTable,ExpShowMult,ExpDetail,ConMeth,ConMult,ConTable,ConShowMult,ConDetail,UnitMeth,UnitMult,UnitShowMult,UnitDetail,FeeMeth,FeeBasis,FeeFactor1,FeeFactor2,FeePctCpl,FeeLabel,PriorFee,AddOns,PrintWBS1AR,Interest,GracePd,PrintWBS1BTD,LimitMeth,LabLimit,ConLimit,ExpLimit,UnitLimit,AddOnLimit,FeeLimit,Retainage,RetPct,RetLimit,RetLab,RetExp,RetCon,RetFee,RetUnit,RetAddOn,ConsolidatePrinting,ConsolidateWBS3,SubLevelTerms,ConsolidatePosting,WBS2ToPost,WBS3ToPost,SubLevelRollup,WBS3Rollup,LimitsBySubLevel,PrintARBreakout,PrintARWBS2Totals,PrintBTDBreakout,PrintBTDWBS2Totals,PrintWBS2AR,PrintWBS2BTD,InterestBySubLevel,TaxBySubLevel,RetainageBySubLevel,BackupReport,BackupComments,BackupVendorInvoice,LabAccount,ConAccount,ExpAccount,UnitAccount,FeeAccount,AddOnAccount,IntAccount,Footer,LabDefault,ExpDefault,ConDefault,UnitDefault,PostFeesByPhase,SpecialOvtMult,BackupLaborCode,BackupEKDescription,PrintBTDReceivedTotal,PrintBTDARBalanceTotal,IntercompanyTax,IntercompanyTaxWBS2ToPost,IntercompanyTaxWBS3ToPost,printLaborForFixedFee,BackupLabor,BackupLaborCategory,BackupEmployeeNumber,BackupLaborSort,BackupLaborTable,BackupExpense,BackupAPDescription,BackupMileage,BackupTransType,BackupExpenseSort,BackupExpenseTable,BackupEXDescription,BackupLabDefault,DraftInvoice,ScheduledBilling,ConsolidateTaxPosting,TaxWBS2ToPost,TaxWBS3ToPost,TaxRetainage,FeeByDetailEnabled,OverUnderAccount,FeeToDate,Fee1,FeePctCpl1,PriorFee1,FeeToDate1,Fee2,FeePctCpl2,PriorFee2,FeeToDate2,Fee3,FeePctCpl3,PriorFee3,FeeToDate3,Fee4,FeePctCpl4,PriorFee4,FeeToDate4,Fee5,FeePctCpl5,PriorFee5,FeeToDate5,CreditTemplate,InvoiceApprovalEnabled,InvoiceApprovalCode,SupportDocuments,SupportDocumentsEX,SupportDocumentsAP,SupportDocumentsBkupRpt,DaysBeforeDue,PayTerms,ShowDueDate,PreInvoice,PreInvTemplate,CancelTemplate,PreInvNote,PreInvWBS2ToPost,PreInvWBS3ToPost,ExpPODetail,ConPODetail,SupportDocumentsUN,EmpTitle,EmpTitleTable,BackupEmpTitle,BackupEmpTitleTable,BackupExpDefault,BackupFees,BackupAddOns)VALUES('<O>',' ','N','Overhead Projects',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.0000,'N',0,0.0000,0.0000,0.0000,0,0.0000,0,0,'N','N','N','N','N','N','N',0,0,0,0,0,1.0000,0,'N','N',0,0.0000,0,'N','N',0,1.0000,'N','N',0,NULL,0.0000,0.0000,0.0000,NULL,0.0000,'N','Y',0.0000,0.0000,'N','0',0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,'N',0.0000,0.0000,'Y','Y','Y','Y','Y','Y','N','N','N','N',NULL,NULL,0,0,'N','N','N','N','N','N','N','N','N','N','N','N','N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N','N','N','N','N',0.0000,'N','N','N','N','N',NULL,NULL,'B','Y','N','Y',0,0,'Y','Y','N','Y',0,0,'Y','N',NULL,'N','N',NULL,NULL,'W','N',NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,'N',NULL,'N','N','N','Y',0,'I','N','N',NULL,NULL,NULL,NULL,NULL,'N','N','N',0,0,0,0,'N','N','N');
INSERT INTO BTDefaults (DefaultType,Company,UseClientAddress,Address1,Address2,Address3,Address4,City,State,ZIP,Country,Description,Notes,Template,NextInvoice,DraftPrint,LabMeth,Mult1,Mult2,Mult3,SeparateOvt,OvtMult,RateTable,ORTable,LabDetail,ShowComment,ShowRate,ShowDate,ShowMults,ShowOvtMult,FlagOverrides,SortMeth1,SortTable1,SortMeth2,SortTable2,ExpMeth,ExpMult,ExpTable,ExpShowMult,ExpDetail,ConMeth,ConMult,ConTable,ConShowMult,ConDetail,UnitMeth,UnitMult,UnitShowMult,UnitDetail,FeeMeth,FeeBasis,FeeFactor1,FeeFactor2,FeePctCpl,FeeLabel,PriorFee,AddOns,PrintWBS1AR,Interest,GracePd,PrintWBS1BTD,LimitMeth,LabLimit,ConLimit,ExpLimit,UnitLimit,AddOnLimit,FeeLimit,Retainage,RetPct,RetLimit,RetLab,RetExp,RetCon,RetFee,RetUnit,RetAddOn,ConsolidatePrinting,ConsolidateWBS3,SubLevelTerms,ConsolidatePosting,WBS2ToPost,WBS3ToPost,SubLevelRollup,WBS3Rollup,LimitsBySubLevel,PrintARBreakout,PrintARWBS2Totals,PrintBTDBreakout,PrintBTDWBS2Totals,PrintWBS2AR,PrintWBS2BTD,InterestBySubLevel,TaxBySubLevel,RetainageBySubLevel,BackupReport,BackupComments,BackupVendorInvoice,LabAccount,ConAccount,ExpAccount,UnitAccount,FeeAccount,AddOnAccount,IntAccount,Footer,LabDefault,ExpDefault,ConDefault,UnitDefault,PostFeesByPhase,SpecialOvtMult,BackupLaborCode,BackupEKDescription,PrintBTDReceivedTotal,PrintBTDARBalanceTotal,IntercompanyTax,IntercompanyTaxWBS2ToPost,IntercompanyTaxWBS3ToPost,printLaborForFixedFee,BackupLabor,BackupLaborCategory,BackupEmployeeNumber,BackupLaborSort,BackupLaborTable,BackupExpense,BackupAPDescription,BackupMileage,BackupTransType,BackupExpenseSort,BackupExpenseTable,BackupEXDescription,BackupLabDefault,DraftInvoice,ScheduledBilling,ConsolidateTaxPosting,TaxWBS2ToPost,TaxWBS3ToPost,TaxRetainage,FeeByDetailEnabled,OverUnderAccount,FeeToDate,Fee1,FeePctCpl1,PriorFee1,FeeToDate1,Fee2,FeePctCpl2,PriorFee2,FeeToDate2,Fee3,FeePctCpl3,PriorFee3,FeeToDate3,Fee4,FeePctCpl4,PriorFee4,FeeToDate4,Fee5,FeePctCpl5,PriorFee5,FeeToDate5,CreditTemplate,InvoiceApprovalEnabled,InvoiceApprovalCode,SupportDocuments,SupportDocumentsEX,SupportDocumentsAP,SupportDocumentsBkupRpt,DaysBeforeDue,PayTerms,ShowDueDate,PreInvoice,PreInvTemplate,CancelTemplate,PreInvNote,PreInvWBS2ToPost,PreInvWBS3ToPost,ExpPODetail,ConPODetail,SupportDocumentsUN,EmpTitle,EmpTitleTable,BackupEmpTitle,BackupEmpTitleTable,BackupExpDefault,BackupFees,BackupAddOns)VALUES('<R>',' ','N','Regular Projects',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.0000,'N',0,0.0000,0.0000,0.0000,0,0.0000,0,0,'N','N','N','N','N','N','N',0,0,0,0,0,1.0000,0,'N','N',0,1.0000,0,'N','N',0,1.0000,'N','N',0,NULL,0.0000,0.0000,0.0000,NULL,0.0000,'N','Y',0.0000,0.0000,'N','0',0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,'N',0.0000,0.0000,'Y','Y','Y','Y','Y','Y','N','N','N','N',NULL,NULL,0,0,'N','N','N','N','N','N','N','N','N','N','N','N','N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N','N','N','N','N',0.0000,'N','N','N','N','N',NULL,NULL,'B','Y','N','Y',0,0,'Y','Y','N','Y',0,0,'Y','N',NULL,'N','N',NULL,NULL,'W','N',NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,'N',NULL,'N','N','N','Y',0,'I','N','N',NULL,NULL,NULL,NULL,NULL,'N','N','N',0,0,0,0,'N','N','N');

DELETE FROM BTLaborCatsData;
INSERT INTO BTLaborCatsData (Category,CategoryCode)VALUES(1,'Principal');
INSERT INTO BTLaborCatsData (Category,CategoryCode)VALUES(2,'ProjMgr');
INSERT INTO BTLaborCatsData (Category,CategoryCode)VALUES(3,'Consultant');
INSERT INTO BTLaborCatsData (Category,CategoryCode)VALUES(4,'SrArch');
INSERT INTO BTLaborCatsData (Category,CategoryCode)VALUES(5,'SrEng');
INSERT INTO BTLaborCatsData (Category,CategoryCode)VALUES(6,'Engineer');
INSERT INTO BTLaborCatsData (Category,CategoryCode)VALUES(7,'Architect');
INSERT INTO BTLaborCatsData (Category,CategoryCode)VALUES(8,'Designer');
INSERT INTO BTLaborCatsData (Category,CategoryCode)VALUES(9,'Artist');
INSERT INTO BTLaborCatsData (Category,CategoryCode)VALUES(10,'Surveyor');
INSERT INTO BTLaborCatsData (Category,CategoryCode)VALUES(11,'Admin');
INSERT INTO BTLaborCatsData (Category,CategoryCode)VALUES(12,'Marketing');

DELETE FROM BTLaborCatsDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM BTRCT;
INSERT INTO BTRCT (TableNo,TableName,AvailableForPlanning,CurrencyCode,FilterOrg,FilterPrincipal,FilterProjMgr,FilterSupervisor,FilterCode)VALUES(1,'Setup','Y',' ',NULL,NULL,NULL,NULL,NULL);

DELETE FROM BTRCTCats;
INSERT INTO BTRCTCats (TableNo,RateID,Category,Rate,Description,SortSeq,EffectiveDate,StartDate,EndDate)VALUES(1,'1',1,0.0000,'Principal',10,NULL,NULL,NULL);
INSERT INTO BTRCTCats (TableNo,RateID,Category,Rate,Description,SortSeq,EffectiveDate,StartDate,EndDate)VALUES(1,'2',2,0.0000,'Project Manager',20,NULL,NULL,NULL);
INSERT INTO BTRCTCats (TableNo,RateID,Category,Rate,Description,SortSeq,EffectiveDate,StartDate,EndDate)VALUES(1,'3',3,0.0000,'Senior Consultant',30,NULL,NULL,NULL);
INSERT INTO BTRCTCats (TableNo,RateID,Category,Rate,Description,SortSeq,EffectiveDate,StartDate,EndDate)VALUES(1,'4',4,0.0000,'Senior Architect',40,NULL,NULL,NULL);
INSERT INTO BTRCTCats (TableNo,RateID,Category,Rate,Description,SortSeq,EffectiveDate,StartDate,EndDate)VALUES(1,'5',5,0.0000,'Senior Engineer',50,NULL,NULL,NULL);
INSERT INTO BTRCTCats (TableNo,RateID,Category,Rate,Description,SortSeq,EffectiveDate,StartDate,EndDate)VALUES(1,'6',6,0.0000,'Engineer',60,NULL,NULL,NULL);
INSERT INTO BTRCTCats (TableNo,RateID,Category,Rate,Description,SortSeq,EffectiveDate,StartDate,EndDate)VALUES(1,'7',7,0.0000,'Architect',70,NULL,NULL,NULL);
INSERT INTO BTRCTCats (TableNo,RateID,Category,Rate,Description,SortSeq,EffectiveDate,StartDate,EndDate)VALUES(1,'8',8,0.0000,'Designer',80,NULL,NULL,NULL);
INSERT INTO BTRCTCats (TableNo,RateID,Category,Rate,Description,SortSeq,EffectiveDate,StartDate,EndDate)VALUES(1,'9',9,0.0000,'Draftsperson',90,NULL,NULL,NULL);
INSERT INTO BTRCTCats (TableNo,RateID,Category,Rate,Description,SortSeq,EffectiveDate,StartDate,EndDate)VALUES(1,'10',10,0.0000,'Surveyor',100,NULL,NULL,NULL);
INSERT INTO BTRCTCats (TableNo,RateID,Category,Rate,Description,SortSeq,EffectiveDate,StartDate,EndDate)VALUES(1,'11',11,0.0000,'Administrative',110,NULL,NULL,NULL);
INSERT INTO BTRCTCats (TableNo,RateID,Category,Rate,Description,SortSeq,EffectiveDate,StartDate,EndDate)VALUES(1,'12',12,0.0000,'Marketing',120,NULL,NULL,NULL);

DELETE FROM CA;
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('101.00','Checking Account',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('102.00','Savings Account',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('103.00','Petty Cash',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('104.00','Payroll Checking Account',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('105.00','Deposits',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('106.00','Investments',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('107.00','Employee Advance Account',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('111.00','AR - Clients',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('113.00','AR - Retainage',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('114.00','Notes Receivable',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('117.00','Prepaid Insurance-Liability',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('118.00','Prepaid Insurance-Other',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('119.00','Allowance for Doubtful Accounts',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('121.00','Unbilled Services',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('151.00','Furniture and Fixtures',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('152.00','Accumulated Deprec-F&F',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('153.00','Leasehold Improvements',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('154.00','Accumulated Amortiz-Leashold Improvement',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('155.00','Automobiles',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('156.00','Accumulated Deprec-Auto',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('157.00','Data Processing Equipment',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('158.00','Accum Deprec-Data Equipment',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('159.00','Land',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('161.00','Organizational Expense',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('162.00','Accumulated Depreciation - OE',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('171.00','Prepaid Interest',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('172.00','Prepaid Insurance',NULL,NULL,'A',1,'Y',NULL,' ',1,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('201.00','Notes Payable-Short Term',NULL,NULL,'A',1,'Y',NULL,' ',2,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('202.00','Mortgage Payable',NULL,NULL,'A',1,'Y',NULL,' ',2,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('210.00','AP - Trade',NULL,NULL,'A',1,'Y',NULL,' ',2,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('211.00','AP - Consultants',NULL,NULL,'A',1,'Y',NULL,' ',2,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('212.00','Employee Expense Payable',NULL,NULL,'A',1,'Y',NULL,' ',2,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('213.00','Consultant Accrual',NULL,NULL,'A',1,'Y',NULL,' ',2,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('221.00','Deposits on Documents',NULL,NULL,'A',1,'Y',NULL,' ',2,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('231.00','Salaries Payable',NULL,NULL,'A',1,'Y',NULL,' ',2,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('232.00','FICA Payable',NULL,NULL,'A',1,'Y',NULL,' ',2,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('233.00','Federal Withholding',NULL,NULL,'A',1,'Y',NULL,' ',2,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('234.00','State Withholding',NULL,NULL,'A',1,'Y',NULL,' ',2,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('235.00','United Way Withholding',NULL,NULL,'A',1,'Y',NULL,' ',2,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('236.00','Health Care Insurance Withholding',NULL,NULL,'A',1,'Y',NULL,' ',2,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('237.00','401(k) Withholding',NULL,NULL,'A',1,'Y',NULL,' ',2,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('239.00','Other Withholding',NULL,NULL,'A',1,'Y',NULL,' ',2,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('251.00','Notes Payable-Long Term',NULL,NULL,'A',1,'Y',NULL,' ',2,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('301.00','Capital',NULL,NULL,'A',1,'Y',NULL,' ',3,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('311.00','Previous Years Retained Earnings',NULL,NULL,'A',1,'Y',NULL,' ',3,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('312.00','Current Years Profit(Loss)',NULL,NULL,'A',1,'Y',NULL,' ',3,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('400.00','Billed Labor Revenue',NULL,NULL,'A',1,'Y',NULL,' ',4,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('401.00','Billed Fee Revenue',NULL,NULL,'A',1,'Y',NULL,' ',4,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('402.00','Unbilled Revenue',NULL,NULL,'A',1,'Y',NULL,' ',4,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('421.00','Reimb Consultant Revenue',NULL,NULL,'A',1,'Y',NULL,' ',4,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('422.00','Reimb Expense Revenue',NULL,NULL,'A',1,'Y',NULL,' ',4,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('425.00','Other Revenue',NULL,NULL,'A',1,'Y',NULL,' ',4,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('431.00','Interest Revenue',NULL,NULL,'A',1,'Y',NULL,' ',4,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('432.00','Sales and Use Tax',NULL,NULL,'A',1,'Y',NULL,' ',4,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('433.00','Errors and Omissions Insurance',NULL,NULL,'A',1,'Y',NULL,' ',4,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('511.00','Structural Consultant',NULL,NULL,'A',1,'Y',NULL,' ',6,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('512.00','Mechanical Consultant',NULL,NULL,'A',1,'Y',NULL,' ',6,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('513.00','Electrical Consultant',NULL,NULL,'A',1,'Y',NULL,' ',6,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('514.00','Civil & Landscape Consultant',NULL,NULL,'A',1,'Y',NULL,' ',6,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('515.00','Other Consultants',NULL,NULL,'A',1,'Y',NULL,' ',6,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('521.00','Travel and Lodging',NULL,NULL,'A',1,'Y',NULL,' ',5,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('521.01','Meals',NULL,NULL,'A',1,'Y',NULL,' ',5,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('522.00','Reproductions',NULL,NULL,'A',1,'Y',NULL,' ',5,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('523.00','Models/Renderings/Photos',NULL,NULL,'A',1,'Y',NULL,' ',5,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('524.00','Long Distance Telephone',NULL,NULL,'A',1,'Y',NULL,' ',5,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('524.01','Fax Expense',NULL,NULL,'A',1,'Y',NULL,' ',5,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('525.00','Postage/Shipping/Delivery',NULL,NULL,'A',1,'Y',NULL,' ',5,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('529.00','Misc Reimbursable Expense',NULL,NULL,'A',1,'Y',NULL,' ',5,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('601.00','Direct Labor-Principals',NULL,NULL,'A',1,'Y',NULL,' ',8,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('602.00','Direct Labor-Employees',NULL,NULL,'A',1,'Y',NULL,' ',8,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('611.00','Structural Consultant',NULL,NULL,'A',1,'Y',NULL,' ',8,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('612.00','Mechanical Consultant',NULL,NULL,'A',1,'Y',NULL,' ',8,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('613.00','Electrical Consultant',NULL,NULL,'A',1,'Y',NULL,' ',8,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('614.00','Civil & Landscape Consult',NULL,NULL,'A',1,'Y',NULL,' ',8,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('615.00','Other Consultants',NULL,NULL,'A',1,'Y',NULL,' ',8,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('621.00','Travel and Lodging',NULL,NULL,'A',1,'Y',NULL,' ',7,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('621.01','Meals',NULL,NULL,'A',1,'Y',NULL,' ',7,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('622.00','Reproductions',NULL,NULL,'A',1,'Y',NULL,' ',7,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('623.00','Models/Renderings/Photos',NULL,NULL,'A',1,'Y',NULL,' ',7,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('624.00','Long Distance Telephone',NULL,NULL,'A',1,'Y',NULL,' ',7,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('624.01','Fax Expense',NULL,NULL,'A',1,'Y',NULL,' ',7,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('625.00','Postage/Shipping/Delivery',NULL,NULL,'A',1,'Y',NULL,' ',7,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('629.00','Misc. Direct Expenses',NULL,NULL,'A',1,'Y',NULL,' ',7,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('651.00','Bad Debt Expense',NULL,NULL,'A',1,'Y',NULL,' ',7,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('701.00','Indirect Labor-Principals',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('702.00','Indirect Labor-Employees',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('703.00','Job Cost Variance',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
--INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('704.00','Temporary Help',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('711.00','Holiday',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('712.00','Vacation',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('713.00','Sick Leave',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('715.00','Management',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('716.00','Accounting',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('717.00','Professional Development',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('718.00','Business Development',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('719.00','Proposals',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('721.00','Employer''s FICA Tax',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('722.00','Federal Unemployment',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('723.00','State Unemployment',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('724.00','Workers'' Compensation',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('729.00','Misc. Payroll Expenses',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('731.00','Health Care Insurance',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('732.00','Life Insurance',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('733.00','Disability Income Insurance',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('734.00','Other Insurance',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('735.00','Pension/Profit Sharing',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('736.00','Education & Seminars',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('737.00','Professional Registration & Dues',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('741.00','Rent',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('742.00','Utilities',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('743.00','Office Supplies/Periodicals',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('744.00','Telephone',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('744.01','Fax Expense',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('745.00','Postage/Shipping/Delivery',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('746.00','Equipment Rental',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('747.00','Repairs & Maintenance',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('748.00','Printing & Reproductions',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('749.00','Other Office Expenses',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('751.00','Legal',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('752.00','Accounting/Audit/Tax',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('753.00','Data Processing',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('754.00','Interest Expense',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('755.00','Professional Liability Insurance',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('756.00','Other Insurance',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('757.00','Misc. Taxes & Fees',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('758.00','Other Expenses',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('759.00','Bad Debt Expense',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('761.00','Auto Gas & Oil',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('762.00','Auto Repairs',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('763.00','Auto Registration/Insurance',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('764.00','Travel',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('765.00','Hotels & Meals',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('771.00','Depreciation - Furniture & Fixtures',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('772.00','Amortization-Leasehold',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('773.00','Depreciation-Automobiles',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('774.00','Depreciation-Data Equip.',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('791.00','Printing/Reproduction Recovery',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('792.00','Misc. Expenses Recovery',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('793.00','Discounts Taken',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('798.00','Company Paid Expenses',NULL,NULL,'A',1,'Y',NULL,' ',9,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('801.00','Provision for Income Tax',NULL,NULL,'A',1,'Y',NULL,' ',10,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('805.00','Interest Expense',NULL,NULL,'A',1,'Y',NULL,' ',10,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('810.00','Rent Income',NULL,NULL,'A',1,'Y',NULL,' ',10,NULL,NULL,'N');
INSERT INTO CA (Account,Name,CashBasisAccount,FASAccount,Status,Detail,GlobalAccount,AccountCurrencyCode,CustomCurrencyCode,Type,UnrealizedLossAccount,UnrealizedGainAccount,CashBasisRevaluation)VALUES('820.01','Gain Loss on Asset Disposal',NULL,NULL,'A',1,'Y',NULL,' ',10,NULL,NULL,'N');

DELETE FROM CFGActivitySubjectData;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGActivityTypeData;
INSERT INTO CFGActivityTypeData (Code,OutlookType)VALUES('EMail','Generic');
INSERT INTO CFGActivityTypeData (Code,OutlookType)VALUES('Event','Calendar');
INSERT INTO CFGActivityTypeData (Code,OutlookType)VALUES('Mailing','Generic');
INSERT INTO CFGActivityTypeData (Code,OutlookType)VALUES('Meeting','Calendar');
INSERT INTO CFGActivityTypeData (Code,OutlookType)VALUES('Merge','Generic');
INSERT INTO CFGActivityTypeData (Code,OutlookType)VALUES('Milestone','Generic');
INSERT INTO CFGActivityTypeData (Code,OutlookType)VALUES('Phone Call','Generic');
INSERT INTO CFGActivityTypeData (Code,OutlookType)VALUES('Task','Task');
INSERT INTO CFGActivityTypeData (Code,OutlookType)VALUES('Touchpoint','Generic');

DELETE FROM CFGActivityTypeDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGAPLiability;
INSERT INTO CFGAPLiability (Code,Account,Org,Description,Company)VALUES('Consultant','211.00',NULL,'AP Consultant Liability',' ');
INSERT INTO CFGAPLiability (Code,Account,Org,Description,Company)VALUES('General','210.00',NULL,'AP General Liability',' ');

DELETE FROM CFGARLedgerHeadingsData;
INSERT INTO CFGARLedgerHeadingsData (ReportColumn)VALUES(1);
INSERT INTO CFGARLedgerHeadingsData (ReportColumn)VALUES(2);
INSERT INTO CFGARLedgerHeadingsData (ReportColumn)VALUES(3);
INSERT INTO CFGARLedgerHeadingsData (ReportColumn)VALUES(4);
INSERT INTO CFGARLedgerHeadingsData (ReportColumn)VALUES(5);
INSERT INTO CFGARLedgerHeadingsData (ReportColumn)VALUES(6);

DELETE FROM CFGARLedgerHeadingsDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGARMap;
INSERT INTO CFGARMap (Company,Account,ARColumn)VALUES(' ','111.00',1);

DELETE FROM CFGAssetMain;
INSERT INTO CFGAssetMain (Company,CapLimit,AssetPeriod,CreateAssetFrom,AssetSourcePO,AssetSourceAP,AssetSourceJE,FAAccount,FAWBS1,FAWBS2,FAWBS3,DOWBS1,DOWBS2,DOWBS3)VALUES(' ',0.0000,0,'AP','N','N','N',NULL,'00001.00',NULL,NULL,NULL,NULL,NULL);

DELETE FROM CFGAwardTypeData;
INSERT INTO CFGAwardTypeData (Code)VALUES('1');
INSERT INTO CFGAwardTypeData (Code)VALUES('2');
INSERT INTO CFGAwardTypeData (Code)VALUES('4');
INSERT INTO CFGAwardTypeData (Code)VALUES('5');
INSERT INTO CFGAwardTypeData (Code)VALUES('6');
INSERT INTO CFGAwardTypeData (Code)VALUES('7');

DELETE FROM CFGAwardTypeDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGBillMainData;
INSERT INTO CFGBillMainData (Company,PrintName,PrintByline,PrintAddress,PrintWBS1No,PrintInvoiceNo,CombineWBS1Invoice,PrintWBS1Name,PrintAROnly,AddressStart,Page2Start,NumberMeth,NextInvoice,NextInvoiceMods,NonBillLaborCode,ReleaseHolds,UserEnteredPriorFee,LabAccount,ConAccount,ExpAccount,UnitAccount,FeeAccount,AddOnAccount,IntAccount,PrintTotalNowDue,ExtendAddOn,ShowNonbillables,ShowNonbillablesMods,DisableFinalLimits,DisableFinalLimitsMods,EffectiveDateEnabled,InvoiceAddressee,PrintContactTitle,PrintContactPrefix,PrintAssignedInvoiceNo,ScheduledBilling,FeeByCategoryEnabled,FeeByDetailEnabled,FeeByDetailMethod,FeeByDetailOverUnderAccount,FeeByDetailLabor,FeeByDetailDirCons,FeeByDetailReimbCons,FeeByDetailDirExp,FeeByDetailReimbExp,FeeByDetailDirUnit,FeeByDetailReimbUnit,InvoiceApprovalEnabled,DefaultInvoiceApprovalCode,PreInvoiceEnabled,PreInvoiceRecAccount)VALUES(' ','Y','Y','Y','Y','Y','N','Y','N',0.00,0.00,'F',1.0000,'N',NULL,'Y','N','400.00','421.00','422.00','422.00','401.00','401.00',NULL,'N','N','N','N','N','N','N','1','N','N','N','N','N','N',NULL,NULL,'N','N','N','N','N','N','N','N',NULL,'N',NULL);

DELETE FROM CFGBillMainDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGCampaignActionData;
INSERT INTO CFGCampaignActionData (Code)VALUES('01');
INSERT INTO CFGCampaignActionData (Code)VALUES('02');
INSERT INTO CFGCampaignActionData (Code)VALUES('03');
INSERT INTO CFGCampaignActionData (Code)VALUES('04');

DELETE FROM CFGCampaignActionDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGCampaignAudienceData;
INSERT INTO CFGCampaignAudienceData (Code)VALUES('01');
INSERT INTO CFGCampaignAudienceData (Code)VALUES('02');
INSERT INTO CFGCampaignAudienceData (Code)VALUES('03');
INSERT INTO CFGCampaignAudienceData (Code)VALUES('04');
INSERT INTO CFGCampaignAudienceData (Code)VALUES('05');

DELETE FROM CFGCampaignAudienceDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGCampaignObjectiveData;
INSERT INTO CFGCampaignObjectiveData (Code)VALUES('01');
INSERT INTO CFGCampaignObjectiveData (Code)VALUES('02');

DELETE FROM CFGCampaignObjectiveDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGCampaignStatusData;
INSERT INTO CFGCampaignStatusData (Code)VALUES('01');
INSERT INTO CFGCampaignStatusData (Code)VALUES('02');
INSERT INTO CFGCampaignStatusData (Code)VALUES('03');

DELETE FROM CFGCampaignStatusDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGCampaignTypeData;
INSERT INTO CFGCampaignTypeData (Code)VALUES('01');
INSERT INTO CFGCampaignTypeData (Code)VALUES('02');
INSERT INTO CFGCampaignTypeData (Code)VALUES('03');
INSERT INTO CFGCampaignTypeData (Code)VALUES('04');
INSERT INTO CFGCampaignTypeData (Code)VALUES('05');

DELETE FROM CFGCampaignTypeDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGClientCurrentStatusData;
INSERT INTO CFGClientCurrentStatusData (Code)VALUES('Existing');
INSERT INTO CFGClientCurrentStatusData (Code)VALUES('Former');
INSERT INTO CFGClientCurrentStatusData (Code)VALUES('Prospect');

DELETE FROM CFGClientCurrentStatusDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGClientRelationshipData;
INSERT INTO CFGClientRelationshipData (Code,InverseCode)VALUES('01',NULL);

DELETE FROM CFGClientRelationshipDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGClientRoleData;
INSERT INTO CFGClientRoleData (Code)VALUES('01');
INSERT INTO CFGClientRoleData (Code)VALUES('sysOwner');

DELETE FROM CFGClientRoleDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGClientTypeData;
INSERT INTO CFGClientTypeData (Code)VALUES('01');
INSERT INTO CFGClientTypeData (Code)VALUES('02');
INSERT INTO CFGClientTypeData (Code)VALUES('03');
INSERT INTO CFGClientTypeData (Code)VALUES('04');
INSERT INTO CFGClientTypeData (Code)VALUES('05');
INSERT INTO CFGClientTypeData (Code)VALUES('06');

DELETE FROM CFGClientTypeDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGCompetitionTypeData;
INSERT INTO CFGCompetitionTypeData (Code)VALUES('37');
INSERT INTO CFGCompetitionTypeData (Code)VALUES('38');
INSERT INTO CFGCompetitionTypeData (Code)VALUES('39');
INSERT INTO CFGCompetitionTypeData (Code)VALUES('40');
INSERT INTO CFGCompetitionTypeData (Code)VALUES('44');
INSERT INTO CFGCompetitionTypeData (Code)VALUES('45');
INSERT INTO CFGCompetitionTypeData (Code)VALUES('47');
INSERT INTO CFGCompetitionTypeData (Code)VALUES('48');
INSERT INTO CFGCompetitionTypeData (Code)VALUES('64');
INSERT INTO CFGCompetitionTypeData (Code)VALUES('65');
INSERT INTO CFGCompetitionTypeData (Code)VALUES('66');
INSERT INTO CFGCompetitionTypeData (Code)VALUES('67');

DELETE FROM CFGCompetitionTypeDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGContactRelationshipData;
INSERT INTO CFGContactRelationshipData (Code,InverseCode)VALUES('01',NULL);
INSERT INTO CFGContactRelationshipData (Code,InverseCode)VALUES('02',NULL);
INSERT INTO CFGContactRelationshipData (Code,InverseCode)VALUES('03',NULL);
INSERT INTO CFGContactRelationshipData (Code,InverseCode)VALUES('04',NULL);

DELETE FROM CFGContactRelationshipDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGContactRoleData;
INSERT INTO CFGContactRoleData (Code)VALUES('Contractor');
INSERT INTO CFGContactRoleData (Code)VALUES('sysOwner');

DELETE FROM CFGContactRoleDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGContactSourceData;
INSERT INTO CFGContactSourceData (Code)VALUES('01');
INSERT INTO CFGContactSourceData (Code)VALUES('02');
INSERT INTO CFGContactSourceData (Code)VALUES('03');
INSERT INTO CFGContactSourceData (Code)VALUES('04');
INSERT INTO CFGContactSourceData (Code)VALUES('05');
INSERT INTO CFGContactSourceData (Code)VALUES('06');
INSERT INTO CFGContactSourceData (Code)VALUES('07');

DELETE FROM CFGContactSourceDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGContactTitleData;
INSERT INTO CFGContactTitleData (Code)VALUES('Director');
INSERT INTO CFGContactTitleData (Code)VALUES('Executive');
INSERT INTO CFGContactTitleData (Code)VALUES('Finance');
INSERT INTO CFGContactTitleData (Code)VALUES('Marketing');
INSERT INTO CFGContactTitleData (Code)VALUES('Sales');
INSERT INTO CFGContactTitleData (Code)VALUES('VP');

DELETE FROM CFGContactTitleDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGContactTypeData;
INSERT INTO CFGContactTypeData (Code)VALUES('C');

DELETE FROM CFGContactTypeDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGContractStatusData;
INSERT INTO CFGContractStatusData (Code)VALUES('01');
INSERT INTO CFGContractStatusData (Code)VALUES('02');
INSERT INTO CFGContractStatusData (Code)VALUES('03');
INSERT INTO CFGContractStatusData (Code)VALUES('04');
INSERT INTO CFGContractStatusData (Code)VALUES('05');
INSERT INTO CFGContractStatusData (Code)VALUES('06');

DELETE FROM CFGContractStatusDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGContractTypeData;
INSERT INTO CFGContractTypeData (Code)VALUES('01');
INSERT INTO CFGContractTypeData (Code)VALUES('02');
INSERT INTO CFGContractTypeData (Code)VALUES('03');
INSERT INTO CFGContractTypeData (Code)VALUES('04');

DELETE FROM CFGContractTypeDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGCubeTranslationData;
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountGroup','10-OtherCharges');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountGroup','1-Asset');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountGroup','2-Liability');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountGroup','3-NetWorth');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountGroup','4-Revenue');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountGroup','5-Reimbursable');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountGroup','6-Reimbursable');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountGroup','7-Direct');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountGroup','8-Direct');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountGroup','9-Indirect');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountType','10-OtherCharges');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountType','1-Asset');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountType','2-Liability');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountType','3-NetWorth');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountType','4-Revenue');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountType','5-Reimbursable');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountType','6-ReimbursableConsultant');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountType','7-Direct');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountType','8-DirectConsultant');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountType','9-Indirect');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('AccountType','U-Unknown');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('BillingStatus','B');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('BillingStatus','D');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('BillingStatus','F');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('BillingStatus','H');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('BillingStatus','M');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('BillingStatus','N');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('BillingStatus','O');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('BillingStatus','R');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('BillingStatus','T');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('BillingStatus','W');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('BillingStatus','X');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('ExpenseType','5-ReimbursableOther');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('ExpenseType','7-DirectOther');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('General','H');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('General','U');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('General','UM');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('ReportType','BalanceSheet');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('ReportType','IncomeStatement');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('Status','A');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('Status','I');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','AL');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','AP');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','AX');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','BA');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','BE');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','BL');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','BU');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','CD');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','CN');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','CP');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','CR');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','CT');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','CV');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','EC');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','EP');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','ER');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','EX');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','HA');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','HB');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','HE');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','HL');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','HU');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','IH');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','IN');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','IX');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','JE');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','JX');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','KC');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','LA');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','LG');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','MI');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','PP');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','PR');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','PX');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','PY');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','RG');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','RX');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','TS');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','UN');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','UP');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','XD');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('TransactionType','XE');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('YesNo','N');
INSERT INTO CFGCubeTranslationData (Usage,Code)VALUES('YesNo','Y');

DELETE FROM CFGCubeTranslationDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGEKMain;
INSERT INTO CFGEKMain (Company,AllowResubmit,AmountPerMile,AllowCompanyPaid,AllowAdvances,DistanceType,CategoriesRequired,CompanyPaidWBS1,CompanyPaidWBS2,CompanyPaidWBS3,CompanyPaidAccount,DisableLogin,ApprovalRequired,ElectronicSignature,AllowSubmitterToApprove,ShowWBS1,ShowWBS2,ShowWBS3,ShowAccount,TreatInactiveAsDormant,SigningMemo,DisallowEditAmountPerMile,ShowSeqNumber,UseApprovalWorkflow,Workflow_UID)VALUES(' ','N',0.0000,'N','Y','M','N',NULL,NULL,NULL,NULL,'N','N','N','N','3','3','3','1','N',NULL,'N','N','N',NULL);

DELETE FROM CFGEMDegreeData;
INSERT INTO CFGEMDegreeData (Code)VALUES('01');
INSERT INTO CFGEMDegreeData (Code)VALUES('02');
INSERT INTO CFGEMDegreeData (Code)VALUES('03');

DELETE FROM CFGEMDegreeDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGEmployeeRelationshipData;
INSERT INTO CFGEmployeeRelationshipData (Code)VALUES('01');
INSERT INTO CFGEmployeeRelationshipData (Code)VALUES('02');
INSERT INTO CFGEmployeeRelationshipData (Code)VALUES('03');
INSERT INTO CFGEmployeeRelationshipData (Code)VALUES('04');
INSERT INTO CFGEmployeeRelationshipData (Code)VALUES('SysOwner');

DELETE FROM CFGEmployeeRelationshipDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGEmployeeRoleData;
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('Admin','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('AirQuality','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('Architect','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('Associate','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('Biologist','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('CADDTechi','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('CivilEngr','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('ConstIns','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('ConstMgr','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('ElectEngr','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('EntitleSpl','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('EnvirPlnr','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('Estimator','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('GISSpecial','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('Hydrolgst','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('InfoTech','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('InteDsnr','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('LandArch','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('MechEng','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('MEPCoordin','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('Planner','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('ProjAdmin','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('ProjArch','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('ProjDsnr','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('ProjEng','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('ProjPlnr','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('ProjSvor','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('QAQCMgr','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('SpecWriter','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('StructEng','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('Surveyor','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('sysBDL','N')
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('sysMC','N') 
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('sysPM','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('sysPR','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('sysPRM','N')
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('sysSP','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('TechEditor','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('TrafficSpe','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('TransEngr','N');
INSERT INTO CFGEmployeeRoleData (Code,DefaultInd)VALUES('TransPlnr','N');

DELETE FROM CFGEmployeeRoleDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGEmployeeTitleData;
INSERT INTO CFGEmployeeTitleData (Code)VALUES('Architect');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('CFO');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('CivilEng');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('Designer');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('Director');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('EE');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('EIT');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('Engineer');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('GE');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('GM');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('ID');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('LA');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('PIC');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('Planner');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('PM');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('President');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('Principal');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('SUP');
INSERT INTO CFGEmployeeTitleData (Code)VALUES('TC');

DELETE FROM CFGEmployeeTitleDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGEmployeeTypeAccounts;
INSERT INTO CFGEmployeeTypeAccounts (Type,Company,DirectAccount,IndirectAccount,OtherPayCostAccount,OtherPay2CostAccount,OtherPay3CostAccount,OtherPay4CostAccount,OtherPay5CostAccount)VALUES('E',' ','602.00','702.00',NULL,NULL,NULL,NULL,NULL);
INSERT INTO CFGEmployeeTypeAccounts (Type,Company,DirectAccount,IndirectAccount,OtherPayCostAccount,OtherPay2CostAccount,OtherPay3CostAccount,OtherPay4CostAccount,OtherPay5CostAccount)VALUES('P',' ','601.00','701.00',NULL,NULL,NULL,NULL,NULL);

DELETE FROM CFGEmployeeTypeData;
INSERT INTO CFGEmployeeTypeData (Type,SortOrder)VALUES('E',1);
INSERT INTO CFGEmployeeTypeData (Type,SortOrder)VALUES('P',2);

DELETE FROM CFGEmployeeTypeDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGEMRegistrationData;
INSERT INTO CFGEMRegistrationData (Code)VALUES('AIA');
INSERT INTO CFGEMRegistrationData (Code)VALUES('EIT');
INSERT INTO CFGEMRegistrationData (Code)VALUES('PE');
INSERT INTO CFGEMRegistrationData (Code)VALUES('PLS');
INSERT INTO CFGEMRegistrationData (Code)VALUES('RLS');

DELETE FROM CFGEMRegistrationDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGEMSkillData;
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('01',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('01SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('02',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('03SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('04SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('05',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('05SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('07',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('07SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('08',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('08SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('09SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('10',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('11',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('11SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('12',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('13SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('14',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('14SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('15',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('16SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('17SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('18SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('19',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('21',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('22SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('23SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('24SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('25SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('26SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('27',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('27SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('28',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('28SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('29SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('30',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('31',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('31SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('32',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('32SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('33',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('33SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('35SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('36',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('36SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('38SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('40',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('40SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('41SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('42',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('43',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('44',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('45',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('45SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('46SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('48SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('49SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('50SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('51SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('53SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('54SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('58SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('59SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('61SF',NULL);
INSERT INTO CFGEMSkillData (Code,CodeSF330)VALUES('62SF',NULL);

DELETE FROM CFGEMSkillDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGEMSkillLevelData;
INSERT INTO CFGEMSkillLevelData (Code)VALUES(1);
INSERT INTO CFGEMSkillLevelData (Code)VALUES(2);
INSERT INTO CFGEMSkillLevelData (Code)VALUES(3);

DELETE FROM CFGEMSkillLevelDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGEMSkillUsageData;
INSERT INTO CFGEMSkillUsageData (Code)VALUES(1);
INSERT INTO CFGEMSkillUsageData (Code)VALUES(2);
INSERT INTO CFGEMSkillUsageData (Code)VALUES(3);
INSERT INTO CFGEMSkillUsageData (Code)VALUES(4);
INSERT INTO CFGEMSkillUsageData (Code)VALUES(5);

DELETE FROM CFGEMSkillUsageDescriptions;
-- INSERTs are located in the language-specific scripts

--DELETE FROM CFGFormat;
--INSERT INTO CFGFormat (PKey,WBS1Length,WBS1LeadZeros,WBS1Delimiter1,WBS1Delimiter1Position,WBS1Delimiter2,WBS1Delimiter2Position,WBS2Length,WBS2LeadZeros,WBS2Delimiter,WBS2DelimiterPosition,WBS3Length,WBS3LeadZeros,WBS3Delimiter,WBS3DelimiterPosition,OrgLevels,OrgDelimiter,Org1Start,Org1Length,Org2Start,Org2Length,Org3Start,Org3Length,Org4Start,Org4Length,Org5Start,Org5Length,LCLevels,LCDelimiter,LC1Start,LC1Length,LC2Start,LC2Length,LC3Start,LC3Length,LC4Start,LC4Length,LC5Start,LC5Length,AccountLength,AccountLeadZeros,AccountDelimiter,AccountDelimiterPosition,EmployeeLength,EmployeeLeadZeros,EmployeeDelimiter,EmployeeDelimiterPosition,ClientLength,ClientLeadZeros,ClientDelimiter,ClientDelimiterPosition,VendorLength,VendorLeadZeros,VendorDelimiter,VendorDelimiterPosition,UnitTableLength,UnitTableLeadZeros,UnitTableDelimiter1,UnitTableDelimiter1Position,UnitTableDelimiter2,UnitTableDelimiter2Position,UnitLength,UnitLeadZeros,UnitDelimiter,UnitDelimiterPosition,RefnoLength,clientAutoNumSrc,clientAutoNumOverride,clientAutoNumSeqLen,employeeAutoNumSrc,employeeAutoNumOverride,employeeAutoNumSeqLen,wbs1AutoNumSrc,wbs1AutoNumOverride,wbs1AutoNumSeqLen,wbs1AutoNumSeqLenPromo,wbs1AutoNumSeqLenOH,vendorAutoNumSrc,vendorAutoNumOverride,vendorAutoNumSeqLen,mktCampaignAutoNumSrc,mktCampaignAutoNumOverride,mktCampaignAutoNumSeqLen,oppAutoNumSrc,oppAutoNumOverride,oppAutoNumSeqLen,rpAutoNumSrc,rpAutoNumOverride,rpAutoNumSeqLen,clientAutoNumSeqStart,employeeAutoNumSeqStart,wbs1AutoNumSeqStart,wbs1AutoNumSeqStartPromo,wbs1AutoNumSeqStartOH,vendorAutoNumSeqStart,mktCampaignAutoNumSeqStart,oppAutoNumSeqStart,rpAutoNumSeqStart,clientAutoNumSeqPos,employeeAutoNumSeqPos,wbs1AutoNumSeqPos,wbs1AutoNumSeqPosPromo,wbs1AutoNumSeqPosOH,vendorAutoNumSeqPos,mktCampaignAutoNumSeqPos,oppAutoNumSeqPos,rpAutoNumSeqPos,equipmentAutoNumSrc,equipmentAutoNumOverride,equipmentAutoNumSeqLen,equipmentAutoNumSeqStart,equipmentAutoNumSeqPos,VariableOrgLevels,WBSLevelDelimiter)VALUES('Dltk',8,'Y','.',6,NULL,0,0,'Y',NULL,0,0,'Y',NULL,0,0,NULL,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,0,0,6,'Y','.',4,4,'Y',NULL,0,20,'N',NULL,0,6,'Y',NULL,0,10,'Y',NULL,0,NULL,0,11,'N',NULL,0,7,NULL,'N',0.0000,NULL,NULL,0.0000,NULL,NULL,0.0000,0.0000,0.0000,NULL,NULL,0.0000,NULL,NULL,0.0000,NULL,NULL,0.0000,NULL,NULL,0.0000,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,NULL,NULL,0.0000,0,1,'N','.');

DELETE FROM CFGLeadRatingData;
INSERT INTO CFGLeadRatingData (Code)VALUES('01');
INSERT INTO CFGLeadRatingData (Code)VALUES('02');
INSERT INTO CFGLeadRatingData (Code)VALUES('03');

DELETE FROM CFGLeadRatingDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGMainData;
INSERT INTO CFGMainData (Company,FirmName,JobCostAtSalary,JobCostFrequency,MaxHourlyRate,RevGenEnabled,IgnoreControlTotals,ConsolidatetimePost,PreserveTimeComments,ADPEnabled,ADPDefaultCompanyCode,XChargeEnabled,TaxAuditingEnabled,DefaultTaxCountryCode,TaxCountryCode,TaxRegistrationNumber,EXCheckTemplate,BenefitAccrualByPayroll,BenefitAccrualFrequency,Byline,Address1,Address2,Address3,Address4,PostingLogs,FunctionalCurrencyCode,PayrollEnabled,DefaultShortDateFormat,DefaultMediumDateFormat,DefaultLongDateFormat,DefaultDateSeparator,DefaultTimeFormat,DefaultdecimalSymbol,DefaultThousandsSeparator,TriangulationCurrencyCode,CurrentBenefitAccrualYear,PayrollInterface,ProjectDownload,JobCostIncludeOVHours,ShowSSN,AllowNoFormat,DefaultPhoneFormatKey,RevByCategoryEnabled,RevenueCategories,Country,DefaultAddressFormat,RevenueGroupsEnabled,EXCheckFormat,PrintDateFormatIndicator,CurrencyFormat,DateFormat,TransCenterApprovalEnabled,AllTransactionsEnabled,RequireApprovalPostTrans,AllowEditSubmittedTrans,RevByContractEnabled,UnbilledGainsLossesMethod,OriginatingVendorEnabled,FirmPhone,FirmPhoneFormat,FirmEmail,EuroTaxRegValidation,RequireTaxCodeAP,CreditCardEnabled,TransDateWithinFiscalYear,TransDateWithinPeriod,DisallowAccountNameChange,EnableApprovalWorkflowGLBud,ApprovalWorkflowGLBud,AccrueBasedOnHoursWorked,AccrueNotLessThanMinumumHours,AccrueMinimumHours,AccrueExcludeOvertimeHours,PrintEXBankAccount)VALUES(' ','','N','B',85.0000,'Y','N','N','N','N',NULL,'N','N',NULL,NULL,NULL,'CheckEXDefault_Two','N','W',NULL,NULL,NULL,NULL,NULL,'N',' ','N','M/d/yyyy','MMMM d, yyyy','dddd, MMMM dd, yyyy','/','h:mm:ss AM/PM','.',',',NULL,2016,'A',NULL,'N','Y','Y',NULL,'N',0,'US',1,'N',0,'N',NULL,'MMMM d, yyyy','N','N','N','N','N','A','N',NULL,NULL,NULL,'N','N','N','N','N','N','N',NULL,'N','N',0.0000,'N','Y');
-- special update if QB enabled
UPDATE CFGMainData SET TransDateWithinFiscalYear = 'Y', TransDateWithinPeriod = 'Y' FROM CFGQuickbooks WHERE Enabled = 'Y'

DELETE FROM CFGOHMain;
INSERT INTO CFGOHMain (Company,OHProcedure,OHAllocMethod,OHBasis,OHRate,OHVarianceWBS1,OHVarianceWBS2,OHVarianceWBS3,OHProvisionalRate,PlugAmtRev,PlugAmtDirLab,PlugAmtDirExp,PlugAmtIndLab,PlugAmtIndExp)VALUES(' ','F','A','DL',165.0000,NULL,NULL,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000);

DELETE FROM CFGOpportunityClosedReasonData;
INSERT INTO CFGOpportunityClosedReasonData (Code)VALUES('01');
INSERT INTO CFGOpportunityClosedReasonData (Code)VALUES('02');
INSERT INTO CFGOpportunityClosedReasonData (Code)VALUES('03');
INSERT INTO CFGOpportunityClosedReasonData (Code)VALUES('04');

DELETE FROM CFGOpportunityClosedReasonDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGOpportunitySourceData;
INSERT INTO CFGOpportunitySourceData (Code)VALUES('CRef');
INSERT INTO CFGOpportunitySourceData (Code)VALUES('Event');
INSERT INTO CFGOpportunitySourceData (Code)VALUES('List');
INSERT INTO CFGOpportunitySourceData (Code)VALUES('PersCont');
INSERT INTO CFGOpportunitySourceData (Code)VALUES('Social');
INSERT INTO CFGOpportunitySourceData (Code)VALUES('TLead');
INSERT INTO CFGOpportunitySourceData (Code)VALUES('Web');
INSERT INTO CFGOpportunitySourceData (Code)VALUES('WIQ');
INSERT INTO CFGOpportunitySourceData (Code)VALUES('WOM');

DELETE FROM CFGOpportunitySourceDescriptions;
-- INSERTs are located in the language-specific scripts

--DELETE FROM CFGOpportunityStageData;
--INSERT INTO CFGOpportunityStageData (Code,Closed)VALUES('Creation','N');
--INSERT INTO CFGOpportunityStageData (Code,Closed)VALUES('IPR','N');
--INSERT INTO CFGOpportunityStageData (Code,Closed)VALUES('Lost','N');
--INSERT INTO CFGOpportunityStageData (Code,Closed)VALUES('NEG','N');
--INSERT INTO CFGOpportunityStageData (Code,Closed)VALUES('NoWinner','N');
--INSERT INTO CFGOpportunityStageData (Code,Closed)VALUES('NQ','N');
--INSERT INTO CFGOpportunityStageData (Code,Closed)VALUES('PreQualify','N');
--INSERT INTO CFGOpportunityStageData (Code,Closed)VALUES('Qualified','N');
--INSERT INTO CFGOpportunityStageData (Code,Closed)VALUES('Submitted','N');
--INSERT INTO CFGOpportunityStageData (Code,Closed)VALUES('Won','N');

--DELETE FROM CFGOpportunityStageDescriptions;
-- INSERTs are located in the language-specific scripts

--DELETE FROM CFGOpportunityStatusData;
--INSERT INTO CFGOpportunityStatusData (Code)VALUES('A');
--INSERT INTO CFGOpportunityStatusData (Code)VALUES('I');

--DELETE FROM CFGOpportunityStatusDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGPrefixData;
INSERT INTO CFGPrefixData (Code)VALUES('Dr.');
INSERT INTO CFGPrefixData (Code)VALUES('Miss');
INSERT INTO CFGPrefixData (Code)VALUES('Mr.');
INSERT INTO CFGPrefixData (Code)VALUES('Mrs.');
INSERT INTO CFGPrefixData (Code)VALUES('Ms.');
INSERT INTO CFGPrefixData (Code)VALUES('Prof.');

DELETE FROM CFGPrefixDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGProbabilityData;
INSERT INTO CFGProbabilityData (Probability)VALUES(5);
INSERT INTO CFGProbabilityData (Probability)VALUES(10);
INSERT INTO CFGProbabilityData (Probability)VALUES(20);
INSERT INTO CFGProbabilityData (Probability)VALUES(30);
INSERT INTO CFGProbabilityData (Probability)VALUES(40);
INSERT INTO CFGProbabilityData (Probability)VALUES(50);
INSERT INTO CFGProbabilityData (Probability)VALUES(60);
INSERT INTO CFGProbabilityData (Probability)VALUES(70);
INSERT INTO CFGProbabilityData (Probability)VALUES(80);
INSERT INTO CFGProbabilityData (Probability)VALUES(90);
INSERT INTO CFGProbabilityData (Probability)VALUES(95);
INSERT INTO CFGProbabilityData (Probability)VALUES(100);

DELETE FROM CFGProbabilityDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGProjectCodeData;
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('001','Y','A01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('002','Y','A02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('003','Y','A03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('004','Y','A04');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('005','Y','A05');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('006','Y','A06');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('007','Y','A07');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('008','Y','A11');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('009','Y','A12');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('010','Y','B01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('011','Y','B02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('012','Y','C02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('013','Y','C04');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('014','Y','C06');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('015','Y','C08');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('016','Y','C09');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('017','Y','C10');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('018','Y','C12');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('019','Y','C13');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('020','Y','C14');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('021','Y','C15');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('022','Y','C17');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('023','Y','C18');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('024','Y','D01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('025','Y','D02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('026','Y','D03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('027','Y','D07');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('028','Y','E01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('029','Y','E02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('030','Y','E04');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('031','Y','E05');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('032','Y','E07');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('033','Y','E09');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('034','Y','F01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('035','Y','F02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('036','Y','F03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('037','Y','F04');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('038','Y','F06');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('039','Y','G01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('040','Y','G02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('041','Y','G06');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('042','Y','H01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('043','Y','H04');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('044','Y','H05');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('045','Y','H06');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('046','Y','H07');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('047','Y','H08');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('048','Y','H09');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('049','Y','H10');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('050','Y','H11');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('051','Y','H12');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('052','Y','I01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('053','Y','I02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('054','Y','I03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('055','Y','I05');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('056','Y','I06');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('057','Y','J01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('058','Y','L01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('059','Y','L03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('060','Y','L04');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('061','Y','L05');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('062','Y','L06');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('063','Y','M02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('064','Y','M03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('065','Y','M04');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('066','Y','M05');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('067','Y','M06');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('068','Y','M07');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('069','Y','M08');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('070','Y','N01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('071','Y','N03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('072','Y','O01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('073','Y','O02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('074','Y','O03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('075','Y','P01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('076','Y','P02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('077','Y','P04');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('078','Y','P05');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('079','Y','P06');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('080','Y','P07');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('081','Y','P10');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('082','Y','P11');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('083','Y','P12');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('084','Y','P08');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('085','Y','P09');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('086','Y','R01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('087','Y','R03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('088','Y','R04');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('089','N','R06');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('090','Y','R09');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('091','Y','R02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('092','Y','R11');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('093','Y','S01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('094','Y','S02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('095','Y','S03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('096','Y','S04');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('097','Y','S05');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('098','Y','S06');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('099','Y','S07');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('100','Y','S08');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('101','Y','S09');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('102','Y','S10');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('103','Y','S12');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('104','Y','S13');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('105','Y','T01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('106','Y','T02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('107','Y','T03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('108','Y','T05');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('109','Y','T06');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('110','Y','U02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('111','Y','U03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('112','Y','V01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('113','Y','W01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('114','Y','W02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('115','Y','W03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('116','Y','W04');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('117','Y','Z01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('A08','Y','A08');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('A09','Y','A09');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('A10','Y','A10');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('C01','Y','C01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('C03','Y','C03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('C05','Y','C05');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('C07','Y','C07');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('C11','Y','C11');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('C16','Y','C16');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('C19','Y','C19');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('D04','Y','D04');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('D05','Y','D05');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('D06','Y','D06');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('D08','Y','D08');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('E03','Y','E03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('E06','Y','E06');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('E08','Y','E08');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('E10','Y','E10');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('E11','Y','E11');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('E12','Y','E12');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('E13','Y','E13');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('F05','Y','F05');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('G03','Y','G03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('G04','Y','G04');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('G05','Y','G05');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('H02','Y','H02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('H03','Y','H03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('H13','Y','H13');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('I04','Y','I04');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('L02','Y','L02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('M01','Y','M01');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('N02','Y','N02');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('P03','Y','P03');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('P13','Y','P13');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('R05','Y','R05');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('R07','Y','R07');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('R08','Y','R08');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('R10','Y','R10');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('R12','Y','R12');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('S11','Y','S11');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('T04','Y','T04');
INSERT INTO CFGProjectCodeData (ProjectCode,Active,ProjectCodeSF330)VALUES('U01','Y','U01');

DELETE FROM CFGProjectCodeDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGProjectMilestoneData
INSERT INTO CFGProjectMilestoneData (Code, SystemInd) VALUES ('SysEstStart','Y')
INSERT INTO CFGProjectMilestoneData (Code, SystemInd) VALUES ('SysEstCompletion','Y')
INSERT INTO CFGProjectMilestoneData (Code, SystemInd) VALUES ('SysContract','Y')
INSERT INTO CFGProjectMilestoneData (Code, SystemInd) VALUES ('SysBidSubmitted','Y')
INSERT INTO CFGProjectMilestoneData (Code, SystemInd) VALUES ('SysActualCompl','Y')
INSERT INTO CFGProjectMilestoneData (Code, SystemInd) VALUES ('SysProfServicesCompl','Y')
INSERT INTO CFGProjectMilestoneData (Code, SystemInd) VALUES ('SysConstCompl','Y')
INSERT INTO CFGProjectMilestoneData (Code, SystemInd) VALUES ('SysStart','Y')

DELETE FROM CFGProjectMilestoneDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGProposalSourceData;
INSERT INTO CFGProposalSourceData (Code)VALUES('01');
INSERT INTO CFGProposalSourceData (Code)VALUES('02');
INSERT INTO CFGProposalSourceData (Code)VALUES('03');
INSERT INTO CFGProposalSourceData (Code)VALUES('04');
INSERT INTO CFGProposalSourceData (Code)VALUES('05');
INSERT INTO CFGProposalSourceData (Code)VALUES('06');
INSERT INTO CFGProposalSourceData (Code)VALUES('07');
INSERT INTO CFGProposalSourceData (Code)VALUES('08');

DELETE FROM CFGProposalSourceDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGProposalStatusData;
INSERT INTO CFGProposalStatusData (Code)VALUES('01');
INSERT INTO CFGProposalStatusData (Code)VALUES('02');
INSERT INTO CFGProposalStatusData (Code)VALUES('03');

DELETE FROM CFGProposalStatusDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGPRResponsibilityData;
INSERT INTO CFGPRResponsibilityData (Code)VALUES('C');
INSERT INTO CFGPRResponsibilityData (Code)VALUES('IE');
INSERT INTO CFGPRResponsibilityData (Code)VALUES('JV');
INSERT INTO CFGPRResponsibilityData (Code)VALUES('P');
INSERT INTO CFGPRResponsibilityData (Code)VALUES('S');
INSERT INTO CFGPRResponsibilityData (Code)VALUES('U');

DELETE FROM CFGPRResponsibilityDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGPYAccrualsData;
INSERT INTO CFGPYAccrualsData (Company,Code,HasCarryoverLimit,CarryoverLimit,Maximum,PreAccrue,PrintOnCheck,ShowOnTK,ScheduleID,CheckBenefitHours,EnableApprovalWorkflow,ApprovalWorkflow,WBS1ExcludeWhere)VALUES(' ','Sick Lv.','N',0.0000,0.0000,'N','N','Y',NULL,'N','N',NULL,NULL);
INSERT INTO CFGPYAccrualsData (Company,Code,HasCarryoverLimit,CarryoverLimit,Maximum,PreAccrue,PrintOnCheck,ShowOnTK,ScheduleID,CheckBenefitHours,EnableApprovalWorkflow,ApprovalWorkflow,WBS1ExcludeWhere)VALUES(' ','Vacation','N',0.0000,0.0000,'N','N','Y',NULL,'N','N',NULL,NULL);

DELETE FROM CFGPYAccrualsDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGResourcePlanning;
INSERT INTO CFGResourcePlanning (Company,MatchWBS1Wildcard,BudgetType,CostRtMethod,CostRtTableNo,BillingRtMethod,BillingRtTableNo,WBS1Level,WBS2Level,WBS3Level,LBCDLevel,OverheadPct,GenResTableNo,GRBillTableNo,CalcExpBillAmtFlg,CalcConBillAmtFlg,ExpBillRtMethod,ExpBillRtTableNo,ConBillRtMethod,ConBillRtTableNo,ExpBillMultiplier,ConBillMultiplier,CopyTo,PctCompleteFormula,LabMultType,Multiplier,ReimbMethod,LabRatedecimals,UntRatedecimals,Hrdecimals,Qtydecimals,Amtdecimals,Multdecimals,Pctdecimals,ExpTab,ConTab,UntTab,PctComplByPeriodFlg,EVFormula,RevenueMethod,AnalysisBasis,GRMethod,StartingDayOfWeek,LabBillMultiplier,UntBillMultiplier,FeesByPeriodFlg,WBS1Match,WBS2Match,WBS3Match,LBCDMatch,EffectiveDateEnabled,SyncFeeEnabled,UtilizationIncludeFlg,LimitFeesToContract,ContractComparisonWithoutRoundingInd,NavigatorCalendarScale,ConWBSLevel,ExpWBSLevel,CompEQLabDirExpFlg,CostGRRtMethod,BillGRRtMethod,TargetMultCost)VALUES(' ','N','C',0,0,0,0,0,0,0,0,0.0000,0,0,'N','N',0,0,0,0,0.0000,0.0000,NULL,0,0,0.0000,'C',2,4,0,3,0,2,2,'Y','Y','N','N',0,1,'P',0,2,1.0000,1.0000,'N','Y','Y','Y','Y','N','N','Y','N','N','w',3,3,'Y',0,0,0.0000);

DELETE FROM CFGRGMethodsData;
INSERT INTO CFGRGMethodsData (Method,Category,Active)VALUES('B',1,'Y');
INSERT INTO CFGRGMethodsData (Method,Category,Active)VALUES('M',1,'Y');
INSERT INTO CFGRGMethodsData (Method,Category,Active)VALUES('N',0,'Y');
INSERT INTO CFGRGMethodsData (Method,Category,Active)VALUES('P',1,'Y');
INSERT INTO CFGRGMethodsData (Method,Category,Active)VALUES('R',1,'Y');
INSERT INTO CFGRGMethodsData (Method,Category,Active)VALUES('W',1,'Y');

DELETE FROM CFGRGMethodsDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGRMSettings;
INSERT INTO CFGRMSettings (PKey,OverUtilOption,EmpUtilPercent,EmpUtilPlusPercent,EmpUtilMorePercent,UnderUtilPercent,OverSchPercent,UnderSchPercent,UseBookingForEmpHours,UseBookingForGenHours,IncludeSoftBookedHours,ProvRateOption,Hrdecimals,StartingDayOfWeek,JTDDate,HardBooked,Amtdecimals)VALUES('Dltk',1,105,0,0,95,105,95,'N','N','N',1,0,2,NULL,'N',0);

DELETE FROM CFGServiceProfileData;
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.18',3,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.19',3,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.23',3,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.24',3,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.25',3,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.26',3,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.27',3,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.34',5,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.39',5,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.43',5,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.47',5,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.51',5,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.55',3,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.A01',3,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.A02',5,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.A03',3,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.A04',5,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.A05',5,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.A06',5,' ','Y','N','Y','N');
INSERT INTO CFGServiceProfileData (Code,FeeBands,CurrencyCode,SystemProfile,Disabled,GlobalProfile,DisableFeeBands)VALUES('2013.A07',5,' ','Y','N','Y','N');

DELETE FROM CFGServiceProfileDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGSuffixData;
INSERT INTO CFGSuffixData (Code)VALUES('I');
INSERT INTO CFGSuffixData (Code)VALUES('II');
INSERT INTO CFGSuffixData (Code)VALUES('III');
INSERT INTO CFGSuffixData (Code)VALUES('Jr.');
INSERT INTO CFGSuffixData (Code)VALUES('Sr.');

DELETE FROM CFGSuffixDescriptions;
-- INSERTs are located in the language-specific scripts

--DELETE FROM CFGTimeAnalysis;
--INSERT INTO CFGTimeAnalysis (Company,StartWBS1,EndWBS1,Account,ReportColumn,AccrualCode)VALUES(' ','00001.00','00001.00',NULL,1,NULL);
--INSERT INTO CFGTimeAnalysis (Company,StartWBS1,EndWBS1,Account,ReportColumn,AccrualCode)VALUES(' ','00002.00','00002.00',NULL,2,'Vacation');
--INSERT INTO CFGTimeAnalysis (Company,StartWBS1,EndWBS1,Account,ReportColumn,AccrualCode)VALUES(' ','00003.00','00003.00',NULL,3,'Sick Lv.');
--INSERT INTO CFGTimeAnalysis (Company,StartWBS1,EndWBS1,Account,ReportColumn,AccrualCode)VALUES(' ','00004.00','00004.00',NULL,4,NULL);
--INSERT INTO CFGTimeAnalysis (Company,StartWBS1,EndWBS1,Account,ReportColumn,AccrualCode)VALUES(' ','00005.00','00005.00',NULL,5,NULL);

--DELETE FROM CFGTimeAnalysisHeadingsData;
--INSERT INTO CFGTimeAnalysisHeadingsData (ReportColumn,Benefit)VALUES(1,'N');
--INSERT INTO CFGTimeAnalysisHeadingsData (ReportColumn,Benefit)VALUES(2,'Y');
--INSERT INTO CFGTimeAnalysisHeadingsData (ReportColumn,Benefit)VALUES(3,'Y');
--INSERT INTO CFGTimeAnalysisHeadingsData (ReportColumn,Benefit)VALUES(4,'Y');
--INSERT INTO CFGTimeAnalysisHeadingsData (ReportColumn,Benefit)VALUES(5,'N');

--DELETE FROM CFGTimeAnalysisHeadingsDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGTKMain;
INSERT INTO CFGTKMain (Company,AllowResubmit,CheckHours,CheckLaborCodes,DisableOvertime,DisableLogin,ApprovalRequired,ElectronicSignature,AllowSubmitterToApprove,ShowWBS1,ShowWBS2,ShowWBS3,ShowLaborCode,ShowBillCategory,LeftGridWidth,PrintComments,PrintClientName,TreatInactiveAsDormant,ShowClient,ShowRatio1,ShowRatio2,ShowRatio3,ShowRatio4,ShowRatio5,ShowRatioCurrent,ShowRatioMTD,ShowRatioYTD,ShowRatioQTD,SigningMemo,AutoRP,RequireComments,AllowUnits,PostUnitsImmediately,LimitIncrement,Increment,AllowStartEndTime,EnableRevAudit,AuditRevAfter,RequireRevExplanation,RequireRevExplanationAfter,EnableBillingXferAudit,EnablePeriodNumber,PeriodNumberOption,PrintHTMLFormatting,HideReverseOption,UseApprovalWorkflow,Workflow_UID,StartEndTimeBy,TimesheetFrequency)VALUES(' ','N','N','N','N','N','N','N','N','3','3','3','1','2',0,'Y','Y','N','2','Y','Y','Y','Y','Y','Y','N','N','N',NULL,'N','N','N','N','N','Q','N','N','S','N','S','N','N','W','Y','N','N',NULL,'P',NULL);

DELETE FROM CFGUnitTypeData;
INSERT INTO CFGUnitTypeData (Code)VALUES('Equipment');
INSERT INTO CFGUnitTypeData (Code)VALUES('Expenses');
INSERT INTO CFGUnitTypeData (Code)VALUES('Labor');

DELETE FROM CFGUnitTypeDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGVendorTypeAPLiability;
INSERT INTO CFGVendorTypeAPLiability (Type,Company,LiabCode)VALUES('C',' ','Consultant');
INSERT INTO CFGVendorTypeAPLiability (Type,Company,LiabCode)VALUES('T',' ','Trade');

DELETE FROM CFGVendorTypeData;
INSERT INTO CFGVendorTypeData (Type)VALUES('C');
INSERT INTO CFGVendorTypeData (Type)VALUES('T');

DELETE FROM CFGVendorTypeDescriptions;
-- INSERTs are located in the language-specific scripts

DELETE FROM CFGXCharge;
INSERT INTO CFGXCharge (Company,XChargeRegLAEnabled,XChargeRegJEEnabled,XChargeOHLAEnabled,XChargeOHJEEnabled,XChargeRegMethod,XChargeRegMult,XChargeOHMult,XChargeRegCreditAccount,XChargeRegDebitAccount,XChargeOHCreditAccount,XChargeOHDebitAccount)VALUES(' ','N','N','N','N',0,0.0000,0.0000,NULL,NULL,NULL,NULL);

DELETE FROM FW_CFGAttachmentCategoryData;
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('Award','Employees');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('Case Study','TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('ClientACH','Firms');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('CompanyOverview','TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('DigitalArtifact','Marketing Campaigns');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('Estimate','Opportunities');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('Estimate','Projects');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('Leadership','Employees');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('MSA','Firms');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('MSA','Opportunities');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('MSA','Projects');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('NDA','Contacts');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('NDA','Firms');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('Notes','Activities');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('Notes','Contacts');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('Proposal','Opportunities');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('Proposal','Projects');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('Reference','TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('Resume','Employees');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('SOW','Firms');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('SOW','Opportunities');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('SOW','Projects');
INSERT INTO FW_CFGAttachmentCategoryData (Code,Application)VALUES('VendorACH','Firms');

DELETE FROM FW_CFGAttachmentCategoryDesc;
-- INSERTs are located in the language-specific scripts

DELETE FROM FW_CFGLabelData;
-- INSERTs are located in the language-specific scripts

--DELETE FROM FW_CFGSystem;
--INSERT INTO FW_CFGSystem (PKey,Version,CurrentPeriod,PeriodsPerYear,CashBasis,RetainageEnabled,RetainersEnabled,AlertsPollingInterval,EnableAutoRetrieve,LookupLimitError,LookupLimit,AppLogo,ReportVersions,ReportRetentionPeriod,AppURL,ShareCalendar,SingleUserLogin,DisableAllLogins,SendErrors,MulticompanyEnabled,MulticurrencyEnabled,AuditingEnabled,AuditHistoryLength,DashboardLookupLimit,ReportLogEnabled,RequireVoucherNumbers,BalanceSheet,LastVoucherAP,LastVoucherEX,DefaultOrg,AutoSumComp,CheckPayee,WorkflowLogEnabled,WorkflowLogLength,InternetDomain,InternetUsername,InternetPassword,ReportAtBilling,CashBasisTimePost,ETEnabled,SpecialOvertimeEnabled,BudgetByVendor,BudgetETC,BudgetEAC,BudgetStartEndDates,BudgetJTD,EnableJEPostBTD,EnableEMProjectAssocUpdate,EnableVEProjectAssocUpdate,OvtPctEnabled,EnableCostRateTable,ReportAtBurden,EnablePayRateTable,ReportAtBillingInBillingCurr,CheckPWPLevel,PRSummaryLastUpdate,WSSServer,WSSAdminPort,WSSVisionSite,WSSUseDocMgt,DisplayCostPointOrg,DefaultAttachmentSaveLocation,ConversionStatus,ICBillingLabRegReclassOnly,ICBillingLabRegLAEnabled,ICBillingLabRegJEEnabled,ICBillingLabOHReclassOnly,ICBillingLabOHLAEnabled,ICBillingLabOHJEEnabled,ICBillingLabPromoReclassOnly,ICBillingLabPromoLAEnabled,ICBillingLabPromoJEEnabled,ICBillingExpRegReclassOnly,ICBillingExpRegCostJEEnabled,ICBillingExpRegJEEnabled,ICBillingExpOHReclassOnly,ICBillingExpOHCostJEEnabled,ICBillingExpOHJEEnabled,ICBillingExpPromoReclassOnly,ICBillingExpPromoCostJEEnabled,ICBillingExpPromoJEEnabled,ICBillingBSOtherReclassOnly,ICBillingBSOtherJEEnabled,ICBillingDetailedSubledgers,ReplaceExchRateWS,TSAccessible,ERAccessible,AuditingEnabledBT,AuditingEnabledCA,AuditingEnabledCL,AuditingEnabledContact,AuditingEnabledEM,AuditingEnabledLead,AuditingEnabledMkt,AuditingEnabledOP,AuditingEnabledPR,AuditingEnabledText,AuditingEnabledUN,AuditingEnabledVE,AuditKeyValuesDelete,DuplicateVendorInvoiceWarning,PrintInvoiceLeadingZeros,PrintVoucherLeadingZeros,UseProjectTerms,DefaultSystemFont,DefaultSystemFontSize,WSSSitePort,APExpenseCodeEnabled,APExpenseCodeRequired,GridViewColumnsLimit,GridViewRecordsLimit,PlanningCheckOutFlg,DefAcctGroupTable,EnableInstantMessaging,LastLabelUpdateDate,GLSummaryLastUpdate,SyncProjToContractFees,RealizationByEmployeeEnabled,RealizationByEmployeeMethod,RealizationByEmployeeFrequency,AuditingEnabledConfiguration,AuditingEnabledRoles,AuditingEnabledScreenDesigner,AuditingEnabledUsers,AuditingEnabledExchangeRates,AnalysisCubePivotCurrency,AnalysisCubeLastUpdated,HelpLocation,NavigatorURL,IncClientInContactLookup,AuditingEnabledActivity,KonaAcctKey,AuditingEnabledUserActivity,WebServiceTimeout,KonaAcctSecret,MarketType,ProductType,AllowFileSave,SFTPHostName,SFTPUsername,SFTPPassword,SessionTimeout,WebAPIEnableWorkflow,WebAPIGenerateCustomPackages,HideKonaMenu,HideiAccessMenu,FileSizeUploadLimit,WAADTenant,WAADClientID,WAADServerClientID,WAADServerClientSecret,WAADAuthType,IndustryType,TwoFactorAuth,AjeraSync,KonaAcctID,KonaURL,EnableLeadQualification,KonaNotificationRole)VALUES('Dltk','1.0.0',0,12,'N','N','N',60000,'N','N',0,NULL,0,24,NULL,'Y','N','N','Y','N','N','Y',30,0,'N','N','N',0.0000,0,NULL,'N','2','N',30,NULL,NULL,NULL,'Y','N','N','N','N','Y','Y','Y','Y','N','N','N','N','N','N','N','Y','3',NULL,NULL,'0',NULL,'N','N',NULL,NULL,'Y','N','N','Y','N','N','Y','N','N','Y','N','N','Y','N','N','Y','N','N','Y','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','Y','Y','N','Tahoma',8,80,'N','N',0,0,'N',0,'N',getutcdate(),NULL,'N','N',NULL,NULL,'N','N','N','N','N',' ',NULL,'H',NULL,'N','N',NULL,'N',100,NULL,'PS','Vision','N',NULL,NULL,NULL,0,'N','Y','N','N',50,NULL,NULL,NULL,NULL,NULL,'AE','N','N',0,NULL,'N',NULL);
UPDATE FW_CFGSystem SET IndustryType = 'AE'

DELETE FROM GR;
INSERT INTO GR (Code,Name,Org,Category,LaborCode,Supervisor,Skill,Status)VALUES('1','Principal',NULL,1,NULL,NULL,NULL,'A');
INSERT INTO GR (Code,Name,Org,Category,LaborCode,Supervisor,Skill,Status)VALUES('10','Surveyor',NULL,10,NULL,NULL,NULL,'A');
INSERT INTO GR (Code,Name,Org,Category,LaborCode,Supervisor,Skill,Status)VALUES('11','Administrative',NULL,11,NULL,NULL,NULL,'A');
INSERT INTO GR (Code,Name,Org,Category,LaborCode,Supervisor,Skill,Status)VALUES('12','Marketing',NULL,12,NULL,NULL,NULL,'A');
INSERT INTO GR (Code,Name,Org,Category,LaborCode,Supervisor,Skill,Status)VALUES('2','Project Manager',NULL,2,NULL,NULL,NULL,'A');
INSERT INTO GR (Code,Name,Org,Category,LaborCode,Supervisor,Skill,Status)VALUES('3','Senior Consultant',NULL,3,NULL,NULL,NULL,'A');
INSERT INTO GR (Code,Name,Org,Category,LaborCode,Supervisor,Skill,Status)VALUES('4','Senior Architect',NULL,4,NULL,NULL,NULL,'A');
INSERT INTO GR (Code,Name,Org,Category,LaborCode,Supervisor,Skill,Status)VALUES('5','Senior Engineer',NULL,5,NULL,NULL,NULL,'A');
INSERT INTO GR (Code,Name,Org,Category,LaborCode,Supervisor,Skill,Status)VALUES('6','Engineer',NULL,6,NULL,NULL,NULL,'A');
INSERT INTO GR (Code,Name,Org,Category,LaborCode,Supervisor,Skill,Status)VALUES('7','Architect',NULL,7,NULL,NULL,NULL,'A');
INSERT INTO GR (Code,Name,Org,Category,LaborCode,Supervisor,Skill,Status)VALUES('8','Designer',NULL,8,NULL,NULL,NULL,'A');
INSERT INTO GR (Code,Name,Org,Category,LaborCode,Supervisor,Skill,Status)VALUES('9','Draftsperson',NULL,9,NULL,NULL,NULL,'A');

--DELETE FROM PR;
--INSERT INTO PR (WBS1,WBS2,WBS3,Name,ChargeType,SubLevel,Principal,ProjMgr,Supervisor,ClientID,CLAddress,Fee,ReimbAllow,ConsultFee,BudOHRate,Status,RevType,MultAmt,Org,UnitTable,StartDate,EndDate,PctComp,LabPctComp,ExpPctComp,BillByDefault,BillableWarning,Memo,BudgetedFlag,BudgetedLevels,BillWBS1,BillWBS2,BillWBS3,XCharge,XChargeMethod,XChargeMult,Description,Closed,ReadOnly,DefaultEffortDriven,DefaultTaskType,VersionID,ContactID,CLBillingAddr,LongName,Address1,Address2,Address3,City,State,Zip,County,Country,FederalInd,ProjectType,Responsibility,Referable,EstCompletionDate,ActCompletionDate,ContractDate,BidDate,ComplDateComment,FirmCost,FirmCostComment,TotalProjectCost,TotalCostComment,OpportunityID,ClientConfidential,ClientAlias,AvailableForCRM,ReadyForApproval,ReadyForProcessing,BillingClientID,BillingContactID,Phone,Fax,EMail,ProposalWBS1,CostRateMeth,CostRateTableNo,PayRateMeth,PayRateTableNo,Locale,LineItemApproval,LineItemApprovalEK,BudgetSource,BudgetLevel,ProfServicesComplDate,ConstComplDate,ProjectCurrencyCode,ProjectExchangeRate,BillingCurrencyCode,BillingExchangeRate,RestrictChargeCompanies,FeeBillingCurrency,ReimbAllowBillingCurrency,ConsultFeeBillingCurrency,RevUpsetLimits,RevUpsetWBS2,RevUpsetWBS3,RevUpsetIncludeComp,RevUpsetIncludeCons,RevUpsetIncludeReimb,PORMBRate,POCNSRate,PlanID,TKCheckRPDate,ICBillingLab,ICBillingLabMethod,ICBillingLabMult,ICBillingExp,ICBillingExpMethod,ICBillingExpMult,RequireComments,TKCheckRPPlannedHrs,BillByDefaultConsultants,BillByDefaultOtherExp,BillByDefaultORTable,PhoneFormat,FaxFormat,RevType2,RevType3,RevType4,RevType5,RevUpsetCategoryToAdjust,FeeFunctionalCurrency,ReimbAllowFunctionalCurrency,ConsultFeeFunctionalCurrency,RevenueMethod,ICBillingLabTableNo,ICBillingExpTableNo,Biller,FeeDirLab,FeeDirExp,ReimbAllowExp,ReimbAllowCons,FeeDirLabBillingCurrency,FeeDirExpBillingCurrency,ReimbAllowExpBillingCurrency,ReimbAllowConsBillingCurrency,FeeDirLabFunctionalCurrency,FeeDirExpFunctionalCurrency,ReimbAllowExpFunctionalCurrency,ReimbAllowConsFunctionalCurrency,RevUpsetIncludeCompDirExp,RevUpsetIncludeReimbCons,AwardType,Duration,ContractTypeGovCon,CompetitionType,MasterContract,Solicitation,NAICS,OurRole,AjeraSync,ServProCode,FESurchargePct,FESurcharge,FEAddlExpensesPct,FEAddlExpenses,FEOtherPct,FEOther,ProjectTemplate,AjeraSpentLabor,AjeraSpentReimbursable,AjeraSpentConsultant,AjeraCostLabor,AjeraCostReimbursable,AjeraCostConsultant,AjeraWIPLabor,AjeraWIPReimbursable,AjeraWIPConsultant,AjeraBilledLabor,AjeraBilledReimbursable,AjeraBilledConsultant,AjeraReceivedLabor,AjeraReceivedReimbursable,AjeraReceivedConsultant,TLInternalKey,TLProjectID,TLProjectName,TLChargeBandInternalKey,TLChargeBandExternalCode,TLSyncModDate,CreateUser,CreateDate,ModUser,ModDate)VALUES('00001.00',' ',' ','Admin','H','N',NULL,NULL,NULL,NULL,NULL,0.0000,0.0000,0.0000,0.0000,'A','B',0.0000,NULL,NULL,NULL,NULL,0.0000,0.0000,0.0000,'C','N',NULL,'N',NULL,NULL,NULL,NULL,'G',0,0.0000,NULL,0,0,0,0,0,NULL,NULL,'General Overhead',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'N',NULL,NULL,NULL,NULL,NULL,0.0000,NULL,0.0000,NULL,NULL,NULL,NULL,'Y','Y','Y',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,'S','S',NULL,NULL,NULL,NULL,' ',0.0000000000,' ',0.0000000000,'N',0.0000,0.0000,0.0000,'N',NULL,NULL,'N','N','N',0.0000,0.0000,NULL,'N','G',0,0.0000,'G',0,0.0000,'C','N','E','E',0,NULL,NULL,'N','N','N','N',0,0.0000,0.0000,0.0000,'B',0,0,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,'N','N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,getutcdate(),NULL,getutcdate());
--INSERT INTO PR (WBS1,WBS2,WBS3,Name,ChargeType,SubLevel,Principal,ProjMgr,Supervisor,ClientID,CLAddress,Fee,ReimbAllow,ConsultFee,BudOHRate,Status,RevType,MultAmt,Org,UnitTable,StartDate,EndDate,PctComp,LabPctComp,ExpPctComp,BillByDefault,BillableWarning,Memo,BudgetedFlag,BudgetedLevels,BillWBS1,BillWBS2,BillWBS3,XCharge,XChargeMethod,XChargeMult,Description,Closed,ReadOnly,DefaultEffortDriven,DefaultTaskType,VersionID,ContactID,CLBillingAddr,LongName,Address1,Address2,Address3,City,State,Zip,County,Country,FederalInd,ProjectType,Responsibility,Referable,EstCompletionDate,ActCompletionDate,ContractDate,BidDate,ComplDateComment,FirmCost,FirmCostComment,TotalProjectCost,TotalCostComment,OpportunityID,ClientConfidential,ClientAlias,AvailableForCRM,ReadyForApproval,ReadyForProcessing,BillingClientID,BillingContactID,Phone,Fax,EMail,ProposalWBS1,CostRateMeth,CostRateTableNo,PayRateMeth,PayRateTableNo,Locale,LineItemApproval,LineItemApprovalEK,BudgetSource,BudgetLevel,ProfServicesComplDate,ConstComplDate,ProjectCurrencyCode,ProjectExchangeRate,BillingCurrencyCode,BillingExchangeRate,RestrictChargeCompanies,FeeBillingCurrency,ReimbAllowBillingCurrency,ConsultFeeBillingCurrency,RevUpsetLimits,RevUpsetWBS2,RevUpsetWBS3,RevUpsetIncludeComp,RevUpsetIncludeCons,RevUpsetIncludeReimb,PORMBRate,POCNSRate,PlanID,TKCheckRPDate,ICBillingLab,ICBillingLabMethod,ICBillingLabMult,ICBillingExp,ICBillingExpMethod,ICBillingExpMult,RequireComments,TKCheckRPPlannedHrs,BillByDefaultConsultants,BillByDefaultOtherExp,BillByDefaultORTable,PhoneFormat,FaxFormat,RevType2,RevType3,RevType4,RevType5,RevUpsetCategoryToAdjust,FeeFunctionalCurrency,ReimbAllowFunctionalCurrency,ConsultFeeFunctionalCurrency,RevenueMethod,ICBillingLabTableNo,ICBillingExpTableNo,Biller,FeeDirLab,FeeDirExp,ReimbAllowExp,ReimbAllowCons,FeeDirLabBillingCurrency,FeeDirExpBillingCurrency,ReimbAllowExpBillingCurrency,ReimbAllowConsBillingCurrency,FeeDirLabFunctionalCurrency,FeeDirExpFunctionalCurrency,ReimbAllowExpFunctionalCurrency,ReimbAllowConsFunctionalCurrency,RevUpsetIncludeCompDirExp,RevUpsetIncludeReimbCons,AwardType,Duration,ContractTypeGovCon,CompetitionType,MasterContract,Solicitation,NAICS,OurRole,AjeraSync,ServProCode,FESurchargePct,FESurcharge,FEAddlExpensesPct,FEAddlExpenses,FEOtherPct,FEOther,ProjectTemplate,AjeraSpentLabor,AjeraSpentReimbursable,AjeraSpentConsultant,AjeraCostLabor,AjeraCostReimbursable,AjeraCostConsultant,AjeraWIPLabor,AjeraWIPReimbursable,AjeraWIPConsultant,AjeraBilledLabor,AjeraBilledReimbursable,AjeraBilledConsultant,AjeraReceivedLabor,AjeraReceivedReimbursable,AjeraReceivedConsultant,TLInternalKey,TLProjectID,TLProjectName,TLChargeBandInternalKey,TLChargeBandExternalCode,TLSyncModDate,CreateUser,CreateDate,ModUser,ModDate)VALUES('00002.00',' ',' ','Vacation','H','N',NULL,NULL,NULL,NULL,NULL,0.0000,0.0000,0.0000,0.0000,'A','B',0.0000,NULL,NULL,NULL,NULL,0.0000,0.0000,0.0000,'C','N',NULL,'N',NULL,NULL,NULL,NULL,'G',0,0.0000,NULL,0,0,0,0,0,NULL,NULL,'Vacation',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'N',NULL,NULL,NULL,NULL,NULL,0.0000,NULL,0.0000,NULL,NULL,NULL,NULL,'Y','Y','Y',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,'S','S',NULL,NULL,NULL,NULL,' ',0.0000000000,' ',0.0000000000,'N',0.0000,0.0000,0.0000,'N',NULL,NULL,'N','N','N',0.0000,0.0000,NULL,'N','G',0,0.0000,'G',0,0.0000,'C','N','E','E',0,NULL,NULL,'N','N','N','N',0,0.0000,0.0000,0.0000,'B',0,0,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,'N','N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,getutcdate(),NULL,getutcdate());
--INSERT INTO PR (WBS1,WBS2,WBS3,Name,ChargeType,SubLevel,Principal,ProjMgr,Supervisor,ClientID,CLAddress,Fee,ReimbAllow,ConsultFee,BudOHRate,Status,RevType,MultAmt,Org,UnitTable,StartDate,EndDate,PctComp,LabPctComp,ExpPctComp,BillByDefault,BillableWarning,Memo,BudgetedFlag,BudgetedLevels,BillWBS1,BillWBS2,BillWBS3,XCharge,XChargeMethod,XChargeMult,Description,Closed,ReadOnly,DefaultEffortDriven,DefaultTaskType,VersionID,ContactID,CLBillingAddr,LongName,Address1,Address2,Address3,City,State,Zip,County,Country,FederalInd,ProjectType,Responsibility,Referable,EstCompletionDate,ActCompletionDate,ContractDate,BidDate,ComplDateComment,FirmCost,FirmCostComment,TotalProjectCost,TotalCostComment,OpportunityID,ClientConfidential,ClientAlias,AvailableForCRM,ReadyForApproval,ReadyForProcessing,BillingClientID,BillingContactID,Phone,Fax,EMail,ProposalWBS1,CostRateMeth,CostRateTableNo,PayRateMeth,PayRateTableNo,Locale,LineItemApproval,LineItemApprovalEK,BudgetSource,BudgetLevel,ProfServicesComplDate,ConstComplDate,ProjectCurrencyCode,ProjectExchangeRate,BillingCurrencyCode,BillingExchangeRate,RestrictChargeCompanies,FeeBillingCurrency,ReimbAllowBillingCurrency,ConsultFeeBillingCurrency,RevUpsetLimits,RevUpsetWBS2,RevUpsetWBS3,RevUpsetIncludeComp,RevUpsetIncludeCons,RevUpsetIncludeReimb,PORMBRate,POCNSRate,PlanID,TKCheckRPDate,ICBillingLab,ICBillingLabMethod,ICBillingLabMult,ICBillingExp,ICBillingExpMethod,ICBillingExpMult,RequireComments,TKCheckRPPlannedHrs,BillByDefaultConsultants,BillByDefaultOtherExp,BillByDefaultORTable,PhoneFormat,FaxFormat,RevType2,RevType3,RevType4,RevType5,RevUpsetCategoryToAdjust,FeeFunctionalCurrency,ReimbAllowFunctionalCurrency,ConsultFeeFunctionalCurrency,RevenueMethod,ICBillingLabTableNo,ICBillingExpTableNo,Biller,FeeDirLab,FeeDirExp,ReimbAllowExp,ReimbAllowCons,FeeDirLabBillingCurrency,FeeDirExpBillingCurrency,ReimbAllowExpBillingCurrency,ReimbAllowConsBillingCurrency,FeeDirLabFunctionalCurrency,FeeDirExpFunctionalCurrency,ReimbAllowExpFunctionalCurrency,ReimbAllowConsFunctionalCurrency,RevUpsetIncludeCompDirExp,RevUpsetIncludeReimbCons,AwardType,Duration,ContractTypeGovCon,CompetitionType,MasterContract,Solicitation,NAICS,OurRole,AjeraSync,ServProCode,FESurchargePct,FESurcharge,FEAddlExpensesPct,FEAddlExpenses,FEOtherPct,FEOther,ProjectTemplate,AjeraSpentLabor,AjeraSpentReimbursable,AjeraSpentConsultant,AjeraCostLabor,AjeraCostReimbursable,AjeraCostConsultant,AjeraWIPLabor,AjeraWIPReimbursable,AjeraWIPConsultant,AjeraBilledLabor,AjeraBilledReimbursable,AjeraBilledConsultant,AjeraReceivedLabor,AjeraReceivedReimbursable,AjeraReceivedConsultant,TLInternalKey,TLProjectID,TLProjectName,TLChargeBandInternalKey,TLChargeBandExternalCode,TLSyncModDate,CreateUser,CreateDate,ModUser,ModDate)VALUES('00003.00',' ',' ','Sick Leave','H','N',NULL,NULL,NULL,NULL,NULL,0.0000,0.0000,0.0000,0.0000,'A','B',0.0000,NULL,NULL,NULL,NULL,0.0000,0.0000,0.0000,'C','N',NULL,'N',NULL,NULL,NULL,NULL,'G',0,0.0000,NULL,0,0,0,0,0,NULL,NULL,'Sick Leave',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'N',NULL,NULL,NULL,NULL,NULL,0.0000,NULL,0.0000,NULL,NULL,NULL,NULL,'Y','Y','Y',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,'S','S',NULL,NULL,NULL,NULL,' ',0.0000000000,' ',0.0000000000,'N',0.0000,0.0000,0.0000,'N',NULL,NULL,'N','N','N',0.0000,0.0000,NULL,'N','G',0,0.0000,'G',0,0.0000,'C','N','E','E',0,NULL,NULL,'N','N','N','N',0,0.0000,0.0000,0.0000,'B',0,0,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,'N','N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,getutcdate(),NULL,getutcdate());
--INSERT INTO PR (WBS1,WBS2,WBS3,Name,ChargeType,SubLevel,Principal,ProjMgr,Supervisor,ClientID,CLAddress,Fee,ReimbAllow,ConsultFee,BudOHRate,Status,RevType,MultAmt,Org,UnitTable,StartDate,EndDate,PctComp,LabPctComp,ExpPctComp,BillByDefault,BillableWarning,Memo,BudgetedFlag,BudgetedLevels,BillWBS1,BillWBS2,BillWBS3,XCharge,XChargeMethod,XChargeMult,Description,Closed,ReadOnly,DefaultEffortDriven,DefaultTaskType,VersionID,ContactID,CLBillingAddr,LongName,Address1,Address2,Address3,City,State,Zip,County,Country,FederalInd,ProjectType,Responsibility,Referable,EstCompletionDate,ActCompletionDate,ContractDate,BidDate,ComplDateComment,FirmCost,FirmCostComment,TotalProjectCost,TotalCostComment,OpportunityID,ClientConfidential,ClientAlias,AvailableForCRM,ReadyForApproval,ReadyForProcessing,BillingClientID,BillingContactID,Phone,Fax,EMail,ProposalWBS1,CostRateMeth,CostRateTableNo,PayRateMeth,PayRateTableNo,Locale,LineItemApproval,LineItemApprovalEK,BudgetSource,BudgetLevel,ProfServicesComplDate,ConstComplDate,ProjectCurrencyCode,ProjectExchangeRate,BillingCurrencyCode,BillingExchangeRate,RestrictChargeCompanies,FeeBillingCurrency,ReimbAllowBillingCurrency,ConsultFeeBillingCurrency,RevUpsetLimits,RevUpsetWBS2,RevUpsetWBS3,RevUpsetIncludeComp,RevUpsetIncludeCons,RevUpsetIncludeReimb,PORMBRate,POCNSRate,PlanID,TKCheckRPDate,ICBillingLab,ICBillingLabMethod,ICBillingLabMult,ICBillingExp,ICBillingExpMethod,ICBillingExpMult,RequireComments,TKCheckRPPlannedHrs,BillByDefaultConsultants,BillByDefaultOtherExp,BillByDefaultORTable,PhoneFormat,FaxFormat,RevType2,RevType3,RevType4,RevType5,RevUpsetCategoryToAdjust,FeeFunctionalCurrency,ReimbAllowFunctionalCurrency,ConsultFeeFunctionalCurrency,RevenueMethod,ICBillingLabTableNo,ICBillingExpTableNo,Biller,FeeDirLab,FeeDirExp,ReimbAllowExp,ReimbAllowCons,FeeDirLabBillingCurrency,FeeDirExpBillingCurrency,ReimbAllowExpBillingCurrency,ReimbAllowConsBillingCurrency,FeeDirLabFunctionalCurrency,FeeDirExpFunctionalCurrency,ReimbAllowExpFunctionalCurrency,ReimbAllowConsFunctionalCurrency,RevUpsetIncludeCompDirExp,RevUpsetIncludeReimbCons,AwardType,Duration,ContractTypeGovCon,CompetitionType,MasterContract,Solicitation,NAICS,OurRole,AjeraSync,ServProCode,FESurchargePct,FESurcharge,FEAddlExpensesPct,FEAddlExpenses,FEOtherPct,FEOther,ProjectTemplate,AjeraSpentLabor,AjeraSpentReimbursable,AjeraSpentConsultant,AjeraCostLabor,AjeraCostReimbursable,AjeraCostConsultant,AjeraWIPLabor,AjeraWIPReimbursable,AjeraWIPConsultant,AjeraBilledLabor,AjeraBilledReimbursable,AjeraBilledConsultant,AjeraReceivedLabor,AjeraReceivedReimbursable,AjeraReceivedConsultant,TLInternalKey,TLProjectID,TLProjectName,TLChargeBandInternalKey,TLChargeBandExternalCode,TLSyncModDate,CreateUser,CreateDate,ModUser,ModDate)VALUES('00004.00',' ',' ','Holiday','H','N',NULL,NULL,NULL,NULL,NULL,0.0000,0.0000,0.0000,0.0000,'A','B',0.0000,NULL,NULL,NULL,NULL,0.0000,0.0000,0.0000,'C','N',NULL,'N',NULL,NULL,NULL,NULL,'G',0,0.0000,NULL,0,0,0,0,0,NULL,NULL,'Holiday',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'N',NULL,NULL,NULL,NULL,NULL,0.0000,NULL,0.0000,NULL,NULL,NULL,NULL,'Y','Y','Y',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,'S','S',NULL,NULL,NULL,NULL,' ',0.0000000000,' ',0.0000000000,'N',0.0000,0.0000,0.0000,'N',NULL,NULL,'N','N','N',0.0000,0.0000,NULL,'N','G',0,0.0000,'G',0,0.0000,'C','N','E','E',0,NULL,NULL,'N','N','N','N',0,0.0000,0.0000,0.0000,'B',0,0,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,'N','N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,getutcdate(),NULL,getutcdate());
--INSERT INTO PR (WBS1,WBS2,WBS3,Name,ChargeType,SubLevel,Principal,ProjMgr,Supervisor,ClientID,CLAddress,Fee,ReimbAllow,ConsultFee,BudOHRate,Status,RevType,MultAmt,Org,UnitTable,StartDate,EndDate,PctComp,LabPctComp,ExpPctComp,BillByDefault,BillableWarning,Memo,BudgetedFlag,BudgetedLevels,BillWBS1,BillWBS2,BillWBS3,XCharge,XChargeMethod,XChargeMult,Description,Closed,ReadOnly,DefaultEffortDriven,DefaultTaskType,VersionID,ContactID,CLBillingAddr,LongName,Address1,Address2,Address3,City,State,Zip,County,Country,FederalInd,ProjectType,Responsibility,Referable,EstCompletionDate,ActCompletionDate,ContractDate,BidDate,ComplDateComment,FirmCost,FirmCostComment,TotalProjectCost,TotalCostComment,OpportunityID,ClientConfidential,ClientAlias,AvailableForCRM,ReadyForApproval,ReadyForProcessing,BillingClientID,BillingContactID,Phone,Fax,EMail,ProposalWBS1,CostRateMeth,CostRateTableNo,PayRateMeth,PayRateTableNo,Locale,LineItemApproval,LineItemApprovalEK,BudgetSource,BudgetLevel,ProfServicesComplDate,ConstComplDate,ProjectCurrencyCode,ProjectExchangeRate,BillingCurrencyCode,BillingExchangeRate,RestrictChargeCompanies,FeeBillingCurrency,ReimbAllowBillingCurrency,ConsultFeeBillingCurrency,RevUpsetLimits,RevUpsetWBS2,RevUpsetWBS3,RevUpsetIncludeComp,RevUpsetIncludeCons,RevUpsetIncludeReimb,PORMBRate,POCNSRate,PlanID,TKCheckRPDate,ICBillingLab,ICBillingLabMethod,ICBillingLabMult,ICBillingExp,ICBillingExpMethod,ICBillingExpMult,RequireComments,TKCheckRPPlannedHrs,BillByDefaultConsultants,BillByDefaultOtherExp,BillByDefaultORTable,PhoneFormat,FaxFormat,RevType2,RevType3,RevType4,RevType5,RevUpsetCategoryToAdjust,FeeFunctionalCurrency,ReimbAllowFunctionalCurrency,ConsultFeeFunctionalCurrency,RevenueMethod,ICBillingLabTableNo,ICBillingExpTableNo,Biller,FeeDirLab,FeeDirExp,ReimbAllowExp,ReimbAllowCons,FeeDirLabBillingCurrency,FeeDirExpBillingCurrency,ReimbAllowExpBillingCurrency,ReimbAllowConsBillingCurrency,FeeDirLabFunctionalCurrency,FeeDirExpFunctionalCurrency,ReimbAllowExpFunctionalCurrency,ReimbAllowConsFunctionalCurrency,RevUpsetIncludeCompDirExp,RevUpsetIncludeReimbCons,AwardType,Duration,ContractTypeGovCon,CompetitionType,MasterContract,Solicitation,NAICS,OurRole,AjeraSync,ServProCode,FESurchargePct,FESurcharge,FEAddlExpensesPct,FEAddlExpenses,FEOtherPct,FEOther,ProjectTemplate,AjeraSpentLabor,AjeraSpentReimbursable,AjeraSpentConsultant,AjeraCostLabor,AjeraCostReimbursable,AjeraCostConsultant,AjeraWIPLabor,AjeraWIPReimbursable,AjeraWIPConsultant,AjeraBilledLabor,AjeraBilledReimbursable,AjeraBilledConsultant,AjeraReceivedLabor,AjeraReceivedReimbursable,AjeraReceivedConsultant,TLInternalKey,TLProjectID,TLProjectName,TLChargeBandInternalKey,TLChargeBandExternalCode,TLSyncModDate,CreateUser,CreateDate,ModUser,ModDate)VALUES('00005.00',' ',' ','Business Development','H','N',NULL,NULL,NULL,NULL,NULL,0.0000,0.0000,0.0000,0.0000,'A','B',0.0000,NULL,NULL,NULL,NULL,0.0000,0.0000,0.0000,'C','N',NULL,'N',NULL,NULL,NULL,NULL,'G',0,0.0000,NULL,0,0,0,0,0,NULL,NULL,'Busines Development',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'N',NULL,NULL,NULL,NULL,NULL,0.0000,NULL,0.0000,NULL,NULL,NULL,NULL,'Y','Y','Y',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,NULL,'S','S',NULL,NULL,NULL,NULL,' ',0.0000000000,' ',0.0000000000,'N',0.0000,0.0000,0.0000,'N',NULL,NULL,'N','N','N',0.0000,0.0000,NULL,'N','G',0,0.0000,'G',0,0.0000,'C','N','E','E',0,NULL,NULL,'N','N','N','N',0,0.0000,0.0000,0.0000,'B',0,0,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,'N','N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,getutcdate(),NULL,getutcdate());

DELETE FROM PurchaseTemplate;
INSERT INTO PurchaseTemplate (Name,Options)VALUES('_DEFAULT','<options><resizeReport>N</resizeReport><ActuateReport></ActuateReport><printFirm>N</printFirm><printByline>N</printByline><printAddress>N</printAddress><firmLeftPos>1.66</firmLeftPos><firmTop>0.01</firmTop><firmAlign>center</firmAlign><firmName></firmName><firmByLine></firmByLine><firmAddress1></firmAddress1><firmAddress2></firmAddress2><firmAddress3></firmAddress3><firmAddress4></firmAddress4><chkPrintTopClause>Y</chkPrintTopClause><chkPrintBottomClause>Y</chkPrintBottomClause><txtTopClauseHeader>B</txtTopClauseHeader><chkPageBreakTopClause>N</chkPageBreakTopClause><chkPageBreakBottomClause>N</chkPageBreakBottomClause><txtShortDate>M/d/yy</txtShortDate><txtMediumDate>MMMM d, yyyy</txtMediumDate><txtLongDate>dddd, MMMM dd, yyyy</txtLongDate><txtFooter></txtFooter><printFooterLine>Y</printFooterLine><printPageNumbers>Y</printPageNumbers><Agreement>B</Agreement><RFQColumn id="1"><colName>Item</colName><selectedRFQ>Y</selectedRFQ><labelText>Item</labelText><colType>Item</colType><colWidth>0.308</colWidth><dataType>S</dataType></RFQColumn><RFQColumn id="2"><colName>Quantity</colName><selectedRFQ>Y</selectedRFQ><labelText>Quantity</labelText><colType>Quantity</colType><colWidth>0.966</colWidth><dataType>N</dataType></RFQColumn><RFQColumn id="3"><colName>Item Code</colName><selectedRFQ>Y</selectedRFQ><labelText>Item Code</labelText><colType>ItemCode</colType><colWidth>1</colWidth><dataType>S</dataType></RFQColumn><RFQColumn id="4"><colName>Item Description</colName><selectedRFQ>Y</selectedRFQ><labelText>Description</labelText><colType>ItemDescription</colType><colWidth>2.215</colWidth><dataType>S</dataType></RFQColumn><RFQColumn id="5"><colName>Requested By</colName><selectedRFQ>Y</selectedRFQ><labelText>Requested By</labelText><colType>RequestedBy</colType><colWidth>0.824</colWidth><dataType>S</dataType></RFQColumn><RFQColumn id="6"><colName>Date Required</colName><selectedRFQ>Y</selectedRFQ><labelText>Date Required</labelText><colType>DateRequired</colType><colWidth>0.849</colWidth><dataType>D</dataType></RFQColumn><RFQColumn id="7"><colName>Unit Price</colName><selectedRFQ>Y</selectedRFQ><labelText>Unit Price</labelText><colType>UnitPrice</colType><colWidth>0.618</colWidth><dataType>N</dataType></RFQColumn><RFQColumn id="8"><colName>REQ #</colName><selectedRFQ>Y</selectedRFQ><labelText>REQ #</labelText><colType>REQNumber</colType><colWidth>0.618</colWidth><dataType>N</dataType></RFQColumn><RFQColumn id="9"><colName>External Notes</colName><selectedRFQ>Y</selectedRFQ><labelText>NOTES:</labelText><colType>ExternalNotes</colType><colWidth>7.441</colWidth><dataType>S</dataType></RFQColumn><POColumn id="1"><colName>Seq</colName><selectedPO>Y</selectedPO><labelText>Seq</labelText><colType>Seq</colType><colWidth>0.375</colWidth><dataType>S</dataType></POColumn><POColumn id="2"><colName>Item Description</colName><selectedPO>Y</selectedPO><labelText>Description</labelText><colType>ItemDescription</colType><colWidth>2.249</colWidth><dataType>S</dataType></POColumn><POColumn id="3"><colName>Item Number</colName><selectedPO>Y</selectedPO><labelText>Item Number</labelText><colType>VendorNumber</colType><colWidth>1.436</colWidth><dataType>S</dataType></POColumn><POColumn id="4"><colName>Quantity</colName><selectedPO>Y</selectedPO><labelText>Quantity</labelText><colType>Quantity</colType><colWidth>0.994</colWidth><dataType>N</dataType></POColumn><POColumn id="5"><colName>Unit Price</colName><selectedPO>Y</selectedPO><labelText>Unit Price</labelText><colType>UnitPrice</colType><colWidth>0.639</colWidth><dataType>N</dataType></POColumn><POColumn id="6"><colName>Net Amount</colName><selectedPO>Y</selectedPO><labelText>Net Amount</labelText><colType>NetAmount</colType><colWidth>1</colWidth><dataType>N</dataType></POColumn><POColumn id="7"><colName>Due Date</colName><selectedPO>Y</selectedPO><labelText>Due Date</labelText><colType>DueDate</colType><colWidth>0.665</colWidth><dataType>D</dataType></POColumn><POColumn id="8"><colName>External Notes</colName><selectedPO>Y</selectedPO><labelText>NOTES:</labelText><colType>ExternalNotes</colType><colWidth>7.441</colWidth><dataType>S</dataType></POColumn><useSignature>N</useSignature><sigTopMargin>0.50</sigTopMargin><sigLeftMargin>0.01</sigLeftMargin><sigLabel>Authorized By:</sigLabel><sigDateLabel>Date:</sigDateLabel><sigLabelWidth>0.85</sigLabelWidth><sigDateLabelWidth>0.50</sigDateLabelWidth><sigLineWidth>3.00</sigLineWidth><sigDateLineWidth>0.50</sigDateLineWidth><sigAuthTopMargin>0.80</sigAuthTopMargin><sigAuthLeftMargin>0.86</sigAuthLeftMargin></options>');

COMMIT TRANSACTION

IF @enUSEnabled = 'Y' EXEC ('AddIndustryDefaultsAEenUS')
IF @enGBEnabled = 'Y' EXEC ('AddIndustryDefaultsAEenGB')
IF @deDEEnabled = 'Y' EXEC ('AddIndustryDefaultsAEdeDE')
IF @esESEnabled = 'Y' EXEC ('AddIndustryDefaultsAEesES')
IF @frCAEnabled = 'Y' EXEC ('AddIndustryDefaultsAEfrCA')
IF @frFREnabled = 'Y' EXEC ('AddIndustryDefaultsAEfrFR')
IF @nlNLEnabled = 'Y' EXEC ('AddIndustryDefaultsAEnlNL')
IF @ptBREnabled = 'Y' EXEC ('AddIndustryDefaultsAEptBR')

PRINT ' '
PRINT 'Load of AE Industry Defaults complete'
	
END
GO
