SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_OpportunityMasterContract_Validation]
    @OpportunityID VARCHAR(32)
  , @ContractType VARCHAR(32)
AS
/* =======================================================================================
    Halff Associates, Inc.   All rights reserved.
    20200624 - Craig H. Anderson
            Validate that the Sales grid is empty for IDIQ/On Call/MSA/GSA type contracts.
            Validate that there are no Phases or Tasks associated with the Opportunity
   ======================================================================================= */
SET NOCOUNT ON;
    BEGIN
        DECLARE @errorMessage VARCHAR(MAX) = '';
        -- Check for Sales
        IF EXISTS
            (
                SELECT 'x'
                FROM dbo.Opportunity o
                    LEFT JOIN
                        (
                            SELECT OpportunityID
                                 , COUNT(*) AS Sales
                            FROM dbo.Opportunities_Sales WITH ( NOLOCK )
                            GROUP BY OpportunityID
                        )            s
                        ON s.OpportunityID = o.OpportunityID
                WHERE o.OpportunityID = @OpportunityID
                      AND @ContractType <> 'Individual Project/Task Order'
                      AND ISNULL(s.Sales, 0) <> 0
            )
            SET @errorMessage = CONCAT(@errorMessage, 'Sales must be zero for Master Contracts. ');
        -- Check for Phases or Tasks
        IF EXISTS
            (
                SELECT 'x'
                FROM dbo.Opportunity o
                    INNER JOIN
                        (
                            SELECT OpportunityID
                                 , COUNT(*) AS items
                            FROM dbo.PR WITH ( NOLOCK )
                            WHERE WBS2 <> ' '
                                  OR WBS3 <> ' '
                            GROUP BY OpportunityID
                        )            p
                        ON o.OpportunityID = p.OpportunityID
                WHERE o.OpportunityID = @OpportunityID
                      AND @ContractType <> 'Individual Project/Task Order'
                      AND p.items > 0
            )
            SET @errorMessage = CONCAT(@errorMessage, 'Master Contracts may not have Phases or Tasks.  ');

        IF EXISTS
            (
                SELECT 'x'
                FROM dbo.Opportunity o
                WHERE o.OpportunityID = @OpportunityID
					  AND @ContractType NOT IN ('Individual Project/Task Order', 'Administrative Qualification')
                      AND RIGHT(ISNULL(o.Opportunity, ''), 3) <> '000'
            )
            SET @errorMessage = CONCAT(@errorMessage, 'Master Contract AVO suffix must be "000".  ');

        --Raise Error if triggered
        IF @errorMessage <> ''
            RAISERROR(@errorMessage, 16, 1);



    END;
GO
