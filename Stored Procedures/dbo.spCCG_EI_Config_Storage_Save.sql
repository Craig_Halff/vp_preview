SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_Config_Storage_Save]
	@fieldsSQL			Nvarchar(max),
	@updateValuesSql	Nvarchar(max),
	@insertValuesSql	Nvarchar(max),
	@deleteSeqs			Nvarchar(max)
AS
BEGIN
	-- EXEC [spCCG_EI_Config_Storage_Save] 'asdasd', '', '', 'aa' --failure test
	-- EXEC [spCCG_EI_Config_Storage_Save] '', '', '', '' --update folder history only

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @sSQL		Nvarchar(max)
	DECLARE @safeSql	int
	DECLARE @ids		TABLE (id int)
	DECLARE @i			int
	DECLARE @newI		int
	DECLARE @seq		Nvarchar(50)

	BEGIN TRANSACTION
	BEGIN TRY
		-- Updates
		IF ISNULL(@updateValuesSql, '') <> '' BEGIN
			SET @sSQL = N'
				UPDATE cs
					SET [Description]	= temp.[Description],
						[UseType]		= temp.[UseType],
						[StorageType]	= temp.[StorageType],
						[Server]		= temp.[Server],
						[Catalog]		= temp.[Catalog],
						[Root]			= temp.[Root],
						[UserName]		= temp.[UserName],
						[Password]		= temp.[Password],
						[RevisionType]	= temp.[RevisionType],
						[MarkupType]	= temp.[MarkupType],
						[Status]		= temp.[Status],
						[Detail]		= temp.[Detail],
						[AllowDelete]	= temp.[AllowDelete],
						[DateChanged]	= temp.[DateChanged],
						[ChangedBy]		= temp.[ChangedBy]
					FROM CCG_EI_ConfigStorage cs
					JOIN (
						VALUES ' + @updateValuesSql + '
					) temp (' + @fieldsSQL + ', Seq) ON temp.Seq = cs.Seq'
			--PRINT @sSQL
			EXEC (@sSQL)
		END

		-- Inserts
		IF ISNULL(@insertValuesSql, '') <> '' BEGIN
			SET @sSQL = N'
				INSERT INTO CCG_EI_ConfigStorage
					(' + @fieldsSQL + ')
					VALUES
					' + @insertValuesSql
			--PRINT @sSQL
			EXEC (@sSQL)
		END

		-- Deletes
		IF ISNULL(@deleteSeqs, '') <> '' BEGIN
			SET @sSQL = N'
				DELETE FROM CCG_EI_ConfigStorage WHERE Seq in (' + @deleteSeqs + ')'
			--PRINT @sSQL
			EXEC (@sSQL)
		END
	END TRY
	BEGIN CATCH
		SELECT
			ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity, ERROR_STATE() AS ErrorState,
			ERROR_PROCEDURE() AS ErrorProcedure, ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage

		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	END CATCH

	IF @@TRANCOUNT > 0 COMMIT TRANSACTION

	
	IF EXISTS(select * from CCG_EI_ConfigStorage where Status = 'A') and NOT EXISTS(select 'x' from CCG_EI_Config where CharIndex('HIST_FOLDER=N',AdvancedOptions,1) > 0)
		
	BEGIN
	--starting in EI 6.5 we will track history of invoice folder values in the CCG_DOCS_PR table once we start using new storage options
	--insert records for all project invoicefolder values that do not already exist
		INSERT INTO [dbo].[CCG_DOCS_PR]
			   ([FILEID],[WBS1],[WBS2],[WBS3]
			   ,[Invoice],[FilePath]
			   ,[FileDescription],[CreateUser],[CreateDate],[StorageSeq]
			   ,[StoragePathOrID],[RevisionSeq],[FileState]--HIST_FOLDER
			   ,[Category])--,BROWSE_INV/BROWSE_FINAL
		select newid(),c.WBS1,' ',' ',NULL,CustInvoiceFolder,'Invoice Folder','History',GETUTCDATE(),NULL,NULL,0,'HIST_FOLDER','BROWSE_INV'
			   from ProjectCustomTabFields c  WITH (NOLOCK)
			   left join CCG_DOCS_PR d on c.WBS1 = d.WBS1 and c.WBS2 = d.WBS2 and c.CustInvoiceFolder = d.FilePath and d.FileState = 'HIST_FOLDER'
				where c.WBS2 = ' ' and c.WBS3 = ' ' and ISNULL(CustInvoiceFolder,'') <> '' and (CustInvoiceFolder like '\\%' or CustInvoiceFolder like '[A-Za-z]:\%')  
				and d.FILEID IS NULL

	--insert records for all project invoicefolderfinals values that do not already exist
		INSERT INTO [dbo].[CCG_DOCS_PR]
			   ([FILEID],[WBS1],[WBS2],[WBS3]
			   ,[Invoice],[FilePath]
			   ,[FileDescription],[CreateUser],[CreateDate],[StorageSeq]
			   ,[StoragePathOrID],[RevisionSeq],[FileState]--HIST_FOLDER
			   ,[Category])--,BROWSE_INV/BROWSE_FINAL
		select newid(),c.WBS1,' ',' ',NULL,CustInvoiceFolderFinals,'Final Folder','History',GETUTCDATE(),NULL,NULL,0,'HIST_FOLDER','BROWSE_FINAL'
			   from ProjectCustomTabFields c  WITH (NOLOCK)
			   left join CCG_DOCS_PR d on c.WBS1 = d.WBS1 and c.WBS2 = d.WBS2 and c.CustInvoiceFolderFinals = d.FilePath and d.FileState = 'HIST_FOLDER'
				where c.WBS2 = ' ' and c.WBS3 = ' ' and ISNULL(CustInvoiceFolderFinals,'') <> '' and (CustInvoiceFolderFinals like '\\%' or CustInvoiceFolderFinals like '[A-Za-z]:\%')  
				and d.FILEID IS NULL
	END

END
GO
