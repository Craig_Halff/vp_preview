SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_StageChange](
	@WBS1 varchar(30),								-- Project number
	@OldStage varchar(70), @NewStage varchar(70),	-- Old & new invoice stage values (keys)
	@Username varchar(255), @Role varchar(255))		-- Username & EI role
AS BEGIN
/*  exec spCCG_EI_StageChange '031481.000','Accounting','Hold','ADMIN',''
	Copyright (c) 2014 Central Consulting Group. All rights reserved.
	select * from ccg_ei_customcolumns
*/
	declare @ReturnValue int
	declare @ReturnMessage varchar(1024)
	set @ReturnMessage = ''
	set @ReturnValue = 0
	declare @HoldNotes varchar(255)
	set nocount on
	BEGIN TRY
		if @NewStage like 'Hold%' and @returnValue = 0
		begin
			
		if not exists(select 'y' from CCG_EI_CustomColumns where wbs1 = @wbs1 and wbs2 = ' ' and isnull(Comments, ' ') <> ' ')
				select @ReturnValue = -1, @ReturnMessage = 'When "Hold" is selected, "Comments" are required.' 
		end
		
		if @NewStage like '%write-off%' and @ReturnValue = 0
		begin
				
		
		if not exists(select 'y' from CCG_EI_CustomColumns where wbs1 = @wbs1 and wbs2 = ' ' and isnull(Comments, ' ')<> ' ')
				select @ReturnValue = -1, @ReturnMessage = 'When "Write-Off" is selected, "comments" are required.' 
		
		end
	select @ReturnValue as ReturnValue,@ReturnMessage as ReturnMessage

	END TRY
	BEGIN CATCH
		select -1 as  ReturnValue, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ReturnMessage
	END CATCH
END

GO
