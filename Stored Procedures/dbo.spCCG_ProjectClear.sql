SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   Procedure [dbo].[spCCG_ProjectClear] @WBS1 varchar (30), @WBS2 varchar (7), @WBS3 varchar (7)
AS 
/* 
Copyright (c) 2019 Central Consulting Group.  All rights reserved.
09/04/2019	David Springer
			Clear custom fields and grids
			Call this on a project INSERT workflows at phase and task level
08/23/2021	David Springer
			Added Phases, Tasks, and CustPhaseTeams grid
*/
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

	Delete From Projects_CustPhaseTeams Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
	Delete From Projects_FinalInvPkgUserDocuments Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
	Delete From Projects_Phases Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
	Delete From Projects_RelatedProjects Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
	Delete From Projects_Sales Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
	Delete From Projects_Spent Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
	Delete From Projects_Tasks Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
	Delete From Projects_TrackNotes Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3

END
GO
