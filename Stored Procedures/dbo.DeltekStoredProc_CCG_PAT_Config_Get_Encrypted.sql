SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_Get_Encrypted]
	@username	Nvarchar(500) = null,
	@encTimeout	int = null
AS
BEGIN
	-- [DeltekStoredProc_CCG_PAT_Config_Get_Encrypted] @username = 'ADMIN'
	-- [DeltekStoredProc_CCG_PAT_Config_Get_Encrypted] @encTimeout = '8'
	SET NOCOUNT ON;

	IF isnull(@username, '') <> ''
		SELECT dbo.fnCCG_PAT_GetE(@username) as userenc;
	ELSE IF isnull(@encTimeout, 0) <> 0
		SELECT dbo.fnCCG_PAT_GetE(Convert(Nvarchar(20), DATEADD(d, @encTimeout, getdate()), 101)) as timeoutenc;
END;

GO
