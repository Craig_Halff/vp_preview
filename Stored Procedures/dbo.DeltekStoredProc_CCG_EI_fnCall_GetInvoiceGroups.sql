SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_fnCall_GetInvoiceGroups]
	@User	Nvarchar(100)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * FROM dbo.fnCCG_EI_GetInvoiceGroups(@User);
END;
GO
