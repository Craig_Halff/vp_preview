SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_ProjectRevenueEstimated] @WBS1 nvarchar (32)
AS
/*
Copyright 2021 (c) Central Consulting Group, Inc.  All rights reserved.
08/05/2021	David Springer
			Calculate Revenue & Weighted Revenue.
			Calculate standard fee fields from estimated fee fields.
			Deltek will not allow access to Weighted Revenue in column field change.
			Call from a project CHANGE workflow when any Estimated Revenue... field has changed
09/22/2021	David Springer
			Revised for Multi-Team 
*/

BEGIN
	Update p
	Set p.Revenue = px.CustEstimatedLaborFee + px.CustEstimatedExpenseFee + px.CustEstimatedConsultantFee,
		p.WeightedRevenue = (px.CustEstimatedLaborFee + px.CustEstimatedExpenseFee + px.CustEstimatedConsultantFee) * p.Probability / 100,
		p.FeeDirLab = px.CustEstimatedLaborFee,
		p.FeeDirExp = IIF (px.CustBillingType like 'Lump Sum%', px.CustEstimatedExpenseFee, 0),
		p.ReimbAllowExp = IIF (px.CustBillingType like 'Cost Plus%', px.CustEstimatedExpenseFee, 0),
		p.ConsultFee = IIF (px.CustBillingType like 'Lump Sum%', px.CustEstimatedConsultantFee, 0),
		p.ReimbAllowCons = IIF (px.CustBillingType like 'Cost Plus%', px.CustEstimatedConsultantFee, 0)
	From PR p, ProjectCustomTabFields px
	Where p.WBS1 = @WBS1
	  and p.WBS2 = ' '
	  and p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2

	Update PR
	Set Fee = FeeDirExp + FeeDirLab,
		ReimbAllow = ReimbAllowCons + ReimbAllowExp
	Where WBS1 = @WBS1

--	Set Billing & Functional Currency at all levels
	Update PR
	Set FeeDirLabBillingCurrency = FeeDirlab, 
		FeeDirLabFunctionalCurrency = FeeDirLab, 
		FeeDirExpBillingCurrency = FeeDirExp, 
		FeeDirExpFunctionalCurrency = FeeDirExp, 
		FeeBillingCurrency = Fee, 
		FeeFunctionalCurrency = Fee, 
		ConsultFeeBillingCurrency = ConsultFee, 
		ConsultFeeFunctionalCurrency = ConsultFee,
		ReimbAllowConsBillingCurrency = ReimbAllowCons,
		ReimbAllowConsFunctionalCurrency = ReimbAllowCons,
		ReimbAllowExpBillingCurrency = ReimbAllowExp,
		ReimbAllowExpFunctionalCurrency = ReimbAllowExp,
		ReimbAllowBillingCurrency = ReimbAllow,
		ReimbAllowFunctionalCurrency = ReimbAllow
	Where WBS1 = @WBS1

	Update p
	Set p.WeightedRevenue = (px.CustEstimatedLaborFee + px.CustEstimatedExpenseFee + px.CustEstimatedConsultantFee) * p.Probability / total.ProbabilityTotal
	From PR p, ProjectCustomTabFields px, 
		(Select Left (p.WBS1, 6) RootWBS1, Sum (Probability) ProbabilityTotal
		From PR p, ProjectCustomTabFields px
		Where p.WBS1 = px.WBS1
		  and p.WBS2 = px.WBS2
		  and p.WBS2 = ' '
		  and px.CustMultiTeamPursuit = 'Y'
		Group by Left (p.WBS1, 6)) total
	Where Left (p.WBS1, 6) = Left (@WBS1, 6)
	  and p.WBS2 = ' '
	  and p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and px.CustMultiTeamPursuit = 'Y'
	  and Left (px.WBS1, 6) = total.RootWBS1

END
GO
