SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyConvertAccrual]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg Nvarchar(1000) output,
@UserName Nvarchar(32) = 'VisionKeyConvert' -- Add by DP 4.0
,@Period integer = 0,
@EntityType Nvarchar(20)
as
begin -- Procedure
	set nocount on
	declare @OldValue Nvarchar(20)
	declare @NewValue Nvarchar(20)
	Declare @Existing integer
	declare @Entity Nvarchar(20)
	declare @length integer
	declare @CvtType integer
	declare @KeysFetch integer
	declare @RetVal integer
	declare @diag smallint
	declare @Errmsg2 Nvarchar(1000),
			@custlabel	Nvarchar(40),
			@Custlabelplural Nvarchar(40)
	declare @message nvarchar(max)
	set @diag = 0
	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'Accrual'
	set @length = 10
	set @Existing = 0
	set @KeysFetch = 0

	execute InsertKeyCvtDriver @Entity -- populate driver table

-- Add by DP 4.0
	delete from keyconvertDriver where entity = N'Accrual' and TableName = N'CFGPYAccrualsData'
	delete from keyconvertDriver where entity = N'Accrual' and TableName = N'CFGPYAccrualsDescriptions'
	
	
	DECLARE @OldName Nvarchar(40)
	DECLARE @NewName Nvarchar(40)
	DECLARE @PKey Nvarchar(50)
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0
--
	select @custlabel = 'Accrual Code',
			 @Custlabelplural = 'Accrual Codes'
	declare KeysCursor cursor for
		select OldKey, NewKey, Pkey from KeyConvertWork where Entity = @EntityType
	open KeysCursor
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue, 
		@PKey

	set @KeysFetch = @@Fetch_Status

While (@KeysFetch = 0)
	begin
		begin tran
-- Add by DP 4.0
		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany Nvarchar(14)
			DECLARE @RecordCount integer

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				set @RecordCount = (select count(*) from KeyConvertWork where Entity = @EntityType)

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),N'KC','Y',  @custlabel + ' Key Convert ('+rtrim(convert(Nvarchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, @custlabel + ' Key Convert', @custlabel);
			end

			Set @PostControlFlag = 1
		end
		set @Existing = 0
		select @Existing =  1, @NewName = Description from CFGPYAccruals where Code = @NewValue
		select @OldName = Description from CFGPYAccruals where Code = @OldValue
--

	If (@Existing = 1)
		begin
		  if (@DeleteExisting = 0)
			begin
			SET @message = dbo.GetMessage('MsgNumAnExistingConvWillNotCont',@custlabel,@NewValue,@custlabel,@custLabelPlural,'','','','','')
			RAISERROR(@message,16,3)
				close keysCursor
				deallocate keysCursor
				if (@@Trancount > 0)
				   ROLLBACK TRANSACTION
				return(50002) -- user defined error
			end
			if (@NewValue = @OldValue)
				begin
				SET @message = dbo.GetMessage('MsgAreSameNumConvNotCont',@custlabelPlural,'','','','','','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end
				declare @MulticompanyEnabled Nvarchar(1)
				declare @MulticurrencyEnabled Nvarchar(1)
				declare @NewFunctionalCurrencyCode Nvarchar(3)
				declare @OldFunctionalCurrencyCode Nvarchar(3)
				declare @oldCompay Nvarchar(14)
				declare @newCompay Nvarchar(14)
				select @MulticompanyEnabled = MulticompanyEnabled,
			   		 @MulticurrencyEnabled = MulticurrencyEnabled from FW_CFGSystem
				
				if (@MulticompanyEnabled = 'Y')
					begin
						select @oldCompay = old.Company,
								 @newCompay = new.Company from CFGPYAccruals old,CFGPYAccruals new, cfgformat 
								where old.Code = @OldValue and new.Code = @NewValue
						if (@oldCompay <> @newCompay)
							begin
							SET @message = dbo.GetMessage('MsgWithMultiCmpYouCannotMerge',@custlabelPlural,@custlabel,@OldValue,@oldCompay,@custlabel,@NewValue,@newCompay,'','')
							RAISERROR(@message,16,3)
								close keysCursor
								deallocate keysCursor
								if (@@Trancount > 0)
								   ROLLBACK TRANSACTION
								return(50003) -- user defined error
							end
					end
/* Removed by DP 4.0
			  exec @RetVal = KeyCvt @Entity = @Entity,
											@CvtType = @CvtType,
											@Length = @Length,
											@ErrMsg = @ErrMsg2 output,
											@Diag = @Diag,
											@ConstrFlag = 3 -- UpdateTables
			Delete from CFGPYAccruals where Code = @OldValue
*/

	        Update EMAccrual
	               Set EMAccrual.CurrentEarned = EMAccrual.CurrentEarned + Tmp.CurrentEarned, 
	               EMAccrual.CurrentTaken = EMAccrual.CurrentTaken + Tmp.CurrentTaken, 
	               EMAccrual.HoursToAccrue = EMAccrual.HoursToAccrue + Tmp.HoursToAccrue 
				from EMAccrual Inner Join EMAccrual As Tmp On EMAccrual.Employee = Tmp.Employee and EMAccrual.EmployeeCompany = Tmp.EmployeeCompany
	               Where EMAccrual.Code = @NewValue And Tmp.Code = @OldValue

            Delete FROM EMAccrual Where EMAccrual.Code = @OldValue 
                   And Exists (Select 'x' from EMAccrual AS Tmp Where Tmp.Code = @NewValue 
                   And Tmp.Employee = EMAccrual.Employee and EMAccrual.EmployeeCompany = Tmp.EmployeeCompany)

	        Update EMAccrualDetail
	               Set EMAccrualDetail.Earned = EMAccrualDetail.Earned + Tmp.Earned, 
	               EMAccrualDetail.Taken = EMAccrualDetail.Taken + Tmp.Taken, 
	               EMAccrualDetail.EarnedSinceLastPayroll = EMAccrualDetail.EarnedSinceLastPayroll + Tmp.EarnedSinceLastPayroll, 
	               EMAccrualDetail.TakenSinceLastPayroll = EMAccrualDetail.TakenSinceLastPayroll + Tmp.TakenSinceLastPayroll 
				from EMAccrualDetail Inner Join EMAccrualDetail As Tmp On EMAccrualDetail.Employee = Tmp.Employee and EMAccrualDetail.EmployeeCompany = Tmp.EmployeeCompany 
	               And EMAccrualDetail.Period = Tmp.Period And EMAccrualDetail.PostSeq = Tmp.PostSeq 
	               Where EMAccrualDetail.Code = @NewValue And Tmp.Code = @OldValue
	
	        	if (@@RowCount > 0)
	            Delete FROM EMAccrualDetail Where EMAccrualDetail.Code = @OldValue 
	                   And Exists (Select 'x' from EMAccrualDetail AS Tmp Where Tmp.Code = @NewValue 
	                   And Tmp.Employee = EMAccrualDetail.Employee and EMAccrualDetail.EmployeeCompany = Tmp.EmployeeCompany And Tmp.Period = EMAccrualDetail.Period 
	                   And Tmp.PostSeq = EMAccrualDetail.PostSeq)		


            Delete FROM PYChecksA Where PYChecksA.Code = @OldValue 
                   And Exists (Select 'x' from PYChecksA AS Tmp Where Tmp.Code = @NewValue 
                   And Tmp.Employee = PYChecksA.Employee 
						 And Tmp.Period = PYChecksA.Period 
                   And Tmp.PostSeq = PYChecksA.PostSeq)	
	    END --Existing

-- Add by DP 4.0
		Else	-- Not Existing
		begin
			select * into #TempKeyConvert from CFGPYAccrualsData where Code = @OldValue
			Update #TempKeyConvert set Code=@NewValue
			Insert CFGPYAccrualsData select * from #TempKeyConvert
			Drop Table #TempKeyConvert
			
			select * into #TempKeyConvert1 from CFGPYAccrualsDescriptions where Code = @OldValue
			Update #TempKeyConvert1 set Code=@NewValue
			Insert CFGPYAccrualsDescriptions select * from #TempKeyConvert1
			Drop Table #TempKeyConvert1
			
			set @NewName = @OldName
		end

		exec @RetVal = KeyCvt @Entity = @Entity,
					 @CvtType = @CvtType,
					 @Length = @Length,
					 @ErrMsg = @ErrMsg2 output,
					 @Diag = @Diag,
					 @NewValue = @NewValue,
					 @OldValue = @OldValue,
					 @ConstrFlag = 3 -- UpdateTables

		Delete from CFGPYAccrualsData where Code = @OldValue
		Delete from CFGPYAccrualsDescriptions where Code = @OldValue
		
--
		if (@RetVal = 0)
			begin
-- Add by DP 4.0
				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq)
					select PKey, Entity, OldKey, NewKey, @OldName, @NewName, @Period, @LastPostSeq from keyConvertWork where Pkey = @PKey
				end
--
				delete from keyConvertWork where current of keysCursor
				commit Transaction
			end
		else
			begin
				set @ErrMsg = @ErrMsg + @ErrMsg2
				rollback transaction
			end
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue, 
		@PKey
	set @KeysFetch = @@Fetch_Status
	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	return(0)
End -- Procedure
GO
