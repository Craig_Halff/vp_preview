SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[buildPRSummaryWBSList]
	@visionuser		Nvarchar(32),
	@sessionID		varchar(32),
	@forceRefresh		Nvarchar(1),
	@prWhere		Nvarchar(max),
	@period			Nvarchar(100),
	@postSeq		Nvarchar(100)
AS
BEGIN
   DECLARE @LDQuery  		Nvarchar(max)
   DECLARE @PRFQuery		Nvarchar(max)
   DECLARE @LedgerAPQuery	Nvarchar(max)
   DECLARE @LedgerARQuery	Nvarchar(max)
   DECLARE @LedgerEXQuery	Nvarchar(max)
   DECLARE @LedgerMiscQuery	Nvarchar(max)

   DECLARE @wkQuery  		Nvarchar(max)
   DECLARE @holdString  	Nvarchar(max)

   DECLARE @prfPostDate		Nvarchar(500)
   DECLARE @allPostDate		Nvarchar(500)
   DECLARE @PRColumns  		Nvarchar(500)
   DECLARE @wbsFrom  		Nvarchar(1000)
   DECLARE @cfgPostFrom		Nvarchar(500)
   DECLARE @wbsWhere		Nvarchar(1000)
   DECLARE @wbsPRFrom  		Nvarchar(1000)
   DECLARE @groupString  	Nvarchar(100)

   DECLARE @additionalSelect  	Nvarchar(1000)
   DECLARE @additionalFrom  	Nvarchar(1000)
   DECLARE @additionalWhere	Nvarchar(1000)

   DECLARE @whereClause  	Nvarchar(max)
   DECLARE @topLevelStr  	Nvarchar(max)

   DECLARE @lastRun  		datetime

   SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
   SET NOCOUNT ON

	DELETE FROM PRSummaryWBSList WHERE sessionID = @sessionID  OR (dateDiff(hh, lastRun, getDate()) > 24)
	SELECT @additionalFrom = '' 
	SELECT @additionalWhere = ''
	SELECT @additionalSelect = ''

	SELECT @lastRun = getDate()

	SELECT @topLevelStr = ' SELECT DISTINCT WBS1, WBS2, WBS3, Period, ''' + @sessionID + ''' AS sessionID, ' +
			   '''' + convert(Nvarchar(25),getDate(),121) + ''' AS lastRun, ' +
				'Max(PostDate) AS lastPostDate, Max(ChargeType) AS ChargeType '

	SELECT @PRColumns = 'SELECT DISTINCT LEVEL3.WBS1, Max(LEVEL3.WBS2) AS WBS2, Max(LEVEL3.WBS3) AS WBS3, Max(LEVEL3.ChargeType) AS ChargeType, LD.Period, '
	SELECT @allPostDate = 'Max(CASE WHEN CFGPostControl.PostDate is not null THEN CFGPostControl.PostDate ELSE ' + 
			   	'LD.TransDate END) AS PostDate ' 
	SELECT @prfPostDate = 'null AS PostDate '

--	SELECT @prfPostDate = 'Max(CASE WHEN len(LD.Period) = 6 THEN ' +
--			   		'substring(convert(Nvarchar(25),LD.Period),1,4) + ''-'' + ' +
--					'CASE WHEN substring(convert(Nvarchar(25),PRF.Period),5,2) = ''00'' THEN ''01'' ELSE ' +
--						'substring(convert(Nvarchar(25),PRF.Period),5,2) END ' +
--					'+ ''-28'' ELSE ' +
--					'''' + convert(Nvarchar(25),getDate(),121) + '''' +
--					' END) AS PostDate '

	SELECT @wbsFrom = 'FROM PR AS LEVEL3 INNER JOIN PR AS LEVEL2 ON LEVEL3.WBS1 = LEVEL2.WBS1 ' + 
			  'AND LEVEL3.WBS2 = LEVEL2.WBS2 AND LEVEL2.WBS3 = N'' '' ' + 
			  'INNER JOIN PR ON LEVEL3.WBS1 = PR.WBS1 AND PR.WBS2 = N'' '' AND PR.WBS3 = N'' '' ' + 
			  'INNER JOIN LD ON LEVEL3.WBS1 = LD.WBS1 AND LEVEL3.WBS2 = LD.WBS2 AND LEVEL3.WBS3 = LD.WBS3 ' +
			  'LEFT JOIN CL AS CLBill ON CLBill.ClientID = PR.BillingClientID ' +
			  'LEFT JOIN CL ON CL.ClientID = PR.ClientID ' +
			  'LEFT JOIN Contacts AS BLCT ON PR.BillingContactID = BLCT.ContactID '

	SELECT @cfgPostFrom = 'LEFT JOIN CFGPostControl ON CFGPostControl.Period = LD.Period AND CFGPostControl.PostSeq = LD.PostSeq '
	SELECT @wbsWhere = ' Level3.ChargeType IN  (''R'',''H'',''P'') '

	If @forceRefresh IS NULL OR @forceRefresh = 'N' 
	Begin
		SELECT @additionalSelect = ', Max(PRSummarySub.ModDate) As ModDate '
		SELECT @additionalFrom = 'LEFT JOIN PRSummarySub ON LEVEL3.WBS1 = PRSummarySub.WBS1 AND LEVEL3.WBS2 = PRSummarySub.WBS2 ' +
				  'AND LEVEL3.WBS3 = PRSummarySub.WBS3 AND LD.Period = PRSummarySub.Period '
		SELECT @additionalWhere = ' WHERE ((ModDate is null) OR (PostDate is null) OR (PostDate > ModDate)) '
	End

	SELECT @groupString = ' GROUP BY LEVEL3.WBS1, LEVEL3.WBS2, LEVEL3.WBS3, LD.Period'
	
	-- Add prWhere filter.
	-- Exepecting in "PR.WBS1 = 'xxxx.xx' AND **" format as passed by Vision PR lookup.
	IF @prWhere IS NOT NULL AND @prWhere <> ''
		SELECT @whereClause = ' AND ' + @prWhere
	ELSE
		SELECT @whereClause = ''
	SELECT @whereClause = @wbsWhere + @whereClause

	-- Add period filter.
	-- Exepecting in "200412" format for single period or "200410, 200411, 200412" for multiple periods.
	IF @period IS NOT NULL AND @period <> ''
	 Begin
		SELECT @whereClause = @whereClause + ' AND CFGPostControl.Period IN (' + @period + ') '
	 End

	-- Add postSeq filter.
	-- Exepecting in "52" format for single period or "50, 51, 52" for multiple periods.
	IF @postSeq IS NOT NULL AND @postSeq <> ''
	 Begin
		SELECT @whereClause = @whereClause + ' AND CFGPostControl.PostSeq IN (' + @postSeq + ') '
	 End

	SELECT @LDQuery = @PRColumns + @allPostDate + @additionalSelect + @wbsFrom + @additionalFrom + @cfgPostFrom + 
			  'WHERE' + @whereClause + @groupString
	SELECT @holdString = ' UNION ALL ' + @LDQuery
	SELECT @LedgerAPQuery = REPLACE(@holdString, 'LD', 'LedgerAP')
	SELECT @LedgerARQuery = REPLACE(@holdString, 'LD', 'LedgerAR')
	SELECT @LedgerEXQuery = REPLACE(@holdString, 'LD', 'LedgerEX')
	SELECT @LedgerMiscQuery = REPLACE(@holdString, 'LD', 'LedgerMisc')

	SELECT @PRFQuery = ''
	SELECT @PRFQuery = ' UNION ALL ' + @PRColumns + @prfPostDate + @additionalSelect + @wbsFrom + @additionalFrom + 
			   'WHERE' + @whereClause + ' AND (RegOH > 0 OR RegOHProjectCurrency > 0) ' + @groupString
	SELECT @PRFQuery = REPLACE(@PRFQuery, 'LD', 'PRF')

	SELECT @topLevelStr = @topLevelStr + ' FROM ( ' + @LDQuery + @LedgerAPQuery + @LedgerARQuery + 
				@LedgerEXQuery + @LedgerMiscQuery + @PRFQuery + ' ) As MidLevel ' + @additionalWhere +
				'GROUP BY Period, MidLevel.WBS1, MidLevel.WBS2, MidLevel.WBS3'
	   
	SELECT @wkQuery = ' INSERT INTO PRSummaryWBSList ' 
				+ '(WBS1,WBS2,WBS3,Period,sessionID,lastRun,lastPostDate,ChargeType) '
				+ @topLevelStr
	---Print @wkQuery
	---Print '--------------------'
	Execute (@wkQuery)

        --Insert Null WBS1 Rows
	INSERT INTO PRSummaryWBSList(Period, sessionID, WBS1, WBS2, WBS3, ChargeType, lastRun, lastPostDate) 
	SELECT DISTINCT Min(Period) AS Period, Min(@sessionID) AS sessionID, WBS1, N' ', N' ', Min(ChargeType) As ChargeType, 
	Min(@lastRun) AS lastRun, Max(lastPostDate) As lastPostDate
	FROM PRSummaryWBSList 
	WHERE WBS2 > N'' AND sessionID = @sessionID
	 AND NOT EXISTS (Select 'x' 
	     FROM PRSummaryWBSList AS subList 
	     WHERE PRSummaryWBSList.WBS1 = subList.WBS1 AND subList.WBS2 = N' '
	     AND PRSummaryWBSList.Period = subList.Period AND sessionID = @sessionID)
	GROUP BY Period, WBS1

        --Insert Null WBS2 Rows
	INSERT INTO PRSummaryWBSList(Period, sessionID, WBS1, WBS2, WBS3, ChargeType, lastRun, lastPostDate) 
	SELECT DISTINCT Min(Period) AS Period, Min(@sessionID) AS sessionID, WBS1, WBS2, N' ', Min(ChargeType) As ChargeType, 
	Min(@lastRun) AS lastRun, Max(lastPostDate) As lastPostDate
	FROM PRSummaryWBSList 
	WHERE WBS3 > N'' AND sessionID = @sessionID
	 AND NOT EXISTS (Select 'x' 
	     FROM PRSummaryWBSList AS subList 
	     WHERE PRSummaryWBSList.WBS1 = subList.WBS1 AND PRSummaryWBSList.WBS2 = subList.WBS2
	     AND subList.WBS3 = N' '
	     AND PRSummaryWBSList.Period = subList.Period AND sessionID = @sessionID)
	GROUP BY Period, WBS1, WBS2

	-- If forceRefresh = 'Y' means refresh Project Summary for projects 
	-- 	based on the above selection(s).  Do not add any other logic to filter out projects.

	--If @forceRefresh IS NULL OR @forceRefresh = 'N' 
	 --Begin
	--	SELECT @topLevelStr = @topLevelStr + ', PRSummarySub '
	--	SELECT @whereClause = @whereClause + ' AND (PRSummarySub.Wbs1 = LEVEL3.Wbs1 ' +
	--			      + ' AND PRSummarySub.Wbs2 = LEVEL3.Wbs2 AND PRSummarySub.Wbs3 = LEVEL3.Wbs3) ' +
	--			      + ' AND PRSummarySub.ModDate < CFGPostControl.PostDate '


	--	SELECT @wkQuery = 'DELETE FROM PRSummaryWBSList ' +
	--			  'WHERE exists (SELECT ''x'' FROM PRSummarySub ' +
	--			  'WHERE PRSummaryWBSList.WBS1 = PRSummarySub.WBS1 ' +
	--			  'AND PRSummaryWBSList.WBS2 = PRSummarySub.WBS2 ' +
	--			  'AND PRSummaryWBSList.WBS3 = PRSummarySub.WBS3 ' +
	--			  'AND PRSummaryWBSList.Period = PRSummarySub.Period ' +
	--			  'AND PRSummaryWBSList.lastPostDate < PRSummarySub.ModDate) '
		--Print @wkQuery
	--	Execute (@wkQuery)
	-- End

	DELETE FROM PRSummarySub
	DELETE FROM PRSummaryMain
	
	INSERT INTO PRSummarySub (Period, WBS1, WBS2, WBS3, ChargeType, ModDate, ModUser)
	SELECT Period, WBS1, WBS2, WBS3, ChargeType, getDate() As ModDate, @visionuser As ModUser
	FROM PRSummaryWBSList
	WHERE  PRSummaryWBSList.sessionID = @sessionID
		AND not exists (SELECT 'x' FROM PRSummarySub
		WHERE PRSummarySub.WBS1 = PRSummaryWBSList.WBS1
		AND PRSummarySub.WBS2 = PRSummaryWBSList.WBS2
		AND PRSummarySub.WBS3 = PRSummaryWBSList.WBS3
		AND PRSummarySub.Period = PRSummaryWBSList.Period)

	INSERT INTO PRSummaryMain (Period, WBS1, WBS2, WBS3, ChargeType, ModDate, ModUser)
	SELECT Period, WBS1, WBS2, WBS3, ChargeType, getDate() As ModDate, @visionuser As ModUser
	FROM PRSummaryWBSList
	WHERE  PRSummaryWBSList.sessionID = @sessionID
		AND not exists (SELECT 'x' FROM PRSummaryMain
		WHERE PRSummaryMain.WBS1 = PRSummaryWBSList.WBS1
		AND PRSummaryMain.WBS2 = PRSummaryWBSList.WBS2
		AND PRSummaryMain.WBS3 = PRSummaryWBSList.WBS3
		AND PRSummaryMain.Period = PRSummaryWBSList.Period)
END

GO
