SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpCalcTPD]
  @strPlanID VARCHAR(32),
  @strCalcLab VARCHAR(1) = 'Y',
  @strCalcExp VARCHAR(1) = 'Y',
  @strCalcCon VARCHAR(1) = 'Y',
  @strCalcUnt VARCHAR(1) = 'Y',
  @strCalcCompF VARCHAR(1) = 'N',
  @strCalcConsF VARCHAR(1) = 'N',
  @strCalcReimA VARCHAR(1) = 'N'
AS

BEGIN -- Procedure rpCalcTPD

  SET NOCOUNT ON
  
  DECLARE @dcLabRevMult decimal(19,4)

  DECLARE @dcPlannedMultiplier decimal(19,4)
  DECLARE @dcPlannedRatio decimal(19,4)
  DECLARE @dcPlannedLaborHrs decimal(19,4)
  DECLARE @dcPlannedLabCost decimal(19,4)
  DECLARE @dcPlannedLabBill decimal(19,4)
  DECLARE @dcPlannedExpCost decimal(19,4)
  DECLARE @dcPlannedExpBill decimal(19,4)
  DECLARE @dcPlannedConCost decimal(19,4)
  DECLARE @dcPlannedConBill decimal(19,4)
  DECLARE @dcPlannedUntQty decimal(19,4)
  DECLARE @dcPlannedUntCost decimal(19,4)
  DECLARE @dcPlannedUntBill decimal(19,4)
  DECLARE @dcLabRevenue decimal(19,4)
  DECLARE @dcExpRevenue decimal(19,4)
  DECLARE @dcConRevenue decimal(19,4)
  DECLARE @dcUntRevenue decimal(19,4)
  DECLARE @dcPlannedDirExpCost decimal(19,4)
  DECLARE @dcPlannedDirExpBill decimal(19,4)
  DECLARE @dcPlannedDirConCost decimal(19,4)
  DECLARE @dcPlannedDirConBill decimal(19,4)
  DECLARE @dcPlannedDirUntCost decimal(19,4)
  DECLARE @dcPlannedDirUntBill decimal(19,4)
  DECLARE @dcLabBillMult decimal(19,4)
  DECLARE @dcUntBillMult decimal(19,4)
  
  DECLARE @dcCompCost decimal(19,4)
  DECLARE @dcCompBill decimal(19,4)
  DECLARE @dcConsCost decimal(19,4)
  DECLARE @dcConsBill decimal(19,4)
  DECLARE @dcReimCost decimal(19,4)
  DECLARE @dcReimBill decimal(19,4)
  
  DECLARE @strLabRevBasis char
  DECLARE @strCalcExpBill char
  DECLARE @strCalcConBill char
  DECLARE @strReimbMethod char
  
  DECLARE @intHrDecimals int
  DECLARE @intQtyDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intLabRevDecimals int
  DECLARE @intECURevDecimals int

  DECLARE @intPlannedLabCount int
  DECLARE @intPlannedExpCount int
  DECLARE @intPlannedConCount int
  DECLARE @intPlannedUntCount int
  
  DECLARE @intLabMultType smallint
     
  SET @dcPlannedMultiplier = 0
  SET @dcPlannedRatio = 0
 
  -- Get decimal settings.
  
  SELECT @intHrDecimals = HrDecimals,
         @intQtyDecimals = QtyDecimals,
         @intAmtCostDecimals = AmtCostDecimals,
         @intAmtBillDecimals = AmtBillDecimals,
         @intLabRevDecimals = LabRevDecimals, 
         @intECURevDecimals = ECURevDecimals
    FROM dbo.RP$tabDecimals(@strPlanID)
  
  -- Check to see if there is any time-phased data
  
  IF (@strCalcLab = 'Y') SET @intPlannedLabCount = CASE WHEN EXISTS (SELECT 'X' FROM RPPlannedLabor WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
  IF (@strCalcExp = 'Y') SET @intPlannedExpCount = CASE WHEN EXISTS (SELECT 'X' FROM RPPlannedExpenses WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
  IF (@strCalcCon = 'Y') SET @intPlannedConCount = CASE WHEN EXISTS (SELECT 'X' FROM RPPlannedConsultant WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
  IF (@strCalcUnt = 'Y') SET @intPlannedUntCount = CASE WHEN EXISTS (SELECT 'X' FROM RPPlannedUnit WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END 

  -- Get parameters from Plan to be used throughout this procedure.
  -- Also, save off Planned numbers to be used in calculation later
  -- in case a particular data type (e.g. Labor/Expense/Consultant/Unit/CCR)
  -- does not have to be calculated.
  
  SELECT @intLabMultType = LabMultType,
         @dcLabRevMult = Multiplier,
         @strLabRevBasis = CASE WHEN LabMultType < 2 THEN 'C' ELSE 'B' END,
         @strCalcExpBill = CalcExpBillAmtFlg, @strCalcConBill = CalcConBillAmtFlg,
         @strReimbMethod = ReimbMethod,
         @dcLabBillMult = LabBillMultiplier,
         @dcUntBillMult = UntBillMultiplier,
         @dcPlannedLaborHrs = PlannedLaborHrs, 
         @dcPlannedLabCost = PlannedLabCost,
         @dcPlannedLabBill = PlannedLabBill,
         @dcLabRevenue = LabRevenue,
         @dcPlannedExpCost = PlannedExpCost,
         @dcPlannedExpBill = PlannedExpBill,
         @dcPlannedDirExpCost = PlannedDirExpCost,
         @dcPlannedDirExpBill = PlannedDirExpBill,
         @dcExpRevenue = ExpRevenue,
         @dcPlannedConCost = PlannedConCost,
         @dcPlannedConBill = PlannedConBill,
         @dcPlannedDirConCost = PlannedDirConCost,
         @dcPlannedDirConBill = PlannedDirConBill,
         @dcConRevenue = ConRevenue,
         @dcPlannedUntQty = PlannedUntQty,
         @dcPlannedUntCost = PlannedUntCost,
         @dcPlannedUntBill = PlannedUntBill,
         @dcPlannedDirUntCost = PlannedDirUntCost,
         @dcPlannedDirUntBill = PlannedDirUntBill,
         @dcUntRevenue = UntRevenue,
         @dcCompCost = CompensationFee,
         @dcCompBill = CompensationFeeBill,
         @dcConsCost = ConsultantFee,
         @dcConsBill = ConsultantFeeBill,
         @dcReimCost = ReimbAllowance,
         @dcReimBill = ReimbAllowanceBill
    FROM RPPlan WHERE PlanID = @strPlanID
    
  -- >>> rpDeleteSummaryTPD should have been called before getting to this point.
           
  -- >>> At this point, the remaining time-phased data belong to either childless-Task or 
  -- >>> Assignment/Expense/Consultant/Unit.
  
  -- Update RPPlannedLabor.

  -- Need to defer the updating of Planned Labor Revenue just in case the Labor Revenue
  -- Method uses Planned Multiplier/Ratio. In this case, Planned Multiplier/Ratio can only
  -- be calculated after all of the Planned Labor/Expense/Cost numbers have been updated.

  IF (@strCalcLab = 'Y')
    BEGIN
    
      IF (@intPlannedLabCount > 0)
        BEGIN
        
          UPDATE RPPlannedLabor
            SET PeriodCost = ROUND((ROUND(PeriodHrs, @intHrDecimals) * CostRate), @intAmtCostDecimals),
                PeriodBill = ROUND((ROUND(PeriodHrs, @intHrDecimals) * BillingRate * @dcLabBillMult), @intAmtBillDecimals)     
            FROM RPPlannedLabor AS TPD
            WHERE TPD.PlanID = @strPlanID
              AND (PeriodCost <> ROUND((ROUND(PeriodHrs, @intHrDecimals) * CostRate), @intAmtCostDecimals) OR
                  PeriodBill <> ROUND((ROUND(PeriodHrs, @intHrDecimals) * BillingRate * @dcLabBillMult), @intAmtBillDecimals))
                  
          SELECT @dcPlannedLaborHrs = ROUND(ISNULL(SUM(PeriodHrs), 0), @intHrDecimals),
                @dcPlannedLabCost = ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals),
                @dcPlannedLabBill = ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals)
            FROM RPPlannedLabor WHERE PlanID = @strPlanID
            
        END
        
      ELSE
        BEGIN 
          SET @dcPlannedLaborHrs = 0
          SET @dcPlannedLabCost = 0
          SET @dcPlannedLabBill = 0
        END
        
    END -- IF (@strCalcLab = 'Y')

  -- Update RPPlannedExpenses
  -- Note: for V3, Direct Bill Expense is not always zero.
  --       Each Expense row already had the appropriate Multiplier calculated upon returned from Lookup.
  -- All TPD Expenses under Task rows (that do not have any children) are considered "Direct" Expenses.
    
  IF (@strCalcExp = 'Y')
    BEGIN

      IF (@intPlannedExpCount > 0)
        BEGIN
            
          UPDATE RPPlannedExpenses
            SET PeriodBill = 
                  ROUND(CASE WHEN @strCalcExpBill = 'Y' 
                              THEN (PeriodCost * ISNULL(E.ExpBillRate, T.ExpBillRate))
                              ELSE PeriodBill END, @intAmtBillDecimals),
                PeriodRev = 
                  CASE WHEN E.DirectAcctFlg = 'N'
                       THEN (CASE WHEN @strReimbMethod = 'C' THEN PeriodCost
                                    ELSE (CASE WHEN @strCalcExpBill = 'Y' 
                                                 THEN ROUND((PeriodCost * ISNULL(E.ExpBillRate, T.ExpBillRate)), @intAmtBillDecimals)
                                                 ELSE PeriodBill END) END)
                       ELSE 0.0 END
            FROM RPPlannedExpenses AS TPD 
              LEFT JOIN RPExpense AS E ON TPD.ExpenseID = E.ExpenseID
              LEFT JOIN RPTask AS T ON TPD.TaskID = T.TaskID
            WHERE TPD.PlanID = @strPlanID
              AND (PeriodBill <>
                    ROUND(CASE WHEN @strCalcExpBill = 'Y' 
                                 THEN (PeriodCost * ISNULL(E.ExpBillRate, T.ExpBillRate))
                                 ELSE PeriodBill END, @intAmtBillDecimals) OR
                  PeriodRev <> 
                    CASE WHEN E.DirectAcctFlg = 'N'
                           THEN (CASE WHEN @strReimbMethod = 'C' THEN PeriodCost
                                        ELSE (CASE WHEN @strCalcExpBill = 'Y' 
                                                     THEN ROUND((PeriodCost * ISNULL(E.ExpBillRate, T.ExpBillRate)), @intAmtBillDecimals)
                                                     ELSE PeriodBill END) END)
                           ELSE 0.0 END)
                          
          SELECT @dcPlannedExpCost = ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals),
                 @dcPlannedExpBill = ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals),
                 @dcExpRevenue = ROUND(ISNULL(SUM(PeriodRev), 0), CASE WHEN @strReimbMethod = 'C' 
                                                                         THEN @intAmtCostDecimals
                                                                         ELSE @intAmtBillDecimals END),
                 @dcPlannedDirExpCost = 
                   ROUND(ISNULL(SUM(CASE WHEN (E.DirectAcctFlg = 'Y' OR E.DirectAcctFlg IS NULL)
                                         THEN PeriodCost ELSE 0.0 END), 0), @intAmtCostDecimals),
                 @dcPlannedDirExpBill = 
                   ROUND(ISNULL(SUM(CASE WHEN (E.DirectAcctFlg = 'Y' OR E.DirectAcctFlg IS NULL)
                                         THEN PeriodBill ELSE 0.0 END), 0), @intAmtBillDecimals)
            FROM RPPlannedExpenses AS TPD LEFT JOIN RPExpense AS E 
              ON TPD.PlanID = E.PlanID AND TPD.TaskID = E.TaskID AND TPD.ExpenseID = E.ExpenseID
            WHERE TPD.PlanID = @strPlanID 

        END
        
      ELSE
        BEGIN
          SET @dcPlannedExpCost = 0
          SET @dcPlannedExpBill = 0
          SET @dcExpRevenue = 0
          SET @dcPlannedDirExpCost = 0
          SET @dcPlannedDirExpBill = 0
        END
        
    END -- If-Then
    
  -- Update RPPlannedConsultant
  -- Note: for V3, Direct Bill Consultant is not always zero.
  --       Each Expense row already had the appropriate Multiplier calculated upon returned from Lookup.
  -- All TPD Consultants under Task rows (that do not have any children) are considered "Direct" Consultants.
  
  IF (@strCalcCon = 'Y')
    BEGIN
    
      IF (@intPlannedConCount > 0)
        BEGIN
            
          UPDATE RPPlannedConsultant
            SET PeriodBill = 
                  ROUND(CASE WHEN @strCalcConBill = 'Y' 
                               THEN (PeriodCost * ISNULL(C.ConBillRate, T.ConBillRate))
                               ELSE PeriodBill END, @intAmtBillDecimals),
                PeriodRev = 
                  CASE WHEN C.DirectAcctFlg = 'N'
                         THEN (CASE WHEN @strReimbMethod = 'C' THEN PeriodCost
                                      ELSE (CASE WHEN @strCalcConBill = 'Y' 
                                                   THEN ROUND((PeriodCost * ISNULL(C.ConBillRate, T.ConBillRate)), @intAmtBillDecimals)
                                                   ELSE PeriodBill END) END)
                         ELSE 0.0 END
            FROM RPPlannedConsultant AS TPD 
              LEFT JOIN RPConsultant AS C ON TPD.ConsultantID = C.ConsultantID
              LEFT JOIN RPTask AS T ON TPD.TaskID = T.TaskID
            WHERE TPD.PlanID = @strPlanID
              AND (PeriodBill <> 
                    ROUND(CASE WHEN @strCalcConBill = 'Y' 
                                  THEN (PeriodCost * ISNULL(C.ConBillRate, T.ConBillRate))
                                  ELSE PeriodBill END, @intAmtBillDecimals) OR
                  PeriodRev <> 
                    CASE WHEN C.DirectAcctFlg = 'N'
                           THEN (CASE WHEN @strReimbMethod = 'C' THEN PeriodCost
                                        ELSE (CASE WHEN @strCalcConBill = 'Y' 
                                                     THEN ROUND((PeriodCost * ISNULL(C.ConBillRate, T.ConBillRate)), @intAmtBillDecimals)
                                                     ELSE PeriodBill END) END)
                           ELSE 0.0 END)
                          
          SELECT @dcPlannedConCost = ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals),
                 @dcPlannedConBill = ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals),
                 @dcConRevenue = ROUND(ISNULL(SUM(PeriodRev), 0), CASE WHEN @strReimbMethod = 'C' 
                                                                         THEN @intAmtCostDecimals
                                                                         ELSE @intAmtBillDecimals END),
                 @dcPlannedDirConCost = 
                   ROUND(ISNULL(SUM(CASE WHEN (C.DirectAcctFlg = 'Y' OR C.DirectAcctFlg IS NULL)
                                         THEN PeriodCost ELSE 0.0 END), 0), @intAmtCostDecimals),
                 @dcPlannedDirConBill = 
                   ROUND(ISNULL(SUM(CASE WHEN (C.DirectAcctFlg = 'Y' OR C.DirectAcctFlg IS NULL)
                                         THEN PeriodBill ELSE 0.0 END), 0), @intAmtBillDecimals)
            FROM RPPlannedConsultant AS TPD LEFT JOIN RPConsultant AS C 
              ON TPD.PlanID = C.PlanID AND TPD.TaskID = C.TaskID AND TPD.ConsultantID = C.ConsultantID
            WHERE TPD.PlanID = @strPlanID 

        END
        
      ELSE
        BEGIN
          SET @dcPlannedConCost = 0
          SET @dcPlannedConBill = 0
          SET @dcConRevenue = 0
          SET @dcPlannedDirConCost = 0
          SET @dcPlannedDirConBill = 0
        END
        
    END -- If-Then

  -- Update RPPlannedUnit.
  -- Unit Cost and Billing amounts are always calculated using UntCostRate and UntBillRate respectively.
 
  IF (@strCalcUnt = 'Y')
    BEGIN
    
      IF (@intPlannedUntCount > 0)
        BEGIN
            
          UPDATE RPPlannedUnit
            SET PeriodCost = ROUND(PeriodQty * (CASE WHEN U.UnitID IS NULL THEN T.UntCostRate ELSE U.UntCostRate END), @intAmtCostDecimals),
                PeriodBill = ROUND(PeriodQty * (CASE WHEN U.UnitID IS NULL THEN T.UntBillRate ELSE U.UntBillRate END) * @dcUntBillMult, @intAmtBillDecimals),    
                PeriodRev = 
                  CASE WHEN U.DirectAcctFlg = 'N'
                         THEN (CASE WHEN @strReimbMethod = 'C' 
                                      THEN ROUND(PeriodQty * (CASE WHEN U.UnitID IS NULL THEN T.UntCostRate ELSE U.UntCostRate END), @intAmtCostDecimals)
                                      ELSE ROUND(PeriodQty * (CASE WHEN U.UnitID IS NULL THEN T.UntBillRate ELSE U.UntBillRate END) * @dcUntBillMult, @intAmtBillDecimals)
                                    END)
                         ELSE 0.0 END
            FROM RPPlannedUnit AS TPD LEFT JOIN RPUnit AS U ON TPD.UnitID = U.UnitID
              LEFT JOIN RPTask AS T ON TPD.TaskID = T.TaskID
            WHERE TPD.PlanID = @strPlanID
              AND (PeriodCost <> ROUND(PeriodQty * (CASE WHEN U.UnitID IS NULL THEN T.UntCostRate ELSE U.UntCostRate END), @intAmtCostDecimals) OR
                  PeriodBill <> ROUND(PeriodQty * (CASE WHEN U.UnitID IS NULL THEN T.UntBillRate ELSE U.UntBillRate END) * @dcUntBillMult, @intAmtBillDecimals) OR
                  PeriodRev <> 
                    CASE WHEN U.DirectAcctFlg = 'N'
                           THEN (CASE WHEN @strReimbMethod = 'C' 
                                        THEN ROUND(PeriodQty * (CASE WHEN U.UnitID IS NULL THEN T.UntCostRate ELSE U.UntCostRate END), @intAmtCostDecimals)
                                        ELSE ROUND(PeriodQty * (CASE WHEN U.UnitID IS NULL THEN T.UntBillRate ELSE U.UntBillRate END) * @dcUntBillMult, @intAmtBillDecimals)
                                      END)
                           ELSE 0.0 END)
                          
          SELECT @dcPlannedUntQty = ROUND(ISNULL(SUM(PeriodQty), 0), @intQtyDecimals),
                 @dcPlannedUntCost = ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals),
                 @dcPlannedUntBill = ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals),
                 @dcUntRevenue = ROUND(ISNULL(SUM(PeriodRev), 0), CASE WHEN @strReimbMethod = 'C' 
                                                                         THEN @intAmtCostDecimals
                                                                         ELSE @intAmtBillDecimals END),
                 @dcPlannedDirUntCost = 
                   ROUND(ISNULL(SUM(CASE WHEN (U.DirectAcctFlg = 'Y' OR U.DirectAcctFlg IS NULL)
                                         THEN PeriodCost ELSE 0.0 END), 0), @intAmtCostDecimals),
                 @dcPlannedDirUntBill = 
                   ROUND(ISNULL(SUM(CASE WHEN (U.DirectAcctFlg = 'Y' OR U.DirectAcctFlg IS NULL)
                                         THEN PeriodBill ELSE 0.0 END), 0), @intAmtBillDecimals)
            FROM RPPlannedUnit AS TPD LEFT JOIN RPUnit AS U 
              ON TPD.PlanID = U.PlanID AND TPD.TaskID = U.TaskID AND TPD.UnitID = U.UnitID
            WHERE TPD.PlanID = @strPlanID
            
        END
        
      ELSE 
        BEGIN
          SET @dcPlannedUntQty = 0
          SET @dcPlannedUntCost = 0
          SET @dcPlannedUntBill = 0
          SET @dcUntRevenue = 0
          SET @dcPlannedDirUntCost = 0
          SET @dcPlannedDirUntBill = 0
        END
        
    END -- If-Then

/* --> Comment out this block of code because we don't want to synchronize
       the Fee fields in RPPlan with the sum of Fees by Period.

  -- Calculate Compensation/Consultant/Reimbursable summary numbers.
  -- Use INNER JOIN to RPTask to filter out orphan records.
  
  IF (@strCalcCompF = 'Y')
    SELECT @dcCompCost = ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals),
           @dcCompBill = ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals)
      FROM RPCompensationFee AS TPD 
        INNER JOIN RPTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID
      WHERE TPD.PlanID = @strPlanID 
        
  IF (@strCalcConsF = 'Y')
    SELECT @dcConsCost = ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals),
           @dcConsBill = ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals)
      FROM RPConsultantFee AS TPD 
        INNER JOIN RPTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID
      WHERE TPD.PlanID = @strPlanID 
      
  IF (@strCalcReimA = 'Y')
    SELECT @dcReimCost = ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals),
           @dcReimBill = ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals)
      FROM RPReimbAllowance AS TPD 
        INNER JOIN RPTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID
      WHERE TPD.PlanID = @strPlanID
      
*/
    
  -- >>> Now we can calculate Planned Multiplier and Planned Ratio 
  -- >>> before calculating PeriodRev for the Labor time-phased data.
  
  IF (@dcPlannedLabCost = 0)
    SET @dcPlannedMultiplier = 0.00
  ELSE
    SET @dcPlannedMultiplier = ROUND((((@dcCompCost + @dcConsCost + @dcReimCost) -
						(@dcPlannedExpCost + @dcPlannedConCost + @dcPlannedUntCost)) / @dcPlannedLabCost), 2)

  IF (@dcPlannedLabBill = 0)
    SET @dcPlannedRatio = 0.00
  ELSE
    SET @dcPlannedRatio = ROUND((((@dcCompBill + @dcConsBill + @dcReimBill) -
						(@dcPlannedExpBill + @dcPlannedConBill + @dcPlannedUntBill)) / @dcPlannedLabBill), 2)
						
  SET @dcLabRevMult = CASE WHEN @intLabMultType = 1 THEN @dcPlannedMultiplier
                           WHEN @intLabMultType = 3 THEN @dcPlannedRatio
                           ELSE @dcLabRevMult END				

  -- Update Labor Revenue time-phased data.
  -- Since @dcLabRevMult could be changed because Planned Multiplier or Planned Ratio was changed,
  -- we will need to recalculate Labor Revenue regardless of whether @strCalcLab is 'Y' or not.

  IF (@strCalcLab = 'Y')
    BEGIN
    
      IF (@intPlannedLabCount > 0)
        BEGIN
        
          UPDATE RPPlannedLabor
            SET PeriodRev = CASE WHEN @strLabRevBasis = 'C' THEN ROUND((PeriodCost * @dcLabRevMult), @intAmtCostDecimals)
                                 ELSE ROUND((PeriodBill * @dcLabRevMult), @intAmtBillDecimals) END
            FROM RPPlannedLabor 
            WHERE PlanID = @strPlanID
              AND PeriodRev <> CASE WHEN @strLabRevBasis = 'C' THEN ROUND((PeriodCost * @dcLabRevMult), @intAmtCostDecimals)
                                    ELSE ROUND((PeriodBill * @dcLabRevMult), @intAmtBillDecimals) END

          SELECT @dcLabRevenue = ROUND(SUM(PeriodRev), CASE WHEN @strLabRevBasis = 'C' 
                                                              THEN @intAmtCostDecimals
                                                              ELSE @intAmtBillDecimals END)
            FROM RPPlannedLabor WHERE PlanID = @strPlanID
      
        END
      ELSE SET @dcLabRevenue = 0
        
    END -- IF (@strCalcLab = 'Y')

  -- Update Summary data of the Plan
  -- I do this update here because I already have the data so might as well use them.
  -- Also, this will allow me to cross check the calculations later.
  
  UPDATE RPPlan
    SET Multiplier = ISNULL(@dcLabRevMult, 0),
        ProjectedMultiplier = ISNULL(@dcPlannedMultiplier, 0), 
        ProjectedRatio = ISNULL(@dcPlannedRatio, 0), 
        PlannedLaborHrs = ISNULL(@dcPlannedLaborHrs, 0), 
        PlannedLabCost = ISNULL(@dcPlannedLabCost, 0), 
        PlannedLabBill = ISNULL(@dcPlannedLabBill, 0), 
        PlannedExpCost = ISNULL(@dcPlannedExpCost, 0), 
        PlannedExpBill = ISNULL(@dcPlannedExpBill, 0), 
        PlannedConCost = ISNULL(@dcPlannedConCost, 0), 
        PlannedConBill = ISNULL(@dcPlannedConBill, 0), 
        PlannedUntQty = ISNULL(@dcPlannedUntQty, 0), 
        PlannedUntCost = ISNULL(@dcPlannedUntCost, 0), 
        PlannedUntBill = ISNULL(@dcPlannedUntBill, 0), 
        LabRevenue = ISNULL(@dcLabRevenue, 0), 
        ExpRevenue = ISNULL(@dcExpRevenue, 0), 
        ConRevenue = ISNULL(@dcConRevenue, 0), 
        UntRevenue = ISNULL(@dcUntRevenue, 0),
        PlannedDirExpCost = ISNULL(@dcPlannedDirExpCost, 0), 
        PlannedDirExpBill = ISNULL(@dcPlannedDirExpBill, 0), 
        PlannedDirConCost = ISNULL(@dcPlannedDirConCost, 0), 
        PlannedDirConBill = ISNULL(@dcPlannedDirConBill, 0),
        PlannedDirUntCost = ISNULL(@dcPlannedDirUntCost, 0), 
        PlannedDirUntBill = ISNULL(@dcPlannedDirUntBill, 0)
    FROM RPPlan
    WHERE PlanID = @strPlanID
    
  -- >>> I need to calculate the Weighted Average & Direct amounts here
  -- >>> for the bottom-most rows so that they can be used later in summing up
  -- >>> for the summary rows.
  
  -- Update bottom-most Tasks.
  
  UPDATE RPTask
    SET PlannedLaborHrs = TPD.PlannedLaborHrs,
        PlannedLabCost = TPD.PlannedLabCost,
        PlannedLabBill = TPD.PlannedLabBill,
        LabRevenue = TPD.LabRevenue,
        PlannedExpCost = TPD.PlannedExpCost,
        PlannedExpBill = TPD.PlannedExpBill,
        ExpRevenue = TPD.ExpRevenue,
        PlannedConCost = TPD.PlannedConCost,
        PlannedConBill = TPD.PlannedConBill,
        ConRevenue = TPD.ConRevenue,
        PlannedUntQty = TPD.PlannedUntQty,
        PlannedUntCost = TPD.PlannedUntCost,
        PlannedUntBill = TPD.PlannedUntBill,
        UntRevenue = TPD.UntRevenue,
        CompensationFee = TPD.CompCost,
        CompensationFeeBill = TPD.CompBill,
        ConsultantFee = TPD.ConsCost,
        ConsultantFeeBill = TPD.ConsBill,
        ReimbAllowance = TPD.ReimCost,
        ReimbAllowanceBill = TPD.ReimBill,
        WeightedLabCost = TPD.WeightedLabCost,
        WeightedLabBill = TPD.WeightedLabBill,
        WeightedExpCost = TPD.WeightedExpCost,
        WeightedExpBill = TPD.WeightedExpBill,
        WeightedConCost = TPD.WeightedConCost,
        WeightedConBill = TPD.WeightedConBill,
        WeightedUntCost = TPD.WeightedUntCost,
        WeightedUntBill = TPD.WeightedUntBill,
        PlannedDirExpCost = TPD.PlannedDirExpCost,
        PlannedDirExpBill = TPD.PlannedDirExpBill,
        PlannedDirConCost = TPD.PlannedDirConCost,
        PlannedDirConBill = TPD.PlannedDirConBill,
        PlannedDirUntCost = TPD.PlannedDirUntCost,
        PlannedDirUntBill = TPD.PlannedDirUntBill,
        PctCompleteLabCost = CASE WHEN XP.PctCompleteFormula = 0
                                  THEN CASE WHEN ACount > 0 
                                            THEN CASE WHEN TPD.PlannedLabCost <> 0 
                                                      THEN ROUND((100.0 * TPD.WeightedLabCost / TPD.PlannedLabCost), 2)
                                                      ELSE 0 END
                                            ELSE T.PctCompleteLabCost END
                                  ELSE T.PctCompleteLabCost END,
        PctCompleteLabBill = CASE WHEN XP.PctCompleteFormula = 0
                                  THEN CASE WHEN ACount > 0 
                                            THEN CASE WHEN TPD.PlannedLabBill <> 0 
                                                      THEN ROUND((100.0 * TPD.WeightedLabBill / TPD.PlannedLabBill), 2)
                                                      ELSE 0 END
                                            ELSE T.PctCompleteLabBill END
                                  ELSE T.PctCompleteLabBill END,
        PctCompleteExpCost = CASE WHEN XP.PctCompleteFormula = 0
                                  THEN CASE WHEN ECount > 0 
                                            THEN CASE WHEN TPD.PlannedExpCost <> 0 
                                                      THEN ROUND((100.0 * TPD.WeightedExpCost / TPD.PlannedExpCost), 2)
                                                      ELSE 0 END
                                            ELSE T.PctCompleteExpCost END
                                  ELSE T.PctCompleteExpCost END,
        PctCompleteExpBill = CASE WHEN XP.PctCompleteFormula = 0
                                  THEN CASE WHEN ECount > 0 
                                            THEN CASE WHEN TPD.PlannedExpBill <> 0 
                                                      THEN ROUND((100.0 * TPD.WeightedExpBill / TPD.PlannedExpBill), 2)
                                                      ELSE 0 END
                                            ELSE T.PctCompleteExpBill END
                                  ELSE T.PctCompleteExpBill END,
        PctCompleteConCost = CASE WHEN XP.PctCompleteFormula = 0
                                  THEN CASE WHEN CCount > 0 
                                            THEN CASE WHEN TPD.PlannedConCost <> 0 
                                                      THEN ROUND((100.0 * TPD.WeightedConCost / TPD.PlannedConCost), 2)
                                                      ELSE 0 END
                                            ELSE T.PctCompleteConCost END
                                  ELSE T.PctCompleteConCost END,
        PctCompleteConBill = CASE WHEN XP.PctCompleteFormula = 0
                                  THEN CASE WHEN CCount > 0 
                                            THEN CASE WHEN TPD.PlannedConBill <> 0 
                                                      THEN ROUND((100.0 * TPD.WeightedConBill / TPD.PlannedConBill), 2)
                                                      ELSE 0 END
                                            ELSE T.PctCompleteConBill END
                                  ELSE T.PctCompleteConBill END,
        PctCompleteUntCost = CASE WHEN XP.PctCompleteFormula = 0
                                  THEN CASE WHEN UCount > 0 
                                            THEN CASE WHEN TPD.PlannedUntCost <> 0 
                                                      THEN ROUND((100.0 * TPD.WeightedUntCost / TPD.PlannedUntCost), 2)
                                                      ELSE 0 END
                                            ELSE T.PctCompleteUntCost END
                                  ELSE T.PctCompleteUntCost END,
        PctCompleteUntBill = CASE WHEN XP.PctCompleteFormula = 0
                                  THEN CASE WHEN UCount > 0 
                                            THEN CASE WHEN TPD.PlannedUntBill <> 0 
                                                      THEN ROUND((100.0 * TPD.WeightedUntBill / TPD.PlannedUntBill), 2)
                                                      ELSE 0 END
                                            ELSE T.PctCompleteUntBill END
                                  ELSE T.PctCompleteUntBill END,

		/* Compensation breakout */
		CompensationFeeDirLab = TPD.DirLabCost,
		CompensationFeeDirLabBill = TPD.DirLabBill, 
		CompensationFeeDirExp = TPD.DirExpCost, 
		CompensationFeeDirExpBill = TPD.DirExpBill,
		ReimbAllowanceExp = TPD.ReimExpCost, 
		ReimbAllowanceExpBill = TPD.ReimExpBill, 
		ReimbAllowanceCon = TPD.ReimConsCost, 
		ReimbAllowanceConBill = TPD.ReimConsBill

    FROM RPTask AS T INNER JOIN
      (SELECT XT.PlanID AS PlanID, XT.TaskID AS TaskID,
              SUM(ISNULL(ACount, 0)) AS ACount,
              SUM(ISNULL(ECount, 0)) AS ECount,
              SUM(ISNULL(CCount, 0)) AS CCount,
              SUM(ISNULL(UCount, 0)) AS UCount,
              ROUND(SUM(ISNULL(LabPeriodHrs, 0)), @intHrDecimals) AS PlannedLaborHrs,
              ROUND(SUM(ISNULL(LabPeriodCost, 0)), @intAmtCostDecimals) AS PlannedLabCost,
              ROUND(SUM(ISNULL(LabPeriodBill, 0)), @intAmtBillDecimals)AS PlannedLabBill,
              ROUND(SUM(ISNULL(LabPeriodRev, 0)), CASE WHEN P.LabMultType < 2 
                                                       THEN @intAmtCostDecimals 
                                                       ELSE @intAmtBillDecimals END) AS LabRevenue,
              ROUND(SUM(ISNULL(ExpPeriodCost, 0)), @intAmtCostDecimals) AS PlannedExpCost,
              ROUND(SUM(ISNULL(ExpPeriodBill, 0)), @intAmtBillDecimals) AS PlannedExpBill,
              ROUND(SUM(ISNULL(ExpPeriodRev, 0)), CASE WHEN P.ReimbMethod = 'C' 
                                                       THEN @intAmtCostDecimals
                                                       ELSE @intAmtBillDecimals END) AS ExpRevenue,
              ROUND(SUM(ISNULL(ConPeriodCost, 0)), @intAmtCostDecimals) AS PlannedConCost,
              ROUND(SUM(ISNULL(ConPeriodBill, 0)), @intAmtBillDecimals) AS PlannedConBill,
              ROUND(SUM(ISNULL(ConPeriodRev, 0)), CASE WHEN P.ReimbMethod = 'C' 
                                                       THEN @intAmtCostDecimals
                                                       ELSE @intAmtBillDecimals END) AS ConRevenue,
              ROUND(SUM(ISNULL(UntPeriodQty, 0)), @intQtyDecimals) AS PlannedUntQty,
              ROUND(SUM(ISNULL(UntPeriodCost, 0)), @intAmtCostDecimals) AS PlannedUntCost,
              ROUND(SUM(ISNULL(UntPeriodBill, 0)), @intAmtBillDecimals) AS PlannedUntBill,
              ROUND(SUM(ISNULL(UntPeriodRev, 0)), CASE WHEN P.ReimbMethod = 'C' 
                                                       THEN @intAmtCostDecimals
                                                       ELSE @intAmtBillDecimals END) AS UntRevenue,
              CASE WHEN @strCalcCompF = 'Y' THEN ROUND(SUM(ISNULL(CompCost, 0)), @intAmtCostDecimals)
                   ELSE XT.CompensationFee END AS CompCost,
              CASE WHEN @strCalcCompF = 'Y' THEN ROUND(SUM(ISNULL(CompBill, 0)), @intAmtBillDecimals)
                   ELSE XT.CompensationFeeBill END AS CompBill,
              CASE WHEN @strCalcConsF = 'Y' THEN ROUND(SUM(ISNULL(ConsCost, 0)), @intAmtCostDecimals)
                   ELSE XT.ConsultantFee END AS ConsCost,
              CASE WHEN @strCalcConsF = 'Y' THEN ROUND(SUM(ISNULL(ConsBill, 0)), @intAmtBillDecimals)
                   ELSE XT.ConsultantFeeBill END AS ConsBill,
              CASE WHEN @strCalcReimA = 'Y' THEN ROUND(SUM(ISNULL(ReimCost, 0)), @intAmtCostDecimals)
                   ELSE XT.ReimbAllowance END AS ReimCost,
              CASE WHEN @strCalcReimA = 'Y' THEN ROUND(SUM(ISNULL(ReimBill, 0)), @intAmtBillDecimals)
                   ELSE XT.ReimbAllowanceBill END AS ReimBill,
              SUM(ISNULL(LabWtCost, 0)) AS WeightedLabCost,
              SUM(ISNULL(LabWtBill, 0)) AS WeightedLabBill,
              SUM(ISNULL(ExpWtCost, 0)) AS WeightedExpCost,
              SUM(ISNULL(ExpWtBill, 0)) AS WeightedExpBill,
              SUM(ISNULL(ConWtCost, 0)) AS WeightedConCost,
              SUM(ISNULL(ConWtBill, 0)) AS WeightedConBill,
              SUM(ISNULL(UntWtCost, 0)) AS WeightedUntCost,
              SUM(ISNULL(UntWtBill, 0)) AS WeightedUntBill,
              ROUND(SUM(ISNULL(ExpDirCost, 0)), @intAmtCostDecimals) AS PlannedDirExpCost,
              ROUND(SUM(ISNULL(ExpDirBill, 0)), @intAmtBillDecimals) AS PlannedDirExpBill,
              ROUND(SUM(ISNULL(ConDirCost, 0)), @intAmtCostDecimals) AS PlannedDirConCost,
              ROUND(SUM(ISNULL(ConDirBill, 0)), @intAmtBillDecimals) AS PlannedDirConBill,
              ROUND(SUM(ISNULL(UntDirCost, 0)), @intAmtCostDecimals) AS PlannedDirUntCost,
              ROUND(SUM(ISNULL(UntDirBill, 0)), @intAmtBillDecimals) AS PlannedDirUntBill,

			  /* Compensation breakout */
              CASE WHEN @strCalcCompF = 'Y' THEN ROUND(SUM(ISNULL(DirLabCost, 0)), @intAmtCostDecimals)
                   ELSE XT.CompensationFeeDirLab END AS DirLabCost,
              CASE WHEN @strCalcCompF = 'Y' THEN ROUND(SUM(ISNULL(DirLabBill, 0)), @intAmtBillDecimals)
                   ELSE XT.CompensationFeeDirLabBill END AS DirLabBill,
              CASE WHEN @strCalcCompF = 'Y' THEN ROUND(SUM(ISNULL(DirExpCost, 0)), @intAmtCostDecimals)
                   ELSE XT.CompensationFeeDirExp END AS DirExpCost,
              CASE WHEN @strCalcCompF = 'Y' THEN ROUND(SUM(ISNULL(DirExpBill, 0)), @intAmtBillDecimals)
                   ELSE XT.CompensationFeeDirExpBill END AS DirExpBill,
              CASE WHEN @strCalcReimA = 'Y' THEN ROUND(SUM(ISNULL(ReimExpCost, 0)), @intAmtCostDecimals)
                   ELSE XT.ReimbAllowanceExp END AS ReimExpCost,
              CASE WHEN @strCalcReimA = 'Y' THEN ROUND(SUM(ISNULL(ReimExpBill, 0)), @intAmtBillDecimals)
                   ELSE XT.ReimbAllowanceExpBill END AS ReimExpBill,
              CASE WHEN @strCalcReimA = 'Y' THEN ROUND(SUM(ISNULL(ReimConsCost, 0)), @intAmtCostDecimals)
                   ELSE XT.ReimbAllowanceCon END AS ReimConsCost,
              CASE WHEN @strCalcReimA = 'Y' THEN ROUND(SUM(ISNULL(ReimConsBill, 0)), @intAmtBillDecimals)
                   ELSE XT.ReimbAllowanceConBill END AS ReimConsBill

         FROM RPTask AS XT LEFT JOIN RPPlan AS P ON P.PlanID = XT.PlanID LEFT JOIN
           (SELECT LT.PlanID AS PlanID, LT.TaskID AS TaskID, 
                   COUNT(XA.AssignmentID) AS ACount, 0 AS ECount, 0 AS CCount, 0 AS UCount,
                   SUM(ISNULL(PeriodHrs, 0)) AS LabPeriodHrs, 
                   SUM(ISNULL(PeriodCost, 0)) AS LabPeriodCost, 
                   SUM(ISNULL(PeriodBill, 0)) AS LabPeriodBill, 
                   SUM(ISNULL(PeriodRev, 0)) AS LabPeriodRev,
                   0 AS ExpPeriodCost, 0 AS ExpPeriodBill, 0 AS ExpPeriodRev,
                   0 AS ConPeriodCost, 0 AS ConPeriodBill, 0 AS ConPeriodRev,
                   0 AS UntPeriodQty, 0 AS UntPeriodCost, 0 AS UntPeriodBill, 0 AS UntPeriodRev,
                   0 AS CompCost, 0 AS CompBill, 0 AS ConsCost, 0 AS ConsBill, 0 AS ReimCost, 0 AS ReimBill,
                   (ISNULL(XA.PctCompleteLabCost, LT.PctCompleteLabCost) / 100.0) * SUM(ISNULL(PeriodCost, 0)) AS LabWtCost,
                   (ISNULL(XA.PctCompleteLabBill, LT.PctCompleteLabBill) / 100.0) * SUM(ISNULL(PeriodBill, 0)) AS LabWtBill,
                   0 AS ExpWtCost, 0 AS ExpWtBill, 0 AS ConWtCost, 0 AS ConWtBill, 0 AS UntWtCost, 0 AS UntWtBill,
                   0 AS ExpDirCost, 0 AS ExpDirBill, 0 AS ConDirCost, 0 AS ConDirBill, 0 AS UntDirCost, 0 AS UntDirBill,

				   /* Compensation breakout */
                   0 AS DirLabCost, 0 AS DirExpCost, 0 AS DirLabBill, 0 AS DirExpBill,
                   0 AS ReimExpCost, 0 AS ReimConsCost, 0 AS ReimExpBill, 0 AS ReimConsBill

              FROM RPTask AS LT 
                LEFT JOIN RPAssignment AS XA ON LT.PlanID = XA.PlanID AND LT.TaskID = XA.TaskID
                LEFT JOIN RPPlannedLabor AS LTPD ON LT.PlanID = LTPD.PlanID AND LT.TaskID = LTPD.TaskID AND ISNULL(LTPD.AssignmentID, 1) = ISNULL(XA.AssignmentID, 1)
              WHERE LT.PlanID = @strPlanID AND LT.ChildrenCount = 0
              GROUP BY  LT.PlanID, LT.TaskID, XA.AssignmentID, XA.PctCompleteLabCost, XA.PctCompleteLabBill,
                LT.PctCompleteLabCost, LT.PctCompleteLabBill
            UNION ALL --> RPPlannedExpenses
            SELECT ET.PlanID AS PlanID, ET.TaskID AS TaskID, 
                   0 AS ACount, COUNT(XE.ExpenseID) AS ECount, 0 AS CCount, 0 AS UCount,
                   0 AS LabPeriodHrs, 0 AS LabPeriodCost, 0 AS LabPeriodBill, 0 AS LabPeriodRev,
                   SUM(ISNULL(PeriodCost, 0)) AS ExpPeriodCost, 
                   SUM(ISNULL(PeriodBill, 0)) AS ExpPeriodBill, 
                   SUM(ISNULL(PeriodRev, 0)) AS ExpPeriodRev,
                   0 AS ConPeriodCost, 0 AS ConPeriodBill, 0 AS ConPeriodRev,
                   0 AS UntPeriodQty, 0 AS UntPeriodCost, 0 AS UntPeriodBill, 0 AS UntPeriodRev,
                   0 AS CompCost, 0 AS CompBill, 0 AS ConsCost, 0 AS ConsBill, 0 AS ReimCost, 0 AS ReimBill,
                   0 AS LabWtCost, 0 AS LabWtBill,
                   (ISNULL(XE.PctCompleteExpCost, ET.PctCompleteExpCost) / 100.0) * SUM(ISNULL(PeriodCost, 0)) AS ExpWtCost,
                   (ISNULL(XE.PctCompleteExpBill, ET.PctCompleteExpBill) / 100.0) * SUM(ISNULL(PeriodBill, 0)) AS ExpWtBill,
                   0 AS ConWtCost, 0 AS ConWtBill,
                   0 AS UntWtCost, 0 AS UntWtBill,
                   ROUND(ISNULL(SUM(CASE WHEN (XE.DirectAcctFlg = 'Y' OR XE.DirectAcctFlg IS NULL)
                                         THEN PeriodCost ELSE 0.0 END), 0), @intAmtCostDecimals) AS ExpDirCost,
                   ROUND(ISNULL(SUM(CASE WHEN (XE.DirectAcctFlg = 'Y' OR XE.DirectAcctFlg IS NULL)
                                         THEN PeriodBill ELSE 0.0 END), 0), @intAmtBillDecimals) AS ExpDirBill,
                   0 AS ConDirCost, 0 AS ConDirBill, 
                   0 AS UntDirCost, 0 AS UntDirBill,

				   /* Compensation breakout */
                   0 AS DirLabCost, 0 AS DirExpCost, 0 AS DirLabBill, 0 AS DirExpBill,
                   0 AS ReimExpCost, 0 AS ReimConsCost, 0 AS ReimExpBill, 0 AS ReimConsBill

              FROM RPTask AS ET 
                LEFT JOIN RPExpense AS XE ON ET.PlanID = XE.PlanID AND ET.TaskID = XE.TaskID
                LEFT JOIN RPPlannedExpenses AS ETPD ON ET.PlanID = ETPD.PlanID AND ET.TaskID = ETPD.TaskID AND ISNULL(ETPD.ExpenseID, 1) = ISNULL(XE.ExpenseID, 1)
              WHERE ET.PlanID = @strPlanID AND ET.ChildrenCount = 0
              GROUP BY ET.PlanID, ET.TaskID, XE.ExpenseID, XE.PctCompleteExpCost, XE.PctCompleteExpBill,
                ET.PctCompleteExpCost, ET.PctCompleteExpBill
            UNION ALL --> RPPlannedConsultant
            SELECT CT.PlanID AS PlanID, CT.TaskID AS TaskID, 
                   0 AS ACount, 0 AS ECount, COUNT(XC.ConsultantID) AS CCount, 0 AS UCount,
                   0 AS LabPeriodHrs, 0 AS LabPeriodCost, 0 AS LabPeriodBill, 0 AS LabPeriodRev,
                   0 AS ExpPeriodCost, 0 AS ExpPeriodBill, 0 AS ExpPeriodRev,
                   SUM(ISNULL(PeriodCost, 0)) AS ConPeriodCost, 
                   SUM(ISNULL(PeriodBill, 0)) AS ConPeriodBill, 
                   SUM(ISNULL(PeriodRev, 0)) AS ConPeriodRev,
                   0 AS UntPeriodQty, 0 AS UntPeriodCost, 0 AS UntPeriodBill, 0 AS UntPeriodRev,
                   0 AS CompCost, 0 AS CompBill, 0 AS ConsCost, 0 AS ConsBill, 0 AS ReimCost, 0 AS ReimBill,
                   0 AS LabWtCost, 0 AS LabWtBill, 0 AS ExpWtCost, 0 AS ExpWtBill,
                   (ISNULL(XC.PctCompleteConCost, CT.PctCompleteConCost) / 100.0) * SUM(ISNULL(PeriodCost, 0)) AS ConWtCost,
                   (ISNULL(XC.PctCompleteConBill, CT.PctCompleteConBill) / 100.0) * SUM(ISNULL(PeriodBill, 0)) AS ConWtBill,
                   0 AS UntWtCost, 0 AS UntWtBill,
                   0 AS ExpDirCost, 0 AS ExpDirBill,
                   ROUND(ISNULL(SUM(CASE WHEN (XC.DirectAcctFlg = 'Y' OR XC.DirectAcctFlg IS NULL)
                                         THEN PeriodCost ELSE 0.0 END), 0), @intAmtCostDecimals) AS ConDirCost,
                   ROUND(ISNULL(SUM(CASE WHEN (XC.DirectAcctFlg = 'Y' OR XC.DirectAcctFlg IS NULL)
                                         THEN PeriodBill ELSE 0.0 END), 0), @intAmtBillDecimals) AS ConDirBill,
                   0 AS UntDirCost, 0 AS UntDirBill,

				   /* Compensation breakout */
                   0 AS DirLabCost, 0 AS DirExpCost, 0 AS DirLabBill, 0 AS DirExpBill,
                   0 AS ReimExpCost, 0 AS ReimConsCost, 0 AS ReimExpBill, 0 AS ReimConsBill

               FROM RPTask AS CT 
                 LEFT JOIN RPConsultant AS XC ON CT.PlanID = XC.PlanID AND CT.TaskID = XC.TaskID
                 LEFT JOIN RPPlannedConsultant AS CTPD ON CT.PlanID = CTPD.PlanID AND CT.TaskID = CTPD.TaskID AND ISNULL(CTPD.ConsultantID, 1) = ISNULL(XC.ConsultantID, 1)
               WHERE CT.PlanID = @strPlanID AND CT.ChildrenCount = 0
               GROUP BY CT.PlanID, CT.TaskID, XC.ConsultantID, XC.PctCompleteConCost, XC.PctCompleteConBill,
                 CT.PctCompleteConCost, CT.PctCompleteConBill
            UNION ALL --> RPPlannedUnit
            SELECT UT.PlanID AS PlanID, UT.TaskID AS TaskID, 
                   0 AS ACount, 0 AS ECount, 0 AS CCount, COUNT(XU.UnitID) AS UCount,
                   0 AS LabPeriodHrs, 0 AS LabPeriodCost, 0 AS LabPeriodBill, 0 AS LabPeriodRev,
                   0 AS ExpPeriodCost, 0 AS ExpPeriodBill, 0 AS ExpPeriodRev,
                   0 AS ConPeriodCost, 0 AS ConPeriodBill, 0 AS ConPeriodRev,
                   SUM(ISNULL(PeriodQty, 0)) AS UntPeriodQty, 
                   SUM(ISNULL(PeriodCost, 0)) AS UntPeriodCost, 
                   SUM(ISNULL(PeriodBill, 0)) AS UntPeriodBill, 
                   SUM(ISNULL(PeriodRev, 0)) AS UntPeriodRev,
                   0 AS CompCost, 0 AS CompBill, 0 AS ConsCost, 0 AS ConsBill, 0 AS ReimCost, 0 AS ReimBill,
                   0 AS LabWtCost, 0 AS LabWtBill, 
                   0 AS ExpWtCost, 0 AS ExpWtBill,
                   0 AS ConWtCost, 0 AS ConWtBill,
                   (ISNULL(XU.PctCompleteUntCost, UT.PctCompleteUntCost) / 100.0) * SUM(ISNULL(PeriodCost, 0)) AS UntWtCost,
                   (ISNULL(XU.PctCompleteUntBill, UT.PctCompleteUntBill) / 100.0) * SUM(ISNULL(PeriodBill, 0)) AS UntWtBill,
                   0 AS ExpDirCost, 0 AS ExpDirBill,
                   0 AS ConDirCost, 0 AS ConDirBill, 
                   ROUND(ISNULL(SUM(CASE WHEN (XU.DirectAcctFlg = 'Y' OR XU.DirectAcctFlg IS NULL)
                                         THEN PeriodCost ELSE 0.0 END), 0), @intAmtCostDecimals) AS UntDirCost,
                   ROUND(ISNULL(SUM(CASE WHEN (XU.DirectAcctFlg = 'Y' OR XU.DirectAcctFlg IS NULL)
                                         THEN PeriodBill ELSE 0.0 END), 0), @intAmtBillDecimals) AS UntDirBill,

				   /* Compensation breakout */
                   0 AS DirLabCost, 0 AS DirExpCost, 0 AS DirLabBill, 0 AS DirExpBill,
                   0 AS ReimExpCost, 0 AS ReimConsCost, 0 AS ReimExpBill, 0 AS ReimConsBill

               FROM RPTask AS UT 
                 LEFT JOIN RPUnit AS XU ON UT.PlanID = XU.PlanID AND UT.TaskID = XU.TaskID
                 LEFT JOIN RPPlannedUnit AS UTPD ON UT.PlanID = UTPD.PlanID AND UT.TaskID = UTPD.TaskID AND ISNULL(UTPD.UnitID, 1) = ISNULL(XU.UnitID, 1)
               WHERE UT.PlanID = @strPlanID AND UT.ChildrenCount = 0
               GROUP BY UT.PlanID, UT.TaskID, XU.UnitID, XU.PctCompleteUntCost, XU.PctCompleteUntBill,
                 UT.PctCompleteUntCost, UT.PctCompleteUntBill
            UNION ALL --> RPCompensationFee
            SELECT CPT.PlanID AS PlanID, CPT.TaskID AS TaskID, 
                   0 AS ACount, 0 AS ECount, 0 AS CCount, 0 AS UCount,
                   0 AS LabPeriodHrs, 0 AS LabPeriodCost, 0 AS LabPeriodBill, 0 AS LabPeriodRev,
                   0 AS ExpPeriodCost, 0 AS ExpPeriodBill, 0 AS ExpPeriodRev,
                   0 AS ConPeriodCost, 0 AS ConPeriodBill, 0 AS ConPeriodRev,
                   0 AS UntPeriodQty, 0 AS UntPeriodCost, 0 AS UntPeriodBill, 0 AS UntPeriodRev,
                   SUM(ISNULL(PeriodCost, 0)) AS CompCost, 
                   SUM(ISNULL(PeriodBill, 0)) AS CompBill, 
                   0 AS ConsCost, 0 AS ConsBill, 0 AS ReimCost, 0 AS ReimBill,
                   0 AS LabWtCost, 0 AS LabWtBill, 0 AS ExpWtCost, 0 AS ExpWtBill, 0 AS ConWtCost, 0 AS ConWtBill, 0 AS UntWtCost, 0 AS UntWtBill,
                   0 AS ExpDirCost, 0 AS ExpDirBill, 0 AS ConDirCost, 0 AS ConDirBill, 0 AS UntDirCost, 0 AS UntDirBill,

				   /* Compensation breakout */
                   SUM(ISNULL(PeriodDirLabCost, 0)) AS DirLabCost, 
                   SUM(ISNULL(PeriodDirExpCost, 0)) AS DirExpCost, 
                   SUM(ISNULL(PeriodDirLabBill, 0)) AS DirLabBill, 
                   SUM(ISNULL(PeriodDirExpBill, 0)) AS DirExpBill,
                   0 AS ReimExpCost, 0 AS ReimConsCost, 0 AS ReimExpBill, 0 AS ReimConsBill

               FROM RPTask AS CPT LEFT JOIN RPCompensationFee AS CPTPD ON CPT.PlanID = CPTPD.PlanID AND CPT.TaskID = CPTPD.TaskID
               WHERE CPT.PlanID = @strPlanID AND CPT.ChildrenCount = 0
               GROUP BY CPT.PlanID, CPT.TaskID
            UNION ALL --> RPConsultantFee
            SELECT CST.PlanID AS PlanID, CST.TaskID AS TaskID, 
                   0 AS ACount, 0 AS ECount, 0 AS CCount, 0 AS UCount,
                   0 AS LabPeriodHrs, 0 AS LabPeriodCost, 0 AS LabPeriodBill, 0 AS LabPeriodRev,
                   0 AS ExpPeriodCost, 0 AS ExpPeriodBill, 0 AS ExpPeriodRev,
                   0 AS ConPeriodCost, 0 AS ConPeriodBill, 0 AS ConPeriodRev,
                   0 AS UntPeriodQty, 0 AS UntPeriodCost, 0 AS UntPeriodBill, 0 AS UntPeriodRev,
                   0 AS CompCost, 0 AS CompBill, 
                   SUM(ISNULL(PeriodCost, 0)) AS ConsCost, 
                   SUM(ISNULL(PeriodBill, 0)) AS ConsBill, 
                   0 AS ReimCost, 0 AS ReimBill,
                   0 AS LabWtCost, 0 AS LabWtBill, 0 AS ExpWtCost, 0 AS ExpWtBill, 0 AS ConWtCost, 0 AS ConWtBill, 0 AS UntWtCost, 0 AS UntWtBill,
                   0 AS ExpDirCost, 0 AS ExpDirBill, 0 AS ConDirCost, 0 AS ConDirBill, 0 AS UntDirCost, 0 AS UntDirBill,

				   /* Compensation breakout */
                   0 AS DirLabCost, 0 AS DirExpCost, 0 AS DirLabBill, 0 AS DirExpBill,
                   0 AS ReimExpCost, 0 AS ReimConsCost, 0 AS ReimExpBill, 0 AS ReimConsBill

               FROM RPTask AS CST LEFT JOIN RPConsultantFee AS CSTPD ON CST.PlanID = CSTPD.PlanID AND CST.TaskID = CSTPD.TaskID
               WHERE CST.PlanID = @strPlanID AND CST.ChildrenCount = 0
               GROUP BY CST.PlanID, CST.TaskID
            UNION ALL --> RPReimbAllowance
            SELECT RT.PlanID AS PlanID, RT.TaskID AS TaskID, 
                   0 AS ACount, 0 AS ECount, 0 AS CCount, 0 AS UCount,
                   0 AS LabPeriodHrs, 0 AS LabPeriodCost, 0 AS LabPeriodBill, 0 AS LabPeriodRev,
                   0 AS ExpPeriodCost, 0 AS ExpPeriodBill, 0 AS ExpPeriodRev,
                   0 AS ConPeriodCost, 0 AS ConPeriodBill, 0 AS ConPeriodRev,
                   0 AS UntPeriodQty, 0 AS UntPeriodCost, 0 AS UntPeriodBill, 0 AS UntPeriodRev,
                   0 AS CompCost, 0 AS CompBill, 0 AS ConsCost, 0 AS ConsBill, 
                   SUM(ISNULL(PeriodCost, 0)) AS ReimCost, 
                   SUM(ISNULL(PeriodBill, 0)) AS ReimBill, 
                   0 AS LabWtCost, 0 AS LabWtBill, 0 AS ExpWtCost, 0 AS ExpWtBill, 0 AS ConWtCost, 0 AS ConWtBill, 0 AS UntWtCost, 0 AS UntWtBill,
                   0 AS ExpDirCost, 0 AS ExpDirBill, 0 AS ConDirCost, 0 AS ConDirBill, 0 AS UntDirCost, 0 AS UntDirBill,

				   /* Compensation breakout */
                   0 AS DirLabCost, 0 AS DirExpCost, 0 AS DirLabBill, 0 AS DirExpBill,
                   SUM(ISNULL(PeriodExpCost, 0)) AS ReimExpCost, 
                   SUM(ISNULL(PeriodConCost, 0)) AS ReimConsCost, 
                   SUM(ISNULL(PeriodExpBill, 0)) AS ReimExpBill, 
                   SUM(ISNULL(PeriodConBill, 0)) AS ReimConsBill

               FROM RPTask AS RT LEFT JOIN RPReimbAllowance AS RTPD ON RT.PlanID = RTPD.PlanID AND RT.TaskID = RTPD.TaskID
               WHERE RT.PlanID = @strPlanID AND RT.ChildrenCount = 0
               GROUP BY RT.PlanID, RT.TaskID) AS XTPD
           ON XT.PlanID = XTPD.PlanID AND XT.TaskID = XTPD.TaskID
           WHERE XT.PlanID = @strPlanID AND XT.ChildrenCount = 0
         GROUP BY XT.PlanID, XT.TaskID, P.LabMultType, P.ReimbMethod,
           XT.CompensationFee, XT.CompensationFeeBill, XT.ConsultantFee, XT.ConsultantFeeBill,
           XT.ReimbAllowance, XT.ReimbAllowanceBill,
           XT.CompensationFeeDirLab, XT.CompensationFeeDirLabBill, XT.CompensationFeeDirExp, XT.CompensationFeeDirExpBill,
           XT.ReimbAllowanceExp, XT.ReimbAllowanceExpBill, XT.ReimbAllowanceCon, XT.ReimbAllowanceConBill
           ) AS TPD 
      ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID
      LEFT JOIN RPPlan AS XP ON T.PlanID = XP.PlanID
 
  IF (@strCalcLab = 'Y')
    BEGIN

      -- Update Assignment.
      
      UPDATE RPAssignment
        SET PlannedLaborHrs = TPD.PlannedLaborHrs, 
            PlannedLabCost = TPD.PlannedLabCost,
            PlannedLabBill = TPD.PlannedLabBill, 
            LabRevenue = TPD.LabRevenue,
            WeightedLabCost = TPD.WeightedLabCost,
            WeightedLabBill = TPD.WeightedLabBill
        FROM RPAssignment AS A INNER JOIN
          (SELECT XA.PlanID, XA.TaskID, XA.AssignmentID, 
                  ROUND(SUM(ISNULL(PeriodHrs, 0)), @intHrDecimals) AS PlannedLaborHrs,
                  ROUND(SUM(ISNULL(PeriodCost, 0)), @intAmtCostDecimals) AS PlannedLabCost,
                  ROUND(SUM(ISNULL(PeriodBill, 0)), @intAmtBillDecimals) AS PlannedLabBill,
                  ROUND(SUM(ISNULL(PeriodRev, 0)), CASE WHEN P.LabMultType < 2 
                                                        THEN @intAmtCostDecimals 
                                                        ELSE @intAmtBillDecimals END) AS LabRevenue,
                  (XA.PctCompleteLabCost / 100.0) * SUM(ISNULL(PeriodCost, 0)) AS WeightedLabCost,
                  (XA.PctCompleteLabBill / 100.0) * SUM(ISNULL(PeriodBill, 0)) AS WeightedLabBill
            FROM RPAssignment AS XA LEFT JOIN RPPlannedLabor AS TPD
              ON XA.PlanID = TPD.PlanID AND XA.TaskID = TPD.TaskID AND XA.AssignmentID = TPD.AssignmentID
              LEFT JOIN RPPlan AS P ON P.PlanID = XA.PlanID
            WHERE XA.PlanID = @strPlanID
            GROUP BY XA.AssignmentID, XA.PlanID, XA.TaskID, 
            XA.PctCompleteLabCost, XA.PctCompleteLabBill, P.LabMultType) AS TPD
        ON A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID
        WHERE
          A.PlannedLaborHrs <> TPD.PlannedLaborHrs OR 
          A.PlannedLabCost <> TPD.PlannedLabCost OR
          A.PlannedLabBill <> TPD.PlannedLabBill OR 
          A.LabRevenue <> TPD.LabRevenue OR
          A.WeightedLabCost <> TPD.WeightedLabCost OR
          A.WeightedLabBill <> TPD.WeightedLabBill

    END

  IF (@strCalcExp = 'Y')
    BEGIN

      -- Update Expense.
      
      UPDATE RPExpense
        SET PlannedExpCost = TPD.PlannedExpCost, 
            PlannedExpBill = TPD.PlannedExpBill,
            ExpRevenue = TPD.ExpRevenue,
            PlannedDirExpCost = TPD.PlannedDirExpCost,
            PlannedDirExpBill = TPD.PlannedDirExpBill,
            WeightedExpCost = TPD.WeightedExpCost,
            WeightedExpBill = TPD.WeightedExpBill
        FROM RPExpense AS E INNER JOIN
          (SELECT XE.PlanID, XE.TaskID, XE.ExpenseID,
                  ROUND(SUM(ISNULL(PeriodCost, 0)), @intAmtCostDecimals) AS PlannedExpCost,
                  ROUND(SUM(ISNULL(PeriodBill, 0)), @intAmtBillDecimals)AS PlannedExpBill,
                  ROUND(SUM(ISNULL(PeriodRev, 0)), CASE WHEN P.ReimbMethod = 'C' 
                                                        THEN @intAmtCostDecimals
                                                        ELSE @intAmtBillDecimals END) AS ExpRevenue,
                  ROUND(ISNULL(SUM(CASE WHEN (XE.DirectAcctFlg = 'Y' OR XE.DirectAcctFlg IS NULL)
                                        THEN PeriodCost ELSE 0.0 END), 0), @intAmtCostDecimals) AS PlannedDirExpCost,
                  ROUND(ISNULL(SUM(CASE WHEN (XE.DirectAcctFlg = 'Y' OR XE.DirectAcctFlg IS NULL)
                                        THEN PeriodBill ELSE 0.0 END), 0), @intAmtBillDecimals) AS PlannedDirExpBill,
                  (XE.PctCompleteExpCost / 100.0) * SUM(ISNULL(PeriodCost, 0)) AS WeightedExpCost,
                  (XE.PctCompleteExpBill / 100.0) * SUM(ISNULL(PeriodBill, 0)) AS WeightedExpBill
            FROM RPExpense AS XE LEFT JOIN RPPlannedExpenses AS XTPD
              ON XE.PlanID = XTPD.PlanID AND XE.TaskID = XTPD.TaskID AND XE.ExpenseID = XTPD.ExpenseID
              LEFT JOIN RPPlan AS P ON P.PlanID = XE.PlanID
            WHERE XE.PlanID = @strPlanID
            GROUP BY XE.ExpenseID, XE.PlanID, XE.TaskID, XE.DirectAcctFlg,
              XE.PctCompleteExpCost, XE.PctCompleteExpBill, P.ReimbMethod) AS TPD
        ON E.PlanID = TPD.PlanID AND E.TaskID = TPD.TaskID AND E.ExpenseID = TPD.ExpenseID
        WHERE
          E.PlannedExpCost <> TPD.PlannedExpCost OR 
          E.PlannedExpBill <> TPD.PlannedExpBill OR
          E.ExpRevenue <> TPD.ExpRevenue OR
          E.PlannedDirExpCost <> TPD.PlannedDirExpCost OR
          E.PlannedDirExpBill <> TPD.PlannedDirExpBill OR
          E.WeightedExpCost <> TPD.WeightedExpCost OR
          E.WeightedExpBill <> TPD.WeightedExpBill
        
    END
    
  IF (@strCalcCon = 'Y')
    BEGIN

      -- Update Consultant.
      
      UPDATE RPConsultant
        SET PlannedConCost = TPD.PlannedConCost, 
            PlannedConBill = TPD.PlannedConBill,
            ConRevenue = TPD.ConRevenue,
            PlannedDirConCost = TPD.PlannedDirConCost,
            PlannedDirConBill = TPD.PlannedDirConBill,
            WeightedConCost = TPD.WeightedConCost,
            WeightedConBill = TPD.WeightedConBill
        FROM RPConsultant AS C INNER JOIN
          (SELECT XC.PlanID, XC.TaskID, XC.ConsultantID,
                  ROUND(SUM(ISNULL(PeriodCost, 0)), @intAmtCostDecimals) AS PlannedConCost,
                  ROUND(SUM(ISNULL(PeriodBill, 0)), @intAmtBillDecimals)AS PlannedConBill,
                  ROUND(SUM(ISNULL(PeriodRev, 0)), CASE WHEN P.ReimbMethod = 'C' 
                                                        THEN @intAmtCostDecimals
                                                        ELSE @intAmtBillDecimals END) AS ConRevenue,
                  ROUND(ISNULL(SUM(CASE WHEN (XC.DirectAcctFlg = 'Y' OR XC.DirectAcctFlg IS NULL)
                                        THEN PeriodCost ELSE 0.0 END), 0), @intAmtCostDecimals) AS PlannedDirConCost,
                  ROUND(ISNULL(SUM(CASE WHEN (XC.DirectAcctFlg = 'Y' OR XC.DirectAcctFlg IS NULL)
                                        THEN PeriodBill ELSE 0.0 END), 0), @intAmtBillDecimals) AS PlannedDirConBill,
                  (XC.PctCompleteConCost / 100.0) * SUM(ISNULL(PeriodCost, 0)) AS WeightedConCost,
                  (XC.PctCompleteConBill / 100.0) * SUM(ISNULL(PeriodBill, 0)) AS WeightedConBill
            FROM RPConsultant AS XC LEFT JOIN RPPlannedConsultant AS XTPD
              ON XC.PlanID = XTPD.PlanID AND XC.TaskID = XTPD.TaskID AND XC.ConsultantID = XTPD.ConsultantID
              LEFT JOIN RPPlan AS P ON P.PlanID = XC.PlanID
            WHERE XC.PlanID = @strPlanID
            GROUP BY XC.ConsultantID, XC.PlanID, XC.TaskID, XC.DirectAcctFlg,
              XC.PctCompleteConCost, XC.PctCompleteConBill, P.ReimbMethod) AS TPD
        ON C.PlanID = TPD.PlanID AND C.TaskID = TPD.TaskID AND C.ConsultantID = TPD.ConsultantID
        WHERE 
          C.PlannedConCost <> TPD.PlannedConCost OR 
          C.PlannedConBill <> TPD.PlannedConBill OR
          C.ConRevenue <> TPD.ConRevenue OR
          C.PlannedDirConCost <> TPD.PlannedDirConCost OR
          C.PlannedDirConBill <> TPD.PlannedDirConBill OR
          C.WeightedConCost <> TPD.WeightedConCost OR
          C.WeightedConBill <> TPD.WeightedConBill

    END

  IF (@strCalcUnt = 'Y')
    BEGIN

      -- Update Unit.
      
      UPDATE RPUnit
        SET PlannedUntQty = TPD.PlannedUntQty,
            PlannedUntCost = TPD.PlannedUntCost, 
            PlannedUntBill = TPD.PlannedUntBill,
            UntRevenue = TPD.UntRevenue,
            PlannedDirUntCost = TPD.PlannedDirUntCost,
            PlannedDirUntBill = TPD.PlannedDirUntBill,
            WeightedUntCost = TPD.WeightedUntCost,
            WeightedUntBill = TPD.WeightedUntBill
        FROM RPUnit AS U INNER JOIN
          (SELECT XU.PlanID, XU.TaskID, XU.UnitID,
                  ROUND(SUM(ISNULL(PeriodQty, 0)), @intQtyDecimals) AS PlannedUntQty,
                  ROUND(SUM(ISNULL(PeriodCost, 0)), @intAmtCostDecimals) AS PlannedUntCost,
                  ROUND(SUM(ISNULL(PeriodBill, 0)), @intAmtBillDecimals)AS PlannedUntBill,
                  ROUND(SUM(ISNULL(PeriodRev, 0)), CASE WHEN P.ReimbMethod = 'C' 
                                                        THEN @intAmtCostDecimals
                                                        ELSE @intAmtBillDecimals END) AS UntRevenue,
                  ROUND(ISNULL(SUM(CASE WHEN (XU.DirectAcctFlg = 'Y' OR XU.DirectAcctFlg IS NULL)
                                        THEN PeriodCost ELSE 0.0 END), 0), @intAmtCostDecimals) AS PlannedDirUntCost,
                  ROUND(ISNULL(SUM(CASE WHEN (XU.DirectAcctFlg = 'Y' OR XU.DirectAcctFlg IS NULL)
                                        THEN PeriodBill ELSE 0.0 END), 0), @intAmtBillDecimals) AS PlannedDirUntBill,
                  (XU.PctCompleteUntCost / 100.0) * SUM(ISNULL(PeriodCost, 0)) AS WeightedUntCost,
                  (XU.PctCompleteUntBill / 100.0) * SUM(ISNULL(PeriodBill, 0)) AS WeightedUntBill
            FROM RPUnit AS XU LEFT JOIN RPPlannedUnit AS XTPD
              ON XU.PlanID = XTPD.PlanID AND XU.TaskID = XTPD.TaskID AND XU.UnitID = XTPD.UnitID
              LEFT JOIN RPPlan AS P ON P.PlanID = XU.PlanID
            WHERE XU.PlanID = @strPlanID
            GROUP BY XU.UnitID, XU.PlanID, XU.TaskID, XU.DirectAcctFlg,
              XU.PctCompleteUntCost, XU.PctCompleteUntBill, P.ReimbMethod) AS TPD
        ON U.PlanID = TPD.PlanID AND U.TaskID = TPD.TaskID AND U.UnitID = TPD.UnitID
        WHERE 
          U.PlannedUntQty <> TPD.PlannedUntQty OR
          U.PlannedUntCost <> TPD.PlannedUntCost OR 
          U.PlannedUntBill <> TPD.PlannedUntBill OR
          U.UntRevenue <> TPD.UntRevenue OR
          U.PlannedDirUntCost <> TPD.PlannedDirUntCost OR
          U.PlannedDirUntBill <> TPD.PlannedDirUntBill OR
          U.WeightedUntCost <> TPD.WeightedUntCost OR
          U.WeightedUntBill <> TPD.WeightedUntBill
  
    END
        
  SET NOCOUNT OFF

END -- rpCalcTPD
GO
