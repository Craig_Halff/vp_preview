SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--exec spCCG_IW_WatcherConfig

CREATE PROCEDURE [dbo].[spCCG_IW_WatcherConfig]
AS
BEGIN
	SET NOCOUNT ON;

	declare @WSEnabled varchar(1) = 'N'
	if exists(SELECT 'x' as result FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeltekStoredProc_CCG_IW_WatcherConfig]') AND type in (N'P', N'PC'))
		set @WSEnabled = 'Y'

	declare @EI varchar(1) = 'N'
	if exists(SELECT 'x' as result FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CCG_EI_Config]') AND type in (N'U'))
		set @EI = 'Y'

	declare @ARM varchar(1) = 'N'
	if exists(SELECT 'x' as result FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CCG_ARM_Config]') AND type in (N'U'))
		set @ARM = 'Y'

	declare @WatcherLogFound varchar(3) = 'N'
	if @WSEnabled = 'Y'
	begin
		if exists(SELECT 'x' as result FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeltekStoredProc_CCG_IW_WatcherLog]') AND type in (N'P', N'PC'))
			set @WatcherLogFound = 'Y'
	
		else if exists(SELECT 'x' as result FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeltekStoredProc_CCG_EI_WatcherLog]') AND type in (N'P', N'PC'))
			set @WatcherLogFound = 'EI'
	end
	else 
	begin
		if exists(SELECT 'x' as result FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCCG_IW_WatcherLog]') AND type in (N'P', N'PC'))
			set @WatcherLogFound = 'Y'
	
		else if exists(SELECT 'x' as result FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCCG_EI_WatcherLog]') AND type in (N'P', N'PC'))
			set @WatcherLogFound = 'EI'
	end

	if @EI = 'Y'
		select Version,DraftInvoiceWildcardString as invoiceWildcardString,ISNULL(FinalInvoiceWildcardString,'') as finalInvoiceWildcardString,UnbilledDetailWildcardString, @EI as EI,@ARM as ARM, @WatcherLogFound as WatcherLogFound, @WSEnabled as WSEnabled from CCG_EI_Config

	else if @ARM = 'Y'
		select Version,'DraftInvoiceWildcardString' as invoiceWildcardString,'FinalInvoiceWildcardString' as finalInvoiceWildcardString,'UnbilledDetailWildcardString' as UnbilledDetailWildcardString, @EI as EI,@ARM as ARM, @WatcherLogFound as WatcherLogFound, @WSEnabled as WSEnabled from CCG_ARM_Config

	else 
		select '0', 'DraftInvoiceWildcardString' as invoiceWildcardString,'FinalInvoiceWildcardString' as finalInvoiceWildcardString,'UnbilledDetailWildcardString' as UnbilledDetailWildcardString,  @EI as EI,@ARM as ARM, @WatcherLogFound as WatcherLogFound, @WSEnabled as WSEnabled

END;

GO
