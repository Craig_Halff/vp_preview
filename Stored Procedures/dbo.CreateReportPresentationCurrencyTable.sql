SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[CreateReportPresentationCurrencyTable]
	@presentationCurrencyCode Nvarchar(3)
	, @targetDate datetime
	, @sessionID varchar(32)
	, @CalledFromRDL varchar(1) = 'N'			-- Optional Parameter: Default is 'N'; It will be 'Y' if it's called from RDL.
as
declare
@fromCurrencyCode Nvarchar(3),
@triangulationCurrencyCode Nvarchar(3),
@rate float(8),
@rate2 float(8),
@triangulationRate float(8),
@presentationCurrencyDecimalPlaces int,
@presentationCurrencyCurrencySymbol Nvarchar(5),
@lastRun datetime,
@enabledCurrenciesEOF bit,
@foundExchangeRate int,
@presentationCurrencyCursorEOF bit,
@rateErrorMessage Nvarchar(50),
@company Nvarchar(14),
@presentationCurrencyRate float(2),
@lastCompany Nvarchar(14),
@sql varchar(max),
@owner varchar(100),
@workTable varchar(100),
@tname VARCHAR(100)
 
begin

Select @lastCompany = 'dfltCompForIRPC'

/* obtain the presentationCurrency decimal places and currency symbol */
Declare presentationCurrencyCursor Cursor For 
	Select DecimalPlaces, CurrencySymbol From FW_CFGCurrency Where Code = @PresentationCurrencyCode

Open presentationCurrencyCursor

Fetch Next From presentationCurrencyCursor Into @presentationCurrencyDecimalPlaces, @presentationCurrencyCurrencySymbol
	Select @presentationCurrencyCursorEOF = @@FETCH_STATUS

If @presentationCurrencyCursorEOF = 1
begin
	Select @presentationCurrencyDecimalPlaces = 0
	Select @presentationCurrencyCurrencySymbol = ''
end

Close presentationCurrencyCursor
Deallocate presentationCurrencyCursor

	IF @CalledFromRDL = 'Y'	-- Use ## Global Temp Table for Report
		Begin
			Set @owner = ''
			Set @tname = '##wkRPC_' + @sessionID 
		End
	Else	-- Use Physical Temp Table for Applications
		Begin
		/* delete existing approver work tables that are three days old*/
			DECLARE db_cursor CURSOR FOR 
			SELECT sys.objects.name AS tname, sys.database_principals.name as owner
            FROM sys.objects inner join sys.database_principals on sys.objects.schema_id = sys.database_principals.principal_id
            WHERE DATEDIFF(DAY,sys.objects.create_date, getdate()) > 2
            and sys.objects.name like 'wkRCPC_%'

			OPEN db_cursor  
			FETCH NEXT FROM db_cursor INTO @tname, @owner  

			WHILE @@FETCH_STATUS = 0  
			BEGIN  
				SET @sql = 'DROP TABLE ' + @owner + '.' + @tname
				EXEC (@sql)

				FETCH NEXT FROM db_cursor INTO @tname, @owner  
			END  

			CLOSE db_cursor  
			DEALLOCATE db_cursor
		/* end delete */

			Set @owner = 'dbo.'
			Set @tname = 'wkRPC_' + @sessionID 
		End

	set @workTable = @owner + @tname
	IF @CalledFromRDL = 'N' and OBJECT_ID(@tname) IS NOT NULL	-- When Not from RDL and table exists, reuse object
		Begin
			begin tran
				select @sql = 'delete from ' + @workTable 
				exec (@sql)
			commit
		End
	ELSE	
		Begin
/* create temporary ReportPresentationCurrency table */
			begin tran
				select @sql = 'CREATE TABLE ' + @workTable + ' (SessionID varchar(32) COLLATE database_default, Company nvarchar(14) COLLATE database_default, FromCurrencyCode nvarchar(3) COLLATE database_default, PresentationCurrencyCode nvarchar(3) COLLATE database_default, Rate decimal(19, 10) DEFAULT ((0)) NOT NULL, PresentationCurrencyDecimalPlaces smallint DEFAULT ((0)) NOT NULL, PresentationCurrencyCurrencySymbol nvarchar(5) COLLATE database_default, [Message] nvarchar(50) COLLATE database_default, LastRun datetime NULL '
				select @sql = @sql + 'CONSTRAINT RPC_' + @sessionID + 'PK PRIMARY KEY NONCLUSTERED (SessionID ASC, Company ASC, FromCurrencyCode ASC, PresentationCurrencyCode ASC) WITH FILLFACTOR = 90)'
				exec (@sql)
			commit
		End

Select @presentationCurrencyRate = 1


/*enabled currencies cursor */
--NH 08/07/2012: Change to establish exchange rates between all enabled currencies for all companies instead of only for enabled currencies of a company
DECLARE enabledCurrencies CURSOR FOR
Select CFGMainData.Company, EnabledCurrencies.CurrencyCode, CFGMainData.TriangulationCurrencyCode From CFGMainData, (Select distinct CurrencyCode From FW_CFGEnabledCurrencies) EnabledCurrencies
Order By CFGMainData.Company, EnabledCurrencies.CurrencyCode

--Select  CFGMainData.Company, FW_CFGEnabledCurrencies.CurrencyCode, CFGMainData.TriangulationCurrencyCode From CFGMainData, FW_CFGEnabledCurrencies Where CFGMainData.Company = FW_CFGEnabledCurrencies.Company 
--Order By CFGMainData.Company, FW_CFGEnabledCurrencies.CurrencyCode
 
Open enabledCurrencies

FETCH NEXT from enabledCurrencies Into @company, @fromCurrencyCode, @triangulationCurrencyCode
	Select @enabledCurrenciesEOF = @@FETCH_STATUS

/* Loop */
While (@enabledCurrenciesEOF = 0)
BEGIN
	/* establish default values */
	Select @rate = 0
	Select @rate2 = 0
	Select @triangulationRate = 0
	Select @foundExchangeRate = 0
	Select @rateErrorMessage = ''
		
	/* first row entered into the ReportPresentationCurrencyTable will be the Presentation Currency itself */
	/* only insert once for each company */
	if @company <> @lastCompany
	begin
		select @sql = 'Insert Into ' + @workTable + ' (SessionID, Company, FromCurrencyCode, PresentationCurrencyCode, Rate, PresentationCurrencyDecimalPlaces, PresentationCurrencyCurrencySymbol, LastRun) values'
		select @sql = @sql + '(''' + @sessionID + ''', ''' + @company + ''', ''' + @presentationCurrencyCode + ''', ''' + @presentationCurrencyCode + ''', ''' + convert(varchar,cast(@presentationCurrencyRate as decimal(19,10))) + ''', ''' + convert(varchar,@presentationCurrencyDecimalPlaces) + ''', ''' + IsNull(@presentationCurrencyCurrencySymbol, '') + ''', ''' + convert(varchar, getDate(), 121) + ''')'
		exec (@sql)
	end

	If @fromCurrencyCode <> @presentationCurrencyCode
	begin

	/* look for a direct exchange rate */
	Execute @foundExchangeRate = GetExchangeRate @fromCurrencyCode, @presentationCurrencyCode,@targetDate, @rate output, 0
 
	if @foundExchangeRate = 1
	 begin 
		 select @sql = 'Insert Into ' + @workTable + ' (SessionID, Company, FromCurrencyCode, PresentationCurrencyCode, Rate, PresentationCurrencyDecimalPlaces, PresentationCurrencyCurrencySymbol, LastRun) values'
		 select @sql = @sql + '(''' + @sessionID + ''', ''' + @company + ''', ''' + @fromCurrencyCode + ''', ''' + @presentationCurrencyCode + ''', ''' + convert(varchar,cast(@rate as decimal(19,10))) + ''', ''' + convert(varchar,@presentationCurrencyDecimalPlaces) + ''', ''' + IsNull(@presentationCurrencyCurrencySymbol, '') + ''', ''' + convert(varchar, getDate(), 121) + ''')'
		 exec (@sql)
	 end
	 

	/* look for an inverse exchange rate */
	if @foundExchangeRate = 0
	begin
		Execute @foundExchangeRate = GetExchangeRate @presentationCurrencyCode, @fromCurrencyCode, @targetDate, @rate output, 1
 
		if @foundExchangeRate = 1
		 
		begin
			 select @sql = 'Insert Into ' + @workTable + ' (SessionID, Company, FromCurrencyCode, PresentationCurrencyCode, Rate, PresentationCurrencyDecimalPlaces, PresentationCurrencyCurrencySymbol, LastRun) values'
			 select @sql = @sql + '(''' + @sessionID + ''', ''' + @company + ''', ''' + @fromCurrencyCode + ''', ''' + @presentationCurrencyCode + ''', ''' + convert(varchar,cast(@rate as decimal(19,10))) + ''', ''' + convert(varchar,@presentationCurrencyDecimalPlaces) + ''', ''' + IsNull(@presentationCurrencyCurrencySymbol, '') + ''', ''' + convert(varchar, getDate(), 121) + ''')'
			 exec (@sql)
		end

		/* look for a triangulation exchange rate */

		if @foundExchangeRate = 0
		begin
			if @triangulationCurrencyCode = @fromCurrencyCode /* error in the vb class multicurrency when this occurs but will set the rate to 0 */
			begin
				Select @rate = 0
				Select @foundExchangeRate = 1
				Select @rateErrorMessage = 'Exchange rate not found from ' + @fromCurrencyCode + ' to ' + @presentationCurrencyCode
			end

			if @triangulationCurrencyCode = @presentationCurrencyCode /* error in the vb class multicurrency when this occurs but will set the rate to 0 */
			begin
				Select @rate = 0
				Select @foundExchangeRate = 1
				Select @rateErrorMessage = 'Exchange rate not found from ' + @fromCurrencyCode + ' to ' + @presentationCurrencyCode
			end

			if @triangulationCurrencyCode = '' /* error in the vb class multicurrency when this occurs but will set the rate to 0 */
			begin
				Select @rate = 0
				Select @foundExchangeRate = 1
				Select @rateErrorMessage = 'Exchange rate not found from ' + @fromCurrencyCode + ' to ' + @presentationCurrencyCode
			end

			if @foundExchangeRate = 1 
			begin
				select @sql = 'Insert Into ' + @workTable + ' (SessionID, Company, FromCurrencyCode, PresentationCurrencyCode, Rate, PresentationCurrencyDecimalPlaces, PresentationCurrencyCurrencySymbol, Message, LastRun) values'
			 	select @sql = @sql + '(''' + @sessionID + ''', ''' + @company + ''', ''' + @fromCurrencyCode + ''', ''' + @presentationCurrencyCode + ''', ''' + convert(varchar,cast(@rate as decimal(19,10)))+ ''', ''' + convert(varchar,@presentationCurrencyDecimalPlaces) + ''', ''' + IsNull(@presentationCurrencyCurrencySymbol, '') + ''', ''' + IsNull(@rateErrorMessage, '') + ''', ''' + convert(varchar, getDate(), 121) + ''')'
			 	exec (@sql)
			end

			/* leg one - toCurrencyCode = triangulationCurrencyCode */
			if @foundExchangeRate = 0
			begin
				Select @rateErrorMessage = Null

				Execute @foundExchangeRate = GetExchangeRate @fromCurrencyCode, @triangulationCurrencyCode, @targetDate, @rate output, 0
				
				if @foundExchangeRate = 0
				begin
					/* look for an inverse exchange rate */
					Execute @foundExchangeRate = GetExchangeRate @triangulationCurrencyCode, @fromCurrencyCode, @targetDate, @rate output, 1
				end

				if @foundExchangeRate = 0
				 	Select @rateErrorMessage = 'Exchange rate not found from ' + @fromCurrencyCode + ' to ' + @presentationCurrencyCode

				/* leg two - fromCurrencyCode - triangulationCurrencyCode and toCurrencyCode = presentationCurrencyCode */
				Execute @foundExchangeRate = GetExchangeRate @triangulationCurrencyCode, @presentationCurrencyCode, @targetDate, @rate2 output, 0
				
				if @foundExchangeRate = 0
				begin
					/* look for an inverse exchange rate */
					Execute @foundExchangeRate = GetExchangeRate @presentationCurrencyCode, @triangulationCurrencyCode, @targetDate, @rate2 output, 1
				end

				if @foundExchangeRate = 0
					Select @rateErrorMessage = 'Exchange rate not found from ' + @fromCurrencyCode + ' to ' + @presentationCurrencyCode
			
				Select @triangulationRate = @rate * @rate2
					
				select @sql = 'Insert Into ' + @workTable + ' (SessionID, Company, FromCurrencyCode, PresentationCurrencyCode, Rate, PresentationCurrencyDecimalPlaces, PresentationCurrencyCurrencySymbol, Message, LastRun) values'
			 	select @sql = @sql + '(''' + @sessionID + ''', ''' + @company + ''', ''' + @fromCurrencyCode + ''', ''' + @presentationCurrencyCode + ''', ''' + convert(varchar,cast(@triangulationRate as decimal(19,10))) + ''', ''' + convert(varchar,@presentationCurrencyDecimalPlaces) + ''', ''' + IsNull(@presentationCurrencyCurrencySymbol, '') + ''', ''' + IsNull(@rateErrorMessage, '') + ''', ''' + convert(varchar, getDate(), 121) + ''')'
			 	exec (@sql)
			end
		end
	end

	end --If @fromCurrencyCode <> @presentationCurrencyCode
	 
	Select @lastCompany = @company
	FETCH NEXT from enabledCurrencies Into @company, @fromCurrencyCode, @triangulationCurrencyCode
	Select @enabledCurrenciesEOF = @@FETCH_STATUS


END
	
	Close enabledCurrencies
	Deallocate enabledCurrencies
	
END -- CreateReportPresentationCurrencyTable
GO
