SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectTeamClear]
AS
/*
Copyright 2019 Central Consulting Group.   All rights reserved.
11/06/2019	David Springer
			Delete employees from non-Regular projects.
			Delete employees from Regular projects where there are no labor hours.
11/19/2019	David Springer
			Change labor hours to > 4, instead of 0 for the project level
12/09/2019	David Springer
			Added employee type vendors cleared from vendor grid
04/09/2020	David Springer
            Revised to remove teminated employees
03/30/2021	Craig H. Anderson
			Refactored to now scan LD table for every ProjectEmpAssoc record. Instead, now we make a temp table to be scanned that is already summarized from LD
10/06/2021	David Springer
			Removed Charge Type <> R for employee vendors
*/
BEGIN
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--	Delete terminated employees from all projects
	Delete a
	From EMProjectAssoc a, EM e
	Where a.Employee = e.Employee
        AND e.Status = 'T'

--	Delete employees from non-regular projects
	Delete a
	From PR p, EMProjectAssoc a
	Where p.WBS1 = a.WBS1
		and p.WBS2 = a.WBS2
		and p.WBS3 = a.WBS3
		and p.ChargeType <> 'R'

--	Delete duplicate employees where one has a role and the other doesn't
	Delete b
	From EMProjectAssoc a, EMProjectAssoc b
	Where a.WBS1 = b.WBS1
		and a.WBS2 = b.WBS2
		and a.WBS3 = b.WBS3
		and a.Employee = b.Employee
		and a.Role <> ''
		and b.Role = ''

--	Delete duplicate employees where role are the same
	Delete b
	From EMProjectAssoc a, EMProjectAssoc b
	Where a.WBS1 = b.WBS1
		and a.WBS2 = b.WBS2
		and a.WBS3 = b.WBS3
		and a.Employee = b.Employee
		and a.Role = b.Role
		and a.RecordID < b.RecordID

--	Delete duplicate employees where role are different
	Delete b
	From EMProjectAssoc a, EMProjectAssoc b
	Where a.WBS1 = b.WBS1
		and a.WBS2 = b.WBS2
		and a.WBS3 = b.WBS3
		and a.Employee = b.Employee
		and a.Role > b.Role
		and a.RecordID <> b.RecordID

	SELECT WBS1
     , Employee
	INTO #myLD1
	FROM dbo.LD WITH (NOLOCK)
	GROUP BY
		WBS1
	  , Employee
	HAVING SUM(RegHrs + OvtHrs) > 4;

	DELETE a
	FROM dbo.PR p
	   , dbo.EMProjectAssoc a
	WHERE
		p.WBS1 = a.WBS1
		AND p.WBS2 = a.WBS2
		AND p.WBS3 = a.WBS3
		AND p.ChargeType = 'R'
		AND p.WBS2 = ' ' -- project level
		AND NOT EXISTS
		(
			SELECT 'x' FROM #myLD1 l WHERE l.Employee = a.Employee AND l.WBS1 = p.WBS1
		);


--	Delete employees from regular projects at WBS2 level
	Select WBS1, WBS2, Employee
	INTO #myLD2
	From LD  WITH (NOLOCK)
	Group by WBS1, WBS2, Employee 
	Having SUM (RegHrs + OvtHrs) > 0

	Delete a
	From PR p, EMProjectAssoc a
	Where p.WBS1 = a.WBS1
		and p.WBS2 = a.WBS2
		and p.WBS3 = a.WBS3
		and p.ChargeType = 'R'
		and p.SubLevel = 'N'
		and p.WBS2 <> ' '  -- WBS2 level
		and p.WBS3 = ' ' 
		and not exists (Select 'x' 
						From #myLD2 l
						Where l.Employee = a.Employee
						and l.WBS1 = p.WBS1
						and l.WBS2 = p.WBS2
						)

--	Delete employees from regular projects at WBS3 level
	Select WBS1, WBS2, WBS3, Employee 
	INTO #myLD3
	From LD 
	Group by WBS1, WBS2, WBS3, Employee 
	Having SUM (RegHrs + OvtHrs) > 0

	Delete a
	From PR p, EMProjectAssoc a
	Where p.WBS1 = a.WBS1
		and p.WBS2 = a.WBS2
		and p.WBS3 = a.WBS3
		and p.ChargeType = 'R'
		and p.SubLevel = 'N'
		and p.WBS3 <> ' ' -- WBS3 level
		and not exists (Select 'x' 
						From #myLD3  l
						Where Employee = a.Employee
						and l.WBS1 = p.WBS1
						and l.WBS2 = p.WBS2
						and l.WBS3 = p.WBS3
						Group by WBS1, WBS2, WBS3, Employee 
						)

--	Delete employee vendors from non-regular projects
	Delete a
	From PR p, ClendorProjectAssoc a, Clendor c
	Where p.WBS1 = a.WBS1
		and p.WBS2 = a.WBS2
		and p.WBS3 = a.WBS3
		and a.ClientID = c.ClientID
		and c.Category = 'E'

END
GO
