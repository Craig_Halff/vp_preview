SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_CustomColumn_Load]
	@VISION_LANGUAGE varchar(10) = NULL
AS
BEGIN
	SET NOCOUNT ON;

    SELECT T1.Seq, IsNull(IsNull(T2.[Label], T3.[Label]), IsNull(T1.[Label], '')) as Label, DataType, DisplayOrder, InitialValue,
            DatabaseField, ColumnUpdate, Width,
            Decimals, ViewRoles, EditRoles, RequiredRoles, RequiredExpression, Link, EnabledWBS1, EnabledWBS2, EnabledWBS3, Status, ModDate, ModUser,
            IsNull(IsNull(T2.[Description], T3.[Description]), IsNull(T1.[Description], '')) as Description, EditOptions, DbColumnName
        FROM CCG_PAT_ConfigCustomColumns T1
            LEFT JOIN CCG_PAT_ConfigCustomColumnsDescriptions T2 on T2.Seq = T1.Seq and T2.UICultureName = @VISION_LANGUAGE
            LEFT JOIN CCG_PAT_ConfigCustomColumnsDescriptions T3 on T3.Seq = T1.Seq and T3.UICultureName = 'en-US'
        ORDER BY DisplayOrder;
END;
GO
