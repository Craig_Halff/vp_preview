SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_DelegationNotifications] ( @Employee nvarchar(20), @RepeatInterval int, @Subject nvarchar(120), @Body nvarchar(1024), @OverrideSenderEmail nvarchar(100), @ShowDaysOutstanding char(1)= 'Y', @MsgPriority nvarchar(10)= NULL, @CCEmployee nvarchar(100)= NULL, @MessageDelay int= 0)
             AS EXEC spCCG_PAT_DelegationNotifications @Employee,@RepeatInterval,@Subject,@Body,@OverrideSenderEmail,@ShowDaysOutstanding,@MsgPriority,@CCEmployee,@MessageDelay
GO
