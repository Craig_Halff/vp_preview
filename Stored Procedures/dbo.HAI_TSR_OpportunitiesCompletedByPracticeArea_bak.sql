SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE   PROCEDURE [dbo].[HAI_TSR_OpportunitiesCompletedByPracticeArea_bak]
	@CompanyId VARCHAR(2),
	@PracticeArea VARCHAR(75),
	@MinPeriod INT,
	@Period INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SET TRANSACTION isolation level READ uncommitted; 

IF EXISTS (SELECT NAME 
           FROM   sysobjects 
           WHERE  NAME = 'setContextInfo' 
                  AND type = 'P') 
  EXECUTE dbo.Setcontextinfo 
    @StrCompany = @CompanyId, 
    @StrAuditingEnabled = 'N', 
    @strUserName = 'HIT', 
    @StrCultureName = 'en-US'; 

SELECT Isnull(Substring(opportunity.org, 7, 3), '')                           AS		group1, 
       groupCFGOrgCodes3.label 
       AS groupDesc1, 
       opportunitycustomtabfields.custfeetypes                                AS        UDCol_CustFeeTypes, 
       Isnull(opportunitycustomtabfields.custreimbexpensemultiplier, 0.00)    AS        UDCol_CustReimbExpenseMultiplier, 
       Isnull(opportunitycustomtabfields.custreimbconsultantmultiplier, 0.00) AS        UDCol_CustReimbConsultantMultiplier, 
       Isnull(opportunitycustomtabfields.custlaborovhprofitmultiplier, 0.00)  AS        UDCol_CustLaborOvhProfitMultiplier, 
       Isnull(opportunitycustomtabfields.custlaborfringemultiplier, 0.00)     AS        UDCol_CustLaborFringeMultiplier, 
       opportunity.NAME oppname, 
       opportunity.opportunity oppnumber, 
       opportunity.prwbs1                                                  AS        prwbs1, 
       OppPR.NAME                                                          AS        prname, 
       opportunity.prproposalwbs1                                          AS        prproposalwbs1, 
       OppPRprop.NAME                                                      AS        prproposalname, 
       opportunity.planid                                                  AS        planid, 
       OppRPPlan.planname                                                  AS        planname, 
       OppRPPlan.plannumber                                                AS        plannumber, 
       CASE WHEN fw_cfgsystem.displaycostpointorg = 'Y' THEN organization.cporgid ELSE opportunity.org END  org, 
       organization.NAME orgname, 
       cfgorganizationstatus.label                                          AS        OrganizationStatus, 
       cl.NAME clname, 
       Isnull(contacts.lastname, '' )  lastname, 
       Isnull(contacts.firstname, '')  firstname, 
       Isnull(contacts.title, '') title, 
       Isnull(contacts.phone, '') bizphone, 
       Isnull(contacts.cellphone, '') cellphone, 
       Isnull(contacts.fax, '') fax, 
       Isnull(contacts.pager, '') pager, 
       Isnull(contacts.email, '') email, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN  
         opportunity.revenue 
         ELSE 0 
       END 
       revenue, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN  
         opportunity.probability  
         ELSE 0 
       END 
       probability, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN  
         opportunity.weightedrevenue  
         ELSE 0 
       END 
       weightedrevenue, 
       opportunity.opendate opendate, 
       opportunity.closedate closedate, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            ( Isnull(Datediff(d, Isnull(opportunity.opendate, opportunity.closedate), 
                  Isnull(opportunity.closedate, Getdate())), 0) ) 
         ELSE 0 
       END 
       daysopen, 
       cfgopportunitystage.description stage, 
       cfgopportunitytype.description  opptype, 
       cfgopportunitysource.description source, 
       cfgopportunitystatus.description oppstatus, 
       opportunity.eststartdate eststartdate, 
       opportunity.estcompletiondate estcompletiondate, 
       opportunity.opportunityid opportunityid, 
       opportunity.clientid clientid, 
       opportunity.contactid contactid, 
       opportunity.servprocode                                                AS        ServiceProfileCode, 
       cfgserviceprofile.description                                          AS        ServiceProfileDesc, 
       opportunity.awardtype                                                  AS        AwardTypeCode, 
       cfgawardtype.description                                               AS        AwardTypeDesc, 
       opportunity.competitiontype                                            AS        CompetitionTypeCode, 
       cfgcompetitiontype.description                                         AS        CompetitionTypeDesc, 
       opportunity.contracttypegovcon                                         AS        ContractTypeGovConCode, 
       cfgcontracttypegovcon.description                                      AS        ContractTypeGovConDesc, 
       opportunity.ourrole                                                    AS        OurRoleCode, 
       cfgprresponsibility.description                                        AS        OurRoleDesc, 
       opportunity.solicitation                                               AS        Solicitation, 
       opportunity.naics                                                      AS        NAICSCode, 
       cfgnaics.description                                                   AS        NAICSDesc, 
       opportunity.address1 address1, 
       opportunity.address2 address2, 
       opportunity.address3 address3, 
       opportunity.city city, 
       opportunity.state state, 
       opportunity.zip zip, 
       opportunity.county county, 
       CFGCountryMain.country country, 
       opportunity.duration duration, 
       cfgmastercontract.description                                          AS        MasterContract, 
       opportunity.createuser createuser, 
       Dateadd("hh", -6, opportunity.createdate) createdate, 
       opportunity.moduser moduser, 
       Dateadd("hh", -6, opportunity.moddate) moddate, 
       opportunity.projmgr projmgr, 
       opportunity.principal principal, 
       opportunity.supervisor supervisor, 
       em_projmgr.lastname + Isnull(', '+em_projmgr.firstname, '') projmgrname, 
       em_principal.lastname + Isnull(', '+em_principal.firstname, '') principalname, 
       em_supervisor.lastname + Isnull(', '+em_supervisor.firstname, '') supervisorname, 
       opppr.principal prprincipalname, 
       opppr.projmgr prprojmgrname, 
       opppr.supervisor prsupervisorname, 
       opppr.responsibility responsibility, 
       opppr.projectype projecttype, 
       opppr.startdate prstartdate, 
       opppr.estcompletiondate prestcompletiondate, 
       opppr.actcompletiondate practcompletiondate, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(opppr.totalprojectcost, 0) 
         ELSE 0 
       END totalprojectcost, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(opppr.firmcost, 0) 
         ELSE 0 
       END firmcost, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(opppr.revjtd, 0) 
         ELSE 0 
       END revjtd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(opppr.revytd, 0) 
         ELSE 0 
       END revytd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(opppr.dirjtd, 0) 
         ELSE 0 
       END dirjtd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(opppr.dirytd, 0) 
         ELSE 0 
       END dirytd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(opppr.otherexpjtd, 0) 
         ELSE 0 
       END otherexpjtd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(opppr.otherexpytd, 0) 
         ELSE 0 
       END otherexpytd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(opppr.laborjtd, 0) 
         ELSE 0 
       END laborjtd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(opppr.laborytd, 0) 
         ELSE 0 
       END laborytd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(opppr.hrsjtd, 0) 
         ELSE 0 
       END hrsjtd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(opppr.hrsytd, 0) 
         ELSE 0 
       END hrsytd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(opppr.billjtd, 0) 
         ELSE 0 
       END billjtd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(opppr.billytd, 0) 
         ELSE 0 
       END billytd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(opppr.arjtd, 0) 
         ELSE 0 
       END arjtd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(opppr.arytd, 0) 
         ELSE 0 
       END arytd, 
       Isnull(opppr.projcurrencycount, 0) 
       prprojcurrencycount, 
       oppprprop.principal prpropprincipalname, 
       oppprprop.projmgr prpropprojmgrname, 
       oppprprop.supervisor prpropsupervisorname, 
       oppprprop.responsibility prpropresponsibility, 
       oppprprop.projectype prpropprojecttype, 
       oppprprop.startdate prpropstartdate, 
       oppprprop.estcompletiondate prpropestcompletiondate, 
       oppprprop.actcompletiondate prpropactcompletiondate, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
			ISNULL(oppprprop.totalprojectcost, 0) 
         ELSE 0 
       END 
       prproptotalprojectcost, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
			ISNULL(oppprprop.firmcost, 0) 
         ELSE 0 
       END prpropfirmcost, 
       0.00 prpropdirjtd, 
       0.00 prpropdirytd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
			ISNULL(oppprprop.otherexpjtd, 0) 
         ELSE 0 
       END prpropotherexpjtd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
			ISNULL(oppprprop.otherexpytd, 0) 
         ELSE 0 
       END prpropotherexpytd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
			ISNULL(oppprprop.laborjtd, 0) 
         ELSE 0 
       END prproplaborjtd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
			ISNULL(oppprprop.laborytd, 0) 
         ELSE 0 
       END prproplaborytd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(oppprprop.hrsjtd, 0) 
         ELSE 0 
       END prprophrsjtd, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY opportunity.opportunityid 
                    ORDER BY opportunity.opportunityid) ) = 1 THEN 
            Isnull(oppprprop.hrsytd, 0) 
         ELSE 0 
       END prprophrsytd, 
       Isnull(oppprprop.projcurrencycount, 0) 
       prpropcurrencycount, 
       CLOwner.client														AS        OwnerNumber, 
       CLOwner.NAME															AS        OwnerName, 
       CLAddressOwner.address												AS        OwnerAddress, 
       CLAddressOwner.address1												AS        OwnerAddress1, 
       CLAddressOwner.address2												AS        OwnerAddress2, 
       CLAddressOwner.address3												AS        OwnerAddress3, 
       CLAddressOwner.address4												AS        OwnerAddress4, 
       CLAddressOwner.city													AS        OwnerCity, 
       CLAddressOwner.state													AS        OwnerState, 
       CLAddressOwner.zip													AS        OwnerZip, 
       fw_cfgcountry.country												AS        OwnerCountry, 
       ''                                                                     AS        currencyCode, 
       0                                                                      AS        CurrencyCodeCount, 
       0                                                                      AS        decimalPlaces, 
       ''                                                                     AS        currencySymbol, 
       ''                                                                     AS        currencycodecustom, 
       0                                                                      AS        CurrencyCodecustomCount, 
       0                                                                      AS        decimalplacescustom, 
       '' 
       currencysymbolcustom, 
       '' 
       currencycodeproject, 
       0                                                                      AS        CurrencyCodeprojectCount, 
       0 
       decimalplacesproject, 
       ''                                                                     AS        currencysymbolproject, 
       '' 
       currencycodepromoproject, 
       0                                                                      AS        CurrencyCodepromoprojectCount, 
       0 
       decimalplacespromoproject, 
       ''                                                                     AS        currencysymbolpromoproject, 
       ''                                                                     AS        currencyCodePres, 
       0                                                                      AS        CurrencyCodePresCount, 
       0                                                                      AS        decimalPlacesPres, 
       ''                                                                     AS        currencySymbolPres, 
       ''                                                                     AS        currencyCodeProj, 
       0                                                                      AS        CurrencyCodeProjCount, 
       0                                                                      AS        decimalPlacesProj, 
       ''                                                                     AS        currencySymbolProj, 
       opportunity.publicnoticedate, 
       cfgmaindata.country                                                    AS        CompanyCountry, 
       CASE 
         WHEN fw_cfgcountry.displaystate IS NULL THEN 'C' 
         ELSE fw_cfgcountry.displaystate 
       END                                                                    AS        Owner_DisplayState, 
       CASE 
         WHEN fw_cfgcountry.addressformat IS NULL THEN 
		 ( CASE 
            WHEN cfgmaindata.defaultaddressformat IS NULL THEN '1' 
            ELSE cfgmaindata.defaultaddressformat END ) 
         ELSE fw_cfgcountry.addressformat 
       END																		AS        Owner_AddressFormat, 
       cfgstates.description													AS        Owner_StateDesc, 
       ''																		AS        dateOptionColumn,/***Hit Rate Columns***/ 
       opportunity.description  
       oppdesc, 
       Isnull( cfgopportunityclosedreason.description, '')						AS        ClosedReason, 
       Isnull( opportunity.closednotes , '')									AS        ClosedNotes 
FROM   fw_cfgsystem, 
       opportunity /***pcCustomCurrencyTables***/ 
       LEFT JOIN opportunitycustomtabfields 
              ON opportunity.opportunityid = opportunitycustomtabfields.opportunityid 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes3 
              ON Substring(opportunity.org, 7, 3) = groupCFGOrgCodes3.code 
                 AND groupCFGOrgCodes3.orglevel = 3 
       LEFT JOIN (organization 
            LEFT JOIN cfgorganizationstatus 
				ON organization.status = cfgorganizationstatus.status) 
              ON opportunity.org = organization.org 
       LEFT JOIN cl 
              ON opportunity.clientid = cl.clientid 
       LEFT JOIN contacts 
              ON opportunity.contactid = contacts.contactid 
       LEFT JOIN cfgmastercontract 
              ON cfgmastercontract.code = opportunity.mastercontract 
       LEFT JOIN cfgopportunitystage 
              ON opportunity.stage = cfgopportunitystage.code 
       LEFT JOIN cfgopportunitytype 
              ON opportunity.opportunitytype = cfgopportunitytype.code 
       LEFT JOIN cfgopportunitysource 
              ON opportunity.source = cfgopportunitysource.code 
       LEFT JOIN cfgopportunitystatus 
              ON opportunity.status = cfgopportunitystatus.code 
       LEFT JOIN cfgopportunityclosedreason 
              ON opportunity.closedreason = cfgopportunityclosedreason.code 
       LEFT JOIN em em_projmgr 
              ON opportunity.projmgr = em_projmgr.employee 
       LEFT JOIN em em_principal 
              ON opportunity.principal = em_principal.employee 
       LEFT JOIN em em_supervisor 
              ON opportunity.supervisor = em_supervisor.employee 
       LEFT JOIN rpplan OppRPPlan 
              ON opportunity.planid = OppRPPlan.planid 
       LEFT JOIN cfgclienttype HitRateCFGClientType 
              ON cl.type = HitRateCFGClientType.code 
       LEFT JOIN opportunityclientassoc HitRateOpportunityClientAssoc 
              ON opportunity.opportunityid = HitRateOpportunityClientAssoc.opportunityid AND HitRateOpportunityClientAssoc.role = 'sysOwner' 
       LEFT JOIN cl clowner 
              ON clowner.clientid = HitRateOpportunityClientAssoc.clientid 
       LEFT JOIN claddress CLAddressOwner 
              ON CLowner.clientid = CLAddressOwner.clientid AND CLAddressOwner.primaryind = 'Y' 
       LEFT JOIN cfgmaindata 
              ON cfgmaindata.company = @CompanyId 
       LEFT JOIN cfgserviceprofile 
              ON cfgserviceprofile.code = opportunity.servprocode 
       LEFT JOIN cfgcompetitiontype 
              ON cfgcompetitiontype.code = opportunity.competitiontype 
       LEFT JOIN cfgcontracttypegovcon 
              ON cfgcontracttypegovcon.code = opportunity.contracttypegovcon 
       LEFT JOIN cfgawardtype 
              ON cfgawardtype.code = opportunity.awardtype 
       LEFT JOIN cfgprresponsibility 
              ON cfgprresponsibility.code = opportunity.ourrole 
       LEFT JOIN cfgnaics 
              ON cfgnaics.code = opportunity.naics 
       LEFT JOIN fw_cfgcountry AS CFGCountryMain 
              ON CFGCountryMain.isocountrycode = opportunity.country 
       LEFT JOIN fw_cfgcountry 
              ON fw_cfgcountry.isocountrycode = CLAddressOwner.country 
       LEFT JOIN cfgstates 
              ON cfgstates.isocountrycode = CLAddressOwner.country 
                 AND cfgstates.code = CLAddressOwner.state 
       LEFT JOIN (
			SELECT  opportunity.opportunityid, 
					pr.wbs1, 
					'' 
					wbs2, 
					'' 
					wbs3, 
					Max(pr.NAME) 
					NAME, 
					Max(pr.projectcurrencycode) 
					projectcurrencycode, 
                    Count(DISTINCT CASE pr.projectcurrencycode 
                        WHEN '' THEN NULL 
                        ELSE pr.projectcurrencycode 
                    END) projcurrencycount, 
                    Max(em_prin.lastname + Isnull(', '+em_prin.firstname, '')) principal, 
                    Max(em_prjmgr.lastname + Isnull(', '+em_prjmgr.firstname, '')) projmgr, 
                    Max(em_sup.lastname + Isnull(', '+em_sup.firstname, '')) supervisor, 
                    Max(Isnull(cfgprresponsibility.description, '')) 
					responsibility, 
                    Max(Isnull(cfgprojecttype.description, ''))  projectype, 
                    Max(pr.startdate) startdate, 
                    Max(pr.estcompletiondate) 
					estcompletiondate, 
                    Max(pr.actcompletiondate) 
					actcompletiondate, 
                    Max(pr.totalprojectcost) AS totalprojectcost, 
                    Max(pr.firmcost) AS firmcost, 
                    0 revjtd, 
                    0 revytd, 
                    0 dirjtd, 
                    0 dirytd, 
                    0 otherexpjtd, 
                    0 otherexpytd, 
                    0 laborjtd, 
					0 laborytd, 
					0 hrsjtd, 
					0 hrsytd, 
					0 billjtd, 
					0 billytd, 
					0 arjtd, 
					0 arytd 
       FROM   pr 
       LEFT JOIN cfgprresponsibility 
              ON pr.responsibility = cfgprresponsibility.code 
       LEFT JOIN cfgprojecttype 
              ON pr.projecttype = cfgprojecttype.code 
       LEFT JOIN em em_sup 
              ON pr.supervisor = em_sup.employee 
       LEFT JOIN em em_prin 
              ON pr.principal = em_prin.employee 
       LEFT JOIN em em_prjmgr 
              ON pr.projmgr = em_prjmgr.employee 
       LEFT JOIN opportunity 
              ON pr.wbs1 = opportunity.prwbs1 
       LEFT JOIN cl 
              ON opportunity.clientid = cl.clientid 
       WHERE  pr.wbs2 = '' 
		AND pr.wbs3 = '' 
		AND (( ( Substring(opportunity.org, 1, 2) = @CompanyId ) 
		AND EXISTS (SELECT 'x' 
                        FROM   opportunitycustomtabfields 
                        INNER JOIN fw_customcolumnvalues CCV 
                        ON CCV.code = opportunitycustomtabfields.custteampracticearea 
       AND CCV.infocenterarea = 'Opportunities' 
       AND CCV.colname = 'CustTeamPracticeArea' 
       WHERE  opportunity.opportunityid = opportunitycustomtabfields.opportunityid 
       AND ( CCV.datavalue = @practiceArea )) 
       AND EXISTS (SELECT 'x' 
			   FROM   cfgopportunitystatus 
			   WHERE  cfgopportunitystatus.code = opportunity.status 
			   AND (( cfgopportunitystatus.description = 'Inactive' 
		   ) 
       )) 
       AND ( Datediff(dd, opportunity.closedate, Getdate()) BETWEEN 0 AND 90 ) 
       AND ( EXISTS (SELECT 'x' 
			   FROM   pr 
			   INNER JOIN opportunity O 
			   ON pr.wbs1 = O.prwbs1 
			   WHERE  pr.wbs2 = ' ' 
			   AND pr.wbs3 = ' ' 
			   AND O.opportunityid = opportunity.opportunityid 
			   AND (( pr.wbs1 IS NOT NULL 
			   AND pr.wbs1 <> ' ' ))) 
       OR EXISTS (SELECT 'x' 
			   FROM   cfgopportunitystage 
			   WHERE  cfgopportunitystage.code = opportunity.stage 
			   AND ((( ( cfgopportunitystage.description = 'Lost') 
				   OR ( cfgopportunitystage.description = 'Cancelled') 
				   OR ( cfgopportunitystage.description = 'No Go'))) 
		)) 
		) )) 
		GROUP  BY opportunity.opportunityid, pr.wbs1) OppPR 
				ON opportunity.opportunityid = opppr.opportunityid 
                 AND opportunity.prwbs1 = OppPR.wbs1 
                 AND OppPR.wbs2 = '' 
                 AND OppPR.wbs3 = ''
       LEFT JOIN (SELECT opportunity.opportunityid, 
					pr.wbs1, 
                    '' wbs2, 
                    '' wbs3, 
                    Max(pr.NAME) 
					NAME, 
                    Max(pr.projectcurrencycode) 
					projectcurrencycode, 
                    Count(DISTINCT CASE pr.projectcurrencycode 
                        WHEN '' THEN NULL 
                        ELSE pr.projectcurrencycode 
                    END) projcurrencycount, 
                    Max(em_prin.lastname  + Isnull(', '+em_prin.firstname, '')) principal, 
                    Max(em_prjmgr.lastname + Isnull(', '+em_prjmgr.firstname, '')) projmgr, 
                    Max(em_sup.lastname + Isnull(', '+em_sup.firstname, '')) supervisor, 
                    Max(Isnull(cfgprresponsibility.description, '')) responsibility, 
                    Max(Isnull(cfgprojecttype.description, ''))  projectype, 
                    Max(pr.startdate) startdate, 
                    Max(pr.estcompletiondate) estcompletiondate, 
                    Max(pr.actcompletiondate) actcompletiondate, 
                    Max(pr.totalprojectcost) AS totalprojectcost, 
                    Max(pr.firmcost) AS firmcost, 
                    Sum(pramts.revjtd) revjtd, 
                    Sum(pramts.revytd) revytd, 
                    Sum(pramts.dirjtd) dirjtd, 
                    Sum(pramts.dirytd) dirytd, 
                    Sum(pramts.otherexpjtd) otherexpjtd, 
                    Sum(pramts.otherexpytd) otherexpytd, 
                    Sum(pramts.laborjtd) laborjtd, 
                    Sum(pramts.laborytd) laborytd, 
                    Sum(pramts.hrsjtd) hrsjtd, 
                    Sum(pramts.hrsytd) hrsytd, 
                    Sum(pramts.billjtd) billjtd, 
                    Sum(pramts.billytd) billytd, 
                    Sum(pramts.arjtd) arjtd, 
                    Sum(pramts.arytd) arytd 
                  FROM   pr 
                         LEFT JOIN (/*Expense Detail-ledgerap ed*/ 
							SELECT ed.wbs1, 
                            CASE WHEN ca.type >= 4 AND ca.type < 5 THEN -ed.amount ELSE 0 END revjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN 
							CASE WHEN ca.type >= 4 AND ca.type < 5 THEN -ed.amount ELSE 0 END ELSE 0 END revytd, 
							CASE WHEN ca.type >= 7 AND ca.type < 9 AND ed.projectcost = 'Y' THEN ed.amount ELSE 0 END dirjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN CASE WHEN ca.type >= 7 AND ca.type < 9 AND ed.projectcost = 'Y' THEN ed.amount ELSE 0 END ELSE 0 END dirytd, 
							CASE WHEN ( ( ca.type >= 5 AND ca.type < 7 ) OR ca.type >= 9 ) AND ed.projectcost = 'Y' THEN ed.amount ELSE 0 END otherexpjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN 
							CASE WHEN ( ( ca.type >= 5 AND ca.type < 7 ) OR ca.type >= 9 ) AND ed.projectcost = 'Y' THEN ed.amount ELSE 0 END ELSE 0 END otherexpytd, 
							CASE WHEN autoentry = 'N' AND transtype =  'IN' AND Isnull(subtype, ' ') <> 'I' AND Isnull(subtype, ' ') <> 'R' AND ( ca.type >= 4 OR ed.account IS NULL ) THEN -amount ELSE 0 END AS billjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN 
								CASE 
									WHEN autoentry = 'N' AND transtype = 'IN' AND Isnull(subtype, ' ') <> 'I' AND Isnull(subtype, ' ') <> 'R' AND ( ca.type >= 4 OR ed.account IS NULL ) THEN -amount ELSE 0 END 
								ELSE 0 
							END AS billytd, 
							CASE
								WHEN ed.transtype = 'IN' AND autoentry = 'N' AND Isnull(ed.subtype, ' ') <> 'X' THEN -ed.amount 
								WHEN ed.transtype = 'CR' AND ed.subtype = 'T' AND Isnull(ed.invoice, ' ') <> ' ' THEN -ed.amount 
								WHEN ed.transtype = 'CR' AND ed.subtype = 'R' THEN ed.amount ELSE 0 
							END AS arjtd, 
							CASE 
								WHEN ed.period >= @MinPeriod THEN 
									CASE 
										WHEN ed.transtype =  'IN' AND autoentry = 'N' AND Isnull(ed.subtype, ' ') <> 'X' THEN -ed.amount 
										WHEN ed.transtype =  'CR' AND ed.subtype = 'T' AND Isnull(ed.invoice,  ' ') <> ' ' THEN -ed.amount 
										WHEN ed.transtype = 'CR' AND ed.subtype = 'R' THEN ed.amount ELSE 0 END ELSE 0 
							END AS arytd, 
							0   laborjtd, 
							0   laborytd, 
							0   hrsjtd, 
							0   hrsytd 
							FROM   ledgerap ed 
							LEFT JOIN pr 
							ON ed.wbs1 = pr.wbs1 AND pr.wbs2 = '' AND pr.wbs3 = '' 
							LEFT JOIN ca 
							ON ed.account = ca.account 
							WHERE  ed.wbs1 IS NOT NULL AND ed.period <= @Period 
							UNION ALL 
							/*Expense Detail-ledgerar ed*/ 
							SELECT ed.wbs1, 
							CASE WHEN ca.type >= 4 AND ca.type < 5 THEN -ed.amount ELSE 0 END revjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN CASE WHEN ca.type >= 4 AND ca.type < 5 THEN -ed.amount ELSE 0 END ELSE 0 END revytd, 
							CASE WHEN ca.type >= 7 AND ca.type < 9 AND ed.projectcost = 'Y' THEN ed.amount ELSE 0 END dirjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN CASE WHEN ca.type >= 7 AND ca.type < 9 AND ed.projectcost = 'Y' THEN ed.amount ELSE 0 END ELSE 0 END dirytd, 
							CASE WHEN ( ( ca.type >= 5 AND ca.type < 7 ) OR ca.type >= 9 ) AND ed.projectcost = 'Y' THEN ed.amount ELSE 0 END otherexpjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN CASE WHEN ( ( ca.type >= 5 AND ca.type < 7 ) OR ca.type >= 9 ) AND ed.projectcost = 'Y' THEN ed.amount ELSE 0 END ELSE 0 END otherexpytd, 
							CASE WHEN autoentry = 'N' AND transtype =  'IN' AND Isnull(subtype, ' ') <> 'I' AND Isnull(subtype, ' ') <> 'R' AND ( ca.type >= 4 OR ed.account IS NULL ) THEN -amount ELSE 0 END AS billjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN CASE WHEN autoentry = 'N' AND transtype = 'IN' AND Isnull(subtype, ' ') <> 'I' AND Isnull(subtype, ' ') <> 'R' AND ( ca.type >= 4 OR ed.account IS NULL ) THEN -amount ELSE 0 END ELSE 0 END AS billytd, 
							CASE 
								WHEN ed.transtype = 'IN' AND autoentry = 'N' AND Isnull(ed.subtype, ' ') <> 'X' THEN -ed.amount 
								WHEN ed.transtype = 'CR' AND ed.subtype = 'T' AND Isnull(ed.invoice, ' ') <> ' ' THEN -ed.amount 
								WHEN ed.transtype = 'CR' AND ed.subtype = 'R' THEN ed.amount 
							ELSE 0 
							END AS arjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN 
							CASE 
								WHEN ed.transtype =  'IN' AND autoentry = 'N' AND Isnull(ed.subtype, ' ') <> 'X' THEN -ed.amount 
								WHEN ed.transtype =  'CR' AND ed.subtype = 'T' AND Isnull(ed.invoice,  ' ') <> ' ' THEN -ed.amount 
								WHEN ed.transtype =  'CR' AND ed.subtype = 'R' THEN ed.amount ELSE 0 END 
							ELSE 0 
							END AS arytd, 
							0   laborjtd, 
							0   laborytd, 
							0   hrsjtd, 
							0   hrsytd 
							FROM   ledgerar ed 
							LEFT JOIN pr 
							ON ed.wbs1 = pr.wbs1 AND pr.wbs2 = '' AND pr.wbs3 = '' 
							LEFT JOIN ca 
							ON ed.account = ca.account 
							WHERE  ed.wbs1 IS NOT NULL AND ed.period <= @Period 
							UNION ALL 
							/*Expense Detail-ledgerex ed*/ 
							SELECT ed.wbs1, 
							CASE WHEN ca.type >= 4 AND ca.type < 5 THEN -ed.amount ELSE 0 END revjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN 
							CASE WHEN ca.type >= 4 AND ca.type < 5 THEN -ed.amount ELSE 0 END ELSE 0 END revytd, 
							CASE WHEN ca.type >= 7 AND ca.type < 9 AND ed.projectcost = 'Y' THEN ed.amount ELSE 0 END dirjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN 
								CASE WHEN ca.type >= 7 AND ca.type < 9 AND ed.projectcost = 'Y' THEN ed.amount ELSE 0 END 
								ELSE 0 
							END dirytd, 
							CASE WHEN ( ( ca.type >= 5 AND ca.type < 7 ) OR ca.type >= 9 ) AND ed.projectcost = 'Y' THEN ed.amount ELSE 0 END otherexpjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN CASE WHEN ( ( ca.type >= 5 AND ca.type < 7 ) OR ca.type >= 9 ) AND ed.projectcost = 'Y' THEN ed.amount ELSE 0 END ELSE 0 END otherexpytd, 
							CASE WHEN autoentry = 'N' AND transtype =  'IN' AND Isnull(subtype, ' ') <> 'I' AND Isnull(subtype, ' ') <> 'R' AND ( ca.type >= 4 OR ed.account IS NULL ) THEN -amount ELSE 0 END AS billjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN 
								CASE WHEN autoentry = 'N' AND transtype = 'IN' AND Isnull(subtype, ' ') <> 'I' AND Isnull(subtype, ' ') <> 'R' AND ( ca.type >= 4 OR ed.account IS NULL ) THEN -amount ELSE 0 END ELSE 0 
							END AS billytd, 
							CASE WHEN ed.transtype = 'IN' AND autoentry = 'N' AND Isnull(ed.subtype, ' ') <> 'X' THEN -ed.amount 
								WHEN ed.transtype = 'CR' AND ed.subtype = 'T' AND Isnull(ed.invoice, ' ') <> ' ' THEN -ed.amount 
								WHEN ed.transtype = 'CR' AND ed.subtype = 'R' THEN ed.amount ELSE 0 
							END AS arjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN 
								CASE 
									WHEN ed.transtype =  'IN' AND autoentry = 'N' AND Isnull(ed.subtype, ' ') <> 'X' THEN -ed.amount 
									WHEN ed.transtype =  'CR' AND ed.subtype = 'T' AND Isnull(ed.invoice,  ' ') <> ' ' THEN -ed.amount 
									WHEN ed.transtype =  'CR' AND ed.subtype = 'R' THEN ed.amount 
									ELSE 0 
									END 
								ELSE 0 
							END AS arytd, 
							0   laborjtd, 
							0   laborytd, 
							0   hrsjtd, 
							0   hrsytd 
							FROM   ledgerex ed 
							LEFT JOIN pr 
							    ON ed.wbs1 = pr.wbs1 
							    AND pr.wbs2 = '' 
							    AND pr.wbs3 = '' 
							LEFT JOIN ca 
							    ON ed.account = ca.account 
							WHERE  ed.wbs1 IS NOT NULL 
							    AND ed.period <= @Period 
							UNION ALL 
							/*Expense Detail-ledgermisc ed*/ 
							SELECT ed.wbs1, 
							CASE WHEN ca.type >= 4 AND ca.type < 5 THEN -ed.amount ELSE 0 END revjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN 
								CASE WHEN ca.type >= 4 AND ca.type < 5 THEN -ed.amount ELSE 0 
								END 
								ELSE 0 
							END revytd, 
							CASE WHEN ca.type >= 7 AND ca.type < 9 AND ed.projectcost = 'Y' 
								THEN ed.amount 
								ELSE 0 
							END dirjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN 
								CASE WHEN ca.type >= 7 AND ca.type < 9 AND ed.projectcost = 'Y' THEN ed.amount 
								ELSE 0 
								END 
								ELSE 0 
							END dirytd, 
							CASE WHEN ( ( ca.type >= 5 AND ca.type < 7 ) OR ca.type >= 9 ) AND ed.projectcost = 'Y' 
								THEN ed.amount 
								ELSE 0 
							END otherexpjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN 
								CASE WHEN ( ( ca.type >= 5 AND ca.type < 7 ) OR ca.type >= 9 ) AND ed.projectcost = 'Y' 
									THEN ed.amount 
									ELSE 0 
								END 
								ELSE 0 
							END otherexpytd, 
							CASE WHEN autoentry = 'N' AND transtype =  'IN' AND Isnull(subtype, ' ') <> 'I' AND Isnull(subtype, ' ') <> 'R' AND ( ca.type >= 4 OR ed.account IS NULL ) 
								THEN -amount 
								ELSE 0 
							END AS billjtd, 
							CASE WHEN ed.period >= @MinPeriod THEN 
								CASE WHEN autoentry = 'N' AND transtype = 'IN' AND Isnull(subtype, ' ') <> 'I' AND Isnull(subtype, ' ') <> 'R' AND ( ca.type >= 4 OR ed.account IS NULL ) 
									THEN -amount 
									ELSE 0 
								END 
								ELSE 0 
							END AS billytd, 
							CASE 
								WHEN ed.transtype = 'IN' AND autoentry = 'N' AND Isnull(ed.subtype, ' ') <> 'X' THEN -ed.amount 
								WHEN ed.transtype = 'CR' AND ed.subtype = 'T' AND Isnull(ed.invoice, ' ') <> ' ' THEN -ed.amount 
								WHEN ed.transtype = 'CR' AND ed.subtype = 'R' THEN ed.amount 
								ELSE 0 
							END AS arjtd, 
							CASE 
								WHEN ed.period >= @MinPeriod THEN 
								CASE 
									WHEN ed.transtype =  'IN' AND autoentry = 'N' AND Isnull(ed.subtype, ' ') <> 'X' THEN -ed.amount 
									WHEN ed.transtype =  'CR' AND ed.subtype = 'T' AND Isnull(ed.invoice,  ' ') <> ' ' THEN -ed.amount 
									WHEN ed.transtype =  'CR' AND ed.subtype = 'R' THEN ed.amount 
								ELSE 0 
								END 
								ELSE 0 
							END AS arytd, 
							0   laborjtd, 
							0   laborytd, 
							0   hrsjtd, 
							0   hrsytd 
							FROM   ledgermisc ed 
							LEFT JOIN pr 
							    ON ed.wbs1 = pr.wbs1 AND pr.wbs2 = '' AND pr.wbs3 = '' 
							LEFT JOIN ca 
							    ON ed.account = ca.account 
							WHERE  ed.wbs1 IS NOT NULL AND ed.period <= @Period 
							UNION ALL /*Labor Detail*/ 
							SELECT ld.wbs1, 
							0       revjtd, 
							0       revytd, 
							0       dirjtd, 
							0       dirytd, 
							0       otherexpjtd, 
							0       otherexpytd, 
							0       billjtd, 
							0       billytd, 
							0       arjtd, 
							0       arytd, 
							( Isnull(regamt, 0) + Isnull(ovtamt, 0) + Isnull(specialovtamt, 0) )  AS laborjtd, 
							CASE 
								WHEN ld.period >= @MinPeriod THEN ( Isnull( regamt, 0) + Isnull(ovtamt, 0) + Isnull(specialovtamt, 0) ) 
								ELSE 0 
							END AS laborytd, 
							reghrs + ovthrs + specialovthrs hrsjtd, 
							CASE 
								WHEN ld.period >= @MinPeriod THEN reghrs + ovthrs + specialovthrs 
								ELSE 0 
							END hrsytd 
							FROM   ld 
							LEFT JOIN pr 
							    ON ld.wbs1 = pr.wbs1 AND pr.wbs2 = '' AND pr.wbs3 = '' 
							WHERE  projectcost = 'Y' AND ld.period <= @Period 
							/*Union Query From PRF */ 
							UNION ALL 
							SELECT prf.wbs1, 
							0         revjtd, 
							0         revytd, 
							0         directjtd, 
							0         directytd, 
							0         otherexpspentjtd, 
							0         otherexpspentytd, 
							0         billedjtd, 
							0         billedytd, 
							0         arjtd, 
							0         arytd, 
							prf.regoh AS laborjtd, 
							CASE 
								WHEN prf.period >= @MinPeriod THEN regoh 
								ELSE 0 
							END       AS laborytd, 
							0         hrsjtd, 
							0         hrsytd 
							FROM   prf 
							LEFT JOIN pr 
							ON pr.wbs1 = prf.wbs1 
							AND pr.wbs2 = '' 
							AND pr.wbs3 = '' 
							WHERE  prf.period < @Period) pramts 
							    ON pramts.wbs1 = pr.wbs1 
							LEFT JOIN cfgprresponsibility 
							    ON pr.responsibility = cfgprresponsibility.code 
							LEFT JOIN cfgprojecttype 
							    ON pr.projecttype = cfgprojecttype.code 
							LEFT JOIN em em_sup 
							    ON pr.supervisor = em_sup.employee 
							LEFT JOIN em em_prin 
							    ON pr.principal = em_prin.employee 
							LEFT JOIN em em_prjmgr 
							    ON pr.projmgr = em_prjmgr.employee 
							LEFT JOIN opportunity 
							    ON pr.wbs1 = opportunity.prproposalwbs1 
							LEFT JOIN cl 
							    ON opportunity.clientid = cl.clientid 
							WHERE  pr.wbs2 = '' 
							AND pr.wbs3 = '' 
							AND (( ( Substring(opportunity.org, 1, 2) = @CompanyId ) 
							AND EXISTS (SELECT 'x' 
									FROM   opportunitycustomtabfields 
									INNER JOIN fw_customcolumnvalues CCV 
									ON CCV.code = opportunitycustomtabfields.custteampracticearea 
									AND CCV.infocenterarea = 'Opportunities' 
									AND CCV.colname = 'CustTeamPracticeArea' 
									WHERE  opportunity.opportunityid = 
									opportunitycustomtabfields.opportunityid 
									AND ( CCV.datavalue = @practiceArea )) 
									AND EXISTS (SELECT 'x' 
										FROM   cfgopportunitystatus 
										WHERE  cfgopportunitystatus.code = opportunity.status 
										AND (( cfgopportunitystatus.description = 'Inactive' 
									) )) 
							AND ( Datediff(dd, opportunity.closedate, Getdate()) BETWEEN 0 AND 90 ) 
							AND ( EXISTS (SELECT 'x' 
									FROM   pr 
									INNER JOIN opportunity O 
									ON pr.wbs1 = O.prwbs1 
									WHERE  pr.wbs2 = ' ' 
									AND pr.wbs3 = ' ' 
									AND O.opportunityid = opportunity.opportunityid 
									AND (( pr.wbs1 IS NOT NULL 
									AND pr.wbs1 <> ' ' ))) 
								OR EXISTS (SELECT 'x' 
										FROM   cfgopportunitystage 
										WHERE  cfgopportunitystage.code = opportunity.stage 
										AND ((( ( cfgopportunitystage.description = 'Lost' ) 
								OR ( cfgopportunitystage.description = 'Cancelled' ) 
								OR ( cfgopportunitystage.description = 'No Go' ) )) 
								)) 
							) )) 
							GROUP  BY opportunity.opportunityid, pr.wbs1) OppPRprop 
							    ON opportunity.opportunityid = oppprprop.opportunityid 
							    AND opportunity.prproposalwbs1 = OppPRprop.wbs1 
							    AND OppPRprop.wbs2 = '' 
							    AND OppPRprop.wbs3 = '' /***GRIDFILTERJOIN***/ 
							WHERE  ( '' = '' 
							  OR ( opportunity.opportunityid = '' ) ) /***GRIDFILTERWHERE***/ 
							AND (( ( Substring(opportunity.org, 1, 2) = @CompanyId ) 
									AND EXISTS (SELECT 'x' 
												FROM   opportunitycustomtabfields 
												INNER JOIN fw_customcolumnvalues CCV 
												ON CCV.code = opportunitycustomtabfields.custteampracticearea 
												AND CCV.infocenterarea = 'Opportunities' 
												AND CCV.colname = 'CustTeamPracticeArea' 
							WHERE  opportunity.opportunityid = opportunitycustomtabfields.opportunityid 
							AND ( CCV.datavalue = @practiceArea )) 
							AND EXISTS (SELECT 'x' 
							FROM   cfgopportunitystatus 
							WHERE  cfgopportunitystatus.code = opportunity.status 
							AND (( cfgopportunitystatus.description = 'Inactive' ) )) 
							AND ( Datediff(dd, opportunity.closedate, Getdate()) BETWEEN 0 AND 90 ) 
							AND ( EXISTS (SELECT 'x' 
							FROM   pr 
							INNER JOIN opportunity O 
							ON pr.wbs1 = O.prwbs1 
							WHERE  pr.wbs2 = ' ' 
							AND pr.wbs3 = ' ' 
							AND O.opportunityid = opportunity.opportunityid 
							AND (( pr.wbs1 IS NOT NULL AND pr.wbs1 <> ' ' ))) 
							OR EXISTS (SELECT 'x' 
							FROM   cfgopportunitystage 
							WHERE  cfgopportunitystage.code = opportunity.stage 
							AND ((( ( cfgopportunitystage.description = 'Lost' ) 
								OR ( cfgopportunitystage.description = 'Cancelled' ) 
								OR ( cfgopportunitystage.description = 'No Go' ) )) 
							)) ) )) /***CL WHERE CLAUSE***/ /***Custom Currency WHERE CLAUSE***/ 
	                        ORDER  BY group1, opportunity.NAME + opportunity.opportunityid

END
GO
