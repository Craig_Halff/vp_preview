SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Save_Invoice_Groups]
	@fieldsSQL			Nvarchar(500),
	@newValuesSql		Nvarchar(max),
	@visionMajorVersion	int,
	@VISION_LANGUAGE	varchar(10)
AS
BEGIN
	/*
	DeltekStoredProc_CCG_EI_Config_Save_Invoice_Groups
		@fieldsSQL = 'InvoiceGroup, CascadeMenu, ThruPeriod, ThruDate',
		@newValuesSql = '(''Amy'', '''', 22, ''12/23/2015''), (''Bob'', '''', null, null), (''Laura'', ''Laura'', null, null), (''Laura Specials'', ''Laura'', null, null), (''Sue Week 1'', ''Sue'', null, null), (''Sue Week 2'', ''Sue'', null, null), (''Sue Week 3'', ''Sue'', 22, ''01/14/2016''), (''Sue Week 4'', ''Sue'', null, null), (''Terri'', '''', 25, ''03/18/2016'')',
		@visionMajorVersion = '7',
		@VISION_LANGUAGE = 'en-US'
 	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	int;			-- INT value indicates which parameter failed the security check

	-- Ensure that our inputs are safe (preventing SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<INSERT VALUES>', @newValuesSql);
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<CSV>', @fieldsSQL) * 2;
	IF @safeSql < 3 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS varchar(4));
		RETURN;
	END;

	BEGIN TRANSACTION;

	SET @sSQL = N'
		MERGE CCG_EI_ConfigInvoiceGroups WITH (HOLDLOCK) AS Target
		USING (VALUES ' + @newValuesSql + N'
			) As Source (' + @fieldsSQL + N')
			ON Target.[InvoiceGroup] = Source.[InvoiceGroup]
		WHEN MATCHED THEN
			UPDATE SET
				[ThruPeriod]	= Source.[ThruPeriod],
				[ThruDate]		= Source.[ThruDate],
				[CascadeMenu]	= Source.[CascadeMenu]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (' + @fieldsSQL + N')
			VALUES (Source.' + REPLACE(@fieldsSQL, N', ', N', Source.') + N')
		WHEN NOT MATCHED BY SOURCE THEN DELETE;';
	--PRINT (@sSQL);
	EXEC (@sSQL);

	DELETE CCG_EI_ConfigInvoiceGroups
        FROM CCG_EI_ConfigInvoiceGroups cig
        WHERE cig.InvoiceGroup not in (
		    SELECT ccvd.Code FROM FW_CustomColumnValuesData ccvd
		    WHERE GridID = (CASE WHEN @visionMajorVersion >= 7 THEN 'X' ELSE '[FORM]' END)
				AND UICultureName = @VISION_LANGUAGE
                AND InfoCenterArea = 'Projects'
				AND ColName = 'custInvoiceGroup'
	    );

	COMMIT TRANSACTION;
END;
GO
