SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_CreateVoucher] ( @Company nvarchar(14), @UsePeriod int, @UseBatch nvarchar(32), @CSVPayableSeqs varchar(max), @Employee nvarchar(20), @User nvarchar(32), @VoucherDate datetime, @DefaultProjAmtMsg nvarchar(max))
             AS EXEC spCCG_PAT_CreateVoucher @Company,@UsePeriod,@UseBatch,@CSVPayableSeqs,@Employee,@User,@VoucherDate,@DefaultProjAmtMsg
GO
