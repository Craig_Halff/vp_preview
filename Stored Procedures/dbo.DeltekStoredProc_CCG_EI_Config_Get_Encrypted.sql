SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Get_Encrypted]
	@username	Nvarchar(500) = null,
	@encTimeout	int = null
AS
BEGIN
	-- DeltekStoredProc_CCG_EI_Config_Get_Encrypted @username = 'ADMIN'
	-- DeltekStoredProc_CCG_EI_Config_Get_Encrypted @encTimeout = '8'
	SET NOCOUNT ON;

	IF ISNULL(@username,'') <> ''
		SELECT dbo.fnCCG_EI_GetE(@username) as userenc;
	ELSE IF ISNULL(@encTimeout,'') <> ''
		SELECT dbo.fnCCG_EI_GetE(Convert(varchar(20), DATEADD(d, @encTimeout, getdate()), 101)) as timeoutenc;
END;
GO
