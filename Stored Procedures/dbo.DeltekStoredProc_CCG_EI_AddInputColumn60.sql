SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_AddInputColumn60] ( @Header varchar(255), @FieldName varchar(255), @FieldType varchar(255), @NullString varchar(20), @ColType char(1), @ColSeq int, @ColDecimals int, @ColWidth int= 0, @ColEnabledWBS1 char(1)='Y', @ColEnabledWBS2 char(1)='Y', @ColEnabledWBS3 char(1)='Y', @ColStatus char(1)='Y', @ColViewRoles varchar(255)='', @ColEditRoles varchar(255)='', @ColRequiredRoles varchar(255)='', @ColRequiredExpression varchar(255)='')
             AS EXEC spCCG_EI_AddInputColumn60 @Header,@FieldName,@FieldType,@NullString,@ColType,@ColSeq,@ColDecimals,@ColWidth,@ColEnabledWBS1,@ColEnabledWBS2,@ColEnabledWBS3,@ColStatus,@ColViewRoles,@ColEditRoles,@ColRequiredRoles,@ColRequiredExpression
GO
