SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_GetTransDocsFilelist] ( @Username nvarchar(32), @CorE char(1), @Invoice nvarchar(12), @WBS1 nvarchar(30), @FW_Files int= 1)
             AS EXEC spCCG_EI_GetTransDocsFilelist @Username,@CorE,@Invoice,@WBS1,@FW_Files
GO
