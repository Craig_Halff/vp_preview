SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPCreatePlanFromProj]
  @strWBS1 nvarchar(30),
  @strUserName nvarchar(32),
  @strPlanID varchar(32) OUTPUT,
  @autoCommit varchar(1) = 'Y'
AS

BEGIN -- Procedure stRPCreatePlanFromProj

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- The purpose of this stored procedure is to create and publish 1.0 Plans from a given Project.
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

  DECLARE @intPlanCount int

  -- If the given Project already have an associated Plan, then exit this stored procedure.

  SET @intPlanCount = 
    CASE WHEN EXISTS 
      (SELECT P.PlanID FROM PNPlan AS P INNER JOIN PNTask AS T ON P.PlanID = T.PlanID WHERE T.WBS1 = @strWBS1 AND T.WBSType = 'WBS1'
       UNION
       SELECT P.PlanID FROM RPPlan AS P INNER JOIN RPTask AS T ON P.PlanID = T.PlanID WHERE T.WBS1 = @strWBS1 AND T.WBSType = 'WBS1' AND P.UtilizationIncludeFlg = 'Y'
      ) 
      THEN 1 ELSE 0 END

  IF (@intPlanCount > 0) RETURN

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  DECLARE @strCompany nvarchar(14)
  DECLARE @strDefaultCompany nvarchar(14)
  DECLARE @strDefaultFunctionalCurrencyCode nvarchar(3)
  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)
  DECLARE @strAccordionFormatID varchar(32)
  DECLARE @strNavigatorCalendarScale varchar(1)
  DECLARE @strEMCostCurrencyCode nvarchar(3)
  DECLARE @strEMBillCurrencyCode nvarchar(3)
  DECLARE @strGRCostCurrencyCode nvarchar(3)
  DECLARE @strGRBillCurrencyCode nvarchar(3)
  DECLARE @strCalcExpBillAmtFlg varchar(1)
  DECLARE @strCalcConBillAmtFlg varchar(1)
  DECLARE @strExpTab varchar(1)
  DECLARE @strConTab varchar(1)
  DECLARE @strBudgetType varchar(1)
  DECLARE @strReimbMethod varchar(1)
  DECLARE @strProjectCurrencyCode nvarchar(3)
  DECLARE @strBillingCurrencyCode nvarchar(3)

  DECLARE @tiDefExpWBSLevel tinyint
  DECLARE @tiDefConWBSLevel tinyint

  DECLARE @siWBS2Length smallint
  DECLARE @siWBS3Length smallint
  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint
  DECLARE @siLCLevels smallint
  DECLARE @siLCFmtLevel smallint

  DECLARE @siGRMethod smallint
  DECLARE @siCostGRRtMethod smallint
  DECLARE @siBillGRRtMethod smallint
  DECLARE @siCostRtMethod smallint
  DECLARE @siBillRtMethod smallint
  DECLARE @siLabMultType smallint

  DECLARE @intCostRtTableNo int
  DECLARE @intBillRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int
  DECLARE @intExpBillRtMethod int 
  DECLARE @intExpBillRtTableNo int 
  DECLARE @intConBillRtMethod int 
  DECLARE @intConBillRtTableNo int 

  DECLARE @dtMinDate datetime
  DECLARE @dtMaxDate datetime
  DECLARE @dtToday datetime 
  DECLARE @dtTomorrow datetime

  DECLARE @decLabBillMultiplier decimal(19, 4)
  DECLARE @decOverheadPct decimal(19, 4)
  DECLARE @decExpBillMultiplier decimal(19, 4)
  DECLARE @decConBillMultiplier decimal(19, 4)
  DECLARE @decTargetMultCost decimal(19, 4)

  DECLARE @strAllocMethod varchar(1)

  DECLARE @tabExpWBSTree TABLE(
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    WBSLevel tinyint,
    IsLeaf bit
    UNIQUE(WBS1, WBS2, WBS3, WBSLevel, IsLeaf)
  )

  DECLARE @tabConWBSTree TABLE(
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    WBSLevel tinyint,
    IsLeaf bit
    UNIQUE(WBS1, WBS2, WBS3, WBSLevel, IsLeaf)
  )

  DECLARE @tabLab TABLE(
    AssignmentID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    ResourceID nvarchar(20) COLLATE database_default,
    StartDate datetime,
    EndDate datetime
    UNIQUE(AssignmentID, WBS1, WBS2, WBS3, ResourceID)
  )

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled 
    FROM FW_CFGSystem

  -- Get the current date in current local and not the UTC date.
  
  SET @dtToday = DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)
  SET @dtTomorrow = DATEADD(dd, 1, @dtToday)  
  
  -- Get Org Format

  SELECT
    @siWBS2Length = WBS2Length,
    @siWBS3Length = WBS3Length,
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length,
    @siLCLevels = LCLevels
    FROM CFGFormat

  -- Get Default Company from CFGRMSettings.

  SELECT
    @strDefaultCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN DefaultCompany ELSE ' ' END
    FROM CFGRMSettings

  -- Get Default Functional Currency Code for Default Company.

  SELECT
    @strDefaultFunctionalCurrencyCode =
      CASE
        WHEN @strMultiCurrencyEnabled = 'Y'
        THEN ISNULL(MIN(FunctionalCurrencyCode), ' ') -- Used MIN so that there always have a result row.
        ELSE ' '
      END
    FROM CFGMainData WHERE Company = @strDefaultCompany

  -- Get Project data.

  SELECT 
    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN COALESCE(CM.Company, @strDefaultCompany) ELSE ' ' END,
    @dtMinDate = COALESCE(StartDate, EstCompletionDate, @dtTomorrow),
    @dtMaxDate = CASE WHEN (EstCompletionDate IS NOT NULL AND EstCompletionDate >= @dtMinDate) THEN EstCompletionDate ELSE @dtMinDate END,
    @strProjectCurrencyCode = COALESCE(ProjectCurrencyCode, @strDefaultFunctionalCurrencyCode),
	@strAllocMethod = AllocMethod,
    @strBillingCurrencyCode = 
      CASE 
        WHEN @strReportAtBillingInBillingCurr = 'Y' 
        THEN COALESCE(BillingCurrencyCode, @strDefaultFunctionalCurrencyCode)
        ELSE COALESCE(ProjectCurrencyCode, @strDefaultFunctionalCurrencyCode) 
      END
    FROM PR 
      LEFT JOIN (
        SELECT DISTINCT Company FROM CFGMainData
      ) AS CM
      ON SUBSTRING(PR.Org, @siOrg1Start, @siOrg1Length) = CM.Company
    WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '

  -- Get Default Plan Settings from CFGResourcePlanning

  SELECT 
    @strBudgetType = BudgetType,
    @siLabMultType =
      CASE BudgetType
        WHEN 'C' THEN 1 /* Planned Multiplier */
        WHEN 'B' THEN 3 /* Planned Ratio */
        WHEN 'A' THEN
          CASE 
            WHEN LabMultType IN (0, 1) THEN 1 /* Planned Multiplier */
            WHEN LabMultType IN (2, 3) THEN 3 /* Planned Ratio */
          END
      END,
    @strReimbMethod = 
      CASE BudgetType
        WHEN 'C' THEN 'C' /* Cost */
        WHEN 'B' THEN 'B' /* Bill */
        WHEN 'A' THEN ReimbMethod
      END,    
    @siGRMethod = GRMethod,
    @siCostGRRtMethod = CostGRRtMethod,
    @siBillGRRtMethod = BillGRRtMethod,
    @intGenResTableNo = GenResTableNo,
    @intGRBillTableNo = GRBillTableNo,
    @siCostRtMethod = CostRtMethod,
    @siBillRtMethod = BillingRtMethod,
    @intCostRtTableNo = CostRtTableNo,
    @intBillRtTableNo = BillingRtTableNo,
    @strCalcExpBillAmtFlg = 'Y',
    @intExpBillRtMethod = ExpBillRtMethod, 
    @intExpBillRtTableNo = ExpBillRtTableNo,
    @decExpBillMultiplier = ExpBillMultiplier,
    @strCalcConBillAmtFlg = 'Y',
    @intConBillRtMethod = ConBillRtMethod, 
    @intConBillRtTableNo = ConBillRtTableNo, 
    @decConBillMultiplier = ConBillMultiplier,
    @decLabBillMultiplier = LabBillMultiplier,
    @decOverheadPct = OverheadPct,
    @decTargetMultCost = TargetMultCost,
    @strExpTab = ExpTab,
    @strConTab = ConTab,
    @strNavigatorCalendarScale = NavigatorCalendarScale,
    @tiDefExpWBSLevel = ExpWBSLevel,
    @tiDefConWBSLevel = ConWBSLevel
    FROM CFGResourcePlanning WHERE Company = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN @strCompany ELSE ' ' END

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Determining the Currency Code of Rate Tables based on Rate Methods

  SELECT @strEMCostCurrencyCode = 
    CASE 
      WHEN @siCostRtMethod = 2 THEN (SELECT CurrencyCode FROM BTRRT WHERE TableNo = @intCostRtTableNo)
      WHEN @siCostRtMethod = 3 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intCostRtTableNo)
      WHEN @siCostRtMethod = 4 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intCostRtTableNo)
    END

  SELECT @strEMBillCurrencyCode = 
    CASE 
      WHEN @siBillRtMethod = 2 THEN (SELECT CurrencyCode FROM BTRRT WHERE TableNo = @intBillRtTableNo)
      WHEN @siBillRtMethod = 3 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intBillRtTableNo)
      WHEN @siBillRtMethod = 4 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intBillRtTableNo)
    END

  SELECT @strGRCostCurrencyCode = 
    CASE 
      WHEN @siCostGRRtMethod = 0 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intGenResTableNo)
      WHEN @siCostGRRtMethod = 1 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intGenResTableNo)
    END

  SELECT @strGRBillCurrencyCode = 
    CASE 
      WHEN @siBillGRRtMethod = 0 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intGRBillTableNo)
      WHEN @siBillGRRtMethod = 1 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intGRBillTableNo)
    END

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  /*
    Need to select either:
      1. Rows at same level as the Default setting (e.g. Level 2).
      2. Rows at level lesser as the Default setting (e.g. Level 1) but are leaf nodes.
  */

  INSERT @tabExpWBSTree(
    WBS1,
    WBS2,
    WBS3,
    WBSLevel,
    IsLeaf
  )
    SELECT
      WBS1,
      WBS2,
      WBS3,
      WBSLevel,
      IsLeaf
      FROM (
        SELECT
          WBS1, WBS2, WBS3,
          CASE 
            WHEN WBS2 = ' ' AND WBS3 = ' ' THEN 1
            WHEN WBS2 != ' ' AND WBS3 = ' ' THEN 2
            WHEN WBS2 != ' ' AND WBS3 != ' ' THEN 3
          END AS WBSLevel,
          CASE WHEN SubLevel = 'N' THEN 1 ELSE 0 END AS IsLeaf
          FROM PR
          WHERE WBS1 = @strWBS1
      ) AS X
      WHERE (WBSLevel = @tiDefExpWBSLevel OR (WBSLevel < @tiDefExpWBSLevel AND IsLeaf = 1))

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  INSERT @tabConWBSTree(
    WBS1,
    WBS2,
    WBS3,
    WBSLevel,
    IsLeaf
  )
    SELECT
      WBS1,
      WBS2,
      WBS3,
      WBSLevel,
      IsLeaf
      FROM (
        SELECT
          WBS1, WBS2, WBS3,
          CASE 
            WHEN WBS2 = ' ' AND WBS3 = ' ' THEN 1
            WHEN WBS2 != ' ' AND WBS3 = ' ' THEN 2
            WHEN WBS2 != ' ' AND WBS3 != ' ' THEN 3
          END AS WBSLevel,
          CASE WHEN SubLevel = 'N' THEN 1 ELSE 0 END AS IsLeaf
    FROM PR
          WHERE WBS1 = @strWBS1
    ) AS X
      WHERE (WBSLevel = @tiDefConWBSLevel OR (WBSLevel < @tiDefConWBSLevel AND IsLeaf = 1))

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Save away the list of Employees who charged to the Project (from the Ledger tables)

  INSERT @tabLab(
    AssignmentID,
    WBS1,
    WBS2,
    WBS3,
    ResourceID,
    StartDate,
    EndDate
  )
    SELECT DISTINCT
      REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS AssignmentID,
      WBS1,
      WBS2,
      WBS3,
      Employee AS ResourceID,
      @dtMinDate AS StartDate,
      @dtMaxDate AS EndDate
      FROM (
        SELECT DISTINCT
          LD.WBS1,
          LD.WBS2,
          LD.WBS3,
          LD.Employee
          FROM LD -- LD table has entries at only the leaf level.
            INNER JOIN EM ON LD.Employee = EM.Employee -- Only pick up records in LD which has legitimate EM.
          WHERE LD.WBS1 = @strWBS1 AND LD.ProjectCost = 'Y' 
          GROUP BY LD.WBS1, LD.WBS2, LD.WBS3, LD.Employee
        UNION SELECT DISTINCT
          TD.WBS1,
          TD.WBS2,
          TD.WBS3,
          TD.Employee
          FROM tkDetail AS TD -- TD table has entries at only the leaf level.
            INNER JOIN EM ON TD.Employee = EM.Employee  
            INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
          WHERE TD.WBS1 = @strWBS1 AND
            (RegHrs > 0 OR OvtHrs > 0 OR SpecialOvtHrs > 0)
          GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.Employee
        UNION SELECT DISTINCT
          TD.WBS1,
          TD.WBS2,
          TD.WBS3,
          TD.Employee
          FROM tsDetail AS TD -- TD table has entries at only the leaf level.
            INNER JOIN EM ON TD.Employee = EM.Employee  
            INNER JOIN tsControl AS TC ON (TC.Posted = 'N' AND TD.Batch = TC.Batch)
          WHERE TD.WBS1 = @strWBS1 AND
            (RegHrs > 0 OR OvtHrs > 0 OR SpecialOvtHrs > 0)
          GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.Employee
      ) AS X
        GROUP BY WBS1, WBS2, WBS3, Employee

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
    IF (@autoCommit='Y') BEGIN TRANSACTION

    -- Insert PNPlan record.

    SET @strPlanID = REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')
  
    INSERT PNPlan (
      PlanID,
      PlanName,
      PlanNumber,
      ProjMgr,
      Principal,
      Supervisor,
      ClientID,
      Org,
      WBS1,
      StartDate,
      EndDate,

      Probability,

      PctCompleteFormula,
      LabMultType,
      ReimbMethod,
      TargetMultCost,
      AvailableFlg,
      UnPostedFlg,
      CommitmentFlg,
      UtilizationIncludeFlg,

      Status,
      Company,
      StartingDayOfWeek,
      CostCurrencyCode,
      BillingCurrencyCode,

      BudgetType,
      CostRtMethod,
      BillingRtMethod,
      CostRtTableNo,
      BillingRtTableNo,
      LabBillMultiplier,
      GRMethod,
      CostGRRtMethod,
      BillGRRtMethod,
      GenResTableNo,
      GRBillTableNo,

      CalcExpBillAmtFlg,
      ExpBillRtMethod, 
      ExpBillRtTableNo,
      ExpBillMultiplier,

      CalcConBillAmtFlg,
      ConBillRtMethod, 
      ConBillRtTableNo,
      ConBillMultiplier,

      CompensationFee,
      ConsultantFee,
      ReimbAllowance,
      CompensationFeeBill,
      ConsultantFeeBill,
      ReimbAllowanceBill,

      CompensationFeeDirLab,
      CompensationFeeDirExp,
      ReimbAllowanceExp,
      ReimbAllowanceCon,
      CompensationFeeDirLabBill,
      CompensationFeeDirExpBill,
      ReimbAllowanceExpBill,
      ReimbAllowanceConBill,

    OverheadPct,
      ExpWBSLevel,
      ConWBSLevel,

      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        @strPlanID AS PlanID,
        ISNULL(PR.Name, ' ') AS PlanName,
        PR.WBS1 AS PlanNumber,
        PR.ProjMgr AS ProjMgr,
        PR.Principal AS Principal,
        PR.Supervisor AS Supervisor,
        PR.ClientID AS ClientID,
        PR.Org AS Org,
        PR.WBS1 AS WBS1,
        @dtMinDate AS StartDate,
        @dtMaxDate AS EndDate,

        100 AS Probability, -- Project Plan always 100%

        2 AS PctCompleteFormula, -- JTD / (JTD + ETC)
        @siLabMultType AS LabMultType, 
        @strReimbMethod AS ReimbMethod,
        @decTargetMultCost AS TargetMultCost,
        'Y' AS AvailableFlg,
        'Y' AS UnPostedFlg,
        'Y' AS CommitmentFlg,
        'Y' AS UtilizationIncludeFlg,

        'A' AS Status, -- Active
        @strCompany Company,
        2 AS StartingDayOfWeek, -- Always set to Monday
        @strProjectCurrencyCode AS CostCurrencyCode,
        @strBillingCurrencyCode AS BillingCurrencyCode,

        @strBudgetType AS BudgetType, -- From CFGResourcePlanning
		@siCostRtMethod AS CostRtMethod,
		@siBillRtMethod AS BillingRtMethod,
         
        CASE WHEN @strMultiCurrencyEnabled <> 'Y' THEN @intCostRtTableNo
        ELSE
        CASE WHEN @strEMCostCurrencyCode = @strProjectCurrencyCode THEN @intCostRtTableNo ELSE 0 END
        END AS CostRtTableNo,
 
		CASE WHEN @strMultiCurrencyEnabled <> 'Y' THEN @intBillRtTableNo
        ELSE
        CASE WHEN @strEMBillCurrencyCode = @strBillingCurrencyCode THEN @intBillRtTableNo ELSE 0 END
        END AS BillingRtTableNo,       
 
        @decLabBillMultiplier AS LabBillMultiplier,
 
        @siGRMethod AS GRMethod,
        @siCostGRRtMethod AS CostGRRtMethod,
        @siBillGRRtMethod AS BillGRRtMethod,

        CASE WHEN @strMultiCurrencyEnabled <> 'Y' THEN @intGenResTableNo
        ELSE
          CASE WHEN @strGRCostCurrencyCode = @strProjectCurrencyCode THEN @intGenResTableNo ELSE 0 END 
        END AS GenResTableNo,

        CASE 
          WHEN @strMultiCurrencyEnabled <> 'Y' 
          THEN @intGRBillTableNo
          ELSE
            CASE 
              WHEN @strGRBillCurrencyCode = @strBillingCurrencyCode
              THEN @intGRBillTableNo
              ELSE 0
            END 
        END AS GRBillTableNo,

        @strCalcExpBillAmtFlg AS CalcExpBillAmtFlg,
        @intExpBillRtMethod AS ExpBillRtMethod, 
        @intExpBillRtTableNo AS ExpBillRtTableNo,
        @decExpBillMultiplier AS ExpBillMultiplier,
 
        @strCalcConBillAmtFlg AS CalcConBillAmtFlg,
        @intConBillRtMethod AS ConBillRtMethod, 
        @intConBillRtTableNo AS ConBillRtTableNo,
        @decConBillMultiplier AS ConBillMultiplier,

        PR.Fee AS CompensationFee,
        PR.ConsultFee AS ConsultantFee,
        PR.ReimbAllow AS ReimbAllowance,

        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeBillingCurrency ELSE PR.Fee END) AS CompensationFeeBill,
        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ConsultFeeBillingCurrency ELSE PR.ConsultFee END) AS ConsultantFeeBill,
        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowBillingCurrency ELSE PR.ReimbAllow END) AS ReimbAllowanceBill,

        PR.FeeDirLab AS CompensationFeeDirLab,
        PR.FeeDirExp AS CompensationFeeDirExp,
        PR.ReimbAllowExp AS ReimbAllowanceExp,
        PR.ReimbAllowCons AS ReimbAllowanceCon,

        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirLabBillingCurrency ELSE PR.FeeDirLab END) AS CompensationFeeDirLabBill,
        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirExpBillingCurrency ELSE PR.FeeDirExp END) AS CompensationFeeDirExpBill,
        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowExpBillingCurrency ELSE PR.ReimbAllowExp END) AS ReimbAllowanceExpBill,
        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowConsBillingCurrency ELSE PR.ReimbAllowCons END) AS ReimbAllowanceConBill,

        @decOverheadPct AS OverheadPct,
        @tiDefExpWBSLevel AS ExpWBSLevel,
        @tiDefConWBSLevel AS ConWBSLevel,

        @strUserName AS CreateUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
        NULL,
        NULL

        FROM PR
        WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND
          (NOT EXISTS 
             (SELECT P.PlanID FROM PNPlan AS P INNER JOIN PNTask AS T ON P.PlanID = T.PlanID WHERE T.WBS1 = @strWBS1 AND T.WBSType = 'WBS1'
              UNION
              SELECT P.PlanID FROM RPPlan AS P INNER JOIN RPTask AS T ON P.PlanID = T.PlanID WHERE T.WBS1 = @strWBS1 AND T.WBSType = 'WBS1' AND P.UtilizationIncludeFlg = 'Y'
             )
          )

   -- If the given Project already have an associated Plan, then exit this stored procedure.
 
    IF (@@ROWCOUNT = 0)
      BEGIN
        SET @strPlanID = NULL
        IF (@autoCommit='Y') ROLLBACK
        RETURN
      END -- END IF (@@ROWCOUNT > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    -- Insert WBS1 PNTask record.

    INSERT PNTask (
      TaskID,
      PlanID,
      Name,
      WBS1,
      WBS2,
      WBS3,
      WBSType,
      ParentOutlineNumber,
      OutlineNumber,
      ChildrenCount,
      OutlineLevel,

      StartDate,
      EndDate,
      RevenueStartDate,
      RevenueEndDate,
      ProjMgr,
      ChargeType,
      ProjectType,
      ClientID,
      Status,
      Org,
      Notes,

      CompensationFee,
      ConsultantFee,
      ReimbAllowance,
      CompensationFeeBill,
      ConsultantFeeBill,
      ReimbAllowanceBill,

      CompensationFeeDirLab,
      CompensationFeeDirExp,
      ReimbAllowanceExp,
      ReimbAllowanceCon,
      CompensationFeeDirLabBill,
      CompensationFeeDirExpBill,
      ReimbAllowanceExpBill,
      ReimbAllowanceConBill,

      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TaskID,
        @strPlanID AS PlanID,
        ISNULL(PR.Name, ' ') AS Name,
        PR.WBS1 AS WBS1,
        NULL AS WBS2,
        NULL AS WBS3,
        'WBS1' AS WBSType,
        NULL AS ParentOutlineNumber,
        '001' AS OutlineNumber,
        ISNULL(X1.WBS2Count, 0) AS ChildrenCount,
        0 AS OutlineLevel,

        @dtMinDate AS StartDate,
        @dtMaxDate  AS EndDate,
        NULL AS RevenueStartDate,
        NULL AS RevenueEndDate,

        PR.ProjMgr AS ProjMgr,
        PR.ChargeType AS ChargeType,
        PR.ProjectType AS ProjectType,
        PR.ClientID AS ClientID,
        ISNULL(PR.Status, 'A') AS Status,
        PR.Org AS Org,
        PAD.Notes AS Notes,

        PR.Fee AS CompensationFee,
        PR.ConsultFee AS ConsultantFee,
        PR.ReimbAllow AS ReimbAllowance,

        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeBillingCurrency ELSE PR.Fee END) AS CompensationFeeBill,
        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ConsultFeeBillingCurrency ELSE PR.ConsultFee END) AS ConsultantFeeBill,
        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowBillingCurrency ELSE PR.ReimbAllow END) AS ReimbAllowanceBill,

        PR.FeeDirLab AS CompensationFeeDirLab,
        PR.FeeDirExp AS CompensationFeeDirExp,
        PR.ReimbAllowExp AS ReimbAllowanceExp,
        PR.ReimbAllowCons AS ReimbAllowanceCon,

        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirLabBillingCurrency ELSE PR.FeeDirLab END) AS CompensationFeeDirLabBill,
        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirExpBillingCurrency ELSE PR.FeeDirExp END) AS CompensationFeeDirExpBill,
        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowExpBillingCurrency ELSE PR.ReimbAllowExp END) AS ReimbAllowanceExpBill,
        (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowConsBillingCurrency ELSE PR.ReimbAllowCons END) AS ReimbAllowanceConBill,

        @strUserName AS CreateUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
        @strUserName AS ModUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate

        FROM PR
          LEFT JOIN (SELECT COUNT(*) AS WBS2Count, WBS1 FROM PR WHERE WBS1 = @strWBS1 AND WBS2 <> ' ' AND WBS3 = ' ' GROUP BY WBS1) AS X1 ON X1.WBS1 = PR.WBS1
          LEFT JOIN (SELECT COUNT(*) AS ACount, MIN(StartDate) AS MinDate, MAX(EndDate) AS MaxDate, WBS1 FROM @tabLab AS A WHERE A.WBS1 = @strWBS1 GROUP BY WBS1) AS X2 ON X2.WBS1 = PR.WBS1
          LEFT JOIN PRAdditionalData AS PAD ON PR.WBS1 = PAD.WBS1 AND PR.WBS2 = PAD.WBS2 AND PR.WBS3 = PAD.WBS3
        WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    -- Insert WBS2 PNTask record, if applicable.

    IF(@siWBS2Length > 0)
      BEGIN

        INSERT PNTask(
          TaskID,
          PlanID,
          Name,
          WBS1,
          WBS2,
          WBS3,
          WBSType,
          ParentOutlineNumber,
          OutlineNumber,
          ChildrenCount,
          OutlineLevel,

          StartDate,
          EndDate,
          RevenueStartDate,
          RevenueEndDate,

          ProjMgr,
          ChargeType,
          ProjectType,
          ClientID,
          Status,
          Org,
          Notes,

          CompensationFee,
          ConsultantFee,
          ReimbAllowance,
          CompensationFeeBill,
          ConsultantFeeBill,
          ReimbAllowanceBill,

          CompensationFeeDirLab,
          CompensationFeeDirExp,
          ReimbAllowanceExp,
          ReimbAllowanceCon,
          CompensationFeeDirLabBill,
          CompensationFeeDirExpBill,
          ReimbAllowanceExpBill,
          ReimbAllowanceConBill,

          CreateUser,
          CreateDate,
          ModUser,
          ModDate
        )
          SELECT
            TaskID,
            PlanID,
            Name,
            WBS1,
            WBS2,
            WBS3,
            WBSType,
            ParentOutlineNumber,
            ParentOutlineNumber + '.' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
            ChildrenCount,
            OutlineLevel,
            StartDate,
            EndDate,
            NULL AS RevenueStartDate,
            NULL AS RevenueEndDate,
			ProjMgr,
            ChargeType,
            ProjectType,
            ClientID,
            Status,
            Org,
            Notes,

            CompensationFee,
            ConsultantFee,
            ReimbAllowance,
            CompensationFeeBill,
            ConsultantFeeBill,
            ReimbAllowanceBill,

            CompensationFeeDirLab,
            CompensationFeeDirExp,
            ReimbAllowanceExp,
            ReimbAllowanceCon,
            CompensationFeeDirLabBill,
            CompensationFeeDirExpBill,
            ReimbAllowanceExpBill,
            ReimbAllowanceConBill,

            CreateUser,
            CreateDate,
            ModUser,
            ModDate
            FROM (
              SELECT
                REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TaskID,
                @strPlanID AS PlanID,
                ISNULL(PR.Name, ' ') AS Name,
                PR.WBS1 AS WBS1,
                PR.WBS2 AS WBS2,
                NULL AS WBS3,
                'WBS2' AS WBSType,
                '001' AS ParentOutlineNumber,
                ISNULL(X1.WBS3Count, 0) AS ChildrenCount,
                1 AS OutlineLevel,
                COALESCE(PR.StartDate, PR.EstCompletionDate, X2.MinDate, @dtMinDate) AS StartDate,
                COALESCE(PR.EstCompletionDate, X2.MaxDate, @dtMaxDate) AS EndDate,
                PR.ProjMgr AS ProjMgr,
                PR.ChargeType AS ChargeType,
                PR.ProjectType AS ProjectType,
                PR.ClientID AS ClientID,
                ISNULL(PR.Status, 'A') AS Status,
                PR.Org AS Org,
                PAD.Notes AS Notes,

                PR.Fee AS CompensationFee,
                PR.ConsultFee AS ConsultantFee,
                PR.ReimbAllow AS ReimbAllowance,

                (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeBillingCurrency ELSE PR.Fee END) AS CompensationFeeBill,
                (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ConsultFeeBillingCurrency ELSE PR.ConsultFee END) AS ConsultantFeeBill,
                (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowBillingCurrency ELSE PR.ReimbAllow END) AS ReimbAllowanceBill,

                PR.FeeDirLab AS CompensationFeeDirLab,
                PR.FeeDirExp AS CompensationFeeDirExp,
                PR.ReimbAllowExp AS ReimbAllowanceExp,
                PR.ReimbAllowCons AS ReimbAllowanceCon,

                (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirLabBillingCurrency ELSE PR.FeeDirLab END) AS CompensationFeeDirLabBill,
                (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirExpBillingCurrency ELSE PR.FeeDirExp END) AS CompensationFeeDirExpBill,
                (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowExpBillingCurrency ELSE PR.ReimbAllowExp END) AS ReimbAllowanceExpBill,
                (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowConsBillingCurrency ELSE PR.ReimbAllowCons END) AS ReimbAllowanceConBill,

                @strUserName AS CreateUser,
                CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
                @strUserName AS ModUser,
                CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate,
                ROW_NUMBER() OVER (PARTITION BY PR.WBS1 ORDER BY PR.WBS2) AS RowID
                FROM PR
                  LEFT JOIN (SELECT COUNT(*) AS WBS3Count, WBS1, WBS2 FROM PR WHERE WBS1 = @strWBS1 AND WBS2 <> ' ' AND WBS3 <> ' ' GROUP BY WBS1, WBS2) AS X1 ON X1.WBS1 = PR.WBS1 AND X1.WBS2 = PR.WBS2
                  LEFT JOIN (SELECT COUNT(*) AS ACount, MIN(StartDate) AS MinDate, MAX(EndDate) AS MaxDate, WBS1, WBS2 FROM @tabLab AS A WHERE A.WBS1 = @strWBS1 AND WBS2 <> ' ' GROUP BY WBS1, WBS2) AS X2 ON X2.WBS1 = PR.WBS1 AND X2.WBS2 = PR.WBS2
                  LEFT JOIN PRAdditionalData AS PAD ON PR.WBS1 = PAD.WBS1 AND PR.WBS2 = PAD.WBS2 AND PR.WBS3 = PAD.WBS3
                WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 <> ' ' AND PR.WBS3 = ' '
            ) AS Y

      END -- END IF(@siWBS2Length > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    -- Insert WBS3 PNTask record, if applicable.

    IF(@siWBS3Length > 0)
      BEGIN

        INSERT PNTask(
          TaskID,
          PlanID,
          Name,
          WBS1,
          WBS2,
          WBS3,
          WBSType,
          ParentOutlineNumber,
          OutlineNumber,
          ChildrenCount,
          OutlineLevel,

          StartDate,
          EndDate,
          RevenueStartDate,
          RevenueEndDate,

          ProjMgr,
          ChargeType,
          ProjectType,
          ClientID,
          Status,
          Org,
          Notes,

          CompensationFee,
          ConsultantFee,
          ReimbAllowance,
          CompensationFeeBill,
          ConsultantFeeBill,
          ReimbAllowanceBill,

          CompensationFeeDirLab,
          CompensationFeeDirExp,
          ReimbAllowanceExp,
          ReimbAllowanceCon,
          CompensationFeeDirLabBill,
          CompensationFeeDirExpBill,
          ReimbAllowanceExpBill,
          ReimbAllowanceConBill,

          CreateUser,
          CreateDate,
          ModUser,
          ModDate
        )
          SELECT
            TaskID,
            PlanID,
            Name,
            WBS1,
            WBS2,
            WBS3,
            WBSType,
            ParentOutlineNumber,
            ParentOutlineNumber + '.' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
            ChildrenCount,
            OutlineLevel,

            StartDate,
            EndDate,
            NULL AS RevenueStartDate,
            NULL AS RevenueEndDate,

            ProjMgr,
            ChargeType,
            ProjectType,
            ClientID,
            Status,
            Org,
            Notes,

            CompensationFee,
            ConsultantFee,
            ReimbAllowance,
            CompensationFeeBill,
            ConsultantFeeBill,
            ReimbAllowanceBill,

            CompensationFeeDirLab,
            CompensationFeeDirExp,
            ReimbAllowanceExp,
            ReimbAllowanceCon,
            CompensationFeeDirLabBill,
            CompensationFeeDirExpBill,
            ReimbAllowanceExpBill,
            ReimbAllowanceConBill,

            CreateUser,
            CreateDate,
            ModUser,
            ModDate
            FROM (
              SELECT
                REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TaskID,
                @strPlanID AS PlanID,
                ISNULL(PR.Name, ' ') AS Name,
                PR.WBS1 AS WBS1,
                PR.WBS2 AS WBS2,
                PR.WBS3 AS WBS3,
                'WBS3' AS WBSType,
                PT.OutlineNumber AS ParentOutlineNumber,
                0 AS ChildrenCount,
                2 AS OutlineLevel,
                COALESCE(PR.StartDate, PR.EstCompletionDate, PT.StartDate, X2.MinDate, @dtMinDate) AS StartDate,
                COALESCE(PR.EstCompletionDate, PT.EndDate, X2.MaxDate, @dtMaxDate) AS EndDate,
                PR.ProjMgr AS ProjMgr,
                PR.ChargeType AS ChargeType,
                PR.ProjectType AS ProjectType,
                PR.ClientID AS ClientID,
                ISNULL(PR.Status, 'A') AS Status,
                PR.Org AS Org,
                PAD.Notes AS Notes,

                PR.Fee AS CompensationFee,
                PR.ConsultFee AS ConsultantFee,
                PR.ReimbAllow AS ReimbAllowance,

                (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeBillingCurrency ELSE PR.Fee END) AS CompensationFeeBill,
                (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ConsultFeeBillingCurrency ELSE PR.ConsultFee END) AS ConsultantFeeBill,
                (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowBillingCurrency ELSE PR.ReimbAllow END) AS ReimbAllowanceBill,

                PR.FeeDirLab AS CompensationFeeDirLab,
                PR.FeeDirExp AS CompensationFeeDirExp,
                PR.ReimbAllowExp AS ReimbAllowanceExp,
                PR.ReimbAllowCons AS ReimbAllowanceCon,

                (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirLabBillingCurrency ELSE PR.FeeDirLab END) AS CompensationFeeDirLabBill,
                (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirExpBillingCurrency ELSE PR.FeeDirExp END) AS CompensationFeeDirExpBill,
                (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowExpBillingCurrency ELSE PR.ReimbAllowExp END) AS ReimbAllowanceExpBill,
                (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowConsBillingCurrency ELSE PR.ReimbAllowCons END) AS ReimbAllowanceConBill,

                @strUserName AS CreateUser,
                CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
                @strUserName AS ModUser,
                CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate,
                ROW_NUMBER() OVER (PARTITION BY PR.WBS1, PR.WBS2 ORDER BY PR.WBS3) AS RowID
                FROM PR
                  INNER JOIN PNTask AS PT ON PR.WBS1 = PT.WBS1 AND PR.WBS2 = PT.WBS2
                  LEFT JOIN (SELECT COUNT(*) AS ACount, MIN(StartDate) AS MinDate, MAX(EndDate) AS MaxDate, WBS1, WBS2, WBS3 
				  FROM @tabLab AS A WHERE A.WBS1 = @strWBS1 AND WBS2 <> ' ' AND WBS3 <> ' ' GROUP BY WBS1, WBS2, WBS3) AS X2 ON X2.WBS1 = PR.WBS1 AND X2.WBS2 = PR.WBS2 AND X2.WBS3 = PR.WBS3
                  LEFT JOIN PRAdditionalData AS PAD ON PR.WBS1 = PAD.WBS1 AND PR.WBS2 = PAD.WBS2 AND PR.WBS3 = PAD.WBS3
                WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 <> ' ' AND PR.WBS3 <> ' '
            ) AS Y

      END -- END IF(@siWBS3Length > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    -- Create WBS Level Format.

    INSERT PNWBSLevelFormat(
      WBSFormatID,
      PlanID,
      FmtLevel,
      WBSType,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT 
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS WBSFormatID,
        @strPlanID AS PlanID,
        1 AS FmtLevel,
        'WBS1' AS WBSType,
        @strUserName AS CreateUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
        @strUserName AS ModUser,
        CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate

    IF(@siWBS2Length > 0)
      BEGIN

        INSERT PNWBSLevelFormat(
          WBSFormatID,
          PlanID,
          FmtLevel,
          WBSType,
          CreateUser,
          CreateDate,
          ModUser,
          ModDate
        )
          SELECT 
            REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS WBSFormatID,
            @strPlanID AS PlanID,
            2 AS FmtLevel,
            'WBS2' AS WBSType,
            @strUserName AS CreateUser,
            CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
            @strUserName AS ModUser,
            CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate

      END -- END IF(@siWBS2Length > 0)

    IF(@siWBS3Length > 0)
      BEGIN

        INSERT PNWBSLevelFormat(
          WBSFormatID,
          PlanID,
          FmtLevel,
          WBSType,
          CreateUser,
          CreateDate,
          ModUser,
          ModDate
        )
          SELECT 
            REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS WBSFormatID,
            @strPlanID AS PlanID,
            3 AS FmtLevel,
            'WBS3' AS WBSType,
            @strUserName AS CreateUser,
            CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
            @strUserName AS ModUser,
            CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate

      END -- END IF(@siWBS3Length > 0)

    IF(@siLCLevels > 0)
      BEGIN

        SELECT @siLCFmtLevel =
          CASE 
            WHEN @siWBS3Length > 0 THEN 4
            WHEN @siWBS3Length = 0 AND @siWBS2Length > 0 THEN 3
            WHEN @siWBS2Length = 0 THEN 2
            ELSE 0
          END

        INSERT PNWBSLevelFormat(
          WBSFormatID,
          PlanID,
          FmtLevel,
          WBSType,
          CreateUser,
          CreateDate,
          ModUser,
          ModDate
        )
        SELECT 
            REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS WBSFormatID,
            @strPlanID AS PlanID,
            @siLCFmtLevel AS FmtLevel,
            'LBCD' AS WBSType,
            @strUserName AS CreateUser,
            CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
            @strUserName AS ModUser,
            CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate

      END -- END IF(@siLCLevels > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	DECLARE @tmpStartDate Datetime = NULL
	DECLARE @tmpEndDate Datetime = NULL

  -- for nodes having wbs3 values as not null
	UPDATE PNTask SET StartDate = NULL,EndDate = NULL where WBS1 = @strWBS1 

	UPDATE T  set @tmpStartDate = COALESCE(PR.StartDate,PR.EstStartDate,PR.ActCompletionDate,PR.EstCompletionDate, NULL),
					@tmpEndDate = CASE WHEN PR.ActCompletionDate >= COALESCE(PR.StartDate,'') AND PR.ActCompletionDate IS NOT NULL THEN PR.ActCompletionDate 
							WHEN PR.EstCompletionDate >= COALESCE(PR.StartDate,'') AND PR.EstCompletionDate IS NOT NULL THEN PR.EstCompletionDate 
						ELSE COALESCE(PR.StartDate,PR.EstStartDate,PR.ActCompletionDate,PR.EstCompletionDate, NULL) END,
					T.StartDate = @tmpStartDate,
					T.EndDate = CASE WHEN @tmpStartDate IS NOT NULL AND @tmpEndDate IS NOT NULL AND @tmpEndDate < @tmpStartDate THEN @tmpStartDate ELSE @tmpEndDate END
			
	FROM PR
         INNER JOIN PNTask AS T ON PR.WBS1 = T.WBS1 AND PR.WBS2 = IsNull(T.WBS2, ' ') AND PR.WBS3 = IsNull(T.WBS3, ' ')
	WHERE T.PlanID = @strPlanID AND T.WBS1 = @strWBS1 AND T.WBS2 IS NOT NULL AND T.WBS3 IS NOT NULL

	
	-- for parent dates working up to WBS2 then to WBS1

	 

	UPDATE  T 
		SET @TmpStartDate =  CASE WHEN (PR.StartDate <= X2.MinDate OR X2.MinDate IS NULL) AND PR.StartDate IS NOT NULL THEN PR.StartDate 
							WHEN (PR.EstStartDate <= X2.MinDate OR  X2.MinDate IS NULL) AND PR.EstStartDate IS NOT NULL  THEN PR.EstStartDate 
						ELSE COALESCE(X2.MinDate,PR.ActCompletionDate,PR.EstCompletionDate) END , 
			@tmpEndDate =	CASE WHEN (PR.ActCompletionDate >= X2.MaxDate AND PR.ActCompletionDate >= X2.MinDate OR X2.MinDate IS NULL ) AND PR.ActCompletionDate IS NOT NULL THEN PR.ActCompletionDate 
							WHEN (PR.EstCompletionDate >= X2.MaxDate AND PR.EstCompletionDate >= X2.MinDate OR x2.MinDate IS NULL) AND PR.EstCompletionDate IS NOT NULL THEN PR.EstCompletionDate 
						ELSE COALESCE(X2.MaxDate,@TmpStartDate) END ,
					T.StartDate = @tmpStartDate,
					T.EndDate = CASE WHEN @tmpStartDate IS NOT NULL AND @tmpEndDate IS NOT NULL AND @tmpEndDate < @tmpStartDate THEN @tmpStartDate ELSE @tmpEndDate END
	FROM PR
         INNER JOIN PNTask as T ON PR.WBS1 = T.WBS1 AND PR.WBS2 = T.WBS2 AND PR.WBS3 = ' '
		  LEFT JOIN (SELECT COUNT(*) AS ACount, MIN(StartDate) AS MinDate, MAX(EndDate) AS MaxDate, WBS1, WBS2 
				  FROM PNTask AS A WHERE A.WBS1 = @strWBS1 AND WBS2 IS NOT NULL AND WBS3 IS NOT NULL GROUP BY WBS1, WBS2) AS X2 ON X2.WBS1 = PR.WBS1 AND X2.WBS2 = PR.WBS2 
	WHERE T.PlanID = @strPlanID AND T.WBS1 = @strWBS1 AND T.WBS2 IS NOT NULL AND T.WBS3 IS NULL
	
	   
   -- for parent dates working up to WBS1

	UPDATE  T 
		SET @TmpStartDate =  CASE WHEN (PR.StartDate <= X2.MinDate OR X2.MinDate IS NULL) AND PR.StartDate IS NOT NULL THEN PR.StartDate 
							WHEN (PR.EstStartDate <= X2.MinDate OR X2.MinDate IS NULL) AND PR.EstStartDate IS NOT NULL THEN PR.EstStartDate 
						ELSE COALESCE(X2.MinDate,PR.ActCompletionDate,PR.EstCompletionDate) END ,
			@tmpEndDate =	CASE WHEN (PR.ActCompletionDate >= X2.MaxDate OR X2.MaxDate IS NULL) AND PR.ActCompletionDate IS NOT NULL THEN PR.ActCompletionDate 
							WHEN (PR.EstCompletionDate >= X2.MaxDate OR X2.MaxDate IS NULL) AND PR.EstCompletionDate IS NOT NULL THEN PR.EstCompletionDate 
						ELSE COALESCE(X2.MaxDate,T.StartDate) END ,
					T.StartDate = @tmpStartDate,
					T.EndDate = CASE WHEN @tmpStartDate IS NOT NULL AND @tmpEndDate IS NOT NULL AND @tmpEndDate < @tmpStartDate THEN @tmpStartDate ELSE @tmpEndDate END
	 FROM PR INNER JOIN PNTask as T ON PR.WBS1 = T.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
	 LEFT JOIN (SELECT COUNT(*) AS ACount, MIN(StartDate) AS MinDate, MAX(EndDate) AS MaxDate, WBS1 
				  FROM PNTask AS A WHERE A.WBS1 = @strWBS1 AND WBS2 IS NOT NULL AND WBS3 IS NULL GROUP BY WBS1 ) AS X2 ON X2.WBS1 = PR.WBS1  
	WHERE T.PlanID = @strPlanID AND T.WBS1 = @strWBS1 AND T.WBS2 IS NULL AND T.WBS3 IS NULL
   
   -- Now set all empty Plan Start and Plan End to its parent's Plan Start and Plan End dates

   UPDATE  T set T.StartDate = @dtMinDate, T.EndDate = @dtMaxDate
		from PNTask T 
	WHERE T.PlanID = @strPlanID
		AND T.WBS1 IS NOT NULL AND T.WBS2 IS NULL AND T.WBS3 IS NULL
		AND t.StartDate IS NULL AND t.EndDate IS NULL

	
	 DECLARE @planDates TABLE (
		PlanID VARCHAR(32),
		OutlineNumber VARCHAR(255),
		StartDate DATETIME,
		EndDate DATETIME 
	);

	DECLARE @planParentMinMaxDates TABLE (
		PlanID VARCHAR(32),
		ParentOutlineNumber VARCHAR(255),
		StartDate DATETIME,
		EndDate DATETIME 
	);
	 
   DECLARE @iLevelTop int = 1
   DECLARE @MaxiLevel int
	SELECT @MaxiLevel = MAX(OutlineLevel) FROM PNTask WHERE PlanID = @strPlanID

	WHILE (@iLevelTop <= @MaxiLevel) 
	BEGIN 

			DELETE FROM @planDates;

			INSERT INTO @planDates
			SELECT PlanID, OutlineNumber, StartDate,EndDate
				FROM PNTask 
				WHERE PNTask.PlanID = @strPlanID 

		 	UPDATE T SET
			StartDate = CASE WHEN T.StartDate IS NULL THEN x.StartDate ELSE T.StartDate END,
			EndDate = CASE WHEN T.EndDate IS NULL THEN x.EndDate ELSE T.EndDate END
			FROM PNTask AS T
			INNER JOIN @planDates AS X ON T.PlanID = X.PlanID AND T.ParentOutlineNumber = X.OutlineNumber
			WHERE T.PlanID = @strPlanID AND T.OutlineLevel = @iLevelTop
			AND (T.StartDate IS NULL OR T.EndDate IS NULL)

		--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		SET @iLevelTop = @iLevelTop + 1

		END

	 --====================================================================================================================================================


  -- Update all WBS levels in PR table with PlanID.

	UPDATE PR SET
		PR.PlanID = @strPlanID
    FROM PR
    WHERE PR.WBS1 = @strWBS1

	--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 
	DECLARE @iLevel INT
	SELECT @iLevel = MAX(OutlineLevel) - 1 FROM PNTask WHERE PlanID = @strPlanID
	-- Traverse upward from bottom level.
	-- Update StartDate and EndDate of parent rows.

	WHILE (@iLevel >= 0) 
	BEGIN

		DELETE FROM @planParentMinMaxDates;

		INSERT INTO @planParentMinMaxDates
		SELECT PlanID, ParentOutlineNumber, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
				FROM PNTask 
				WHERE PNTask.PlanID = @strPlanID 
				GROUP BY PlanID, ParentOutlineNumber

		DECLARE @PNTaskStartDate Datetime = NULL
		DECLARE @PNTaskEndDate Datetime = NULL
		 
		UPDATE T SET
			@PNTaskStartDate = CASE WHEN X.StartDate < T.StartDate THEN X.StartDate ELSE T.StartDate END, 
			@PNTaskEndDate = CASE WHEN X.EndDate > T.EndDate THEN X.EndDate ELSE T.EndDate END,
			StartDate = @PNTaskStartDate,
			EndDate = @PNTaskEndDate
			FROM PNTask AS T
			INNER JOIN @planParentMinMaxDates AS X ON T.PlanID = X.PlanID AND T.OutlineNumber = X.ParentOutlineNumber
			WHERE T.PlanID = @strPlanID AND T.OutlineLevel = @iLevel
			AND (X.StartDate < T.StartDate OR X.EndDate > T.EndDate)

		SET @iLevel = @iLevel - 1

	END
	--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	  
	  UPDATE T SET StartDate = CASE WHEN T.StartDate IS NULL AND T.EndDate IS NOT NULL THEN T.EndDate ELSE T.StartDate END,
			EndDate = CASE WHEN T.EndDate IS NULL AND T.StartDate IS NOT NULL THEN T.StartDate ELSE T.EndDate END
	  FROM PNTask AS T WHERE T.WBS1 = @strWBS1

	  -- While adding project if allocation metniod is r (use revenue forecast as allocation method) then sync plan dates with revenue dates
	  IF (@strAllocMethod = 'r')
	  BEGIN
		  UPDATE T SET RevenueStartDate = StartDate ,RevenueEndDate = EndDate
		  FROM PNTask AS T WHERE T.WBS1 = @strWBS1 
	  END

  --Update the LastPlanAction Field 
	 
  UPDATE PNPlan SET LastPlanAction = 'CREATED'  WHERE PlanID = @strPlanID

  -- Publish the PN Plan.
  EXECUTE dbo.stRPPublish @strPlanID, 0 
  
  IF (@autoCommit='Y') COMMIT

	

  SET NOCOUNT OFF
  

END -- stRPCreatePlanFromProj

 
GO
