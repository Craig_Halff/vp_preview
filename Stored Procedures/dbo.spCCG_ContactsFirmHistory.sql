SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_ContactsFirmHistory] (
	@ContactID   varchar(32), 
	@ClientIDOld varchar(32),
	@TitleOld    varchar (255)
)
AS 
/* 
Copyright (c) 2017 Central Consulting Group.  All rights reserved.
04/25/2017 David Springer
           This is based on a Contacts custom grid called Firm History with the following columns:
           FH Date, FH Company (character data type), FH Title 
           Call this from a CHANGE workflow when ClientID changes and ClientID.Old not empty
           Pass ContactID and ClientID.Old
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
BEGIN
-- Populate the Firm History grid
   Insert into Contacts_FirmHistory 
   (ContactID, Seq, custFHDate, custFHCompany, custFHTitle)
   Select @ContactID, Replace (NewID(), '-', '') , getDate(), CL.Name, @TitleOld
   From CLendor CL WITH (NOLOCK)
   Where CL.ClientID = @ClientIDOld

END

GO
