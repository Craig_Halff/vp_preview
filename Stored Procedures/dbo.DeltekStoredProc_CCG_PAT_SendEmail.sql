SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_SendEmail] ( @sourceApplication varchar(40), @from nvarchar(80), @to nvarchar(max), @subject nvarchar(120), @subjectbatch nvarchar(120), @body nvarchar(max), @delay int, @priority varchar(12), @ccSender bit)
             AS EXEC spCCG_PAT_SendEmail @sourceApplication,@from,@to,@subject,@subjectbatch,@body,@delay,@priority,@ccSender
GO
