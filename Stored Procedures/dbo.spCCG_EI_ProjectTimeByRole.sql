SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_ProjectTimeByRole] (
	@RptStoreId uniqueidentifier,
	@BeginStages varchar(255), @EndStages varchar(512),
	@BeginDate datetime, @EndDate datetime,
	@InclWeekends char(1)
)
AS BEGIN
/*
	Copyright (c) 2017 Central Consulting Group.  All rights reserved.

	select * from CCG_EI_History where WBS1='2003005.00' and ActionDate between '11/11/2013' and '11/24/2013' and ActionTaken='Stage Change' order by ActionDate
	declare @id uniqueidentifier
	set @id=newid()
	exec spCCG_EI_ProjectTimeByRole @id,'Review','Final,Do not bill this month','11/1/2013','11/30/2013','Y'
	select * from CCG_EI_RptStore where RptStoreId=@id
	set @id=newid()
	exec spCCG_EI_ProjectTimeByRole @id,'Review','Final,Do not bill this month','11/1/2013','11/30/2013','N'
	select * from CCG_EI_RptStore where RptStoreId=@id
*/
	set nocount on
	declare @sql Nvarchar(max), @field varchar(100), @name varchar(100), @count int, @MAXROLES int
	set @MAXROLES=6

	if IsNull(@BeginDate,'')='' set @BeginDate='1/1/1990'
	if IsNull(@EndDate,'')=''   set @EndDate=GETDATE()

	-- Get the names and Cust* fields of the EI roles:
	/* No good if collation different on the SQL instance:
	CREATE TABLE #CCG_EI_RptStoreTemp(
		RptStoreId		uniqueidentifier NOT NULL,
		Col1Field varchar(50), Col1Name varchar(50),
		Col2Field varchar(50), Col2Name varchar(50),
		Col3Field varchar(50), Col3Name varchar(50),
		Col4Field varchar(50), Col4Name varchar(50),
		Col5Field varchar(50), Col5Name varchar(50),
		Col6Field varchar(50), Col6Name varchar(50)
	) on [PRIMARY]*/
	select newid() as RptStoreId,
		Cast('' as varchar(50)) as Col1Field, Cast('' as varchar(50)) as Col1Name,
		Cast('' as varchar(50)) as Col2Field, Cast('' as varchar(50)) as Col2Name,
		Cast('' as varchar(50)) as Col3Field, Cast('' as varchar(50)) as Col3Name,
		Cast('' as varchar(50)) as Col4Field, Cast('' as varchar(50)) as Col4Name,
		Cast('' as varchar(50)) as Col5Field, Cast('' as varchar(50)) as Col5Name,
		Cast('' as varchar(50)) as Col6Field, Cast('' as varchar(50)) as Col6Name
	into #CCG_EI_RptStoreTemp
	where 1=2

	set @sql='insert into #CCG_EI_RptStoreTemp(RptStoreId, '
	set @sql = @sql + 'Col1Field, Col1Name, Col2Field, Col2Name, '
	set @sql = @sql + 'Col3Field, Col3Name, Col4Field, Col4Name, Col5Field, Col5Name, Col6Field, Col6Name)'
	set @sql = @sql + ' values (''' + RTrim(convert(char(255),@RptStoreId)) + ''''
	declare myCursor cursor fast_forward for
		select isnull(crd.ColumnLabel,cr.ColumnLabel) as ColumnLabel, VisionField
		from CCG_EI_ConfigRoles cr
		left join CCG_EI_ConfigRolesDescriptions crd on crd.[Role]=cr.[Role] and crd.UICultureName='en-US'
		where cr.[Status]='A'
		order by cr.ColumnOrder
	open myCursor
	fetch next from myCursor into @field, @name
	set @count=0
	while @@FETCH_STATUS = 0
	begin
		set @count = @count + 1
		if @count <= @MAXROLES
			set @sql = @sql + ', ''' + @field + ''',''' + @name + ''''
		fetch next from myCursor into @field, @name
	end
	close myCursor
	deallocate myCursor
	while @count < @MAXROLES
	begin
		set @count = @count + 1
		set @sql = @sql + ', null, null'
	end
	set @sql = @sql + ')'
	--print @sql
	execute (@sql)
	-- select * from #CCG_EI_RptStoreTemp where RptStoreId=@RptStoreId

	/* Split the begin and end stages strings */
	/* No good if collation different on the SQL instance:
	CREATE TABLE #CCG_EI_RptStoreTempStages(
		RptStoreId uniqueidentifier NOT NULL,
		BeginStage varchar(100),
		EndStage varchar(100)
	) on [PRIMARY]*/
	select newid() as RptStoreId, Cast('' as varchar(100)) as BeginStage, Cast('' as varchar(100)) as EndStage
	into #CCG_EI_RptStoreTempStages
	where 1=2

	declare @BeginStageString varchar(255), @EndStageString varchar(255), @pos int, @piece varchar(255)
	if IsNull(@BeginStages,'') <> ''
	begin
		set @BeginStageString = Replace(@BeginStages, ', ', ',')
		if Right(RTrim(@BeginStages),1) <> ','
		   set @BeginStageString = @BeginStageString + ','

		set @pos = PATINDEX('%,%', @BeginStageString)
		while @pos <> 0
		begin
		   set @piece = left(@BeginStageString, @pos - 1)
		   insert #CCG_EI_RptStoreTempStages (RptStoreId, BeginStage) values (@RptStoreId, @piece)
		   set @BeginStageString = stuff(@BeginStageString, 1, @pos, '')
		   set @pos =  patindex('%,%' , @BeginStageString)
		end
	end
	if IsNull(@EndStages,'') <> ''
	begin
		set @EndStageString = Replace(@EndStages, ', ', ',')
		if Right(RTrim(@EndStages),1) <> ','
		   set @EndStageString = @EndStageString + ','

		set @pos = PATINDEX('%,%', @EndStageString)
		while @pos <> 0
		begin
		   set @piece = left(@EndStageString, @pos - 1)
		   insert #CCG_EI_RptStoreTempStages (RptStoreId, EndStage) values (@RptStoreId, @piece)
		   set @EndStageString = stuff(@EndStageString, 1, @pos, '')
		   set @pos =  patindex('%,%' , @EndStageString)
		end
	end
	-- select * from #CCG_EI_RptStoreTempStages where RptStoreId=@RptStoreId

	-- Begin actually collecting and putting the data together:
	set @sql =
	' insert into CCG_EI_RptStore (RptStoreId, EndDate, EndStage, WBS1, ' +
	'	Col1Field, Col1Name, Col1Emp, Col1Value, Col2Field, Col2Name, Col2Emp, Col2Value, ' +
	'	Col3Field, Col3Name, Col3Emp, Col3Value, Col4Field, Col4Name, Col4Emp, Col4Value, ' +
	'	Col5Field, Col5Name, Col5Emp, Col5Value, Col6Field, Col6Name, Col6Emp, Col6Value) ' +
	' select ''' + Cast(@RptStoreId as varchar(36)) + ''', ActionDate as EndDate, eih.InvoiceStage, eih.WBS1,  ' +
	'	rst.Col1Field, rst.Col1Name, CustInvoiceGroup as Col1Emp, 0 as Col1Value,  ' +
	'	rst.Col2Field, rst.Col2Name, EM2.LastName + '', '' + isnull(EM2.FirstName,'''') as Col2Emp, 0 as Col2Value, ' +
	'	rst.Col3Field, rst.Col3Name, EM3.LastName + '', '' + isnull(EM3.FirstName,'''') as Col3Emp, 0 as Col3Value, ' +
	'	rst.Col4Field, rst.Col4Name, EM4.LastName + '', '' + isnull(EM4.FirstName,'''') as Col4Emp, 0 as Col4Value, ' +
	'	rst.Col5Field, rst.Col5Name, EM5.LastName + '', '' + isnull(EM5.FirstName,'''') as Col5Emp, 0 as Col5Value, ' +
	'	rst.Col6Field, rst.Col6Name, EM6.LastName + '', '' + isnull(EM6.FirstName,'''') as Col6Emp, 0 as Col6Value  ' +
	' from CCG_EI_History eih ' +
	' inner join #CCG_EI_RptStoreTempStages rsts on IsNull(rsts.EndStage,'''')=eih.InvoiceStage and rsts.RptStoreId=''' + Cast(@RptStoreId as varchar(36)) + ''' ' +
	' left join ProjectCustomTabFields PCTF on PCTF.WBS1=eih.WBS1 and PCTF.WBS2='' ''  '

	select @sql = @sql + ' left join EM EM2 on EM2.Employee=' + case When Col2Name is null Then '''''' Else 'PCTF.' + Col2Name End from #CCG_EI_RptStoreTemp where RptStoreId=@RptStoreId
	select @sql = @sql + ' left join EM EM3 on EM3.Employee=' + case When Col3Name is null Then '''''' Else 'PCTF.' + Col3Name End from #CCG_EI_RptStoreTemp where RptStoreId=@RptStoreId
	select @sql = @sql + ' left join EM EM4 on EM4.Employee=' + case When Col4Name is null Then '''''' Else 'PCTF.' + Col4Name End from #CCG_EI_RptStoreTemp where RptStoreId=@RptStoreId
	select @sql = @sql + ' left join EM EM5 on EM5.Employee=' + case When Col5Name is null Then '''''' Else 'PCTF.' + Col5Name End from #CCG_EI_RptStoreTemp where RptStoreId=@RptStoreId
	select @sql = @sql + ' left join EM EM6 on EM6.Employee=' + case When Col6Name is null Then '''''' Else 'PCTF.' + Col6Name End from #CCG_EI_RptStoreTemp where RptStoreId=@RptStoreId

	set @sql = @sql +
	' , #CCG_EI_RptStoreTemp rst ' +
	' where rst.RptStoreId=''' + Cast(@RptStoreId as varchar(36)) + ''' and ActionDate>=''' + convert(varchar(10),@BeginDate,101) + ''' and ActionDate<''' + convert(varchar(10),DateAdd(DAY,1,@EndDate),101) + ''' and eih.ActionTaken=''Stage Change'' '
	/*print @sql*/
	execute (@sql)

	/* Now update the BeginDate column */
	update CCG_EI_RptStore set BeginDate=MaxActionDate
	from CCG_EI_RptStore
	inner join (
		select rs.WBS1, Max(ActionDate) as MaxActionDate
		from CCG_EI_RptStore rs
		inner join CCG_EI_History eih on eih.WBS1=rs.WBS1 and eih.ActionDate < rs.EndDate
		inner join #CCG_EI_RptStoreTempStages rsts on IsNull(rsts.BeginStage,'')=eih.InvoiceStage and rsts.RptStoreId=@RptStoreId	/* Find the end stage */
		where rs.RptStoreId=@RptStoreId
		group by rs.WBS1
	) as maxdate on maxdate.WBS1=CCG_EI_RptStore.WBS1

	/* See if there is an earlier stage that is still after the begin date - e.g. 2 "Review" stages within the timeframe */
	update CCG_EI_RptStore set BeginDate=MinActionDate
	from CCG_EI_RptStore
	inner join (
		select rs.WBS1, Min(ActionDate) as MinActionDate
		from CCG_EI_RptStore rs
		inner join CCG_EI_History eih on eih.WBS1=rs.WBS1 and eih.ActionDate < rs.BeginDate and eih.ActionDate>=@BeginDate
		inner join #CCG_EI_RptStoreTempStages rsts on IsNull(rsts.BeginStage,'')=eih.InvoiceStage and rsts.RptStoreId=@RptStoreId	/* Find the end stage */
		where rs.RptStoreId=@RptStoreId
		group by rs.WBS1
	) as maxdate on maxdate.WBS1=CCG_EI_RptStore.WBS1

	/* Now update the BeginStage column */
	update rs set rs.BeginStage=eih.InvoiceStage
	from CCG_EI_RptStore rs
	inner join CCG_EI_History eih on eih.WBS1=rs.WBS1 and eih.ActionDate=rs.BeginDate

	declare @DateSQL varchar(300)
	if @InclWeekends='Y'
		set @DateSQL= ' Then Cast(DateDiff(minute,eih.PriorInvoiceStageDateSet,eih.ActionDate) as float)'
	else
		set @DateSQL= ' Then dbo.fnCCG_EI_GetBusinessDays(DateAdd(minute, DateDiff(minute, getutcdate(), getdate()),eih.PriorInvoiceStageDateSet),DateAdd(minute, DateDiff(minute, getutcdate(), getdate()),eih.ActionDate),dbo.GetActiveCompany())'

	set @sql =
	' update CCG_EI_RptStore set Col1Value=val1, Col2Value=val2, Col3Value=val3, Col4Value=val4, Col5Value=val5,Col6Value=val6 ' +
	' from CCG_EI_RptStore ' +
	' inner join ( ' +
	'	select eih.WBS1, SUM(Case When '
	if exists (select 'x' from #CCG_EI_RptStoreTemp where Col2Name is not null)
		select @sql = @sql + ' IsNull(PCTF.' + Col2Name + ','''')<>eih.ActionTakenBy ' from #CCG_EI_RptStoreTemp where RptStoreId=@RptStoreId
	if exists (select 'x' from #CCG_EI_RptStoreTemp where Col3Name is not null)
		select @sql = @sql + ' and IsNull(PCTF.' + Col3Name + ','''')<>eih.ActionTakenBy ' from #CCG_EI_RptStoreTemp where RptStoreId=@RptStoreId
	if exists (select 'x' from #CCG_EI_RptStoreTemp where Col4Name is not null)
		select @sql = @sql + ' and IsNull(PCTF.' + Col4Name + ','''')<>eih.ActionTakenBy ' from #CCG_EI_RptStoreTemp where RptStoreId=@RptStoreId
	if exists (select 'x' from #CCG_EI_RptStoreTemp where Col5Name is not null)
		select @sql = @sql + ' and IsNull(PCTF.' + Col5Name + ','''')<>eih.ActionTakenBy ' from #CCG_EI_RptStoreTemp where RptStoreId=@RptStoreId
	if exists (select 'x' from #CCG_EI_RptStoreTemp where Col6Name is not null)
		select @sql = @sql + ' and IsNull(PCTF.' + Col6Name + ','''')<>eih.ActionTakenBy ' from #CCG_EI_RptStoreTemp where RptStoreId=@RptStoreId
	set @sql = @sql + @DateSQL + ' Else 0 End) / 1440.0 as val1, '

	set @DateSQL = ','''')=eih.ActionTakenBy ' + @DateSQL + ' / 1440.0 Else 0 End) '
	if exists (select 'x' from #CCG_EI_RptStoreTemp where Col2Name is not null)
		select @sql = @sql + ' SUM(Case When IsNull(PCTF.' + Col2Name + @DateSQL + ' as val2, ' from #CCG_EI_RptStoreTemp where RptStoreId=@RptStoreId
	else
		set @sql = @sql + ' 0 as val2, '
	if exists (select 'x' from #CCG_EI_RptStoreTemp where Col3Name is not null)
		select @sql = @sql + ' SUM(Case When IsNull(PCTF.' + Col3Name + @DateSQL + ' as val3, ' from #CCG_EI_RptStoreTemp where RptStoreId=@RptStoreId
	else
		set @sql = @sql + ' 0 as val3, '
	if exists (select 'x' from #CCG_EI_RptStoreTemp where Col4Name is not null)
		select @sql = @sql + ' SUM(Case When IsNull(PCTF.' + Col4Name + @DateSQL + ' as val4, ' from #CCG_EI_RptStoreTemp where RptStoreId=@RptStoreId
	else
		set @sql = @sql + ' 0 as val4, '
	if exists (select 'x' from #CCG_EI_RptStoreTemp where Col5Name is not null)
		select @sql = @sql + ' SUM(Case When IsNull(PCTF.' + Col5Name + @DateSQL + ' as val5, ' from #CCG_EI_RptStoreTemp where RptStoreId=@RptStoreId
	else
		set @sql = @sql + ' 0 as val5, '
	if exists (select 'x' from #CCG_EI_RptStoreTemp where Col6Name is not null)
		select @sql = @sql + ' SUM(Case When IsNull(PCTF.' + Col6Name + @DateSQL + ' as val6  ' from #CCG_EI_RptStoreTemp where RptStoreId=@RptStoreId
	else
		set @sql = @sql + ' 0 as val6 '

	set @sql = @sql +
	'	from CCG_EI_History eih ' +
	'	inner join ProjectCustomTabFields PCTF on PCTF.WBS1=eih.WBS1 and PCTF.WBS2='' '' ' +
	'	inner join CCG_EI_RptStore rs on rs.WBS1=eih.WBS1 and eih.ActionDate>rs.BeginDate and eih.ActionDate<=rs.EndDate ' +
	'	where rs.RptStoreId=''' + Cast(@RptStoreId as varchar(36)) + ''' and eih.ActionTaken=''Stage Change''' +
	'	group by eih.WBS1 ' +
	' ) vals on vals.WBS1=CCG_EI_RptStore.WBS1 ' +
	' where CCG_EI_RptStore.RptStoreId=''' + Cast(@RptStoreId as varchar(36)) + ''' '
	/*print @sql*/
	execute (@sql)

	drop table #CCG_EI_RptStoreTemp
	drop table #CCG_EI_RptStoreTempStages
END
GO
