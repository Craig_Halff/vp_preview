SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Save_Stage_Rights]
	@fieldsSQL			Nvarchar(max),
	@newValuesSql		Nvarchar(max),
	@VISION_LANGUAGE	varchar(10)
AS
BEGIN
	/*
	DeltekStoredProc_CCG_EI_Config_Save_Stage_Rights
		@fieldsSQL = 'StageFlow, Stage, Role, CanView, CanSet, CanModify, PackageView, PackageModify, SendEmailOnStageSet, CCSender, Renotify, EmailSubject, EmailSubjectBatch, EmailMessage',
		@newValuesSql = '(''Approval Required'', ''Accounting'', ''Approver'', ''N'', ''N'', ''Y'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''Approval Required'', ''Re-Review'', ''Approver'', ''N'', ''Y'', ''N'', ''Y'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''Approval Required'', ''Invoice as is'', ''Approver'', ''Y'', ''N'', ''Y'', ''N'', ''N'', ''Y'', ''N'', 2, ''Invoice [:PR.WBS1] ready for approval'', ''Electronic Invoicing Reminder'', ''Please have your invoice(s) approved within 3 days.  [:LINK]''), (''Approval Required'', ''Invoice as noted'', ''Approver'', ''Y'', ''N'', ''Y'', ''N'', ''N'', ''Y'', ''N'', 2, ''Invoice [:PR.WBS1] ready for approval'', ''Electronic Invoicing Reminder'', ''Please have your invoice(s) approved within 3 days.''), (''Approval Required'', ''Do not bill this month'', ''Approver'', ''Y'', ''N'', ''Y'', ''N'', ''N'', ''Y'', ''N'', 2, ''Invoice [:PR.WBS1] ready for approval'', ''Electronic Invoicing Reminder'', ''Please have your invoice(s) approved within 3 days.''), (''Approval Required'', ''Approved'', ''Approver'', ''N'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''Approval Required'', ''Approved - DNI'', ''Approver'', ''N'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''Approval Required'', ''Write-off approval'', ''Approver'', ''Y'', ''N'', ''Y'', ''N'', ''N'', ''Y'', ''N'', 2, ''Invoice [:PR.WBS1] ready for write-off approval'', ''Electronic Invoicing Reminder'', ''Please have your invoice(s) approved within 3 days.''), (''Approval Required'', ''Write-off approved'', ''Approver'', ''N'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''Approval Required'', ''Review'', ''Reviewer'', ''Y'', ''N'', ''Y'', ''N'', ''N'', ''Y'', ''N'', 2, ''Invoice [:PR.WBS1] ready for review'', ''Electronic Invoicing Reminder'', ''Please have your invoice(s) reviewed within 3 days.  [:LINK]''), (''Approval Required'', ''Re-Review'', ''Reviewer'', ''Y'', ''N'', ''Y'', ''N'', ''N'', ''Y'', ''N'', 2, ''Invoice [:PR.WBS1] ready for review'', ''Electronic Invoicing Reminder'', ''Please have your invoice(s) reviewed within 3 days.  [:LINK]''), (''Approval Required'', ''Invoice as is'', ''Reviewer'', ''N'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''Approval Required'', ''Invoice as noted'', ''Reviewer'', ''N'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''Approval Required'', ''Do not bill this month'', ''Reviewer'', ''N'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''Approval Required'', ''Revise and return'', ''Reviewer'', ''N'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''DLR Group'', ''PM Input'', ''Reviewer'', ''Y'', ''N'', ''Y'', ''Y'', ''N'', ''Y'', ''N'', NULL, ''Please send invoicing information within 2 days'', ''Please Invoice'', ''Test''), (''DLR Group'', ''Invoice as is'', ''Reviewer'', ''N'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''DLR Group'', ''Invoice as noted'', ''Reviewer'', ''N'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''DLR Group'', ''Do not bill this month'', ''Reviewer'', ''N'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''DLR Group'', ''Send Draft'', ''Reviewer'', ''N'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''No Approval Required'', ''Re-Review'', ''Approver'', ''N'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''No Approval Required'', ''Write-off approval'', ''Approver'', ''Y'', ''N'', ''Y'', ''N'', ''N'', ''Y'', ''N'', 2, ''Invoice [:PR.WBS1] ready for write-off approval'', ''Electronic Invoicing Reminder'', ''Please have your invoice(s) approved within 3 days.''), (''No Approval Required'', ''Write-off approved'', ''Approver'', ''N'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''No Approval Required'', ''Accounting'', ''Reviewer'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''No Approval Required'', ''Review'', ''Reviewer'', ''Y'', ''N'', ''Y'', ''N'', ''N'', ''Y'', ''N'', 2, ''Invoice [:PR.WBS1] ready for review'', ''Electronic Invoicing Reminder'', ''Please have your invoice(s) reviewed within 3 days.  [:LINK]''), (''No Approval Required'', ''Re-Review'', ''Reviewer'', ''Y'', ''N'', ''Y'', ''N'', ''N'', ''Y'', ''N'', 2, ''Invoice [:PR.WBS1] ready for review'', ''Electronic Invoicing Reminder'', ''Please have your invoice(s) reviewed within 3 days.  [:LINK]''), (''No Approval Required'', ''Invoice as is'', ''Reviewer'', ''Y'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''No Approval Required'', ''Invoice as noted'', ''Reviewer'', ''Y'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''No Approval Required'', ''Do not bill this month'', ''Reviewer'', ''Y'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''No Approval Required'', ''Revise and return'', ''Reviewer'', ''Y'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''No Approval Required'', ''Write-off approval'', ''Reviewer'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''No Approval Required'', ''Write-off approved'', ''Reviewer'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', ''''), (''No Approval Required'', ''Final'', ''Reviewer'', ''Y'', ''N'', ''N'', ''N'', ''N'', ''N'', ''N'', NULL, '''', '''', '''')',
		@VISION_LANGUAGE = 'en-US'
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	int;			-- INT value indicates which parameter failed the security check
	DECLARE @ids		TABLE (id int);
	DECLARE @i			int;
	DECLARE @newI		int;
	DECLARE @seq		varchar(50);

	-- Ensure that our inputs are safe (preventing SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<CSV>', @fieldsSQL);
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<INSERT VALUES>', @newValuesSql) * 2;
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @VISION_LANGUAGE) * 4;
	IF @safeSql < 7 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS varchar(4));
		RETURN;
	END;

	BEGIN TRANSACTION;

	SET @sSQL = N'
		MERGE CCG_EI_ConfigRights WITH (HOLDLOCK) AS Target
		USING (VALUES ' + @newValuesSql + N'
			) As Source (' + @fieldsSQL + N')
			ON Target.StageFlow = Source.StageFlow
				AND Target.Stage = Source.Stage
				AND Target.Role = Source.Role
		WHEN MATCHED THEN
			UPDATE SET
				[Role]					= Source.[Role],
				[CanView]				= Source.[CanView],
				[CanModify]				= Source.[CanModify],
				[CanSet]				= Source.[CanSet],
				[SendEmailOnStageSet]	= Source.[SendEmailOnStageSet],
				[EmailSubject]			= Source.[EmailSubject],
				[EmailMessage]			= Source.[EmailMessage],
				[EmailSubjectBatch]		= Source.[EmailSubjectBatch],
				[CCSender]				= Source.[CCSender],
				[Renotify]				= Source.[Renotify],
				[PackageView]			= Source.[PackageView],
				[PackageModify]			= Source.[PackageModify]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (' + @fieldsSQL + N')
			VALUES (Source.' + REPLACE(@fieldsSQL, N', ', N', Source.') + N')
		WHEN NOT MATCHED BY SOURCE THEN DELETE;';
	--PRINT (@sSQL);
	EXEC (@sSQL);

	-- Do Stage Rights deletes (all empty ones)
	-- probably not necessary, but keeps the database cleaner
	DELETE FROM CCG_EI_ConfigRights
		WHERE CanView = 'N' and CanSet = 'N' and CanModify = 'N' and SendEmailOnStageSet = 'N';
	DELETE FROM CCG_EI_ConfigRightsDescriptions
		WHERE StageFlow+'::'+Role+'::'+Stage not in (
			SELECT StageFlow+'::'+Role+'::'+Stage FROM CCG_EI_ConfigRights
		);

	-- Update the multilingual descriptions table
	SET @sSQL = N'
		MERGE CCG_EI_ConfigRightsDescriptions WITH (HOLDLOCK) AS Target
		USING CCG_EI_ConfigRights AS Source
			ON Target.StageFlow = Source.StageFlow
				AND Target.Stage = Source.Stage
				AND Target.Role = Source.Role
		WHEN MATCHED AND Target.UICultureName = '''+@VISION_LANGUAGE+N''' THEN
			UPDATE SET
				Target.EmailSubject = ISNULL(Source.EmailSubject, ''''),
				Target.EmailSubjectBatch = ISNULL(Source.EmailSubjectBatch, ''''),
				Target.EmailMessage = ISNULL(Source.EmailMessage, '''')
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (StageFlow, Role, Stage, UICultureName,
				EmailSubject, EmailSubjectBatch,
				EmailMessage)
			VALUES (Source.StageFlow, Source.Role, Source.Stage, '''+@VISION_LANGUAGE+N''',
				ISNULL(Source.EmailSubject, ''''), ISNULL(Source.EmailSubjectBatch, ''''),
				ISNULL(Source.EmailMessage, ''''))
		WHEN NOT MATCHED BY SOURCE THEN DELETE;';
	EXEC (@sSQL);

	COMMIT TRANSACTION;
END;
GO
