SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_EI_Object_Exists]
	@objectName	varchar(512),
	@type		varchar(50),					-- Possible values: "SP", "FN", "TB"
	@isDeltekSP	bit = 0
AS
BEGIN
	-- DEBUG TEST: EXEC [dbo].[spCCG_EI_Object_Exists] 'fnCCG_PAT_CopyFrom', 'FN'
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	IF @isDeltekSP = 1 SET @objectName = 'DeltekStoredProc_CCG_EI_' + @objectName
	ELSE IF @type = 'SP' and Left(@objectName, 5) <> 'spCCG' SET @objectName = 'spCCG_EI_' + @objectName
	IF @type = 'SP' Or @type Is NULL
		SELECT 'x' as result
			FROM sys.objects
			WHERE object_id = OBJECT_ID(N'[dbo].[' + @objectName + ']') AND type in (N'P', N'PC')
	ELSE IF @type = 'FN'
		SELECT 'x' as result
			FROM sys.objects
			WHERE object_id = OBJECT_ID(N'[dbo].[' + @objectName + ']') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT')
	ELSE IF @type = 'U' OR @type = 'TB'
		SELECT 'x' as result
			FROM sys.objects
			WHERE object_id = OBJECT_ID(N'[dbo].[' + @objectName + ']') AND type in (N'U')
END
GO
