SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_PAT_StageChange](
	@PayableSeq int,								-- PayableSeq to itentify item
	@OldStage varchar(70), @NewStage varchar(70),	-- Old & new invoice stage values (keys)
	@Username varchar(255), @Role varchar(255))		-- Username & EI role
AS BEGIN
/*
	Copyright (c) 2015 Central Consulting Group. All rights reserved.
	exec spCCG_PAT_StageChange 6283, '', 'Ready for Review', '', ''
	--enable for accounting users
	select * from ccg_pat_payable where payablenumber = '3323' --6283
	select * from ccg_pat_projectamount 
*/
	set nocount on
	BEGIN TRY
		
		declare @ReturnValue int
		declare @ReturnMessage varchar(1024)
		set @ReturnValue = 0;
		set @ReturnMessage = ''
		declare @PayableType varchar(10), @sourceType varchar(10), @sourcePKey varchar(50)
		declare @chargetype varchar(1)
		select @PayableType = PayableType, @sourceType = SourceType, @sourcePKey = SourcePKey from CCG_PAT_Payable where Seq = @PayableSeq
		--select * from CCG_PAT_ConfigStages
		declare @StageType varchar(10) --'Next','Continue','Approve','Archive'
		declare @OldStageType varchar(10) --'Next','Continue','Approve','Archive'
		declare @StageSubType varchar(10)--'Ready'
		
		select @StageType = [TYPE], @StageSubType = SubType from CCG_PAT_ConfigStages where Stage = @NewStage
		select @OldStageType = [TYPE] from CCG_PAT_ConfigStages where Stage = @OldStage		
		
		declare @Employee varchar(20) = (select distinct top 1 Employee from SEUser where Username = @Username)
		declare @EmployeeEmail varchar(500) = (select distinct top 1 EMail from EM where Employee = @Employee)

        if @oldstage = 'Pending' 
        begin
			if @StageType = 'Next' and not exists (select 'x' from CCG_PAT_Pending where PayableSeq = @PayableSeq and Employee <> @Employee and Employee <> 'Accounting')
			begin 
				select @ReturnValue = -1, @ReturnMessage = 'No route exists.'
			end
			else begin
				if @PayableType <> 'P'
				begin
					if exists (select 'x' from CCG_PAT_Payable p 
								left join VEAccounting vea on vea.Vendor = p.Vendor
							where p.Seq = @PayableSeq and @PayableType = 'I' and 
								(isnull(p.LiabCode,'') = '' or (
										isnull(p.PayDate,'') = '' and isnull(isnull(p.Payterms, vea.PayTerms), '') not in ('PWP', 'Next', 'Hold')
									) or isnull(p.PayTerms,'') = '' 
									or isnull(p.BankCode,'') = '' or isnull(p.ControlAmount, 0) = 0))					-- Invoice 
						select @ReturnValue = -1, @ReturnMessage = 'Required fields must be set: Liability Code, Bank Code, Payment Terms, Control Amount.'
					else if exists (select 'x' from CCG_PAT_Payable p													-- Disbursement
							where p.Seq = @PayableSeq and @PayableType = 'D' and (isnull(p.BankCode,'') = '' or isnull(p.ControlAmount, 0) = 0))
						select @ReturnValue = -1, @ReturnMessage = 'Required fields must be set: Bank Code and Control Amount.'
					else if exists (
							select 'x' from CCG_PAT_Payable p
							inner join CCG_PAT_ProjectAmount pa on pa.PayableSeq = p.Seq
							inner join PR on PR.WBS1 = pa.WBS1 and PR.WBS2 = ' ' and PR.WBS3 = ' '
							where p.Seq = @PayableSeq and ((p.Company = '02' and Left(PR.Org, 2) <> '02') or (p.Company <> '02' and Left(PR.Org, 2) = '02'))
						)
						select @ReturnValue = -1, @ReturnMessage = 'Garcia Land Data projects are Company specific.'
					else 
						update pa 
							set pa.bankcode = '212.20'
							--select 'Invoice : ' + pa.payablenumber
							from ccg_pat_payable pa
								inner join ccg_pat_projectamount pat on pa.seq = pat.PayableSeq
								inner join VendorCustomTabFields vct on vct.Vendor = pa.Vendor						
							where payableseq = @PayableSeq and vct.CustVirtualCardEnrolled = 'Y'
								and isnull(pat.description,' ') = ' ' and pa.Company = '01'
				end
			end			
        end 

		if @ReturnValue = 0 and @NewStage = 'Forward' and not exists (select 'x' from CCG_PAT_Pending where PayableSeq = @PayableSeq and Employee <> @Employee and Employee <> 'Accounting')
		begin 
			select @ReturnValue = -1, @ReturnMessage = 'Need to identify who to forward to.'
		end
		
		if @ReturnValue = 0 and @NewStage = 'Accounting Verified' 
		begin
			if @OldStageType <> 'Approve' and @OldStageType <> 'AdminHold'
				select @ReturnValue = -1, @ReturnMessage = 'Accounting Verified may only be selected from an Approve stage'
			else if exists (select 'x' from CCG_PAT_Payable p 
						left join CCG_PAT_ProjectAmount pa on pa.PayableSeq = p.Seq
						left join VEAccounting vea on vea.Vendor = p.Vendor
					where p.Seq = @PayableSeq and p.PayableType <> 'D' and (isnull(p.LiabCode,'') = '' 
						or isnull(p.BankCode,'') = '' or isnull(p.PayTerms,'') = '' or (
									isnull(p.PayDate,'') = '' and isnull(isnull(p.Payterms, vea.PayTerms), '') not in ('PWP', 'Next', 'Hold')
									)
						or isnull(p.ControlAmount, 0) = 0 or isnull(pa.GLAccount,'') = '')
				)
				select @ReturnValue = -1, @ReturnMessage = 'Required fields must be set: Liability Code, Bank Code, Payment Terms, Payment Date, Control Amount, GL Account.'
			else if exists (select 'x' from CCG_PAT_Payable p 
						left join CCG_PAT_ProjectAmount pa on pa.PayableSeq = p.Seq
						left join VEAccounting vea on vea.Vendor = p.Vendor
					where p.Seq = @PayableSeq and p.PayableType = 'D' and (isnull(p.BankCode,'') = ''
						or isnull(p.CheckNumber,'') = '' or isnull(p.ControlAmount, 0) = 0 or isnull(pa.GLAccount,'') = '')
				)
				select @ReturnValue = -1, @ReturnMessage = 'Required fields must be set: Bank Code, Check Number, Control Amount, GL Account.'
		end

		if @ReturnValue = 0 and @StageType = 'Next'
		begin
			if not exists(select status from CCG_PAT_Payable p inner join VE on p.Vendor = VE.Vendor inner join VEAccounting vea on VE.Vendor = vea.Vendor and vea.Company = p.Company where p.seq = @PayableSeq and VE.Status <> 'D')
				select @ReturnValue = -1, @ReturnMessage = 'Vendor is dormant or not approved for processing.'
		end		

		if @ReturnValue = 0 and @StageType = 'Approve' 
		begin
			
			if not exists (select 'y' from CCG_PAT_ProjectAmount where PayableSeq = @PayableSeq and ISNULL(WBS1,'') <> '')
				select @ReturnValue = -1, @ReturnMessage = 'Project detail is required.'
			
			else if @PayableType in ('I','D') and exists(
                           select 'y' from CCG_PAT_ProjectAmount pa
                           left join PR on pa.WBS1 = PR.WBS1 and pa.WBS2 = PR.WBS2  and pa.WBS3 = PR.WBS3
                           WHERE pa.PayableSeq = @PayableSeq /*and  ((ISNULL(PR.SubLevel,'Y') = 'Y'*/ and ISNULL(PR.ChargeType,'R') = 'R' and pa.wbs1 = ' ' --missing, invalid, or not lowest
                     ) 
                           select @ReturnValue = -1, @ReturnMessage = 'Project required on every row.  Please enter phase information before proceeding, if uncertain enter phase 00.'

			else if @sourceType = 'U'
				update UDIC_PurchaseOrder set CustApprovedBy = @Employee where UDIC_UID = @sourcePKey
		end

		
		if @ReturnValue = 0 and @NewStage Like '%Rejected%' /*update with actual stage keys and field names below*/
		begin
		--select top 10 * from CCG_PAT_CustomColumns
			if @PayableType in ('I','D') and exists(
				select 'y' from CCG_PAT_CustomColumns pacc
				WHERE pacc.PayableSeq = @PayableSeq and  isnull(pacc.Comments ,'') = ''
			)
				select @ReturnValue = -1, @ReturnMessage = 'Comments are required for this Stage.'

			if @PayableType in ('I','D') and not exists(
				select 'y' from CCG_PAT_CustomColumns pacc
				WHERE pacc.PayableSeq = @PayableSeq 
			)
				select @ReturnValue = -1, @ReturnMessage = 'Comments are required for this Stage.'
				
		end

		if @ReturnValue = 0 and @StageSubType = 'Ready'
		begin
			if exists(select 'p' from CCG_PAT_Pending where PayableSeq = @PayableSeq)
				select @ReturnValue = -1, @ReturnMessage = 'Pending Approvals Found' 
			--select * from CCG_PAT_History
			else if not exists(select 'a' from CCG_PAT_History where PayableSeq = @PayableSeq and ActionTaken = 'Stage Change' and Stage in (select Stage from CCG_PAT_ConfigStages where [TYPE] = 'Approve'))
				select @ReturnValue = -1, @ReturnMessage = 'No Approvals Found' 
			else if exists(select ActionTakenBy,max(case when h.ApprovedDetail=p.ApprovedDetail then 1 else 0 end) as approved   from CCG_PAT_History h inner join CCG_PAT_Payable p  on p.seq = @PayableSeq and p.seq = h.PayableSeq 
					where h.ApprovedDetail is not null  group by ActionTakenBy having max(case when h.ApprovedDetail=p.ApprovedDetail then 1 else 0 end) = 0)
				select @ReturnValue = 0, @ReturnMessage = 'Possible change detected after approval' 

		end

		-- Send email updates and notifications
		declare @subject varchar(max), @body varchar(max)
		if @ReturnValue = 0 and @OldStage = 'Pending' and @NewStage = 'Ready for Approval' 
		begin
			declare @id varchar(40), @approver varchar(500), @EmployeeName varchar(500), @vendor varchar(500), @requesterEmail varchar(500)
			if @sourceType = 'SP' 
			begin
				select @id = i.CustNumber, 
						@approver = isnull(isnull(EM_A.PreferredName, EM_A.FirstName) + ' ' + EM_A.LastName, ''),
						@vendor = isnull(VE.Name,''),
						@requesterEmail = EM_R.EMail
					from UDIC_CheckRequest i 
						outer apply (select top 1 p.Employee 
							from CCG_PAT_Pending p 
							where p.PayableSeq = @PayableSeq order by p.SortOrder) as p					
						left join EM EM_A on EM_A.Employee = p.Employee --i.CustCheckRequestApprover 
						left join VE on i.CustCheckRequestPayee = VE.Vendor
						left join EM EM_R on EM_R.Employee = i.CustCheckRequestRequestedby
					where UDIC_UID = @sourcePKey
				set @subject = 'Check Request Routed ('+@vendor+' – '+@id+')'
				set @body = 'Your check request has been routed for approval to '+@approver+'.'
			end
			else if @sourceType = 'PD'
			begin
				select @id = i.CustNumber, 
						@approver = isnull(isnull(EM_A.PreferredName, EM_A.FirstName) + ' ' + EM_A.LastName, ''),
						@EmployeeName = isnull(isnull(EM_B.PreferredName, EM_B.FirstName) + ' ' + EM_B.LastName, ''),
						@requesterEmail = EM_R.EMail
					from UDIC_PerDiemRequest i 
						left join EM EM_A on EM_A.Employee = i.CustApprover
						left join EM EM_B on EM_B.Employee = i.CustEmployee 
						left join EM EM_R on EM_R.Employee = i.CustRequestedBy
					where UDIC_UID = @sourcePKey
				set @subject = 'Per Diem Request Routed ('+@EmployeeName+' – '+@id+')'
				set @body = 'Your per diem request has been routed for approval to '+@approver+'.'
			end
			else if @sourceType = 'U'
			begin
				select @id = i.CustPONumber, 
						@approver = isnull(isnull(EM_A.PreferredName, EM_A.FirstName) + ' ' + EM_A.LastName, ''),
						@vendor = isnull(VE.Name,''),
						@requesterEmail = EM_R.EMail
					from UDIC_PurchaseOrder i 
						left join EM EM_A on EM_A.Employee = i.CustApprover 
						left join VE on i.CustVendor = VE.Vendor
						left join EM EM_R on EM_R.Employee = i.CustRequestedBy
					where UDIC_UID = @sourcePKey				
				set @subject = 'Purchase Order Request Routed ('+@vendor+' – '+@id+')'
				set @body = 'Your purchase order request has been routed for approval to '+@approver+'.'
			end
		end
		if @subject is not null 
		begin
			insert into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, BCCList, GroupSubject, [Subject], Body)
				values ('Payables Approval Tracking', isnull(@EmployeeEmail, ''), isnull(@requesterEmail, ''), '', '', '', @subject, @body)
		end

		select @ReturnValue as ReturnValue,@ReturnMessage as ReturnMessage
	END TRY
	BEGIN CATCH
		select -1 as  ReturnValue, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ReturnMessage
	END CATCH
END
GO
