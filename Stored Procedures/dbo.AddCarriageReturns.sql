SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AddCarriageReturns]
AS
BEGIN
   DECLARE @tabname nvarchar(100)
   DECLARE @colname nvarchar(100)

   DECLARE varchars INSENSITIVE CURSOR FOR
    SELECT sys.objects.name AS tabname, sys.columns.name AS colname
     FROM sys.objects INNER JOIN sys.columns 
       ON sys.objects.object_id=sys.columns.object_id INNER JOIN sys.types
       ON sys.types.system_type_id=sys.columns.system_type_id
    WHERE sys.objects.type='U' 
      AND (sys.types.name='nvarchar' or sys.types.name = 'varchar')
      AND sys.columns.max_length >= 50
   
   OPEN varchars
   
   FETCH NEXT FROM varchars INTO @tabname, @colname
   
   WHILE @@FETCH_STATUS = 0
      BEGIN
	 EXECUTE ('UPDATE ' + @tabname + 
		  ' SET ' + @colname + ' = REPLACE(' + @colname + ',char(10),char(13) + char(10))' +
		  ' WHERE CHARINDEX(char(10),' + @colname + ') > 0' +
		  ' AND CHARINDEX(char(13) + char(10),' + @colname + ') = 0')
         FETCH NEXT FROM varchars INTO @tabname, @colname
      END
   
   CLOSE varchars
   DEALLOCATE varchars
END
GO
