SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_Load_Docs_For_Wbs]
	@wbs1		Nvarchar(30)
AS
BEGIN
	-- TEST: EXEC [dbo].[spCCG_EI_Load_Docs_For_Wbs] '2003005.00'
	SET NOCOUNT ON;

	SELECT em.Employee, dpr.*
		FROM CCG_DOCS_PR dpr
			LEFT JOIN SEUser on dpr.createuser = seuser.Username
			LEFT JOIN EM on SEUser.Employee = EM.Employee
		WHERE WBS1 = @wbs1 and dpr.Category = 'ADD_DOCS'

END
GO
