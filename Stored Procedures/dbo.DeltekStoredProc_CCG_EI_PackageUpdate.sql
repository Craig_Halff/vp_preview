SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_PackageUpdate] ( @wbs1 nvarchar(32), @documentId varchar(300), @newOrder int)
             AS EXEC spCCG_EI_PackageUpdate @wbs1,@documentId,@newOrder
GO
