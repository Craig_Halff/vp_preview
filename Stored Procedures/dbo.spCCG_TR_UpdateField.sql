SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_TR_UpdateField](@Username varchar(32), @TableName varchar(10), 
	@Period int, @PostSeq int, @PKey varchar(32),
	@TransDate datetime, @Desc1 varchar(80), @Desc2 varchar(80), @Comment varchar(max), @ApprovalComment varchar(255), @FNStatus varchar(2),
	@NewPKey varchar(32) = ''
)
AS BEGIN

	declare @sql varchar(max), @emp varchar(32), @whereClause varchar(200)
	select @emp=Employee from SEUser where Username=@Username
	
	SET @sql = 'update ' + @TableName + ' set '
	SET @whereClause = ' where Period=' + Cast(@Period as varchar) + ' and PostSeq=' + Cast(@PostSeq as varchar) + ' and PKey=''' + @PKey + ''''

	IF @NewPKey = '' SET @NewPKey = Left(Replace(Convert(varchar(255), newid()),'-',''),31)

	if @TableName='LD' or @TableName='BILD'
	begin		
		If @TableName='LD' BEGIN	-- IF LD Table...
			-- We want to add a corresponding updated record to BILD
			
			declare @commonColumns varchar(2000), @nonCommonColumns varchar(1000), 
				@transDateQuoted varchar(100), @commentQuoted varchar(max)

			set @nonCommonColumns = 'Comment,TransDate,PKey,TimekeeperEndDate,'
			select @commonColumns = STUFF((
				SELECT ',' + c.name
					FROM sys.columns c
					WHERE c.object_id = OBJECT_ID('BILD') and c.name in (select cols.name from sys.columns cols where cols.object_id=OBJECT_ID('LD'))
					  and charindex(','+c.name+',', ','+@nonCommonColumns) < 1
					FOR XML PATH('')), 1, 1, '')
			
			IF @Comment is null Set @commentQuoted = 'NULL'
			ELSE Set @commentQuoted = '''' + @Comment + ''''

			IF IsNull(@TransDate, '') = '' Set @transDateQuoted = 'NULL'
			ELSE Set @transDateQuoted = '''' + Convert(varchar,@TransDate,101) + ''''

			set @sql = ' 
				insert into BILD (' + @nonCommonColumns + @commonColumns + ') 
					select IsNull(' + @commentQuoted + ', LD.Comment), 
						IsNull(' + @transDateQuoted + ', LD.TransDate), 
						''' + @NewPKey + ''', tc.EndDate, ' + @commonColumns + '
					from LD 
						LEFT JOIN tkControl tc ON tc.StartDate <= LD.TransDate AND tc.EndDate >= LD.TransDate
						' + @whereClause + '; '
					 
			
			set @sql = @sql + '; exec spCCG_TR_InsertApproval ''LD'',' + Cast(@Period as varchar) + ',' + 
			Cast(@PostSeq as varchar) + ',''' + @PKey + ''',null,''' + IsNull(@emp,'') + ''',''' + 
			IsNull(@ApprovalComment,'') + ''',null,null,''' + IsNull(@FNStatus,'') + ''' ';
			-- LD remains unchanged other than BillStatus, change this after the approval so bill status M does not get copied into History
			set @sql = @sql + 'update LD set BillStatus = ''M'' ' + @whereClause	
			
			set @sql = @sql +
			' ; update CCG_TR_History set NewTable=''BILD'',  NewPKey=''' + @NewPKey + ''',NewPostSeq = OriginalPostSeq, NewPeriod = OriginalPeriod where OriginalTable=''LD'' and OriginalPeriod=' + Cast(@Period as varchar) + ' and OriginalPostSeq=' + Cast(@PostSeq as varchar) + ' and OriginalPKey=''' + @PKey + '''  '
			
			set @sql = @sql +
			' ; update CCG_TR_Approvals set TableName=''BILD'',  PKey=''' + @NewPKey + ''' ' + @whereClause + ' and  TableName=''LD'' '
			set @sql = @sql +
			' ; update CCG_TR_ApprovalsHistory set TableName=''BILD'',  PKey=''' + @NewPKey + ''' ' + @whereClause + ' and  TableName=''LD'' '
			set @sql = @sql +
				' ; insert into CCG_TR_History (OriginalTable, OriginalPeriod, OriginalPostSeq, OriginalPKey, NewTable, NewPeriod, NewPostSeq, NewPKey, ActionStatus, ActionTaken, ActionDetail, ActionDate, ActionTakenBy, WBS1, WBS2, WBS3, OldValue, NewValue, OldValueDecimal, NewValueDecimal, ModificationComment, ApprovalComment, DateLastNotificationSent) ' +
				' select ''BILD'', OriginalPeriod, OriginalPostSeq, ''' + @NewPKey + ''', null, null, null, null, ActionStatus, ActionTaken, ActionDetail, ActionDate, ActionTakenBy, WBS1, WBS2, WBS3, OldValue, NewValue, OldValueDecimal, NewValueDecimal, ModificationComment, ApprovalComment, DateLastNotificationSent ' +
					' from CCG_TR_History where OriginalTable=''LD'' and OriginalPeriod=' + Cast(@Period as varchar) + ' and OriginalPostSeq=' + Cast(@PostSeq as varchar) + ' and OriginalPKey=''' + @PKey + '''  '


		END
		ELSE BEGIN	-- IF BILD Table...
			set @sql = @sql + ' Pool=Pool '
			if IsNull(@TransDate, '') <> ''
				set @sql = @sql + ', TransDate=''' + Convert(varchar,@TransDate,101) + ''''
			if @Comment is not null 
				set @sql = @sql +', Comment=''' + @Comment + ''''

			set @sql = @sql + @whereClause
			
			set @sql = @sql + '; exec spCCG_TR_InsertApproval ''' + @TableName + ''',' + Cast(@Period as varchar) + ',' + 
			Cast(@PostSeq as varchar) + ',''' + @PKey + ''',null,''' + IsNull(@emp,'') + ''',''' + 
			IsNull(@ApprovalComment,'') + ''',null,null,''' + IsNull(@FNStatus,'') + ''' ';

		END
	end 
	else begin
		set @sql = @sql + ' LinkWBS3=LinkWBS3 '
		if @TransDate is not null 
			set @sql = @sql +', TransDate=''' + Convert(varchar,@TransDate,101) + ''''
		if @Desc1 is not null
			set @sql = @sql + ', Desc1=''' + @Desc1 + ''''
		if @Desc2 is not null
			set @sql = @sql + ', Desc2=''' + @Desc2 + ''''

		set @sql = @sql + @whereClause
		
		set @sql = @sql + '; exec spCCG_TR_InsertApproval ''' + @TableName + ''',' + Cast(@Period as varchar) + ',' + 
		Cast(@PostSeq as varchar) + ',''' + @PKey + ''',null,''' + IsNull(@emp,'') + ''',''' + 
		IsNull(@ApprovalComment,'') + ''',null,null,''' + IsNull(@FNStatus,'') + ''' ';

	end

	--print @sql;
	execute(@sql)
END
GO
