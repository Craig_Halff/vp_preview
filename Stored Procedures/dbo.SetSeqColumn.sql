SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SetSeqColumn]
  @TName AS sysname,
  @SortCol AS sysname,
  @KeyCol1 AS sysname,
  @KeyCol2 AS sysname,
  @KeyCol3 AS sysname,
  @UICultureName AS varchar(10) = ''
AS
BEGIN

SET NOCOUNT ON

DECLARE @Sql1 AS Nvarchar(1000),
        @Key1 AS Nvarchar(100),
        @Key2 AS Nvarchar(100),
        @Key3 AS Nvarchar(100),
        @Seq AS smallint

SET @Seq = 1
SET @Sql1 = 'SELECT ' + @KeyCol1
IF @KeyCol2 <> ''
  SET @Sql1 = @Sql1 + ' , ' + @KeyCol2
IF @KeyCol3 <> ''
  SET @Sql1 = @Sql1 + ' , ' + @KeyCol3
SET @Sql1 = @Sql1 + ' FROM ' + @TName
IF @UICultureName <> ''
  SET @Sql1 = @Sql1 + ' WHERE UICultureName = ''' + @UICultureName + ''''
SET @Sql1 = @Sql1  + ' ORDER BY ' + @SortCol
EXECUTE ('DECLARE curCT CURSOR FOR ' + @Sql1)
OPEN curCT
IF @KeyCol3 <> ''
  FETCH NEXT FROM curCT INTO @Key1, @Key2, @Key3
ELSE
  IF @KeyCol2 <> ''
    FETCH NEXT FROM curCT INTO @Key1, @Key2
  ELSE
    FETCH NEXT FROM curCT INTO @Key1
WHILE @@FETCH_STATUS = 0
BEGIN
  SET @Sql1 = 'UPDATE ' + @TName + ' SET Seq = ' + CAST(@Seq AS VARCHAR(4)) + ' WHERE ' + @KeyCol1  + ' = N' + '''' + REPLACE(@Key1,'''','''''') +  ''''
  IF @KeyCol2 <> ''
    SET @Sql1 = @Sql1 + ' AND ' + @KeyCol2  + ' = N' + '''' + REPLACE(@Key2,'''','''''') +  ''''
  IF @KeyCol3 <> ''
    SET @Sql1 = @Sql1 + ' AND ' + @KeyCol3  + ' = N' + '''' + REPLACE(@Key3,'''','''''') +  ''''
  EXECUTE (@Sql1)
  SET @Seq = @Seq + 1
  IF @KeyCol3 <> ''
    FETCH NEXT FROM curCT INTO @Key1, @Key2, @Key3
  ELSE
    IF @KeyCol2 <> ''
      FETCH NEXT FROM curCT INTO @Key1, @Key2
    ELSE
      FETCH NEXT FROM curCT INTO @Key1
END
CLOSE curCT
DEALLOCATE curCT

END
GO
