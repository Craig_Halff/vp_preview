SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectDescriptionHistory]
	@WBS1			varchar(32),
	@Category		varchar (255), 
	@DescriptionOld	varchar (8000),
	@DescriptionNew	varchar (8000), 
	@ModUser		varchar (225)
AS 
/* 
Copyright (c) 2018 Central Consulting Group.  All rights reserved.
11/19/2019	David Springer
			Write description to Project Description History grid
			Call this on Project Descriptions grid INSERT or CHANGE workflow.
01/23/2020	David Springer
			Added update to Role Description
*/
BEGIN
SET NOCOUNT ON

	Insert Into Projects_DescriptionHistory
	(WBS1, WBS2, WBS3, Seq, CustDescriptionHistoryDate, CustDescriptionHistoryCategory, CustDescriptionHistoryOld, CustDescriptionHistoryNew, CustDescriptionHistoryModUser)
	Select @WBS1, ' ', ' ', Replace (NewID(), '-', ''), getDate(), c.Description, @DescriptionOld, @DescriptionNew, dbo.fnCCG_UserEmployeeNo (@ModUser)
	From CFGDescriptionCategory c
	Where Code = @Category

	UPDATE dbo.EMProjectAssoc
	Set RoleDescription = @DescriptionNew
	Where WBS1 = @WBS1
	  and WBS2 = ' '
	  and RoleDescription = @DescriptionOld

END

GO
