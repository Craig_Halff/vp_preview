SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Upsert_Invoice_Group]
	@currentInvoiceGroup	varchar(100),
	@sThruPeriod			varchar(20),
	@thruDate				varchar(100)
AS
BEGIN
	/*
	DeltekStoredProc_CCG_EI_Upsert_Invoice_Group
		@currentInvoiceGroup = 'Amy',
		@sThruPeriod = '',
		@thruDate = '12/31/2015'
	*/

	SET NOCOUNT OFF;

	SET @sThruPeriod = NULLIF(@sThruPeriod, '');
	SET @thruDate = NULLIF(@thruDate, '');
	DECLARE @thruPeriod Int = (CASE WHEN @sThruPeriod = 'NULL' THEN -1 ELSE Cast(@sThruPeriod As Int) END);

	If Not Exists (
		Select InvoiceGroup, ThruPeriod, ThruDate
			From CCG_EI_ConfigInvoiceGroups
			Where InvoiceGroup = @currentInvoiceGroup
	)
		INSERT INTO CCG_EI_ConfigInvoiceGroups (InvoiceGroup, ThruPeriod, ThruDate)
			Values (@currentInvoiceGroup, NULLIF(@thruPeriod,-1), @thruDate);
	ELSE
		UPDATE CCG_EI_ConfigInvoiceGroups
			SET ThruPeriod = NULLIF(ISNULL(@thruPeriod, ThruPeriod),-1),
				ThruDate = NULLIF(ISNULL(@thruDate, ThruDate),'NULL')
			WHERE InvoiceGroup = @currentInvoiceGroup;
END;

GO
