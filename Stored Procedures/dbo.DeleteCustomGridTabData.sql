SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteCustomGridTabData] @sInfoCenter nvarchar(60), @sKey1 nvarchar(60), @sKey2 nvarchar(60), @sKey3 nvarchar(60)
AS
DECLARE @SqlStmt nvarchar(max)
DECLARE @WhereClause nvarchar(2000)
DECLARE @sTableName nvarchar(100)
DECLARE @isTemplate varchar(1)
DECLARE @isWorkFlowActivity varchar(1)
DECLARE @originalInfoCenter varchar(30)
DECLARE @key1Varchar varchar(60)
DECLARE @parameterVarchar nvarchar(100) = '@key1 varchar(60)'
DECLARE @parameterNvarchar nvarchar(100) = '@key1 nvarchar(60)'

SET @isTemplate = 'N'
SET @isWorkFlowActivity = 'N'
SET @originalInfoCenter = @sInfoCenter

IF @sInfoCenter = 'ProjectsTemplate'
   BEGIN
	SET @isTemplate = 'Y'
	SET @sInfoCenter = 'Projects'
   END
ELSE IF @sInfoCenter = 'WorkflowActivityAction'
   BEGIN
      SET @isWorkFlowActivity = 'Y'
	SET @sInfoCenter = 'Activity'
   END
ELSE IF @sInfoCenter = 'WorkflowActivityEvent'
   BEGIN
      SET @isWorkFlowActivity = 'Y'
	SET @sInfoCenter = 'Activity'
   END


  DECLARE curTableList CURSOR LOCAL FOR
      SELECT TableName      
      FROM FW_CustomGridsData
      WHERE InfoCenterArea = @sInfoCenter


IF @isWorkFlowActivity = 'Y'
   SET @sInfoCenter = @originalInfoCenter


IF @sInfoCenter = 'ChartOfAccounts'
      BEGIN
         SET @WhereClause = ' WHERE Account = @key1' --nvarchar
      END
ELSE IF @sInfoCenter = 'Clients'
      BEGIN
         SET @WhereClause = ' WHERE ClientID = @key1'
      END
ELSE IF @sInfoCenter = 'Contacts'
      BEGIN
         SET @WhereClause = ' WHERE ContactID = @key1'
      END
ELSE IF @sInfoCenter = 'Employees'
      BEGIN
         SET @WhereClause = ' WHERE Employee = @key1' --nvarchar
      END
ELSE IF @sInfoCenter = 'Projects'
      BEGIN
         SET @WhereClause = ' WHERE WBS1 = @key1' --nvarchar 
         If @sKey2 <> ' '
		 BEGIN
			SET @WhereClause = @WhereClause + ' AND WBS2 = @key2'
			SET @parameterNvarchar = @parameterNvarchar + ',@key2 nvarchar(60)'
			IF @sKey3 <> ' '
			BEGIN
				SET @WhereClause = @WhereClause + ' AND WBS3 = @key3'
				SET @parameterNvarchar = @parameterNvarchar + ',@key3 nvarchar(60)'
			END
		 END
      END
ELSE IF @sInfoCenter = 'Vendors'
      BEGIN
         SET @WhereClause = ' WHERE Vendor = @key1' --nvarchar
      END
ELSE IF @sInfoCenter = 'TextLibrary'
      BEGIN
         SET @WhereClause = ' WHERE Name = @key1'
      END
ELSE IF @sInfoCenter = 'Units'
      BEGIN
         SET @WhereClause = ' WHERE UnitTable = @key1' --nvarchar
      END
ELSE IF @sInfoCenter = 'Leads'
      BEGIN
         SET @WhereClause = ' WHERE LeadID = @key1'
      END
ELSE IF @sInfoCenter = 'MktCampaigns'
      BEGIN
         SET @WhereClause = ' WHERE CampaignID = @key1'
      END
ELSE IF @sInfoCenter = 'ProjectPlan'
      BEGIN
         SET @WhereClause = ' WHERE PlanID = @key1'
      END
ELSE IF @sInfoCenter = 'Activity'
      BEGIN
         SET @WhereClause = ' WHERE ActivityID = @key1'
      END
ELSE IF @sInfoCenter = 'WorkflowActivityAction'
	BEGIN
	   SET @WhereClause = ' WHERE ActivityID in (SELECT ActivityID FROM WorkflowActivity WHERE ActionID = @key1)'
	END
ELSE IF @sInfoCenter = 'WorkflowActivityEvent'
	BEGIN
         SET @WhereClause = ' WHERE ActivityID in (SELECT ActivityID FROM WorkflowActivity WHERE ActionID in (SELECT ActionID FROM WorkflowActions WHERE EventID = @key1))'
	END


  OPEN curTableList;

   -- Create individual select statements
   -- sp_executesql is used to parameterize the queries and allow reuse of query plans
   -- nvarchar is used to pass in keys but varchar key queries convert the key to varchar so that
   -- sql server does not need to implicitly convert.

  FETCH NEXT FROM curTableList INTO @sTableName

   WHILE @@FETCH_STATUS = 0            
      BEGIN
		IF @isTemplate = 'Y'
			Set @sTableName = @sTableName + 'Template'
        ELSE IF @isWorkflowActivity = 'Y'
			Set @sTableName = 'Workflow' + @sTableName

		SET @SqlStmt = 'DELETE FROM ' + @sTableName + @WhereClause
		IF (@sInfoCenter IN ('Clients','Contacts','TextLibrary','Leads','MktCampaigns','ProjectPlan',
								'Activity','WorkflowActivityAction','WorkflowActivityEvent')) --varchar
			BEGIN
				IF @key1Varchar IS NULL
					SET @key1Varchar = CONVERT(varchar,@sKey1)
				EXECUTE sp_executesql @sqlstmt,@parameterVarchar,@key1 = @key1Varchar
			END
		ELSE IF (@sInfoCenter IN ('ChartOfAccounts','Employees','Vendors','Units')) --nvarchar
			BEGIN
				EXECUTE sp_executesql @sqlstmt,@parameterNvarchar,@key1 = @sKey1
			END
		ELSE --Projects
			BEGIN
				IF (@sKey2 = ' ')
					EXECUTE sp_executesql @sqlstmt,@parameterNvarchar,@key1 = @sKey1
				ELSE IF (@sKey3 = ' ')
					EXECUTE sp_executesql @sqlstmt,@parameterNvarchar,@key1 = @sKey1,@key2 = @sKey2
				ELSE
					EXECUTE sp_executesql @sqlstmt,@parameterNvarchar,@key1 = @sKey1,@key2 = @sKey2,@key3 = @sKey3
			END

	FETCH NEXT FROM curTableList INTO @sTableName
   END

   CLOSE curTableList
   DEALLOCATE curTableList
GO
