SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_UpdatePending] (
	@wbs1				Nvarchar(32) = '',
	@user				Nvarchar(32) = '',
	@employeeId			Nvarchar(32) = '',
	@historyInserts		char(1)	= 'C'			-- (C)hanges; (N)ever; (A)lways
)
AS
BEGIN
	-- Populate pending table with list of non-acct employees who have view/edit rights to this new stage
	declare @sql nvarchar(max) = (select top 1 SQLPart from CCG_EI_SQLPartsCache where [Type] = 'RoleCaseClause')
	declare @filterSQL varchar(1) = (select top 1 isnull(FilterSQL, 'N') from CCG_EI_Config)
	declare @tabNewPending table (WBS1 Nvarchar(30), Emp Nvarchar(20), CanModify char(1), Role Nvarchar(30))
	set @wbs1 = isnull(@wbs1, '')
	set @user = isnull(@user, '')

	if isnull(@sql, '') = ''
	begin
		delete from CCG_EI_SQLPartsCache where [Role] is null and [Type] = 'RoleCaseClause' -- should not exist [JAM]

		select @sql = IsNull(STUFF((
			SELECT distinct N'
				when ro.VisionField = '''+VisionField+N''' then '+
					(case when left(VisionField, 4) = 'Cust' then 'PCTF.' when left(VisionField, 3) <> 'PR.' then 'PR.' else '' end) + VisionField
				from CCG_EI_ConfigRoles
				where VisionField <> ''
				order by N'
				when ro.VisionField = '''+VisionField+N''' then '+
					(case when left(VisionField, 4) = 'Cust' then 'PCTF.' when left(VisionField, 3) <> 'PR.' then 'PR.' else '' end) + VisionField
				FOR XML PATH(''), TYPE
			).value('.', 'nvarchar(MAX)'),1,1,''),'')

		insert into CCG_EI_SQLPartsCache ([Role], [Type], [SQLPart]) values (NULL, 'RoleCaseClause', @sql)
	end

	-- Determine who is responsible for the current project stages
	set @sql = '
	select e.WBS1, case when 0 = 1 then e.WBS1 '+@sql+' else null end as Employee, max(CanModify), substring(max(case when CanModify = ''Y'' then ''1'' else ''0'' end + ro.Role), 2, 10000) as Role
		from CCG_EI e
			inner join PR on PR.WBS1 = e.WBS1 and PR.WBS2 = N'' '' and PR.WBS3 = N'' ''
			inner join ProjectCustomTabFields PCTF on PCTF.WBS1 = PR.WBS1 and PCTF.WBS2 = N'' '' and PCTF.WBS3 = N'' ''
			inner join CCG_EI_ConfigRights r on r.StageFlow = PCTF.CustInvoiceStageFlow and r.Stage = e.InvoiceStage
			inner join CCG_EI_ConfigRoles ro on ro.Role = r.Role '
		+ (case when @filterSQL = 'Y' and @wbs1 = '' and @user <> '' then '
			inner join (
				select distinct wbs1 from dbo.fnCCG_EI_ProjectFilter('''', N'''+@user+''')
			) ProjectFilter on ProjectFilter.wbs1 = PR.wbs1'
		else '' 
		end) + '
		where case when 0 = 1 then e.WBS1 '+@sql+' else null end is not null
			and r.CanView = ''Y'' and PCTF.CustInvoiceGroup is not null '
		+ (case when @wbs1 <> '' then '
			and e.WBS1 = N'''+@wbs1+'''' 
		when @wbs1 = '' and (@user = '' or @filterSQL = 'N') then '
			and e.DateChanged > getdate() - 365								-- Limit to records modified in last year [precaution for performance]
			and PR.Status <> ''D'' and PR.ChargeType = ''R'' '
		else '' 
		end) + '
		group by e.WBS1, case when 0 = 1 then e.WBS1 '+@sql+' else null end '
	insert into @tabNewPending exec(@sql)
	--select * from @tabNewPending

	-- Detemine the changes and insert history of changes
	if isnull(@historyInserts, 'N') <> 'N' 
	begin
		if @historyInserts = 'C'
			-- removed from pending
			INSERT INTO CCG_EI_History (ActionTaken, ActionDate, ActionTakenBy, ActionRecipient, WBS1)
				select 'Pending Update', getutcdate(), @employeeId, p.Employee, @wbs1
					from CCG_EI_Pending p
						full outer join @tabNewPending tp on tp.WBS1 = p.WBS1 and p.Employee = tp.Emp
					where (isnull(@wbs1,'') = '' or p.WBS1 = @wbs1) and (p.CanModify = 'Y' or tp.CanModify = 'Y')
						and isnull(p.Employee,'') <> '' and isnull(tp.Emp,'') = ''
		else if @historyInserts = 'A'
			-- all pending
			INSERT INTO CCG_EI_History (ActionTaken, ActionDate, ActionTakenBy, ActionRecipient, WBS1)
				select 'Pending Update', getutcdate(), @employeeId, tp.Emp, @wbs1
					from @tabNewPending tp 
					where (isnull(@wbs1,'') = '' or tp.WBS1 = @wbs1) and tp.CanModify = 'Y'
	end

	delete from CCG_EI_Pending where (isnull(@wbs1,'') = '' or WBS1 = @wbs1)
	--print @sql
	insert into CCG_EI_Pending (WBS1, Employee, CanModify, Role) select WBS1, Emp, CanModify, Role from @tabNewPending
END
GO
