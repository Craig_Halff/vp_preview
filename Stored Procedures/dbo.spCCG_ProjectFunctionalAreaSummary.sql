SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectFunctionalAreaSummary] @WBS1 varchar(32)
AS 
/* 
Copyright (c) 2019 Central Consulting Group.  All rights reserved.
12/04/2019	David Springer
			Write to Functional Area Summary grid at project level from phase level Functional Area fields
*/
BEGIN
SET NOCOUNT ON

	Delete From Projects_FunctionalAreaSummary Where WBS1 = @WBS1

	Insert Into Projects_FunctionalAreaSummary
	(WBS1, WBS2, WBS3, Seq, CustFunctionalAreaSummary, CustFunctionalAreaSummaryFee, CustFunctionalAreaSummaryPercent)
	Select WBS1, ' ', ' ', Replace (NewID(), '-', ''), CustFunctionalArea, Fee, Pct
	From 
	--	Fee per Functional Area
		(	Select c.WBS1, px.CustFunctionalArea, 
			Sum (d.FeeDirLab + d.FeeDirExp + d.ConsultFee + d.ReimbAllowCons + d.ReimbAllowExp) Fee,
			Sum (d.FeeDirLab + d.FeeDirExp + d.ConsultFee + d.ReimbAllowCons + d.ReimbAllowExp) / t.TotalFee * 100 Pct
			From Contracts c, ContractDetails d, ProjectCustomTabFields px,
			--	Total Project Fee
				(Select c.WBS1, Sum (d.FeeDirLab + d.FeeDirExp + d.ConsultFee + d.ReimbAllowCons + d.ReimbAllowExp) TotalFee
				From Contracts c, ContractDetails d
				Where c.FeeIncludeInd = 'Y'
				  and c.WBS1 = d.WBS1
				  and d.WBS2 <> ' ' -- lowest level fee
				  and c.ContractNumber = d.ContractNumber
				Group by c.WBS1) t
			Where c.WBS1 = @WBS1
			  and c.FeeIncludeInd = 'Y'
			  and c.WBS1 = d.WBS1
			  and c.ContractNumber = d.ContractNumber
			  and d.WBS2 <> ' ' -- lowest level fee
			  and d.WBS1 = px.WBS1
			  and d.WBS2 = px.WBS2
			  and px.WBS3 = ' ' -- phase level Functional Area
			  and px.CustFunctionalArea is not null
			  and c.WBS1 = t.WBS1
			Group by c.WBS1, px.CustFunctionalArea, t.TotalFee
		 ) a
END
GO
