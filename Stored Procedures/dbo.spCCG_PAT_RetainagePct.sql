SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_RetainagePct](
	@PayableSeq int,
	@WBS1		Nvarchar(32),
	@Vendor		Nvarchar(20) = '',
	@Contract	Nvarchar(50) = '')
AS BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	if exists (select * from sys.objects where object_id = object_id(N'[dbo].[spCCG_PAT_RetainagePctOverride]') and type in (N'P', N'PC'))
	begin
		-- Override procedure
		declare @sql Nvarchar(max) = N'spCCG_PAT_RetainagePctOverride '+cast(@PayableSeq as Nvarchar(20))+',
			N'''+isnull(@WBS1,'')+''', N'''+isnull(@Vendor,'')+''', N'''+isnull(@Contract,'')+''''
		exec(@sql)
	end
	else begin
		select WBS1, BT.RetPct as [Percent], BT.Retainage as HasRetainage, 'N' as AllowPercentInput /* Allow for user to enter percent */
			from BT where BT.WBS1 = @WBS1  and BT.Retainage = 'Y'
		union all select N'', 10, 'N', 'Y' /* defaults if no retainage is found in BT */
	end
END
GO
