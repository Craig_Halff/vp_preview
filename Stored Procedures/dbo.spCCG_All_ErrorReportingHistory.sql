SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_All_ErrorReportingHistory] 
	@product varchar(64) = '',
	@user Nvarchar(32) = '',
	@ErrorNumber varchar(64) = null
AS
BEGIN
	-- EXEC [spCCG_All_ErrorReportingHistory] @product = 'ARM', @user = ''
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF (@errorNumber IS NULL)
		SELECT TOP 100 * 
			FROM [dbo].[CCG_All_ErrorReporting] r
			WHERE (@product = '' or r.product = @product) and (@user = '' or r.[user] = @user)
			ORDER BY dateModified DESC
	ELSE
		SELECT * FROM [dbo].[CCG_All_ErrorReporting] WHERE errorNumber = @errorNumber ORDER BY dateModified DESC
END
GO
