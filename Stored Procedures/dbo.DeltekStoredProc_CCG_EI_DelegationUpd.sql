SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_DelegationUpd] ( @Id varchar(32), @Username nvarchar(32), @Employee nvarchar(20), @Delegate nvarchar(20), @FromDate varchar(20), @ToDate varchar(20), @ForClientId varchar(32), @ForWBS1 nvarchar(30), @Dual char(1), @ApprovedBy nvarchar(20), @ApprovedOn datetime)
             AS EXEC spCCG_EI_DelegationUpd @Id,@Username,@Employee,@Delegate,@FromDate,@ToDate,@ForClientId,@ForWBS1,@Dual,@ApprovedBy,@ApprovedOn
GO
