SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_ProjectPracticeLeaderSync]
AS
    /* 
	Copyright (c) 2021 Halff Associates, Inc.  All rights reserved.
	8/23/2021	Craig H. Anderson

	Sync Project Practice Area Leaders to Practice Area hub changes
*/
    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    BEGIN
        UPDATE px
        SET px.CustPracticeAreaLeader = pa.CustPrimaryPracticeAreaLeader
        FROM dbo.ProjectCustomTabFields      AS px
            INNER JOIN dbo.PR                AS p
                ON px.WBS1 = p.WBS1
                    AND p.WBS2 = ' '
            INNER JOIN dbo.UDIC_PracticeArea AS pa
                ON px.CustTeamPracticeArea = pa.CustNumber
        WHERE
            px.WBS2 = ' '
            AND p.Status = 'A'
            AND px.CustPracticeAreaLeader <> pa.CustPrimaryPracticeAreaLeader;
    END;
GO
