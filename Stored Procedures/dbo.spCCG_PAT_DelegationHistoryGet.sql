SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_DelegationHistoryGet] (@Username Nvarchar(32), @BeginDate datetime, @EndDate datetime)
AS BEGIN
/*
	Copyright (c) 2020 EleVia Software. All rights reserved.

	select * from CCG_PAT_Delegation
	exec spCCG_PAT_DelegationHistoryGet 'ADMIN','',''
	exec spCCG_PAT_DelegationHistoryGet 'ADMIN','2/21/2017','2/28/2017'
	exec spCCG_PAT_DelegationHistoryGet 'ADMIN','2/21/2017','3/06/2017'
	exec spCCG_PAT_DelegationHistoryGet 'ADMIN','3/25/2017','3/26/2017'
*/
	set nocount on
	IF ISNULL(@BeginDate,'') <> '' AND ISNULL(@EndDate,'') <> ''
		SELECT TOP 1000 dh.Seq, dh.Id, dh.Delegate, dh.Employee, dh.FromDate, dh.ToDate,
				dh.ActionTaken, dh.ActionTakenBy, dh.ActionDate, dh.ActionRecipient, dh.Description
			FROM CCG_PAT_HistoryDelegation dh
			WHERE FromDate <= @EndDate and ToDate >= @BeginDate
			ORDER BY dh.ActionDate DESC
	ELSE
		SELECT TOP 1000 dh.Seq, dh.Id, dh.Delegate, dh.Employee, dh.FromDate, dh.ToDate,
				dh.ActionTaken, dh.ActionTakenBy, dh.ActionDate, dh.ActionRecipient, dh.Description
			FROM CCG_PAT_HistoryDelegation dh
			ORDER BY dh.ActionDate DESC
END
GO
