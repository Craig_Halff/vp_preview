SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPShiftTask_RevForecast]
  @strRowID nvarchar(255),
  @strFromStartDate varchar(8),
  @strFromEndDate varchar(8),
  @strToStartDate varchar(8),
  @strToEndDate varchar(8),
  @bitShiftAll bit, /* This flag is applicable only for ShiftMode = {1, 3}. 0 = Shift using the "From" range, 1 = Shift the entire Assignment Row */
  @bitFixStartDate bit /* This flag is applicable only for ShiftMode = {1, 3}. 1 = Hold "To" StartDate fixed, 0 = Hold "To" EndDate fixed */
AS

BEGIN -- Procedure stRPShiftTasks

  SET NOCOUNT ON
  
  DECLARE @dtFromStartDate datetime
  DECLARE @dtFromEndDate datetime

  DECLARE @dtToStartDate datetime
  DECLARE @dtToEndDate datetime

  DECLARE @dtLeafRevenueStartDate datetime
  DECLARE @dtLeafRevenueEndDate datetime

  DECLARE @dtTSKRevStartDate datetime
  DECLARE @dtTSKRevEndDate datetime

  DECLARE @dtOvrdFromStartDate datetime
  DECLARE @dtOvrdFromEndDate datetime

  DECLARE @dtMinASGDate datetime
  DECLARE @dtMaxASGDate datetime
   
  DECLARE @strCompany nvarchar(14)
  DECLARE @strPlanID varchar(32) = NULL
  DECLARE @strTaskID varchar(32) = NULL
  DECLARE @strTPDTaskID varchar(32) = NULL
  DECLARE @strNextTimePhaseID varchar(32) = NULL
  DECLARE @strNextLeafTaskID varchar(32) = NULL
  DECLARE @strResourceID nvarchar(20) = NULL
  DECLARE @strGenericResourceID nvarchar(20) = NULL
  DECLARE @strUserName nvarchar(32)
  DECLARE @strAECFlg varchar(1) = ''
  DECLARE @strRescheduleFlg varchar(1) = 'P' /* 'P' = Reschedule Plan Date, 'A' = Reschedule Assignment Date */

  DECLARE @strOvrdFromStartDate varchar(8)
  DECLARE @strOvrdFromEndDate varchar(8)

  DECLARE @decSumHrs decimal(19,4) = 0

  DECLARE @intFromDurationWD int = 0

  -- Declare Temp tables.
  
  DECLARE @tabTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    RevenueStartDate datetime,
    RevenueEndDate datetime,
    ToRevenueStartDate datetime,
    ToRevenueEndDate datetime,
    SumBill decimal(19,4),
	SumCost decimal(19,4),
    MinTPDDate datetime,
    MaxTPDDate datetime
    UNIQUE(PlanID, TaskID)
  )
   DECLARE @tabRevTPD TABLE (
    TaskID varchar(32) COLLATE database_default,
    TimePhaseID varchar(32) COLLATE database_default,
    StartDate datetime,
    EndDate datetime
    UNIQUE(TimePhaseID)
  )  
  DECLARE @tabTPD TABLE (
    PlanID varchar(32) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    --SumHrs decimal(19,4),
    SumBill decimal(19,4),
    SumCost decimal(19,4),
    MinTPDDate datetime,
    MaxTPDDate datetime
    UNIQUE(PlanID, OutlineNumber)
  )
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

  -- Set Dates

  SET @dtFromStartDate = 
    CASE 
      WHEN DATALENGTH(@strFromStartDate) = 0 
      THEN NULL
      ELSE CONVERT(datetime, @strFromStartDate)
    END

  SET @dtFromEndDate = 
    CASE
      WHEN DATALENGTH(@strFromEndDate) = 0
      THEN NULL
      ELSE CONVERT(datetime, @strFromEndDate)
    END

  SET @dtToStartDate = 
    CASE
      WHEN DATALENGTH(@strToStartDate) = 0
      THEN NULL
      ELSE CONVERT(datetime, @strToStartDate)
    END

  SET @dtToEndDate = 
    CASE
    WHEN DATALENGTH(@strToEndDate) = 0
    THEN NULL
    ELSE CONVERT(datetime, @strToEndDate)
  END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 

  --   For Revenue Tab, always on a WBS row {PP, PV}, @strRowID = '|<PNTask.TaskID>'

  --      <PNTask.TaskID> could be TaskID of WBS row at any levels.
  
  SELECT
    @strPlanID = PlanID,
    @strTaskID = TaskID
    FROM dbo.stRP$tabParseRowID(@strRowID)

  SET @strRescheduleFlg = 'P'

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get various parameters.
  SELECT 
    @strCompany = P.Company,
    @dtTSKRevStartDate = T.RevenueStartDate,
    @dtTSKRevEndDate = T.RevenueEndDate
    FROM PNTask AS T
      INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID
    WHERE T.TaskID = @strTaskID

  -- Keep track of original Duration of the "From" range.

  SET @intFromDurationWD = dbo.DLTK$NumWorkingDays(@dtFromStartDate, @dtFromEndDate, @strCompany)

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  INSERT @tabRevTPD(
    TaskID,
    TimePhaseID,
    StartDate,
    EndDate
  )
    SELECT 
	  A.TaskID,
      A.TimePhaseID, 
      A.StartDate,
      A.EndDate
      FROM PNPlannedRevenueLabor AS A
        INNER JOIN PNTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
        INNER JOIN PNTask AS PT ON A.PlanID = PT.PlanID 
      WHERE
        A.PlanID = @strPlanID AND
        PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
        AT.OutlineNumber LIKE PT.OutlineNumber + '%'

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  -- Shift Revenue Forecast TPD	
  SELECT
        @strOvrdFromStartDate = 
          CASE
            WHEN @bitShiftAll = 1 
            THEN 
              CASE 
                WHEN @strRescheduleFlg = 'P' 
                THEN CONVERT(VARCHAR(8), @dtTSKRevStartDate, 112)                 
              END
            ELSE @strFromStartDate
          END,
        @strOvrdFromEndDate = 
          CASE
            WHEN @bitShiftAll = 1 
            THEN 
              CASE 
                WHEN @strRescheduleFlg = 'P' 
                THEN CONVERT(VARCHAR(8), @dtTSKRevEndDate, 112)                 
              END
            ELSE @strFromEndDate
          END

  -- @bitShiftAll = 1 means we need to shift all Revenue forecast TPD
  IF @bitShiftAll = 1 
	BEGIN
	  DECLARE csrShiftAllTPD CURSOR LOCAL FAST_FORWARD FOR
		SELECT DISTINCT
		  A.TimePhaseID 
		  FROM @tabRevTPD AS A

	  OPEN csrShiftAllTPD
	  FETCH NEXT FROM csrShiftAllTPD INTO @strNextTimePhaseID 

	  WHILE (@@FETCH_STATUS = 0)
		BEGIN
		--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		  EXECUTE dbo.stRPShiftTPD_RevForecast @strNextTimePhaseID, @strOvrdFromStartDate, @strOvrdFromEndDate, @strToStartDate, @strToEndDate, @bitShiftAll, @bitFixStartDate

		  FETCH NEXT FROM csrShiftAllTPD INTO @strNextTimePhaseID 

		END -- While

	  CLOSE csrShiftAllTPD
	  DEALLOCATE csrShiftAllTPD	
	END
  ELSE -- shift only partial TPD @bitShiftAll = 0
	BEGIN
		DECLARE csrShiftPartialTPD CURSOR LOCAL FAST_FORWARD FOR
		SELECT DISTINCT TaskID 
		  FROM @tabRevTPD 

	  OPEN csrShiftPartialTPD
	  FETCH NEXT FROM csrShiftPartialTPD INTO @strTPDTaskID 

	  WHILE (@@FETCH_STATUS = 0)
		BEGIN
		--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		  EXECUTE dbo.stRPShiftPartialTPD_RevForecast @strTPDTaskID, @strOvrdFromStartDate, @strOvrdFromEndDate, @strToStartDate, @strToEndDate, @bitShiftAll, @bitFixStartDate

		  FETCH NEXT FROM csrShiftPartialTPD INTO @strTPDTaskID 

		END -- While

	  CLOSE csrShiftPartialTPD
	  DEALLOCATE csrShiftPartialTPD	
	END

 -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  -- Change Task {RevenueStartDate, RevenueEndDate} only when user is changing "Plan Revenue Forecast Dates" always at WBS Level.
  
  IF (@strRescheduleFlg = 'P')
    BEGIN

      -- Adjusting "From" Date for WBS row.
      -- When "Shift All", the caller does not pass in "From" Date, and the "From" Date is derived from the current Task.

      SELECT
        @dtOvrdFromStartDate = 
          CASE
            WHEN @bitShiftAll = 1 AND @strRescheduleFlg = 'P'
            THEN @dtTSKRevStartDate
            ELSE @dtFromStartDate
          END,
        @dtOvrdFromEndDate = 
          CASE
            WHEN @bitShiftAll = 1 AND @strRescheduleFlg = 'P'
            THEN @dtTSKRevEndDate
            ELSE @dtFromEndDate
          END

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      INSERT @tabTPD(
        PlanID,
        OutlineNumber,
        SumBill,
		SumCost,
        MinTPDDate,
        MaxTPDDate
      )
        SELECT
          TPD.PlanID AS PlanID,
          AT.OutlineNumber AS OutlineNumber,
          SUM(TPD.PeriodBill) AS SumBill,
          SUM(TPD.PeriodCost) AS SumCost,
          MIN(TPD.StartDate) AS MinTPDDate,
          MAX(TPD.EndDate) AS MaxTPDDate
          FROM PNPlannedRevenueLabor AS TPD
            INNER JOIN PNTask AS AT ON TPD.PlanID = AT.PlanID AND TPD.TaskID = AT.TaskID  
            INNER JOIN PNTask AS PT ON AT.PlanID = PT.PlanID
          WHERE TPD.PlanID = @strPlanID AND
            PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
            AT.OutlineNumber LIKE PT.OutlineNumber + '%' 
			AND (AT.ChildrenCount = 0 OR AT.TaskID IN 
					(SELECT T.TaskID FROM PNTask T 
					 INNER JOIN PNTask LT ON T.PlanID = LT.PlanID 
					  AND LT.ParentOutlineNumber = T.OutlineNumber AND LT.WBSType = 'LBCD' 
					  WHERE T.PlanID = @strPlanID))
          GROUP BY TPD.PlanID, TPD.TaskID, AT.OutlineNumber

      INSERT @tabTask(
        PlanID,
        TaskID,
        RevenueStartDate,
        RevenueEndDate,
        ToRevenueStartDate,
        ToRevenueEndDate,
        SumBill,
		SumCost,
        MinTPDDate,
        MaxTPDDate
      )
        SELECT
          XT.PlanID AS PlanID,
          XT.TaskID AS TaskID,
          XT.RevenueStartDate AS RevenueStartDate,
          XT.RevenueEndDate AS RevenueEndDate,
          XT.ToRevenueStartDate AS ToRevenueStartDate,
          XT.ToRevenueEndDate AS ToRevenueEndDate,
          ISNULL(SUM(TPD.SumBill), 0) AS SumBill,
          ISNULL(SUM(TPD.SumCost), 0) AS SumCost,
          MIN(TPD.MinTPDDate) AS MinTPDDate,
          MAX(TPD.MinTPDDate) AS MinTPDDate
          FROM (
            SELECT 
              YT.PlanID AS PlanID,
              YT.TaskID AS TaskID,
              YT.OutlineNumber AS OutlineNumber,
              YT.RevenueStartDate AS RevenueStartDate,
              YT.RevenueEndDate AS RevenueEndDate,
              CASE 
                WHEN @bitFixStartDate = 0
                THEN
                  CASE
                    WHEN YT.DurationWD = 0
                    THEN @dtToEndDate
                    ELSE
                      dbo.DLTK$AddBusinessDays(
                        @dtToEndDate, 
                        -1 * ((YT.DurationWD - 1) + YT.OffsetWD), 
                        @strCompany
                      )
                  END
                ELSE 
                  dbo.DLTK$AddBusinessDays(
                    @dtToStartDate, 
                    YT.OffsetWD, 
                    @strCompany
                  )
              END AS ToRevenueStartDate,
              CASE 
                WHEN @bitFixStartDate = 1
                THEN
                  dbo.DLTK$AddBusinessDays(
                    @dtToStartDate,
                    CASE
                      WHEN YT.DurationWD <> 0
                      THEN ((YT.DurationWD - 1) + YT.OffsetWD)
                      ELSE (YT.OffsetWD) 
                    END, 
                    @strCompany
                  )
                ELSE 
                  dbo.DLTK$AddBusinessDays(
                    @dtToEndDate, 
                    -1 * (YT.OffsetWD), 
                    @strCompany
                  )
              END AS ToRevenueEndDate
              FROM (
                SELECT
                  CT.PlanID AS PlanID,
                  CT.TaskID AS TaskID,
                  CT.OutlineNumber AS OutlineNumber,
                  CT.RevenueStartDate AS RevenueStartDate,
                  CT.RevenueEndDate AS RevenueEndDate,
                  CASE
                    WHEN @bitShiftAll = 1
                    THEN dbo.DLTK$NumWorkingDays(CT.RevenueStartDate, CT.RevenueEndDate, @strCompany)
                    ELSE @intFromDurationWD
                  END AS DurationWD,
                  CASE 
                    WHEN @bitFixStartDate = 1
                    THEN
                      CASE 
                        WHEN @dtOvrdFromStartDate = CT.RevenueStartDate 
                        THEN 0
                        ELSE dbo.DLTK$NumWorkingDays(@dtOvrdFromStartDate, CT.RevenueStartDate, @strCompany) - 1
                      END
                    ELSE
                      /* When approaching from the rear @bitFixStartDate = 0, need to reverse the sign for OffsetWD. */
                      /* OffsetWD is the distance from OvrdFromEndDate to EndDate of WBS row. */
                      CASE
                        WHEN @dtOvrdFromEndDate = CT.RevenueEndDate
                        THEN 0
                        ELSE dbo.DLTK$NumWorkingDays(CT.RevenueEndDate, @dtOvrdFromEndDate, @strCompany) - 1
                      END
                  END AS OffsetWD
                  FROM 
                    PNTask AS PT
                      INNER JOIN PNTask AS CT ON PT.PlanID = CT.PlanID
                    WHERE
                      PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
                      CT.OutlineNumber LIKE PT.OutlineNumber + '%'   
              ) AS YT
          ) AS XT
            LEFT JOIN @tabTPD AS TPD
              ON XT.PlanID = TPD.PlanID AND TPD.OutlineNumber LIKE XT.OutlineNumber + '%'
          GROUP BY XT.PlanID, XT.TaskID, XT.RevenueStartDate, XT.RevenueEndDate, XT.ToRevenueStartDate, XT.ToRevenueEndDate

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      UPDATE ZT SET
        RevenueStartDate = 
          CASE 
            WHEN @bitShiftAll = 1 THEN
              CASE
                WHEN ToRevenueStartDate IS NULL
                THEN
                  CASE
                    WHEN ZT.RevenueStartDate <= ToRevenueEndDate
                    THEN ZT.RevenueStartDate
                    ELSE ToRevenueEndDate
                  END
                WHEN XT.MinTPDDate IS NULL THEN ToRevenueStartDate
                ELSE
                  CASE
                    WHEN ToRevenueStartDate < XT.MinTPDDate
                    THEN ToRevenueStartDate
                    ELSE XT.MinTPDDate
                  END
              END
            WHEN @bitShiftAll = 0 THEN
              CASE
                WHEN XT.SumBill = 0 THEN ZT.RevenueStartDate
                ELSE
                  CASE
                    WHEN ZT.RevenueStartDate <= XT.MinTPDDate
                    THEN ZT.RevenueStartDate
                    ELSE XT.MinTPDDate
                  END
              END
          END,
        RevenueEndDate = 
          CASE 
            WHEN @bitShiftAll = 1 THEN
              CASE
                WHEN ToRevenueEndDate IS NULL
                THEN
                  CASE
                    WHEN ZT.RevenueEndDate >= ToRevenueStartDate
                    THEN ZT.RevenueEndDate
                    ELSE ToRevenueStartDate
                  END
                WHEN XT.SumBill = 0 THEN ToRevenueEndDate
                WHEN XT.MaxTPDDate IS NULL THEN ToRevenueEndDate
                ELSE
                  CASE
                    WHEN ToRevenueEndDate > XT.MaxTPDDate
                    THEN ToRevenueEndDate
                    ELSE XT.MaxTPDDate
                  END
              END
            WHEN @bitShiftAll = 0 THEN
              CASE
                WHEN XT.SumBill = 0 THEN ZT.RevenueEndDate
                ELSE
                  CASE
                    WHEN ZT.RevenueEndDate >= XT.MaxTPDDate
                    THEN ZT.RevenueEndDate
                    ELSE XT.MaxTPDDate
                  END
              END
          END,
		ModDate=LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19),
		ModUser=@strUserName
        FROM PNTask AS ZT
          INNER JOIN @tabTask AS XT ON ZT.PlanID = XT.PlanID AND ZT.TaskID = XT.TaskID
    
    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


    END /* END IF (@strRescheduleFlg =  'P') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     
  SET NOCOUNT OFF

END -- stRPShiftTask_RevForecast
GO
