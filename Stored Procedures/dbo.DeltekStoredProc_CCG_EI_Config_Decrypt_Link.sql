SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--UNICODE TBD
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Decrypt_Link]
	@emailLink	varchar(max)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dbo.fnCCG_EI_GetD(@emailLink) AS decoded;
END;
GO
