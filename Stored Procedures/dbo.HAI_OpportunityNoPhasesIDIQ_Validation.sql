SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_OpportunityNoPhasesIDIQ_Validation]
    @OpportunityID VARCHAR(32)
  , @ContractType VARCHAR(32)
AS
/* =======================================================================================
    Halff Associates, Inc.   All rights reserved.
    20200624 - Craig H. Anderson
            Validate that the Opportunity has no Phases or Tasks unless it is an
            "Individual Project/Task Order" type contract.
   ======================================================================================= */
SET NOCOUNT ON;
    BEGIN
        IF EXISTS
            (
                SELECT 'x'
                FROM dbo.Opportunity o
                    INNER JOIN
                        (
                            SELECT OpportunityID
                                 , COUNT(*) AS items
                            FROM dbo.PR
                            WHERE WBS2 <> ' '
                                  OR WBS3 <> ' '
                            GROUP BY OpportunityID
                        )            p
                        ON o.OpportunityID = p.OpportunityID
                WHERE o.OpportunityID = @OpportunityID
                      AND @ContractType <> 'Individual Project/Task Order'
                      AND p.items > 0
            )
            RAISERROR('Only Individual Project/Task Order contract types may have Phases or Tasks.', 16, 1);
    END;
GO
