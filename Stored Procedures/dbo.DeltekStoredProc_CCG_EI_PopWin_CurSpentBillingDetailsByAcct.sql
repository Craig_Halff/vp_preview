SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_PopWin_CurSpentBillingDetailsByAcct] ( @WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7), @Key varchar(32), @AcctName varchar(200), @UserName varchar(32))
             AS EXEC spCCG_EI_PopWin_CurSpentBillingDetailsByAcct @WBS1,@WBS2,@WBS3,@Key,@AcctName,@UserName
GO
