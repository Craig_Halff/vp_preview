SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_PopWin_JTDBilledDetails] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7), @UserName varchar(32))
AS BEGIN
/*
	Copyright (c) 2020 Central Consulting Group.  All rights reserved.

	[spCCG_EI_PopWin_JTDBilledDetails] '1999015.00',' ',' ', ''
*/
	declare @T TABLE (TopOrder	int, Row_Detail varchar(255), Descr	varchar(255), InvDate date, InvAmt money, Labor money, Fee money, Consultant money, Expense money, Other money, ID varchar(30))

	insert into @T (TopOrder, Row_Detail, Descr, InvDate, InvAmt, Labor, Fee, Consultant, Expense, Other, ID)
		select (row_number() over(order by InvoiceDate desc, Invoice desc)), '',
		Invoice, InvoiceDate,
		Sum(Amount) as Total,
		Sum(Case When InvoiceSection='L' Then Amount Else 0 End) as AmountL,
		Sum(Case When InvoiceSection='F' Then Amount Else 0 End) as AmountF,
		Sum(Case When InvoiceSection='C' Then Amount Else 0 End) as AmountC,
		Sum(Case When InvoiceSection in ('E','U') Then Amount Else 0 End) as AmountE,
		Sum(Case When InvoiceSection not in ('L','F','C','E','U') Then Amount Else 0 End) as AmountOther,
		''
	from (
		select LedgerAR.Invoice, InvoiceSection, InvoiceDate, Sum(-LedgerAR.Amount) as Amount
		from LedgerAR inner join AR on AR.WBS1=LedgerAR.WBS1 and AR.WBS2=LedgerAR.WBS2 and AR.WBS3=LedgerAR.WBS3 and AR.Invoice=LedgerAR.Invoice
		left join CA on CA.Account=LedgerAR.Account
		where LedgerAR.WBS1=@WBS1 and (@WBS2=' ' or @WBS2=LedgerAR.WBS2) and (@WBS3=' ' or @WBS3=LedgerAR.WBS3)
		  and (ledgerAR.TransType = 'IN' AND IsNull(SubType, ' ') not in ('I','R') AND AutoEntry = 'N')
		  and (CA.Type = 4 OR LedgerAR.Account IS NULL)
		group by LedgerAR.Invoice, InvoiceSection, InvoiceDate
	) as yaya
	group by Invoice, InvoiceDate

	-- Add grand total line:
	insert into @T (TopOrder, Row_Detail, Descr, InvAmt, Labor, Fee, Consultant, Expense, Other)
		select 900 as TopOrder, 'BackgroundColor=#888888;ForegroundColor=#ffffff;Bold=Y', 'GRAND TOTAL', (select sum(InvAmt) from @T), (select sum(Labor) from @T), (select sum(Fee) from @T), (select sum(Consultant) from @T), (select sum(Expense) from @T), (select sum(Other) from @T)
	
	delete from @T where InvAmt = 0 

	-- Final recordsets
	select 'T;Align=Center', '', 'D;Align=Center', 'C2;NS', 'C2;NS', 'C2;NS', 'C2;NS', 'C2;NS', 'C2;NS',   'JTD Billed: ' + @WBS1 + case when @WBS2 <> '' then +' - ' + @WBS2 else '' end + case when @WBS3 <> '' then +' - ' + @WBS3 else '' end 
	select Descr as [Invoice], Row_Detail, InvDate as [Date], InvAmt as [Total], Labor, Fee, Consultant, Expense, Other from @T 

	select 'width=800
		height=600' as AdvancedOptions
END
GO
