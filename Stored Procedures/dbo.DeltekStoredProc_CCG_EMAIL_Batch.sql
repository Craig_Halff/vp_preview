SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--select * from CCG_Email_Messages
--exec DeltekStoredProc_CCG_EMAIL_Batch 1,1,'Batch','test@test.com','Payables Approval Tracking','test@test.com'
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EMAIL_Batch]
@breakOnApp		bit,
@breakOnSender  bit,
@batchType varchar(50),
@toList varchar(1000),
@sourceApplication varchar(1000),
@sender varchar(1000)
AS BEGIN
	SET NOCOUNT ON;

	select * from CCG_Email_Messages where 
	ToList = @toList -- or ISNULL(@toList,'') = '' 
	and
	case when @batchType = 'Batch' and ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]()) > 0 then 1
		 when @batchType <> 'Batch' and ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]()) <= 0 then 1
		 else 0 end = 1
	and
	case when @breakOnApp = 0 then 1 when SourceApplication = @sourceApplication then 1 else 0 end = 1
	and
	case when @breakOnSender = 0 then 1 when Sender = @sender then 1 else 0 end = 1
	
	order by ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]()),SourceApplication, ToList,Sender

END;

GO
