SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[HAI_TSR_ProjectSummaryJTDAllOffices]
	@CompanyId varchar(2),
	@MinPeriod INT,
	@Period INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

-- CREATE A UNIQUE ID
DECLARE @SessionId varchar(32) = REPLACE(CAST(NEWID() AS varchar(36)), '-', '')

DECLARE @s1 VARCHAR(MAX) = 'AND (LD.Period <= ' + CAST(@Period AS VARCHAR(10)) + ') '
DECLARE @s2 VARCHAR(MAX) = '(((SUBSTRING(PR.Org, 1, 2) = /*N*/''' + @CompanyId + ''') AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A''))'
DECLARE @s3 VARCHAR(100) = CAST(@Period AS VARCHAR(10))

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'setContextInfo' AND type = 'P')  
	Execute dbo.setContextInfo @StrCompany = /*N*/@CompanyId, @StrAuditingEnabled  = 'N',@strUserName = /*N*/'HIT',@StrCultureName = 'en-US';

Exec setCurrentActivityOnly @SessionId
, ' PR LEFT JOIN CL AS CLBill ON CLBill.ClientID = PR.BillingClientID  LEFT JOIN CL ON CL.ClientID = PR.ClientID  LEFT JOIN Contacts AS BLCT ON PR.BillingContactID = BLCT.ContactID,  PR AS LEVEL2, PR AS LEVEL3 '
,@s1
, ' '
,@s2
,@s3
,' '
,'870'

DECLARE @sql NVARCHAR(MAX) = N'DECLARE @tabWBSList TABLE(WBS1 nvarchar(30) COLLATE database_default, WBS2 nvarchar(7) COLLATE database_default, WBS3 nvarchar(7) COLLATE database_default PRIMARY KEY(WBS1, WBS2, WBS3))  INSERT @tabWBSList (WBS1,WBS2,WBS3)  SELECT pr.wbs1 ,level2.wbs2 ,level3.wbs3  FROM PR as level3, PR As LEVEL2 , PR  , ##RWL_' + @SessionId + ' WHERE LEVEL3.SubLevel=''N'' AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/'''' AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + '''  DECLARE @tabContract TABLE(contractWBS1 nvarchar(30) COLLATE database_default, contractWBS2 nvarchar(7) COLLATE database_default, contractWBS3 nvarchar(7) COLLATE database_default, contractFee decimal(19,4), contractReimbAllow decimal(19,4), contractConsultFee decimal(19,4), contractFeeDirectLabor decimal(19,4), contractFeeDirectExpense decimal(19,4), contractReimbAllowExpense decimal(19,4), contractReimbAllowConsultant decimal(19,4) PRIMARY KEY(contractWBS1, contractWBS2, contractWBS3)) INSERT @tabContract (contractWBS1,contractWBS2,contractWBS3,contractFee,contractReimbAllow,contractConsultFee,contractFeeDirectLabor,contractFeeDirectExpense,contractReimbAllowExpense,contractReimbAllowConsultant) SELECT contractDetails.WBS1 as contractWBS1, '' '' as contractWBS2, '' '' as contractWBS3, SUM(contractDetails.fee) as contractFee, SUM(contractDetails.reimbAllow) as contractReimbAllow, SUM(contractDetails.consultFee) as contractConsultFee, SUM(contractDetails.FeeDirLab) as contractFeeDirectLabor, SUM(contractDetails.FeeDirExp) as contractFeeDirectExpense, SUM(contractDetails.ReimbAllowExp) as contractReimbAllowExpense, SUM(contractDetails.reimbAllowCons) as contractReimbAllowConsultant FROM contractDetails JOIN contracts on contractDetails.wbs1=contracts.wbs1 and contractDetails.contractNumber = contracts.contractNumber WHERE  contracts.period <=@Period  AND contractDetails.WBS2 =/*N*/'' '' AND EXISTS (select * from @tabWBSList wbsList where wbsList.wbs1=contractDetails.wbs1) GROUP BY contractDetails.WBS1 UNION ALL SELECT contractDetails.WBS1 as contractWBS1, contractDetails.WBS2 as contractWBS2, '' '' as contractWBS3, SUM(contractDetails.fee) as contractFee, SUM(contractDetails.reimbAllow) as contractReimbAllow, SUM(contractDetails.consultFee) as contractConsultFee, SUM(contractDetails.FeeDirLab) as contractFeeDirectLabor, SUM(contractDetails.FeeDirExp) as contractFeeDirectExpense, SUM(contractDetails.ReimbAllowExp) as contractReimbAllowExpense, SUM(contractDetails.reimbAllowCons) as contractReimbAllowConsultant FROM contractDetails JOIN contracts on contractDetails.wbs1=contracts.wbs1 and contractDetails.contractNumber = contracts.contractNumber WHERE  contracts.period <=@Period  AND NOT contractDetails.WBS2 =/*N*/'' '' AND EXISTS (select * from @tabWBSList wbsList where wbsList.wbs1=contractDetails.wbs1 and wbsList.wbs2=contractDetails.wbs2) GROUP BY contractDetails.WBS1,contractDetails.WBS2 UNION ALL SELECT contractDetails.WBS1 as contractWBS1, contractDetails.WBS2 as contractWBS2, contractDetails.WBS3 as contractWBS3, SUM(contractDetails.fee) as contractFee, SUM(contractDetails.reimbAllow) as contractReimbAllow, SUM(contractDetails.consultFee) as contractConsultFee, SUM(contractDetails.FeeDirLab) as contractFeeDirectLabor, SUM(contractDetails.FeeDirExp) as contractFeeDirectExpense, SUM(contractDetails.ReimbAllowExp) as contractReimbAllowExpense, SUM(contractDetails.reimbAllowCons) as contractReimbAllowConsultant FROM contractDetails JOIN contracts on contractDetails.wbs1=contracts.wbs1 and contractDetails.contractNumber = contracts.contractNumber WHERE   contracts.period <=@Period  AND NOT contractDetails.WBS3 =/*N*/'' '' AND EXISTS (select * from @tabWBSList wbsList where wbsList.wbs1=contractDetails.wbs1 and wbsList.wbs2=contractDetails.wbs2 and wbsList.wbs3=contractDetails.wbs3) GROUP BY contractDetails.WBS1,contractDetails.WBS2,contractDetails.WBS3 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SELECT omega.*
, phi.ETC
, phi.EAC
, phi.PrcntComp
, phi.WriteoffAdjmt
FROM
(
SELECT IsNull(substring(PR.Org, 4, 2), '''') As group1, Min(groupCFGOrgCodes2.Label) As groupDesc1, IsNull(substring(PR.Org, 7, 3), '''') As group2, Min(groupCFGOrgCodes3.Label) As groupDesc2, ''0'' as recordType,'''' subType, 
      0 AS hrsCur,0 hrsCurLabReg,0 hrsCurLabOvt,0 AS hrsYTD,0 hrsYTDLabReg,0 hrsYTDLabOvt,0 AS hrsJTD,0 hrsJTDLabReg,0 hrsJTDLabOvt,0 AS hrsCustom,0 hrsCustomLabReg,0 hrsCustomLabOvt,0 AS hrsBud, 
      0 AS hrsPctComp_c,0 as hrsPctComp_b, 0 AS hrsETC, 0 AS hrsEAC, 
      0 amtCurLabReg_c,0 amtCurLabReg_b,0 amtCurLabOvt_c,0 amtCurLabOvt_b,0 amtYTDLabReg_c,0 amtYTDLabReg_b,0 amtYTDLabOvt_c,0 amtYTDLabOvt_b,0 amtJTDLabReg_c,0 amtJTDLabReg_b,0 amtJTDLabOvt_c,0 amtJTDLabOvt_b,0 amtCustomLabReg_c,0 amtCustomLabReg_b,0 amtCustomLabOvt_c,0 amtCustomLabOvt_b,0 amtCur_c,0 amtCur_b,0 amtYTD_c,0 amtYTD_b,0 amtJTD_c,0 amtJTD_b,0 amtCustom_c,0 amtCustom_b,
      0 AS amtBud_c,0 amtBud_b, 0 AS amtEAC_c,0 amtEAC_b, 0 AS amtETC_c,0 amtETC_b, 
      0 AS amtPctComp_c,0 amtPctComp_b, 0 AS amtPctCompLab_c,0 amtPctCompLab_b, 0 AS amtPctCompExp_c,0 amtPctCompExp_b, 
      0 AS arCur_c,0 AS arCur_b, 0 AS arYTD_c,0 AS arYTD_b, 0 AS arJTD_c,0 AS arJTD_b, 0 AS arCustom_c,0 AS arCustom_b, 
      0 AS billCur_c,0 AS billCur_b, 0 AS billYTD_c,0 AS billYTD_b, 0 AS billJTD_c,0 AS billJTD_b, 0 AS billCustom_c,0 AS billCustom_b, 
      0 AS recdCur_c,0 AS recdCur_b, 0 AS recdYTD_c,0 AS recdYTD_b, 0 AS recdJTD_c,0 AS recdJTD_b, 0 AS recdCustom_c,0 AS recdCustom_b, 
      0 AS directCur_c,0 directCur_b, 0 AS directYTD_c,0 directYTD_b, 0 AS directJTD_c,0 directJTD_b, 0 AS directCustom_c,0 directCustom_b, 
      0 AS reimbCur_c,0 reimbCur_b, 0 AS reimbYTD_c,0 reimbYTD_b, 0 AS reimbJTD_c,0 reimbJTD_b, 0 AS reimbCustom_c,0 reimbCustom_b, 
      0 AS revCur_c,0 AS revCur_b, 0 AS revYTD_c,0 AS revYTD_b, 0 AS revJTD_c,0 AS revJTD_b, 0 AS revCustom_c,0 AS revCustom_b, 
      0.00 AS ubGLCur_c, 0.00 AS ubGLYTD_c, 0.00 AS ubGLJTD_c, 0.00 AS ubGLCustom_c, 0 althrsBud,0 altamtBud_c,0 altamtBud_b, 0.0 poAmt_c,0.00 poAmt_b, 
      0.0 rppctcmpcalc, 0.00 targetMult_c,0.00 targetMult_b,0.00 rpfee_c,0.00 rpfee_b,0.00 rpconsultfee_c,0.00 rpconsultfee_b,0.00 rpreimballow_c,0.00 rpreimballow_b,
     Max(ProjectCustomTabFieldsWBS1.CustFeeTypeRollup) AS wbs1customCol1, Max(ProjectCustomTabFieldsWBS1.CustEfforttoComplete) AS wbs1customCol2, Max(ProjectCustomTabFieldsWBS1.CustEffortatCompletion) AS wbs1customCol3, Max(ProjectCustomTabFieldsWBS1.CustPercentComplete) AS wbs1customCol4, Max(ProjectCustomTabFieldsWBS1.CustWriteoffAdjmt) AS wbs1customCol5, 
     Max(ProjectCustomTabFieldsWBS2.CustFeeTypeRollup) AS wbs2customCol1, Max(ProjectCustomTabFieldsWBS2.CustEfforttoComplete) AS wbs2customCol2, Max(ProjectCustomTabFieldsWBS2.CustEffortatCompletion) AS wbs2customCol3, Max(ProjectCustomTabFieldsWBS2.CustPercentComplete) AS wbs2customCol4, Max(ProjectCustomTabFieldsWBS2.CustWriteoffAdjmt) AS wbs2customCol5, 
     Max(ProjectCustomTabFieldsWBS3.CustFeeTypeRollup) as wbs3customCol1, Max(ProjectCustomTabFieldsWBS3.CustEfforttoComplete) as wbs3customCol2, Max(ProjectCustomTabFieldsWBS3.CustEffortatCompletion) as wbs3customCol3, Max(ProjectCustomTabFieldsWBS3.CustPercentComplete) as wbs3customCol4, Max(ProjectCustomTabFieldsWBS3.CustWriteoffAdjmt) as wbs3customCol5, 
      '''' As currencyCodeProj,'''' As currencyCodeBill,'''' As currencyCodeFunct,'''' as currencyCodePres_Cost, '''' as currencyCodePres_Bill, '''' as currencyCodePres_ARFee, '''' as currencyCode_Opportunity, 
      0 As currencyCodeProjCount, 0 As currencyCodeBillCount, 0 As currencyCodeFunctCount, 0 As currencyCodePresCount_Cost, 0 As currencyCodePresCount_Bill, 0 As currencyCodePresCount_ARFee, 0 As currencyCodeCount_Opportunity, 
       min(LEVEL3.WBS1) WBS1,min(LEVEL3.WBS2) WBS2,min(LEVEL3.WBS3) WBS3,min(PR.Name) w1Name,min(LEVEL2.Name) w2Name,min(LEVEL3.Name) w3Name,'''' ChargeType,
      max(PR.ChargeType) ChargeTypeCode,min(PR.SubLevel) SubLevel1,min(LEVEL2.SubLevel) SubLevel2,min(LEVEL3.SubLevel) SubLevel3, min(PR.Principal) w1principal,min(LEVEL2.Principal) w2principal,min(LEVEL3.Principal) w3principal,
      '''' w1prinName,'''' w2prinName,'''' w3prinName,
      min(PR.ProjMgr) w1projMgr,min(LEVEL2.ProjMgr) w2projMgr,min(LEVEL3.ProjMgr) w3projMgr,
      '''' w1prgName,'''' w2prgName,'''' w3prgName,
      min(PR.Supervisor) w1supervisor,min(LEVEL2.Supervisor) w2supervisor,min(LEVEL3.Supervisor) w3supervisor,'''' w1supName,'''' w2supName,'''' w3supName,
      min(PR.Address1) PRAddress1,min(PR.Address2) PRAddress2,min(PR.Address3) PRAddress3, min(PR.City) PRCity,min(PR.State) PRState,min(PR.Zip) PRZip, 
      min(PR.County) PRCounty,min(Country_PR.Country) as PRCountry,min(PR.Phone) PRPhone,min(PR.Fax) PRFax,min(PR.Email) PREmail, 
      min(PR.ClientID) ClientID,'''' ClientName,min(PR.CLAddress) CLAddress,'''' CLAddress1,'''' CLAddress2,'''' CLAddress3,'''' CLAddress4,
      '''' CLCity,'''' CLState,'''' CLZip,'''' CLCountry,'''' CLPhone,'''' CLFax,'''' CLEMail,
      min(PR.Org) w1Org,min(LEVEL2.Org) w2Org,min(LEVEL3.Org) w3Org,'''' w1OrgName,'''' w2OrgName,'''' w3OrgName,min(PR.TLProjectName) as w1TLProjectName, min(Level2.TLProjectName) as w2TLProjectName, min(Level3.TLProjectName) as w3TLProjectName, max(PR.ServProCode) as ServiceProfileCode, max(CFGServiceProfile.Description) as ServiceProfileDesc, max(LEVEL3.multamt) mult,sum(LEVEL3.multamt) mult_sum,
      sum(LEVEL3.fee) As fee_c, sum(LEVEL3.fee) As fee_b, sum(LEVEL3.consultFee) As consultFee_c, sum(LEVEL3.consultFee) As consultFee_b, sum(LEVEL3.reimbAllow) As reimbAllow_c, sum(LEVEL3.reimbAllow) As reimbAllow_b, sum(LEVEL3.feeDirLab) As feeDirectLabor,  sum(LEVEL3.feeDirExp) As feeDirectExpense,  sum(LEVEL3.reimbAllowExp) As reimbAllowExpense,  sum(LEVEL3.reimbAllowCons) As reimbAllowConsultant,  
      sum(ISNULL(WBSLevelSumQuery.LevelSumContractFee,0.0)) As contractFee,  sum(ISNULL(WBSLevelSumQuery.LevelSumContractConsultFee,0.0)) As contractConsultFee,  sum(ISNULL(WBSLevelSumQuery.LevelSumContractReimbAllow,0.0)) As contractReimbAllow,  sum(ISNULL(WBSLevelSumQuery.LevelSumContractFeeDirectLabor,0.0)) As contractFeeDirectLabor,  sum(ISNULL(WBSLevelSumQuery.LevelSumContractFeeDirectExpense,0.0)) As contractFeeDirectExpense,  sum(ISNULL(WBSLevelSumQuery.LevelSumContractReimbAllowExpense,0.0)) As contractReimbAllowExpense,  sum(ISNULL(WBSLevelSumQuery.LevelSumContractReimbAllowConsultant,0.0)) As contractReimbAllowConsultant,  
       min(case when LEVEL3.SubLevel=''N'' then LEVEL3.BudOHRate else 0 end ) BudOHRate,min(PR.Status) w1Status,min(LEVEL2.Status) w2Status,min(LEVEL3.Status) w3Status,'''' w1StatusDesc,'''' w2StatusDesc,'''' w3StatusDesc,
      max(PR.RevType) wbs1RevType,max(LEVEL2.RevType) wbs2RevType,max(LEVEL3.RevType) wbs3RevType,
      '''' wbs1RevTypeDesc,'''' wbs2RevTypeDesc,'''' wbs3RevTypeDesc,
      min(case when LEVEL3.sublevel=''N'' then LEVEL3.UnitTable else '''' end) UnitTable,min(PR.StartDate) w1StartDate,min(LEVEL2.StartDate) w2StartDate,min(LEVEL3.StartDate) w3StartDate,
      min(PR.EndDate) w1EndDate,min(LEVEL2.EndDate) w2EndDate,min(LEVEL3.EndDate) w3EndDate,
      min(PR.PctComp) w1PctComp,min(LEVEL2.PctComp) w2PctComp,min(LEVEL3.PctComp) w3PctComp,min(PR.LabPctComp) w1LabPctComp,min(LEVEL2.LabPctComp) w2LabPctComp,min(LEVEL3.LabPctComp) w3LabPctComp,min(PR.ExpPctComp) w1ExpPctComp,min(LEVEL2.ExpPctComp) w2ExpPctComp,min(LEVEL3.ExpPctComp) w3ExpPctComp,
      min(case when LEVEL3.sublevel=''N'' then CASE LEVEL3.BillByDefault WHEN ''Y'' THEN ''Yes'' WHEN ''N'' THEN ''No'' ELSE ''Category'' End else '''' end) BillByDefault,min(case when LEVEL3.sublevel=''N'' then CASE LEVEL3.BillableWarning WHEN ''N'' THEN ''None'' WHEN ''W'' THEN ''Warning'' ELSE ''Error'' End else '''' end) BillableWarning,min(case when LEVEL3.sublevel=''N'' THEN IsNull(CASE LEVEL3.BudgetedFlag WHEN ''N'' THEN ''None'' WHEN ''W'' THEN ''Warning'' ELSE ''Error'' End,'''') else '''' end) BudgetedFlag,
      min(case when LEVEL3.sublevel=''N'' THEN IsNull(CASE LEVEL3.BudgetSource WHEN ''B'' THEN ''Budget Worksheet'' WHEN ''P'' THEN ''Resource Planning'' ELSE '''' End,'''') else '''' end) BudgetSource,min(case when LEVEL3.sublevel=''N'' THEN IsNull(CASE LEVEL3.BudgetLevel WHEN ''E'' THEN ''Employee Only'' WHEN ''L'' THEN ''Labor Code Only'' WHEN ''B'' THEN ''Both'' ELSE '''' End ,'''') else '''' end) BudgetLevel,min(case when LEVEL3.sublevel=''N'' THEN IsNull(LEVEL3.BudgetedLevels,/*N*/'''') else '''' end) BudgetedLevels,
      min(case when LEVEL3.sublevel=''N'' then CASE LEVEL3.XCharge WHEN ''G'' THEN ''Global'' WHEN ''N'' THEN ''None'' ELSE ''Project'' End else '''' end) XCharge,min(case when LEVEL3.sublevel=''N'' then CASE LEVEL3.XChargeMethod WHEN ''1'' THEN ''BillingTerms'' ELSE ''Multiplier'' End else '''' end) XChargeMethod,min(case when LEVEL3.sublevel=''N'' then LEVEL3.XChargeMult else 0 end) XChargeMult,
      min(PR.ContactID) ContactID,'''' ContactFirstName,'''' ContactLastName,min(PR.CLBillingAddr) CLBillingAddr,
      min(PR.LongName) w1LongName,min(LEVEL2.LongName) w2LongName,min(LEVEL3.LongName) w3LongName,
      '''' CLBAddress1,'''' CLBAddress2,'''' CLBAddress3,'''' CLBAddress4,'''' CLBCity,'''' CLBState,'''' CLBZip,'''' CLBCountry,'''' CLBPhone,'''' CLBFax,'''' CLBEMail,
      min(PR.FederalInd) w1FederalInd,min(LEVEL2.FederalInd) w2FederalInd,min(LEVEL3.FederalInd) w3FederalInd,
      min(PR.ProjectType) w1ProjectType,min(LEVEL2.ProjectType) w2ProjectType,min(LEVEL3.ProjectType) w3ProjectType,
      '''' w1projectTypeDesc,'''' w2projectTypeDesc,'''' w3projectTypeDesc,
      min(PR.Responsibility) w1Responsibility,min(LEVEL2.Responsibility) w2Responsibility,min(LEVEL3.Responsibility) w3Responsibility,
      '''' w1ResponsibilityDesc,'''' w2ResponsibilityDesc,'''' w3ResponsibilityDesc,
      min(PR.Referable) w1Referable,min(LEVEL2.Referable) w2Referable,min(LEVEL3.Referable) w3Referable,
      min(PR.EstCompletionDate) w1EstCompletionDate,min(LEVEL2.EstCompletionDate) w2EstCompletionDate,min(LEVEL3.EstCompletionDate) w3EstCompletionDate,
      min(PR.ActCompletionDate) w1ActCompletionDate,min(LEVEL2.ActCompletionDate) w2ActCompletionDate,min(LEVEL3.ActCompletionDate) w3ActCompletionDate,
      min(PR.ContractDate) w1ContractDate,min(LEVEL2.ContractDate) w2ContractDate,min(LEVEL3.ContractDate) w3ContractDate,
      min(PR.ConstComplDate) w1ConstComplDate,min(LEVEL2.ConstComplDate) w2ConstComplDate,min(LEVEL3.ConstComplDate) w3ConstComplDate,
      min(PR.ProfServicesComplDate) w1ProfServicesComplDate,min(LEVEL2.ProfServicesComplDate) w2ProfServicesComplDate,min(LEVEL3.ProfServicesComplDate) w3ProfServicesComplDate,
      min(PR.BidDate) w1BidDate,min(LEVEL2.BidDate) w2BidDate,min(LEVEL3.BidDate) w3BidDate,
      min(PR.ComplDateComment) w1ComplDateComment,min(LEVEL2.ComplDateComment) w2ComplDateComment,min(LEVEL3.ComplDateComment) w3ComplDateComment,
      min(PR.FirmCost) w1FirmCost,min(LEVEL2.FirmCost) w2FirmCost,min(LEVEL3.FirmCost) w3FirmCost,
      min(PR.FirmCostComment) w1FirmCostComment,min(LEVEL2.FirmCostComment) w2FirmCostComment,min(LEVEL3.FirmCostComment) w3FirmCostComment,
      min(PR.TotalProjectCost) w1TotalProjectCost,min(LEVEL2.TotalProjectCost) w2TotalProjectCost,min(LEVEL3.TotalProjectCost) w3TotalProjectCost,min(PR.TotalCostComment) w1TotalCostComment,min(LEVEL2.TotalCostComment) w2TotalCostComment,min(LEVEL3.TotalCostComment) w3TotalCostComment,
      min(PR.ClientConfidential) ClientConfidential,min(PR.ClientAlias) ClientAlias,min(PR.BillingClientID) BillingClientID,'''' BillClientName,
      min(PR.BillingContactID) BillingContactID,'''' billContactLastName,'''' billContactFirstName,
      min(PR.ProposalWBS1) ProposalWBS1,min(PR.CreateUser) w1CreateUser,min(LEVEL2.CreateUser) w2CreateUser,min(LEVEL3.CreateUser) w3CreateUser,
      min(DateAdd("hh",-5,PR.CreateDate)) w1CreateDate, 
      min(DateAdd("hh",-5,LEVEL2.CreateDate)) w2CreateDate, 
      min(DateAdd("hh",-5,LEVEL3.CreateDate)) w3CreateDate, 
      min(PR.ModUser) w1ModUser,min(LEVEL2.ModUser) w2ModUser,min(LEVEL3.ModUser) w3ModUser,
      min(DateAdd("hh",-5,PR.ModDate)) w1ModDate, 
      min(DateAdd("hh",-5,LEVEL2.ModDate)) w2ModDate, 
      min(DateAdd("hh",-5,LEVEL3.ModDate)) w3ModDate, 
      min(CASE PR.CostRateMeth WHEN 0 THEN /*N*/''None'' WHEN 4 THEN /*N*/''From Employee Cost Rate'' WHEN 1 THEN /*N*/''From Labor Rate Table'' WHEN 2 THEN /*N*/''From Category Rate Table'' WHEN 3 THEN /*N*/''From Labor Code Table'' ELSE '''' END) w1CostRateMeth,
      min(CASE LEVEL2.CostRateMeth WHEN 0 THEN /*N*/''None'' WHEN 4 THEN /*N*/''From Employee Cost Rate'' WHEN 1 THEN /*N*/''From Labor Rate Table'' WHEN 2 THEN /*N*/''From Category Rate Table'' WHEN 3 THEN /*N*/''From Labor Code Table'' ELSE '''' END) w2CostRateMeth,
      min(CASE LEVEL3.CostRateMeth WHEN 0 THEN /*N*/''None'' WHEN 4 THEN /*N*/''From Employee Cost Rate'' WHEN 1 THEN /*N*/''From Labor Rate Table'' WHEN 2 THEN /*N*/''From Category Rate Table'' WHEN 3 THEN /*N*/''From Labor Code Table'' ELSE '''' END) w3CostRateMeth,
      '''' w1CostRateTableNo,'''' w2CostRateTableNo,'''' w3CostRateTableNo,
      min(CASE PR.PayRateMeth WHEN 0 THEN ''None'' WHEN 4 THEN /*N*/''From Employee Cost Rate'' WHEN 1 THEN /*N*/''From Labor Rate Table'' WHEN 2 THEN /*N*/''From Category Rate Table'' WHEN 3 THEN /*N*/''From Labor Code Table'' ELSE '''' END) w1PayRateMeth,
      min(CASE LEVEL2.PayRateMeth WHEN 0 THEN /*N*/''None'' WHEN 4 THEN ''From Employee Cost Rate'' WHEN 1 THEN /*N*/''From Labor Rate Table'' WHEN 2 THEN /*N*/''From Category Rate Table'' WHEN 3 THEN /*N*/''From Labor Code Table'' ELSE '''' END) w2PayRateMeth,
      min(CASE LEVEL3.PayRateMeth WHEN 0 THEN /*N*/''None'' WHEN 4 THEN ''From Employee Cost Rate'' WHEN 1 THEN /*N*/''From Labor Rate Table'' WHEN 2 THEN /*N*/''From Category Rate Table'' WHEN 3 THEN /*N*/''From Labor Code Table'' ELSE '''' END) w3PayRateMeth,
      '''' w1PayRateTableNo,'''' w2PayRateTableNo,'''' w3PayRateTableNo,
      min(PR.Locale) w1Locale,min(LEVEL2.Locale) w2Locale,min(LEVEL3.Locale) w3Locale,
      min(CASE PR.LineItemApproval WHEN ''Y'' THEN /*N*/''Yes'' WHEN ''N'' THEN /*N*/''No'' ELSE /*N*/''System'' End) w1LineItemApproval,min(CASE LEVEL2.LineItemApproval WHEN ''Y'' THEN /*N*/''Yes'' WHEN ''N'' THEN /*N*/''No'' ELSE /*N*/''System'' End) w2LineItemApproval,min(CASE LEVEL3.LineItemApproval WHEN ''Y'' THEN /*N*/''Yes'' WHEN ''N'' THEN /*N*/''No'' ELSE /*N*/''System'' End) w3LineItemApproval,
      min(CASE PR.LineItemApprovalEK WHEN ''Y'' THEN /*N*/''Yes'' WHEN ''N'' THEN /*N*/''No'' ELSE /*N*/''System'' End) w1LineItemApprovalEK,min(CASE LEVEL2.LineItemApprovalEK WHEN ''Y'' THEN /*N*/''Yes'' WHEN ''N'' THEN /*N*/''No'' ELSE /*N*/''System'' End) w2LineItemApprovalEK,min(CASE LEVEL3.LineItemApprovalEK WHEN ''Y'' THEN /*N*/''Yes'' WHEN ''N'' THEN /*N*/''No'' ELSE /*N*/''System'' End) w3LineItemApprovalEK,
      '''' opp_name,'''' opp_number,'''' opp_prproposalwbs1,'''' opp_prproposalname,'''' opp_org,'''' opp_orgname,'''' opp_clname,'''' opp_contactname,'''' opp_projmgrname,'''' opp_principalname,'''' opp_supervisorname,
      0 opp_revenue,0 opp_probability,0 opp_weightedrevenue,'''' opp_opendate,'''' opp_closedate,0 opp_daysopen,'''' opp_stage,'''' opp_type,'''' opp_source,'''' opp_status,'''' opp_eststartdate,'''' opp_estcompletiondate,
      '''' opp_address1,'''' opp_address2,'''' opp_address3,'''' opp_city,'''' opp_state,'''' opp_zip,'''' opp_country,'''' opp_createuser,'''' opp_createdate,'''' opp_moduser,'''' opp_moddate,'''' opp_oppdesc 
 FROM  PR As LEVEL3 LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1=ProjectCustomTabFields.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFields.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFields.WBS3  LEFT JOIN ProjectCustomTabFields as ProjectCustomTabFieldsWBS3 ON  LEVEL3.WBS1=ProjectCustomTabFieldsWBS3.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFieldsWBS3.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFieldsWBS3.WBS3  LEFT JOIN  (SELECT LEVEL3.WBS1, Max(PRNested.MaxWBS2) AS MaxWBS2, Max(PRNested.MaxWBS3) AS MaxWBS3, Max(CASE WHEN LEVEL3.WBS2 = '' '' THEN ISNULL(tabContract.contractFee,0.0) ELSE -999999999.99 END) As LevelSumContractFee, Max(CASE WHEN LEVEL3.WBS2 = '' '' THEN ISNULL(tabContract.contractReimbAllow,0.0) ELSE -999999999.99 END) As LevelSumContractReimbAllow, Max(CASE WHEN LEVEL3.WBS2 = '' '' THEN ISNULL(tabContract.contractConsultFee,0.0) ELSE -999999999.99 END) As LevelSumContractConsultFee, Max(CASE WHEN LEVEL3.WBS2 = '' '' THEN ISNULL(tabContract.contractFeeDirectLabor,0.0) ELSE -999999999.99 END) As LevelSumContractFeeDirectLabor, Max(CASE WHEN LEVEL3.WBS2 = '' '' THEN ISNULL(tabContract.contractFeeDirectExpense,0.0) ELSE -999999999.99 END) As LevelSumContractFeeDirectExpense, Max(CASE WHEN LEVEL3.WBS2 = '' '' THEN ISNULL(tabContract.contractReimbAllowExpense,0.0) ELSE -999999999.99 END) As LevelSumContractReimbAllowExpense, Max(CASE WHEN LEVEL3.WBS2 = '' '' THEN ISNULL(tabContract.contractReimbAllowConsultant,0.0) ELSE -999999999.99 END) As LevelSumContractReimbAllowConsultant FROM PR As LEVEL3  INNER JOIN  (SELECT   LEVEL3.WBS1, RTrim(Substring(Max(Left(LEVEL3.WBS2 + Space(7),7) + Left(LEVEL3.WBS3 + Space(7),7)), 1, 7)) AS MaxWBS2, RTrim(Substring(Max(Left(LEVEL3.WBS2 + Space(7),7) + Left(LEVEL3.WBS3 + Space(7),7)), 8, 7)) AS MaxWBS3 FROM PR,  PR AS LEVEL2,  PR AS LEVEL3 , ##RWL_' + @SessionId + '  WHERE LEVEL3.WBS1 = PR.WBS1 AND LEVEL3.WBS1 = LEVEL2.WBS1 AND LEVEL3.WBS2 = LEVEL2.WBS2 AND PR.WBS2 = '' '' AND PR.WBS3 = '' '' AND LEVEL2.WBS3 = '' ''  AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + '''  GROUP BY LEVEL3.WBS1) As PRNested ON LEVEL3.WBS1 = PRNested.WBS1  LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1 = ProjectCustomTabFields.WBS1 And  LEVEL3.WBS2 = ProjectCustomTabFields.WBS2 And  LEVEL3.WBS3 = ProjectCustomTabFields.WBS3 LEFT JOIN @tabContract tabContract ON tabContract.contractWBS1 = LEVEL3.WBS1 AND tabContract.contractWBS2 = LEVEL3.WBS2 AND tabContract.contractWBS3 = LEVEL3.WBS3  GROUP BY LEVEL3.WBS1) As WBSLevelSumQuery ON  LEVEL3.WBS1 = WBSLevelSumQuery.WBS1 AND  LEVEL3.WBS2 = WBSLevelSumQuery.MaxWBS2 AND  LEVEL3.WBS3 = WBSLevelSumQuery.MaxWBS3 ,  
PR AS LEVEL2 LEFT JOIN ProjectCustomTabFields As ProjectCustomTabFieldsWBS2 ON LEVEL2.WBS1=ProjectCustomTabFieldsWBS2.WBS1  AND LEVEL2.WBS2=ProjectCustomTabFieldsWBS2.WBS2 AND ProjectCustomTabFieldsWBS2.WBS3=/*N*/'''',  
PR LEFT JOIN ProjectCustomTabFields As ProjectCustomTabFieldsWBS1 ON PR.WBS1=ProjectCustomTabFieldsWBS1.WBS1  AND ProjectCustomTabFieldsWBS1.WBS2=/*N*/'''' AND ProjectCustomTabFieldsWBS1.WBS3=/*N*/''''  LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes2 ON substring(PR.Org, 4, 2) = groupCFGOrgCodes2.Code and groupCFGOrgCodes2.orgLevel = 2 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3 LEFT JOIN CFGServiceProfile on CFGServiceProfile.Code = PR.ServProCode  LEFT JOIN FW_CFGCountry Country_PR ON PR.Country=Country_PR.ISOCountryCode , ##RWL_' + @SessionId + ' 
WHERE LEVEL3.SubLevel=''N''  AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + ''' 
	   and LEVEL3.WBS1 not like ''ZZ%''
       AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 
       AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/'''' 
GROUP BY IsNull(substring(PR.Org, 4, 2), ''''), IsNull(substring(PR.Org, 7, 3), '''')

/***Labor***/
UNION ALL 
SELECT IsNull(substring(PR.Org, 4, 2), '''') As group1, Min(groupCFGOrgCodes2.Label) As groupDesc1, IsNull(substring(PR.Org, 7, 3), '''') As group2, Min(groupCFGOrgCodes3.Label) As groupDesc2, ''1'' AS recordType,'''' subType, 
      sum(CASE WHEN Period = @Period THEN IsNull(RegHrs, 0) + IsNull(OvtHrs, 0) + IsNull(SpecialOvtHrs, 0) ELSE 0 END) AS hrsCur, sum(CASE WHEN Period = @Period THEN IsNull(RegHrs, 0) ELSE 0 END) AS hrsCurLabReg, sum(CASE WHEN Period = @Period THEN IsNull(OvtHrs, 0) ELSE 0 END) AS hrsCurLabOvt, 
      sum(CASE WHEN Period >= @MinPeriod THEN IsNull(RegHrs, 0) + IsNull(OvtHrs,0) + IsNull(SpecialOvtHrs,0) ELSE 0 END) AS hrsYTD, sum(CASE WHEN Period >= @MinPeriod THEN IsNull(RegHrs,0) ELSE 0 END) AS hrsYTDLabReg, sum(CASE WHEN Period >= @MinPeriod THEN IsNull(OvtHrs,0) ELSE 0 END) AS hrsYTDLabOvt, 
      sum(IsNull(RegHrs, 0) + IsNull(OvtHrs, 0) + IsNull(SpecialOvtHrs, 0)) AS hrsJTD, sum(IsNull(RegHrs, 0)) AS hrsJTDLabReg, sum(IsNull(OvtHrs, 0)) AS hrsJTDLabOvt, 
      sum(CASE WHEN Period >= 000000 AND Period <= @Period THEN IsNull(RegHrs, 0) + IsNull(OvtHrs,0) + IsNull(SpecialOvtHrs,0) ELSE 0 END) AS hrsCustom, sum(CASE WHEN Period >= 000000 AND Period <= @Period THEN IsNull(RegHrs,0) ELSE 0 END) AS hrsCustomLabReg, sum(CASE WHEN Period >= 000000 AND Period <= @Period THEN IsNull(OvtHrs,0) ELSE 0 END) AS hrsCustomLabOvt, 
      0 AS hrsBud, 0 AS hrsPctComp_c,0 as hrsPctComp_b, 0 AS hrsETC, 0 AS hrsEAC, 
      sum((CASE WHEN Period = @Period THEN IsNull(regAmt, 0) ELSE 0 END) ) AS amtCurLabReg_c, sum(CASE WHEN Period = @Period THEN BillExt ELSE 0 END) AS amtCurLabReg_b,
      sum((CASE WHEN Period = @Period THEN IsNull(ovtAmt, 0) ELSE 0 END) ) AS amtCurLabOvt_c, 0 as amtCurLabOvt_b,
      sum((CASE WHEN Period >= @MinPeriod THEN IsNull(regAmt,0) ELSE 0 END) ) AS amtYTDLabReg_c, sum(CASE WHEN Period >= @MinPeriod THEN BillExt ELSE 0 END) AS amtYTDLabReg_b,sum((CASE WHEN Period >= @MinPeriod THEN IsNull(ovtAmt,0) ELSE 0 END) ) AS amtYTDLabOvt_c, 0 as amtYTDLabOvt_b,
      sum(IsNull(regAmt, 0) ) AS amtJTDLabReg_c, sum(BillExt) AS amtJTDLabReg_b,
      sum(IsNull(ovtAmt, 0) ) AS amtJTDLabOvt_c,0 as amtJTDLabOvt_b,
      sum((CASE WHEN Period >= 000000 AND Period <= @Period THEN IsNull(regAmt,0) ELSE 0 END) ) AS amtCustomLabReg_c, sum(CASE WHEN Period >= 000000 AND Period <= @Period THEN BillExt ELSE 0 END) AS amtCustomLabReg_b,sum((CASE WHEN Period >= 000000 AND Period <= @Period THEN IsNull(ovtAmt,0) ELSE 0 END) ) AS amtCustomLabOvt_c, 0 as amtCustomLabOvt_b,sum((CASE WHEN Period = @Period THEN IsNull(regAmt, 0) + IsNull(ovtAmt, 0) + IsNull(specialOvtAmt, 0) ELSE 0 END) ) AS amtCur_c, sum(CASE WHEN Period = @Period THEN BillExt ELSE 0 END) AS amtCur_b, 
      sum((CASE WHEN Period >= @MinPeriod THEN IsNull(regAmt, 0) + IsNull(ovtAmt,0) + IsNull(specialOvtAmt,0) ELSE 0 END) ) AS amtYTD_c, sum(CASE WHEN Period >= @MinPeriod THEN BillExt ELSE 0 END) AS amtYTD_b, 
      sum((IsNull(regAmt, 0) + IsNull(ovtAmt, 0) + IsNull(specialOvtAmt, 0)) ) AS amtJTD_c, sum(BillExt) AS amtJTD_b, 
      sum((CASE WHEN Period >= 000000 AND Period <= @Period THEN IsNull(regAmt, 0) + IsNull(ovtAmt,0) + IsNull(specialOvtAmt,0) ELSE 0 END) ) AS amtCustom_c, sum(CASE WHEN Period >= 000000 AND Period <= @Period THEN BillExt ELSE 0 END) AS amtCustom_b, 
      0 AS amtBud_c,0 amtBud_b, 0 AS amtEAC_c,0 amtEAC_b, 0 AS amtETC_c,0 amtETC_b, 
      0 AS amtPctComp_c,0 amtPctComp_b, 0 AS amtPctCompLab_c,0 amtPctCompLab_b, 0 AS amtPctCompExp_c,0 amtPctCompExp_b, 
      0 AS arCur_c,0 AS arCur_b, 0 AS arYTD_c,0 AS arYTD_b, 0 AS arJTD_c,0 AS arJTD_b, 0 AS arCustom_c,0 AS arCustom_b, 
      0 AS billCur_c,0 AS billCur_b, 0 AS billYTD_c,0 AS billYTD_b, 0 AS billJTD_c,0 AS billJTD_b, 0 AS billCustom_c,0 AS billCustom_b, 
      0 AS recdCur_c,0 AS recdCur_b, 0 AS recdYTD_c,0 AS recdYTD_b, 0 AS recdJTD_c,0 AS recdJTD_b, 0 AS recdCustom_c,0 AS recdCustom_b, 
      0 AS directCur_c,0 directCur_b, 0 AS directYTD_c,0 directYTD_b, 0 AS directJTD_c,0 directJTD_b, 0 AS directCustom_c,0 directCustom_b, 
      0 AS reimbCur_c,0 reimbCur_b, 0 AS reimbYTD_c,0 reimbYTD_b, 0 AS reimbJTD_c,0 reimbJTD_b, 0 AS reimbCustom_c,0 reimbCustom_b, 
      0 AS revCur_c,0 AS revCur_b, 0 AS revYTD_c,0 AS revYTD_b, 0 AS revJTD_c,0 AS revJTD_b, 0 AS revCustom_c,0 AS revCustom_b, 
      0.00 AS ubGLCur_c, 0.00 AS ubGLYTD_c, 0.00 AS ubGLJTD_c, 0.00 AS ubGLCustom_c, 0 althrsBud,0 altamtBud_c,0 altamtBud_b,
       0.0 poAmt_c,0.00 poAmt_b,
      0.0 rppctcmpcalc, 0.00 targetMult_c,0.00 targetMult_b,0.00 rpfee_c,0.00 rpfee_b,0.00 rpconsultfee_c,0.00 rpconsultfee_b,0.00 rpreimballow_c,0.00 rpreimballow_b,
     '''' AS wbs1customCol1,'''' AS wbs1customCol2,'''' AS wbs1customCol3,'''' AS wbs1customCol4,'''' AS wbs1customCol5,
     '''' AS wbs2customCol1,'''' AS wbs2customCol2,'''' AS wbs2customCol3,'''' AS wbs2customCol4,'''' AS wbs2customCol5,
     '''' AS wbs3customCol1,'''' AS wbs3customCol2,'''' AS wbs3customCol3,'''' AS wbs3customCol4,'''' AS wbs3customCol5,
      '''' As currencyCodeProj,'''' As currencyCodeBill,'''' As currencyCodeFunct,'''' as currencyCodePres_Cost, '''' as currencyCodePres_Bill, '''' as currencyCodePres_ARFee, '''' as currencyCode_Opportunity, 
      0 As currencyCodeProjCount, 0 As currencyCodeBillCount, 0 As currencyCodeFunctCount, 0 As currencyCodePresCount_Cost, 0 As currencyCodePresCount_Bill, 0 As currencyCodePresCount_ARFee, 0 As currencyCodeCount_Opportunity, 
       min(PR.WBS1) WBS1,min(LEVEL2.WBS2) WBS2,min(LEVEL3.WBS3) WBS3,'''' w1Name,'''' w2Name,'''' w3Name,
      '''' ChargeType,max(PR.ChargeType) ChargeTypeCode,min(PR.SubLevel) SubLevel1,min(LEVEL2.SubLevel) SubLevel2,min(LEVEL3.SubLevel) SubLevel3,
      '''' w1principal,'''' w2principal,'''' w3principal,'''' w1prinName,'''' w2prinName,'''' w3prinName,'''' w1projMgr,'''' w2projMgr,'''' w3projMgr,'''' w1prgName,'''' w2prgName,'''' w3prgName,'''' w1supervisor,'''' w2supervisor,'''' w3supervisor,'''' w1supName,'''' w2supName,'''' w3supName,
      '''' PRAddress1,'''' PRAddress2,'''' PRAddress3, '''' PRCity,'''' PRState,'''' PRZip,'''' PRCounty,'''' PRCountry,'''' PRPhone,'''' PRFax,'''' PREmail,
      '''' ClientID,'''' ClientName,'''' CLAddress,'''' CLAddress1,'''' CLAddress2,'''' CLAddress3,'''' CLAddress4,'''' CLCity,'''' CLState,'''' CLZip,'''' CLCountry,'''' CLPhone,'''' CLFax,'''' CLEMail,
      '''' w1Org,'''' w2Org,'''' w3Org,'''' w1OrgName,'''' w2OrgName,'''' w3OrgName,
      '''' as w1TLProjectName, '''' as w2TLProjectName, '''' as w3TLProjectName, '''' ServiceProfileCode,'''' ServiceProfileDesc,
      0 mult,0 mult_sum,0 fee_c,0 fee_b,
      0 consultFee_c,0 consultFee_b,0 reimbAllow_c,0 reimbAllow_b,
      0.0 feeDirectLabor, 0.0 feeDirectExpense, 0.0 reimbAllowExpense, 0.0 reimbAllowConsultant,
      0 contractfee, 0 contractconsultFee, 0 contractreimbAllow,
      0.0 contractFeeDirectLabor, 0.0 contractFeeDirectExpense, 0.0 contractReimbAllowExpense, 0.0 contractReimbAllowConsultant,
      0 BudOHRate,'''' w1Status,'''' w2Status,'''' w3Status,'''' w1StatusDesc,'''' w2StatusDesc,'''' w3StatusDesc,'''' wbs1RevType,'''' wbs2RevType, '''' wbs3RevType,'''' wbs1RevTypeDesc,'''' wbs2RevTypeDesc,'''' wbs3RevTypeDesc,'''' UnitTable,
      NULL w1StartDate,NULL w2StartDate,NULL w3StartDate,NULL w1EndDate,NULL w2EndDate,NULL w3EndDate,
      0.00 w1PctComp,0.00 w2PctComp,0.00 w3PctComp,0.00 w1LabPctComp,0.00 w2LabPctComp,0.00 w3LabPctComp,0.00 w1ExpPctComp,0.00 w2ExpPctComp,0.00 w3ExpPctComp,
      '''' BillByDefault,'''' BillableWarning,'''' BudgetedFlag,'''' BudgetSource,'''' BudgetLevel,'''' BudgetedLevels,
      '''' XCharge,'''' XChargeMethod,0 XChargeMult,'''' ContactID,'''' ContactFirstName,'''' ContactLastName,'''' CLBillingAddr,'''' w1LongName,'''' w2LongName,'''' w3LongName,
      '''' CLBAddress1,'''' CLBAddress2,'''' CLBAddress3,'''' CLBAddress4,'''' CLBCity,'''' CLBState,'''' CLBZip,'''' CLBCountry,'''' CLBPhone,'''' CLBFax,'''' CLBEMail,
      '''' w1FederalInd,'''' w2FederalInd,'''' w3FederalInd,'''' w1ProjectType,'''' w2ProjectType,'''' w3ProjectType,'''' w1projectTypeDesc,'''' w2projectTypeDesc,'''' w3projectTypeDesc,
      '''' w1Responsibility,'''' w2Responsibility,'''' w3Responsibility,'''' w1ResponsibilityDesc,'''' w2ResponsibilityDesc,'''' w3ResponsibilityDesc,'''' w1Referable,'''' w2Referable,'''' w3Referable,
      NULL w1EstCompletionDate,NULL w2EstCompletionDate,NULL w3EstCompletionDate,NULL w1ActCompletionDate,NULL w2ActCompletionDate,NULL w3ActCompletionDate,NULL w1ContractDate,NULL w2ContractDate,NULL w3ContractDate,
      NULL w1ConstComplDate,NULL w2ConstComplDate,NULL w3ConstComplDate,NULL w1ProfServicesComplDate,NULL w2ProfServicesComplDate,NULL w3ProfServicesComplDate,NULL w1BidDate,NULL w2BidDate,NULL w3BidDate,
      '''' w1ComplDateComment,'''' w2ComplDateComment,'''' w3ComplDateComment,0 w1FirmCost,0 w2FirmCost,0 w3FirmCost,'''' w1FirmCostComment,'''' w2FirmCostComment,'''' w3FirmCostComment,
      0 w1TotalProjectCost,0 w2TotalProjectCost,0 w3TotalProjectCost,'''' w1TotalCostComment,'''' w2TotalCostComment,'''' w3TotalCostComment,
      '''' ClientConfidential,'''' ClientAlias,'''' BillingClientID,'''' BillClientName,'''' BillingContactID,'''' billContactLastName,'''' billContactFirstName,'''' ProposalWBS1,
      '''' w1CreateUser,'''' w2CreateUser,'''' w3CreateUser,NULL w1CreateDate,NULL w2CreateDate,NULL w3CreateDate,
      '''' w1ModUser,'''' w2ModUser,'''' w3ModUser,NULL w1ModDate,NULL w2ModDate,NULL w3ModDate,
      '''' w1CostRateMeth,'''' w2CostRateMeth,'''' w3CostRateMeth,'''' w1CostRateTableNo,'''' w3CostRateTableNo,'''' w3CostRateTableNo,
      '''' w1PayRateMeth,'''' w2PayRateMeth,'''' w3PayRateMeth,'''' w1PayRateTableNo,'''' w2PayRateTableNo,'''' w3PayRateTableNo,'''' w1Locale,'''' w1Locale,'''' w1Locale,
      '''' w1LineItemApproval,'''' w2LineItemApproval,'''' w3LineItemApproval,'''' w1LineItemApprovalEK,'''' w2LineItemApprovalEK,'''' w3LineItemApprovalEK,
      '''' opp_name,'''' opp_number,'''' opp_prproposalwbs1,'''' opp_prproposalname,'''' opp_org,'''' opp_orgname,'''' opp_clname,'''' opp_contactname,'''' opp_projmgrname,'''' opp_principalname,'''' opp_supervisorname,
      0 opp_revenue,'''' opp_probability,0 opp_weightedrevenue,NULL opp_opendate,NULL opp_closedate,'''' opp_daysopen,'''' opp_stage,'''' opp_type,'''' opp_source,'''' opp_status,NULL opp_eststartdate,NULL opp_estcompletiondate,
      '''' opp_address1,'''' opp_address2,'''' opp_address3,'''' opp_city,'''' opp_state,'''' opp_zip,'''' opp_country,'''' opp_createuser,NULL opp_createdate,'''' opp_moduser,NULL opp_moddate,'''' opp_oppdesc 
FROM PR As LEVEL3 LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1=ProjectCustomTabFields.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFields.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFields.WBS3 , PR AS LEVEL2 , PR  LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes2 ON substring(PR.Org, 4, 2) = groupCFGOrgCodes2.Code and groupCFGOrgCodes2.orgLevel = 2 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3, ##RWL_' + @SessionId + ' , LD  
WHERE LD.WBS1 = LEVEL3.WBS1 AND LD.WBS2 = LEVEL3.WBS2 AND LD.WBS3 = LEVEL3.WBS3  AND LD.ProjectCost = ''Y''  AND LD.Period <= @Period AND (LD.Period >= @MinPeriod OR PR.ChargeType <> ''H'')  AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + ''' 
		 and LEVEL3.WBS1 not like ''ZZ%''
       AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 
       AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/'''' 
GROUP BY IsNull(substring(PR.Org, 4, 2), ''''), IsNull(substring(PR.Org, 7, 3), '''')

/***Overhead***/
UNION ALL 
SELECT IsNull(substring(PR.Org, 4, 2), '''') As group1, Min(groupCFGOrgCodes2.Label) As groupDesc1, IsNull(substring(PR.Org, 7, 3), '''') As group2, Min(groupCFGOrgCodes3.Label) As groupDesc2, ''2'' AS recordType,'''' subType, 
      0 AS hrsCur,0 hrsCurLabReg,0 hrsCurLabOvt,0 AS hrsYTD,0 hrsYTDLabReg,0 hrsYTDLabOvt,0 AS hrsJTD,0 hrsJTDLabReg,0 hrsJTDLabOvt,0 AS hrsCustom,0 hrsCustomLabReg,0 hrsCustomLabOvt,0 AS hrsBud, 
      0 AS hrsPctComp_c,0 as hrsPctComp_b, 0 AS hrsETC, 0 AS hrsEAC, 
      0 amtCurLabReg_c,0 amtCurLabReg_b,0 amtCurLabOvt_c,0 amtCurLabOvt_b,0 amtYTDLabReg_c,0 amtYTDLabReg_b,0 amtYTDLabOvt_c,0 amtYTDLabOvt_b,0 amtJTDLabReg_c,0 amtJTDLabReg_b,0 amtJTDLabOvt_c,0 amtJTDLabOvt_b,0 amtCustomLabReg_c,0 amtCustomLabReg_b,0 amtCustomLabOvt_c,0 amtCustomLabOvt_b,sum(CASE WHEN Period = @Period THEN regOH ELSE 0 END) AS amtCur_c, 0 amtCur_b,sum(CASE WHEN Period >= @MinPeriod THEN regOH ELSE 0 END) AS amtYTD_c,0 amtYTD_b, sum(regOH) AS amtJTD_c,0 amtJTD_b, sum(CASE WHEN Period >= 000000 AND Period <= @Period THEN regOH ELSE 0 END) AS amtCustom_c,0 amtCustom_b, 
      0 AS amtBud_c,0 amtBud_b, 0 AS amtEAC_c,0 amtEAC_b, 0 AS amtETC_c,0 amtETC_b, 
      0 AS amtPctComp_c,0 amtPctComp_b, 0 AS amtPctCompLab_c,0 amtPctCompLab_b, 0 AS amtPctCompExp_c,0 amtPctCompExp_b, 
      0 AS arCur_c,0 AS arCur_b, 0 AS arYTD_c,0 AS arYTD_b, 0 AS arJTD_c,0 AS arJTD_b, 0 AS arCustom_c,0 AS arCustom_b, 
      0 AS billCur_c,0 AS billCur_b, 0 AS billYTD_c,0 AS billYTD_b, 0 AS billJTD_c,0 AS billJTD_b, 0 AS billCustom_c,0 AS billCustom_b, 
      0 AS recdCur_c,0 AS recdCur_b, 0 AS recdYTD_c,0 AS recdYTD_b, 0 AS recdJTD_c,0 AS recdJTD_b, 0 AS recdCustom_c,0 AS recdCustom_b, 
      0 AS directCur_c,0 directCur_b, 0 AS directYTD_c,0 directYTD_b, 0 AS directJTD_c,0 directJTD_b, 0 AS directCustom_c,0 directCustom_b, 
      0 AS reimbCur_c,0 reimbCur_b, 0 AS reimbYTD_c,0 reimbYTD_b, 0 AS reimbJTD_c,0 reimbJTD_b, 0 AS reimbCustom_c,0 reimbCustom_b, 
      0 AS revCur_c,0 AS revCur_b, 0 AS revYTD_c,0 AS revYTD_b, 0 AS revJTD_c,0 AS revJTD_b, 0 AS revCustom_c,0 AS revCustom_b, 
      0.00 AS ubGLCur_c, 0.00 AS ubGLYTD_c, 0.00 AS ubGLJTD_c, 0.00 AS ubGLCustom_c, 0 althrsBud,0 altamtBud_c,0 altamtBud_b, 0.0 poAmt_c,0.00 poAmt_b,
      0.0 rppctcmpcalc, 0.00 targetMult_c,0.00 targetMult_b,0.00 rpfee_c,0.00 rpfee_b,0.00 rpconsultfee_c,0.00 rpconsultfee_b,0.00 rpreimballow_c,0.00 rpreimballow_b,
     '''' AS wbs1customCol1,'''' AS wbs1customCol2,'''' AS wbs1customCol3,'''' AS wbs1customCol4,'''' AS wbs1customCol5,
     '''' AS wbs2customCol1,'''' AS wbs2customCol2,'''' AS wbs2customCol3,'''' AS wbs2customCol4,'''' AS wbs2customCol5,
     '''' AS wbs3customCol1,'''' AS wbs3customCol2,'''' AS wbs3customCol3,'''' AS wbs3customCol4,'''' AS wbs3customCol5,
      '''' As currencyCodeProj,'''' As currencyCodeBill,'''' As currencyCodeFunct,'''' as currencyCodePres_Cost, '''' as currencyCodePres_Bill, '''' as currencyCodePres_ARFee, '''' as currencyCode_Opportunity, 
      0 As currencyCodeProjCount, 0 As currencyCodeBillCount, 0 As currencyCodeFunctCount, 0 As currencyCodePresCount_Cost, 0 As currencyCodePresCount_Bill, 0 As currencyCodePresCount_ARFee, 0 As currencyCodeCount_Opportunity, 
       min(PR.WBS1) WBS1,min(LEVEL2.WBS2) WBS2,min(LEVEL3.WBS3) WBS3,'''' w1Name,'''' w2Name,'''' w3Name,
      '''' ChargeType,max(PR.ChargeType) ChargeTypeCode,min(PR.SubLevel) SubLevel1,min(LEVEL2.SubLevel) SubLevel2,min(LEVEL3.SubLevel) SubLevel3,
      '''' w1principal,'''' w2principal,'''' w3principal,'''' w1prinName,'''' w2prinName,'''' w3prinName,'''' w1projMgr,'''' w2projMgr,'''' w3projMgr,'''' w1prgName,'''' w2prgName,'''' w3prgName,'''' w1supervisor,'''' w2supervisor,'''' w3supervisor,'''' w1supName,'''' w2supName,'''' w3supName,
      '''' PRAddress1,'''' PRAddress2,'''' PRAddress3, '''' PRCity,'''' PRState,'''' PRZip,'''' PRCounty,'''' PRCountry,'''' PRPhone,'''' PRFax,'''' PREmail,
      '''' ClientID,'''' ClientName,'''' CLAddress,'''' CLAddress1,'''' CLAddress2,'''' CLAddress3,'''' CLAddress4,'''' CLCity,'''' CLState,'''' CLZip,'''' CLCountry,'''' CLPhone,'''' CLFax,'''' CLEMail,
      '''' w1Org,'''' w2Org,'''' w3Org,'''' w1OrgName,'''' w2OrgName,'''' w3OrgName,
      '''' as w1TLProjectName, '''' as w2TLProjectName, '''' as w3TLProjectName, '''' ServiceProfileCode,'''' ServiceProfileDesc,
      0 mult,0 mult_sum,0 fee_c,0 fee_b,
      0 consultFee_c,0 consultFee_b,0 reimbAllow_c,0 reimbAllow_b,
      0.0 feeDirectLabor, 0.0 feeDirectExpense, 0.0 reimbAllowExpense, 0.0 reimbAllowConsultant,
      0 contractfee, 0 contractconsultFee, 0 contractreimbAllow,
      0.0 contractFeeDirectLabor, 0.0 contractFeeDirectExpense, 0.0 contractReimbAllowExpense, 0.0 contractReimbAllowConsultant,
      0 BudOHRate,'''' w1Status,'''' w2Status,'''' w3Status,'''' w1StatusDesc,'''' w2StatusDesc,'''' w3StatusDesc,'''' wbs1RevType,'''' wbs2RevType, '''' wbs3RevType,'''' wbs1RevTypeDesc,'''' wbs2RevTypeDesc,'''' wbs3RevTypeDesc,'''' UnitTable,
      NULL w1StartDate,NULL w2StartDate,NULL w3StartDate,NULL w1EndDate,NULL w2EndDate,NULL w3EndDate,
      0.00 w1PctComp,0.00 w2PctComp,0.00 w3PctComp,0.00 w1LabPctComp,0.00 w2LabPctComp,0.00 w3LabPctComp,0.00 w1ExpPctComp,0.00 w2ExpPctComp,0.00 w3ExpPctComp,
      '''' BillByDefault,'''' BillableWarning,'''' BudgetedFlag,'''' BudgetSource,'''' BudgetLevel,'''' BudgetedLevels,
      '''' XCharge,'''' XChargeMethod,0 XChargeMult,'''' ContactID,'''' ContactFirstName,'''' ContactLastName,'''' CLBillingAddr,'''' w1LongName,'''' w2LongName,'''' w3LongName,
      '''' CLBAddress1,'''' CLBAddress2,'''' CLBAddress3,'''' CLBAddress4,'''' CLBCity,'''' CLBState,'''' CLBZip,'''' CLBCountry,'''' CLBPhone,'''' CLBFax,'''' CLBEMail,
      '''' w1FederalInd,'''' w2FederalInd,'''' w3FederalInd,'''' w1ProjectType,'''' w2ProjectType,'''' w3ProjectType,'''' w1projectTypeDesc,'''' w2projectTypeDesc,'''' w3projectTypeDesc,
      '''' w1Responsibility,'''' w2Responsibility,'''' w3Responsibility,'''' w1ResponsibilityDesc,'''' w2ResponsibilityDesc,'''' w3ResponsibilityDesc,'''' w1Referable,'''' w2Referable,'''' w3Referable,
      NULL w1EstCompletionDate,NULL w2EstCompletionDate,NULL w3EstCompletionDate,NULL w1ActCompletionDate,NULL w2ActCompletionDate,NULL w3ActCompletionDate,NULL w1ContractDate,NULL w2ContractDate,NULL w3ContractDate,
      NULL w1ConstComplDate,NULL w2ConstComplDate,NULL w3ConstComplDate,NULL w1ProfServicesComplDate,NULL w2ProfServicesComplDate,NULL w3ProfServicesComplDate,NULL w1BidDate,NULL w2BidDate,NULL w3BidDate,
      '''' w1ComplDateComment,'''' w2ComplDateComment,'''' w3ComplDateComment,0 w1FirmCost,0 w2FirmCost,0 w3FirmCost,'''' w1FirmCostComment,'''' w2FirmCostComment,'''' w3FirmCostComment,
      0 w1TotalProjectCost,0 w2TotalProjectCost,0 w3TotalProjectCost,'''' w1TotalCostComment,'''' w2TotalCostComment,'''' w3TotalCostComment,
      '''' ClientConfidential,'''' ClientAlias,'''' BillingClientID,'''' BillClientName,'''' BillingContactID,'''' billContactLastName,'''' billContactFirstName,'''' ProposalWBS1,
      '''' w1CreateUser,'''' w2CreateUser,'''' w3CreateUser,NULL w1CreateDate,NULL w2CreateDate,NULL w3CreateDate,
      '''' w1ModUser,'''' w2ModUser,'''' w3ModUser,NULL w1ModDate,NULL w2ModDate,NULL w3ModDate,
      '''' w1CostRateMeth,'''' w2CostRateMeth,'''' w3CostRateMeth,'''' w1CostRateTableNo,'''' w3CostRateTableNo,'''' w3CostRateTableNo,
      '''' w1PayRateMeth,'''' w2PayRateMeth,'''' w3PayRateMeth,'''' w1PayRateTableNo,'''' w2PayRateTableNo,'''' w3PayRateTableNo,'''' w1Locale,'''' w1Locale,'''' w1Locale,
      '''' w1LineItemApproval,'''' w2LineItemApproval,'''' w3LineItemApproval,'''' w1LineItemApprovalEK,'''' w2LineItemApprovalEK,'''' w3LineItemApprovalEK,
      '''' opp_name,'''' opp_number,'''' opp_prproposalwbs1,'''' opp_prproposalname,'''' opp_org,'''' opp_orgname,'''' opp_clname,'''' opp_contactname,'''' opp_projmgrname,'''' opp_principalname,'''' opp_supervisorname,
      0 opp_revenue,'''' opp_probability,0 opp_weightedrevenue,NULL opp_opendate,NULL opp_closedate,'''' opp_daysopen,'''' opp_stage,'''' opp_type,'''' opp_source,'''' opp_status,NULL opp_eststartdate,NULL opp_estcompletiondate,
      '''' opp_address1,'''' opp_address2,'''' opp_address3,'''' opp_city,'''' opp_state,'''' opp_zip,'''' opp_country,'''' opp_createuser,NULL opp_createdate,'''' opp_moduser,NULL opp_moddate,'''' opp_oppdesc 
FROM PR As LEVEL3 LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1=ProjectCustomTabFields.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFields.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFields.WBS3 , PR AS LEVEL2 , PR  LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes2 ON substring(PR.Org, 4, 2) = groupCFGOrgCodes2.Code and groupCFGOrgCodes2.orgLevel = 2 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3, ##RWL_' + @SessionId + ' , PRF 
WHERE ((PRF.Wbs1 = LEVEL3.Wbs1 AND PRF.Wbs2 = LEVEL3.Wbs2 AND PRF.Wbs3 = LEVEL3.Wbs3) 
 and LEVEL3.WBS1 not like ''ZZ%''
 AND (PRF.Wbs1 = LEVEL2.Wbs1 AND PRF.Wbs2 = LEVEL2.wbs2)
 AND (PRF.Wbs1 = PR.Wbs1) 
 AND (PR.Wbs2 = /*N*/'' '' AND PR.Wbs3 = /*N*/'' '' AND LEVEL2.Wbs3 = /*N*/'' '')) AND PR.ChargeType = ''R'' AND PRF.Period <= @Period AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + ''' 
       AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 
       AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/'''' 
GROUP BY IsNull(substring(PR.Org, 4, 2), ''''), IsNull(substring(PR.Org, 7, 3), '''')

/***Direct and Reimbursable Expenses***/
UNION ALL 
SELECT IsNull(substring(PR.Org, 4, 2), '''') As group1, Min(groupCFGOrgCodes2.Label) As groupDesc1, IsNull(substring(PR.Org, 7, 3), '''') As group2, Min(groupCFGOrgCodes3.Label) As groupDesc2, 
       CASE WHEN CA.Type < 7 THEN ''5''  WHEN CA.Type >= 7 AND CA.Type < 9 THEN ''3''  WHEN CA.Type >= 9 AND CA.Type < 10 THEN ''6''  ELSE ''7'' END As recordType,  min(CASE WHEN (CA.Type = 6) OR (CA.Type = 8) THEN ''0'' ELSE ''1'' END) subType, 
      0 AS hrsCur,0 hrsCurLabReg,0 hrsCurLabOvt,0 AS hrsYTD,0 hrsYTDLabReg,0 hrsYTDLabOvt,0 AS hrsJTD,0 hrsJTDLabReg,0 hrsJTDLabOvt,0 AS hrsCustom,0 hrsCustomLabReg,0 hrsCustomLabOvt,0 AS hrsBud, 
      0 AS hrsPctComp_c,0 as hrsPctComp_b, 0 AS hrsETC, 0 AS hrsEAC, 
      0 amtCurLabReg_c,0 amtCurLabReg_b,0 amtCurLabOvt_c,0 amtCurLabOvt_b,0 amtYTDLabReg_c,0 amtYTDLabReg_b,0 amtYTDLabOvt_c,0 amtYTDLabOvt_b,0 amtJTDLabReg_c,0 amtJTDLabReg_b,0 amtJTDLabOvt_c,0 amtJTDLabOvt_b,0 amtCustomLabReg_c,0 amtCustomLabReg_b,0 amtCustomLabOvt_c,0 amtCustomLabOvt_b,
      sum(CASE WHEN Period = @Period THEN amount ELSE 0 END) AS amtCur_c, sum(CASE WHEN Period = @Period THEN BillExt ELSE 0 END) AS amtCur_b, 
      sum(CASE WHEN Period >= @MinPeriod THEN amount ELSE 0 END) AS amtYTD_c, sum(CASE WHEN Period >= @MinPeriod THEN BillExt ELSE 0 END) AS amtYTD_b, 
      sum(amount) AS amtJTD_c, sum(BillExt) AS amtJTD_b, 
      sum(CASE WHEN Period >= @MinPeriod THEN amount ELSE 0 END) AS amtCustom_c, sum(CASE WHEN Period >= @MinPeriod THEN BillExt ELSE 0 END) AS amtCustom_b, 
      0 AS amtBud_c,0 amtBud_b, 0 AS amtEAC_c,0 amtEAC_b, 0 AS amtETC_c,0 amtETC_b, 
      0 AS amtPctComp_c,0 amtPctComp_b, 0 AS amtPctCompLab_c,0 amtPctCompLab_b, 0 AS amtPctCompExp_c,0 amtPctCompExp_b, 
      0 AS arCur_c,0 AS arCur_b, 0 AS arYTD_c,0 AS arYTD_b, 0 AS arJTD_c,0 AS arJTD_b, 0 AS arCustom_c,0 AS arCustom_b, 0 AS billCur_c,0 AS billCur_b, 0 AS billYTD_c,0 AS billYTD_b, 0 AS billJTD_c,0 AS billJTD_b, 0 AS billCustom_c,0 AS billCustom_b, 0 AS recdCur_c,0 AS recdCur_b, 0 AS recdYTD_c,0 AS recdYTD_b, 0 AS recdJTD_c,0 AS recdJTD_b, 0 AS recdCustom_c,0 AS recdCustom_b, 
      sum(CASE WHEN Period = @Period AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(amount, 0)  ELSE 0 END) AS directCur_c, sum(CASE WHEN Period = @Period AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(BillExt, 0)  ELSE 0 END) AS directCur_b, 
      sum(CASE WHEN Period >= @MinPeriod AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(amount, 0) ELSE 0 END) AS directYTD_c, sum(CASE WHEN Period >= @MinPeriod AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(BillExt, 0) ELSE 0 END) AS directYTD_b, 
      sum(CASE WHEN (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(amount, 0) ELSE 0 END) AS directJTD_c, sum(CASE WHEN (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(BillExt, 0) ELSE 0 END) AS directJTD_b, 
      sum(CASE WHEN Period >= 000000 AND Period <= @Period AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(amount, 0) ELSE 0 END) AS directCustom_c, sum(CASE WHEN Period >= 000000 AND Period <= @Period AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(BillExt, 0) ELSE 0 END) AS directCustom_b, 
      sum(CASE WHEN Period = @Period AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(amount, 0)  ELSE 0 END) AS reimbCur_c, sum(CASE WHEN Period = @Period AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(BillExt, 0)  ELSE 0 END) AS reimbCur_b, 
      sum(CASE WHEN Period >= @MinPeriod AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(amount, 0) ELSE 0 END) AS reimbYTD_c, sum(CASE WHEN Period >= @MinPeriod AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(BillExt, 0) ELSE 0 END) AS reimbYTD_b, 
      sum(CASE WHEN (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(amount, 0) ELSE 0 END) AS reimbJTD_c, sum(CASE WHEN (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(BillExt, 0) ELSE 0 END) AS reimbJTD_b, 
      sum(CASE WHEN Period >= 000000 AND Period <= @Period AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(amount, 0) ELSE 0 END) AS reimbCustom_c, sum(CASE WHEN Period >= 000000 AND Period <= @Period AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(BillExt, 0) ELSE 0 END) AS reimbCustom_b, 
      0 AS revCur_c,0 AS revCur_b, 0 AS revYTD_c,0 AS revYTD_b, 0 AS revJTD_c,0 AS revJTD_b, 0 AS revCustom_c,0 AS revCustom_b, 0 AS ubGLCur_c, 0 AS ubGLYTD_c, 0 AS ubGLJTD_c,0 AS ubGLCustom_c, 0 althrsBud,0 altamtBud_c,0 altamtBud_b, 0.0 poAmt_c,0.00 poAmt_b,
      0.0 rppctcmpcalc, 0.00 targetMult_c,0.00 targetMult_b,0.00 rpfee_c,0.00 rpfee_b,0.00 rpconsultfee_c,0.00 rpconsultfee_b,0.00 rpreimballow_c,0.00 rpreimballow_b,
     '''' AS wbs1customCol1,'''' AS wbs1customCol2,'''' AS wbs1customCol3,'''' AS wbs1customCol4,'''' AS wbs1customCol5,
     '''' AS wbs2customCol1,'''' AS wbs2customCol2,'''' AS wbs2customCol3,'''' AS wbs2customCol4,'''' AS wbs2customCol5,
     '''' AS wbs3customCol1,'''' AS wbs3customCol2,'''' AS wbs3customCol3,'''' AS wbs3customCol4,'''' AS wbs3customCol5,
      '''' As currencyCodeProj,'''' As currencyCodeBill,'''' As currencyCodeFunct,'''' as currencyCodePres_Cost, '''' as currencyCodePres_Bill, '''' as currencyCodePres_ARFee, '''' as currencyCode_Opportunity, 
      0 As currencyCodeProjCount, 0 As currencyCodeBillCount, 0 As currencyCodeFunctCount, 0 As currencyCodePresCount_Cost, 0 As currencyCodePresCount_Bill, 0 As currencyCodePresCount_ARFee, 0 As currencyCodeCount_Opportunity, 
       min(PR.WBS1) WBS1,min(LEVEL2.WBS2) WBS2,min(LEVEL3.WBS3) WBS3,'''' w1Name,'''' w2Name,'''' w3Name,
      '''' ChargeType,max(PR.ChargeType) ChargeTypeCode,min(PR.SubLevel) SubLevel1,min(LEVEL2.SubLevel) SubLevel2,min(LEVEL3.SubLevel) SubLevel3,
      '''' w1principal,'''' w2principal,'''' w3principal,'''' w1prinName,'''' w2prinName,'''' w3prinName,'''' w1projMgr,'''' w2projMgr,'''' w3projMgr,'''' w1prgName,'''' w2prgName,'''' w3prgName,'''' w1supervisor,'''' w2supervisor,'''' w3supervisor,'''' w1supName,'''' w2supName,'''' w3supName,
      '''' PRAddress1,'''' PRAddress2,'''' PRAddress3, '''' PRCity,'''' PRState,'''' PRZip,'''' PRCounty,'''' PRCountry,'''' PRPhone,'''' PRFax,'''' PREmail,
      '''' ClientID,'''' ClientName,'''' CLAddress,'''' CLAddress1,'''' CLAddress2,'''' CLAddress3,'''' CLAddress4,'''' CLCity,'''' CLState,'''' CLZip,'''' CLCountry,'''' CLPhone,'''' CLFax,'''' CLEMail,
      '''' w1Org,'''' w2Org,'''' w3Org,'''' w1OrgName,'''' w2OrgName,'''' w3OrgName,
      '''' as w1TLProjectName, '''' as w2TLProjectName, '''' as w3TLProjectName, '''' ServiceProfileCode,'''' ServiceProfileDesc,
      0 mult,0 mult_sum,0 fee_c,0 fee_b,
      0 consultFee_c,0 consultFee_b,0 reimbAllow_c,0 reimbAllow_b,
      0.0 feeDirectLabor, 0.0 feeDirectExpense, 0.0 reimbAllowExpense, 0.0 reimbAllowConsultant,
      0 contractfee, 0 contractconsultFee, 0 contractreimbAllow,
      0.0 contractFeeDirectLabor, 0.0 contractFeeDirectExpense, 0.0 contractReimbAllowExpense, 0.0 contractReimbAllowConsultant,
      0 BudOHRate,'''' w1Status,'''' w2Status,'''' w3Status,'''' w1StatusDesc,'''' w2StatusDesc,'''' w3StatusDesc,'''' wbs1RevType,'''' wbs2RevType, '''' wbs3RevType,'''' wbs1RevTypeDesc,'''' wbs2RevTypeDesc,'''' wbs3RevTypeDesc,'''' UnitTable,
      NULL w1StartDate,NULL w2StartDate,NULL w3StartDate,NULL w1EndDate,NULL w2EndDate,NULL w3EndDate,
      0.00 w1PctComp,0.00 w2PctComp,0.00 w3PctComp,0.00 w1LabPctComp,0.00 w2LabPctComp,0.00 w3LabPctComp,0.00 w1ExpPctComp,0.00 w2ExpPctComp,0.00 w3ExpPctComp,
      '''' BillByDefault,'''' BillableWarning,'''' BudgetedFlag,'''' BudgetSource,'''' BudgetLevel,'''' BudgetedLevels,
      '''' XCharge,'''' XChargeMethod,0 XChargeMult,'''' ContactID,'''' ContactFirstName,'''' ContactLastName,'''' CLBillingAddr,'''' w1LongName,'''' w2LongName,'''' w3LongName,
      '''' CLBAddress1,'''' CLBAddress2,'''' CLBAddress3,'''' CLBAddress4,'''' CLBCity,'''' CLBState,'''' CLBZip,'''' CLBCountry,'''' CLBPhone,'''' CLBFax,'''' CLBEMail,
      '''' w1FederalInd,'''' w2FederalInd,'''' w3FederalInd,'''' w1ProjectType,'''' w2ProjectType,'''' w3ProjectType,'''' w1projectTypeDesc,'''' w2projectTypeDesc,'''' w3projectTypeDesc,
      '''' w1Responsibility,'''' w2Responsibility,'''' w3Responsibility,'''' w1ResponsibilityDesc,'''' w2ResponsibilityDesc,'''' w3ResponsibilityDesc,'''' w1Referable,'''' w2Referable,'''' w3Referable,
      NULL w1EstCompletionDate,NULL w2EstCompletionDate,NULL w3EstCompletionDate,NULL w1ActCompletionDate,NULL w2ActCompletionDate,NULL w3ActCompletionDate,NULL w1ContractDate,NULL w2ContractDate,NULL w3ContractDate,
      NULL w1ConstComplDate,NULL w2ConstComplDate,NULL w3ConstComplDate,NULL w1ProfServicesComplDate,NULL w2ProfServicesComplDate,NULL w3ProfServicesComplDate,NULL w1BidDate,NULL w2BidDate,NULL w3BidDate,
      '''' w1ComplDateComment,'''' w2ComplDateComment,'''' w3ComplDateComment,0 w1FirmCost,0 w2FirmCost,0 w3FirmCost,'''' w1FirmCostComment,'''' w2FirmCostComment,'''' w3FirmCostComment,
      0 w1TotalProjectCost,0 w2TotalProjectCost,0 w3TotalProjectCost,'''' w1TotalCostComment,'''' w2TotalCostComment,'''' w3TotalCostComment,
      '''' ClientConfidential,'''' ClientAlias,'''' BillingClientID,'''' BillClientName,'''' BillingContactID,'''' billContactLastName,'''' billContactFirstName,'''' ProposalWBS1,
      '''' w1CreateUser,'''' w2CreateUser,'''' w3CreateUser,NULL w1CreateDate,NULL w2CreateDate,NULL w3CreateDate,
      '''' w1ModUser,'''' w2ModUser,'''' w3ModUser,NULL w1ModDate,NULL w2ModDate,NULL w3ModDate,
      '''' w1CostRateMeth,'''' w2CostRateMeth,'''' w3CostRateMeth,'''' w1CostRateTableNo,'''' w3CostRateTableNo,'''' w3CostRateTableNo,
      '''' w1PayRateMeth,'''' w2PayRateMeth,'''' w3PayRateMeth,'''' w1PayRateTableNo,'''' w2PayRateTableNo,'''' w3PayRateTableNo,'''' w1Locale,'''' w1Locale,'''' w1Locale,
      '''' w1LineItemApproval,'''' w2LineItemApproval,'''' w3LineItemApproval,'''' w1LineItemApprovalEK,'''' w2LineItemApprovalEK,'''' w3LineItemApprovalEK,
      '''' opp_name,'''' opp_number,'''' opp_prproposalwbs1,'''' opp_prproposalname,'''' opp_org,'''' opp_orgname,'''' opp_clname,'''' opp_contactname,'''' opp_projmgrname,'''' opp_principalname,'''' opp_supervisorname,
      0 opp_revenue,'''' opp_probability,0 opp_weightedrevenue,NULL opp_opendate,NULL opp_closedate,'''' opp_daysopen,'''' opp_stage,'''' opp_type,'''' opp_source,'''' opp_status,NULL opp_eststartdate,NULL opp_estcompletiondate,
      '''' opp_address1,'''' opp_address2,'''' opp_address3,'''' opp_city,'''' opp_state,'''' opp_zip,'''' opp_country,'''' opp_createuser,NULL opp_createdate,'''' opp_moduser,NULL opp_moddate,'''' opp_oppdesc 
FROM PR As LEVEL3 LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1=ProjectCustomTabFields.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFields.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFields.WBS3 , PR AS LEVEL2 , PR  LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes2 ON substring(PR.Org, 4, 2) = groupCFGOrgCodes2.Code and groupCFGOrgCodes2.orgLevel = 2 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3, ##RWL_' + @SessionId + ' , LedgerAP Left Join CA On LedgerAP.Account = CA.Account 
WHERE ((LedgerAP.Wbs1 = LEVEL3.Wbs1 AND LedgerAP.Wbs2 = LEVEL3.Wbs2 AND LedgerAP.Wbs3 = LEVEL3.Wbs3) 
 and LEVEL3.WBS1 not like ''ZZ%''
 AND (LedgerAP.Wbs1 = LEVEL2.Wbs1 AND LedgerAP.Wbs2 = LEVEL2.wbs2)
 AND (LedgerAP.Wbs1 = PR.Wbs1) 
 AND (PR.Wbs2 = /*N*/'' '' AND PR.Wbs3 = /*N*/'' '' AND LEVEL2.Wbs3 = /*N*/'' '')) AND ProjectCost = ''Y''  AND Period <= @Period AND (Period >= @MinPeriod OR PR.ChargeType <> ''H'')  AND CA.Type >= 5 AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + ''' 
       AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 
       AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/'''' 
GROUP BY IsNull(substring(PR.Org, 4, 2), ''''), IsNull(substring(PR.Org, 7, 3), ''''),  CASE WHEN CA.Type < 7 THEN ''5''  WHEN CA.Type >= 7 AND CA.Type < 9 THEN ''3''  WHEN CA.Type >= 9 AND CA.Type < 10 THEN ''6''  ELSE ''7'' END,  CASE WHEN (CA.Type = 6) OR (CA.Type = 8) THEN ''0'' ELSE ''1'' END

/***Direct and Reimbursable Expenses***/
UNION ALL 
SELECT IsNull(substring(PR.Org, 4, 2), '''') As group1, Min(groupCFGOrgCodes2.Label) As groupDesc1, IsNull(substring(PR.Org, 7, 3), '''') As group2, Min(groupCFGOrgCodes3.Label) As groupDesc2, 
       CASE WHEN CA.Type < 7 THEN ''5''  WHEN CA.Type >= 7 AND CA.Type < 9 THEN ''3''  WHEN CA.Type >= 9 AND CA.Type < 10 THEN ''6''  ELSE ''7'' END As recordType,  min(CASE WHEN (CA.Type = 6) OR (CA.Type = 8) THEN ''0'' ELSE ''1'' END) subType, 
      0 AS hrsCur,0 hrsCurLabReg,0 hrsCurLabOvt,0 AS hrsYTD,0 hrsYTDLabReg,0 hrsYTDLabOvt,0 AS hrsJTD,0 hrsJTDLabReg,0 hrsJTDLabOvt,0 AS hrsCustom,0 hrsCustomLabReg,0 hrsCustomLabOvt,0 AS hrsBud, 
      0 AS hrsPctComp_c,0 as hrsPctComp_b, 0 AS hrsETC, 0 AS hrsEAC, 
      0 amtCurLabReg_c,0 amtCurLabReg_b,0 amtCurLabOvt_c,0 amtCurLabOvt_b,0 amtYTDLabReg_c,0 amtYTDLabReg_b,0 amtYTDLabOvt_c,0 amtYTDLabOvt_b,0 amtJTDLabReg_c,0 amtJTDLabReg_b,0 amtJTDLabOvt_c,0 amtJTDLabOvt_b,0 amtCustomLabReg_c,0 amtCustomLabReg_b,0 amtCustomLabOvt_c,0 amtCustomLabOvt_b,
      sum(CASE WHEN Period = @Period THEN amount ELSE 0 END) AS amtCur_c, sum(CASE WHEN Period = @Period THEN BillExt ELSE 0 END) AS amtCur_b, 
      sum(CASE WHEN Period >= @MinPeriod THEN amount ELSE 0 END) AS amtYTD_c, sum(CASE WHEN Period >= @MinPeriod THEN BillExt ELSE 0 END) AS amtYTD_b, 
      sum(amount) AS amtJTD_c, sum(BillExt) AS amtJTD_b, 
      sum(CASE WHEN Period >= @MinPeriod THEN amount ELSE 0 END) AS amtCustom_c, sum(CASE WHEN Period >= @MinPeriod THEN BillExt ELSE 0 END) AS amtCustom_b, 
      0 AS amtBud_c,0 amtBud_b, 0 AS amtEAC_c,0 amtEAC_b, 0 AS amtETC_c,0 amtETC_b, 
      0 AS amtPctComp_c,0 amtPctComp_b, 0 AS amtPctCompLab_c,0 amtPctCompLab_b, 0 AS amtPctCompExp_c,0 amtPctCompExp_b, 
      0 AS arCur_c,0 AS arCur_b, 0 AS arYTD_c,0 AS arYTD_b, 0 AS arJTD_c,0 AS arJTD_b, 0 AS arCustom_c,0 AS arCustom_b, 0 AS billCur_c,0 AS billCur_b, 0 AS billYTD_c,0 AS billYTD_b, 0 AS billJTD_c,0 AS billJTD_b, 0 AS billCustom_c,0 AS billCustom_b, 0 AS recdCur_c,0 AS recdCur_b, 0 AS recdYTD_c,0 AS recdYTD_b, 0 AS recdJTD_c,0 AS recdJTD_b, 0 AS recdCustom_c,0 AS recdCustom_b, 
      sum(CASE WHEN Period = @Period AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(amount, 0)  ELSE 0 END) AS directCur_c, sum(CASE WHEN Period = @Period AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(BillExt, 0)  ELSE 0 END) AS directCur_b, 
      sum(CASE WHEN Period >= @MinPeriod AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(amount, 0) ELSE 0 END) AS directYTD_c, sum(CASE WHEN Period >= @MinPeriod AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(BillExt, 0) ELSE 0 END) AS directYTD_b, 
      sum(CASE WHEN (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(amount, 0) ELSE 0 END) AS directJTD_c, sum(CASE WHEN (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(BillExt, 0) ELSE 0 END) AS directJTD_b, 
      sum(CASE WHEN Period >= 000000 AND Period <= @Period AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(amount, 0) ELSE 0 END) AS directCustom_c, sum(CASE WHEN Period >= 000000 AND Period <= @Period AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(BillExt, 0) ELSE 0 END) AS directCustom_b, 
      sum(CASE WHEN Period = @Period AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(amount, 0)  ELSE 0 END) AS reimbCur_c, sum(CASE WHEN Period = @Period AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(BillExt, 0)  ELSE 0 END) AS reimbCur_b, 
      sum(CASE WHEN Period >= @MinPeriod AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(amount, 0) ELSE 0 END) AS reimbYTD_c, sum(CASE WHEN Period >= @MinPeriod AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(BillExt, 0) ELSE 0 END) AS reimbYTD_b, 
      sum(CASE WHEN (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(amount, 0) ELSE 0 END) AS reimbJTD_c, sum(CASE WHEN (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(BillExt, 0) ELSE 0 END) AS reimbJTD_b, 
      sum(CASE WHEN Period >= 000000 AND Period <= @Period AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(amount, 0) ELSE 0 END) AS reimbCustom_c, sum(CASE WHEN Period >= 000000 AND Period <= @Period AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(BillExt, 0) ELSE 0 END) AS reimbCustom_b, 
      0 AS revCur_c,0 AS revCur_b, 0 AS revYTD_c,0 AS revYTD_b, 0 AS revJTD_c,0 AS revJTD_b, 0 AS revCustom_c,0 AS revCustom_b, 0 AS ubGLCur_c, 0 AS ubGLYTD_c, 0 AS ubGLJTD_c,0 AS ubGLCustom_c, 0 althrsBud,0 altamtBud_c,0 altamtBud_b, 0.0 poAmt_c,0.00 poAmt_b,
      0.0 rppctcmpcalc, 0.00 targetMult_c,0.00 targetMult_b,0.00 rpfee_c,0.00 rpfee_b,0.00 rpconsultfee_c,0.00 rpconsultfee_b,0.00 rpreimballow_c,0.00 rpreimballow_b,
     '''' AS wbs1customCol1,'''' AS wbs1customCol2,'''' AS wbs1customCol3,'''' AS wbs1customCol4,'''' AS wbs1customCol5,
     '''' AS wbs2customCol1,'''' AS wbs2customCol2,'''' AS wbs2customCol3,'''' AS wbs2customCol4,'''' AS wbs2customCol5,
     '''' AS wbs3customCol1,'''' AS wbs3customCol2,'''' AS wbs3customCol3,'''' AS wbs3customCol4,'''' AS wbs3customCol5,
      '''' As currencyCodeProj,'''' As currencyCodeBill,'''' As currencyCodeFunct,'''' as currencyCodePres_Cost, '''' as currencyCodePres_Bill, '''' as currencyCodePres_ARFee, '''' as currencyCode_Opportunity, 
      0 As currencyCodeProjCount, 0 As currencyCodeBillCount, 0 As currencyCodeFunctCount, 0 As currencyCodePresCount_Cost, 0 As currencyCodePresCount_Bill, 0 As currencyCodePresCount_ARFee, 0 As currencyCodeCount_Opportunity, 
       min(PR.WBS1) WBS1,min(LEVEL2.WBS2) WBS2,min(LEVEL3.WBS3) WBS3,'''' w1Name,'''' w2Name,'''' w3Name,
      '''' ChargeType,max(PR.ChargeType) ChargeTypeCode,min(PR.SubLevel) SubLevel1,min(LEVEL2.SubLevel) SubLevel2,min(LEVEL3.SubLevel) SubLevel3,
      '''' w1principal,'''' w2principal,'''' w3principal,'''' w1prinName,'''' w2prinName,'''' w3prinName,'''' w1projMgr,'''' w2projMgr,'''' w3projMgr,'''' w1prgName,'''' w2prgName,'''' w3prgName,'''' w1supervisor,'''' w2supervisor,'''' w3supervisor,'''' w1supName,'''' w2supName,'''' w3supName,
      '''' PRAddress1,'''' PRAddress2,'''' PRAddress3, '''' PRCity,'''' PRState,'''' PRZip,'''' PRCounty,'''' PRCountry,'''' PRPhone,'''' PRFax,'''' PREmail,
      '''' ClientID,'''' ClientName,'''' CLAddress,'''' CLAddress1,'''' CLAddress2,'''' CLAddress3,'''' CLAddress4,'''' CLCity,'''' CLState,'''' CLZip,'''' CLCountry,'''' CLPhone,'''' CLFax,'''' CLEMail,
      '''' w1Org,'''' w2Org,'''' w3Org,'''' w1OrgName,'''' w2OrgName,'''' w3OrgName,
      '''' as w1TLProjectName, '''' as w2TLProjectName, '''' as w3TLProjectName, '''' ServiceProfileCode,'''' ServiceProfileDesc,
      0 mult,0 mult_sum,0 fee_c,0 fee_b,
      0 consultFee_c,0 consultFee_b,0 reimbAllow_c,0 reimbAllow_b,
      0.0 feeDirectLabor, 0.0 feeDirectExpense, 0.0 reimbAllowExpense, 0.0 reimbAllowConsultant,
      0 contractfee, 0 contractconsultFee, 0 contractreimbAllow,
      0.0 contractFeeDirectLabor, 0.0 contractFeeDirectExpense, 0.0 contractReimbAllowExpense, 0.0 contractReimbAllowConsultant,
      0 BudOHRate,'''' w1Status,'''' w2Status,'''' w3Status,'''' w1StatusDesc,'''' w2StatusDesc,'''' w3StatusDesc,'''' wbs1RevType,'''' wbs2RevType, '''' wbs3RevType,'''' wbs1RevTypeDesc,'''' wbs2RevTypeDesc,'''' wbs3RevTypeDesc,'''' UnitTable,
      NULL w1StartDate,NULL w2StartDate,NULL w3StartDate,NULL w1EndDate,NULL w2EndDate,NULL w3EndDate,
      0.00 w1PctComp,0.00 w2PctComp,0.00 w3PctComp,0.00 w1LabPctComp,0.00 w2LabPctComp,0.00 w3LabPctComp,0.00 w1ExpPctComp,0.00 w2ExpPctComp,0.00 w3ExpPctComp,
      '''' BillByDefault,'''' BillableWarning,'''' BudgetedFlag,'''' BudgetSource,'''' BudgetLevel,'''' BudgetedLevels,
      '''' XCharge,'''' XChargeMethod,0 XChargeMult,'''' ContactID,'''' ContactFirstName,'''' ContactLastName,'''' CLBillingAddr,'''' w1LongName,'''' w2LongName,'''' w3LongName,
      '''' CLBAddress1,'''' CLBAddress2,'''' CLBAddress3,'''' CLBAddress4,'''' CLBCity,'''' CLBState,'''' CLBZip,'''' CLBCountry,'''' CLBPhone,'''' CLBFax,'''' CLBEMail,
      '''' w1FederalInd,'''' w2FederalInd,'''' w3FederalInd,'''' w1ProjectType,'''' w2ProjectType,'''' w3ProjectType,'''' w1projectTypeDesc,'''' w2projectTypeDesc,'''' w3projectTypeDesc,
      '''' w1Responsibility,'''' w2Responsibility,'''' w3Responsibility,'''' w1ResponsibilityDesc,'''' w2ResponsibilityDesc,'''' w3ResponsibilityDesc,'''' w1Referable,'''' w2Referable,'''' w3Referable,
      NULL w1EstCompletionDate,NULL w2EstCompletionDate,NULL w3EstCompletionDate,NULL w1ActCompletionDate,NULL w2ActCompletionDate,NULL w3ActCompletionDate,NULL w1ContractDate,NULL w2ContractDate,NULL w3ContractDate,
      NULL w1ConstComplDate,NULL w2ConstComplDate,NULL w3ConstComplDate,NULL w1ProfServicesComplDate,NULL w2ProfServicesComplDate,NULL w3ProfServicesComplDate,NULL w1BidDate,NULL w2BidDate,NULL w3BidDate,
      '''' w1ComplDateComment,'''' w2ComplDateComment,'''' w3ComplDateComment,0 w1FirmCost,0 w2FirmCost,0 w3FirmCost,'''' w1FirmCostComment,'''' w2FirmCostComment,'''' w3FirmCostComment,
      0 w1TotalProjectCost,0 w2TotalProjectCost,0 w3TotalProjectCost,'''' w1TotalCostComment,'''' w2TotalCostComment,'''' w3TotalCostComment,
      '''' ClientConfidential,'''' ClientAlias,'''' BillingClientID,'''' BillClientName,'''' BillingContactID,'''' billContactLastName,'''' billContactFirstName,'''' ProposalWBS1,
      '''' w1CreateUser,'''' w2CreateUser,'''' w3CreateUser,NULL w1CreateDate,NULL w2CreateDate,NULL w3CreateDate,
      '''' w1ModUser,'''' w2ModUser,'''' w3ModUser,NULL w1ModDate,NULL w2ModDate,NULL w3ModDate,
      '''' w1CostRateMeth,'''' w2CostRateMeth,'''' w3CostRateMeth,'''' w1CostRateTableNo,'''' w3CostRateTableNo,'''' w3CostRateTableNo,
      '''' w1PayRateMeth,'''' w2PayRateMeth,'''' w3PayRateMeth,'''' w1PayRateTableNo,'''' w2PayRateTableNo,'''' w3PayRateTableNo,'''' w1Locale,'''' w1Locale,'''' w1Locale,
      '''' w1LineItemApproval,'''' w2LineItemApproval,'''' w3LineItemApproval,'''' w1LineItemApprovalEK,'''' w2LineItemApprovalEK,'''' w3LineItemApprovalEK,
      '''' opp_name,'''' opp_number,'''' opp_prproposalwbs1,'''' opp_prproposalname,'''' opp_org,'''' opp_orgname,'''' opp_clname,'''' opp_contactname,'''' opp_projmgrname,'''' opp_principalname,'''' opp_supervisorname,
      0 opp_revenue,'''' opp_probability,0 opp_weightedrevenue,NULL opp_opendate,NULL opp_closedate,'''' opp_daysopen,'''' opp_stage,'''' opp_type,'''' opp_source,'''' opp_status,NULL opp_eststartdate,NULL opp_estcompletiondate,
      '''' opp_address1,'''' opp_address2,'''' opp_address3,'''' opp_city,'''' opp_state,'''' opp_zip,'''' opp_country,'''' opp_createuser,NULL opp_createdate,'''' opp_moduser,NULL opp_moddate,'''' opp_oppdesc 
FROM PR As LEVEL3 LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1=ProjectCustomTabFields.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFields.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFields.WBS3 , PR AS LEVEL2 , PR  LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes2 ON substring(PR.Org, 4, 2) = groupCFGOrgCodes2.Code and groupCFGOrgCodes2.orgLevel = 2 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3, ##RWL_' + @SessionId + ' , LedgerAR Left Join CA On LedgerAR.Account = CA.Account 
WHERE ((LedgerAR.Wbs1 = LEVEL3.Wbs1 AND LedgerAR.Wbs2 = LEVEL3.Wbs2 AND LedgerAR.Wbs3 = LEVEL3.Wbs3) 
 and LEVEL3.WBS1 not like ''ZZ%''
 AND (LedgerAR.Wbs1 = LEVEL2.Wbs1 AND LedgerAR.Wbs2 = LEVEL2.wbs2)
 AND (LedgerAR.Wbs1 = PR.Wbs1) 
 AND (PR.Wbs2 = /*N*/'' '' AND PR.Wbs3 = /*N*/'' '' AND LEVEL2.Wbs3 = /*N*/'' '')) AND ProjectCost = ''Y''  AND Period <= @Period AND (Period >= @MinPeriod OR PR.ChargeType <> ''H'')  AND CA.Type >= 5 AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + ''' 
       AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 
       AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/'''' 
GROUP BY IsNull(substring(PR.Org, 4, 2), ''''), IsNull(substring(PR.Org, 7, 3), ''''),  CASE WHEN CA.Type < 7 THEN ''5''  WHEN CA.Type >= 7 AND CA.Type < 9 THEN ''3''  WHEN CA.Type >= 9 AND CA.Type < 10 THEN ''6''  ELSE ''7'' END,  CASE WHEN (CA.Type = 6) OR (CA.Type = 8) THEN ''0'' ELSE ''1'' END

/***Direct and Reimbursable Expenses***/
UNION ALL 
SELECT IsNull(substring(PR.Org, 4, 2), '''') As group1, Min(groupCFGOrgCodes2.Label) As groupDesc1, IsNull(substring(PR.Org, 7, 3), '''') As group2, Min(groupCFGOrgCodes3.Label) As groupDesc2, 
       CASE WHEN CA.Type < 7 THEN ''5''  WHEN CA.Type >= 7 AND CA.Type < 9 THEN ''3''  WHEN CA.Type >= 9 AND CA.Type < 10 THEN ''6''  ELSE ''7'' END As recordType,  min(CASE WHEN (CA.Type = 6) OR (CA.Type = 8) THEN ''0'' ELSE ''1'' END) subType, 
      0 AS hrsCur,0 hrsCurLabReg,0 hrsCurLabOvt,0 AS hrsYTD,0 hrsYTDLabReg,0 hrsYTDLabOvt,0 AS hrsJTD,0 hrsJTDLabReg,0 hrsJTDLabOvt,0 AS hrsCustom,0 hrsCustomLabReg,0 hrsCustomLabOvt,0 AS hrsBud, 
      0 AS hrsPctComp_c,0 as hrsPctComp_b, 0 AS hrsETC, 0 AS hrsEAC, 
      0 amtCurLabReg_c,0 amtCurLabReg_b,0 amtCurLabOvt_c,0 amtCurLabOvt_b,0 amtYTDLabReg_c,0 amtYTDLabReg_b,0 amtYTDLabOvt_c,0 amtYTDLabOvt_b,0 amtJTDLabReg_c,0 amtJTDLabReg_b,0 amtJTDLabOvt_c,0 amtJTDLabOvt_b,0 amtCustomLabReg_c,0 amtCustomLabReg_b,0 amtCustomLabOvt_c,0 amtCustomLabOvt_b,
      sum(CASE WHEN Period = @Period THEN amount ELSE 0 END) AS amtCur_c, sum(CASE WHEN Period = @Period THEN BillExt ELSE 0 END) AS amtCur_b, 
      sum(CASE WHEN Period >= @MinPeriod THEN amount ELSE 0 END) AS amtYTD_c, sum(CASE WHEN Period >= @MinPeriod THEN BillExt ELSE 0 END) AS amtYTD_b, 
      sum(amount) AS amtJTD_c, sum(BillExt) AS amtJTD_b, 
      sum(CASE WHEN Period >= @MinPeriod THEN amount ELSE 0 END) AS amtCustom_c, sum(CASE WHEN Period >= @MinPeriod THEN BillExt ELSE 0 END) AS amtCustom_b, 
      0 AS amtBud_c,0 amtBud_b, 0 AS amtEAC_c,0 amtEAC_b, 0 AS amtETC_c,0 amtETC_b, 
      0 AS amtPctComp_c,0 amtPctComp_b, 0 AS amtPctCompLab_c,0 amtPctCompLab_b, 0 AS amtPctCompExp_c,0 amtPctCompExp_b, 
      0 AS arCur_c,0 AS arCur_b, 0 AS arYTD_c,0 AS arYTD_b, 0 AS arJTD_c,0 AS arJTD_b, 0 AS arCustom_c,0 AS arCustom_b, 0 AS billCur_c,0 AS billCur_b, 0 AS billYTD_c,0 AS billYTD_b, 0 AS billJTD_c,0 AS billJTD_b, 0 AS billCustom_c,0 AS billCustom_b, 0 AS recdCur_c,0 AS recdCur_b, 0 AS recdYTD_c,0 AS recdYTD_b, 0 AS recdJTD_c,0 AS recdJTD_b, 0 AS recdCustom_c,0 AS recdCustom_b, 
      sum(CASE WHEN Period = @Period AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(amount, 0)  ELSE 0 END) AS directCur_c, sum(CASE WHEN Period = @Period AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(BillExt, 0)  ELSE 0 END) AS directCur_b, 
      sum(CASE WHEN Period >= @MinPeriod AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(amount, 0) ELSE 0 END) AS directYTD_c, sum(CASE WHEN Period >= @MinPeriod AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(BillExt, 0) ELSE 0 END) AS directYTD_b, 
      sum(CASE WHEN (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(amount, 0) ELSE 0 END) AS directJTD_c, sum(CASE WHEN (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(BillExt, 0) ELSE 0 END) AS directJTD_b, 
      sum(CASE WHEN Period >= 000000 AND Period <= @Period AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(amount, 0) ELSE 0 END) AS directCustom_c, sum(CASE WHEN Period >= 000000 AND Period <= @Period AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(BillExt, 0) ELSE 0 END) AS directCustom_b, 
      sum(CASE WHEN Period = @Period AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(amount, 0)  ELSE 0 END) AS reimbCur_c, sum(CASE WHEN Period = @Period AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(BillExt, 0)  ELSE 0 END) AS reimbCur_b, 
      sum(CASE WHEN Period >= @MinPeriod AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(amount, 0) ELSE 0 END) AS reimbYTD_c, sum(CASE WHEN Period >= @MinPeriod AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(BillExt, 0) ELSE 0 END) AS reimbYTD_b, 
      sum(CASE WHEN (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(amount, 0) ELSE 0 END) AS reimbJTD_c, sum(CASE WHEN (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(BillExt, 0) ELSE 0 END) AS reimbJTD_b, 
      sum(CASE WHEN Period >= 000000 AND Period <= @Period AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(amount, 0) ELSE 0 END) AS reimbCustom_c, sum(CASE WHEN Period >= 000000 AND Period <= @Period AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(BillExt, 0) ELSE 0 END) AS reimbCustom_b, 
      0 AS revCur_c,0 AS revCur_b, 0 AS revYTD_c,0 AS revYTD_b, 0 AS revJTD_c,0 AS revJTD_b, 0 AS revCustom_c,0 AS revCustom_b, 0 AS ubGLCur_c, 0 AS ubGLYTD_c, 0 AS ubGLJTD_c,0 AS ubGLCustom_c, 0 althrsBud,0 altamtBud_c,0 altamtBud_b, 0.0 poAmt_c,0.00 poAmt_b,
      0.0 rppctcmpcalc, 0.00 targetMult_c,0.00 targetMult_b,0.00 rpfee_c,0.00 rpfee_b,0.00 rpconsultfee_c,0.00 rpconsultfee_b,0.00 rpreimballow_c,0.00 rpreimballow_b,
     '''' AS wbs1customCol1,'''' AS wbs1customCol2,'''' AS wbs1customCol3,'''' AS wbs1customCol4,'''' AS wbs1customCol5,
     '''' AS wbs2customCol1,'''' AS wbs2customCol2,'''' AS wbs2customCol3,'''' AS wbs2customCol4,'''' AS wbs2customCol5,
     '''' AS wbs3customCol1,'''' AS wbs3customCol2,'''' AS wbs3customCol3,'''' AS wbs3customCol4,'''' AS wbs3customCol5,
      '''' As currencyCodeProj,'''' As currencyCodeBill,'''' As currencyCodeFunct,'''' as currencyCodePres_Cost, '''' as currencyCodePres_Bill, '''' as currencyCodePres_ARFee, '''' as currencyCode_Opportunity, 
      0 As currencyCodeProjCount, 0 As currencyCodeBillCount, 0 As currencyCodeFunctCount, 0 As currencyCodePresCount_Cost, 0 As currencyCodePresCount_Bill, 0 As currencyCodePresCount_ARFee, 0 As currencyCodeCount_Opportunity, 
       min(PR.WBS1) WBS1,min(LEVEL2.WBS2) WBS2,min(LEVEL3.WBS3) WBS3,'''' w1Name,'''' w2Name,'''' w3Name,
      '''' ChargeType,max(PR.ChargeType) ChargeTypeCode,min(PR.SubLevel) SubLevel1,min(LEVEL2.SubLevel) SubLevel2,min(LEVEL3.SubLevel) SubLevel3,
      '''' w1principal,'''' w2principal,'''' w3principal,'''' w1prinName,'''' w2prinName,'''' w3prinName,'''' w1projMgr,'''' w2projMgr,'''' w3projMgr,'''' w1prgName,'''' w2prgName,'''' w3prgName,'''' w1supervisor,'''' w2supervisor,'''' w3supervisor,'''' w1supName,'''' w2supName,'''' w3supName,
      '''' PRAddress1,'''' PRAddress2,'''' PRAddress3, '''' PRCity,'''' PRState,'''' PRZip,'''' PRCounty,'''' PRCountry,'''' PRPhone,'''' PRFax,'''' PREmail,
      '''' ClientID,'''' ClientName,'''' CLAddress,'''' CLAddress1,'''' CLAddress2,'''' CLAddress3,'''' CLAddress4,'''' CLCity,'''' CLState,'''' CLZip,'''' CLCountry,'''' CLPhone,'''' CLFax,'''' CLEMail,
      '''' w1Org,'''' w2Org,'''' w3Org,'''' w1OrgName,'''' w2OrgName,'''' w3OrgName,
      '''' as w1TLProjectName, '''' as w2TLProjectName, '''' as w3TLProjectName, '''' ServiceProfileCode,'''' ServiceProfileDesc,
      0 mult,0 mult_sum,0 fee_c,0 fee_b,
      0 consultFee_c,0 consultFee_b,0 reimbAllow_c,0 reimbAllow_b,
      0.0 feeDirectLabor, 0.0 feeDirectExpense, 0.0 reimbAllowExpense, 0.0 reimbAllowConsultant,
      0 contractfee, 0 contractconsultFee, 0 contractreimbAllow,
      0.0 contractFeeDirectLabor, 0.0 contractFeeDirectExpense, 0.0 contractReimbAllowExpense, 0.0 contractReimbAllowConsultant,
      0 BudOHRate,'''' w1Status,'''' w2Status,'''' w3Status,'''' w1StatusDesc,'''' w2StatusDesc,'''' w3StatusDesc,'''' wbs1RevType,'''' wbs2RevType, '''' wbs3RevType,'''' wbs1RevTypeDesc,'''' wbs2RevTypeDesc,'''' wbs3RevTypeDesc,'''' UnitTable,
      NULL w1StartDate,NULL w2StartDate,NULL w3StartDate,NULL w1EndDate,NULL w2EndDate,NULL w3EndDate,
      0.00 w1PctComp,0.00 w2PctComp,0.00 w3PctComp,0.00 w1LabPctComp,0.00 w2LabPctComp,0.00 w3LabPctComp,0.00 w1ExpPctComp,0.00 w2ExpPctComp,0.00 w3ExpPctComp,
      '''' BillByDefault,'''' BillableWarning,'''' BudgetedFlag,'''' BudgetSource,'''' BudgetLevel,'''' BudgetedLevels,
      '''' XCharge,'''' XChargeMethod,0 XChargeMult,'''' ContactID,'''' ContactFirstName,'''' ContactLastName,'''' CLBillingAddr,'''' w1LongName,'''' w2LongName,'''' w3LongName,
      '''' CLBAddress1,'''' CLBAddress2,'''' CLBAddress3,'''' CLBAddress4,'''' CLBCity,'''' CLBState,'''' CLBZip,'''' CLBCountry,'''' CLBPhone,'''' CLBFax,'''' CLBEMail,
      '''' w1FederalInd,'''' w2FederalInd,'''' w3FederalInd,'''' w1ProjectType,'''' w2ProjectType,'''' w3ProjectType,'''' w1projectTypeDesc,'''' w2projectTypeDesc,'''' w3projectTypeDesc,
      '''' w1Responsibility,'''' w2Responsibility,'''' w3Responsibility,'''' w1ResponsibilityDesc,'''' w2ResponsibilityDesc,'''' w3ResponsibilityDesc,'''' w1Referable,'''' w2Referable,'''' w3Referable,
      NULL w1EstCompletionDate,NULL w2EstCompletionDate,NULL w3EstCompletionDate,NULL w1ActCompletionDate,NULL w2ActCompletionDate,NULL w3ActCompletionDate,NULL w1ContractDate,NULL w2ContractDate,NULL w3ContractDate,
      NULL w1ConstComplDate,NULL w2ConstComplDate,NULL w3ConstComplDate,NULL w1ProfServicesComplDate,NULL w2ProfServicesComplDate,NULL w3ProfServicesComplDate,NULL w1BidDate,NULL w2BidDate,NULL w3BidDate,
      '''' w1ComplDateComment,'''' w2ComplDateComment,'''' w3ComplDateComment,0 w1FirmCost,0 w2FirmCost,0 w3FirmCost,'''' w1FirmCostComment,'''' w2FirmCostComment,'''' w3FirmCostComment,
      0 w1TotalProjectCost,0 w2TotalProjectCost,0 w3TotalProjectCost,'''' w1TotalCostComment,'''' w2TotalCostComment,'''' w3TotalCostComment,
      '''' ClientConfidential,'''' ClientAlias,'''' BillingClientID,'''' BillClientName,'''' BillingContactID,'''' billContactLastName,'''' billContactFirstName,'''' ProposalWBS1,
      '''' w1CreateUser,'''' w2CreateUser,'''' w3CreateUser,NULL w1CreateDate,NULL w2CreateDate,NULL w3CreateDate,
      '''' w1ModUser,'''' w2ModUser,'''' w3ModUser,NULL w1ModDate,NULL w2ModDate,NULL w3ModDate,
      '''' w1CostRateMeth,'''' w2CostRateMeth,'''' w3CostRateMeth,'''' w1CostRateTableNo,'''' w3CostRateTableNo,'''' w3CostRateTableNo,
      '''' w1PayRateMeth,'''' w2PayRateMeth,'''' w3PayRateMeth,'''' w1PayRateTableNo,'''' w2PayRateTableNo,'''' w3PayRateTableNo,'''' w1Locale,'''' w1Locale,'''' w1Locale,
      '''' w1LineItemApproval,'''' w2LineItemApproval,'''' w3LineItemApproval,'''' w1LineItemApprovalEK,'''' w2LineItemApprovalEK,'''' w3LineItemApprovalEK,
      '''' opp_name,'''' opp_number,'''' opp_prproposalwbs1,'''' opp_prproposalname,'''' opp_org,'''' opp_orgname,'''' opp_clname,'''' opp_contactname,'''' opp_projmgrname,'''' opp_principalname,'''' opp_supervisorname,
      0 opp_revenue,'''' opp_probability,0 opp_weightedrevenue,NULL opp_opendate,NULL opp_closedate,'''' opp_daysopen,'''' opp_stage,'''' opp_type,'''' opp_source,'''' opp_status,NULL opp_eststartdate,NULL opp_estcompletiondate,
      '''' opp_address1,'''' opp_address2,'''' opp_address3,'''' opp_city,'''' opp_state,'''' opp_zip,'''' opp_country,'''' opp_createuser,NULL opp_createdate,'''' opp_moduser,NULL opp_moddate,'''' opp_oppdesc 
FROM PR As LEVEL3 LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1=ProjectCustomTabFields.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFields.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFields.WBS3 , PR AS LEVEL2 , PR  LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes2 ON substring(PR.Org, 4, 2) = groupCFGOrgCodes2.Code and groupCFGOrgCodes2.orgLevel = 2 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3, ##RWL_' + @SessionId + ' , LedgerEX Left Join CA On LedgerEX.Account = CA.Account 
WHERE ((LedgerEX.Wbs1 = LEVEL3.Wbs1 AND LedgerEX.Wbs2 = LEVEL3.Wbs2 AND LedgerEX.Wbs3 = LEVEL3.Wbs3) 
 and LEVEL3.WBS1 not like ''ZZ%''
 AND (LedgerEX.Wbs1 = LEVEL2.Wbs1 AND LedgerEX.Wbs2 = LEVEL2.wbs2)
 AND (LedgerEX.Wbs1 = PR.Wbs1) 
 AND (PR.Wbs2 = /*N*/'' '' AND PR.Wbs3 = /*N*/'' '' AND LEVEL2.Wbs3 = /*N*/'' '')) AND ProjectCost = ''Y''  AND Period <= @Period AND (Period >= @MinPeriod OR PR.ChargeType <> ''H'')  AND CA.Type >= 5 AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + ''' 
       AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 
       AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/'''' 
GROUP BY IsNull(substring(PR.Org, 4, 2), ''''), IsNull(substring(PR.Org, 7, 3), ''''),  CASE WHEN CA.Type < 7 THEN ''5''  WHEN CA.Type >= 7 AND CA.Type < 9 THEN ''3''  WHEN CA.Type >= 9 AND CA.Type < 10 THEN ''6''  ELSE ''7'' END,  CASE WHEN (CA.Type = 6) OR (CA.Type = 8) THEN ''0'' ELSE ''1'' END

/***Direct and Reimbursable Expenses***/
UNION ALL 
SELECT IsNull(substring(PR.Org, 4, 2), '''') As group1, Min(groupCFGOrgCodes2.Label) As groupDesc1, IsNull(substring(PR.Org, 7, 3), '''') As group2, Min(groupCFGOrgCodes3.Label) As groupDesc2, 
       CASE WHEN CA.Type < 7 THEN ''5''  WHEN CA.Type >= 7 AND CA.Type < 9 THEN ''3''  WHEN CA.Type >= 9 AND CA.Type < 10 THEN ''6''  ELSE ''7'' END As recordType,  min(CASE WHEN (CA.Type = 6) OR (CA.Type = 8) THEN ''0'' ELSE ''1'' END) subType, 
      0 AS hrsCur,0 hrsCurLabReg,0 hrsCurLabOvt,0 AS hrsYTD,0 hrsYTDLabReg,0 hrsYTDLabOvt,0 AS hrsJTD,0 hrsJTDLabReg,0 hrsJTDLabOvt,0 AS hrsCustom,0 hrsCustomLabReg,0 hrsCustomLabOvt,0 AS hrsBud, 
      0 AS hrsPctComp_c,0 as hrsPctComp_b, 0 AS hrsETC, 0 AS hrsEAC, 
      0 amtCurLabReg_c,0 amtCurLabReg_b,0 amtCurLabOvt_c,0 amtCurLabOvt_b,0 amtYTDLabReg_c,0 amtYTDLabReg_b,0 amtYTDLabOvt_c,0 amtYTDLabOvt_b,0 amtJTDLabReg_c,0 amtJTDLabReg_b,0 amtJTDLabOvt_c,0 amtJTDLabOvt_b,0 amtCustomLabReg_c,0 amtCustomLabReg_b,0 amtCustomLabOvt_c,0 amtCustomLabOvt_b,
      sum(CASE WHEN Period = @Period THEN amount ELSE 0 END) AS amtCur_c, sum(CASE WHEN Period = @Period THEN BillExt ELSE 0 END) AS amtCur_b, 
      sum(CASE WHEN Period >= @MinPeriod THEN amount ELSE 0 END) AS amtYTD_c, sum(CASE WHEN Period >= @MinPeriod THEN BillExt ELSE 0 END) AS amtYTD_b, 
      sum(amount) AS amtJTD_c, sum(BillExt) AS amtJTD_b, 
      sum(CASE WHEN Period >= @MinPeriod THEN amount ELSE 0 END) AS amtCustom_c, sum(CASE WHEN Period >= @MinPeriod THEN BillExt ELSE 0 END) AS amtCustom_b, 
      0 AS amtBud_c,0 amtBud_b, 0 AS amtEAC_c,0 amtEAC_b, 0 AS amtETC_c,0 amtETC_b, 
      0 AS amtPctComp_c,0 amtPctComp_b, 0 AS amtPctCompLab_c,0 amtPctCompLab_b, 0 AS amtPctCompExp_c,0 amtPctCompExp_b, 
      0 AS arCur_c,0 AS arCur_b, 0 AS arYTD_c,0 AS arYTD_b, 0 AS arJTD_c,0 AS arJTD_b, 0 AS arCustom_c,0 AS arCustom_b, 0 AS billCur_c,0 AS billCur_b, 0 AS billYTD_c,0 AS billYTD_b, 0 AS billJTD_c,0 AS billJTD_b, 0 AS billCustom_c,0 AS billCustom_b, 0 AS recdCur_c,0 AS recdCur_b, 0 AS recdYTD_c,0 AS recdYTD_b, 0 AS recdJTD_c,0 AS recdJTD_b, 0 AS recdCustom_c,0 AS recdCustom_b, 
      sum(CASE WHEN Period = @Period AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(amount, 0)  ELSE 0 END) AS directCur_c, sum(CASE WHEN Period = @Period AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(BillExt, 0)  ELSE 0 END) AS directCur_b, 
      sum(CASE WHEN Period >= @MinPeriod AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(amount, 0) ELSE 0 END) AS directYTD_c, sum(CASE WHEN Period >= @MinPeriod AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(BillExt, 0) ELSE 0 END) AS directYTD_b, 
      sum(CASE WHEN (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(amount, 0) ELSE 0 END) AS directJTD_c, sum(CASE WHEN (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(BillExt, 0) ELSE 0 END) AS directJTD_b, 
      sum(CASE WHEN Period >= 000000 AND Period <= @Period AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(amount, 0) ELSE 0 END) AS directCustom_c, sum(CASE WHEN Period >= 000000 AND Period <= @Period AND (CA.Type >= 7 AND CA.Type < 9) THEN IsNull(BillExt, 0) ELSE 0 END) AS directCustom_b, 
      sum(CASE WHEN Period = @Period AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(amount, 0)  ELSE 0 END) AS reimbCur_c, sum(CASE WHEN Period = @Period AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(BillExt, 0)  ELSE 0 END) AS reimbCur_b, 
      sum(CASE WHEN Period >= @MinPeriod AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(amount, 0) ELSE 0 END) AS reimbYTD_c, sum(CASE WHEN Period >= @MinPeriod AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(BillExt, 0) ELSE 0 END) AS reimbYTD_b, 
      sum(CASE WHEN (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(amount, 0) ELSE 0 END) AS reimbJTD_c, sum(CASE WHEN (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(BillExt, 0) ELSE 0 END) AS reimbJTD_b, 
      sum(CASE WHEN Period >= 000000 AND Period <= @Period AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(amount, 0) ELSE 0 END) AS reimbCustom_c, sum(CASE WHEN Period >= 000000 AND Period <= @Period AND (CA.Type >= 5 AND CA.Type < 7) THEN IsNull(BillExt, 0) ELSE 0 END) AS reimbCustom_b, 
      0 AS revCur_c,0 AS revCur_b, 0 AS revYTD_c,0 AS revYTD_b, 0 AS revJTD_c,0 AS revJTD_b, 0 AS revCustom_c,0 AS revCustom_b, 0 AS ubGLCur_c, 0 AS ubGLYTD_c, 0 AS ubGLJTD_c,0 AS ubGLCustom_c, 0 althrsBud,0 altamtBud_c,0 altamtBud_b, 0.0 poAmt_c,0.00 poAmt_b,
      0.0 rppctcmpcalc, 0.00 targetMult_c,0.00 targetMult_b,0.00 rpfee_c,0.00 rpfee_b,0.00 rpconsultfee_c,0.00 rpconsultfee_b,0.00 rpreimballow_c,0.00 rpreimballow_b,
     '''' AS wbs1customCol1,'''' AS wbs1customCol2,'''' AS wbs1customCol3,'''' AS wbs1customCol4,'''' AS wbs1customCol5,
     '''' AS wbs2customCol1,'''' AS wbs2customCol2,'''' AS wbs2customCol3,'''' AS wbs2customCol4,'''' AS wbs2customCol5,
     '''' AS wbs3customCol1,'''' AS wbs3customCol2,'''' AS wbs3customCol3,'''' AS wbs3customCol4,'''' AS wbs3customCol5,
      '''' As currencyCodeProj,'''' As currencyCodeBill,'''' As currencyCodeFunct,'''' as currencyCodePres_Cost, '''' as currencyCodePres_Bill, '''' as currencyCodePres_ARFee, '''' as currencyCode_Opportunity, 
      0 As currencyCodeProjCount, 0 As currencyCodeBillCount, 0 As currencyCodeFunctCount, 0 As currencyCodePresCount_Cost, 0 As currencyCodePresCount_Bill, 0 As currencyCodePresCount_ARFee, 0 As currencyCodeCount_Opportunity, 
       min(PR.WBS1) WBS1,min(LEVEL2.WBS2) WBS2,min(LEVEL3.WBS3) WBS3,'''' w1Name,'''' w2Name,'''' w3Name,
      '''' ChargeType,max(PR.ChargeType) ChargeTypeCode,min(PR.SubLevel) SubLevel1,min(LEVEL2.SubLevel) SubLevel2,min(LEVEL3.SubLevel) SubLevel3,
      '''' w1principal,'''' w2principal,'''' w3principal,'''' w1prinName,'''' w2prinName,'''' w3prinName,'''' w1projMgr,'''' w2projMgr,'''' w3projMgr,'''' w1prgName,'''' w2prgName,'''' w3prgName,'''' w1supervisor,'''' w2supervisor,'''' w3supervisor,'''' w1supName,'''' w2supName,'''' w3supName,
      '''' PRAddress1,'''' PRAddress2,'''' PRAddress3, '''' PRCity,'''' PRState,'''' PRZip,'''' PRCounty,'''' PRCountry,'''' PRPhone,'''' PRFax,'''' PREmail,
      '''' ClientID,'''' ClientName,'''' CLAddress,'''' CLAddress1,'''' CLAddress2,'''' CLAddress3,'''' CLAddress4,'''' CLCity,'''' CLState,'''' CLZip,'''' CLCountry,'''' CLPhone,'''' CLFax,'''' CLEMail,
      '''' w1Org,'''' w2Org,'''' w3Org,'''' w1OrgName,'''' w2OrgName,'''' w3OrgName,
      '''' as w1TLProjectName, '''' as w2TLProjectName, '''' as w3TLProjectName, '''' ServiceProfileCode,'''' ServiceProfileDesc,
      0 mult,0 mult_sum,0 fee_c,0 fee_b,
      0 consultFee_c,0 consultFee_b,0 reimbAllow_c,0 reimbAllow_b,
      0.0 feeDirectLabor, 0.0 feeDirectExpense, 0.0 reimbAllowExpense, 0.0 reimbAllowConsultant,
      0 contractfee, 0 contractconsultFee, 0 contractreimbAllow,
      0.0 contractFeeDirectLabor, 0.0 contractFeeDirectExpense, 0.0 contractReimbAllowExpense, 0.0 contractReimbAllowConsultant,
      0 BudOHRate,'''' w1Status,'''' w2Status,'''' w3Status,'''' w1StatusDesc,'''' w2StatusDesc,'''' w3StatusDesc,'''' wbs1RevType,'''' wbs2RevType, '''' wbs3RevType,'''' wbs1RevTypeDesc,'''' wbs2RevTypeDesc,'''' wbs3RevTypeDesc,'''' UnitTable,
      NULL w1StartDate,NULL w2StartDate,NULL w3StartDate,NULL w1EndDate,NULL w2EndDate,NULL w3EndDate,
      0.00 w1PctComp,0.00 w2PctComp,0.00 w3PctComp,0.00 w1LabPctComp,0.00 w2LabPctComp,0.00 w3LabPctComp,0.00 w1ExpPctComp,0.00 w2ExpPctComp,0.00 w3ExpPctComp,
      '''' BillByDefault,'''' BillableWarning,'''' BudgetedFlag,'''' BudgetSource,'''' BudgetLevel,'''' BudgetedLevels,
      '''' XCharge,'''' XChargeMethod,0 XChargeMult,'''' ContactID,'''' ContactFirstName,'''' ContactLastName,'''' CLBillingAddr,'''' w1LongName,'''' w2LongName,'''' w3LongName,
      '''' CLBAddress1,'''' CLBAddress2,'''' CLBAddress3,'''' CLBAddress4,'''' CLBCity,'''' CLBState,'''' CLBZip,'''' CLBCountry,'''' CLBPhone,'''' CLBFax,'''' CLBEMail,
      '''' w1FederalInd,'''' w2FederalInd,'''' w3FederalInd,'''' w1ProjectType,'''' w2ProjectType,'''' w3ProjectType,'''' w1projectTypeDesc,'''' w2projectTypeDesc,'''' w3projectTypeDesc,
      '''' w1Responsibility,'''' w2Responsibility,'''' w3Responsibility,'''' w1ResponsibilityDesc,'''' w2ResponsibilityDesc,'''' w3ResponsibilityDesc,'''' w1Referable,'''' w2Referable,'''' w3Referable,
      NULL w1EstCompletionDate,NULL w2EstCompletionDate,NULL w3EstCompletionDate,NULL w1ActCompletionDate,NULL w2ActCompletionDate,NULL w3ActCompletionDate,NULL w1ContractDate,NULL w2ContractDate,NULL w3ContractDate,
      NULL w1ConstComplDate,NULL w2ConstComplDate,NULL w3ConstComplDate,NULL w1ProfServicesComplDate,NULL w2ProfServicesComplDate,NULL w3ProfServicesComplDate,NULL w1BidDate,NULL w2BidDate,NULL w3BidDate,
      '''' w1ComplDateComment,'''' w2ComplDateComment,'''' w3ComplDateComment,0 w1FirmCost,0 w2FirmCost,0 w3FirmCost,'''' w1FirmCostComment,'''' w2FirmCostComment,'''' w3FirmCostComment,
      0 w1TotalProjectCost,0 w2TotalProjectCost,0 w3TotalProjectCost,'''' w1TotalCostComment,'''' w2TotalCostComment,'''' w3TotalCostComment,
      '''' ClientConfidential,'''' ClientAlias,'''' BillingClientID,'''' BillClientName,'''' BillingContactID,'''' billContactLastName,'''' billContactFirstName,'''' ProposalWBS1,
      '''' w1CreateUser,'''' w2CreateUser,'''' w3CreateUser,NULL w1CreateDate,NULL w2CreateDate,NULL w3CreateDate,
      '''' w1ModUser,'''' w2ModUser,'''' w3ModUser,NULL w1ModDate,NULL w2ModDate,NULL w3ModDate,
      '''' w1CostRateMeth,'''' w2CostRateMeth,'''' w3CostRateMeth,'''' w1CostRateTableNo,'''' w3CostRateTableNo,'''' w3CostRateTableNo,
      '''' w1PayRateMeth,'''' w2PayRateMeth,'''' w3PayRateMeth,'''' w1PayRateTableNo,'''' w2PayRateTableNo,'''' w3PayRateTableNo,'''' w1Locale,'''' w1Locale,'''' w1Locale,
      '''' w1LineItemApproval,'''' w2LineItemApproval,'''' w3LineItemApproval,'''' w1LineItemApprovalEK,'''' w2LineItemApprovalEK,'''' w3LineItemApprovalEK,
      '''' opp_name,'''' opp_number,'''' opp_prproposalwbs1,'''' opp_prproposalname,'''' opp_org,'''' opp_orgname,'''' opp_clname,'''' opp_contactname,'''' opp_projmgrname,'''' opp_principalname,'''' opp_supervisorname,
      0 opp_revenue,'''' opp_probability,0 opp_weightedrevenue,NULL opp_opendate,NULL opp_closedate,'''' opp_daysopen,'''' opp_stage,'''' opp_type,'''' opp_source,'''' opp_status,NULL opp_eststartdate,NULL opp_estcompletiondate,
      '''' opp_address1,'''' opp_address2,'''' opp_address3,'''' opp_city,'''' opp_state,'''' opp_zip,'''' opp_country,'''' opp_createuser,NULL opp_createdate,'''' opp_moduser,NULL opp_moddate,'''' opp_oppdesc 
FROM PR As LEVEL3 LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1=ProjectCustomTabFields.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFields.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFields.WBS3 , PR AS LEVEL2 , PR  LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes2 ON substring(PR.Org, 4, 2) = groupCFGOrgCodes2.Code and groupCFGOrgCodes2.orgLevel = 2 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3, ##RWL_' + @SessionId + ' , LedgerMisc Left Join CA On LedgerMisc.Account = CA.Account 
WHERE ((LedgerMisc.Wbs1 = LEVEL3.Wbs1 AND LedgerMisc.Wbs2 = LEVEL3.Wbs2 AND LedgerMisc.Wbs3 = LEVEL3.Wbs3) 
 and LEVEL3.WBS1 not like ''ZZ%''
 AND (LedgerMisc.Wbs1 = LEVEL2.Wbs1 AND LedgerMisc.Wbs2 = LEVEL2.wbs2)
 AND (LedgerMisc.Wbs1 = PR.Wbs1) 
 AND (PR.Wbs2 = /*N*/'' '' AND PR.Wbs3 = /*N*/'' '' AND LEVEL2.Wbs3 = /*N*/'' '')) AND ProjectCost = ''Y''  AND Period <= @Period AND (Period >= @MinPeriod OR PR.ChargeType <> ''H'')  AND CA.Type >= 5 AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + ''' 
       AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 
       AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/'''' 
GROUP BY IsNull(substring(PR.Org, 4, 2), ''''), IsNull(substring(PR.Org, 7, 3), ''''),  CASE WHEN CA.Type < 7 THEN ''5''  WHEN CA.Type >= 7 AND CA.Type < 9 THEN ''3''  WHEN CA.Type >= 9 AND CA.Type < 10 THEN ''6''  ELSE ''7'' END,  CASE WHEN (CA.Type = 6) OR (CA.Type = 8) THEN ''0'' ELSE ''1'' END

/***LedgerAR: Billed and Received***/
UNION ALL 
SELECT IsNull(substring(PR.Org, 4, 2), '''') As group1, Min(groupCFGOrgCodes2.Label) As groupDesc1, IsNull(substring(PR.Org, 7, 3), '''') As group2, Min(groupCFGOrgCodes3.Label) As groupDesc2, ''9'' AS recordType,'''' subType, 
      0 AS hrsCur,0 hrsCurLabReg,0 hrsCurLabOvt,0 AS hrsYTD,0 hrsYTDLabReg,0 hrsYTDLabOvt,0 AS hrsJTD,0 hrsJTDLabReg,0 hrsJTDLabOvt,0 AS hrsCustom,0 hrsCustomLabReg,0 hrsCustomLabOvt,0 AS hrsBud, 
      0 AS hrsPctComp_c,0 as hrsPctComp_b, 0 AS hrsETC, 0 AS hrsEAC, 
      0 amtCurLabReg_c,0 amtCurLabReg_b,0 amtCurLabOvt_c,0 amtCurLabOvt_b,0 amtYTDLabReg_c,0 amtYTDLabReg_b,0 amtYTDLabOvt_c,0 amtYTDLabOvt_b,0 amtJTDLabReg_c,0 amtJTDLabReg_b,0 amtJTDLabOvt_c,0 amtJTDLabOvt_b,0 amtCustomLabReg_c,0 amtCustomLabReg_b,0 amtCustomLabOvt_c,0 amtCustomLabOvt_b,
      0 AS amtCur_c,0 as amtCur_b, 0 AS amtYTD_c,0 amtYTD_b, 0 AS amtJTD_c,0 amtJTD_b, 0 AS amtCustom_c,0 amtCustom_b, 
      0 AS amtBud_c,0 amtBud_b, 0 AS amtEAC_c,0 amtEAC_b, 0 AS amtETC_c,0 amtETC_b, 
      0 AS amtPctComp_c,0 amtPctComp_b, 0 AS amtPctCompLab_c,0 amtPctCompLab_b, 0 AS amtPctCompExp_c,0 amtPctCompExp_b, 
      0 AS arCur_c,0 AS arCur_b, 0 AS arYTD_c,0 AS arYTD_b, 0 AS arJTD_c,0 AS arJTD_b, 0 AS arCustom_c,0 AS arCustom_b, 
      sum(CASE WHEN TransType = /*N*/''IN'' AND Period = @Period THEN -amount ELSE 0 END) AS billCur_c, sum(CASE WHEN TransType = /*N*/''IN'' AND Period = @Period THEN -amount ELSE 0 END) AS billCur_b, 
      sum(CASE WHEN TransType = /*N*/''IN'' AND Period >= @MinPeriod THEN -amount ELSE 0 END) AS billYTD_c, sum(CASE WHEN TransType = /*N*/''IN'' AND Period >= @MinPeriod THEN -amount ELSE 0 END) AS billYTD_b, 
      sum(CASE WHEN TransType = /*N*/''IN'' THEN -amount ELSE 0 END) AS billJTD_c, sum(CASE WHEN TransType = /*N*/''IN'' THEN -amount ELSE 0 END) AS billJTD_b, 
      sum(CASE WHEN TransType = /*N*/''IN'' AND Period >= 000000 AND Period <= @Period THEN -amount ELSE 0 END) AS billCustom_c, sum(CASE WHEN TransType = /*N*/''IN'' AND Period >= 000000 AND Period <= @Period THEN -amount ELSE 0 END) AS billCustom_b, 
      sum(CASE WHEN TransType = /*N*/''CR'' AND Period = @Period THEN -amount ELSE 0 END) AS recdCur_c, sum(CASE WHEN TransType = /*N*/''CR'' AND Period = @Period THEN -amount ELSE 0 END) AS recdCur_b, 
      sum(CASE WHEN TransType = /*N*/''CR'' AND Period >= @MinPeriod THEN -amount ELSE 0 END) AS recdYTD_c, sum(CASE WHEN TransType = /*N*/''CR'' AND Period >= @MinPeriod THEN -amount ELSE 0 END) AS recdYTD_b, 
      sum(CASE WHEN TransType = /*N*/''CR'' THEN -amount ELSE 0 END) AS recdJTD_c, sum(CASE WHEN TransType = /*N*/''CR'' THEN -amount ELSE 0 END) AS recdJTD_b, 
      sum(CASE WHEN TransType = /*N*/''CR'' AND Period >= 000000 AND Period <= @Period THEN -amount ELSE 0 END) AS recdCustom_c, sum(CASE WHEN TransType = /*N*/''CR'' AND Period >= 000000 AND Period <= @Period THEN -amount ELSE 0 END) AS recdCustom_b, 
      0 AS directCur_c,0 directCur_b, 0 AS directYTD_c,0 directYTD_b, 0 AS directJTD_c,0 directJTD_b, 0 AS directCustom_c,0 directCustom_b, 0 AS reimbCur_c,0 reimbCur_b, 0 AS reimbYTD_c,0 reimbYTD_b, 0 AS reimbJTD_c,0 reimbJTD_b, 0 AS reimbCustom_c,0 reimbCustom_b, 0 AS revCur_c,0 AS revCur_b, 0 AS revYTD_c,0 AS revYTD_b, 0 AS revJTD_c,0 AS revJTD_b, 0 AS revCustom_c,0 AS revCustom_b, 0 AS ubGLCur_c, 0 AS ubGLYTD_c, 0 AS ubGLJTD_c,0 AS ubGLCustom_c, 0 althrsBud,0 altamtBud_c,0 altamtBud_b, 0.0 poAmt_c,0.00 poAmt_b,
      0.0 rppctcmpcalc, 0.00 targetMult_c,0.00 targetMult_b,0.00 rpfee_c,0.00 rpfee_b,0.00 rpconsultfee_c,0.00 rpconsultfee_b,0.00 rpreimballow_c,0.00 rpreimballow_b,
     '''' AS wbs1customCol1,'''' AS wbs1customCol2,'''' AS wbs1customCol3,'''' AS wbs1customCol4,'''' AS wbs1customCol5,
     '''' AS wbs2customCol1,'''' AS wbs2customCol2,'''' AS wbs2customCol3,'''' AS wbs2customCol4,'''' AS wbs2customCol5,
     '''' AS wbs3customCol1,'''' AS wbs3customCol2,'''' AS wbs3customCol3,'''' AS wbs3customCol4,'''' AS wbs3customCol5,
      '''' As currencyCodeProj,'''' As currencyCodeBill,'''' As currencyCodeFunct,'''' as currencyCodePres_Cost, '''' as currencyCodePres_Bill, '''' as currencyCodePres_ARFee, '''' as currencyCode_Opportunity, 
      0 As currencyCodeProjCount, 0 As currencyCodeBillCount, 0 As currencyCodeFunctCount, 0 As currencyCodePresCount_Cost, 0 As currencyCodePresCount_Bill, 0 As currencyCodePresCount_ARFee, 0 As currencyCodeCount_Opportunity, 
       min(PR.WBS1) WBS1,min(LEVEL2.WBS2) WBS2,min(LEVEL3.WBS3) WBS3,'''' w1Name,'''' w2Name,'''' w3Name,
      '''' ChargeType,max(PR.ChargeType) ChargeTypeCode,min(PR.SubLevel) SubLevel1,min(LEVEL2.SubLevel) SubLevel2,min(LEVEL3.SubLevel) SubLevel3,
      '''' w1principal,'''' w2principal,'''' w3principal,'''' w1prinName,'''' w2prinName,'''' w3prinName,'''' w1projMgr,'''' w2projMgr,'''' w3projMgr,'''' w1prgName,'''' w2prgName,'''' w3prgName,'''' w1supervisor,'''' w2supervisor,'''' w3supervisor,'''' w1supName,'''' w2supName,'''' w3supName,
      '''' PRAddress1,'''' PRAddress2,'''' PRAddress3, '''' PRCity,'''' PRState,'''' PRZip,'''' PRCounty,'''' PRCountry,'''' PRPhone,'''' PRFax,'''' PREmail,
      '''' ClientID,'''' ClientName,'''' CLAddress,'''' CLAddress1,'''' CLAddress2,'''' CLAddress3,'''' CLAddress4,'''' CLCity,'''' CLState,'''' CLZip,'''' CLCountry,'''' CLPhone,'''' CLFax,'''' CLEMail,
      '''' w1Org,'''' w2Org,'''' w3Org,'''' w1OrgName,'''' w2OrgName,'''' w3OrgName,
      '''' as w1TLProjectName, '''' as w2TLProjectName, '''' as w3TLProjectName, '''' ServiceProfileCode,'''' ServiceProfileDesc,
      0 mult,0 mult_sum,0 fee_c,0 fee_b,
      0 consultFee_c,0 consultFee_b,0 reimbAllow_c,0 reimbAllow_b,
      0.0 feeDirectLabor, 0.0 feeDirectExpense, 0.0 reimbAllowExpense, 0.0 reimbAllowConsultant,
      0 contractfee, 0 contractconsultFee, 0 contractreimbAllow,
      0.0 contractFeeDirectLabor, 0.0 contractFeeDirectExpense, 0.0 contractReimbAllowExpense, 0.0 contractReimbAllowConsultant,
      0 BudOHRate,'''' w1Status,'''' w2Status,'''' w3Status,'''' w1StatusDesc,'''' w2StatusDesc,'''' w3StatusDesc,'''' wbs1RevType,'''' wbs2RevType, '''' wbs3RevType,'''' wbs1RevTypeDesc,'''' wbs2RevTypeDesc,'''' wbs3RevTypeDesc,'''' UnitTable,
      NULL w1StartDate,NULL w2StartDate,NULL w3StartDate,NULL w1EndDate,NULL w2EndDate,NULL w3EndDate,
      0.00 w1PctComp,0.00 w2PctComp,0.00 w3PctComp,0.00 w1LabPctComp,0.00 w2LabPctComp,0.00 w3LabPctComp,0.00 w1ExpPctComp,0.00 w2ExpPctComp,0.00 w3ExpPctComp,
      '''' BillByDefault,'''' BillableWarning,'''' BudgetedFlag,'''' BudgetSource,'''' BudgetLevel,'''' BudgetedLevels,
      '''' XCharge,'''' XChargeMethod,0 XChargeMult,'''' ContactID,'''' ContactFirstName,'''' ContactLastName,'''' CLBillingAddr,'''' w1LongName,'''' w2LongName,'''' w3LongName,
      '''' CLBAddress1,'''' CLBAddress2,'''' CLBAddress3,'''' CLBAddress4,'''' CLBCity,'''' CLBState,'''' CLBZip,'''' CLBCountry,'''' CLBPhone,'''' CLBFax,'''' CLBEMail,
      '''' w1FederalInd,'''' w2FederalInd,'''' w3FederalInd,'''' w1ProjectType,'''' w2ProjectType,'''' w3ProjectType,'''' w1projectTypeDesc,'''' w2projectTypeDesc,'''' w3projectTypeDesc,
      '''' w1Responsibility,'''' w2Responsibility,'''' w3Responsibility,'''' w1ResponsibilityDesc,'''' w2ResponsibilityDesc,'''' w3ResponsibilityDesc,'''' w1Referable,'''' w2Referable,'''' w3Referable,
      NULL w1EstCompletionDate,NULL w2EstCompletionDate,NULL w3EstCompletionDate,NULL w1ActCompletionDate,NULL w2ActCompletionDate,NULL w3ActCompletionDate,NULL w1ContractDate,NULL w2ContractDate,NULL w3ContractDate,
      NULL w1ConstComplDate,NULL w2ConstComplDate,NULL w3ConstComplDate,NULL w1ProfServicesComplDate,NULL w2ProfServicesComplDate,NULL w3ProfServicesComplDate,NULL w1BidDate,NULL w2BidDate,NULL w3BidDate,
      '''' w1ComplDateComment,'''' w2ComplDateComment,'''' w3ComplDateComment,0 w1FirmCost,0 w2FirmCost,0 w3FirmCost,'''' w1FirmCostComment,'''' w2FirmCostComment,'''' w3FirmCostComment,
      0 w1TotalProjectCost,0 w2TotalProjectCost,0 w3TotalProjectCost,'''' w1TotalCostComment,'''' w2TotalCostComment,'''' w3TotalCostComment,
      '''' ClientConfidential,'''' ClientAlias,'''' BillingClientID,'''' BillClientName,'''' BillingContactID,'''' billContactLastName,'''' billContactFirstName,'''' ProposalWBS1,
      '''' w1CreateUser,'''' w2CreateUser,'''' w3CreateUser,NULL w1CreateDate,NULL w2CreateDate,NULL w3CreateDate,
      '''' w1ModUser,'''' w2ModUser,'''' w3ModUser,NULL w1ModDate,NULL w2ModDate,NULL w3ModDate,
      '''' w1CostRateMeth,'''' w2CostRateMeth,'''' w3CostRateMeth,'''' w1CostRateTableNo,'''' w3CostRateTableNo,'''' w3CostRateTableNo,
      '''' w1PayRateMeth,'''' w2PayRateMeth,'''' w3PayRateMeth,'''' w1PayRateTableNo,'''' w2PayRateTableNo,'''' w3PayRateTableNo,'''' w1Locale,'''' w1Locale,'''' w1Locale,
      '''' w1LineItemApproval,'''' w2LineItemApproval,'''' w3LineItemApproval,'''' w1LineItemApprovalEK,'''' w2LineItemApprovalEK,'''' w3LineItemApprovalEK,
      '''' opp_name,'''' opp_number,'''' opp_prproposalwbs1,'''' opp_prproposalname,'''' opp_org,'''' opp_orgname,'''' opp_clname,'''' opp_contactname,'''' opp_projmgrname,'''' opp_principalname,'''' opp_supervisorname,
      0 opp_revenue,'''' opp_probability,0 opp_weightedrevenue,NULL opp_opendate,NULL opp_closedate,'''' opp_daysopen,'''' opp_stage,'''' opp_type,'''' opp_source,'''' opp_status,NULL opp_eststartdate,NULL opp_estcompletiondate,
      '''' opp_address1,'''' opp_address2,'''' opp_address3,'''' opp_city,'''' opp_state,'''' opp_zip,'''' opp_country,'''' opp_createuser,NULL opp_createdate,'''' opp_moduser,NULL opp_moddate,'''' opp_oppdesc 
FROM PR As LEVEL3 LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1=ProjectCustomTabFields.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFields.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFields.WBS3 , PR AS LEVEL2 , PR  LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes2 ON substring(PR.Org, 4, 2) = groupCFGOrgCodes2.Code and groupCFGOrgCodes2.orgLevel = 2 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3, ##RWL_' + @SessionId + ' , LedgerAR Left Join CA ON LedgerAR.Account = CA.Account 
WHERE ((LedgerAR.Wbs1 = LEVEL3.Wbs1 AND LedgerAR.Wbs2 = LEVEL3.Wbs2 AND LedgerAR.Wbs3 = LEVEL3.Wbs3) 
 and LEVEL3.WBS1 not like ''ZZ%''
 AND (LedgerAR.Wbs1 = LEVEL2.Wbs1 AND LedgerAR.Wbs2 = LEVEL2.wbs2)
 AND (LedgerAR.Wbs1 = PR.Wbs1) 
 AND (PR.Wbs2 = /*N*/'' '' AND PR.Wbs3 = /*N*/'' '' AND LEVEL2.Wbs3 = /*N*/'' '')) AND Period <= @Period AND AutoEntry = ''N''
      AND ( (TransType = /*N*/''IN''  AND IsNull(SubType, '' '') <> ''I'' AND IsNull(SubType, '' '') <> ''R''  AND (CA.Type >= 4 OR LedgerAR.Account IS NULL)  AND (NOT (TransType = /*N*/''IN'' AND LinkWBS1 IS NOT NULL))       )
       OR  (TransType = /*N*/''CR'' AND (SubType IN (''R'', ''X'') OR SubType = ''T'' AND IsNull(Invoice, /*N*/'' '') = /*N*/'' '')) ) 
       AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + ''' 
       AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 
       AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/'''' 
GROUP BY IsNull(substring(PR.Org, 4, 2), ''''), IsNull(substring(PR.Org, 7, 3), '''')

/***LedgerAR: AR, Revenue Columns***/
UNION ALL 
SELECT IsNull(substring(PR.Org, 4, 2), '''') As group1, Min(groupCFGOrgCodes2.Label) As groupDesc1, IsNull(substring(PR.Org, 7, 3), '''') As group2, Min(groupCFGOrgCodes3.Label) As groupDesc2, ''8'' AS recordType,'''' subType, 
      0 AS hrsCur,0 hrsCurLabReg,0 hrsCurLabOvt,0 AS hrsYTD,0 hrsYTDLabReg,0 hrsYTDLabOvt,0 AS hrsJTD,0 hrsJTDLabReg,0 hrsJTDLabOvt,0 AS hrsCustom,0 hrsCustomLabReg,0 hrsCustomLabOvt,0 AS hrsBud, 
      0 AS hrsPctComp_c,0 as hrsPctComp_b, 0 AS hrsETC, 0 AS hrsEAC, 
      0 amtCurLabReg_c,0 amtCurLabReg_b,0 amtCurLabOvt_c,0 amtCurLabOvt_b,0 amtYTDLabReg_c,0 amtYTDLabReg_b,0 amtYTDLabOvt_c,0 amtYTDLabOvt_b,0 amtJTDLabReg_c,0 amtJTDLabReg_b,0 amtJTDLabOvt_c,0 amtJTDLabOvt_b,0 amtCustomLabReg_c,0 amtCustomLabReg_b,0 amtCustomLabOvt_c,0 amtCustomLabOvt_b,
      0 AS amtCur_c,0 amtCur_b, 0 AS amtYTD_c,0 amtYTD_b, 0 AS amtJTD_c,0 amtJTD_b, 0 AS amtCustom_c,0 amtCustom_b, 
      0 AS amtBud_c,0 amtBud_b, 0 AS amtEAC_c,0 amtEAC_b, 0 AS amtETC_c,0 amtETC_b, 
      0 AS amtPctComp_c,0 amtPctComp_b, 0 AS amtPctCompLab_c,0 amtPctCompLab_b, 0 AS amtPctCompExp_c,0 amtPctCompExp_b, 
      sum(CASE WHEN Period = @Period THEN CASE WHEN TransType = /*N*/''IN'' AND AutoEntry = ''N'' AND IsNull(SubType, '' '') <> ''X'' THEN -amount WHEN TransType = /*N*/''CR'' AND SubType = ''T'' AND IsNull(Invoice, /*N*/'' '') <> /*N*/'' '' THEN -amount WHEN TransType = /*N*/''CR'' AND SubType = ''R'' THEN amount ELSE 0 END ELSE 0 END) AS arCur_c, sum(CASE WHEN Period = @Period THEN CASE WHEN TransType = /*N*/''IN'' AND AutoEntry = ''N'' AND IsNull(SubType, '' '') <> ''X'' THEN -amount WHEN TransType = /*N*/''CR'' AND SubType = ''T'' AND IsNull(Invoice, /*N*/'' '') <> /*N*/'' '' THEN -amount WHEN TransType = /*N*/''CR'' AND SubType = ''R'' THEN amount ELSE 0 END ELSE 0 END) AS arCur_b, 
      sum(CASE WHEN Period >= @MinPeriod THEN CASE WHEN TransType = /*N*/''IN'' AND AutoEntry = ''N'' AND IsNull(SubType, '' '') <> ''X'' THEN -amount WHEN TransType = /*N*/''CR'' AND SubType = ''T'' AND IsNull(Invoice, /*N*/'' '') <> /*N*/'' '' THEN -amount WHEN TransType = /*N*/''CR'' AND SubType = ''R'' THEN amount ELSE 0 END ELSE 0 END) AS arYTD_c, sum(CASE WHEN Period >= @MinPeriod THEN CASE WHEN TransType = /*N*/''IN'' AND AutoEntry = ''N'' AND IsNull(SubType, '' '') <> ''X'' THEN -amount WHEN TransType = /*N*/''CR'' AND SubType = ''T'' AND IsNull(Invoice, /*N*/'' '') <> /*N*/'' '' THEN -amount WHEN TransType = /*N*/''CR'' AND SubType = ''R'' THEN amount ELSE 0 END ELSE 0 END) AS arYTD_b, 
      sum(CASE WHEN TransType = /*N*/''IN'' AND AutoEntry = ''N'' AND IsNull(SubType, '' '') <> ''X'' THEN -amount WHEN TransType = /*N*/''CR'' AND SubType = ''T'' AND IsNull(Invoice, /*N*/'' '') <> /*N*/'' '' THEN -amount WHEN TransType = /*N*/''CR'' AND SubType = ''R'' THEN amount ELSE 0 END) AS arJTD_c, sum(CASE WHEN TransType = /*N*/''IN'' AND AutoEntry = ''N'' AND IsNull(SubType, '' '') <> ''X'' THEN -amount WHEN TransType = /*N*/''CR'' AND SubType = ''T'' AND IsNull(Invoice, /*N*/'' '') <> /*N*/'' '' THEN -amount WHEN TransType = /*N*/''CR'' AND SubType = ''R'' THEN amount ELSE 0 END) AS arJTD_b, 
      sum(CASE WHEN Period >= 000000 AND Period <= @Period THEN CASE WHEN TransType = /*N*/''IN'' AND AutoEntry = ''N'' AND IsNull(SubType, '' '') <> ''X'' THEN -amount WHEN TransType = /*N*/''CR'' AND SubType = ''T'' AND IsNull(Invoice, /*N*/'' '') <> /*N*/'' '' THEN -amount WHEN TransType = /*N*/''CR'' AND SubType = ''R'' THEN amount ELSE 0 END ELSE 0 END) AS arCustom_c, sum(CASE WHEN Period >= 000000 AND Period <= @Period THEN CASE WHEN TransType = /*N*/''IN'' AND AutoEntry = ''N'' AND IsNull(SubType, '' '') <> ''X'' THEN -amount WHEN TransType = /*N*/''CR'' AND SubType = ''T'' AND IsNull(Invoice, /*N*/'' '') <> /*N*/'' '' THEN -amount WHEN TransType = /*N*/''CR'' AND SubType = ''R'' THEN amount ELSE 0 END ELSE 0 END) AS arCustom_b, 
      0 AS billCur_c,0 AS billCur_b, 0 AS billYTD_c,0 AS billYTD_b, 0 AS billJTD_c,0 AS billJTD_b, 0 AS billCustom_c,0 AS billCustom_b, 0 AS recdCur_c,0 AS recdCur_b, 0 AS recdYTD_c,0 AS recdYTD_b, 0 AS recdJTD_c,0 AS recdJTD_b, 0 AS recdCustom_c,0 AS recdCustom_b, 0 AS directCur_c,0 directCur_b, 0 AS directYTD_c,0 directYTD_b, 0 AS directJTD_c,0 directJTD_b, 0 AS directCustom_c,0 directCustom_b, 0 AS reimbCur_c,0 reimbCur_b, 0 AS reimbYTD_c,0 reimbYTD_b, 0 AS reimbJTD_c,0 reimbJTD_b, 0 AS reimbCustom_c,0 reimbCustom_b, 
      sum(CASE WHEN CA.Type = 4 AND Period = @Period AND (NOT (TransType = /*N*/''IN'' AND LinkWBS1 IS NOT NULL))    THEN IsNull(-amount, 0) ELSE 0 END) AS revCur_c, sum(CASE WHEN CA.Type = 4 AND Period = @Period AND (NOT (TransType = /*N*/''IN'' AND LinkWBS1 IS NOT NULL))    THEN IsNull(-amount, 0) ELSE 0 END) AS revCur_b, 
      sum(CASE WHEN CA.Type = 4 AND Period >= @MinPeriod AND (NOT (TransType = /*N*/''IN'' AND LinkWBS1 IS NOT NULL))    THEN IsNull(-amount, 0) ELSE 0 END) AS revYTD_c, sum(CASE WHEN CA.Type = 4 AND Period >= @MinPeriod AND (NOT (TransType = /*N*/''IN'' AND LinkWBS1 IS NOT NULL))    THEN IsNull(-amount, 0) ELSE 0 END) AS revYTD_b, 
      sum(CASE WHEN CA.Type = 4 AND (NOT (TransType = /*N*/''IN'' AND LinkWBS1 IS NOT NULL))    THEN IsNull(-amount, 0) ELSE 0 END) AS revJTD_c,sum(CASE WHEN CA.Type = 4 AND (NOT (TransType = /*N*/''IN'' AND LinkWBS1 IS NOT NULL))    THEN IsNull(-amount, 0) ELSE 0 END) AS revJTD_b,
      sum(CASE WHEN CA.Type = 4 AND Period >= 000000 AND Period <= @Period AND (NOT (TransType = /*N*/''IN'' AND LinkWBS1 IS NOT NULL))    THEN IsNull(-amount, 0) ELSE 0 END) AS revCustom_c, sum(CASE WHEN CA.Type = 4 AND Period >= 000000 AND Period <= @Period AND (NOT (TransType = /*N*/''IN'' AND LinkWBS1 IS NOT NULL))    THEN IsNull(-amount, 0) ELSE 0 END) AS revCustom_b, 0 AS ubGLCur_c, 0 AS ubGLYTD_c, 0 AS ubGLJTD_c,0 AS ubGLCustom_c, 0 althrsBud,0 altamtBud_c,0 altamtBud_b,
       0.0 poAmt_c,0.00 poAmt_b,
      0.0 rppctcmpcalc, 0.00 targetMult_c,0.00 targetMult_b,0.00 rpfee_c,0.00 rpfee_b,0.00 rpconsultfee_c,0.00 rpconsultfee_b,0.00 rpreimballow_c,0.00 rpreimballow_b,
     '''' AS wbs1customCol1,'''' AS wbs1customCol2,'''' AS wbs1customCol3,'''' AS wbs1customCol4,'''' AS wbs1customCol5,
     '''' AS wbs2customCol1,'''' AS wbs2customCol2,'''' AS wbs2customCol3,'''' AS wbs2customCol4,'''' AS wbs2customCol5,
     '''' AS wbs3customCol1,'''' AS wbs3customCol2,'''' AS wbs3customCol3,'''' AS wbs3customCol4,'''' AS wbs3customCol5,
      '''' As currencyCodeProj,'''' As currencyCodeBill,'''' As currencyCodeFunct,'''' as currencyCodePres_Cost, '''' as currencyCodePres_Bill, '''' as currencyCodePres_ARFee, '''' as currencyCode_Opportunity, 
      0 As currencyCodeProjCount, 0 As currencyCodeBillCount, 0 As currencyCodeFunctCount, 0 As currencyCodePresCount_Cost, 0 As currencyCodePresCount_Bill, 0 As currencyCodePresCount_ARFee, 0 As currencyCodeCount_Opportunity, 
       min(PR.WBS1) WBS1,min(LEVEL2.WBS2) WBS2,min(LEVEL3.WBS3) WBS3,'''' w1Name,'''' w2Name,'''' w3Name,
      '''' ChargeType,max(PR.ChargeType) ChargeTypeCode,min(PR.SubLevel) SubLevel1,min(LEVEL2.SubLevel) SubLevel2,min(LEVEL3.SubLevel) SubLevel3,
      '''' w1principal,'''' w2principal,'''' w3principal,'''' w1prinName,'''' w2prinName,'''' w3prinName,'''' w1projMgr,'''' w2projMgr,'''' w3projMgr,'''' w1prgName,'''' w2prgName,'''' w3prgName,'''' w1supervisor,'''' w2supervisor,'''' w3supervisor,'''' w1supName,'''' w2supName,'''' w3supName,
      '''' PRAddress1,'''' PRAddress2,'''' PRAddress3, '''' PRCity,'''' PRState,'''' PRZip,'''' PRCounty,'''' PRCountry,'''' PRPhone,'''' PRFax,'''' PREmail,
      '''' ClientID,'''' ClientName,'''' CLAddress,'''' CLAddress1,'''' CLAddress2,'''' CLAddress3,'''' CLAddress4,'''' CLCity,'''' CLState,'''' CLZip,'''' CLCountry,'''' CLPhone,'''' CLFax,'''' CLEMail,
      '''' w1Org,'''' w2Org,'''' w3Org,'''' w1OrgName,'''' w2OrgName,'''' w3OrgName,
      '''' as w1TLProjectName, '''' as w2TLProjectName, '''' as w3TLProjectName, '''' ServiceProfileCode,'''' ServiceProfileDesc,
      0 mult,0 mult_sum,0 fee_c,0 fee_b,
      0 consultFee_c,0 consultFee_b,0 reimbAllow_c,0 reimbAllow_b,
      0.0 feeDirectLabor, 0.0 feeDirectExpense, 0.0 reimbAllowExpense, 0.0 reimbAllowConsultant,
      0 contractfee, 0 contractconsultFee, 0 contractreimbAllow,
      0.0 contractFeeDirectLabor, 0.0 contractFeeDirectExpense, 0.0 contractReimbAllowExpense, 0.0 contractReimbAllowConsultant,
      0 BudOHRate,'''' w1Status,'''' w2Status,'''' w3Status,'''' w1StatusDesc,'''' w2StatusDesc,'''' w3StatusDesc,'''' wbs1RevType,'''' wbs2RevType, '''' wbs3RevType,'''' wbs1RevTypeDesc,'''' wbs2RevTypeDesc,'''' wbs3RevTypeDesc,'''' UnitTable,
      NULL w1StartDate,NULL w2StartDate,NULL w3StartDate,NULL w1EndDate,NULL w2EndDate,NULL w3EndDate,
      0.00 w1PctComp,0.00 w2PctComp,0.00 w3PctComp,0.00 w1LabPctComp,0.00 w2LabPctComp,0.00 w3LabPctComp,0.00 w1ExpPctComp,0.00 w2ExpPctComp,0.00 w3ExpPctComp,
      '''' BillByDefault,'''' BillableWarning,'''' BudgetedFlag,'''' BudgetSource,'''' BudgetLevel,'''' BudgetedLevels,
      '''' XCharge,'''' XChargeMethod,0 XChargeMult,'''' ContactID,'''' ContactFirstName,'''' ContactLastName,'''' CLBillingAddr,'''' w1LongName,'''' w2LongName,'''' w3LongName,
      '''' CLBAddress1,'''' CLBAddress2,'''' CLBAddress3,'''' CLBAddress4,'''' CLBCity,'''' CLBState,'''' CLBZip,'''' CLBCountry,'''' CLBPhone,'''' CLBFax,'''' CLBEMail,
      '''' w1FederalInd,'''' w2FederalInd,'''' w3FederalInd,'''' w1ProjectType,'''' w2ProjectType,'''' w3ProjectType,'''' w1projectTypeDesc,'''' w2projectTypeDesc,'''' w3projectTypeDesc,
      '''' w1Responsibility,'''' w2Responsibility,'''' w3Responsibility,'''' w1ResponsibilityDesc,'''' w2ResponsibilityDesc,'''' w3ResponsibilityDesc,'''' w1Referable,'''' w2Referable,'''' w3Referable,
      NULL w1EstCompletionDate,NULL w2EstCompletionDate,NULL w3EstCompletionDate,NULL w1ActCompletionDate,NULL w2ActCompletionDate,NULL w3ActCompletionDate,NULL w1ContractDate,NULL w2ContractDate,NULL w3ContractDate,
      NULL w1ConstComplDate,NULL w2ConstComplDate,NULL w3ConstComplDate,NULL w1ProfServicesComplDate,NULL w2ProfServicesComplDate,NULL w3ProfServicesComplDate,NULL w1BidDate,NULL w2BidDate,NULL w3BidDate,
      '''' w1ComplDateComment,'''' w2ComplDateComment,'''' w3ComplDateComment,0 w1FirmCost,0 w2FirmCost,0 w3FirmCost,'''' w1FirmCostComment,'''' w2FirmCostComment,'''' w3FirmCostComment,
      0 w1TotalProjectCost,0 w2TotalProjectCost,0 w3TotalProjectCost,'''' w1TotalCostComment,'''' w2TotalCostComment,'''' w3TotalCostComment,
      '''' ClientConfidential,'''' ClientAlias,'''' BillingClientID,'''' BillClientName,'''' BillingContactID,'''' billContactLastName,'''' billContactFirstName,'''' ProposalWBS1,
      '''' w1CreateUser,'''' w2CreateUser,'''' w3CreateUser,NULL w1CreateDate,NULL w2CreateDate,NULL w3CreateDate,
      '''' w1ModUser,'''' w2ModUser,'''' w3ModUser,NULL w1ModDate,NULL w2ModDate,NULL w3ModDate,
      '''' w1CostRateMeth,'''' w2CostRateMeth,'''' w3CostRateMeth,'''' w1CostRateTableNo,'''' w3CostRateTableNo,'''' w3CostRateTableNo,
      '''' w1PayRateMeth,'''' w2PayRateMeth,'''' w3PayRateMeth,'''' w1PayRateTableNo,'''' w2PayRateTableNo,'''' w3PayRateTableNo,'''' w1Locale,'''' w1Locale,'''' w1Locale,
      '''' w1LineItemApproval,'''' w2LineItemApproval,'''' w3LineItemApproval,'''' w1LineItemApprovalEK,'''' w2LineItemApprovalEK,'''' w3LineItemApprovalEK,
      '''' opp_name,'''' opp_number,'''' opp_prproposalwbs1,'''' opp_prproposalname,'''' opp_org,'''' opp_orgname,'''' opp_clname,'''' opp_contactname,'''' opp_projmgrname,'''' opp_principalname,'''' opp_supervisorname,
      0 opp_revenue,'''' opp_probability,0 opp_weightedrevenue,NULL opp_opendate,NULL opp_closedate,'''' opp_daysopen,'''' opp_stage,'''' opp_type,'''' opp_source,'''' opp_status,NULL opp_eststartdate,NULL opp_estcompletiondate,
      '''' opp_address1,'''' opp_address2,'''' opp_address3,'''' opp_city,'''' opp_state,'''' opp_zip,'''' opp_country,'''' opp_createuser,NULL opp_createdate,'''' opp_moduser,NULL opp_moddate,'''' opp_oppdesc 
FROM PR As LEVEL3 LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1=ProjectCustomTabFields.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFields.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFields.WBS3 , PR AS LEVEL2 , PR  LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes2 ON substring(PR.Org, 4, 2) = groupCFGOrgCodes2.Code and groupCFGOrgCodes2.orgLevel = 2 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3, ##RWL_' + @SessionId + ' , LedgerAR left Join CA On LedgerAR.Account = CA.Account 
WHERE ((LedgerAR.Wbs1 = LEVEL3.Wbs1 AND LedgerAR.Wbs2 = LEVEL3.Wbs2 AND LedgerAR.Wbs3 = LEVEL3.Wbs3) 
 and LEVEL3.WBS1 not like ''ZZ%''
 AND (LedgerAR.Wbs1 = LEVEL2.Wbs1 AND LedgerAR.Wbs2 = LEVEL2.wbs2)
 AND (LedgerAR.Wbs1 = PR.Wbs1) 
 AND (PR.Wbs2 = /*N*/'' '' AND PR.Wbs3 = /*N*/'' '' AND LEVEL2.Wbs3 = /*N*/'' '')) AND Period <= @Period AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + ''' 
       AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 
       AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/'''' 
GROUP BY IsNull(substring(PR.Org, 4, 2), ''''), IsNull(substring(PR.Org, 7, 3), '''')

/***LedgerAP: Revenue Columns***/
UNION ALL 
SELECT IsNull(substring(PR.Org, 4, 2), '''') As group1, Min(groupCFGOrgCodes2.Label) As groupDesc1, IsNull(substring(PR.Org, 7, 3), '''') As group2, Min(groupCFGOrgCodes3.Label) As groupDesc2, ''8'' AS recordType,'''' subType, 
      0 AS hrsCur,0 hrsCurLabReg,0 hrsCurLabOvt,0 AS hrsYTD,0 hrsYTDLabReg,0 hrsYTDLabOvt,0 AS hrsJTD,0 hrsJTDLabReg,0 hrsJTDLabOvt,0 AS hrsCustom,0 hrsCustomLabReg,0 hrsCustomLabOvt,0 AS hrsBud, 
      0 AS hrsPctComp_c,0 as hrsPctComp_b, 0 AS hrsETC, 0 AS hrsEAC, 
      0 amtCurLabReg_c,0 amtCurLabReg_b,0 amtCurLabOvt_c,0 amtCurLabOvt_b,0 amtYTDLabReg_c,0 amtYTDLabReg_b,0 amtYTDLabOvt_c,0 amtYTDLabOvt_b,0 amtJTDLabReg_c,0 amtJTDLabReg_b,0 amtJTDLabOvt_c,0 amtJTDLabOvt_b,0 amtCustomLabReg_c,0 amtCustomLabReg_b,0 amtCustomLabOvt_c,0 amtCustomLabOvt_b,
      0 AS amtCur_c,0 amtCur_b, 0 AS amtYTD_c,0 amtYTD_b, 0 AS amtJTD_c,0 amtJTD_b, 0 AS amtCustom_c,0 amtCustom_b, 
      0 AS amtBud_c,0 amtBud_b, 0 AS amtEAC_c,0 amtEAC_b, 0 AS amtETC_c,0 amtETC_b, 
      0 AS amtPctComp_c,0 amtPctComp_b, 0 AS amtPctCompLab_c,0 amtPctCompLab_b, 0 AS amtPctCompExp_c,0 amtPctCompExp_b, 
      0 AS arCur_c,0 AS arCur_b, 0 AS arYTD_c,0 AS arYTD_b, 0 AS arJTD_c,0 AS arJTD_b, 0 AS arCustom_c,0 AS arCustom_b, 0 AS billCur_c,0 AS billCur_b, 0 AS billYTD_c,0 AS billYTD_b, 0 AS billJTD_c,0 AS billJTD_b, 0 AS billCustom_c,0 AS billCustom_b, 0 AS recdCur_c,0 AS recdCur_b, 0 AS recdYTD_c,0 AS recdYTD_b, 0 AS recdJTD_c,0 AS recdJTD_b, 0 AS recdCustom_c,0 AS recdCustom_b, 0 AS directCur_c,0 directCur_b, 0 AS directYTD_c,0 directYTD_b, 0 AS directJTD_c,0 directJTD_b, 0 AS directCustom_c,0 directCustom_b, 0 AS reimbCur_c,0 reimbCur_b, 0 AS reimbYTD_c,0 reimbYTD_b, 0 AS reimbJTD_c,0 reimbJTD_b, 0 AS reimbCustom_c,0 reimbCustom_b, 
      sum(CASE WHEN Period = @Period THEN IsNull(-amount, 0) ELSE 0 END) AS revCur_c, sum(CASE WHEN Period = @Period THEN IsNull(-amount, 0) ELSE 0 END) AS revCur_b, 
      sum(CASE WHEN Period >= @MinPeriod THEN IsNull(-amount, 0) ELSE 0 END) AS revYTD_c, sum(CASE WHEN Period >= @MinPeriod THEN IsNull(-amount, 0) ELSE 0 END) AS revYTD_b, 
      sum(IsNull(-amount, 0)) AS revJTD_c,sum(IsNull(-amount, 0)) AS revJTD_b,
      sum(CASE WHEN Period >= 000000 AND Period <= @Period THEN IsNull(-amount, 0) ELSE 0 END) AS revCustom_c, sum(CASE WHEN Period >= 000000 AND Period <= @Period THEN IsNull(-amount, 0) ELSE 0 END) AS revCustom_b, 0 AS ubGLCur_c, 0 AS ubGLYTD_c, 0 AS ubGLJTD_c,0 AS ubGLCustom_c, 0 althrsBud,0 altamtBud_c,0 altamtBud_b,
       0.0 poAmt_c,0.00 poAmt_b,
      0.0 rppctcmpcalc, 0.00 targetMult_c,0.00 targetMult_b,0.00 rpfee_c,0.00 rpfee_b,0.00 rpconsultfee_c,0.00 rpconsultfee_b,0.00 rpreimballow_c,0.00 rpreimballow_b,
     '''' AS wbs1customCol1,'''' AS wbs1customCol2,'''' AS wbs1customCol3,'''' AS wbs1customCol4,'''' AS wbs1customCol5,
     '''' AS wbs2customCol1,'''' AS wbs2customCol2,'''' AS wbs2customCol3,'''' AS wbs2customCol4,'''' AS wbs2customCol5,
     '''' AS wbs3customCol1,'''' AS wbs3customCol2,'''' AS wbs3customCol3,'''' AS wbs3customCol4,'''' AS wbs3customCol5,
      '''' As currencyCodeProj,'''' As currencyCodeBill,'''' As currencyCodeFunct,'''' as currencyCodePres_Cost, '''' as currencyCodePres_Bill, '''' as currencyCodePres_ARFee, '''' as currencyCode_Opportunity, 
      0 As currencyCodeProjCount, 0 As currencyCodeBillCount, 0 As currencyCodeFunctCount, 0 As currencyCodePresCount_Cost, 0 As currencyCodePresCount_Bill, 0 As currencyCodePresCount_ARFee, 0 As currencyCodeCount_Opportunity, 
       min(PR.WBS1) WBS1,min(LEVEL2.WBS2) WBS2,min(LEVEL3.WBS3) WBS3,'''' w1Name,'''' w2Name,'''' w3Name,
      '''' ChargeType,max(PR.ChargeType) ChargeTypeCode,min(PR.SubLevel) SubLevel1,min(LEVEL2.SubLevel) SubLevel2,min(LEVEL3.SubLevel) SubLevel3,
      '''' w1principal,'''' w2principal,'''' w3principal,'''' w1prinName,'''' w2prinName,'''' w3prinName,'''' w1projMgr,'''' w2projMgr,'''' w3projMgr,'''' w1prgName,'''' w2prgName,'''' w3prgName,'''' w1supervisor,'''' w2supervisor,'''' w3supervisor,'''' w1supName,'''' w2supName,'''' w3supName,
      '''' PRAddress1,'''' PRAddress2,'''' PRAddress3, '''' PRCity,'''' PRState,'''' PRZip,'''' PRCounty,'''' PRCountry,'''' PRPhone,'''' PRFax,'''' PREmail,
      '''' ClientID,'''' ClientName,'''' CLAddress,'''' CLAddress1,'''' CLAddress2,'''' CLAddress3,'''' CLAddress4,'''' CLCity,'''' CLState,'''' CLZip,'''' CLCountry,'''' CLPhone,'''' CLFax,'''' CLEMail,
      '''' w1Org,'''' w2Org,'''' w3Org,'''' w1OrgName,'''' w2OrgName,'''' w3OrgName,
      '''' as w1TLProjectName, '''' as w2TLProjectName, '''' as w3TLProjectName, '''' ServiceProfileCode,'''' ServiceProfileDesc,
      0 mult,0 mult_sum,0 fee_c,0 fee_b,
      0 consultFee_c,0 consultFee_b,0 reimbAllow_c,0 reimbAllow_b,
      0.0 feeDirectLabor, 0.0 feeDirectExpense, 0.0 reimbAllowExpense, 0.0 reimbAllowConsultant,
      0 contractfee, 0 contractconsultFee, 0 contractreimbAllow,
      0.0 contractFeeDirectLabor, 0.0 contractFeeDirectExpense, 0.0 contractReimbAllowExpense, 0.0 contractReimbAllowConsultant,
      0 BudOHRate,'''' w1Status,'''' w2Status,'''' w3Status,'''' w1StatusDesc,'''' w2StatusDesc,'''' w3StatusDesc,'''' wbs1RevType,'''' wbs2RevType, '''' wbs3RevType,'''' wbs1RevTypeDesc,'''' wbs2RevTypeDesc,'''' wbs3RevTypeDesc,'''' UnitTable,
      NULL w1StartDate,NULL w2StartDate,NULL w3StartDate,NULL w1EndDate,NULL w2EndDate,NULL w3EndDate,
      0.00 w1PctComp,0.00 w2PctComp,0.00 w3PctComp,0.00 w1LabPctComp,0.00 w2LabPctComp,0.00 w3LabPctComp,0.00 w1ExpPctComp,0.00 w2ExpPctComp,0.00 w3ExpPctComp,
      '''' BillByDefault,'''' BillableWarning,'''' BudgetedFlag,'''' BudgetSource,'''' BudgetLevel,'''' BudgetedLevels,
      '''' XCharge,'''' XChargeMethod,0 XChargeMult,'''' ContactID,'''' ContactFirstName,'''' ContactLastName,'''' CLBillingAddr,'''' w1LongName,'''' w2LongName,'''' w3LongName,
      '''' CLBAddress1,'''' CLBAddress2,'''' CLBAddress3,'''' CLBAddress4,'''' CLBCity,'''' CLBState,'''' CLBZip,'''' CLBCountry,'''' CLBPhone,'''' CLBFax,'''' CLBEMail,
      '''' w1FederalInd,'''' w2FederalInd,'''' w3FederalInd,'''' w1ProjectType,'''' w2ProjectType,'''' w3ProjectType,'''' w1projectTypeDesc,'''' w2projectTypeDesc,'''' w3projectTypeDesc,
      '''' w1Responsibility,'''' w2Responsibility,'''' w3Responsibility,'''' w1ResponsibilityDesc,'''' w2ResponsibilityDesc,'''' w3ResponsibilityDesc,'''' w1Referable,'''' w2Referable,'''' w3Referable,
      NULL w1EstCompletionDate,NULL w2EstCompletionDate,NULL w3EstCompletionDate,NULL w1ActCompletionDate,NULL w2ActCompletionDate,NULL w3ActCompletionDate,NULL w1ContractDate,NULL w2ContractDate,NULL w3ContractDate,
      NULL w1ConstComplDate,NULL w2ConstComplDate,NULL w3ConstComplDate,NULL w1ProfServicesComplDate,NULL w2ProfServicesComplDate,NULL w3ProfServicesComplDate,NULL w1BidDate,NULL w2BidDate,NULL w3BidDate,
      '''' w1ComplDateComment,'''' w2ComplDateComment,'''' w3ComplDateComment,0 w1FirmCost,0 w2FirmCost,0 w3FirmCost,'''' w1FirmCostComment,'''' w2FirmCostComment,'''' w3FirmCostComment,
      0 w1TotalProjectCost,0 w2TotalProjectCost,0 w3TotalProjectCost,'''' w1TotalCostComment,'''' w2TotalCostComment,'''' w3TotalCostComment,
      '''' ClientConfidential,'''' ClientAlias,'''' BillingClientID,'''' BillClientName,'''' BillingContactID,'''' billContactLastName,'''' billContactFirstName,'''' ProposalWBS1,
      '''' w1CreateUser,'''' w2CreateUser,'''' w3CreateUser,NULL w1CreateDate,NULL w2CreateDate,NULL w3CreateDate,
      '''' w1ModUser,'''' w2ModUser,'''' w3ModUser,NULL w1ModDate,NULL w2ModDate,NULL w3ModDate,
      '''' w1CostRateMeth,'''' w2CostRateMeth,'''' w3CostRateMeth,'''' w1CostRateTableNo,'''' w3CostRateTableNo,'''' w3CostRateTableNo,
      '''' w1PayRateMeth,'''' w2PayRateMeth,'''' w3PayRateMeth,'''' w1PayRateTableNo,'''' w2PayRateTableNo,'''' w3PayRateTableNo,'''' w1Locale,'''' w1Locale,'''' w1Locale,
      '''' w1LineItemApproval,'''' w2LineItemApproval,'''' w3LineItemApproval,'''' w1LineItemApprovalEK,'''' w2LineItemApprovalEK,'''' w3LineItemApprovalEK,
      '''' opp_name,'''' opp_number,'''' opp_prproposalwbs1,'''' opp_prproposalname,'''' opp_org,'''' opp_orgname,'''' opp_clname,'''' opp_contactname,'''' opp_projmgrname,'''' opp_principalname,'''' opp_supervisorname,
      0 opp_revenue,'''' opp_probability,0 opp_weightedrevenue,NULL opp_opendate,NULL opp_closedate,'''' opp_daysopen,'''' opp_stage,'''' opp_type,'''' opp_source,'''' opp_status,NULL opp_eststartdate,NULL opp_estcompletiondate,
      '''' opp_address1,'''' opp_address2,'''' opp_address3,'''' opp_city,'''' opp_state,'''' opp_zip,'''' opp_country,'''' opp_createuser,NULL opp_createdate,'''' opp_moduser,NULL opp_moddate,'''' opp_oppdesc 
FROM PR As LEVEL3 LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1=ProjectCustomTabFields.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFields.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFields.WBS3 , PR AS LEVEL2 , PR  LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes2 ON substring(PR.Org, 4, 2) = groupCFGOrgCodes2.Code and groupCFGOrgCodes2.orgLevel = 2 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3, ##RWL_' + @SessionId + ' , LedgerAP Left Join CA ON LedgerAP.Account = CA.Account 
WHERE ((LedgerAP.Wbs1 = LEVEL3.Wbs1 AND LedgerAP.Wbs2 = LEVEL3.Wbs2 AND LedgerAP.Wbs3 = LEVEL3.Wbs3) 
 and LEVEL3.WBS1 not like ''ZZ%''
 AND (LedgerAP.Wbs1 = LEVEL2.Wbs1 AND LedgerAP.Wbs2 = LEVEL2.wbs2)
 AND (LedgerAP.Wbs1 = PR.Wbs1) 
 AND (PR.Wbs2 = /*N*/'' '' AND PR.Wbs3 = /*N*/'' '' AND LEVEL2.Wbs3 = /*N*/'' '')) AND Period <= @Period AND CA.Type = 4  AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + ''' 
       AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 
       AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/'''' 
GROUP BY IsNull(substring(PR.Org, 4, 2), ''''), IsNull(substring(PR.Org, 7, 3), '''')

/***LedgerEX: Revenue Columns***/
UNION ALL 
SELECT IsNull(substring(PR.Org, 4, 2), '''') As group1, Min(groupCFGOrgCodes2.Label) As groupDesc1, IsNull(substring(PR.Org, 7, 3), '''') As group2, Min(groupCFGOrgCodes3.Label) As groupDesc2, ''8'' AS recordType,'''' subType, 
      0 AS hrsCur,0 hrsCurLabReg,0 hrsCurLabOvt,0 AS hrsYTD,0 hrsYTDLabReg,0 hrsYTDLabOvt,0 AS hrsJTD,0 hrsJTDLabReg,0 hrsJTDLabOvt,0 AS hrsCustom,0 hrsCustomLabReg,0 hrsCustomLabOvt,0 AS hrsBud, 
      0 AS hrsPctComp_c,0 as hrsPctComp_b, 0 AS hrsETC, 0 AS hrsEAC, 
      0 amtCurLabReg_c,0 amtCurLabReg_b,0 amtCurLabOvt_c,0 amtCurLabOvt_b,0 amtYTDLabReg_c,0 amtYTDLabReg_b,0 amtYTDLabOvt_c,0 amtYTDLabOvt_b,0 amtJTDLabReg_c,0 amtJTDLabReg_b,0 amtJTDLabOvt_c,0 amtJTDLabOvt_b,0 amtCustomLabReg_c,0 amtCustomLabReg_b,0 amtCustomLabOvt_c,0 amtCustomLabOvt_b,
      0 AS amtCur_c,0 amtCur_b, 0 AS amtYTD_c,0 amtYTD_b, 0 AS amtJTD_c,0 amtJTD_b, 0 AS amtCustom_c,0 amtCustom_b, 
      0 AS amtBud_c,0 amtBud_b, 0 AS amtEAC_c,0 amtEAC_b, 0 AS amtETC_c,0 amtETC_b, 
      0 AS amtPctComp_c,0 amtPctComp_b, 0 AS amtPctCompLab_c,0 amtPctCompLab_b, 0 AS amtPctCompExp_c,0 amtPctCompExp_b, 
      0 AS arCur_c,0 AS arCur_b, 0 AS arYTD_c,0 AS arYTD_b, 0 AS arJTD_c,0 AS arJTD_b, 0 AS arCustom_c,0 AS arCustom_b, 0 AS billCur_c,0 AS billCur_b, 0 AS billYTD_c,0 AS billYTD_b, 0 AS billJTD_c,0 AS billJTD_b, 0 AS billCustom_c,0 AS billCustom_b, 0 AS recdCur_c,0 AS recdCur_b, 0 AS recdYTD_c,0 AS recdYTD_b, 0 AS recdJTD_c,0 AS recdJTD_b, 0 AS recdCustom_c,0 AS recdCustom_b, 0 AS directCur_c,0 directCur_b, 0 AS directYTD_c,0 directYTD_b, 0 AS directJTD_c,0 directJTD_b, 0 AS directCustom_c,0 directCustom_b, 0 AS reimbCur_c,0 reimbCur_b, 0 AS reimbYTD_c,0 reimbYTD_b, 0 AS reimbJTD_c,0 reimbJTD_b, 0 AS reimbCustom_c,0 reimbCustom_b, 
      sum(CASE WHEN Period = @Period THEN IsNull(-amount, 0) ELSE 0 END) AS revCur_c, sum(CASE WHEN Period = @Period THEN IsNull(-amount, 0) ELSE 0 END) AS revCur_b, 
      sum(CASE WHEN Period >= @MinPeriod THEN IsNull(-amount, 0) ELSE 0 END) AS revYTD_c, sum(CASE WHEN Period >= @MinPeriod THEN IsNull(-amount, 0) ELSE 0 END) AS revYTD_b, 
      sum(IsNull(-amount, 0)) AS revJTD_c,sum(IsNull(-amount, 0)) AS revJTD_b,
      sum(CASE WHEN Period >= 000000 AND Period <= @Period THEN IsNull(-amount, 0) ELSE 0 END) AS revCustom_c, sum(CASE WHEN Period >= 000000 AND Period <= @Period THEN IsNull(-amount, 0) ELSE 0 END) AS revCustom_b, 0 AS ubGLCur_c, 0 AS ubGLYTD_c, 0 AS ubGLJTD_c,0 AS ubGLCustom_c, 0 althrsBud,0 altamtBud_c,0 altamtBud_b,
       0.0 poAmt_c,0.00 poAmt_b,
      0.0 rppctcmpcalc, 0.00 targetMult_c,0.00 targetMult_b,0.00 rpfee_c,0.00 rpfee_b,0.00 rpconsultfee_c,0.00 rpconsultfee_b,0.00 rpreimballow_c,0.00 rpreimballow_b,
     '''' AS wbs1customCol1,'''' AS wbs1customCol2,'''' AS wbs1customCol3,'''' AS wbs1customCol4,'''' AS wbs1customCol5,
     '''' AS wbs2customCol1,'''' AS wbs2customCol2,'''' AS wbs2customCol3,'''' AS wbs2customCol4,'''' AS wbs2customCol5,
     '''' AS wbs3customCol1,'''' AS wbs3customCol2,'''' AS wbs3customCol3,'''' AS wbs3customCol4,'''' AS wbs3customCol5,
      '''' As currencyCodeProj,'''' As currencyCodeBill,'''' As currencyCodeFunct,'''' as currencyCodePres_Cost, '''' as currencyCodePres_Bill, '''' as currencyCodePres_ARFee, '''' as currencyCode_Opportunity, 
      0 As currencyCodeProjCount, 0 As currencyCodeBillCount, 0 As currencyCodeFunctCount, 0 As currencyCodePresCount_Cost, 0 As currencyCodePresCount_Bill, 0 As currencyCodePresCount_ARFee, 0 As currencyCodeCount_Opportunity, 
       min(PR.WBS1) WBS1,min(LEVEL2.WBS2) WBS2,min(LEVEL3.WBS3) WBS3,'''' w1Name,'''' w2Name,'''' w3Name,
      '''' ChargeType,max(PR.ChargeType) ChargeTypeCode,min(PR.SubLevel) SubLevel1,min(LEVEL2.SubLevel) SubLevel2,min(LEVEL3.SubLevel) SubLevel3,
      '''' w1principal,'''' w2principal,'''' w3principal,'''' w1prinName,'''' w2prinName,'''' w3prinName,'''' w1projMgr,'''' w2projMgr,'''' w3projMgr,'''' w1prgName,'''' w2prgName,'''' w3prgName,'''' w1supervisor,'''' w2supervisor,'''' w3supervisor,'''' w1supName,'''' w2supName,'''' w3supName,
      '''' PRAddress1,'''' PRAddress2,'''' PRAddress3, '''' PRCity,'''' PRState,'''' PRZip,'''' PRCounty,'''' PRCountry,'''' PRPhone,'''' PRFax,'''' PREmail,
      '''' ClientID,'''' ClientName,'''' CLAddress,'''' CLAddress1,'''' CLAddress2,'''' CLAddress3,'''' CLAddress4,'''' CLCity,'''' CLState,'''' CLZip,'''' CLCountry,'''' CLPhone,'''' CLFax,'''' CLEMail,
      '''' w1Org,'''' w2Org,'''' w3Org,'''' w1OrgName,'''' w2OrgName,'''' w3OrgName,
      '''' as w1TLProjectName, '''' as w2TLProjectName, '''' as w3TLProjectName, '''' ServiceProfileCode,'''' ServiceProfileDesc,
      0 mult,0 mult_sum,0 fee_c,0 fee_b,
      0 consultFee_c,0 consultFee_b,0 reimbAllow_c,0 reimbAllow_b,
      0.0 feeDirectLabor, 0.0 feeDirectExpense, 0.0 reimbAllowExpense, 0.0 reimbAllowConsultant,
      0 contractfee, 0 contractconsultFee, 0 contractreimbAllow,
      0.0 contractFeeDirectLabor, 0.0 contractFeeDirectExpense, 0.0 contractReimbAllowExpense, 0.0 contractReimbAllowConsultant,
      0 BudOHRate,'''' w1Status,'''' w2Status,'''' w3Status,'''' w1StatusDesc,'''' w2StatusDesc,'''' w3StatusDesc,'''' wbs1RevType,'''' wbs2RevType, '''' wbs3RevType,'''' wbs1RevTypeDesc,'''' wbs2RevTypeDesc,'''' wbs3RevTypeDesc,'''' UnitTable,
      NULL w1StartDate,NULL w2StartDate,NULL w3StartDate,NULL w1EndDate,NULL w2EndDate,NULL w3EndDate,
      0.00 w1PctComp,0.00 w2PctComp,0.00 w3PctComp,0.00 w1LabPctComp,0.00 w2LabPctComp,0.00 w3LabPctComp,0.00 w1ExpPctComp,0.00 w2ExpPctComp,0.00 w3ExpPctComp,
      '''' BillByDefault,'''' BillableWarning,'''' BudgetedFlag,'''' BudgetSource,'''' BudgetLevel,'''' BudgetedLevels,
      '''' XCharge,'''' XChargeMethod,0 XChargeMult,'''' ContactID,'''' ContactFirstName,'''' ContactLastName,'''' CLBillingAddr,'''' w1LongName,'''' w2LongName,'''' w3LongName,
      '''' CLBAddress1,'''' CLBAddress2,'''' CLBAddress3,'''' CLBAddress4,'''' CLBCity,'''' CLBState,'''' CLBZip,'''' CLBCountry,'''' CLBPhone,'''' CLBFax,'''' CLBEMail,
      '''' w1FederalInd,'''' w2FederalInd,'''' w3FederalInd,'''' w1ProjectType,'''' w2ProjectType,'''' w3ProjectType,'''' w1projectTypeDesc,'''' w2projectTypeDesc,'''' w3projectTypeDesc,
      '''' w1Responsibility,'''' w2Responsibility,'''' w3Responsibility,'''' w1ResponsibilityDesc,'''' w2ResponsibilityDesc,'''' w3ResponsibilityDesc,'''' w1Referable,'''' w2Referable,'''' w3Referable,
      NULL w1EstCompletionDate,NULL w2EstCompletionDate,NULL w3EstCompletionDate,NULL w1ActCompletionDate,NULL w2ActCompletionDate,NULL w3ActCompletionDate,NULL w1ContractDate,NULL w2ContractDate,NULL w3ContractDate,
      NULL w1ConstComplDate,NULL w2ConstComplDate,NULL w3ConstComplDate,NULL w1ProfServicesComplDate,NULL w2ProfServicesComplDate,NULL w3ProfServicesComplDate,NULL w1BidDate,NULL w2BidDate,NULL w3BidDate,
      '''' w1ComplDateComment,'''' w2ComplDateComment,'''' w3ComplDateComment,0 w1FirmCost,0 w2FirmCost,0 w3FirmCost,'''' w1FirmCostComment,'''' w2FirmCostComment,'''' w3FirmCostComment,
      0 w1TotalProjectCost,0 w2TotalProjectCost,0 w3TotalProjectCost,'''' w1TotalCostComment,'''' w2TotalCostComment,'''' w3TotalCostComment,
      '''' ClientConfidential,'''' ClientAlias,'''' BillingClientID,'''' BillClientName,'''' BillingContactID,'''' billContactLastName,'''' billContactFirstName,'''' ProposalWBS1,
      '''' w1CreateUser,'''' w2CreateUser,'''' w3CreateUser,NULL w1CreateDate,NULL w2CreateDate,NULL w3CreateDate,
      '''' w1ModUser,'''' w2ModUser,'''' w3ModUser,NULL w1ModDate,NULL w2ModDate,NULL w3ModDate,
      '''' w1CostRateMeth,'''' w2CostRateMeth,'''' w3CostRateMeth,'''' w1CostRateTableNo,'''' w3CostRateTableNo,'''' w3CostRateTableNo,
      '''' w1PayRateMeth,'''' w2PayRateMeth,'''' w3PayRateMeth,'''' w1PayRateTableNo,'''' w2PayRateTableNo,'''' w3PayRateTableNo,'''' w1Locale,'''' w1Locale,'''' w1Locale,
      '''' w1LineItemApproval,'''' w2LineItemApproval,'''' w3LineItemApproval,'''' w1LineItemApprovalEK,'''' w2LineItemApprovalEK,'''' w3LineItemApprovalEK,
      '''' opp_name,'''' opp_number,'''' opp_prproposalwbs1,'''' opp_prproposalname,'''' opp_org,'''' opp_orgname,'''' opp_clname,'''' opp_contactname,'''' opp_projmgrname,'''' opp_principalname,'''' opp_supervisorname,
      0 opp_revenue,'''' opp_probability,0 opp_weightedrevenue,NULL opp_opendate,NULL opp_closedate,'''' opp_daysopen,'''' opp_stage,'''' opp_type,'''' opp_source,'''' opp_status,NULL opp_eststartdate,NULL opp_estcompletiondate,
      '''' opp_address1,'''' opp_address2,'''' opp_address3,'''' opp_city,'''' opp_state,'''' opp_zip,'''' opp_country,'''' opp_createuser,NULL opp_createdate,'''' opp_moduser,NULL opp_moddate,'''' opp_oppdesc 
FROM PR As LEVEL3 LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1=ProjectCustomTabFields.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFields.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFields.WBS3 , PR AS LEVEL2 , PR  LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes2 ON substring(PR.Org, 4, 2) = groupCFGOrgCodes2.Code and groupCFGOrgCodes2.orgLevel = 2 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3, ##RWL_' + @SessionId + ' , LedgerEX Left Join CA ON LedgerEX.Account = CA.Account 
WHERE ((LedgerEX.Wbs1 = LEVEL3.Wbs1 AND LedgerEX.Wbs2 = LEVEL3.Wbs2 AND LedgerEX.Wbs3 = LEVEL3.Wbs3) 
 and LEVEL3.WBS1 not like ''ZZ%''
 AND (LedgerEX.Wbs1 = LEVEL2.Wbs1 AND LedgerEX.Wbs2 = LEVEL2.wbs2)
 AND (LedgerEX.Wbs1 = PR.Wbs1) 
 AND (PR.Wbs2 = /*N*/'' '' AND PR.Wbs3 = /*N*/'' '' AND LEVEL2.Wbs3 = /*N*/'' '')) AND Period <= @Period AND CA.Type = 4  AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + ''' 
       AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 
       AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/'''' 
GROUP BY IsNull(substring(PR.Org, 4, 2), ''''), IsNull(substring(PR.Org, 7, 3), '''')

/***LedgerMisc: Revenue Columns***/
UNION ALL 
SELECT IsNull(substring(PR.Org, 4, 2), '''') As group1, Min(groupCFGOrgCodes2.Label) As groupDesc1, IsNull(substring(PR.Org, 7, 3), '''') As group2, Min(groupCFGOrgCodes3.Label) As groupDesc2, ''8'' AS recordType,'''' subType, 
      0 AS hrsCur,0 hrsCurLabReg,0 hrsCurLabOvt,0 AS hrsYTD,0 hrsYTDLabReg,0 hrsYTDLabOvt,0 AS hrsJTD,0 hrsJTDLabReg,0 hrsJTDLabOvt,0 AS hrsCustom,0 hrsCustomLabReg,0 hrsCustomLabOvt,0 AS hrsBud, 
      0 AS hrsPctComp_c,0 as hrsPctComp_b, 0 AS hrsETC, 0 AS hrsEAC, 
      0 amtCurLabReg_c,0 amtCurLabReg_b,0 amtCurLabOvt_c,0 amtCurLabOvt_b,0 amtYTDLabReg_c,0 amtYTDLabReg_b,0 amtYTDLabOvt_c,0 amtYTDLabOvt_b,0 amtJTDLabReg_c,0 amtJTDLabReg_b,0 amtJTDLabOvt_c,0 amtJTDLabOvt_b,0 amtCustomLabReg_c,0 amtCustomLabReg_b,0 amtCustomLabOvt_c,0 amtCustomLabOvt_b,
      0 AS amtCur_c,0 amtCur_b, 0 AS amtYTD_c,0 amtYTD_b, 0 AS amtJTD_c,0 amtJTD_b, 0 AS amtCustom_c,0 amtCustom_b, 
      0 AS amtBud_c,0 amtBud_b, 0 AS amtEAC_c,0 amtEAC_b, 0 AS amtETC_c,0 amtETC_b, 
      0 AS amtPctComp_c,0 amtPctComp_b, 0 AS amtPctCompLab_c,0 amtPctCompLab_b, 0 AS amtPctCompExp_c,0 amtPctCompExp_b, 
      0 AS arCur_c,0 AS arCur_b, 0 AS arYTD_c,0 AS arYTD_b, 0 AS arJTD_c,0 AS arJTD_b, 0 AS arCustom_c,0 AS arCustom_b, 0 AS billCur_c,0 AS billCur_b, 0 AS billYTD_c,0 AS billYTD_b, 0 AS billJTD_c,0 AS billJTD_b, 0 AS billCustom_c,0 AS billCustom_b, 0 AS recdCur_c,0 AS recdCur_b, 0 AS recdYTD_c,0 AS recdYTD_b, 0 AS recdJTD_c,0 AS recdJTD_b, 0 AS recdCustom_c,0 AS recdCustom_b, 0 AS directCur_c,0 directCur_b, 0 AS directYTD_c,0 directYTD_b, 0 AS directJTD_c,0 directJTD_b, 0 AS directCustom_c,0 directCustom_b, 0 AS reimbCur_c,0 reimbCur_b, 0 AS reimbYTD_c,0 reimbYTD_b, 0 AS reimbJTD_c,0 reimbJTD_b, 0 AS reimbCustom_c,0 reimbCustom_b, 
      sum(CASE WHEN Period = @Period THEN IsNull(-amount, 0) ELSE 0 END) AS revCur_c, sum(CASE WHEN Period = @Period THEN IsNull(-amount, 0) ELSE 0 END) AS revCur_b, 
      sum(CASE WHEN Period >= @MinPeriod THEN IsNull(-amount, 0) ELSE 0 END) AS revYTD_c, sum(CASE WHEN Period >= @MinPeriod THEN IsNull(-amount, 0) ELSE 0 END) AS revYTD_b, 
      sum(IsNull(-amount, 0)) AS revJTD_c,sum(IsNull(-amount, 0)) AS revJTD_b,
      sum(CASE WHEN Period >= 000000 AND Period <= @Period THEN IsNull(-amount, 0) ELSE 0 END) AS revCustom_c, sum(CASE WHEN Period >= 000000 AND Period <= @Period THEN IsNull(-amount, 0) ELSE 0 END) AS revCustom_b, 0 AS ubGLCur_c, 0 AS ubGLYTD_c, 0 AS ubGLJTD_c,0 AS ubGLCustom_c, 0 althrsBud,0 altamtBud_c,0 altamtBud_b,
       0.0 poAmt_c,0.00 poAmt_b,
      0.0 rppctcmpcalc, 0.00 targetMult_c,0.00 targetMult_b,0.00 rpfee_c,0.00 rpfee_b,0.00 rpconsultfee_c,0.00 rpconsultfee_b,0.00 rpreimballow_c,0.00 rpreimballow_b,
     '''' AS wbs1customCol1,'''' AS wbs1customCol2,'''' AS wbs1customCol3,'''' AS wbs1customCol4,'''' AS wbs1customCol5,
     '''' AS wbs2customCol1,'''' AS wbs2customCol2,'''' AS wbs2customCol3,'''' AS wbs2customCol4,'''' AS wbs2customCol5,
     '''' AS wbs3customCol1,'''' AS wbs3customCol2,'''' AS wbs3customCol3,'''' AS wbs3customCol4,'''' AS wbs3customCol5,
      '''' As currencyCodeProj,'''' As currencyCodeBill,'''' As currencyCodeFunct,'''' as currencyCodePres_Cost, '''' as currencyCodePres_Bill, '''' as currencyCodePres_ARFee, '''' as currencyCode_Opportunity, 
      0 As currencyCodeProjCount, 0 As currencyCodeBillCount, 0 As currencyCodeFunctCount, 0 As currencyCodePresCount_Cost, 0 As currencyCodePresCount_Bill, 0 As currencyCodePresCount_ARFee, 0 As currencyCodeCount_Opportunity, 
       min(PR.WBS1) WBS1,min(LEVEL2.WBS2) WBS2,min(LEVEL3.WBS3) WBS3,'''' w1Name,'''' w2Name,'''' w3Name,
      '''' ChargeType,max(PR.ChargeType) ChargeTypeCode,min(PR.SubLevel) SubLevel1,min(LEVEL2.SubLevel) SubLevel2,min(LEVEL3.SubLevel) SubLevel3,
      '''' w1principal,'''' w2principal,'''' w3principal,'''' w1prinName,'''' w2prinName,'''' w3prinName,'''' w1projMgr,'''' w2projMgr,'''' w3projMgr,'''' w1prgName,'''' w2prgName,'''' w3prgName,'''' w1supervisor,'''' w2supervisor,'''' w3supervisor,'''' w1supName,'''' w2supName,'''' w3supName,
      '''' PRAddress1,'''' PRAddress2,'''' PRAddress3, '''' PRCity,'''' PRState,'''' PRZip,'''' PRCounty,'''' PRCountry,'''' PRPhone,'''' PRFax,'''' PREmail,
      '''' ClientID,'''' ClientName,'''' CLAddress,'''' CLAddress1,'''' CLAddress2,'''' CLAddress3,'''' CLAddress4,'''' CLCity,'''' CLState,'''' CLZip,'''' CLCountry,'''' CLPhone,'''' CLFax,'''' CLEMail,
      '''' w1Org,'''' w2Org,'''' w3Org,'''' w1OrgName,'''' w2OrgName,'''' w3OrgName,
      '''' as w1TLProjectName, '''' as w2TLProjectName, '''' as w3TLProjectName, '''' ServiceProfileCode,'''' ServiceProfileDesc,
      0 mult,0 mult_sum,0 fee_c,0 fee_b,
      0 consultFee_c,0 consultFee_b,0 reimbAllow_c,0 reimbAllow_b,
      0.0 feeDirectLabor, 0.0 feeDirectExpense, 0.0 reimbAllowExpense, 0.0 reimbAllowConsultant,
      0 contractfee, 0 contractconsultFee, 0 contractreimbAllow,
      0.0 contractFeeDirectLabor, 0.0 contractFeeDirectExpense, 0.0 contractReimbAllowExpense, 0.0 contractReimbAllowConsultant,
      0 BudOHRate,'''' w1Status,'''' w2Status,'''' w3Status,'''' w1StatusDesc,'''' w2StatusDesc,'''' w3StatusDesc,'''' wbs1RevType,'''' wbs2RevType, '''' wbs3RevType,'''' wbs1RevTypeDesc,'''' wbs2RevTypeDesc,'''' wbs3RevTypeDesc,'''' UnitTable,
      NULL w1StartDate,NULL w2StartDate,NULL w3StartDate,NULL w1EndDate,NULL w2EndDate,NULL w3EndDate,
      0.00 w1PctComp,0.00 w2PctComp,0.00 w3PctComp,0.00 w1LabPctComp,0.00 w2LabPctComp,0.00 w3LabPctComp,0.00 w1ExpPctComp,0.00 w2ExpPctComp,0.00 w3ExpPctComp,
      '''' BillByDefault,'''' BillableWarning,'''' BudgetedFlag,'''' BudgetSource,'''' BudgetLevel,'''' BudgetedLevels,
      '''' XCharge,'''' XChargeMethod,0 XChargeMult,'''' ContactID,'''' ContactFirstName,'''' ContactLastName,'''' CLBillingAddr,'''' w1LongName,'''' w2LongName,'''' w3LongName,
      '''' CLBAddress1,'''' CLBAddress2,'''' CLBAddress3,'''' CLBAddress4,'''' CLBCity,'''' CLBState,'''' CLBZip,'''' CLBCountry,'''' CLBPhone,'''' CLBFax,'''' CLBEMail,
      '''' w1FederalInd,'''' w2FederalInd,'''' w3FederalInd,'''' w1ProjectType,'''' w2ProjectType,'''' w3ProjectType,'''' w1projectTypeDesc,'''' w2projectTypeDesc,'''' w3projectTypeDesc,
      '''' w1Responsibility,'''' w2Responsibility,'''' w3Responsibility,'''' w1ResponsibilityDesc,'''' w2ResponsibilityDesc,'''' w3ResponsibilityDesc,'''' w1Referable,'''' w2Referable,'''' w3Referable,
      NULL w1EstCompletionDate,NULL w2EstCompletionDate,NULL w3EstCompletionDate,NULL w1ActCompletionDate,NULL w2ActCompletionDate,NULL w3ActCompletionDate,NULL w1ContractDate,NULL w2ContractDate,NULL w3ContractDate,
      NULL w1ConstComplDate,NULL w2ConstComplDate,NULL w3ConstComplDate,NULL w1ProfServicesComplDate,NULL w2ProfServicesComplDate,NULL w3ProfServicesComplDate,NULL w1BidDate,NULL w2BidDate,NULL w3BidDate,
      '''' w1ComplDateComment,'''' w2ComplDateComment,'''' w3ComplDateComment,0 w1FirmCost,0 w2FirmCost,0 w3FirmCost,'''' w1FirmCostComment,'''' w2FirmCostComment,'''' w3FirmCostComment,
      0 w1TotalProjectCost,0 w2TotalProjectCost,0 w3TotalProjectCost,'''' w1TotalCostComment,'''' w2TotalCostComment,'''' w3TotalCostComment,
      '''' ClientConfidential,'''' ClientAlias,'''' BillingClientID,'''' BillClientName,'''' BillingContactID,'''' billContactLastName,'''' billContactFirstName,'''' ProposalWBS1,
      '''' w1CreateUser,'''' w2CreateUser,'''' w3CreateUser,NULL w1CreateDate,NULL w2CreateDate,NULL w3CreateDate,
      '''' w1ModUser,'''' w2ModUser,'''' w3ModUser,NULL w1ModDate,NULL w2ModDate,NULL w3ModDate,
      '''' w1CostRateMeth,'''' w2CostRateMeth,'''' w3CostRateMeth,'''' w1CostRateTableNo,'''' w3CostRateTableNo,'''' w3CostRateTableNo,
      '''' w1PayRateMeth,'''' w2PayRateMeth,'''' w3PayRateMeth,'''' w1PayRateTableNo,'''' w2PayRateTableNo,'''' w3PayRateTableNo,'''' w1Locale,'''' w1Locale,'''' w1Locale,
      '''' w1LineItemApproval,'''' w2LineItemApproval,'''' w3LineItemApproval,'''' w1LineItemApprovalEK,'''' w2LineItemApprovalEK,'''' w3LineItemApprovalEK,
      '''' opp_name,'''' opp_number,'''' opp_prproposalwbs1,'''' opp_prproposalname,'''' opp_org,'''' opp_orgname,'''' opp_clname,'''' opp_contactname,'''' opp_projmgrname,'''' opp_principalname,'''' opp_supervisorname,
      0 opp_revenue,'''' opp_probability,0 opp_weightedrevenue,NULL opp_opendate,NULL opp_closedate,'''' opp_daysopen,'''' opp_stage,'''' opp_type,'''' opp_source,'''' opp_status,NULL opp_eststartdate,NULL opp_estcompletiondate,
      '''' opp_address1,'''' opp_address2,'''' opp_address3,'''' opp_city,'''' opp_state,'''' opp_zip,'''' opp_country,'''' opp_createuser,NULL opp_createdate,'''' opp_moduser,NULL opp_moddate,'''' opp_oppdesc 
FROM PR As LEVEL3 LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1=ProjectCustomTabFields.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFields.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFields.WBS3 , PR AS LEVEL2 , PR  LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes2 ON substring(PR.Org, 4, 2) = groupCFGOrgCodes2.Code and groupCFGOrgCodes2.orgLevel = 2 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3, ##RWL_' + @SessionId + ' , LedgerMisc Left Join CA ON LedgerMisc.Account = CA.Account 
WHERE ((LedgerMisc.Wbs1 = LEVEL3.Wbs1 AND LedgerMisc.Wbs2 = LEVEL3.Wbs2 AND LedgerMisc.Wbs3 = LEVEL3.Wbs3) 
 and LEVEL3.WBS1 not like ''ZZ%''
 AND (LedgerMisc.Wbs1 = LEVEL2.Wbs1 AND LedgerMisc.Wbs2 = LEVEL2.wbs2)
 AND (LedgerMisc.Wbs1 = PR.Wbs1) 
 AND (PR.Wbs2 = /*N*/'' '' AND PR.Wbs3 = /*N*/'' '' AND LEVEL2.Wbs3 = /*N*/'' '')) AND Period <= @Period AND CA.Type = 4  AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + ''' 
       AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 
       AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/'''' 
GROUP BY IsNull(substring(PR.Org, 4, 2), ''''), IsNull(substring(PR.Org, 7, 3), '''')

/***LedgerMisc: unbilled Gain and Lose Columns***/
UNION ALL 
SELECT IsNull(substring(PR.Org, 4, 2), '''') As group1, Min(groupCFGOrgCodes2.Label) As groupDesc1, IsNull(substring(PR.Org, 7, 3), '''') As group2, Min(groupCFGOrgCodes3.Label) As groupDesc2, ''8'' AS recordType,'''' subType, 
      0 AS hrsCur,0 hrsCurLabReg,0 hrsCurLabOvt,0 AS hrsYTD,0 hrsYTDLabReg,0 hrsYTDLabOvt,0 AS hrsJTD,0 hrsJTDLabReg,0 hrsJTDLabOvt,0 AS hrsCustom,0 hrsCustomLabReg,0 hrsCustomLabOvt,0 AS hrsBud, 
      0 AS hrsPctComp_c,0 as hrsPctComp_b, 0 AS hrsETC, 0 AS hrsEAC, 
      0 amtCurLabReg_c,0 amtCurLabReg_b,0 amtCurLabOvt_c,0 amtCurLabOvt_b,0 amtYTDLabReg_c,0 amtYTDLabReg_b,0 amtYTDLabOvt_c,0 amtYTDLabOvt_b,0 amtJTDLabReg_c,0 amtJTDLabReg_b,0 amtJTDLabOvt_c,0 amtJTDLabOvt_b,0 amtCustomLabReg_c,0 amtCustomLabReg_b,0 amtCustomLabOvt_c,0 amtCustomLabOvt_b,
      0 AS amtCur_c,0 amtCur_b, 0 AS amtYTD_c,0 amtYTD_b, 0 AS amtJTD_c,0 amtJTD_b, 0 AS amtCustom_c,0 amtCustom_b, 
      0 AS amtBud_c,0 amtBud_b, 0 AS amtEAC_c,0 amtEAC_b, 0 AS amtETC_c,0 amtETC_b, 
      0 AS amtPctComp_c,0 amtPctComp_b, 0 AS amtPctCompLab_c,0 amtPctCompLab_b, 0 AS amtPctCompExp_c,0 amtPctCompExp_b, 
      0 AS arCur_c,0 AS arCur_b, 0 AS arYTD_c,0 AS arYTD_b, 0 AS arJTD_c,0 AS arJTD_b, 0 AS arCustom_c,0 AS arCustom_b, 0 AS billCur_c,0 AS billCur_b, 0 AS billYTD_c,0 AS billYTD_b, 0 AS billJTD_c,0 AS billJTD_b, 0 AS billCustom_c,0 AS billCustom_b, 0 AS recdCur_c,0 AS recdCur_b, 0 AS recdYTD_c,0 AS recdYTD_b, 0 AS recdJTD_c,0 AS recdJTD_b, 0 AS recdCustom_c,0 AS recdCustom_b, 0 AS directCur_c,0 directCur_b, 0 AS directYTD_c,0 directYTD_b, 0 AS directJTD_c,0 directJTD_b, 0 AS directCustom_c,0 directCustom_b, 0 AS reimbCur_c,0 reimbCur_b, 0 AS reimbYTD_c,0 reimbYTD_b, 0 AS reimbJTD_c,0 reimbJTD_b, 0 AS reimbCustom_c,0 reimbCustom_b, 0 AS revCur_c, 0 AS revCur_b, 0 AS revYTD_c, 0 AS revYTD_b, 0 AS revJTD_c,0 AS revJTD_b,0 AS revCustom_c, 0 AS revCustom_b, 
      sum(CASE WHEN Period = @Period THEN IsNull(amount, 0) ELSE 0 END) AS ubGLCur_c, 
      sum(CASE WHEN Period >= @MinPeriod THEN IsNull(amount, 0) ELSE 0 END) AS ubGLYTD_c, 
      sum(IsNull(amount, 0)) AS ubGLJTD_c,
      sum(CASE WHEN Period >= 000000 AND Period <= @Period THEN IsNull(amount, 0) ELSE 0 END) AS ubGLCustom_c, 0 althrsBud,0 altamtBud_c,0 altamtBud_b,
       0.0 poAmt_c,0.00 poAmt_b,
      0.0 rppctcmpcalc, 0.00 targetMult_c,0.00 targetMult_b,0.00 rpfee_c,0.00 rpfee_b,0.00 rpconsultfee_c,0.00 rpconsultfee_b,0.00 rpreimballow_c,0.00 rpreimballow_b,
     '''' AS wbs1customCol1,'''' AS wbs1customCol2,'''' AS wbs1customCol3,'''' AS wbs1customCol4,'''' AS wbs1customCol5,
     '''' AS wbs2customCol1,'''' AS wbs2customCol2,'''' AS wbs2customCol3,'''' AS wbs2customCol4,'''' AS wbs2customCol5,
     '''' AS wbs3customCol1,'''' AS wbs3customCol2,'''' AS wbs3customCol3,'''' AS wbs3customCol4,'''' AS wbs3customCol5,
      '''' As currencyCodeProj,'''' As currencyCodeBill,'''' As currencyCodeFunct,'''' as currencyCodePres_Cost, '''' as currencyCodePres_Bill, '''' as currencyCodePres_ARFee, '''' as currencyCode_Opportunity, 
      0 As currencyCodeProjCount, 0 As currencyCodeBillCount, 0 As currencyCodeFunctCount, 0 As currencyCodePresCount_Cost, 0 As currencyCodePresCount_Bill, 0 As currencyCodePresCount_ARFee, 0 As currencyCodeCount_Opportunity, 
       min(PR.WBS1) WBS1,min(LEVEL2.WBS2) WBS2,min(LEVEL3.WBS3) WBS3,'''' w1Name,'''' w2Name,'''' w3Name,
      '''' ChargeType,max(PR.ChargeType) ChargeTypeCode,min(PR.SubLevel) SubLevel1,min(LEVEL2.SubLevel) SubLevel2,min(LEVEL3.SubLevel) SubLevel3,
      '''' w1principal,'''' w2principal,'''' w3principal,'''' w1prinName,'''' w2prinName,'''' w3prinName,'''' w1projMgr,'''' w2projMgr,'''' w3projMgr,'''' w1prgName,'''' w2prgName,'''' w3prgName,'''' w1supervisor,'''' w2supervisor,'''' w3supervisor,'''' w1supName,'''' w2supName,'''' w3supName,
      '''' PRAddress1,'''' PRAddress2,'''' PRAddress3, '''' PRCity,'''' PRState,'''' PRZip,'''' PRCounty,'''' PRCountry,'''' PRPhone,'''' PRFax,'''' PREmail,
      '''' ClientID,'''' ClientName,'''' CLAddress,'''' CLAddress1,'''' CLAddress2,'''' CLAddress3,'''' CLAddress4,'''' CLCity,'''' CLState,'''' CLZip,'''' CLCountry,'''' CLPhone,'''' CLFax,'''' CLEMail,
      '''' w1Org,'''' w2Org,'''' w3Org,'''' w1OrgName,'''' w2OrgName,'''' w3OrgName,
      '''' as w1TLProjectName, '''' as w2TLProjectName, '''' as w3TLProjectName, '''' ServiceProfileCode,'''' ServiceProfileDesc,
      0 mult,0 mult_sum,0 fee_c,0 fee_b,
      0 consultFee_c,0 consultFee_b,0 reimbAllow_c,0 reimbAllow_b,
      0.0 feeDirectLabor, 0.0 feeDirectExpense, 0.0 reimbAllowExpense, 0.0 reimbAllowConsultant,
      0 contractfee, 0 contractconsultFee, 0 contractreimbAllow,
      0.0 contractFeeDirectLabor, 0.0 contractFeeDirectExpense, 0.0 contractReimbAllowExpense, 0.0 contractReimbAllowConsultant,
      0 BudOHRate,'''' w1Status,'''' w2Status,'''' w3Status,'''' w1StatusDesc,'''' w2StatusDesc,'''' w3StatusDesc,'''' wbs1RevType,'''' wbs2RevType, '''' wbs3RevType,'''' wbs1RevTypeDesc,'''' wbs2RevTypeDesc,'''' wbs3RevTypeDesc,'''' UnitTable,
      NULL w1StartDate,NULL w2StartDate,NULL w3StartDate,NULL w1EndDate,NULL w2EndDate,NULL w3EndDate,
      0.00 w1PctComp,0.00 w2PctComp,0.00 w3PctComp,0.00 w1LabPctComp,0.00 w2LabPctComp,0.00 w3LabPctComp,0.00 w1ExpPctComp,0.00 w2ExpPctComp,0.00 w3ExpPctComp,
      '''' BillByDefault,'''' BillableWarning,'''' BudgetedFlag,'''' BudgetSource,'''' BudgetLevel,'''' BudgetedLevels,
      '''' XCharge,'''' XChargeMethod,0 XChargeMult,'''' ContactID,'''' ContactFirstName,'''' ContactLastName,'''' CLBillingAddr,'''' w1LongName,'''' w2LongName,'''' w3LongName,
      '''' CLBAddress1,'''' CLBAddress2,'''' CLBAddress3,'''' CLBAddress4,'''' CLBCity,'''' CLBState,'''' CLBZip,'''' CLBCountry,'''' CLBPhone,'''' CLBFax,'''' CLBEMail,
      '''' w1FederalInd,'''' w2FederalInd,'''' w3FederalInd,'''' w1ProjectType,'''' w2ProjectType,'''' w3ProjectType,'''' w1projectTypeDesc,'''' w2projectTypeDesc,'''' w3projectTypeDesc,
      '''' w1Responsibility,'''' w2Responsibility,'''' w3Responsibility,'''' w1ResponsibilityDesc,'''' w2ResponsibilityDesc,'''' w3ResponsibilityDesc,'''' w1Referable,'''' w2Referable,'''' w3Referable,
      NULL w1EstCompletionDate,NULL w2EstCompletionDate,NULL w3EstCompletionDate,NULL w1ActCompletionDate,NULL w2ActCompletionDate,NULL w3ActCompletionDate,NULL w1ContractDate,NULL w2ContractDate,NULL w3ContractDate,
      NULL w1ConstComplDate,NULL w2ConstComplDate,NULL w3ConstComplDate,NULL w1ProfServicesComplDate,NULL w2ProfServicesComplDate,NULL w3ProfServicesComplDate,NULL w1BidDate,NULL w2BidDate,NULL w3BidDate,
      '''' w1ComplDateComment,'''' w2ComplDateComment,'''' w3ComplDateComment,0 w1FirmCost,0 w2FirmCost,0 w3FirmCost,'''' w1FirmCostComment,'''' w2FirmCostComment,'''' w3FirmCostComment,
      0 w1TotalProjectCost,0 w2TotalProjectCost,0 w3TotalProjectCost,'''' w1TotalCostComment,'''' w2TotalCostComment,'''' w3TotalCostComment,
      '''' ClientConfidential,'''' ClientAlias,'''' BillingClientID,'''' BillClientName,'''' BillingContactID,'''' billContactLastName,'''' billContactFirstName,'''' ProposalWBS1,
      '''' w1CreateUser,'''' w2CreateUser,'''' w3CreateUser,NULL w1CreateDate,NULL w2CreateDate,NULL w3CreateDate,
      '''' w1ModUser,'''' w2ModUser,'''' w3ModUser,NULL w1ModDate,NULL w2ModDate,NULL w3ModDate,
      '''' w1CostRateMeth,'''' w2CostRateMeth,'''' w3CostRateMeth,'''' w1CostRateTableNo,'''' w3CostRateTableNo,'''' w3CostRateTableNo,
      '''' w1PayRateMeth,'''' w2PayRateMeth,'''' w3PayRateMeth,'''' w1PayRateTableNo,'''' w2PayRateTableNo,'''' w3PayRateTableNo,'''' w1Locale,'''' w1Locale,'''' w1Locale,
      '''' w1LineItemApproval,'''' w2LineItemApproval,'''' w3LineItemApproval,'''' w1LineItemApprovalEK,'''' w2LineItemApprovalEK,'''' w3LineItemApprovalEK,
      '''' opp_name,'''' opp_number,'''' opp_prproposalwbs1,'''' opp_prproposalname,'''' opp_org,'''' opp_orgname,'''' opp_clname,'''' opp_contactname,'''' opp_projmgrname,'''' opp_principalname,'''' opp_supervisorname,
      0 opp_revenue,'''' opp_probability,0 opp_weightedrevenue,NULL opp_opendate,NULL opp_closedate,'''' opp_daysopen,'''' opp_stage,'''' opp_type,'''' opp_source,'''' opp_status,NULL opp_eststartdate,NULL opp_estcompletiondate,
      '''' opp_address1,'''' opp_address2,'''' opp_address3,'''' opp_city,'''' opp_state,'''' opp_zip,'''' opp_country,'''' opp_createuser,NULL opp_createdate,'''' opp_moduser,NULL opp_moddate,'''' opp_oppdesc 
FROM PR As LEVEL3 LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1=ProjectCustomTabFields.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFields.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFields.WBS3 , PR AS LEVEL2 , PR  LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes2 ON substring(PR.Org, 4, 2) = groupCFGOrgCodes2.Code and groupCFGOrgCodes2.orgLevel = 2 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3, ##RWL_' + @SessionId + ' , LedgerMisc Left Join CA ON LedgerMisc.Account = CA.Account  Left Join CFGAutoPosting on CFGAutoPosting.Company = /*N*/@CompanyId 
WHERE ((LedgerMisc.Wbs1 = LEVEL3.Wbs1 AND LedgerMisc.Wbs2 = LEVEL3.Wbs2 AND LedgerMisc.Wbs3 = LEVEL3.Wbs3) 
 and LEVEL3.WBS1 not like ''ZZ%''
 AND (LedgerMisc.Wbs1 = LEVEL2.Wbs1 AND LedgerMisc.Wbs2 = LEVEL2.wbs2)
 AND (LedgerMisc.Wbs1 = PR.Wbs1) 
 AND (PR.Wbs2 = /*N*/'' '' AND PR.Wbs3 = /*N*/'' '' AND LEVEL2.Wbs3 = /*N*/'' '')) AND Period <= @Period AND ((CFGAutoPosting.UnbilledServices = LedgerMISC.Account And LedgerMISC.GainsAndLossesType = ''U'')  OR (CFGAutoPosting.UnbilledServices1 = LedgerMISC.Account And LedgerMISC.GainsAndLossesType = ''U'')  OR (CFGAutoPosting.UnbilledServices2 = LedgerMISC.Account And LedgerMISC.GainsAndLossesType = ''U'')  OR (CFGAutoPosting.UnbilledServices3 = LedgerMISC.Account And LedgerMISC.GainsAndLossesType = ''U'')  OR (CFGAutoPosting.UnbilledServices4 = LedgerMISC.Account And LedgerMISC.GainsAndLossesType = ''U'')  OR (CFGAutoPosting.UnbilledServices5 = LedgerMISC.Account And LedgerMISC.GainsAndLossesType = ''U''))  AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + ''' 
       AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 
       AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/'''' 
GROUP BY IsNull(substring(PR.Org, 4, 2), ''''), IsNull(substring(PR.Org, 7, 3), '''')

/***Labor Budgets***/
UNION ALL 
SELECT IsNull(substring(PR.Org, 4, 2), '''') As group1, Min(groupCFGOrgCodes2.Label) As groupDesc1, IsNull(substring(PR.Org, 7, 3), '''') As group2, Min(groupCFGOrgCodes3.Label) As groupDesc2, ''1'' AS recordType,'''' subType, 
      0 AS hrsCur, 0 hrsCurLabReg,0 hrsCurLabOvt,0 AS hrsYTD, 0 hrsYTDLabReg,0 hrsYTDLabOvt,0 AS hrsJTD, 0 hrsJTDLabReg,0 hrsJTDLabOvt,0 AS hrsCustom, 0 hrsCustomLabReg,0 hrsCustomLabOvt,
      sum(Hrsbud) AS hrsBud, sum(coalesce(nullif(PR.PctComp, 0),nullif(PR.LabPctComp, 0),nullif(LEVEL2.PctComp, 0),nullif(LEVEL2.LabPctComp, 0),nullif(LEVEL3.PctComp, 0),nullif(LEVEL3.LabPctComp, 0),nullif(LB.Pct, 0),0) * IsNull(HrsBud, 0)) AS hrsPctComp_c,
      sum(coalesce(nullif(PR.PctComp, 0),nullif(PR.LabPctComp, 0),nullif(LEVEL2.PctComp, 0),nullif(LEVEL2.LabPctComp, 0),nullif(LEVEL3.PctComp, 0),nullif(LEVEL3.LabPctComp, 0),nullif(LB.Pct, 0),0) * IsNull(HrsBud, 0)) AS hrsPctComp_b,
      sum(ETCHrs) AS hrsETC, sum(EACHrs) AS hrsEAC, 0 amtCurLabReg_c,0 amtCurLabReg_b,0 amtCurLabOvt_c,0 amtCurLabOvt_b,0 amtYTDLabReg_c,0 amtYTDLabReg_b,0 amtYTDLabOvt_c,0 amtYTDLabOvt_b,0 amtJTDLabReg_c,0 amtJTDLabReg_b,0 amtJTDLabOvt_c,0 amtJTDLabOvt_b,0 amtCustomLabReg_c,0 amtCustomLabReg_b,0 amtCustomLabOvt_c,0 amtCustomLabOvt_b,0 AS amtCur_c,0 amtCur_b,0 AS amtYTD_c,0 amtYTD_b,0 AS amtJTD_c,0 amtJTD_b,0 AS amtCustom_c,0 amtCustom_b,
      sum(AmtBud) AS amtBud_c, sum(BillBud) AS amtBud_b, sum(EACAmt) AS amtEAC_c, sum(BillEACAmt) AS amtEAC_b, sum(ETCAmt) AS amtETC_c, sum(BillETCAmt) AS amtETC_b, 
      sum(coalesce(nullif(PR.PctComp, 0),nullif(PR.LabPctComp, 0),nullif(LEVEL2.PctComp, 0),nullif(LEVEL2.LabPctComp, 0),nullif(LEVEL3.PctComp, 0),nullif(LEVEL3.LabPctComp, 0),nullif(LB.Pct, 0),0) * IsNull(amtBud, 0)) AS amtPctComp_c,
      sum(coalesce(nullif(PR.PctComp, 0),nullif(PR.LabPctComp, 0),nullif(LEVEL2.PctComp, 0),nullif(LEVEL2.LabPctComp, 0),nullif(LEVEL3.PctComp, 0),nullif(LEVEL3.LabPctComp, 0),nullif(LB.Pct, 0),0) * IsNull(billBud, 0)) AS amtPctComp_b,
      sum(coalesce(nullif(PR.PctComp, 0),nullif(PR.LabPctComp, 0),nullif(LEVEL2.PctComp, 0),nullif(LEVEL2.LabPctComp, 0),nullif(LEVEL3.PctComp, 0),nullif(LEVEL3.LabPctComp, 0),nullif(LB.Pct, 0),0) * IsNull(amtBud, 0)) AS amtPctCompLab_c,
      sum(coalesce(nullif(PR.PctComp, 0),nullif(PR.LabPctComp, 0),nullif(LEVEL2.PctComp, 0),nullif(LEVEL2.LabPctComp, 0),nullif(LEVEL3.PctComp, 0),nullif(LEVEL3.LabPctComp, 0),nullif(LB.Pct, 0),0) * IsNull(billBud, 0)) AS amtPctCompLab_b,
      0.00 as amtPctCompExp_c,0.00 as amtPctCompExp_b,
      0 AS arCur_c,0 AS arCur_b, 0 AS arYTD_c,0 AS arYTD_b, 0 AS arJTD_c,0 AS arJTD_b, 0 AS arCustom_c,0 AS arCustom_b, 
      0 AS billCur_c,0 AS billCur_b, 0 AS billYTD_c,0 AS billYTD_b, 0 AS billJTD_c,0 AS billJTD_b, 0 AS billCustom_c,0 AS billCustom_b, 
      0 AS recdCur_c,0 AS recdCur_b, 0 AS recdYTD_c,0 AS recdYTD_b, 0 AS recdJTD_c,0 AS recdJTD_b, 0 AS recdCustom_c,0 AS recdCustom_b, 
      0 AS directCur_c,0 directCur_b, 0 AS directYTD_c,0 directYTD_b, 0 AS directJTD_c,0 directJTD_b, 0 AS directCustom_c,0 directCustom_b, 
      0 AS reimbCur_c,0 reimbCur_b, 0 AS reimbYTD_c,0 reimbYTD_b, 0 AS reimbJTD_c,0 reimbJTD_b, 0 AS reimbCustom_c,0 reimbCustom_b, 
      0 AS revCur_c,0 AS revCur_b, 0 AS revYTD_c,0 AS revYTD_b, 0 AS revJTD_c,0 AS revJTD_b, 0 AS revCustom_c,0 AS revCustom_b, 
      0.00 AS ubGLCur_c, 0.00 AS ubGLYTD_c, 0.00 AS ubGLJTD_c, 0.00 AS ubGLCustom_c, 0 althrsBud,0 altamtBud_c,0 altamtBud_b,
       0.0 poAmt_c,0.00 poAmt_b,
      0.0 rppctcmpcalc, 0.00 targetMult_c,0.00 targetMult_b,0.00 rpfee_c,0.00 rpfee_b,0.00 rpconsultfee_c,0.00 rpconsultfee_b,0.00 rpreimballow_c,0.00 rpreimballow_b,
     '''' AS wbs1customCol1,'''' AS wbs1customCol2,'''' AS wbs1customCol3,'''' AS wbs1customCol4,'''' AS wbs1customCol5,
     '''' AS wbs2customCol1,'''' AS wbs2customCol2,'''' AS wbs2customCol3,'''' AS wbs2customCol4,'''' AS wbs2customCol5,
     '''' AS wbs3customCol1,'''' AS wbs3customCol2,'''' AS wbs3customCol3,'''' AS wbs3customCol4,'''' AS wbs3customCol5,
      '''' As currencyCodeProj,'''' As currencyCodeBill,'''' As currencyCodeFunct,'''' as currencyCodePres_Cost, '''' as currencyCodePres_Bill, '''' as currencyCodePres_ARFee, '''' as currencyCode_Opportunity, 
      0 As currencyCodeProjCount, 0 As currencyCodeBillCount, 0 As currencyCodeFunctCount, 0 As currencyCodePresCount_Cost, 0 As currencyCodePresCount_Bill, 0 As currencyCodePresCount_ARFee, 0 As currencyCodeCount_Opportunity, 
       min(PR.WBS1) WBS1,min(LEVEL2.WBS2) WBS2,min(LEVEL3.WBS3) WBS3,'''' w1Name,'''' w2Name,'''' w3Name,
      '''' ChargeType,max(PR.ChargeType) ChargeTypeCode,min(PR.SubLevel) SubLevel1,min(LEVEL2.SubLevel) SubLevel2,min(LEVEL3.SubLevel) SubLevel3,
      '''' w1principal,'''' w2principal,'''' w3principal,'''' w1prinName,'''' w2prinName,'''' w3prinName,'''' w1projMgr,'''' w2projMgr,'''' w3projMgr,'''' w1prgName,'''' w2prgName,'''' w3prgName,'''' w1supervisor,'''' w2supervisor,'''' w3supervisor,'''' w1supName,'''' w2supName,'''' w3supName,
      '''' PRAddress1,'''' PRAddress2,'''' PRAddress3, '''' PRCity,'''' PRState,'''' PRZip,'''' PRCounty,'''' PRCountry,'''' PRPhone,'''' PRFax,'''' PREmail,
      '''' ClientID,'''' ClientName,'''' CLAddress,'''' CLAddress1,'''' CLAddress2,'''' CLAddress3,'''' CLAddress4,'''' CLCity,'''' CLState,'''' CLZip,'''' CLCountry,'''' CLPhone,'''' CLFax,'''' CLEMail,
      '''' w1Org,'''' w2Org,'''' w3Org,'''' w1OrgName,'''' w2OrgName,'''' w3OrgName,
      '''' as w1TLProjectName, '''' as w2TLProjectName, '''' as w3TLProjectName, '''' ServiceProfileCode,'''' ServiceProfileDesc,
      0 mult,0 mult_sum,0 fee_c,0 fee_b,
      0 consultFee_c,0 consultFee_b,0 reimbAllow_c,0 reimbAllow_b,
      0.0 feeDirectLabor, 0.0 feeDirectExpense, 0.0 reimbAllowExpense, 0.0 reimbAllowConsultant,
      0 contractfee, 0 contractconsultFee, 0 contractreimbAllow,
      0.0 contractFeeDirectLabor, 0.0 contractFeeDirectExpense, 0.0 contractReimbAllowExpense, 0.0 contractReimbAllowConsultant,
      0 BudOHRate,'''' w1Status,'''' w2Status,'''' w3Status,'''' w1StatusDesc,'''' w2StatusDesc,'''' w3StatusDesc,'''' wbs1RevType,'''' wbs2RevType, '''' wbs3RevType,'''' wbs1RevTypeDesc,'''' wbs2RevTypeDesc,'''' wbs3RevTypeDesc,'''' UnitTable,
      NULL w1StartDate,NULL w2StartDate,NULL w3StartDate,NULL w1EndDate,NULL w2EndDate,NULL w3EndDate,
      0.00 w1PctComp,0.00 w2PctComp,0.00 w3PctComp,0.00 w1LabPctComp,0.00 w2LabPctComp,0.00 w3LabPctComp,0.00 w1ExpPctComp,0.00 w2ExpPctComp,0.00 w3ExpPctComp,
      '''' BillByDefault,'''' BillableWarning,'''' BudgetedFlag,'''' BudgetSource,'''' BudgetLevel,'''' BudgetedLevels,
      '''' XCharge,'''' XChargeMethod,0 XChargeMult,'''' ContactID,'''' ContactFirstName,'''' ContactLastName,'''' CLBillingAddr,'''' w1LongName,'''' w2LongName,'''' w3LongName,
      '''' CLBAddress1,'''' CLBAddress2,'''' CLBAddress3,'''' CLBAddress4,'''' CLBCity,'''' CLBState,'''' CLBZip,'''' CLBCountry,'''' CLBPhone,'''' CLBFax,'''' CLBEMail,
      '''' w1FederalInd,'''' w2FederalInd,'''' w3FederalInd,'''' w1ProjectType,'''' w2ProjectType,'''' w3ProjectType,'''' w1projectTypeDesc,'''' w2projectTypeDesc,'''' w3projectTypeDesc,
      '''' w1Responsibility,'''' w2Responsibility,'''' w3Responsibility,'''' w1ResponsibilityDesc,'''' w2ResponsibilityDesc,'''' w3ResponsibilityDesc,'''' w1Referable,'''' w2Referable,'''' w3Referable,
      NULL w1EstCompletionDate,NULL w2EstCompletionDate,NULL w3EstCompletionDate,NULL w1ActCompletionDate,NULL w2ActCompletionDate,NULL w3ActCompletionDate,NULL w1ContractDate,NULL w2ContractDate,NULL w3ContractDate,
      NULL w1ConstComplDate,NULL w2ConstComplDate,NULL w3ConstComplDate,NULL w1ProfServicesComplDate,NULL w2ProfServicesComplDate,NULL w3ProfServicesComplDate,NULL w1BidDate,NULL w2BidDate,NULL w3BidDate,
      '''' w1ComplDateComment,'''' w2ComplDateComment,'''' w3ComplDateComment,0 w1FirmCost,0 w2FirmCost,0 w3FirmCost,'''' w1FirmCostComment,'''' w2FirmCostComment,'''' w3FirmCostComment,
      0 w1TotalProjectCost,0 w2TotalProjectCost,0 w3TotalProjectCost,'''' w1TotalCostComment,'''' w2TotalCostComment,'''' w3TotalCostComment,
      '''' ClientConfidential,'''' ClientAlias,'''' BillingClientID,'''' BillClientName,'''' BillingContactID,'''' billContactLastName,'''' billContactFirstName,'''' ProposalWBS1,
      '''' w1CreateUser,'''' w2CreateUser,'''' w3CreateUser,NULL w1CreateDate,NULL w2CreateDate,NULL w3CreateDate,
      '''' w1ModUser,'''' w2ModUser,'''' w3ModUser,NULL w1ModDate,NULL w2ModDate,NULL w3ModDate,
      '''' w1CostRateMeth,'''' w2CostRateMeth,'''' w3CostRateMeth,'''' w1CostRateTableNo,'''' w3CostRateTableNo,'''' w3CostRateTableNo,
      '''' w1PayRateMeth,'''' w2PayRateMeth,'''' w3PayRateMeth,'''' w1PayRateTableNo,'''' w2PayRateTableNo,'''' w3PayRateTableNo,'''' w1Locale,'''' w1Locale,'''' w1Locale,
      '''' w1LineItemApproval,'''' w2LineItemApproval,'''' w3LineItemApproval,'''' w1LineItemApprovalEK,'''' w2LineItemApprovalEK,'''' w3LineItemApprovalEK,
      '''' opp_name,'''' opp_number,'''' opp_prproposalwbs1,'''' opp_prproposalname,'''' opp_org,'''' opp_orgname,'''' opp_clname,'''' opp_contactname,'''' opp_projmgrname,'''' opp_principalname,'''' opp_supervisorname,
      0 opp_revenue,'''' opp_probability,0 opp_weightedrevenue,NULL opp_opendate,NULL opp_closedate,'''' opp_daysopen,'''' opp_stage,'''' opp_type,'''' opp_source,'''' opp_status,NULL opp_eststartdate,NULL opp_estcompletiondate,
      '''' opp_address1,'''' opp_address2,'''' opp_address3,'''' opp_city,'''' opp_state,'''' opp_zip,'''' opp_country,'''' opp_createuser,NULL opp_createdate,'''' opp_moduser,NULL opp_moddate,'''' opp_oppdesc 
FROM PR As LEVEL3 LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1=ProjectCustomTabFields.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFields.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFields.WBS3 , PR AS LEVEL2 , PR  LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes2 ON substring(PR.Org, 4, 2) = groupCFGOrgCodes2.Code and groupCFGOrgCodes2.orgLevel = 2 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3, ##RWL_' + @SessionId + ' , LB  
WHERE ((LB.Wbs1 = LEVEL3.Wbs1 AND LB.Wbs2 = LEVEL3.Wbs2 AND LB.Wbs3 = LEVEL3.Wbs3) 
 and LEVEL3.WBS1 not like ''ZZ%''
 AND (LB.Wbs1 = LEVEL2.Wbs1 AND LB.Wbs2 = LEVEL2.wbs2)
 AND (LB.Wbs1 = PR.Wbs1) 
 AND (PR.Wbs2 = /*N*/'' '' AND PR.Wbs3 = /*N*/'' '' AND LEVEL2.Wbs3 = /*N*/'' '')) AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + ''' 
       AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 
       AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/'''' 
GROUP BY IsNull(substring(PR.Org, 4, 2), ''''), IsNull(substring(PR.Org, 7, 3), '''')

/***OH Budgets***/
UNION ALL 
SELECT IsNull(substring(PR.Org, 4, 2), '''') As group1, Min(groupCFGOrgCodes2.Label) As groupDesc1, IsNull(substring(PR.Org, 7, 3), '''') As group2, Min(groupCFGOrgCodes3.Label) As groupDesc2, ''2'' AS recordType,'''' subType, 
      0 AS hrsCur,0 hrsCurLabReg,0 hrsCurLabOvt,0 AS hrsYTD,0 hrsYTDLabReg,0 hrsYTDLabOvt,0 AS hrsJTD,0 hrsJTDLabReg,0 hrsJTDLabOvt,0 AS hrsCustom,0 hrsCustomLabReg,0 hrsCustomLabOvt,0 AS hrsBud, 
      0 AS hrsPctComp_c,0 as hrsPctComp_b, 0 AS hrsETC, 0 AS hrsEAC, 
      0 amtCurLabReg_c,0 amtCurLabReg_b,0 amtCurLabOvt_c,0 amtCurLabOvt_b,0 amtYTDLabReg_c,0 amtYTDLabReg_b,0 amtYTDLabOvt_c,0 amtYTDLabOvt_b,0 amtJTDLabReg_c,0 amtJTDLabReg_b,0 amtJTDLabOvt_c,0 amtJTDLabOvt_b,0 amtCustomLabReg_c,0 amtCustomLabReg_b,0 amtCustomLabOvt_c,0 amtCustomLabOvt_b,
      0 AS amtCur_c,0 amtCur_b, 0 AS amtYTD_c,0 amtYTD_b, 0 AS amtJTD_c,0 amtJTD_b, 0 AS amtCustom_c,0 amtCustom_b, 
sum(CASE WHEN IsNull(Level3.BudOHRate,0) <> 0     THEN ((Level3.BudOHRate / 100) * amtBud) ELSE (1.875 * amtBud) END) AS amtBud_c,0 amtBud_b, 0 amtEAC_c,0 amtEAC_b,0 amtETC_c,0 amtETC_b, 
      sum(coalesce(nullif(PR.PctComp, 0),nullif(PR.LabPctComp, 0),nullif(LEVEL2.PctComp, 0),nullif(LEVEL2.LabPctComp, 0),nullif(LEVEL3.PctComp, 0),nullif(LEVEL3.LabPctComp, 0),nullif(LB.Pct, 0),0)  * IsNull(amtBud, 0) * CASE WHEN Level3.BudOHRate = 0 THEN 1.875 ELSE Level3.BudOHRate/100 END )  AS amtPctComp_c, 0 amtPctComp_b,
      sum(coalesce(nullif(PR.PctComp, 0),nullif(PR.LabPctComp, 0),nullif(LEVEL2.PctComp, 0),nullif(LEVEL2.LabPctComp, 0),nullif(LEVEL3.PctComp, 0),nullif(LEVEL3.LabPctComp, 0),nullif(LB.Pct, 0),0)  * IsNull(amtBud, 0) * CASE WHEN Level3.BudOHRate = 0 THEN 1.875 ELSE Level3.BudOHRate/100 END )  AS amtPctCompLab_c, 
      0 amtPctCompLab_b,0 amtPctCompExp_c,0 amtPctCompExp_b,
      0 AS arCur_c,0 AS arCur_b, 0 AS arYTD_c,0 AS arYTD_b, 0 AS arJTD_c,0 AS arJTD_b, 0 AS arCustom_c,0 AS arCustom_b, 
      0 AS billCur_c,0 AS billCur_b, 0 AS billYTD_c,0 AS billYTD_b, 0 AS billJTD_c,0 AS billJTD_b, 0 AS billCustom_c,0 AS billCustom_b, 
      0 AS recdCur_c,0 AS recdCur_b, 0 AS recdYTD_c,0 AS recdYTD_b, 0 AS recdJTD_c,0 AS recdJTD_b, 0 AS recdCustom_c,0 AS recdCustom_b, 
      0 AS directCur_c,0 directCur_b, 0 AS directYTD_c,0 directYTD_b, 0 AS directJTD_c,0 directJTD_b, 0 AS directCustom_c,0 directCustom_b, 
      0 AS reimbCur_c,0 reimbCur_b, 0 AS reimbYTD_c,0 reimbYTD_b, 0 AS reimbJTD_c,0 reimbJTD_b, 0 AS reimbCustom_c,0 reimbCustom_b, 
      0 AS revCur_c,0 AS revCur_b, 0 AS revYTD_c,0 AS revYTD_b, 0 AS revJTD_c,0 AS revJTD_b, 0 AS revCustom_c,0 AS revCustom_b, 
      0.00 AS ubGLCur_c, 0.00 AS ubGLYTD_c, 0.00 AS ubGLJTD_c, 0.00 AS ubGLCustom_c, 0 althrsBud,0 altamtBud_c,0 altamtBud_b,
       0.0 poAmt_c,0.00 poAmt_b,
      0.0 rppctcmpcalc, 0.00 targetMult_c,0.00 targetMult_b,0.00 rpfee_c,0.00 rpfee_b,0.00 rpconsultfee_c,0.00 rpconsultfee_b,0.00 rpreimballow_c,0.00 rpreimballow_b,
     '''' AS wbs1customCol1,'''' AS wbs1customCol2,'''' AS wbs1customCol3,'''' AS wbs1customCol4,'''' AS wbs1customCol5,
     '''' AS wbs2customCol1,'''' AS wbs2customCol2,'''' AS wbs2customCol3,'''' AS wbs2customCol4,'''' AS wbs2customCol5,
     '''' AS wbs3customCol1,'''' AS wbs3customCol2,'''' AS wbs3customCol3,'''' AS wbs3customCol4,'''' AS wbs3customCol5,
      '''' As currencyCodeProj,'''' As currencyCodeBill,'''' As currencyCodeFunct,'''' as currencyCodePres_Cost, '''' as currencyCodePres_Bill, '''' as currencyCodePres_ARFee, '''' as currencyCode_Opportunity, 
      0 As currencyCodeProjCount, 0 As currencyCodeBillCount, 0 As currencyCodeFunctCount, 0 As currencyCodePresCount_Cost, 0 As currencyCodePresCount_Bill, 0 As currencyCodePresCount_ARFee, 0 As currencyCodeCount_Opportunity, 
       min(PR.WBS1) WBS1,min(LEVEL2.WBS2) WBS2,min(LEVEL3.WBS3) WBS3,'''' w1Name,'''' w2Name,'''' w3Name,
      '''' ChargeType,max(PR.ChargeType) ChargeTypeCode,min(PR.SubLevel) SubLevel1,min(LEVEL2.SubLevel) SubLevel2,min(LEVEL3.SubLevel) SubLevel3,
      '''' w1principal,'''' w2principal,'''' w3principal,'''' w1prinName,'''' w2prinName,'''' w3prinName,'''' w1projMgr,'''' w2projMgr,'''' w3projMgr,'''' w1prgName,'''' w2prgName,'''' w3prgName,'''' w1supervisor,'''' w2supervisor,'''' w3supervisor,'''' w1supName,'''' w2supName,'''' w3supName,
      '''' PRAddress1,'''' PRAddress2,'''' PRAddress3, '''' PRCity,'''' PRState,'''' PRZip,'''' PRCounty,'''' PRCountry,'''' PRPhone,'''' PRFax,'''' PREmail,
      '''' ClientID,'''' ClientName,'''' CLAddress,'''' CLAddress1,'''' CLAddress2,'''' CLAddress3,'''' CLAddress4,'''' CLCity,'''' CLState,'''' CLZip,'''' CLCountry,'''' CLPhone,'''' CLFax,'''' CLEMail,
      '''' w1Org,'''' w2Org,'''' w3Org,'''' w1OrgName,'''' w2OrgName,'''' w3OrgName,
      '''' as w1TLProjectName, '''' as w2TLProjectName, '''' as w3TLProjectName, '''' ServiceProfileCode,'''' ServiceProfileDesc,
      0 mult,0 mult_sum,0 fee_c,0 fee_b,
      0 consultFee_c,0 consultFee_b,0 reimbAllow_c,0 reimbAllow_b,
      0.0 feeDirectLabor, 0.0 feeDirectExpense, 0.0 reimbAllowExpense, 0.0 reimbAllowConsultant,
      0 contractfee, 0 contractconsultFee, 0 contractreimbAllow,
      0.0 contractFeeDirectLabor, 0.0 contractFeeDirectExpense, 0.0 contractReimbAllowExpense, 0.0 contractReimbAllowConsultant,
      0 BudOHRate,'''' w1Status,'''' w2Status,'''' w3Status,'''' w1StatusDesc,'''' w2StatusDesc,'''' w3StatusDesc,'''' wbs1RevType,'''' wbs2RevType, '''' wbs3RevType,'''' wbs1RevTypeDesc,'''' wbs2RevTypeDesc,'''' wbs3RevTypeDesc,'''' UnitTable,
      NULL w1StartDate,NULL w2StartDate,NULL w3StartDate,NULL w1EndDate,NULL w2EndDate,NULL w3EndDate,
      0.00 w1PctComp,0.00 w2PctComp,0.00 w3PctComp,0.00 w1LabPctComp,0.00 w2LabPctComp,0.00 w3LabPctComp,0.00 w1ExpPctComp,0.00 w2ExpPctComp,0.00 w3ExpPctComp,
      '''' BillByDefault,'''' BillableWarning,'''' BudgetedFlag,'''' BudgetSource,'''' BudgetLevel,'''' BudgetedLevels,
      '''' XCharge,'''' XChargeMethod,0 XChargeMult,'''' ContactID,'''' ContactFirstName,'''' ContactLastName,'''' CLBillingAddr,'''' w1LongName,'''' w2LongName,'''' w3LongName,
      '''' CLBAddress1,'''' CLBAddress2,'''' CLBAddress3,'''' CLBAddress4,'''' CLBCity,'''' CLBState,'''' CLBZip,'''' CLBCountry,'''' CLBPhone,'''' CLBFax,'''' CLBEMail,
      '''' w1FederalInd,'''' w2FederalInd,'''' w3FederalInd,'''' w1ProjectType,'''' w2ProjectType,'''' w3ProjectType,'''' w1projectTypeDesc,'''' w2projectTypeDesc,'''' w3projectTypeDesc,
      '''' w1Responsibility,'''' w2Responsibility,'''' w3Responsibility,'''' w1ResponsibilityDesc,'''' w2ResponsibilityDesc,'''' w3ResponsibilityDesc,'''' w1Referable,'''' w2Referable,'''' w3Referable,
      NULL w1EstCompletionDate,NULL w2EstCompletionDate,NULL w3EstCompletionDate,NULL w1ActCompletionDate,NULL w2ActCompletionDate,NULL w3ActCompletionDate,NULL w1ContractDate,NULL w2ContractDate,NULL w3ContractDate,
      NULL w1ConstComplDate,NULL w2ConstComplDate,NULL w3ConstComplDate,NULL w1ProfServicesComplDate,NULL w2ProfServicesComplDate,NULL w3ProfServicesComplDate,NULL w1BidDate,NULL w2BidDate,NULL w3BidDate,
      '''' w1ComplDateComment,'''' w2ComplDateComment,'''' w3ComplDateComment,0 w1FirmCost,0 w2FirmCost,0 w3FirmCost,'''' w1FirmCostComment,'''' w2FirmCostComment,'''' w3FirmCostComment,
      0 w1TotalProjectCost,0 w2TotalProjectCost,0 w3TotalProjectCost,'''' w1TotalCostComment,'''' w2TotalCostComment,'''' w3TotalCostComment,
      '''' ClientConfidential,'''' ClientAlias,'''' BillingClientID,'''' BillClientName,'''' BillingContactID,'''' billContactLastName,'''' billContactFirstName,'''' ProposalWBS1,
      '''' w1CreateUser,'''' w2CreateUser,'''' w3CreateUser,NULL w1CreateDate,NULL w2CreateDate,NULL w3CreateDate,
      '''' w1ModUser,'''' w2ModUser,'''' w3ModUser,NULL w1ModDate,NULL w2ModDate,NULL w3ModDate,
      '''' w1CostRateMeth,'''' w2CostRateMeth,'''' w3CostRateMeth,'''' w1CostRateTableNo,'''' w3CostRateTableNo,'''' w3CostRateTableNo,
      '''' w1PayRateMeth,'''' w2PayRateMeth,'''' w3PayRateMeth,'''' w1PayRateTableNo,'''' w2PayRateTableNo,'''' w3PayRateTableNo,'''' w1Locale,'''' w1Locale,'''' w1Locale,
      '''' w1LineItemApproval,'''' w2LineItemApproval,'''' w3LineItemApproval,'''' w1LineItemApprovalEK,'''' w2LineItemApprovalEK,'''' w3LineItemApprovalEK,
      '''' opp_name,'''' opp_number,'''' opp_prproposalwbs1,'''' opp_prproposalname,'''' opp_org,'''' opp_orgname,'''' opp_clname,'''' opp_contactname,'''' opp_projmgrname,'''' opp_principalname,'''' opp_supervisorname,
      0 opp_revenue,'''' opp_probability,0 opp_weightedrevenue,NULL opp_opendate,NULL opp_closedate,'''' opp_daysopen,'''' opp_stage,'''' opp_type,'''' opp_source,'''' opp_status,NULL opp_eststartdate,NULL opp_estcompletiondate,
      '''' opp_address1,'''' opp_address2,'''' opp_address3,'''' opp_city,'''' opp_state,'''' opp_zip,'''' opp_country,'''' opp_createuser,NULL opp_createdate,'''' opp_moduser,NULL opp_moddate,'''' opp_oppdesc 
FROM PR As LEVEL3 LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1=ProjectCustomTabFields.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFields.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFields.WBS3 , PR AS LEVEL2 , PR  LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes2 ON substring(PR.Org, 4, 2) = groupCFGOrgCodes2.Code and groupCFGOrgCodes2.orgLevel = 2 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3, ##RWL_' + @SessionId + ' , LB 
WHERE ((LB.Wbs1 = LEVEL3.Wbs1 AND LB.Wbs2 = LEVEL3.Wbs2 AND LB.Wbs3 = LEVEL3.Wbs3) 
 and LEVEL3.WBS1 not like ''ZZ%''
 AND (LB.Wbs1 = LEVEL2.Wbs1 AND LB.Wbs2 = LEVEL2.wbs2)
 AND (LB.Wbs1 = PR.Wbs1) 
 AND (PR.Wbs2 = /*N*/'' '' AND PR.Wbs3 = /*N*/'' '' AND LEVEL2.Wbs3 = /*N*/'' '')) AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + ''' 
       AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 
       AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/''''  AND PR.ChargeType = ''R''
GROUP BY IsNull(substring(PR.Org, 4, 2), ''''), IsNull(substring(PR.Org, 7, 3), '''')

/***Direct and Reimbursable Budgets***/
UNION ALL 
SELECT IsNull(substring(PR.Org, 4, 2), '''') As group1, Min(groupCFGOrgCodes2.Label) As groupDesc1, IsNull(substring(PR.Org, 7, 3), '''') As group2, Min(groupCFGOrgCodes3.Label) As groupDesc2, 
       CASE WHEN CA.Type < 7 THEN ''5''  WHEN CA.Type >= 7 AND CA.Type < 9 THEN ''3''  WHEN CA.Type >= 9 AND CA.Type < 10 THEN ''6''  ELSE ''7'' END As recordType,  min(CASE WHEN (CA.Type = 6) OR (CA.Type = 8) THEN ''0'' ELSE ''1'' END) subType,
      0 AS hrsCur,0 hrsCurLabReg,0 hrsCurLabOvt,0 AS hrsYTD,0 hrsYTDLabReg,0 hrsYTDLabOvt,0 AS hrsJTD,0 hrsJTDLabReg,0 hrsJTDLabOvt,0 AS hrsCustom,0 hrsCustomLabReg,0 hrsCustomLabOvt,0 AS hrsBud, 
      0 AS hrsPctComp_c,0 as hrsPctComp_b, 0 AS hrsETC, 0 AS hrsEAC, 
      0 amtCurLabReg_c,0 amtCurLabReg_b,0 amtCurLabOvt_c,0 amtCurLabOvt_b,0 amtYTDLabReg_c,0 amtYTDLabReg_b,0 amtYTDLabOvt_c,0 amtYTDLabOvt_b,0 amtJTDLabReg_c,0 amtJTDLabReg_b,0 amtJTDLabOvt_c,0 amtJTDLabOvt_b,0 amtCustomLabReg_c,0 amtCustomLabReg_b,0 amtCustomLabOvt_c,0 amtCustomLabOvt_b,
      0 AS amtCur_c,0 amtCur_b, 0 AS amtYTD_c,0 amtYTD_b, 0 AS amtJTD_c,0 amtJTD_b, 0 AS amtCustom_c,0 amtCustom_b, 
sum(AmtBud) AS amtBud_c,sum(BillBud) AS amtBud_b, sum(EACAmt) AS amtEAC_c,sum(BillEACAmt) As amtEAC_b, sum(ETCAmt) AS amtETC_c,sum(BillETCAmt) As amtETC_b, 
      sum(coalesce(nullif(PR.PctComp, 0),nullif(PR.ExpPctComp, 0),nullif(LEVEL2.PctComp, 0),nullif(LEVEL2.ExpPctComp, 0),nullif(LEVEL3.PctComp, 0) ,nullif(LEVEL3.ExpPctComp, 0),nullif(EB.Pct, 0),0) * IsNull(amtBud, 0)) AS amtPctComp_c, 
      sum(coalesce(nullif(PR.PctComp, 0),nullif(PR.ExpPctComp, 0),nullif(LEVEL2.PctComp, 0),nullif(LEVEL2.ExpPctComp, 0),nullif(LEVEL3.PctComp, 0) ,nullif(LEVEL3.ExpPctComp, 0),nullif(EB.Pct, 0),0) * IsNull(billBud, 0)) AS amtPctComp_b, 0.00 as amtPctCompLab_c,0.00 amtPctCompLab_b,
      sum(coalesce(nullif(PR.PctComp, 0),nullif(PR.ExpPctComp, 0),nullif(LEVEL2.PctComp, 0),nullif(LEVEL2.ExpPctComp, 0),nullif(LEVEL3.PctComp, 0) ,nullif(LEVEL3.ExpPctComp, 0),nullif(EB.Pct, 0),0) * IsNull(amtBud, 0)) AS amtPctCompExp_c, 
      sum(coalesce(nullif(PR.PctComp, 0),nullif(PR.ExpPctComp, 0),nullif(LEVEL2.PctComp, 0),nullif(LEVEL2.ExpPctComp, 0),nullif(LEVEL3.PctComp, 0) ,nullif(LEVEL3.ExpPctComp, 0),nullif(EB.Pct, 0),0) * IsNull(billBud, 0)) AS amtPctCompExp_b, 
      0 AS arCur_c,0 AS arCur_b, 0 AS arYTD_c,0 AS arYTD_b, 0 AS arJTD_c,0 AS arJTD_b, 0 AS arCustom_c,0 AS arCustom_b, 
      0 AS billCur_c,0 AS billCur_b, 0 AS billYTD_c,0 AS billYTD_b, 0 AS billJTD_c,0 AS billJTD_b, 0 AS billCustom_c,0 AS billCustom_b, 
      0 AS recdCur_c,0 AS recdCur_b, 0 AS recdYTD_c,0 AS recdYTD_b, 0 AS recdJTD_c,0 AS recdJTD_b, 0 AS recdCustom_c,0 AS recdCustom_b, 
      0 AS directCur_c,0 directCur_b, 0 AS directYTD_c,0 directYTD_b, 0 AS directJTD_c,0 directJTD_b, 0 AS directCustom_c,0 directCustom_b, 
      0 AS reimbCur_c,0 reimbCur_b, 0 AS reimbYTD_c,0 reimbYTD_b, 0 AS reimbJTD_c,0 reimbJTD_b, 0 AS reimbCustom_c,0 reimbCustom_b, 
      0 AS revCur_c,0 AS revCur_b, 0 AS revYTD_c,0 AS revYTD_b, 0 AS revJTD_c,0 AS revJTD_b, 0 AS revCustom_c,0 AS revCustom_b, 
      0.00 AS ubGLCur_c, 0.00 AS ubGLYTD_c, 0.00 AS ubGLJTD_c, 0.00 AS ubGLCustom_c, 0 althrsBud,0 altamtBud_c,0 altamtBud_b, 0.0 poAmt_c,0.00 poAmt_b,
      0.0 rppctcmpcalc, 0.00 targetMult_c,0.00 targetMult_b,0.00 rpfee_c,0.00 rpfee_b,0.00 rpconsultfee_c,0.00 rpconsultfee_b,0.00 rpreimballow_c,0.00 rpreimballow_b,
     '''' AS wbs1customCol1,'''' AS wbs1customCol2,'''' AS wbs1customCol3,'''' AS wbs1customCol4,'''' AS wbs1customCol5,
     '''' AS wbs2customCol1,'''' AS wbs2customCol2,'''' AS wbs2customCol3,'''' AS wbs2customCol4,'''' AS wbs2customCol5,
     '''' AS wbs3customCol1,'''' AS wbs3customCol2,'''' AS wbs3customCol3,'''' AS wbs3customCol4,'''' AS wbs3customCol5,
      '''' As currencyCodeProj,'''' As currencyCodeBill,'''' As currencyCodeFunct,'''' as currencyCodePres_Cost, '''' as currencyCodePres_Bill, '''' as currencyCodePres_ARFee, '''' as currencyCode_Opportunity, 
      0 As currencyCodeProjCount, 0 As currencyCodeBillCount, 0 As currencyCodeFunctCount, 0 As currencyCodePresCount_Cost, 0 As currencyCodePresCount_Bill, 0 As currencyCodePresCount_ARFee, 0 As currencyCodeCount_Opportunity, 
       min(PR.WBS1) WBS1,min(LEVEL2.WBS2) WBS2,min(LEVEL3.WBS3) WBS3,'''' w1Name,'''' w2Name,'''' w3Name,
      '''' ChargeType,max(PR.ChargeType) ChargeTypeCode,min(PR.SubLevel) SubLevel1,min(LEVEL2.SubLevel) SubLevel2,min(LEVEL3.SubLevel) SubLevel3,
      '''' w1principal,'''' w2principal,'''' w3principal,'''' w1prinName,'''' w2prinName,'''' w3prinName,'''' w1projMgr,'''' w2projMgr,'''' w3projMgr,'''' w1prgName,'''' w2prgName,'''' w3prgName,'''' w1supervisor,'''' w2supervisor,'''' w3supervisor,'''' w1supName,'''' w2supName,'''' w3supName,
      '''' PRAddress1,'''' PRAddress2,'''' PRAddress3, '''' PRCity,'''' PRState,'''' PRZip,'''' PRCounty,'''' PRCountry,'''' PRPhone,'''' PRFax,'''' PREmail,
      '''' ClientID,'''' ClientName,'''' CLAddress,'''' CLAddress1,'''' CLAddress2,'''' CLAddress3,'''' CLAddress4,'''' CLCity,'''' CLState,'''' CLZip,'''' CLCountry,'''' CLPhone,'''' CLFax,'''' CLEMail,
      '''' w1Org,'''' w2Org,'''' w3Org,'''' w1OrgName,'''' w2OrgName,'''' w3OrgName,
      '''' as w1TLProjectName, '''' as w2TLProjectName, '''' as w3TLProjectName, '''' ServiceProfileCode,'''' ServiceProfileDesc,
      0 mult,0 mult_sum,0 fee_c,0 fee_b,
      0 consultFee_c,0 consultFee_b,0 reimbAllow_c,0 reimbAllow_b,
      0.0 feeDirectLabor, 0.0 feeDirectExpense, 0.0 reimbAllowExpense, 0.0 reimbAllowConsultant,
      0 contractfee, 0 contractconsultFee, 0 contractreimbAllow,
      0.0 contractFeeDirectLabor, 0.0 contractFeeDirectExpense, 0.0 contractReimbAllowExpense, 0.0 contractReimbAllowConsultant,
      0 BudOHRate,'''' w1Status,'''' w2Status,'''' w3Status,'''' w1StatusDesc,'''' w2StatusDesc,'''' w3StatusDesc,'''' wbs1RevType,'''' wbs2RevType, '''' wbs3RevType,'''' wbs1RevTypeDesc,'''' wbs2RevTypeDesc,'''' wbs3RevTypeDesc,'''' UnitTable,
      NULL w1StartDate,NULL w2StartDate,NULL w3StartDate,NULL w1EndDate,NULL w2EndDate,NULL w3EndDate,
      0.00 w1PctComp,0.00 w2PctComp,0.00 w3PctComp,0.00 w1LabPctComp,0.00 w2LabPctComp,0.00 w3LabPctComp,0.00 w1ExpPctComp,0.00 w2ExpPctComp,0.00 w3ExpPctComp,
      '''' BillByDefault,'''' BillableWarning,'''' BudgetedFlag,'''' BudgetSource,'''' BudgetLevel,'''' BudgetedLevels,
      '''' XCharge,'''' XChargeMethod,0 XChargeMult,'''' ContactID,'''' ContactFirstName,'''' ContactLastName,'''' CLBillingAddr,'''' w1LongName,'''' w2LongName,'''' w3LongName,
      '''' CLBAddress1,'''' CLBAddress2,'''' CLBAddress3,'''' CLBAddress4,'''' CLBCity,'''' CLBState,'''' CLBZip,'''' CLBCountry,'''' CLBPhone,'''' CLBFax,'''' CLBEMail,
      '''' w1FederalInd,'''' w2FederalInd,'''' w3FederalInd,'''' w1ProjectType,'''' w2ProjectType,'''' w3ProjectType,'''' w1projectTypeDesc,'''' w2projectTypeDesc,'''' w3projectTypeDesc,
      '''' w1Responsibility,'''' w2Responsibility,'''' w3Responsibility,'''' w1ResponsibilityDesc,'''' w2ResponsibilityDesc,'''' w3ResponsibilityDesc,'''' w1Referable,'''' w2Referable,'''' w3Referable,
      NULL w1EstCompletionDate,NULL w2EstCompletionDate,NULL w3EstCompletionDate,NULL w1ActCompletionDate,NULL w2ActCompletionDate,NULL w3ActCompletionDate,NULL w1ContractDate,NULL w2ContractDate,NULL w3ContractDate,
      NULL w1ConstComplDate,NULL w2ConstComplDate,NULL w3ConstComplDate,NULL w1ProfServicesComplDate,NULL w2ProfServicesComplDate,NULL w3ProfServicesComplDate,NULL w1BidDate,NULL w2BidDate,NULL w3BidDate,
      '''' w1ComplDateComment,'''' w2ComplDateComment,'''' w3ComplDateComment,0 w1FirmCost,0 w2FirmCost,0 w3FirmCost,'''' w1FirmCostComment,'''' w2FirmCostComment,'''' w3FirmCostComment,
      0 w1TotalProjectCost,0 w2TotalProjectCost,0 w3TotalProjectCost,'''' w1TotalCostComment,'''' w2TotalCostComment,'''' w3TotalCostComment,
      '''' ClientConfidential,'''' ClientAlias,'''' BillingClientID,'''' BillClientName,'''' BillingContactID,'''' billContactLastName,'''' billContactFirstName,'''' ProposalWBS1,
      '''' w1CreateUser,'''' w2CreateUser,'''' w3CreateUser,NULL w1CreateDate,NULL w2CreateDate,NULL w3CreateDate,
      '''' w1ModUser,'''' w2ModUser,'''' w3ModUser,NULL w1ModDate,NULL w2ModDate,NULL w3ModDate,
      '''' w1CostRateMeth,'''' w2CostRateMeth,'''' w3CostRateMeth,'''' w1CostRateTableNo,'''' w3CostRateTableNo,'''' w3CostRateTableNo,
      '''' w1PayRateMeth,'''' w2PayRateMeth,'''' w3PayRateMeth,'''' w1PayRateTableNo,'''' w2PayRateTableNo,'''' w3PayRateTableNo,'''' w1Locale,'''' w1Locale,'''' w1Locale,
      '''' w1LineItemApproval,'''' w2LineItemApproval,'''' w3LineItemApproval,'''' w1LineItemApprovalEK,'''' w2LineItemApprovalEK,'''' w3LineItemApprovalEK,
      '''' opp_name,'''' opp_number,'''' opp_prproposalwbs1,'''' opp_prproposalname,'''' opp_org,'''' opp_orgname,'''' opp_clname,'''' opp_contactname,'''' opp_projmgrname,'''' opp_principalname,'''' opp_supervisorname,
      0 opp_revenue,'''' opp_probability,0 opp_weightedrevenue,NULL opp_opendate,NULL opp_closedate,'''' opp_daysopen,'''' opp_stage,'''' opp_type,'''' opp_source,'''' opp_status,NULL opp_eststartdate,NULL opp_estcompletiondate,
      '''' opp_address1,'''' opp_address2,'''' opp_address3,'''' opp_city,'''' opp_state,'''' opp_zip,'''' opp_country,'''' opp_createuser,NULL opp_createdate,'''' opp_moduser,NULL opp_moddate,'''' opp_oppdesc 
FROM PR As LEVEL3 LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1=ProjectCustomTabFields.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFields.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFields.WBS3 , PR AS LEVEL2 , PR  LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes2 ON substring(PR.Org, 4, 2) = groupCFGOrgCodes2.Code and groupCFGOrgCodes2.orgLevel = 2 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3, ##RWL_' + @SessionId + ' , EB LEFT JOIN CA ON EB.Account = CA.Account 
WHERE ((EB.Wbs1 = LEVEL3.Wbs1 AND EB.Wbs2 = LEVEL3.Wbs2 AND EB.Wbs3 = LEVEL3.Wbs3) 
 and LEVEL3.WBS1 not like ''ZZ%''
 AND (EB.Wbs1 = LEVEL2.Wbs1 AND EB.Wbs2 = LEVEL2.wbs2)
 AND (EB.Wbs1 = PR.Wbs1) 
 AND (PR.Wbs2 = /*N*/'' '' AND PR.Wbs3 = /*N*/'' '' AND LEVEL2.Wbs3 = /*N*/'' ''))      AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + ''' 
       AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 
       AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/'''' 
GROUP BY IsNull(substring(PR.Org, 4, 2), ''''), IsNull(substring(PR.Org, 7, 3), ''''),  CASE WHEN CA.Type < 7 THEN ''5''  WHEN CA.Type >= 7 AND CA.Type < 9 THEN ''3''  WHEN CA.Type >= 9 AND CA.Type < 10 THEN ''6''  ELSE ''7'' END,  CASE WHEN (CA.Type = 6) OR (CA.Type = 8) THEN ''0'' ELSE ''1'' END

UNION ALL 
SELECT IsNull(substring(PR.Org, 4, 2), '''') As group1, Min(groupCFGOrgCodes2.Label) As groupDesc1, IsNull(substring(PR.Org, 7, 3), '''') As group2, Min(groupCFGOrgCodes3.Label) As groupDesc2,  min(CASE WHEN CA.Type < 7 THEN ''5''  WHEN CA.Type >= 7 AND CA.Type < 9 THEN ''3''  WHEN CA.Type >= 9 AND cA.Type < 10 THEN ''6''  ELSE ''7'' END) As recordType,  min(CASE WHEN (CA.Type = 6) OR (CA.Type = 8) THEN ''0'' ELSE ''1'' END) subType,
      0 AS hrsCur,0 hrsCurLabReg,0 hrsCurLabOvt,0 AS hrsYTD,0 hrsYTDLabReg,0 hrsYTDLabOvt,0 AS hrsJTD,0 hrsJTDLabReg,0 hrsJTDLabOvt,0 AS hrsCustom,0 hrsCustomLabReg,0 hrsCustomLabOvt,0 AS hrsBud, 
      0 AS hrsPctComp_c,0 as hrsPctComp_b, 0 AS hrsETC, 0 AS hrsEAC, 
      0 amtCurLabReg_c,0 amtCurLabReg_b,0 amtCurLabOvt_c,0 amtCurLabOvt_b,0 amtYTDLabReg_c,0 amtYTDLabReg_b,0 amtYTDLabOvt_c,0 amtYTDLabOvt_b,0 amtJTDLabReg_c,0 amtJTDLabReg_b,0 amtJTDLabOvt_c,0 amtJTDLabOvt_b,0 amtCustomLabReg_c,0 amtCustomLabReg_b,0 amtCustomLabOvt_c,0 amtCustomLabOvt_b,0 AS amtCur_c,0 amtCur_b, 0 AS amtYTD_c,0 amtYTD_b, 0 AS amtJTD_c,0 amtJTD_b, 0 AS amtCustom_c,0 amtCustom_b, 
      0 AS amtBud_c,0 amtBud_b, 0 AS amtEAC_c,0 amtEAC_b, 0 AS amtETC_c,0 amtETC_b, 
      0 AS amtPctComp_c,0 amtPctComp_b, 0 AS amtPctCompLab_c,0 amtPctCompLab_b, 0 AS amtPctCompExp_c,0 amtPctCompExp_b, 
      0 AS arCur_c,0 AS arCur_b, 0 AS arYTD_c,0 AS arYTD_b, 0 AS arJTD_c,0 AS arJTD_b, 0 AS arCustom_c,0 AS arCustom_b, 
      0 AS billCur_c,0 AS billCur_b, 0 AS billYTD_c,0 AS billYTD_b, 0 AS billJTD_c,0 AS billJTD_b, 0 AS billCustom_c,0 AS billCustom_b, 
      0 AS recdCur_c,0 AS recdCur_b, 0 AS recdYTD_c,0 AS recdYTD_b, 0 AS recdJTD_c,0 AS recdJTD_b, 0 AS recdCustom_c,0 AS recdCustom_b, 
      0 AS directCur_c,0 directCur_b, 0 AS directYTD_c,0 directYTD_b, 0 AS directJTD_c,0 directJTD_b, 0 AS directCustom_c,0 directCustom_b, 
      0 AS reimbCur_c,0 reimbCur_b, 0 AS reimbYTD_c,0 reimbYTD_b, 0 AS reimbJTD_c,0 reimbJTD_b, 0 AS reimbCustom_c,0 reimbCustom_b, 
      0 AS revCur_c,0 AS revCur_b, 0 AS revYTD_c,0 AS revYTD_b, 0 AS revJTD_c,0 AS revJTD_b, 0 AS revCustom_c,0 AS revCustom_b, 
      0.00 AS ubGLCur_c, 0.00 AS ubGLYTD_c, 0.00 AS ubGLJTD_c, 0.00 AS ubGLCustom_c, 0 althrsBud,0 altamtBud_c,0 altamtBud_b,
       sum(IsNull(POCommitment.amount,0.0)) as poAmt_c,
       sum(isnull(POCommitment.BillExt,0)) as poAmt_b,
      0.0 rppctcmpcalc, 0.00 targetMult_c,0.00 targetMult_b,0.00 rpfee_c,0.00 rpfee_b,0.00 rpconsultfee_c,0.00 rpconsultfee_b,0.00 rpreimballow_c,0.00 rpreimballow_b,
     '''' AS wbs1customCol1,'''' AS wbs1customCol2,'''' AS wbs1customCol3,'''' AS wbs1customCol4,'''' AS wbs1customCol5,
     '''' AS wbs2customCol1,'''' AS wbs2customCol2,'''' AS wbs2customCol3,'''' AS wbs2customCol4,'''' AS wbs2customCol5,
     '''' AS wbs3customCol1,'''' AS wbs3customCol2,'''' AS wbs3customCol3,'''' AS wbs3customCol4,'''' AS wbs3customCol5,
      '''' As currencyCodeProj,'''' As currencyCodeBill,'''' As currencyCodeFunct,'''' as currencyCodePres_Cost, '''' as currencyCodePres_Bill, '''' as currencyCodePres_ARFee, '''' as currencyCode_Opportunity, 
      0 As currencyCodeProjCount, 0 As currencyCodeBillCount, 0 As currencyCodeFunctCount, 0 As currencyCodePresCount_Cost, 0 As currencyCodePresCount_Bill, 0 As currencyCodePresCount_ARFee, 0 As currencyCodeCount_Opportunity, 
       min(PR.WBS1) WBS1,min(LEVEL2.WBS2) WBS2,min(LEVEL3.WBS3) WBS3,'''' w1Name,'''' w2Name,'''' w3Name,
      '''' ChargeType,max(PR.ChargeType) ChargeTypeCode,min(PR.SubLevel) SubLevel1,min(LEVEL2.SubLevel) SubLevel2,min(LEVEL3.SubLevel) SubLevel3,
      '''' w1principal,'''' w2principal,'''' w3principal,'''' w1prinName,'''' w2prinName,'''' w3prinName,'''' w1projMgr,'''' w2projMgr,'''' w3projMgr,'''' w1prgName,'''' w2prgName,'''' w3prgName,'''' w1supervisor,'''' w2supervisor,'''' w3supervisor,'''' w1supName,'''' w2supName,'''' w3supName,
      '''' PRAddress1,'''' PRAddress2,'''' PRAddress3, '''' PRCity,'''' PRState,'''' PRZip,'''' PRCounty,'''' PRCountry,'''' PRPhone,'''' PRFax,'''' PREmail,
      '''' ClientID,'''' ClientName,'''' CLAddress,'''' CLAddress1,'''' CLAddress2,'''' CLAddress3,'''' CLAddress4,'''' CLCity,'''' CLState,'''' CLZip,'''' CLCountry,'''' CLPhone,'''' CLFax,'''' CLEMail,
      '''' w1Org,'''' w2Org,'''' w3Org,'''' w1OrgName,'''' w2OrgName,'''' w3OrgName,
      '''' as w1TLProjectName, '''' as w2TLProjectName, '''' as w3TLProjectName, '''' ServiceProfileCode,'''' ServiceProfileDesc,
      0 mult,0 mult_sum,0 fee_c,0 fee_b,
      0 consultFee_c,0 consultFee_b,0 reimbAllow_c,0 reimbAllow_b,
      0.0 feeDirectLabor, 0.0 feeDirectExpense, 0.0 reimbAllowExpense, 0.0 reimbAllowConsultant,
      0 contractfee, 0 contractconsultFee, 0 contractreimbAllow,
      0.0 contractFeeDirectLabor, 0.0 contractFeeDirectExpense, 0.0 contractReimbAllowExpense, 0.0 contractReimbAllowConsultant,
      0 BudOHRate,'''' w1Status,'''' w2Status,'''' w3Status,'''' w1StatusDesc,'''' w2StatusDesc,'''' w3StatusDesc,'''' wbs1RevType,'''' wbs2RevType, '''' wbs3RevType,'''' wbs1RevTypeDesc,'''' wbs2RevTypeDesc,'''' wbs3RevTypeDesc,'''' UnitTable,
      NULL w1StartDate,NULL w2StartDate,NULL w3StartDate,NULL w1EndDate,NULL w2EndDate,NULL w3EndDate,
      0.00 w1PctComp,0.00 w2PctComp,0.00 w3PctComp,0.00 w1LabPctComp,0.00 w2LabPctComp,0.00 w3LabPctComp,0.00 w1ExpPctComp,0.00 w2ExpPctComp,0.00 w3ExpPctComp,
      '''' BillByDefault,'''' BillableWarning,'''' BudgetedFlag,'''' BudgetSource,'''' BudgetLevel,'''' BudgetedLevels,
      '''' XCharge,'''' XChargeMethod,0 XChargeMult,'''' ContactID,'''' ContactFirstName,'''' ContactLastName,'''' CLBillingAddr,'''' w1LongName,'''' w2LongName,'''' w3LongName,
      '''' CLBAddress1,'''' CLBAddress2,'''' CLBAddress3,'''' CLBAddress4,'''' CLBCity,'''' CLBState,'''' CLBZip,'''' CLBCountry,'''' CLBPhone,'''' CLBFax,'''' CLBEMail,
      '''' w1FederalInd,'''' w2FederalInd,'''' w3FederalInd,'''' w1ProjectType,'''' w2ProjectType,'''' w3ProjectType,'''' w1projectTypeDesc,'''' w2projectTypeDesc,'''' w3projectTypeDesc,
      '''' w1Responsibility,'''' w2Responsibility,'''' w3Responsibility,'''' w1ResponsibilityDesc,'''' w2ResponsibilityDesc,'''' w3ResponsibilityDesc,'''' w1Referable,'''' w2Referable,'''' w3Referable,
      NULL w1EstCompletionDate,NULL w2EstCompletionDate,NULL w3EstCompletionDate,NULL w1ActCompletionDate,NULL w2ActCompletionDate,NULL w3ActCompletionDate,NULL w1ContractDate,NULL w2ContractDate,NULL w3ContractDate,
      NULL w1ConstComplDate,NULL w2ConstComplDate,NULL w3ConstComplDate,NULL w1ProfServicesComplDate,NULL w2ProfServicesComplDate,NULL w3ProfServicesComplDate,NULL w1BidDate,NULL w2BidDate,NULL w3BidDate,
      '''' w1ComplDateComment,'''' w2ComplDateComment,'''' w3ComplDateComment,0 w1FirmCost,0 w2FirmCost,0 w3FirmCost,'''' w1FirmCostComment,'''' w2FirmCostComment,'''' w3FirmCostComment,
      0 w1TotalProjectCost,0 w2TotalProjectCost,0 w3TotalProjectCost,'''' w1TotalCostComment,'''' w2TotalCostComment,'''' w3TotalCostComment,
      '''' ClientConfidential,'''' ClientAlias,'''' BillingClientID,'''' BillClientName,'''' BillingContactID,'''' billContactLastName,'''' billContactFirstName,'''' ProposalWBS1,
      '''' w1CreateUser,'''' w2CreateUser,'''' w3CreateUser,NULL w1CreateDate,NULL w2CreateDate,NULL w3CreateDate,
      '''' w1ModUser,'''' w2ModUser,'''' w3ModUser,NULL w1ModDate,NULL w2ModDate,NULL w3ModDate,
      '''' w1CostRateMeth,'''' w2CostRateMeth,'''' w3CostRateMeth,'''' w1CostRateTableNo,'''' w3CostRateTableNo,'''' w3CostRateTableNo,
      '''' w1PayRateMeth,'''' w2PayRateMeth,'''' w3PayRateMeth,'''' w1PayRateTableNo,'''' w2PayRateTableNo,'''' w3PayRateTableNo,'''' w1Locale,'''' w1Locale,'''' w1Locale,
      '''' w1LineItemApproval,'''' w2LineItemApproval,'''' w3LineItemApproval,'''' w1LineItemApprovalEK,'''' w2LineItemApprovalEK,'''' w3LineItemApprovalEK,
      '''' opp_name,'''' opp_number,'''' opp_prproposalwbs1,'''' opp_prproposalname,'''' opp_org,'''' opp_orgname,'''' opp_clname,'''' opp_contactname,'''' opp_projmgrname,'''' opp_principalname,'''' opp_supervisorname,
      0 opp_revenue,'''' opp_probability,0 opp_weightedrevenue,NULL opp_opendate,NULL opp_closedate,'''' opp_daysopen,'''' opp_stage,'''' opp_type,'''' opp_source,'''' opp_status,NULL opp_eststartdate,NULL opp_estcompletiondate,
      '''' opp_address1,'''' opp_address2,'''' opp_address3,'''' opp_city,'''' opp_state,'''' opp_zip,'''' opp_country,'''' opp_createuser,NULL opp_createdate,'''' opp_moduser,NULL opp_moddate,'''' opp_oppdesc 
FROM PR As LEVEL3 LEFT JOIN ProjectCustomTabFields ON  LEVEL3.WBS1=ProjectCustomTabFields.WBS1 AND LEVEL3.WBS2=ProjectCustomTabFields.WBS2 AND  LEVEL3.WBS3=ProjectCustomTabFields.WBS3 , PR AS LEVEL2 , PR  LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes2 ON substring(PR.Org, 4, 2) = groupCFGOrgCodes2.Code and groupCFGOrgCodes2.orgLevel = 2 LEFT JOIN cfgOrgCodes AS groupCFGOrgCodes3 ON substring(PR.Org, 7, 3) = groupCFGOrgCodes3.Code and groupCFGOrgCodes3.orgLevel = 3, ##RWL_' + @SessionId + ' , POCommitment 
 Left Join POMaster on POMaster.MasterPKey = POCommitment.MasterPKey 
 Left Join CA on CA.Account = POCommitment.Account 
 Left Join VE on VE.Vendor = POMaster.Vendor 
WHERE ((POCommitment.Wbs1 = LEVEL3.Wbs1 AND POCommitment.Wbs2 = LEVEL3.Wbs2 AND POCommitment.Wbs3 = LEVEL3.Wbs3) 
 and LEVEL3.WBS1 not like ''ZZ%''
 AND (POCommitment.Wbs1 = LEVEL2.Wbs1 AND POCommitment.Wbs2 = LEVEL2.wbs2)
 AND (POCommitment.Wbs1 = PR.Wbs1) 
 AND (PR.Wbs2 = /*N*/'' '' AND PR.Wbs3 = /*N*/'' '' AND LEVEL2.Wbs3 = /*N*/'' '')) AND (((SUBSTRING(PR.Org, 1, 2) = /*N*/@CompanyId) AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Regular'')))) AND (PR.Status = ''A'')) AND  LEVEL3.WBS1 = ##RWL_' + @SessionId + '.WBS1 AND ##RWL_' + @SessionId + '.WBS2 = /*N*/'' '' AND ##RWL_' + @SessionId + '.WBS3 = /*N*/'' ''  AND ##RWL_' + @SessionId + '.SessionID = ''' + @SessionId + ''' 
       AND PR.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS1 = LEVEL3.WBS1 AND LEVEL2.WBS2 = LEVEL3.WBS2 
       AND PR.WBS2 = /*N*/'''' AND PR.WBS3 = /*N*/'''' AND LEVEL2.WBS3 = /*N*/'''' 
GROUP BY IsNull(substring(PR.Org, 4, 2), ''''), IsNull(substring(PR.Org, 7, 3), ''''),  CASE WHEN CA.Type < 7 THEN ''5''  WHEN CA.Type >= 7 AND CA.Type < 9 THEN ''3''  WHEN CA.Type >= 9 AND CA.Type < 10 THEN ''6''  ELSE ''7'' END,  CASE WHEN (CA.Type = 6) OR (CA.Type = 8) THEN ''0'' ELSE ''1'' END
) AS omega
LEFT JOIN
(
	SELECT SUBSTRING(Org, 7, 3) AS Org
	, SUM(ETC) AS ETC
	, SUM(EAC) AS EAC
	, (SUM(PrcntComp) / COUNT(*)) AS PrcntComp 
	, SUM(WriteoffAdjmt) AS WriteoffAdjmt
	FROM
	(
		SELECT alpha.Org
		, alpha.WBS1
		, alpha.ChargeType
		, TRY_PARSE(beta.custefforttocomplete AS NUMERIC(10, 0) USING ''en-US'' ) AS ETC
		, TRY_PARSE(beta.CustEffortatCompletion AS NUMERIC(10, 0) USING ''en-US'' ) AS EAC
		, TRY_PARSE(beta.custpercentcomplete AS NUMERIC(10, 0) USING ''en-US'' ) AS PrcntComp 
		, TRY_PARSE(beta.CustWriteoffAdjmt AS NUMERIC(10, 0) USING ''en-US'' ) AS WriteoffAdjmt
		FROM 
		(
			SELECT mu.WBS1
			, mu.Org
			, mu.ChargeType 
			FROM PR mu
			INNER JOIN ##rwl_' + @sessionId + ' phi
			ON phi.WBS1 = mu.WBS1
			AND phi.WBS2 = mu.WBS2
			AND phi.WBS3 = mu.WBS3
			WHERE mu.Status = ''A'' 
			AND mu.WBS2 = '''' 
			AND mu.WBS3 = ''''
			AND mu.ChargeType = ''R'' 
		) alpha
		LEFT JOIN
		(
			SELECT WBS1
			, CASE WHEN PATINDEX(''(%)'', custefforttocomplete ) != 0 
				THEN REPLACE(REPLACE(custefforttocomplete, ''('', ''-''), '')'', '''') 
				ELSE custefforttocomplete 
			END AS custefforttocomplete
			, CASE WHEN PATINDEX(''(%)'', CustEffortatCompletion ) != 0
				THEN REPLACE(REPLACE(CustEffortatCompletion, ''('', ''-''), '')'', '''') 
				ELSE CustEffortatCompletion
			END AS CustEffortatCompletion
			, REPLACE(custpercentcomplete, ''%'', '''') AS custpercentcomplete
			, CASE WHEN PATINDEX(''(%)'', CustWriteoffAdjmt ) != 0
				THEN REPLACE(REPLACE(CustWriteoffAdjmt, ''('', ''-''), '')'', '''') 
				ELSE CustWriteoffAdjmt
			END AS CustWriteoffAdjmt
			FROM projectcustomtabfields
			WHERE WBS2 = ''''
			AND WBS3 = ''''
		) beta
		ON beta.WBS1 = alpha.WBS1 
	) AS gamma
	GROUP BY gamma.Org
) AS phi
ON phi.Org = omega.group2
phi
where phi.WBS1 not like ''ZZ%''
ORDER  BY group1, 
          group2, 
          Recordtype, 
          subtype 
OPTION (RECOMPILE) 

DROP TABLE ##rwl_' + @SessionId

DECLARE @ParmDefinition nvarchar(max) = N'@CompanyId VARCHAR(2), @MinPeriod INT, @Period INT'; 
EXECUTE sp_executesql @sql, @ParmDefinition, @CompanyId, @MinPeriod, @Period 

END
GO
