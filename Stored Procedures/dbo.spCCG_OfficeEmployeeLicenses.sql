SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_OfficeEmployeeLicenses] @Org varchar (25)
AS 
/* 
Copyright (c) 2019 Central Consulting Group.  All rights reserved.
12/06/2019	David Springer
			Populate UDIC_Office Employee Licenses grid
			Call this from an Employee License (Registration) INSERT, CHANGE or DELETE workflow
*/
DECLARE @OfficeCode varchar (4) = substring (@Org, 4, 2)
BEGIN
SET NOCOUNT ON

	Delete s From UDIC_Office o, UDIC_Office_EmployeeLicenses s Where o.UDIC_UID = s.UDIC_UID and o.CustOfficeNumber = @OfficeCode

	Insert Into UDIC_Office_EmployeeLicenses
	(UDIC_UID, Seq, CustLicenseEmployee, CustLicenseType, CustLicenseState, CustLicenseNumber, CustLicenseExpiration)
	Select o.UDIC_UID, Replace (NewID(), '-', ''), e.Employee, r.Registration, r.StateRegistered, r.RegistrationNo, r.DateExpires
	From EM e, EMRegistration r, UDIC_Office o
	Where e.Status = 'A'
	  and e.Employee = r.Employee
	  and e.Org not like '02%'
	  and substring (e.Org, 4, 2) = o.CustOfficeNumber
	  and o.CustOfficeNumber = @OfficeCode
	  and o.CustState = r.StateRegistered
END
GO
