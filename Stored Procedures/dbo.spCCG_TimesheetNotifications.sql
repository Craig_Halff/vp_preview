SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_TimesheetNotifications]
    @AltApprover varchar(1), @OpsMgr varchar(1), @MgmtLeader varchar(1)
As
/*
Copyright 2017 (c) Central Consulting Group.   All rights reserved.
08/31/2017	David Springer
			Notify employees and management of unsubmitted and unapproved timesheets
			@AltApprover = Y or N to determine whether to include cc
10/13/2021	Craig H. Anderson
			Added Team, approver, and Alt Approver anmes to the grid and eliminated the Approver name tha preceded the table.
			Changed the red and blue CSS color to "Halff" red and blue.
*/
Set NoCount On;
Set Transaction Isolation Level Read Uncommitted;
Declare @EndDate          date
      , @TKGroup          varchar(255)
      , @EKApprover       varchar(8000)
      , @EmailTo          varchar(200)
      , @EmailApprover    varchar(200)
      , @EmailAltApprover varchar(200)
      , @EmailOpsMgr      varchar(200)
      , @EmailMgmtLeader  varchar(200)
      , @Employee         varchar(255)
      , @Company          varchar(10)
      , @CompanyFrom      varchar(255)
      , @Status           varchar(255)
      , @ExpectedHours    float
      , @Hours            float
      , @StatusOrder      int
      , @CSS              varchar(Max)
      , @Body             varchar(Max)
      , @Class            varchar(100)
      , @Loop1            integer
      , @numRows          integer
      , @numRowsApproved  integer
      , @Subject          varchar(255)
      , @CCList           varchar(255)
      , @ApproverName     varchar(255)
      , @AltApproverName  varchar(255)
      , @Team             varchar(3);

    Begin
        -- Set the appropriate subject line
        If @AltApprover = 'N'
           And @OpsMgr = 'N'
            Set @Subject = 'Reminder to Review and Approve Timesheets';
        Else Set @Subject = 'Timesheet Review and Approval Past Due';

        -- CSS style rules
        Set @CSS
            = '
<style type="text/css">
	p {padding-top:15px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;}
	td, th {border: 1px solid black;padding: 5px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;}
	th {background-color: #6E6E6E;color: white;text-align:center;}
	.c1 , .c3, .c4,.c5{text-align:left;}
	.c2 {text-align:center;}
	.c6, .c7 {text-align:right;}
	.red {color:#E31837;}
	.blue {color:#00539B;}
	.black {color:#000000;}
</style>
'       ;

        -- Table variable to hold query results
        Declare @EmpList table (
            Employee varchar(255) Not Null
          , Company varchar(10)
          , Status varchar(255)
          , ExpectedHours float
          , Hours float
          , StatusOrder int
          , Approver varchar(255)
          , AltApprover varchar(255)
          , Team varchar(3)
        );

        -- Determine timesheet end date for processing records
        Select @EndDate = EndDate
        From dbo.tkControl
        Where
            Cast(DateAdd(dd, -3, GetDate()) As date)
            Between StartDate And EndDate
            And Company = '01';

        Declare curEKApprover Insensitive Cursor For
        Select Distinct
               a1.TKGroup + ' - ' + IsNull(a1.PreferredName, a1.FirstName) + ' ' + IsNull(a1.LastName, '') As TKGroup
             , IsNull(ex.CustEKApprover, '')
             , IsNull(a1.EMail, '') As EmailApprover
             , IsNull(a2.EMail, '') As EmailAltApprover
             , IsNull(a3.EMail, '') As EmailOpsMgr
             , IsNull(a5.EMail, '') As EmailMgmtLeader
        From dbo.EMAllCompany a1
           , dbo.EM e
           , dbo.EmployeeCustomTabFields ex
            Left Join dbo.EMMain a2
                On a2.Employee = ex.CustEKAlternateApprover
            Left Join dbo.EMMain a3
                On a3.Employee = ex.CustEKOperationsManager
            Left Join dbo.EMMain a4
                On a4.Employee = ex.CustEKDirector
            Left Join dbo.EMMain a5
                On a5.Employee = ex.CustEKManagementLeader
        Where
            e.Employee = ex.Employee
            --and e.Org not like '02%'
            And Left(e.Org, 2) Not In ( '02' )
            And e.Status = 'A'
            And ex.CustEKApprover = a1.Employee
            And ex.CustEKAlternateApprover = a2.Employee
            And
                (
                    Not Exists
            (
                Select 'x'
                From dbo.tkMaster
                Where
                    Employee = e.Employee
                    And EndDate = @EndDate
            )
                    Or Exists
            (
                Select 'x'
                From dbo.tkMaster
                Where
                    Employee = e.Employee
                    And EndDate = @EndDate
                    And Submitted In ( 'N', 'Y' )
            ) -- In progress & Submitted
                );

        -- Get each EKApprover
        Open curEKApprover;
        Fetch Next From curEKApprover
        Into @TKGroup, @EKApprover, @EmailApprover, @EmailAltApprover, @EmailOpsMgr, @EmailMgmtLeader;
        Set @Loop1 = @@FETCH_STATUS;
        While @Loop1 = 0
            Begin
                -- Clear table
                Delete From @EmpList;

                -- Insert results
                Insert Into @EmpList
                Select IsNull(e.PreferredName, e.FirstName) + ' ' + IsNull(e.LastName, '') As Employee
                     , IsNull(e.EmployeeCompany, '') As EmployeeCompany
                     , Case When t.Submitted = 'N' Then 'In Progress'
                           When t.Submitted = 'Y' Then 'Submitted'
                           When t.Submitted = 'A' Then 'Approved'
                           When t.Submitted = 'P' Then 'Processed' -- not needed here but for completeness
                           Else 'Missing'
                       End As Status
                     , IsNull(e.HoursPerDay, 0) * 5 As ExpectedHours
                     , IsNull(d.Hrs, 0.0000) As Hrs
                     , Case When t.Submitted = 'N' Then 1
                           When t.Submitted = 'Y' Then 2
                           When t.Submitted = 'A' Then 3
                           Else 0
                       End As StatusOrder
                     , IsNull(e1.PreferredName, e1.FirstName) + ' ' + IsNull(e1.LastName, '') As Approver
                     , IsNull(e2.PreferredName, e2.FirstName) + ' ' + IsNull(e2.LastName, '') As AltApprover
                     , Right(e.Org, 3) As Team
                From dbo.EM e
                    Inner Join dbo.EmployeeCustomTabFields ex
                        On e.Employee = ex.Employee
                    Left Join dbo.tkMaster t
                        On t.Employee = e.Employee
                           And t.EndDate = @EndDate
                    Left Join
                        (
                            Select Employee, EndDate, Sum(RegHrs + OvtHrs + SpecialOvtHrs) As Hrs
                            From dbo.tkDetail
                            Group By
                                Employee, EndDate
                        ) d
                        On d.Employee = e.Employee
                           And d.EndDate = t.EndDate
                    Left Join dbo.EM e1
                        On ex.CustEKApprover = e1.Employee
                    Left Join dbo.EM e2
                        On ex.CustEKAlternateApprover = e2.Employee
                Where
                    e.Status = 'A' -- active employees
                    And
                        (
                            e.HireDate Is Null
                            Or e.HireDate < @EndDate
                        ) -- who have been here prior to this period end date
                    And
                        (
                            ex.CustUnpaidLeaveEnd Is Null
                            Or ex.CustUnpaidLeaveEnd > @EndDate
                        ) -- and who if returning from leave has been here since this end date
                    And ex.CustEKApprover = @EKApprover -- this group/team
                    And IsNull(t.Submitted, 'N') <> 'P' -- exclude processed
                    And IsNull(t.Submitted, 'N') <> Case When @MgmtLeader = 'Y' Then 'A' Else '' End;

                Select @numRows = Count(*)From @EmpList; -- total number of results
                Select @numRowsApproved = Count(*)
                From @EmpList
                Where Status = 'Approved'; -- number of 'Approved'

                If (
                       @numRows > 0 -- At least 1 result
                       And @numRowsApproved <> @numRows
                   ) -- not all results are 'Approved'
                    Begin

                        Declare curEmps Insensitive Cursor For
                        Select Employee, Company, Status, ExpectedHours, Hours, StatusOrder, Approver, AltApprover, Team
                        From @EmpList
                        Order By
                            StatusOrder, Employee;

                        -- Open the table with headers
                        Set @Body
                            = '<br> 
<table cellpadding=''5'' cellspacing=''0'' style=''border:1px solid black;border-collapse:collapse;''>
	<thead>
		<tr>
			<th>Employee</th>
			<th>Status</th>
			<th>Team</th>
			<th>Approver</th>
			<th>Alternate<br>Approver</th>
			<th>Expected<br>Hours</th>
			<th>Hours<br>Entered</th>
		</tr>
	</thead>
	<tbody>
'                       ;
                        Open curEmps;
                        Fetch Next From curEmps
                        Into @Employee, @Company, @Status, @ExpectedHours, @Hours, @StatusOrder, @ApproverName, @AltApproverName, @Team;
                        While @@FETCH_STATUS = 0
                            Begin

                                -- Set the class of the row
                                If (@Status = 'Approved') Set @Class = ' class="black"';
                                Else If (@Status = 'Submitted') Set @Class = ' class="blue"';
                                Else Set @Class = ' class="red"';

                                Set @Body = @Body + '		<tr' + @Class + '> 
			<td class="c1">'                + IsNull(@Employee, '') + '</td> 
			<td class="c2">'                + IsNull(@Status, '') + '</td> 
			<td class="c3">'                + @Team + '</td>
			<td class="c4">'                + @ApproverName + '</td> 
			<td class="c5">'                + @AltApproverName + '</td> 
			<td class="c6">'                + Convert(varchar(8), Convert(decimal(8, 2), IsNull(@ExpectedHours, 0))) + '</td> 
			<td class="c7">'                + Convert(varchar(8), Convert(decimal(8, 2), IsNull(@Hours, 0))) + '</td> 
		</tr>
'                               ;
                                Fetch Next From curEmps
                                Into @Employee, @Company, @Status, @ExpectedHours, @Hours, @StatusOrder, @ApproverName, @AltApproverName, @Team;
                            End;
                        Close curEmps;
                        Deallocate curEmps;

                        -- Close the table
                        Set @Body = @Body + '	</tbody>
</table>
'                       ;
                        Set @CompanyFrom = '';
                        Set @CCList = '';

                        If @Company In ( '01', '05' ) Set @CompanyFrom = 'cmoney@halff.com';
                        Else If @Company In ( '03', '04' ) Set @CompanyFrom = 'cmoney@halff.com';

                        If IsNull(@AltApprover, '') <> 'Y' Set @EmailAltApprover = '';
                        If IsNull(@OpsMgr, '') <> 'Y' Set @EmailOpsMgr = '';
                        If IsNull(@MgmtLeader, '') <> 'Y' Set @EmailMgmtLeader = '';

                        -- Payroll
                        If IsNull(@OpsMgr, '') = 'Y'
                            Begin
                                Select @EmailOpsMgr = Stuff((
                                                                Select ';' + A.email
                                                                From
                                                                    (Select @EmailOpsMgr As email Union Select @CompanyFrom As email) As A
                                                                Where A.email <> ''
                                                                For Xml Path('')
                                                            )
                                                          , 1
                                                          , 1
                                                          , ''
                                                           );
                            End;

                        -- To Email
                        If IsNull(@MgmtLeader, '') = 'Y'
                            Begin
                                Set @EmailTo = IsNull(@EmailOpsMgr, '');
                                Set @EmailApprover = '';
                                Set @EmailAltApprover = '';
                                Set @EmailOpsMgr = '';
                            End;
                        Else Set @EmailTo = IsNull(@EmailApprover, '');

                        -- Build the CCList from non-empty values
                        Select @CCList = Stuff((
                                                   Select ';' + A.email
                                                   From
                                                       (
                                                           Select @EmailAltApprover As email
                                                           Union
                                                           Select @EmailOpsMgr As email
                                                           Union
                                                           Select @EmailMgmtLeader As email
                                                       ) As A
                                                   Where A.email <> ''
                                                   For Xml Path('')
                                               )
                                             , 1
                                             , 1
                                             , ''
                                              );

                        Insert Into dbo.CCG_Email_Messages
                            (
                                SourceApplication
                              , Sender
                              , ToList
                              , CCList
                              , Subject
                              , Body
                              , CreateDateTime
                              , MaxDelay
                            )
                        Values
                        ('Timesheet Notification', IsNull(@CompanyFrom, ''), IsNull(@EmailTo, ''), IsNull(@CCList, ''), @Subject, @CSS + @Body, GetDate(), 1);
                    --			Values ('Timesheet Notification', IsNull(@CompanyFrom, ''), IsNull('jsagel@halff.com', ''), IsNull('jsagel@halff.com', ''), @Subject, @CSS + @Body, getDate(), 1)
                    End;

                Fetch Next From curEKApprover
                Into @TKGroup, @EKApprover, @EmailApprover, @EmailAltApprover, @EmailOpsMgr, @EmailMgmtLeader;
                Set @Loop1 = @@FETCH_STATUS;
            End;
        Close curEKApprover;
        Deallocate curEKApprover;

    End;


GO
