SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_SaveStage]
	@wbs1				Nvarchar(32),
	@oldStage			Nvarchar(50),
	@newStage			Nvarchar(50),
	@newStageVerified	varchar(50),
	@employee			Nvarchar(32),	
	@delegateFor		Nvarchar(32),
	@role				varchar(32),
	@stageReRoutedFrom Nvarchar(50) = null--optional stage to pass in if custom stagechage logic rerouted the stage.  NewStage is always the final/result stage, if stageReRoutedFrom is passed in that is the stage originally selected by the user.
AS
BEGIN
	-- Copyright (c) 2018 EleVia Software and Central Consulting Group.  All rights reserved.
	/*
	spCCG_EI_SaveStage @wbs1 = '2002019.00', @oldStage = 'Invoice as noted', @newStage = 'Final', @newStageVerified = 'Final', @employee = '00003', @delegateFor = '', @role = '!ACCT'
	SELECT * FROM CCG_EI_Pending WHERE (WBS1 = '2002019.00')
	SELECT * FROM CCG_EI WHERE (WBS1 = '2002019.00')
	*/
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @rowsAffected	int

	-- Ensure that our inputs are safe (prevent SQL injection)
	/*
	DECLARE @safeSql		int
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @wbs1)
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @newStageVerified) * 2
	IF @safeSql < 3 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS varchar(4))
		RETURN
	END
	*/

	declare @updateMainStage Nvarchar(50) = ''

	BEGIN TRANSACTION
	BEGIN TRY
		declare @ParallelApproval char(1) = 'N'
		select @ParallelApproval = isnull(c.ParallelApproval, 'N') from CCG_EI_Config c

		if ISNULL(@stageReRoutedFrom,'') <> ''
		begin
			-- Insert reRoutedFrom stage change history record
			insert into CCG_EI_History (
					ActionTaken, ActionDate, ActionTakenBy, WBS1, InvoiceStage, PriorInvoiceStage, PriorInvoiceStageDateSet, DelegateFor)
				Values ('Stage Change-RD', getutcdate()-.00003, @employee, @wbs1, @stageReRoutedFrom, @oldStage,
					dbo.fnCCG_EI_GetPriorStageChangeActionDateForPrjDate(@wbs1, getutcdate()), nullif(@delegateFor,''))
		end

		if @role = 'ACCT' or @role = 'ROACCT' or not exists (select 'x' from CCG_EI_Pending where WBS1 = @wbs1 and CanModify = 'Y') or @ParallelApproval = 'N'
		begin
			set @updateMainStage = @newStageVerified
		end
		else begin
			declare @pendingNotCompleteCnt int, @empPending int = 0, @pendingAllCnt int = 0			
			select @pendingAllCnt = count('x'), @pendingNotCompleteCnt = sum(case when isnull(p.NextInvoiceStage,'') = '' then 1 else 0 end),
					@empPending = sum(case when (p.Employee = @employee or d.Employee is not null) and isnull(p.NextInvoiceStage,'') = '' then 1 else 0 end) 
				from CCG_EI_Pending p
						outer apply (select employee from dbo.[fnCCG_EI_DelegateFor](@employee, @wbs1) d where p.Employee = d.Employee) d
					where p.WBS1 = @wbs1 and p.CanModify = 'Y' 

			if @empPending = 0
			begin
				-- Something is amiss (either already set the stage or accessing record they should not have access to now)
				select -1 as result, 'Unknown error: You do not have permissions to update the stage for this row' as errormessage
				ROLLBACK TRANSACTION
				return
			end
			else begin
				-- Did we only have one pending record (= not parallel approval for this WBS1)?
				if @pendingAllCnt = 1 begin
					set @updateMainStage = @newStageVerified
				end
				else begin
					update CCG_EI_Pending set NextInvoiceStage = @newStageVerified 
						where WBS1 = @wbs1 and isnull(NextInvoiceStage, '') = ''
							and (Employee = @employee or Employee in (select employee from dbo.[fnCCG_EI_DelegateFor](@employee, @wbs1))) and CanModify = 'Y' 				

					-- Insert parallel stage change history record
					insert into CCG_EI_History (
							ActionTaken, ActionDate, ActionTakenBy, WBS1, InvoiceStage, PriorInvoiceStage, PriorInvoiceStageDateSet, DelegateFor)
						Values ('Stage Change-P', getutcdate()-.00002, @employee, @wbs1, @newStage, @oldStage,
							dbo.fnCCG_EI_GetPriorStageChangeActionDateForPrjDate(@wbs1, getutcdate()), nullif(@delegateFor,''))

					-- Check if this is the last parallel pending record
					if @pendingNotCompleteCnt = @empPending 
					begin
						-- Determine the next stage to use for the project
						declare @maxWeight int = 0
						select @maxWeight = max(s.Weight)
							from CCG_EI_Pending p
								inner join CCG_EI_ConfigStages s on s.Stage = p.NextInvoiceStage
							where p.WBS1 = @wbs1

						-- Make sure we update the main EI stage [happens below]
						select @updateMainStage = max(p.NextInvoiceStage)
							from CCG_EI_Pending p
								inner join CCG_EI_ConfigStages s on s.Stage = p.NextInvoiceStage
							where p.WBS1 = @wbs1 and s.Weight = @maxWeight
					end
					else SET @rowsAffected = @rowsAffected + @@ROWCOUNT
				end
			end
		end

		if @updateMainStage <> ''
		begin
			-- Delete all invoices for the invoice group
			Delete from CCG_EI where WBS1 = @wbs1
			SET @rowsAffected = @@ROWCOUNT

			-- Insert back only those in the current grid:
			Insert into CCG_EI (WBS1, InvoiceStage, DateChanged, ChangedBy)
				Select WBS1, @updateMainStage, getutcdate(), @employee
					From ProjectCustomTabFields									-- make sure this project exists
					Where WBS1 = @wbs1 and WBS2 = N' ' and WBS3 = N' '
			SET @rowsAffected = @rowsAffected + @@ROWCOUNT

			-- Insert main stage change history record
			insert into CCG_EI_History (
					ActionTaken, ActionDate, ActionTakenBy, WBS1, InvoiceStage, PriorInvoiceStage, PriorInvoiceStageDateSet, DelegateFor)
				Values ('Stage Change', getutcdate(), @employee, @wbs1, @newStage, @oldStage,
					dbo.fnCCG_EI_GetPriorStageChangeActionDateForPrjDate(@wbs1, getutcdate()), nullif(@delegateFor,''))
			SET @rowsAffected = @rowsAffected + @@ROWCOUNT

			-- Sync to Vision if configured (use server time so date in Vision looks normal (not utc))
			IF (SELECT COUNT('x') FROM sys.objects b, sys.columns a
				WHERE b.object_id = OBJECT_ID(N'[dbo].[ProjectCustomTabFields]') AND type in (N'U')
					AND a.object_id = b.object_id
					AND (LOWER(a.name) = 'custinvoicestage' OR LOWER(a.name) = 'custinvoicestagedate')) = 2
			BEGIN
				If Exists(Select * From CCG_EI_Config Where SyncInvoiceStageToVision = 'Y') BEGIN
					EXEC (N'Update ProjectCustomTabFields
						Set CustInvoiceStage = ''' + @newStageVerified + N''',
							CustInvoiceStageDate = getdate()
						Where WBS1 = N''' + @wbs1 + ''' and WBS2 = N'' '' and WBS3 = N'' ''')
					SET @rowsAffected = @rowsAffected + @@ROWCOUNT
				END
			END

			exec spCCG_EI_UpdatePending @wbs1
		end

		COMMIT TRANSACTION
		SELECT @rowsAffected As RowsAffected
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		-- select -1 as result, isnull(error_message(), 'Unknown error') + ' (' + cast(isnull(error_message(), -1) as varchar(20)) + ')' as errormessage
	END CATCH
END
GO
