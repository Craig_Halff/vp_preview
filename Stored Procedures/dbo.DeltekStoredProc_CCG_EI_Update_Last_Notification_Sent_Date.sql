SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Update_Last_Notification_Sent_Date]
	@wbs1		Nvarchar(32)
AS
BEGIN
	-- DeltekStoredProc_CCG_EI_Update_Last_Notification_Sent_Date @wbs1 = '1999015.00'
	SET NOCOUNT ON;

	Update CCG_EI Set DateLastNotificationSent = getutcdate()
		Where WBS1 = @wbs1;

END;



GO
