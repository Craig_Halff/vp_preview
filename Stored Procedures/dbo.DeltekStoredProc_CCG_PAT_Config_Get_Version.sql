SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_Get_Version] 
             AS EXEC spCCG_PAT_Config_Get_Version 
GO
