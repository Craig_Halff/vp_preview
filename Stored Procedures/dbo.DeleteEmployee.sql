SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteEmployee] @Employee Nvarchar(32)
AS
    DELETE FROM CustomProposalEmployee WHERE Employee = @Employee
    DELETE FROM CustomProposalEmployeeGraphics WHERE Employee = @Employee
    DELETE FROM CustomProposalProjTeam WHERE Employee = @Employee
    DELETE FROM DocumentsEM WHERE Employee = @Employee

    DELETE FROM EMAccrualDetail WHERE Employee = @Employee
    DELETE FROM EMAccrual WHERE Employee = @Employee
    DELETE FROM EMActivity WHERE Employee = @Employee
    DELETE FROM EMClientAssoc WHERE Employee = @Employee
    DELETE FROM EMContactAssoc WHERE Employee = @Employee
    DELETE FROM EMDegree WHERE Employee = @Employee
    DELETE FROM EMDirectDeposit WHERE Employee = @Employee
    DELETE FROM EMEKGroups WHERE Employee = @Employee
    DELETE FROM EMLocale WHERE Employee = @Employee
    DELETE FROM EMPayroll WHERE Employee = @Employee
    DELETE FROM EMPayrollWithholding WHERE Employee = @Employee
    DELETE FROM EMPhoto WHERE Employee = @Employee
    DELETE FROM EmployeeCustomTabFields WHERE Employee = @Employee
    DELETE FROM EmployeeFileLinks WHERE Employee = @Employee
    DELETE FROM EMProjectAssoc WHERE Employee = @Employee
    DELETE FROM EMProjectAssocTemplate WHERE Employee = @Employee
    DELETE FROM EMRegistration WHERE Employee = @Employee
    DELETE FROM EMResume WHERE Employee = @Employee
    DELETE FROM EMSkills WHERE Employee = @Employee
    DELETE FROM EMSubscr WHERE Employee = @Employee
    DELETE FROM EMTKGroups WHERE Employee = @Employee
    --DELETE FROM OpportunityEMAssoc WHERE Employee = @Employee
    DELETE FROM SF330Preferences WHERE Exists (Select 'x' FROM SEUSER
      WHERE SF330Preferences.Username = SEUSER.Username AND SEUSER.Employee = @Employee)
    DELETE FROM WorkflowEMActivity WHERE Employee = @Employee

    Exec DeleteCustomGridTabData 'Employees',@Employee, '', ''
    
    UPDATE Activity SET Employee = null WHERE Employee = @Employee
    UPDATE CustomProposal SET ProposalManager = null WHERE ProposalManager = @Employee
    --UPDATE Opportunity SET Principal = null WHERE Principal = @Employee
    --UPDATE Opportunity SET ProjMgr = null WHERE ProjMgr = @Employee
    --UPDATE Opportunity SET Supervisor = null WHERE Supervisor = @Employee
    --UPDATE Opportunity SET ProposalManager = null WHERE ProposalManager = @Employee
    --UPDATE Opportunity SET BusinessDeveloperLead = null WHERE BusinessDeveloperLead = @Employee
    --UPDATE Opportunity SET MarketingCoordinator = null WHERE MarketingCoordinator = @Employee
    --UPDATE OpportunityProposals SET Employee = null WHERE Employee = @Employee
    UPDATE PR SET Principal = null WHERE Principal = @Employee
    UPDATE PR SET ProjMgr = null WHERE ProjMgr = @Employee
    UPDATE PR SET Supervisor = null WHERE Supervisor = @Employee
    UPDATE PR SET ProposalManager = null WHERE ProposalManager = @Employee
    UPDATE PR SET BusinessDeveloperLead = null WHERE BusinessDeveloperLead = @Employee
    UPDATE PR SET MarketingCoordinator = null WHERE MarketingCoordinator = @Employee
    UPDATE PR SET Biller = null WHERE Biller = @Employee
    UPDATE PRTemplate SET Principal = null WHERE Principal = @Employee
    UPDATE PRTemplate SET ProjMgr = null WHERE ProjMgr = @Employee
    UPDATE PRTemplate SET Supervisor = null WHERE Supervisor = @Employee
    UPDATE PRTemplate SET ProposalManager = null WHERE ProposalManager = @Employee
    UPDATE PRTemplate SET BusinessDeveloperLead = null WHERE BusinessDeveloperLead = @Employee
    UPDATE PRTemplate SET MarketingCoordinator = null WHERE MarketingCoordinator = @Employee
    UPDATE PRTemplate SET Biller = null WHERE Biller = @Employee
    UPDATE RPPlan SET Principal = null WHERE Principal = @Employee
    UPDATE RPPlan SET ProjMgr = null WHERE ProjMgr = @Employee
    UPDATE RPPlan SET Supervisor = null WHERE Supervisor = @Employee
    UPDATE PNPlan SET Principal = null WHERE Principal = @Employee
    UPDATE PNPlan SET ProjMgr = null WHERE ProjMgr = @Employee
    UPDATE PNPlan SET Supervisor = null WHERE Supervisor = @Employee
    UPDATE RPTask SET ProjMgr = null WHERE ProjMgr = @Employee
    UPDATE PNTask SET ProjMgr = null WHERE ProjMgr = @Employee
    UPDATE RPAssignment SET ResourceID = null WHERE ResourceID = @Employee
    UPDATE PNAssignment SET ResourceID = null WHERE ResourceID = @Employee
    UPDATE SEUser SET Employee = null WHERE Employee = @Employee
    UPDATE SF255Employees SET Employee = null WHERE Employee = @Employee
    UPDATE WorkflowActivity SET Employee = null WHERE Employee = @Employee

GO
