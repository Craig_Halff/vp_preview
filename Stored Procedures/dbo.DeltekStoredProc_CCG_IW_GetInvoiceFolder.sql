SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_IW_GetInvoiceFolder]
(@WBS1 Nvarchar(30),@context varchar(30)= NULL)
AS
BEGIN

	set @WBS1 = Replace(@WBS1,'''', '''''')
	SET NOCOUNT ON;
	if @context like 'Final%' and exists (select 'x' from syscolumns a,sysobjects b where a.id = b.id and a.name = 'custInvoiceFolderFinals' and b.name = 'ProjectCustomTabFields')
	begin
		
			exec(N'select ISNULL(case when LEN(custInvoiceFolderFinals) > 0 then custInvoiceFolderFinals else custInvoiceFolder end,'''') as InvoiceFolder from ProjectCustomTabFields where WBS1=''' + @WBS1 + ''' and WBS2='' ''')
	end
	else if exists (select 'x' from syscolumns a,sysobjects b where a.id = b.id and a.name = 'custInvoiceFolder' and b.name = 'ProjectCustomTabFields')
	begin
			exec(N'select ISNULL(custInvoiceFolder,'''') as InvoiceFolder from ProjectCustomTabFields where WBS1=''' + @WBS1 + ''' and WBS2='' ''')
	end
	else
		select '[:c$]' as InvoiceFolder
END;

GO
