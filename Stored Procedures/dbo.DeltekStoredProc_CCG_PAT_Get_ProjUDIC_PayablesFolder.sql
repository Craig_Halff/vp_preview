SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Get_ProjUDIC_PayablesFolder]
	@wbs1			Nvarchar(30)
AS
BEGIN
	-- [DeltekStoredProc_CCG_PAT_Get_ProjUDIC_PayablesFolder] '2003005.00'
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max) = N'SELECT CustPayablesFolder FROM ProjectCustomTabFields WHERE WBS1 = N''' + CAST(@wbs1 as Nvarchar) + ''' and WBS2 = N'' ''';
	EXEC (@sql);
END;

GO
