SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FormExport_DoWork]
	@FileID		varchar(100),
	@Seq		int
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE CCG_PAT_Payable
		SET FileID = @FileID
		WHERE SEQ = @Seq
END;
GO
