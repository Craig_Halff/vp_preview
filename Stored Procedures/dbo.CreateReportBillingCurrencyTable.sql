SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[CreateReportBillingCurrencyTable]
	@targetDate datetime
	, @sessionID varchar(32)
	, @CalledFromRDL varchar(1) = 'N'			-- Optional Parameter: Default is 'N'; It will be 'Y' if it's called from RDL.
as
declare
@toCurrencyCode Nvarchar(3),
@fromCurrencyCode Nvarchar(3),
@triangulationCurrencyCode Nvarchar(3),
@rate float(8),
@rate2 float(8),
@triangulationRate float(8),
@billingCurrencyDecimalPlaces int,
@billingCurrencyCurrencySymbol Nvarchar(5),
@lastRun datetime,
@enabledFromCurrenciesEOF bit,
@enabledToCurrenciesEOF bit,
@foundExchangeRate int,
@billingCurrencyCursorEOF bit,
@rateErrorMessage Nvarchar(50),
@company Nvarchar(14),
@billingCurrencyRate float(2),
@lastCompany Nvarchar(14),
@sql varchar(max),
@owner varchar(100),
@workTable varchar(100),
@tname VARCHAR(100)
 
begin

Select @lastCompany = 'dfltCompForIRPC'

	IF @CalledFromRDL = 'Y'	-- Use ## Global Temp Table for Report
		Begin
			Set @owner = ''
			Set @tname = '##wkRPC_' + @sessionID 
		End
	Else	-- Use Physical Temp Table for Applications
		Begin
		/* delete existing approver work tables that are three days old*/
			DECLARE db_cursor CURSOR FOR 
			SELECT sys.objects.name AS tname, sys.database_principals.name as owner
            FROM sys.objects inner join sys.database_principals on sys.objects.schema_id = sys.database_principals.principal_id
            WHERE DATEDIFF(DAY,sys.objects.create_date, getdate()) > 2
            and sys.objects.name like 'wkRCPC_%'

			OPEN db_cursor  
			FETCH NEXT FROM db_cursor INTO @tname, @owner  

			WHILE @@FETCH_STATUS = 0  
			BEGIN  
				SET @sql = 'DROP TABLE ' + @owner + '.' + @tname
				EXEC (@sql)

				FETCH NEXT FROM db_cursor INTO @tname, @owner  
			END  

			CLOSE db_cursor  
			DEALLOCATE db_cursor
		/* end delete */

			Set @owner = 'dbo.'
			Set @tname = 'wkRPC_' + @sessionID 
		End

	set @workTable = @owner + @tname
	IF @CalledFromRDL = 'N' and OBJECT_ID(@tname) IS NOT NULL	-- When Not from RDL and table exists, reuse object
		Begin
			begin tran
				select @sql = 'delete from ' + @workTable 
				exec (@sql)
			commit
		End
	ELSE	
		Begin
/* create temporary ReportBillingCurrency table */
			begin tran
				select @sql = 'CREATE TABLE ' + @workTable + ' (SessionID varchar(32) COLLATE database_default, Company nvarchar(14) COLLATE database_default, FromCurrencyCode nvarchar(3) COLLATE database_default, BillingCurrencyCode nvarchar(3) COLLATE database_default, Rate decimal(19, 10) DEFAULT ((0)) NOT NULL, BillingCurrencyDecimalPlaces smallint DEFAULT ((0)) NOT NULL, BillingCurrencyCurrencySymbol nvarchar(5) COLLATE database_default, [Message] nvarchar(50) COLLATE database_default, LastRun datetime NULL '
				select @sql = @sql + 'CONSTRAINT RPC_' + @sessionID + 'PK PRIMARY KEY NONCLUSTERED (SessionID ASC, Company ASC, FromCurrencyCode ASC, BillingCurrencyCode ASC) WITH FILLFACTOR = 90)'
				exec (@sql)
			commit
		End

Select @billingCurrencyRate = 1
/*enabled currencies cursor: ToCurrencyCode */
DECLARE enabledToCurrencies CURSOR FOR
Select distinct EnabledCurrencies.CurrencyCode, EnabledCurrencies.DecimalPlaces, EnabledCurrencies.CurrencySymbol 
From CFGMainData, (Select distinct CurrencyCode, DecimalPlaces, CurrencySymbol From FW_CFGEnabledCurrencies Left Join FW_CFGCurrency on FW_CFGEnabledCurrencies.CurrencyCode = FW_CFGCurrency.Code) EnabledCurrencies
Order By EnabledCurrencies.CurrencyCode

Open enabledToCurrencies

FETCH NEXT from enabledToCurrencies Into @toCurrencyCode, @billingCurrencyDecimalPlaces, @billingCurrencyCurrencySymbol
	Select @enabledToCurrenciesEOF = @@FETCH_STATUS

/* Loop */
While (@enabledToCurrenciesEOF = 0)
BEGIN

		/*enabled currencies cursor: FromCurrencyCode */
		DECLARE enabledFromCurrencies CURSOR FOR
		Select CFGMainData.Company, EnabledCurrencies.CurrencyCode, CFGMainData.TriangulationCurrencyCode From CFGMainData, (Select distinct CurrencyCode From FW_CFGEnabledCurrencies) EnabledCurrencies
		Order By CFGMainData.Company, EnabledCurrencies.CurrencyCode
 
		Open enabledFromCurrencies

		FETCH NEXT from enabledFromCurrencies Into @company, @fromCurrencyCode, @triangulationCurrencyCode
			Select @enabledFromCurrenciesEOF = @@FETCH_STATUS

		/* Loop */
		While (@enabledFromCurrenciesEOF = 0)
		BEGIN
			/* establish default values */
			Select @rate = 0
			Select @rate2 = 0
			Select @triangulationRate = 0
			Select @foundExchangeRate = 0
			Select @rateErrorMessage = ''
		
			/* first row entered into the ReportPresentationCurrencyTable will be the Presentation Currency itself */
			/* only insert once for each company */
			if @company <> @lastCompany
			begin
				select @sql = 'Insert Into ' + @workTable + ' (SessionID, Company, FromCurrencyCode, BillingCurrencyCode, Rate, BillingCurrencyDecimalPlaces, BillingCurrencyCurrencySymbol, LastRun) values'
				select @sql = @sql + '(''' + @sessionID + ''', ''' + @company + ''', ''' + @toCurrencyCode + ''', ''' + @toCurrencyCode + ''', ''' + convert(varchar,cast(@billingCurrencyRate as decimal(19,10))) + ''', ''' + convert(varchar,@billingCurrencyDecimalPlaces) + ''', ''' + IsNull(@billingCurrencyCurrencySymbol, '') + ''', ''' + convert(varchar, getDate(), 121) + ''')'
				exec (@sql)
			end

			If @fromCurrencyCode <> @toCurrencyCode
			begin

			/* look for a direct exchange rate */
			Execute @foundExchangeRate = GetExchangeRate @fromCurrencyCode, @toCurrencyCode,@targetDate, @rate output, 0
 
			if @foundExchangeRate = 1
			 begin 
				 select @sql = 'Insert Into ' + @workTable + ' (SessionID, Company, FromCurrencyCode, BillingCurrencyCode, Rate, BillingCurrencyDecimalPlaces, BillingCurrencyCurrencySymbol, LastRun) values'
				 select @sql = @sql + '(''' + @sessionID + ''', ''' + @company + ''', ''' + @fromCurrencyCode + ''', ''' + @toCurrencyCode + ''', ''' + convert(varchar,cast(@rate as decimal(19,10))) + ''', ''' + convert(varchar,@billingCurrencyDecimalPlaces) + ''', ''' + IsNull(@billingCurrencyCurrencySymbol, '') + ''', ''' + convert(varchar, getDate(), 121) + ''')'
				 exec (@sql)
			 end
	 

			/* look for an inverse exchange rate */
			if @foundExchangeRate = 0
			begin
				Execute @foundExchangeRate = GetExchangeRate @toCurrencyCode, @fromCurrencyCode, @targetDate, @rate output, 1
 
				if @foundExchangeRate = 1
		 
				begin
					 select @sql = 'Insert Into ' + @workTable + ' (SessionID, Company, FromCurrencyCode, BillingCurrencyCode, Rate, BillingCurrencyDecimalPlaces, BillingCurrencyCurrencySymbol, LastRun) values'
					 select @sql = @sql + '(''' + @sessionID + ''', ''' + @company + ''', ''' + @fromCurrencyCode + ''', ''' + @toCurrencyCode + ''', ''' + convert(varchar,cast(@rate as decimal(19,10))) + ''', ''' + convert(varchar,@billingCurrencyDecimalPlaces) + ''', ''' + IsNull(@billingCurrencyCurrencySymbol, '') + ''', ''' + convert(varchar, getDate(), 121) + ''')'
					 exec (@sql)
				end

				/* look for a triangulation exchange rate */

				if @foundExchangeRate = 0
				begin
					if @triangulationCurrencyCode = @fromCurrencyCode /* error in the vb class multicurrency when this occurs but will set the rate to 0 */
					begin
						Select @rate = 0
						Select @foundExchangeRate = 1
						Select @rateErrorMessage = 'Exchange rate not found from ' + @fromCurrencyCode + ' to ' + @toCurrencyCode
					end

					if @triangulationCurrencyCode = @toCurrencyCode /* error in the vb class multicurrency when this occurs but will set the rate to 0 */
					begin
						Select @rate = 0
						Select @foundExchangeRate = 1
						Select @rateErrorMessage = 'Exchange rate not found from ' + @fromCurrencyCode + ' to ' + @toCurrencyCode
					end

					if @triangulationCurrencyCode = '' /* error in the vb class multicurrency when this occurs but will set the rate to 0 */
					begin
						Select @rate = 0
						Select @foundExchangeRate = 1
						Select @rateErrorMessage = 'Exchange rate not found from ' + @fromCurrencyCode + ' to ' + @toCurrencyCode
					end

					if @foundExchangeRate = 1 
					begin
						select @sql = 'Insert Into ' + @workTable + ' (SessionID, Company, FromCurrencyCode, BillingCurrencyCode, Rate, BillingCurrencyDecimalPlaces, BillingCurrencyCurrencySymbol, Message, LastRun) values'
			 			select @sql = @sql + '(''' + @sessionID + ''', ''' + @company + ''', ''' + @fromCurrencyCode + ''', ''' + @toCurrencyCode + ''', ''' + convert(varchar,cast(@rate as decimal(19,10)))+ ''', ''' + convert(varchar,@billingCurrencyDecimalPlaces) + ''', ''' + IsNull(@billingCurrencyCurrencySymbol, '') + ''', ''' + IsNull(@rateErrorMessage, '') + ''', ''' + convert(varchar, getDate(), 121) + ''')'
			 			exec (@sql)
					end

					/* leg one - toCurrencyCode = triangulationCurrencyCode */
					if @foundExchangeRate = 0
					begin
						Select @rateErrorMessage = Null

						Execute @foundExchangeRate = GetExchangeRate @fromCurrencyCode, @triangulationCurrencyCode, @targetDate, @rate output, 0
				
						if @foundExchangeRate = 0
						begin
							/* look for an inverse exchange rate */
							Execute @foundExchangeRate = GetExchangeRate @triangulationCurrencyCode, @fromCurrencyCode, @targetDate, @rate output, 1
						end

						if @foundExchangeRate = 0
				 			Select @rateErrorMessage = 'Exchange rate not found from ' + @fromCurrencyCode + ' to ' + @toCurrencyCode

						/* leg two - fromCurrencyCode - triangulationCurrencyCode and toCurrencyCode = presentationCurrencyCode */
						Execute @foundExchangeRate = GetExchangeRate @triangulationCurrencyCode, @toCurrencyCode, @targetDate, @rate2 output, 0
				
						if @foundExchangeRate = 0
						begin
							/* look for an inverse exchange rate */
							Execute @foundExchangeRate = GetExchangeRate @toCurrencyCode, @triangulationCurrencyCode, @targetDate, @rate2 output, 1
						end

						if @foundExchangeRate = 0
							Select @rateErrorMessage = 'Exchange rate not found from ' + @fromCurrencyCode + ' to ' + @toCurrencyCode
			
						Select @triangulationRate = @rate * @rate2
					
						select @sql = 'Insert Into ' + @workTable + ' (SessionID, Company, FromCurrencyCode, BillingCurrencyCode, Rate, BillingCurrencyDecimalPlaces, BillingCurrencyCurrencySymbol, Message, LastRun) values'
			 			select @sql = @sql + '(''' + @sessionID + ''', ''' + @company + ''', ''' + @fromCurrencyCode + ''', ''' + @toCurrencyCode + ''', ''' + convert(varchar,cast(@triangulationRate as decimal(19,10))) + ''', ''' + convert(varchar,@billingCurrencyDecimalPlaces) + ''', ''' + IsNull(@billingCurrencyCurrencySymbol, '') + ''', ''' + IsNull(@rateErrorMessage, '') + ''', ''' + convert(varchar, getDate(), 121) + ''')'
			 			exec (@sql)
					end
				end
			end

			end --If @fromCurrencyCode <> @toCurrencyCode
	 
			Select @lastCompany = @company
			FETCH NEXT from enabledFromCurrencies Into @company, @fromCurrencyCode, @triangulationCurrencyCode
			Select @enabledFromCurrenciesEOF = @@FETCH_STATUS


		END

			Select @lastCompany = 'dfltCompForIRPC'
			Close enabledFromCurrencies
			Deallocate enabledFromCurrencies

	FETCH NEXT from enabledToCurrencies Into @toCurrencyCode, @billingCurrencyDecimalPlaces, @billingCurrencyCurrencySymbol
	Select @enabledToCurrenciesEOF = @@FETCH_STATUS


END
	
	Close enabledToCurrencies
	Deallocate enabledToCurrencies
	
END -- CreateReportBillingCurrencyTable
GO
