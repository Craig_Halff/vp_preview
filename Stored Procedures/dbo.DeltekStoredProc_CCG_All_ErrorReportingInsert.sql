SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_All_ErrorReportingInsert] ( @ErrorNumber varchar(64)= NULL, @lineNumber int= NULL, @errorMessage varchar(max)= NULL, @details varchar(max)= NULL, @product varchar(64)= NULL, @productVersion varchar(64)= NULL, @user nvarchar(32)= NULL, @SessionID varchar(64)= NULL, @ComputerName varchar(64)= NULL)
             AS EXEC spCCG_All_ErrorReportingInsert @ErrorNumber,@lineNumber,@errorMessage,@details,@product,@productVersion,@user,@SessionID,@ComputerName
GO
