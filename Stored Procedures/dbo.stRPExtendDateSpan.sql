SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPExtendDateSpan]
  @strPlanID varchar(32),
  @bitCalledFromRM bit = 0
AS

BEGIN -- Procedure stRPExtendDateSpan

  SET NOCOUNT ON

  DECLARE @iLevel int

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @iLevel = MAX(OutlineLevel) - 1 FROM PNTask WHERE PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION

    -- Extend StartDate and EndDate of PNAssignment rows when Start/End Dates of TPD rows were extended.

    UPDATE A SET 
      StartDate = CASE WHEN X.StartDate < A.StartDate THEN X.StartDate ELSE A.StartDate END, 
      EndDate = CASE WHEN X.EndDate > A.EndDate THEN X.EndDate ELSE A.EndDate END
      FROM PNAssignment AS A 
        INNER JOIN (
          SELECT PlanID, TaskID, AssignmentID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
            FROM PNPlannedLabor
            WHERE PlanID = @strPlanID AND AssignmentID IS NOT NULL
            GROUP BY PlanID, TaskID, AssignmentID
        ) AS X 
        ON A.PlanID = X.PlanID AND A.TaskID = X.TaskID AND A.AssignmentID = X.AssignmentID
        WHERE A.PlanID = @strPlanID 
          AND (X.StartDate < A.StartDate OR X.EndDate > A.EndDate)

    -- Extend StartDate and EndDate of PNExpense rows when Start/End Dates of TPD rows were extended.

    UPDATE E SET 
      StartDate = CASE WHEN X.StartDate < E.StartDate THEN X.StartDate ELSE E.StartDate END, 
      EndDate = CASE WHEN X.EndDate > E.EndDate THEN X.EndDate ELSE E.EndDate END
      FROM PNExpense AS E 
        INNER JOIN (
          SELECT PlanID, TaskID, ExpenseID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
            FROM PNPlannedExpenses
            WHERE PlanID = @strPlanID AND ExpenseID IS NOT NULL
            GROUP BY PlanID, TaskID, ExpenseID
        ) AS X 
        ON E.PlanID = X.PlanID AND E.TaskID = X.TaskID AND E.ExpenseID = X.ExpenseID
        WHERE E.PlanID = @strPlanID 
          AND (X.StartDate < E.StartDate OR X.EndDate > E.EndDate)

    -- Extend StartDate and EndDate of PNExpense rows when Start/End Dates of TPD rows were extended.

    UPDATE C SET 
      StartDate = CASE WHEN X.StartDate < C.StartDate THEN X.StartDate ELSE C.StartDate END, 
      EndDate = CASE WHEN X.EndDate > C.EndDate THEN X.EndDate ELSE C.EndDate END
      FROM PNConsultant AS C 
        INNER JOIN (
          SELECT PlanID, TaskID, ConsultantID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
            FROM PNPlannedConsultant
            WHERE PlanID = @strPlanID AND ConsultantID IS NOT NULL
            GROUP BY PlanID, TaskID, ConsultantID
        ) AS X 
        ON C.PlanID = X.PlanID AND C.TaskID = X.TaskID AND C.ConsultantID = X.ConsultantID
        WHERE C.PlanID = @strPlanID 
          AND (X.StartDate < C.StartDate OR X.EndDate > C.EndDate)

    -- Extend StartDate and EndDate of PNTask rows when Start/End Dates of PNAssignment, PNExpense, or PNConsultant rows were extended.

	DECLARE @strUserName nvarchar(20) = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

    UPDATE T SET 
      StartDate = CASE WHEN X.StartDate < T.StartDate THEN X.StartDate ELSE T.StartDate END, 
      EndDate = CASE WHEN X.EndDate > T.EndDate THEN X.EndDate ELSE T.EndDate END,
	  ModDate=LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19),
	  ModUser=@strUserName
      FROM PNTask AS T 
        INNER JOIN (
          SELECT PlanID, TaskID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
            FROM PNAssignment
            WHERE PlanID = @strPlanID
            GROUP BY PlanID, TaskID
          UNION
          SELECT PlanID, TaskID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
            FROM PNExpense
            WHERE PlanID = @strPlanID
            GROUP BY PlanID, TaskID
          UNION
          SELECT PlanID, TaskID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate 
            FROM PNConsultant
            WHERE PlanID = @strPlanID
            GROUP BY PlanID, TaskID
        ) AS X 
        ON T.PlanID = X.PlanID AND T.TaskID = X.TaskID
        WHERE T.PlanID = @strPlanID 
          AND (X.StartDate < T.StartDate OR X.EndDate > T.EndDate)

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Traverse upward from bottom level.
    -- Update StartDate and EndDate of parent rows.

    WHILE (@iLevel >= 0) 
      BEGIN

        UPDATE T SET
          StartDate = CASE WHEN X.StartDate < T.StartDate THEN X.StartDate ELSE T.StartDate END, 
          EndDate = CASE WHEN X.EndDate > T.EndDate THEN X.EndDate ELSE T.EndDate END,
		  ModDate=LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19),
		  ModUser=@strUserName
          FROM PNTask AS T
            INNER JOIN (
              SELECT 
                PlanID, 
                ParentOutlineNumber, 
                MIN(StartDate) AS StartDate, 
                MAX(EndDate) AS EndDate 
                FROM PNTask 
                WHERE PNTask.PlanID = @strPlanID 
                GROUP BY PlanID, ParentOutlineNumber
            ) AS X ON T.PlanID = X.PlanID AND T.OutlineNumber = X.ParentOutlineNumber
          WHERE T.PlanID = @strPlanID AND T.OutlineLevel = @iLevel
            AND (X.StartDate < T.StartDate OR X.EndDate > T.EndDate)

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        SET @iLevel = @iLevel - 1

      END

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- If this SP was called from Resource Management then need to update RP tables.

    IF (@bitCalledFromRM = 1)
      BEGIN

        UPDATE RA SET
          StartDate = CASE WHEN RA.StartDate <> PA.StartDate THEN PA.StartDate ELSE RA.StartDate END, 
          EndDate = CASE WHEN RA.EndDate <> PA.EndDate THEN PA.EndDate ELSE RA.EndDate END
          FROM RPAssignment AS RA
            INNER JOIN PNAssignment AS PA
              ON RA.PlanID = PA.PlanID AND RA.TaskID = PA.TaskID AND RA.AssignmentID = PA.AssignmentID
          WHERE RA.PlanID = @strPlanID 
            AND (RA.StartDate <> PA.StartDate OR RA.EndDate <> PA.EndDate)
 
        UPDATE RE SET
          StartDate = CASE WHEN RE.StartDate <> PE.StartDate THEN PE.StartDate ELSE RE.StartDate END, 
          EndDate = CASE WHEN RE.EndDate <> PE.EndDate THEN PE.EndDate ELSE RE.EndDate END
          FROM RPExpense AS RE
            INNER JOIN PNExpense AS PE
              ON RE.PlanID = PE.PlanID AND RE.TaskID = PE.TaskID AND RE.ExpenseID = PE.ExpenseID
          WHERE RE.PlanID = @strPlanID 
            AND (RE.StartDate <> PE.StartDate OR RE.EndDate <> PE.EndDate)

        UPDATE RC SET
          StartDate = CASE WHEN RC.StartDate <> PC.StartDate THEN PC.StartDate ELSE RC.StartDate END, 
          EndDate = CASE WHEN RC.EndDate <> PC.EndDate THEN PC.EndDate ELSE RC.EndDate END
          FROM RPConsultant AS RC
            INNER JOIN PNConsultant AS PC
              ON RC.PlanID = PC.PlanID AND RC.TaskID = PC.TaskID AND RC.ConsultantID = PC.ConsultantID
          WHERE RC.PlanID = @strPlanID 
            AND (RC.StartDate <> PC.StartDate OR RC.EndDate <> PC.EndDate)

         UPDATE RT SET
          StartDate = CASE WHEN RT.StartDate <> PT.StartDate THEN PT.StartDate ELSE RT.StartDate END, 
          EndDate = CASE WHEN RT.EndDate <> PT.EndDate THEN PT.EndDate ELSE RT.EndDate END
          FROM RPTask AS RT
            INNER JOIN PNTask AS PT
              ON RT.PlanID = PT.PlanID AND RT.TaskID = PT.TaskID 
          WHERE RT.PlanID = @strPlanID 
            AND (RT.StartDate <> PT.StartDate OR RT.EndDate <> PT.EndDate)

      END /* END IF (@bitCalledFromRM = 1) */

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  COMMIT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF
     
END -- stRPExtendDateSpan
GO
