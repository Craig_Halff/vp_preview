SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_GetSQLCustomColumns] ( @emptyFields int, @levels int, @role varchar(32), @wsEnabled int, @quickLoad int, @sqlPart varchar(max), @outerApplies varchar(max), @sqlGroupBy varchar(max))
             AS EXEC spCCG_EI_GetSQLCustomColumns @emptyFields,@levels,@role,@wsEnabled,@quickLoad,@sqlPart,@outerApplies,@sqlGroupBy
GO
