SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCGKeyConvertProject] (@OldWBS1 Nvarchar(30), @NewWBS1 Nvarchar(30))
WITH ENCRYPTION
AS
BEGIN
/*
	Copyright (c) 2017 Central Consulting Group. All rights reserved.

	select * from CCG_EI_Messages where WBS1 in ('2003005.00','2003005.xx')
	select * from CCG_EI_History  where WBS1 in ('2003005.00','2003005.xx')
	select * from CCG_EI          where WBS1 in ('2003005.00','2003005.xx')
*/
	set nocount on

	BEGIN TRY

		begin transaction

		update CCG_EI_Messages set WBS1=@NewWBS1 where WBS1=@OldWBS1
		update CCG_EI_History  set WBS1=@NewWBS1 where WBS1=@OldWBS1
		update CCG_EI          set WBS1=@NewWBS1 where WBS1=@OldWBS1
		update CCG_EI_CustomColumns set WBS1=@NewWBS1 where WBS1=@OldWBS1
		update CCG_EI_HistoryUpdateColumns set WBS1=@NewWBS1 where WBS1=@OldWBS1

		select 0 as Result, '' as ErrorMessage
		commit transaction

	END TRY
	BEGIN CATCH
		/* SQL found some error - return the message and number: */
		select -1 as Result, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage
		rollback transaction
	END CATCH

END
GO
