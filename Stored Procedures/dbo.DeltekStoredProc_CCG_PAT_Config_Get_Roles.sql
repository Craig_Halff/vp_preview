SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_Get_Roles]
AS
BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_PAT_Config_Get_Roles]
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	Select [Role] as [Key], [Role] as Value
		FROM SE
		ORDER BY 1;
END;
GO
