SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PNRefreshRates]
  @strPlanID varchar(32),
  @strRefreshLabCostRates varchar(1) = 'Y',
  @strRefreshLabBillRates varchar(1) = 'Y',
  @strRefreshExpBillRates varchar(1) = 'Y',
  @strRefreshConBillRates varchar(1) = 'Y'

AS

BEGIN -- Procedure PNRefreshRates

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to refresh rates in a Navigator Plan.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON

  DECLARE @intHrDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intRtCostDecimals int
  DECLARE @intRtBillDecimals int
  DECLARE @intLabRevDecimals int
  DECLARE @intECURevDecimals int

  DECLARE @strCompany Nvarchar(14)
  DECLARE @strNextTaskID varchar(32)
  DECLARE @strRecalLab varchar(1)
  
  DECLARE @sintGRMethod smallint
  DECLARE @sintCostRtMethod smallint
  DECLARE @sintBillingRtMethod smallint
  
  DECLARE @intCostRtTableNo int
  DECLARE @intBillingRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int

  DECLARE @sintExpBillRtMethod smallint
  DECLARE @intExpBillRtTableNo int
  DECLARE @decExpBillMultiplier decimal(19,4)

  DECLARE @sintConBillRtMethod smallint
  DECLARE @intConBillRtTableNo int
  DECLARE @decConBillMultiplier decimal(19,4)

  DECLARE csrPNExpense CURSOR FOR
    SELECT DISTINCT TaskID FROM PNExpense WHERE PlanID = @strPlanID

  DECLARE csrPNConsultant CURSOR FOR
    SELECT DISTINCT TaskID FROM PNConsultant WHERE PlanID = @strPlanID
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get settings from PNPlan.

  SELECT 
    @strCompany = P.Company,
    @sintGRMethod = P.GRMethod,
    @intGenResTableNo = P.GenResTableNo,
    @intGRBillTableNo = P.GRBillTableNo,
    @sintCostRtMethod = P.CostRtMethod,
    @sintBillingRtMethod = P.BillingRtMethod,
    @intCostRtTableNo = P.CostRtTableNo,
    @intBillingRtTableNo = P.BillingRtTableNo,
    @sintExpBillRtMethod = P.ExpBillRtMethod,
    @intExpBillRtTableNo = P.ExpBillRtTableNo,
    @decExpBillMultiplier = P.ExpBillMultiplier,
    @sintConBillRtMethod = P.ConBillRtMethod,
    @intConBillRtTableNo = P.ConBillRtTableNo,
    @decConBillMultiplier = P.ConBillMultiplier,
    @intHrDecimals = 2,
    @intAmtCostDecimals = 4, 
    @intAmtBillDecimals = 4, 
    @intRtCostDecimals = 4, 
    @intRtBillDecimals = 4, 
    @intLabRevDecimals = 4, 
    @intECURevDecimals = 4
    FROM PNPlan AS P 
    WHERE P.PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Update override rates for Assignment rows.

  UPDATE PNAssignment SET
    CostRate = CASE WHEN @strRefreshLabCostRates = 'Y'
      THEN
        dbo.PN$OvrLabRate(
          'C',
          @strPlanID,
          ResourceID,
          Category,
          NULL,
          GRLBCD 
        )
      ELSE CostRate END,
    BillingRate = CASE WHEN @strRefreshLabBillRates = 'Y'
      THEN
        dbo.PN$OvrLabRate(
          'B',
          @strPlanID,
          ResourceID,
          Category,
          NULL,
          GRLBCD
        )
      ELSE BillingRate END,
    ModDate = GETUTCDATE()
    WHERE PlanID = @strPlanID
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  UPDATE PNPlannedLabor SET
    CostRate =  CASE WHEN @strRefreshLabCostRates = 'Y'
      THEN
        dbo.RP$LabRate
          (A.ResourceID,
           ISNULL(A.Category, 0),
           A.GRLBCD,
           NULL,
           @sintCostRtMethod,
           @intCostRtTableNo,
           @sintGRMethod,
           @intGenResTableNo,
           PNPlannedLabor.StartDate,
           PNPlannedLabor.EndDate,
           @intRtCostDecimals,
           0,
           ISNULL(A.CostRate, 0)
          )
      ELSE PNPlannedLabor.CostRate END,
    BillingRate =  CASE WHEN @strRefreshLabBillRates = 'Y'
      THEN
        dbo.RP$LabRate
          (A.ResourceID,
           ISNULL(A.Category, 0),
           A.GRLBCD,
           NULL,
           @sintBillingRtMethod,
           @intBillingRtTableNo,
           @sintGRMethod,
           @intGRBillTableNo,
           PNPlannedLabor.StartDate,
           PNPlannedLabor.EndDate,
           @intRtBillDecimals,
           0,
           ISNULL(A.BillingRate, 0)
          )
      ELSE PNPlannedLabor.BillingRate END,
    ModDate = GETUTCDATE()
    FROM PNPlannedLabor
      INNER JOIN PNAssignment AS A ON PNPlannedLabor.PlanID = A.PlanID AND PNPlannedLabor.TaskID = A.TaskID AND PNPlannedLabor.AssignmentID = A.AssignmentID
      LEFT JOIN EM ON A.ResourceID = EM.Employee
    WHERE PNPlannedLabor.PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strRefreshExpBillRates = 'Y')
    BEGIN

      -- Update Expense Rates and Bill Amounts.

      UPDATE PNExpense SET
        ExpBillRate = 
          dbo.PN$ExpConMult(
            Account, 
            @sintExpBillRtMethod, 
            @intExpBillRtTableNo, 
            @decExpBillMultiplier
          ),
        PlannedExpBill = PlannedExpCost *
          dbo.PN$ExpConMult(
            Account, 
            @sintExpBillRtMethod, 
            @intExpBillRtTableNo, 
            @decExpBillMultiplier
          )
        WHERE PlanID = @strPlanID

      -- Spread Expense Amounts

      OPEN csrPNExpense

      FETCH NEXT FROM csrPNExpense INTO @strNextTaskID

      WHILE (@@FETCH_STATUS = 0)
        BEGIN
          EXECUTE dbo.PNSpreadExpTPD @strNextTaskID
          FETCH NEXT FROM csrPNExpense INTO @strNextTaskID
        END -- While

      CLOSE csrPNExpense
      DEALLOCATE csrPNExpense

    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strRefreshConBillRates = 'Y')
    BEGIN

      -- Update Consultant Rates and Bill Amounts.

      UPDATE PNConsultant SET
        ConBillRate = 
          dbo.PN$ExpConMult(
            Account, 
            @sintConBillRtMethod, 
            @intConBillRtTableNo, 
            @decConBillMultiplier
          ),
        PlannedConBill = PlannedConCost *
          dbo.PN$ExpConMult(
            Account, 
            @sintConBillRtMethod, 
            @intConBillRtTableNo, 
            @decConBillMultiplier
          )
        WHERE PlanID = @strPlanID

      -- Spread Consultant Amounts

      OPEN csrPNConsultant

      FETCH NEXT FROM csrPNConsultant INTO @strNextTaskID

      WHILE (@@FETCH_STATUS = 0)
        BEGIN
          EXECUTE dbo.PNSpreadConTPD @strNextTaskID
          FETCH NEXT FROM csrPNConsultant INTO @strNextTaskID
        END -- While

      CLOSE csrPNConsultant
      DEALLOCATE csrPNConsultant

    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Recalculate the entire Plan.

  SET @strRecalLab = CASE WHEN (@strRefreshLabCostRates = 'Y' OR @strRefreshLabBillRates = 'Y') THEN 'Y' ELSE 'N' END

  EXECUTE dbo.PNDeleteSummaryTPD @strPlanID, @strRecalLab, @strRefreshExpBillRates, @strRefreshConBillRates
  EXECUTE dbo.PNCalcTPD @strPlanID, @strRecalLab, @strRefreshExpBillRates, @strRefreshConBillRates
  EXECUTE dbo.PNSumUpTPD @strPlanID, @strRecalLab, @strRefreshExpBillRates, @strRefreshConBillRates

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF

END -- PNRefreshRates
GO
