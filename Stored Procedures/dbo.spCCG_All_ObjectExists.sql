SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_All_ObjectExists] (
	@objectName	varchar(512),
	@columnName	Nvarchar(500)
) 
AS BEGIN
	-- EXEC [dbo].[spCCG_All_ObjectExists] 'spCCG_All_ObjectExists', '@objectName'
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 'Y' as [Exists]
		FROM syscolumns c, sysobjects o
		WHERE o.id = OBJECT_ID(N'[dbo].[' + @objectName + ']') and c.name = @columnName and c.id = o.id
END
GO
