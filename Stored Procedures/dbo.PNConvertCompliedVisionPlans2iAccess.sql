SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PNConvertCompliedVisionPlans2iAccess]
AS

BEGIN -- Procedure PNConvertCompliedVisionPlans2iAccess

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to convert Plans from Vision format to iAccess format. 
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @strPlanID varchar(32)
  DECLARE @strWBS1 nvarchar(30)

  DECLARE @tabPlan TABLE (
    PlanID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default
    UNIQUE(PlanID)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find Vision Plans that are iAcces-Compliant.
  -- Must use a temp table here otherwise the cursor will get confused when Plans are being changed inside a loop. 

  INSERT @tabPlan(
    PlanID,
    WBS1
  )
    SELECT DISTINCT 
      RP.PlanID AS PlanID,
      RP.WBS1 AS WBS1
      FROM RPPlan AS RP
        INNER JOIN PR ON RP.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
        LEFT JOIN PNPlan AS PN ON RP.PlanID = PN.PlanID 
        CROSS APPLY dbo.PM$tabPlanValidate(RP.PlanID) AS V
      WHERE RP.UtilizationIncludeFlg = 'Y'
        AND PN.PlanID IS NULL
        AND V.MappedToMultipleProjs = 0
        AND V.HasMultipleScales = 0
        AND V.HasIllegalLevel = 0
        AND V.HasResourceAtNonLeaf = 0
        AND V.HasIllegalCalendarScale = 0
        AND V.HasDuplicateResources = 0
        AND V.HasOverflowedTPD = 0
        AND V.HasExpenseAtMultLevels = 0
        AND V.HasConsultantAtMultLevels = 0
        AND V.HasPlannedUnits = 0
        AND V.HasIllegalCurrency = 0
        AND V.HasUnmappedWBSRows = 0
        AND RP.PlanID NOT IN (
          SELECT PlanID
            FROM (
              SELECT
                T.PlanID,
                PR.WBS1 AS WBS1, 
                COUNT(*) OVER (PARTITION BY PR.WBS1) AS CountRows
                FROM (
                  SELECT DISTINCT
                    XT.PlanID,
                    XT.WBS1,
                    XT.WBS2,
                    XT.WBS3
                    FROM RPTask AS XT 
                    WHERE XT.WBSType = 'WBS1' AND XT.WBS2 IS NULL AND XT.WBS3 IS NULL
                ) AS T
                  INNER JOIN PR ON T.WBS1 = PR.WBS1 AND ISNULL(T.WBS2, ' ') = PR.WBS2 AND ISNULL(T.WBS3, ' ') = PR.WBS3
                WHERE PR.WBS2 = ' ' AND PR.WBS3 = ' '
            ) AS X
            WHERE CountRows > 1
        )

  DECLARE csr20180501_RPPlan CURSOR LOCAL FAST_FORWARD FOR
    SELECT PlanID, WBS1
      FROM @tabPlan AS RP

  OPEN csr20180501_RPPlan
  FETCH NEXT FROM csr20180501_RPPlan INTO @strPlanID, @strWBS1

  WHILE (@@FETCH_STATUS = 0)
    BEGIN

      PRINT '>>> Start Processing PlanID = "' + @strPlanID + '", WBS1 = "' + @strWBS1 + '"'
      EXECUTE dbo.PNConvertOneVisionPlan2iAccess @strPlanID
      PRINT '<<< End Processing PlanID = "' + @strPlanID + '", WBS1 = "' + @strWBS1 + '"'

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      FETCH NEXT FROM csr20180501_RPPlan INTO @strPlanID, @strWBS1

    END -- While

  CLOSE csr20180501_RPPlan
  DEALLOCATE csr20180501_RPPlan

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

END -- PNConvertCompliedVisionPlans2iAccess
GO
