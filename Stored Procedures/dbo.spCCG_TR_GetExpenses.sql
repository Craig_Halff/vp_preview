SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_TR_GetExpenses](@WBS1 varchar(32), @WBS2 varchar(7), @WBS3 varchar(7), @ThruDate varchar(32))
AS BEGIN

	set nocount on
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	select tr.Seq, d.Org, PR1.ProjectCurrencyCode as PCC, PR1.BillingCurrencyCode as BCC,
		IsNull(tr.NewBillStatus,d.BillStatus) as BillStatus, IsNull(tr.NewBillStatus,d.BillStatus) as BillStatusDB, d.BillStatus as BillStatusOrig, 
		
		IsNull(tr.ModComment,Case When a.BillStatus='H' and d.BillStatus='B' Then null Else ISNULL(case when tr.OriginalPkey is null then a.ModificationComment else null end,'') end) as ModComment, 
		Case When CA.Type in (5,6) Then 'True' Else 'False' End as Billable, 
		IsNull(tr.NewWBS1,d.WBS1) as WBS1, IsNull(tr.NewWBS1,d.WBS1) as WBS1DB, d.WBS1 as WBS1Orig, IsNull(PR1tr.Name,PR1.Name) as WBS1Name, PR1.Name as WBS1NameOrig,
		IsNull(tr.NewWBS2,d.WBS2) as WBS2, IsNull(tr.NewWBS2,d.WBS2) as WBS2DB, d.WBS2 as WBS2Orig, IsNull(PR1tr.Name,PR2.Name) as WBS2Name, PR2.Name as WBS2NameOrig,
		IsNull(tr.NewWBS3,d.WBS3) as WBS3, IsNull(tr.NewWBS3,d.WBS3) as WBS3DB, d.WBS3 as WBS3Orig, IsNull(PR1tr.Name,PR3.Name) as WBS3Name, PR3.Name as WBS3NameOrig,
		
		IsNull(tr.NewTransDate,d.TransDate) as TransDate, IsNull(tr.NewTransDate,d.TransDate) as TransDateDB, d.TransDate as TransDateOrig,
		d.Account, CA.Name as AccountName, d.TransType, d.RefNo,
		IsNull(IsNull(tr.ApprComment,a.ApprovalComment),'') as ApprComment,
		IsNull(tr.NewCost,d.AmountBillingCurrency) as Cost, 0.0 as CostDB, d.AmountBillingCurrency as CostOrig,
		d.BillExt,
		IsNull(tr.NewDesc1,d.Desc1) as Desc1, IsNull(tr.NewDesc1,d.Desc1) as Desc1DB, d.Desc1 as Desc1Orig,
		IsNull(tr.NewDesc2,d.Desc2) as Desc2, IsNull(tr.NewDesc2,d.Desc2) as Desc2DB, d.Desc2 as Desc2Orig,
		d.TableName AS OriginalTable, d.Period as OriginalPeriod, d.PostSeq as OriginalPostSeq, d.PKey as OriginalPKey, 
		Case When tr.NewWBS1 is null and tr.NewWBS2 is null and tr.NewWBS3 is null Then d.XferWBS1 Else d.WBS1 End as XferWBS1, 
		Case When tr.NewWBS1 is null and tr.NewWBS2 is null and tr.NewWBS3 is null Then d.XferWBS2 Else d.WBS2 End as XferWBS2, 
		Case When tr.NewWBS1 is null and tr.NewWBS2 is null and tr.NewWBS3 is null Then d.XferWBS3 Else d.WBS3 End as XferWBS3, 
		Case When tr.NewWBS1 is null and tr.NewWBS2 is null and tr.NewWBS3 is null Then d.XferAccount Else null End as XferAccount,  
		d.Vendor, tr.[Status], PR1.ChargeType, a.IncludeFuture, a.BillNonBillable, a.SubmittedBy, a.SubmittedDate, a.ApprovedBy, a.ApprovedDate, 
		Case When IsNull(a.SubmittedBy,'')='' Then 'False' Else 'True' End as Submitted,
		Case When IsNull(a.ApprovedBy, '')='' Then 'False' Else 'True' End as Approved,  convert(bit,0) as Approve,
		IsNull(tr.ApprComment,ISNULL(case when tr.OriginalPkey is null then a.ApprovalComment else null end,'')) as ApprovalComment, a.BillStatus as ApprovedBillStatus,
		'' as HistoryButton, Case When EverModified.Period is null Then 'N' Else 'Y' End as HasBeenModified, 
		Case When CA.Type in (5,6) Then 'Y' Else 'N' End  as BillExtend
	from (
		--exec spCCG_TR_GetExpenses '1999009.00',' ',' '
		select 'LedgerAP' as TableName, Period, PostSeq, PKey, Org,
			WBS1, WBS2, WBS3, TransDate, BillStatus, Account, TransType, RefNo, Desc1, Desc2, Vendor,
			AmountBillingCurrency, BillExt, XferWBS1, XferWBS2, XferWBS3, XferAccount			
		from LedgerAP l
		where l.WBS1 = @WBS1 and l.BillStatus in ('B','H','W') and l.SuppressBill <> 'Y' and l.ProjectCost = 'Y' 
		  and not ((l.TransType = 'UN') and (l.SubType = 'B')) 
		   
		  and (@WBS2=' ' or @WBS2=l.WBS2) and (@WBS3=' ' or @WBS3=l.WBS3)
		UNION ALL
		select 'LedgerAR' as TableName, Period, PostSeq, PKey, Org,
			WBS1, WBS2, WBS3, TransDate, BillStatus, Account, TransType, RefNo, Desc1, Desc2, Vendor,
			AmountBillingCurrency, BillExt, XferWBS1, XferWBS2, XferWBS3, XferAccount			
		from LedgerAR l
		where l.WBS1 = @WBS1 and l.BillStatus in ('B','H','W') and l.SuppressBill <> 'Y' and l.ProjectCost = 'Y' 
		  and not ((l.TransType = 'UN') and (l.SubType = 'B')) 
		   
		  and (@WBS2=' ' or @WBS2=l.WBS2) and (@WBS3=' ' or @WBS3=l.WBS3)
		UNION ALL
		select 'LedgerEX' as TableName, Period, PostSeq, PKey, Org,
			WBS1, WBS2, WBS3, TransDate, BillStatus, Account, TransType, RefNo, Desc1, Desc2, Vendor,
			AmountBillingCurrency, BillExt, XferWBS1, XferWBS2, XferWBS3, XferAccount			
		from LedgerEX l
		where l.WBS1 = @WBS1 and l.BillStatus in ('B','H','W') and l.SuppressBill <> 'Y' and l.ProjectCost = 'Y' 
		  and not ((l.TransType = 'UN') and (l.SubType = 'B')) 
		   
		  and (@WBS2=' ' or @WBS2=l.WBS2) and (@WBS3=' ' or @WBS3=l.WBS3)
		UNION ALL
		select 'LedgerMisc' as TableName, Period, PostSeq, PKey, Org,
			WBS1, WBS2, WBS3, TransDate, BillStatus, Account, TransType, RefNo, Desc1, Desc2, Vendor,
			AmountBillingCurrency, BillExt, XferWBS1, XferWBS2, XferWBS3, XferAccount			
		from LedgerMisc l
		where l.WBS1 = @WBS1 and l.BillStatus in ('B','H','W') and l.SuppressBill <> 'Y' and l.ProjectCost = 'Y' 
		  and not ((l.TransType = 'UN') and (l.SubType = 'B')) 
		   
		  and (@WBS2=' ' or @WBS2=l.WBS2) and (@WBS3=' ' or @WBS3=l.WBS3)
		UNION ALL
		select 'BIED' as TableName, Period, PostSeq, PKey, Org,
			WBS1, WBS2, WBS3, TransDate, BillStatus, Account, TransType, RefNo, Desc1, Desc2, Vendor,
			AmountBillingCurrency, BillExt, XferWBS1, XferWBS2, XferWBS3, XferAccount			
		from BIED l
		where l.WBS1 = @WBS1 and l.BillStatus in ('B','H','W') and l.SuppressBill <> 'Y' and l.ProjectCost = 'Y' 
		  and not ((l.TransType = 'UN') and (l.SubType = 'B')) 
		   
		  and (@WBS2=' ' or @WBS2=l.WBS2) and (@WBS3=' ' or @WBS3=l.WBS3)
	) as d
	left join CCG_TR_Approvals a on a.TableName=d.TableName and a.Period=d.Period and a.PostSeq=d.PostSeq and a.PKey=d.PKey
	left join (
		select distinct TableName, Period, PostSeq, PKey from (
			select OriginalTable as TableName, OriginalPeriod as Period, OriginalPostSeq as PostSeq, OriginalPKey as PKey from CCG_TR_History
			union
			select NewTable, NewPeriod, NewPostSeq, NewPKey from CCG_TR_History
		) as yaya
	) as EverModified on EverModified.TableName=d.TableName and EverModified.Period=d.Period and EverModified.PostSeq=d.PostSeq and EverModified.PKey=d.PKey
	left join CCG_TR tr on tr.OriginalTable=d.TableName and tr.OriginalPeriod=d.Period and tr.OriginalPostSeq=d.PostSeq and tr.OriginalPKey=d.PKey
	left join PR PR1 on PR1.WBS1=d.WBS1 and PR1.WBS2=' '
	left join PR PR2 on PR2.WBS1=d.WBS1 and PR2.WBS2=d.WBS2 and PR2.WBS2<>' ' and PR2.WBS3=' '
	left join PR PR3 on PR3.WBS1=d.WBS1 and PR3.WBS2=d.WBS2 and PR3.WBS3=d.WBS3 and PR3.WBS3<>' '
	left join PR PR1tr on PR1tr.WBS1=tr.NewWBS1 and PR1tr.WBS2=' '
	left join PR PR2tr on PR2tr.WBS1=tr.NewWBS1 and PR2tr.WBS2=tr.NewWBS2 and PR2tr.WBS2<>' ' and PR2tr.WBS3=' '
	left join PR PR3tr on PR3tr.WBS1=tr.NewWBS1 and PR3tr.WBS2=tr.NewWBS2 and PR3tr.WBS3=tr.NewWBS3 and PR3tr.WBS3<>' '
	left join CA ON d.Account = CA.Account 
	where  (IsNull(tr.NewTransDate,'12/31/2030') <= @ThruDate or d.TransDate <= @ThruDate)
	--and CA.Type in (5,6)--Consider option to show more and use this logic in billable field
	order by IsNull(tr.NewWBS1,d.WBS1),IsNull(tr.NewWBS2,d.WBS2),IsNull(tr.NewWBS3,d.WBS3),
		IsNull(tr.NewTransDate,d.TransDate),d.Account --7,12,17,22,25
END
GO
