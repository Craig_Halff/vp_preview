SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[pmDelUnit]
  @strPlanID varchar(32),
  @strUnitID varchar(32)
AS

BEGIN -- Procedure pmDelUnit

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to delete a Unit row and its associated rows.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    
  DELETE RPPlannedUnit WHERE PlanID = @strPlanID AND UnitID = @strUnitID
  DELETE RPBaselineUnit WHERE PlanID = @strPlanID AND UnitID = @strUnitID
  DELETE RPUnit WHERE PlanID = @strPlanID AND UnitID = @strUnitID

  SET NOCOUNT OFF

END -- pmDelUnit
GO
