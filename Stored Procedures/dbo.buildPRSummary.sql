SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[buildPRSummary]
	@visionuser		Nvarchar(32),
	@forceRefresh		Nvarchar(1),
	@prWhere		Nvarchar(max),
	@period			Nvarchar(100),
	@postSeq		Nvarchar(100)
AS
BEGIN
   DECLARE @sessionID		varchar(32)
   
   SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
   SET NOCOUNT ON

	IF @visionuser IS NOT NULL AND @visionuser <> ''
		SELECT @sessionID = @visionuser
	ELSE
		SELECT @sessionID = 'NoUser'

	SELECT @sessionID = Substring(@sessionID,1,16) +
		+ convert(Nvarchar(25),DATEPART(yyyy,getUTCDate()),121)
		+ convert(Nvarchar(25),DATEPART(mm,getUTCDate()),121)
		+ convert(Nvarchar(25),DATEPART(dd,getUTCDate()),121)
		+ convert(Nvarchar(25),DATEPART(hh,getUTCDate()),121)
		+ convert(Nvarchar(25),DATEPART(mi,getUTCDate()),121)
		+ convert(Nvarchar(25),DATEPART(ss,getUTCDate()),121)
		+ convert(Nvarchar(25),DATEPART(ms,getUTCDate()),121)
	
	EXECUTE buildPRSummaryWBSList 	@visionuser, @sessionID, @forceRefresh, @prWhere, @period, @postSeq
	EXECUTE buildPRSummarySub 	@visionuser, @sessionID
	EXECUTE buildPRSummaryMain 	@visionuser, @sessionID
	
	UPDATE STATISTICS PRSummarySub
	UPDATE STATISTICS PRSummaryMain

 	UPDATE FW_CFGSystem SET PRSummaryLastUpdate = Getutcdate()
END

GO
