SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_CreateDeltekSPs] (@SpPrefix Nvarchar(32))
AS BEGIN
/*
       Copyright (c) 2017 EleVia Software. All rights reserved.

       exec spCCG_CreateDeltekSPs 'spCCG_PAT_'
*/
       set nocount on
       declare @sql Nvarchar(max), @params Nvarchar(max), @sqlDrop Nvarchar(max), @schema Nvarchar(max), @spName Nvarchar(max)
       declare dropCursor cursor fast_forward for
             select SCHEMA_NAME(SCHEMA_ID), name from sys.objects
				where name like 'DeltekStoredProc_' + Right(@SpPrefix,Len(@SpPrefix)-2) + '%' and type in (N'P', N'PC')
					and name not in ('DeltekStoredProc_CCG_EI_WatcherConfig', 'DeltekStoredProc_CCG_EI_GetInvoiceFolder')
				order by 1
       open dropCursor
       fetch next from dropCursor into @schema, @spName
       while @@fetch_status=0
       begin
             set @sqlDrop = N'drop procedure [' + @schema + '].[' + @spName + ']'
             print @sqlDrop
             execute(@sqlDrop)

             fetch next from dropCursor into @schema, @spName
       end
       close dropCursor
       deallocate dropCursor

       declare createCursor cursor fast_forward for
             select SCHEMA_NAME(SCHEMA_ID), name from sys.objects where name like @SpPrefix + '%' and type in (N'P', N'PC') order by 1
       open createCursor
       fetch next from createCursor into @schema, @spName
       while @@fetch_status=0
       begin
             select @params = IsNull(STUFF((SELECT ', ' + Value
             from (select ParameterName + ' ' + ParameterDataType + Size + replace(dbo.[fnCCG_GetParameterDefaultValue](@spName, ParameterName, 0),'''','''''') as Value, parameter_id from (
                    select SCHEMA_NAME(SCHEMA_ID) AS [Schema], SO.Name AS [ObjectName], SO.Type_Desc, PM.Parameter_ID,
                           Case When pm.system_type_id = pm.user_type_id Then 'system_type' Else 'user_type' End as TypeDescr,
                           Case When PM.Parameter_ID = 0 Then 'Returns' Else PM.Name End AS ParameterName,
                           TYPE_NAME(PM.User_Type_ID) as ParameterDataType,
                           Case When TYPE_NAME(PM.User_Type_ID) IN ('float', 'uniqueidentifier', 'datetime', 'date', 'bit', 'bigint', 'int', 'image', 'money', 'xml', 'tinyint', 'text', 'ntext', 'smallint', 'smallmoney')
                                        Then ''
                                        When TYPE_NAME(PM.User_Type_ID) IN ('decimal', 'numeric')
                                        Then '(' + CAST( Precision AS Nvarchar(4) ) + ', ' + CAST( Scale AS Nvarchar(4)) + ')'
                                        Else
                                        Case When PM.Max_Length <> -1 then '('+CAST( PM.Max_Length / (case when TYPE_NAME(PM.User_Type_ID) IN ('nvarchar', 'nchar') then 2 else 1 end) AS Nvarchar(4))+')'
                                                     When (TYPE_NAME(PM.User_Type_ID) = 'xml') or (pm.system_type_id <> pm.user_type_id) then ''
                                                     Else '(max)'
                                        End
                                        End AS Size,
                           Case When PM.Is_Output = 1 THEN 'Output' Else '' End as Direction
                    from sys.objects AS SO
                    inner join sys.parameters AS PM ON SO.OBJECT_ID = PM.OBJECT_ID
                    where SO.object_id in (select object_id from sys.objects where name = @spName)
                    ) T1
             ) T order by parameter_id
             FOR XML PATH(''), TYPE
             ).value('.', 'Nvarchar(MAX)'),1,1,''),'')

             set @sql = 'exec spCCG_MakeDeltekSP ''' + @spName + ''', N''' + @params + ''''
             print Cast(Len(@sql) as Nvarchar) + ' ' + @sql
             execute(@sql)
             fetch next from createCursor into @schema, @spName
       end
       close createCursor
       deallocate createCursor
END
GO
