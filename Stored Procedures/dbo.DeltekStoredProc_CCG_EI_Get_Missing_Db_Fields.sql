SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Get_Missing_Db_Fields]
AS
BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_EI_Get_Missing_Db_Fields]
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT syscolumns.name
        FROM sysobjects
			INNER JOIN syscolumns ON syscolumns.id = sysobjects.id
        WHERE sysobjects.Name = 'CCG_EI_CustomColumns'
			AND sysobjects.xtype = 'U' AND syscolumns.name NOT IN ('WBS1', 'WBS2', 'WBS3')
        ORDER BY syscolumns.name;

END;
GO
