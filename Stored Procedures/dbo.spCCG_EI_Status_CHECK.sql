SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_Status_CHECK](@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7), 
	@NewValue varchar(255)) 
AS BEGIN
/*
	Copyright (c) 2016 EleVia Software  All rights reserved.
*/
	if isnull(@newvalue,' ') = 'C' set @newvalue = 'D'
	if @NewValue not in ('A','I','D')
		select -1 as ReturnValue, 'Invalid value.  Status must be A, I or C.' as ReturnMessage
	else 
		select 0 as ReturnValue, '' as ReturnMessage
END
GO
