SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_All_GetTableDefinition] ( @tableName varchar(500), @sql varchar(max))
             AS EXEC spCCG_All_GetTableDefinition @tableName,@sql
GO
