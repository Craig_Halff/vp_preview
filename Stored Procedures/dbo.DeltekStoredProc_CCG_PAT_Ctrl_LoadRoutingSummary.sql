SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Ctrl_LoadRoutingSummary]
	@payableSeq	int
AS
BEGIN
	-- [DeltekStoredProc_CCG_PAT_Ctrl_LoadRoutingSummary] 49
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	SELECT summary.PayableSeq, nextRoute.Seq as nextRouteSeq, RemainingRoutes, nextRoute.Employee as nextEmployee,
            nextRoute.Stage, nextRoute.Description, nextRoute.Route, nextRoute.DateLastNotificationSent, nextRoute.SortOrder, nextRoute.Parallel
        FROM (
            SELECT PayableSeq, MIN(SortOrder) as nextRouteOrder, COUNT(*) as RemainingRoutes
                FROM CCG_PAT_Pending
				WHERE @payableSeq <= 0 OR PayableSeq = @payableSeq
                GROUP BY PayableSeq
            ) summary
            INNER JOIN CCG_PAT_Pending nextRoute
                ON summary.PayableSeq = nextRoute.PayableSeq and summary.nextRouteOrder = nextRoute.SortOrder;
END;

GO
