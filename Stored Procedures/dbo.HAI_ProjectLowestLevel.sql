SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_ProjectLowestLevel]
AS
BEGIN
    /*
	Adds new Entries to the UDIC_ProjectLowestLevel table as needed.

	20201117	Craig Anderson
				Re-written to cover all WBS levels and using correct column names.
	20210202	Craig H. Anderson
				Add code to delete PLL records no longer in PR that also have no mileage log entries
	*/
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    -- Tasks
    INSERT INTO dbo.UDIC_ProjectLowestLevel
    (
        UDIC_UID
      , CreateDate
      , CreateUser
      , CustWBS1
      , CustWBS1Name
      , CustName
      , CustWBS2
      , CustWBS2Name
      , CustWBS3
      , CustWBS3Name
      , CustStatus
      , CustChargeType
    )
    SELECT  REPLACE(NEWID(), '-', '') AS PKey
          , GETDATE()
          , 'HAI_ProjectLowestLev'
          , p.WBS1
          , p.Name
          --NC1001.000 | 000005 | 000038 PTO | Austin | Austin Team 38
          , p.WBS1 + ' | ' + p.WBS2 + ' | ' + p.WBS3 + ' ' + p1.Name + ' | ' + p2.Name + ' | '
            + p3.Name                 AS Name
          , p.WBS2
          , p2.Name
          , p.WBS3
          , p3.Name
          , p.Status
          , p.ChargeType
    FROM    dbo.PR p1, dbo.PR p2, dbo.PR p3, dbo.PR p
    WHERE   p.WBS1 = p1.WBS1
            AND p.WBS1 = p2.WBS1
            AND p.WBS1 = p3.WBS1
            AND p.WBS2 = p2.WBS2
            AND p.WBS3 = p3.WBS3
            AND p1.WBS2 = ' '
            AND p2.WBS2 = p3.WBS2
            AND p2.WBS3 = ' '
            AND p3.WBS3 <> ' '
            AND p.SubLevel = 'N'
            AND p.Status <> 'D'
            AND p1.Status <> 'D'
            AND p1.ReadyForProcessing = 'Y'
            AND p2.ReadyForProcessing = 'Y'
            AND p3.ReadyForProcessing = 'Y'
            AND NOT EXISTS (
        SELECT  'x'
        FROM    dbo.UDIC_ProjectLowestLevel l WITH (NOLOCK)
        WHERE   l.CustWBS1 = p.WBS1
                AND l.CustWBS2 = p.WBS2
                AND l.CustWBS3 = p.WBS3
    );

    --AND p.WBS1 = @WBS1;


    -- Phases
    INSERT INTO dbo.UDIC_ProjectLowestLevel
    (
        UDIC_UID
      , CreateDate
      , CreateUser
      , CustWBS1
      , CustWBS1Name
      , CustName
      , CustWBS2
      , CustWBS2Name
      , CustWBS3
      , CustWBS3Name
      , CustStatus
      , CustChargeType
    )
    SELECT  REPLACE(NEWID(), '-', '')                                   AS PKey
          , GETDATE()
          , 'HAI_ProjectLowestLev'
          , p1.WBS1
          , p1.Name
          --T32442.117 | 000117 Atmos/Midlothian MS | Midlothian MS
          , p1.WBS1 + ' | ' + p2.WBS2 + ' ' + p1.Name + ' | ' + p2.Name AS Name
          , p2.WBS2
          , p2.Name
          , p3.WBS3
          , p3.Name
          , p2.Status
          , p2.ChargeType
    FROM    dbo.PR p1, dbo.PR p2, dbo.PR p3, dbo.PR p
    WHERE   p.WBS1 = p1.WBS1
            AND p.WBS1 = p2.WBS1
            AND p.WBS1 = p3.WBS1
            AND p.WBS2 = p2.WBS2
            AND p.WBS3 = p3.WBS3
            AND p1.WBS2 = ' '
            AND p2.WBS2 = p3.WBS2
            AND p2.WBS3 = ' '
            AND p3.WBS3 = ' '
            AND p.SubLevel = 'N'
            AND p1.ReadyForProcessing = 'Y'
            AND p2.ReadyForProcessing = 'Y'
            AND p.Status <> 'D'
            AND p1.Status <> 'D'
            AND NOT EXISTS (
        SELECT  'x'
        FROM    dbo.UDIC_ProjectLowestLevel l WITH (NOLOCK)
        WHERE   l.CustWBS1 = p.WBS1
                AND l.CustWBS2 = p.WBS2
                AND l.CustWBS3 = p.WBS3
    );


    -- Projects
    INSERT INTO dbo.UDIC_ProjectLowestLevel
    (
        UDIC_UID
      , CreateDate
      , CreateUser
      , CustWBS1
      , CustWBS1Name
      , CustName
      , CustWBS2
      , CustWBS2Name
      , CustWBS3
      , CustWBS3Name
      , CustStatus
      , CustChargeType
    )
    SELECT  REPLACE(NEWID(), '-', '') AS PKey
          , GETDATE()
          , 'HAI_ProjectLowestLev'
          , p1.WBS1
          , p1.Name
          , p1.WBS1 + ' ' + p1.Name   AS Name
          , p.WBS2
          , p2.Name
          , p1.WBS3
          , p3.Name
          , p1.Status
          , p1.ChargeType
    FROM    dbo.PR p1, dbo.PR p2, dbo.PR p3, dbo.PR p
    WHERE   p.WBS1 = p1.WBS1
            AND p.WBS1 = p2.WBS1
            AND p.WBS1 = p3.WBS1
            AND p.WBS2 = p2.WBS2
            AND p.WBS3 = p3.WBS3
            AND p1.WBS2 = ' '
            AND p2.WBS2 = ' '
            AND p2.WBS3 = ' '
            AND p3.WBS3 = ' '
            AND p.SubLevel = 'N'
            AND p1.ReadyForProcessing = 'Y'
            AND p.Status <> 'D'
            AND NOT EXISTS (
        SELECT  'x'
        FROM    dbo.UDIC_ProjectLowestLevel l WITH (NOLOCK)
        WHERE   l.CustWBS1 = p.WBS1
                AND l.CustWBS2 = p.WBS2
                AND l.CustWBS3 = p.WBS3
    );

    -- Identify and delete PLL records with no corresponding PR record,
    -- which also do not have any Mileage Log records that refer to them
    DELETE  FROM pll
    --SELECT  pll.UDIC_UID, pll.CustWBS1, pll.CustWBS2, pll.CustWBS3, p.WBS1, p.WBS2, p.WBS3
    FROM    dbo.UDIC_ProjectLowestLevel AS pll
    LEFT OUTER JOIN dbo.PR              AS p WITH (NOLOCK)
        ON pll.CustWBS1 = p.WBS1
           AND  pll.CustWBS2 = p.WBS2
           AND  pll.CustWBS3 = p.WBS3
    WHERE   p.WBS1 IS NULL
            AND NOT EXISTS (
        SELECT  'x'
        FROM    dbo.UDIC_FleetManagement_MileageLog l WITH (NOLOCK)
        WHERE   l.CustProject = pll.UDIC_UID
    );

END;
GO
