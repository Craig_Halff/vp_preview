SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_CheckIfEmployeeIsSupervisor](@Employee Nvarchar(20))
AS
BEGIN
/*
	Copyright (c) 2020 EleVia Software. All rights reserved.
*/
	SET NOCOUNT ON

	IF EXISTS(SELECT TOP 1 'x' FROM EM WHERE EM.Supervisor = @Employee AND EM.Status = 'A')
		SELECT 1
	ELSE
		SELECT 0
END
GO
