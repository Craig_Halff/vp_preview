SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EMAIL_Batches]
@breakOnApp		bit,
@breakOnSender  bit,
@standbyDelay int = 0
AS BEGIN
	SET NOCOUNT ON;
	--exec [DeltekStoredProc_CCG_EMAIL_Batches] 1,1
	DECLARE @disabled	varchar(1) = null
	DECLARE @sSQL		nvarchar(max);
	BEGIN TRY  --don't let this stuff 

		--SELECT * FROM CCG_Email_Config
		IF exists (select 'x' from syscolumns a,sysobjects b where a.id = b.id and a.name = 'Disabled' and b.name = 'CCG_Email_Config')
		BEGIN
			SET @sSQL = N'SELECT @outcol1=Disabled FROM CCG_Email_Config'
			EXECUTE sp_executesql @sSQL, N'@outcol1	varchar(1) OUTPUT', @outcol1=@disabled OUTPUT
		END

		--print ISNULL(@disabled,'_')
		IF ISNULL(@disabled,'N') = 'Y'
		BEGIN
			print 'disabled'
				IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CCG_Email_Log]') AND type in (N'U'))
				BEGIN
				 exec('insert into CCG_Email_Log (Action,DETAIL) values (''BATCHES_CHECK'',''DISABLED'')')
				END
				select '' as ToList,'' as BatchType from CCG_Email_Messages where seq = -99
				select 'Y' as [Disabled]
		
			RETURN
		END

		IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CCG_Email_Log]') AND type in (N'U'))
		BEGIN
		 exec('insert into CCG_Email_Log (Action) values (''BATCHES_CHECK'')')
		END
	END TRY
	BEGIN CATCH
		PRINT 'TRY1 ERR:' + ERROR_MESSAGE()
	END CATCH

	IF @breakOnApp = 0 and @breakOnSender = 0
	BEGIN
		select ToList,case when ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]()) <= 0 then 'Immediate' else 'Batch' end as BatchType from CCG_Email_Messages  where ISNULL(ToList,'') <> '' and DateAdd(MI,ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]())+@standbyDelay, CreateDateTime) < GETUTCDATE() 
		group by ToList,case when ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]()) <= 0 then 'Immediate' else 'Batch' end  
		order by ToList,case when ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]()) <= 0 then 'Immediate' else 'Batch' end
	END
	ELSE IF @breakOnApp = 1 and @breakOnSender = 0
	BEGIN
		select SourceApplication,ToList,case when ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]()) <= 0 then 'Immediate' else 'Batch' end as BatchType from CCG_Email_Messages  where ISNULL(ToList,'') <> '' and DateAdd(MI,ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]())+@standbyDelay, CreateDateTime) < GETUTCDATE() 
		group by SourceApplication,ToList,case when ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]()) <= 0 then 'Immediate' else 'Batch' end  
		order by SourceApplication,ToList,case when ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]()) <= 0 then 'Immediate' else 'Batch' end
	END
	ELSE IF @breakOnApp = 0 and @breakOnSender = 1
	BEGIN
		select Sender,ToList,case when ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]()) <= 0 then 'Immediate' else 'Batch' end as BatchType from CCG_Email_Messages  where ISNULL(ToList,'') <> '' and DateAdd(MI,ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]())+@standbyDelay, CreateDateTime) < GETUTCDATE() 
		group by Sender,ToList,case when ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]()) <= 0 then 'Immediate' else 'Batch' end  
		order by Sender,ToList,case when ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]()) <= 0 then 'Immediate' else 'Batch' end
	END
	ELSE IF @breakOnApp = 1 and @breakOnSender = 1
	BEGIN
		select SourceApplication,Sender,ToList,case when ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]()) <= 0 then 'Immediate' else 'Batch' end as BatchType from CCG_Email_Messages  where ISNULL(ToList,'') <> '' and DateAdd(MI,ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]())+@standbyDelay, CreateDateTime) < GETUTCDATE() 
		group by SourceApplication,Sender,ToList,case when ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]()) <= 0 then 'Immediate' else 'Batch' end  
		order by SourceApplication,Sender,ToList,case when ISNULL(MaxDelay,[dbo].[fnCCG_Email_DefaultDelay]()) <= 0 then 'Immediate' else 'Batch' end
	END

END;

GO
