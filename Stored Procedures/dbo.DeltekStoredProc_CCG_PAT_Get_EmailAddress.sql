SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Get_EmailAddress] (
	@email	Nvarchar(100),
	@domain	Nvarchar(100)
) AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_Get_EmailAddress] 'amasconi@amc.com', ''
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	SELECT case when Email = @email then 1 else 0 end as exactmatch, max(recordsource), max(Email), max(Vendor), max(Address),COUNT(distinct vendor) as numvefound,COUNT(*) as numfound
        FROM (
            SELECT 'VEADDRESS' as recordsource, Vendor, Address, Email
                FROM VEAddress
                WHERE Email = @email or Email like N'%'+@domain
			UNION
            SELECT 'Contact', Vendor, VEAddress, Email
                FROM Contacts
                WHERE Email = @email or Email like N'%'+@domain
        ) matches
        GROUP BY case when Email = @email then 1 else 0 end
        ORDER BY 1 desc, 2 desc;
END;

GO
