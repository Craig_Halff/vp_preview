SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyConvertUnit]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg Nvarchar(1000) output,
@UserName Nvarchar(32) = 'VisionKeyConvert', -- Add by DP 4.0
@Period integer = 0,
@EntityType Nvarchar(20)
as
begin -- Procedure
	set nocount on
	declare @OldValue Nvarchar(30)
	declare @NewValue Nvarchar(30)
	Declare @Existing integer
	declare @Entity Nvarchar(20)
	declare @length integer
	declare @CvtType integer
	declare @KeysFetch integer
	declare @RetVal integer
	declare @diag smallint
	declare @Errmsg2 Nvarchar(1000),
			@custlabel	Nvarchar(40),
			@Custlabelplural Nvarchar(40)
	declare @TableName 		Nvarchar(100)
	declare @Sql 				Nvarchar(1000)
	declare @message nvarchar(max)

	set @diag = 0
	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'Unit'
	set @length = 20
	set @Existing = 0
	set @KeysFetch = 0

	execute InsertKeyCvtDriver @Entity -- populate driver table

-- Add by DP 4.0
	delete from keyconvertDriver where entity = N'Unit' and TableName = N'UNUnit'
	DECLARE @OldName Nvarchar(40)
	DECLARE @NewName Nvarchar(40)
	DECLARE @PKey Nvarchar(50)
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0
--
	select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'unitLabel'
	select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'unitLabelPlural'

	declare KeysCursor cursor for
		select OldKey, NewKey, Pkey from KeyConvertWork where Entity = @EntityType
	open KeysCursor
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue, 
		@PKey

	set @KeysFetch = @@Fetch_Status

While (@KeysFetch = 0)
	begin
		begin tran
-- Add by DP 4.0
		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany Nvarchar(14)
			DECLARE @RecordCount integer

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				set @RecordCount = (select count(*) from KeyConvertWork where Entity = @EntityType)

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),N'KC','Y',  @custlabel + ' Key Convert ('+rtrim(convert(Nvarchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, left(@custlabel + ' Key Convert', 40), @custlabel);
			end

			Set @PostControlFlag = 1
		end

		set @Existing = 0
		select @Existing =  1, @NewName = Name from UNUnit where Unit = @NewValue
		select @OldName = Name from UNUnit where Unit = @OldValue
--

	If (@Existing = 1)
		begin
		  if (@DeleteExisting = 0)
			begin
			set @message = dbo.GetMessage('MsgNumAnExistingConvWillNotCont',@custlabel,@NewValue,@custlabel,@custLabelPlural,'','','','','')
			RAISERROR(@message,16,3)
				close keysCursor
				deallocate keysCursor
				if (@@Trancount > 0)
				   ROLLBACK TRANSACTION
				return(50002) -- user defined error
			end
			if (@NewValue = @OldValue)
				begin
				set @message = dbo.GetMessage('MsgAreSameNumConvNotCont',@custlabelPlural,'','','','','','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end

			declare @MulticompanyEnabled Nvarchar(1)
			declare @MulticurrencyEnabled Nvarchar(1)
			select @MulticompanyEnabled = MulticompanyEnabled,
		   		 @MulticurrencyEnabled = MulticurrencyEnabled from FW_CFGSystem
			if (@MulticurrencyEnabled = 'Y')
				declare @oldCurrency Nvarchar(14)
				declare @newCurrency Nvarchar(14)
				begin
					set @oldCurrency = ''
					set @newCurrency = ''
					select 
						@oldCurrency = oldTable.ProjectCurrencyCode+';'+oldTable.BillingCurrencyCode,
						@newCurrency = newTable.ProjectCurrencyCode+';'+newTable.BillingCurrencyCode
					from UNTable newTable inner join UNUnit newUnit ON newTable.UnitTable = newUnit.UnitTable,
					UNTable oldTable inner join UNUnit oldUnit ON oldTable.UnitTable = oldUnit.UnitTable AND oldUnit.unit = @OldValue
					where newUnit.unit = @NewValue
					and (oldTable.ProjectCurrencyCode+oldTable.BillingCurrencyCode <> newTable.ProjectCurrencyCode+newTable.BillingCurrencyCode)
					order by newUnit.unit,
					newUnit.unitTable

					if (@oldCurrency <> @newCurrency)
						begin
						set @message = dbo.GetMessage('MsgWithMultiCmpYouCantMergeDiffCurr',@custlabelPlural,@custlabel,@OldValue,@oldCurrency,@custlabel,@NewValue,@newCurrency,'','')
						RAISERROR(@message,16,3)
							close keysCursor
							deallocate keysCursor
							if (@@Trancount > 0)
							   ROLLBACK TRANSACTION
							return(50003) -- user defined error
						end
				end

/* Removed by DP 4.0

			  exec @RetVal = KeyCvt @Entity = @Entity,
											@CvtType = @CvtType,
											@Length = 20,
											@ErrMsg = @ErrMsg2 output,
											@Diag = @Diag,
											@ConstrFlag = 3 -- UpdateTables

				Delete from UNUnit where Unit = @OldValue
*/
				delete from unMaster where unit = @oldValue and exists
				(select 'x' from unMaster tmp where tmp.unit = @NewValue and tmp.batch = unMaster.batch)

				update unDetail
				set unDetail.pKey = unDetail.pKey + 'x'
				 from unDetail inner join unDetail as tmp on unDetail.batch = tmp.batch 
				and unDetail.pkey = tmp.pkey
				where unDetail.unit = @OldValue and tmp.unit = @NewValue

				/*handle duplicate custom tab records, possible when copying one Unit to another*/
				declare custTable cursor for
				select tableName from FW_CustomGrids where infocenterarea = N'Units'
				open custTable
				fetch next from custTable into @TableName
				while (@@FETCH_STATUS = 0)
				begin
					set @Sql = 'Delete FROM ' + @TableName + ' WHERE ' + @TableName + '.Unit =N''' +  @OldValue + ''' and ' +
								  'exists (select ''x'' from ' + @TableName + ' new where new.Unit = N''' + @NewValue + ''' and new.seq = ' + @TableName + '.seq)'
					if (@Diag = 1)
						print @Sql
					execute(@Sql)
					fetch next from custTable into @TableName
				end
				close custTable
				deallocate custTable					

-- Add by DP 12/09/2009
				Update UNUnit Set Unit=@NewValue where Unit = @OldValue and not exists (select 'x' from UNUnit new Where new.Unit = @NewValue and new.UnitTable = UNUnit.UnitTable)

				IF EXISTS (SELECT 'x' FROM sys.objects WHERE name = 'UnitCustomTabFields' AND type = 'U')
					begin
						delete from UnitCustomTabFields where unit = @oldValue 
							and exists (select 'x' from UnitCustomTabFields new where new.unit = @NewValue and new.UnitTable=UnitCustomTabFields.UnitTable )
					end

	    END --Existing
-- Add by DP 4.0
		Else	-- Not Existing
		begin
			select * into #TempKeyConvert from UNUnit where Unit = @OldValue
			Update #TempKeyConvert set Unit=@NewValue
			Insert UNUnit select * from #TempKeyConvert
			Drop Table #TempKeyConvert
			set @NewName = @OldName
		end

		exec @RetVal = KeyCvt @Entity = @Entity,
					 @CvtType = @CvtType,
					 @Length = 10,
					 @ErrMsg = @ErrMsg2 output,
					 @Diag = @Diag,
					 @NewValue = @NewValue,
					 @OldValue = @OldValue,
					 @ConstrFlag = 3 -- UpdateTables

		Delete from UNUnit where Unit = @OldValue
--
		if (@RetVal = 0)
			begin
-- Add by DP 4.0
				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq)
					select PKey, Entity, OldKey, NewKey, @OldName, @NewName, @Period, @LastPostSeq from keyConvertWork where Pkey = @PKey
				end
--
				delete from keyConvertWork where current of keysCursor
				commit Transaction
			end
		else
			begin
				set @ErrMsg = @ErrMsg + @ErrMsg2
				rollback transaction
			end
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue,
		@PKey
	set @KeysFetch = @@Fetch_Status
	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	return(0)
End -- Procedure
GO
