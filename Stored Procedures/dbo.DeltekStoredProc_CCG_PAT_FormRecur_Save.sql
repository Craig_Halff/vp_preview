SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FormRecur_Save]
	@isNew			bit,
	@seq			int,
	@Company		Nvarchar(14),
	@desc			Nvarchar(500),
	@Vendor			Nvarchar(20),
	@Freq			varchar(20),
	@FreqParam		int,
	@CopyAmount		char(1),
	@Status			char(1),
	@User			Nvarchar(50)
AS
BEGIN
	SET NOCOUNT ON;

	IF @isNew = 1
        INSERT INTO CCG_PAT_Recur (Company, Description, Vendor,
                Freq, FreqParam, CopyAmount,
                Status, ModDate, ModUser)
            VALUES (@Company, @desc, @Vendor, @Freq, @FreqParam, @CopyAmount, @Status, GETUTCDATE(), @User);
    ELSE IF @seq > 0
        UPDATE CCG_PAT_Recur
            SET Company = @Company,
                Description = @desc,
                Vendor = @Vendor,
                Freq = @Freq,
                FreqParam = @FreqParam,
                CopyAmount = @CopyAmount,
                Status = @Status,
                ModDate = GETUTCDATE(),
                ModUser = @User
            WHERE seq = @seq;

	SELECT @@ROWCOUNT as RowsAffected;
END;

GO
