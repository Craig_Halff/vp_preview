SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FormExport_VoucherShown]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SET NOCOUNT ON;
	DECLARE @Count int;

	SELECT Period as Value, Period as Display
        FROM CFGDates
        WHERE Period in (select distinct Period from CFGDatesStatus where Closed <> 'Y')
        ORDER BY 1 Desc;

	SET @Count = @@ROWCOUNT;
	IF @Count < 4
		SELECT TOP (4 - @Count) Period as Value, Convert(Nvarchar(10), Period) + ' (C)' as Display
			FROM CFGDates
			WHERE Period in (select distinct Period from CFGDatesStatus where Closed = 'Y')
			ORDER BY 1 Desc
END;
GO
