SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FormEdit_LoadLiabCode]
	@VisionMultiCompany	bit,
	@v					bit,
	@mc					bit,
	@ve					Nvarchar(max),
	@company			Nvarchar(max)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max);

	SET @sql = N'
		SELECT l.code, min(l.Description ' +
				(CASE WHEN @VisionMultiCompany = 1 THEN ' + '' ('' + ISNULL(l.Org, l.company) + '')''' ELSE '' END) + '), ' +
				(CASE WHEN @v = 1 THEN ' max(case when ve.category is null then null else c.liabcode end)' ELSE ''''' as vedef' END) + '
			FROM CFGAPLiability l' +
				(CASE WHEN @v = 1 THEN '
					LEFT JOIN VE on ve.Vendor = N''' + @ve + '''
					LEFT JOIN CFGvendorTypeAPLiability c on ve.Category = c.Type and l.Company = c.Company '
				ELSE '' END) +
				(CASE WHEN @mc = 1 THEN '
			WHERE l.Company = N''' + ISNULL(@company,'') + ''' ' ELSE '' END) + '
			GROUP BY l.code
			ORDER BY 2';
	EXEC (@sql);
END;

GO
