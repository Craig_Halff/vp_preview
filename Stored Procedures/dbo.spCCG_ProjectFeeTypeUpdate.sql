SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_ProjectFeeTypeUpdate] @WBS1 varchar(30)
As
/* 
Copyright (c) 2021 Central Consulting Group.  All rights reserved.
08/13/2021	David Springer
			Update Project custom Fee Type
			Call this on Project Phases or Tasks grid Insert or Change workflow when Phase Billing Type has changed
			
			CHA Verified the fixes.
*/
Declare @FeeTypes varchar(255);

    Begin
        Set NoCount On;

        Select @FeeTypes = Coalesce(@FeeTypes + ',', '') + A.BillingType
        From
            (
                Select Case CustPhaseBillingType When 'Cost Plus Max_Mult' Then 'CPM'
                           When 'Cost Plus Max_Rate' Then 'CPM'
                           When 'Cost Plus_Mult' Then 'CP'
                           When 'Cost Plus_Rate' Then 'CP'
                           When 'Lump Sum' Then 'LS'
                           When 'Lump Sum by Task' Then 'LS'
                           When 'Non-Billable' Then 'NB'
                       End As BillingType
                     , Sum(CustPhaseTotal) As PhaseTotal
                From dbo.Projects_Phases
                Where
                    WBS1 = @WBS1
                    And WBS2 = ' '
                Group By Case CustPhaseBillingType When 'Cost Plus Max_Mult' Then 'CPM'
                             When 'Cost Plus Max_Rate' Then 'CPM'
                             When 'Cost Plus_Mult' Then 'CP'
                             When 'Cost Plus_Rate' Then 'CP'
                             When 'Lump Sum' Then 'LS'
                             When 'Lump Sum by Task' Then 'LS'
                             When 'Non-Billable' Then 'NB'
                         End
            ) As A
        Order By A.PhaseTotal Desc;

        Update dbo.ProjectCustomTabFields
        Set CustFeeTypes = @FeeTypes
        Where
            WBS1 = @WBS1
            And WBS2 = ' ';

    End;
GO
