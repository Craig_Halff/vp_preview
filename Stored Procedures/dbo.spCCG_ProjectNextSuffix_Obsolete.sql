SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_ProjectNextSuffix_Obsolete] @WBS1 varchar (30), @ModUser varchar (100)
AS
/*
Copyright 2021 Central Consulting Group.   All rights reserved.
06/24/2021	David Springer
			Create a new suffix project with data from first project.
08/11/2021	David Springer
			Added code to support suffixes .M01
*/
Declare @NewWBS1	varchar (30),
        @Root		varchar (32),
        @Suffix		varchar (32)

BEGIN
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	If @WBS1 like '%.M%'
		Begin
		Set @Root = left (@WBS1, len (@WBS1) -2)
		Select @Suffix = right ('0' + ltrim (str (MAX (right (WBS1, 2)) + 1)), 2) From PR Where WBS1 like @Root + '%'
		End
	Else
		Begin
		Set @Root = left (@WBS1, len (@WBS1) -3)
		Select @Suffix = right ('00' + ltrim (str (MAX (right (WBS1, 3)) + 1)), 3) From PR Where WBS1 like @Root + '%' AND WBS1 NOT LIKE '%.999'
		End

	Set @NewWBS1 = @Root + @Suffix

-- Create Next Project Suffix
   Insert Into PR
   (WBS1, WBS2, WBS3, CreateDate, CreateUser, ChargeType,
    Name, LongName, Status, SiblingWBS1,
    ProjectType, Source, Responsibility, Stage, Principal, ProjMgr, Supervisor,
	ClientID, CLAddress, ContactID, BillingClientID, CLBillingAddr, BillingContactID,
	RevType, AvailableForCRM, ReadyForApproval, ReadyForProcessing,
	PlanID, BillingCurrencyCode, ProjectCurrencyCode)
   Select 
    @NewWBS1, ' ', ' ', getDate(), @ModUser, ChargeType,
    Name + ' ?????', LongName, 'A',  'P' + substring (@NewWBS1, 2, 99),
    ProjectType, Source, Responsibility, '10' Stage, Principal, ProjMgr, Supervisor,
	ClientID, CLAddress, ContactID, BillingClientID, CLBillingAddr, BillingContactID,
	RevType, AvailableForCRM, ReadyForApproval, 'N' ReadyForProcessing,
	PlanID, BillingCurrencyCode, ProjectCurrencyCode
   From PR 
   Where WBS1 = @WBS1
     and WBS2 = ' '

   Insert Into ProjectCustomTabFields
   (WBS1, WBS2, WBS3, CustPracticeArea,
    CustAdministrativeAssistant, CustBiller, CustManagementLeader, CustDirector, CustCorporateMarketing, CustLocationURL, CustFederalInd)
   Select 
    @NewWBS1, ' ', ' ', CustPracticeArea,
       CustAdministrativeAssistant, CustBiller, CustManagementLeader, CustDirector, CustCorporateMarketing, 'https://projectlocator.halff.com/map?project=' + Right(@NewWBS1,9), CustFederalInd
   From ProjectCustomTabFields
   Where WBS1 = @WBS1
     and WBS2 = ' '


   Insert Into PRClientAssoc 
   (WBS1, ClientID, PrimaryInd, Role, ClientInd, VendorInd, CreateUser, CreateDate)
   Select 
    @NewWBS1, ClientID, 'Y', 'sysOwner', 'Y', 'N', 'CCG Procedure', getDate()
   From PR 
   Where WBS1 = @WBS1
     and WBS2 = ' '
     and ClientID is not null

   Insert Into PRContactAssoc
   (WBS1, ContactID, PrimaryInd, Role, CreateUser, CreateDate)
   Select 
    @NewWBS1, ContactID, 'Y', 'sysOwner', 'CCG Procedure', getDate()
   From PR 
   Where WBS1 = @WBS1
     and WBS2 = ' '
     and ContactID is not null

-- Billing Client
   Insert Into PRClientAssoc 
   (WBS1, ClientID, PrimaryInd, Role, ClientInd, VendorInd, CreateUser, CreateDate)
   Select 
    @NewWBS1, BillingClientID, 'N', 'sysClient', 'Y', 'N', @ModUser, getDate()
   From PR p
   Where WBS1 = @WBS1
     and WBS2 = N' ' -- project level only
     and BillingClientID is not null
     and not exists (Select * From PRClientAssoc Where WBS1 = @NewWBS1 and ClientID = p.BillingClientID)

   Insert Into PRContactAssoc
   (WBS1, ContactID, Role, PrimaryInd, CreateUser, CreateDate)
   Select 
    @NewWBS1, BillingContactID, 'sysClient', 'Y', @ModUser, getDate()
   From PR p
   Where WBS1 = @WBS1
     and WBS2 = N' ' -- project level only
     and BillingContactID is not null
     and not exists (Select * From PRContactAssoc Where WBS1 = @NewWBS1 and ContactID = p.BillingContactID)

--	Create Plan
	Insert Into PNPlan
	(PlanID, WBS1, PlanNumber, Status, PlanName, ClientID, ProjMgr, Principal, Supervisor, Org, StartDate, EndDate, 
	OverheadPct, AvailableFlg, UnPostedFlg, UtilizationIncludeFlg, CalcExpBillAmtFlg, CalcConBillAmtFlg, CommitmentFlg,
	ExpBillRtMethod, ExpBillRtTableNo, ConBillRtMethod, ConBillRtTableNo,
	BudgetType, CostRtMethod, CostRtTableNo, BillingRtMethod, BillingRtTableNo, GRMethod, GenResTableNo, BillGRRtMethod, GRBillTableNo,
	LabMultType,ExpBillMultiplier, ConBillMultiplier,
	CreateUser, CreateDate, VersionID, Company, CostCurrencyCode, BillingCurrencyCode)
	Select @NewWBS1, @NewWBS1, @NewWBS1, Status, Longname, ClientID, ProjMgr, Principal, Supervisor, Org, StartDate, EstCompletionDate,
		1.8, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 
		'2', '3', '2', '3', 
		'B', '2', '0', '2', '0', '1', '45', '1', '45',
		'3', 1, 1, 
		'ADMIN', getDate(), 'CF94652D43974434BC632A9F2FCA2C67', ' ', ProjectCurrencyCode, BillingCurrencyCode
	From PR
	Where WBS1 = @WBS1
	  and WBS2 = N' '

	Insert Into PNTask
	(TaskID, PlanID, WBS1, Name, StartDate, EndDate, WBSType, OutlineNumber, ChildrenCount, OutlineLevel)
	Select Replace (NewID(), '-', '') ID, @NewWBS1, @NewWBS1, Longname, IsNull (StartDate, getDate()), IsNull (EstCompletionDate, getDate()), 'WBS1', '001', '0', '0'
	From PR
	Where WBS1 = @WBS1
	  and WBS2 = N' '

	Insert Into PNWBSLevelFormat
	(WBSFormatID, PlanID, FmtLevel, WBSType, WBSMatch, CreateUser, CreateDate)
	Select Replace (NewID(), '-', ''), PlanID, '1', WBSType, 'Y', 'CCG', getDate()
	From PNTask t
	Where WBSType = 'WBS1'
	  and not exists (Select 'x' From PNWBSLevelFormat Where PlanID = t.PlanID and WBSType = t.WBSType)

	exec stRPPublish @NewWBS1 -- NewWBS1 = PlanID

END
GO
