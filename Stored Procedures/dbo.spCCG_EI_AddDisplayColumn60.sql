SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_AddDisplayColumn60](
	@OverridePriorSettings	char(1),
	@Header					varchar(255),
	@FieldName				varchar(255),
	@ColType				char(1),
	@DrilldownColumn		char(1),
	@ColSeq					int,
	@ColDecimals			int,
	@ColWidth				int = 0,
	@ColColumnName			varchar(100)='',
	@ColViewRoles			varchar(255)='',
	@ColEnabledWBS1			char(1)='Y',
	@ColEnabledWBS2			char(1)='Y',
	@ColEnabledWBS3			char(1)='Y',
	@ColStatus				char(1)='Y',
	@Link					varchar(250)=''
)
AS BEGIN
/*
	Copyright (c) 2019 Central Consulting Group.  All rights reserved.
	
	select * from CCG_EI_ConfigCustomColumns
	select * from CCG_EI_CustomColumns

	Examples:
	exec spCCG_EI_AddDisplayColumn60 'Y', 'JTD\nBilled', 'JTDBilled', 'C', 'N', 1, 0, 0, 'Y','N','N', 'A', '', ''
	
	Remove all UDCs and all associated data:
	delete from CCG_EI_ConfigCustomColumns
	DROP TABLE CCG_EI_CustomColumns
	CREATE TABLE [dbo].[CCG_EI_CustomColumns](
		[WBS1] [varchar](30) NOT NULL,
		[WBS2] [varchar](7) NOT NULL,
		[WBS3] [varchar](7) NOT NULL,
	 CONSTRAINT [CCG_EI_CustomColumnsPK] PRIMARY KEY NONCLUSTERED 
	(
		[WBS1] ASC,
		[WBS2] ASC,
		[WBS3] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
*/
	set nocount on
	declare @sql		varchar(max)
	declare @debug		char(1)
	declare @linkPart 	varchar(500) = (case when isnull(@Link,'')<>'' then @Link when isnull(@ColColumnName,'')='' then @FieldName else @ColColumnName end)
	set @debug='N'
	
	set @Header = REPLACE(@Header,'''','''''')
	print @Header
	-- Add column into EI configuration
	if @OverridePriorSettings='Y' 
	begin
		delete CCG_EI_ConfigCustomColumnsDescriptions from CCG_EI_ConfigCustomColumnsDescriptions CCD 
			inner join CCG_EI_ConfigCustomColumns CC on CCD.Seq=CC.Seq 
			where CC.Label=@Header or (InitialValue='dbo.fnCCG_EI_' + @FieldName and isnull(@ColColumnName,'')=isnull(DbColumnName,'')) 
		delete from CCG_EI_ConfigCustomColumns where Label=@Header or (InitialValue='dbo.fnCCG_EI_' + @FieldName and isnull(@ColColumnName,'')=isnull(DbColumnName,''))
	end
	
	set @sql='if not exists (select ''x'' from CCG_EI_ConfigCustomColumns where Label=''' + @Header + ''')' + CHAR(13) + CHAR(10) +
		' begin' + CHAR(13) + CHAR(10) +
		'	print ''	Adding configuration row for ' + @FieldName + '''' + CHAR(13) + CHAR(10) +
		'	insert into CCG_EI_ConfigCustomColumns (Label, DataType, DisplayOrder, ' +
		'		InitialValue, Link,' + CHAR(13) + CHAR(10) +
		'		DatabaseField, ColumnUpdate, Width, Decimals,' + CHAR(13) + CHAR(10) +
		'		ViewRoles, EditRoles, RequiredRoles, RequiredExpression,' + CHAR(13) + CHAR(10) +
		'		EnabledWBS1, EnabledWBS2, EnabledWBS3, Status, ModDate, ModUser, DbColumnName)' + CHAR(13) + CHAR(10) +
		'	values (''' + @Header + ''', ''' + @ColType + ''', ' + Cast(@ColSeq as varchar(10)) + CHAR(13) + CHAR(10) +
		'		, ''dbo.fnCCG_EI_' + @FieldName + ''','
	if @DrilldownColumn='Y'
		set @sql = @sql + '''dbo.fnCCG_EI_' + @linkPart + 'Link'',' + CHAR(13) + CHAR(10)
	else
		set @sql = @sql + ''''',' + CHAR(13) + CHAR(10)
	set @sql = @sql + 
		'		'''', '''', ' + Cast(@ColWidth as varchar(10)) + ', ' + + Cast(@ColDecimals as varchar(10)) + + ', ' + CHAR(13) + CHAR(10) +
		'		'''', '''', '''', '''',   ' + CHAR(13) + CHAR(10) +
		'		''' + @ColEnabledWBS1 + ''', ''' + @ColEnabledWBS2 + ''', ''' + @ColEnabledWBS3 + ''', ''' + @ColStatus + ''', ' + CHAR(13) + CHAR(10) +
		'		GETDATE(), ''CCG'', '''+@ColColumnName+''')' + CHAR(13) + CHAR(10) +
		' end'
	if @debug='Y' print @sql
	execute(@sql)
	
	set @sql= 'if not exists (select ''x'' from CCG_EI_ConfigCustomColumnsDescriptions where Label=''' + @Header + ''')' + CHAR(13) + CHAR(10) +
		' begin' + CHAR(13) + CHAR(10) +
		'	insert into CCG_EI_ConfigCustomColumnsDescriptions (Seq,UICultureName, Label)' + CHAR(13) + CHAR(10) +
		'	(select Seq,' + '''en-US''' + ', Label from CCG_EI_ConfigCustomColumns where Label=''' + @Header + ''')' + CHAR(13) + CHAR(10) +
		' end'
	if @debug='Y' print @sql
	execute(@sql)

	delete from CCG_EI_SQLPartsCache	-- clear the cache to force refresh of custom column SQL
END
GO
