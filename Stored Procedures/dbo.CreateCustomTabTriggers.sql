SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CreateCustomTabTriggers] (
	@TableName	VARCHAR(100),
	@InfoCenterArea	VARCHAR(32)
)
AS
BEGIN
	DECLARE	@AuditTriggersXML		XML,
		@InsertTrigger			VARCHAR(MAX),
		@UpdateTrigger			VARCHAR(MAX),
		@DeleteTrigger			VARCHAR(MAX),
		@StartText			VARCHAR(1000),
		@EndText			VARCHAR(1000),
		@DeleteAfterStartText		VARCHAR(1000),
		@InsertSQL			VARCHAR(1000),
		@DeleteSQL			VARCHAR(1000),
		@UpdateBeforeSQL		VARCHAR(1000),
		@UpdateAfterSQL			VARCHAR(1000),
		@DescripInsertSQL		VARCHAR(1000),
		@DescripBeforeUpdateSQL		VARCHAR(1000),
		@DescripAfterUpdateSQL		VARCHAR(1000),
		@DescripDeleteSQL		VARCHAR(1000),
		@SkipInsertSQL			VARCHAR(1000),
		@SkipUpdateSQL			VARCHAR(1000),
		@SkipDeleteSQL			VARCHAR(1000),
		@DropTrigger			VARCHAR(1000),
		@PKey				VARCHAR(1000),
		@UpdateCheck			VARCHAR(1000),
		@InsertPKey			VARCHAR(1000),
		@UpdatePKey			VARCHAR(1000),
		@DeletePKey			VARCHAR(1000),
		@ColumnName			NVARCHAR(256),
		@DataType			VARCHAR(256),
		@ColDataType			VARCHAR(256),
		@LookupTable			VARCHAR(100),
		@LookupValueColumn		VARCHAR(100),
		@LookupJoinColumn		VARCHAR(100),
		@TriggerName			VARCHAR(200),
		@SQLStr				VARCHAR(MAX)

	SET @AuditTriggersXML = dbo.AuditTriggersXML()

	SELECT
		@StartText = C.value('(shared/start)[1]', 'VARCHAR(1000)'),
		@EndText = C.value('(shared/end)[1]', 'VARCHAR(1000)'),
		@DeleteAfterStartText = C.value('(delete/deleteAfterStart)[1]', 'VARCHAR(1000)'),
		@InsertSQL = C.value('(insert/columnTemplate)[1]', 'VARCHAR(1000)'),
		@UpdateBeforeSQL = C.value('(update/beforeColumnTemplate)[1]', 'VARCHAR(1000)'),
		@UpdateAfterSQL = C.value('(update/afterColumnTemplate)[1]', 'VARCHAR(1000)'),
		@DeleteSQL = C.value('(delete/columnTemplate)[1]', 'VARCHAR(1000)'),
		@DescripInsertSQL = C.value('(insert/codeColumnTemplate)[1]', 'VARCHAR(1000)'),
		@DescripBeforeUpdateSQL = C.value('(update/beforeCodeColumnTemplate)[1]', 'VARCHAR(1000)'),
		@DescripAfterUpdateSQL = C.value('(update/afterCodeColumnTemplate)[1]', 'VARCHAR(1000)'),
		@DescripDeleteSQL = C.value('(delete/codeColumnTemplate)[1]', 'VARCHAR(1000)'),
		@SkipInsertSQL = C.value('(insert/skipColumnTemplate)[1]', 'VARCHAR(1000)'),
		@SkipUpdateSQL = C.value('(update/skipColumnTemplate)[1]', 'VARCHAR(1000)'),
		@SkipDeleteSQL = C.value('(delete/skipColumnTemplate)[1]', 'VARCHAR(1000)'),
		@DropTrigger = C.value('(shared/deleteTrigger)[1]', 'VARCHAR(1000)')
	FROM @AuditTriggersXML.nodes('root') AS T(C)

	SET @InsertTrigger = REPLACE(REPLACE(@StartText, '{0}', @TableName), '{1}', 'Insert')
	SET @UpdateTrigger = REPLACE(REPLACE(@StartText, '{0}', @TableName), '{1}', 'Update')
	SET @DeleteTrigger = REPLACE(REPLACE(@StartText, '{0}', @TableName), '{1}', 'Delete')
	SET @DeleteTrigger = @DeleteTrigger + @DeleteAfterStartText

	--Loop through primary keys for current table, and create primary key
	DECLARE @PKeysTable TABLE (
		TABLE_QUALIFIER		nvarchar(256),
		TABLE_OWNER			nvarchar(256),
		TABLE_NAME			nvarchar(256),
		COLUMN_NAME			nvarchar(256),
		KEY_SEQ				smallint,
		PK_NAME				nvarchar(256)
	)
	INSERT INTO @PKeysTable
	EXEC sp_pkeys @TableName

	SET @PKey = ''
	SET @UpdateCheck = ''

	DECLARE curPKeys CURSOR FOR
	SELECT COLUMN_NAME FROM @PKeysTable ORDER BY KEY_SEQ
	BEGIN
		OPEN curPKeys

		FETCH NEXT FROM curPKeys INTO @ColumnName

		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF @PKey <> ''
				SET @PKey = @PKey + ' + ''|'' + '
			SET @PKey = @PKey + 'CONVERT(NVARCHAR(255),{0}.[' + @ColumnName + '],121)'
			SET @UpdateCheck = @UpdateCheck + 'INSERTED.[' + @ColumnName + '] = DELETED.[' + @ColumnName + '] AND '

			FETCH NEXT FROM curPKeys INTO @ColumnName
		END
	END
	CLOSE curPKeys
	DEALLOCATE curPKeys
	
	SET @InsertPKey = REPLACE(@PKey, '{0}', 'INSERTED')
	SET @DeletePKey = REPLACE(@PKey, '{0}', 'DELETED')
	SET @UpdatePKey = @InsertPKey

	--Loop through columns from current table
	DECLARE curColumns CURSOR FOR
    SELECT sys.columns.name AS name, CASE WHEN sys.objects.type = 'U' AND sys.columns.max_length = -1 THEN 'text' ELSE sys.types.name END AS datatype, C.DataType AS ColDataType
    FROM ((sys.columns INNER JOIN sys.objects ON sys.columns.object_id = sys.objects.object_id) INNER JOIN sys.types ON sys.columns.user_type_id =  sys.types.user_type_id)
    LEFT JOIN (CustomTablesWithUDIC G INNER JOIN FW_CustomColumns C ON C.InfoCenterArea = G.InfoCenterArea AND C.GridID = G.GridID AND c.InfoCenterArea = @InfoCenterArea) ON G.TableName = sys.objects.name AND C.Name = sys.columns.name
    WHERE sys.objects.type = 'U' AND sys.objects.name = @TableName
    ORDER BY sys.columns.column_id
	BEGIN
		OPEN curColumns

		FETCH NEXT FROM curColumns INTO @ColumnName, @DataType, @ColDataType

		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF @ColumnName IN ('createdate', 'createuser', 'moddate', 'moduser')
				FETCH NEXT FROM curColumns INTO @ColumnName, @DataType, @ColDataType
			ELSE BEGIN
				IF @DataType IN ('image', 'text', 'ntext') BEGIN
					SET @InsertTrigger = @InsertTrigger + REPLACE(REPLACE(REPLACE(@SkipInsertSQL, '{0}', @ColumnName), '{1}', @DataType), '{2}', @InsertPKey)
					SET @UpdateTrigger = @UpdateTrigger + REPLACE(REPLACE(REPLACE(@SkipUpdateSQL, '{0}', @ColumnName), '{1}', @DataType), '{2}', @UpdatePKey)
					SET @DeleteTrigger = @DeleteTrigger + REPLACE(REPLACE(REPLACE(@SkipDeleteSQL, '{0}', @ColumnName), '{1}', @DataType), '{2}', @DeletePKey)
				END
				ELSE BEGIN
					SET @LookupTable = ''
					SET @LookupValueColumn = ''
					SET @LookupJoinColumn = ''
					
					SELECT @LookupTable = LookupTable, @LookupValueColumn = LookupValueColumn, @LookupJoinColumn = LookupJoinColumn FROM dbo.GetLookupCols(@TableName, @ColumnName, @ColDataType)

					IF @LookupTable = '' BEGIN
						SET @InsertTrigger = @InsertTrigger + REPLACE(REPLACE(@InsertSQL, '{0}', @ColumnName), '{1}', @InsertPKey)
						SET @UpdateTrigger = @UpdateTrigger + REPLACE(REPLACE(@UpdateBeforeSQL, '{0}', @ColumnName), '{1}', @UpdatePKey)
						SET @UpdateTrigger = @UpdateTrigger + @UpdateCheck
						SET @UpdateTrigger = @UpdateTrigger + REPLACE(@UpdateAfterSQL, '{0}', @ColumnName)
						SET @DeleteTrigger = @DeleteTrigger + REPLACE(REPLACE(@DeleteSQL, '{0}', @ColumnName), '{1}', @DeletePKey)
					END
					ELSE BEGIN
						SET @InsertTrigger = @InsertTrigger + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@DescripInsertSQL, '{0}', @ColumnName), '{1}', @InsertPKey), '{2}', @LookupTable), '{3}', REPLACE(@LookupValueColumn, '{0}', 'newDesc')), '{4}', @LookupJoinColumn)
						SET @UpdateTrigger = @UpdateTrigger + REPLACE(REPLACE(REPLACE(REPLACE(@DescripBeforeUpdateSQL, '{0}', @ColumnName), '{1}', @UpdatePKey), '{2}', REPLACE(@LookupValueColumn, '{0}', 'oldDesc')), '{3}', REPLACE(@LookupValueColumn, '{0}', 'newDesc'))
						SET @UpdateTrigger = @UpdateTrigger + @UpdateCheck
						SET @UpdateTrigger = @UpdateTrigger + REPLACE(REPLACE(REPLACE(@DescripAfterUpdateSQL, '{0}', @ColumnName), '{1}', @LookupTable), '{2}', @LookupJoinColumn)
						SET @DeleteTrigger = @DeleteTrigger + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@DescripDeleteSQL, '{0}', @ColumnName), '{1}', @DeletePKey), '{2}', @LookupTable), '{3}', REPLACE(@LookupValueColumn, '{0}', 'oldDesc')), '{4}', @LookupJoinColumn)
					END
				END

				FETCH NEXT FROM curColumns INTO @ColumnName, @DataType, @ColDataType
			END			
		END
	END
	CLOSE curColumns
	DEALLOCATE curColumns

	SET @DeleteTrigger = @DeleteTrigger + CHAR(13) + 'END'

	SET @InsertTrigger = @InsertTrigger + @EndText
	SET @UpdateTrigger = @UpdateTrigger + @EndText
	SET @DeleteTrigger = @DeleteTrigger + @EndText

	--Create Insert Trigger
	SET @TriggerName = 'dbo.VisionAudit_Insert_' + @TableName
	PRINT 'Creating trigger ' + @TriggerName + '...'
	SET @SQLStr = REPLACE(REPLACE(@DropTrigger, '{0}', @TableName), '{1}', 'Insert')
	EXEC (@SQLStr)
	EXEC (@InsertTrigger)
	IF (@@ERROR = 0)
		PRINT '...Trigger ' + @TriggerName + ' created'
	ELSE
		PRINT '...FAILED to create trigger ' + @TriggerName

	--Create Update Trigger
	SET @TriggerName = 'dbo.VisionAudit_Update_' + @TableName
	PRINT 'Creating trigger ' + @TriggerName + '...'
	SET @SQLStr = REPLACE(REPLACE(@DropTrigger, '{0}', @TableName), '{1}', 'Update')
	EXEC (@SQLStr)
	EXEC (@UpdateTrigger)
	IF (@@ERROR = 0)
		PRINT '...Trigger ' + @TriggerName + ' created'
	ELSE
		PRINT '...FAILED to create trigger ' + @TriggerName

	--Create Delete Trigger
	SET @TriggerName = 'dbo.VisionAudit_Delete_' + @TableName
	PRINT 'Creating trigger ' + @TriggerName + '...'
	SET @SQLStr = REPLACE(REPLACE(@DropTrigger, '{0}', @TableName), '{1}', 'Delete')
	EXEC (@SQLStr)
	EXEC (@DeleteTrigger)
	IF (@@ERROR = 0)
		PRINT '...Trigger ' + @TriggerName + ' created'
	ELSE
		PRINT '...FAILED to create trigger ' + @TriggerName

	IF (SELECT AuditingEnabled FROM FW_CFGSystem) = 'N' BEGIN
		SET @SQLStr = 'ALTER TABLE ' + @TableName + ' disable trigger all'
		EXEC (@SQLStr)
	END
END
GO
