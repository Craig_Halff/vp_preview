SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_UpdatePending] ( @wbs1 nvarchar(32)= '', @user nvarchar(32)= '', @employeeId nvarchar(32)= '', @historyInserts char(1)= 'C')
             AS EXEC spCCG_EI_UpdatePending @wbs1,@user,@employeeId,@historyInserts
GO
