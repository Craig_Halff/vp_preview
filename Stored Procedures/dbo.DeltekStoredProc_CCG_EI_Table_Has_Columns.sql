SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Table_Has_Columns]
	@tableName		varchar(512),
	@columnName1	varchar(512),
	@columnName2	varchar(512) = NULL,
	@columnName3	varchar(512) = NULL
AS
BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_EI_Table_Has_Columns] 'ProjectCustomTabFields', 'CustInvoiceStage', 'CustInvoiceStageDate'
	SET NOCOUNT ON;
	DECLARE @fields int;
	SET @fields = 1
		+ (CASE WHEN @columnName2 IS NULL THEN 0 ELSE 1 END)
		+ (CASE WHEN @columnName3 IS NULL THEN 0 ELSE 1 END);

	SELECT CASE WHEN COUNT('x') = @fields THEN 'yes' ELSE 'no' END As HasFields
		FROM sys.objects b, sys.columns a
		WHERE b.object_id = OBJECT_ID(N'[dbo].[' + @tableName + ']') AND type in (N'U')
			AND a.object_id = b.object_id
			AND (a.name = @columnName1
				OR a.name = ISNULL(@columnName2, '')
				OR a.name = ISNULL(@columnName3, '')
			);
END
GO
