SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_SetStage] ( @PendingSeq int, @PayableSeq int, @Stage varchar(30), @ChangedBy nvarchar(32))
             AS EXEC spCCG_PAT_SetStage @PendingSeq,@PayableSeq,@Stage,@ChangedBy
GO
