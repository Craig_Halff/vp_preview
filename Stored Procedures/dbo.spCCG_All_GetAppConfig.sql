SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_All_GetAppConfig](
	@app	varchar(50)
)
AS BEGIN
	if @app not in ('ARM','EI','TR','PAT') return
	declare @sql nvarchar(max) = N'select * from CCG_'+@app+'_Config'
	exec (@sql)
END
GO
