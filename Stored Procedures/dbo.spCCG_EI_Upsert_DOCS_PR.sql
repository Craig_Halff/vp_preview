SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_Upsert_DOCS_PR]
	@FILEID		varchar(50),
	@WBS1		Nvarchar(30),
	@WBS2		Nvarchar(7),
	@WBS3		Nvarchar(7),
	@Invoice	Nvarchar(32),
	@FilePath   Nvarchar(500),
	@FileDescription Nvarchar(500),
	@FileSize	int,
	@CreateUser Nvarchar(32),
	@CreateDate Datetime,
	@StorageSeq int,
	@StoragePathOrID	Nvarchar(500),
	@RevisionSeq int,
	@Filestate varchar(50),
	@Category  varchar(50)
AS
BEGIN
	-- EXEC [dbo].[spCCG_EI_Upsert_DOCS_PR] '2003005.00'
	SET NOCOUNT ON;
	
	declare @state varchar(50)

	SELECT @state = FileState from CCG_DOCS_PR where FILEID = @FILEID

	IF ISNULL(@state,'') = ''
	BEGIN

		INSERT INTO [dbo].[CCG_DOCS_PR]
           ([FILEID]
           ,[WBS1]
           ,[WBS2]
           ,[WBS3]
           ,[Invoice]
           ,[FilePath]
           ,[FileDescription]
		   ,[FileSize]
           --,[SortBy]
           ,[CreateUser]
           ,[CreateDate]
           ,[StorageSeq]
           ,[StoragePathOrID]
           ,[RevisionSeq]
           ,[FileState]
           ,[Category])
     VALUES
           (@FILEID
           ,@WBS1
           ,@WBS2
           ,@WBS3
           ,@Invoice
           ,@FilePath
           ,@FileDescription
		   ,@FileSize
           --,@SortBy
           ,@CreateUser
           ,@CreateDate
           ,@StorageSeq
           ,@StoragePathOrID
           ,@RevisionSeq
           ,@FileState
           ,@Category
		   )


	END
	ELSE IF @state = 'PENDING'
		--special case for pending file, only update the new state to keep edits from watcherlog
		UPDATE [CCG_DOCS_PR]
		SET
           [FileState] = @Filestate
		   WHERE FILEID = @FILEID
	ELSE 
	BEGIN
		UPDATE [CCG_DOCS_PR]
		SET
           [WBS1] = @WBS1
           ,[WBS2] = @WBS2
           ,[WBS3] = @WBS3
           ,[Invoice] = @Invoice
           ,[FilePath] = @FilePath
           ,[FileDescription] = @FileDescription
		   ,[FileSize] = @FileSize
           --,[SortBy]
           ,[CreateUser] = @CreateUser
           ,[CreateDate] = @CreateDate
           ,[StorageSeq] = @StorageSeq
           ,[StoragePathOrID] = @StoragePathOrID
           ,[RevisionSeq] = @RevisionSeq
           ,[FileState] = @Filestate
           ,[Category] = @Category

		   WHERE FILEID = @FILEID

	END
END;
GO
