SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_AddDisplayColumn] ( @OverridePriorSettings char(1), @Header varchar(255), @FieldName varchar(255), @ColType char(1), @DrilldownColumn char(1), @ColSeq int, @ColDecimals int, @ColWidth int= 0, @ColEnabledWBS1 char(1)='Y', @ColEnabledWBS2 char(1)='Y', @ColEnabledWBS3 char(1)='Y', @ColStatus char(1)='A', @ColViewRoles varchar(255)='')
             AS EXEC spCCG_PAT_AddDisplayColumn @OverridePriorSettings,@Header,@FieldName,@ColType,@DrilldownColumn,@ColSeq,@ColDecimals,@ColWidth,@ColEnabledWBS1,@ColEnabledWBS2,@ColEnabledWBS3,@ColStatus,@ColViewRoles
GO
