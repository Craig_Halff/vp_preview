SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_OrgStructureChange] 
	@UDIC_UID varchar(32),
	@Position varchar (255) -- Team Leader, Director, Management Leader & Administrative Assistant or Team Practice Area
AS 
/* 
Copyright (c) 2017 Central Consulting Group.  All rights reserved.
03/24/2017	David Springer
			Update Team Leader, Director, Management Leader, Admin Assistant & Contract Admin
			in Opportunity and Project.
			Call this on a Org Structure CHANGE workflow 
			when Team Leader, Director, Management Leader & Admin Assistant have changed.
02/05/2018	David Springer
			Added Team Practice Area (which is not a Postion)
06/29/2018	David Springer
			Added Billing Team Leader and Emloyee info center
08/08/2019	David Springer
			Added Supervisor, EKPracticeLeader, Team Leader checkbox, Director checkbox, Operations Manager checkbox
12/09/2019	David Springer
			Matched Org Structure Sync procedure
07/14/2020	David Springer
			Added Operations Manager for Project info center
02/23/2021	Craig H. Anderson
			Modified to account for Regional Practice Leaders (exclude them)
*/
BEGIN
SET NOCOUNT ON

If @Position = 'Administrative Assistant'
	Begin
	Update ox
	Set ox.CustAdministrativeAssistant = x.CustAdministrativeAssistant
	From Opportunity o, OpportunityCustomTabFields ox, UDIC_OrganizationStructure x
	Where o.OpportunityID = ox.OpportunityID
	  and x.UDIC_UID = @UDIC_UID
	  and o.Org = x.CustOrganization
	  and (o.Status = 'A' or o.Stage = '11')  -- Awarded

	Update px
	Set px.CustAdministrativeAssistant = x.CustAdministrativeAssistant
	From PR p, ProjectCustomTabFields px, UDIC_OrganizationStructure x
	Where p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.WBS3 = px.WBS3
	  and x.UDIC_UID = @UDIC_UID
	  and p.Org = x.CustOrganization
	-- project level is Active
	  and exists (Select 'x' From PR Where WBS1 = p.WBS1 and WBS2 = ' ' and Status <> 'D')
	End

If @Position = 'Marketing Manager'
	Begin
	Update ox
	Set ox.CustMarketingManager = x.CustMarketingManager
	From Opportunity o, OpportunityCustomTabFields ox, UDIC_OrganizationStructure x
	Where o.OpportunityID = ox.OpportunityID
	  and x.UDIC_UID = @UDIC_UID
	  and o.Org = x.CustOrganization
	  and o.Status = 'A'
	-- Existing marketing manager is termed
	  and exists (Select 'x' From EM Where Employee = ox.CustMarketingManager and Status = 'T')

	Update px
	Set px.CustMarketingManager = x.CustMarketingManager
	From PR p, ProjectCustomTabFields px, UDIC_OrganizationStructure x
	Where p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.WBS3 = px.WBS3
	  and x.UDIC_UID = @UDIC_UID
	  and p.Org = x.CustOrganization
	-- project level is Active
	  and exists (Select 'x' From PR Where WBS1 = p.WBS1 and WBS2 = ' ' and Status <> 'D')
	-- Existing marketing manager is termed
	  and exists (Select 'x' From EM Where Employee = px.CustMarketingManager and Status = 'T')
	End

-- Opportunity only
If @Position = 'Contract Admin'
	Begin
	Update ox
	Set ox.CustBiller = x.CustContractAdmin
	From Opportunity o, OpportunityCustomTabFields ox, UDIC_OrganizationStructure x
	Where o.OpportunityID = ox.OpportunityID
	  and x.UDIC_UID = @UDIC_UID
	  and o.Org = x.CustOrganization
	  and (o.Status = 'A' or o.Stage = '11')  -- Awarded
	End

-- ####################  Team Leader  ##########################
If @Position = 'Team Leader'
	Begin
	Insert Into Employees_RoleLog
	(Employee, Seq, CustRole, CustDate, CustAction)
	Select Employee, Replace (NewID(), '-', '') Seq, 'Team Leader' Role, getDate(), 'Added'
	From (Select distinct Employee
		From EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
		Where x.UDIC_UID = @UDIC_UID
			and x.CustTeamLeader = ex.Employee
			and ex.CustTeamLeader = 'N'
			and not exists (Select 'x' From Employees_RoleLog Where Employee = ex.Employee and CustRole = 'Team Leader' and CustAction = 'Added')
		) a

	Update ex
	Set ex.CustTeamLeader = 'Y'
	From EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where x.UDIC_UID = @UDIC_UID
	  and x.CustTeamLeader = ex.Employee

	Update ex
	Set ex.CustTeamLeader = 'N'
	From EmployeeCustomTabFields ex
	Where ex.CustTeamLeader = 'Y'
	  and not exists (Select 'x' From UDIC_OrganizationStructure x Where x.CustTeamLeader = ex.Employee)

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Team Leader'
	  and ex.CustTeamLeader = 'N'

	Update o
	Set o.Principal = x.CustTeamLeader
	From Opportunity o, UDIC_OrganizationStructure x
	Where x.UDIC_UID = @UDIC_UID
	  and o.Org = x.CustOrganization
	  and (o.Status = 'A' or o.Stage = '11')  -- Awarded

	Update p
	Set p.Principal = x.CustTeamLeader
	From PR p, UDIC_OrganizationStructure x
	Where x.UDIC_UID = @UDIC_UID
	  and p.Org = x.CustOrganization
	-- project level is Active
	  and exists (Select 'x' From PR Where WBS1 = p.WBS1 and WBS2 = ' ' and Status <> 'D')

	Update ex
	Set ex.CustEKApprover = x.CustTeamLeader
	From EM e, EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where e.Employee = ex.Employee
	  and x.UDIC_UID = @UDIC_UID
	  and e.Org = x.CustOrganization
	  and (e.Status <> 'T' or DateAdd (day, -365, getDate()) > e.TerminationDate)

	Update px
	Set px.CustBillingTeamLeader = x.CustTeamLeader
	From PR p, ProjectCustomTabFields px, UDIC_OrganizationStructure x
	Where x.UDIC_UID = @UDIC_UID
	  and p.Org = x.CustOrganization
	  and p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.WBS3 = px.WBS3
	-- project level is Active
	  and exists (Select 'x' From PR Where WBS1 = p.WBS1 and WBS2 = ' ' and Status <> 'D')

	Update e
	Set e.Supervisor = x.CustTeamLeader
	From EMCompany e, UDIC_OrganizationStructure x
	Where x.UDIC_UID = @UDIC_UID
	  and e.Org = x.CustOrganization
	  and (e.Status <> 'T' or DateAdd (day, -10, getDate()) < e.TerminationDate)
	  and IsNull(e.Supervisor,'1') <> IsNull(x.CustTeamLeader,'1') 

	End

-- ####################  Alternate Approver  ##########################
If @Position = 'Alternate Approver'
	Begin
	Update ex
	Set ex.CustEKAlternateApprover = x.CustAlternateApprover
	From EM e, EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where e.Employee = ex.Employee
	  and x.UDIC_UID = @UDIC_UID
	  and e.Org = x.CustOrganization
	  and (e.Status <> 'T' or DateAdd (day, -365, getDate()) > e.TerminationDate)
	End

-- ####################  Director  ##########################
If @Position = 'Director'
	Begin
	Insert Into Employees_RoleLog
	(Employee, Seq, CustRole, CustDate, CustAction)
	Select Employee, Replace (NewID(), '-', '') Seq, 'Director' Role, getDate(), 'Added'
	From (Select distinct Employee
		From EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
		Where x.UDIC_UID = @UDIC_UID
			and x.CustDirector = ex.Employee
			and ex.CustRoleDirector = 'N'
			and not exists (Select 'x' From Employees_RoleLog Where Employee = ex.Employee and CustRole = 'Director' and CustAction = 'Added')
		) a

	Update ex
	Set ex.CustRoleDirector = 'Y'
	From EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where x.UDIC_UID = @UDIC_UID
	  and x.CustDirector = ex.Employee

	Update ex
	Set ex.CustRoleDirector = 'N'
	From EmployeeCustomTabFields ex
	Where ex.CustRoleDirector = 'Y'
	  and not exists (Select 'x' From UDIC_OrganizationStructure x Where x.CustDirector = ex.Employee)

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Director'
	  and ex.CustRoleDirector = 'N'

	Update ox
	Set ox.CustDirector = x.CustDirector
	From Opportunity o, OpportunityCustomTabFields ox, UDIC_OrganizationStructure x
	Where o.OpportunityID = ox.OpportunityID
	  and x.UDIC_UID = @UDIC_UID
	  and o.Org = x.CustOrganization
	  and (o.Status = 'A' or o.Stage = '11')  -- Awarded

	Update px
	Set px.CustDirector = x.CustDirector
	From PR p, ProjectCustomTabFields px, UDIC_OrganizationStructure x
	Where p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.WBS3 = px.WBS3
	  and x.UDIC_UID = @UDIC_UID
	  and p.Org = x.CustOrganization
	-- project level is Active
	  and exists (Select 'x' From PR Where WBS1 = p.WBS1 and WBS2 = ' ' and Status <> 'D')

	Update ex
	Set ex.CustEKDirector = x.CustDirector
	From EM e, EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where e.Employee = ex.Employee
	  and x.UDIC_UID = @UDIC_UID
	  and e.Org = x.CustOrganization
	  and (e.Status <> 'T' or DateAdd (day, -365, getDate()) > e.TerminationDate)
	End

-- ####################  Operations Manager  ##########################
If @Position = 'Operations Manager'
	Begin
/*	
	Update ex
	Set ex.CustOperationsManager = 'Y'
	From EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where x.UDIC_UID = @UDIC_UID
	  and x.CustOperationsManager = ex.Employee
*/
	Update ex
	Set ex.CustOperationsManager = 'N'
	From EmployeeCustomTabFields ex
	Where ex.CustOperationsManager = 'Y'
	  and not exists (Select 'x' From UDIC_OrganizationStructure x Where x.CustOperationsManager = ex.Employee)

	Update l
	Set l.CustAction = 'Removed'
	From EmployeeCustomTabFields ex, Employees_RoleLog l
	Where ex.Employee = l.Employee
	  and l.CustAction = 'Added'
	  and l.CustRole = 'Operations Manager'
	  and ex.CustOperationsManager = 'N'

	Update ex
	Set ex.CustEKOperationsManager = x.CustOperationsManager
	From EM e, EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where e.Employee = ex.Employee
	  and x.UDIC_UID = @UDIC_UID
	  and e.Org = x.CustOrganization
	  and (e.Status <> 'T' or DateAdd (day, -365, getDate()) > e.TerminationDate)

	Update px
	Set px.CustOperationsManager = x.CustOperationsManager
	From PR p, ProjectCustomTabFields px, UDIC_OrganizationStructure x
	Where x.UDIC_UID = @UDIC_UID
	  and x.CustOrganization = p.Org
	-- project level is Active
	  and exists (Select 'x' From PR Where WBS1 = p.WBS1 and WBS2 = ' ' and Status <> 'D')
	  and p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.WBS3 = px.WBS3
	End

-- ####################  Team Practice Area  ##########################
If @Position = 'Team Practice Area'
	Begin
	Update ox
	Set ox.CustTeamPracticeArea = x.CustTeamPracticeArea
	From Opportunity o, OpportunityCustomTabFields ox, UDIC_OrganizationStructure x
	Where o.OpportunityID = ox.OpportunityID
	  and x.UDIC_UID = @UDIC_UID
	  and o.Org = x.CustOrganization
	  and (o.Status = 'A' or DateAdd (day, -90, getDate()) < o.CloseDate)

	Update px
	Set px.CustTeamPracticeArea = x.CustTeamPracticeArea
	From PR p, ProjectCustomTabFields px, UDIC_OrganizationStructure x
	Where p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.WBS3 = px.WBS3
	  and x.UDIC_UID = @UDIC_UID
	  and p.Org = x.CustOrganization
	-- project level is Active
	  and exists (Select 'x' From PR Where WBS1 = p.WBS1 and WBS2 = ' ' and Status <> 'D')

	Update ex
	Set ex.CustTeamPracticeArea = x.CustTeamPracticeArea
	From EM e, EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where e.Employee = ex.Employee
	  and x.UDIC_UID = @UDIC_UID
	  and e.Org = x.CustOrganization
	  and (e.Status <> 'T' or DateAdd (day, -365, getDate()) > e.TerminationDate)

	Update ex
	Set ex.CustEKPracticeLeader = pa.Employee
	From UDIC_OrganizationStructure x, Employees_PracticeAreas pa, EM e, EmployeeCustomTabFields ex
	Where x.UDIC_UID = @UDIC_UID
	  and x.CustTeamPracticeArea = pa.CustPracticeAreaLeader
	  and pa.CustPracticeAreaLeader = ex.CustTeamPracticeArea
	  AND pa.CustPracticeAreaLeaderType = 'Primary'
	  and ex.Employee = e.Employee
	  and (e.Status <> 'T' or DateAdd (day, -10, getDate()) < e.TerminationDate)
	  and IsNull(ex.CustEKPracticeLeader,'1') <> IsNull(pa.Employee,'1') 

	End

-- ####################  Management Leader  ##########################
If @Position = 'Management Leader'
	Begin
	Update ox
	Set ox.CustManagementLeader = x.CustManagementLeader
	From Opportunity o, OpportunityCustomTabFields ox, UDIC_OrganizationStructure x
	Where o.OpportunityID = ox.OpportunityID
	  and x.UDIC_UID = @UDIC_UID
	  and o.Org = x.CustOrganization
	  and (o.Status = 'A' or o.Stage = '11')  -- Awarded

	Update px
	Set px.CustManagementLeader = x.CustManagementLeader
	From PR p, ProjectCustomTabFields px, UDIC_OrganizationStructure x
	Where p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.WBS3 = px.WBS3
	  and x.UDIC_UID = @UDIC_UID
	  and p.Org = x.CustOrganization
	-- project level is Active
	  and exists (Select 'x' From PR Where WBS1 = p.WBS1 and WBS2 = ' ' and Status <> 'D')

	Update ex
	Set ex.CustEKManagementLeader = x.CustManagementLeader
	From EM e, EmployeeCustomTabFields ex, UDIC_OrganizationStructure x
	Where e.Employee = ex.Employee
	  and x.UDIC_UID = @UDIC_UID
	  and e.Org = x.CustOrganization
	  and (e.Status <> 'T' or DateAdd (day, -365, getDate()) > e.TerminationDate)
	End

END
GO
