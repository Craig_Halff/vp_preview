SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WBSInTables] 
  @WBS1 nvarchar(30),
  @WBS2 nvarchar(30),
  @WBS3 nvarchar(30),
  @TableName nvarchar(50) output, 
  @CheckPlanning varchar(1) = 'N'

as
begin

  DECLARE @BaseStmt	nvarchar(max)
  DECLARE @SqlStmt nvarchar(max)
  DECLARE @conditions nvarchar(max)
  DECLARE @strParms nvarchar(max)

  set @TableName = ''
  SET @strParms = N'@WBS1 nvarchar(30), @WBS2 nvarchar(30), @WBS3 nvarchar(30), @TableName nvarchar(50) OUT'

  SET @conditions = '?WBS1 = @WBS1' + 
	case when rtrim(@WBS2) <> '' then ' and ?WBS2 = @WBS2' else '' end +
	case when rtrim(@WBS3) <> '' then ' and ?WBS3 = @WBS3' else '' end   
  set @BaseStmt = 'Select TOP 1 @TableName=''?Table'' from ?Table where ' + @conditions

  IF RTRIM (@WBS1) = '' RETURN  

  -- LD table, BilledWBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','LD'),'?WBS','BilledWBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- LD table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','LD'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

    -- LedgerAP table, BilledWBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','LedgerAP'),'?WBS','BilledWBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- LedgerAP table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','LedgerAP'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- LedgerAR table, BilledWBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','LedgerAR'),'?WBS','BilledWBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- LedgerAR table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','LedgerAR'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- LedgerEX table, BilledWBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','LedgerEX'),'?WBS','BilledWBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- LedgerEX table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','LedgerEX'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- LedgerMisc table, BilledWBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','LedgerMisc'),'?WBS','BilledWBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- LedgerMisc table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','LedgerMisc'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- POCOMMITMENT table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','POCOMMITMENT'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- tkDetail table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','tkDetail'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- tsDetail table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','tsDetail'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- ADPCode table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','ADPCode'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- apDetail table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','apDetail'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- apppChecks table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','apppChecks'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- AR table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','AR'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- ARC table, WBS1 column
  if rtrim(@WBS2)='' and rtrim(@WBS3)=''
  BEGIN 
    SET @SqlStmt = replace(replace(@BaseStmt,'?Table','ARC'),'?WBS','WBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN  
  END

  -- ARCreditMemo table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','ARCreditMemo'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- ARPreInvoice table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','ARPreInvoice'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- ARPreInvoiceDetail table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','ARPreInvoiceDetail'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- BIED table, BilledWBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','BIED'),'?WBS','BilledWBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- BIED table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','BIED'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- BILD table, BilledWBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','BILD'),'?WBS','BilledWBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- BILD table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','BILD'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- billInvMaster table, BillWBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','billInvMaster'),'?WBS','BillWBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  if rtrim(@WBS2)='' and rtrim(@WBS3)=''
  BEGIN
    -- billInvMaster table, ByGroupWBS1 column
    SET @SqlStmt = replace(replace(@BaseStmt,'?Table','billInvMaster'),'?WBS','ByGroupWBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN  

    -- billInvMaster table, MainWBS1 column
    SET @SqlStmt = replace(replace(@BaseStmt,'?Table','billInvMaster'),'?WBS','MainWBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN  

    -- billPreInvoice table, WBS1 column
    SET @SqlStmt = replace(replace(@BaseStmt,'?Table','billPreInvoice'),'?WBS','WBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN  
  END 

  -- BTF table, PostWBS column(s) (only test if PostWBS1 <> WBS1 or if PostWBS2 being tested
  IF RTRIM(@WBS2)=''
    BEGIN
      SET @SqlStmt = replace(replace(@BaseStmt,'?Table','BTF'),'?WBS','PostWBS') + ' and PostWBS1 <> WBS1'
      EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
      IF (@TableName<> '') RETURN  
    END
  ELSE
    BEGIN
      SET @SqlStmt = replace(replace(@BaseStmt,'?Table','BTF'),'?WBS','PostWBS')
      EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
      IF (@TableName<> '') RETURN  
    END

  if RTRIM(@WBS2)='' and RTRIM(@WBS3)=''
  BEGIN 

    -- BTBGSubs table, MainWBS1 column
    SET @SqlStmt = replace(replace(@BaseStmt,'?Table','BTBGSubs'),'?WBS','MainWBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN  

    -- BTBGSubs table, SubWBS1 column
    SET @SqlStmt = replace(replace(@BaseStmt,'?Table','BTBGSubs'),'?WBS','SubWBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN  

    -- RevenueGroupSubs table, MainWBS1 column
    SET @SqlStmt = replace(replace(@BaseStmt,'?Table','RevenueGroupSubs'),'?WBS','MainWBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN  

    -- RevenueGroupSubs table, SubWBS1 column
    SET @SqlStmt = replace(replace(@BaseStmt,'?Table','RevenueGroupSubs'),'?WBS','SubWBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN  

  END

  -- RevenueGroup table, RevWBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','RevenueGroup'),'?WBS','RevWBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- cdDetail table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','cdDetail'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- CFGAPDiscount table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','CFGAPDiscount'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- CFGAssetMain table
  SET @SqlStmt = 'Select TOP 1 @TableName=''CFGAssetMain'' from CFGAssetMain WHERE ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','DOWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','FAWBS') + ') ' 
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN   

  -- CFGAutoPosting table
  SET @SqlStmt = 'Select TOP 1 @TableName=''CFGAutoPosting'' from CFGAutoPosting WHERE ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ConsAccrWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','EmplAdvCreditWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','EmplExpCreditWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','LaborCreditWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','MiscCreditWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','PrintsCreditWBS') + ') OR '
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','RealizedGainsWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','RealizedLossesWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','UnbilledRealizedGainsWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','UnbilledRealizedLossesWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','UnbilledUnrealizedGainsWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','UnbilledUnrealizedLossesWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','UnrealizedGainsWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','UnrealizedLossesWBS') + ') ' 
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN   

  -- Organization table
  SET @SqlStmt = 'Select TOP 1 @TableName=''Organization'' from Organization WHERE ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ConsAccrWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','EmplAdvWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','EmplExpWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','FICAWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingBSOtherCreditWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingBSOtherDebitWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingExpOHCreditWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingExpOHDebitWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingExpOHFromWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingExpOHToWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingExpPromoCreditWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingExpPromoDebitWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingExpPromoFromWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingExpPromoToWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingExpRegCreditWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingExpRegDebitWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingExpRegFromWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingExpRegToWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingLabOHCreditWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingLabOHDebitWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingLabOHFromWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingLabOHToWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingLabPromoCreditWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingLabPromoDebitWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingLabPromoFromWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingLabPromoToWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingLabRegCreditWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingLabRegDebitWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingLabRegFromWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingLabRegToWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','LabDistWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','MiscWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','OHVarianceWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','PayrollContributionCreditWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','PayrollContributionDebitWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','PayrollWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','PrintsWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','RealizedGainsWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','RealizedLossesWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','UnbilledRealizedGainsWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','UnbilledRealizedLossesWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','UnbilledUnrealizedGainsWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','UnbilledUnrealizedLossesWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','UnitsBSWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','UnitsIndWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','UnitsOtherWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','UnrealizedGainsWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','UnrealizedLossesWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','XChargeOHCreditWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','XChargeOHDebitWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','XChargeOHFromWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','XChargeOHToWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','XChargeRegCreditWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','XChargeRegDebitWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','XChargeRegFromWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','XChargeRegToWBS') + ') ' 
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- CFGPYMain table
  SET @SqlStmt = 'Select TOP 1 @TableName=''CFGPYMain'' from CFGPYMain WHERE ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','BonusCostWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','FICAWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','OtherPay2CostWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','OtherPay3CostWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','OtherPay4CostWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','OtherPay5CostWBS') + ') OR '
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','OtherPayCostWBS') + ') ' 
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- CFGPYContribution table
  SET @SqlStmt = 'Select TOP 1 @TableName=''CFGPYContribution'' from CFGPYContribution WHERE ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','CreditWBS') + ') OR '
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','DebitWBS') + ') ' 
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- CFGPYWHCodes, PayrollWBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','CFGPYWHCodes'),'?WBS','PayrollWBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- CFGEKCategory table, CompanyPaidWBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','CFGEKCategory'),'?WBS','CompanyPaidWBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- CFGEKMain table, CompanyPaidWBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','CFGEKMain'),'?WBS','CompanyPaidWBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- CFGOHMain table, OHVarianceWBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','CFGOHMain'),'?WBS','OHVarianceWBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- CFGCreditCardSecondary table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','CFGCreditCardSecondary'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  


  if RTRIM(@WBS2)='' and RTRIM(@WBS3)='' 
  BEGIN

    -- CFGTimeAnalysis table, EndWBS1 column
    SET @SqlStmt = replace(replace(@BaseStmt,'?Table','CFGTimeAnalysis'),'?WBS','EndWBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN  

    -- CFGTimeAnalysis table, StartWBS1 column
    SET @SqlStmt = replace(replace(@BaseStmt,'?Table','CFGTimeAnalysis'),'?WBS','StartWBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN  

	-- InvoiceVoid table
	SET @SqlStmt = replace(replace(@BaseStmt,'?Table','InvoiceVoid '),'?WBS','WBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN   
  END

  -- CFGTKCategory table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','CFGTKCategory'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- CFGBanks table, WireTransferFeeDebitWBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','CFGBanks'),'?WBS','WireTransferFeeDebitWBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- CFGBankEntriesCodeData table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','CFGBankEntriesCodeData'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- CFGBillTaxes table
  SET @SqlStmt = 'Select TOP 1 @TableName=''CFGBillTaxes'' from CFGBillTaxes WHERE ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','NonRecoverCreditWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','NonRecoverDebitWBS') + ') ' 
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- CFGICBilling table, ICBillingReclassWBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','CFGICBilling'),'?WBS','ICBillingReclassWBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- CFGICBillingTerms table
  SET @SqlStmt = 'Select TOP 1 @TableName=''CFGICBillingTerms'' from CFGICBillingTerms WHERE ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingInvoiceWBS') + ') OR ' 
  SET @SqlStmt = @SqlStmt + '(' + replace(@conditions, '?WBS','ICBillingVoucherWBS') + ') ' 
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- CFGInventoryMain table, InventoryWBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','CFGInventoryMain'),'?WBS','InventoryWBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- crDetail table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','crDetail'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- cvDetail table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','cvDetail'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  
  
  -- ekDetail table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','ekDetail'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- exDetail table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','exDetail'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- inDetail table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','inDetail'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- inMaster table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','inMaster'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- jeDetail table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','jeDetail'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- laDetail table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','laDetail'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- miDetail table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','miDetail'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- POPRWBS table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','POPRWBS'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- POPRCOSTDETAIL table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','POPRCOSTDETAIL'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- POPQWBS table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','POPQWBS'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- POPQCOSTDETAIL table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','POPQCOSTDETAIL'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- POCOSTDETAIL table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','POCOSTDETAIL'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- POWBS table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','POWBS'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- OverheadAllocation table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','OverheadAllocation'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- prDetail table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','prDetail'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- PRF table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','PRF'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- UN table, CreditWBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','UN'),'?WBS','CreditWBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- unDetail table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','unDetail'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  -- upDetail table, WBS column(s)
  SET @SqlStmt = replace(replace(@BaseStmt,'?Table','upDetail'),'?WBS','WBS')
  EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
  IF (@TableName<> '') RETURN  

  if @CheckPlanning = 'Y' 
  BEGIN 

    -- RPAssignment table, WBS column(s)
    SET @SqlStmt = replace(replace(@BaseStmt,'?Table','RPAssignment'),'?WBS','WBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN  

    -- RPExpense table, WBS column(s)
    SET @SqlStmt = replace(replace(@BaseStmt,'?Table','RPExpense'),'?WBS','WBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN  

    -- RPConsultant table, WBS column(s)
    SET @SqlStmt = replace(replace(@BaseStmt,'?Table','RPConsultant'),'?WBS','WBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN  

    -- RPUnit table, WBS column(s)
    SET @SqlStmt = replace(replace(@BaseStmt,'?Table','RPUnit'),'?WBS','WBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN  

    -- PNAssignment table, WBS column(s)
    SET @SqlStmt = replace(replace(@BaseStmt,'?Table','PNAssignment'),'?WBS','WBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN  

    -- PNExpense table, WBS column(s)
    SET @SqlStmt = replace(replace(@BaseStmt,'?Table','PNExpense'),'?WBS','WBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN  

    -- PNConsultant table, WBS column(s)
    SET @SqlStmt = replace(replace(@BaseStmt,'?Table','PNConsultant'),'?WBS','WBS')
    EXEC sp_executesql @SqlStmt, @strParms, @WBS1 = @WBS1, @WBS2 = @WBS2, @WBS3 = @WBS3, @TableName = @TableName out
    IF (@TableName<> '') RETURN  

  END
end -- WBSInTables  
GO
