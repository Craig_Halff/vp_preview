SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPCreatePlanForAll_PR_Opp]
AS

BEGIN -- Procedure stRPCreatePlanForAll_PR_Opp

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to convert Plans from Vision format to 1.0 format. 
-- The Vision DB must have 1.0 Schema applied already.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @strPlanID varchar(32)
  DECLARE @strOpportunityID varchar(32)
  DECLARE @strWBS1 nvarchar(30)
  DECLARE @strRowID nvarchar(255)

  DECLARE @strUserName nvarchar(32)
  DECLARE @strCreateUser nvarchar(32)

  DECLARE @tabPR TABLE (
    WBS1 nvarchar(30) COLLATE database_default
    UNIQUE(WBS1)
  )

  DECLARE @tabOpp TABLE (
    OpportunityID varchar(32) COLLATE database_default
    UNIQUE(OpportunityID)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))
  SELECT @strCreateUser = CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'CreatePlanForAll_PR_Opp' ELSE 'CPFP_' + @strUserName END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find Projects that do not have any Plan.
  -- Must use a temp table here otherwise the cursor will get confused when Plans are being changed inside a loop.
  -- Select only Projects with "Active" Status.

  INSERT @tabPR(
    WBS1
  )
    SELECT DISTINCT
      PR.WBS1
      FROM PR
      LEFT JOIN (
        SELECT P.WBS1 FROM PNPlan AS P 
        UNION
        SELECT P.WBS1 FROM RPPlan AS P
      ) AS XP
        ON PR.WBS1 = XP.WBS1
      WHERE PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND Status = 'A'
        AND XP.WBS1 IS NULL

  DECLARE csr20170202_PR CURSOR LOCAL FAST_FORWARD FOR
    SELECT WBS1
      FROM @tabPR AS RP

  OPEN csr20170202_PR
  FETCH NEXT FROM csr20170202_PR INTO @strWBS1

  WHILE (@@FETCH_STATUS = 0)
    BEGIN

      PRINT '>>> Start Processing WBS1 = "' + @strWBS1 + '"'

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      EXECUTE dbo.stRPCreatePlanFromProj 
        @strWBS1,
        @strCreateUser,
        @strPlanID OUTPUT

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      SELECT 
        @strRowID = '|' + T.TaskID
        FROM PNTask AS T
        WHERE T.PlanID = @strPlanID AND T.OutlineLevel = 0

      EXECUTE dbo.stRPAddJTDAssignment @strRowID, 1
      EXECUTE dbo.stRPAddJTDExpense @strRowID, 1
      EXECUTE dbo.stRPAddJTDConsultant @strRowID, 1

	  --Since the plan is created during upgrading, we need to change ModUser,
	  -- or it will be "UNKNOWN"
	  --No need to change CreateUser which is "CreatePlanForAll_PR_Opp" 
	  UPDATE PNPlan SET  ModUser = 'VisionUpgrade'
	  WHERE PlanID = @strPlanID

	  UPDATE RPPlan SET  ModUser = 'VisionUpgrade'
	  WHERE PlanID = @strPlanID
     --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      PRINT '<<< End Processing WBS1 = "' + @strWBS1 + '"'

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      FETCH NEXT FROM csr20170202_PR INTO @strWBS1

    END -- While

  CLOSE csr20170202_PR
  DEALLOCATE csr20170202_PR

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find Opportunities that do not have any Plan.
  -- Must use a temp table here otherwise the cursor will get confused when Plans are being changed inside a loop. 
  -- Select only Opportunities with "Active" Status.

  INSERT @tabOpp(
    OpportunityID
  )
    SELECT DISTINCT
      O.OpportunityID AS OpportunityID
      FROM Opportunity AS O
      LEFT JOIN (
        SELECT P.OpportunityID FROM PNPlan AS P 
          WHERE P.OpportunityID IS NOT NULL AND P.WBS1 IS NULL
        UNION
        SELECT P.OpportunityID FROM RPPlan AS P 
          WHERE P.OpportunityID IS NOT NULL AND P.WBS1 IS NULL
      ) AS XP
        ON O.OpportunityID = XP.OpportunityID
      WHERE O.Status = 'A' AND XP.OpportunityID IS NULL

  DECLARE csr20170202_Opp CURSOR LOCAL FAST_FORWARD FOR
    SELECT OpportunityID
      FROM @tabOpp AS RP

  OPEN csr20170202_Opp
  FETCH NEXT FROM csr20170202_Opp INTO @strOpportunityID

  WHILE (@@FETCH_STATUS = 0)
    BEGIN

      PRINT '>>> Start Processing OpportunityID = "' + @strOpportunityID + '"'

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      EXECUTE dbo.stRPCreatePlanFromOpportunity 
        @strOpportunityID,
        @strCreateUser,
        @strPlanID OUTPUT

      -- Since Opportunity Plan does not have any JTD, there is no need to create JTD Assignments/Expenses/Consultants.

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      PRINT '<<< End Processing OpportunityID = "' + @strOpportunityID + '"'

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      FETCH NEXT FROM csr20170202_Opp INTO @strOpportunityID

    END -- While

  CLOSE csr20170202_Opp
  DEALLOCATE csr20170202_Opp

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

END -- stRPCreatePlanForAll_PR_Opp
GO
