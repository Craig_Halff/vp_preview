SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_Config_VendorGet] (
	@vendorId					Nvarchar(20),
	@TaxAuditingEnabled			bit
) AS
BEGIN
	-- EXEC [spCCG_PAT_Config_VendorGet] '0000000001', 0, 0, 'Y'
	DECLARE @TaxAuditingPost72 bit =1,
		@SecondaryTaxEnabledValue char(1)='Y'

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max) = N'

		SELECT ve.Vendor, Name, STATUS, vea.Company, RegAccount,
				ReimbAccount, OHAccount, PayTerms, DefaultExpenseCode as ExpenseCode, TaxAuditingEnabled, ReadyForProcessing ' +
			(CASE
				WHEN @TaxAuditingEnabled = 1 AND @TaxAuditingPost72 = 0
					THEN ', SecondaryTaxEnabled, ISNULL(vea.DefaultTaxCode, m.DefaultTaxCode) as DefaultTaxCode,
						DefaultTax2Code'
				WHEN @TaxAuditingEnabled = 1 AND @TaxAuditingPost72 = 1
					THEN ', '''+@SecondaryTaxEnabledValue+''' as SecondaryTaxEnabled,
						ISNULL(vea.DefaultTaxCode, t1.TaxCode) as DefaultTaxCode, t2.TaxCode as DefaultTax2Code'
				ELSE ''
			END) + '
			FROM VE
				LEFT JOIN VEAccounting vea on ve.vendor = vea.vendor
				LEFT JOIN CFGMainData m on vea.Company = m.Company '
			+(CASE WHEN @TaxAuditingEnabled = 1 AND @TaxAuditingPost72 = 1 THEN '
                LEFT JOIN CFGMainDataDefaultTaxCodes t1 on m.Company = t1.Company and t1.Seq = 1
                LEFT JOIN CFGMainDataDefaultTaxCodes t2 on m.Company = t2.Company and t2.Seq = 2'
				ELSE ''
			END) + '
			WHERE ve.vendor = N''' + @vendorId + '''';

	EXEC (@sql);
END;
GO
