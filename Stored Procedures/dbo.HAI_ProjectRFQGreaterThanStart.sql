SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[HAI_ProjectRFQGreaterThanStart] @WBS1 VARCHAR (30), @RFQDate DATE, @EstStartDate DATE
AS
/* =======================================================================================
Copyright (c) 2020  Halff Associates, Inc.  All rights reserved.
	
20200704	Craig H. Anderson
			RFQ Deadline must be less than Est. Start Date.
20210714	David Springer
			Revised for Vantagepoint
20210804	Jeremy Baummer
			Changed Error Message to point user to Milestone Grid
   ======================================================================================= */
SET NOCOUNT ON;
    BEGIN
        IF @RFQDate >= @EstStartDate
            RAISERROR('RFQ Date must be less than Est. Start Date in the Milestone Grid.                        ', 16, 1);
    END;
GO
