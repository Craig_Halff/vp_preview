SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ngRPShiftDates]
  @strRowID Nvarchar(255),
  @strFromStartDate varchar(8),
  @strFromEndDate varchar(8),
  @strToStartDate varchar(8),
  @strToEndDate varchar(8),
  @intShiftMode int, /* 1 = Maintain Pattern, 2 = Distribute Evenly, 3 = No Redistribute */
  @bitShiftAll bit, /* This flag is applicable only for ShiftMode = {1, 3}. 0 = Shift using the "From" range, 1 = Shift the entire Assignment Row */
  @bitFixStartDate bit /* This flag is applicable only for ShiftMode = {1, 3}. 1 = Hold "To" StartDate fixed, 0 = Hold "To" EndDate fixed */
AS

BEGIN -- Procedure ngRPShiftDates

  SET NOCOUNT ON
  
  DECLARE @dtFromStartDate datetime
  DECLARE @dtFromEndDate datetime

  DECLARE @dtToStartDate datetime
  DECLARE @dtToEndDate datetime

  DECLARE @dtMinTPDDate datetime
  DECLARE @dtMaxTPDDate datetime
  DECLARE @dtASGStartDate datetime
  DECLARE @dtASGEndDate datetime
  DECLARE @dtTSKStartDate datetime
  DECLARE @dtTSKEndDate datetime
   
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strAT_TaskID varchar(32)
  DECLARE @strAssignmentID varchar(32)
  DECLARE @strResourceID Nvarchar(20)
  DECLARE @strGenericResourceID Nvarchar(20)
  DECLARE @strResourceType varchar(1)
  DECLARE @strIDPrefix Nvarchar(255)
  DECLARE @strUserName Nvarchar(32)
  DECLARE @strCalcID varchar(34)

  DECLARE @decSumHrs decimal(19,4) = 0

  DECLARE @siGRMethod smallint
  DECLARE @siCostRtMethod smallint
  DECLARE @siBillingRtMethod smallint
  
  DECLARE @intCostRtTableNo int
  DECLARE @intBillingRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int

  DECLARE @siLabRateDecimals smallint = 4
 
  -- Declare Temp tables.
  
  DECLARE @tabAssignment TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ResourceID Nvarchar(20) COLLATE database_default,
    Category smallint,
    GRLBCD Nvarchar(14) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    CostRate decimal(19,4),
    BillingRate decimal(19,4),
    AT_OutlineNumber varchar(255) COLLATE database_default
    UNIQUE(PlanID, TaskID, AssignmentID)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

  -- Set Dates

  SET @dtFromStartDate = 
    CASE 
      WHEN DATALENGTH(@strFromStartDate) = 0 
      THEN NULL
      ELSE CONVERT(datetime, @strFromStartDate)
    END

  SET @dtFromEndDate = 
    CASE
      WHEN DATALENGTH(@strFromEndDate) = 0
      THEN NULL
      ELSE CONVERT(datetime, @strFromEndDate)
    END

  SET @dtToStartDate = 
    CASE
      WHEN DATALENGTH(@strToStartDate) = 0
      THEN NULL
      ELSE CONVERT(datetime, @strToStartDate)
    END

  SET @dtToEndDate = 
    CASE
    WHEN DATALENGTH(@strToEndDate) = 0
    THEN NULL
    ELSE CONVERT(datetime, @strToEndDate)
  END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. E~<RPAssignment.ResourceID>|<RPTask.TaskID>
  --   2. G~<RPAssignment.GenericResourceID>|<RPTask.TaskID>

  SET @strIDPrefix = LEFT(@strRowID, CHARINDEX('|', @strRowID) - 1)
  SET @strResourceType = SUBSTRING(@strIDPrefix, 1, 1)

  IF (@strResourceType = 'E')
    BEGIN
      SET @strResourceID = REPLACE(@strIDPrefix, 'E~', '')
      SET @strGenericResourceID = NULL
    END
  ELSE IF (@strResourceType = 'G')
    BEGIN
      SET @strGenericResourceID = REPLACE(@strIDPrefix, 'G~', '')
      SET @strResourceID = NULL
    END
  ELSE
    BEGIN
      SET @strResourceID = NULL
      SET @strGenericResourceID = NULL
    END

  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

  -- Get various parameters.

  SELECT 
    @strCompany = P.Company,
    @strPlanID = T.PlanID,
    @dtTSKStartDate = T.StartDate,
    @dtTSKEndDate = T.EndDate,
    @siGRMethod = P.GRMethod,
    @intGenResTableNo = P.GenResTableNo,
    @intGRBillTableNo = P.GRBillTableNo,
    @siCostRtMethod = P.CostRtMethod,
    @siBillingRtMethod = P.BillingRtMethod,
    @intCostRtTableNo = P.CostRtTableNo,
    @intBillingRtTableNo = P.BillingRtTableNo
    FROM RPTask AS T
      INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID
    WHERE T.TaskID = @strTaskID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@intShiftMode = 1) /* 1 = Maintain Pattern */
    BEGIN

      EXEC dbo.ngRPShiftTasks
        @strRowID,
        @strFromStartDate,
        @strFromEndDate,
        @strToStartDate,
        @strToEndDate,
        @bitShiftAll, 
        @bitFixStartDate 

    END /* END IF (@intShiftMode = 1) */
    
  ELSE IF (@intShiftMode IN (2, 3)) /* --> (@intShiftMode = 2 or 3) */
    BEGIN

      EXEC dbo.ngRPChangeDuration
        @strRowID,
        @strToStartDate,
        @strToEndDate,
        @intShiftMode

    END /* END ELSE IF (@intShiftMode = 3) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET @strCalcID = 'P~' + @strPlanID
  EXECUTE dbo.ngRPReCalcPlan @strCalcID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     
  SET NOCOUNT OFF

END -- ngRPShiftDates
GO
