SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_DelegationGetClients]
AS
BEGIN
	/* Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved. */
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

	select distinct BillingClientID as ClientId, CL.Name from PR inner join CL on CL.ClientID = PR.BillingClientID
		where PR.Status <> 'D'
		order by CL.Name ASC
END
GO
