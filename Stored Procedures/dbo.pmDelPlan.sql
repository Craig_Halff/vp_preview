SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[pmDelPlan]
  @strPlanID varchar(32)
AS

BEGIN -- Procedure pmDelPlan

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to delete a Plan.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  
  UPDATE PR SET PR.PlanID = NULL
    FROM PR INNER JOIN RPPlan AS P ON PR.WBS1 = ISNULL(P.WBS1,N' ')
    WHERE P.PlanID = @strPlanID
  
  UPDATE PRDefaults SET PRDefaults.PlanID = NULL
    FROM PRDefaults AS PR INNER JOIN RPPlan AS P ON PR.PlanID = P.PlanID
    WHERE P.PlanID = @strPlanID
  
  UPDATE PRTemplate SET PRTemplate.PlanID = NULL
    FROM PRTemplate AS PR INNER JOIN RPPlan AS P ON PR.WBS1 = ISNULL(P.WBS1,N' ')
    WHERE P.PlanID = @strPlanID
  
  DELETE PlanningCustomTabFields WHERE PlanID = @strPlanID
  
  DELETE EMActivity
    FROM EMActivity
    INNER JOIN Activity ON EMActivity.ActivityID = Activity.ActivityID
    INNER JOIN RPAssignment ON Activity.AssignmentID = RPAssignment.AssignmentID
    WHERE RPAssignment.PlanID = @strPlanID
  
  DELETE ActivityCustomTabFields 
    FROM ActivityCustomTabFields
    INNER JOIN Activity ON ActivityCustomTabFields.ActivityID = Activity.ActivityID
    INNER JOIN RPAssignment ON Activity.AssignmentID = RPAssignment.AssignmentID
    WHERE RPAssignment.PlanID = @strPlanID

  DELETE Activity 
    FROM Activity
    INNER JOIN RPAssignment ON Activity.AssignmentID = RPAssignment.AssignmentID
    WHERE RPAssignment.PlanID = @strPlanID
  
  DELETE RPCalendarInterval WHERE PlanID = @strPlanID
  DELETE RPAccordionFormat WHERE PlanID = @strPlanID
  DELETE RPWBSLevelFormat WHERE PlanID = @strPlanID
  DELETE RPPlannedLabor WHERE PlanID = @strPlanID
  DELETE RPPlannedExpenses WHERE PlanID = @strPlanID
  DELETE RPPlannedConsultant WHERE PlanID = @strPlanID
  DELETE RPPlannedUnit WHERE PlanID = @strPlanID
  DELETE RPBaselineLabor WHERE PlanID = @strPlanID
  DELETE RPBaselineExpenses WHERE PlanID = @strPlanID
  DELETE RPBaselineConsultant WHERE PlanID = @strPlanID
  DELETE RPBaselineUnit WHERE PlanID = @strPlanID
  DELETE RPAssignment WHERE PlanID = @strPlanID
  DELETE RPExpense WHERE PlanID = @strPlanID
  DELETE RPConsultant WHERE PlanID = @strPlanID
  DELETE RPUnit WHERE PlanID = @strPlanID
  DELETE RPCompensationFee WHERE PlanID = @strPlanID
  DELETE RPConsultantFee WHERE PlanID = @strPlanID
  DELETE RPReimbAllowance WHERE PlanID = @strPlanID
  DELETE RPEVT WHERE PlanID = @strPlanID
  DELETE RPDependency WHERE PlanID = @strPlanID
  DELETE RPTopDown WHERE PlanID = @strPlanID
  DELETE RPTopDownGR WHERE PlanID = @strPlanID
  DELETE RPPlanSubscr WHERE PlanID = @strPlanID
  DELETE RPTask WHERE PlanID = @strPlanID
  DELETE RPPlan WHERE PlanID = @strPlanID

  SET NOCOUNT OFF

END -- pmDelPlan
GO
