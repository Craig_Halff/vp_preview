SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_DelegationGet] ( @Username nvarchar(32), @BeginDate varchar(20), @EndDate varchar(20), @CurrentEmployeeId nvarchar(20), @IsSupervisorApprovalOnly int= 0, @ShowAllDelegations int= 1, @CanSelfDelegate int= 1)
             AS EXEC spCCG_EI_DelegationGet @Username,@BeginDate,@EndDate,@CurrentEmployeeId,@IsSupervisorApprovalOnly,@ShowAllDelegations,@CanSelfDelegate
GO
