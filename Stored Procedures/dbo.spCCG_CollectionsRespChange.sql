SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_CollectionsRespChange] @wbs VARCHAR(11)
AS
    /* 
Copyright (c) 2020 Halff Associates, Inc.  All rights reserved.
Update the CustRequestedActionOwner for Incomplete actions when Rssponsibility shifts

09/30/2020  Craig H. Anderson
			(left the name with the CCG prefix just to keep the stored procedures together alphabetically)
10/18/2020	Craig H. Anderson
			Modified to use the new CustCollectionsResponsibility field.
*/
    BEGIN
        SET NOCOUNT ON;

        BEGIN TRANSACTION;

       /* --CHA for later--
		DECLARE @resp VARCHAR(3) =              (
                        SELECT CustCollectionsResponsibility
                        FROM dbo.ProjectCustomTabFields
                        WHERE
                            WBS1 = @wbs
                            AND WBS2 = ' '
                    );
		*/
        UPDATE c
        SET c.CustRequestedActionOwner = IIF(p.CustCollectionsResponsibility = 'PA'
                                             , c.CustBiller
                                             , c.CustProjMgr)
        FROM dbo.UDIC_Collection              AS c
        INNER JOIN dbo.ProjectCustomTabFields AS p WITH (NOLOCK)
            ON c.CustProject = p.WBS1
               AND p.WBS2 = ' '
        WHERE
            c.CustProject = @wbs
            AND c.CustCollectionStatus = 'Open'
            AND c.CustRequestedActionStatus = 'Incomplete';
/*
        INSERT INTO UDIC_Collection_History
        (
            UDIC_UID
          , Seq
          , CustHistoryActionStatus
          , CustHistoryAction
          , CustHistoryActionStart
        )
*/

        COMMIT TRANSACTION;
    END;
GO
