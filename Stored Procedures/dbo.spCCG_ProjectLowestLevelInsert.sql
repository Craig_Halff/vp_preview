SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_ProjectLowestLevelInsert]
    @WBS1 varchar(32), @WBS2 varchar(7), @WBS3 varchar(7)
AS
/*
Copyright 2019 (c) Central Consulting Group.   All rights reserved.
07/29/2019	David Springer
			Write project levels to UDIC Project Lowest Levels
			Call from project CHANGE workflows when Approved for Use in Processing = checked, or Status has changed
12/18/2020	Craig H. Anderson
			Stop deleting records when they are no longer the lowest level (p.Sublevel = 'Y')
			and instead mark their status as closed CustStatus = 'D'
1/4/2021	Craig H. Anderson
			when a WBS level's status is marked as 'D', we not change the status of the PLL entry as well.  Deletion only occurs 
			if there are no mileage entries referencing the PLL WBS level.
*/
DECLARE @Status varchar(1);
BEGIN
    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    SELECT  @Status = Status
    FROM    dbo.PR
    WHERE   WBS1 = @WBS1
            AND WBS2 = @WBS2
            AND WBS3 = @WBS3;

    IF @WBS2 = ' ' -- project
    BEGIN
        IF @Status = 'D'
        BEGIN
            DELETE  l
            FROM    dbo.UDIC_ProjectLowestLevel l
            WHERE   l.CustWBS1 = @WBS1
                    AND NOT EXISTS (
                SELECT  *
                FROM    dbo.UDIC_FleetManagement_MileageLog m
                WHERE   m.CustProject = l.UDIC_UID
            );
            UPDATE  l
            SET l.CustStatus = p.Status, l.CustLastModified = GETDATE()
            FROM    dbo.UDIC_ProjectLowestLevel l, dbo.PR p
            WHERE   l.CustWBS1 = @WBS1
                    AND l.CustWBS1 = p.WBS1
                    AND l.CustWBS2 = p.WBS2
                    AND l.CustWBS3 = p.WBS3;
        END;
        ELSE
            UPDATE  l
            SET l.CustStatus = p.Status, l.CustLastModified = GETDATE()
            FROM    dbo.UDIC_ProjectLowestLevel l, dbo.PR p
            WHERE   l.CustWBS1 = @WBS1
                    AND l.CustWBS1 = p.WBS1
                    AND l.CustWBS2 = p.WBS2
                    AND l.CustWBS3 = p.WBS3;

    END;

    IF @WBS2 <> ' '
       AND  @WBS3 = ' ' -- phase
    BEGIN
        IF @Status = 'D'
        BEGIN
            DELETE  l
            FROM    dbo.UDIC_ProjectLowestLevel l
            WHERE   l.CustWBS1 = @WBS1
                    AND l.CustWBS2 = @WBS2
                    AND NOT EXISTS (
                SELECT  *
                FROM    dbo.UDIC_FleetManagement_MileageLog m
                WHERE   m.CustProject = l.UDIC_UID
            );
            UPDATE  dbo.UDIC_ProjectLowestLevel
            SET CustStatus = @Status, CustLastModified = GETDATE()
            WHERE   CustWBS1 = @WBS1
                    AND CustWBS2 = @WBS2
                    AND CustWBS3 = ' ';
        END;
        ELSE
            UPDATE  dbo.UDIC_ProjectLowestLevel
            SET CustStatus = @Status, CustLastModified = GETDATE()
            WHERE   CustWBS1 = @WBS1
                    AND CustWBS2 = @WBS2
                    AND CustWBS3 = ' ';
    END;

    IF @WBS3 <> ' ' -- task
    BEGIN
        IF @Status = 'D'
        BEGIN
            DELETE  l
            FROM    dbo.UDIC_ProjectLowestLevel l
            WHERE   l.CustWBS1 = @WBS1
                    AND l.CustWBS2 = @WBS2
                    AND l.CustWBS3 = @WBS3
                    AND NOT EXISTS (
                SELECT  *
                FROM    dbo.UDIC_FleetManagement_MileageLog m
                WHERE   m.CustProject = l.UDIC_UID
            );
            UPDATE  dbo.UDIC_ProjectLowestLevel
            SET CustStatus = @Status, CustLastModified = GETDATE()
            WHERE   CustWBS1 = @WBS1
                    AND CustWBS2 = @WBS2
                    AND CustWBS3 = @WBS3;
        END;
        ELSE
            UPDATE  dbo.UDIC_ProjectLowestLevel
            SET CustStatus = @Status, CustLastModified = GETDATE()
            WHERE   CustWBS1 = @WBS1
                    AND CustWBS2 = @WBS2
                    AND CustWBS3 = @WBS3;
    END;

    INSERT INTO dbo.UDIC_ProjectLowestLevel
    (
        UDIC_UID
      , CreateDate
      , CreateUser
      , CustName
      , CustWBS1
      , CustWBS1Name
      , CustWBS2
      , CustWBS2Name
      , CustWBS3
      , CustWBS3Name
      , CustChargeType
      , CustStatus
    )
    -- Tasks
    SELECT  REPLACE(NEWID(), '-', '') AS ID
          , GETDATE()
          , 'CCG Procedure'
          , p2.WBS1 + ' | ' + p2.WBS2 + ' | ' + p3.WBS3 + ' ' + p1.Name + ' | ' + p2.Name + ' | '
            + p3.Name                 AS Name
          , p1.WBS1
          , p1.Name
          , p2.WBS2
          , p2.Name
          , p3.WBS3
          , p3.Name
          , p3.ChargeType
          , p3.Status
    FROM    dbo.PR p3, dbo.PR p2, dbo.PR p1
    WHERE   p3.WBS1 = @WBS1
            -- and p3.WBS2 = @WBS2
            --  and p3.WBS3 = @WBS3
            AND p3.WBS3 <> ' ' -- Task
            AND p3.SubLevel = 'N'
            AND p3.ReadyForProcessing = 'Y'
            AND p3.Status <> 'D'
            AND p3.WBS1 = p2.WBS1
            AND p3.WBS2 = p2.WBS2
            AND p2.WBS3 = ' ' -- Phase
            AND p2.ReadyForProcessing = 'Y'
            AND p2.Status <> 'D'
            AND p2.WBS1 = p1.WBS1
            AND p1.WBS2 = ' ' -- Project
            AND p1.ReadyForProcessing = 'Y'
            AND p1.Status <> 'D'
            AND NOT EXISTS (
        SELECT  'x'
        FROM    dbo.UDIC_ProjectLowestLevel
        WHERE   CustWBS1 = p3.WBS1
                AND CustWBS2 = ISNULL(p3.WBS2, ' ')
                AND CustWBS3 = ISNULL(p3.WBS3, ' ')
    )
    UNION ALL
    -- Phases
    SELECT  REPLACE(NEWID(), '-', '')                                   AS ID
          , GETDATE()
          , 'CCG Procedure'
          , p2.WBS1 + ' | ' + p2.WBS2 + ' ' + p1.Name + ' | ' + p2.Name AS Name
          , p1.WBS1
          , p1.Name
          , p2.WBS2
          , p2.Name
          , ' '                                                         AS WBS3
          , NULL
          , p2.ChargeType
          , p2.Status
    FROM    dbo.PR p2, dbo.PR p1
    WHERE   p2.WBS1 = @WBS1
            --and p2.WBS2 = @WBS2
            AND p2.WBS2 <> ' '
            AND p2.WBS3 = ' ' -- Phase
            AND p2.SubLevel = 'N'
            AND p2.ReadyForProcessing = 'Y'
            AND p2.Status <> 'D'
            AND p2.WBS1 = p1.WBS1
            AND p1.WBS2 = ' ' -- Project
            AND p1.ReadyForProcessing = 'Y'
            AND p1.Status <> 'D'
            AND NOT EXISTS (
        SELECT  'x'
        FROM    dbo.UDIC_ProjectLowestLevel
        WHERE   CustWBS1 = p2.WBS1
                AND CustWBS2 = ISNULL(p2.WBS2, ' ')
    )
    UNION ALL
    -- Project
    SELECT  REPLACE(NEWID(), '-', '') AS ID
          , GETDATE()
          , 'CCG Procedure'
          , p1.WBS1 + ' ' + p1.Name   AS Name
          , p1.WBS1
          , p1.Name
          , ' '                       AS WBS2
          , NULL
          , ' '                       AS WBS3
          , NULL
          , p1.ChargeType
          , p1.Status
    FROM    dbo.PR p1
    WHERE   p1.WBS1 = @WBS1
            AND p1.WBS2 = ' ' -- project level 
            AND p1.SubLevel = 'N'
            AND p1.ReadyForProcessing = 'Y'
            AND p1.Status <> 'D'
            AND NOT EXISTS (
        SELECT  'x' FROM    dbo.UDIC_ProjectLowestLevel WHERE  CustWBS1 = p1.WBS1
    )
    ORDER BY p1.WBS1, p2.WBS2, p3.WBS3;

    /*
Delete From l
From UDIC_ProjectLowestLevel l, PR p
Where l.CustWBS1 = @WBS1
  and l.CustWBS1 = p.WBS1
  and l.CustWBS2 = p.WBS2
  and l.CustWBS3 = p.WBS3
  and p.Sublevel = 'Y' -- there are children
  */

    UPDATE  l
    SET l.CustStatus = 'D'
    FROM    dbo.UDIC_ProjectLowestLevel AS l, dbo.PR p
    WHERE   l.CustWBS1 = @WBS1
            AND l.CustWBS1 = p.WBS1
            AND l.CustWBS2 = p.WBS2
            AND l.CustWBS3 = p.WBS3
            AND p.SubLevel = 'Y';
END;
GO
