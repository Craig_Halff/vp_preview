SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_EmployeeBioHistory]
	@Employee		varchar(32),
	@Category		varchar (255), 
	@BioOld	varchar (8000),
	@BioNew	varchar (8000), 
	@ModUser		varchar (225)
AS 
/* 
Copyright (c) 2018 Central Consulting Group.  All rights reserved.
11/19/2019	David Springer
			Write Bio to Employee Bio History grid
			Call this on Employee Bios grid INSERT or CHANGE workflow.
*/
BEGIN
SET NOCOUNT ON

	Insert Into Employees_BioHistory
	(Employee, Seq, CustBioHistoryDate, CustBioHistoryCategory, CustBioHistoryOld, CustBioHistoryNew, CustBioHistoryModUser)
	Select @Employee, Replace (NewID(), '-', ''), getDate(), c.Description, @BioOld, @BioNew, dbo.fnCCG_UserEmployeeNo (@ModUser)
	From CFGResumeCategory c
	Where Code = @Category
END
GO
