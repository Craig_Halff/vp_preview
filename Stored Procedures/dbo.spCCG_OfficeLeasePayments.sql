SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_OfficeLeasePayments] @OfficeNumber varchar (4)
AS 
/* 
Copyright (c) 2019 Central Consulting Group.  All rights reserved.
12/06/2019	David Springer
			Populate UDIC_Office Lease Payments grid
			Call this from an Office scheduled workflow
*/
BEGIN
SET NOCOUNT ON

	Delete s From UDIC_Office o, UDIC_Office_LeasePayments s Where o.UDIC_UID = s.UDIC_UID and o.CustOfficeNumber = @OfficeNumber

	Insert Into UDIC_Office_LeasePayments
	(UDIC_UID, Seq, CustLeasePaymentDate, CustLeasePaymentAccount, CustLeaseHolder, CustLeasePaymentAmount, CustLeasePaymentComments)
	Select o.UDIC_UID, Replace (NewID(), '-', ''), l.TransDate, l.Account, l.Vendor, l.Amount, l.Desc2
	From LedgerAP l, UDIC_Office o
	Where l.TransType = 'AP'
	  and l.Account = '736.01'
	  and substring (l.Org, 4, 2) = o.CustOfficeNumber
	  and o.CustOfficeNumber = @OfficeNumber

END
GO
