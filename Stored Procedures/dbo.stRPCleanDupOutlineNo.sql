SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPCleanDupOutlineNo]  
AS


BEGIN -- Procedure rpReCalc

  SET NOCOUNT ON

  DECLARE @strPlanID VARCHAR(32)

  DECLARE csrRPPlan CURSOR FOR
    select distinct t1.planid from pntask t1 , pntask t2 where t1.planid = t2.planid and t1.OutlineNumber 
=    t2.outlinenumber and t1.taskid <> t2.taskid

  OPEN csrRPPlan

  FETCH NEXT FROM csrRPPlan INTO @strPlanID

  WHILE (@@FETCH_STATUS = 0)
    BEGIN
    
         EXECUTE dbo.stRPReorderOutlineNumber @strPlanID,1
            
      FETCH NEXT FROM csrRPPlan INTO @strPlanID

    END -- While


  CLOSE csrRPPlan
  DEALLOCATE csrRPPlan
 
END -- stRPCleanDupOutlineNo
GO
