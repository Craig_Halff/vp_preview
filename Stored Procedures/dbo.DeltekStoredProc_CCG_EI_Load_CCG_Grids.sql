SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Load_CCG_Grids]
	@VISION_LANGUAGE	varchar(10)
AS
BEGIN
	-- DeltekStoredProc_CCG_EI_Load_CCG_Grids @VISION_LANGUAGE = 'en-US'
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT ci.GridInstanceName, /*ci.GridWBSLevel,*/ ci.GridOrder, cid.GridInstanceLabel,
		IsNull(cg.GridVisibleInEI, 'N') as GridVisibleInEI, IsNull(cg.GridProjectsVisibleInEI, 'N') as GridProjectsVisibleInEI
	FROM CCG_GRID_ConfigInstance ci
		INNER JOIN CCG_GRID_ConfigInstanceDescriptions cid
			ON cid.GridInstanceName = ci.GridInstanceName and cid.UICultureName = @VISION_LANGUAGE
		LEFT JOIN CCG_EI_ConfigGrid cg
			on cg.GridInstanceName = ci.GridInstanceName
			where ci.GridInstanceType='Project'
        ORDER BY ci.GridInstanceName;

END;
GO
