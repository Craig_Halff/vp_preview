SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_Get_Labels]
	@visionMajorVersion	int,
	@VISION_LANGUAGE	Nvarchar(10)
AS
BEGIN
	-- [DeltekStoredProc_CCG_PAT_Config_Get_Labels] @visionMajorVersion = '7', @VISION_LANGUAGE = 'en-US'
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	bit;

	-- Ensure that our inputs are safe (prevent SQL injection)
	SET @safeSql = dbo.fnCCG_PAT_RegexMatch('<STRING VALUE>', @VISION_LANGUAGE);
	IF @safeSql = 0 BEGIN
		SELECT 'ERROR: Unsafe parameters used! Execution cancelled.';
		RETURN;
	END;

	IF @visionMajorVersion >= 7
		SET @sSQL = N'
		SELECT
			MAX(Case When UICultureName = ''' + @VISION_LANGUAGE + ''' AND LabelName = ''wbs1Label''         Then LabelValue Else '''' End) as wbs1Label,
			MAX(Case When UICultureName = ''' + @VISION_LANGUAGE + ''' AND LabelName = ''wbs2Label''         Then LabelValue Else '''' End) as wbs2Label,
			MAX(Case When UICultureName = ''' + @VISION_LANGUAGE + ''' AND LabelName = ''wbs3Label''         Then LabelValue Else '''' End) as wbs3Label,
			MAX(Case When UICultureName = ''' + @VISION_LANGUAGE + ''' AND LabelName = ''wbs1LabelPlural''   Then LabelValue Else '''' End) as wbs1LabelPlural,
			MAX(Case When UICultureName = ''' + @VISION_LANGUAGE + ''' AND LabelName = ''clientLabel''       Then LabelValue Else '''' End) as clientLabel,
			MAX(Case When UICultureName = ''' + @VISION_LANGUAGE + ''' AND LabelName = ''clientLabelPlural'' Then LabelValue Else '''' End) as clientLabelPlural,
			MAX(Case When UICultureName = ''' + @VISION_LANGUAGE + ''' AND LabelName = ''vendorLabel''       Then LabelValue Else '''' End) as vendorLabel,
			MAX(Case When UICultureName = ''' + @VISION_LANGUAGE + ''' AND LabelName = ''vendorLabelPlural'' Then LabelValue Else '''' End) as vendorLabelPlural
		FROM FW_CFGLabelData';
	ELSE IF @visionMajorVersion = 6 OR @visionMajorVersion = 0		-- Default
		SET @sSQL = N'
		SELECT
			MAX(Case When UICultureName = ''' + @VISION_LANGUAGE + ''' AND LabelName = ''wbs1Label''         Then LabelValue Else '''' End) as wbs1Label,
			MAX(Case When UICultureName = ''' + @VISION_LANGUAGE + ''' AND LabelName = ''wbs2Label''         Then LabelValue Else '''' End) as wbs2Label,
			MAX(Case When UICultureName = ''' + @VISION_LANGUAGE + ''' AND LabelName = ''wbs3Label''         Then LabelValue Else '''' End) as wbs3Label,
            MAX(Case When UICultureName = ''' + @VISION_LANGUAGE + ''' AND LabelName = ''wbs1LabelPlural''   Then LabelValue Else '''' End) as wbs1LabelPlural,
			MAX(Case When UICultureName = ''' + @VISION_LANGUAGE + ''' AND LabelName = ''clientLabel''       Then LabelValue Else '''' End) as clientLabel,
            MAX(Case When UICultureName = ''' + @VISION_LANGUAGE + ''' AND LabelName = ''clientLabelPlural'' Then LabelValue Else '''' End) as clientLabelPlural,
			MAX(Case When UICultureName = ''' + @VISION_LANGUAGE + ''' AND LabelName = ''vendorLabel''       Then LabelValue Else '''' End) as vendorLabel,
			MAX(Case When UICultureName = ''' + @VISION_LANGUAGE + ''' AND LabelName = ''vendorLabelPlural'' Then LabelValue Else '''' End) as vendorLabelPlural
		FROM CFGLabelData';
	ELSE															-- 5.1
		SET @sSQL = N'
		SELECT
			MIN(wbs1Label) as wbs1Label, MIN(wbs1LabelPlural) as wbs1LabelPlural,
            MIN(wbs2Label) as wbs2Label, MIN(wbs3Label) as wbs3Label,
            MIN(clientLabel) as clientLabel, MIN(clientLabelPlural) as clientLabelPlural,
			MIN(vendorLabel) as vendorLabel, MIN(vendorLabelPlural) as vendorLabelPlural
        FROM CFGLabels';

	EXEC (@sSQL);
END;

GO
