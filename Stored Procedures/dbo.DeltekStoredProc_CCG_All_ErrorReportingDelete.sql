SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_All_ErrorReportingDelete] ( @product varchar(64)= '')
             AS EXEC spCCG_All_ErrorReportingDelete @product
GO
