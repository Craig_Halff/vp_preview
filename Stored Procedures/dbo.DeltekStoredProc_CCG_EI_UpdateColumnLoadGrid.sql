SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_UpdateColumnLoadGrid] ( @wbs1list nvarchar(max), @initialValue varchar(max), @columnUpdate varchar(500), @databaseField varchar(255), @visionTable varchar(100), @databaseModEmpField varchar(500), @emptySQLValue varchar(100), @valueColumnName varchar(max), @outerApplyStr varchar(max))
             AS EXEC spCCG_EI_UpdateColumnLoadGrid @wbs1list,@initialValue,@columnUpdate,@databaseField,@visionTable,@databaseModEmpField,@emptySQLValue,@valueColumnName,@outerApplyStr
GO
