SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_ImportLines_UpdateKey] (
	@key			varchar(50),
	@type			varchar(50),
	@name			Nvarchar(50),
	@defaultFor		Nvarchar(50),
	@value			Nvarchar(max),
	@employee		Nvarchar(32),
	@makeDefault	bit
) AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @curDefault Nvarchar(50)

    SELECT @curDefault = CONVERT(Nvarchar(50), detail)
        FROM CCG_PAT_ConfigOptions
        WHERE Type = 'SavedTextFileDefault' and [Description] = @defaultFor;

	IF NOT EXISTS(SELECT * FROM CCG_PAT_ConfigOptions WHERE Type = @type and ConfigID = @key)
		-- no existing row; need to insert
		INSERT INTO CCG_PAT_ConfigOptions
			(ConfigID, Type, Description, Detail, ChangedBy, DateChanged) VALUES
			(@key, @type, @name, @value, @employee, getutcdate());
	ELSE BEGIN
		UPDATE CCG_PAT_ConfigOptions
            SET Description = @name,
                Detail = @value,
                ChangedBy = @employee,
                DateChanged = getutcdate()
            WHERE ConfigID = @key and Type = @type
	END;

	IF ISNULL(@curDefault, '') <> '' BEGIN
		IF @curDefault = @key AND @makeDefault = 0 AND ISNULL(@defaultFor, '') <> ''
			-- this item is no longer default for this vendor
            DELETE FROM CCG_PAT_ConfigOptions
                WHERE Type = 'SavedTextFileDefault'
					and Description = @defaultFor;
        ELSE IF @curDefault <> @key AND @makeDefault = 1 AND ISNULL(@defaultFor, '') <> ''
            -- this item is no longer default for this vendor
            UPDATE CCG_PAT_ConfigOptions
                SET Detail = @key
                WHERE Type = 'SavedTextFileDefault' and Description = @defaultFor;
	END
	ELSE IF @makeDefault = 1 BEGIN		-- no existing default; make this the default
        DELETE FROM CCG_PAT_ConfigOptions
            WHERE Type = 'SavedTextFileDefault'
				and Description = @defaultFor;
        INSERT INTO CCG_PAT_ConfigOptions
            (Type, Description, Detail, ChangedBy, DateChanged) VALUES
			('SavedTextFileDefault', @defaultFor, @key, @employee, getutcdate());
	END;

	SELECT @@ROWCOUNT as RowsAffected;
END;

GO
