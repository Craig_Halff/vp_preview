SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectBillingTermsDelete]
   @WBS1        varchar(32), 
   @WBS2        varchar(7), 
   @WBS3        varchar(7),
   @BillingType varchar (255)
AS
/*
Copyright (c) 2017 Central Consulting Group. All rights reserved.
04/03/2017 David Springer
           Deletel billing terms.
           Call from Project CHANGE workflow when Billing Type has changed to Non-Billable or empty.
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
BEGIN

	Update ProjectCustomTabFields
	Set CustFeeType = null
	Where WBS1 = @WBS1
	  and WBS2 = @WBS2
	  and WBS3 = @WBS3
	  and custBillingType is null

 -- Project level
	If @WBS2 = ' '
		Begin
		Delete From BTA Where WBS1 = @WBS1
		Delete From BTF Where WBS1 = @WBS1
		Delete From BTTaxCodes Where WBS1 = @WBS1
		Delete From BT Where WBS1 = @WBS1

      -- Assign the phase & task level Fee Type = project level Fee Type
         Update p2
	     Set p2.CustFeeType = p1.CustFeeType
		 From ProjectCustomTabFields p1, ProjectCustomTabFields p2
		 Where p1.WBS1 = @WBS1
		   and p1.WBS2 = ' ' -- project level
		   and p1.WBS1 = p2.WBS1
		   and p2.WBS2 <> ' ' -- phase & task level

		End

 -- Phase level
	If @WBS2 <> ' ' and @WBS3 = ' '
		Begin
		Delete From BTA Where WBS1 = @WBS1 and WBS2 = @WBS2
		Delete From BTF Where WBS1 = @WBS1 and WBS2 = @WBS2
		Delete From BTTaxCodes Where WBS1 = @WBS1 and WBS2 = @WBS2
		Delete From BT Where WBS1 = @WBS1 and WBS2 = @WBS2

      -- Assign the task level Fee Type = phase level Fee Type
         Update p2
	     Set p2.CustFeeType = p1.CustFeeType
		 From ProjectCustomTabFields p1, ProjectCustomTabFields p2
		 Where p1.WBS1 = @WBS1
		   and p1.WBS2 = @WBS2 -- phase level
		   and p1.WBS3 = ' '
		   and p1.WBS1 = p2.WBS1
		   and p1.WBS2 = p2.WBS2
		   and p2.WBS3 <> ' ' -- task level

		End

 -- Task level
	If @WBS3 <> ' '
		Begin
		Delete From BTA Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
		Delete From BTF Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
		Delete From BTTaxCodes Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
		Delete From BT Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3

      -- Assign the task level Fee Type = phase level Fee Type
         Update p2
	     Set p2.CustFeeType = p1.CustFeeType
		 From ProjectCustomTabFields p1, ProjectCustomTabFields p2
		 Where p1.WBS1 = @WBS1
		   and p1.WBS2 = @WBS2 -- phase level
		   and p1.WBS3 = ' '
		   and p1.WBS1 = p2.WBS1
		   and p1.WBS2 = p2.WBS2
		   and p2.WBS3 <> ' ' -- task level
		End

   If @BillingType = 'Non-Billable'
      Begin
	  Update ProjectCustomTabFields
	  Set CustFeeType = 'Non-Billable'
      Where WBS1 = @WBS1
        and WBS2 = CASE When @WBS2 = ' ' Then WBS2 Else @WBS2 END
        and WBS3 = CASE When @WBS3 = ' ' Then WBS3 Else @WBS3 END
      End
END
GO
