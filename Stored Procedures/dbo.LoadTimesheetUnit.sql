SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE PROCEDURE [dbo].[LoadTimesheetUnit] @activeCompany Nvarchar(14), @Employee Nvarchar(20), @EndDate datetime, @StartDate datetime, @ForReport Nvarchar(1) = 'Y', @PrintByProject Nvarchar(1) = 'N'
AS

SELECT
	Employee = @Employee,
	StartDate = CONVERT(Nvarchar(10),@StartDate,121),
	EndDate = CONVERT(Nvarchar(10),@EndDate,121),
	tkUnitDetail.Seq,
	tkUnitDetail.WBS1, MAX(PR1.Name) As WBS1Name, MAX(CL.Name) As ClientName, MAX(PR1.SubLevel) As WBS2Level,
	MAX(PR1.ChargeType) As ChargeType,
	tkUnitDetail.WBS2, MAX(CASE WHEN tkUnitDetail.WBS2 = N'' THEN N'' ELSE PR2.Name END) As WBS2Name, MAX(PR2.SubLevel) As WBS3Level,
	tkUnitDetail.WBS3, MAX(CASE WHEN tkUnitDetail.WBS3 = N'' THEN N'' ELSE PR3.Name END) As WBS3Name,

	MIN(tkUnitDetail.Unit) AS Unit, MIN(tkUnitDetail.UnitTable) AS UnitTable, 
	MIN(UN.Name) AS UnitDescription, 
	MIN(UN.PluralLabel) AS UOM,

	MIN(CFGMainData.FunctionalCurrencyCode) AS FunctionalCurrencyCode, 
	MIN(PR1.ProjectCurrencyCode) AS CurrencyCode, 
	MIN(PR1.BillingCurrencyCode) AS BillingCurrencyCode, 
	MIN(PR3.Org) AS Org, 
	MIN(PR3.UnitTable) AS DefaultUnitTable, 
	Qty_day1 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN Qty ELSE 0 END)),
	TransComment_day1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN TransComment ELSE NULL END),
	Qty_day2 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN Qty ELSE 0 END)),
	TransComment_day2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN TransComment ELSE NULL END),
	Qty_day3 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN Qty ELSE 0 END)),
	TransComment_day3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN TransComment ELSE NULL END),
	Qty_day4 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN Qty ELSE 0 END)),
	TransComment_day4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN TransComment ELSE NULL END),
	Qty_day5 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN Qty ELSE 0 END)),
	TransComment_day5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN TransComment ELSE NULL END),
	Qty_day6 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN Qty ELSE 0 END)),
	TransComment_day6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN TransComment ELSE NULL END),
	Qty_day7 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN Qty ELSE 0 END)),
	TransComment_day7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN TransComment ELSE NULL END),
	Qty_day8 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN Qty ELSE 0 END)),
	TransComment_day8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN TransComment ELSE NULL END),
	Qty_day9 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN Qty ELSE 0 END)),
	TransComment_day9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN TransComment ELSE NULL END),
	Qty_day10 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN Qty ELSE 0 END)),
	TransComment_day10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN TransComment ELSE NULL END),
	Qty_day11 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN Qty ELSE 0 END)),
	TransComment_day11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN TransComment ELSE NULL END),
	Qty_day12 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN Qty ELSE 0 END)),
	TransComment_day12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN TransComment ELSE NULL END),
	Qty_day13 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN Qty ELSE 0 END)),
	TransComment_day13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN TransComment ELSE NULL END),
	Qty_day14 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN Qty ELSE 0 END)),
	TransComment_day14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN TransComment ELSE NULL END),
	Qty_day15 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN Qty ELSE 0 END)),
	TransComment_day15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN TransComment ELSE NULL END),
	Qty_day16 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN Qty ELSE 0 END)),
	TransComment_day16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN TransComment ELSE NULL END),
	Qty_day17 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN Qty ELSE 0 END)),
	TransComment_day17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN TransComment ELSE NULL END),
	Qty_day18 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN Qty ELSE 0 END)),
	TransComment_day18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN TransComment ELSE NULL END),
	Qty_day19 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN Qty ELSE 0 END)),
	TransComment_day19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN TransComment ELSE NULL END),
	Qty_day20 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN Qty ELSE 0 END)),
	TransComment_day20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN TransComment ELSE NULL END),
	Qty_day21 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN Qty ELSE 0 END)),
	TransComment_day21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN TransComment ELSE NULL END),
	Qty_day22 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN Qty ELSE 0 END)),
	TransComment_day22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN TransComment ELSE NULL END),
	Qty_day23 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN Qty ELSE 0 END)),
	TransComment_day23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN TransComment ELSE NULL END),
	Qty_day24 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN Qty ELSE 0 END)),
	TransComment_day24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN TransComment ELSE NULL END),
	Qty_day25 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN Qty ELSE 0 END)),
	TransComment_day25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN TransComment ELSE NULL END),
	Qty_day26 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN Qty ELSE 0 END)),
	TransComment_day26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN TransComment ELSE NULL END),
	Qty_day27 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN Qty ELSE 0 END)),
	TransComment_day27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN TransComment ELSE NULL END),
	Qty_day28 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN Qty ELSE 0 END)),
	TransComment_day28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN TransComment ELSE NULL END),
	Qty_day29 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN Qty ELSE 0 END)),
	TransComment_day29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN TransComment ELSE NULL END),
	Qty_day30 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN Qty ELSE 0 END)),
	TransComment_day30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN TransComment ELSE NULL END),
	Qty_day31 = CONVERT(DECIMAL(14,3),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN Qty ELSE 0 END)),
	TransComment_day31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN TransComment ELSE NULL END),
	RowTotal = CONVERT(DECIMAL(20,3),SUM(Qty)),
	Filler = SPACE(5),
	MAX(tkUnitDetail.EmployeeCompany) AS EmployeeCompany
FROM
	CFGMainData,
	tkUnitDetail
	LEFT JOIN PR AS PR1 ON PR1.WBS1 = tkUnitDetail.WBS1 --AND PR1.WBS2 = ' ' AND tkUnitDetail.WBS3 = ' '
	LEFT JOIN PR AS PR2 on PR2.WBS1 = tkUnitDetail.WBS1 AND PR2.WBS2 = tkUnitDetail.WBS2 --AND PR2.WBS3 = ' ' AND PR2.WBS2 <> ' '
	LEFT JOIN PR AS PR3 on PR3.WBS1 = tkUnitDetail.WBS1 AND PR3.WBS2 = tkUnitDetail.WBS2 AND PR3.WBS3 = tkUnitDetail.WBS3 --AND PR3.WBS3 <> ' '
	LEFT JOIN CL ON PR1.ClientID = CL.ClientID
	LEFT JOIN UN ON tkUnitDetail.UnitTable = UN.UnitTable AND tkUnitDetail.Unit = UN.Unit AND UN.Company = @activeCompany
WHERE
	CFGMainData.Company = @activeCompany AND 
	Employee = @Employee AND
	tkUnitDetail.EndDate = @EndDate AND
	EmployeeCompany = @activeCompany AND
	PR1.WBS2 = N' ' AND
	PR1.WBS3 = N' ' AND
	PR2.WBS3 = N' '
GROUP BY
	tkUnitDetail.WBS1, tkUnitDetail.WBS2, tkUnitDetail.WBS3, tkUnitDetail.Seq
ORDER BY
	Seq

GO
