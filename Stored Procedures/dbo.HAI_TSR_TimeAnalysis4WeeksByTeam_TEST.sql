SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Conrad Harrison>
-- Create date: <May 22, 2019,,>
-- Description:	<Description,,>
-- =============================================
--CREATE PROCEDURE [dbo].[HAI_TSR_TimeAnalysis4WeeksByTeam_TEST]
CREATE PROCEDURE [dbo].[HAI_TSR_TimeAnalysis4WeeksByTeam_TEST]
--ALTER PROCEDURE [dbo].[HAI_TSR_TimeAnalysis4WeeksByTeam]
	@CompanyId varchar(2),
	@Team varchar(75),
	@LastSunday DATETIME,
	@LastSundayMinus27Days DATETIME,
	@LastSundayMinus27DaysAndTime DATETIME,
	@LastSundayAndTime DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	IF EXISTS (SELECT name FROM sysobjects WHERE name = 'setContextInfo' AND type = 'P')  

	Execute dbo.setContextInfo 
	-- PASSING IN COMPANYID
	--@StrCompany = /*N*/'01',
	@StrCompany = /*N*/@CompanyId,
	@StrAuditingEnabled  = 'N',
	@strUserName = /*N*/'HIT',
	@StrCultureName = 'en-US';-- Halff Associates Time Analysis
-- Group by Company, Office, Team, Team Practice Area
/*
DECLARE '2018-04-16T00:00:00.000'	date = '01/01/2005',
		'2018-05-13T00:00:00.000'	date = '12/31/2005'
*/
--ABOVE THIS LINE IS FOR TESTING ONLY

/**********************************************************************************************************************************

Query originally written by David Spring and sent to Darrell Aaron on 2018-04-20

Modified by Darrell Aaron on 2018-05-01 to make Vision reportable
	-Changed input parameters to custStartDate but needed a DECLARE of @StartDate and @EndDate to modify those dates
	-Added Deltek standard grouping options, Deltek sorting and Deltek WHERE clause
	-Converted Expected to float so that it would SUM in the report properly
	-Changed old SQL style joins to modern so that Deltek script injection would work properly
	-Deleted custom filtering parameters for Company, Team, TPA, Office because those can all be handled in Vision UI

**********************************************************************************************************************************/

-- COMMENTING @START DATE AND @ END DATE OUT AND PASSING IN LAST SUNDAY VARIABLES
--DECLARE @StartDate date, @EndDate date
BEGIN
--  Backup Start Date to prior Monday
	--Set @StartDate = DateAdd (day, -DatePart (dw, '2018-04-16T00:00:00.000') + 2, '2018-04-16T00:00:00.000')
--  Backup End Date to prior Sunday
	--Set @EndDate = DateAdd (day, -DatePart (dw, '2018-05-13T00:00:00.000') + 1, '2018-05-13T00:00:00.000')

	Select 
		--TRIAL COMPANY CODE BELOW
		org1.Code Company, 
		--org1.Code + ' - ' + org1.Label Company, 
		org2.Code + ' - ' + org2.Label Office, 
		tpa.TeamPracticeArea, 
		--TRIAL TEAM CODE BELOW
		org3.Code Team, 
		--org3.Code + ' - ' + org3.Label Team, 
		EM.Employee + ' - ' + EM.LastName + ', ' + IsNull (EM.PreferredName, EM.FirstName) Employee,
		-- COMMENTING @START DATE AND @ END DATE OUT AND PASSING IN LAST SUNDAY VARIABLES
		dbo.WorkDays (dbo.GreatestDate (@LastSundayMinus27DaysAndTime, EM.HireDate), 
		dbo.LeastDate (IsNull (EM.TerminationDate, @LastSundayAndTime), @LastSundayAndTime)) * 8 StdHrs,
	--  Least of the Total Hours or the Std Hours, but if rehired, then check the Hire Date against the Trans Date
		CONVERT(float, dbo.Least (Sum (l.RegHrs + l.OvtHrs + l.SpecialOvtHrs), 
					CASE When EM.HireDate > min (l.TransDate) Then 9999999 
					-- COMMENTING @START DATE AND @ END DATE OUT AND PASSING IN LAST SUNDAY VARIABLES
						Else abs (dbo.WorkDays (dbo.GreatestDate (@LastSundayMinus27DaysAndTime, EM.HireDate), 
						dbo.LeastDate (IsNull (EM.TerminationDate, @LastSundayAndTime), @LastSundayAndTime))) * 8
					END
				  )) Expected,
		Sum (l.RegHrs + l.OvtHrs + l.SpecialOvtHrs) Total,
		Sum (CASE When l.ChargeType = 'R' Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) Direct,
		Sum (CASE When l.ChargeType <> 'R' Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) InDirect,
		-- Direct / (Expected - Benefits) calculate this column
		EM.TargetRatio, -- Hide this column
		-- Target = Sum (TargetRatio * Expected) / Sum (Expected) calculate this column  /*MATHEMATICALLY THIS IS JUST TARGET RATIO ON AN INDIVIDUAL BASIS*/
		-- Variance from Goal = TargetRatio - (Direct / (Expected - Benefits)) calculate this column
		Sum (CASE When pto.StartWBS1 is not null Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) PTO,
		Sum (CASE When oh.StartWBS1 is not null Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) Overhead,
		Sum (CASE When l.ChargeType = 'P' Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) Prospect,
		Sum (CASE When mktg.StartWBS1 is not null Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) Marketing,
		Sum (CASE When pd.StartWBS1 is not null Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) ProfDevelop,
		Sum (CASE When conf.StartWBS1 is not null Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) Conference,
		Sum (CASE When b.StartWBS1 is not null Then l.RegHrs + l.OvtHrs + l.SpecialOvtHrs Else 0 END) Benefits -- Hide this column
		-- Other Benefits = Benefits - PTO calculate this column
	From EM
		Left Join CFGOrgCodes org1 on org1.Code = Left (EM.Org, (Select Org1Length From CFGFormat)) 
			and org1.OrgLevel = '1' -- Company
		Left Join CFGOrgCodes org2 on org2.Code = Substring (EM.Org, (Select Org1Length + 2 From CFGFormat), (Select Org2Length From CFGFormat)) 
			and org2.OrgLevel = '2'  -- Office
		Left Join CFGOrgCodes org3 on org3.Code = Right (EM.Org, (Select Org3Length From CFGFormat)) 
			and org3.OrgLevel = '3'  -- Team
		INNER JOIN EmployeeCustomTabFields ECT ON ECT.Employee = EM.Employee
		Left Join
		(Select Code, DataValue TeamPracticeArea From FW_CustomColumnValues
		Where InfocenterArea = 'Employees' and ColName = 'CustTeamPracticeArea') tpa on tpa.Code = ECT.CustTeamPracticeArea
		INNER JOIN LD l ON l.Employee = EM.Employee
		Left Join  -- Total Benefits
		(Select a.StartWBS1, a.EndWBS1 From CFGTimeAnalysisHeadingsData h, CFGTimeAnalysis a
		Where h.ReportColumn = a.ReportColumn and h.Benefit = 'Y') b on l.WBS1 between b.StartWBS1 and b.EndWBS1
		Left Join  -- PTO
		(Select a.StartWBS1, a.EndWBS1  From CFGTimeAnalysisHeadings h, CFGTimeAnalysis a
		Where h.ReportColumn = a.ReportColumn and h.Label = 'PTO') pto on l.WBS1 between pto.StartWBS1 and pto.EndWBS1
		Left Join  -- Overhead
		(Select a.StartWBS1, a.EndWBS1 From CFGTimeAnalysisHeadings h, CFGTimeAnalysis a
		Where h.ReportColumn = a.ReportColumn and h.Label = 'Overhead') oh on l.WBS1 between oh.StartWBS1 and oh.EndWBS1
		Left Join  -- Marketing
		(Select a.StartWBS1, a.EndWBS1 From CFGTimeAnalysisHeadings h, CFGTimeAnalysis a
		Where h.ReportColumn = a.ReportColumn and h.Label = 'Marketing') mktg on l.WBS1 between mktg.StartWBS1 and mktg.EndWBS1
		Left Join  -- Professional Development
		(Select a.StartWBS1, a.EndWBS1 From CFGTimeAnalysisHeadings h, CFGTimeAnalysis a
		Where h.ReportColumn = a.ReportColumn and h.Label = 'Prof Develop') pd on l.WBS1 between pd.StartWBS1 and pd.EndWBS1
		Left Join  -- Conference
		(Select a.StartWBS1, a.EndWBS1 From CFGTimeAnalysisHeadings h, CFGTimeAnalysis a
		Where h.ReportColumn = a.ReportColumn and h.Label = 'Conference') conf on l.WBS1 between conf.StartWBS1 and conf.EndWBS1


	Where l.TransDate 
		-- ADJUST BELOW TO ACCOUNT FOR PASS IN VARIABLES
		between @LastSundayMinus27DaysAndTime and @LastSundayAndTime  
		-- ADJUST BELOW TO ACCOUNT FOR PASS IN VARIABLES
		AND (((SUBSTRING(EM.Org, 1, 2) = /*N*/@CompanyId) 
		-- ADJUST BELOW TO ACCOUNT FOR PASS IN VARIABLES
		AND (SUBSTRING(EM.Org, 7, 2) = /*N*/@Team) 
		AND ( EXISTS (Select 'x' FROM CFGEmployeeStatus 
				WHERE CFGEmployeeStatus.status = EM.Status 
				AND ((CFGEmployeeStatus.Status = /*N*/'A'))) 
				-- ADJUST BELOW TO ACCOUNT FOR PASS IN VARIABLES
				OR ((EM.TerminationDate >= convert(dateTime,Replace(@LastSundayMinus27Days,'T',' '),121)) 
				AND (EM.TerminationDate <= convert(dateTime,Replace(@LastSunday,'T',' '),121))))))
				--OR ((EM.TerminationDate >= convert(dateTime,Replace('2018-04-16','T',' '),121)) 
				--AND (EM.TerminationDate <= convert(dateTime,Replace('2018-05-13','T',' '),121))))))
	Group by /***EXTEND GROUP CLAUSE***/ org1.code, 
		org1.Label, 
		org2.Code, 
		org2.Label, 
		org3.Code, 
		org3.Label, 
		tpa.TeamPracticeArea, 
		EM.Employee, 
		EM.LastName, 
		IsNull (EM.PreferredName, EM.FirstName), 
		EM.TargetRatio, 
		EM.HoursPerDay, 
		EM.HireDate, 
		EM.TerminationDate
	/***ORDER CLAUSE***/
--  Order by should be "Variance from Goal" ascending (done in report builder)
END
END

GO
