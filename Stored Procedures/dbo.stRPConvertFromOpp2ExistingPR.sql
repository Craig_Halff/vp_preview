SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPConvertFromOpp2ExistingPR]
AS

BEGIN -- Procedure stRPConvertFromOpp2ExistingPR

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- The purpose of this stored procedure is to move data from Opportunities and associations to existing Projects.
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  SET NOCOUNT ON

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @tabCPAInserted TABLE (
    ClientID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default
    UNIQUE (ClientID, WBS1, WBS2, WBS3)
  )

  DECLARE @tabPR_R TABLE (
    WBS1 nvarchar(30) COLLATE database_default,
    SiblingWBS1 nvarchar(30) COLLATE database_default
    UNIQUE(WBS1, SiblingWBS1)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- An Opportunity could be linked 2 different Projects:
  -- 1. Regular Project: PR.ChargeType = 'R' AND O.OpportunityID = PR.OpportunityID AND O.PRWBS1 = PR.WBS1.
  -- 2. Proposal Project: PR.ChargeType = 'P' AND O.OpportunityID = PR.OpportunityID AND O.PRProposalWBS1 = PR.WBS1.
  -- Furthermore, an Opportunity could be linked to no Project, to one Project, or two Projects.

  -- Save "R" Projects with non-null ProposalWBS1.
  -- There could be multiple "R" Projects with the same ProposalWBS1.
 
  INSERT @tabPR_R(
    WBS1,
    SiblingWBS1
  )
    SELECT DISTINCT 
      MIN(O.PRWBS1) OVER (PARTITION BY O.PRProposalWBS1) AS WBS1,
      O.PRProposalWBS1 AS SiblingWBS1
      FROM PR AS PR_R
        INNER JOIN Opportunity AS O ON PR_R.WBS1 = O.PRWBS1
        LEFT JOIN PR AS PR_P ON O.PRProposalWBS1 = PR_P.WBS1
      WHERE O.PRProposalWBS1 IS NOT NULL
        AND PR_R.WBS2 = ' ' AND PR_R.WBS3 = ' ' AND PR_R.ChargeType = 'R' AND PR_R.OpportunityID = O.OpportunityID
        AND PR_P.WBS2 = ' ' AND PR_P.WBS3 = ' ' AND PR_P.ChargeType = 'P' AND PR_P.OpportunityID = O.OpportunityID
        AND (PR_R.SiblingWBS1 IS NULL AND PR_P.SiblingWBS1 IS NULL)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION LinkSiblings

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Make link to sibling "R" Project.
    -- Must have INNER JOIN with PR_P to avoid orphaned ProposalWBS1. 

    UPDATE PR_R SET
      SiblingWBS1 = XPR.SiblingWBS1
      FROM  @tabPR_R AS XPR
        INNER JOIN PR AS PR_R ON XPR.WBS1 = PR_R.WBS1 AND PR_R.WBS2 = ' ' AND PR_R.WBS3 = ' '
        INNER JOIN PR AS PR_P ON XPR.SiblingWBS1 = PR_P.WBS1 AND PR_P.WBS2 = ' ' AND PR_P.WBS3 = ' ' 
      WHERE PR_R.SiblingWBS1 IS NULL

    -- Make link to sibling "P" Project.

    UPDATE PR_P SET
      SiblingWBS1 = XPR.WBS1
      FROM  @tabPR_R AS XPR
        INNER JOIN PR AS PR_P ON XPR.SiblingWBS1 = PR_P.WBS1 AND PR_P.WBS2 = ' ' AND PR_P.WBS3 = ' '
      WHERE PR_P.SiblingWBS1 IS NULL

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Sync SiblingWBS1 downward.

    UPDATE PR_R SET
      SiblingWBS1 = PR_TR.SiblingWBS1
      FROM  @tabPR_R AS XPR
        INNER JOIN PR AS PR_TR ON XPR.WBS1 = PR_TR.WBS1 AND PR_TR.WBS2 = ' ' AND PR_TR.WBS3 = ' '
        INNER JOIN PR AS PR_R ON PR_TR.WBS1 = PR_R.WBS1 AND (PR_TR.WBS2 + '|' + PR_TR.WBS3) <> (PR_R.WBS2 + '|' + PR_R.WBS3)

    UPDATE PR_P SET
      SiblingWBS1 = PR_TP.SiblingWBS1
      FROM  @tabPR_R AS XPR
        INNER JOIN PR AS PR_TP ON XPR.SiblingWBS1 = PR_TP.WBS1 AND PR_TP.WBS2 = ' ' AND PR_TP.WBS3 = ' '
        INNER JOIN PR AS PR_P ON PR_TP.WBS1 = PR_P.WBS1 AND (PR_TP.WBS2 + '|' + PR_TP.WBS3) <> (PR_P.WBS2 + '|' + PR_P.WBS3)

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  COMMIT TRANSACTION LinkSiblings

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Process existing Projects that were not created by the converting process "Opp to Proj"

    UPDATE PR SET
      OpenDate = COALESCE(PR.OpenDate, O.OpenDate),
      CloseDate = COALESCE(PR.CloseDate, O.CloseDate),
      ClosedReason = COALESCE(PR.ClosedReason, O.ClosedReason),  
      ClosedNotes = COALESCE(PR.ClosedNotes, O.ClosedNotes),
      Revenue =
        CASE
          WHEN PR.Revenue = 0
          THEN O.Revenue
          ELSE PR.Revenue
        END,
      WeightedRevenue = 
        CASE
          WHEN PR.WeightedRevenue = 0
          THEN O.WeightedRevenue
          ELSE PR.WeightedRevenue
        END,
      Probability = 
        CASE
          WHEN PR.Probability = 0
          THEN O.Probability
          ELSE PR.Probability
        END,
      LabCostTable = 
        CASE
          WHEN PR.LabCostTable = 0
          THEN O.LabCostTable
          ELSE PR.LabCostTable
        END,
      LabBillTable = 
        CASE
          WHEN PR.LabBillTable = 0
          THEN O.LabBillTable
          ELSE PR.LabBillTable
        END,
      EstFees = 
        CASE
          WHEN PR.EstFees = 0
          THEN O.EstFees
          ELSE PR.EstFees
        END,
	  Source = COALESCE(PR.Source, O.Source),
      MasterContract = COALESCE(PR.MasterContract, O.MasterContract),
      Solicitation = COALESCE(PR.Solicitation, O.Solicitation),
      ContractTypeGovCon = COALESCE(PR.ContractTypeGovCon, O.ContractTypeGovCon),
      CompetitionType = COALESCE(PR.CompetitionType, O.CompetitionType),
      AwardType = COALESCE(PR.AwardType, O.AwardType),
      PeriodOfPerformance = 
        CASE
          WHEN PR.PeriodOfPerformance = 0
          THEN O.PeriodOfPerformance
          ELSE PR.PeriodOfPerformance
        END,
      Duration = COALESCE(PR.Duration, O.Duration),
      MarketingCoordinator = COALESCE(PR.MarketingCoordinator, O.MarketingCoordinator),
      ProposalManager = COALESCE(PR.ProposalManager, O.ProposalManager),
      BusinessDeveloperLead = COALESCE(PR.BusinessDeveloperLead, O.BusinessDeveloperLead),
      IQID = COALESCE(PR.IQID, O.IQID),
      IQLastUpdate = COALESCE(PR.IQLastUpdate, O.IQLastUpdate),
      SFID = COALESCE(PR.SFID, O.SFID),
      SFLastModifiedDate = COALESCE(PR.SFLastModifiedDate, O.SFLastModifiedDate),
      TotalContractValue =
        CASE
          WHEN PR.TotalContractValue = 0
          THEN O.TotalContractValue
          ELSE PR.TotalContractValue
        END,
      AllocMethod = COALESCE(PR.AllocMethod, O.AllocMethod),
      Timescale = COALESCE(PR.Timescale, O.Timescale),
      PIMID = COALESCE(PR.PIMID, O.OpportunityID),

      ModUser = 'DPS20UPDPR',
      ModDate = GETUTCDATE()

      FROM Opportunity AS O
        INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND PR.CreateUser <> 'DPS20UPDPR'

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Process Projects that were created by the converting process "Opp to Proj"

   UPDATE PR SET
      ProjectType = COALESCE(PR.ProjectType, O.OpportunityType),
      OpenDate = COALESCE(PR.OpenDate, O.OpenDate),
      CloseDate = COALESCE(PR.CloseDate, O.CloseDate),
      ClosedReason = COALESCE(PR.ClosedReason, O.ClosedReason),  
      ClosedNotes = COALESCE(PR.ClosedNotes, O.ClosedNotes),
      Revenue =
        CASE
          WHEN PR.Revenue = 0
          THEN O.Revenue
          ELSE PR.Revenue
        END,
      WeightedRevenue = 
        CASE
          WHEN PR.WeightedRevenue = 0
          THEN O.WeightedRevenue
          ELSE PR.WeightedRevenue
        END,
      Probability = 
        CASE
          WHEN PR.Probability = 0
          THEN O.Probability
          ELSE PR.Probability
        END,
      LabCostTable = 
        CASE
          WHEN PR.LabCostTable = 0
          THEN O.LabCostTable
          ELSE PR.LabCostTable
        END,
      LabBillTable = 
        CASE
          WHEN PR.LabBillTable = 0
          THEN O.LabBillTable
          ELSE PR.LabBillTable
        END,
      ProjectCurrencyCode = COALESCE(PR.ProjectCurrencyCode, O.OppCurrencyCode),
      BillingCurrencyCode = COALESCE(PR.BillingCurrencyCode, O.BillingCurrencyCode),
      Org = COALESCE(PR.Org, O.Org),
      Principal = COALESCE(PR.Principal, O.Principal),
      ProjMgr = COALESCE(PR.ProjMgr, O.ProjMgr),
      Supervisor = COALESCE(PR.Supervisor, O.Supervisor),
      BillingClientID = COALESCE(PR.BillingClientID, O.ClientID),
      BillingContactID = COALESCE(PR.BillingContactID, O.ContactID),
      Responsibility = COALESCE(PR.Responsibility, O.OurRole),
      EstCompletionDate = COALESCE(PR.EstCompletionDate, O.EstCompletionDate),
      EstStartDate = COALESCE(PR.EstStartDate, O.EstStartDate),
      EstFees = 
        CASE
          WHEN PR.EstFees = 0
          THEN O.EstFees
          ELSE PR.EstFees
        END,
      Source = COALESCE(PR.Source, O.Source),
      MasterContract = COALESCE(PR.MasterContract, O.MasterContract),
      Solicitation = COALESCE(PR.Solicitation, O.Solicitation),
      NAICS = COALESCE(PR.NAICS, O.NAICS),
      ContractTypeGovCon = COALESCE(PR.ContractTypeGovCon, O.ContractTypeGovCon),
      CompetitionType = COALESCE(PR.CompetitionType, O.CompetitionType),
      AwardType = COALESCE(PR.AwardType, O.AwardType),
      PeriodOfPerformance = 
        CASE
          WHEN PR.PeriodOfPerformance = 0
          THEN O.PeriodOfPerformance
          ELSE PR.PeriodOfPerformance
        END,
      Duration = COALESCE(PR.Duration, O.Duration),
      MarketingCoordinator = COALESCE(PR.MarketingCoordinator, O.MarketingCoordinator),
      ProposalManager = COALESCE(PR.ProposalManager, O.ProposalManager),
      BusinessDeveloperLead = COALESCE(PR.BusinessDeveloperLead, O.BusinessDeveloperLead),
      IQID = COALESCE(PR.IQID, O.IQID),
      IQLastUpdate = COALESCE(PR.IQLastUpdate, O.IQLastUpdate),
      SFID = COALESCE(PR.SFID, O.SFID),
      SFLastModifiedDate = COALESCE(PR.SFLastModifiedDate, O.SFLastModifiedDate),
      TotalContractValue =
        CASE
          WHEN PR.TotalContractValue = 0
          THEN O.TotalContractValue
          ELSE PR.TotalContractValue
        END,
      AllocMethod = COALESCE(PR.AllocMethod, O.AllocMethod),
      Timescale = COALESCE(PR.Timescale, O.Timescale),
      PIMID = COALESCE(PR.PIMID, O.OpportunityID),

      ModUser = 'DPS20UPDPR',
      ModDate = GETUTCDATE()

      FROM Opportunity AS O
        INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND PR.CreateUser = 'DPS20UPDPR'

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Update Address fields in PR table.
    -- If all of the PR Address fields are NULL, then set these PR fields to be the same as Address fields in Opportunity.
    --   Address1
    --   Address2
    --   Address3
    --   City
    --   State
    --   Zip
    --   County
    --   Country  

    UPDATE PR SET
      Address1 = O.Address1,
      Address2 = O.Address2,
      Address3 = O.Address3,
      City = O.City,
      State = O.State,
      Zip = O.Zip,
      County = O.County,
      Country = O.Country,
      ModUser = 'DPS20UPDPR',
      ModDate = GETUTCDATE()
      FROM Opportunity AS O
        INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
      WHERE (
        PR.Address1 IS NULL AND
        PR.Address2 IS NULL AND
        PR.Address3 IS NULL AND
        PR.City IS NULL AND
        PR.State IS NULL AND
        PR.Zip IS NULL AND
        PR.County IS NULL AND
        PR.Country IS NULL
      )

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move File Links from OpportunityFileLinks to PRFileLinks.

    INSERT PRFileLinks(
      LinkID,
      WBS1,
      WBS2,
      WBS3,
      Description,
      FilePath,
      Graphic,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        OFL.LinkID AS LinkID,
        PR.WBS1 AS WBS1,
        PR.WBS2 AS WBS2,
        PR.WBS3 AS WBS3,
        OFL.Description AS Description,
        OFL.FilePath AS FilePath,
        OFL.Graphic As Graphic,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM OpportunityFileLinks AS OFL
          INNER JOIN Opportunity AS O ON OFL.OpportunityID = O.OpportunityID
          INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          LEFT JOIN PRFileLinks AS PFL ON OFL.FilePath = PFL.FilePath
            AND PR.WBS1 = PFL.WBS1 AND PR.WBS2 = PFL.WBS2 AND PR.WBS3 = PFL.WBS3
        WHERE PFL.WBS1 IS NULL

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move Documents from DocumentsOpportunity to DocumentsPR.

    INSERT DocumentsPR(
      ListID,
      WBS1,
      WBS2,
      WBS3,
      FolderURL,
      DocID,
      DocURL,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        OD.ListID AS ListID,
        PR.WBS1 AS WBS1,
        PR.WBS2 AS WBS2,
        PR.WBS3 AS WBS3,
        OD.FolderURL AS FolderURL,
        OD.DocID AS DocID,
        OD.DocURL AS DocURL,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM DocumentsOpportunity AS OD
          INNER JOIN Opportunity AS O ON OD.OpportunityID = O.OpportunityID
          INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          LEFT JOIN DocumentsPR AS PD ON 
            OD.FolderURL = PD.FolderURL AND OD.DocID = PD.DocID AND OD.DocURL = PD.DocURL
            AND PR.WBS1 = PD.WBS1 AND PR.WBS2 = PD.WBS2 AND PR.WBS3 = PD.WBS3
        WHERE PD.WBS1 IS NULL

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Update FW_Attachments.Application from Opportunities to Projects.

	UPDATE FW_Files Set FileApplicationType='Projects',
      ModUser = 'DPS20UPDPR',
      ModDate = GETUTCDATE()
	from FW_Files 
	INNER JOIN FW_Attachments ON FW_Attachments.FileID = FW_Files.FileID
	INNER JOIN Opportunity AS O ON FW_Attachments.Key1 = O.OpportunityID
	INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
	LEFT JOIN FW_Attachments AS FA ON FW_Attachments.FileID = FA.FileID and PR.WBS1 = FA.Key1 and isnull(FA.Key2, ' ') = ' ' and isnull(FA.Key3, ' ') = ' ' and FA.Application='Projects'
	Where FW_Files.FileApplicationType='Opportunities' and FA.FileID IS NULL

	UPDATE FW_Attachments Set Application='Projects', Key1=PR.WBS1, Key2 = ' ', Key3 = ' ',
      ModUser = 'DPS20UPDPR',
      ModDate = GETUTCDATE()
	from FW_Attachments 
	INNER JOIN Opportunity AS O ON FW_Attachments.Key1 = O.OpportunityID
	INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
	LEFT JOIN FW_Attachments AS FA ON FW_Attachments.FileID = FA.FileID and PR.WBS1 = FA.Key1 and isnull(FA.Key2, ' ') = ' ' and isnull(FA.Key3, ' ') = ' ' and FA.Application='Projects'
	Where FW_Attachments.Application='Opportunities' and FA.FileID IS NULL

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move Marketing Campaigns from MktCampaignOppAssoc to MktCampaignProjectAssoc.

    INSERT MktCampaignProjectAssoc(
      CampaignID,
      WBS1,
      WBS2,
      WBS3,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        OMC.CampaignID AS CampaignID,
        PR.WBS1 AS WBS1,
        PR.WBS2 AS WBS2,
        PR.WBS3 AS WBS3,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM MktCampaignOppAssoc AS OMC
          INNER JOIN Opportunity AS O ON OMC.OpportunityID = O.OpportunityID
          INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          LEFT JOIN MktCampaignProjectAssoc AS PMC ON OMC.CampaignID = PMC.CampaignID 
            AND PR.WBS1 = PMC.WBS1 AND PR.WBS2 = PMC.WBS2 AND PR.WBS3 = PMC.WBS3
        WHERE PMC.CampaignID IS NULL

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move Competitions from OpportunityCompetitionAssoc to PRCompetitionAssoc.

    INSERT PRCompetitionAssoc(
      RecordID,
      WBS1,
      WBS2,
      WBS3,
      ClientID,
      Strengths,
      Weakness,
      Notes,
      Incumbent
    )
      SELECT
        OC.RecordID AS RecordID,
        PR.WBS1 AS WBS1,
        PR.WBS2 AS WBS2,
        PR.WBS3 AS WBS3,
        OC.ClientID AS ClientID,
        OC.Strengths AS Strengths,
        OC.Weakness AS Weakness,
        OC.Notes AS Notes,
        OC.Incumbent AS Incumbent
        FROM OpportunityCompetitionAssoc AS OC
          INNER JOIN Opportunity AS O ON OC.OpportunityID = O.OpportunityID
          INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          LEFT JOIN PRCompetitionAssoc AS PC ON OC.RecordID = PC.RecordID 
        WHERE PC.WBS1 IS NULL

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move Employees from OpportunityEMAssoc to EMProjectAssoc.

    INSERT EMProjectAssoc(
      RecordID,
      WBS1,
      WBS2,
      WBS3,
      Employee,
      Role,
      RoleDescription,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RecordID,
        X.WBS1 AS WBS1,
        X.WBS2 AS WBS2,
        X.WBS3 AS WBS3,
        X.Employee AS Employee,
        X.Role AS Role,
        X.RoleDescription AS RoleDescription,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM ( /* X */
          SELECT DISTINCT
            PR.WBS1 AS WBS1,
            PR.WBS2 AS WBS2,
            PR.WBS3 AS WBS3,
            OEA.Employee AS Employee,
            OEA.Role AS Role,
            OEA.RoleDescription AS RoleDescription
            FROM Opportunity AS O
              INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
              INNER JOIN OpportunityEMAssoc AS OEA ON O.OpportunityID = OEA.OpportunityID AND OEA.Role = 'sysMC'
              LEFT JOIN EMProjectAssoc AS PEA ON OEA.Employee = PEA.Employee AND OEA.Role = PEA.Role
                AND PR.WBS1 = PEA.WBS1 AND PR.WBS2 = PEA.WBS2 AND PR.WBS3 = PEA.WBS3 AND PEA.Role = 'sysMC' 
            WHERE PEA.WBS1 IS NULL 
          UNION 
          SELECT DISTINCT
            PR.WBS1 AS WBS1,
            PR.WBS2 AS WBS2,
            PR.WBS3 AS WBS3,
            OEA.Employee AS Employee,
            OEA.Role AS Role,
            OEA.RoleDescription AS RoleDescription
            FROM Opportunity AS O
              INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
              INNER JOIN OpportunityEMAssoc AS OEA ON O.OpportunityID = OEA.OpportunityID AND OEA.Role = 'sysPRM'
              LEFT JOIN EMProjectAssoc AS PEA ON OEA.Employee = PEA.Employee AND OEA.Role = PEA.Role
                AND PR.WBS1 = PEA.WBS1 AND PR.WBS2 = PEA.WBS2 AND PR.WBS3 = PEA.WBS3 AND PEA.Role = 'sysPRM' 
            WHERE PEA.WBS1 IS NULL 
          UNION
          SELECT DISTINCT
            PR.WBS1 AS WBS1,
            PR.WBS2 AS WBS2,
            PR.WBS3 AS WBS3,
            OEA.Employee AS Employee,
            OEA.Role AS Role,
            OEA.RoleDescription AS RoleDescription
            FROM Opportunity AS O
              INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
              INNER JOIN OpportunityEMAssoc AS OEA ON O.OpportunityID = OEA.OpportunityID AND OEA.Role = 'sysBDL'
              LEFT JOIN EMProjectAssoc AS PEA ON OEA.Employee = PEA.Employee AND OEA.Role = PEA.Role
                AND PR.WBS1 = PEA.WBS1 AND PR.WBS2 = PEA.WBS2 AND PR.WBS3 = PEA.WBS3 AND PEA.Role = 'sysBDL' 
            WHERE PEA.WBS1 IS NULL 

          /* Only add Key Employees {ProjMgr, Principal, Supervisor} to EMProjectAssoc for newly created Projects. */

          UNION
          SELECT DISTINCT
            PR.WBS1 AS WBS1,
            PR.WBS2 AS WBS2,
            PR.WBS3 AS WBS3,
            OEA.Employee AS Employee,
            OEA.Role AS Role,
            OEA.RoleDescription AS RoleDescription
            FROM Opportunity AS O
              INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND PR.CreateUser = 'DPS20UPDPR'
              INNER JOIN OpportunityEMAssoc AS OEA ON O.OpportunityID = OEA.OpportunityID AND OEA.Role = 'sysPM'
              LEFT JOIN EMProjectAssoc AS PEA ON OEA.Employee = PEA.Employee AND OEA.Role = PEA.Role
                AND PR.WBS1 = PEA.WBS1 AND PR.WBS2 = PEA.WBS2 AND PR.WBS3 = PEA.WBS3 AND PEA.Role = 'sysPM' 
            WHERE PEA.WBS1 IS NULL 
          UNION
          SELECT DISTINCT
            PR.WBS1 AS WBS1,
            PR.WBS2 AS WBS2,
            PR.WBS3 AS WBS3,
            OEA.Employee AS Employee,
            OEA.Role AS Role,
            OEA.RoleDescription AS RoleDescription
            FROM Opportunity AS O
              INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND PR.CreateUser = 'DPS20UPDPR'
              INNER JOIN OpportunityEMAssoc AS OEA ON O.OpportunityID = OEA.OpportunityID AND OEA.Role = 'sysPR'
              LEFT JOIN EMProjectAssoc AS PEA ON OEA.Employee = PEA.Employee AND OEA.Role = PEA.Role
                AND PR.WBS1 = PEA.WBS1 AND PR.WBS2 = PEA.WBS2 AND PR.WBS3 = PEA.WBS3 AND PEA.Role = 'sysPR' 
            WHERE PEA.WBS1 IS NULL 
          UNION
          SELECT DISTINCT
            PR.WBS1 AS WBS1,
            PR.WBS2 AS WBS2,
            PR.WBS3 AS WBS3,
            OEA.Employee AS Employee,
            OEA.Role AS Role,
            OEA.RoleDescription AS RoleDescription
            FROM Opportunity AS O
              INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND PR.CreateUser = 'DPS20UPDPR'
              INNER JOIN OpportunityEMAssoc AS OEA ON O.OpportunityID = OEA.OpportunityID AND OEA.Role = 'sysSP'
              LEFT JOIN EMProjectAssoc AS PEA ON OEA.Employee = PEA.Employee AND OEA.Role = PEA.Role
                AND PR.WBS1 = PEA.WBS1 AND PR.WBS2 = PEA.WBS2 AND PR.WBS3 = PEA.WBS3 AND PEA.Role = 'sysSP'
            WHERE PEA.WBS1 IS NULL 

          /* When Project is newly created in this conversion process, transfer Opportunity-Employees with other roles or no role to Project. */

          UNION
          SELECT DISTINCT
            PR.WBS1 AS WBS1,
            PR.WBS2 AS WBS2,
            PR.WBS3 AS WBS3,
            OEA.Employee AS Employee,
            OEA.Role AS Role,
            OEA.RoleDescription AS RoleDescription
            FROM Opportunity AS O
              INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND PR.CreateUser = 'DPS20UPDPR'
              INNER JOIN OpportunityEMAssoc AS OEA ON O.OpportunityID = OEA.OpportunityID
                AND (OEA.Role IS NULL OR OEA.Role NOT IN ('sysMC', 'sysPRM', 'sysBDL', 'sysPM', 'sysPR', 'sysSP'))
              LEFT JOIN EMProjectAssoc AS PEA ON OEA.Employee = PEA.Employee AND ISNULL(OEA.Role, '|') = ISNULL(PEA.Role, '|')
                AND PR.WBS1 = PEA.WBS1 AND PR.WBS2 = PEA.WBS2 AND PR.WBS3 = PEA.WBS3 
                AND (PEA.Role IS NULL OR PEA.Role NOT IN ('sysMC', 'sysPRM', 'sysBDL', 'sysPM', 'sysPR', 'sysSP'))
            WHERE PEA.WBS1 IS NULL 

        ) AS X

    -- Ensure that the 3 Key "Pursuit" Employees are updated on Project row if needed.

    --> Marketing Coordinator

    UPDATE PR SET
      MarketingCoordinator = 
        CASE
          WHEN PEA.Role = 'sysMC' AND PR.MarketingCoordinator IS NULL
          THEN PEA.Employee
          ELSE PR.MarketingCoordinator
        END
      FROM EMProjectAssoc AS PEA 
        INNER JOIN PR ON PR.WBS1 = PEA.WBS1 AND PR.WBS2 = PEA.WBS2 AND PR.WBS3 = PEA.WBS3 
          AND PEA.Role = 'sysMC' 
      WHERE PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND PR.MarketingCoordinator IS NULL 

    --> Proposal Manager

    UPDATE PR SET
      ProposalManager =
        CASE
          WHEN PEA.Role = 'sysPRM' AND PR.ProposalManager IS NULL
          THEN PEA.Employee
          ELSE PR.ProposalManager
        END
      FROM EMProjectAssoc AS PEA 
        INNER JOIN PR ON PR.WBS1 = PEA.WBS1 AND PR.WBS2 = PEA.WBS2 AND PR.WBS3 = PEA.WBS3 
          AND PEA.Role = 'sysPRM'
      WHERE PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND PR.ProposalManager IS NULL

    --> Business Developer Lead

    UPDATE PR SET
      BusinessDeveloperLead = 
        CASE
          WHEN PEA.Role = 'sysBDL' AND PR.BusinessDeveloperLead IS NULL
          THEN PEA.Employee
          ELSE PR.BusinessDeveloperLead
        END
      FROM EMProjectAssoc AS PEA 
        INNER JOIN PR ON PR.WBS1 = PEA.WBS1 AND PR.WBS2 = PEA.WBS2 AND PR.WBS3 = PEA.WBS3 
          AND PEA.Role = 'sysBDL'
      WHERE PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND PR.BusinessDeveloperLead IS NULL

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move Clendors (Firms Grid) from OpportunityClientAssoc to ClendorProjectAssoc.

    -- Insert Clendors in ClendorProjectAssoc at WBS1 level.

    INSERT ClendorProjectAssoc(
      ClientID,
      WBS1,
      WBS2,
      WBS3,
      TeamStatus,
      ClientConfidential,
      Role,
      RoleDescription,
      Address,
      PrimaryInd,
      ClientInd,
      VendorInd,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      OUTPUT
        INSERTED.ClientID,
        INSERTED.WBS1,
        INSERTED.WBS2,
        INSERTED.WBS3
        INTO @tabCPAInserted(
          ClientID,
          WBS1,
          WBS2,
          WBS3
        )
      SELECT
        OCA.ClientID AS ClientID,
        PR.WBS1 AS WBS1,
        PR.WBS2 AS WBS2,
        PR.WBS3 AS WBS3,
        NULL AS TeamStatus,
        NULL AS ClientConfidential,
        CASE 
          WHEN EXISTS(
            SELECT DISTINCT WBS1, WBS2, WBS3 
              FROM ClendorProjectAssoc 
              WHERE Role = 'sysOwner' AND ClientInd = 'Y' 
                AND WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          )
          THEN NULL
          ELSE OCA.Role
        END AS Role,
        COALESCE(PCA.RoleDescription, OCA.RoleDescription) AS RoleDescription,
        OCA.Address AS Address,
        CASE 
          WHEN EXISTS(
            SELECT DISTINCT WBS1, WBS2, WBS3 
              FROM ClendorProjectAssoc 
              WHERE PrimaryInd = 'Y' AND ClientInd = 'Y' 
                AND WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          )
          THEN 'N'
          ELSE OCA.PrimaryInd
        END AS PrimaryInd,
        OCA.ClientInd AS ClientInd,
        OCA.VendorInd AS VendorInd,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM OpportunityClientAssoc AS OCA
          INNER JOIN Opportunity AS O ON OCA.OpportunityID = O.OpportunityID
          INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' 
          LEFT JOIN ClendorProjectAssoc AS PCA ON OCA.ClientID = PCA.ClientID 
            AND PR.WBS1 = PCA.WBS1 AND PR.WBS2 = PCA.WBS2 AND PR.WBS3 = PCA.WBS3
        WHERE PCA.ClientID IS NULL

    -- Must propagate downward, those Clendors of Type "Client" (but not "Vendor"), from WBS1 to all descendant WBS levels.

    INSERT ClendorProjectAssoc(
      ClientID,
      WBS1,
      WBS2,
      WBS3,
      TeamStatus,
      ClientConfidential,
      Role,
      RoleDescription,
      Address,
      PrimaryInd,
      ClientInd,
      VendorInd,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        PCAT.ClientID AS ClientID,
        PRC.WBS1 AS WBS1,
        PRC.WBS2 AS WBS2,
        PRC.WBS3 AS WBS3,
        PCAT.TeamStatus AS TeamStatus,
        PCAT.ClientConfidential AS ClientConfidential,
        PCAT.Role AS Role,
        PCAT.RoleDescription AS RoleDescription,
        PCAT.Address AS Address,
        PCAT.PrimaryInd AS PrimaryInd,
        PCAT.ClientInd AS ClientInd,
        PCAT.VendorInd AS VendorInd,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM ClendorProjectAssoc AS PCAT
          /* Only want to propagate downward those ClendorProjectAssoc rows that were inserted in the step above from WBS1 level. */
          /* Only want to propagate downward the Client rows. */
          INNER JOIN @tabCPAInserted AS ICPA ON PCAT.ClientInd = 'Y' AND PCAT.ClientID = ICPA.ClientID
            AND PCAT.WBS1 = ICPA.WBS1 AND PCAT.WBS2 = ICPA.WBS2 AND PCAT.WBS3 = ICPA.WBS3
          INNER JOIN PR AS PRT ON PCAT.WBS1 = PRT.WBS1 AND PRT.WBS2 = ' ' AND PRT.WBS3 = ' '  
          INNER JOIN PR AS PRC ON PRT.WBS1 = PRC.WBS1 
            AND NOT (PRC.WBS2 = ' ' AND PRC.WBS3 = ' ')   
          LEFT JOIN ClendorProjectAssoc AS PCAC ON PCAC.ClientInd = 'Y' AND PCAT.ClientID = PCAC.ClientID 
            AND PRC.WBS1 = PCAC.WBS1 AND PRC.WBS2 = PCAC.WBS2 AND PRC.WBS3 = PCAC.WBS3
        WHERE PCAC.ClientID IS NULL

    --MJU SERA Architects and TKDA bug null addresses appearing in PR.CLAddress, so update null address in the association table before Updating PR.CLAddress
	UPDATE ClendorProjectAssoc
		SET Address = PR.CLAddress
		FROM PR 
		  INNER JOIN ClendorProjectAssoc PCA on PCA.ClientID = PR.ClientID and PCA.wbs1 = PR.WBS1 and PCA.WBS2 = PR.WBS2 and PCA.WBS3 = PR.WBS3
		WHERE PCA.Address is null and PR.CLAddress is not null and PCA.PrimaryInd = 'Y' and PCA.ClientInd = 'Y'

    -- Update the Primary ClientID and CLAddress for all WBS levels.

    UPDATE PR SET 
      ClientID = PCA.ClientID,
      CLAddress = PCA.Address
      FROM PR
        INNER JOIN ClendorProjectAssoc AS PCA ON PR.WBS1 = PCA.WBS1 AND PR.WBS2 = PCA.WBS2 AND PR.WBS3 = PCA.WBS3
      WHERE PCA.PrimaryInd = 'Y' AND ClientInd = 'Y' AND PCA.Address is not null

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move Contacts from OpportunityContactAssoc to PRContactAssoc.
    -- PRContactAssoc is only at WBS1 level.

    INSERT PRContactAssoc(
      ContactID,
      WBS1,
      Role,
      RoleDescription,
      PrimaryInd,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        OCA.ContactID AS ContactID,
        PR.WBS1 AS WBS1,
        CASE 
          WHEN EXISTS(
            SELECT DISTINCT WBS1
              FROM PRContactAssoc 
              WHERE Role = 'sysOwner' AND WBS1 = PR.WBS1
          )
          THEN NULL
          ELSE OCA.Role
        END AS Role,
        COALESCE(PCA.RoleDescription, OCA.RoleDescription) AS RoleDescription,
        CASE 
          WHEN 
            EXISTS(
              SELECT DISTINCT WBS1
                FROM PRContactAssoc 
                WHERE PrimaryInd = 'Y' AND WBS1 = PR.WBS1
            )
            OR 
            OCA.contactID not in (select C.ContactID from Contacts C 
            Inner join ClendorProjectAssoc P ON P.ClientID = C.ClientID 
            AND  P.PrimaryIND = 'Y' AND P.WBS1 = PR.WBS1) 
          THEN 'N'
          ELSE OCA.PrimaryInd
        END AS PrimaryInd,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM OpportunityContactAssoc AS OCA
          INNER JOIN Opportunity AS O ON OCA.OpportunityID = O.OpportunityID
          INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          LEFT JOIN PRContactAssoc AS PCA ON OCA.ContactID = PCA.ContactID AND PR.WBS1 = PCA.WBS1 
        WHERE PCA.ContactID IS NULL

    -- Update the Primary ContactID for all WBS levels.

    UPDATE PR SET 
      ContactID = PCA.ContactID
      FROM PR
        INNER JOIN PRContactAssoc AS PCA ON PR.WBS1 = PCA.WBS1
      WHERE PCA.PrimaryInd = 'Y'

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move Proposals from OpportunityProposals to PRProposals.

    INSERT PRProposals(
      ProposalID,
      WBS1,
      WBS2,
      WBS3,
      Name,
      Number,
      Type,
      Org,
      Employee,
      Status,
      Source,
      DateAdvertised,
      SubmittalDate,
      Notes,
      LinkedProposal,
      LinkedGovtProposal,
      DueDate,
      AwardDate,
      Fee,
      LinkedSF330,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        OP.ProposalID AS ProposalID,
        PR.WBS1 AS WBS1,
        PR.WBS2 AS WBS2,
        PR.WBS3 AS WBS3,
        OP.Name AS Name,
        OP.Number AS Number,
        OP.Type AS Type,
        OP.Org AS Org,
        OP.Employee AS Employee,
        OP.Status AS Status,
        OP.Source AS Source,
        OP.DateAdvertised AS DateAdvertised,
        OP.SubmittalDate AS SubmittalDate,
        OP.Notes AS Notes,
        OP.LinkedProposal AS LinkedProposal,
        OP.LinkedGovtProposal AS LinkedGovtProposal,
        OP.DueDate AS DueDate,
        OP.AwardDate AS AwardDate,
        OP.Fee AS Fee,
        OP.LinkedSF330 AS LinkedSF330,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM OpportunityProposals AS OP
          INNER JOIN Opportunity AS O ON OP.OpportunityID = O.OpportunityID
          INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          LEFT JOIN PRProposals AS PP ON OP.ProposalID = PP.ProposalID 
        WHERE PP.WBS1 IS NULL

    -- Add rows from PRProposals to CustomProposal, if one does not exist in CustomProposal.

    INSERT CustomProposal(
      CustomPropID,
      Name,
      Number,
      ProposalManager,
      DateAdvertised,
      DueDate,
      Org,
      PreferredDateFormat,
      LastSectionNumber,
      WBS1,
      ClientID,
      Status,
      Source,
      SubmittalDate,
      AwardDate,
      Fee,
      Type,
      Notes,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT 
        REPLACE(NEWID(), '-', '') AS CustomPropID,
        PRP.Name,
        PRP.Number,
        PRP.Employee AS ProposalManager,
        PRP.DateAdvertised,
        PRP.DueDate,
        PRP.Org,
        NULL AS PreferredDateFormat,
        0 AS LastSectionNumber,
        PRP.WBS1,  
        PR.ClientID, 
        PRP.Status,
        PRP.Source,
        PRP.SubmittalDate,
        PRP.AwardDate,
        PRP.Fee,
        PRP.Type,
        PRP.Notes,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM PRProposals AS PRP
          INNER JOIN PR ON PRP.WBS1 = PR.WBS1 AND PRP.WBS2 = PR.WBS2 AND PRP.WBS3 = PR.WBS3
        WHERE NOT EXISTS (SELECT 'x' FROM CustomProposal WHERE CustomProposal.Name = PRP.Name AND CustomProposal.WBS1 = PRP.WBS1)

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move Estimated Revenue from OpportunityRevenue to PRRevenue.

    INSERT PRRevenue(
      RevAllocID,
      WBS1,
      WBS2,
      WBS3,
      Description,
      RevenueDate,
      RevenueAmt,
      PercentRevenue,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        OER.RevAllocID AS RevAllocID,
        PR.WBS1 AS WBS1,
        PR.WBS2 AS WBS2,
        PR.WBS3 AS WBS3,
        OER.Description AS Description,
        OER.RevenueDate AS RevenueDate,
        OER.RevenueAmt AS RevenueAmt,
        OER.PercentRevenue AS PercentRevenue,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM OpportunityRevenue AS OER
          INNER JOIN Opportunity AS O ON OER.OpportunityID = O.OpportunityID
          INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          LEFT JOIN PRRevenue AS PER ON OER.RevAllocID = PER.RevAllocID 
        WHERE PER.WBS1 IS NULL

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move Project Codes from OpportunityProjectCodes to PRProjectCodes.

    INSERT PRProjectCodes(
      WBS1,
      WBS2,
      WBS3,
      ProjectCode,
      Fees,
      Seq,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        PR.WBS1 AS WBS1,
        PR.WBS2 AS WBS2,
        PR.WBS3 AS WBS3,
        OPC.ProjectCode AS ProjectCode,
        OPC.Fee AS Fees,
        0 AS Seq,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM OpportunityProjectCodes AS OPC
          INNER JOIN Opportunity AS O ON OPC.OpportunityID = O.OpportunityID
          INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          LEFT JOIN PRProjectCodes AS PPC ON OPC.ProjectCode = PPC.ProjectCode
            AND PR.WBS1 = PPC.WBS1 AND PR.WBS2 = PPC.WBS2 AND PR.WBS3 = PPC.WBS3
        WHERE PPC.WBS1 IS NULL

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Add a row to PRDescriptions if one does not exist.

    INSERT PRDescriptions(
      WBS1,
      WBS2,
      WBS3,
      DescCategory,
      Description,
      DefaultInd,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        PR.WBS1 AS WBS1,
        PR.WBS2 AS WBS2,
        PR.WBS3 AS WBS3,
        'sysDesc' AS DescCategory,
        O.Description AS Description,
        'N' AS DefaultInd,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM Opportunity AS O 
          INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          LEFT JOIN PRDescriptions AS PD ON PR.WBS1 = PD.WBS1 AND PR.WBS2 = PD.WBS2 AND PR.WBS3 = PD.WBS3 AND PD.DescCategory = 'sysDesc'
        WHERE O.Description IS NOT NULL AND PD.WBS1 IS NULL

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move Photos from OpportunityPhoto to PRAdditionalData.

    UPDATE PAD SET
      WBS1 = PR.WBS1,
      WBS2 = PR.WBS2,
      WBS3 = PR.WBS3,
      Photo = OP.Photo,
      ModUser = 'DPS20UPDPR',
      ModDate = GETUTCDATE()
      FROM OpportunityPhoto AS OP
        INNER JOIN Opportunity AS O ON OP.OpportunityID = O.OpportunityID
        INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND PR.CreateUser = 'DPS20UPDPR'
        INNER JOIN PRAdditionalData AS PAD ON PR.WBS1 = PAD.WBS1 AND PR.WBS2 = PAD.WBS2 AND PR.WBS3 = PAD.WBS3 AND PAD.Photo IS NULL
      WHERE OP.Photo IS NOT NULL

    INSERT PRAdditionalData(
      PRAdditionalDataID,
      WBS1,
      WBS2,
      WBS3,
      Photo,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS PRAdditionalDataID,
        PR.WBS1 AS WBS1,
        PR.WBS2 AS WBS2,
        PR.WBS3 AS WBS3,
        OP.Photo AS Photo,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM OpportunityPhoto AS OP
          INNER JOIN Opportunity AS O ON OP.OpportunityID = O.OpportunityID AND OP.Photo IS NOT NULL
          INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          LEFT JOIN PRAdditionalData AS PAD ON PR.WBS1 = PAD.WBS1 AND PR.WBS2 = PAD.WBS2 AND PR.WBS3 = PAD.WBS3
        WHERE PAD.PRAdditionalDataID IS NULL AND PR.CreateUser = 'DPS20UPDPR'

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move KonaSpace from Opportunity to PRAdditionalData.

    UPDATE PAD SET
      WBS1 = PR.WBS1,
      WBS2 = PR.WBS2,
      WBS3 = PR.WBS3,
      KonaSpace = O.KonaSpace,
      ModUser = 'DPS20UPDPR',
      ModDate = GETUTCDATE()
      FROM Opportunity AS O
        INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND PR.CreateUser = 'DPS20UPDPR'
        INNER JOIN PRAdditionalData AS PAD ON PR.WBS1 = PAD.WBS1 AND PR.WBS2 = PAD.WBS2 AND PR.WBS3 = PAD.WBS3 AND PAD.KonaSpace = 0
      WHERE O.KonaSpace <> 0

    INSERT PRAdditionalData(
      PRAdditionalDataID,
      WBS1,
      WBS2,
      WBS3,
      KonaSpace,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS PRAdditionalDataID,
        PR.WBS1 AS WBS1,
        PR.WBS2 AS WBS2,
        PR.WBS3 AS WBS3,
        O.KonaSpace AS KonaSpace,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM Opportunity AS O
          INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          LEFT JOIN PRAdditionalData AS PAD ON PR.WBS1 = PAD.WBS1 AND PR.WBS2 = PAD.WBS2 AND PR.WBS3 = PAD.WBS3
        WHERE O.KonaSpace <> 0 AND PAD.PRAdditionalDataID IS NULL AND PR.CreateUser = 'DPS20UPDPR'

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move Activity Associations (only for non-Milestone Activities) from Opportunity to Project.

    UPDATE OA SET
      WBS1 = PR.WBS1,
      WBS2 = PR.WBS2,
      WBS3 = PR.WBS3,
      OpportunityID = NULL,
      ModUser = 'DPS20UPDPR',
      ModDate = GETUTCDATE()
    FROM Activity AS OA
      INNER JOIN Opportunity AS O ON OA.OpportunityID = O.OpportunityID
      INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
      LEFT JOIN Activity AS PA ON OA.ActivityID = PA.ActivityID
        AND PR.WBS1 = PA.WBS1 AND PR.WBS2 = PA.WBS2 AND PR.WBS3 = PA.WBS3 AND PA.Type <> 'Milestone'
    WHERE OA.Type <> 'Milestone' AND PA.WBS1 IS NULL

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
    -- Move Service Estimate Data from Opportunity  to Project.

    -- PRConsultant
    INSERT PRConsultant(
      ConsultantID,
      WBS1,
      Account,
      OppConCost,
      OppConBill,
      DirectAcctFlg,
      SeqNo,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        OppConsultant.ConsultantID AS ConsultantID,
        PR.WBS1 AS WBS1,
        OppConsultant.Account AS Account,
        OppConsultant.OppConCost AS OppConCost,
        OppConsultant.OppConBill AS OppConBill,
        OppConsultant.DirectAcctFlg AS DirectAcctFlg,
        OppConsultant.SeqNo AS SeqNo,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM OppConsultant  
          INNER JOIN Opportunity AS O ON OppConsultant.OpportunityID = O.OpportunityID
          INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
            AND O.OppCurrencyCode = PR.ProjectCurrencyCode AND O.BillingCurrencyCode = PR.BillingCurrencyCode 
          LEFT JOIN PRConsultant ON OppConsultant.ConsultantID = PRConsultant.ConsultantID 
        WHERE PRConsultant.WBS1 IS NULL

    -- PRExpense
    INSERT PRExpense(
      ExpenseID,
      WBS1,
      Account,
      OppExpCost,
      OppExpBill,
      DirectAcctFlg,
      SeqNo,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        OppExpense.ExpenseID AS ExpenseID,
        PR.WBS1 AS WBS1,
        OppExpense.Account AS Account,
        OppExpense.OppExpCost AS OppConCost,
        OppExpense.OppExpBill AS OppConBill,
        OppExpense.DirectAcctFlg AS DirectAcctFlg,
        OppExpense.SeqNo AS SeqNo,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM OppExpense  
          INNER JOIN Opportunity AS O ON OppExpense.OpportunityID = O.OpportunityID
          INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
            AND O.OppCurrencyCode = PR.ProjectCurrencyCode AND O.BillingCurrencyCode = PR.BillingCurrencyCode 
          LEFT JOIN PRExpense ON OppExpense.ExpenseID = PRExpense.ExpenseID 
        WHERE PRExpense.WBS1 IS NULL

    -- PRLabor
    INSERT PRLabor(
      LaborID,
      WBS1,
      Name,
      EstimateHrs,
      EstimateCost,
      EstimateBill,
      CostRate,
      BillingRate,
      Category,
      GenericResourceID,
      SeqNo,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        OppLabor.LaborID AS LaborID,
        PR.WBS1 AS WBS1,
        OppLabor.Name AS Name,
        OppLabor.EstimateHrs AS EstimateHrs,
        OppLabor.EstimateCost AS EstimateCost,
        OppLabor.EstimateBill AS EstimateBill,
        OppLabor.CostRate AS CostRate,
        OppLabor.BillingRate AS BillingRate,
        OppLabor.Category AS Category,
        OppLabor.GenericResourceID AS GenericResourceID,
        OppLabor.SeqNo AS SeqNo,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM OppLabor  
          INNER JOIN Opportunity AS O ON OppLabor.OpportunityID = O.OpportunityID
          INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
            AND O.OppCurrencyCode = PR.ProjectCurrencyCode AND O.BillingCurrencyCode = PR.BillingCurrencyCode 
          LEFT JOIN PRLabor ON OppLabor.LaborID = PRLabor.LaborID 
        WHERE PRLabor.WBS1 IS NULL

    -- PRUnit
    INSERT PRUnit(
      UnitID,
      WBS1,
      Unit,
      UnitTable,
      PlannedQty,
      PlannedCost,
      PlannedBill,
      CostRate,
      BillingRate,
      DirectAcctFlg,
      SeqNo,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
        OppUnit.UnitID AS UnitID,
        PR.WBS1 AS WBS1,
        OppUnit.Unit AS Unit,
        OppUnit.UnitTable AS UnitTable,
        OppUnit.PlannedQty AS PlannedQty,
        OppUnit.PlannedCost AS PlannedCost,
        OppUnit.PlannedBill AS PlannedBill,
        OppUnit.CostRate AS CostRate,
        OppUnit.BillingRate AS BillingRate,
        OppUnit.DirectAcctFlg AS DirectAcctFlg,
        OppUnit.SeqNo AS SeqNo,
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate
        FROM OppUnit  
          INNER JOIN Opportunity AS O ON OppUnit.OpportunityID = O.OpportunityID
          INNER JOIN PR ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
            AND O.OppCurrencyCode = PR.ProjectCurrencyCode AND O.BillingCurrencyCode = PR.BillingCurrencyCode 
          LEFT JOIN PRUnit ON OppUnit.UnitID = PRUnit.UnitID 
        WHERE PRUnit.WBS1 IS NULL

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move Custom Grids and Fields from Opportunity to Project.

    EXECUTE dbo.stRPConvertOppUDIC2PR

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move Opportunity Milestones in Activity table to Project Milestones in table PRMilestone.

    EXECUTE dbo.stRPConvertMilestonesFromOppToPR

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  COMMIT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF

END -- stRPConvertFromOpp2ExistingPR
GO
