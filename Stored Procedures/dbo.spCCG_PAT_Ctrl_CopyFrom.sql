SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_Ctrl_CopyFrom]
	@v							Nvarchar(100),
	@vendorID					Nvarchar(20),
	@originatingVendorEnabled	bit
AS
BEGIN
	-- [DeltekStoredProc_CCG_PAT_Ctrl_LoadRoutingSummary] 0
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	DECLARE @sql Nvarchar(max)

	IF LEFT(@v, 3) = N'CF|' and LEFT(@v, 7) <> N'CF|PAT|'
	BEGIN									-- copy from function
		SET @v = SUBSTRING(@v, 4, 100)
		SET @v = REPLACE(@v, N'''', N'''''')

		SET @sql = N'
		SELECT -1 as PASeq, PA.*,
				PR1.Name as WBS1Name, PR.Name as WBSName, PR.Sublevel, PR.ChargeType, PR.Status
			FROM dbo.fnCCG_PAT_CopyFrom(N''' + @vendorID + ''') PA
				LEFT JOIN PR PR1 on PA.WBS1 = PR1.WBS1 and PR1.WBS2 = N'' '' and PR1.WBS3 = N'' ''
				LEFT JOIN PR on PA.WBS1 = PR.WBS1 and PA.WBS2 = PR.WBS2 and PA.WBS3 = PR.WBS3
			WHERE Vendor = N''' + @vendorID + ''' and PA.ID = ''' + @v + '''
			ORDER BY PA.SortOrder, PA.WBS1, PA.WBS2, PA.WBS3 '
		EXEC (@sql)
	END
    ELSE IF LEFT(@v, 3) = N'RC|' 
	BEGIN										-- recurring items
        SET @v = SUBSTRING(@v, 4, 100)

        SELECT -1 as PASeq, PA.WBS1, PA.WBS2, PA.WBS3, case when r.copyamount = 'Y' then PA.Amount else 0 end as Amount, case when r.copyamount = 'Y' then PA.NetAmount else 0 end as NetAmount,
                PA.GLAccount, PA.SuppressBill, PA.Description, ptx.TaxCode, ptx.PKey, case when r.copyamount = 'Y' then  PA.TaxAmount else 0 end as TaxAmount, PA.OriginatingVendor, PR1.Name as WBS1Name,
                PR.Name as WBSName, PR.Sublevel, PR.ChargeType, PR.Status, p.LiabCode, p.BankCode,
                case
                    when upper(ISNULL(p.PayTerms, 'DATE')) = 'DATE' and vea.PayTerms like '[0-9]%' then vea.PayTerms
                    else p.PayTerms
                end as PayTerms, p.[Address], p.Company
            FROM CCG_PAT_ProjectAmount PA
                INNER JOIN (
                    SELECT top 1 * FROM CCG_PAT_Payable p
                        WHERE p.RecurSeq = CAST(@v as INT)
                        ORDER BY PayableDate desc
                ) p on PA.PayableSeq = p.seq
				LEFT JOIN CCG_PAT_ProjectAmountTax ptx on ptx.PayableSeq = PA.PayableSeq and ptx.PKey = PA.PKey
                LEFT JOIN VEAccounting vea on (p.Company = vea.Company or p.Company is null)
                    and p.Vendor = vea.Vendor
                INNER JOIN CCG_PAT_RECUR r on r.seq = p.recurseq
                LEFT JOIN PR PR1 on PA.WBS1 = PR1.WBS1 and PR1.WBS2 = N' ' and PR1.WBS3 = N' '
                LEFT JOIN PR on PA.WBS1 = PR.WBS1 and PA.WBS2 = PR.WBS2 and PA.WBS3 = PR.WBS3
			ORDER BY PA.Seq, PA.WBS1, PA.WBS2, PA.WBS3, ptx.PKey
	END
    ELSE
	BEGIN
		DECLARE @CF_PAT bit = case when LEFT(@v, 7) = N'CF|PAT|' then 1 else 0 end
		IF @CF_PAT = 1 SET @v = SUBSTRING(@v, 8, 100) 

        -- don't import amount for now, hardcoded to 0 in queries, make configurable?
        SELECT -1 as PASeq, PA.WBS1, PA.WBS2, PA.WBS3, case when @CF_PAT = 1 then PA.Amount else 0 end as Amount, 
				case when @CF_PAT = 1 then PA.NetAmount else 0 end as NetAmount, case when @CF_PAT = 1 then PA.TaxAmount else 0 end as TaxAmount,
				PA.GLAccount, PA.SuppressBill,
                PA.Description, ptx.TaxCode, ptx.PKey, PA.OriginatingVendor, PR1.Name as WBS1Name, PR.Name as WBSName, PR.Sublevel,
                PR.ChargeType, PR.Status, p.LiabCode, p.BankCode, p.PayTerms, p.[Address], p.Company
            FROM CCG_PAT_ProjectAmount PA
                INNER JOIN CCG_PAT_Payable p on PA.PayableSeq = p.seq
				LEFT JOIN CCG_PAT_ProjectAmountTax ptx on ptx.PayableSeq = PA.PayableSeq and ptx.PKey = PA.PKey
                LEFT JOIN PR PR1 on PA.WBS1 = PR1.WBS1 and PR1.WBS2 = N' ' and PR1.WBS3 = N' '
                LEFT JOIN PR on PA.WBS1 = PR.WBS1 and PA.WBS2 = PR.WBS2 and PA.WBS3 = PR.WBS3
            WHERE PA.PayableSeq = CAST(@v AS INT)
			ORDER BY PA.WBS1, PA.WBS2, PA.WBS3, ptx.PKey
	END
END
GO
