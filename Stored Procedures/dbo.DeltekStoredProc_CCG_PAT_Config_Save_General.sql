SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_Save_General]
	@newValuesSql		Nvarchar(max)
AS
BEGIN
	/*
	[DeltekStoredProc_CCG_PAT_Config_Save_General]
		@newValuesSql = 'Series = 50, ActivationPassword = ''j+F83PH9RvHxJgWZNg4mjaUNs2DYdSMEMHyvuMyKdlO7BNH88/bBoxcLLxW0iTYd'', LogLevel = ''Warning'', LogFile = ''C:\Temp\EI.log'', InvoiceNumberLength = 7, ProjectNameLength = 40, DraftInvoiceWildcardString = ''*Project [:PR.WBS1] *Draft?2*.pdf'', UnbilledDetailWildcardString = ''Project [:PR.WBS1] Invoice Draft U*.pdf'', FinalInvoiceWildcardString = ''*Project [:PR.WBS1] *.pdf'', AcctRoles = ''DEFAULT;'', ROAcctRoles = ''CRMADM;'', SupportMessages = ''Y'', NotificationInterval = 2, SupportPeriodBillThru = ''N'', SupportDateBillThru = ''Y'', ShowResetVisible = ''A'', FreezeColumns = ''Col1Only'', AdvanceOnStageChange = ''NONE'', FilterSQL = ''Y'', FilterBillGroup = ''N'', HideDormant = ''Y'', FilterPDF = ''N'', FilterPDFDays = -1, MainGridFiltering = ''T'', SupportPackaging = ''Y'', DocsEnabled = ''Y'', DocsStorage = ''Copy'', DocsPackaging = ''N'', SupportBuiltInFeePctCpl = ''Q'', AllowFeePctCplReduction = ''Y'', AllowFeeToExceed100Pct = ''Y'', SaveFeePctCplImmediately = ''Y'', PctCompleteEnableUnitsColumns = ''Y'', SyncInvoiceStageToVision = ''Y'', ConfigEmployees = ''00001;00002'', SubgridMode = ''Wbs3SubGrid'''
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	int;			-- INT value indicates which parameter failed the security check

	-- Ensure that our inputs are safe (preventing SQL injection)
	SET @safeSql = dbo.fnCCG_PAT_RegexMatch(N'<SET LIST>', @newValuesSql);
	IF @safeSql < 1 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS Nvarchar(4));
		RETURN;
	END;

	SET @sSQL = N'
		UPDATE CCG_PAT_Config SET
			' + @newValuesSql;
	EXEC (@sSQL);
END;

GO
