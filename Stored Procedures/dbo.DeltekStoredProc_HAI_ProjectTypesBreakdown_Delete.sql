SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_HAI_ProjectTypesBreakdown_Delete]
	@WBS1					NVARCHAR(30)
	--, @WBS2					NVARCHAR(30)
	--, @WBS3					NVARCHAR(30)
    , @Seq					NVARCHAR(32)

AS 
BEGIN
  ------------------------------------------------------------------------
  SET NOCOUNT ON;

  ------------------------------------------------------------------------

DELETE 
FROM dbo.Projects_ProjectTypesBreakdown
WHERE WBS1 = @WBS1
	AND WBS2 = ' ' --@WBS2
	AND WBS3 = ' ' --@WBS3
	AND Seq = @Seq

  ------------------------------------------------------------------------
END
GO
