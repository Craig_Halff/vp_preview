SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Save_Security]
	@newValuesSql		Nvarchar(max)
AS
BEGIN
	/*
	DeltekStoredProc_CCG_EI_Config_Save_Security
		@newValuesSql = '(''DEFAULT'', ''Y'',''Y'',''N'',''N'',''N'',''N'',''N'',''N'',''N''), (''PROJECTMGR'', ''Y'',''N'',''N'',''N'',''N'',''N'',''Y'',''N'',''N'')'
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	int;			-- INT value indicates which parameter failed the security check

	-- Ensure that our inputs are safe (preventing SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<INSERT VALUES>', @newValuesSql) ;
	IF @safeSql < 1 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS varchar(4));
		RETURN;
	END;

	BEGIN TRANSACTION;
	DELETE FROM CCG_EI_ConfigSecurity;

	SET @sSQL = N'
		INSERT INTO CCG_EI_ConfigSecurity (
			Role, PctComplFullRights, PctComplChangeFeeMethod, PctComplChangeFeeBasis,
            PctComplChangeFee, PctComplChangeConstrAmt, PctComplChangeConstrFeePct, PctComplChangeUnitQty,
            PctComplChangeUnitPrice, PctComplChangeUnitLabel)
			VALUES ' + @newValuesSql;
	EXEC (@sSQL);
	COMMIT TRANSACTION;
END;
GO
