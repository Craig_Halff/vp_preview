SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_OppFeeTypeUpdate_All]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
DECLARE @OpportunityID NVARCHAR(50)

BEGIN TRY
	BEGIN TRANSACTION; 

	DECLARE CursorQuery CURSOR FOR
	(
		SELECT [OpportunityID]
		--,[Opportunity]
		--,[Name]
		FROM [Opportunity]
	)

	OPEN CursorQuery 
	FETCH NEXT FROM CursorQuery INTO @OpportunityID

	WHILE @@FETCH_STATUS = 0
		BEGIN
			-- Database changes here
			DECLARE @feeTypes VARCHAR(MAX)

			SELECT @feeTypes = COALESCE(@feeTypes+', ' ,'') + [CustPhaseBillingType]
			FROM 
			(
				SELECT [OpportunityID]
				,[CustPhaseTotal]
				,[CustPhaseBillingType]
				,[CustPhasePercent]
				FROM 
				(
					SELECT [OpportunityID]
					,SUM([CustPhaseTotal]) AS [CustPhaseTotal]
					,[CustPhaseBillingType]
					,SUM([CustPhasePercent]) AS [CustPhasePercent]
					FROM 
					(
						SELECT [OpportunityID]
						,[CustPhaseTotal]
						,CASE [CustPhaseBillingType]
							WHEN 'Cost Plus Max_Mult' THEN 'CPM'
							WHEN 'Cost Plus Max_Rate' THEN 'CPM'
							WHEN 'Cost Plus_Mult' THEN 'CP'
							WHEN 'Cost Plus_Rate' THEN 'CP'
							WHEN 'Lump Sum' THEN 'LS'
							WHEN 'Lump Sum by Task' THEN 'LS'
							WHEN 'Non-Billable' THEN 'NB'
						END AS [CustPhaseBillingType]
						,[CustPhasePercent]
						FROM [Opportunities_Phases]
						WHERE OpportunityID = @OpportunityID
					) AS B
					WHERE OpportunityID = @OpportunityID
					GROUP BY [OpportunityID], [CustPhaseBillingType]		
				) AS A
				WHERE OpportunityID = @OpportunityID
			) AS A
			ORDER BY [CustPhasePercent] DESC

			UPDATE [OpportunityCustomTabFields] 
			SET [CustFeeTypes] = @feeTypes
			WHERE [OpportunityID] = @OpportunityID
			-- End changes

			-- Next iteration
			SET @feeTypes = NULL
			FETCH NEXT FROM CursorQuery INTO @OpportunityID
		END

	-- Commit changes
	COMMIT TRANSACTION;  

	CLOSE CursorQuery 
	DEALLOCATE CursorQuery 

END TRY
BEGIN CATCH
	SELECT ERROR_NUMBER() AS ErrorNumber  
		  ,ERROR_MESSAGE() AS ErrorMessage;  

	-- Rollback all changes
    ROLLBACK TRANSACTION;  
END CATCH


END

GO
