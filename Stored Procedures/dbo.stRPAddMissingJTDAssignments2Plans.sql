SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPAddMissingJTDAssignments2Plans]
AS

BEGIN -- Procedure stRPAddMissingJTDAssignments2Plans

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to add missing JTD assignments to all Plans in a DB. 
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @strMsg nvarchar(max)
  DECLARE @chrNewLine AS char(2) = CHAR(13) + CHAR(10)

  DECLARE @strPlanID varchar(32)
  DECLARE @strPlanName nvarchar(255)
  DECLARE @strPlanNumber nvarchar(30)
  DECLARE @strWBS1 nvarchar(30)
  DECLARE @strRowID nvarchar(255)

  DECLARE @strHasMissingJTDLab varchar(1)
  DECLARE @strHasMissingJTDExp varchar(1)
  DECLARE @strHasMissingJTDCon varchar(1)

  DECLARE @tabPlan TABLE (
    PlanID varchar(32) COLLATE database_default,
    PlanName nvarchar(255) COLLATE database_default,
    PlanNumber nvarchar(30) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    RowID nvarchar(255) COLLATE database_default, 
    HasMissingJTDLab varchar(1) COLLATE database_default,
    HasMissingJTDExp varchar(1) COLLATE database_default,
    HasMissingJTDCon varchar(1) COLLATE database_default
    UNIQUE(PlanID, PlanName, PlanNumber, WBS1)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find Plans that have missing JTD Assignments.
  -- Must use a temp table here otherwise the cursor will get confused when Plans are being changed inside a loop. 

  INSERT @tabPlan(
    PlanID,
    PlanName,
    PlanNumber,
    WBS1,
    RowID,
    HasMissingJTDLab,
    HasMissingJTDExp,
    HasMissingJTDCon
  )
    SELECT DISTINCT
      X.PlanID,
      X.PlanName,
      X.PlanNumber,
      X.WBS1,
      X.RowID,
      X.HasMissingJTDLab,
      X.HasMissingJTDExp,
      X.HasMissingJTDCon
      FROM (
        SELECT
          P.PlanID AS PlanID,
          P.PlanName AS PlanName,
          P.PlanNumber AS PlanNumber,
          P.WBS1 AS WBS1,
          '|' + TT.TaskID AS RowID,
          J.HasMissingJTDLab AS HasMissingJTDLab,
          J.HasMissingJTDExp AS HasMissingJTDExp,
          J.HasMissingJTDCon AS HasMissingJTDCon 
          FROM RPPlan AS P
            /* Need TaskID of the top most Task row to be used later in the calls to AddJTD */
            INNER JOIN RPTask AS TT ON P.PlanID = TT.PlanID AND TT.OutlineLevel = 0
            CROSS APPLY dbo.stRP$tabHasMissingJTDAssignments(P.PlanID) AS J
      ) AS X
      WHERE HasMissingJTDLab = 'Y' OR HasMissingJTDExp = 'Y' OR HasMissingJTDCon = 'Y'

  DECLARE csr20191017_PNPlan CURSOR LOCAL FAST_FORWARD FOR
    SELECT
      RP.PlanID,
      RP.PlanName,
      RP.PlanNumber,
      RP.WBS1,
      RP.RowID,
      RP.HasMissingJTDLab,
      RP.HasMissingJTDExp,
      RP.HasMissingJTDCon
      FROM @tabPlan AS RP
      ORDER BY RP.PlanName

  OPEN csr20191017_PNPlan
  FETCH NEXT FROM csr20191017_PNPlan INTO @strPlanID, @strPlanName, @strPlanNumber, @strWBS1, @strRowID, @strHasMissingJTDLab, @strHasMissingJTDExp, @strHasMissingJTDCon

  WHILE (@@FETCH_STATUS = 0)
    BEGIN

      -- Note that PlanName could have serveral embedded "%", 
      -- the strange code with REPLACE was to handle this issue so that RAISERROR can work w/o error.

      SET @strMsg =
        REPLACE(REPLACE(
          '>>> Start Adding Missing JTD Assignments ' + @chrNewLine +
          '--> PlanID = "' + @strPlanID + '"' + @chrNewLine +
          '--> PlanName = "' + @strPlanName + '"' + @chrNewLine +
          '--> PlanNumber = "' + @strPlanNumber + '"' + @chrNewLine +
          '--> WBS1 = "' + @strWBS1 + '"' + @chrNewLine +
          '--> HasMissingJTDLab = "' + @strHasMissingJTDLab + '"' + @chrNewLine +
          '--> HasMissingJTDExp = "' + @strHasMissingJTDExp + '"' + @chrNewLine +
          '--> HasMissingJTDCon = "' + @strHasMissingJTDCon + '"'
        , '%', '%%'), '%%%%', '%%')

      RAISERROR(@strMsg, 0, 1) WITH NOWAIT, LOG

      IF(@strHasMissingJTDLab = 'Y') EXECUTE dbo.stRPAddJTDAssignment @strRowID, 1
      IF(@strHasMissingJTDExp = 'Y') EXECUTE dbo.stRPAddJTDExpense @strRowID, 1
      IF(@strHasMissingJTDCon = 'Y') EXECUTE dbo.stRPAddJTDConsultant @strRowID, 1
 
      SET @strMsg = 
        REPLACE(REPLACE(
          '<<< End Processing PlanID = "' + @strPlanID + '", PlanName = "' + @strPlanName + '"' + @chrNewLine + @chrNewLine
        , '%', '%%'), '%%%%', '%%')

      RAISERROR(@strMsg, 0, 1) WITH NOWAIT, LOG

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        FETCH NEXT FROM csr20191017_PNPlan INTO @strPlanID, @strPlanName, @strPlanNumber, @strWBS1, @strRowID, @strHasMissingJTDLab, @strHasMissingJTDExp, @strHasMissingJTDCon

    END -- While

  CLOSE csr20191017_PNPlan
  DEALLOCATE csr20191017_PNPlan

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

END -- stRPAddMissingJTDAssignments2Plans
GO
