SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_SendEmail] (
	@sourceApplication	varchar(40),
	@from				Nvarchar(80),
	@to					Nvarchar(max),
	@subject			Nvarchar(120),
	@subjectbatch		Nvarchar(120),
	@body				Nvarchar(max),
	@delay				int,
	@priority			varchar(12),
	@ccSender			bit
) AS
BEGIN
	SET NOCOUNT ON
	
	IF @delay <= 0
		INSERT INTO CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, BCCList, Subject,
				GroupSubject, Body, MaxDelay, Priority)
			VALUES (@sourceApplication, left(@from, 80), @to, (Case WHEN @ccSender = 1 THEN @from ELSE null END), null, left(@subject, 120),
				left(@subjectbatch, 120), @body, DEFAULT, @priority)
	ELSE
		INSERT INTO CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, BCCList, Subject,
				GroupSubject, Body, MaxDelay, Priority)
			VALUES (@sourceApplication, left(@from, 80), @to, (Case WHEN @ccSender = 1 THEN @from ELSE null END), null, left(@subject, 120),
				left(@subjectbatch, 120), @body, @delay, @priority)

	SELECT @@ROWCOUNT as RowsAffected
END
GO
