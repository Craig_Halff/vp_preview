SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteTextLibrary] @Name Nvarchar(50)
AS
   delete from TextLibFileLinks where Name=@Name
   delete from TextLibraryCustomTabFields where Name=@Name
   Exec DeleteCustomGridTabData 'TextLibrary',@Name,'',''
GO
