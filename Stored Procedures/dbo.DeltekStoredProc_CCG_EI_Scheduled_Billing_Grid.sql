SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Scheduled_Billing_Grid]
	@WBS1		Nvarchar(30),
	@WBS2		Nvarchar(30),
	@WBS3		Nvarchar(30),
	@phaseSeq	int
AS
BEGIN
	/*
	DeltekStoredProc_CCG_EI_Scheduled_Billing_Grid
		@WBS1 = '2002019.00',
		@WBS2 = ' ',
		@WBS3 = ' ',
		@phaseSeq = '0'
	*/

	SET NOCOUNT ON;

    SELECT /* BTSchedule.WBS1, BTSchedule.WBS2, BTSchedule.WBS3, BTSchedule.Seq, BTSchedule.PhaseSeq, */
		Convert(varchar(10), BTSchedule.ScheduleDate, 101) as [Schedule Date],
		BTLaborCats.[Description], BTSchedule.PctCompl as [Pct Compl], BTSchedule.Amount, BTSchedule.Billed
    FROM BTSchedule
		LEFT JOIN BTLaborCats ON BTLaborCats.Category = BTSchedule.Category
	WHERE WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3 and PhaseSeq = @phaseSeq
    ORDER BY Seq;

END;
GO
