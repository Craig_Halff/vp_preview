SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_AdvSearch_UpdateKey]
	@TABLENAME			varchar(200),		-- Deprecated
	@SAVED_OPTION_KEY	Nvarchar(200),
	@EMPLOYEEID			Nvarchar(32),
	@key				Nvarchar(500),
	@nameQuoted			Nvarchar(200),
	@value				Nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @sql nvarchar(max)
	DECLARE @Detail Nvarchar(max)
	DECLARE @valueQuoted Nvarchar(max) = N'''' + Replace(@value, '''', '''''') + ''''

    IF ISNULL(@key, N'') <> N'' BEGIN
		SET @sql = N'
            SELECT @Detail = Detail FROM CCG_PAT_ConfigOptions
                WHERE Type = ''' + @SAVED_OPTION_KEY + ''' and ConfigID = ''' + @key + ''''

		EXEC sp_executesql @sql, N'@Detail Nvarchar(max) output', @Detail output		
	END

    IF ISNULL(@Detail, N'') = N'' BEGIN
        -- no existing row, need to insert
        SET @sql = N'
            INSERT INTO CCG_PAT_ConfigOptions (ConfigID, Type, Description,
					Detail, ChangedBy, DateChanged)
                VALUES (''' + @key + ''', ''' + @SAVED_OPTION_KEY + ''', N' + @nameQuoted + ',
					N' + @valueQuoted + ', N''' + @EMPLOYEEID + ''', getutcdate())'
		EXEC (@sql)
    END
    ELSE IF @Detail <> @value BEGIN
        SET @sql = N'
            UPDATE CCG_PAT_ConfigOptions
                SET Description = N' + @nameQuoted + ',
                    Detail = N' + @valueQuoted + ',
                    ChangedBy = N''' + @EMPLOYEEID + ''',
                    DateChanged = getutcdate()
                WHERE ConfigID = ''' + @key + ''' and Type = ''' + @SAVED_OPTION_KEY + ''''
		EXEC (@sql)
	END
	SELECT @@ROWCOUNT as RowsAffected
END
GO
