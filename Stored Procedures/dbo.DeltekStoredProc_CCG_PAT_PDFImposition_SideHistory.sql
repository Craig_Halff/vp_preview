SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_PDFImposition_SideHistory] ( @seq int, @userLanguage varchar(10), @mode varchar(20), @stageFilterCSV varchar(1000)= '')
             AS EXEC spCCG_PAT_PDFImposition_SideHistory @seq,@userLanguage,@mode,@stageFilterCSV
GO
