SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_ValidateProjectAmountUpdate] ( @field varchar(100), @wbs1 nvarchar(30), @wbs2 nvarchar(7), @wbs3 nvarchar(7), @incInactive bit, @regularOnly bit, @VP2 bit= 0)
             AS EXEC spCCG_PAT_ValidateProjectAmountUpdate @field,@wbs1,@wbs2,@wbs3,@incInactive,@regularOnly,@VP2
GO
