SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Halff_InvoiceStatusEmail] 
	@statusType		VARCHAR(50)	--  {In Progress,Completed}
	,@past			INT			-- -15
	,@future		INT			-- 7
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE	  @NL		CHAR(2) = CHAR(13) + CHAR(10)
		, @TAB		CHAR(1) = CHAR(9)

		, @yellow	varchar(50)	= ' style="background-color: #F8FF71;"'
		, @orange	varchar(50)	= ' style="background-color: #FFD671;"'
		, @dkorange varchar(50)	= ' style="background-color: #FFA471;"'
		, @red		varchar(50)	= ' style="background-color: #FF7171;color:#FFFFFF;"'
		, @purple	varchar(50)	= ' style="background-color: #C271FF;color:#FFFFFF;"'
		, @gray		varchar(50) = ' style="background-color: #D9D9D9;"' 
		, @redText	varchar(50)	= ' style="color:#FF0000;"'

		, @CSS AS varchar(max) =  
'<style type="text/css">
	p {padding-top:15px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;}
	td, th {border: 1px solid black;padding: 5px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;font-size:9.5pt;}
	th {background-color: #6E6E6E;color: white;text-align:center;}
	.c1, .c2,. c3, .c4, .c5, .c6, .c7, .c8, .c9, .c10, .c11, .c12 {text-align:left;}
	.ctr {text-align:center;}
</style>'

		, @TableHeader AS	varchar(max) = 
'	<thead>
		<tr>
			<th style="width:45px;">Team</th>
			<th style="width:90px;">Reviewer</th>
			<th style="width:90px;">Project</th>
			<th>Project Name</th>
			<th style="width:60px;">Draft<br/>Goal</th>
			<th style="width:60px;">Draft<br/>Sent</th>
			<th style="width:60px;">Draft<br/>Returned</th>
			<th style="width:70px;">Revision<br/>Sent</th>
			<th style="width:70px;">Revision<br/>Returned</th>
			<th style="width:60px;">Final<br/>Sent</th>
			<th style="width:60px;">Final<br/>Goal</th>
			<th>Notes</th>
		</tr>
	</thead>'

		, @Now AS datetime = GETDATE()	
		, @StatusOrder		integer
		, @Status			varchar (20)
		, @Biller			varchar (50)
        , @Email			varchar (100)
		, @LastName			varchar (50)
		, @FirstName		varchar (25)
		, @Org				varchar (30)
		, @Reviewer			varchar (50)
		, @WBS1				varchar (30)
		, @Name				varchar (40)
		, @InvoiceStage		varchar (50)
		, @DraftGoal		datetime
		, @DraftSent		datetime
		, @DraftReturned	datetime
		, @ReviseAndReturn	datetime
		, @RevisionReady	datetime
		, @RevisionSent		datetime
		, @RevisionReturned	datetime
		, @FinalSent		datetime
		, @FinalGoal		datetime
		, @Notes			varchar (max)
		, @Loop1			integer
		, @Loop2			integer

DECLARE @tblDraftSent table
(  
	WBS1				varchar (30)
	, ActionDate		datetime
);

DECLARE @tblDraftReturned table
(  
	WBS1				varchar (30)
	, ActionDate		datetime
);

DECLARE @tblReviseAndReturn table
(  
	WBS1				varchar (30)
	, ActionDate		datetime
);

DECLARE @tblRevisionReady table
(  
	WBS1				varchar (30)
	, ActionDate		datetime
);

DECLARE @tblRevisionSent table
(  
	WBS1				varchar (30)
	, ActionDate		datetime
);

DECLARE @tblRevisionReturned table
(  
	WBS1				varchar (30)
	, ActionDate		datetime
);

DECLARE @tblFinalSent table
(  
	WBS1				varchar (30)
	, ActionDate		datetime
);

DECLARE @tblComposite table
(  
    Biller				varchar (8000)
    , Email				varchar (100)
	, LastName			varchar (50)
	, FirstName			varchar (25)
	, Org				varchar (30)
	, Reviewer			varchar (50)
	, WBS1				varchar (30)
	, [Name]			varchar (40)
	, InvoiceStage		varchar (50)
	, DraftGoal			datetime
	, DraftSent			datetime
	, DraftReturned		datetime
	, ReviseAndReturn	datetime
	, RevisionReady		datetime
	, RevisionSent		datetime
	, RevisionReturned	datetime
	, FinalSent			datetime
	, FinalGoal			datetime
	, Notes				varchar(max)
);

-- Draft Sent field
INSERT INTO @tblDraftSent
	SELECT A.WBS1 
	, MAX(B.ActionDate) AS ActionDate
	FROM
	(
		SELECT WBS1
		, B.CustDraftGoal
		FROM [dbo].[ProjectCustomTabFields] A, [dbo].[UDIC_Accounting] B
		WHERE A.CustBillingWeek IS NOT NULL
		-- today - @past <= draft goal date <= today + @future
		AND B.CustDraftGoal >= DATEADD(dd, -21, @Now)
		AND B.CustDraftGoal <= DATEADD(dd, 7, @Now)	
		AND B.CustWeek = RIGHT(A.CustBillingWeek, 1)
	) AS A
	INNER JOIN
	(
		SELECT WBS1
		, CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET,  ActionDate), DATENAME(TZOFFSET, SYSDATETIMEOFFSET()))) AS ActionDate
		FROM [dbo].[CCG_EI_History]
		WHERE ActionTaken = 'Stage Change'
		AND InvoiceStage = 'Ready For Review'
	) AS B
	ON B.WBS1 = A.WBS1
	-- draft goal date - @past <= action date <= today
	WHERE B.ActionDate >= DATEADD(dd, -15, A.CustDraftGoal)		
	AND B.ActionDate <= DATEADD(dd, 15, A.CustDraftGoal)	
	GROUP BY A.WBS1

-- Draft Returned field
INSERT INTO @tblDraftReturned
	SELECT A.WBS1
	, MAX(B.ActionDate) AS ActionDate
	FROM  @tblDraftSent A
	INNER JOIN
	(
		SELECT WBS1
		,CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET,  ActionDate), DATENAME(TZOFFSET, SYSDATETIMEOFFSET()))) AS ActionDate
		FROM [dbo].[CCG_EI_History]
		WHERE ActionTaken = 'Stage Change'
		AND InvoiceStage IN 
		(
			'Invoice As Is'
			,'Invoice As Noted'
			,'Revise and Return'
			,'Hold'
			,'Write-Off'
		)
	) AS B
	ON B.WBS1 = A.WBS1
	-- date >= draft sent date
	WHERE B.ActionDate >= A.ActionDate
	  AND A.ActionDate is not null
	GROUP BY A.WBS1

-- Revise and Return
INSERT INTO @tblReviseAndReturn
	SELECT A.WBS1
	, MAX(B.ActionDate) AS ActionDate
	FROM @tblDraftReturned A
	INNER JOIN 
	(
		SELECT WBS1
		,CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET,  ActionDate), DATENAME(TZOFFSET, SYSDATETIMEOFFSET()))) AS ActionDate
		FROM [dbo].[CCG_EI_History]
		WHERE ActionTaken = 'Stage Change'
		AND InvoiceStage = 'Revise and Return'
	) AS B
	ON B.WBS1 = A.WBS1
	AND B.ActionDate >= A.ActionDate
	AND A.ActionDate is not null
	GROUP BY A.WBS1

-- Revision Ready
INSERT INTO @tblRevisionReady
	SELECT WBS1
	, MAX( CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET,  ActionDate), DATENAME(TZOFFSET, SYSDATETIMEOFFSET()))) ) AS ActionDate
	FROM [dbo].[CCG_EI_History]
	WHERE ActionTaken = 'Stage Change'
	AND InvoiceStage = 'Revision Ready'
	GROUP BY WBS1

-- Draft Revision Sent field
INSERT INTO @tblRevisionSent
	SELECT A.WBS1
	, B.ActionDate 
	FROM @tblReviseAndReturn A
	INNER JOIN @tblRevisionReady B
	ON B.WBS1 = A.WBS1
	AND B.ActionDate >= A.ActionDate

-- Draft Revision Returned field
INSERT INTO @tblRevisionReturned
	SELECT A.WBS1
	, MAX(B.ActionDate) AS ActionDate
	FROM @tblRevisionSent A
	INNER JOIN
	(
		SELECT WBS1
		,CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET,  ActionDate), DATENAME(TZOFFSET, SYSDATETIMEOFFSET()))) AS ActionDate
		FROM [dbo].[CCG_EI_History]
		WHERE ActionTaken = 'Stage Change'
		AND InvoiceStage IN 
		(
			'Invoice As Is'
			,'Invoice As Noted'
			,'Revise and Return'
			,'Hold'
			,'Write-Off'
		)
	) AS B
	ON B.WBS1 = A.WBS1
	WHERE B.ActionDate >= A.ActionDate
	GROUP BY A.WBS1

-- Final Sent field
INSERT INTO @tblFinalSent
	SELECT A.WBS1
	, MAX(B.ActionDate) AS ActionDate 
	FROM @tblDraftReturned A
	INNER JOIN
	(
		SELECT WBS1
		,CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET,  ActionDate), DATENAME(TZOFFSET, SYSDATETIMEOFFSET()))) AS ActionDate
		FROM [dbo].[CCG_EI_History]
		WHERE ActionTaken = 'Stage Change'
		AND InvoiceStage = 'Final Invoice Sent'
	) AS B
	ON B.WBS1 = A.WBS1
	AND B.ActionDate >= A.ActionDate
	GROUP BY A.WBS1

-- Composite table with all results
INSERT INTO @tblComposite
(
	Biller
    , Email
	, LastName
	, FirstName
	, Org
	, Reviewer
	, WBS1
	, [Name]
	, InvoiceStage
	, DraftGoal
	, DraftSent
	, DraftReturned
	, ReviseAndReturn
	, RevisionReady
	, RevisionSent
	, RevisionReturned
	, FinalSent
	, FinalGoal
	, Notes
)
	SELECT A.Biller
	, A.EMail
	, A.LastName
	, A.FirstName
	, A.Org
	, A.Reviewer
	, A.WBS1
	, A.[Name]
	, B.InvoiceStage
	, A.CustDraftGoal
	, D.ActionDate AS DraftSent
	, E.ActionDate AS DraftReturned
	, F.ActionDate AS ReviseAndReturn
	, G.ActionDate AS RevisionReady
	, H.ActionDate AS RevisionSent
	, I.ActionDate AS RevisionReturned
	, J.ActionDate AS FinalSent 
	, A.CustFinalGoal
	, CASE WHEN B.InvoiceStage IN ('Hold', 'Write-Off', 'No Invoice') THEN C.Comments ELSE C.CA_Notes END AS Notes
	FROM
	(
		SELECT ISNULL (p.Biller, '001975') AS Biller
		, e1.EMail
		, e1.LastName
		, e1.FirstName
		--DEBUG-CH Modified line below as per Joseph's comments on 20191010
		, RIGHT(p.Org, (SELECT Org3Length from CFGFormat)) AS Org
		--, RIGHT(p.Org, 3) AS Org
		, e2.LastName Reviewer
		, p.WBS1
		, p.[Name]
		, pCustom.CreateDate
		, RIGHT(pCustom.CustBillingWeek, 1) AS CustBillingWeek
		, tbl.CustDraftGoal
		, tbl.CustFinalGoal
		FROM PR p, ProjectCustomTabFields pCustom, EMMain e1, EMMain e2, [dbo].[UDIC_Accounting] tbl
		WHERE p.WBS2 = ' '												--- project level
		AND pCustom.WBS2 = ' '											--- project level
		AND p.WBS1 NOT LIKE 'Z%'
		AND p.ChargeType = 'R'											--- Chargeable
		AND p.[Status] = 'A'											--- Active
		AND p.Biller = e1.Employee										--- CA
		AND ISNULL(pCustom.CustInvoiceReviewer, '001975') = e2.Employee	--- Reviewer
		AND p.WBS1 = pCustom.WBS1
		AND pCustom.CustBillingWeek IS NOT NULL							--- MUST have the billing week non-null
		AND tbl.CustWeek = RIGHT(pCustom.CustBillingWeek, 1)			--- 'Week B' --> 'B'
		AND tbl.CustDraftGoal > pCustom.CreateDate						--- Exclude projects created AFTER the draft goal date
		AND tbl.CustDraftGoal >= DATEADD(dd, @past, @Now)				--- (today - @past days) <= date <= (today + @future days)
		AND	tbl.CustDraftGoal <= DATEADD(dd, @future, @Now)
				
		--OR
		--(
		--(SELECT MAX(ActionDate) AS ActionDate
		--FROM [dbo].[CCG_EI_History]
		--WHERE ActionTaken = 'Stage Change'
		--  AND WBS1 = p.WBS1
		--GROUP BY WBS1) >= DATEADD(dd, @past, @Now)
		--)
		
	) AS A
	LEFT JOIN 
	(
		SELECT B.WBS1
		, B.ActionDate
		, B.InvoiceStage 
		FROM 
		(
			SELECT WBS1
			, MAX(ActionDate) AS ActionDate		
			FROM [dbo].[CCG_EI_History]
			WHERE ActionTaken = 'Stage Change'
			GROUP BY WBS1
		) AS A
		INNER JOIN								
		(
			SELECT WBS1
			, ActionDate
			, InvoiceStage
			FROM [dbo].[CCG_EI_History]
			WHERE ActionTaken = 'Stage Change'
		) AS B
		ON B.WBS1 = A.WBS1
		AND B.ActionDate = A.ActionDate
	) AS B ON B.WBS1 = A.WBS1								-- Invoice Stage
	LEFT JOIN 
	(
		SELECT WBS1
		, CASE 
			WHEN Comments IS NOT NULL AND LEN(Comments) > 50 THEN LEFT(Comments, 50) + '...'
			ELSE Comments
			END AS Comments
		, CASE 
			WHEN CA_Notes IS NOT NULL AND LEN(CA_Notes) > 50 THEN LEFT(CA_Notes, 50) + '...'
			ELSE CA_Notes
			END AS CA_Notes
		FROM [dbo].[CCG_EI_CustomColumns]
		WHERE WBS2 = ' '	-- project level
		AND WBS3 = ' '
		-- exclude rows where both Comments and CA_Notes are NULL or empty
		AND NOT	
		(	
			(Comments IS NULL OR Comments = '') 
			AND (CA_Notes IS NULL OR CA_Notes = '') 
		)
	) AS C ON C.WBS1 = A.WBS1								-- Notes
	LEFT JOIN @tblDraftSent D ON D.WBS1 = A.WBS1			-- Draft Sent
	LEFT JOIN @tblDraftReturned E ON E.WBS1 = A.WBS1		-- Draft Returned
	LEFT JOIN @tblReviseAndReturn AS F ON F.WBS1 = A.WBS1	-- Revise and Return
	LEFT JOIN @tblRevisionReady AS G ON G.WBS1 = A.WBS1		-- Revision Ready
	LEFT JOIN @tblRevisionSent AS H ON H.WBS1 = A.WBS1		-- Revision Sent
	LEFT JOIN @tblRevisionReturned AS I ON I.WBS1 = A.WBS1	-- Revision Returned
	LEFT JOIN @tblFinalSent AS J ON J.WBS1 = A.WBS1			-- Final Sent
	ORDER BY A.LastName, A.FirstName, A.CustFinalGoal, A.Org, A.Reviewer, A.WBS1

-- TO DEBUG -- 
--SELECT * FROM @tblComposite ORDER BY LastName, FirstName

	DECLARE curBiller INSENSITIVE CURSOR FOR
		SELECT DISTINCT Biller, EMail, LastName, FirstName
		FROM @tblComposite 
		ORDER BY LastName, FirstName

	OPEN curBiller
		FETCH NEXT FROM curBiller INTO @Biller, @Email, @LastName, @FirstName
		Set @Loop1 = @@FETCH_STATUS
		While @Loop1 = 0

		BEGIN
			DECLARE @lastStatus integer = -1
			DECLARE @Body varchar(max) = ''

			DECLARE curProject INSENSITIVE CURSOR FOR
				SELECT * 
				FROM
				(
					SELECT 0 AS StatusOrder, 'In Progress' AS [Status], Org, Reviewer, WBS1, [Name], InvoiceStage, DraftGoal, DraftSent, DraftReturned, ReviseAndReturn, RevisionReady, RevisionSent, FinalSent, FinalGoal, Notes
					FROM @tblComposite  
					WHERE Biller = @Biller
					AND 
					(	
						FinalSent IS NULL
						AND InvoiceStage NOT IN ('Hold', 'Write-Off', 'No Invoice')
					)
					UNION ALL
					SELECT 1 AS StatusOrder, 'Completed' AS [Status], Org, Reviewer, WBS1, [Name], InvoiceStage, DraftGoal, DraftSent, DraftReturned, ReviseAndReturn, RevisionReady, RevisionSent, FinalSent, FinalGoal, Notes
					FROM @tblComposite  
					WHERE Biller = @Biller
					AND (
						FinalSent IS NOT NULL 
						OR InvoiceStage IN ('Hold', 'Write-Off', 'No Invoice')
					)
				) AS alpha
				WHERE alpha.Status = @statusType
				ORDER BY [StatusOrder], FinalGoal, Org, Reviewer, WBS1

			OPEN curProject
				FETCH NEXT FROM curProject INTO @StatusOrder, @Status, @Org, @Reviewer, @WBS1, @Name, @InvoiceStage, @DraftGoal, @DraftSent, @DraftReturned, @ReviseAndReturn, @RevisionReady, @RevisionSent, @FinalSent, @FinalGoal, @Notes 
				Set @Loop2 = @@FETCH_STATUS
				While @Loop2 = 0
				BEGIN

					DECLARE @txt2 nvarchar(50) = ''
							, @bckgrnd6 nvarchar(50) = ''
							, @bckgrnd7 nvarchar(50) = ''
							, @bckgrnd8 nvarchar(50) = ''
							, @bckgrnd9 nvarchar(50) = ''
							, @bckgrnd10 nvarchar(50) = ''

					-- Date comparisons here should yield negative numbers if it is before the milestone due date, positive numbers for late
					DECLARE @dCol6 int = DATEDIFF(day, @DraftGoal, ISNULL(@DraftSent, @Now))
					DECLARE @dCol7 int = DATEDIFF(day, DATEADD(dd, 5, ISNULL(@DraftSent, @Now)), ISNULL(@DraftReturned, @Now))
					DECLARE @dCol8 int = DATEDIFF(day, DATEADD(dd, 2, ISNULL(@DraftReturned, @Now)), ISNULL(@RevisionSent, @Now))
					DECLARE @dCol9 int = DATEDIFF(day, DATEADD(dd, 1, ISNULL(@RevisionSent, @Now)), ISNULL(@RevisionReturned, @Now))
					DECLARE @dCol10 int = DATEDIFF(day, ISNULL(@FinalGoal, @Now), ISNULL(@FinalSent, @Now))

					-- Red text IFF (Draft Returned is null and Today > Draft Sent + 5) OR (Revision Sent is null AND today > Revision Sent + 1)
					IF ((@DraftReturned IS NULL AND @Now > DATEADD(dd, 5, ISNULL(@DraftSent, @Now))) OR (@RevisionSent IS NULL AND @Now > DATEADD(dd, 1, ISNULL(@RevisionSent, @Now))) )
						SET @txt2 = @redText

					SELECT @bckgrnd6  = (CASE 
											WHEN @dCol6 <= 0 THEN '' 
											WHEN @dCol6 >= 1 AND @dCol6 <= 2 THEN @yellow 
											WHEN @dCol6 >= 3 AND @dCol6 <= 4 THEN @orange
											WHEN @dCol6 >= 5 AND @dCol6 <= 6 THEN @dkorange
											WHEN @dCol6 >= 7 AND @dCol6 <= 9 THEN @red
											WHEN @dCol6 >= 10				 THEN @purple END)

					SELECT @bckgrnd7  = (CASE 
											WHEN @dCol7 <= 0 THEN '' 
											WHEN @dCol7 >= 1 AND @dCol7 <= 2 THEN @yellow 
											WHEN @dCol7 >= 3 AND @dCol7 <= 4 THEN @orange
											WHEN @dCol7 >= 5 AND @dCol7 <= 6 THEN @dkorange
											WHEN @dCol7 >= 7 AND @dCol7 <= 9 THEN @red
											WHEN @dCol7 >= 10				 THEN @purple END)	

					IF (@ReviseAndReturn > @DraftSent)
						BEGIN
							SELECT @bckgrnd8  = (CASE 
													WHEN @dCol8 <= 0 THEN '' 
													WHEN @dCol8 >= 1 AND @dCol8 <= 2 THEN @yellow 
													WHEN @dCol8 >= 3 AND @dCol8 <= 4 THEN @orange
													WHEN @dCol8 >= 5 AND @dCol8 <= 6 THEN @dkorange
													WHEN @dCol8 >= 7 AND @dCol8 <= 9 THEN @red
													WHEN @dCol8 >= 10				 THEN @purple END)	

							SELECT @bckgrnd9  = (CASE 
													WHEN @dCol9 <= 0 THEN '' 
													WHEN @dCol9 >= 1 AND @dCol9 <= 2 THEN @yellow 
													WHEN @dCol9 >= 3 AND @dCol9 <= 4 THEN @orange
													WHEN @dCol9 >= 5 AND @dCol9 <= 6 THEN @dkorange
													WHEN @dCol9 >= 7 AND @dCol9 <= 9 THEN @red
													WHEN @dCol9 >= 10				 THEN @purple END)
						END
					ELSE
						BEGIN
							SET @bckgrnd8 = @gray
							SET @bckgrnd9 = @gray
						END

					SELECT @bckgrnd10  = (CASE 
											WHEN @dCol10 <= 0 THEN '' 
											WHEN @dCol10 >= 1 AND @dCol10 <= 2 THEN @yellow 
											WHEN @dCol10 >= 3 AND @dCol10 <= 4 THEN @orange
											WHEN @dCol10 >= 5 AND @dCol10 <= 6 THEN @dkorange
											WHEN @dCol10 >= 7 AND @dCol10 <= 9 THEN @red
											WHEN @dCol10 >= 10				   THEN @purple END)	
					---------------------------------------------------------------------

					IF (@StatusOrder != @lastStatus)
						BEGIN
							IF (@lastStatus != -1)	-- If not the first table,
								BEGIN
									-- Close the table
									Set @Body = @Body + 
'	</tbody>
</table>' + @NL
								END

							-- Open the table with headers
							Set @Body = @Body + 
								'<p><strong>' + IsNull(@FirstName, '') + ' ' + IsNull(@LastName, '') + (CASE WHEN @StatusOrder = 0 THEN ' - In Progress' ELSE ' - Completed' END) + '</strong></p>' + @NL + 
								'<table cellpadding=''5'' cellspacing=''0'' style=''border:1px solid black;border-collapse:collapse;''>' + @NL + 
									@TableHeader +	@NL + 
								'	<tbody>' + @NL
						END
					---------------------------------------------------------------------

					Set @Body = @Body + 
'		<tr>
			<td class="c1">' + ISNULL(@Org, ' ') + '</td>
			<td class="c2"' + @txt2 + '>' + ISNULL(@Reviewer, ' ') + '</td>
			<td class="c3">' + ISNULL(@WBS1, ' ') + '</td>
			<td class="c4">' + ISNULL(@Name, ' ') + '</td>
			<td class="c5">' + CASE WHEN @DraftGoal IS NOT NULL THEN REPLACE(RIGHT(CONVERT(VARCHAR(6), @DraftGoal, 107), 6), ' ', '-') ELSE ' ' END + '</td>' + @NL
				
					IF (@InvoiceStage = 'No Invoice')	
						BEGIN	-- {NO INVOICE} invoice stage col width = 5
							Set @Body = @Body +
'			<td class="c6" colspan="5" style="text-align:center;">' + @InvoiceStage + '</td>' + @NL
						END

					ELSE IF (@InvoiceStage = 'Hold'		
							OR @InvoiceStage = 'Write-Off')
						BEGIN	-- {HOLD, WRITE-OFF} Draft Sent, Draft Received, invoice stage col width = 3
							Set @Body = @Body +
'			<td class="c6"' + @bckgrnd6 + '>' + CASE WHEN @DraftSent IS NOT NULL THEN REPLACE(RIGHT(CONVERT(VARCHAR(6), @DraftSent, 107), 6), ' ', '-') ELSE ' ' END + '</td>
			<td class="c7"' + @bckgrnd7 + '>' + CASE WHEN @DraftReturned IS NOT NULL THEN REPLACE(RIGHT(CONVERT(VARCHAR(6), @DraftReturned, 107), 6), ' ', '-') ELSE ' ' END + '</td> 
			<td class="c8" colspan="3" style="text-align:center;">' + @InvoiceStage + '</td>' + @NL
						END

					ELSE
						BEGIN -- DEFAULT
							Set @Body = @Body +
'			<td class="c6"' + @bckgrnd6 + '>' + CASE WHEN @DraftSent IS NOT NULL THEN REPLACE(RIGHT(CONVERT(VARCHAR(6), @DraftSent, 107), 6), ' ', '-') ELSE ' ' END + '</td>
			<td class="c7"' + @bckgrnd7 + '>' + CASE WHEN @DraftReturned IS NOT NULL THEN REPLACE(RIGHT(CONVERT(VARCHAR(6), @DraftReturned, 107), 6), ' ', '-') ELSE ' ' END + '</td>
			<td class="c8"' + @bckgrnd8 + '>' + CASE WHEN @RevisionSent IS NOT NULL THEN REPLACE(RIGHT(CONVERT(VARCHAR(6), @RevisionSent, 107), 6), ' ', '-') ELSE ' ' END + '</td>
			<td class="c9"' + @bckgrnd9 + '>' + CASE WHEN @RevisionReturned IS NOT NULL THEN REPLACE(RIGHT(CONVERT(VARCHAR(6), @RevisionReturned, 107), 6), ' ', '-') ELSE ' ' END + '</td>
			<td class="c10"' + @bckgrnd10 + '>' + CASE WHEN @FinalSent IS NOT NULL THEN REPLACE(RIGHT(CONVERT(VARCHAR(6), @FinalSent, 107), 6), ' ', '-') ELSE ' ' END + '</td>' + @NL
						END

				Set @Body = @Body +
'			<td class="c11">' + CASE WHEN @FinalGoal IS NOT NULL THEN REPLACE(RIGHT(CONVERT(VARCHAR(6), @FinalGoal, 107), 6), ' ', '-') ELSE ' ' END + '</td>
			<td class="c12">' + ISNULL(@Notes, '') + '</td>
		</tr>' + @NL


				SET @lastStatus = @StatusOrder

				-- Next project row
				FETCH NEXT FROM curProject INTO @StatusOrder, @Status, @Org, @Reviewer, @WBS1, @Name, @InvoiceStage, @DraftGoal, @DraftSent, @DraftReturned, @ReviseAndReturn, @RevisionReady, @RevisionSent, @FinalSent, @FinalGoal, @Notes
				SET @Loop2 = @@FETCH_STATUS
			END

		CLOSE curProject
		DEALLOCATE curProject

		-- Close the table
		Set @Body = @Body + 
'	</tbody>
</table>' + @NL

		-- Queue email
		Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, BCCList, Subject, Body, CreateDateTime, MaxDelay)
		--Values ('Invoice Status', 'kcarter@halff.com', 'jsagel@halff.com', 'jsagel@halff.com', 'jsagel@halff.com', 'Invoice Status ' + @statusType + ' - ' + @FirstName + ' ' + @LastName, @CSS + @NL + @Body, getDate(), 1)
		Values ('Invoice Status', 'jkubik@halff.com', @Email, 'jkubik@halff.com', 'kcarter@halff.com; rchampaign@halff.com', 'Invoice Status ' + @statusType + ' - ' + @FirstName + ' ' + @LastName, @CSS + @NL + @Body, getDate(), 1)

		FETCH NEXT FROM curBiller INTO @Biller, @Email, @LastName, @FirstName
		SET @Loop1 = @@FETCH_STATUS
	END
CLOSE curBiller
DEALLOCATE curBiller

END

GO
