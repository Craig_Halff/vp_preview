SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Stamp_Load]
	@VISION_LANGUAGE	varchar(10)
AS
BEGIN
	-- [DeltekStoredProc_CCG_PAT_Stamp_Load] 'en-US'
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT T1.Seq, IsNull(IsNull(T2.[Label], T3.[Label]), IsNull(T1.[Label], '')) as Label, StampType, DisplayOrder,
            IsNull(IsNull(T2.Content, T3.Content), IsNull(T1.Content, '')) as Content,
            PositionType, PageSet, Width, ValidRoles, Status, ModDate, ModUser
        FROM CCG_PAT_ConfigStamps T1
            LEFT JOIN CCG_PAT_ConfigStampsDescriptions T2 on T2.Seq = T1.Seq and T2.UICultureName = @VISION_LANGUAGE
            LEFT JOIN CCG_PAT_ConfigStampsDescriptions T3 on T3.Seq = T1.Seq and T3.UICultureName = 'en-US'
        ORDER BY T1.DisplayOrder;
END;
GO
