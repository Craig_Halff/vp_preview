SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[QBClearAll]
AS
BEGIN
    /*clear all QBO tables*/
    delete from ChangeDataCapture where Integration = 'QBO'
    delete from QBOTransRef
    delete from QBOAccount
    delete from QBODefaultAccountMapping
    delete from QBOTaxCode
    delete from QBOTaxCodePurchaseTaxRate
    delete from QBOTaxCodeSalesTaxRate

    update CL set QBOId = null, QBOLastUpdated = null
    update CLAddress set QBOID = null, QBOAddressID = null, QBOIsBillingAddr = 'N', QBOIsShippingAddr = 'N', QBOLastUpdated = null
    update Contacts set QBOID = null, QBOIsMainContact = 'N', QBOLastUpdated = null
    update EMMain set QBOID = null, QBOVendorID = null, QBOAddressID = null, QBOLastUpdated = null
    update CA set QBOAccountID = null
    update CFGBillTaxesData set QBOID = null, QBOLastUpdated = null

    delete from CFGQuickBooks 
END
GO
