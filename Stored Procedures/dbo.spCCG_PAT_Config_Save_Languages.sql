SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_Config_Save_Languages]
	@fieldsSQL			Nvarchar(max),
	@updateValuesSql	Nvarchar(max),
	@insertValuesSql	Nvarchar(max),
	@deleteSql			Nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	int;

	BEGIN TRANSACTION;
	BEGIN TRY
		-- Updates
		IF ISNULL(@updateValuesSql, N'') <> N'' BEGIN
			SET @sSQL = N'
				UPDATE lang
					SET Label = vals.Label
					FROM CCG_Language_Labels lang
						INNER JOIN (
							VALUES ' + @updateValuesSql + '
						) vals (Label, Id, Product, UICultureName) On lang.Id = vals.Id
							AND lang.Product = vals.Product
							AND lang.UICultureName = vals.UICultureName';
			EXEC (@sSQL);
		END;

		-- Inserts
		IF ISNULL(@insertValuesSql, N'') <> N'' BEGIN
			SET @sSQL = N'
				INSERT INTO CCG_Language_Labels
					(' + @fieldsSQL + ')
					VALUES
					' + @insertValuesSql;
			--PRINT @sSQL;
			EXEC (@sSQL);
		END;

		-- Deletes
		IF ISNULL(@deleteSql, N'') <> N'' BEGIN
			SET @sSQL = N'
				DELETE lang
					FROM CCG_Language_Labels lang
						INNER JOIN (
							VALUES ' + @deleteSql + '
						) vals (Label, Id, Product, UICultureName) On lang.Id = vals.Id
							AND lang.Product = vals.Product
							AND lang.UICultureName = vals.UICultureName';
			--PRINT @sSQL;
			EXEC (@sSQL);
		END;
	END TRY
	BEGIN CATCH
		SELECT
			ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity,
			ERROR_STATE() AS ErrorState, ERROR_PROCEDURE() AS ErrorProcedure,
			ERROR_LINE() AS ErrorLine, ERROR_MESSAGE() AS ErrorMessage;

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;
	END CATCH;

	IF @@TRANCOUNT > 0 COMMIT TRANSACTION;
END;
GO
