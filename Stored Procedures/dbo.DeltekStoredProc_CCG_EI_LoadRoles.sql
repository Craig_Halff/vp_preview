SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_LoadRoles] ( @visionLanguage varchar(10))
             AS EXEC spCCG_EI_LoadRoles @visionLanguage
GO
