SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_KeyConvert_GetSessionDetails] (@SessionSeq varchar(max), @entityType varchar(max))
AS BEGIN
	-- EXEC [spCCG_KeyConvert_GetSessionDetails] '', ''
	if isnull(@SessionSeq,'') = ''
		select top 500 case w.Entity
				when 'WBS1' then 'Project'
				when 'WBS2' then 'Phase'
				when 'WBS3' then 'Task'
				else w.Entity end as Entity, w.NewKey, w.NewKey2, w.NewKey3, w.OldKey, w.OldKey2, w.OldKey3, w.PKey, NULL as CreateDate,
				'' as Notes
			from KeyConvertWork w
			where w.PKey not in (select l.PKey from CCG_KeyConvertLog l where l.Entity = w.Entity and isnull(l.Notes,'') = '')
				and (Entity = @entityType or isnull(@entityType,'') = '')
		order by w.Entity
	else
		select top 500 case l.Entity
				when 'WBS1' then 'Project'
				when 'WBS2' then 'Phase'
				when 'WBS3' then 'Task'
				else l.Entity end as Entity, l.NewKey, l.NewKey2, l.NewKey3, l.OldKey, l.OldKey2, l.OldKey3, l.PKey, l.CreateDate, l.Notes
			from CCG_KeyConvertSessions s
				inner join CCG_KeyConvertLog l on l.SessionSeq = s.Seq
			where (Entity = @entityType or isnull(@entityType,'') = '')
				and s.Seq = @SessionSeq
		order by l.Entity
END
GO
