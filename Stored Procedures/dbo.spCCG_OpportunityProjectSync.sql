SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_OpportunityProjectSync] @OpportunityID varchar (32)
AS
/*
Copyright (c) 2018 Central Consulting Group. All rights reserved.
03/05/2018 David Springer
           Update Project with changes from Opportunity
           Call this on an Opportunity change workflow
10/04/2020 Craig H. Anderson
           Added code to also copy CustOperationsManager to ProjectCustomTabFields
07/13/2021 Craig H. Anderson
			Added code to also handle CustProjectType and CustService
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
BEGIN

	Update p
	Set p.Name = Left (o.Name, 40),
		p.LongName = o.Name,
		p.ClientID = o.ClientID,
		p.ContactID = o.ContactID,
		p.BillingClientID = ox.CustBillingCompany,
		p.BillingContactID = ox.CustBillingContact,
		--p.ProjectType = o.OpportunityType,
		p.Responsibility = o.OurRole,
		p.ProjMgr = o.ProjMgr,
		p.Supervisor = o.Supervisor,
		p.Biller = ox.CustBiller,
		p.StartDate = o.EstStartDate,
		p.EstCompletionDate = o.EstCompletionDate
	From PR p, Opportunity o WITH (NOLOCK), OpportunityCustomTabFields ox WITH (NOLOCK)
	Where o.OpportunityID = @OpportunityID
	  and o.OpportunityID = ox.OpportunityID
	  and o.PRProposalWBS1 = p.WBS1
	  and p.WBS2 = ' '


	Update px
	Set px.CustPracticeArea = ox.CustPracticeArea
	, px.CustOperationsManager = ox.CustOperationsManager
	, px.CustProjectType = ox.CustProjectType
	, px.CustService = ox.CustService
	From PR p WITH (NOLOCK), ProjectCustomTabFields px, Opportunity o WITH (NOLOCK), OpportunityCustomTabFields ox WITH (NOLOCK)
	Where o.OpportunityID = @OpportunityID
	  and o.OpportunityID = ox.OpportunityID
	  and o.PRProposalWBS1 = p.WBS1
	  and p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.WBS2 = ' '

END
GO
