SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteSF255] @SF255ID varchar(32)
AS
    delete from SF255Consultant where SF255ID = @SF255ID
    delete from SF255Disciplines where SF255ID = @SF255ID
    delete from SF255Employees where SF255ID = @SF255ID
    delete from SF255LocationsCount where SF255ID = @SF255ID
    delete from SF255Projects where SF255ID = @SF255ID
GO
