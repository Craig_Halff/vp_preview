SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[UpdateContractDetailSubTotals]
@WBS1	Nvarchar(30) = ''
As
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
begin
--Run this script only if "At Lowest Level Only (System will automatically Update Higher Levels)" is turned on
IF EXISTS(SELECT AutoSumComp
          FROM   FW_CFGSystem
          WHERE  AutoSumComp = 'Y')
  BEGIN 
      --Add missing phases
      INSERT INTO ContractDetails
                  (ContractNumber,
                   WBS1,
                   WBS2,
                   WBS3)
      SELECT DISTINCT Task.ContractNumber,
                      Task.WBS1,
                      Task.WBS2,
                      ' ' WBS3
      FROM   ContractDetails Task
             LEFT OUTER JOIN ContractDetails phase
                          ON task.ContractNumber = phase.ContractNumber
                             AND task.WBS1 = phase.WBS1
                             AND task.WBS2 = phase.WBS2
                             AND phase.WBS3 = ' '
      WHERE  (ISNULL(@WBS1, '') = '' OR Task.WBS1 = @WBS1)
			 AND Task.WBS3 <> ' '
             AND phase.WBS1 IS NULL
             AND NOT EXISTS (SELECT 'X'
                             FROM   ContractDetails
                             WHERE  ContractDetails.ContractNumber = Task.ContractNumber
                                    AND task.WBS1 = ContractDetails.WBS1
                                    AND task.WBS2 = ContractDetails.WBS2
                                    AND ContractDetails.WBS3 = ' ')

      --Update phase totals
      UPDATE ContractDetails
      SET    Fee = cd2.Fee,
             ConsultFee = cd2.ConsultFee,
             ReimbAllow = cd2.ReimbAllow,
             FeeBillingCurrency = cd2.FeeBillingCurrency,
             ConsultFeeBillingCurrency = cd2.ConsultFeeBillingCurrency,
             ReimbAllowBillingCurrency = cd2.ReimbAllowBillingCurrency,
             FeeFunctionalCurrency = cd2.FeeFunctionalCurrency,
             ConsultFeeFunctionalCurrency = cd2.ConsultFeeFunctionalCurrency,
             ReimbAllowFunctionalCurrency = cd2.ReimbAllowFunctionalCurrency,
             FeeDirLab = cd2.FeeDirLab,
             FeeDirExp = cd2.FeeDirExp,
             ReimbAllowExp = cd2.ReimbAllowExp,
             ReimbAllowCons = cd2.ReimbAllowCons,
             FeeDirLabBillingCurrency = cd2.FeeDirLabBillingCurrency,
             FeeDirExpBillingCurrency = cd2.FeeDirExpBillingCurrency,
             ReimbAllowExpBillingCurrency = cd2.ReimbAllowExpBillingCurrency,
             ReimbAllowConsBillingCurrency = cd2.ReimbAllowConsBillingCurrency,
             FeeDirLabFunctionalCurrency = cd2.FeeDirLabFunctionalCurrency,
             FeeDirExpFunctionalCurrency = cd2.FeeDirExpFunctionalCurrency,
             ReimbAllowExpFunctionalCurrency = cd2.ReimbAllowExpFunctionalCurrency,
             ReimbAllowConsFunctionalCurrency = cd2.ReimbAllowConsFunctionalCurrency,
             Total = cd2.Total,
             TotalBillingCurrency = cd2.TotalBillingCurrency,
             TotalFunctionalCurrency = cd2.TotalFunctionalCurrency
      FROM   (SELECT ContractNumber,
                     WBS1,
                     WBS2,
                     Isnull(Sum(cd2.Fee), 0)                              Fee,
                     Isnull(Sum(cd2.ConsultFee), 0)                       ConsultFee,
                     Isnull(Sum(cd2.ReimbAllow), 0)                       ReimbAllow,
                     Isnull(Sum(cd2.FeeBillingCurrency), 0)               FeeBillingCurrency,
                     Isnull(Sum(cd2.ConsultFeeBillingCurrency), 0)        ConsultFeeBillingCurrency,
                     Isnull(Sum(cd2.ReimbAllowBillingCurrency), 0)        ReimbAllowBillingCurrency,
                     Isnull(Sum(cd2.FeeFunctionalCurrency), 0)            FeeFunctionalCurrency,
                     Isnull(Sum(cd2.ConsultFeeFunctionalCurrency), 0)     ConsultFeeFunctionalCurrency,
                     Isnull(Sum(cd2.ReimbAllowFunctionalCurrency), 0)     ReimbAllowFunctionalCurrency,
                     Isnull(Sum(cd2.FeeDirLab), 0)                        FeeDirLab,
                     Isnull(Sum(cd2.FeeDirExp), 0)                        FeeDirExp,
                     Isnull(Sum(cd2.ReimbAllowExp), 0)                    ReimbAllowExp,
                     Isnull(Sum(cd2.ReimbAllowCons), 0)                   ReimbAllowCons,
                     Isnull(Sum(cd2.FeeDirLabBillingCurrency), 0)         FeeDirLabBillingCurrency,
                     Isnull(Sum(cd2.FeeDirExpBillingCurrency), 0)         FeeDirExpBillingCurrency,
                     Isnull(Sum(cd2.ReimbAllowExpBillingCurrency), 0)     ReimbAllowExpBillingCurrency,
                     Isnull(Sum(cd2.ReimbAllowConsBillingCurrency), 0)    ReimbAllowConsBillingCurrency,
                     Isnull(Sum(cd2.FeeDirLabFunctionalCurrency), 0)      FeeDirLabFunctionalCurrency,
                     Isnull(Sum(cd2.FeeDirExpFunctionalCurrency), 0)      FeeDirExpFunctionalCurrency,
                     Isnull(Sum(cd2.ReimbAllowExpFunctionalCurrency), 0)  ReimbAllowExpFunctionalCurrency,
                     Isnull(Sum(cd2.ReimbAllowConsFunctionalCurrency), 0) ReimbAllowConsFunctionalCurrency,
                     Isnull(Sum(cd2.Total), 0)                            Total,
                     Isnull(Sum(cd2.TotalBillingCurrency), 0)             TotalBillingCurrency,
                     Isnull(Sum(cd2.TotalFunctionalCurrency), 0)          TotalFunctionalCurrency
              FROM   ContractDetails cd2
              WHERE  (ISNULL(@WBS1, '') = '' OR cd2.WBS1 = @WBS1) AND cd2.WBS3 <> N' ' 
              GROUP  BY ContractNumber,
                        WBS1,
                        WBS2) cd2
      WHERE  ContractDetails.WBS1 = cd2.WBS1
             AND ContractDetails.WBS2 = cd2.WBS2
             AND ContractDetails.WBS3 = N' '
             AND ContractDetails.ContractNumber = cd2.ContractNumber
  END

END
/* sp_UpdatePRFees.SQL */
/* Stored procedure to refresh the fee, consultfee and reimballow fields for parent records */
/* SSR (04/13/04): Added the ability to run this SP for Project Templates. */
/* James Cheng/Charles Gilbert 09/05/13: Removed duplicate subqueries to improve performance */
/* WCG (09/13/13): Changed LIKEs to =, added Read Uncommitted to prevent deadlocking */
/* Derong Wang (01/16/14): Add columns for compensation breakout */
/* Dennis Perng (10/17/14): If @project is '%', remove WBS1 where clause. */
/* Dennis Perng (11/04/14): Fix defect. */
/* Last Modified : Dennis Perng 11/04/14 */

Print ' '
Print 'Drop UpdatePRFees procedure (if it exists)...'
GO
