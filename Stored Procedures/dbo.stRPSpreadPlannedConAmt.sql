SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPSpreadPlannedConAmt]
  @strRowID nvarchar(255),
  @strScopeStartDate varchar(8),
  @strScopeEndDate varchar(8),
  @decAmt decimal(19,4), 
  @strAmtType varchar(1), -- C = Cost, B = Bill
  @bitCalledFromRM bit
AS

BEGIN -- Procedure stRPSpreadPlannedConAmt

  SET NOCOUNT ON

  DECLARE @_SectionSign nchar = NCHAR(167) -- N'§'
  DECLARE @_AccountVendorSign nchar = NCHAR(10132) -- N'➔'

  DECLARE @dtScopeStartDate datetime
  DECLARE @dtScopeEndDate datetime
  DECLARE @dtMinTPD datetime
  DECLARE @dtMaxTPD datetime
  DECLARE @dtScopeBeforeDate datetime
  DECLARE @dtScopeAfterDate datetime
  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime

  DECLARE @intOutlineLevel int
   
  DECLARE @strCompany nvarchar(14)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strAccount nvarchar(26)
  DECLARE @strVendor nvarchar(40)
  DECLARE @strIDPrefix nvarchar(255)
  DECLARE @strUserName nvarchar(32)

  DECLARE @decScopeTotalAmt decimal(19,4) = 0
  DECLARE @decScopeTotalWD decimal(19,4) = 0
  DECLARE @decBeforeWD decimal(19,4) = 0
  DECLARE @decAfterWD decimal(19,4) = 0
  DECLARE @decDeltaAmt decimal(19,4) = 0
  
  DECLARE @intAccountVendorIndex int
  DECLARE @intRowIDSeperatorIndex int

  DECLARE @siAmtDecimals smallint
  DECLARE @bitIsOnLeafTask as bit = 0
  DECLARE @bitCalculateBilling as bit = 0

   
  -- Declare Temp tables.
  
  DECLARE @tabCalendar TABLE (
    StartDate	datetime,
    EndDate	datetime
    UNIQUE(StartDate, EndDate)
  )

  DECLARE @tabConsultant TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    ConsultantID varchar(32) COLLATE database_default,
    Account nvarchar(26) COLLATE database_default,
    Vendor nvarchar(40) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    ScopeWD decimal(19,4),
    CT_OutlineNumber varchar(255) COLLATE database_default,
    CT_OutlineLevel int
    UNIQUE(PlanID, TaskID, ConsultantID)
  )

  DECLARE @tabTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    Name nvarchar(255) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int
    UNIQUE(PlanID, TaskID, OutlineNumber)
  )

  DECLARE @tabTPD TABLE(
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    ConsultantID varchar(32) COLLATE database_default,
    StartDate datetime, 
    EndDate datetime, 
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, ConsultantID, StartDate, EndDate)
  ) 

  DECLARE @tabS1TPD TABLE (
    RowID varchar(32) COLLATE database_default,
    TimePhaseID varchar(32) COLLATE database_default,
    CIStartDate datetime, 
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    ConsultantID varchar(32) COLLATE database_default,
    StartDate datetime, 
    EndDate datetime, 
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4)
    UNIQUE(RowID, PlanID, TaskID, ConsultantID, StartDate, EndDate)
  )

  DECLARE @tabSpreadTPD TABLE (
    RowSeq bigint,
    MAXSeq bigint,
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    ConsultantID varchar(32) COLLATE database_default,
    Account nvarchar(26) COLLATE database_default,
    StartDate datetime, 
    EndDate datetime, 
    Dividend decimal(19,4),
    Divisor decimal(19,4),
    OriginalPeriodCost decimal(19,4),
    OriginalPeriodBill decimal(19,4)
    UNIQUE(RowSeq, MAXSeq, TimePhaseID, PlanID, TaskID, ConsultantID, StartDate, EndDate),
    CHECK(StartDate <= EndDate)
  )

  DECLARE @tabS2TPD TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    ConsultantID varchar(32) COLLATE database_default,
    StartDate datetime, 
    EndDate datetime, 
    PeriodCost decimal(19,4),
    PeriodBill decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, ConsultantID, StartDate, EndDate)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

  SELECT 
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE())))
    FROM CFGRMSettings

  -- Set Dates

  SET @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)
  SET @dtScopeStartDate = CONVERT(datetime, @strScopeStartDate)
  SET @dtScopeEndDate = CONVERT(datetime, @strScopeEndDate)
  SET @dtScopeBeforeDate = DATEADD(DAY, -1, @dtScopeStartDate)
  SET @dtScopeAfterDate = DATEADD(DAY, 1, @dtScopeEndDate)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  
  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. For an Expense row, @strRowID = '<PNConsultant.Account>➔<PNConsultant.Vendor>|<PNTask.TaskID>
  --   2. For an Expense row, @strRowID = '<PNConsultant.Account>➔|<PNTask.TaskID>

  -- Determining Account, Vendor and TaskID.

  SET @intAccountVendorIndex = CHARINDEX(@_AccountVendorSign,@strRowID)
  SET @intRowIDSeperatorIndex = CHARINDEX('|',@strRowID)
  
  IF @intAccountVendorIndex > 0
    BEGIN
      SET @strAccount = LEFT(@strRowID,@intAccountVendorIndex - 1)
    END

  SET @strVendor =
    CASE
      WHEN @intRowIDSeperatorIndex = @intAccountVendorIndex + 1
      THEN NULL
      ELSE SUBSTRING(@strRowID,@intAccountVendorIndex + 1,@intRowIDSeperatorIndex - @intAccountVendorIndex - 1)
    END
  SET @strTaskID = SUBSTRING(@strRowID,@intRowIDSeperatorIndex + 1,LEN(@strRowID) - @intRowIDSeperatorIndex)

  -- Get various parameters.

  SELECT 
    @strCompany = P.Company,
    @strPlanID = T.PlanID,
    @intOutlineLevel = T.OutlineLevel,
    @bitCalculateBilling = 
      CASE 
        WHEN 
          P.BillingCurrencyCode = P.CostCurrencyCode
            AND ((P.ConBillRtMethod > 0 AND P.BudgetType = 'B')
            OR P.BudgetType = 'A')
        THEN 1
        ELSE 0 
      END 
    FROM PNTask AS T
      INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID
    WHERE T.TaskID = @strTaskID

  -- Decimals set basend on the logic hidden inside the tabDecimals function

  SELECT 
    @siAmtDecimals = CASE WHEN @strAmtType = 'C' THEN AmtCostDecimals ELSE AmtBillDecimals END
    FROM dbo.stRP$tabPlanDecimals(@strPlanID)

  -- Determine whether the input Task is a leaf Task.

  SELECT
    @bitIsOnLeafTask = 
      CASE
        WHEN A.ConsultantID IS NOT NULL
        THEN 1
        ELSE 0
      END
    FROM PNConsultant AS A
    WHERE PlanID = @strPlanID AND TaskID = @strTaskID
      AND ((A.Account = @strAccount
      AND ISNULL(A.Vendor, '|') = ISNULL(@strVendor, '|'))
        OR (@strVendor IS NULL AND @strAccount IS NULL))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabConsultant(
    PlanID,
    TaskID,
    ConsultantID,
    Account,
    Vendor,
    StartDate,
    EndDate,
    ScopeWD,
    CT_OutlineNumber,
    CT_OutlineLevel
  )
    SELECT DISTINCT
      C.PlanID,
      C.TaskID,
      C.ConsultantID,
      C.Account,
      C.Vendor,
      C.StartDate,
      C.EndDate,
      CASE
        WHEN (((C.StartDate > @dtScopeEndDate) OR (C.EndDate < @dtScopeStartDate)) AND C.TaskID <> @strTaskID)
        THEN 0
        ELSE
          dbo.DLTK$NumWorkingDays(
            CASE
              WHEN (C.StartDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
              THEN C.StartDate
              ELSE @dtScopeStartDate
            END, 
            CASE
              WHEN (C.EndDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
              THEN C.EndDate
              ELSE @dtScopeEndDate
            END, 
            @strCompany
          )
      END AS ScopeWD,
      CT.OutlineNumber AS CT_OutlineNumber,
      CT.OutlineLevel AS CT_OutlineLevel
      FROM PNConsultant AS C
        INNER JOIN PNTask AS CT ON C.PlanID = CT.PlanID AND C.TaskID = CT.TaskID
        INNER JOIN PNTask AS PT ON C.PlanID = PT.PlanID 
      WHERE
        PT.TaskID = @strTaskID AND
        CT.OutlineNumber LIKE PT.OutlineNumber + '%' AND
        ((C.Account = @strAccount AND ISNULL(C.Vendor, '|') = ISNULL(@strVendor, '|'))
        OR (@strVendor IS NULL AND @strAccount IS NULL))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabTask(
    PlanID,
    TaskID,
    Name,
    ParentOutlineNumber,
    OutlineNumber,
    OutlineLevel
  )
    SELECT DISTINCT
      PT.PlanID AS PlanID,
      PT.TaskID AS TaskID,
      PT.Name AS Name,
      PT.ParentOutlineNumber AS ParentOutlineNumber,
      PT.OutlineNumber AS OutlineNumber,
      PT.OutlineLevel AS OutlineLevel
      FROM @tabConsultant AS C
        INNER JOIN PNTask AS PT ON C.PlanID = PT.PlanID
      WHERE PT.PlanID = @strPlanID AND PT.OutlineLevel >= @intOutlineLevel AND PT.OutlineLevel <= C.CT_OutlineLevel
        AND C.CT_OutlineNumber LIKE PT.OutlineNumber + '%' 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collecting Labor Time-Phased Data to be used in subsequent calculations.
  -- The TPD records come from only the Assignment record indentified by @strConsultantID.

  INSERT @tabTPD(
    TimePhaseID,
    PlanID,
    TaskID,
    ConsultantID,
    StartDate,
    EndDate,
    PeriodCost,
    PeriodBill
  )
    SELECT
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID,
      TPD.TaskID AS TaskID,
      TPD.ConsultantID AS ConsultantID,
      TPD.StartDate AS StartDate,
      TPD.EndDate AS EndDate,
      TPD.PeriodCost AS PeriodCost,
      TPD.PeriodBill AS PeriodBill
      FROM PNPlannedConsultant AS TPD
        INNER JOIN @tabConsultant AS A ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID AND TPD.ConsultantID = A.ConsultantID AND TPD.ConsultantID IS NOT NULL
      WHERE TPD.StartDate <= @dtScopeEndDate AND TPD.EndDate >= @dtScopeStartDate

  SELECT 
    @dtMinTPD = COALESCE(MIN(StartDate), @dtScopeEndDate), 
    @dtMaxTPD = COALESCE(MAX(EndDate), @dtScopeStartDate)
    FROM @tabTPD AS TPD

  SET @decBeforeWD = dbo.DLTK$NumWorkingDays(@dtMinTPD, @dtScopeBeforeDate, @strCompany)
  SET @decAfterWD = dbo.DLTK$NumWorkingDays(@dtScopeAfterDate, @dtMaxTPD, @strCompany)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Build Calendar.
  -- Calendar has 1 range because there is no calendar for expenses in 1.0.

  INSERT @tabCalendar(
    StartDate,
    EndDate
  )	
    SELECT 
      @dtScopeStartDate AS StartDate,
      @dtScopeEndDate AS EndDate

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Split up TPD in @tabTPD to match with Calendar Intervals.
  -- @tabTPD could contain chunks that are larger than a calendar interval which need to be broken up.
  -- @tabTPD could contain chunks that are overlapping with the calendar interval boundaries which need to be broken off.
  -- @tabTPD could contain chunks that are smaller and contain within the span of a calendar interval.

  INSERT @tabS1TPD(
    RowID,
    TimePhaseID,
    CIStartDate,
    PlanID, 
    TaskID,
    ConsultantID,
    StartDate, 
    EndDate, 
    PeriodCost,
    PeriodBill
   )
     SELECT
      REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
      TimePhaseID AS TimePhaseID,
      CIStartDate AS CIStartDate,
      PlanID AS PlanID, 
      TaskID AS TaskID,
      ConsultantID AS ConsultantID,
      StartDate AS StartDate, 
      EndDate AS EndDate, 
      ROUND(ISNULL(PeriodCost, 0), @siAmtDecimals) AS PeriodCost,
      ROUND(ISNULL(PeriodBill, 0), @siAmtDecimals) AS PeriodBill
    FROM (
      SELECT 
        CI.StartDate AS CIStartDate, 
        TPD.TimePhaseID AS TimePhaseID,
        TPD.PlanID AS PlanID, 
        TPD.TaskID AS TaskID,
        TPD.ConsultantID AS ConsultantID,
        CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
        CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
        CASE 
          WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate) AND @strAmtType = 'C'
          THEN PeriodCost * 
            dbo.DLTK$ProrateRatio(
              CASE 
                WHEN TPD.StartDate > CI.StartDate 
                THEN TPD.StartDate 
                ELSE CI.StartDate 
              END, 
              CASE 
                WHEN TPD.EndDate < CI.EndDate 
                THEN TPD.EndDate 
                ELSE CI.EndDate 
              END, 
              TPD.StartDate, TPD.EndDate,
              @strCompany)
          ELSE PeriodCost 
        END AS PeriodCost,
        CASE 
          WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate) AND @strAmtType = 'B'
          THEN PeriodBill * 
            dbo.DLTK$ProrateRatio(
              CASE 
                WHEN TPD.StartDate > CI.StartDate 
                THEN TPD.StartDate 
                ELSE CI.StartDate 
              END, 
              CASE 
                WHEN TPD.EndDate < CI.EndDate 
                THEN TPD.EndDate 
                ELSE CI.EndDate 
              END, 
              TPD.StartDate, TPD.EndDate,
              @strCompany)
          ELSE PeriodBill 
        END AS PeriodBill
        FROM @tabCalendar AS CI 
          INNER JOIN @tabTPD AS TPD 
            ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
    ) AS X

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     
  -- Adjust time-phased data to compensate for rounding errors after the splitting process.

  IF (@strAmtType = 'C')
    BEGIN
      UPDATE @tabS1TPD SET PeriodCost = (TPD.PeriodCost + D.DeltaAmt)
        FROM @tabS1TPD AS TPD 
          INNER JOIN (
            SELECT YTPD.TimePhaseID AS TimePhaseID, (YTPD.PeriodCost - SUM(ISNULL(XTPD.PeriodCost, 0))) AS DeltaAmt
              FROM @tabS1TPD AS XTPD 
                INNER JOIN @tabTPD AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
              GROUP BY YTPD.TimePhaseID, YTPD.PeriodCost
          ) AS D ON TPD.TimePhaseID = D.TimePhaseID
        WHERE RowID IN (
          SELECT RowID FROM @tabS1TPD AS ATPD 
            INNER JOIN (
              SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabS1TPD GROUP BY TimePhaseID
            ) AS BTPD ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate
        )
    END
  ELSE
    BEGIN
      UPDATE @tabS1TPD SET PeriodBill = (TPD.PeriodBill + D.DeltaAmt)
        FROM @tabS1TPD AS TPD 
          INNER JOIN (
            SELECT YTPD.TimePhaseID AS TimePhaseID, (YTPD.PeriodBill - SUM(ISNULL(XTPD.PeriodBill, 0))) AS DeltaAmt
              FROM @tabS1TPD AS XTPD 
                INNER JOIN @tabTPD AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
              GROUP BY YTPD.TimePhaseID, YTPD.PeriodBill
          ) AS D ON TPD.TimePhaseID = D.TimePhaseID
        WHERE RowID IN (
          SELECT RowID FROM @tabS1TPD AS ATPD 
            INNER JOIN (
              SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabS1TPD GROUP BY TimePhaseID
            ) AS BTPD ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate
        )
    END
             
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- At this point we have a table with TPD rows aligned on the scope boundaries.
  -- Let's find out whether there is any hour within the scope. 
  -- This information is needed to decide whether the spread of new hours will be proportional or based on Work-Days.

  IF (@strAmtType = 'C')
    BEGIN
      SELECT @decScopeTotalAmt = SUM(PeriodCost)
        FROM @tabS1TPD
        WHERE StartDate <= @dtScopeEndDate AND EndDate >= @dtScopeStartDate
    END
  ELSE
    BEGIN
      SELECT @decScopeTotalAmt = SUM(PeriodBill)
        FROM @tabS1TPD
        WHERE StartDate <= @dtScopeEndDate AND EndDate >= @dtScopeStartDate
    END

  -- Spread new hours to TPD rows within the scope.

  IF (@decScopeTotalAmt > 0)
    BEGIN /* Spread proportional */

      -- Load table @tabSpreadTPD with original amounts which will be used as the spread factor.
      -- Need to use this intermediate table to control the sequencing of the TPD rows.

      INSERT @tabSpreadTPD(
        RowSeq,
        MAXSeq,
        TimePhaseID,
        PlanID, 
        TaskID,
        ConsultantID,
        Account,
        StartDate, 
        EndDate, 
        Dividend,
        Divisor,
        OriginalPeriodCost,
        OriginalPeriodBill
      )
        SELECT
          X.RowSeq AS RowSeq,
          MAX(X.RowSeq) OVER () AS MAXSeq,
          X.TimePhaseID AS TimePhaseID,
          X.PlanID AS PlanID, 
          X.TaskID AS TaskID,
          X.ConsultantID AS ConsultantID,
          X.Account AS Account,
          X.StartDate AS StartDate, 
          X.EndDate AS EndDate, 
          X.PeriodAmt AS Dividend,
          SUM(X.PeriodAmt) OVER (ORDER BY X.RowSeq ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) AS Divisor,
          X.OriginalPeriodCost,
          X.OriginalPeriodBill
          FROM ( /* X */
            SELECT
              ROW_NUMBER() OVER(ORDER BY C.PlanID, C.CT_OutlineNumber, C.Account, C.Vendor) AS RowSeq,
              TPD.RowID AS TimePhaseID,
              TPD.PlanID AS PlanID, 
              TPD.TaskID AS TaskID,
              TPD.ConsultantID AS ConsultantID,
              C.Account AS Account,
              TPD.StartDate AS StartDate, 
              TPD.EndDate AS EndDate, 
              CASE @strAmtType
                WHEN 'C' THEN TPD.PeriodCost
                WHEN 'B' THEN TPD.PeriodBill
              END AS PeriodAmt,
              TPD.PeriodCost AS OriginalPeriodCost,
              TPD.PeriodBill AS OriginalPeriodBill
              FROM @tabS1TPD AS TPD
                INNER JOIN @tabConsultant AS C ON TPD.PlanID = C.PlanID AND TPD.TaskID = C.TaskID AND TPD.ConsultantID = C.ConsultantID
              WHERE TPD.StartDate <= @dtScopeEndDate AND TPD.EndDate >= @dtScopeStartDate
          ) AS X

    END /* END IF (@decScopeTotalAmt > 0) THEN */
  ELSE
    BEGIN /* Spread based on Leaf Work Days */

      -- Defect #1426044, Luong Vu
      -- When we get into this ELSE-branch, there is a scenario where one side
      -- of the PeriodCost or PeriodBill already had a TPD value. In that scenario,
      -- we must preserve the existing TPD value going forward in the rest of this script.

      -- Calculate total work days for all of Assignments that crossed the scope range.

      SELECT @decScopeTotalWD = SUM(ScopeWD)
        FROM @tabConsultant

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      IF (@decScopeTotalWD > 0)
        BEGIN

          INSERT @tabSpreadTPD(
            RowSeq,
            MAXSeq,
            TimePhaseID,
            PlanID, 
            TaskID,
            ConsultantID,
            Account,
            StartDate, 
            EndDate, 
            Dividend,
            Divisor,
            OriginalPeriodCost,
            OriginalPeriodBill
          )
            SELECT
              X.RowSeq AS RowSeq,
              MAX(X.RowSeq) OVER () AS MAXSeq,
              X.TimePhaseID AS TimePhaseID,
              X.PlanID AS PlanID, 
              X.TaskID AS TaskID,
              X.ConsultantID AS ConsultantID,
              X.Account AS Account,
              X.StartDate AS StartDate, 
              X.EndDate AS EndDate, 
              X.ScopeWD AS Dividend,
              SUM(X.ScopeWD) OVER (ORDER BY X.RowSeq ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) AS Divisor,
              X.OriginalPeriodCost,
              X.OriginalPeriodBill
              FROM ( /* X */
                SELECT
                  ROW_NUMBER() OVER(ORDER BY C.PlanID, C.CT_OutlineNumber, C.Account, C.Vendor) AS RowSeq,
                  REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
                  C.PlanID AS PlanID, 
                  C.TaskID AS TaskID,
                  C.ConsultantID AS ConsultantID,
                  C.Account AS Account,
                  CASE
                    WHEN (C.StartDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
                    THEN C.StartDate
                    ELSE @dtScopeStartDate
                  END AS StartDate, 
                  CASE
                    WHEN (C.EndDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
                    THEN C.EndDate
                    ELSE @dtScopeEndDate
                  END AS EndDate, 
                  C.ScopeWD AS ScopeWD,
                  ISNULL(TPD.PeriodCost,0) AS OriginalPeriodCost,
                  ISNULL(TPD.PeriodBill,0) AS OriginalPeriodBill
                  FROM @tabConsultant AS C
                    LEFT JOIN @tabS1TPD AS TPD ON TPD.PlanID = C.PlanID AND TPD.TaskID = C.TaskID AND TPD.ConsultantID = C.ConsultantID
                      AND TPD.StartDate <= @dtScopeEndDate AND TPD.EndDate >= @dtScopeStartDate
              ) AS X

        END /* END IF (@decScopeTotalWD > 0) THEN */

      ELSE IF (@decScopeTotalWD = 0 AND @bitIsOnLeafTask = 1)
        BEGIN

          INSERT @tabSpreadTPD(
            RowSeq,
            MAXSeq,
            TimePhaseID,
            PlanID, 
            TaskID,
            ConsultantID,
            Account,
            StartDate, 
            EndDate, 
            Dividend,
            Divisor,
            OriginalPeriodCost,
            OriginalPeriodBill
          )
            SELECT
              X.RowSeq AS RowSeq,
              MAX(X.RowSeq) OVER () AS MAXSeq,
              X.TimePhaseID AS TimePhaseID,
              X.PlanID AS PlanID, 
              X.TaskID AS TaskID,
              X.ConsultantID AS ConsultantID,
              X.Account AS Account,
              X.StartDate AS StartDate, 
              X.EndDate AS EndDate, 
              X.ScopeWD AS Dividend,
              SUM(X.ScopeWD) OVER (ORDER BY X.RowSeq ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) AS Divisor,
              X.OriginalPeriodCost,
              X.OriginalPeriodBill
              FROM ( /* X */
                SELECT
                  ROW_NUMBER() OVER(ORDER BY C.PlanID, C.CT_OutlineNumber, C.Account, C.Vendor) AS RowSeq,
                  REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
                  C.PlanID AS PlanID, 
                  C.TaskID AS TaskID,
                  C.ConsultantID AS ConsultantID,
                  C.Account AS Account,
                  CASE
                    WHEN (C.StartDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
                    THEN C.StartDate
                    ELSE @dtScopeStartDate
                  END AS StartDate, 
                  CASE
                    WHEN (C.EndDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
                    THEN C.EndDate
                    ELSE @dtScopeEndDate
                  END AS EndDate, 
                  1 AS ScopeWD,
                  ISNULL(TPD.PeriodCost,0) AS OriginalPeriodCost,
                  ISNULL(TPD.PeriodBill,0) AS OriginalPeriodBill
                  FROM @tabConsultant AS C
                    LEFT JOIN @tabS1TPD AS TPD ON TPD.PlanID = C.PlanID AND TPD.TaskID = C.TaskID AND TPD.ConsultantID = C.ConsultantID
                      AND TPD.StartDate <= @dtScopeEndDate AND TPD.EndDate >= @dtScopeStartDate
              ) AS X

        END /* END ELSE IF (@decScopeTotalWD = 0 AND @bitIsOnLeafTask = 1) */

    END /* END IF (@decScopeTotalAmt > 0) ELSE */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Spread @decAmt.
  
  ;

  WITH Spread AS (
    SELECT
      TPD.RowSeq AS RowSeq,
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID, 
      TPD.TaskID AS TaskID,
      TPD.ConsultantID AS ConsultantID,
      TPD.Account AS Account,
      TPD.StartDate AS StartDate, 
      TPD.EndDate AS EndDate, 
      CONVERT(decimal(19,4), ROUND(ISNULL((@decAmt * (TPD.Dividend / TPD.Divisor)), 0.0000), @siAmtDecimals)) AS PeriodAmt,
      CONVERT(decimal(19,4), @decAmt - ROUND(ISNULL((@decAmt * (TPD.Dividend / TPD.Divisor)), 0.0000), @siAmtDecimals)) AS RemainingAmt,
      TPD.OriginalPeriodCost AS OriginalPeriodCost,
      TPD.OriginalPeriodBill As OriginalPeriodBill
      FROM @tabSpreadTPD AS TPD
      WHERE TPD.RowSeq = 1
    UNION ALL
    SELECT
      TPD.RowSeq AS RowSeq,
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID, 
      TPD.TaskID AS TaskID,
      TPD.ConsultantID AS ConsultantID,
      TPD.Account AS Account,
      TPD.StartDate AS StartDate, 
      TPD.EndDate AS EndDate,
      CASE
        WHEN TPD.RowSeq = TPD.MAXSeq
        THEN S.RemainingAmt
        ELSE CONVERT(decimal(19,4), ROUND(ISNULL((S.RemainingAmt * (TPD.Dividend / TPD.Divisor)), 0.0000), @siAmtDecimals))
      END AS PeriodAmt,
      CASE
        WHEN TPD.RowSeq = TPD.MAXSeq
        THEN 0
        ELSE CONVERT(decimal(19,4), S.RemainingAmt - ROUND(ISNULL((S.RemainingAmt * (TPD.Dividend / TPD.Divisor)), 0.0000), @siAmtDecimals))
      END AS RemainingAmt,
      TPD.OriginalPeriodCost AS OriginalPeriodCost,
      TPD.OriginalPeriodBill As OriginalPeriodBill
      FROM Spread AS S 
        INNER JOIN @tabSpreadTPD AS TPD ON S.RowSeq + 1 = TPD.RowSeq
      WHERE TPD.RowSeq > 1 AND S.RemainingAmt > 0
  )

    INSERT @tabS2TPD(
      TimePhaseID,
      PlanID, 
      TaskID,
      ConsultantID,
      StartDate, 
      EndDate, 
      PeriodCost,
      PeriodBill
    ) 
      SELECT
        S.TimePhaseID AS TimePhaseID,
        S.PlanID AS PlanID, 
        S.TaskID AS TaskID,
        S.ConsultantID AS ConsultantID,
        S.StartDate AS StartDate, 
        S.EndDate AS EndDate, 
        CASE
          WHEN @strAmtType = 'C' 
          THEN ISNULL(S.PeriodAmt, 0.0000)
          ELSE ISNULL(S.OriginalPeriodCost, 0.0000)
        END AS PeriodCost,
        CASE
          WHEN @strAmtType = 'B' THEN ISNULL(S.PeriodAmt, 0.0000)
          WHEN @strAmtType = 'C' AND @bitCalculateBilling = 1 THEN
            ROUND(ISNULL(ISNULL(S.PeriodAmt, 0.0000) * dbo.PN$ConRate(S.PlanID, S.Account), 0.0000), @siAmtDecimals)
          ELSE ISNULL(S.OriginalPeriodBill, 0.0000)
        END AS PeriodBill
        FROM Spread AS S
        OPTION (MAXRECURSION 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- At this point, there may be rows in @tabS2TPD with zero cost and bill amounts as a result of rounding. 
  -- Need to delete these rows with zero cost and bill amounts.

  DELETE @tabS2TPD WHERE PeriodCost = 0 AND PeriodBill = 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION

  DELETE PNPlannedConsultant 
    WHERE 
      PlanID = @strPlanID 
      AND (
        TimePhaseID IN (SELECT DISTINCT TimePhaseID FROM @tabTPD) 
      )

  INSERT PNPlannedConsultant(
    TimePhaseID,
    PlanID, 
    TaskID,
    ConsultantID,
    StartDate, 
    EndDate, 
    PeriodCost,
    PeriodBill,
    CreateUser,
    ModUser,
    CreateDate,
    ModDate
  )
      SELECT
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID, 
      TPD.TaskID AS TaskID,
      TPD.ConsultantID AS ConsultantID,
      TPD.StartDate AS StartDate, 
      TPD.EndDate AS EndDate, 
      TPD.PeriodCost AS PeriodCost,
      TPD.PeriodBill AS PeriodBill,
      CASE WHEN DATALENGTH(@strUserName) = 0 THEN N'stRPSpreadPlannedConAmt' ELSE N'SHR_' + @strUserName END AS CreateUser,
      CASE WHEN DATALENGTH(@strUserName) = 0 THEN N'stRPSpreadPlannedConAmt' ELSE N'SHR_' + @strUserName END AS ModUser,
      LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As CreateDate,
      LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As ModDate 
    FROM @tabS2TPD AS TPD
    WHERE PeriodCost > 0 OR PeriodBill > 0

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Update VesionID.

    EXECUTE dbo.stRPUpdateVersionID @strPlanID, @bitCalledFromRM 

    -- Set LastPlanAction.

    IF (@bitCalledFromRM = 0)
      BEGIN
        UPDATE PNPlan SET LastPlanAction = 'SAVED' WHERE PlanID = @strPlanID
      END /* END IF (@bitCalledFromRM = 0) */

  COMMIT

END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     


GO
