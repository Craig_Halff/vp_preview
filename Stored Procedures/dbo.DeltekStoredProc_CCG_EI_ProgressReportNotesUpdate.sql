SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_ProgressReportNotesUpdate] ( @WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
             AS EXEC spCCG_EI_ProgressReportNotesUpdate @WBS1,@WBS2,@WBS3
GO
