SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_GetSQLWhereForMainGrid](@delegation varchar(1), @sqlPart varchar(max) output, @sqlPart2 varchar(max) output)
AS BEGIN
	-- Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved.
	/*
	declare @sqlpart varchar(max)
	EXEC [spCCG_EI_GetSQLWhereForMainGrid] @delegation = 'Y', @sqlPart = @sqlpart OUTPUT
	print @sqlpart
	*/
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	declare @rtnVal varchar(max) = (select top 1 SQLPart from CCG_EI_SQLPartsCache where [Type] = 'MainGridWhere-'+@delegation)
	declare @rtnVal2 varchar(max) = (select top 1 SQLPart from CCG_EI_SQLPartsCache where [Type] = 'MainGridWhere2-'+@delegation)

	if isnull(@rtnVal,'') <> '' and isnull(@rtnVal2,'') <> ''
	begin
		SET @sqlPart = @rtnVal
		SET @sqlPart2 = @rtnVal2
		RETURN
	end
	else begin
		set @rtnVal = ''
		set @rtnVal2 = ''

		declare @first bit = 1, @Role varchar(30), @VisionField varchar(max)
		declare cursorRoles cursor fast_forward for
			select Role, VisionField
				from CCG_EI_ConfigRoles
				where Status = 'A' and VisionField <> ''
				order by ColumnOrder

		open cursorRoles
		fetch next from cursorRoles into @Role, @VisionField
		while @@fetch_status=0
		begin
			if @first = 0 set @rtnVal = @rtnVal + ' or '
			if @first = 0 set @rtnVal2 = @rtnVal2 + ' or '
			set @Role = rtrim(@Role)

			set @rtnVal2 = @rtnVal2 + ' (Rights.Role = '''+@Role+''' and PCTF.'+@VisionField+' = ''[:Employee]'')
				'

			if @delegation = 'N'
			begin
				set @rtnVal = @rtnVal + ' (Rights.Role = '''+@Role+''' and PCTF.'+@VisionField+' = ''[:Employee]'')
					'
			end
			else begin
				set @rtnVal = @rtnVal + ' (Rights.Role = '''+@Role+''' and (
							   (PCTF.'+@VisionField+' = d3.Employee AND ISNULL(d3.Employee,/*N*/'''')<>/*N*/'''')
							OR (PCTF.'+@VisionField+' = d2.Employee AND ISNULL(d3.Employee,/*N*/'''')=/*N*/'''' AND ISNULL(d2.Employee,/*N*/'''')<>/*N*/'''')
                            OR (PCTF.'+@VisionField+' = d1.Employee AND ISNULL(d3.Employee,/*N*/'''')=/*N*/'''' AND ISNULL(d2.Employee,/*N*/'''')=/*N*/'''' AND ISNULL(d1.Employee,/*N*/'''')<>/*N*/'''')
							OR (PCTF.'+@VisionField+' = /*N*/''[:Employee]'' AND (ISNULL(dt3.Delegate,'''')=/*N*/'''' OR dt3.Dual=''Y'') AND (ISNULL(dt2.Delegate,/*N*/'''')=/*N*/'''' OR dt2.Dual=''Y'') AND (ISNULL(dt1.Delegate,/*N*/'''')=/*N*/'''' OR dt1.Dual=''Y''))
						))
					'
			end

			set @first = 0
			fetch next from cursorRoles into @Role, @VisionField
		end
		close cursorRoles
		deallocate cursorRoles
	end

	delete from CCG_EI_SQLPartsCache where [Type] = 'MainGridWhere-'+@delegation or [Type] = 'MainGridWhere2-'+@delegation
	insert into CCG_EI_SQLPartsCache ([Role], [Type], [SQLPart]) values (NULL, 'MainGridWhere-'+@delegation, @rtnVal)
	insert into CCG_EI_SQLPartsCache ([Role], [Type], [SQLPart]) values (NULL, 'MainGridWhere2-'+@delegation, @rtnVal2)

	SET @sqlPart = @rtnVal
	SET @sqlPart2 = @rtnVal2
END
GO
