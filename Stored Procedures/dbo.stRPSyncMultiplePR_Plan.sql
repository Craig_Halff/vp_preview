SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPSyncMultiplePR_Plan]
AS

BEGIN -- Procedure stRPSyncMultiplePR_Plan

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to fix WBS Structure in a Plan when it is out-of-sync with Project Structure. 
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @strPlanID varchar(32)
  DECLARE @strWBS1 nvarchar(30)

  DECLARE @tabPlan TABLE (
    PlanID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default
    UNIQUE(PlanID, WBS1)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find Plans that have differences between PR and PNTask.
  -- This script only considers "Active" Projects 
  -- Must use a temp table here otherwise the cursor will get confused when Plans are being changed inside a loop. 

  INSERT @tabPlan(
    PlanID,
    WBS1
  )
    SELECT DISTINCT
      P.PlanID,
      X.WBS1
      FROM (
        SELECT
          PR.WBS1,
          PR.WBS2,
          PR.WBS3,
          PR.Name
          FROM PR
            LEFT JOIN PNTask AS T ON PR.WBS1 = T.WBS1 AND PR.WBS2 = ISNULL(T.WBS2, ' ') AND PR.WBS3 = ISNULL(T.WBS3, ' ')
          WHERE T.PlanID IS NULL
        ) AS X
          INNER JOIN PR ON PR.WBS1 = X.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          INNER JOIN PNPlan AS P ON X.WBS1 = P.WBS1


  DECLARE csr20170505_PNPlan CURSOR LOCAL FAST_FORWARD FOR
    SELECT PlanID, WBS1
      FROM @tabPlan AS RP

  OPEN csr20170505_PNPlan
  FETCH NEXT FROM csr20170505_PNPlan INTO @strPlanID, @strWBS1

  WHILE (@@FETCH_STATUS = 0)
    BEGIN

      PRINT '>>> Start Processing PlanID = "' + @strPlanID + '", WBS1 = "' + @strWBS1 + '"'
      EXECUTE dbo.stRPSyncWBSStructure @strPlanID
      PRINT '<<< End Processing PlanID = "' + @strPlanID + '", WBS1 = "' + @strWBS1 + '"'

    --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      FETCH NEXT FROM csr20170505_PNPlan INTO @strPlanID, @strWBS1

    END -- While

  CLOSE csr20170505_PNPlan
  DEALLOCATE csr20170505_PNPlan

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

END -- stRPSyncMultiplePR_Plan
GO
