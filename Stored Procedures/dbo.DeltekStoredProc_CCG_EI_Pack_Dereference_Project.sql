SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Pack_Dereference_Project]
	@getAddress			bit,
	@fieldsql			varchar(500),
	@id					Nvarchar(500),
	@fieldname			varchar(50),
	@XML_String			VARCHAR(500) = ''
AS
BEGIN
	/*
	[DeltekStoredProc_CCG_EI_Pack_Dereference_Project] 0, 'Contactid', '2003005.00', 'Contact', ''
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	int;

	-- Ensure that our inputs are safe (prevent SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<SQL PHRASE>', @fieldsql);
	SET @safeSql = @safeSql | (dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @id) * 2);
	SET @safeSql = @safeSql | (dbo.fnCCG_EI_RegexMatch('<FIELD>|', @fieldname) * 4);
	SET @safeSql = @safeSql | (dbo.fnCCG_EI_RegexMatch('<SQL PHRASE>|', @XML_String) * 8);
	IF @safeSql < 15 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS varchar(4));
		RETURN;
	END;
	SET @fieldname = LOWER(@fieldname);

	IF @getAddress = 0 SET @sSQL = '
		SELECT ' + @fieldsql + ' FROM PR
			INNER JOIN ProjectCustomTabFields C on PR.WBS1 = C.WBS1 and C.WBS2 = '' ''
			INNER JOIN BT on PR.WBS1 = BT.WBS1 and BT.WBS2 = '' ''
			WHERE PR.WBS1 = ''' + @id + ''' and PR.WBS2 = '' ''';
	ELSE BEGIN
		IF @fieldname = 'projectlocation' SET @sSQL = '
			SELECT top 1 ''ProjectLocation'' as Description, Address1, Address2, Address3,
					City, State, ZIP, Country
				FROM PR
				WHERE WBS1 = ''' + @id + ''' and WBS2 = '' ''';
		ELSE IF @fieldname = 'primaryclientaddress' SET @sSQL = '
			SELECT top 1 CLAddress.[Address] as Description, CLAddress.Address1, CLAddress.Address2,
					CLAddress.Address3, CLAddress.Address4, CLAddress.City, CLAddress.State,
					CLAddress.ZIP, CLAddress.Country
				FROM CLAddress INNER JOIN PR on WBS1 = ''' + @id + ''' and WBS2 = '' ''
					and PR.CLAddress = CLAddress.Address and PR.ClientID = CLAddress.ClientID';
		ELSE IF @fieldname = 'billclientaddress' OR @fieldname = 'billingclientaddress' SET @sSQL = '
			SELECT top 1 CLAddress.[Address] as Description, CLAddress.Address1, CLAddress.Address2,
					CLAddress.Address3, CLAddress.Address4, CLAddress.City, CLAddress.State,
					CLAddress.ZIP, CLAddress.Country
				FROM CLAddress INNER JOIN PR on WBS1 = ''' + @id + ''' and WBS2 = '' ''
					and PR.CLBillingAddr = CLAddress.Address and PR.BillingClientID = CLAddress.ClientID';
	END;
	SET @sSQL += ' ' + @XML_String;

	--PRINT @sSQL
	EXEC(@sSQL);
END;
GO
