SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_ProjectBTMemoValidate]
    @WBS1 AS VARCHAR(12)
AS
    /**********************************************************************************
			Copyright (c) 2021 Halff Associates Inc. All rights reserved.

			Check to make sure that the character count of Billing Terms Notes is 255 
			characters or less so that the value can be passed to dob.BT when billing 
			terms are updated.

			08/24/2021	Jeremy Baummer - Created

	***********************************************************************************/
SET NOCOUNT ON
BEGIN
	IF(SELECT LEN(dbo.CCG_StripHTMLTag(px.CustBillingTermNotes,'')) FROM dbo.ProjectCustomTabFields AS px WHERE px.WBS1 = @WBS1 AND px.WBS2 = ' ') >=255
		RAISERROR('Billing Term Notes can not be longer than 255 characters.',16,1)
END
GO
