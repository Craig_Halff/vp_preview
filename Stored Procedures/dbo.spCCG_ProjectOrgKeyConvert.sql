SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectOrgKeyConvert] @WBS1 varchar (32), @Org varchar (32)
AS
/*
Copyright (c) 2018 Central Consulting Group. All rights reserved.
03/05/2018 David Springer
           Write a project org key convert record
           Call this on an Opportunity change workflow when Org has changed.
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
BEGIN

	INSERT INTO KeyConvertWorkPROrg
	(WBS1, WBS2, WBS3, NewOrg, Timeframe, StartPeriod, Account)
	Select WBS1, WBS2, WBS3, @Org, '2', '0', Null
	From PR 
	Where WBS1 = @WBS1

END
GO
