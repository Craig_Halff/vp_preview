SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[KeyNewValOrgLabcd]
	@ColumnName 	Nvarchar(100),
	@cvtType			integer, -- 0 oldVal -> NewVal; 1 --Length Change; 2 --Remove leading zeros; 3 --Company subcode
	@RetValue 		Nvarchar(500) output,
   @ChangeSide	   Nvarchar(5) = 'Left', --left or right
	@CvtLevel		integer = 1, --used for subcodes ignored otherwise
	@cLevels			integer,
	@cDelimiter		varchar(1),
	@c1Start			integer,
	@c1Length		integer,
	@c2Start			integer,
	@c2Length		integer,
	@c3Start			integer,
	@c3Length		integer,
	@c4Start			integer,
	@c4Length		integer,
	@c5Start			integer,
	@c5Length		integer,
	@OldLevels		integer,   
	@OldDelimiter	varchar(1),
	@Old1Start		integer,   
	@Old1Length		integer,   
	@Old2Start		integer,
	@Old2Length		integer, 
	@Old3Start		integer,   
	@Old3Length		integer,   
	@Old4Start		integer,   
	@Old4Length		integer,   
	@Old5Start		integer,   
	@Old5Length		integer,
	@Order			varchar(5) = '12345'
as
declare @modColName Nvarchar(200),
@i  integer,
@tmpVal  integer
begin
/*
	if (@cvtType = 1) --Length/format change
		begin
			set @RetValue = 'right(replicate(''0'',' + convert(Nvarchar,(@c1Length)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old1Start) + ',' + convert(Nvarchar,@Old1Length) + '),''''),' + convert(Nvarchar,@c1Length) +')' +
					case when @cLevels > 1 then ' + ''' + @cDelimiter + ''' + ' else ' + ' end + 
				'right(replicate(''0'',' + convert(Nvarchar,(@c2Length)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old2Start) + ',' + convert(Nvarchar,@Old2Length) + '),''''),' + convert(Nvarchar,@c2Length) +')' +
					case when @cLevels > 2 then ' + ''' + @cDelimiter + ''' + ' else ' + ' end + 				
				'right(replicate(''0'',' + convert(Nvarchar,(@c3Length)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old3Start) + ',' + convert(Nvarchar,@Old3Length) + '),''''),' + convert(Nvarchar,@c3Length) +')' +
					case when @cLevels > 3 then ' + ''' + @cDelimiter + ''' + ' else ' + ' end + 		
				'right(replicate(''0'',' + convert(Nvarchar,(@c4Length)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old4Start) + ',' + convert(Nvarchar,@Old4Length) + '),''''),' + convert(Nvarchar,@c4Length) +')' +
					case when @cLevels > 4 then ' + ''' + @cDelimiter + ''' + ' else ' + ' end + 						
				'right(replicate(''0'',' + convert(Nvarchar,(@c5Length)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old5Start) + ',' + convert(Nvarchar,@Old5Length) + '),''''),' + convert(Nvarchar,@c5Length) +')'

		end*/

	declare @theLen integer
	set @i=1
	set @retValue = ''
	if (@cLevels = 0)
	  begin
		 set @retValue = 'null'
	  end
   if (@CvtType = 3)
	  begin
		 set @i = convert(Nvarchar,@CvtLevel)
			set @TmpVal = @i
		select @theLen = 
			case @i when 1 then @c1Length
			when 2 then @c2Length
			when 3 then @c3Length
			when 4 then @c4Length
			when 5 then @c5Length
			end

			if (@ChangeSide = 'Left')
			begin
				select @RetValue =
					case @TmpVal
					when 1 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old1Start) + ',' + convert(Nvarchar,@Old1Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
					when 2 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old2Start) + ',' + convert(Nvarchar,@Old2Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
					when 3 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old3Start) + ',' + convert(Nvarchar,@Old3Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
					when 4 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old4Start) + ',' + convert(Nvarchar,@Old4Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
					when 5 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old5Start) + ',' + convert(Nvarchar,@Old5Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
					end
			end
			if (@ChangeSide = 'Right')
			begin	
				select @RetValue = case @TmpVal
					when 1 then 'left(isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old1Start) + ',' + convert(Nvarchar,@Old1Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
					when 2 then 'left(isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old2Start) + ',' + convert(Nvarchar,@Old2Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
					when 3 then 'left(isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old3Start) + ',' + convert(Nvarchar,@Old3Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
					when 4 then 'left(isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old4Start) + ',' + convert(Nvarchar,@Old4Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
					when 5 then 'left(isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old5Start) + ',' + convert(Nvarchar,@Old5Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
					end
			end
		  end
else
	begin --not cvt 3
	--print @order + ' Order'
	   while (@i<=@cLevels)
		  begin
			set @tmpVal = convert(integer,substring(@Order,@i,1))
			select @theLen = 
				case @i when 1 then @c1Length
				when 2 then @c2Length
				when 3 then @c3Length
				when 4 then @c4Length
				when 5 then @c5Length
				end
	--print 'theLen ' + convert(Nvarchar,@theLen) + isNull(@ColumnName,'NULL') + isNull(convert(Nvarchar,@Old1Start),'NULL') + isNull(convert(Nvarchar,@Old1Length),'NULL')
			if (@ChangeSide = 'Left')
			begin
				select @RetValue = @RetValue + 
					case @i when 1 then '' else ' + ''' + @cDelimiter + ''' + ' end
					+ case @TmpVal
					when 1 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old1Start) + ',' + convert(Nvarchar,@Old1Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
					when 2 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old2Start) + ',' + convert(Nvarchar,@Old2Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
					when 3 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old3Start) + ',' + convert(Nvarchar,@Old3Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
					when 4 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old4Start) + ',' + convert(Nvarchar,@Old4Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
					when 5 then 'right(replicate(''0'',' + convert(Nvarchar,(@theLen)) + ') + isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old5Start) + ',' + convert(Nvarchar,@Old5Length) + '),''''),' + convert(Nvarchar,@theLen) +')'
					end
			end
			if (@ChangeSide = 'Right')
			begin	
				select @RetValue = @RetValue + 
					case @i when 1 then '' else ' + ''' + @cDelimiter + ''' + ' end
					+ case @TmpVal
					when 1 then 'left(isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old1Start) + ',' + convert(Nvarchar,@Old1Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
					when 2 then 'left(isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old2Start) + ',' + convert(Nvarchar,@Old2Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
					when 3 then 'left(isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old3Start) + ',' + convert(Nvarchar,@Old3Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
					when 4 then 'left(isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old4Start) + ',' + convert(Nvarchar,@Old4Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
					when 5 then 'left(isNull(substring(' + @ColumnName + ',' + convert(Nvarchar,@Old5Start) + ',' + convert(Nvarchar,@Old5Length) + '),'''') + replicate(''0'',' + convert(Nvarchar,(@theLen)) + '),' + convert(Nvarchar,@theLen) +')'
					end
			end
			set @i = @i + 1
		  end
	end
end
GO
