SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_UMF_GetData] (
	@stripHTML			int,				/* 0 or 1 */
	@product			varchar(20),		/* EI/PAT/etc. */
	@cacheField			varchar(100),
	@updateTable		varchar(200),
	@updateField		varchar(100),
	@wbs1				Nvarchar(30),
	@wbs2				Nvarchar(7),
	@wbs3				Nvarchar(7),
	@payableSeq			int = 0				/* Only provided if product = PAT */
)
AS
BEGIN
	/* Copyright (c) 2018 EleVia Software and Central Consulting Group.  All rights reserved. */
	/*
		exec [spCCG_EI_UMF_GetData] 0, 'EI', '', 'ProjectCustomTabFields', 'CustMemorandumField', '2002010.00', ' ', ' '
	*/
	SET NOCOUNT ON
	declare @sql varchar(max)

	if @product = 'EI'
		-- Are we using a cache field in EI/PAT?
		--	If the field is in EI, get the value from CCG_EI_CustomColumns if there, otherwise the Vision table
		if isnull(@cacheField,'') <> ''
			set @sql = N'
				select '+(case when @stripHTML=1 then 'dbo.fnCCG_StripHtml(' else '' end)+' IsNull(eicc.'+@cacheField+', t.'+@updateField+') '+(case when @stripHTML=1 then ')' else '' end)+'
					from '+@updateTable+' t
						left join CCG_EI_CustomColumns eicc on eicc.WBS1 = t.WBS1 and eicc.WBS2 = t.WBS2 and eicc.WBS3 = t.WBS3
					where t.WBS1 = N'''+@wbs1+''' and t.WBS2 = N'''+@wbs2+''' and t.WBS3 = N'''+@wbs3+''''
		else
			set @sql = N'
				select '+(case when @stripHTML=1 then 'dbo.fnCCG_StripHtml(' else '' end) + @updateField + (case when @stripHTML=1 then ')' else '' end)+'
					from '+@updateTable+' t
					where t.WBS1 = N'''+@wbs1+''' and t.WBS2 = N'''+@wbs2+''' and t.WBS3 = N'''+@wbs3+''''

	else if @product = 'PAT'
		-- Are we using a cache field in PAT?
		--	If the field is in PAT, get the value from CCG_PAT_CustomColumns if there, otherwise the Vision table
		if isnull(@cacheField,'') <> ''
			if isnull(@wbs1,'') = '' or isnull(@updateField,'') = '' or isnull(@updateTable,'') = ''
				set @sql = N'
					select '+(case when @stripHTML=1 then 'dbo.fnCCG_StripHtml(' else '' end)+' IsNull(cc.'+@cacheField+', '''') '+(case when @stripHTML=1 then ')' else '' end)+'
						from CCG_PAT_CustomColumns cc
						where cc.PayableSeq = '+cast(@payableSeq as varchar(20))
			else
				set @sql = N'
					select '+(case when @stripHTML=1 then 'dbo.fnCCG_StripHtml(' else '' end)+' IsNull(t.'+@updateField+', cc.'+@cacheField+') '+(case when @stripHTML=1 then ')' else '' end)+'
						from '+@updateTable+' t
							outer apply (select * from CCG_PAT_CustomColumns cc where cc.PayableSeq = '+cast(@payableSeq as varchar(20))+') cc
						where t.WBS1 = N'''+@wbs1+''' and t.WBS2 = N'''+@wbs2+''' and t.WBS3 = N'''+@wbs3+''''
		else
			set @sql = N'
				select '+(case when @stripHTML=1 then 'dbo.fnCCG_StripHtml(' else '' end) + @updateField + (case when @stripHTML=1 then ')' else '' end)+'
					from '+@updateTable+' t
					where t.WBS1 = N'''+@wbs1+''' and t.WBS2 = N'''+@wbs2+''' and t.WBS3 = N'''+@wbs3+''''

	exec (@sql)
END
GO
