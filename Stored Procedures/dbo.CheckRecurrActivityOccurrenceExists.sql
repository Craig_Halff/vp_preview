SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create Proc [dbo].[CheckRecurrActivityOccurrenceExists]
(
	@activityID varchar(32)
	,@startDate datetime 
)
As
Begin 
	Declare @val bit =0 , @recurrEndType char(1) , @recurrenceInd char(1)

	Select @recurrEndType = RecurrEndType , @recurrenceInd = RecurrenceInd from Activity where ActivityID = @activityID
	
	If @recurrenceInd = 'N' 
	Begin
		Set @val =0
	End 
	Else If @recurrEndType='N' 
	Begin
		Set @val=1
	End
	Else 
	Begin
		If Exists (Select 1 from AllActivities where ActivityID = @activityID and StartDate != @startDate) Set @val=1
	End
	
	Return (@val)
End
GO
