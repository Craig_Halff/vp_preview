SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_TeamGoalsAbsorption] @UDIC_UID varchar (32)
AS 
/* 
Copyright (c) 2019 Central Consulting Group.  All rights reserved.
08/15/2019	David Springer
			Update UDIC_TargetGoals Team Absorption
			Call this from an Target Goals scheduled workflow.
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
BEGIN

	Update t
	Set t.CustTeamChargeableCost = Round(a.Absorption,0)
	From UDIC_TeamGoal t,
	(Select os.CustOrganization Org, Sum (UtilizationRatio / 100 * JobCostRate * HoursPerDay * 5) Absorption
	From EM e, UDIC_OrganizationStructure os, CFGFormat f
	Where Status = 'A'
	  and Right (Org, f.Org3Length) = Right (os.CustOrganization, f.Org3Length)
	  and os.CustPrimaryTeamLocation = 'Y'
	  and e.Type <> 'C'
	Group by os.CustOrganization
	) a
	Where t.UDIC_UID = @UDIC_UID
	  and t.CustOrganization = a.Org

END
GO
