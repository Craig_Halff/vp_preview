SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_AddDisplayColumn60] ( @OverridePriorSettings char(1), @Header varchar(255), @FieldName varchar(255), @ColType char(1), @DrilldownColumn char(1), @ColSeq int, @ColDecimals int, @ColWidth int= 0, @ColColumnName varchar(100)='', @ColViewRoles varchar(255)='', @ColEnabledWBS1 char(1)='Y', @ColEnabledWBS2 char(1)='Y', @ColEnabledWBS3 char(1)='Y', @ColStatus char(1)='Y', @Link varchar(250)='')
             AS EXEC spCCG_EI_AddDisplayColumn60 @OverridePriorSettings,@Header,@FieldName,@ColType,@DrilldownColumn,@ColSeq,@ColDecimals,@ColWidth,@ColColumnName,@ColViewRoles,@ColEnabledWBS1,@ColEnabledWBS2,@ColEnabledWBS3,@ColStatus,@Link
GO
