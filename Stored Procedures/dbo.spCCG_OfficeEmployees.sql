SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_OfficeEmployees] @Org varchar (25)
AS 
/* 
Copyright (c) 2019 Central Consulting Group.  All rights reserved.
12/06/2019	David Springer
			Populate UDIC_Office Employees grid
			Call this from an Employee INSERT or CHANGE workflow when Status or Title has changed
12/09/2019	David Springer
			Added Full Time, Part Time & Contract summaries
03/16/2020	David Springer
			Added spCCG_OfficeFirmSync
*/
DECLARE @OfficeCode varchar (4) = substring (@Org, 4, 2),
		@UDIC_UID	varchar (32)
BEGIN
SET NOCOUNT ON

	SELECT @UDIC_UID = UDIC_UID FROM dbo.UDIC_Office WHERE CustOfficeNumber = @OfficeCode

	Delete s From UDIC_Office o, UDIC_Office_Employees s Where o.UDIC_UID = s.UDIC_UID and o.CustOfficeNumber = @OfficeCode

	Insert Into UDIC_Office_Employees
	(UDIC_UID, Seq, CustEmployee, CustEmployeeLastFirst, CustEmployeeTitle)
	Select o.UDIC_UID, Replace (NewID(), '-', ''), e.Employee, e.LastName + e.FirstName, e.Title
	From EM e, UDIC_Office o
	Where e.Status = 'A'
	  and e.Org not like '02%'
	  and substring (e.Org, 4, 2) = o.CustOfficeNumber
	  and o.CustOfficeNumber = @OfficeCode


--	Employment Type Summary
	Update o
	Set o.CustFullTimeEmployees = IsNull (e.Qty, 0)
	From UDIC_Office o
		Left Join
		(Select substring (Org, 4, 2) Office, Count (*) Qty
		From EM e, EmployeeCustomTabFields ex
		Where e.Employee = ex.Employee
		  and e.Status = 'A'
		  and e.Org not like '02%'
		  and ex.CustEmploymentType = 'Full Time'
		Group by substring (Org, 4, 2)) e on e.Office = o.CustOfficeNumber
	Where o.CustOfficeNumber = @OfficeCode

	Update o
	Set o.CustPartTimeEmployees = IsNull (e.Qty, 0)
	From UDIC_Office o
		Left Join
		(Select substring (Org, 4, 2) Office, Count (*) Qty
		From EM e, EmployeeCustomTabFields ex
		Where e.Employee = ex.Employee
		  and e.Status = 'A'
		  and e.Org not like '02%'
		  and ex.CustEmploymentType = 'Part Time'
		Group by substring (Org, 4, 2)) e on e.Office = o.CustOfficeNumber
	Where o.CustOfficeNumber = @OfficeCode

	Update o
	Set o.CustContractEmployees = IsNull (e.Qty, 0)
	From UDIC_Office o
		Left Join
		(Select substring (Org, 4, 2) Office, Count (*) Qty
		From EM e, EmployeeCustomTabFields ex
		Where e.Employee = ex.Employee
		  and e.Status = 'A'
		  and e.Org not like '02%'
		  and ex.CustEmploymentType = 'Contract'
		Group by substring (Org, 4, 2)) e on e.Office = o.CustOfficeNumber
	Where o.CustOfficeNumber = @OfficeCode

	Update UDIC_Office Set CustTotalEmployees = CustFullTimeEmployees + CustPartTimeEmployees + CustContractEmployees
	Where CustOfficeNumber = @OfficeCode

--	Corporate
	Update o
	Set o.CustFullTimeEmployees = IsNull (e.Qty, 0)
	From UDIC_Office o
		Left Join
		(Select Count (*) Qty
		From EM e, EmployeeCustomTabFields ex
		Where e.Employee = ex.Employee
		  and e.Status = 'A'
		  and e.Org not like '02%'
		  and ex.CustEmploymentType = 'Full Time') e on 1 = 1
	Where o.CustOfficeNumber = '00'

	Update o
	Set o.CustPartTimeEmployees = IsNull (e.Qty, 0)
	From UDIC_Office o
		Left Join
		(Select Count (*) Qty
		From EM e, EmployeeCustomTabFields ex
		Where e.Employee = ex.Employee
		  and e.Status = 'A'
		  and e.Org not like '02%'
		  and ex.CustEmploymentType = 'Part Time') e on 1 = 1
	Where o.CustOfficeNumber = '00'

	Update o
	Set o.CustContractEmployees = IsNull (e.Qty, 0)
	From UDIC_Office o
		Left Join
		(Select Count (*) Qty
		From EM e, EmployeeCustomTabFields ex
		Where e.Employee = ex.Employee
		  and e.Status = 'A'
		  and e.Org not like '02%'
		  and ex.CustEmploymentType = 'Contract') e on 1 = 1
	Where o.CustOfficeNumber = '00'

	Update UDIC_Office Set CustTotalEmployees = CustFullTimeEmployees + CustPartTimeEmployees + CustContractEmployees
	Where CustOfficeNumber = '00'

	EXEC dbo.spCCG_OfficeFirmSync @UDIC_UID
	
END
GO
