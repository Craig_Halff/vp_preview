SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Load_Rights_Description]
	@StageFlow varchar(50),
	@Stage varchar(50),
	@Role varchar(50),
	@UICultureName varchar(10)
AS
BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_EI_Load_Rights_Description] 'Default','Review','Reviewer','en-US'
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT *
        FROM  CCG_EI_ConfigRightsDescriptions
		WHERE StageFlow = @StageFlow and Stage = @Stage and [Role] = @Role and UICultureName = @UICultureName and ISNULL(EmailSubject,'')<>''

END;
GO
