SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[HAI_QAErrorPercentages]
    @QAReview1Goal NVARCHAR(10),
    @QAReview2Goal NVARCHAR(10),
    @QAReview3Goal NVARCHAR(10)
AS
    DECLARE @Error1 NVARCHAR(255) = N'QA Review 1 Goal must have a percentage.                                                                                                                                    ';
    DECLARE @Error2 NVARCHAR(255) = N'Please select N/A from the bottom of the list if Review 2 is not required.                                                                                                  ';
	DECLARE @Error3 NVARCHAR(255) = N'Please select N/A from the bottom of the list if Review 3 is not required.                                                                                                  ';
	DECLARE @Error4 NVARCHAR(255) = N'The percentage for QA Review 2 Goal must be greater than the percentage for QA Review 1.  Please select N/A from the bottom of the list if Review 2 is not required.                                                                                    ';
    DECLARE @Error5 NVARCHAR(255) = N'The percentage for QA Review 3 Goal must be greater than the percentage for QA Review 2.  Please select N/A from the bottom of the list if Review 3 is not required.                                                                                    ';
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
	IF @QAReview1Goal = ''
		RAISERROR(@Error1, 16, 1);

	IF @QAReview2Goal = ''
		RAISERROR(@Error2, 16, 1);

	IF @QAReview3Goal = ''
		RAISERROR(@Error3, 16, 1);

	IF CAST(REPLACE(@QAReview1Goal, '%', '') as int) >= CAST(REPLACE(@QAReview2Goal, '%', '') as int)
		RAISERROR(@Error4, 16, 1);
 
	IF CAST(REPLACE(@QAReview2Goal, '%', '') as int) >= CAST(REPLACE(@QAReview3Goal, '%', '') + 1 as int)
		RAISERROR(@Error5, 16, 1);
		
END

GO
