SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPSumUpFees]
  @strPlanID varchar(32)
AS

BEGIN -- Procedure stRPSumUpFees

  SET NOCOUNT ON

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)
  DECLARE @strAutoSumComp varchar(1)

  DECLARE @strProjectCurrencyCode nvarchar(3)
  DECLARE @strBillingCurrencyCode nvarchar(3)
  DECLARE @strFunctionalCurrencyCode nvarchar(3)
  DECLARE @strCompany nvarchar(14)
  DECLARE @strWBS1 nvarchar(30)
  DECLARE @strUserName nvarchar(32)

  DECLARE @tiMaxWBSLevel tinyint = 1

  DECLARE @tabTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    WBSType varchar(4) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int,
    ChildrenCount int,
    CompensationFeeDirLab decimal(19, 4),
    CompensationFeeDirLabBill decimal(19, 4),
    CompensationFeeDirExp decimal(19, 4),
    CompensationFeeDirExpBill decimal(19, 4),
    ReimbAllowanceExp decimal(19, 4),
    ReimbAllowanceExpBill decimal(19, 4),
    ReimbAllowanceCon decimal(19, 4),
    ReimbAllowanceConBill decimal(19, 4),
    CompensationFee decimal(19, 4),
    CompensationFeeBill decimal(19, 4),
    ConsultantFee decimal(19, 4),
    ConsultantFeeBill decimal(19, 4),
    ReimbAllowance decimal(19, 4),
    ReimbAllowanceBill decimal(19, 4)
    UNIQUE (PlanID, TaskID, ParentOutlineNumber, OutlineNumber)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, AutoSumComp, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled,
    @strAutoSumComp = AutoSumComp 
    FROM FW_CFGSystem

  -- If AutoSumComp flag is not 'Y' then there is no point in going further, bail out.

  IF (@strAutoSumComp <> 'Y') RETURN

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))
  SELECT @strUserName = CASE WHEN DATALENGTH(@strUserName) = 0 THEN N'stRPSumUpFees' ELSE N'SUF_' + @strUserName END

  -- Determine the maximum WBS Level

  SELECT @tiMaxWBSLevel =
    CASE
      WHEN WBS2Length > 0 AND WBS3Length = 0 THEN 2
      WHEN WBS3Length > 0 THEN 3
    END
    FROM CFGFormat

  -- Get Currency Codes from Project and CFGMainData.

  SELECT
    @strCompany = Company,
    @strWBS1 = WBS1
    FROM PNPlan
    WHERE PlanID = @strPlanID

  SELECT
    @strFunctionalCurrencyCode = CASE WHEN @strMultiCompanyEnabled = 'N' THEN ' ' ELSE FunctionalCurrencyCode END
    FROM CFGMainData
    WHERE Company = @strCompany

  SELECT
    @strProjectCurrencyCode = ProjectCurrencyCode,
    @strBillingCurrencyCode = CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN BillingCurrencyCode ELSE ProjectCurrencyCode END
  FROM PR
  WHERE WBS1 = @strWBS1 AND WBS2 = ' ' AND WBS3 = ' '

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Insert leaf Task.

  INSERT @tabTask(
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    WBSType,
    ParentOutlineNumber,
    OutlineNumber,
    OutlineLevel,
    ChildrenCount,
    CompensationFeeDirLab,
    CompensationFeeDirLabBill,
    CompensationFeeDirExp,
    CompensationFeeDirExpBill,
    ReimbAllowanceExp,
    ReimbAllowanceExpBill,
    ReimbAllowanceCon,
    ReimbAllowanceConBill,
    CompensationFee,
    CompensationFeeBill,
    ConsultantFee,
    ConsultantFeeBill,
    ReimbAllowance,
    ReimbAllowanceBill
  )
    SELECT
      T.PlanID AS PlanID,
      T.TaskID AS TaskID,
      T.WBS1 AS WBS1,
      T.WBS2 AS WBS2,
      T.WBS3 AS WBS3,
      T.WBSType AS WBSType,
      T.ParentOutlineNumber AS ParentOutlineNumber,
      T.OutlineNumber AS OutlineNumber,
      T.OutlineLevel AS OutlineLevel,
      T.ChildrenCount AS ChildrenCount,
      T.CompensationFeeDirLab AS CompensationFeeDirLab,
      T.CompensationFeeDirLabBill AS CompensationFeeDirLabBill,
      T.CompensationFeeDirExp AS CompensationFeeDirExp,
      T.CompensationFeeDirExpBill AS CompensationFeeDirExpBill,
      T.ReimbAllowanceExp AS ReimbAllowanceExp,
      T.ReimbAllowanceExpBill AS ReimbAllowanceExpBill,
      T.ReimbAllowanceCon AS ReimbAllowanceCon,
      T.ReimbAllowanceConBill AS ReimbAllowanceConBill,
      (T.CompensationFeeDirLab + T.CompensationFeeDirExp) AS CompensationFee,
      (T.CompensationFeeDirLabBill + T.CompensationFeeDirExpBill) AS CompensationFeeBill,
      T.ConsultantFee AS ConsultantFee,
      T.ConsultantFeeBill AS ConsultantFeeBill,
      (T.ReimbAllowanceExp + T.ReimbAllowanceCon) AS ReimbAllowance,
      (T.ReimbAllowanceExpBill + T.ReimbAllowanceConBill) AS ReimbAllowanceBill
      FROM PNTask AS T
      WHERE T.PlanID = @strPlanID AND T.ChildrenCount = 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Summing up Fees from leaf Tasks to parent Tasks.

  INSERT @tabTask(
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    WBSType,
    ParentOutlineNumber,
    OutlineNumber,
    OutlineLevel,
    ChildrenCount,
    CompensationFeeDirLab,
    CompensationFeeDirLabBill,
    CompensationFeeDirExp,
    CompensationFeeDirExpBill,
    ReimbAllowanceExp,
    ReimbAllowanceExpBill,
    ReimbAllowanceCon,
    ReimbAllowanceConBill,
    CompensationFee,
    CompensationFeeBill,
    ConsultantFee,
    ConsultantFeeBill,
    ReimbAllowance,
    ReimbAllowanceBill
  )
    SELECT 
      PT.PlanID AS PlanID,
      PT.TaskID AS TaskID,
      PT.WBS1 AS WBS1,
      PT.WBS2 AS WBS2,
      PT.WBS3 AS WBS3,
      PT.WBSType AS WBSType,
      PT.ParentOutlineNumber AS ParentOutlineNumber,
      PT.OutlineNumber AS OutlineNumber,
      PT.OutlineLevel AS OutlineLevel,
      PT.ChildrenCount AS ChildrenCount,
      SUM(CT.CompensationFeeDirLab) AS CompensationFeeDirLab,
      SUM(CT.CompensationFeeDirLabBill) AS CompensationFeeDirLabBill,
      SUM(CT.CompensationFeeDirExp) AS CompensationFeeDirExp,
      SUM(CT.CompensationFeeDirExpBill) AS CompensationFeeDirExpBill,
      SUM(CT.ReimbAllowanceExp) AS ReimbAllowanceExp,
      SUM(CT.ReimbAllowanceExpBill) AS ReimbAllowanceExpBill,
      SUM(CT.ReimbAllowanceCon) AS ReimbAllowanceCon,
      SUM(CT.ReimbAllowanceConBill) AS ReimbAllowanceConBill,
      SUM(CT.CompensationFee) AS CompensationFee,
      SUM(CT.CompensationFeeBill) AS CompensationFeeBill,
      SUM(CT.ConsultantFee) AS ConsultantFee,
      SUM(CT.ConsultantFeeBill) AS ConsultantFeeBill,
      SUM(CT.ReimbAllowance) AS ReimbAllowance,
      SUM(CT.ReimbAllowanceBill) AS ReimbAllowanceBill
      FROM PNTask AS PT
        INNER JOIN @tabTask AS CT ON PT.PlanID = CT.PlanID AND CT.ChildrenCount = 0
          AND CT.OutlineNumber LIKE PT.OutlineNumber + '%'
      WHERE PT.PlanID = @strPlanID AND PT.ChildrenCount > 0
      GROUP BY PT.PlanID, PT.TaskID, PT.WBS1, PT.WBS2, PT.WBS3, PT.WBSType,
        PT.ParentOutlineNumber, PT.OutlineNumber, PT.OutlineLevel, PT.ChildrenCount

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Update CompensationFee and ReimbAllowance for leaf PNTask rows.

  UPDATE T SET
    CompensationFee = PT.CompensationFee,
    CompensationFeeBill = PT.CompensationFeeBill,
    ReimbAllowance = PT.ReimbAllowance,
    ReimbAllowanceBill = PT.ReimbAllowanceBill,
    ModUser = @strUserName,
    ModDate = LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19)
  FROM PNTask AS T
    INNER JOIN @tabTask AS PT 
      ON T.PlanID = PT.PlanID AND T.TaskID = PT.TaskID AND PT.ChildrenCount = 0 /* leaf rows only */

  -- Update Fees for non-leaf PNTask rows.

  UPDATE T SET
    CompensationFeeDirLab = PT.CompensationFeeDirLab,
    CompensationFeeDirLabBill = PT.CompensationFeeDirLabBill,
    CompensationFeeDirExp = PT.CompensationFeeDirExp,
    CompensationFeeDirExpBill = PT.CompensationFeeDirExpBill,
    ReimbAllowanceExp = PT.ReimbAllowanceExp,
    ReimbAllowanceExpBill = PT.ReimbAllowanceExpBill,
    ReimbAllowanceCon = PT.ReimbAllowanceCon,
    ReimbAllowanceConBill = PT.ReimbAllowanceConBill,
    CompensationFee = PT.CompensationFee,
    CompensationFeeBill = PT.CompensationFeeBill,
    ConsultantFee = PT.ConsultantFee,
    ConsultantFeeBill = PT.ConsultantFeeBill,
    ReimbAllowance = PT.ReimbAllowance,
    ReimbAllowanceBill = PT.ReimbAllowanceBill,
    ModUser = @strUserName,
    ModDate = LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19)
  FROM PNTask AS T
    INNER JOIN @tabTask AS PT 
      ON T.PlanID = PT.PlanID AND T.TaskID = PT.TaskID AND PT.ChildrenCount > 0 /* non-leaf rows only */

  -- Update Fees for PNPlan.

  UPDATE P SET
    CompensationFeeDirLab = TT.CompensationFeeDirLab,
    CompensationFeeDirLabBill = TT.CompensationFeeDirLabBill,
    CompensationFeeDirExp = TT.CompensationFeeDirExp,
    CompensationFeeDirExpBill = TT.CompensationFeeDirExpBill,
    ReimbAllowanceExp = TT.ReimbAllowanceExp,
    ReimbAllowanceExpBill = TT.ReimbAllowanceExpBill,
    ReimbAllowanceCon = TT.ReimbAllowanceCon,
    ReimbAllowanceConBill = TT.ReimbAllowanceConBill,
    CompensationFee = TT.CompensationFee,
    CompensationFeeBill = TT.CompensationFeeBill,
    ConsultantFee = TT.ConsultantFee,
    ConsultantFeeBill = TT.ConsultantFeeBill,
    ReimbAllowance = TT.ReimbAllowance,
    ReimbAllowanceBill = TT.ReimbAllowanceBill
  FROM PNPlan AS P
    INNER JOIN @tabTask AS TT 
      ON P.PlanID = TT.PlanID AND TT.ParentOutlineNumber IS NULL AND TT.OutlineLevel = 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Update Fees for all RPTask rows.

  UPDATE T SET
    CompensationFeeDirLab = PT.CompensationFeeDirLab,
    CompensationFeeDirLabBill = PT.CompensationFeeDirLabBill,
    CompensationFeeDirExp = PT.CompensationFeeDirExp,
    CompensationFeeDirExpBill = PT.CompensationFeeDirExpBill,
    ReimbAllowanceExp = PT.ReimbAllowanceExp,
    ReimbAllowanceExpBill = PT.ReimbAllowanceExpBill,
    ReimbAllowanceCon = PT.ReimbAllowanceCon,
    ReimbAllowanceConBill = PT.ReimbAllowanceConBill,
    CompensationFee = PT.CompensationFee,
    CompensationFeeBill = PT.CompensationFeeBill,
    ConsultantFee = PT.ConsultantFee,
    ConsultantFeeBill = PT.ConsultantFeeBill,
    ReimbAllowance = PT.ReimbAllowance,
    ReimbAllowanceBill = PT.ReimbAllowanceBill,
    ModUser = @strUserName,
    ModDate = LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19)
  FROM RPTask AS T
    INNER JOIN @tabTask AS PT 
      ON T.PlanID = PT.PlanID AND T.TaskID = PT.TaskID

  -- Update Fees for RPPlan.

  UPDATE P SET
    CompensationFeeDirLab = TT.CompensationFeeDirLab,
    CompensationFeeDirLabBill = TT.CompensationFeeDirLabBill,
    CompensationFeeDirExp = TT.CompensationFeeDirExp,
    CompensationFeeDirExpBill = TT.CompensationFeeDirExpBill,
    ReimbAllowanceExp = TT.ReimbAllowanceExp,
    ReimbAllowanceExpBill = TT.ReimbAllowanceExpBill,
    ReimbAllowanceCon = TT.ReimbAllowanceCon,
    ReimbAllowanceConBill = TT.ReimbAllowanceConBill,
    CompensationFee = TT.CompensationFee,
    CompensationFeeBill = TT.CompensationFeeBill,
    ConsultantFee = TT.ConsultantFee,
    ConsultantFeeBill = TT.ConsultantFeeBill,
    ReimbAllowance = TT.ReimbAllowance,
    ReimbAllowanceBill = TT.ReimbAllowanceBill
  FROM RPPlan AS P
    INNER JOIN @tabTask AS TT 
      ON P.PlanID = TT.PlanID AND TT.ParentOutlineNumber IS NULL AND TT.OutlineLevel = 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Update Fees for PR rows.
  -- Must update for all 3 Currencies: {Project, Billing, Functional}

  UPDATE PR SET

    Fee = PT.CompensationFee,
    ConsultFee = PT.ConsultantFee,
    ReimbAllow = PT.ReimbAllowance,

    FeeBillingCurrency = PT.CompensationFeeBill,
    ConsultFeeBillingCurrency = PT.ConsultantFeeBill,
    ReimbAllowBillingCurrency = PT.ReimbAllowanceBill,

    FeeFunctionalCurrency = 
      CASE
        WHEN @strFunctionalCurrencyCode = @strProjectCurrencyCode THEN PT.CompensationFee
        WHEN @strFunctionalCurrencyCode = @strBillingCurrencyCode THEN PT.CompensationFeeBill
        ELSE 0
      END,

    ConsultFeeFunctionalCurrency = 
      CASE
        WHEN @strFunctionalCurrencyCode = @strProjectCurrencyCode THEN PT.ConsultantFee
        WHEN @strFunctionalCurrencyCode = @strBillingCurrencyCode THEN PT.ConsultantFeeBill
        ELSE 0
      END,

    ReimbAllowFunctionalCurrency = 
      CASE
        WHEN @strFunctionalCurrencyCode = @strProjectCurrencyCode THEN PT.ReimbAllowance
        WHEN @strFunctionalCurrencyCode = @strBillingCurrencyCode THEN PT.ReimbAllowanceBill
        ELSE 0
      END,

    FeeDirLab = PT.CompensationFeeDirLab,
    FeeDirExp = PT.CompensationFeeDirExp,
    ReimbAllowExp = PT.ReimbAllowanceExp,
    ReimbAllowCons = PT.ReimbAllowanceCon,

    FeeDirLabBillingCurrency = PT.CompensationFeeDirLabBill,
    FeeDirExpBillingCurrency = PT.CompensationFeeDirExpBill,
    ReimbAllowExpBillingCurrency = PT.ReimbAllowanceExpBill,
    ReimbAllowConsBillingCurrency = PT.ReimbAllowanceConBill,

    FeeDirLabFunctionalCurrency = 
      CASE
        WHEN @strFunctionalCurrencyCode = @strProjectCurrencyCode THEN PT.CompensationFeeDirLab
        WHEN @strFunctionalCurrencyCode = @strBillingCurrencyCode THEN PT.CompensationFeeDirLabBill
        ELSE 0
      END,
    
    FeeDirExpFunctionalCurrency = 
       CASE
        WHEN @strFunctionalCurrencyCode = @strProjectCurrencyCode THEN PT.CompensationFeeDirExp
        WHEN @strFunctionalCurrencyCode = @strBillingCurrencyCode THEN PT.CompensationFeeDirExpBill
        ELSE 0
      END,

    ReimbAllowExpFunctionalCurrency = 
      CASE
        WHEN @strFunctionalCurrencyCode = @strProjectCurrencyCode THEN PT.ReimbAllowanceExp
        WHEN @strFunctionalCurrencyCode = @strBillingCurrencyCode THEN PT.ReimbAllowanceExpBill
        ELSE 0
      END,

    ReimbAllowConsFunctionalCurrency = 
      CASE
        WHEN @strFunctionalCurrencyCode = @strProjectCurrencyCode THEN PT.ReimbAllowanceCon
        WHEN @strFunctionalCurrencyCode = @strBillingCurrencyCode THEN PT.ReimbAllowanceConBill
        ELSE 0
      END

  FROM PR
    INNER JOIN @tabTask AS PT 
      ON PR.WBS1 = PT.WBS1 AND PR.WBS2 = ISNULL(PT.WBS2, ' ') AND PR.WBS3 = ISNULL(PT.WBS3, ' ')
        AND PT.OutlineLevel <= (@tiMaxWBSLevel - 1) AND PT.WBSType IN ('WBS1', 'WBS2', 'WBS3')

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF
     
END -- stRPSumUpFees
GO
