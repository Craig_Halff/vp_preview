SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Load_Stage_ML] ( @Stage varchar(50), @UICultureName varchar(10))
             AS EXEC spCCG_PAT_Load_Stage_ML @Stage,@UICultureName
GO
