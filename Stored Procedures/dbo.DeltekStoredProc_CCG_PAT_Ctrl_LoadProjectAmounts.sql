SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Ctrl_LoadProjectAmounts] ( @custFields nvarchar(max), @outerApplies nvarchar(max), @useVisionAmountField nvarchar(100), @visionAmounts bit, @originatingVendorEnabled bit, @taxAuditingEnabled bit, @vendorID nvarchar(20), @voucher nvarchar(12), @seq int)
             AS EXEC spCCG_PAT_Ctrl_LoadProjectAmounts @custFields,@outerApplies,@useVisionAmountField,@visionAmounts,@originatingVendorEnabled,@taxAuditingEnabled,@vendorID,@voucher,@seq
GO
