SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_FileHelper_insertXFDFDB] ( @User nvarchar(32), @FILEID varchar(50), @XFDFFile nvarchar(max))
             AS EXEC spCCG_EI_FileHelper_insertXFDFDB @User,@FILEID,@XFDFFile
GO
