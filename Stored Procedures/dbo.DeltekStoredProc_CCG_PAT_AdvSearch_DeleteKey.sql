SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_AdvSearch_DeleteKey]
	@TABLENAME			Nvarchar(200),
	@SAVED_OPTION_KEY	Nvarchar(200),
	@key				Nvarchar(500)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max);

    SET @sql = N'
		DELETE FROM ' + @TABLENAME + '
			WHERE ConfigID = ''' + @key + ''' and Type = ''' + @SAVED_OPTION_KEY + '''';

	EXEC (@sql);

	SELECT @@ROWCOUNT as RowsAffected;
END;

GO
