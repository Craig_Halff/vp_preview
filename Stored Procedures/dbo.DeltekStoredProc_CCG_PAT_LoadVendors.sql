SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_LoadVendors] ( @VendorDisplayFormat nvarchar(max), @Company nvarchar(14), @UnionAlias bit= 1, @ApprovedOnly int= 1)
             AS EXEC spCCG_PAT_LoadVendors @VendorDisplayFormat,@Company,@UnionAlias,@ApprovedOnly
GO
