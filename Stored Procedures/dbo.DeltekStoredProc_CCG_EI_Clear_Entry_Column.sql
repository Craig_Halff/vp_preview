SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Clear_Entry_Column]
	@KEY_EMPLOYEEID			Nvarchar(32),
	@columnDataType			varchar(100),
	@customColumnId			varchar(100),
	@customColumnLabel		Nvarchar(100),
	@databaseField			varchar(500),
	@databaseModEmpField	Nvarchar(500),
	@isDecimalValue			bit,
	@wbs1List				Nvarchar(max)
AS
BEGIN
	/*
	DeltekStoredProc_CCG_EI_Clear_Entry_Column
		@KEY_EMPLOYEEID = '00001',
		@columnDataType = 'A',
		@customColumnId = '291',
		@customColumnLabel = 'Acct Notes!',
		@databaseField = 'AcctNotes',
		@databaseModEmpField = 'AcctNotes_ModEmp',
		@isDecimalValue = '0',
		@wbs1List = '''xxxxx'',''1999015.00'',''2002005.00'',''2002010.00'',''2002019.00'',''2003005.00'''
 	*/
	SET NOCOUNT ON;

	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	bit;

	-- Ensure that our inputs are safe (prevent SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @KEY_EMPLOYEEID);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @columnDataType);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<NUMBER>', @customColumnId);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @customColumnLabel);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<FIELD>', @databaseField);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<FIELD>', @databaseModEmpField);
	SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<CSV>', @wbs1List);
	IF @safeSql = 0 BEGIN
		SELECT 'ERROR: Unsafe parameters used! Execution cancelled.';
		RETURN;
	END;

	SET @sSQL = N'
		INSERT INTO CCG_EI_HistoryUpdateColumns
			(ActionTaken, ActionDate, ActionTakenBy, WBS1, WBS2, WBS3,
				DataType, ConfigCustomColumn, ColumnLabel,
				OldValue, NewValue,
				OldValueDecimal, NewValueDecimal)
			SELECT ''Clear'', getutcdate(), ''' + @KEY_EMPLOYEEID + N''', WBS1, WBS2, WBS3,
				''' + @columnDataType + N''', ' + @customColumnId + N', ''' + @customColumnLabel + N''',
				MAX(' + @databaseField + N'), NULL, ' +
					CASE
						WHEN @isDecimalValue = 1 THEN N'MAX(' + @databaseField + N')'
						ELSE N'NULL'
					END + N', NULL
			FROM CCG_EI_CustomColumns
			WHERE WBS1 in (' + @wbs1List + N') and NOT ' + @databaseField + N' IS NULL
			GROUP BY WBS1, WBS2, WBS3';

	--PRINT @sSQL;
	EXEC (@sSQL);

	SET @sSQL =	N'
		Update CCG_EI_CustomColumns
			SET ' + @databaseField + N' = NULL, ' + @databaseModEmpField + N' = NULL
			Where WBS1 in (' + @wbs1List + N') and NOT ' + @databaseField + N' IS NULL ';

	--PRINT @sSQL;
	EXEC (@sSQL);

	SELECT @@ROWCOUNT;
END;
GO
