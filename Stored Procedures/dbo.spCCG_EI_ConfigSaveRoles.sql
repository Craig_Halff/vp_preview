SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_ConfigSaveRoles] (
	@fieldsSQL			varchar(max),
	@newValuesSql		varchar(max),
	@deleteRolesSql		varchar(max),
	@visionLanguage		varchar(10)
)
AS
BEGIN
	/* Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved. */
	/*
	spCCG_EI_ConfigSaveRoles
		@fieldsSQL = 'Role, VisionField, ColumnLabel, RoleDescription, MessageDelay, HidePdfTools, HighlightColor, MarkupColor, Status, AllowMessaging, AllowInputColControls, AllowResetStageMenu, EditAllAnnot, DocRights, ColumnOrder',
		@newValuesSql = '(''Accounting'', '''', ''Accounting'', ''Accounting'', 0, '';'', ''-4144897'', ''-16776961'', ''A'', ''N'', ''Y'', ''Y'', ''Y'', ''Follow'', -1), (''Reviewer'', ''CustInvoiceReviewer'', ''Reviewer3'', ''Reviewer'', 10, '';'', ''-16192'', ''-65536'', ''A'', ''N'', ''N'', ''N'', ''N'', ''Follow'', 1), (''Approver'', ''CustInvoiceApprover'', ''Approver'', ''Approver'', 240, '';'', ''-4128832'', ''-16728064'', ''A'', ''N'', ''N'', ''N'', ''N'', ''Follow'', 2)',
		@visionLanguage = 'en-US'
	*/
	SET NOCOUNT ON
	declare @sSQL		varchar(max)
	declare @safeSql	int			-- int value indicates which parameter failed the security check

	BEGIN TRANSACTION
	BEGIN TRY
	set @sSQL = '
		merge CCG_EI_ConfigRoles WITH (HOLDLOCK) AS Target
		using (values ' + @newValuesSql + '
			) As Source (' + @fieldsSQL + ')
			on Target.Role = Source.Role
		when matched then
			update set
				[VisionField]			= Source.[VisionField],
				[ColumnLabel]			= Source.[ColumnLabel],
				[ColumnOrder]			= Source.[ColumnOrder],
				[RoleDescription]		= Source.[RoleDescription],
				[MarkupColor]			= Source.[MarkupColor],
				[HighlightColor]		= Source.[HighlightColor],
				[HidePdfTools]			= Source.[HidePdfTools],
				[Status]				= Source.[Status],
				[AllowMessaging]		= Source.[AllowMessaging],
				[AllowInputColControls]	= Source.[AllowInputColControls],
				[AllowResetStageMenu]	= Source.[AllowResetStageMenu],
				[MessageDelay]			= Source.[MessageDelay],
				[EditAllAnnot]			= Source.[EditAllAnnot],
				[DocRights]				= Source.[DocRights]
		when not matched BY TARGET then
			insert (' + @fieldsSQL + ')
			values (Source.' + REPLACE(@fieldsSQL, ', ', ', Source.') + ');'		-- semicolin needed for MERGE
			-- when not matched BY SOURCE then DELETE
	--PRINT (@sSQL)
	exec (@sSQL)

	-- Update the multilingual descriptions table
	set @sSQL = '
		merge CCG_EI_ConfigRolesDescriptions WITH (HOLDLOCK) AS Target
		using CCG_EI_ConfigRoles AS Source on Target.Role = Source.Role
		when matched AND Target.UICultureName = '''+@visionLanguage+''' then
			update set
				[ColumnLabel]		= Source.[ColumnLabel],
				[RoleDescription]	= Source.[RoleDescription]
		when not matched BY TARGET then
			insert (Role, UICultureName, ColumnLabel, RoleDescription)
				values (Source.Role, '''+@visionLanguage+''', Source.ColumnLabel, Source.RoleDescription)
		when not matched BY SOURCE then DELETE;'									-- semicolin needed for MERGE
	exec (@sSQL)

	-- Delete Roles
	if isnull(@deleteRolesSql, '') <> ''
	begin
		set @sSQL = '
			delete from CCG_EI_ConfigRightsDescriptions where Role in (' + @deleteRolesSql + ')
			delete from CCG_EI_ConfigRights where Role in (' + @deleteRolesSql + ')
			delete from CCG_EI_ConfigRoles where Role in (' + @deleteRolesSql + ')'
		exec (@sSQL)
	end

	delete from CCG_EI_SQLPartsCache where [Type] like 'MainGridWhere%' or [Type] like 'RoleFields%' or [Type] = 'RoleCaseClause'

	COMMIT TRANSACTION
	select 0 as result, '' as errormessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		select -1 as result, isnull(error_message(), 'Unknown error') + ' (' + cast(isnull(error_message(), -1) as varchar(20)) + ')' as errormessage
	END CATCH
END
GO
