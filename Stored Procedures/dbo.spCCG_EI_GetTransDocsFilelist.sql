SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_GetTransDocsFilelist](
	@Username	Nvarchar(32), 
	@CorE		char(1), 
	@Invoice	Nvarchar(12), 
	@WBS1		Nvarchar(30),
	@FW_Files	int	= 1				/* 0 or 1 */
)
AS BEGIN
/*
	Copyright (c) 2017 Central Consulting Group.  All rights reserved.

	** USE spCCG_EI_GetTransDocsFilelistOverride to customize this list and avoid EI updates overwriting the stored procedure.
	select * from ledgerap where wbs1='2003005.00' and transtype='ap' and transdate='11/30/12'
	select * from VO where vendor='00DREWTILE' and voucher='0000769'
	select * from billcondetail where mainwbs1='2003005.00'  and transdate='11/30/12'
	exec spCCG_EI_GetTransDocsFilelist 'ADMIN', 'C', '<Draft>', '2003005.00'
	exec spCCG_EI_GetTransDocsFilelist 'ADMIN', 'C', '0001000', '2003005.00'
	exec spCCG_EI_GetTransDocsFilelist 'ADMIN', 'E', '0001000', '2003005.00'
	exec spCCG_EI_GetTransDocsFilelistOverride 'ADMIN', 'E', '0001000', '2003005.00'
*/
	declare @MaxPerMenu		int = 5
	declare @UnbilledLabel	varchar(50)	= 'Unbilled_>_'
	declare @BilledLabel	varchar(50)	= 'Billed_>_'
	declare @dateFormat		smallint	= 101		-- USA mm/dd/yyyy
	declare @T table (
		Seq				int identity(1,1),			/* For updates */
		MenuFolder		varchar(255),				/* Includes cascades - e.g. Billed>2014 */
		MenuLabel		varchar(255),				/* Last menu label after cascades - e.g. Invoice: AJ Carl Engineers 0938 3/19/2013 */
		OnInvoice		char(1),					/* Indicator if the item is on the passed in invoice - e.g. * or blank */
		OrigInvoice		Nvarchar(32),
		Description1	Nvarchar(100),
		Description2	Nvarchar(100),
		FileName		varchar(255),
		TransDate		datetime,
		FileID			uniqueidentifier,
		SourceTable		varchar(32),
		Invoice			Nvarchar(32),
		PayableType		varchar(32),
		TopOrder		smallint
	)

	if @CorE = 'E'
		insert into @T (MenuFolder, MenuLabel, OnInvoice, OrigInvoice, Description1, Description2, 
				FileName, TransDate, FileID, SourceTable, Invoice, PayableType, TopOrder)
			select MenuFolder, MenuLabel, OnInvoice, OrigInvoice, Description1, Description2, 
					FileName, TransDate, FileID, SourceTable, Invoice, PayableType, TopOrder
				from dbo.fnCCG_EI_GetTransDocsFilelist(@Username, @CorE, @Invoice, @WBS1)
				order by TransDate desc
	else if @CorE='C' and exists (select 'x' from sys.objects where object_id = OBJECT_ID(N'[dbo].[CCG_PAT_ProjectAmount]') AND type in (N'U'))
	begin
		insert into @T (MenuFolder, MenuLabel, OnInvoice, OrigInvoice, Description1, Description2, 
				FileName, TransDate, FileID, SourceTable, Invoice, PayableType, TopOrder)
			select MenuFolder, MenuLabel, OnInvoice, OrigInvoice, Description1, Description2, 
					FileName, TransDate, FileID, SourceTable, Invoice, PayableType, TopOrder
				from dbo.fnCCG_EI_GetTransDocsFilelist(@Username, @CorE, @Invoice, @WBS1)
		if @FW_Files = 0
			insert into @T (MenuFolder, MenuLabel, OnInvoice, OrigInvoice, Description1, Description2, 
					FileName, TransDate, FileID, SourceTable, Invoice, PayableType, TopOrder)
				select MenuFolder, MenuLabel, OnInvoice, OrigInvoice, Description1, Description2, 
						FileName, TransDate, FileID, SourceTable, Invoice, PayableType, TopOrder
					from dbo.fnCCG_EI_GetTransDocsFilelistPAT(@Username, @CorE, @Invoice, @WBS1)
					order by TransDate desc
	end
	else
		insert into @T (MenuFolder, MenuLabel, OnInvoice, OrigInvoice, Description1, Description2, 
				FileName, TransDate, FileID, SourceTable, Invoice, PayableType, TopOrder)
			select MenuFolder, MenuLabel, OnInvoice, OrigInvoice, Description1, Description2, 
					FileName, TransDate, FileID, SourceTable, Invoice, PayableType, TopOrder
				from dbo.fnCCG_EI_GetTransDocsFilelist(@Username, @CorE, @Invoice, @WBS1)
				order by TransDate desc

	if @CorE = 'C'
		update @T set MenuFolder = case When IsNull(Invoice,N'<Draft>')=N'<Draft>' Then @UnbilledLabel Else @BilledLabel End,
			MenuLabel = OnInvoice + ' ' + case When IsNull(PayableType,'')<>'' Then PayableType + ': ' Else '' End + IsNull(Description1,'') + ' (#' + IsNull(OrigInvoice,'') + ' - ' + IsNull(Description2,'') + ') ' +
			Convert(varchar, TransDate, 101) + case When Invoice is null Then '' Else ' [CL Inv ' + Invoice + ']' End
	else
		update @T set MenuFolder = case When IsNull(Invoice,N'<Draft>')=N'<Draft>' Then @UnbilledLabel Else @BilledLabel End,
			MenuLabel = OnInvoice + ' ' + IsNull(Description2,'') + ' (' + IsNull(Description1,'') + ') ' +
			Convert(varchar, TransDate, 101) + case When Invoice is null Then '' Else ' [CL Inv ' + Invoice + ']' End

	declare @cnt int, @Seq int, @i int, @TransDate datetime, @lastMenuFolder varchar(100)
	select @cnt=count(*) from @T where menuFolder=@BilledLabel

	if @cnt > @MaxPerMenu
	begin
		declare overCursor cursor fast_forward for select Seq, TransDate from @T where menuFolder=@BilledLabel order by 1
		open overCursor
		fetch next from overCursor into @Seq, @TransDate
		set @i = 1
		set @lastMenuFolder = @BilledLabel + Convert(varchar, @TransDate, @DateFormat)

		while @@fetch_status=0
		begin
			if @i<>0 and @i % @MaxPerMenu = 0
			begin
				update @T set MenuFolder=@lastMenuFolder where Seq<=@Seq and MenuFolder=@BilledLabel
				fetch next from overCursor into @Seq, @TransDate
				set @lastMenuFolder = @BilledLabel + Convert(varchar, @TransDate, @DateFormat)
				set @i = 0
			end
			else
				fetch next from overCursor into @Seq, @TransDate
			set @i = @i + 1
		end

		close overCursor
		deallocate overCursor
		update @T set MenuFolder=@lastMenuFolder where MenuFolder=@BilledLabel
	end

	select @cnt=count(*) from @T where menuFolder=@UnbilledLabel

	if @cnt > @MaxPerMenu
	begin
		declare overCursor cursor fast_forward for select Seq, TransDate from @T where menuFolder=@UnbilledLabel order by 1
		open overCursor
		fetch next from overCursor into @Seq, @TransDate
		set @i = 1
		set @lastMenuFolder = @UnbilledLabel + Convert(varchar, @TransDate, @DateFormat)

		while @@fetch_status=0
		begin
			if @i<>0 and @i % @MaxPerMenu = 0
			begin
				update @T set MenuFolder=@lastMenuFolder where Seq<=@Seq and MenuFolder=@UnbilledLabel
				fetch next from overCursor into @Seq, @TransDate
				set @lastMenuFolder = @UnbilledLabel + Convert(varchar, @TransDate, @DateFormat)
				set @i = 0
			end
			else
				fetch next from overCursor into @Seq, @TransDate
			set @i = @i + 1
		end

		close overCursor
		deallocate overCursor
		update @T set MenuFolder=@lastMenuFolder where MenuFolder=@UnbilledLabel
	end

	-- exec spCCG_EI_GetTransDocsFilelist 'ADMIN', 'E', '0001000', '2003005.00'
	-- Back out "Billed" top menu if only showing billed:
	declare @ShowUnbilledOnlyInCEMenus varchar(1)
	select @ShowUnbilledOnlyInCEMenus=ShowUnbilledOnlyInCEMenus from CCG_EI_Config

	if @ShowUnbilledOnlyInCEMenus='Y'
		update @T set MenuFolder=Replace(MenuFolder,@UnbilledLabel,'')

	select * from @T order by Seq, MenuFolder
END
GO
