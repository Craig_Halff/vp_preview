SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_ProjectMasterContract_Validation]
    @WBS1 VARCHAR(32)
  , @ContractType VARCHAR(32)
AS
    /* =======================================================================================
    Halff Associates, Inc.   All rights reserved.
    20200628 - Craig H. Anderson
            Validate that this project may be set to Master Contract type
   ======================================================================================= */

    BEGIN
        SET NOCOUNT ON;
        DECLARE @errorMessage VARCHAR(MAX) = '';

        -- Check for Sales
        IF EXISTS
            (
                SELECT 'x'
                FROM dbo.PR p
                    LEFT JOIN
                        (
                            SELECT ps.WBS1
                                 , COUNT(*) AS Sales
                            FROM dbo.Projects_Sales ps WITH ( NOLOCK )
                            WHERE ps.WBS1 = @WBS1
                            GROUP BY ps.WBS1
                        )   s
                        ON p.WBS1 = s.WBS1
                WHERE p.WBS1 = @WBS1
                      AND @ContractType <> 'Individual Project/Task Order'
                      AND ISNULL(s.Sales, 0) <> 0
                      AND p.WBS2 = ' '
            )
            SET @errorMessage = CONCAT(@errorMessage, 'Master Contracts may not have Sales records.');


        -- Check for Contracts grid entries
        IF
            (
                SELECT ISNULL(COUNT(*), 0) FROM dbo.Contracts c WHERE c.WBS1 = @WBS1
            ) > 0
            SET @errorMessage = CONCAT(@errorMessage, 'Contract grid must be empty for Master Contracts');

        -- Check for Phases or Tasks
        IF EXISTS
            (
                SELECT 'x'
                FROM dbo.PR p
                    INNER JOIN
                        (
                            SELECT WBS1
                                 , COUNT(*) AS items
                            FROM dbo.PR WITH ( NOLOCK )
                            WHERE WBS1 = @WBS1
                                  AND
                                      (
                                          WBS2 <> ' '
                                          OR WBS3 <> ' '
                                      )
                            GROUP BY WBS1
                        )   ph
                        ON p.WBS1 = ph.WBS1
                WHERE p.WBS1 = @WBS1
                      AND @ContractType <> 'Individual Project/Task Order'
                      AND ph.items <> 0
            )
            SET @errorMessage = CONCAT(@errorMessage, 'Master Contracts may not have Phases or Tasks.');

        -- Check Suffix
        IF EXISTS
            (
                SELECT 'x'
                FROM dbo.PR p
                WHERE p.WBS1 = @WBS1
                      AND RIGHT(ISNULL(p.WBS1, ''), 3) <> '000'
            )
            SET @errorMessage = CONCAT(@errorMessage, 'Master Contract AVO suffix must be "000".  ');

        -- Check Ready For Processing, must be "N"
        IF EXISTS
            (
                SELECT 'x'
                FROM dbo.PR p
                WHERE p.WBS1 = @WBS1
                      AND p.ReadyForProcessing = 'Y'
            )
            SET @errorMessage = CONCAT(@errorMessage, 'Master Contracts must have "Approved for use in Processing" set to "No".  ');

        --Raise Error if triggered
        IF @errorMessage <> ''
            RAISERROR(@errorMessage, 16, 1);

    END;
GO
