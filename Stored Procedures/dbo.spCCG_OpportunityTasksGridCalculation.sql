SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_OpportunityTasksGridCalculation] @OpportunityID varchar (32), @TaskPhaseCode varchar (7)
AS
/*
Copyright 2017 Central Consulting Group.   All rights reserved.
03/10/2017 David Springer
           Validate Task Phase Code exists.
           Sum the Tasks to the Phase grid.
           Call this from an Opportuntiy Task INSERT, CHANGE & DELETE workflows.
*/
SET NOCOUNT ON
BEGIN

-- If there is no Phase to support this Task, raise errro -- add the Phase
   If not exists (Select 'x ' From Opportunities_Phases
                  Where OpportunityID = @OpportunityID
                    and CustPhaseCode = @TaskPhaseCode)
      Raiserror ('Halff Custom Error:  Phase Code does not exist in the Phase grid.  Add it there and save, then add it here.                       ', 16, 1)
/*
      Insert Into Opportunities_Phases
      (OpportunityID, Seq, CustPhaseCode, CustPhaseName)
      Values (@OpportunityID, Replace (NEWID(), '-', ''), @TaskPhaseCode, @TaskPhaseName)
*/
-- Update Phase from Tasks
   Update p
   Set p.CustPhaseFeeDirLab = IsNull (t.Labor, 0),
       p.CustPhaseFeeDirExp = IsNull (t.DirExp, 0),
       p.CustPhaseConsultFee = IsNull (t.ConsultFee, 0),
       p.CustPhaseReimbAllowCons = IsNull (t.ReimbCons, 0),
       p.CustPhaseReimbAllowExp = IsNull (t.ReimbExp, 0)
   From Opportunities_Phases p
        Left Join
        (Select OpportunityID, CustTaskPhaseCode, 
                Sum (CustTaskFeeDirLab) Labor,
                Sum (CustTaskFeeDirExp) DirExp,
                Sum (CustTaskConsultFee) ConsultFee,
                Sum (CustTaskReimbAllowCons) ReimbCons,
                Sum (CustTaskReimbAllowExp) ReimbExp
         From Opportunities_Tasks
         Group by OpportunityID, CustTaskPhaseCode) t on p.OpportunityID = t.OpportunityID and p.CustPhaseCode = t.custTaskPhaseCode
   Where p.OpportunityID = @OpportunityID
     and p.CustPhaseCode = @TaskPhaseCode
     
   Update Opportunities_Phases
   Set CustPhaseTotal = CustPhaseFeeDirLab + CustPhaseFeeDirExp + CustPhaseConsultFee + CustPhaseReimbAllowCons + CustPhaseReimbAllowExp
   Where OpportunityID = @OpportunityID
     and CustPhaseCode = @TaskPhaseCode

/*
-- If there are more than one Billing Type used for this Phase in the Tasks grid
-- Set the Phase Contract Type = Task Terms
   If 1 < (Select Count (Distinct CustTaskBillingType) 
           From Opportunities_Tasks 
           Where OpportunityID = @OpportunityID
             and CustTaskPhaseCode = @TaskPhaseCode)
      
      Update Opportunities_Phases
      Set CustPhaseBillingType = 'Task Terms'
      Where OpportunityID = @OpportunityID
        and CustPhaseCode = @TaskPhaseCode

-- Set Phase Expense Type from the Task Expense Type
   Update t
   Set t.CustPhaseExpenseType = f.ExpType
   From Opportunities_Phases t, 
       (Select OpportunityID, CustTaskPhaseCode, min (CustTaskExpenseType) ExpType
        From Opportunities_Tasks
        Group by OpportunityID, CustTaskPhaseCode) f
   Where t.OpportunityID = @OpportunityID
     and t.OpportunityID = f.OpportunityID
     and t.CustPhaseCode = f.CustTaskPhaseCode
*/

   exec spCCG_OpportunityPhasesGridCalcBottomUp @OpportunityID
END
GO
