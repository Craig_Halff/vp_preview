SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectRevenue]
AS
/*
Copyright (c) 2017 Central Consulting Group. All rights reserved.
05/10/2017 David Springer
           Update Vision Project Revenue Fields from Backlog application.
           Call this on the Project Scheduled workflow / button.
Select * From Backlog.dbo.IVsnRevImport 
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

BEGIN

Update p
	Set p.CustRevenueLabor = LaborAmt,
		p.CustRevenueExpense = ExpenseAmt,
		p.CustRevenueConsultant = ConsultantAmt
	From ProjectCustomTabFields p, HAI_RevenueImport b
	Where p.WBS1 = b.WBS1
	  and p.WBS2 = b.WBS2
	  and p.WBS3 = b.WBS3

END
GO
