SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FormRecur_LoadData]
AS
BEGIN
	SET NOCOUNT ON;
	-- DECLARE @sql Nvarchar(max);

	SELECT r.Seq, r.Company, r.Description, r.Vendor, r.Freq, r.FreqParam, r.Freq as FreqDesc, maxSeq.PayableDate as NextDate,
            case when r.CopyAmount = 'Y' then 'True' else 'False' end as CopyAmount,
            case when r.Status = 'A' then 'True' else 'False' end as Status, r.ModDate, r.ModUser, maxSeq.PayableSeq, maxSeq.PayableDate as PayableDate
        FROM CCG_PAT_Recur r
            LEFT JOIN (
                SELECT RecurSeq, max(Seq) as PayableSeq, max(PayableDate) as PayableDate
                    FROM CCG_PAT_Payable
                    WHERE RecurSeq is not null
                    GROUP BY RecurSeq
            ) maxSeq on r.Seq = maxSeq.RecurSeq
END;

GO
