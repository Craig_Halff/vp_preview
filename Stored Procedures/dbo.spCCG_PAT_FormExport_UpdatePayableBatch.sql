SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_FormExport_UpdatePayableBatch]
	@Batch		Nvarchar(max),
	@IDs		varchar(max)
AS
BEGIN
	-- Copyright (c) 2020 EleVia Software.  All rights reserved. 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

	set @Batch = Replace(@Batch, N'''', N'''''')
	DECLARE @sql nvarchar(max) = N'
		UPDATE CCG_PAT_Payable
			SET Batch = nullif(N'''+@Batch+''', '''')
			WHERE SEQ in (' + @IDs + ') and (ISNULL(batch, '''') = '''' or '''+@Batch+''' = '''') '

	EXEC(@sql)
	SELECT @@ROWCOUNT as RowsAffected
END
GO
