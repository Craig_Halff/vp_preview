SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[pmDelExpense]
  @strPlanID varchar(32),
  @strExpenseID varchar(32)
AS

BEGIN -- Procedure pmDelExpense

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to delete a Expense row and its associated rows.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    
  DELETE RPPlannedExpenses WHERE PlanID = @strPlanID AND ExpenseID = @strExpenseID
  DELETE RPBaselineExpenses WHERE PlanID = @strPlanID AND ExpenseID = @strExpenseID
  DELETE RPExpense WHERE PlanID = @strPlanID AND ExpenseID = @strExpenseID

  SET NOCOUNT OFF

END -- pmDelExpense
GO
