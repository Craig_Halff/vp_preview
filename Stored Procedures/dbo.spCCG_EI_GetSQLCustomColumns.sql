SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_GetSQLCustomColumns](
	@emptyFields	int,					-- 0 or 1
	@levels			int,
	@role			varchar(32),
	@wsEnabled		int,					-- 0 or 1
	@quickLoad		int,					-- 0 or 1
	@sqlPart		varchar(max) output,
	@outerApplies	varchar(max) output,
	@sqlGroupBy		varchar(max) output
)
AS BEGIN
	-- Copyright (c) 2017 EleVia Software.  All rights reserved.
	/*
	delete from CCG_EI_SQLPartsCache
	declare @sqlpart varchar(max), @outerApplies varchar(max), @sqlGroupBy varchar(max)
	EXEC [spCCG_EI_GetSQLCustomColumns] 0, 1, 'ACCT', 0, @sqlPart = @sqlpart OUTPUT, @outerApplies = @outerApplies OUTPUT, @sqlGroupBy = @sqlGroupBy OUTPUT
	print @sqlpart
	print @outerApplies
	print @sqlGroupBy
	--delete from CCG_EI_SQLPartsCache
	*/
	set nocount on
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	declare @newPart varchar(max) = ''
	declare @sqlPartsType varchar(200) = 'CC_Fields_'+CAST(@emptyFields as varchar(10))+CAST(@levels as varchar(10))+(case when isnull(@quickLoad,0) = 1 then '_QL' else '' end) 
	declare @commonFnToCustMap varchar(max) = '|', @fnsAlreadyAdded varchar(max) = '|'
	declare @rtnVal varchar(max) = (select top 1 SQLPart from CCG_EI_SQLPartsCache where [Type] = @sqlPartsType and [Role] = @role),
		@rtnValOA varchar(max) = (select top 1 SQLPart from CCG_EI_SQLPartsCache where [Type] = @sqlPartsType+'_OA' and [Role] = @role),
		@rtnValGroup varchar(max) = (select top 1 SQLPart from CCG_EI_SQLPartsCache where [Type] = @sqlPartsType+'_GB' and [Role] = @role)
	declare @ccAvailableFields table (oname varchar(500), otype varchar(10), colName varchar(500), system_type_id int)

	if isnull(@rtnVal,'') <> '' and isnull(@rtnValGroup,'') <> ''
	begin
		set @sqlPart = @rtnVal
		set @outerApplies = @rtnValOA
		set @sqlGroupBy = @rtnValGroup
		RETURN
	end
	else begin
		set @rtnVal = ''
		set @rtnValOA = ''
		set @rtnValGroup = ''

		declare @DatabaseField	varchar(250),
				@DbColumnName	varchar(100),
				@DataType		varchar(20),
				@InitialValue	varchar(500),
				@Link			varchar(250),
				@Seq			int,
				@ViewRoles		varchar(max),
				@AccountingRole	char(1) = (case when @role in ('ACCT','ROACCT') then 'Y' else 'N' end)

		declare @fnParameters	varchar(200) = (case
			when @levels in (1,9) then 'PR.WBS1, N'' '', N'' '''
			when @levels = 2 then 'PR.WBS1, PR.WBS2, N'' '''
			when @levels = 3 then 'PR.WBS1, PR.WBS2, PR.WBS3' else '' end), @fnParametersSpecific varchar(200)
		declare @linkParameters	varchar(200) = @fnParameters, @linkParametersSpecific varchar(200)

		declare cursorCustomColumns cursor fast_forward for
			select cc.DatabaseField, cc.DbColumnName, cc.DataType, cc.InitialValue, cc.Link, cc.Seq, cc.ViewRoles
				from CCG_EI_ConfigCustomColumns cc
				where Status = 'A' and (@levels <> 2 or EnabledWBS2 <> 'N') and (@levels <> 3 or EnabledWBS3 <> 'N')
					and (@quickLoad = 0 
						or (QuickLoad = 'Y' or (QuickLoad = 'A' and @AccountingRole = 'Y') or (QuickLoad = 'O' and @AccountingRole = 'N'))
					) and (@levels <> 9 or isnull(cc.ColumnUpdate, '') <> '')
			order by DisplayOrder, cc.Seq

		open cursorCustomColumns
		fetch next from cursorCustomColumns into @DatabaseField, @DbColumnName, @DataType, @InitialValue, @Link, @Seq, @ViewRoles
		while @@fetch_status=0
		begin
			-- determine if user role can view this CC
			if isnull(@ViewRoles,'') = ''
				or (@AccountingRole = 'Y' and lower(@ViewRoles) = 'accounting')
				or (@AccountingRole = 'N' and lower(@ViewRoles) = '!accounting')
			begin
				declare @oName varchar(10), @oType varchar(10) = '', @HasParamCol varchar(1), @HasWBS1 varchar(1),
						@HasWBS2 varchar(1), @HasWBS3 varchar(1), @HasUserNameParam	varchar(1), @HasEmployeeParam varchar(1),
						@tableFunction varchar(1) = 'N', @HasUserNameParamLink varchar(1), @HasEmployeeParamLink varchar(1),
						@UserDefinedParameters varchar(1) = 'N', @InitError varchar(max) = '', @InitErrorLink varchar(max) = '',
						@key varchar(30) = 'Cust_'+cast(@Seq as varchar(20)), @keyColumnName varchar(30) = 'Cust_'+cast(@Seq as varchar(20))

				if isnull(@InitialValue,'') <> '' and charindex('(',@InitialValue) > 0
				begin
					set @tableFunction = 'Y'
					set @UserDefinedParameters = 'Y'
					-- If this table function has been used previously, use that InitialValue & seq instead so we don't double-load table functions (SPS for EI 6)
					declare @str varchar(500), @pos int, @pos2 int
					--print 'checking1 ' + @commonFnToCustMap + ' for ' + @InitialValue
					set @pos = CharIndex('|' + @InitialValue + '^', @commonFnToCustMap)
					if @pos > 0
					begin
						-- Reset @InitialValue
						set @str = SubString(@commonFnToCustMap, @pos + 1, CharIndex('|', @commonFnToCustMap, @pos + 1) - @pos - 1)
						set @pos2 = CharIndex('^',@str)
						set @InitialValue = SubString(@str, 1, @pos2 - 1)
						set @key = 'Cust_' + SubString(@str, @pos2 + 1, Len(@str) - @pos2)
						--print 'Reset @InitialValue to ' + @InitialValue
					end else begin
						set @commonFnToCustMap = @commonFnToCustMap + @InitialValue + '^' + Cast(@Seq as varchar) + '|'
					end
	/*
	delete from CCG_EI_SQLPartsCache
	declare @sqlpart varchar(max), @outerApplies varchar(max)
	EXEC [spCCG_EI_GetSQLCustomColumns] 0, 1, 'ACCT', 0, @sqlPart = @sqlpart OUTPUT, @outerApplies = @outerApplies OUTPUT
	print @sqlpart
	print @outerApplies
	--delete from CCG_EI_SQLPartsCache
	*/				end
				else begin
					exec spCCG_EI_ValidateCustomColumn @InitialValue, '', @lookForErrors = 1, @ValidateOnly = 1, @oName = @oName OUTPUT, @oType = @oType OUTPUT,
						@HasParamCol = @HasParamCol OUTPUT, @HasWBS1 = @HasWBS1 OUTPUT, @HasWBS2 = @HasWBS2 OUTPUT,
						@HasWBS3 = @HasWBS3 OUTPUT, @HasUserNameParam = @HasUserNameParam OUTPUT, @HasEmployeeParam = @HasEmployeeParam OUTPUT,
						@InitError = @InitError OUTPUT
					set @fnParametersSpecific = @fnParameters + (case when @HasUserNameParam='Y' then ', N''[:User]''' else '' end)
							+ (case when @HasEmployeeParam='Y' then ', N''[:Employee]''' else '' end)
					if @oType in ('TF','FT','IF') and isnull(@InitialValue,'')<>''
					begin
						set @tableFunction = 'Y'
						-- If this table function has been used previously, use that InitialValue & seq instead so we don't double-load table functions (SPS for EI 6)
						--print 'checking2 ' + @commonFnToCustMap + ' for ' + @InitialValue
						set @pos = CharIndex('|' + @InitialValue + '^', @commonFnToCustMap)
						if @pos > 0
						begin
							-- Reset @InitialValue
							set @str = SubString(@commonFnToCustMap, @pos + 1, CharIndex('|', @commonFnToCustMap, @pos + 1) - @pos - 1)
							--print @str
							set @pos2 = CharIndex('^',@str)
							set @InitialValue = SubString(@str, 1, @pos2 - 1)
							set @key = 'Cust_' + SubString(@str, @pos2 + 1, Len(@str) - @pos2)
							--print 'Reset @InitialValue to ' + @InitialValue
						end else begin
							set @commonFnToCustMap = @commonFnToCustMap + @InitialValue + '^' + Cast(@Seq as varchar) + '|'
						end
					end
				end

				if @tableFunction = 'Y'
				begin
					delete from @ccAvailableFields
					insert into @ccAvailableFields
						exec spCCG_EI_ValidateCustomColumn @InitialValue, '', 0, 2, @oName = @oName OUTPUT, @oType = @oType OUTPUT,
							@HasParamCol = @HasParamCol OUTPUT, @HasWBS1 = @HasWBS1 OUTPUT, @HasWBS2 = @HasWBS2 OUTPUT,
							@HasWBS3 = @HasWBS3 OUTPUT, @HasUserNameParam = @HasUserNameParamLink OUTPUT, @HasEmployeeParam = @HasEmployeeParamLink OUTPUT,
							@InitError = @InitErrorLink OUTPUT
				end

				if isnull(@Link,'') <> ''
				begin
					exec spCCG_EI_ValidateCustomColumn @Link, '', @lookForErrors = 1, @ValidateOnly = 1, @oName = @oName OUTPUT, @oType = @oType OUTPUT,
						@HasParamCol = @HasParamCol OUTPUT, @HasWBS1 = @HasWBS1 OUTPUT, @HasWBS2 = @HasWBS2 OUTPUT,
						@HasWBS3 = @HasWBS3 OUTPUT, @HasUserNameParam = @HasUserNameParamLink OUTPUT, @HasEmployeeParam = @HasEmployeeParamLink OUTPUT,
						@InitError = @InitErrorLink OUTPUT
					set @linkParametersSpecific = @linkParameters + (case when @HasUserNameParamLink='Y' then ', N''[:User]''' else '' end)
						+ (case when @HasEmployeeParamLink='Y' then ', N''[:Employee]''' else '' end)
				end

				declare @preDate varchar(30) = '', @postDate varchar(30) = '', @colValue varchar(100), @colDetail varchar(100),
					@emptySql varchar(20) = '''''', @errorSql varchar(20) = '''ERR'''

                if @DataType = 'D'
                begin
					if @wsEnabled = 1 begin
						set @preDate = 'CONVERT(VARCHAR(10),'
						set @postDate = ', 120)'
					end
					set @emptySql = '''1900-01-01'''
					set @errorSql = '''1900-12-31'''
                end
                else if @DataType = 'T'
				begin
					if @wsEnabled = 1 begin
						set @preDate = 'CONVERT(VARCHAR(19),'
						set @postDate = ', 120)'
					end
					set @emptySql = '''1900-01-01'''
					set @errorSql = '''1900-12-31'''
                end
				else if @DataType = 'I'
				begin
					set @emptySql = '0'
					set @errorSql = '-1'
				end
				else if @DataType = 'N'
				begin
					set @emptySql = '0.0'
					set @errorSql = '-1.0'
				end

				if isnull(@DbColumnName,'') = '' or isnull(@DbColumnName,'') = 'ColValue'
				begin
					if @tableFunction = 'Y' begin
						set @colValue = case when exists (select 'x' from @ccAvailableFields where colName = 'ColValue') then 'ColValue' else '' end
						set @colDetail = case when exists (select 'x' from @ccAvailableFields where colName = 'ColDetail') then 'ColDetail' else '' end
					end
					else begin
						set @colValue = 'ColValue'
						set @colDetail = 'ColDetail'
					end
				end
				else begin
					if @tableFunction = 'Y' begin
						set @colValue = isnull((select top 1 colName from @ccAvailableFields where colName in (@DbColumnName, @DbColumnName + 'Value', @DbColumnName + '_Value')), '')
						set @colDetail = isnull((select top 1 colName from @ccAvailableFields where colName in (@DbColumnName + 'Detail', @DbColumnName + '_Detail')), '')
					end
					else begin
						set @colValue = @DbColumnName + 'Value'
						set @colDetail = @DbColumnName + 'Detail'
					end
				end

				if left(@InitialValue, 2)='fn' set @InitialValue = 'dbo.' + @InitialValue

                if isnull(@DatabaseField,'') <> ''
                begin
					--print @DatabaseField + '@tableFunction=' + ISNULL(@tableFunction,'NULL')
                    -- we have an update column
                    if ISNULL(@tableFunction,'') <> 'Y'
                    begin
                        if isnull(@InitialValue,'')<>'' and left(@InitialValue, 6)='dbo.fn' begin -- show function value when no current value
                            set @rtnValGroup = @rtnValGroup + ', EI_Cust.'+@DatabaseField+'_ModEmp, '+@InitialValue+'('+@fnParametersSpecific+'), EI_Cust.'+@DatabaseField
                            set @rtnVal = @rtnVal + ', case when EI_Cust.'+@DatabaseField+'_ModEmp IS NULL then '+@preDate+' '+@InitialValue+'('+@fnParametersSpecific+') '+@postDate+'
                                ELSE '+@preDate+' EI_Cust.'+@DatabaseField+' '+@postDate+' end as ['+@keyColumnName+']'
						end
                        else begin -- just show current value
							set @rtnValGroup = @rtnValGroup + ', EI_Cust.'+@DatabaseField
                            set @rtnVal = @rtnVal + ', '+@preDate+' EI_Cust.'+@DatabaseField+' '+@postDate+' as ['+@keyColumnName+']'
						end
                    end
                    else begin
						set @rtnValGroup = @rtnValGroup + ', EI_Cust.'+@DatabaseField+'_ModEmp, '+@key+'.'+@colValue+', EI_Cust.'+@DatabaseField+', '+@key+'.'+@colDetail
                        set @rtnVal = @rtnVal + ',
                                CASE WHEN EI_Cust.'+@DatabaseField+'_ModEmp IS NULL THEN '+@preDate+' '+@key+'.'+@colValue+' '+@postDate+' ELSE '+@preDate+' EI_Cust.'+@DatabaseField+' '+@postDate+' END as ['+@keyColumnName+'],
                                CASE WHEN EI_Cust.'+@DatabaseField+'_ModEmp IS NULL OR ISNULL('+@preDate+' '+@key+'.'+@colValue+' '+@postDate+', '+@emptySql+') = ISNULL('+@preDate+' EI_Cust.'+@DatabaseField+' '+@postDate+', '+@emptySql+') THEN
                                    '+@key+'.'+@colDetail+' ELSE '''' END as ['+@keyColumnName+'_DETAIL]'
					end
                end
                else if isnull(@InitialValue,'')<>'' and left(@InitialValue, 6)='dbo.fn'
                begin
                    -- we have a function, but not an update field. Write through columns use this.
                    if @emptyFields = 1 begin
                        set @rtnVal = @rtnVal + ', '+@emptySql+' as ['+@keyColumnName+']'
					end
                    else if isnull(@InitError,'') <> '' begin
                        set @rtnVal = @rtnVal + ', '+@errorSql+' as ['+@keyColumnName+']'
					end
                    else if @tableFunction <> 'Y' begin
						set @rtnValGroup = @rtnValGroup + ', '+@InitialValue+'('+@fnParametersSpecific+')'
                        set @rtnVal = @rtnVal + ', '+@preDate+' '+@InitialValue+'('+@fnParametersSpecific+') '+@postDate+' as ['+@keyColumnName+']'
					end
                    else begin
						set @rtnValGroup = @rtnValGroup + ', '+@key+'.'+@colValue+', '+@key+'.'+@colDetail
                        set @rtnVal = @rtnVal + ', '+@preDate+' '+@key+'.'+@colValue+' '+@postDate+' as ['+@keyColumnName+'], '+@key+'.'+@colDetail+' as ['+@keyColumnName+'_DETAIL]'
					end

                    if isnull(@Link,'') <> ''
                    begin
                        if @emptyFields = 1
                            set @rtnVal = @rtnVal + ', '''' as ['+@key+'_LINK]'
                        else if isnull(@InitErrorLink,'') <> ''
                            set @rtnVal = @rtnVal + ', ''ERR:'+@InitErrorLink+''' as ['+@keyColumnName+'_LINK]'
                        else begin
							set @rtnValGroup = @rtnValGroup + ', '+@Link+'('+@linkParametersSpecific+')'
                            set @rtnVal = @rtnVal + ', '+@Link+'('+@linkParametersSpecific+') as ['+@keyColumnName+'_LINK]'
						end
                    end
					else if @tableFunction = 'Y'
                    begin
						-- Look for a _LINK column in the table function
						declare @linkCol varchar(500) = isnull((select top 1 colName from @ccAvailableFields where colName in (@DbColumnName + 'Link', @DbColumnName + '_Link')), '')
						if @linkCol <> '' begin
							set @rtnValGroup = @rtnValGroup + ', '+@key+'.'+@linkCol
							set @rtnVal = @rtnVal + ', '+@key+'.'+@linkCol+' as ['+@keyColumnName+'_LINK]'
						end
					end
                end
                else begin
                    -- not a function or update field
                    if isnull(@InitialValue,'')=''
                        set @rtnVal = @rtnVal + ', '+@emptySql+' as ['+@keyColumnName+']'
                    else begin
						set @rtnValGroup = @rtnValGroup + ', '+@InitialValue
                        set @rtnVal = @rtnVal + ', '+@InitialValue + ' as ['+@keyColumnName+']'
					end
                end

				--print @InitialValue + ',4 ' + @fnsAlreadyAdded
				if @tableFunction = 'Y' and isnull(@InitialValue,'')<>'' and CharIndex('|' + @InitialValue + '|', @fnsAlreadyAdded) = 0
				begin
					if @UserDefinedParameters = 'Y'
						set @rtnValOA = @rtnValOA + ' OUTER APPLY '+@InitialValue+' as '+@key
					else
						set @rtnValOA = @rtnValOA + ' OUTER APPLY '+@InitialValue+'(PR.WBS1, PR.WBS2, PR.WBS3'+
							(case when @HasUserNameParam='Y' then ', N''[:User]''' else '' end) +
							(case when @HasEmployeeParam='Y' then ', N''[:Employee]''' else '' end) +') as '+@key
					set @fnsAlreadyAdded = @fnsAlreadyAdded + @InitialValue + '|'
				end
			end

			fetch next from cursorCustomColumns into @DatabaseField, @DbColumnName, @DataType, @InitialValue, @Link, @Seq, @ViewRoles
		end
		close cursorCustomColumns
		deallocate cursorCustomColumns
	end
	delete from CCG_EI_SQLPartsCache where [Role] = @role and [Type] like @sqlPartsType+'%'
	insert into CCG_EI_SQLPartsCache ([Role], [Type], [SQLPart]) values (@role, @sqlPartsType, @rtnVal)
	insert into CCG_EI_SQLPartsCache ([Role], [Type], [SQLPart]) values (@role, @sqlPartsType+'_OA', @rtnValOA)
	insert into CCG_EI_SQLPartsCache ([Role], [Type], [SQLPart]) values (@role, @sqlPartsType+'_GB', @rtnValGroup)

	set @sqlPart = @rtnVal
	set @outerApplies = @rtnValOA
	set @sqlGroupBy = @rtnValGroup
END
GO
