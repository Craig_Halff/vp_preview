SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPDelAssignmentForPlan]
  @strPlanID VARCHAR(32)
AS

BEGIN -- Procedure rpReCalc

  SET NOCOUNT ON

  DECLARE @strNextRowID nvarchar(255)
  
  DECLARE csrPlan CURSOR FOR
    SELECT CASE WHEN GenericResourceID is NULL THEN 'E~' + ResourceID + '|' + TaskID ELSE 'G~' + GenericResourceID + '|' + TaskID END
	FROM PNAssignment  WHERE PlanID = @strPlanID


  OPEN csrPlan

  FETCH NEXT FROM csrPlan INTO @strNextRowID

  WHILE (@@FETCH_STATUS = 0)
    BEGIN
    
	  EXECUTE dbo.stRPDelAssignment @strNextRowID, 1
            
      FETCH NEXT FROM csrPlan INTO @strNextRowID

    END -- While


  CLOSE csrPlan
  DEALLOCATE csrPlan

 
END -- stRPDelAssignmentForPlan
GO
