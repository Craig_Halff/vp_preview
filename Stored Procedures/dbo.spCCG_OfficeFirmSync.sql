SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_OfficeFirmSync] @UDIC_UID varchar (32)
AS 
/* 
Copyright (c) 2020 Central Consulting Group.  All rights reserved.
03/16/2020	David Springer
			Sync Firms configuration table with Office info center.
			Call this from an Office INSERT or CHANGE workflow
			Called from spCCG_OfficeEmployees
Select * From Firm
Select * From dbo.FirmDisciplinesSF330
SELECT * FROM dbo.UDIC_Office_Skills
*/
BEGIN
SET NOCOUNT ON

	UPDATE f
	SET f.DateEstablished = o.CustDateEstablished,
		f.Address1 = o.CustAddress,
		f.City = o.CustCity,
		f.State = o.CustState,
		f.Zip = o.CustZip,
		f.OfficePhone = o.CustMainPhone,
		f.TotalPersonnel = o.CustTotalEmployees
	FROM dbo.UDIC_Office o, dbo.Firm f
	WHERE o.UDIC_UID = @UDIC_UID
	  AND o.CustOfficeName = f.OfficeLocation

	INSERT INTO dbo.Firm
	(FirmID, CreateDate, CreateUser, Name, OfficeLocation, Address1, City, State, Zip, Country, 
		OfficePhone, OfficePhoneFormat, ParentInd, OwnershipType, DateEstablished)
	SELECT Replace (NEWID(), '-', '') ID, GETDATE(), 'CCG Procedure', 'Halff Associates, Inc.', o.CustOfficeName, o.CustAddress, o.CustCity, o.CustState, o.CustZip, 'US', 
		o.CustMainPhone, '(###) ###-####', 'N', 'Inc.', o.CustDateEstablished
	FROM dbo.UDIC_Office o
	WHERE UDIC_UID = @UDIC_UID
	  AND NOT EXISTS (SELECT 'x' FROM dbo.Firm WHERE OfficeLocation = o.CustOfficeName)

END
GO
