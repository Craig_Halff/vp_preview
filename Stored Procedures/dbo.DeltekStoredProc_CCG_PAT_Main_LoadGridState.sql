SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Main_LoadGridState]
	@EMPLOYEEID		Nvarchar(32)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT seq, Detail FROM CCG_PAT_ConfigOptions
        WHERE changedBy = @EMPLOYEEID and type = 'GridOptions'
        ORDER BY datechanged desc
END;

GO
