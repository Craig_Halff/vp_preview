SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectSalesValidation] @WBS1 varchar(32), @SalesDate date
AS 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
/* 
Copyright (c) 2018 Central Consulting Group.  All rights reserved.
08/02/2018 David Springer
           Validate Sales record is only changed in the current month
		   Call this from a Projects_Sales INSERT, CHANGE or DELETE workflow.
*/
BEGIN
 
	If Convert (varchar (6), @SalesDate, 112) <> Convert (varchar (6), getDate(), 112) 
		RAISERROR ('Sales may only be modified in the current month.                                                   ', 16, 1)

END
GO
