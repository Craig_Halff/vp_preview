SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[pmGetRPPlan]
  @strPlanID VARCHAR(32),
  @strETCDate VARCHAR(10) = '', -- Date must be in format: 'yyyymmdd'
  @strRetrievalMode VARCHAR(1) = '3'
AS

BEGIN -- Procedure pmGetRPPlan

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
   
  IF (DATALENGTH(@strETCDate) = 0) SET @strETCDate = CONVERT(VARCHAR, GETDATE(), 112)
  
  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime

  DECLARE @strMultiCompanyEnabled VARCHAR(1)
  
  DECLARE @intHrDecimals int
  DECLARE @intQtyDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intRtCostDecimals int
  DECLARE @intRtBillDecimals int
  DECLARE @intLabRevDecimals int
  DECLARE @intECURevDecimals int

  DECLARE @intBaselineLabCount int
  DECLARE @intBaselineExpCount int
  DECLARE @intBaselineConCount int
  DECLARE @intBaselineUntCount int

  DECLARE @sintAmtDecimals smallint
    
  DECLARE @strUserName Nvarchar(32)

  DECLARE @strExpTab varchar(1)
  DECLARE @strConTab varchar(1)
  DECLARE @strUntTab varchar(1)

  DECLARE @strPlanCompany Nvarchar(14)
  DECLARE @strBudgetType varchar(1)

  DECLARE @siGRMethod smallint
  DECLARE @intGRBillTableNo int
  DECLARE @intGenResTableNo int
  
  DECLARE @strUnposted varchar(1)
  DECLARE @strCommitmentFlg varchar(1)
  
  DECLARE @decJTDLabHrs decimal(19, 4)
  DECLARE @decJTDLabCost decimal(19, 4)
  DECLARE @decJTDLabBill decimal(19, 4)

  DECLARE @decJTDExpCost decimal(19, 4)
  DECLARE @decJTDExpBill decimal(19, 4)
  DECLARE @decJTDDirExpCost decimal(19, 4)
  DECLARE @decJTDDirExpBill decimal(19, 4)
  DECLARE @decJTDReimExpCost decimal(19, 4)
  DECLARE @decJTDReimExpBill decimal(19, 4)

  DECLARE @decJTDConCost decimal(19, 4)
  DECLARE @decJTDConBill decimal(19, 4)
  DECLARE @decJTDDirConCost decimal(19, 4)
  DECLARE @decJTDDirConBill decimal(19, 4)
  DECLARE @decJTDReimConCost decimal(19, 4)
  DECLARE @decJTDReimConBill decimal(19, 4)

  DECLARE @decJTDUntQty decimal(19, 4)
  DECLARE @decJTDUntCost decimal(19, 4)
  DECLARE @decJTDUntBill decimal(19, 4)
  DECLARE @decJTDDirUntCost decimal(19, 4)
  DECLARE @decJTDDirUntBill decimal(19, 4)
  DECLARE @decJTDReimUntCost decimal(19, 4)
  DECLARE @decJTDReimUntBill decimal(19, 4)

  DECLARE @decJTDCostRev decimal(19, 4)
  DECLARE @decJTDBillingRev decimal(19, 4)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Declare Temp tables.
           
  DECLARE @tabTopTask 
    TABLE (TaskID varchar(32) COLLATE database_default,
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(7) COLLATE database_default,
           WBS3 Nvarchar(7) COLLATE database_default,
           LaborCode Nvarchar(14) COLLATE database_default
           PRIMARY KEY(TaskID))

  DECLARE @tabBTRLTCodes 
    TABLE (LaborCodeMask Nvarchar(14) COLLATE database_default,
           Description Nvarchar(40) COLLATE database_default
           PRIMARY KEY(LaborCodeMask)) 
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get MultiCompanyEnabled Flag.
  
  SELECT @strMultiCompanyEnabled = MultiCompanyEnabled FROM FW_CFGSystem
            
  -- Save User Name string for use later.
  
  SET @strUserName = dbo.GetVisionAuditUserName()
          
  -- Get decimal settings.
  
  SELECT @intHrDecimals = HrDecimals,
         @intQtyDecimals = QtyDecimals,
         @intAmtCostDecimals = AmtCostDecimals,
         @intAmtBillDecimals = AmtBillDecimals,
         @intRtCostDecimals = RtCostDecimals,
         @intRtBillDecimals = RtBillDecimals,
         @intLabRevDecimals = LabRevDecimals, 
         @intECURevDecimals = ECURevDecimals
    FROM dbo.RP$tabDecimals(@strPlanID)
    
  -- Get Plan's settings.
  
  SELECT 
    @strPlanCompany = Company,
    @strBudgetType = BudgetType,
    @siGRMethod = GRMethod,
    @intGRBillTableNo = GRBillTableNo,
    @intGenResTableNo = GenResTableNo,
    @strUnposted = UnpostedFlg,
    @strCommitmentFlg = CommitmentFlg
    FROM RPPlan WHERE PlanID = @strPlanID
    
  -- Get Labor Code Description when GRMethod is Labor Code.
 
  IF (@siGRMethod = 1) -- Labor Code
    BEGIN
    
      INSERT @tabBTRLTCodes(LaborCodeMask, Description)
        SELECT DISTINCT LaborCodeMask, MIN(Description) AS Description
        FROM BTRLTCodes
        WHERE TableNo = CASE WHEN @strBudgetType = 'B' THEN @intGRBillTableNo ELSE @intGenResTableNo END
        GROUP BY LaborCodeMask
    
    END --IF (@siGRMethod = 1)

  -- Get flags to determine which features are being used.
  
  SELECT
     @strExpTab = ExpTab,
     @strConTab = ConTab,
     @strUntTab = UntTab,
     @sintAmtDecimals = AmtDecimals
     FROM CFGResourcePlanning
     WHERE CFGResourcePlanning.Company = @strPlanCompany
     
  SELECT 
    @intBaselineExpCount = 0,
    @intBaselineConCount = 0,
    @intBaselineUntCount = 0

    SET @intBaselineLabCount = CASE WHEN EXISTS (SELECT 'X' FROM RPBaselineLabor WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
    IF (@strExpTab = 'Y') SET @intBaselineExpCount = CASE WHEN EXISTS (SELECT 'X' FROM RPBaselineExpenses WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
    IF (@strConTab = 'Y') SET @intBaselineConCount = CASE WHEN EXISTS (SELECT 'X' FROM RPBaselineConsultant WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
    IF (@strUntTab = 'Y') SET @intBaselineUntCount = CASE WHEN EXISTS (SELECT 'X' FROM RPBaselineUnit WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
     
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get Plan Summary JTD

  IF (@strRetrievalMode != '1')
    BEGIN

      -- Set JTD/ETC Dates.
      
      SET @dtETCDate = CONVERT(datetime, REPLACE(@strETCDate, '-', ''))
      SET @dtJTDDate = DATEADD(d, -1, @dtETCDate)

      -- Initialize JTD Variables.

      SELECT 
        @decJTDLabHrs = 0,
        @decJTDLabCost = 0,
        @decJTDLabBill = 0,
        @decJTDExpCost = 0,
        @decJTDExpBill = 0,
        @decJTDDirExpCost = 0,
        @decJTDDirExpBill = 0,
        @decJTDReimExpCost = 0,
        @decJTDReimExpBill = 0,
        @decJTDConCost = 0,
        @decJTDConBill = 0,
        @decJTDReimConCost = 0,
        @decJTDReimConBill = 0,
        @decJTDDirConCost = 0,
        @decJTDDirConBill = 0,
        @decJTDUntQty = 0,
        @decJTDUntCost = 0,
        @decJTDUntBill = 0,
        @decJTDDirUntCost = 0,
        @decJTDDirUntBill = 0,
        @decJTDReimUntCost = 0,
        @decJTDReimUntBill = 0,
        @decJTDCostRev = 0,
        @decJTDBillingRev = 0

      -- Find Top Tasks.
 
      INSERT @tabTopTask(TaskID, WBS1, WBS2, WBS3, LaborCode)
        SELECT DISTINCT TaskID, WBS1, WBS2, WBS3, LaborCode
          FROM RPTask AS T
          INNER JOIN (SELECT MIN(OutlineLevel) AS OutlineLevel
              FROM RPTask AS XT INNER JOIN RPWBSLevelFormat AS F ON XT.PlanID = F.PlanID
              AND XT.WBSType = F.WBSType AND F.WBSMatch = 'Y'
              WHERE XT.PlanID = @strPlanID) AS ZT ON T.OutlineLevel = ZT.OutlineLevel
          WHERE T.PlanID = @strPlanID AND T.WBS1 IS NOT NULL AND T.WBS1 != '<none>'
      
      IF (@@ROWCOUNT > 0)
        BEGIN
        
        -- Posted Labor JTD.
        
        SELECT
          @decJTDLabHrs = ISNULL(SUM(RegHrs + OvtHrs + SpecialOvtHrs), 0), 
          @decJTDLabCost = ISNULL(SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency), 0), 
          @decJTDLabBill = ISNULL(SUM(BillExt), 0)
          FROM LD
            INNER JOIN @tabTopTask AS T ON (LD.WBS1 = T.WBS1
              AND LD.ProjectCost = 'Y' AND LD.TransDate <= @dtJTDDate
              AND LD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
              AND LD.WBS3 LIKE (ISNULL(T.WBS3, '%'))
              AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%'))) 

        -- UnPosted Labor JTD.

        IF (@strUnposted = 'Y')
          BEGIN
          
            SELECT
              @decJTDLabHrs = @decJTDLabHrs + ISNULL(SUM(PeriodHrs), 0), 
              @decJTDLabCost = @decJTDLabCost + ISNULL(SUM(PeriodCost), 0), 
              @decJTDLabBill = @decJTDLabBill + ISNULL(SUM(PeriodBill), 0)
              FROM
                (SELECT
                   SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs, 
                   SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost, 
                   SUM(BillExt) AS PeriodBill
                   FROM tkDetail AS TD
                     INNER JOIN @tabTopTask AS T ON (TD.WBS1 = T.WBS1
                       AND TD.TransDate <= @dtJTDDate
                       AND TD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
                       AND TD.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                       AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%')))
                     INNER JOIN EM ON TD.Employee = EM.Employee  
                     INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
                 UNION ALL
                 SELECT
                   SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs, 
                   SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost, 
                   SUM(BillExt) AS PeriodBill
                   FROM tsDetail AS TD
                     INNER JOIN @tabTopTask AS T ON (TD.WBS1 = T.WBS1
                       AND TD.TransDate <= @dtJTDDate
                       AND TD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
                       AND TD.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                       AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%')))
                     INNER JOIN EM ON TD.Employee = EM.Employee  
                     INNER JOIN tsControl AS TC ON (TC.Posted = 'N' AND TD.Batch = TC.Batch)
               ) AS X

          END --IF (@strUnposted = 'Y')

        --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
              
        IF (@strExpTab = 'Y')
          BEGIN
          
            -- Posted Expense JTD.
            
            SELECT
              @decJTDExpCost = SUM(ISNULL(PeriodCost, 0)), 
              @decJTDExpBill = SUM(ISNULL(PeriodBill, 0)),
              @decJTDDirExpCost = SUM(ISNULL(PeriodDirCost, 0)), 
              @decJTDDirExpBill = SUM(ISNULL(PeriodDirBill, 0)),
              @decJTDReimExpCost = SUM(ISNULL(PeriodReimCost, 0)), 
              @decJTDReimExpBill = SUM(ISNULL(PeriodReimBill, 0))
              FROM (
                SELECT
                  SUM(AmountProjectCurrency) AS PeriodCost,  
                  SUM(BillExt) AS PeriodBill,
                  SUM(CASE WHEN CA.Type = 7 THEN AmountProjectCurrency ELSE 0 END) AS PeriodDirCost,
                  SUM(CASE WHEN CA.Type = 7 THEN BillExt ELSE 0 END) AS PeriodDirBill,
                  SUM(CASE WHEN CA.Type = 5 THEN AmountProjectCurrency ELSE 0 END) AS PeriodReimCost,
                  SUM(CASE WHEN CA.Type = 5 THEN BillExt ELSE 0 END) AS PeriodReimBill
                  FROM LedgerAR AS Ledger
                    INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                      AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                      AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                      AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                      AND Ledger.TransType != (CASE WHEN @strUntTab = 'Y' THEN 'UN' ELSE '' END)
                    INNER JOIN CA ON Ledger.Account = CA.Account
                  WHERE CA.Type IN (5, 7) 
                UNION ALL
                SELECT
                  SUM(AmountProjectCurrency) AS PeriodCost,  
                  SUM(BillExt) AS PeriodBill,
                  SUM(CASE WHEN CA.Type = 7 THEN AmountProjectCurrency ELSE 0 END) AS PeriodDirCost,
                  SUM(CASE WHEN CA.Type = 7 THEN BillExt ELSE 0 END) AS PeriodDirBill,
                  SUM(CASE WHEN CA.Type = 5 THEN AmountProjectCurrency ELSE 0 END) AS PeriodReimCost,
                  SUM(CASE WHEN CA.Type = 5 THEN BillExt ELSE 0 END) AS PeriodReimBill
                  FROM LedgerAP AS Ledger
                    INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                      AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                      AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                      AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                      AND Ledger.TransType != (CASE WHEN @strUntTab = 'Y' THEN 'UN' ELSE '' END)
                    INNER JOIN CA ON Ledger.Account = CA.Account
                  WHERE CA.Type IN (5, 7) 
                UNION ALL
                SELECT
                  SUM(AmountProjectCurrency) AS PeriodCost,  
                  SUM(BillExt) AS PeriodBill,
                  SUM(CASE WHEN CA.Type = 7 THEN AmountProjectCurrency ELSE 0 END) AS PeriodDirCost,
                  SUM(CASE WHEN CA.Type = 7 THEN BillExt ELSE 0 END) AS PeriodDirBill,
                  SUM(CASE WHEN CA.Type = 5 THEN AmountProjectCurrency ELSE 0 END) AS PeriodReimCost,
                  SUM(CASE WHEN CA.Type = 5 THEN BillExt ELSE 0 END) AS PeriodReimBill
                  FROM LedgerEX AS Ledger
                    INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                      AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                      AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                      AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                      AND Ledger.TransType != (CASE WHEN @strUntTab = 'Y' THEN 'UN' ELSE '' END)
                    INNER JOIN CA ON Ledger.Account = CA.Account
                  WHERE CA.Type IN (5, 7) 
                UNION ALL
                SELECT
                  SUM(AmountProjectCurrency) AS PeriodCost,  
                  SUM(BillExt) AS PeriodBill,
                  SUM(CASE WHEN CA.Type = 7 THEN AmountProjectCurrency ELSE 0 END) AS PeriodDirCost,
                  SUM(CASE WHEN CA.Type = 7 THEN BillExt ELSE 0 END) AS PeriodDirBill,
                  SUM(CASE WHEN CA.Type = 5 THEN AmountProjectCurrency ELSE 0 END) AS PeriodReimCost,
                  SUM(CASE WHEN CA.Type = 5 THEN BillExt ELSE 0 END) AS PeriodReimBill
                  FROM LedgerMISC AS Ledger
                    INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                      AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                      AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                      AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                      AND Ledger.TransType != (CASE WHEN @strUntTab = 'Y' THEN 'UN' ELSE '' END)
                    INNER JOIN CA ON Ledger.Account = CA.Account
                  WHERE CA.Type IN (5, 7) 
              ) AS X

            -- UnPosted Expense JTD.

            IF (@strCommitmentFlg = 'Y')
              BEGIN
              
                SELECT
                  @decJTDExpCost = @decJTDExpCost + SUM(ISNULL(PeriodCost, 0)), 
                  @decJTDExpBill = @decJTDExpBill + SUM(ISNULL(PeriodBill, 0)),
                  @decJTDDirExpCost = @decJTDDirExpCost + SUM(ISNULL(PeriodDirCost, 0)), 
                  @decJTDDirExpBill = @decJTDDirExpBill + SUM(ISNULL(PeriodDirBill, 0)),
                  @decJTDReimExpCost = @decJTDReimExpCost + SUM(ISNULL(PeriodReimCost, 0)), 
                  @decJTDReimExpBill = @decJTDReimExpBill + SUM(ISNULL(PeriodReimBill, 0))
                  FROM (
                    SELECT -- POC with no Change Orders.
                      SUM(AmountProjectCurrency) AS PeriodCost,  
                      SUM(BillExt) AS PeriodBill,
                      SUM(CASE WHEN CA.Type = 7 THEN AmountProjectCurrency ELSE 0 END) AS PeriodDirCost,
                      SUM(CASE WHEN CA.Type = 7 THEN BillExt ELSE 0 END) AS PeriodDirBill,
                      SUM(CASE WHEN CA.Type = 5 THEN AmountProjectCurrency ELSE 0 END) AS PeriodReimCost,
                      SUM(CASE WHEN CA.Type = 5 THEN BillExt ELSE 0 END) AS PeriodReimBill
                      FROM POCommitment AS POC 
                        INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
                        INNER JOIN POMaster AS POM ON POD.MasterPKey = POM.MasterPKey
                        INNER JOIN @tabTopTask AS T ON POC.WBS1 = T.WBS1
                          AND POC.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                          AND POC.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                        INNER JOIN CA ON POC.Account = CA.Account
                      WHERE POM.OrderDate <= @dtJTDDate
                        AND AmountProjectCurrency != 0 AND BillExt != 0
                        AND CA.Type IN (5, 7) 
                    UNION ALL
                    SELECT -- POC with Change Orders.
                      SUM(AmountProjectCurrency) AS PeriodCost,  
                      SUM(BillExt) AS PeriodBill,
                      SUM(CASE WHEN CA.Type = 7 THEN AmountProjectCurrency ELSE 0 END) AS PeriodDirCost,
                      SUM(CASE WHEN CA.Type = 7 THEN BillExt ELSE 0 END) AS PeriodDirBill,
                      SUM(CASE WHEN CA.Type = 5 THEN AmountProjectCurrency ELSE 0 END) AS PeriodReimCost,
                      SUM(CASE WHEN CA.Type = 5 THEN BillExt ELSE 0 END) AS PeriodReimBill
                      FROM POCommitment AS POC 
                        INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
                        INNER JOIN POCOMaster AS POCOM ON POD.COPKey = POCOM.PKey
                        INNER JOIN POMaster AS POM ON POCOM.MasterPKey = POM.MasterPKey
                        INNER JOIN @tabTopTask AS T ON POC.WBS1 = T.WBS1
                          AND POC.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                          AND POC.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                        INNER JOIN CA ON POC.Account = CA.Account
                      WHERE POCOM.OrderDate <= @dtJTDDate
                        AND AmountProjectCurrency != 0 AND BillExt != 0
                        AND CA.Type IN (5, 7) 
                    ) AS X
                                    
              END --IF (@strUnposted = 'Y')
                    
          END --IF (@strExpTab = 'Y')

        --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
              
        IF (@strConTab = 'Y')
          BEGIN
          
            -- Posted Consultant JTD.
            
            SELECT
              @decJTDConCost = SUM(ISNULL(PeriodCost, 0)), 
              @decJTDConBill = SUM(ISNULL(PeriodBill, 0)),
              @decJTDDirConCost = SUM(ISNULL(PeriodDirCost, 0)), 
              @decJTDDirConBill = SUM(ISNULL(PeriodDirBill, 0)),
              @decJTDReimConCost = SUM(ISNULL(PeriodReimCost, 0)), 
              @decJTDReimConBill = SUM(ISNULL(PeriodReimBill, 0))
              FROM (
                SELECT
                  SUM(AmountProjectCurrency) AS PeriodCost,  
                  SUM(BillExt) AS PeriodBill,
                  SUM(CASE WHEN CA.Type = 8 THEN AmountProjectCurrency ELSE 0 END) AS PeriodDirCost,
                  SUM(CASE WHEN CA.Type = 8 THEN BillExt ELSE 0 END) AS PeriodDirBill,
                  SUM(CASE WHEN CA.Type = 6 THEN AmountProjectCurrency ELSE 0 END) AS PeriodReimCost,
                  SUM(CASE WHEN CA.Type = 6 THEN BillExt ELSE 0 END) AS PeriodReimBill
                  FROM LedgerAR AS Ledger
                    INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                      AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                      AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                      AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                      AND Ledger.TransType != (CASE WHEN @strUntTab = 'Y' THEN 'UN' ELSE '' END)
                    INNER JOIN CA ON Ledger.Account = CA.Account
                  WHERE CA.Type IN (6, 8) 
                UNION ALL
                SELECT
                  SUM(AmountProjectCurrency) AS PeriodCost,  
                  SUM(BillExt) AS PeriodBill,
                  SUM(CASE WHEN CA.Type = 8 THEN AmountProjectCurrency ELSE 0 END) AS PeriodDirCost,
                  SUM(CASE WHEN CA.Type = 8 THEN BillExt ELSE 0 END) AS PeriodDirBill,
                  SUM(CASE WHEN CA.Type = 6 THEN AmountProjectCurrency ELSE 0 END) AS PeriodReimCost,
                  SUM(CASE WHEN CA.Type = 6 THEN BillExt ELSE 0 END) AS PeriodReimBill
                  FROM LedgerAP AS Ledger
                    INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                      AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                      AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                      AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                      AND Ledger.TransType != (CASE WHEN @strUntTab = 'Y' THEN 'UN' ELSE '' END)
                    INNER JOIN CA ON Ledger.Account = CA.Account
                  WHERE CA.Type IN (6, 8) 
                UNION ALL
                SELECT
                  SUM(AmountProjectCurrency) AS PeriodCost,  
                  SUM(BillExt) AS PeriodBill,
                  SUM(CASE WHEN CA.Type = 8 THEN AmountProjectCurrency ELSE 0 END) AS PeriodDirCost,
                  SUM(CASE WHEN CA.Type = 8 THEN BillExt ELSE 0 END) AS PeriodDirBill,
                  SUM(CASE WHEN CA.Type = 6 THEN AmountProjectCurrency ELSE 0 END) AS PeriodReimCost,
                  SUM(CASE WHEN CA.Type = 6 THEN BillExt ELSE 0 END) AS PeriodReimBill
                  FROM LedgerEX AS Ledger
                    INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                      AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                      AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                      AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                      AND Ledger.TransType != (CASE WHEN @strUntTab = 'Y' THEN 'UN' ELSE '' END)
                    INNER JOIN CA ON Ledger.Account = CA.Account
                  WHERE CA.Type IN (6, 8) 
                UNION ALL
                SELECT
                  SUM(AmountProjectCurrency) AS PeriodCost,  
                  SUM(BillExt) AS PeriodBill,
                  SUM(CASE WHEN CA.Type = 8 THEN AmountProjectCurrency ELSE 0 END) AS PeriodDirCost,
                  SUM(CASE WHEN CA.Type = 8 THEN BillExt ELSE 0 END) AS PeriodDirBill,
                  SUM(CASE WHEN CA.Type = 6 THEN AmountProjectCurrency ELSE 0 END) AS PeriodReimCost,
                  SUM(CASE WHEN CA.Type = 6 THEN BillExt ELSE 0 END) AS PeriodReimBill
                  FROM LedgerMISC AS Ledger
                    INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                      AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                      AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                      AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                      AND Ledger.TransType != (CASE WHEN @strUntTab = 'Y' THEN 'UN' ELSE '' END)
                    INNER JOIN CA ON Ledger.Account = CA.Account
                  WHERE CA.Type IN (6, 8) 
              ) AS X
                            
            -- UnPosted Consultant JTD.

            IF (@strCommitmentFlg = 'Y')
              BEGIN
              
                SELECT
                  @decJTDConCost = @decJTDConCost + SUM(ISNULL(PeriodCost, 0)), 
                  @decJTDConBill = @decJTDConBill + SUM(ISNULL(PeriodBill, 0)),
                  @decJTDDirConCost = @decJTDDirConCost + SUM(ISNULL(PeriodDirCost, 0)), 
                  @decJTDDirConBill = @decJTDDirConBill + SUM(ISNULL(PeriodDirBill, 0)),
                  @decJTDReimConCost = @decJTDReimConCost + SUM(ISNULL(PeriodReimCost, 0)), 
                  @decJTDReimConBill = @decJTDReimConBill + SUM(ISNULL(PeriodReimBill, 0))
                  FROM (
                    SELECT -- POC with no Change Orders.
                      SUM(AmountProjectCurrency) AS PeriodCost,  
                      SUM(BillExt) AS PeriodBill,
                      SUM(CASE WHEN CA.Type = 8 THEN AmountProjectCurrency ELSE 0 END) AS PeriodDirCost,
                      SUM(CASE WHEN CA.Type = 8 THEN BillExt ELSE 0 END) AS PeriodDirBill,
                      SUM(CASE WHEN CA.Type = 6 THEN AmountProjectCurrency ELSE 0 END) AS PeriodReimCost,
                      SUM(CASE WHEN CA.Type = 6 THEN BillExt ELSE 0 END) AS PeriodReimBill
                      FROM POCommitment AS POC 
                        INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
                        INNER JOIN POMaster AS POM ON POD.MasterPKey = POM.MasterPKey
                        INNER JOIN @tabTopTask AS T ON (POC.WBS1 = T.WBS1
                          AND POC.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                          AND POC.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                        INNER JOIN CA ON POC.Account = CA.Account
                      WHERE POM.OrderDate <= @dtJTDDate
                        AND AmountProjectCurrency != 0 AND BillExt != 0
                        AND CA.Type IN (6, 8) 
                    UNION ALL
                    SELECT -- POC with Change Orders.
                      SUM(AmountProjectCurrency) AS PeriodCost,  
                      SUM(BillExt) AS PeriodBill,
                      SUM(CASE WHEN CA.Type = 8 THEN AmountProjectCurrency ELSE 0 END) AS PeriodDirCost,
                      SUM(CASE WHEN CA.Type = 8 THEN BillExt ELSE 0 END) AS PeriodDirBill,
                      SUM(CASE WHEN CA.Type = 6 THEN AmountProjectCurrency ELSE 0 END) AS PeriodReimCost,
                      SUM(CASE WHEN CA.Type = 6 THEN BillExt ELSE 0 END) AS PeriodReimBill
                      FROM POCommitment AS POC 
                        INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
                        INNER JOIN POCOMaster AS POCOM ON POD.COPKey = POCOM.PKey
                        INNER JOIN POMaster AS POM ON POCOM.MasterPKey = POM.MasterPKey
                        INNER JOIN @tabTopTask AS T ON (POC.WBS1 = T.WBS1
                          AND POC.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                          AND POC.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                        INNER JOIN CA ON POC.Account = CA.Account
                      WHERE POCOM.OrderDate <= @dtJTDDate
                        AND AmountProjectCurrency != 0 AND BillExt != 0
                        AND CA.Type IN (6, 8) 
                   ) AS X
               
              END -- If-Then (@strUnposted = 'Y')

          END --IF (@strConTab = 'Y')

        --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        IF (@strUntTab = 'Y')
          BEGIN
          
            -- Posted Unit JTD.
            
            SELECT
              @decJTDUntQty = ISNULL(SUM(UnitQuantity), 0),
              @decJTDUntCost = ISNULL(SUM(AmountProjectCurrency), 0), 
              @decJTDUntBill = ISNULL(SUM(BillExt), 0),
              @decJTDDirUntCost = ISNULL(SUM(CASE WHEN CA.Type IN (7, 8) THEN AmountProjectCurrency ELSE 0 END), 0),
              @decJTDDirUntBill = ISNULL(SUM(CASE WHEN CA.Type IN (7, 8) THEN BillExt ELSE 0 END), 0),
              @decJTDReimUntCost = ISNULL(SUM(CASE WHEN CA.Type IN (5, 6) THEN AmountProjectCurrency ELSE 0 END), 0),
              @decJTDReimUntBill = ISNULL(SUM(CASE WHEN CA.Type IN (5, 6) THEN BillExt ELSE 0 END), 0)
              FROM LedgerMISC AS Ledger
                INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                  AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                  AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                  AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                  AND Ledger.TransType = 'UN'
                INNER JOIN CA ON Ledger.Account = CA.Account
                    
          END --IF (@strUntTab = 'Y')
            
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

 -- Get JTD Cost Revenue.
        
        SELECT 
          @decJTDCostRev = SUM(ISNULL(R.JTDRevenue, 0)) 
          FROM
            (SELECT SUM(-AmountProjectCurrency) AS JTDRevenue
               FROM LedgerAR AS Ledger 
                 INNER JOIN @tabTopTask AS T ON Ledger.WBS1 = T.WBS1 
                   AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%')) 
                   AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                 INNER JOIN CA ON Ledger.Account = CA.Account
               WHERE (CA.Type = 4 
                 AND NOT (Ledger.TransType='IN' AND (Ledger.SubType IS NOT NULL AND Ledger.SubType IN ('R','I')))) 
                 AND Ledger.TransDate <= @dtJTDDate
             UNION ALL
             SELECT SUM(-AmountProjectCurrency) AS JTDRevenue
               FROM LedgerAP AS Ledger 
                 INNER JOIN @tabTopTask AS T ON Ledger.WBS1 = T.WBS1 
                   AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%')) 
                   AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                 INNER JOIN CA ON Ledger.Account = CA.Account
               WHERE (CA.Type = 4 
                 AND NOT (Ledger.TransType='IN' AND (Ledger.SubType IS NOT NULL AND Ledger.SubType IN ('R','I')))) 
                 AND Ledger.TransDate <= @dtJTDDate
             UNION ALL
             SELECT SUM(-AmountProjectCurrency) AS JTDRevenue
               FROM LedgerEX AS Ledger 
                 INNER JOIN @tabTopTask AS T ON Ledger.WBS1 = T.WBS1 
                   AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%')) 
                   AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                 INNER JOIN CA ON Ledger.Account = CA.Account
               WHERE (CA.Type = 4 
                 AND NOT (Ledger.TransType='IN' AND (Ledger.SubType IS NOT NULL AND Ledger.SubType IN ('R','I')))) 
                 AND Ledger.TransDate <= @dtJTDDate
             UNION ALL
             SELECT SUM(-AmountProjectCurrency) AS JTDRevenue
               FROM LedgerMISC AS Ledger 
                 INNER JOIN @tabTopTask AS T ON Ledger.WBS1 = T.WBS1 
                   AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%')) 
                   AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                 INNER JOIN CA ON Ledger.Account = CA.Account
               WHERE (CA.Type = 4 
                 AND NOT (Ledger.TransType='IN' AND (Ledger.SubType IS NOT NULL AND Ledger.SubType IN ('R','I')))) 
                 AND Ledger.TransDate <= @dtJTDDate) AS R

        -- Get JTD Billing Revenue.
        
        SELECT 
          @decJTDBillingRev = SUM(ISNULL(R.JTDRevenue, 0)) 
          FROM
            (SELECT SUM(-AmountBillingCurrency) AS JTDRevenue
               FROM LedgerAR AS Ledger 
                 INNER JOIN @tabTopTask AS T ON Ledger.WBS1 = T.WBS1 
                   AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%')) 
                   AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                 INNER JOIN CA ON Ledger.Account = CA.Account
               WHERE (CA.Type = 4 
                 AND NOT (Ledger.TransType='IN' AND (Ledger.SubType IS NOT NULL AND Ledger.SubType IN ('R','I')))) 
                 AND Ledger.TransDate <= @dtJTDDate
             UNION ALL
             SELECT SUM(-AmountBillingCurrency) AS JTDRevenue
               FROM LedgerAP AS Ledger 
                 INNER JOIN @tabTopTask AS T ON Ledger.WBS1 = T.WBS1 
                   AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%')) 
                   AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                 INNER JOIN CA ON Ledger.Account = CA.Account
               WHERE (CA.Type = 4 
                 AND NOT (Ledger.TransType='IN' AND (Ledger.SubType IS NOT NULL AND Ledger.SubType IN ('R','I')))) 
                 AND Ledger.TransDate <= @dtJTDDate
             UNION ALL
             SELECT SUM(-AmountBillingCurrency) AS JTDRevenue
               FROM LedgerEX AS Ledger 
                 INNER JOIN @tabTopTask AS T ON Ledger.WBS1 = T.WBS1 
                   AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%')) 
                   AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                 INNER JOIN CA ON Ledger.Account = CA.Account
               WHERE (CA.Type = 4 
                 AND NOT (Ledger.TransType='IN' AND (Ledger.SubType IS NOT NULL AND Ledger.SubType IN ('R','I')))) 
                 AND Ledger.TransDate <= @dtJTDDate
             UNION ALL
             SELECT SUM(-AmountBillingCurrency) AS JTDRevenue
               FROM LedgerMISC AS Ledger 
                 INNER JOIN @tabTopTask AS T ON Ledger.WBS1 = T.WBS1 
                   AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%')) 
                   AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                 INNER JOIN CA ON Ledger.Account = CA.Account
               WHERE (CA.Type = 4 
                 AND NOT (Ledger.TransType='IN' AND (Ledger.SubType IS NOT NULL AND Ledger.SubType IN ('R','I')))) 
                 AND Ledger.TransDate <= @dtJTDDate) AS R
                  
        END --If-Then (@@ROWCOUNT > 0)
    
    END -- If-Then (@strRetrievalMode != '1')

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Combine JTDDirExp with JTDDirUnt and JTDReimExp with JTDReimUnt.

    SELECT @decJTDDirExpCost = @decJTDDirExpCost + @decJTDDirUntCost
    SELECT @decJTDDirExpBill = @decJTDDirExpBill + @decJTDDirUntBill
    SELECT @decJTDReimExpCost = @decJTDReimExpCost + @decJTDReimUntCost
    SELECT @decJTDReimExpBill = @decJTDReimExpBill + @decJTDReimUntBill

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Generate <Plan> node.
  
    SELECT  
      P.PlanName AS PlanName, 
      ISNULL(P.PlanNumber, '') AS PlanNumber, 
      REPLACE(CONVERT(VARCHAR, P.StartDate, 121), ' ', 'T') AS StartDate, 
      REPLACE(CONVERT(VARCHAR, P.EndDate, 121), ' ', 'T') AS EndDate, 
      ISNULL(P.WBS1, '') AS WBS1, 
      ISNULL(PR.Name, '') AS prName, 
      ISNULL(Opportunity.Name, '') AS oppName, 
      ISNULL(P.OpportunityID, '') AS OpportunityID, 
      P.Probability AS Probability, 
      ISNULL(CL.Name, '') AS clName, 
      ISNULL(PL.Lastname + (ISNULL(', ' + PL.FirstName, '')), '') AS emPrincipal, 
      ISNULL(PM.Lastname + (ISNULL(', ' + PM.FirstName, '')), '') AS emProjMgr, 
      ISNULL(SP.Lastname + (ISNULL(', ' + SP.FirstName, '')), '') AS emSupervisor, 
      ISNULL(PL.Email, '') AS emPrincipalEmail, 
      ISNULL(PM.Email, '') AS emProjMgrEmail, 
      ISNULL(SP.Email, '') AS emSupervisorEmail, 
      ISNULL(Organization.Name, '') AS orgName, 
      CAST(P.PctCompleteFormula AS int) AS PctCompleteFormula, 
      P.LabMultType AS LabMultType, 
      P.Multiplier AS Multiplier, 
      P.ReimbMethod AS ReimbMethod, 
      P.AnalysisBasis AS AnalysisBasis, 
      P.TargetMultCost AS TargetMultCost, 
      P.TargetMultBill AS TargetMultBill, 
      P.CostCurrencyCode AS CostCurrencyCode, 
      P.BillingCurrencyCode AS BillingCurrencyCode, 
      ISNULL(MD.FirmName, '') AS FirmName, 
      P.UtilizationIncludeFlg AS UtilizationIncludeFlg, 
      P.UnpostedFlg AS UnpostedFlg, 
      P.CommitmentFlg AS CommitmentFlg, 
      P.AvailableFlg AS AvailableFlg, 
      ISNULL(P.Status, '') AS Status, 
      ISNULL(P.CreateUser, '') AS CreateUser, 
      REPLACE(CONVERT(VARCHAR, P.CreateDate, 121), ' ', 'T') AS CreateDate, 
      ISNULL(P.ModUser, '') AS ModUser, 
      REPLACE(CONVERT(VARCHAR, P.ModDate, 121), ' ', 'T') AS ModDate,
      ISNULL(P.CheckedOutUser, '') AS CheckedOutUser, 
      ISNULL(REPLACE(CONVERT(VARCHAR, P.CheckedOutDate, 121), ' ', 'T'), '') AS CheckedOutDate, 
      ISNULL(P.CheckedOutID, '') AS CheckedOutID, 
      P.PlanID AS PlanID, 
      ISNULL(P.ClientID, '') AS ClientID, 
      ISNULL(P.Org, '') AS Org, 
      ISNULL(P.ProjMgr, '') AS ProjMgr, 
      ISNULL(P.Principal, '') AS Principal, 
      ISNULL(P.Supervisor, '') AS Supervisor, 
      ISNULL(P.Company, '') AS Company, 
      P.BudgetType AS BudgetType, 
      P.GRMethod AS GRMethod, 
      P.CostRtMethod AS CostRtMethod, 
      P.CostRtTableNo AS CostRtTableNo, 
      P.GenResTableNo AS GenResTableNo, 
      P.BillingRtMethod AS BillingRtMethod, 
      P.BillingRtTableNo AS BillingRtTableNo, 
      P.LabBillMultiplier AS LabBillMultiplier, 
      P.GRBillTableNo AS GRBillTableNo, 
      P.CalcExpBillAmtFlg AS CalcExpBillAmtFlg, 
      P.ExpBillRtMethod AS ExpBillRtMethod, 
      P.ExpBillRtTableNo AS ExpBillRtTableNo, 
      P.ExpBillMultiplier AS ExpBillMultiplier, 
      P.CalcConBillAmtFlg AS CalcConBillAmtFlg, 
      P.ConBillRtMethod AS ConBillRtMethod , 
      P.ConBillRtTableNo AS ConBillRtTableNo, 
      P.ConBillMultiplier AS ConBillMultiplier, 
      P.UntBillMultiplier AS UntBillMultiplier, 
      P.StartingDayOfWeek AS StartingDayOfWeek, 
      REPLACE(CONVERT(VARCHAR, P.StartDate, 121), ' ', 'T') AS OldStartDate, 
      '' AS SaveChange,
      P.TopDownFee AS TopDownFee, 
      P.CompensationFee AS CompensationFee, 
      P.CompensationFeeDirLab AS CompensationFeeDirLab, 
      P.CompensationFeeDirExp AS CompensationFeeDirExp, 
      P.ConsultantFee AS ConsultantFee, 
      P.ReimbAllowance AS ReimbAllowance, 
      P.ReimbAllowanceExp AS ReimbAllowanceExp, 
      P.ReimbAllowanceCon AS ReimbAllowanceCon, 
      P.CompensationFeeBill AS CompensationFeeBill, 
      P.CompensationFeeDirLabBill AS CompensationFeeDirLabBill, 
      P.CompensationFeeDirExpBill AS CompensationFeeDirExpBill, 
      P.ConsultantFeeBill AS ConsultantFeeBill, 
      P.ReimbAllowanceBill AS ReimbAllowanceBill, 
      P.ReimbAllowanceExpBill AS ReimbAllowanceExpBill, 
      P.ReimbAllowanceConBill AS ReimbAllowanceConBill, 
      P.OverheadPct AS OverheadPct, 
      '' AS Overhead,
      P.ContingencyPct AS ContingencyPct, 
      P.ContingencyAmt AS ContingencyAmt, 
      '' AS ProfitPct,
      '' AS ProfitAmt,
      '' AS Markup,
      P.ProjectedMultiplier AS ProjectedMultiplier, 
      P.ProjectedRatio AS ProjectedRatio, 
      '' AS Variance,
      '' AS VariancePct,
      '' AS ReimbVariance,
      CASE WHEN @sintAmtDecimals = -1 THEN ISNULL(CC.DecimalPlaces, 2) 
           ELSE @sintAmtDecimals END AS AmtCostDecimals, 
      CASE WHEN @sintAmtDecimals = -1 THEN ISNULL(BC.DecimalPlaces, 2)
           ELSE @sintAmtDecimals END AS AmtBillDecimals, 
      CASE WHEN @sintAmtDecimals = -1 
           THEN CASE WHEN P.LabMultType < 2 THEN ISNULL(CC.DecimalPlaces, 2)
                     ELSE ISNULL(BC.DecimalPlaces, 2) END 
           ELSE @sintAmtDecimals END AS LabRevDecimals,
      CASE WHEN @sintAmtDecimals = -1 
           THEN CASE WHEN P.ReimbMethod = 'C' THEN ISNULL(CC.DecimalPlaces, 2)
                     ELSE ISNULL(BC.DecimalPlaces, 2) END 
           ELSE @sintAmtDecimals END AS ECURevDecimals,
      ISNULL(CCR.CompCost, 0) AS CompCost, 
      ISNULL(CCR.CompBill, 0) AS CompBill, 
    
      ISNULL(CCR.CompDirLabCost, 0) AS CompDirLabCost, 
      ISNULL(CCR.CompDirLabBill, 0) AS CompDirLabBill, 
      ISNULL(CCR.CompDirExpCost, 0) AS CompDirExpCost, 
      ISNULL(CCR.CompDirExpBill, 0) AS CompDirExpBill, 
      ISNULL(CCR.ConsCost, 0) AS ConsCost, 
      ISNULL(CCR.ConsBill, 0) AS ConsBill, 
      ISNULL(CCR.ReimCost, 0) AS ReimCost, 
      ISNULL(CCR.ReimBill, 0) AS ReimBill, 
      ISNULL(CCR.ReimExpCost, 0) AS ReimExpCost, 
      ISNULL(CCR.ReimExpBill, 0) AS ReimExpBill, 
      ISNULL(CCR.ReimConCost, 0) AS ReimConCost, 
      ISNULL(CCR.ReimConBill, 0) AS ReimConBill, 
      0 AS PlannedLaborHrs, 
      0 AS PlannedLabCost, 
      0 AS PlannedLabBill, 
      0 AS PlannedExpCost, 
      0 AS PlannedExpBill, 
      0 AS PlannedConCost, 
      0 AS PlannedConBill, 
      0 AS PlannedUntQty, 
      0 AS PlannedUntCost, 
      0 AS PlannedUntBill, 
      0 AS LabRevenue, 
      0 AS ExpRevenue, 
      0 AS ConRevenue, 
      0 AS UntRevenue, 
      0 AS WeightedLabCost, 
      0 AS WeightedLabBill, 
      0 AS WeightedExpCost, 
      0 AS WeightedExpBill, 
      0 AS WeightedConCost, 
      0 AS WeightedConBill, 
      0 AS WeightedUntCost, 
      0 AS WeightedUntBill, 
      0 AS PlannedDirExpCost, 
      0 AS PlannedDirExpBill, 
      0 AS PlannedDirConCost, 
      0 AS PlannedDirConBill, 
      0 AS PlannedDirUntCost, 
      0 AS PlannedDirUntBill, 

      P.BaselineLaborHrs AS BaselineLaborHrs, 
      P.BaselineLabCost AS BaselineLabCost, 
      P.BaselineLabBill AS BaselineLabBill, 
      P.BaselineExpCost AS BaselineExpCost, 
      P.BaselineExpBill AS BaselineExpBill, 
      P.BaselineConCost AS BaselineConCost, 
      P.BaselineConBill AS BaselineConBill, 
      P.BaselineUntQty AS BaselineUntQty, 
      P.BaselineUntCost AS BaselineUntCost, 
      P.BaselineUntBill AS BaselineUntBill, 

      0 AS ETCLaborHrs, 
      0 AS ETCLabCost, 
      0 AS ETCLabBill, 
      0 AS ETCExpCost, 
      0 AS ETCExpBill, 
      0 AS ETCConCost, 
      0 AS ETCConBill, 
      0 AS ETCUntQty, 
      0 AS ETCUntCost, 
      0 AS ETCUntBill, 

      0 AS ETCDirExpCost, 
      0 AS ETCDirExpBill, 
      0 AS ETCDirConCost, 
      0 AS ETCDirConBill, 
      0 AS ETCDirUntCost, 
      0 AS ETCDirUntBill, 
      0 AS ETCLabHourBill,
      0 AS ETCOverheadLabCost,
      0 AS ETCRateHourCost,
      0 AS ETCRateHourBill,
      0 AS ETCRedimExpCost,
      0 AS ETCRedimExpBill,
      0 AS ETCRedimConCost,
      0 AS ETCRedimConBill,
      0 AS ETCTotalCost,
      0 AS ETCTotalBill,
      0 AS ETCDirLabCost,

      LTRIM(STR(ISNULL(@decJTDLabHrs, 0), 19, 2)) AS ActualLaborHrs, 
      LTRIM(STR(ISNULL(@decJTDLabCost, 0), 19, 2)) AS ActualLabCost, 
      LTRIM(STR(ISNULL(@decJTDLabBill, 0), 19, 2)) AS ActualLabBill, 
      LTRIM(STR(ISNULL(@decJTDExpCost, 0), 19, 2)) AS ActualExpCost, 
      LTRIM(STR(ISNULL(@decJTDExpBill, 0), 19, 2)) AS ActualExpBill, 
      LTRIM(STR(ISNULL(@decJTDConCost, 0), 19, 2)) AS ActualConCost, 
      LTRIM(STR(ISNULL(@decJTDConBill, 0), 19, 2)) AS ActualConBill, 
      LTRIM(STR(ISNULL(@decJTDUntQty, 0), 19, 2)) AS ActualUntQty, 
      LTRIM(STR(ISNULL(@decJTDUntCost, 0), 19, 2)) AS ActualUntCost, 
      LTRIM(STR(ISNULL(@decJTDUntBill, 0), 19, 2)) AS ActualUntBill, 

	  0 AS ActualDirLabCost,
      LTRIM(STR(ISNULL(@decJTDDirExpCost, 0), 19, 2)) AS ActualDirExpCost,
      LTRIM(STR(ISNULL(@decJTDDirExpBill, 0), 19, 2)) AS ActualDirExpBill,
      LTRIM(STR(ISNULL(@decJTDDirConCost, 0), 19, 2)) AS ActualDirConCost,
      LTRIM(STR(ISNULL(@decJTDDirConBill, 0), 19, 2)) AS ActualDirConBill,

      0 AS ActualDirUntCost,
      0 AS ActualDirUntBill,

      

      LTRIM(STR(ISNULL(@decJTDReimExpCost, 0), 19, 2)) AS ActualRedimExpCost,
      LTRIM(STR(ISNULL(@decJTDReimExpBill, 0), 19, 2)) AS ActualRedimExpBill,
      LTRIM(STR(ISNULL(@decJTDReimConCost, 0), 19, 2)) AS ActualRedimConCost,
      LTRIM(STR(ISNULL(@decJTDReimConBill, 0), 19, 2)) AS ActualRedimConBill,

      0 AS ActualTotalCost,
      0 AS ActualTotalBill,

      '' AS ActualCost,
      '' AS ActualProfit,
      '' AS ActualProfitPct,
      '' AS ActualBillValue,
      '' AS ActualVariance,
      '' AS ActualVariancePct,
      '' AS ActualRateHourCost,
      '' AS ActualRateHourBill,
      '' AS ActualLabHourBill,
      0 AS ActualOverheadLabCost,

      0 AS EACTotalCost,
      0 AS EACTotalBill,

      0 AS EACLaborHrs, 
      0 AS EACLabCost, 
      0 AS EACLabBill, 
      0 AS EACExpCost, 
      0 AS EACExpBill, 
      0 AS EACConCost, 
      0 AS EACConBill, 
      0 AS EACUntQty, 
      0 AS EACUntCost, 
      0 AS EACUntBill, 
      '' AS EACLabHourBill,
      '' AS EACOverheadLabCost,
      '' AS EACRateHourCost,
      '' AS EACRateHourBill,
      '' AS EACDirLabCost,

      0  AS EACDirExpCost,
      0  AS EACDirExpBill,
      0  AS EACDirConCost,
      0  AS EACDirConBill,
      0  AS EACDirUntCost,
      0  AS EACDirUntBill,

      0  AS EACRedimExpCost,
      0  AS EACRedimExpBill,
      0  AS EACRedimConCost,
      0  AS EACRedimConBill,

      0 AS VarianceLaborHrs, 
      0 AS VarianceLabCost, 
      0 AS VarianceLabBill, 
      0 AS VarianceExpCost, 
      0 AS VarianceExpBill, 
      0 AS VarianceConCost, 
      0 AS VarianceConBill, 
      0 AS VarianceUntQty, 
      0 AS VarianceUntCost, 
      0 AS VarianceUntBill, 
    --Derong change here for billing revenue.
      LTRIM(STR(ISNULL(@decJTDCostRev, 0), 19, 2)) AS ActualCostRevenue, 
      LTRIM(STR(ISNULL(@decJTDBillingRev, 0), 19, 2)) AS ActualBillingRevenue, 

      LTRIM(STR(CASE WHEN ISNULL(@decJTDLabCost, 0) = 0 THEN 0  
                     ELSE (ISNULL(@decJTDCostRev, 0) - ISNULL(@decJTDExpCost, 0) - ISNULL(@decJTDConCost, 0) - ISNULL(@decJTDUntCost, 0)) /  
                     @decJTDLabCost END , 19, 2)) AS EMultCost, 

      LTRIM(STR(CASE WHEN ISNULL(@decJTDLabBill, 0) = 0 THEN 0  
                     ELSE (ISNULL(@decJTDCostRev, 0) - ISNULL(@decJTDExpBill, 0) - ISNULL(@decJTDConBill, 0) - ISNULL(@decJTDUntBill, 0)) /  
                     @decJTDLabBill END , 19, 2)) AS EMultBill, 

      0 AS EACMultCost, 
      0 AS EACMultBill, 
      0 AS BudgetMultCost, 
      0 AS BudgetMultBill, 

      0 AS PctComplete, 
      0 AS PctCompleteBill, 

      0 AS PctCompleteLabCost, 
      0 AS PctCompleteExpCost, 
      0 AS PctCompleteConCost, 
      0 AS PctCompleteUntCost, 

      0 AS PctCompleteLabBill, 
      0 AS PctCompleteExpBill, 
      0 AS PctCompleteConBill, 
      0 AS PctCompleteUntBill, 

      CASE WHEN (@intBaselineLabCount + @intBaselineExpCount + @intBaselineConCount + @intBaselineUntCount) > 0
        THEN 'Y' ELSE 'N' END AS BaselineExistFlg, 

      ISNULL(X.WBSCount, 0) AS WBSLookup,

      0 AS PlannedReimbConCost,
      0 AS PlannedReimbExpCost,
      0 AS PlannedReimbConBill,
      0 AS PlannedReimbExpBill,
      
      '' AS CostRtTableNoDescr, 
      '' AS GenResTableNoDescr, 
      '' AS BillingRtTableNoDescr, 
      '' AS GRBillTableNoDescr, 
      '' AS ExpBillRtTableNoDescr, 
      '' AS ConBillRtTableNoDescr,
      P.CostCurrencyCode AS CostCurrencyCodeAnalysis, 
      P.BillingCurrencyCode AS BillingCurrencyCodeAnalysis,
      '' AS DirLabBillVar,
      '' AS DirExpBillVar,
      '' AS DirConBillVar,
      '' AS ReimExpBillVar,
      '' AS ReimConBillVar,
      '' AS TotalContractBill,
      '' AS PlannedTotalContractBill,
      '' AS TotalContractBillVar,
      '' AS PlannedDirLabCost,
      '' AS AverageRateHour,
      '' AS DirLabHourCost,
      '' AS AverageRateHourCost,
      '' AS DirLabProfit,
      '' AS DirExpProfit,
      '' AS DirConProfit,
      '' AS ReimExpProfit,
      '' AS ReimConProfit,
      '' AS TotalContractCost,
      '' AS PlannedTotalContractCost,
      '' AS TotalContractProfit,

      '' AS PlannedRatio,
      '' AS RelizationRatio,
      '' AS PlannedVariance,
      '' AS ExpendedHourBill,
      '' AS ExpendedPctBill,
    
      '' AS PlannedMult,
      '' AS EffectiveMult,
      '' AS ExpendedHourCost,
      '' AS ExpendedPctCost

      FROM RPPlan AS P LEFT JOIN Opportunity ON P.OpportunityID = Opportunity.OpportunityID 
        LEFT JOIN EM  AS PM ON P.ProjMgr = PM.Employee 
        LEFT JOIN EM  AS PL ON P.Principal = PL.Employee 
        LEFT JOIN EM  AS SP ON P.Supervisor = SP.Employee 
        LEFT JOIN CL ON P.ClientID = CL.ClientID 
        LEFT JOIN Organization ON P.Org = Organization.Org 
        LEFT JOIN (SELECT DISTINCT PL.PlanID, 
          MAX(CASE WHEN ((T .WBS1 IS NULL OR T .WBS1 = '<none>') AND (PR.WBS1 IS NULL)) THEN 0 
                   WHEN ((NOT(T .WBS1 IS NULL OR T .WBS1 = '<none>')) AND (PR.WBS1 IS NULL)) THEN 1 
                   WHEN ((NOT (T .WBS1 IS NULL OR T .WBS1 = '<none>')) AND (NOT (PR.WBS1  IS NULL))) THEN 2 END) AS WBSCount 
          FROM RPPlan AS PL LEFT JOIN RPTask AS T ON PL.PlanID = T .PlanID LEFT JOIN PR  ON T .WBS1 = PR.WBS1 GROUP BY PL.PlanID) AS X  
          ON X.PlanID = P.PlanID 
        LEFT JOIN PR ON P.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' 
        LEFT JOIN (SELECT PlanID, SUM(CompensationFee) AS CompCost, SUM(CompensationFeeBill) AS CompBill, 
                     SUM(CompensationFeeDirLab) AS CompDirLabCost, SUM(CompensationFeeDirLabBill) AS CompDirLabBill, 
                     SUM(CompensationFeeDirExp) AS CompDirExpCost, SUM(CompensationFeeDirExpBill) AS CompDirExpBill, 
                     SUM(ConsultantFee) AS ConsCost, SUM(ConsultantFeeBill) AS ConsBill, 
                     SUM(ReimbAllowance) AS ReimCost, SUM(ReimbAllowanceBill) AS ReimBill,
                     SUM(ReimbAllowanceExp) AS ReimExpCost, SUM(ReimbAllowanceExpBill) AS ReimExpBill, 
                     SUM(ReimbAllowanceCon) AS ReimConCost, SUM(ReimbAllowanceConBill) AS ReimConBill  
                     FROM RPTask WHERE OutlineLevel = 0 AND PlanID = @strPlanID GROUP BY PlanID) AS CCR 
          ON P.PlanID = CCR.PlanID 
        LEFT JOIN CFGMainData AS MD ON P.Company = MD.Company 
        LEFT JOIN FW_CFGCurrency AS CC ON CC.Code = P.CostCurrencyCode 
        LEFT JOIN FW_CFGCurrency AS BC ON BC.Code = P.BillingCurrencyCode 
      WHERE P.PlanID = @strPlanID 

  SET NOCOUNT OFF

END -- pmGetRPPlan
GO
