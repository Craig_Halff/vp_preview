SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/* 
	Labor Adjustment excel table input procedure to update laDetail, laMaster, and laControl tables.
	Author: Conrad Harrison
	Date: 2018-06-14
*/
CREATE PROCEDURE [dbo].[HAI_LaborAdjustmentImport]
	@excelFilePath	nvarchar(250),	----** EXAMPLE IS 'C:\ExcelImport\Labor_Adjustment.xlsx'
	@excelSheetName nvarchar(250)	----** EXAMPLE IS 'Sheet1'
		
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'TempImportTable')
		DROP TABLE dbo.TempImportTable

	BEGIN TRANSACTION 

	BEGIN TRY

		----- **IMPORT FROM EXCEL FILE
		EXEC(
		'SELECT * INTO dbo.TempImportTable FROM OPENROWSET(''Microsoft.ACE.OLEDB.12.0'', 
			''Excel 12.0;HDR=YES;IMEX=1;Database=' + @excelFilePath + ''', 
			''SELECT * FROM [' + @excelSheetName + '$]'')')


		----- **EDITTING INPUTTED EXCEL DATA FOR THE INSERTED TABLE AND ADDING A PKEY
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''SpecialOvtHrs'')
				UPDATE dbo.TempImportTable SET SpecialOvtHrs = 0 WHERE SpecialOvtHrs IS NULL')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''OvtAmt'')
				UPDATE dbo.TempImportTable SET OvtAmt = 0 WHERE OvtAmt IS NULL')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''RegAmtProjectFunctionalCurrency'')
				UPDATE dbo.TempImportTable SET RegAmtProjectFunctionalCurrency = 0 WHERE RegAmtProjectFunctionalCurrency IS NULL')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''OvtAmtProjectFunctionalCurrency'')
				UPDATE dbo.TempImportTable SET OvtAmtProjectFunctionalCurrency = 0 WHERE OvtAmtProjectFunctionalCurrency IS NULL')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''SpecialOvtAmtProjectFunctionalCurrency'')
				UPDATE dbo.TempImportTable SET SpecialOvtAmtProjectFunctionalCurrency = 0 WHERE SpecialOvtAmtProjectFunctionalCurrency IS NULL')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''SpecialOvtAmt'')
				UPDATE dbo.TempImportTable SET SpecialOvtAmt = 0 WHERE SpecialOvtAmt IS NULL')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''RealizationAmount'')
				UPDATE dbo.TempImportTable SET RealizationAmount = 0 WHERE RealizationAmount IS NULL')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''PKey'')
				UPDATE dbo.TempImportTable SET PKey = Replace(NewID(), ''-'', '''') WHERE PKey IS NULL')


		----- ** ALTER COLUMNS
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''Batch'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN Batch VARCHAR(30)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''Employee'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN Employee VARCHAR(20)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''PKey'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN PKey VARCHAR(44)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''Seq'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN Seq INT')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''TransDate'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN TransDate DATETIME')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''WBS1'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN WBS1 VARCHAR(50)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''WBS2'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN WBS2 VARCHAR(50)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''WBS3'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN WBS3 VARCHAR(50)')		
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''LaborCode'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN LaborCode VARCHAR(50)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''RegHrs'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN RegHrs DECIMAL(19, 4)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''OvtHrs'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN OvtHrs DECIMAL(19, 4)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''RegAmt'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN RegAmt DECIMAL(19, 4)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''OvtAmt'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN OvtAmt DECIMAL(19, 4)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''BillExt'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN BillExt DECIMAL(19, 4)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''SuppressBill'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN SuppressBill VARCHAR(1)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''TransComment'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN TransComment VARCHAR(MAX)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''SpecialOvtHrs'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN SpecialOvtHrs DECIMAL(19, 4)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''SpecialOvtAmt'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN SpecialOvtAmt DECIMAL(19, 4)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''BillCategory'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN BillCategory DECIMAL(19, 4)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''RegAmtProjectFunctionalCurrency'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN RegAmtProjectFunctionalCurrency DECIMAL(19, 4)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''OvtAmtProjectFunctionalCurrency'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN OvtAmtProjectFunctionalCurrency DECIMAL(19, 4)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''SpecialOvtAmtProjectFunctionalCurrency'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN SpecialOvtAmtProjectFunctionalCurrency DECIMAL(19, 4)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''LinkCompany'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN LinkCompany VARCHAR(14)')
		EXEC('IF EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''RealizationAmount'')
				ALTER TABLE dbo.TempImportTable ALTER COLUMN RealizationAmount DECIMAL(19, 4)')
		EXEC('IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Object_ID = Object_ID(N''dbo.TempImportTable'') AND Name = N''Company'')
				ALTER TABLE dbo.TempImportTable ADD Company VARCHAR(14)')

		EXEC('UPDATE dbo.TempImportTable SET Company = ''01'' WHERE Company IS NULL')

		----- **INSERTING INTO laDetail table from TempImportTable
		EXEC('INSERT INTO laDetail
		(
			Batch
			, Employee
			, PKey
			, Seq
			, TransDate
			, WBS1
			, WBS2
			, WBS3
			, LaborCode
			, RegHrs
			, OvtHrs
			, RegAmt
			, OvtAmt
			, BillExt
			, SuppressBill
			, TransComment
			, SpecialOvtHrs
			, SpecialOvtAmt
			, BillCategory
			, RegAmtProjectFunctionalCurrency
			, OvtAmtProjectFunctionalCurrency
			, SpecialOvtAmtProjectFunctionalCurrency
			, LinkCompany
			, RealizationAmount
		)
		SELECT Batch
			, Employee
			, PKey
			, Seq
			, TransDate
			, WBS1
			, WBS2
			, WBS3
			, LaborCode
			, RegHrs
			, OvtHrs
			, RegAmt
			, OvtAmt
			, BillExt
			, SuppressBill
			, TransComment
			, SpecialOvtHrs
			, SpecialOvtAmt
			, BillCategory
			, RegAmtProjectFunctionalCurrency
			, OvtAmtProjectFunctionalCurrency
			, SpecialOvtAmtProjectFunctionalCurrency
			, LinkCompany
			, RealizationAmount 
		FROM dbo.TempImportTable')

		----- **INSERTING INTO laMaster table from TempImportTable
		EXEC('INSERT INTO laMaster
		(
			Batch
			, Seq
			, Employee
		)
		SELECT DISTINCT Batch
			, 0 AS Seq
			, Employee 
		FROM dbo.TempImportTable')

		EXEC('UPDATE laMaster
			SET laMaster.Posted = ''N''
			FROM laMaster Y, dbo.TempImportTable X
			WHERE Y.Posted IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laMaster
			SET laMaster.Status = ''N''
			FROM laMaster Y, dbo.TempImportTable X
			WHERE Y.Status IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laMaster
			SET laMaster.Employee = ''001975'' 
			FROM laMaster Y, dbo.TempImportTable X
			WHERE Y.Employee IS NULL AND Y.Batch = X.Batch')


		 --- **INSERTING INTO laControl table from TempImportTable
		EXEC('INSERT INTO laControl
		(
			Batch
			, Company
		)
		SELECT DISTINCT Batch
		, Company 
		FROM dbo.TempImportTable')

		EXEC('UPDATE laControl
			SET laControl.PostPeriod = 0
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.PostPeriod IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.PostSeq = 0
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.PostSeq IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.Recurring = ''N''
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.Recurring IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.Selected = ''N''
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.Selected IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.Posted = ''N''
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.Posted IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.Creator = ''AH1975''
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.Creator IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.Period = CONVERT(NVARCHAR(6), GETDATE(), 112)
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.Period IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.EndDate = CURRENT_TIMESTAMP
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.EndDate IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.RegHrsTotal = 0
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.RegHrsTotal IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.OvtHrsTotal = 0
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.OvtHrsTotal IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.RegAmtTotal = 0
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.RegAmtTotal IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.OvtAmtTotal = 0
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.OvtAmtTotal IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.BillExtTotal = 0
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.BillExtTotal IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.SpecialOvtHrsTotal = 0
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.SpecialOvtHrsTotal IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.SpecialOvtAmtTotal = 0
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.SpecialOvtAmtTotal IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.Company = ''01''
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.Company IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.RegAmtProjectFunctionalCurrencyTotal = 0
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.RegAmtProjectFunctionalCurrencyTotal IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.OvtAmtProjectFunctionalCurrencyTotal = 0
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.OvtAmtProjectFunctionalCurrencyTotal IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.SpecialOvtAmtProjectFunctionalCurrencyTotal = 0
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.SpecialOvtAmtProjectFunctionalCurrencyTotal IS NULL AND Y.Batch = X.Batch')

		EXEC('UPDATE laControl
			SET laControl.RealizationTotal = 0
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.RealizationTotal IS NULL AND Y.Batch = X.Batch')

		EXEC('Update laControl
			SET laControl.DiaryNo = 0
			FROM laControl Y, dbo.TempImportTable X
			WHERE Y.DiaryNo IS NULL AND Y.Batch = X.Batch')


		--- **CLEARING TempImportTable table, BELOW
		IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'TempImportTable')
			DROP TABLE dbo.TempImportTable

		-- Return 0 to the calling program to indicate success.  
	END TRY
	BEGIN CATCH
		SELECT   
			ERROR_NUMBER() AS ErrorNumber  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_PROCEDURE() AS ErrorProcedure  
			,ERROR_LINE() AS ErrorLine  
			,ERROR_MESSAGE() AS ErrorMessage

		IF @@TRANCOUNT > 0  
			ROLLBACK TRANSACTION  

		-- Return 99 to the calling program to indicate failure.  
		PRINT N'An error occurred importing the Labor Adjustment file.';  
		RETURN 99  
	END CATCH

	IF @@TRANCOUNT > 0  
		COMMIT TRANSACTION

	PRINT N'The laControl, laDetail, and laMaster tables have been updated with the Labor Adjustment batch from the excel file.';  
	RETURN 0;  

END
GO
