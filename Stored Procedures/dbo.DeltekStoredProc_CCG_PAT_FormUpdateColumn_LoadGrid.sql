SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_FormUpdateColumn_LoadGrid]
	@InitialValue	Nvarchar(200),
	@ColumnUpdate	Nvarchar(100),
	@DatabaseField	Nvarchar(100),
	@visionTable	Nvarchar(100),
	@invoiceGroup	Nvarchar(max)
AS
BEGIN
	-- [DeltekStoredProc_CCG_PAT_FormUpdateColumn_LoadGrid] '','','','',''
	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max);

	SET @sql = N'
        SELECT PR.WBS1, PR.WBS2, PR.WBS3, PR.Name, ' + @InitialValue + '(PR.WBS1, PR.WBS2, PR.WBS3) as ''' + @ColumnUpdate + ''',
                eicc.' + @DatabaseField + ', ''True'' as ''Update'', '''' as Result
            FROM PR
                INNER JOIN CCG_EI_CustomColumns eicc
					on PR.WBS1 = eicc.WBS1 and PR.WBS2 = eicc.WBS2 and PR.WBS3 = eicc.WBS3
                INNER JOIN ProjectCustomTabFields PCTF1
					on PR.WBS1 = PCTF1.WBS1 and PCTF1.WBS2 = N'' ''
						and PCTF1.custInvoiceGroup = ''' + Replace(@invoiceGroup, '''', '''''') + ''' ' +
				(CASE WHEN ISNULL(@visionTable, '') <> '' AND UPPER(@visionTable) <> 'PR' THEN '
                LEFT JOIN ' + @visionTable + ' on PR.WBS1 = ' + @visionTable + '.WBS1
					and PR.WBS2 = ' + @visionTable + '.WBS2 and PR.WBS3 = ' + @visionTable + '.WBS3 ' ELSE '' END) + '
            WHERE NOT eicc.' + @DatabaseField + ' is NULL
				and eicc.' + @DatabaseField + ' <> '  + @InitialValue + '(PR.WBS1, PR.WBS2, PR.WBS3)
            ORDER BY 1, 2, 3';
	EXEC(@sql);
END;

GO
