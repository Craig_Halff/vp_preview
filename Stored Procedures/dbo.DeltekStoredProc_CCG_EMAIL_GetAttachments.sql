SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EMAIL_GetAttachments]
	@msgSeqList varchar(max)
AS BEGIN
	SET NOCOUNT ON;
	DECLARE @sSQL		varchar(max);

	--Replace don't allow single quotes to be passed in to reduce SQL injection risk
	set @sSQL = 'select MessageSeq,Filename,Item,KeepAfterSent from CCG_EMAIL_ATTACHMENTS where MessageSeq in (' + Replace(@msgSeqList,'''','') + ')'
	exec(@sSQL	)

END;

GO
