SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_EI_Load_Stages]
	@VISION_LANGUAGE varchar(10)
AS
BEGIN
	-- EXEC [dbo].[spCCG_EI_Load_Stages] 'en-US'
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SELECT T1.Stage, IsNull(T2.StageLabel, '') as StageLabel, IsNull(T2.StageDescription, '') as StageDescription,
			T1.Status, T1.InvoiceType, T1.RightDoc, T1.RequireStamp, T1.Weight
		FROM CCG_EI_ConfigStages T1
			left join CCG_EI_ConfigStagesDescriptions T2 on T2.Stage = T1.Stage and T2.UICultureName = @VISION_LANGUAGE
			left join CCG_EI_ConfigStageFlows ConfigStageFlows on T1.Stage = ConfigStageFlows.Stage
		GROUP BY T1.Stage, T2.StageLabel, T2.StageDescription, T1.Status, T1.InvoiceType, T1.RightDoc, T1.RequireStamp, T1.Weight  -- all stage columns need to be listed here
		ORDER BY avg(isnull(StageOrder, 99)), T2.StageLabel
END
GO
