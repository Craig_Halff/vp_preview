SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPDelAssignmentForTaskID]
  @strRowID nvarchar(255),
  @bitCalledFromRM bit = 0
AS

BEGIN -- Procedure stRPDelAssignmentForTaskID

  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strResource nvarchar(20)
  DECLARE @strResourceType varchar(1)
  DECLARE @strResourceID nvarchar(20)
  DECLARE @strGenericResourceID nvarchar(20)
  DECLARE @strOutlineNumber varchar(255)
  DECLARE @strNextRowID nvarchar(255)

  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))
  SET @strResourceType = SUBSTRING(@strRowID, 1, 1)
  SET @strResource = SUBSTRING(@strRowID, 3, CHARINDEX('|', @strRowID) - 3)
  
IF (@strResourceType = 'E')
  BEGIN
    SET @strResourceID = @strResource
	SET @strGenericResourceID = '<null>'
  END
ELSE
  BEGIN
	SET @strGenericResourceID = @strResource
    SET @strResourceID = '<null>'
  END

  -- Get plan id, outline number
  SELECT 
	@strPlanID = PlanID,
    @strOutlineNumber = OutlineNumber
    FROM PNTask
	WHERE PNTask.TaskID = @strTaskID

  DECLARE csrTaskID CURSOR FOR
    SELECT @strResourceType + '~' + @strResource + '|' + A.TaskID
	FROM PNAssignment A
	  INNER JOIN PNTask T ON A.TaskID = T.TaskID
	WHERE A.PlanID = @strPlanID 
	 AND  T.OutlineNumber LIKE @strOutlineNumber + '%'
	 AND  ISNULL(A.ResourceID, '<null>') = @strResourceID 
	 AND  ISNULL(A.GenericResourceID, '<null>') = @strGenericResourceID

  OPEN csrTaskID

  FETCH NEXT FROM csrTaskID INTO @strNextRowID

  WHILE (@@FETCH_STATUS = 0)
    BEGIN
    
	  EXECUTE dbo.stRPDelAssignment @strNextRowID, @bitCalledFromRM
            
      FETCH NEXT FROM csrTaskID INTO @strNextRowID

    END -- While


  CLOSE csrTaskID
  DEALLOCATE csrTaskID

 
END -- stRPDelAssignmentForTaskID
GO
