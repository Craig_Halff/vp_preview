SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ngRPShiftAssigment]
  @strAssignmentID varchar(32),
  @strFromStartDate varchar(8),
  @strFromEndDate varchar(8),
  @strToStartDate varchar(8),
  @strToEndDate varchar(8),
  @bitShiftAll bit, /* 0 = Shift using the "From" range, 1 = Shift the entire Assignment Row */
  @bitFixStartDate bit /* 1 = Hold "To" StartDate fixed, 0 = Hold "To" EndDate fixed */
AS

BEGIN -- Procedure ngRPShiftAssigment

  SET NOCOUNT ON
  
  DECLARE @dtFromStartDate datetime
  DECLARE @dtFromEndDate datetime
  DECLARE @dtMinTPD datetime
  DECLARE @dtMaxTPD datetime
  DECLARE @dtBeforeDate datetime
  DECLARE @dtAfterDate datetime

  DECLARE @dtToOrgStartDate datetime
  DECLARE @dtToOrgEndDate datetime

  DECLARE @dtToTPDStartDate datetime
  DECLARE @dtToTPDEndDate datetime

  DECLARE @dtASGStartDate datetime
  DECLARE @dtASGEndDate datetime
  DECLARE @dtToASGStartDate datetime
  DECLARE @dtToASGEndDate datetime

  DECLARE @strCompany Nvarchar(14)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strResourceID Nvarchar(20)
  DECLARE @strGenericResourceID Nvarchar(20)
  DECLARE @strUserName Nvarchar(32)

  DECLARE @siGRMethod smallint
  DECLARE @siCostRtMethod smallint
  DECLARE @siBillingRtMethod smallint
  
  DECLARE @intCostRtTableNo int
  DECLARE @intBillingRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int

  DECLARE @siLabRateDecimals smallint
  DECLARE @siHrDecimals smallint
  
  DECLARE @decBeforeWD decimal(19,4) = 0
  DECLARE @decAfterWD decimal(19,4) = 0
  DECLARE @decASGOffsetWD decimal(19,4) = 0
  DECLARE @decASGDurationWD decimal(19,4) = 0

  DECLARE @intMinRowSeq int = 0
  DECLARE @intMaxRowSeq int = 0

  -- Declare Temp tables.
  
	DECLARE @tabCalendar TABLE (
		StartDate	datetime,
		EndDate	datetime
    UNIQUE(StartDate, EndDate)
	)

  DECLARE @tabAssignment TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ResourceID Nvarchar(20) COLLATE database_default,
    GenericResourceID Nvarchar(20) COLLATE database_default,
    Category smallint,
    GRLBCD Nvarchar(14) COLLATE database_default,
    CostRate decimal(19,4),
    BillingRate decimal(19,4),
    StartDate datetime,
    EndDate datetime,
    AT_OutlineNumber varchar(255) COLLATE database_default
    UNIQUE(PlanID, TaskID, AssignmentID)
  )

	DECLARE @tabFromTPD TABLE(
	  TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
	  AssignmentID varchar(32) COLLATE database_default,
    StartDate datetime, 
    EndDate datetime, 
    PeriodHrs decimal(19,4),
    AT_OutlineNumber varchar(255) COLLATE database_default
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, StartDate, EndDate)
  ) 

	DECLARE @tabS1TPD TABLE (
	  RowSeq int,
    NewTPID varchar(32) COLLATE database_default,
    TimePhaseID varchar(32) COLLATE database_default,
    CIStartDate datetime, 
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    StartDate datetime, 
    EndDate datetime, 
    PeriodHrs decimal(19,4), 
    PeriodWD decimal(19,4)
    UNIQUE(RowSeq, NewTPID, PlanID, TaskID, AssignmentID, CIStartDate, StartDate, EndDate)
  )

	DECLARE @tabS2TPD TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    StartDate datetime, 
    EndDate datetime, 
    PeriodHrs decimal(19,4), 
    CostRate decimal(19,4),
    BillingRate decimal(19,4)
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, StartDate, EndDate)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  SELECT @strCompany = dbo.GetActiveCompany()
  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

  SELECT 
    @siHrDecimals = HrDecimals,
    @siLabRateDecimals = LabRateDecimals
    FROM CFGResourcePlanning Where Company = @strCompany

  -- Set Dates

  SET @dtFromStartDate = CONVERT(datetime, @strFromStartDate)
  SET @dtFromEndDate = CONVERT(datetime, @strFromEndDate)
  SET @dtBeforeDate = DATEADD(DAY, -1, @dtFromStartDate)
  SET @dtAfterDate = DATEADD(DAY, 1, @dtFromEndDate)

  SET @dtToOrgStartDate = CONVERT(datetime, @strToStartDate)
  SET @dtToOrgEndDate = CONVERT(datetime, @strToEndDate)
  SET @dtToTPDStartDate = CONVERT(datetime, @strToStartDate)
  SET @dtToTPDEndDate = CONVERT(datetime, @strToEndDate)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabAssignment(
    PlanID,
    TaskID,
    AssignmentID,
    ResourceID,
    GenericResourceID,
    Category,
    GRLBCD,
    CostRate,
    BillingRate,
    StartDate,
    EndDate,
    AT_OutlineNumber
  )
    SELECT DISTINCT
      A.PlanID,
      A.TaskID,
      A.AssignmentID,
      A.ResourceID,
      A.GenericResourceID,
      A.Category,
      A.GRLBCD,
      A.CostRate,
      A.BillingRate,
      A.StartDate,
      A.EndDate,
      AT.OutlineNumber AS AT_OutlineNumber
      FROM RPAssignment AS A
        INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
      WHERE
        A.AssignmentID = @strAssignmentID AND
        A.StartDate <= @dtFromEndDate AND A.EndDate >= @dtFromStartDate

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get various parameters from Plan, Task, and Assigment.

  SELECT 
    @strPlanID = T.PlanID,
    @strTaskID = A.TaskID,
    @strCompany = P.Company,
    @siGRMethod = P.GRMethod,
    @intGenResTableNo = P.GenResTableNo,
    @intGRBillTableNo = P.GRBillTableNo,
    @siCostRtMethod = P.CostRtMethod,
    @siBillingRtMethod = P.BillingRtMethod,
    @intCostRtTableNo = P.CostRtTableNo,
    @intBillingRtTableNo = P.BillingRtTableNo,
    @strResourceID = A.ResourceID,
    @strGenericResourceID = A.GenericResourceID,
    @dtASGStartDate = A.StartDate,
    @dtASGEndDate = A.EndDate
    FROM @tabAssignment AS A
      INNER JOIN RPTask AS T ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
      INNER JOIN RPPlan AS P ON A.PlanID = P.PlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collecting Labor Time-Phased Data to be used in subsequent calculations.
  -- The TPD records come from only the Assignment record indentified by @strAssignmentID.

  INSERT @tabFromTPD(
    TimePhaseID,
    PlanID,
    TaskID,
    AssignmentID,
    StartDate,
    EndDate,
    PeriodHrs,
    AT_OutlineNumber
  )
    SELECT
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID,
      TPD.TaskID AS TaskID,
      TPD.AssignmentID AS AssignmentID,
      TPD.StartDate AS StartDate,
      TPD.EndDate AS EndDate,
      TPD.PeriodHrs AS PeriodHrs,
      A.AT_OutlineNumber AS AT_OutlineNumber
      FROM RPPlannedLabor AS TPD
        INNER JOIN @tabAssignment AS A ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID AND TPD.AssignmentID = A.AssignmentID AND TPD.AssignmentID IS NOT NULL
      WHERE TPD.PlanID = @strPlanID AND TPD.PeriodHrs <> 0 AND 
        TPD.StartDate <= @dtFromEndDate AND TPD.EndDate >= @dtFromStartDate

  IF (@@ROWCOUNT > 0) /* If there is no TPD in the From range then do not need to do shifting of TPD. */
    BEGIN

      SELECT 
        @dtMinTPD = MIN(StartDate), 
        @dtMaxTPD = MAX(EndDate)
        FROM @tabFromTPD AS TPD

      SET @decBeforeWD = dbo.DLTK$NumWorkingDays(@dtMinTPD, @dtBeforeDate, @strCompany)
      SET @decAfterWD = dbo.DLTK$NumWorkingDays(@dtAfterDate, @dtMaxTPD, @strCompany)

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- Build Calendar.
      -- Calendar has at most 3 ranges {Before, Main, After}.

      IF (@decBeforeWD > 0)
        BEGIN

	        INSERT @tabCalendar(
	          StartDate,
	          EndDate
	        )	
		        SELECT 
		          @dtMinTPD AS StartDate,
		          @dtBeforeDate AS EndDate

        END

	    INSERT @tabCalendar(
	      StartDate,
	      EndDate
	    )	
		    SELECT 
		      @dtFromStartDate AS StartDate,
		      @dtFromEndDate AS EndDate

      IF (@decAfterWD > 0)
        BEGIN

	        INSERT @tabCalendar(
	          StartDate,
	          EndDate
	        )	
		        SELECT 
		          @dtAfterDate AS StartDate,
		          @dtMaxTPD AS EndDate

        END

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- Split up TPD in @tabFromTPD to match with Calendar Intervals.
      -- @tabFromTPD could contain chunks that are larger than a calendar interval which need to be broken up.
      -- @tabFromTPD could contain chunks that are overlapping with the calendar interval boundaries which need to be broken off.
      -- @tabFromTPD could contain chunks that are smaller and contain within the span of a calendar interval.

      INSERT @tabS1TPD(
        RowSeq,
        NewTPID,
        TimePhaseID,
        CIStartDate,
        PlanID, 
        TaskID,
        AssignmentID,
        StartDate, 
        EndDate, 
        PeriodHrs,
        PeriodWD
       )
         SELECT
          ROW_NUMBER() OVER (ORDER BY AT_OutlineNumber, StartDate) AS RowSeq,
          REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS NewTPID,
          TimePhaseID AS TimePhaseID,
          CIStartDate AS CIStartDate,
          PlanID AS PlanID, 
          TaskID AS TaskID,
          AssignmentID AS AssignmentID,
          StartDate AS StartDate, 
          EndDate AS EndDate, 
          ROUND(ISNULL(PeriodHrs, 0), @siHrDecimals) AS PeriodHrs,
          dbo.DLTK$NumWorkingDays(StartDate, EndDate, @strCompany) AS PeriodWD
        FROM (
          SELECT 
            CI.StartDate AS CIStartDate, 
            TPD.TimePhaseID AS TimePhaseID,
            TPD.PlanID AS PlanID, 
            TPD.TaskID AS TaskID,
            TPD.AssignmentID AS AssignmentID,
            CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
            CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
            CASE 
              WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
              THEN PeriodHrs * 
                dbo.DLTK$ProrateRatio(
                  CASE 
                    WHEN TPD.StartDate > CI.StartDate 
                    THEN TPD.StartDate 
                    ELSE CI.StartDate 
                  END, 
                  CASE 
                    WHEN TPD.EndDate < CI.EndDate 
                    THEN TPD.EndDate 
                    ELSE CI.EndDate 
                  END, 
                  TPD.StartDate, TPD.EndDate,
                  @strCompany)
              ELSE PeriodHrs 
            END AS PeriodHrs,
            TPD.AT_OutlineNumber AS AT_OutlineNumber
            FROM @tabCalendar AS CI 
              INNER JOIN @tabFromTPD AS TPD 
                ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
        ) AS X
        WHERE (PeriodHrs IS NOT NULL AND PeriodHrs > 0)

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     
      -- Adjust time-phased data to compensate for rounding errors after the splitting process.

      UPDATE @tabS1TPD SET PeriodHrs = (TPD.PeriodHrs + D.DeltaHrs)
        FROM @tabS1TPD AS TPD 
          INNER JOIN (
            SELECT YTPD.TimePhaseID AS TimePhaseID, (YTPD.PeriodHrs - SUM(ISNULL(XTPD.PeriodHrs, 0))) AS DeltaHrs
              FROM @tabS1TPD AS XTPD 
                INNER JOIN @tabFromTPD AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
              GROUP BY YTPD.TimePhaseID, YTPD.PeriodHrs
          ) AS D ON TPD.TimePhaseID = D.TimePhaseID
        WHERE NewTPID IN (
          SELECT NewTPID FROM @tabS1TPD AS ATPD 
            INNER JOIN (
              SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabS1TPD GROUP BY TimePhaseID
            ) AS BTPD ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate
        )
             
    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- At this point we have a table with TPD rows aligned on the scope boundaries.

      -- Copy from @tabS1TPD those TPD rows that are outside of scope to @tabS2TPD.
      -- These TPD rows will retain the original hours as in @tabS1TPD.

      INSERT @tabS2TPD(
        TimePhaseID,
        PlanID, 
        TaskID,
        AssignmentID,
        StartDate, 
        EndDate, 
        PeriodHrs,
        CostRate,
        BillingRate
       )
         SELECT
          TPD.NewTPID AS TimePhaseID,
          TPD.PlanID AS PlanID, 
          TPD.TaskID AS TaskID,
          TPD.AssignmentID AS AssignmentID,
          TPD.StartDate AS StartDate, 
          TPD.EndDate AS EndDate, 
          TPD.PeriodHrs AS PeriodHrs,
          dbo.RP$LabRate(
            A.ResourceID,
            ISNULL(A.Category, 0),
            A.GRLBCD,
            NULL,
            @siCostRtMethod,
            @intCostRtTableNo,
            @siGRMethod,
            @intGenResTableNo,
            TPD.StartDate,
            TPD.EndDate,
            @siLabRateDecimals,
            0,
            ISNULL(A.CostRate, 0)
          ) AS CostRate,
          dbo.RP$LabRate(
            A.ResourceID,
            ISNULL(A.Category, 0),
            A.GRLBCD,
            NULL,
            @siBillingRtMethod,
            @intBillingRtTableNo,
            @siGRMethod,
            @intGRBillTableNo,
            TPD.StartDate,
            TPD.EndDate,
            @siLabRateDecimals,
            0,
            ISNULL(A.BillingRate, 0)
          ) AS BillingRate
        FROM @tabS1TPD AS TPD
          INNER JOIN @tabAssignment AS A ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID AND TPD.AssignmentID = A.AssignmentID
        WHERE (TPD.EndDate < @dtFromStartDate OR TPD.StartDate > @dtFromEndDate) AND TPD.PeriodHrs > 0

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      --> Need to adjust "To" Start and End Dates for special circumstances when Shift ALL <--
      --> Need to do this in two passes.                                                   <--
      
      --> Pass #1: 
      -->   Determine new "To" Start/End Dates for TPD. 
      -->   If there is a gap between Assignment Start/End Dates and TPD Start/End Dates
      -->   then need to offset TPD Start/End Dates the same number of work days from Assignment Start/End Dates.
      --> Pass #2: 
      -->   After accounted for the leading/trailing edge gap, we need to determine the "To" TPD Dates range.
      -->   If @bitFixStartDate = 1, then "To" End Date = "To" Start Date + Number of Work Days of TPD in "From" range.
      -->   If @bitFixStartDate = 0, then "To" Start Date = "To" End Date - Number of Work Days of TPD in "From" range.

      IF (@bitShiftAll = 1)
        BEGIN

          SELECT @dtToTPDStartDate =
            CASE 
              WHEN (@bitFixStartDate = 1 AND @dtFromStartDate < @dtMinTPD)
              THEN
                dbo.DLTK$AddBusinessDays(
                  @dtToOrgStartDate, 
                  (dbo.DLTK$NumWorkingDays(@dtFromStartDate, @dtMinTPD, @strCompany) - 1), 
                  @strCompany
                )
              ELSE @dtToTPDStartDate
            END

          SELECT @dtToTPDEndDate =
            CASE 
              WHEN (@bitFixStartDate = 0 AND @dtFromEndDate > @dtMaxTPD)
              THEN
                dbo.DLTK$AddBusinessDays(
                  @dtToOrgEndDate, 
                  (dbo.DLTK$NumWorkingDays(@dtFromEndDate, @dtMaxTPD, @strCompany) - 1), 
                  @strCompany
                )
              ELSE @dtToTPDEndDate
            END

        --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

          SELECT @dtToTPDStartDate =
            CASE
              WHEN @bitFixStartDate = 0 
              THEN
                dbo.DLTK$AddBusinessDays(
                  @dtToTPDEndDate,
                  -1 * ((
                    SELECT 
                      dbo.DLTK$NumWorkingDays(MIN(StartDate), MAX(EndDate), @strCompany)
                      FROM @tabS1TPD AS TPD
                      WHERE TPD.StartDate <= @dtFromEndDate AND TPD.EndDate >= @dtFromStartDate AND
                        (TPD.PeriodHrs IS NOT NULL AND TPD.PeriodHrs > 0)
                  ) - 1),
                  @strCompany
                )
              ELSE @dtToTPDStartDate
            END

          SELECT @dtToTPDEndDate =
            CASE
              WHEN @bitFixStartDate = 1 
              THEN
                dbo.DLTK$AddBusinessDays(
                  @dtToTPDStartDate,
                  ((
                    SELECT 
                      dbo.DLTK$NumWorkingDays(MIN(StartDate), MAX(EndDate), @strCompany)
                      FROM @tabS1TPD AS TPD
                      WHERE TPD.StartDate <= @dtFromEndDate AND TPD.EndDate >= @dtFromStartDate AND
                        (TPD.PeriodHrs IS NOT NULL AND TPD.PeriodHrs > 0)
                  ) - 1),
                  @strCompany
                )
              ELSE @dtToTPDEndDate
            END

        END

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      -- Find the MIN and MAX RowSeq.
  
      SELECT 
        @intMinRowSeq = MIN(RowSeq),
        @intMaxRowSeq = MAX(RowSeq)
        FROM @tabS1TPD AS TPD
        WHERE TPD.StartDate <= @dtFromEndDate AND TPD.EndDate >= @dtFromStartDate AND
          (TPD.PeriodHrs IS NOT NULL AND TPD.PeriodHrs > 0)

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Copy rows from @tabS1TPD to @tabS2TPD with new set of {StartDate, EndDate}

    ;

      WITH NewDates AS (
        SELECT 
          TPD.RowSeq AS RowSeq,
          TPD.NewTPID AS NewTPID,
          @dtToTPDStartDate AS NewStartDate,
          CASE
            WHEN TPD.RowSeq = @intMaxRowSeq
            THEN @dtToTPDEndDate
            ELSE dbo.DLTK$AddBusinessDays(@dtToTPDStartDate, (TPD.PeriodWD - 1), @strCompany)
          END  AS NewEndDate,
          TPD.PeriodWD AS PeriodWD,
          ISNULL((SELECT DATEDIFF(D, TPD.EndDate, NTPD.StartDate) FROM @tabS1TPD AS NTPD WHERE NTPD.RowSeq = TPD.RowSeq + 1), 0) AS GapDays
        FROM @tabS1TPD AS TPD
        WHERE 
          TPD.StartDate <= @dtFromEndDate AND TPD.EndDate >= @dtFromStartDate 
          AND TPD.RowSeq = @intMinRowSeq
          AND TPD.PeriodHrs IS NOT NULL AND TPD.PeriodHrs > 0
        UNION ALL
        SELECT 
          TPD.RowSeq AS RowSeq,
          TPD.NewTPID AS NewTPID,
          dbo.DLTK$AddBusinessDays(ND.NewEndDate, ND.GapDays, @strCompany) AS NewStartDate,
          CASE
            WHEN TPD.RowSeq = @intMaxRowSeq
            THEN @dtToTPDEndDate
            ELSE 
              dbo.DLTK$AddBusinessDays(
                dbo.DLTK$AddBusinessDays(ND.NewEndDate, ND.GapDays, @strCompany), 
                (TPD.PeriodWD - 1), 
                @strCompany
              )
          END AS NewEndDate,
          TPD.PeriodWD AS PeriodWD,
          ISNULL((SELECT DATEDIFF(D, TPD.EndDate, NTPD.StartDate) FROM @tabS1TPD AS NTPD WHERE NTPD.RowSeq = TPD.RowSeq + 1), 0) AS GapDays
        FROM @tabS1TPD AS TPD
          INNER JOIN NewDates AS ND
            ON TPD.RowSeq = ND.RowSeq + 1 
        WHERE 
          TPD.RowSeq <= @intMaxRowSeq
      )

      INSERT @tabS2TPD(
        TimePhaseID,
        PlanID, 
        TaskID,
        AssignmentID,
        StartDate, 
        EndDate, 
        PeriodHrs,
        CostRate,
        BillingRate
      )
        SELECT
          TPD.NewTPID AS TimePhaseID,
          TPD.PlanID AS PlanID, 
          TPD.TaskID AS TaskID,
          TPD.AssignmentID AS AssignmentID,
          ND.NewStartDate AS StartDate, 
          ND.NewEndDate AS EndDate, 
          TPD.PeriodHrs AS PeriodHrs,
          dbo.RP$LabRate(
            A.ResourceID,
            ISNULL(A.Category, 0),
            A.GRLBCD,
            NULL,
            @siCostRtMethod,
            @intCostRtTableNo,
            @siGRMethod,
            @intGenResTableNo,
            ND.NewStartDate,
            ND.NewEndDate,
            @siLabRateDecimals,
            0,
            ISNULL(A.CostRate, 0)
          ) AS CostRate,
          dbo.RP$LabRate(
            A.ResourceID,
            ISNULL(A.Category, 0),
            A.GRLBCD,
            NULL,
            @siBillingRtMethod,
            @intBillingRtTableNo,
            @siGRMethod,
            @intGRBillTableNo,
            ND.NewStartDate,
            ND.NewEndDate,
            @siLabRateDecimals,
            0,
            ISNULL(A.BillingRate, 0)
          ) AS BillingRate
      FROM (
        SELECT
          XTPD.RowSeq AS RowSeq,
          XTPD.NewTPID AS NewTPID,
          XTPD.PlanID AS PlanID, 
          XTPD.TaskID AS TaskID,
          XTPD.AssignmentID AS AssignmentID,
          XTPD.PeriodHrs AS PeriodHrs
          FROM @tabS1TPD AS XTPD
          WHERE 
            XTPD.StartDate <= @dtFromEndDate AND XTPD.EndDate >= @dtFromStartDate 
            AND (XTPD.PeriodHrs IS NOT NULL AND XTPD.PeriodHrs > 0)
      ) AS TPD
        INNER JOIN NewDates AS ND ON TPD.RowSeq = ND.RowSeq AND TPD.NewTPID = ND.NewTPID
        INNER JOIN @tabAssignment AS A ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID AND TPD.AssignmentID = A.AssignmentID
        OPTION (MAXRECURSION 0)
        
    ;

      -- At this point we have all of the TPD at correct {StartDate, EndDate} in @tabS2TPD.
      -- Note that the new TPD at new {StartDate, EndDate} could be overlapping with existing TPD in that date range.

    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      DELETE RPPlannedLabor 
        WHERE 
          PlanID = @strPlanID 
          AND (
            TimePhaseID IN (
              SELECT DISTINCT TimePhaseID FROM @tabFromTPD
            ) 
            OR PeriodHrs = 0
          )

      INSERT RPPlannedLabor(
        TimePhaseID,
        PlanID, 
        TaskID,
        AssignmentID,
        StartDate, 
        EndDate, 
        PeriodHrs,
        CostRate,
        BillingRate,
        CreateUser,
        ModUser,
        CreateDate,
        ModDate
      )
         SELECT
          TPD.TimePhaseID AS TimePhaseID,
          TPD.PlanID AS PlanID, 
          TPD.TaskID AS TaskID,
          TPD.AssignmentID AS AssignmentID,
          TPD.StartDate AS StartDate, 
          TPD.EndDate AS EndDate, 
          TPD.PeriodHrs AS PeriodHrs,
          TPD.CostRate,
          TPD.BillingRate,
          CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'ngRPShiftAssigment' ELSE 'SHA_' + @strUserName END AS CreateUser,
          CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'ngRPShiftAssigment' ELSE 'SHA_' + @strUserName END AS ModUser,
          LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As CreateDate,
          LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As ModDate 
        FROM @tabS2TPD AS TPD
        WHERE PeriodHrs > 0

    END /* END IF (@@ROWCOUNT > 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@bitShiftAll = 1)
    BEGIN

      -- Calculating WD for Assignment Duration and Offset.

      SELECT @decASGDurationWD = dbo.DLTK$NumWorkingDays(@dtASGStartDate, @dtASGEndDate, @strCompany) 

      SELECT @decASGOffsetWD = 
        CASE 
          WHEN @bitFixStartDate = 1
          THEN
            CASE 
              WHEN @dtFromStartDate = @dtASGStartDate 
              THEN 0
              ELSE dbo.DLTK$NumWorkingDays(@dtFromStartDate, @dtASGStartDate, @strCompany)
            END
          ELSE
            CASE
              WHEN @dtASGEndDate = @dtFromEndDate
              THEN 0
              ELSE dbo.DLTK$NumWorkingDays(@dtASGEndDate, @dtFromEndDate, @strCompany)
            END
        END
                  
      -- Need to calculate @dtToASGStartDate or @dtToASGEndDate because of @bitFixStartDate setiting
                  
      SELECT
        @dtToASGStartDate = 
          CASE 
            WHEN @bitFixStartDate = 0
            THEN
              dbo.DLTK$AddBusinessDays(
                @dtToOrgEndDate, 
                -1 * ((@decASGDurationWD - 1) + @decASGOffsetWD), 
                @strCompany
              )
            ELSE 
              dbo.DLTK$AddBusinessDays(
                @dtToOrgStartDate, 
                @decASGOffsetWD, 
                @strCompany
              )
          END,
        @dtToASGEndDate = 
          CASE 
            WHEN @bitFixStartDate = 1
            THEN
              dbo.DLTK$AddBusinessDays(
                @dtToOrgStartDate, 
                ((@decASGDurationWD - 1) + @decASGOffsetWD), 
                @strCompany
              )
            ELSE 
              dbo.DLTK$AddBusinessDays(
                @dtToOrgEndDate, 
                -1 * (@decASGOffsetWD), 
                @strCompany
              )
          END

      UPDATE RPAssignment SET
        StartDate = 
          CASE
            WHEN ISNULL(@strFromStartDate, '') = '' THEN StartDate
            WHEN SumHrs = 0 THEN @dtToASGStartDate
            WHEN MinTPDDate IS NULL THEN @dtToASGStartDate
            ELSE
              CASE
                WHEN @dtToASGStartDate < MinTPDDate
                THEN @dtToASGStartDate
                ELSE MinTPDDate
              END
          END,
        EndDate = 
          CASE
            WHEN ISNULL(@strFromEndDate, '') = '' THEN EndDate
            WHEN SumHrs = 0 THEN @dtToASGEndDate
            WHEN MaxTPDDate IS NULL THEN @dtToASGEndDate
            ELSE
              CASE
                WHEN @dtToASGEndDate > MaxTPDDate
                THEN @dtToASGEndDate
                ELSE MaxTPDDate
              END
          END
        FROM RPAssignment AS A
          LEFT JOIN (
            SELECT
              PlanID, 
              TaskID,
              AssignmentID,
              SUM(PeriodHrs) AS SumHrs,
              MIN(StartDate) AS MinTPDDate,
              MAX(EndDate) AS MaxTPDDate
              FROM RPPlannedLabor AS XTPD
              WHERE PlanID = @strPlanID AND TaskID = @strTaskID AND AssignmentID = @strAssignmentID
              GROUP BY PlanID, TaskID, AssignmentID
          ) AS TPD
            ON A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID
        WHERE A.PlanID = @strPlanID AND A.TaskID = @strTaskID AND A.AssignmentID = @strAssignmentID

    END /* END IF (@bitShiftAll = 1) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     
  SET NOCOUNT OFF

END -- ngRPShiftAssigment
GO
