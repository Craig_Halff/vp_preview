SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpSpreadLabVar]
  @strPlanID varchar(32),
  @dtETCDate datetime,
  @strMatchWBS1Wildcard varchar(1),
  @strUnpostedFlg varchar(1),
  @sintGRMethod smallint,
  @intGenResTableNo int,
  @intGRBillTableNo int,
  @sintCostRtMethod smallint,
  @sintBillingRtMethod smallint,
  @intCostRtTableNo int,
  @intBillingRtTableNo int,
  @strIgnoreVariance varchar(1) = 'N'
AS

BEGIN -- Procedure rpSpreadLabVar

  SET NOCOUNT ON
  
  DECLARE @intHrDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intLabRevDecimals int
  DECLARE @intRtCostDecimals int
  DECLARE @intRtBillDecimals int
  
  DECLARE @dtCalendarMin datetime
  
  DECLARE @strCompany Nvarchar(14)
  
  SELECT @dtCalendarMin = MIN(StartDate) FROM #Calendar

  -- Get decimal settings.
  
  SELECT @intHrDecimals = HrDecimals,
         @intAmtCostDecimals = AmtCostDecimals,
         @intAmtBillDecimals = AmtBillDecimals,
         @intLabRevDecimals = LabRevDecimals,
         @intRtCostDecimals = RtCostDecimals,
         @intRtBillDecimals = RtBillDecimals
    FROM dbo.RP$tabDecimals(@strPlanID)
    
  -- Get Current Company setting.
  
  SELECT @strCompany = dbo.GetActiveCompany()

  -- Get all Assignment rows that have time-phased data before or spanning across ETC Date.
  -- 11/19/2009 LV:
  --   I changed to "Filter Assignment rows only when @strIgnoreVariance != 'Y'".
  --   "Otherwise, get all of the Assignment rows"
  --   @strIgnoreVariance = 'N' was the original criteria for Spread Variance from Optimation.
  
  SELECT DISTINCT A.PlanID, A. TaskID, A.AssignmentID,
    A.WBS1, A.WBS2, A.WBS3, A.LaborCode,
    A.ResourceID, A.Category, A.GRLBCD,
    A.PlannedLaborHrs AS PlannedLabHrs,
    CONVERT(decimal(19,4), 0) AS VARLabHrs,
    dbo.DLTK$NumWorkingDays(@dtETCDate, XTPD.EndDate, @strCompany) AS RemainWD,
    XTPD.StartDate AS MinDate,
    XTPD.EndDate AS MaxDate,
    A.CostRate,
    A.BillingRate    
    INTO #Assignment 
    FROM RPAssignment AS A 
      LEFT JOIN 
        (SELECT TaskID, AssignmentID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate
				   FROM RPPlannedLabor WHERE PlanID = @strPlanID AND AssignmentID IS NOT NULL
			     GROUP BY TaskID, AssignmentID) AS XTPD 
        ON A.TaskID = XTPD.TaskID AND A.AssignmentID = XTPD.AssignmentID
    WHERE A.PlanID = @strPlanID AND 
      ((@strIgnoreVariance != 'Y' AND XTPD.StartDate <= @dtETCDate) OR
       (@strIgnoreVariance = 'Y'))
     
  -- Compute ETC time-phased data for the selected Assignment rows.
  
  SELECT TPD.TaskID AS TaskID, TPD.AssignmentID AS AssignmentID, 
    CASE WHEN TPD.StartDate <= @dtETCDate THEN @dtETCDate ELSE TPD.StartDate END AS StartDate, 
    TPD.EndDate AS EndDate,
    ROUND(CASE WHEN TPD.StartDate <= @dtETCDate AND TPD.EndDate >= @dtETCDate
               THEN PeriodHrs * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)
               ELSE PeriodHrs END, @intHrDecimals) AS PeriodHrs
    INTO #ETCLabTPD
    FROM RPPlannedLabor AS TPD 
      INNER JOIN #Assignment AS A ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID AND TPD.AssignmentID = A.AssignmentID
    WHERE TPD.PlanID = @strPlanID AND TPD.EndDate >= @dtETCDate
        
  -- Compute JTD time-phased data for the selected Assignment rows.
  
  SELECT X.TaskID AS TaskID, X.AssignmentID AS AssignmentID,  
    CI.StartDate AS StartDate,  
    CI.EndDate AS EndDate,  
    ROUND(SUM(ISNULL(X.PeriodHrs, 0)), 2) AS PeriodHrs
    INTO #JTDLabTPD
    FROM #Assignment AS ZA
      INNER JOIN dbo.RP$tabJTDALab(@strPlanID, @dtETCDate, @strUnpostedFlg, @sintGRMethod, @strMatchWBS1Wildcard) AS X  
        ON (ZA.TaskID = X.TaskID AND ZA.AssignmentID = X.AssignmentID)  
      INNER JOIN #Calendar AS CI  
        ON (CASE WHEN (@strIgnoreVariance != 'Y' AND X.TransDate < ZA.MinDate) THEN ZA.MinDate
                 WHEN (@strIgnoreVariance = 'Y' AND X.TransDate < @dtCalendarMin) THEN @dtCalendarMin 
                 ELSE X.TransDate END) BETWEEN CI.StartDate AND CI.EndDate  
    GROUP BY X.AssignmentID, CI.PeriodScale, CI.StartDate, X.TaskID, CI.EndDate, ZA.MinDate, ZA.MaxDate
      
  -- Compute Variance = Planned - JTD.
  
  UPDATE #Assignment 
    SET VARLabHrs = ROUND(ISNULL((PlannedLabHrs - ISNULL(JTDHrs, 0)), 0), @intHrDecimals)
    FROM #Assignment AS A LEFT JOIN
      (SELECT TaskID, AssignmentID,
         ROUND(SUM(ISNULL(PeriodHrs, 0)), 2) AS JTDHrs
         FROM #JTDLabTPD
         GROUP BY TaskID, AssignmentID) AS YA
      ON A.TaskID = YA.TaskID AND A.AssignmentID = YA.AssignmentID
       
  -- Eliminate Assignment rows with Variance less than zero.
  
  DELETE #Assignment WHERE @strIgnoreVariance != 'Y' AND VARLabHrs < 0 
  
  -- Delete Labor time-phased data row for the selected Assignment rows.
  
  DELETE RPPlannedLabor
    FROM RPPlannedLabor AS TPD INNER JOIN #Assignment AS A 
      ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID AND A.AssignmentID = TPD.AssignmentID
      
  -- Recreate Labor time-phased data row for the selected Assignment rows.
  -- Revised Planned Hrs = JTD Hrs + ETC Hrs + Spread Hrs
  -- Spread Hrs = 0 for days before ETC Date.
  
  INSERT INTO RPPlannedLabor(TimePhaseID, PlanID, TaskID, AssignmentID,
    StartDate, EndDate, PeriodHrs, CostRate, BillingRate,
    PeriodCount, PeriodScale, CreateDate, ModDate)
    SELECT
      REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
      PlanID, 
      TaskID, 
      AssignmentID,
      StartDate, 
      EndDate, 
      PeriodHrs, 
      CostRate, 
      BillingRate,
      PeriodCount, 
      PeriodScale, 
      LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate, 
      LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
      FROM
        (SELECT
           A.PlanID AS PlanID, 
           A.TaskID AS TaskID, 
           A.AssignmentID AS AssignmentID,
           CASE WHEN (MinDate BETWEEN CI.StartDate AND CI.EndDate) THEN MinDate ELSE CI.StartDate END AS StartDate,
           CASE WHEN (MaxDate BETWEEN CI.StartDate AND CI.EndDate) THEN MaxDate ELSE CI.EndDate END AS EndDate,
           ROUND((CASE WHEN CI.PeriodScale <> 'o' THEN ISNULL(JTD.PeriodHrs, 0) ELSE 0 END + 
                  (CASE WHEN @strIgnoreVariance = 'Y' THEN ISNULL(ETC.PeriodHrs, 0)
                        ELSE 0 END) +
                  (CASE WHEN @strIgnoreVariance = 'Y' THEN 0
                        WHEN (MaxDate < @dtETCDate AND CI.StartDate <= @dtETCDate AND CI.EndDate >= @dtETCDate AND CI.PeriodScale <> 'o') THEN VARLabHrs
                        WHEN ETC.StartDate IS NULL THEN 0
                        WHEN RemainWD = 0 THEN 0
                        ELSE ((VARLabHrs * dbo.DLTK$NumWorkingDays(ETC.StartDate, ETC.EndDate, @strCompany)) / 
                              RemainWD) END)), @intHrDecimals) AS PeriodHrs,
           dbo.RP$LabRate
             (A.ResourceID,
              A.Category,
              A.GRLBCD,
              A.LaborCode,
              @sintCostRtMethod,
              @intCostRtTableNo,
              @sintGRMethod,
              @intGenResTableNo,
              CASE WHEN (MinDate BETWEEN CI.StartDate AND CI.EndDate) THEN MinDate ELSE CI.StartDate END,
              CASE WHEN (MaxDate BETWEEN CI.StartDate AND CI.EndDate) THEN MaxDate ELSE CI.EndDate END,
              @intRtCostDecimals,
              EM.ProvCostRate,
              A.CostRate) AS CostRate,
           dbo.RP$LabRate
             (A.ResourceID,
              A.Category,
              A.GRLBCD,
              A.LaborCode,
              @sintBillingRtMethod,
              @intBillingRtTableNo,
              @sintGRMethod,
              @intGRBillTableNo,
              CASE WHEN (MinDate BETWEEN CI.StartDate AND CI.EndDate) THEN MinDate ELSE CI.StartDate END,
              CASE WHEN (MaxDate BETWEEN CI.StartDate AND CI.EndDate) THEN MaxDate ELSE CI.EndDate END,
              @intRtBillDecimals,
              EM.ProvBillRate,
              A.BillingRate) AS BillingRate,
           1 AS PeriodCount, 
           CI.PeriodScale AS PeriodScale
           FROM #Calendar AS CI LEFT JOIN #Assignment AS A ON 1 = 1
             LEFT JOIN EM ON A.ResourceID = EM.Employee
             LEFT JOIN #ETCLabTPD AS ETC ON (ETC.StartDate BETWEEN CI.StartDate AND CI.EndDate
               AND A.TaskID = ETC.TaskID AND A.AssignmentID = ETC.AssignmentID)
             LEFT JOIN #JTDLabTPD AS JTD ON (JTD.StartDate BETWEEN CI.StartDate AND CI.EndDate
               AND A.TaskID = JTD.TaskID AND A.AssignmentID = JTD.AssignmentID)
           WHERE (@strIgnoreVariance != 'Y' AND
                  (ETC.TaskID IS NOT NULL OR 
                   JTD.TaskID IS NOT NULL OR 
                   (MaxDate < @dtETCDate AND CI.StartDate <= @dtETCDate AND CI.EndDate >= @dtETCDate)))
             OR (@strIgnoreVariance = 'Y')
        ) AS Z
      WHERE Z.PeriodHrs > 0        
     
  -- Adjust Planned Labor time-phased data to compensate for rounding errors.
 
  IF (@strIgnoreVariance != 'Y')
    UPDATE RPPlannedLabor SET PeriodHrs = ROUND((TPD.PeriodHrs + DH.DeltaHrs), @intHrDecimals)
      FROM RPPlannedLabor AS TPD 
        INNER JOIN #Assignment AS A ON TPD.EndDate = A.MaxDate
          AND TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID AND TPD.AssignmentID = A.AssignmentID
        INNER JOIN
          (SELECT XA.PlanID, XA.TaskID, XA.AssignmentID, -- Compute Delta Hours
            (XA.PlannedLabHrs - SUM(ISNULL(XTPD.PeriodHrs, 0))) AS DeltaHrs
            FROM RPPlannedLabor AS XTPD INNER JOIN #Assignment AS XA 
              ON XTPD.PlanID = XA.PlanID AND XTPD.TaskID = XA.TaskID AND XTPD.AssignmentID = XA.AssignmentID
            GROUP BY XA.PlanID, XA.TaskID, XA.AssignmentID, XA.PlannedLabHrs) AS DH
          ON TPD.PlanID = DH.PlanID AND TPD.TaskID = DH.TaskID AND TPD.AssignmentID = DH.AssignmentID
          
  -- Update Assignment Start/End Date, if necessary.
  
  UPDATE RPAssignment SET StartDate = TPD.StartDate, EndDate = TPD.EndDate
    FROM RPAssignment AS A
      INNER JOIN
        (SELECT XA.PlanID, XA.TaskID AS TaskID, XA.AssignmentID AS AssignmentID,
          MIN(XTPD.StartDate) AS StartDate, MAX(XTPD.EndDate) AS EndDate
          FROM RPPlannedLabor AS XTPD INNER JOIN #Assignment AS XA 
            ON XTPD.PlanID = XA.PlanID AND XTPD.TaskID = XA.TaskID AND XTPD.AssignmentID = XA.AssignmentID
          GROUP BY XA.PlanID, XA.TaskID, XA.AssignmentID) AS TPD
        ON A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID
   
  SET NOCOUNT OFF

END -- rpSpreadLabVar
GO
