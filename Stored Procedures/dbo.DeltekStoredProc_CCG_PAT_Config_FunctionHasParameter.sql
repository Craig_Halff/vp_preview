SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_FunctionHasParameter]
	@function		nvarchar(100),
	@parameterName	nvarchar(100)
AS
BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_PAT_Config_FunctionHasParameter] 'fnCCG_PAT_GetGroups', 'Context'
	-- EXEC [dbo].[DeltekStoredProc_CCG_PAT_Config_FunctionHasParameter] 'fnCCG_PAT_GetGroups', 'Language'
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

	SELECT CASE WHEN COUNT('x') = 1 THEN 'yes' ELSE 'no' END As HasFields
		FROM sys.objects b, sys.parameters a
		WHERE b.object_id = OBJECT_ID(N'[dbo].[' + @function + ']') AND type in (N'TF', N'FN')
			AND a.object_id = b.object_id
			AND a.name = N'@' + @parameterName
END;
GO
