SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_LoadCompany] ( @requiredCompany nvarchar(16)= N' ')
             AS EXEC spCCG_PAT_LoadCompany @requiredCompany
GO
