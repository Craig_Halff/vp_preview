SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_CheckIfEmployeeIsSupervisor] (@Employee Nvarchar(20))
AS
BEGIN
	/* Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved. */
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	if exists(select top 1 'x' from EM where EM.Supervisor = @Employee AND EM.Status = 'A')
		select 1
	else
		select 0
END
GO
