SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Main_SetApprovedDetail]
	@approvedDetail		Nvarchar(max),
	@seq				int
AS
BEGIN
	SET NOCOUNT ON
	UPDATE CCG_PAT_Payable SET ApprovedDetail = @approvedDetail
		WHERE Seq = @seq and ISNULL(ApprovedDetail, N'') = N''
END;

GO
