SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_InsertLanguageEntry] ( @Group1 nvarchar(255), @Id nvarchar(255), @Product varchar(255), @UICultureName varchar(20), @Value nvarchar(255))
             AS EXEC spCCG_PAT_InsertLanguageEntry @Group1,@Id,@Product,@UICultureName,@Value
GO
