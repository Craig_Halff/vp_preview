SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Conrad Harrison
-- Create date: 2018-07-10
-- Modified on: 2018-11-13 by Conrad Harrison
-- Description:	Stored Procedure that evaluates for Ops_Manager, Director, or Team Leader and sends appropriate Email
-- =============================================
CREATE PROCEDURE [dbo].[HAI_spCCG_UtilizationNotification_Ops_Dir_TL_PracLead_COMBINED] 
	-- Add the parameters for the stored procedure here
	--**COMMENTED OUT Evaluation Email BELOW
	--@EvaluationEmail VARCHAR(100)

AS

SET NOCOUNT ON;

DECLARE

--PASSED IN EVALUATION EMAIL
@EvaluationEmail							VARCHAR(100),
@EmployeeEmail								VARCHAR(100),
@EmployeeNumber								VARCHAR(100),
@CombinedBody								varchar (max),
@CombinedCSS            					varchar (max),
@HalffEmailPass								VARCHAR(100),
@OpsMgr_Dir_TL_SP							varchar (max),
--@DirectorSP								varchar (max),
--@TeamLeaderSP								varchar (max),
@PracticeLeaderEmpNum						varchar(100),
@PracticeArea								varchar(100),
@PracticeLeaderSP							varchar (max),
@StoredProcedureToRun						varchar (max),
@EmployeeRole								varchar (max),
@OpsMgrHeader								VARCHAR(100),
@DirectorHeader								VARCHAR(100),
@TeamLeaderHeader							VARCHAR(100),
@PracticeLeaderHeader						VARCHAR(100),
@RowCount									integer,
--**ADDING IN LOOP
@Loop1										integer,

--**TESTING EMAILS
@EmailSentTime								Datetime,
@TestEmail1									VARCHAR(100),
@TestEmail2									VARCHAR(100),
@TestEmail3									VARCHAR(100),
@TestEmail4									VARCHAR(100),
@TestEmail5									VARCHAR(100),
@TestEmail6									VARCHAR(100),
@TestEmail7									VARCHAR(100),
@TestEmail8									VARCHAR(100),
@TestEmailDestination						VARCHAR(100)

Set @HalffEmailPass	= 'halff.com'
Set @OpsMgrHeader = 'Operations Manager Utilization'
Set @DirectorHeader = 'Director Utilization'
Set @TeamLeaderHeader = 'Team Leader Utilization'
Set @PracticeLeaderHeader = 'Practice Leader Utilization'

--**--**FOR EMAIL PROXY TESTING
SET @TestEmail1	= 'jsagel@halff.com'							
SET @TestEmail2	= 'jsagel@halff.com'								
SET @TestEmail3	= 'jsagel@halff.com'					
SET @TestEmail4	= 'jsagel@halff.com'				
SET @TestEmail5	= 'jsagel@halff.com'							
SET @TestEmail6	= 'jsagel@halff.com'						
SET @TestEmail7	= 'jsagel@halff.com'		
SET @TestEmail8	= 'jsagel@halff.com'		

--SET @TestEmail1	= 'charrison@halff.com'							
--SET @TestEmail2	= 'charrison@halff.com'									
--SET @TestEmail3	= 'charrison@halff.com'							
--SET @TestEmail4	= 'charrison@halff.com'						
--SET @TestEmail5	= 'charrison@halff.com'									
--SET @TestEmail6	= 'charrison@halff.com'								
--SET @TestEmail7	= 'charrison@halff.com'				
--SET @TestEmail8	= 'charrison@halff.com'		

SET @RowCount = 0

DECLARE All_Employees insensitive cursor for

SELECT DISTINCT
A.Employee, B.Email FROM
(
	SELECT Employee
	FROM [HalffImport].[dbo].[EMCompany]
	WHERE Status = 'A'
) AS A
INNER JOIN
(
	SELECT Employee, Email
	FROM
	[HalffImport].[dbo].[EMMain]
) AS B
ON B.Employee=A.Employee

OPEN All_Employees
Fetch Next From All_Employees into @EmployeeNumber, @EmployeeEmail
While @@Fetch_Status = 0
Begin

Set @EmployeeRole = (
SELECT TOP 1 
[Role]
FROM

(
	SELECT  ect.[Employee]
	,em.Email
	,ect.[CustEKOperationsManager]
	,'OPS_MANAGER' AS [Role]
	,1 AS [order]
	FROM [HalffImport].[dbo].[EmployeeCustomTabFields] ect, [HalffImport].[dbo].[EMMain] em
	Where [CustEKOperationsManager] = ect.[Employee]
	and em.Employee = ect.[Employee]

	Union All

	SELECT  ect.[Employee]
	,em.Email
	,[CustEKDirector]
	,'DIRECTOR' AS [Role]
	,2 AS [order]
	FROM [HalffImport].[dbo].[EmployeeCustomTabFields] ect, [HalffImport].[dbo].[EMMain] em
	Where [CustEKDirector] = ect.[Employee]
	and em.Employee = ect.[Employee]

	Union All 

	SELECT  ect.[Employee]
	,em.Email
	,[CustEKApprover]
	,'TEAM_LEADER' AS [Role]
	,3 AS [order]
	FROM [HalffImport].[dbo].[EmployeeCustomTabFields] ect, [HalffImport].[dbo].[EMMain] em
	Where [CustEKApprover] = ect.[Employee]
	and em.Employee = ect.[Employee]

) AS A
WHERE A.[Employee] = @EmployeeNumber 

ORDER BY A.[order]
)

-- CSS style rules
	SET @CombinedCSS = 
'
<style type="text/css">
	--p {padding-top:15px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;}
	th {border: 2px solid black;padding: 5px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;font-size:10.5pt;}
	th {background-color: #808080;color: white;text-align:center;}
	.team {text-align:left; border-left: 2px solid Black; background-color: white; border-bottom: 1px solid #6E6E6E;}
	.teamB {font-weight: bold; text-align:left; border-left: 2px solid Black; background-color: white; border-bottom: 1px solid #6E6E6E;}
	.LW {color: black; background-color: white; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
	.LWb {font-weight: bold; color: black; background-color: white; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
	.L4W {color: black; background-color: #e6e6e6; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;} 
	.L4Wb {font-weight: bold; color: black; background-color: #e6e6e6; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;} 
	.L12W {background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
	.L12Wb {font-weight: bold; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
	.L12Wr {color: black; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-right: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
	.L12Wrb {font-weight: bold; color: black; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-right: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
	.LWF {color: black; font-weight: bold; background-color: white; text-align:right; border-left: 2px solid Black; border-top: 2px solid Black;}
	.LWFLeft {font-weight: bold; background-color: white; text-align:left; border-left: 2px solid Black; border-top: 2px solid Black;}
	.L4WF {color: black; font-weight: bold; background-color: #e6e6e6; text-align:right; border-left: 2px solid Black; border-top: 2px solid Black;} 
	.L12WF {font-weight: bold; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-top: 2px solid Black;}
	.L12WrF {color: black; font-weight: bold; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-right: 2px solid Black; border-top: 2px solid Black;}
	.redText {color:#FF0000;}
	</style>
'

Select @PracticeArea = epa.CustPracticeAreaLeader,
@PracticeLeaderEmpNum = epa.Employee 
FROM [HalffImport].[dbo].[Employees_PracticeAreas] epa
Where epa.Employee = @EmployeeNumber	

--**TESTING WITH PROXY EMAILS AND PRINT STATEMENTS BELOW
		--IF @EmployeeRole = 'OPS_MANAGER' OR @EmployeeRole = 'DIRECTOR' OR @EmployeeRole = 'TEAM_LEADER' and @EmployeeNumber <> '001'
		--BEGIN
		--SET @RowCount = @RowCount + 1
		--Print 'Row: ' + CAST(@RowCount AS VARCHAR(MAX)) + ' EmpNum: ' + CAST(@EmployeeNumber AS VARCHAR(MAX)) + ' Email: ' + CAST(@EmployeeEmail AS VARCHAR(MAX)) + ' PracLeader_EmpNum: ' + CAST(@PracticeLeaderEmpNum AS VARCHAR(MAX))
		--END
		--IF @EmployeeRole is null and @EmployeeNumber = @PracticeLeaderEmpNum
		--BEGIN
		--SET @RowCount = @RowCount + 1
		--Print 'Row: ' + CAST(@RowCount AS VARCHAR(MAX)) + ' EmpNum: ' + CAST(@EmployeeNumber AS VARCHAR(MAX)) + ' Email: ' + CAST(@EmployeeEmail AS VARCHAR(MAX)) + ' PracLeader_EmpNum: ' + CAST(@PracticeLeaderEmpNum AS VARCHAR(MAX))
		--END

    -- Insert statements for procedures here
	
	IF @EmployeeRole = 'OPS_MANAGER'
	Begin
		Execute [dbo].[HAI_spCCG_UtilizationNotification_Ops_Manager] @EmployeeEmail, @OpsMgr_Dir_TL_SP Output
	END

	IF @EmployeeRole = 'DIRECTOR'
	Begin
		Execute [dbo].[HAI_spCCG_UtilizationNotification_Director] @EmployeeEmail, @OpsMgr_Dir_TL_SP Output
	END

	IF @EmployeeRole = 'TEAM_LEADER' and @EmployeeNumber <> '001'
	BEGIN
		Execute [dbo].[HAI_spCCG_UtilizationNotification_TeamLeader] @EmployeeEmail, @OpsMgr_Dir_TL_SP Output
	END

	IF @EmployeeNumber = @PracticeLeaderEmpNum 
	BEGIN
		Execute [dbo].[HAI_spCCG_UtilizationNotification_Practice_Leader] @EmployeeEmail, @PracticeLeaderSP Output
	END	

SET @CombinedBody = 

	CASE 
		WHEN @EmployeeRole = 'OPS_MANAGER' 
							and @EmployeeNumber <> @PracticeLeaderEmpNum
			THEN @OpsMgrHeader + @OpsMgr_Dir_TL_SP
		WHEN @EmployeeRole = 'OPS_MANAGER' 
							and @EmployeeNumber = @PracticeLeaderEmpNum
			THEN @OpsMgrHeader + @OpsMgr_Dir_TL_SP + @PracticeLeaderHeader + @PracticeLeaderSP 
		WHEN @EmployeeRole = 'DIRECTOR' 
							and @EmployeeNumber <> @PracticeLeaderEmpNum
			THEN @DirectorHeader + @OpsMgr_Dir_TL_SP 
		WHEN @EmployeeRole = 'DIRECTOR' 
							and @EmployeeNumber = @PracticeLeaderEmpNum
			THEN @DirectorHeader + @OpsMgr_Dir_TL_SP + @PracticeLeaderHeader + @PracticeLeaderSP
		WHEN @EmployeeRole = 'TEAM_LEADER' 
							and @EmployeeNumber <> @PracticeLeaderEmpNum
			THEN @TeamLeaderHeader + @OpsMgr_Dir_TL_SP
		WHEN @EmployeeRole = 'TEAM_LEADER' 
							and @EmployeeNumber = @PracticeLeaderEmpNum
			THEN @TeamLeaderHeader + @OpsMgr_Dir_TL_SP + @PracticeLeaderHeader + @PracticeLeaderSP	

		WHEN @EmployeeRole is null and @EmployeeNumber = @PracticeLeaderEmpNum
			THEN @PracticeLeaderHeader + @PracticeLeaderSP			 
		END

SET @EmailSentTime = GETDATE() 

	--** TEST EMAIL CASE LOGIC
	Set @TestEmailDestination =
		CASE WHEN @RowCount = 1 THEN @TestEmail1
			 WHEN @RowCount = 2 THEN @TestEmail2
			 WHEN @RowCount = 3 THEN @TestEmail3
			 WHEN @RowCount = 4 THEN @TestEmail4
			 WHEN @RowCount = 5 THEN @TestEmail5
			 WHEN @RowCount = 6 THEN @TestEmail6
			 WHEN @RowCount = 7 THEN @TestEmail7
			 WHEN @RowCount = 8 THEN @TestEmail8
			 Else 'jsagel@halff.com'
			 --Else 'charrison@halff.com'
		End

	--**ADJUST EMAIL RECIPIENT BELOW, CURRENTLY SET TO THE PROXY RECIPIENTS

	 IF @EmployeeRole = 'OPS_MANAGER' OR @EmployeeRole = 'DIRECTOR' OR @EmployeeRole = 'TEAM_LEADER' and @EmployeeNumber <> '001'
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Utilization Summary', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Utilization Summary For: ' + CAST(@EmployeeEmail as Varchar(100)) + ' Role: ' + CAST(@EmployeeRole as VARCHAR(100)) + ' On: ' + CAST(@EmailSentTime as Varchar(100)), @CombinedCSS + @CombinedBody, getDate(), 1)
		END
		--ADDED AND @EmployeeNumber <> '003374' below to account for Ryan Smith (003374) not having any teams under his Practice Lead, a special case
	If @EmployeeRole is Null AND @EmployeeNumber = @PracticeLeaderEmpNum 
	--AND @EmployeeNumber <> '003374'	
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Utilization Summary', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Utilization Summary For: ' + CAST(@EmployeeEmail as Varchar(100)) + ' Role: NONE  On: ' + CAST(@EmailSentTime as Varchar(100)), @CombinedCSS + @CombinedBody, getDate(), 1)
		END

	--THE ACTUAL RECIPIENTS BELOW

	-- IF @EmployeeRole = 'OPS_MANAGER' OR @EmployeeRole = 'DIRECTOR' OR @EmployeeRole = 'TEAM_LEADER' and @EmployeeNumber <> '001'
	--	BEGIN
	--		Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
	--		Values ('Utilization Summary', @EmployeeEmail, @EmployeeEmail, @EmployeeEmail, 'Utilization Summary For: ' + CAST(@EmployeeEmail as Varchar(100)) + ' Role: ' + CAST(@EmployeeRole as VARCHAR(100)) + ' On: ' + CAST(@EmailSentTime as Varchar(100)), @CombinedCSS + @CombinedBody, getDate(), 1)
	--	END

	--If @EmployeeRole is Null AND @EmployeeNumber = @PracticeLeaderEmpNum
	--	BEGIN
	--		Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
	--		Values ('Utilization Summary', @EmployeeEmail, @EmployeeEmail, @EmployeeEmail, 'Utilization Summary For: ' + CAST(@EmployeeEmail as Varchar(100)) + ' Role: NONE  On: ' + CAST(@EmailSentTime as Varchar(100)), @CombinedCSS + @CombinedBody, getDate(), 1)
	--	END

FETCH Next From All_Employees into @EmployeeNumber, @EmployeeEmail
END

Close All_Employees
Deallocate All_Employees




GO
