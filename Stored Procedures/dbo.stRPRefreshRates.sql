SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPRefreshRates]
  @strPlanID varchar(32),
  @strRefreshLabCostRates varchar(1) = 'Y',
  @strRefreshLabBillRates varchar(1) = 'Y',
  @strRefreshExpBillRates varchar(1) = 'Y',
  @strRefreshConBillRates varchar(1) = 'Y',
  @bitRefreshRPSide bit = 0

AS

BEGIN -- Procedure stRPRefreshRates

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to refresh rates in a Navigator Plan.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON

  DECLARE @siAmtDecimals smallint
  DECLARE @bitCalculateConBilling bit = 0
  DECLARE @bitCalculateExpBilling bit = 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Decimals set basend on the logic hidden inside the tabDecimals function

  SELECT 
    @siAmtDecimals = AmtBillDecimals
    FROM dbo.stRP$tabPlanDecimals(@strPlanID)

  -- Get the flags for Exp and Con to see if we need to recalculate the Billing amount

  SELECT 
    @bitCalculateExpBilling = 
      CASE 
        WHEN 
          P.BillingCurrencyCode = P.CostCurrencyCode
            AND ((P.ExpBillRtMethod > 0 AND P.BudgetType = 'B')
            OR P.BudgetType = 'A')
        THEN 1
        ELSE 0 
      END,
    @bitCalculateConBilling = 
      CASE 
        WHEN 
          P.BillingCurrencyCode = P.CostCurrencyCode
            AND ((P.ConBillRtMethod > 0 AND P.BudgetType = 'B')
            OR P.BudgetType = 'A')
        THEN 1
        ELSE 0 
      END 
    FROM PNPlan AS P 
    WHERE P.PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Update override rates for Assignment rows.
  
  BEGIN TRANSACTION

  UPDATE PNAssignment SET
    CostRate = CASE WHEN @strRefreshLabCostRates = 'Y'
      THEN 0
      ELSE CostRate END,
    BillingRate = CASE WHEN @strRefreshLabBillRates = 'Y'
      THEN 0
      ELSE BillingRate END,
    ModDate = GETUTCDATE()
    WHERE PlanID = @strPlanID
  
  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  UPDATE PNPlannedLabor SET
    CostRate =  CASE WHEN @strRefreshLabCostRates = 'Y'
      THEN 0
      ELSE PNPlannedLabor.CostRate END,
    BillingRate =  CASE WHEN @strRefreshLabBillRates = 'Y'
      THEN 0
      ELSE PNPlannedLabor.BillingRate END,
    ModDate = GETUTCDATE()
    FROM PNPlannedLabor
      INNER JOIN PNAssignment AS A ON PNPlannedLabor.PlanID = A.PlanID AND PNPlannedLabor.TaskID = A.TaskID AND PNPlannedLabor.AssignmentID = A.AssignmentID
      LEFT JOIN EM ON A.ResourceID = EM.Employee
    WHERE PNPlannedLabor.PlanID = @strPlanID

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  IF (@strRefreshExpBillRates = 'Y' AND @bitCalculateExpBilling = 1)
    BEGIN

      -- Update Expense Bill Amounts.

      UPDATE PNPlannedExpenses SET
        PeriodBill = ROUND(ISNULL(PeriodCost * dbo.PN$ExpRate(@strPlanID, A.Account), 0.0000), @siAmtDecimals)
        FROM PNExpense A
          INNER JOIN PNPlannedExpenses TPD ON TPD.ExpenseID = A.ExpenseID AND TPD.PlanID = A.PlanID
        WHERE TPD.PlanID = @strPlanID

    END

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  IF (@strRefreshConBillRates = 'Y' AND @bitCalculateConBilling = 1)
    BEGIN

      -- Update Consultant Bill Amounts.

      UPDATE PNPlannedConsultant SET
        PeriodBill = ROUND(ISNULL(PeriodCost * dbo.PN$ConRate(@strPlanID, A.Account), 0.0000), @siAmtDecimals)
        FROM PNConsultant A
          INNER JOIN PNPlannedConsultant TPD ON TPD.ConsultantID = A.ConsultantID AND TPD.PlanID = A.PlanID
        WHERE TPD.PlanID = @strPlanID

    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
  IF (@bitRefreshRPSide = 1)
    BEGIN

      UPDATE RPAssignment SET
        CostRate = CASE WHEN @strRefreshLabCostRates = 'Y'
          THEN 0
          ELSE CostRate END,
        BillingRate = CASE WHEN @strRefreshLabBillRates = 'Y'
          THEN 0
          ELSE BillingRate END,
        ModDate = GETUTCDATE()
        WHERE PlanID = @strPlanID
  
     --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      UPDATE RPPlannedLabor SET
        CostRate =  CASE WHEN @strRefreshLabCostRates = 'Y'
          THEN 0
          ELSE RPPlannedLabor.CostRate END,
        BillingRate =  CASE WHEN @strRefreshLabBillRates = 'Y'
          THEN 0
          ELSE RPPlannedLabor.BillingRate END,
        ModDate = GETUTCDATE()
        FROM RPPlannedLabor
          INNER JOIN RPAssignment AS A ON RPPlannedLabor.PlanID = A.PlanID AND RPPlannedLabor.TaskID = A.TaskID AND RPPlannedLabor.AssignmentID = A.AssignmentID
          LEFT JOIN EM ON A.ResourceID = EM.Employee
        WHERE RPPlannedLabor.PlanID = @strPlanID

     --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      IF (@strRefreshExpBillRates = 'Y' AND @bitCalculateExpBilling = 1)
        BEGIN

          -- Update Expense Bill Amounts.

          UPDATE RPPlannedExpenses SET
            PeriodBill = ROUND(ISNULL(PeriodCost * dbo.RP$ExpRate(@strPlanID,A.Account), 0.0000), @siAmtDecimals)
            FROM RPExpense A
              INNER JOIN RPPlannedExpenses TPD ON TPD.ExpenseID = A.ExpenseID AND TPD.PlanID = A.PlanID
            WHERE TPD.PlanID = @strPlanID

        END

     --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      IF (@strRefreshConBillRates = 'Y' AND @bitCalculateConBilling = 1)
        BEGIN

          -- Update Consultant Bill Amounts.

          UPDATE RPPlannedConsultant SET
            PeriodBill = ROUND(ISNULL(PeriodCost * dbo.RP$ConRate(@strPlanID, A.Account), 0.0000), @siAmtDecimals)
            FROM RPConsultant A
              INNER JOIN RPPlannedConsultant TPD ON TPD.ConsultantID = A.ConsultantID AND TPD.PlanID = A.PlanID
            WHERE TPD.PlanID = @strPlanID

        END

    END /* IF (@bitRefeshRPSide = 1) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  COMMIT TRANSACTION

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF

END -- stRPRefreshRates
GO
