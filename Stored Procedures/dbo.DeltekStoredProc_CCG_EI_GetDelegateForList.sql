SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_GetDelegateForList] ( @Employee nvarchar(20), @IncludeSelf int= 1)
             AS EXEC spCCG_EI_GetDelegateForList @Employee,@IncludeSelf
GO
