SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_UpdateCustFeeTypeRollup]
AS
/* 
	Copyright (c) 2021 Halff Associates, Inc.  All rights reserved.
	Jackson Gray

	20210529	Craig H. Anderson
				Added query hints to not lock on read tables.
*/
    BEGIN
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
        SET NOCOUNT ON;

        DECLARE @ProjectTable TABLE (
            WBS1 NVARCHAR(30)
        );

        DECLARE @p0 TABLE (
            WBS1 NVARCHAR(30)
          , WBS2 NVARCHAR(30)
          , WBS3 NVARCHAR(30)
          , CustFeeType NVARCHAR(100)
          , TotalCompensation DECIMAL
        );

        INSERT INTO @ProjectTable
            SELECT A.WBS1
                FROM dbo.PR                           A WITH (NOLOCK)
                INNER JOIN dbo.ProjectCustomTabFields B WITH (NOLOCK)
                    ON B.WBS1 = A.WBS1
                        AND B.WBS2 = A.WBS2
                        AND B.WBS3 = A.WBS3
                WHERE
                A.WBS2 = ''
                    AND A.WBS3 = ''
                    AND A.ChargeType = 'R'
                    AND A.Status = 'A';

        INSERT INTO @p0
            SELECT A.WBS1
                 , A.WBS2
                 , A.WBS3
                 , COALESCE(A.CustLumpSumPhase, A.CustFeeType) AS CustFeeType
                 , A.TotalCompensation
                FROM (
                    SELECT t1.WBS1
                         , t1.WBS2
                         , t1.WBS3
                         , CASE (t2.CustFeeType)
                               WHEN 'Cost Plus'
                                   THEN
                                   'CP'
                               WHEN 'Cost Plus Fixed Fee'
                                   THEN
                                   'CPFF'
                               WHEN 'Cost Plus Max'
                                   THEN
                                   'CPM'
                               WHEN 'Lump Sum'
                                   THEN
                                   'LS'
                               WHEN 'Non-Billable'
                                   THEN
                                   'NB'
                               WHEN 'Percent of Construction'
                                   THEN
                                   'PC'
                               WHEN 'Sublevel Terms'
                                   THEN
                                   NULL
                           END                                                                                                                                         AS CustFeeType
                         , CASE (t2.CustLumpSumPhase) WHEN 'Y' THEN 'LS' WHEN 'N' THEN NULL END                                                                        AS CustLumpSumPhase
                         , (ISNULL(t1.FeeDirLab, 0) + ISNULL(t1.FeeDirExp, 0) + ISNULL(t1.ConsultFee, 0) + ISNULL(t1.ReimbAllowCons, 0) + ISNULL(t1.ReimbAllowExp, 0)) AS TotalCompensation
                        --,ISNULL(Fee, 0) + ISNULL(ReimbAllow, 0) + ISNULL(ConsultFee, 0) AS TotalCompensation
                        FROM dbo.PR                           t1 WITH (NOLOCK)
                        INNER JOIN dbo.ProjectCustomTabFields t2 WITH (NOLOCK)
                            ON t2.WBS1 = t1.WBS1
                                AND t2.WBS2 = t1.WBS2
                                AND t2.WBS3 = t1.WBS3
                        WHERE
                        t1.WBS1 IN (
                            SELECT WBS1 FROM @ProjectTable
                        )
                ) A
                ORDER BY
                A.WBS1
              , A.WBS2
              , A.WBS3;

        UPDATE dbo.ProjectCustomTabFields
            SET CustFeeTypeRollup = tempTable.CustFeeType
            FROM (
                SELECT DISTINCT
                       beta.WBS1
                     , ''   AS WBS2
                     , ''   AS WBS3
                     , STUFF((
                                 SELECT ',' + A.CustFeeType AS [text()]
                                     FROM (
                                         SELECT WBS1
                                              , CustFeeType
                                              , SUM(TotalCompensation) AS TotalCompensation
                                             FROM @p0
                                             GROUP BY
                                             WBS1
                                           , CustFeeType
                                     ) A
                                     WHERE A.WBS1 = beta.WBS1
                                     ORDER BY A.TotalCompensation DESC
                                 FOR XML PATH('')
                             )
                           , 1
                           , 1
                           , ''
                       )    AS CustFeeType
                     , NULL AS TotalCompensation
                    FROM @p0 beta
                    WHERE
                    WBS2 <> ''
                        AND WBS3 = ''
                UNION
                SELECT A.WBS1
                     , A.WBS2
                     , A.WBS3
                     , COALESCE(B.CustFeeType, A.CustFeeType) AS CustFeeType
                     , B.TotalCompensation
                    FROM (
                        SELECT DISTINCT
                               beta.WBS1
                             , beta.WBS2
                             , '' AS WBS3
                             , STUFF((
                                         SELECT ',' + A.CustFeeType AS [text()]
                                             FROM (
                                                 SELECT WBS1
                                                      , WBS2
                                                      , CustFeeType
                                                      , SUM(TotalCompensation) AS TotalCompensation
                                                     FROM @p0
                                                     WHERE
                                                     WBS2 <> ''
                                                         AND WBS3 <> ''
                                                     GROUP BY
                                                     WBS1
                                                   , WBS2
                                                   , CustFeeType
                                             ) A
                                             WHERE
                                             A.WBS1 = beta.WBS1
                                                 AND A.WBS2 = beta.WBS2
                                             ORDER BY A.TotalCompensation DESC
                                         FOR XML PATH('')
                                     )
                                   , 1
                                   , 1
                                   , ''
                               )  AS CustFeeType
                            FROM @p0 beta
                            WHERE beta.WBS2 <> ''
                    )                                                                                                     A
                    LEFT JOIN (SELECT WBS1, WBS2, CustFeeType, TotalCompensation FROM @p0 WHERE WBS2 <> '' AND WBS3 = '') B
                        ON A.WBS1 = B.WBS1
                            AND A.WBS2 = B.WBS2
                UNION
                SELECT WBS1
                     , WBS2
                     , WBS3
                     , CustFeeType
                     , TotalCompensation
                    FROM @p0
                    WHERE
                    WBS2 <> ''
                        AND WBS3 <> ''
            ) tempTable
            WHERE
            ProjectCustomTabFields.WBS1 = tempTable.WBS1
                AND ProjectCustomTabFields.WBS2 = tempTable.WBS2
    END;
GO
