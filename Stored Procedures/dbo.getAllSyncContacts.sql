SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[getAllSyncContacts]
  @strEmployee VARCHAR(32)
AS

BEGIN -- Procedure getAllSyncContacts

declare @VisioSyncCategory varchar(100)
declare @ContactID varchar(32)
declare @count int 


set @VisioSyncCategory =  'DLTKSYNC'

-- check if the vision sync category is already in db or not
Select @count = count (*)   from  CFGContactInfoCat where code = @VisioSyncCategory

if @count = 0
begin
--Insert a new contact category 'Vision Contact CateGory' into the table 
 Insert into CFGContactInfoCat (code, category, Seq) 
     Select @VisioSyncCategory as code, @VisioSyncCategory as category, isnull(max (seq),0) + 1 as Seq  
     from CFGContactInfoCat
end

-- Get all contacts in RPAssignment with ResourceID not null

   DECLARE ContactInfo INSENSITIVE CURSOR FOR
	select Contacts.ContactID from contacts, emcontactAssoc 
		where emcontactAssoc.employee = @strEmployee
		and emcontactAssoc.contactID = contacts.contactID  
		and contacts.Type = 'C' 
		Order by emcontactAssoc.CreateDate

   BEGIN
     OPEN ContactInfo
     FETCH NEXT from ContactInfo INTO @ContactID 
     WHILE @@FETCH_STATUS = 0
     BEGIN
		Select @count = count (*)   from  ContactMoreInfo where category = @VisioSyncCategory and contactID = @ContactID
		
		if @count = 0 
			begin
				Insert into ContactMoreInfo (contactId, category, CreateDate, ModDate) 
					   values (@ContactID, 
							   @VisioSyncCategory, 
								CONVERT(VARCHAR, GETUTCDATE(), 121), 
								CONVERT(VARCHAR, GETUTCDATE(), 121) )		 
			end
		else
			begin
				Update ContactMoreInfo set ModDate = CONVERT(VARCHAR, GETUTCDATE(), 121) 
				where  ContactID = @ContactID and category = @VisioSyncCategory
			end
	    FETCH NEXT from ContactInfo INTO @ContactID 
     END   
     CLOSE ContactInfo
     DEALLOCATE ContactInfo
   end
  
END -- getAllSyncContacts
GO
