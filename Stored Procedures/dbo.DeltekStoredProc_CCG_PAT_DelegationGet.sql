SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_DelegationGet] ( @Username nvarchar(32), @BeginDate datetime, @EndDate datetime, @UserEmployeeId nvarchar(20), @IsSupervisorApprovalOnly bit= 0, @ShowAllDelegations bit= 1, @CanSelfDelegate bit= 1)
             AS EXEC spCCG_PAT_DelegationGet @Username,@BeginDate,@EndDate,@UserEmployeeId,@IsSupervisorApprovalOnly,@ShowAllDelegations,@CanSelfDelegate
GO
