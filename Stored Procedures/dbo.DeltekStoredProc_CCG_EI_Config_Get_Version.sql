SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Get_Version] ( @softwareVersion varchar(20)= null)
             AS EXEC spCCG_EI_Config_Get_Version @softwareVersion
GO
