SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE PROCEDURE [dbo].[LoadTimesheet] @activeCompany Nvarchar(14), @Employee Nvarchar(20), @EndDate datetime, @StartDate datetime, @ForReport varchar(1) = 'Y', @PrintByProject varchar(1) = 'N'
AS
DECLARE
	@ExistInTKDetail int,
	@ExistInTKMaster int,
	@EmployeeTKGroup Nvarchar(10),
	@MasterTKGroup Nvarchar(10),
	@tkMsterTKGroup Nvarchar(10),
	@TKGroupHasCategory int,
	@UseApprovalWorkflow varchar(1),
	@tkMasterStatus varchar(1)

DECLARE
	@LC1Start int, @LC1Length int,
	@LC2Start int, @LC2Length int,
	@LC3Start int, @LC3Length int,
	@LC4Start int, @LC4Length int,
	@LC5Start int, @LC5Length int

SELECT
	@LC1Start = LC1Start, @LC1Length = LC1Length,	@LC2Start = LC2Start, @LC2Length = LC2Length,
	@LC3Start = LC3Start, @LC3Length = LC3Length,	@LC4Start = LC4Start, @LC4Length = LC4Length,
	@LC5Start = LC5Start, @LC5Length = LC5Length
FROM	FW_CFGSystem, CFGFormat

SELECT @UseApprovalWorkflow = UseApprovalWorkflow FROM CFGTKMain WHERE Company = @activeCompany

SELECT	@ExistInTKDetail = COUNT(*) FROM tkDetail WHERE Employee = @Employee AND tkDetail.EndDate = @EndDate AND EmployeeCompany = @activeCompany
SELECT	@ExistInTKMaster = COUNT(*) FROM tkMaster WHERE Employee = @Employee AND tkMaster.EndDate = @EndDate AND EmployeeCompany = @activeCompany
SELECT	@EmployeeTKGroup = ISNULL(TKGroup,N'<ALLGROUP>') FROM EMAllCompany WHERE Employee = @Employee AND EmployeeCompany = @activeCompany
SELECT	@TKGroupHasCategory = COUNT(*) FROM CFGTKCategory WHERE TKGroup = @EmployeeTKGroup AND Company = @activeCompany

IF @ExistInTKMaster = 0		-- new timesheet, detemine which group's categories to use
	SELECT @MasterTKGroup = CASE WHEN @TKGroupHasCategory = 0 THEN '<ALLGROUP>' ELSE @EmployeeTKGroup END, @tkMasterStatus = 'N'
ELSE	-- existing timesheet, then use special categories from saved TKGroup info
BEGIN
	SELECT @tkMsterTKGroup = TKGroup, @tkMasterStatus = Submitted FROM tkMaster WHERE Employee = @Employee AND tkMaster.EndDate = @EndDate AND EmployeeCompany = @activeCompany
	SELECT @TKGroupHasCategory = COUNT(*) FROM CFGTKCategory WHERE TKGroup = @tkMsterTKGroup AND Company = @activeCompany
	SELECT @MasterTKGroup = CASE WHEN @TKGroupHasCategory = 0 THEN '<ALLGROUP>' ELSE @tkMsterTKGroup END
END

IF @ForReport = 'Y'	-- Report
	IF @ExistInTKDetail = 0
		BEGIN
			/* vvvvv Generate special category records if no tkDetail record for employee/enddate vvvvv */
			SELECT
				@MasterTKGroup as TKGroup,
				Employee = @Employee,
				StartDate = @StartDate,
				EndDate = @EndDate,
				Seq = SortSeq,
				TKC.Category As Category,
				isCategory = 'Y',
				TKC.Description As WBS1, TKC.Description As WBS1Name, '' As ClientName, 'N' As WBS2Level,
				Null As WBS2, '' As WBS2Name, 'N' As WBS3Level,
				Null As WBS3, '' As WBS3Name,
				Null As LaborCode, '' As LCName, 0 As BillCategory,
			
				R_day1 = 0, O_day1 = 0, S_day1 = 0, T_day1 = 0, TransComment_day1 = NULL,
				R_day2 = 0, O_day2 = 0, S_day2 = 0, T_day2 = 0, TransComment_day2 = NULL,
				R_day3 = 0, O_day3 = 0, S_day3 = 0, T_day3 = 0, TransComment_day3 = NULL,
				R_day4 = 0, O_day4 = 0, S_day4 = 0, T_day4 = 0, TransComment_day4 = NULL,
				R_day5 = 0, O_day5 = 0, S_day5 = 0, T_day5 = 0, TransComment_day5 = NULL,
				R_day6 = 0, O_day6 = 0, S_day6 = 0, T_day6 = 0, TransComment_day6 = NULL,
				R_day7 = 0, O_day7 = 0, S_day7 = 0, T_day7 = 0, TransComment_day7 = NULL,
				R_day8 = 0, O_day8 = 0, S_day8 = 0, T_day8 = 0, TransComment_day8 = NULL,
				R_day9 = 0, O_day9 = 0, S_day9 = 0, T_day9 = 0, TransComment_day9 = NULL,
				R_day10 = 0, O_day10 = 0, S_day10 = 0, T_day10 = 0, TransComment_day10 = NULL,
				R_day11 = 0, O_day11 = 0, S_day11 = 0, T_day11 = 0, TransComment_day11 = NULL,
				R_day12 = 0, O_day12 = 0, S_day12 = 0, T_day12 = 0, TransComment_day12 = NULL,
				R_day13 = 0, O_day13 = 0, S_day13 = 0, T_day13 = 0, TransComment_day13 = NULL,
				R_day14 = 0, O_day14 = 0, S_day14 = 0, T_day14 = 0, TransComment_day14 = NULL,
				R_day15 = 0, O_day15 = 0, S_day15 = 0, T_day15 = 0, TransComment_day15 = NULL,
				R_day16 = 0, O_day16 = 0, S_day16 = 0, T_day16 = 0, TransComment_day16 = NULL,
				R_day17 = 0, O_day17 = 0, S_day17 = 0, T_day17 = 0, TransComment_day17 = NULL,
				R_day18 = 0, O_day18 = 0, S_day18 = 0, T_day18 = 0, TransComment_day18 = NULL,
				R_day19 = 0, O_day19 = 0, S_day19 = 0, T_day19 = 0, TransComment_day19 = NULL,
				R_day20 = 0, O_day20 = 0, S_day20 = 0, T_day20 = 0, TransComment_day20 = NULL,
				R_day21 = 0, O_day21 = 0, S_day21 = 0, T_day21 = 0, TransComment_day21 = NULL,
				R_day22 = 0, O_day22 = 0, S_day22 = 0, T_day22 = 0, TransComment_day22 = NULL,
				R_day23 = 0, O_day23 = 0, S_day23 = 0, T_day23 = 0, TransComment_day23 = NULL,
				R_day24 = 0, O_day24 = 0, S_day24 = 0, T_day24 = 0, TransComment_day24 = NULL,
				R_day25 = 0, O_day25 = 0, S_day25 = 0, T_day25 = 0, TransComment_day25 = NULL,
				R_day26 = 0, O_day26 = 0, S_day26 = 0, T_day26 = 0, TransComment_day26 = NULL,
				R_day27 = 0, O_day27 = 0, S_day27 = 0, T_day27 = 0, TransComment_day27 = NULL,
				R_day28 = 0, O_day28 = 0, S_day28 = 0, T_day28 = 0, TransComment_day28 = NULL,
				R_day29 = 0, O_day29 = 0, S_day29 = 0, T_day29 = 0, TransComment_day29 = NULL,
				R_day30 = 0, O_day30 = 0, S_day30 = 0, T_day30 = 0, TransComment_day30 = NULL,
				R_day31 = 0, O_day31 = 0, S_day31 = 0, T_day31 = 0, TransComment_day31 = NULL,
				RowRegular = 0, RowOvt = 0, RowOvt2 = 0,
				BillingCategoryName = Null,
				RptSort = 'SPCAT',
				Locale = NULL,
				LocaleDescription = NULL,
				EmployeeCompany = @activeCompany,
				CustTKUD01 = NULL,
				CustTKUD02 = 0,
				CustTKUD03 = NULL,
				CustTKUD01Label = NULL,
				CustTKUD02Label = NULL,
				CustTKUD03Label = NULL,
				Status = ''
			FROM
				CFGTKCategory TKC
			WHERE
				TKC.TKGroup = @MasterTKGroup AND 1=0	-- suppress these records for report
				AND TKC.Company = @activeCompany
			ORDER BY
				isCategory Desc, Seq
			/* ^^^^^ Generate special category records if no tkDetail record ^^^^^ */
		END
	ELSE	/* has tkDetail records for @Employee and @EndDate */
		BEGIN
			/* vvvvv Generate special category records if has no tkDetail records vvvvv */
			SELECT
				@MasterTKGroup as TKGroup,
				Employee = @Employee,
				StartDate = @StartDate,
				EndDate = @EndDate,
				Seq = SortSeq,
				TKC.Category As Category,
				isCategory = 'Y',
				TKC.Description As WBS1, TKC.Description As WBS1Name, '' As ClientName, 'N' As WBS2Level,
				Null As WBS2, '' As WBS2Name, 'N' As WBS3Level,
				Null As WBS3, '' As WBS3Name,
				Null As LaborCode, '' As LCName, 0 As BillCategory,
			
				R_day1 = 0, O_day1 = 0, S_day1 = 0, T_day1 = 0, TransComment_day1 = NULL,
				R_day2 = 0, O_day2 = 0, S_day2 = 0, T_day2 = 0, TransComment_day2 = NULL,
				R_day3 = 0, O_day3 = 0, S_day3 = 0, T_day3 = 0, TransComment_day3 = NULL,
				R_day4 = 0, O_day4 = 0, S_day4 = 0, T_day4 = 0, TransComment_day4 = NULL,
				R_day5 = 0, O_day5 = 0, S_day5 = 0, T_day5 = 0, TransComment_day5 = NULL,
				R_day6 = 0, O_day6 = 0, S_day6 = 0, T_day6 = 0, TransComment_day6 = NULL,
				R_day7 = 0, O_day7 = 0, S_day7 = 0, T_day7 = 0, TransComment_day7 = NULL,
				R_day8 = 0, O_day8 = 0, S_day8 = 0, T_day8 = 0, TransComment_day8 = NULL,
				R_day9 = 0, O_day9 = 0, S_day9 = 0, T_day9 = 0, TransComment_day9 = NULL,
				R_day10 = 0, O_day10 = 0, S_day10 = 0, T_day10 = 0, TransComment_day10 = NULL,
				R_day11 = 0, O_day11 = 0, S_day11 = 0, T_day11 = 0, TransComment_day11 = NULL,
				R_day12 = 0, O_day12 = 0, S_day12 = 0, T_day12 = 0, TransComment_day12 = NULL,
				R_day13 = 0, O_day13 = 0, S_day13 = 0, T_day13 = 0, TransComment_day13 = NULL,
				R_day14 = 0, O_day14 = 0, S_day14 = 0, T_day14 = 0, TransComment_day14 = NULL,
				R_day15 = 0, O_day15 = 0, S_day15 = 0, T_day15 = 0, TransComment_day15 = NULL,
				R_day16 = 0, O_day16 = 0, S_day16 = 0, T_day16 = 0, TransComment_day16 = NULL,
				R_day17 = 0, O_day17 = 0, S_day17 = 0, T_day17 = 0, TransComment_day17 = NULL,
				R_day18 = 0, O_day18 = 0, S_day18 = 0, T_day18 = 0, TransComment_day18 = NULL,
				R_day19 = 0, O_day19 = 0, S_day19 = 0, T_day19 = 0, TransComment_day19 = NULL,
				R_day20 = 0, O_day20 = 0, S_day20 = 0, T_day20 = 0, TransComment_day20 = NULL,
				R_day21 = 0, O_day21 = 0, S_day21 = 0, T_day21 = 0, TransComment_day21 = NULL,
				R_day22 = 0, O_day22 = 0, S_day22 = 0, T_day22 = 0, TransComment_day22 = NULL,
				R_day23 = 0, O_day23 = 0, S_day23 = 0, T_day23 = 0, TransComment_day23 = NULL,
				R_day24 = 0, O_day24 = 0, S_day24 = 0, T_day24 = 0, TransComment_day24 = NULL,
				R_day25 = 0, O_day25 = 0, S_day25 = 0, T_day25 = 0, TransComment_day25 = NULL,
				R_day26 = 0, O_day26 = 0, S_day26 = 0, T_day26 = 0, TransComment_day26 = NULL,
				R_day27 = 0, O_day27 = 0, S_day27 = 0, T_day27 = 0, TransComment_day27 = NULL,
				R_day28 = 0, O_day28 = 0, S_day28 = 0, T_day28 = 0, TransComment_day28 = NULL,
				R_day29 = 0, O_day29 = 0, S_day29 = 0, T_day29 = 0, TransComment_day29 = NULL,
				R_day30 = 0, O_day30 = 0, S_day30 = 0, T_day30 = 0, TransComment_day30 = NULL,
				R_day31 = 0, O_day31 = 0, S_day31 = 0, T_day31 = 0, TransComment_day31 = NULL,
				RowRegular = 0, RowOvt = 0, RowOvt2 = 0,
				BillingCategoryName = Null,
				RptSort = 'SPCAT',
				Locale = NULL,
				LocaleDescription = NULL,
				EmployeeCompany = @activeCompany,
				CustTKUD01 = NULL,
				CustTKUD02 = 0,
				CustTKUD03 = NULL,
				CustTKUD01Label = NULL,
				CustTKUD02Label = NULL,
				CustTKUD03Label = NULL,
				Status = ''
			FROM
				CFGTKCategory TKC
				LEFT OUTER JOIN (SELECT DISTINCT Category FROM tkDetail WHERE Employee = @Employee AND tkDetail.EndDate = @EndDate AND tkDetail.EmployeeCompany = @activeCompany AND (Category IS NOT NULL OR Category = N' ')) x
				ON TKC.Category = x.Category
			WHERE
				TKC.TKGroup = @MasterTKGroup AND 1=0	-- suppress these records for report
				AND X.Category IS NULL
				AND TKC.Company = @activeCompany
			/* ^^^^^ Generate special category records if has no tkDetail records ^^^^^ */
		
			/* vvvvv Get special category tkDetail records vvvvv */
			UNION ALL
			SELECT
				@MasterTKGroup as TKGroup,
				Employee = @Employee,
				StartDate = @StartDate,
				EndDate = @EndDate,
				Seq = CFGTKCategory.SortSeq,
				tkDetail.Category, isCategory = 'Y',
				CFGTKCategory.Description As WBS1, CFGTKCategory.Description As WBS1Name, '' As ClientName, 'N' As WBS2Level,
				Null As WBS2, '' As WBS2Name, 'N' As WBS3Level,
				Null As WBS3, '' As WBS3Name,
				Null As LaborCode, '' As LCName, tkDetail.BillCategory,
			
				R_day1 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN RegHrs ELSE 0 END),
				O_day1 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN OvtHrs ELSE 0 END),
				S_day1 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN SpecialOvtHrs ELSE 0 END),
				T_day1 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN TransComment ELSE NULL END),
			
				R_day2 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN RegHrs ELSE 0 END),
				O_day2 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN OvtHrs ELSE 0 END),
				S_day2 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN SpecialOvtHrs ELSE 0 END),
				T_day2 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN TransComment ELSE NULL END),
			
				R_day3 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN RegHrs ELSE 0 END),
				O_day3 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN OvtHrs ELSE 0 END),
				S_day3 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN SpecialOvtHrs ELSE 0 END),
				T_day3 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN TransComment ELSE NULL END),
			
				R_day4 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN RegHrs ELSE 0 END),
				O_day4 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN OvtHrs ELSE 0 END),
				S_day4 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN SpecialOvtHrs ELSE 0 END),
				T_day4 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN TransComment ELSE NULL END),
			
				R_day5 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN RegHrs ELSE 0 END),
				O_day5 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN OvtHrs ELSE 0 END),
				S_day5 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN SpecialOvtHrs ELSE 0 END),
				T_day5 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN TransComment ELSE NULL END),
			
				R_day6 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN RegHrs ELSE 0 END),
				O_day6 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN OvtHrs ELSE 0 END),
				S_day6 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN SpecialOvtHrs ELSE 0 END),
				T_day6 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN TransComment ELSE NULL END),
			
				R_day7 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN RegHrs ELSE 0 END),
				O_day7 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN OvtHrs ELSE 0 END),
				S_day7 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN SpecialOvtHrs ELSE 0 END),
				T_day7 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN TransComment ELSE NULL END),
			
				R_day8 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN RegHrs ELSE 0 END),
				O_day8 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN OvtHrs ELSE 0 END),
				S_day8 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN SpecialOvtHrs ELSE 0 END),
				T_day8 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN TransComment ELSE NULL END),
			
				R_day9 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN RegHrs ELSE 0 END),
				O_day9 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN OvtHrs ELSE 0 END),
				S_day9 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN SpecialOvtHrs ELSE 0 END),
				T_day9 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN TransComment ELSE NULL END),
			
				R_day10 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN RegHrs ELSE 0 END),
				O_day10 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN OvtHrs ELSE 0 END),
				S_day10 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN SpecialOvtHrs ELSE 0 END),
				T_day10 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN TransComment ELSE NULL END),
			
				R_day11 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN RegHrs ELSE 0 END),
				O_day11 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN OvtHrs ELSE 0 END),
				S_day11 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN SpecialOvtHrs ELSE 0 END),
				T_day11 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN TransComment ELSE NULL END),
			
				R_day12 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN RegHrs ELSE 0 END),
				O_day12 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN OvtHrs ELSE 0 END),
				S_day12 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN SpecialOvtHrs ELSE 0 END),
				T_day12 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN TransComment ELSE NULL END),
			
				R_day13 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN RegHrs ELSE 0 END),
				O_day13 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN OvtHrs ELSE 0 END),
				S_day13 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN SpecialOvtHrs ELSE 0 END),
				T_day13 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN TransComment ELSE NULL END),
			
				R_day14 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN RegHrs ELSE 0 END),
				O_day14 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN OvtHrs ELSE 0 END),
				S_day14 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN SpecialOvtHrs ELSE 0 END),
				T_day14 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN TransComment ELSE NULL END),
			
				R_day15 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN RegHrs ELSE 0 END),
				O_day15 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN OvtHrs ELSE 0 END),
				S_day15 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN SpecialOvtHrs ELSE 0 END),
				T_day15 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN TransComment ELSE NULL END),
			
				R_day16 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN RegHrs ELSE 0 END),
				O_day16 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN OvtHrs ELSE 0 END),
				S_day16 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN SpecialOvtHrs ELSE 0 END),
				T_day16 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN TransComment ELSE NULL END),
			
				R_day17 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN RegHrs ELSE 0 END),
				O_day17 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN OvtHrs ELSE 0 END),
				S_day17 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN SpecialOvtHrs ELSE 0 END),
				T_day17 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN TransComment ELSE NULL END),
			
				R_day18 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN RegHrs ELSE 0 END),
				O_day18 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN OvtHrs ELSE 0 END),
				S_day18 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN SpecialOvtHrs ELSE 0 END),
				T_day18 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN TransComment ELSE NULL END),
			
				R_day19 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN RegHrs ELSE 0 END),
				O_day19 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN OvtHrs ELSE 0 END),
				S_day19 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN SpecialOvtHrs ELSE 0 END),
				T_day19 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN TransComment ELSE NULL END),
			
				R_day20 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN RegHrs ELSE 0 END),
				O_day20 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN OvtHrs ELSE 0 END),
				S_day20 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN SpecialOvtHrs ELSE 0 END),
				T_day20 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN TransComment ELSE NULL END),
			
				R_day21 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN RegHrs ELSE 0 END),
				O_day21 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN OvtHrs ELSE 0 END),
				S_day21 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN SpecialOvtHrs ELSE 0 END),
				T_day21 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN TransComment ELSE NULL END),
			
				R_day22 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN RegHrs ELSE 0 END),
				O_day22 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN OvtHrs ELSE 0 END),
				S_day22 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN SpecialOvtHrs ELSE 0 END),
				T_day22 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN TransComment ELSE NULL END),
			
				R_day23 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN RegHrs ELSE 0 END),
				O_day23 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN OvtHrs ELSE 0 END),
				S_day23 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN SpecialOvtHrs ELSE 0 END),
				T_day23 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN TransComment ELSE NULL END),
			
				R_day24 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN RegHrs ELSE 0 END),
				O_day24 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN OvtHrs ELSE 0 END),
				S_day24 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN SpecialOvtHrs ELSE 0 END),
				T_day24 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN TransComment ELSE NULL END),
			
				R_day25 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN RegHrs ELSE 0 END),
				O_day25 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN OvtHrs ELSE 0 END),
				S_day25 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN SpecialOvtHrs ELSE 0 END),
				T_day25 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN TransComment ELSE NULL END),
			
				R_day26 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN RegHrs ELSE 0 END),
				O_day26 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN OvtHrs ELSE 0 END),
				S_day26 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN SpecialOvtHrs ELSE 0 END),
				T_day26 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN TransComment ELSE NULL END),
			
				R_day27 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN RegHrs ELSE 0 END),
				O_day27 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN OvtHrs ELSE 0 END),
				S_day27 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN SpecialOvtHrs ELSE 0 END),
				T_day27 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN TransComment ELSE NULL END),
			
				R_day28 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN RegHrs ELSE 0 END),
				O_day28 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN OvtHrs ELSE 0 END),
				S_day28 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN SpecialOvtHrs ELSE 0 END),
				T_day28 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN TransComment ELSE NULL END),
			
				R_day29 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN RegHrs ELSE 0 END),
				O_day29 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN OvtHrs ELSE 0 END),
				S_day29 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN SpecialOvtHrs ELSE 0 END),
				T_day29 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN TransComment ELSE NULL END),
			
				R_day30 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN RegHrs ELSE 0 END),
				O_day30 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN OvtHrs ELSE 0 END),
				S_day30 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN SpecialOvtHrs ELSE 0 END),
				T_day30 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN TransComment ELSE NULL END),
			
				R_day31 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN RegHrs ELSE 0 END),
				O_day31 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN OvtHrs ELSE 0 END),
				S_day31 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN SpecialOvtHrs ELSE 0 END),
				T_day31 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN TransComment ELSE NULL END),
				RowRegular = SUM(RegHrs),
				RowOvt = SUM(OvtHrs),
				RowOvt2 = SUM(SpecialOvtHrs),
				BillingCategoryName = MAX(BTLaborCats.Description),
				RptSort = 'SPCAT',
				Locale = MAX(tkDetail.Locale),
				LocaleDescription = MAX(LocaleList.Description),
				MAX(tkDetail.EmployeeCompany) AS EmployeeCompany,
				CustTKUD01 = max(CustomColVal.DataValue),
				CustTKUD02 = max(tkCustomFields.CustTKUD02),
				CustTKUD03 = max(tkCustomFields.CustTKUD03),
				CustTKUD01Label = isnull(max(UD01.Label),'CustTKUD01'),
				CustTKUD02Label = isnull(max(UD02.Label),'CustTKUD02'),
				CustTKUD03Label = isnull(max(UD03.Label),'CustTKUD03'),
				Status = ''
			FROM
				tkDetail
				LEFT JOIN CFGTKCategory ON tkDetail.Category = CFGTKCategory.Category
				LEFT JOIN BTLaborCats ON tkDetail.BillCategory = BTLaborCats.Category
				LEFT JOIN (SELECT Locale, Description FROM CFGLocale
				UNION ALL SELECT Locale, Description FROM TaxLocale) LocaleList ON tkDetail.Locale = LocaleList.Locale
				LEFT JOIN tkCustomFields on tkCustomFields.EndDate=tkDetail.EndDate and tkCustomFields.Employee=tkDetail.Employee and tkCustomFields.EmployeeCompany=tkDetail.EmployeeCompany and tkCustomFields.Seq=tkDetail.Seq 
				LEFT JOIN transactionUDF as UD01 on UD01.TransType='TS' and UD01.Company=tkDetail.EmployeeCompany and UD01.UDFID='CustTKUD01' 
                LEFT JOIN FW_CustomColumnValues as CustomColVal on UD01.UDFID = CustomColVal.ColName and CustomColVal.InfoCenterArea = 'Time' and tkCustomFields.CustTKUD01=CustomColVal.Code 
				LEFT JOIN transactionUDF as UD02 on UD02.TransType='TS' and UD02.Company=tkDetail.EmployeeCompany and UD02.UDFID='CustTKUD02' 
				LEFT JOIN transactionUDF as UD03 on UD03.TransType='TS' and UD03.Company=tkDetail.EmployeeCompany and UD03.UDFID='CustTKUD03' 
			WHERE
				CFGTKCategory.TKGroup = @MasterTKGroup
				And tkDetail.Employee = @Employee
				And tkDetail.EndDate = @EndDate
				And tkDetail.EmployeeCompany = @activeCompany
			-- suppress special category lines with no hours & comments for report
				And (tkDetail.RegHrs <> 0 OR tkDetail.OvtHrs <> 0 OR tkDetail.SpecialOvtHrs <> 0 OR TransComment IS NOT NULL)
				AND CFGTKCategory.Company = @activeCompany
			GROUP BY
				CFGTKCategory.Description, tkDetail.WBS2, tkDetail.WBS3, CFGTKCategory.SortSeq,
				tkDetail.Category, tkDetail.LaborCode, tkDetail.BillCategory, BTLaborCats.Category
			/* ^^^^^ Get special category tkDetail records ^^^^^ */
			
			/* vvvvv Get non special category tkDetail records vvvvv */
			UNION ALL SELECT
				@MasterTKGroup as TKGroup,
				Employee = @Employee,
				StartDate = @StartDate,
				EndDate = @EndDate,
				Seq = CASE WHEN @PrintByProject = 'N' THEN tkDetail.Seq ELSE '' END,
				tkDetail.Category,
				isCategory = 'N',
				tkDetail.WBS1, MAX(PR1.Name) As WBS1Name, MAX(CL.Name) As ClientName, MAX(PR1.SubLevel) As WBS2Level,
				tkDetail.WBS2, MAX(CASE WHEN tkDetail.WBS2 = N'' THEN '' ELSE PR2.Name END) As WBS2Name, MAX(PR2.SubLevel) As WBS3Level,
				tkDetail.WBS3, MAX(CASE WHEN tkDetail.WBS3 = N'' THEN '' ELSE PR3.Name END) As WBS3Name,
				tkDetail.LaborCode,
				MAX(LC1.Label + 
					(CASE WHEN LC2.Label IS NULL THEN '' ELSE '/' + LC2.Label END) +
					(CASE WHEN LC3.Label IS NULL THEN '' ELSE '/' + LC3.Label END) +
					(CASE WHEN LC4.Label IS NULL THEN '' ELSE '/' + LC4.Label END) +
					(CASE WHEN LC5.Label IS NULL THEN '' ELSE '/' + LC5.Label END)
				) As LCName,
				tkDetail.BillCategory,
			
				R_day1 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN RegHrs ELSE 0 END),
				O_day1 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN OvtHrs ELSE 0 END),
				S_day1 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN SpecialOvtHrs ELSE 0 END),
				T_day1 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN TransComment ELSE NULL END),
			
				R_day2 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN RegHrs ELSE 0 END),
				O_day2 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN OvtHrs ELSE 0 END),
				S_day2 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN SpecialOvtHrs ELSE 0 END),
				T_day2 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN TransComment ELSE NULL END),
			
				R_day3 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN RegHrs ELSE 0 END),
				O_day3 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN OvtHrs ELSE 0 END),
				S_day3 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN SpecialOvtHrs ELSE 0 END),
				T_day3 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN TransComment ELSE NULL END),
			
				R_day4 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN RegHrs ELSE 0 END),
				O_day4 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN OvtHrs ELSE 0 END),
				S_day4 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN SpecialOvtHrs ELSE 0 END),
				T_day4 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN TransComment ELSE NULL END),
			
				R_day5 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN RegHrs ELSE 0 END),
				O_day5 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN OvtHrs ELSE 0 END),
				S_day5 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN SpecialOvtHrs ELSE 0 END),
				T_day5 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN TransComment ELSE NULL END),
			
				R_day6 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN RegHrs ELSE 0 END),
				O_day6 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN OvtHrs ELSE 0 END),
				S_day6 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN SpecialOvtHrs ELSE 0 END),
				T_day6 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN TransComment ELSE NULL END),
			
				R_day7 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN RegHrs ELSE 0 END),
				O_day7 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN OvtHrs ELSE 0 END),
				S_day7 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN SpecialOvtHrs ELSE 0 END),
				T_day7 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN TransComment ELSE NULL END),
			
				R_day8 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN RegHrs ELSE 0 END),
				O_day8 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN OvtHrs ELSE 0 END),
				S_day8 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN SpecialOvtHrs ELSE 0 END),
				T_day8 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN TransComment ELSE NULL END),
			
				R_day9 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN RegHrs ELSE 0 END),
				O_day9 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN OvtHrs ELSE 0 END),
				S_day9 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN SpecialOvtHrs ELSE 0 END),
				T_day9 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN TransComment ELSE NULL END),
			
				R_day10 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN RegHrs ELSE 0 END),
				O_day10 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN OvtHrs ELSE 0 END),
				S_day10 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN SpecialOvtHrs ELSE 0 END),
				T_day10 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN TransComment ELSE NULL END),
			
				R_day11 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN RegHrs ELSE 0 END),
				O_day11 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN OvtHrs ELSE 0 END),
				S_day11 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN SpecialOvtHrs ELSE 0 END),
				T_day11 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN TransComment ELSE NULL END),
			
				R_day12 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN RegHrs ELSE 0 END),
				O_day12 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN OvtHrs ELSE 0 END),
				S_day12 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN SpecialOvtHrs ELSE 0 END),
				T_day12 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN TransComment ELSE NULL END),
			
				R_day13 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN RegHrs ELSE 0 END),
				O_day13 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN OvtHrs ELSE 0 END),
				S_day13 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN SpecialOvtHrs ELSE 0 END),
				T_day13 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN TransComment ELSE NULL END),
			
				R_day14 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN RegHrs ELSE 0 END),
				O_day14 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN OvtHrs ELSE 0 END),
				S_day14 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN SpecialOvtHrs ELSE 0 END),
				T_day14 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN TransComment ELSE NULL END),
			
				R_day15 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN RegHrs ELSE 0 END),
				O_day15 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN OvtHrs ELSE 0 END),
				S_day15 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN SpecialOvtHrs ELSE 0 END),
				T_day15 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN TransComment ELSE NULL END),
			
				R_day16 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN RegHrs ELSE 0 END),
				O_day16 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN OvtHrs ELSE 0 END),
				S_day16 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN SpecialOvtHrs ELSE 0 END),
				T_day16 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN TransComment ELSE NULL END),
			
				R_day17 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN RegHrs ELSE 0 END),
				O_day17 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN OvtHrs ELSE 0 END),
				S_day17 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN SpecialOvtHrs ELSE 0 END),
				T_day17 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN TransComment ELSE NULL END),
			
				R_day18 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN RegHrs ELSE 0 END),
				O_day18 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN OvtHrs ELSE 0 END),
				S_day18 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN SpecialOvtHrs ELSE 0 END),
				T_day18 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN TransComment ELSE NULL END),
			
				R_day19 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN RegHrs ELSE 0 END),
				O_day19 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN OvtHrs ELSE 0 END),
				S_day19 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN SpecialOvtHrs ELSE 0 END),
				T_day19 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN TransComment ELSE NULL END),
			
				R_day20 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN RegHrs ELSE 0 END),
				O_day20 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN OvtHrs ELSE 0 END),
				S_day20 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN SpecialOvtHrs ELSE 0 END),
				T_day20 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN TransComment ELSE NULL END),
			
				R_day21 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN RegHrs ELSE 0 END),
				O_day21 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN OvtHrs ELSE 0 END),
				S_day21 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN SpecialOvtHrs ELSE 0 END),
				T_day21 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN TransComment ELSE NULL END),
			
				R_day22 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN RegHrs ELSE 0 END),
				O_day22 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN OvtHrs ELSE 0 END),
				S_day22 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN SpecialOvtHrs ELSE 0 END),
				T_day22 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN TransComment ELSE NULL END),
			
				R_day23 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN RegHrs ELSE 0 END),
				O_day23 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN OvtHrs ELSE 0 END),
				S_day23 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN SpecialOvtHrs ELSE 0 END),
				T_day23 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN TransComment ELSE NULL END),
			
				R_day24 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN RegHrs ELSE 0 END),
				O_day24 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN OvtHrs ELSE 0 END),
				S_day24 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN SpecialOvtHrs ELSE 0 END),
				T_day24 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN TransComment ELSE NULL END),
			
				R_day25 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN RegHrs ELSE 0 END),
				O_day25 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN OvtHrs ELSE 0 END),
				S_day25 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN SpecialOvtHrs ELSE 0 END),
				T_day25 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN TransComment ELSE NULL END),
			
				R_day26 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN RegHrs ELSE 0 END),
				O_day26 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN OvtHrs ELSE 0 END),
				S_day26 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN SpecialOvtHrs ELSE 0 END),
				T_day26 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN TransComment ELSE NULL END),
			
				R_day27 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN RegHrs ELSE 0 END),
				O_day27 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN OvtHrs ELSE 0 END),
				S_day27 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN SpecialOvtHrs ELSE 0 END),
				T_day27 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN TransComment ELSE NULL END),
			
				R_day28 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN RegHrs ELSE 0 END),
				O_day28 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN OvtHrs ELSE 0 END),
				S_day28 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN SpecialOvtHrs ELSE 0 END),
				T_day28 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN TransComment ELSE NULL END),
			
				R_day29 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN RegHrs ELSE 0 END),
				O_day29 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN OvtHrs ELSE 0 END),
				S_day29 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN SpecialOvtHrs ELSE 0 END),
				T_day29 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN TransComment ELSE NULL END),
			
				R_day30 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN RegHrs ELSE 0 END),
				O_day30 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN OvtHrs ELSE 0 END),
				S_day30 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN SpecialOvtHrs ELSE 0 END),
				T_day30 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN TransComment ELSE NULL END),
			
				R_day31 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN RegHrs ELSE 0 END),
				O_day31 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN OvtHrs ELSE 0 END),
				S_day31 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN SpecialOvtHrs ELSE 0 END),
				T_day31 = SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END),
				TransComment_day31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN TransComment ELSE NULL END),
				RowRegular = SUM(RegHrs),
				RowOvt = SUM(OvtHrs),
				RowOvt2 = SUM(SpecialOvtHrs),
				BillingCategoryName = MAX(BTLaborCats.Description),
				RptSort = tkDetail.WBS1,
				Locale = MAX(tkDetail.Locale),
				LocaleDescription = MAX(LocaleList.Description),
				EmployeeCompany = @activeCompany,
				CustTKUD01 = max(CustomColVal.DataValue),
				CustTKUD02 = max(tkCustomFields.CustTKUD02),
				CustTKUD03 = max(tkCustomFields.CustTKUD03),
				CustTKUD01Label = isnull(max(UD01.Label),'CustTKUD01'),
				CustTKUD02Label = isnull(max(UD02.Label),'CustTKUD02'),
				CustTKUD03Label = isnull(max(UD03.Label),'CustTKUD03'),
				Status = MAX(CASE WHEN tkDetail.WBS3 <> ' ' THEN PR3.Status
				              WHEN tkDetail.WBS2 <> ' ' THEN PR2.Status
							  ELSE PR1.Status
						 END)
			FROM
				tkDetail
				LEFT JOIN PR AS PR1 ON PR1.WBS1 = tkDetail.WBS1 --AND PR1.WBS2 = ' ' AND tkDetail.WBS3 = ' '
				LEFT JOIN PR AS PR2 on PR2.WBS1 = tkDetail.WBS1 AND PR2.WBS2 = tkDetail.WBS2 --AND PR2.WBS3 = ' ' AND PR2.WBS2 <> ' '
				LEFT JOIN PR AS PR3 on PR3.WBS1 = tkDetail.WBS1 AND PR3.WBS2 = tkDetail.WBS2 AND PR3.WBS3 = tkDetail.WBS3 --AND PR3.WBS3 <> ' '
				LEFT JOIN CL ON PR1.ClientID = CL.ClientID
				LEFT JOIN CFGLCCodes AS LC1 ON (LC1.LCLevel = 1 AND LC1.Code = SUBSTRING(tkDetail.LaborCode, @LC1Start, @LC1Length))
				LEFT JOIN CFGLCCodes AS LC2 ON (LC2.LCLevel = 2 AND LC2.Code = SUBSTRING(tkDetail.LaborCode, @LC2Start, @LC2Length))
				LEFT JOIN CFGLCCodes AS LC3 ON (LC3.LCLevel = 3 AND LC3.Code = SUBSTRING(tkDetail.LaborCode, @LC3Start, @LC3Length))
				LEFT JOIN CFGLCCodes AS LC4 ON (LC4.LCLevel = 4 AND LC4.Code = SUBSTRING(tkDetail.LaborCode, @LC4Start, @LC4Length))
				LEFT JOIN CFGLCCodes AS LC5 ON (LC5.LCLevel = 5 AND LC5.Code = SUBSTRING(tkDetail.LaborCode, @LC5Start, @LC5Length))
				LEFT JOIN BTLaborCats ON tkDetail.BillCategory = BTLaborCats.Category
				LEFT JOIN (SELECT Locale, Description FROM CFGLocale
				 UNION ALL SELECT Locale, Description FROM TaxLocale) LocaleList ON tkDetail.Locale = LocaleList.Locale
				LEFT JOIN tkCustomFields on tkCustomFields.EndDate=tkDetail.EndDate and tkCustomFields.Employee=tkDetail.Employee and tkCustomFields.EmployeeCompany=tkDetail.EmployeeCompany and tkCustomFields.Seq=tkDetail.Seq 
				LEFT JOIN transactionUDF as UD01 on UD01.TransType='TS' and UD01.Company=tkDetail.EmployeeCompany and UD01.UDFID='CustTKUD01' 
                LEFT JOIN FW_CustomColumnValues as CustomColVal on UD01.UDFID = CustomColVal.ColName and CustomColVal.InfoCenterArea = 'Time' and tkCustomFields.CustTKUD01=CustomColVal.Code 
				LEFT JOIN transactionUDF as UD02 on UD02.TransType='TS' and UD02.Company=tkDetail.EmployeeCompany and UD02.UDFID='CustTKUD02' 
				LEFT JOIN transactionUDF as UD03 on UD03.TransType='TS' and UD03.Company=tkDetail.EmployeeCompany and UD03.UDFID='CustTKUD03' 
			WHERE
				tkDetail.Employee = @Employee AND
				tkDetail.EndDate = @EndDate AND
				tkDetail.EmployeeCompany = @activeCompany AND
				PR1.WBS2 = N' ' AND
				PR1.WBS3 = N' ' AND
				PR2.WBS3 = N' ' AND
				(tkDetail.Category = N' ' OR tkDetail.Category IS NULL)
				-- suppress non-special category lines with no hours & comments for report
				And (tkDetail.RegHrs <> 0 OR tkDetail.OvtHrs <> 0 OR tkDetail.SpecialOvtHrs <> 0 OR TransComment IS NOT NULL)
			GROUP BY
				tkDetail.WBS1, tkDetail.WBS2, tkDetail.WBS3, tkDetail.Seq,
				tkDetail.Category, tkDetail.LaborCode, tkDetail.BillCategory, BTLaborCats.Category
			/* ^^^^^ Get non special category tkDetail records ^^^^^ */
			ORDER BY
				isCategory Desc, Seq, RptSort, WBS2, WBS3, LaborCode
		END
ELSE	-- data entry
	IF @ExistInTKDetail = 0
		BEGIN
			/* vvvvv Generate special category records if no tkDetail record for employee/enddate vvvvv */
			SELECT
				SortOrder = 0,
				@MasterTKGroup as TKGroup,
				Employee = @Employee,
				StartDate = CONVERT(VARCHAR(10),@StartDate,121),
				EndDate = CONVERT(VARCHAR(10),@EndDate,121),
				Seq = SortSeq,
				TKC.Category As Category,
				isCategory = 'Y',
				TKC.Description As WBS1, TKC.Description As WBS1Name, '' As ClientName, 'N' As WBS2Level,
				'' As ChargeType,
				' ' As WBS2, '' As WBS2Name, 'N' As WBS3Level,
				' ' As WBS3, '' As WBS3Name,
				'' As LaborCode, '' As LCName, 0 As BillCategory,
			
				R_day1 = 0, O_day1 = 0, S_day1 = 0, T_day1 = 0, TransComment_day1 = NULL, OrigDetailSeq_day1 = -1, LineItemApprovedBy1 = Null, StartTime1 = Null, EndTime1 = NULL,
				R_day2 = 0, O_day2 = 0, S_day2 = 0, T_day2 = 0, TransComment_day2 = NULL, OrigDetailSeq_day2 = -1, LineItemApprovedBy2 = Null, StartTime2 = Null, EndTime2 = NULL,
				R_day3 = 0, O_day3 = 0, S_day3 = 0, T_day3 = 0, TransComment_day3 = NULL, OrigDetailSeq_day3 = -1, LineItemApprovedBy3 = Null, StartTime3 = Null, EndTime3 = NULL,
				R_day4 = 0, O_day4 = 0, S_day4 = 0, T_day4 = 0, TransComment_day4 = NULL, OrigDetailSeq_day4 = -1, LineItemApprovedBy4 = Null, StartTime4 = Null, EndTime4 = NULL,
				R_day5 = 0, O_day5 = 0, S_day5 = 0, T_day5 = 0, TransComment_day5 = NULL, OrigDetailSeq_day5 = -1, LineItemApprovedBy5 = Null, StartTime5 = Null, EndTime5 = NULL,
				R_day6 = 0, O_day6 = 0, S_day6 = 0, T_day6 = 0, TransComment_day6 = NULL, OrigDetailSeq_day6 = -1, LineItemApprovedBy6 = Null, StartTime6 = Null, EndTime6 = NULL,
				R_day7 = 0, O_day7 = 0, S_day7 = 0, T_day7 = 0, TransComment_day7 = NULL, OrigDetailSeq_day7 = -1, LineItemApprovedBy7 = Null, StartTime7 = Null, EndTime7 = NULL,
				R_day8 = 0, O_day8 = 0, S_day8 = 0, T_day8 = 0, TransComment_day8 = NULL, OrigDetailSeq_day8 = -1, LineItemApprovedBy8 = Null, StartTime8 = Null, EndTime8 = NULL,
				R_day9 = 0, O_day9 = 0, S_day9 = 0, T_day9 = 0, TransComment_day9 = NULL, OrigDetailSeq_day9 = -1, LineItemApprovedBy9 = Null, StartTime9 = Null, EndTime9 = NULL,
				R_day10 = 0, O_day10 = 0, S_day10 = 0, T_day10 = 0, TransComment_day10 = NULL, OrigDetailSeq_day10 = -1, LineItemApprovedBy10 = Null, StartTime10 = Null, EndTime10 = NULL,
				R_day11 = 0, O_day11 = 0, S_day11 = 0, T_day11 = 0, TransComment_day11 = NULL, OrigDetailSeq_day11 = -1, LineItemApprovedBy11 = Null, StartTime11 = Null, EndTime11 = NULL,
				R_day12 = 0, O_day12 = 0, S_day12 = 0, T_day12 = 0, TransComment_day12 = NULL, OrigDetailSeq_day12 = -1, LineItemApprovedBy12 = Null, StartTime12 = Null, EndTime12 = NULL,
				R_day13 = 0, O_day13 = 0, S_day13 = 0, T_day13 = 0, TransComment_day13 = NULL, OrigDetailSeq_day13 = -1, LineItemApprovedBy13 = Null, StartTime13 = Null, EndTime13 = NULL,
				R_day14 = 0, O_day14 = 0, S_day14 = 0, T_day14 = 0, TransComment_day14 = NULL, OrigDetailSeq_day14 = -1, LineItemApprovedBy14 = Null, StartTime14 = Null, EndTime14 = NULL,
				R_day15 = 0, O_day15 = 0, S_day15 = 0, T_day15 = 0, TransComment_day15 = NULL, OrigDetailSeq_day15 = -1, LineItemApprovedBy15 = Null, StartTime15 = Null, EndTime15 = NULL,
				R_day16 = 0, O_day16 = 0, S_day16 = 0, T_day16 = 0, TransComment_day16 = NULL, OrigDetailSeq_day16 = -1, LineItemApprovedBy16 = Null, StartTime16 = Null, EndTime16 = NULL,
				R_day17 = 0, O_day17 = 0, S_day17 = 0, T_day17 = 0, TransComment_day17 = NULL, OrigDetailSeq_day17 = -1, LineItemApprovedBy17 = Null, StartTime17 = Null, EndTime17 = NULL,
				R_day18 = 0, O_day18 = 0, S_day18 = 0, T_day18 = 0, TransComment_day18 = NULL, OrigDetailSeq_day18 = -1, LineItemApprovedBy18 = Null, StartTime18 = Null, EndTime18 = NULL,
				R_day19 = 0, O_day19 = 0, S_day19 = 0, T_day19 = 0, TransComment_day19 = NULL, OrigDetailSeq_day19 = -1, LineItemApprovedBy19 = Null, StartTime19 = Null, EndTime19 = NULL,
				R_day20 = 0, O_day20 = 0, S_day20 = 0, T_day20 = 0, TransComment_day20 = NULL, OrigDetailSeq_day20 = -1, LineItemApprovedBy20 = Null, StartTime20 = Null, EndTime20 = NULL,
				R_day21 = 0, O_day21 = 0, S_day21 = 0, T_day21 = 0, TransComment_day21 = NULL, OrigDetailSeq_day21 = -1, LineItemApprovedBy21 = Null, StartTime21 = Null, EndTime21 = NULL,
				R_day22 = 0, O_day22 = 0, S_day22 = 0, T_day22 = 0, TransComment_day22 = NULL, OrigDetailSeq_day22 = -1, LineItemApprovedBy22 = Null, StartTime22 = Null, EndTime22 = NULL,
				R_day23 = 0, O_day23 = 0, S_day23 = 0, T_day23 = 0, TransComment_day23 = NULL, OrigDetailSeq_day23 = -1, LineItemApprovedBy23 = Null, StartTime23 = Null, EndTime23 = NULL,
				R_day24 = 0, O_day24 = 0, S_day24 = 0, T_day24 = 0, TransComment_day24 = NULL, OrigDetailSeq_day24 = -1, LineItemApprovedBy24 = Null, StartTime24 = Null, EndTime24 = NULL,
				R_day25 = 0, O_day25 = 0, S_day25 = 0, T_day25 = 0, TransComment_day25 = NULL, OrigDetailSeq_day25 = -1, LineItemApprovedBy25 = Null, StartTime25 = Null, EndTime25 = NULL,
				R_day26 = 0, O_day26 = 0, S_day26 = 0, T_day26 = 0, TransComment_day26 = NULL, OrigDetailSeq_day26 = -1, LineItemApprovedBy26 = Null, StartTime26 = Null, EndTime26 = NULL,
				R_day27 = 0, O_day27 = 0, S_day27 = 0, T_day27 = 0, TransComment_day27 = NULL, OrigDetailSeq_day27 = -1, LineItemApprovedBy27 = Null, StartTime27 = Null, EndTime27 = NULL,
				R_day28 = 0, O_day28 = 0, S_day28 = 0, T_day28 = 0, TransComment_day28 = NULL, OrigDetailSeq_day28 = -1, LineItemApprovedBy28 = Null, StartTime28 = Null, EndTime28 = NULL,
				R_day29 = 0, O_day29 = 0, S_day29 = 0, T_day29 = 0, TransComment_day29 = NULL, OrigDetailSeq_day29 = -1, LineItemApprovedBy29 = Null, StartTime29 = Null, EndTime29 = NULL,
				R_day30 = 0, O_day30 = 0, S_day30 = 0, T_day30 = 0, TransComment_day30 = NULL, OrigDetailSeq_day30 = -1, LineItemApprovedBy30 = Null, StartTime30 = Null, EndTime30 = NULL,
				R_day31 = 0, O_day31 = 0, S_day31 = 0, T_day31 = 0, TransComment_day31 = NULL, OrigDetailSeq_day31 = -1, LineItemApprovedBy31 = Null, StartTime31 = Null, EndTime31 = NULL,
				RowRegular = 0, RowOvt = 0, RowOvt2 = 0,
				LineItemWBSLock = 'Y',
				BudgetedFlag = 'N',
				BudgetedLevels = '',
				BudgetSource = '',
				BudgetLevel = '',
				LineItemApproval = 'N',
				Locale = '',
				WBS1Locale = '',
				WBS2Locale = '',
				WBS3Locale = '',
				TKCheckRPDate = 'N',
				RequireComments = 'C',
				TKCAllowStartEndTime = TKC.AllowStartEndTime,
				BillingCategoryName = '',
				RowTotal = 0,
				ExistingRow = 'Y',
				'' AS LC1Name, 
				'' AS LC2Name, 
				'' AS LC3Name, 
				'' AS LC4Name, 
				'' AS LC5Name,
				EmployeeCompany = @activeCompany,
				Status = '',
				Item_UID1 = NULL, StatusCode1 = NULL,
				Item_UID2 = NULL, StatusCode2 = NULL,
				Item_UID3 = NULL, StatusCode3 = NULL,
				Item_UID4 = NULL, StatusCode4 = NULL,
				Item_UID5 = NULL, StatusCode5 = NULL,
				Item_UID6 = NULL, StatusCode6 = NULL,
				Item_UID7 = NULL, StatusCode7 = NULL,
				Item_UID8 = NULL, StatusCode8 = NULL,
				Item_UID9 = NULL, StatusCode9 = NULL,
				Item_UID10 = NULL, StatusCode10 = NULL,
				Item_UID11 = NULL, StatusCode11 = NULL,
				Item_UID12 = NULL, StatusCode12 = NULL,
				Item_UID13 = NULL, StatusCode13 = NULL,
				Item_UID14 = NULL, StatusCode14 = NULL,
				Item_UID15 = NULL, StatusCode15 = NULL,
				Item_UID16 = NULL, StatusCode16 = NULL,
				Item_UID17 = NULL, StatusCode17 = NULL,
				Item_UID18 = NULL, StatusCode18 = NULL,
				Item_UID19 = NULL, StatusCode19 = NULL,
				Item_UID20 = NULL, StatusCode20 = NULL,
				Item_UID21 = NULL, StatusCode21 = NULL,
				Item_UID22 = NULL, StatusCode22 = NULL,
				Item_UID23 = NULL, StatusCode23 = NULL,
				Item_UID24 = NULL, StatusCode24 = NULL,
				Item_UID25 = NULL, StatusCode25 = NULL,
				Item_UID26 = NULL, StatusCode26 = NULL,
				Item_UID27 = NULL, StatusCode27 = NULL,
				Item_UID28 = NULL, StatusCode28 = NULL,
				Item_UID29 = NULL, StatusCode29 = NULL,
				Item_UID30 = NULL, StatusCode30 = NULL,
				Item_UID31 = NULL, StatusCode31 = NULL,
				LineApprovalStatus = 'N',
				calStartDateTime1 = NULL, calEndDateTime1 = NULL,
				calStartDateTime2 = NULL, calEndDateTime2 = NULL,
				calStartDateTime3 = NULL, calEndDateTime3 = NULL,
				calStartDateTime4 = NULL, calEndDateTime4 = NULL,
				calStartDateTime5 = NULL, calEndDateTime5 = NULL,
				calStartDateTime6 = NULL, calEndDateTime6 = NULL,
				calStartDateTime7 = NULL, calEndDateTime7 = NULL,
				calStartDateTime8 = NULL, calEndDateTime8 = NULL,
				calStartDateTime9 = NULL, calEndDateTime9 = NULL,
				calStartDateTime10 = NULL, calEndDateTime10 = NULL,
				calStartDateTime11 = NULL, calEndDateTime11 = NULL,
				calStartDateTime12 = NULL, calEndDateTime12 = NULL,
				calStartDateTime13 = NULL, calEndDateTime13 = NULL,
				calStartDateTime14 = NULL, calEndDateTime14 = NULL,
				calStartDateTime15 = NULL, calEndDateTime15 = NULL,
				calStartDateTime16 = NULL, calEndDateTime16 = NULL,
				calStartDateTime17 = NULL, calEndDateTime17 = NULL,
				calStartDateTime18 = NULL, calEndDateTime18 = NULL,
				calStartDateTime19 = NULL, calEndDateTime19 = NULL,
				calStartDateTime20 = NULL, calEndDateTime20 = NULL,
				calStartDateTime21 = NULL, calEndDateTime21 = NULL,
				calStartDateTime22 = NULL, calEndDateTime22 = NULL,
				calStartDateTime23 = NULL, calEndDateTime23 = NULL,
				calStartDateTime24 = NULL, calEndDateTime24 = NULL,
				calStartDateTime25 = NULL, calEndDateTime25 = NULL,
				calStartDateTime26 = NULL, calEndDateTime26 = NULL,
				calStartDateTime27 = NULL, calEndDateTime27 = NULL,
				calStartDateTime28 = NULL, calEndDateTime28 = NULL,
				calStartDateTime29 = NULL, calEndDateTime29 = NULL,
				calStartDateTime30 = NULL, calEndDateTime30 = NULL,
				calStartDateTime31 = NULL, calEndDateTime31 = NULL, 
                CustTKUD01=NULL, 
                CustTKUD02=0, 
                CustTKUD03=NULL                
			FROM
				CFGTKCategory TKC
			WHERE
				TKC.TKGroup = @MasterTKGroup
				AND TKC.Company = @activeCompany
			ORDER BY
				isCategory Desc, Seq
			/* ^^^^^ Generate special category records if no tkDetail record ^^^^^ */
		END
	ELSE	/* has tkDetail records for @Employee and @EndDate */
		BEGIN
			declare @key nvarchar(255)
			select @key = @Employee+'|'+CONVERT(varchar(10),@EndDate,23)+'|'+@activeCompany

			SELECT Item_UID, ApplicationKey, ItemParent_UID INTO #ApprovalItem FROM (
			SELECT Item_UID, ApplicationKey, ItemParent_UID, StatusCode, ItemType_UID, IsClosed, row_number() OVER(PARTITION BY ApplicationKey ORDER BY CreateDate DESC) AS rn 
			FROM ApprovalItem 
			WHERE @UseApprovalWorkflow = 'Y' AND ApplicationID = 'TIME' AND ApprovalLevel = 'Detail' AND ItemType_UID <> 'SYSTEM:ASSIGNTO' 
			AND SUBSTRING(ApplicationKey,17,255) = @key
			) x
			WHERE rn = 1

			/* vvvvv Generate special category records if has no tkDetail records vvvvv */
			SELECT 
				SortOrder = 0,
				@MasterTKGroup as TKGroup,
				Employee = @Employee,
				StartDate = CONVERT(VARCHAR(10),@StartDate,121),
				EndDate = CONVERT(VARCHAR(10),@EndDate,121),
				Seq = SortSeq,
				TKC.Category As Category,
				isCategory = 'Y',
				TKC.Description As WBS1, TKC.Description As WBS1Name, '' As ClientName, 'N' As WBS2Level,
				'' As ChargeType,
				' ' As WBS2, '' As WBS2Name, 'N' As WBS3Level,
				' ' As WBS3, '' As WBS3Name,
				'' As LaborCode, '' As LCName, 0 As BillCategory,
			
				R_day1 = 0, O_day1 = 0, S_day1 = 0, T_day1 = 0, TransComment_day1 = NULL, OrigDetailSeq_day1 = -1, LineItemApprovedBy1 = Null, StartTime1 = Null, EndTime1 = NULL, 
				R_day2 = 0, O_day2 = 0, S_day2 = 0, T_day2 = 0, TransComment_day2 = NULL, OrigDetailSeq_day2 = -1, LineItemApprovedBy2 = Null, StartTime2 = Null, EndTime2 = NULL, 
				R_day3 = 0, O_day3 = 0, S_day3 = 0, T_day3 = 0, TransComment_day3 = NULL, OrigDetailSeq_day3 = -1, LineItemApprovedBy3 = Null, StartTime3 = Null, EndTime3 = NULL, 
				R_day4 = 0, O_day4 = 0, S_day4 = 0, T_day4 = 0, TransComment_day4 = NULL, OrigDetailSeq_day4 = -1, LineItemApprovedBy4 = Null, StartTime4 = Null, EndTime4 = NULL, 
				R_day5 = 0, O_day5 = 0, S_day5 = 0, T_day5 = 0, TransComment_day5 = NULL, OrigDetailSeq_day5 = -1, LineItemApprovedBy5 = Null, StartTime5 = Null, EndTime5 = NULL, 
				R_day6 = 0, O_day6 = 0, S_day6 = 0, T_day6 = 0, TransComment_day6 = NULL, OrigDetailSeq_day6 = -1, LineItemApprovedBy6 = Null, StartTime6 = Null, EndTime6 = NULL, 
				R_day7 = 0, O_day7 = 0, S_day7 = 0, T_day7 = 0, TransComment_day7 = NULL, OrigDetailSeq_day7 = -1, LineItemApprovedBy7 = Null, StartTime7 = Null, EndTime7 = NULL, 
				R_day8 = 0, O_day8 = 0, S_day8 = 0, T_day8 = 0, TransComment_day8 = NULL, OrigDetailSeq_day8 = -1, LineItemApprovedBy8 = Null, StartTime8 = Null, EndTime8 = NULL, 
				R_day9 = 0, O_day9 = 0, S_day9 = 0, T_day9 = 0, TransComment_day9 = NULL, OrigDetailSeq_day9 = -1, LineItemApprovedBy9 = Null, StartTime9 = Null, EndTime9 = NULL, 
				R_day10 = 0, O_day10 = 0, S_day10 = 0, T_day10 = 0, TransComment_day10 = NULL, OrigDetailSeq_day10 = -1, LineItemApprovedBy10 = Null, StartTime10 = Null, EndTime10 = NULL, 
				R_day11 = 0, O_day11 = 0, S_day11 = 0, T_day11 = 0, TransComment_day11 = NULL, OrigDetailSeq_day11 = -1, LineItemApprovedBy11 = Null, StartTime11 = Null, EndTime11 = NULL, 
				R_day12 = 0, O_day12 = 0, S_day12 = 0, T_day12 = 0, TransComment_day12 = NULL, OrigDetailSeq_day12 = -1, LineItemApprovedBy12 = Null, StartTime12 = Null, EndTime12 = NULL, 
				R_day13 = 0, O_day13 = 0, S_day13 = 0, T_day13 = 0, TransComment_day13 = NULL, OrigDetailSeq_day13 = -1, LineItemApprovedBy13 = Null, StartTime13 = Null, EndTime13 = NULL, 
				R_day14 = 0, O_day14 = 0, S_day14 = 0, T_day14 = 0, TransComment_day14 = NULL, OrigDetailSeq_day14 = -1, LineItemApprovedBy14 = Null, StartTime14 = Null, EndTime14 = NULL, 
				R_day15 = 0, O_day15 = 0, S_day15 = 0, T_day15 = 0, TransComment_day15 = NULL, OrigDetailSeq_day15 = -1, LineItemApprovedBy15 = Null, StartTime15 = Null, EndTime15 = NULL, 
				R_day16 = 0, O_day16 = 0, S_day16 = 0, T_day16 = 0, TransComment_day16 = NULL, OrigDetailSeq_day16 = -1, LineItemApprovedBy16 = Null, StartTime16 = Null, EndTime16 = NULL, 
				R_day17 = 0, O_day17 = 0, S_day17 = 0, T_day17 = 0, TransComment_day17 = NULL, OrigDetailSeq_day17 = -1, LineItemApprovedBy17 = Null, StartTime17 = Null, EndTime17 = NULL, 
				R_day18 = 0, O_day18 = 0, S_day18 = 0, T_day18 = 0, TransComment_day18 = NULL, OrigDetailSeq_day18 = -1, LineItemApprovedBy18 = Null, StartTime18 = Null, EndTime18 = NULL, 
				R_day19 = 0, O_day19 = 0, S_day19 = 0, T_day19 = 0, TransComment_day19 = NULL, OrigDetailSeq_day19 = -1, LineItemApprovedBy19 = Null, StartTime19 = Null, EndTime19 = NULL, 
				R_day20 = 0, O_day20 = 0, S_day20 = 0, T_day20 = 0, TransComment_day20 = NULL, OrigDetailSeq_day20 = -1, LineItemApprovedBy20 = Null, StartTime20 = Null, EndTime20 = NULL, 
				R_day21 = 0, O_day21 = 0, S_day21 = 0, T_day21 = 0, TransComment_day21 = NULL, OrigDetailSeq_day21 = -1, LineItemApprovedBy21 = Null, StartTime21 = Null, EndTime21 = NULL, 
				R_day22 = 0, O_day22 = 0, S_day22 = 0, T_day22 = 0, TransComment_day22 = NULL, OrigDetailSeq_day22 = -1, LineItemApprovedBy22 = Null, StartTime22 = Null, EndTime22 = NULL, 
				R_day23 = 0, O_day23 = 0, S_day23 = 0, T_day23 = 0, TransComment_day23 = NULL, OrigDetailSeq_day23 = -1, LineItemApprovedBy23 = Null, StartTime23 = Null, EndTime23 = NULL, 
				R_day24 = 0, O_day24 = 0, S_day24 = 0, T_day24 = 0, TransComment_day24 = NULL, OrigDetailSeq_day24 = -1, LineItemApprovedBy24 = Null, StartTime24 = Null, EndTime24 = NULL, 
				R_day25 = 0, O_day25 = 0, S_day25 = 0, T_day25 = 0, TransComment_day25 = NULL, OrigDetailSeq_day25 = -1, LineItemApprovedBy25 = Null, StartTime25 = Null, EndTime25 = NULL, 
				R_day26 = 0, O_day26 = 0, S_day26 = 0, T_day26 = 0, TransComment_day26 = NULL, OrigDetailSeq_day26 = -1, LineItemApprovedBy26 = Null, StartTime26 = Null, EndTime26 = NULL, 
				R_day27 = 0, O_day27 = 0, S_day27 = 0, T_day27 = 0, TransComment_day27 = NULL, OrigDetailSeq_day27 = -1, LineItemApprovedBy27 = Null, StartTime27 = Null, EndTime27 = NULL, 
				R_day28 = 0, O_day28 = 0, S_day28 = 0, T_day28 = 0, TransComment_day28 = NULL, OrigDetailSeq_day28 = -1, LineItemApprovedBy28 = Null, StartTime28 = Null, EndTime28 = NULL, 
				R_day29 = 0, O_day29 = 0, S_day29 = 0, T_day29 = 0, TransComment_day29 = NULL, OrigDetailSeq_day29 = -1, LineItemApprovedBy29 = Null, StartTime29 = Null, EndTime29 = NULL, 
				R_day30 = 0, O_day30 = 0, S_day30 = 0, T_day30 = 0, TransComment_day30 = NULL, OrigDetailSeq_day30 = -1, LineItemApprovedBy30 = Null, StartTime30 = Null, EndTime30 = NULL, 
				R_day31 = 0, O_day31 = 0, S_day31 = 0, T_day31 = 0, TransComment_day31 = NULL, OrigDetailSeq_day31 = -1, LineItemApprovedBy31 = Null, StartTime31 = Null, EndTime31 = NULL, 
				RowRegular = 0, RowOvt = 0, RowOvt2 = 0,
				LineItemWBSLock = 'Y',
				BudgetedFlag = 'N',
				BudgetedLevels = '',
				BudgetSource = '',
				BudgetLevel = '',
				LineItemApproval = 'N',
				Locale = '',
				WBS1Locale = '',
				WBS2Locale = '',
				WBS3Locale = '',
				TKCheckRPDate = 'N',
				RequireComments = 'C',
				TKCAllowStartEndTime = TKC.AllowStartEndTime,
				BillingCategoryName = '',
				RowTotal = 0,
				ExistingRow = 'Y',
				'' AS LC1Name, 
				'' AS LC2Name, 
				'' AS LC3Name, 
				'' AS LC4Name, 
				'' AS LC5Name,
				EmployeeCompany = @activeCompany,
				Status = '',
				Item_UID1 = NULL, StatusCode1 = NULL,
				Item_UID2 = NULL, StatusCode2 = NULL,
				Item_UID3 = NULL, StatusCode3 = NULL,
				Item_UID4 = NULL, StatusCode4 = NULL,
				Item_UID5 = NULL, StatusCode5 = NULL,
				Item_UID6 = NULL, StatusCode6 = NULL,
				Item_UID7 = NULL, StatusCode7 = NULL,
				Item_UID8 = NULL, StatusCode8 = NULL,
				Item_UID9 = NULL, StatusCode9 = NULL,
				Item_UID10 = NULL, StatusCode10 = NULL,
				Item_UID11 = NULL, StatusCode11 = NULL,
				Item_UID12 = NULL, StatusCode12 = NULL,
				Item_UID13 = NULL, StatusCode13 = NULL,
				Item_UID14 = NULL, StatusCode14 = NULL,
				Item_UID15 = NULL, StatusCode15 = NULL,
				Item_UID16 = NULL, StatusCode16 = NULL,
				Item_UID17 = NULL, StatusCode17 = NULL,
				Item_UID18 = NULL, StatusCode18 = NULL,
				Item_UID19 = NULL, StatusCode19 = NULL,
				Item_UID20 = NULL, StatusCode20 = NULL,
				Item_UID21 = NULL, StatusCode21 = NULL,
				Item_UID22 = NULL, StatusCode22 = NULL,
				Item_UID23 = NULL, StatusCode23 = NULL,
				Item_UID24 = NULL, StatusCode24 = NULL,
				Item_UID25 = NULL, StatusCode25 = NULL,
				Item_UID26 = NULL, StatusCode26 = NULL,
				Item_UID27 = NULL, StatusCode27 = NULL,
				Item_UID28 = NULL, StatusCode28 = NULL,
				Item_UID29 = NULL, StatusCode29 = NULL,
				Item_UID30 = NULL, StatusCode30 = NULL,
				Item_UID31 = NULL, StatusCode31 = NULL,
				LineApprovalStatus = 'N',
				calStartDateTime1 = NULL, calEndDateTime1 = NULL,
				calStartDateTime2 = NULL, calEndDateTime2 = NULL,
				calStartDateTime3 = NULL, calEndDateTime3 = NULL,
				calStartDateTime4 = NULL, calEndDateTime4 = NULL,
				calStartDateTime5 = NULL, calEndDateTime5 = NULL,
				calStartDateTime6 = NULL, calEndDateTime6 = NULL,
				calStartDateTime7 = NULL, calEndDateTime7 = NULL,
				calStartDateTime8 = NULL, calEndDateTime8 = NULL,
				calStartDateTime9 = NULL, calEndDateTime9 = NULL,
				calStartDateTime10 = NULL, calEndDateTime10 = NULL,
				calStartDateTime11 = NULL, calEndDateTime11 = NULL,
				calStartDateTime12 = NULL, calEndDateTime12 = NULL,
				calStartDateTime13 = NULL, calEndDateTime13 = NULL,
				calStartDateTime14 = NULL, calEndDateTime14 = NULL,
				calStartDateTime15 = NULL, calEndDateTime15 = NULL,
				calStartDateTime16 = NULL, calEndDateTime16 = NULL,
				calStartDateTime17 = NULL, calEndDateTime17 = NULL,
				calStartDateTime18 = NULL, calEndDateTime18 = NULL,
				calStartDateTime19 = NULL, calEndDateTime19 = NULL,
				calStartDateTime20 = NULL, calEndDateTime20 = NULL,
				calStartDateTime21 = NULL, calEndDateTime21 = NULL,
				calStartDateTime22 = NULL, calEndDateTime22 = NULL,
				calStartDateTime23 = NULL, calEndDateTime23 = NULL,
				calStartDateTime24 = NULL, calEndDateTime24 = NULL,
				calStartDateTime25 = NULL, calEndDateTime25 = NULL,
				calStartDateTime26 = NULL, calEndDateTime26 = NULL,
				calStartDateTime27 = NULL, calEndDateTime27 = NULL,
				calStartDateTime28 = NULL, calEndDateTime28 = NULL,
				calStartDateTime29 = NULL, calEndDateTime29 = NULL,
				calStartDateTime30 = NULL, calEndDateTime30 = NULL,
				calStartDateTime31 = NULL, calEndDateTime31 = NULL, 
                CustTKUD01=NULL, 
                CustTKUD02=0, 
                CustTKUD03=NULL
			FROM
				CFGTKCategory TKC
				LEFT OUTER JOIN (SELECT DISTINCT Category FROM tkDetail WHERE Employee = @Employee AND tkDetail.EndDate = @EndDate AND tkDetail.EmployeeCompany = @activeCompany AND (Category IS NOT NULL OR Category = N' ')) x
				ON TKC.Category = x.Category
			WHERE
				TKC.TKGroup = @MasterTKGroup
				AND X.Category IS NULL
				AND TKC.Company = @activeCompany
			/* ^^^^^ Generate special category records if has no tkDetail records ^^^^^ */
		
			/* vvvvv Get special category tkDetail records vvvvv */
			UNION ALL
			SELECT 
				SortOrder, TKGroup, Employee, StartDate, EndDate, Seq, Category, isCategory,				
                WBS1, WBS1Name, ClientName, WBS2Level, ChargeType, WBS2, WBS2Name, WBS3Level, WBS3, WBS3Name,
				LaborCode, LCName, BillCategory,

				R_day1, O_day1, S_day1, T_day1, TransComment_day1, OrigDetailSeq_day1, LineItemApprovedBy1, StartTime1, EndTime1,
				R_day2, O_day2, S_day2, T_day2, TransComment_day2, OrigDetailSeq_day2, LineItemApprovedBy2, StartTime2, EndTime2,
				R_day3, O_day3, S_day3, T_day3, TransComment_day3, OrigDetailSeq_day3, LineItemApprovedBy3, StartTime3, EndTime3,
				R_day4, O_day4, S_day4, T_day4, TransComment_day4, OrigDetailSeq_day4, LineItemApprovedBy4, StartTime4, EndTime4,
				R_day5, O_day5, S_day5, T_day5, TransComment_day5, OrigDetailSeq_day5, LineItemApprovedBy5, StartTime5, EndTime5,
				R_day6, O_day6, S_day6, T_day6, TransComment_day6, OrigDetailSeq_day6, LineItemApprovedBy6, StartTime6, EndTime6,
				R_day7, O_day7, S_day7, T_day7, TransComment_day7, OrigDetailSeq_day7, LineItemApprovedBy7, StartTime7, EndTime7,
				R_day8, O_day8, S_day8, T_day8, TransComment_day8, OrigDetailSeq_day8, LineItemApprovedBy8, StartTime8, EndTime8,
				R_day9, O_day9, S_day9, T_day9, TransComment_day9, OrigDetailSeq_day9, LineItemApprovedBy9, StartTime9, EndTime9,
				R_day10, O_day10, S_day10, T_day10, TransComment_day10, OrigDetailSeq_day10, LineItemApprovedBy10, StartTime10, EndTime10,
				R_day11, O_day11, S_day11, T_day11, TransComment_day11, OrigDetailSeq_day11, LineItemApprovedBy11, StartTime11, EndTime11,
				R_day12, O_day12, S_day12, T_day12, TransComment_day12, OrigDetailSeq_day12, LineItemApprovedBy12, StartTime12, EndTime12,
				R_day13, O_day13, S_day13, T_day13, TransComment_day13, OrigDetailSeq_day13, LineItemApprovedBy13, StartTime13, EndTime13,
				R_day14, O_day14, S_day14, T_day14, TransComment_day14, OrigDetailSeq_day14, LineItemApprovedBy14, StartTime14, EndTime14,
				R_day15, O_day15, S_day15, T_day15, TransComment_day15, OrigDetailSeq_day15, LineItemApprovedBy15, StartTime15, EndTime15,
				R_day16, O_day16, S_day16, T_day16, TransComment_day16, OrigDetailSeq_day16, LineItemApprovedBy16, StartTime16, EndTime16,
				R_day17, O_day17, S_day17, T_day17, TransComment_day17, OrigDetailSeq_day17, LineItemApprovedBy17, StartTime17, EndTime17, 
				R_day18, O_day18, S_day18, T_day18, TransComment_day18, OrigDetailSeq_day18, LineItemApprovedBy18, StartTime18, EndTime18,
				R_day19, O_day19, S_day19, T_day19, TransComment_day19, OrigDetailSeq_day19, LineItemApprovedBy19, StartTime19, EndTime19,
				R_day20, O_day20, S_day20, T_day20, TransComment_day20, OrigDetailSeq_day20, LineItemApprovedBy20, StartTime20, EndTime20,
				R_day21, O_day21, S_day21, T_day21, TransComment_day21, OrigDetailSeq_day21, LineItemApprovedBy21, StartTime21, EndTime21,
				R_day22, O_day22, S_day22, T_day22, TransComment_day22, OrigDetailSeq_day22, LineItemApprovedBy22, StartTime22, EndTime22,
				R_day23, O_day23, S_day23, T_day23, TransComment_day23, OrigDetailSeq_day23, LineItemApprovedBy23, StartTime23, EndTime23,
				R_day24, O_day24, S_day24, T_day24, TransComment_day24, OrigDetailSeq_day24, LineItemApprovedBy24, StartTime24, EndTime24,
				R_day25, O_day25, S_day25, T_day25, TransComment_day25, OrigDetailSeq_day25, LineItemApprovedBy25, StartTime25, EndTime25,
				R_day26, O_day26, S_day26, T_day26, TransComment_day26, OrigDetailSeq_day26, LineItemApprovedBy26, StartTime26, EndTime26,
				R_day27, O_day27, S_day27, T_day27, TransComment_day27, OrigDetailSeq_day27, LineItemApprovedBy27, StartTime27, EndTime27,
				R_day28, O_day28, S_day28, T_day28, TransComment_day28, OrigDetailSeq_day28, LineItemApprovedBy28, StartTime28, EndTime28,
				R_day29, O_day29, S_day29, T_day29, TransComment_day29, OrigDetailSeq_day29, LineItemApprovedBy29, StartTime29, EndTime29,
				R_day30, O_day30, S_day30, T_day30, TransComment_day30, OrigDetailSeq_day30, LineItemApprovedBy30, StartTime30, EndTime30,
				R_day31, O_day31, S_day31, T_day31, TransComment_day31, OrigDetailSeq_day31, LineItemApprovedBy31, StartTime31, EndTime31,

				RowRegular, RowOvt,	RowOvt2, LineItemWBSLock,
				BudgetedFlag, BudgetedLevels, BudgetSource, BudgetLevel, LineItemApproval,
				Locale, WBS1Locale, WBS2Locale, WBS3Locale,
				TKCheckRPDate, RequireComments, TKCAllowStartEndTime, BillingCategoryName, RowTotal, ExistingRow,
				LC1Name, LC2Name, LC3Name, LC4Name, LC5Name, EmployeeCompany, Status,

				Item_UID1, StatusCode1 = NULL,
				Item_UID2, StatusCode2 = NULL,
				Item_UID3, StatusCode3 = NULL,
				Item_UID4, StatusCode4 = NULL,
				Item_UID5, StatusCode5 = NULL,
				Item_UID6, StatusCode6 = NULL,
				Item_UID7, StatusCode7 = NULL,
				Item_UID8, StatusCode8 = NULL,
				Item_UID9, StatusCode9 = NULL,
				Item_UID10, StatusCode10 = NULL,
				Item_UID11, StatusCode11 = NULL,
				Item_UID12, StatusCode12 = NULL,
				Item_UID13, StatusCode13 = NULL,
				Item_UID14, StatusCode14 = NULL,
				Item_UID15, StatusCode15 = NULL,
				Item_UID16, StatusCode16 = NULL,
				Item_UID17, StatusCode17 = NULL,
				Item_UID18, StatusCode18 = NULL,
				Item_UID19, StatusCode19 = NULL,
				Item_UID20, StatusCode20 = NULL,
				Item_UID21, StatusCode21 = NULL,
				Item_UID22, StatusCode22 = NULL,
				Item_UID23, StatusCode23 = NULL,
				Item_UID24, StatusCode24 = NULL,
				Item_UID25, StatusCode25 = NULL,
				Item_UID26, StatusCode26 = NULL,
				Item_UID27, StatusCode27 = NULL,
				Item_UID28, StatusCode28 = NULL,
				Item_UID29, StatusCode29 = NULL,
				Item_UID30, StatusCode30 = NULL,
				Item_UID31, StatusCode31 = NULL,
				LineApprovalStatus = 
				CASE WHEN @tkMasterStatus = 'N' THEN 'N'
				WHEN (LineItemApprovalStatus1+LineItemApprovalStatus2+LineItemApprovalStatus3+LineItemApprovalStatus4+LineItemApprovalStatus5
				+LineItemApprovalStatus6+LineItemApprovalStatus7+LineItemApprovalStatus8+LineItemApprovalStatus9+LineItemApprovalStatus10
				+LineItemApprovalStatus11+LineItemApprovalStatus12+LineItemApprovalStatus13+LineItemApprovalStatus14+LineItemApprovalStatus15
				+LineItemApprovalStatus16+LineItemApprovalStatus17+LineItemApprovalStatus18+LineItemApprovalStatus19+LineItemApprovalStatus20
				+LineItemApprovalStatus21+LineItemApprovalStatus22+LineItemApprovalStatus23+LineItemApprovalStatus24+LineItemApprovalStatus25
				+LineItemApprovalStatus26+LineItemApprovalStatus27+LineItemApprovalStatus28+LineItemApprovalStatus29+LineItemApprovalStatus30+LineItemApprovalStatus31) BETWEEN 1 AND 99 THEN 'A'
				WHEN (LineItemApprovalStatus1+LineItemApprovalStatus2+LineItemApprovalStatus3+LineItemApprovalStatus4+LineItemApprovalStatus5
				+LineItemApprovalStatus6+LineItemApprovalStatus7+LineItemApprovalStatus8+LineItemApprovalStatus9+LineItemApprovalStatus10
				+LineItemApprovalStatus11+LineItemApprovalStatus12+LineItemApprovalStatus13+LineItemApprovalStatus14+LineItemApprovalStatus15
				+LineItemApprovalStatus16+LineItemApprovalStatus17+LineItemApprovalStatus18+LineItemApprovalStatus19+LineItemApprovalStatus20
				+LineItemApprovalStatus21+LineItemApprovalStatus22+LineItemApprovalStatus23+LineItemApprovalStatus24+LineItemApprovalStatus25
				+LineItemApprovalStatus26+LineItemApprovalStatus27+LineItemApprovalStatus28+LineItemApprovalStatus29+LineItemApprovalStatus30+LineItemApprovalStatus31) >= 10000 THEN 'J'
				WHEN (LineItemApprovalStatus1+LineItemApprovalStatus2+LineItemApprovalStatus3+LineItemApprovalStatus4+LineItemApprovalStatus5
				+LineItemApprovalStatus6+LineItemApprovalStatus7+LineItemApprovalStatus8+LineItemApprovalStatus9+LineItemApprovalStatus10
				+LineItemApprovalStatus11+LineItemApprovalStatus12+LineItemApprovalStatus13+LineItemApprovalStatus14+LineItemApprovalStatus15
				+LineItemApprovalStatus16+LineItemApprovalStatus17+LineItemApprovalStatus18+LineItemApprovalStatus19+LineItemApprovalStatus20
				+LineItemApprovalStatus21+LineItemApprovalStatus22+LineItemApprovalStatus23+LineItemApprovalStatus24+LineItemApprovalStatus25
				+LineItemApprovalStatus26+LineItemApprovalStatus27+LineItemApprovalStatus28+LineItemApprovalStatus29+LineItemApprovalStatus30+LineItemApprovalStatus31) = 0 THEN 'N'
				ELSE 'Y'
				END,
				calStartDateTime1, calEndDateTime1,
				calStartDateTime2, calEndDateTime2,
				calStartDateTime3, calEndDateTime3,
				calStartDateTime4, calEndDateTime4,
				calStartDateTime5, calEndDateTime5,
				calStartDateTime6, calEndDateTime6,
				calStartDateTime7, calEndDateTime7,
				calStartDateTime8, calEndDateTime8,
				calStartDateTime9, calEndDateTime9,
				calStartDateTime10, calEndDateTime10,
				calStartDateTime11, calEndDateTime11,
				calStartDateTime12, calEndDateTime12,
				calStartDateTime13, calEndDateTime13,
				calStartDateTime14, calEndDateTime14,
				calStartDateTime15, calEndDateTime15,
				calStartDateTime16, calEndDateTime16,
				calStartDateTime17, calEndDateTime17,
				calStartDateTime18, calEndDateTime18,
				calStartDateTime19, calEndDateTime19,
				calStartDateTime20, calEndDateTime20,
				calStartDateTime21, calEndDateTime21,
				calStartDateTime22, calEndDateTime22,
				calStartDateTime23, calEndDateTime23,
				calStartDateTime24, calEndDateTime24,
				calStartDateTime25, calEndDateTime25,
				calStartDateTime26, calEndDateTime26,
				calStartDateTime27, calEndDateTime27,
				calStartDateTime28, calEndDateTime28,
				calStartDateTime29, calEndDateTime29,
				calStartDateTime30, calEndDateTime30,
				calStartDateTime31, calEndDateTime31, 
                CustTKUD01=NULL, 
                CustTKUD02=0,
                CustTKUD03=NULL 
			 FROM
			(SELECT 
				SortOrder = 0,
				@MasterTKGroup as TKGroup,
				Employee = @Employee,
				StartDate = CONVERT(VARCHAR(10),@StartDate,121),
				EndDate = CONVERT(VARCHAR(10),@EndDate,121),
				Seq = CFGTKCategory.SortSeq,
				tkDetail.Category, isCategory = 'Y',
				CFGTKCategory.Description As WBS1, CFGTKCategory.Description As WBS1Name, '' As ClientName, 'N' As WBS2Level,
				MAX(PR1.ChargeType) As ChargeType,
				' ' As WBS2, '' As WBS2Name, 'N' As WBS3Level,
				' ' As WBS3, '' As WBS3Name,
				'' As LaborCode, '' As LCName, tkDetail.BillCategory,
			
				R_day1 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN RegHrs ELSE 0 END)),
				O_day1 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN OvtHrs ELSE 0 END)),
				S_day1 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN SpecialOvtHrs ELSE 0 END)),
				T_day1 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN #ApprovalItem.Item_UID ELSE NULL END),

				R_day2 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN RegHrs ELSE 0 END)),
				O_day2 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN OvtHrs ELSE 0 END)),
				S_day2 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN SpecialOvtHrs ELSE 0 END)),
				T_day2 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day3 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN RegHrs ELSE 0 END)),
				O_day3 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN OvtHrs ELSE 0 END)),
				S_day3 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN SpecialOvtHrs ELSE 0 END)),
				T_day3 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day4 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN RegHrs ELSE 0 END)),
				O_day4 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN OvtHrs ELSE 0 END)),
				S_day4 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN SpecialOvtHrs ELSE 0 END)),
				T_day4 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day5 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN RegHrs ELSE 0 END)),
				O_day5 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN OvtHrs ELSE 0 END)),
				S_day5 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN SpecialOvtHrs ELSE 0 END)),
				T_day5 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day6 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN RegHrs ELSE 0 END)),
				O_day6 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN OvtHrs ELSE 0 END)),
				S_day6 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN SpecialOvtHrs ELSE 0 END)),
				T_day6 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day7 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN RegHrs ELSE 0 END)),
				O_day7 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN OvtHrs ELSE 0 END)),
				S_day7 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN SpecialOvtHrs ELSE 0 END)),
				T_day7 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day8 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN RegHrs ELSE 0 END)),
				O_day8 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN OvtHrs ELSE 0 END)),
				S_day8 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN SpecialOvtHrs ELSE 0 END)),
				T_day8 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day9 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN RegHrs ELSE 0 END)),
				O_day9 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN OvtHrs ELSE 0 END)),
				S_day9 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN SpecialOvtHrs ELSE 0 END)),
				T_day9 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day10 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN RegHrs ELSE 0 END)),
				O_day10 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN OvtHrs ELSE 0 END)),
				S_day10 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN SpecialOvtHrs ELSE 0 END)),
				T_day10 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day11 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN RegHrs ELSE 0 END)),
				O_day11 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN OvtHrs ELSE 0 END)),
				S_day11 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN SpecialOvtHrs ELSE 0 END)),
				T_day11 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day12 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN RegHrs ELSE 0 END)),
				O_day12 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN OvtHrs ELSE 0 END)),
				S_day12 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN SpecialOvtHrs ELSE 0 END)),
				T_day12 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day13 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN RegHrs ELSE 0 END)),
				O_day13 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN OvtHrs ELSE 0 END)),
				S_day13 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN SpecialOvtHrs ELSE 0 END)),
				T_day13 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day14 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN RegHrs ELSE 0 END)),
				O_day14 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN OvtHrs ELSE 0 END)),
				S_day14 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN SpecialOvtHrs ELSE 0 END)),
				T_day14 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day15 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN RegHrs ELSE 0 END)),
				O_day15 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN OvtHrs ELSE 0 END)),
				S_day15 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN SpecialOvtHrs ELSE 0 END)),
				T_day15 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day16 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN RegHrs ELSE 0 END)),
				O_day16 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN OvtHrs ELSE 0 END)),
				S_day16 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN SpecialOvtHrs ELSE 0 END)),
				T_day16 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day17 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN RegHrs ELSE 0 END)),
				O_day17 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN OvtHrs ELSE 0 END)),
				S_day17 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN SpecialOvtHrs ELSE 0 END)),
				T_day17 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day18 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN RegHrs ELSE 0 END)),
				O_day18 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN OvtHrs ELSE 0 END)),
				S_day18 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN SpecialOvtHrs ELSE 0 END)),
				T_day18 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day19 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN RegHrs ELSE 0 END)),
				O_day19 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN OvtHrs ELSE 0 END)),
				S_day19 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN SpecialOvtHrs ELSE 0 END)),
				T_day19 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day20 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN RegHrs ELSE 0 END)),
				O_day20 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN OvtHrs ELSE 0 END)),
				S_day20 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN SpecialOvtHrs ELSE 0 END)),
				T_day20 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day21 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN RegHrs ELSE 0 END)),
				O_day21 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN OvtHrs ELSE 0 END)),
				S_day21 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN SpecialOvtHrs ELSE 0 END)),
				T_day21 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day22 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN RegHrs ELSE 0 END)),
				O_day22 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN OvtHrs ELSE 0 END)),
				S_day22 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN SpecialOvtHrs ELSE 0 END)),
				T_day22 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day23 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN RegHrs ELSE 0 END)),
				O_day23 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN OvtHrs ELSE 0 END)),
				S_day23 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN SpecialOvtHrs ELSE 0 END)),
				T_day23 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day24 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN RegHrs ELSE 0 END)),
				O_day24 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN OvtHrs ELSE 0 END)),
				S_day24 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN SpecialOvtHrs ELSE 0 END)),
				T_day24 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day25 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN RegHrs ELSE 0 END)),
				O_day25 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN OvtHrs ELSE 0 END)),
				S_day25 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN SpecialOvtHrs ELSE 0 END)),
				T_day25 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day26 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN RegHrs ELSE 0 END)),
				O_day26 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN OvtHrs ELSE 0 END)),
				S_day26 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN SpecialOvtHrs ELSE 0 END)),
				T_day26 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day27 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN RegHrs ELSE 0 END)),
				O_day27 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN OvtHrs ELSE 0 END)),
				S_day27 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN SpecialOvtHrs ELSE 0 END)),
				T_day27 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day28 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN RegHrs ELSE 0 END)),
				O_day28 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN OvtHrs ELSE 0 END)),
				S_day28 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN SpecialOvtHrs ELSE 0 END)),
				T_day28 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day29 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN RegHrs ELSE 0 END)),
				O_day29 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN OvtHrs ELSE 0 END)),
				S_day29 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN SpecialOvtHrs ELSE 0 END)),
				T_day29 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day30 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN RegHrs ELSE 0 END)),
				O_day30 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN OvtHrs ELSE 0 END)),
				S_day30 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN SpecialOvtHrs ELSE 0 END)),
				T_day30 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day31 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN RegHrs ELSE 0 END)),
				O_day31 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN OvtHrs ELSE 0 END)),
				S_day31 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN SpecialOvtHrs ELSE 0 END)),
				T_day31 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN #ApprovalItem.Item_UID ELSE NULL END),

				RowRegular = CONVERT(DECIMAL(14,2),SUM(RegHrs)),
				RowOvt = CONVERT(DECIMAL(14,2),SUM(OvtHrs)),
				RowOvt2 = CONVERT(DECIMAL(14,2),SUM(SpecialOvtHrs)),
				LineItemWBSLock = 'Y',
				BudgetedFlag = 'N',
				BudgetedLevels = '',
				BudgetSource = '',
				BudgetLevel = '',
				MAX(ISNULL(PR1.LineItemApproval,'N')) As LineItemApproval,
				Locale = '',
				WBS1Locale = '',
				WBS2Locale = '',
				WBS3Locale = '',
				TKCheckRPDate = 'N',
				RequireComments = 'C',
				TKCAllowStartEndTime = MIN(CFGTKCategory.AllowStartEndTime),
				BillingCategoryName ='',
				RowTotal = CONVERT(DECIMAL(14,2),SUM(RegHrs)) + CONVERT(DECIMAL(14,2),SUM(OvtHrs)) + CONVERT(DECIMAL(14,2),SUM(SpecialOvtHrs)),
				ExistingRow = 'Y',
				'' AS LC1Name, 
				'' AS LC2Name, 
				'' AS LC3Name, 
				'' AS LC4Name, 
				'' AS LC5Name,
				EmployeeCompany = @activeCompany,
				Status = '',
				calStartDateTime1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END), 
                CustTKUD01=NULL, 
                CustTKUD02=0,
                CustTKUD03=NULL
			FROM
				tkDetail LEFT JOIN CFGTKCategory ON tkDetail.Category = CFGTKCategory.Category
				LEFT JOIN #ApprovalItem ON REPLACE(Str(tkDetail.Seq, 4), Space(1), '0') + '|' + CONVERT(VARCHAR, tkDetail.TransDate, 23) + '|' + tkDetail.Employee + '|' + CONVERT(VARCHAR, tkDetail.EndDate, 23) + '|' + tkDetail.EmployeeCompany = #ApprovalItem.ApplicationKey
				LEFT JOIN PR AS PR1 ON tkDetail.WBS1 = PR1.WBS1 AND PR1.WBS2 = N' ' AND PR1.WBS3 = N' '
			WHERE
				CFGTKCategory.TKGroup = @MasterTKGroup
				And tkDetail.Employee = @Employee
				And tkDetail.EndDate = @EndDate
				And tkDetail.EmployeeCompany = @activeCompany
				AND CFGTKCategory.Company = @activeCompany
			GROUP BY
				CFGTKCategory.Description, tkDetail.WBS2, tkDetail.WBS3, CFGTKCategory.SortSeq,
				tkDetail.Category, tkDetail.LaborCode, tkDetail.BillCategory
			/* ^^^^^ Get special category tkDetail records ^^^^^ */
			) _TimeCategory

			/* vvvvv Get non special category tkDetail records vvvvv */
			UNION ALL 
			SELECT 
				SortOrder, TKGroup, Employee = _Time.Employee, StartDate, 
				EndDate = _Time.EndDate, _Time.Seq, Category, isCategory,
				WBS1, WBS1Name, ClientName, WBS2Level, ChargeType, WBS2, WBS2Name, WBS3Level, WBS3, WBS3Name,
				LaborCode, LCName, BillCategory,

				R_day1, O_day1, S_day1, T_day1, TransComment_day1, OrigDetailSeq_day1, LineItemApprovedBy1, StartTime1, EndTime1,
				R_day2, O_day2, S_day2, T_day2, TransComment_day2, OrigDetailSeq_day2, LineItemApprovedBy2, StartTime2, EndTime2,
				R_day3, O_day3, S_day3, T_day3, TransComment_day3, OrigDetailSeq_day3, LineItemApprovedBy3, StartTime3, EndTime3,
				R_day4, O_day4, S_day4, T_day4, TransComment_day4, OrigDetailSeq_day4, LineItemApprovedBy4, StartTime4, EndTime4,
				R_day5, O_day5, S_day5, T_day5, TransComment_day5, OrigDetailSeq_day5, LineItemApprovedBy5, StartTime5, EndTime5,
				R_day6, O_day6, S_day6, T_day6, TransComment_day6, OrigDetailSeq_day6, LineItemApprovedBy6, StartTime6, EndTime6,
				R_day7, O_day7, S_day7, T_day7, TransComment_day7, OrigDetailSeq_day7, LineItemApprovedBy7, StartTime7, EndTime7,
				R_day8, O_day8, S_day8, T_day8, TransComment_day8, OrigDetailSeq_day8, LineItemApprovedBy8, StartTime8, EndTime8,
				R_day9, O_day9, S_day9, T_day9, TransComment_day9, OrigDetailSeq_day9, LineItemApprovedBy9, StartTime9, EndTime9,
				R_day10, O_day10, S_day10, T_day10, TransComment_day10, OrigDetailSeq_day10, LineItemApprovedBy10, StartTime10, EndTime10,
				R_day11, O_day11, S_day11, T_day11, TransComment_day11, OrigDetailSeq_day11, LineItemApprovedBy11, StartTime11, EndTime11,
				R_day12, O_day12, S_day12, T_day12, TransComment_day12, OrigDetailSeq_day12, LineItemApprovedBy12, StartTime12, EndTime12,
				R_day13, O_day13, S_day13, T_day13, TransComment_day13, OrigDetailSeq_day13, LineItemApprovedBy13, StartTime13, EndTime13,
				R_day14, O_day14, S_day14, T_day14, TransComment_day14, OrigDetailSeq_day14, LineItemApprovedBy14, StartTime14, EndTime14,
				R_day15, O_day15, S_day15, T_day15, TransComment_day15, OrigDetailSeq_day15, LineItemApprovedBy15, StartTime15, EndTime15,
				R_day16, O_day16, S_day16, T_day16, TransComment_day16, OrigDetailSeq_day16, LineItemApprovedBy16, StartTime16, EndTime16,
				R_day17, O_day17, S_day17, T_day17, TransComment_day17, OrigDetailSeq_day17, LineItemApprovedBy17, StartTime17, EndTime17, 
				R_day18, O_day18, S_day18, T_day18, TransComment_day18, OrigDetailSeq_day18, LineItemApprovedBy18, StartTime18, EndTime18,
				R_day19, O_day19, S_day19, T_day19, TransComment_day19, OrigDetailSeq_day19, LineItemApprovedBy19, StartTime19, EndTime19,
				R_day20, O_day20, S_day20, T_day20, TransComment_day20, OrigDetailSeq_day20, LineItemApprovedBy20, StartTime20, EndTime20,
				R_day21, O_day21, S_day21, T_day21, TransComment_day21, OrigDetailSeq_day21, LineItemApprovedBy21, StartTime21, EndTime21,
				R_day22, O_day22, S_day22, T_day22, TransComment_day22, OrigDetailSeq_day22, LineItemApprovedBy22, StartTime22, EndTime22,
				R_day23, O_day23, S_day23, T_day23, TransComment_day23, OrigDetailSeq_day23, LineItemApprovedBy23, StartTime23, EndTime23,
				R_day24, O_day24, S_day24, T_day24, TransComment_day24, OrigDetailSeq_day24, LineItemApprovedBy24, StartTime24, EndTime24,
				R_day25, O_day25, S_day25, T_day25, TransComment_day25, OrigDetailSeq_day25, LineItemApprovedBy25, StartTime25, EndTime25,
				R_day26, O_day26, S_day26, T_day26, TransComment_day26, OrigDetailSeq_day26, LineItemApprovedBy26, StartTime26, EndTime26,
				R_day27, O_day27, S_day27, T_day27, TransComment_day27, OrigDetailSeq_day27, LineItemApprovedBy27, StartTime27, EndTime27,
				R_day28, O_day28, S_day28, T_day28, TransComment_day28, OrigDetailSeq_day28, LineItemApprovedBy28, StartTime28, EndTime28,
				R_day29, O_day29, S_day29, T_day29, TransComment_day29, OrigDetailSeq_day29, LineItemApprovedBy29, StartTime29, EndTime29,
				R_day30, O_day30, S_day30, T_day30, TransComment_day30, OrigDetailSeq_day30, LineItemApprovedBy30, StartTime30, EndTime30,
				R_day31, O_day31, S_day31, T_day31, TransComment_day31, OrigDetailSeq_day31, LineItemApprovedBy31, StartTime31, EndTime31,

				RowRegular, RowOvt,	RowOvt2, LineItemWBSLock,
				BudgetedFlag, BudgetedLevels, BudgetSource, BudgetLevel, LineItemApproval,
				Locale, WBS1Locale, WBS2Locale, WBS3Locale,
				TKCheckRPDate, RequireComments, TKCAllowStartEndTime, BillingCategoryName, RowTotal, ExistingRow,
				LC1Name, LC2Name, LC3Name, LC4Name, LC5Name, EmployeeCompany = _Time.EmployeeCompany, _Time.Status,

				Item_UID1, StatusCode1 = NULL,
				Item_UID2, StatusCode2 = NULL,
				Item_UID3, StatusCode3 = NULL,
				Item_UID4, StatusCode4 = NULL,
				Item_UID5, StatusCode5 = NULL,
				Item_UID6, StatusCode6 = NULL,
				Item_UID7, StatusCode7 = NULL,
				Item_UID8, StatusCode8 = NULL,
				Item_UID9, StatusCode9 = NULL,
				Item_UID10, StatusCode10 = NULL,
				Item_UID11, StatusCode11 = NULL,
				Item_UID12, StatusCode12 = NULL,
				Item_UID13, StatusCode13 = NULL,
				Item_UID14, StatusCode14 = NULL,
				Item_UID15, StatusCode15 = NULL,
				Item_UID16, StatusCode16 = NULL,
				Item_UID17, StatusCode17 = NULL,
				Item_UID18, StatusCode18 = NULL,
				Item_UID19, StatusCode19 = NULL,
				Item_UID20, StatusCode20 = NULL,
				Item_UID21, StatusCode21 = NULL,
				Item_UID22, StatusCode22 = NULL,
				Item_UID23, StatusCode23 = NULL,
				Item_UID24, StatusCode24 = NULL,
				Item_UID25, StatusCode25 = NULL,
				Item_UID26, StatusCode26 = NULL,
				Item_UID27, StatusCode27 = NULL,
				Item_UID28, StatusCode28 = NULL,
				Item_UID29, StatusCode29 = NULL,
				Item_UID30, StatusCode30 = NULL,
				Item_UID31, StatusCode31 = NULL,
				LineApprovalStatus = 
				CASE WHEN @tkMasterStatus = 'N' THEN 'N'
				WHEN (LineItemApprovalStatus1+LineItemApprovalStatus2+LineItemApprovalStatus3+LineItemApprovalStatus4+LineItemApprovalStatus5
				+LineItemApprovalStatus6+LineItemApprovalStatus7+LineItemApprovalStatus8+LineItemApprovalStatus9+LineItemApprovalStatus10
				+LineItemApprovalStatus11+LineItemApprovalStatus12+LineItemApprovalStatus13+LineItemApprovalStatus14+LineItemApprovalStatus15
				+LineItemApprovalStatus16+LineItemApprovalStatus17+LineItemApprovalStatus18+LineItemApprovalStatus19+LineItemApprovalStatus20
				+LineItemApprovalStatus21+LineItemApprovalStatus22+LineItemApprovalStatus23+LineItemApprovalStatus24+LineItemApprovalStatus25
				+LineItemApprovalStatus26+LineItemApprovalStatus27+LineItemApprovalStatus28+LineItemApprovalStatus29+LineItemApprovalStatus30+LineItemApprovalStatus31) BETWEEN 1 AND 99 THEN 'A'
				WHEN (LineItemApprovalStatus1+LineItemApprovalStatus2+LineItemApprovalStatus3+LineItemApprovalStatus4+LineItemApprovalStatus5
				+LineItemApprovalStatus6+LineItemApprovalStatus7+LineItemApprovalStatus8+LineItemApprovalStatus9+LineItemApprovalStatus10
				+LineItemApprovalStatus11+LineItemApprovalStatus12+LineItemApprovalStatus13+LineItemApprovalStatus14+LineItemApprovalStatus15
				+LineItemApprovalStatus16+LineItemApprovalStatus17+LineItemApprovalStatus18+LineItemApprovalStatus19+LineItemApprovalStatus20
				+LineItemApprovalStatus21+LineItemApprovalStatus22+LineItemApprovalStatus23+LineItemApprovalStatus24+LineItemApprovalStatus25
				+LineItemApprovalStatus26+LineItemApprovalStatus27+LineItemApprovalStatus28+LineItemApprovalStatus29+LineItemApprovalStatus30+LineItemApprovalStatus31) >= 10000 THEN 'J'
				WHEN (LineItemApprovalStatus1+LineItemApprovalStatus2+LineItemApprovalStatus3+LineItemApprovalStatus4+LineItemApprovalStatus5
				+LineItemApprovalStatus6+LineItemApprovalStatus7+LineItemApprovalStatus8+LineItemApprovalStatus9+LineItemApprovalStatus10
				+LineItemApprovalStatus11+LineItemApprovalStatus12+LineItemApprovalStatus13+LineItemApprovalStatus14+LineItemApprovalStatus15
				+LineItemApprovalStatus16+LineItemApprovalStatus17+LineItemApprovalStatus18+LineItemApprovalStatus19+LineItemApprovalStatus20
				+LineItemApprovalStatus21+LineItemApprovalStatus22+LineItemApprovalStatus23+LineItemApprovalStatus24+LineItemApprovalStatus25
				+LineItemApprovalStatus26+LineItemApprovalStatus27+LineItemApprovalStatus28+LineItemApprovalStatus29+LineItemApprovalStatus30+LineItemApprovalStatus31) = 0 THEN 'N'
				ELSE 'Y'
				END,
				calStartDateTime1, calEndDateTime1,
				calStartDateTime2, calEndDateTime2,
				calStartDateTime3, calEndDateTime3,
				calStartDateTime4, calEndDateTime4,
				calStartDateTime5, calEndDateTime5,
				calStartDateTime6, calEndDateTime6,
				calStartDateTime7, calEndDateTime7,
				calStartDateTime8, calEndDateTime8,
				calStartDateTime9, calEndDateTime9,
				calStartDateTime10, calEndDateTime10,
				calStartDateTime11, calEndDateTime11,
				calStartDateTime12, calEndDateTime12,
				calStartDateTime13, calEndDateTime13,
				calStartDateTime14, calEndDateTime14,
				calStartDateTime15, calEndDateTime15,
				calStartDateTime16, calEndDateTime16,
				calStartDateTime17, calEndDateTime17,
				calStartDateTime18, calEndDateTime18,
				calStartDateTime19, calEndDateTime19,
				calStartDateTime20, calEndDateTime20,
				calStartDateTime21, calEndDateTime21,
				calStartDateTime22, calEndDateTime22,
				calStartDateTime23, calEndDateTime23,
				calStartDateTime24, calEndDateTime24,
				calStartDateTime25, calEndDateTime25,
				calStartDateTime26, calEndDateTime26,
				calStartDateTime27, calEndDateTime27,
				calStartDateTime28, calEndDateTime28,
				calStartDateTime29, calEndDateTime29,
				calStartDateTime30, calEndDateTime30,
				calStartDateTime31, calEndDateTime31, 
   				CustTKUD01=ISNULL(tkCustomFields.CustTKUD01,''),
				CustTKUD02=ISNULL(tkCustomFields.CustTKUD02,0),
				CustTKUD03=ISNULL(tkCustomFields.CustTKUD03,'') 
			 FROM
			(SELECT 
				SortOrder = 0,
				@MasterTKGroup as TKGroup,
				Employee = @Employee,
				StartDate = CONVERT(VARCHAR(10),@StartDate,121),
				EndDate = CONVERT(VARCHAR(10),@EndDate,121),
				tkDetail.Seq,
				tkDetail.Category,
				isCategory = 'N',
				tkDetail.WBS1, MAX(PR1.Name) As WBS1Name, MAX(CL.Name) As ClientName, MAX(PR1.SubLevel) As WBS2Level,
				MAX(PR1.ChargeType) As ChargeType,
				tkDetail.WBS2, MAX(CASE WHEN tkDetail.WBS2 = N'' THEN '' ELSE PR2.Name END) As WBS2Name, MAX(PR2.SubLevel) As WBS3Level,
				tkDetail.WBS3, MAX(CASE WHEN tkDetail.WBS3 = N'' THEN '' ELSE PR3.Name END) As WBS3Name,
				tkDetail.LaborCode,
				MAX(LC1.Label + 
					(CASE WHEN LC2.Label IS NULL THEN '' ELSE '/' + LC2.Label END) +
					(CASE WHEN LC3.Label IS NULL THEN '' ELSE '/' + LC3.Label END) +
					(CASE WHEN LC4.Label IS NULL THEN '' ELSE '/' + LC4.Label END) +
					(CASE WHEN LC5.Label IS NULL THEN '' ELSE '/' + LC5.Label END)
				) As LCName,
				tkDetail.BillCategory,
			
				R_day1 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN RegHrs ELSE 0 END)),
				O_day1 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN OvtHrs ELSE 0 END)),
				S_day1 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN SpecialOvtHrs ELSE 0 END)),
				T_day1 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN #ApprovalItem.Item_UID ELSE NULL END),

				R_day2 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN RegHrs ELSE 0 END)),
				O_day2 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN OvtHrs ELSE 0 END)),
				S_day2 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN SpecialOvtHrs ELSE 0 END)),
				T_day2 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day3 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN RegHrs ELSE 0 END)),
				O_day3 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN OvtHrs ELSE 0 END)),
				S_day3 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN SpecialOvtHrs ELSE 0 END)),
				T_day3 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day4 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN RegHrs ELSE 0 END)),
				O_day4 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN OvtHrs ELSE 0 END)),
				S_day4 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN SpecialOvtHrs ELSE 0 END)),
				T_day4 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day5 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN RegHrs ELSE 0 END)),
				O_day5 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN OvtHrs ELSE 0 END)),
				S_day5 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN SpecialOvtHrs ELSE 0 END)),
				T_day5 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day6 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN RegHrs ELSE 0 END)),
				O_day6 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN OvtHrs ELSE 0 END)),
				S_day6 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN SpecialOvtHrs ELSE 0 END)),
				T_day6 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day7 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN RegHrs ELSE 0 END)),
				O_day7 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN OvtHrs ELSE 0 END)),
				S_day7 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN SpecialOvtHrs ELSE 0 END)),
				T_day7 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day8 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN RegHrs ELSE 0 END)),
				O_day8 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN OvtHrs ELSE 0 END)),
				S_day8 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN SpecialOvtHrs ELSE 0 END)),
				T_day8 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day9 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN RegHrs ELSE 0 END)),
				O_day9 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN OvtHrs ELSE 0 END)),
				S_day9 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN SpecialOvtHrs ELSE 0 END)),
				T_day9 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day10 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN RegHrs ELSE 0 END)),
				O_day10 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN OvtHrs ELSE 0 END)),
				S_day10 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN SpecialOvtHrs ELSE 0 END)),
				T_day10 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day11 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN RegHrs ELSE 0 END)),
				O_day11 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN OvtHrs ELSE 0 END)),
				S_day11 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN SpecialOvtHrs ELSE 0 END)),
				T_day11 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day12 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN RegHrs ELSE 0 END)),
				O_day12 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN OvtHrs ELSE 0 END)),
				S_day12 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN SpecialOvtHrs ELSE 0 END)),
				T_day12 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day13 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN RegHrs ELSE 0 END)),
				O_day13 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN OvtHrs ELSE 0 END)),
				S_day13 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN SpecialOvtHrs ELSE 0 END)),
				T_day13 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day14 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN RegHrs ELSE 0 END)),
				O_day14 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN OvtHrs ELSE 0 END)),
				S_day14 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN SpecialOvtHrs ELSE 0 END)),
				T_day14 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day15 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN RegHrs ELSE 0 END)),
				O_day15 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN OvtHrs ELSE 0 END)),
				S_day15 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN SpecialOvtHrs ELSE 0 END)),
				T_day15 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day16 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN RegHrs ELSE 0 END)),
				O_day16 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN OvtHrs ELSE 0 END)),
				S_day16 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN SpecialOvtHrs ELSE 0 END)),
				T_day16 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day17 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN RegHrs ELSE 0 END)),
				O_day17 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN OvtHrs ELSE 0 END)),
				S_day17 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN SpecialOvtHrs ELSE 0 END)),
				T_day17 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day18 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN RegHrs ELSE 0 END)),
				O_day18 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN OvtHrs ELSE 0 END)),
				S_day18 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN SpecialOvtHrs ELSE 0 END)),
				T_day18 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day19 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN RegHrs ELSE 0 END)),
				O_day19 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN OvtHrs ELSE 0 END)),
				S_day19 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN SpecialOvtHrs ELSE 0 END)),
				T_day19 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day20 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN RegHrs ELSE 0 END)),
				O_day20 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN OvtHrs ELSE 0 END)),
				S_day20 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN SpecialOvtHrs ELSE 0 END)),
				T_day20 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day21 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN RegHrs ELSE 0 END)),
				O_day21 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN OvtHrs ELSE 0 END)),
				S_day21 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN SpecialOvtHrs ELSE 0 END)),
				T_day21 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day22 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN RegHrs ELSE 0 END)),
				O_day22 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN OvtHrs ELSE 0 END)),
				S_day22 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN SpecialOvtHrs ELSE 0 END)),
				T_day22 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day23 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN RegHrs ELSE 0 END)),
				O_day23 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN OvtHrs ELSE 0 END)),
				S_day23 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN SpecialOvtHrs ELSE 0 END)),
				T_day23 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day24 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN RegHrs ELSE 0 END)),
				O_day24 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN OvtHrs ELSE 0 END)),
				S_day24 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN SpecialOvtHrs ELSE 0 END)),
				T_day24 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day25 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN RegHrs ELSE 0 END)),
				O_day25 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN OvtHrs ELSE 0 END)),
				S_day25 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN SpecialOvtHrs ELSE 0 END)),
				T_day25 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day26 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN RegHrs ELSE 0 END)),
				O_day26 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN OvtHrs ELSE 0 END)),
				S_day26 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN SpecialOvtHrs ELSE 0 END)),
				T_day26 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day27 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN RegHrs ELSE 0 END)),
				O_day27 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN OvtHrs ELSE 0 END)),
				S_day27 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN SpecialOvtHrs ELSE 0 END)),
				T_day27 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day28 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN RegHrs ELSE 0 END)),
				O_day28 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN OvtHrs ELSE 0 END)),
				S_day28 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN SpecialOvtHrs ELSE 0 END)),
				T_day28 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day29 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN RegHrs ELSE 0 END)),
				O_day29 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN OvtHrs ELSE 0 END)),
				S_day29 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN SpecialOvtHrs ELSE 0 END)),
				T_day29 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day30 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN RegHrs ELSE 0 END)),
				O_day30 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN OvtHrs ELSE 0 END)),
				S_day30 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN SpecialOvtHrs ELSE 0 END)),
				T_day30 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN #ApprovalItem.Item_UID ELSE NULL END),
			
				R_day31 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN RegHrs ELSE 0 END)),
				O_day31 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN OvtHrs ELSE 0 END)),
				S_day31 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN SpecialOvtHrs ELSE 0 END)),
				T_day31 = CONVERT(DECIMAL(14,2),SUM(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN RegHrs+OvtHrs+SpecialOvtHrs ELSE 0 END)),
				TransComment_day31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN TransComment ELSE NULL END),
				OrigDetailSeq_day31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN tkDetail.Seq ELSE -1 END),
				LineItemApprovedBy31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN LineItemApprovalStatus ELSE NULL END),
				StartTime31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN REPLACE(CONVERT(VARCHAR,StartDateTime,121),' ','T') ELSE NULL END),
				EndTime31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN REPLACE(CONVERT(VARCHAR,EndDateTime,121),' ','T') ELSE NULL END),
				LineItemApprovalStatus31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 
				THEN (CASE WHEN LineItemApprovalStatus = 'A' THEN 1 WHEN LineItemApprovalStatus = 'Y' THEN 100 WHEN LineItemApprovalStatus = 'J' THEN 10000 ELSE 0 END) ELSE 0 END),
				Item_UID31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN #ApprovalItem.Item_UID ELSE NULL END),

				RowRegular = CONVERT(DECIMAL(14,2),SUM(RegHrs)),
				RowOvt = CONVERT(DECIMAL(14,2),SUM(OvtHrs)),
				RowOvt2 = CONVERT(DECIMAL(14,2),SUM(SpecialOvtHrs)),
				LineItemWBSLock = CASE WHEN @UseApprovalWorkflow = 'N' THEN 'N' WHEN SUM(CASE WHEN LineItemApprovalStatus IS NULL THEN 0 WHEN LineItemApprovalStatus = 'A' THEN 1 ELSE 0 END) > 0 THEN 'Y' ELSE 'N' END,
				MAX(PR3.BudgetedFlag) As BudgetedFlag,
				MAX(PR3.BudgetedLevels) As BudgetedLevels,
				MAX(PR3.BudgetSource) As BudgetSource,
				MAX(PR3.BudgetLevel) As BudgetLevel,
				MAX(PR1.LineItemApproval) As LineItemApproval,
				Locale = tkDetail.Locale,
				WBS1Locale = MAX(PR1.Locale),
				WBS2Locale = MAX(PR2.Locale),
				WBS3Locale = MAX(PR3.Locale),
				MAX(PR3.TKCheckRPDate) As TKCheckRPDate,
				MAX(PR3.RequireComments) AS RequireComments,
				TKCAllowStartEndTime = 'N',	-- N/A, on non-special category line, it depends on the employee's setting
				BillingCategoryName = MAX(BTLaborCats.Description),
				RowTotal = CONVERT(DECIMAL(14,2),SUM(RegHrs)) + CONVERT(DECIMAL(14,2),SUM(OvtHrs)) + CONVERT(DECIMAL(14,2),SUM(SpecialOvtHrs)),
				ExistingRow = 'Y',
				ISNULL(MAX(LC1.Label), '') AS LC1Name, 
				ISNULL(MAX(LC2.Label), '') AS LC2Name, 
				ISNULL(MAX(LC3.Label), '') AS LC3Name, 
				ISNULL(MAX(LC4.Label), '') AS LC4Name, 
				ISNULL(MAX(LC5.Label), '') AS LC5Name,
				MAX(tkDetail.EmployeeCompany) AS EmployeeCompany,
				Status = MAX(CASE WHEN tkDetail.WBS3 <> ' ' THEN PR3.Status
				              WHEN tkDetail.WBS2 <> ' ' THEN PR2.Status
							  ELSE PR1.Status
						 END),
				calStartDateTime1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime1 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 1 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime2 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 2 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime3 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 3 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime4 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 4 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime5 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 5 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime6 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 6 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime7 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 7 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime8 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 8 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime9 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 9 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime10 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 10 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime11 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 11 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime12 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 12 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime13 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 13 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime14 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 14 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime15 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 15 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime16 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 16 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime17 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 17 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime18 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 18 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime19 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 19 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime20 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 20 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime21 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 21 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime22 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 22 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime23 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 23 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime24 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 24 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime25 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 25 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime26 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 26 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime27 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 27 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime28 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 28 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime29 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 29 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime30 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 30 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END),
				calStartDateTime31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN REPLACE(CONVERT(VARCHAR,calStartDateTime,121),' ','T') ELSE NULL END),
				calEndDateTime31 = MAX(CASE WHEN DateDiff(dd, @StartDate, TransDate)+1 = 31 THEN REPLACE(CONVERT(VARCHAR,calEndDateTime,121),' ','T') ELSE NULL END)
			FROM
				tkDetail
				LEFT JOIN #ApprovalItem ON REPLACE(Str(tkDetail.Seq, 4), Space(1), '0') + '|' + CONVERT(VARCHAR, tkDetail.TransDate, 23) + '|' + tkDetail.Employee + '|' + CONVERT(VARCHAR, tkDetail.EndDate, 23) + '|' + tkDetail.EmployeeCompany = #ApprovalItem.ApplicationKey
				LEFT JOIN PR AS PR1 ON PR1.WBS1 = tkDetail.WBS1 --AND PR1.WBS2 = ' ' AND tkDetail.WBS3 = ' '
				LEFT JOIN PR AS PR2 on PR2.WBS1 = tkDetail.WBS1 AND PR2.WBS2 = tkDetail.WBS2 --AND PR2.WBS3 = ' ' AND PR2.WBS2 <> ' '
				LEFT JOIN PR AS PR3 on PR3.WBS1 = tkDetail.WBS1 AND PR3.WBS2 = tkDetail.WBS2 AND PR3.WBS3 = tkDetail.WBS3 --AND PR3.WBS3 <> ' '
				LEFT JOIN CL ON PR1.ClientID = CL.ClientID
				LEFT JOIN CFGLCCodes AS LC1 ON (LC1.LCLevel = 1 AND LC1.Code = SUBSTRING(tkDetail.LaborCode, @LC1Start, @LC1Length))
				LEFT JOIN CFGLCCodes AS LC2 ON (LC2.LCLevel = 2 AND LC2.Code = SUBSTRING(tkDetail.LaborCode, @LC2Start, @LC2Length))
				LEFT JOIN CFGLCCodes AS LC3 ON (LC3.LCLevel = 3 AND LC3.Code = SUBSTRING(tkDetail.LaborCode, @LC3Start, @LC3Length))
				LEFT JOIN CFGLCCodes AS LC4 ON (LC4.LCLevel = 4 AND LC4.Code = SUBSTRING(tkDetail.LaborCode, @LC4Start, @LC4Length))
				LEFT JOIN CFGLCCodes AS LC5 ON (LC5.LCLevel = 5 AND LC5.Code = SUBSTRING(tkDetail.LaborCode, @LC5Start, @LC5Length))
				LEFT JOIN BTLaborCats ON tkDetail.BillCategory = BTLaborCats.Category
			WHERE
				Employee = @Employee AND
				tkDetail.EndDate = @EndDate AND
				tkDetail.EmployeeCompany = @activeCompany AND
				PR1.WBS2 = N' ' AND
				PR1.WBS3 = N' ' AND
				PR2.WBS3 = N' ' AND
				(tkDetail.Category = N' ' OR tkDetail.Category IS NULL)
			GROUP BY
				tkDetail.WBS1, tkDetail.WBS2, tkDetail.WBS3, tkDetail.Seq,
				tkDetail.Category, tkDetail.LaborCode, tkDetail.BillCategory, tkDetail.Locale
			/* ^^^^^ Get non special category tkDetail records ^^^^^ */
			) _Time
    		LEFT JOIN tkCustomFields on tkCustomFields.EndDate=_Time.EndDate and tkCustomFields.Employee=_Time.Employee and tkCustomFields.EmployeeCompany=_Time.EmployeeCompany and tkCustomFields.Seq=_Time.Seq 
			ORDER BY
				isCategory Desc, Seq
		END
GO
