SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPConvertOpp2PR_Main]
AS

BEGIN -- Procedure stRPConvertOpp2PR_Main

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- The is the main Stored Procedure to move data from Opportunities to Projects.
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  SET NOCOUNT ON

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  --> Before the main conversion of data from Opportunity to Project, we need to fix potential problems with data 
  --> in the trio relationship between Opportunity|RegularProject|ProposalProject.

  BEGIN TRANSACTION Fix_Opp_RegProj_PropProj

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Scenario: Removing RegularProjects which appeared multiple times in trios of Opportunity|RegularProject|ProposalProject.
    -- RegularProjects on that list must be updated to set PR.OpportunityID = NULL for the whole WBS Tree.

    UPDATE PR SET
      PR.OpportunityID = NULL
      FROM PR
      WHERE PR.WBS1 IN
        (

          SELECT
            PR_R_WBS1
            FROM ( /* X */

              /* Use EXCEPT operation to identify RegularProjects which must be removed from the trio-relationship */

              SELECT DISTINCT 
                O.Name AS O_Name,
                O.PRWBS1 AS O_PRWBS1, 
                O.PRProposalWBS1 AS O_PRProposalWBS1, 
                O.OpportunityID AS O_OpportunityID,
                PR_R.WBS1 AS PR_R_WBS1,
                PR_P.WBS1 AS PR_P_WBS1
                FROM Opportunity AS O
                  LEFT JOIN PR AS PR_R
                    ON O.OpportunityID = PR_R.OpportunityID
                      AND PR_R.WBS2 = ' ' AND PR_R.WBS3 = ' ' AND PR_R.ChargeType = 'R'
                  LEFT JOIN PR AS PR_P
                    ON O.OpportunityID = PR_P.OpportunityID
                      AND PR_P.WBS2 = ' ' AND PR_P.WBS3 = ' ' AND PR_P.ChargeType = 'P'
                  WHERE (
                    (O.PRWBS1 IS NOT NULL AND PR_R.WBS1 IS NOT NULL AND O.PRWBS1 = PR_R.WBS1) OR
                    (O.PRProposalWBS1 IS NOT NULL AND PR_P.WBS1 IS NOT NULL AND O.PRProposalWBS1 = PR_P.WBS1)
                  )

              EXCEPT

              /* Collect total population of trios of Opportunity|RegularProject|ProposalProject. */

              SELECT DISTINCT 
                O.Name AS O_Name,
                O.PRWBS1 AS O_PRWBS1, 
                O.PRProposalWBS1 AS O_PRProposalWBS1, 
                O.OpportunityID AS O_OpportunityID,
                PR_R.WBS1 AS PR_R_WBS1,
                PR_P.WBS1 AS PR_P_WBS1
                FROM Opportunity AS O
                  LEFT JOIN PR AS PR_R
                    ON O.PRWBS1 = PR_R.WBS1 AND O.OpportunityID = PR_R.OpportunityID
                      AND PR_R.WBS2 = ' ' AND PR_R.WBS3 = ' ' AND PR_R.ChargeType = 'R'
                  LEFT JOIN PR AS PR_P
                    ON O.PRProposalWBS1 = PR_P.WBS1 AND O.OpportunityID = PR_P.OpportunityID
                      AND PR_P.WBS2 = ' ' AND PR_P.WBS3 = ' ' AND PR_P.ChargeType = 'P'
                  WHERE (
                    (O.PRWBS1 IS NOT NULL AND PR_R.WBS1 IS NOT NULL AND O.PRWBS1 = PR_R.WBS1) OR
                    (O.PRProposalWBS1 IS NOT NULL AND PR_P.WBS1 IS NOT NULL AND O.PRProposalWBS1 = PR_P.WBS1)
                  )

            ) AS X

        )

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Scenario: From perspective of Opportunity, removing linkage from Opportunities to RegularProjects, where there were multiple linkages.
    -- Set O.PRWBS1 = NULL

    UPDATE O SET
      PRWBS1 = NULL
      FROM Opportunity AS O
        WHERE O.OpportunityID IN 
          ( 
            SELECT DISTINCT
              O_OpportunityID
              FROM ( /* X */
                SELECT DISTINCT 
                  O.Name AS O_Name,
                  O.PRWBS1 AS O_PRWBS1, 
                  O.OpportunityID AS O_OpportunityID,
                  PR.WBS1 AS PR_R_WBS1
                  FROM Opportunity AS O
                    INNER JOIN PR
                      ON O.PRWBS1 = PR.WBS1
                        AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND PR.ChargeType = 'R' 
                EXCEPT
                /* Collect total population of pairs of Opportunity|RegularProject. */
                SELECT DISTINCT 
                  O.Name AS O_Name,
                  O.PRWBS1 AS O_PRWBS1, 
                  O.OpportunityID AS O_OpportunityID,
                  PR.WBS1 AS PR_R_WBS1
                  FROM Opportunity AS O
                    INNER JOIN PR
                      ON O.PRWBS1 = PR.WBS1 AND O.OpportunityID = PR.OpportunityID
                        AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND PR.ChargeType = 'R'
              ) AS X
          )

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  COMMIT TRANSACTION Fix_Opp_RegProj_PropProj

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION Convert_Opp_Proj

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Convert Stages from Opportunity to Project

    EXECUTE dbo.stRPConvertStagesFromOppToPR

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Create Project for those Opportunities w/o Project.

    EXECUTE dbo.stRPCreatePRFromUnlinkedOpp

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move data from Opportunity to Project.

    EXECUTE dbo.stRPConvertFromOpp2ExistingPR

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Make sure that Projects have Stage.
    -- At this point, Projects had been created from Opportunities.
    -- Existing Projects should already have Stage popuplated.
    -- But, just in case something fell through the crack, attempt to set Stage one more time to cover all bases.

    DECLARE @strStageCode nvarchar(10)

    SELECT @strStageCode = Code
      FROM ( /* Z */
        SELECT TOP 1 Code
          FROM ( /* X */
            SELECT Code AS Code, CONVERT(bigint, 1) AS Rank 
              FROM CFGProjectStageData 
              WHERE Step = 'W' AND Code LIKE ('%WON%')
            UNION
            SELECT Code AS Code, CONVERT(bigint, 2) AS Rank 
              FROM CFGProjectStageData 
              WHERE Step = 'W' AND Code LIKE ('%WIN%')
            UNION
            SELECT Code AS Code, CONVERT(bigint, 3) AS Rank 
              FROM CFGProjectStageData 
              WHERE Step = 'W' AND Code = '~WDEF~'
            UNION
            SELECT TOP 1 CODE AS Code, -1 - POWER(CAST(-2 AS BIGINT), 63) AS Rank 
              FROM CFGProjectStageData
              WHERE Step = 'W'
          ) AS X
          ORDER BY RANK ASC
      ) Z

    UPDATE PR SET
      Stage = @strStageCode
      FROM PR
      WHERE Stage IS NULL AND ChargeType IN ('R', 'H', 'P')

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Remove data from Opportunity.

    EXECUTE dbo.stRPRemoveOppRelatedData

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  COMMIT TRANSACTION Convert_Opp_Proj

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF

END -- stRPConvertOpp2PR_Main
GO
