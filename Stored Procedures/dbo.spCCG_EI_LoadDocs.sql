SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_LoadDocs]
	@wbs1		Nvarchar(30),
	@context	varchar(30) = 'DRAFT',--DRAFT,FINAL,INVOICE,PACKAGE,UNBILLEDDETAIL,NEWEST (DRAFT OR FINAL)
	@newest		bit = 0
AS
BEGIN
	-- TEST: EXEC [dbo].[spCCG_EI_LoadDocs] '2003005.00', 'DRAFT',1
	-- TEST: EXEC [dbo].[spCCG_EI_LoadDocs] '2003005.00', 'UNBILLEDDETAIL',1
	SET NOCOUNT ON;
	IF @context = 'NEWEST'
	BEGIN
		SELECT top 1 em.Employee,convert(Varchar(19),  dpr.Createdate, 120) as CreateDateString, dpr.*
		FROM CCG_DOCS_PR dpr
			LEFT JOIN SEUser on dpr.createuser = seuser.Username
			LEFT JOIN EM on SEUser.Employee = EM.Employee
		WHERE WBS1 = @wbs1 and dpr.Category in ('INVOICE')
		order by dpr.SortBy desc, dpr.CreateDate desc
	END
	ELSE IF @newest = 1 and @context = 'DRAFT'
	BEGIN
		SELECT top 1 em.Employee,convert(Varchar(19),  dpr.Createdate, 120) as CreateDateString, dpr.*
		FROM CCG_DOCS_PR dpr
			LEFT JOIN SEUser on dpr.createuser = seuser.Username
			LEFT JOIN EM on SEUser.Employee = EM.Employee
		WHERE WBS1 = @wbs1 and dpr.Category = 'INVOICE' and dpr.Invoice = '<Draft>'
		order by dpr.SortBy desc, dpr.CreateDate desc
	END
	ELSE IF @newest = 1 and @context = 'FINAL'
	BEGIN
		SELECT top 1 em.Employee,convert(Varchar(19),  dpr.Createdate, 120) as CreateDateString, dpr.*
		FROM CCG_DOCS_PR dpr
			LEFT JOIN SEUser on dpr.createuser = seuser.Username
			LEFT JOIN EM on SEUser.Employee = EM.Employee
		WHERE WBS1 = @wbs1 and dpr.Category = 'INVOICE' and dpr.Invoice != '<Draft>'
		order by dpr.SortBy desc, dpr.CreateDate desc
	END
	
	ELSE IF @newest = 0 and @context = 'FINAL'
	BEGIN
		SELECT em.Employee,convert(Varchar(19),  dpr.Createdate, 120) as CreateDateString, dpr.*
		FROM CCG_DOCS_PR dpr
			LEFT JOIN SEUser on dpr.createuser = seuser.Username
			LEFT JOIN EM on SEUser.Employee = EM.Employee
		WHERE WBS1 = @wbs1 and dpr.Category = 'INVOICE' and dpr.Invoice != '<Draft>'
		order by dpr.SortBy desc, dpr.CreateDate desc
	END
	ELSE IF @newest = 1 and @context = 'UNBILLEDDETAIL'
	BEGIN

		declare @lastDraft datetime
		declare @pairWithInvoice varchar(1) = 'Y'
		if exists (select 'x' from CCG_EI_Config where CharIndex('PairUnbilledWithInv=N',AdvancedOptions,1) > 0)
		begin
			set @pairWithInvoice='N'
		end
		--attempt to only show an unbilled if there is a draft around the same age
		SELECT @lastDraft = Max(dpr.Createdate)
		FROM CCG_DOCS_PR dpr		WHERE WBS1 = @wbs1 and dpr.Category = 'INVOICE' and dpr.Invoice = '<Draft>'

		if @lastDraft is null or @pairWithInvoice='N'
			set @lastDraft = Getdate() -31
		else 
			set @lastDraft = @lastDraft - 0.002 --incase unbilled was processed first allow 3 minutes or so of leway

		--print @lastDraft

		SELECT top 1 em.Employee,convert(Varchar(19), dpr.Createdate, 120) as CreateDateString, dpr.*
		FROM CCG_DOCS_PR dpr
			LEFT JOIN SEUser on dpr.createuser = seuser.Username
			LEFT JOIN EM on SEUser.Employee = EM.Employee
		WHERE WBS1 = @wbs1 and dpr.Category = 'UNBILLEDDETAIL' and dpr.CreateDate > @lastDraft
		order by dpr.SortBy desc, dpr.CreateDate desc
	END
	ELSE IF @newest = 1
	BEGIN
		SELECT top 1 em.Employee,convert(Varchar(19), dpr.Createdate, 120) as CreateDateString, dpr.*
		FROM CCG_DOCS_PR dpr
			LEFT JOIN SEUser on dpr.createuser = seuser.Username
			LEFT JOIN EM on SEUser.Employee = EM.Employee
		WHERE WBS1 = @wbs1 and dpr.Category = @context
		order by dpr.SortBy desc, dpr.CreateDate desc
	END
	ELSE 
	BEGIN

		SELECT em.Employee,convert(Varchar(19), dpr.Createdate, 120) as CreateDateString, dpr.*
		FROM CCG_DOCS_PR dpr
			LEFT JOIN SEUser on dpr.createuser = seuser.Username
			LEFT JOIN EM on SEUser.Employee = EM.Employee
		WHERE WBS1 = @wbs1 and dpr.Category = @context
		order by dpr.SortBy desc, dpr.CreateDate desc
	END

END;
GO
