SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_Config_Get_Version] 
	@softwareVersion varchar(20) = null
AS
BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT version, currentperiod, appurl AS visionurl, MulticompanyEnabled,
			MulticurrencyEnabled, AuditingEnabled, AuditingEnabledBT, (select top 1 version from CCG_EI_Config) as appVersion,
			(select top 1 v.DatabaseVersion from CCG_Version v where Product = 'Electronic Invoicing - Packaging') as PackagingVersion
		FROM CFGSystem

	if isnull(@softwareVersion, '') <> ''
		update CCG_Version set SoftwareVersion = @softwareVersion where Product = 'Electronic Invoicing' 
			and (SoftwareVersion is null or SoftwareVersion <> @softwareVersion)
END
GO
