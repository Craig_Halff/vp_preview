SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_OpportunityNumberValidation] @OpportunityNo varchar (255), @ContractType varchar (255)
AS
/*
Copyright (c) 2018 Central Consulting Group. All rights reserved.
05/11/2018 David Springer
           Validate number meets standards.
*/
BEGIN
	If @OpportunityNo not like '%.___'
		RAISERROR ('Opportunity Number must have a three digit extension using a period.                                        ', 16, 1)

	If @OpportunityNo not like 'P%'
		RAISERROR ('Opportunity Number must be prefixed with a "P".                                              ', 16, 1)

	If Len (@OpportunityNo) <> 10
		RAISERROR ('Opportunity Number must be 10 characters in length.                                          ', 16, 1)

	IF Right(@OpportunityNo,3) = '000' and (@ContractType = 'Individual Project/Task Order' or @ContractType = 'Administrative Qualification')
		RAISERROR ('Opportunity Number may not have a "000" Suffix for current contract type.                                              ', 16, 1)

	IF Right(@OpportunityNo,3) <> '000' and (@ContractType <> 'Individual Project/Task Order' and @ContractType <> 'Administrative Qualification')
		RAISERROR ('Opportunity Number must have a "000" Suffix for current contract type.                                              ', 16, 1)

END;

GO
