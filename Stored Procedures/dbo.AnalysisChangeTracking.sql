SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AnalysisChangeTracking]
  @bitEnable bit = 1
AS

BEGIN -- Procedure AnalysisChangeTracking

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to enable or disable Change Tracking.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

IF @bitEnable = 1
BEGIN

ALTER DATABASE CURRENT
SET CHANGE_TRACKING = ON
(CHANGE_RETENTION = 5 DAYS, AUTO_CLEANUP = ON)

ALTER TABLE consolidations
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE Eliminations
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE PRF
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE PR
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE ConsolidationBudgets
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE ConsolidationBudgetsDetail
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE Contacts
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE EMMain
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE EMCompany
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE RPPlan
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE RPTask
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE LedgerAR
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE AR
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE ContractCredit
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE Contracts
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE ContractDetails
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE LB
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE EB
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE RPBaselineExpenses
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE RPBaselineConsultant
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE RPBaselineLabor
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE RPPlannedLabor
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE RPAssignment
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE RPPlannedExpenses
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE RPPlannedConsultant
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE RPExpense
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE RPConsultant
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE LD
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE BILD
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE LedgerMisc ENABLE CHANGE_TRACKING
WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE Clendor
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE VEAccounting
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE CLAddress
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE CA
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE CAB
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE CABDetail
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE UNTABLE
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE UNUNIT
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE LedgerAP
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE LedgerEX
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE Activity
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE CFGConsolidationCompanies
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

ALTER TABLE CFGConsolidationGroups
ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON)

END

ELSE BEGIN

ALTER TABLE consolidations
DISABLE CHANGE_TRACKING 

ALTER TABLE Eliminations
DISABLE CHANGE_TRACKING 

ALTER TABLE PRF
DISABLE CHANGE_TRACKING 

ALTER TABLE PR
DISABLE CHANGE_TRACKING 

ALTER TABLE ConsolidationBudgets
DISABLE CHANGE_TRACKING 

ALTER TABLE ConsolidationBudgetsDetail
DISABLE CHANGE_TRACKING 

ALTER TABLE Contacts
DISABLE CHANGE_TRACKING 

ALTER TABLE EMMain
DISABLE CHANGE_TRACKING 

ALTER TABLE EMCompany
DISABLE CHANGE_TRACKING 

ALTER TABLE RPPlan
DISABLE CHANGE_TRACKING 

ALTER TABLE RPTask
DISABLE CHANGE_TRACKING 

ALTER TABLE LedgerAR
DISABLE CHANGE_TRACKING 

ALTER TABLE AR
DISABLE CHANGE_TRACKING 

ALTER TABLE ContractCredit
DISABLE CHANGE_TRACKING 

ALTER TABLE Contracts
DISABLE CHANGE_TRACKING 

ALTER TABLE ContractDetails
DISABLE CHANGE_TRACKING 

ALTER TABLE LB
DISABLE CHANGE_TRACKING 

ALTER TABLE EB
DISABLE CHANGE_TRACKING 

ALTER TABLE RPBaselineExpenses
DISABLE CHANGE_TRACKING 

ALTER TABLE RPBaselineConsultant
DISABLE CHANGE_TRACKING 

ALTER TABLE RPBaselineLabor
DISABLE CHANGE_TRACKING 

ALTER TABLE RPPlannedLabor
DISABLE CHANGE_TRACKING 

ALTER TABLE RPAssignment
DISABLE CHANGE_TRACKING 

ALTER TABLE RPPlannedExpenses
DISABLE CHANGE_TRACKING 

ALTER TABLE RPPlannedConsultant
DISABLE CHANGE_TRACKING 

ALTER TABLE RPExpense
DISABLE CHANGE_TRACKING 

ALTER TABLE RPConsultant
DISABLE CHANGE_TRACKING 

ALTER TABLE LD
DISABLE CHANGE_TRACKING 

ALTER TABLE BILD
DISABLE CHANGE_TRACKING 

ALTER TABLE LedgerMisc 
DISABLE CHANGE_TRACKING

ALTER TABLE Clendor
DISABLE CHANGE_TRACKING 

ALTER TABLE VEAccounting
DISABLE CHANGE_TRACKING 

ALTER TABLE CLAddress
DISABLE CHANGE_TRACKING 

ALTER TABLE CA
DISABLE CHANGE_TRACKING 

ALTER TABLE CAB
DISABLE CHANGE_TRACKING 

ALTER TABLE CABDetail
DISABLE CHANGE_TRACKING 

ALTER TABLE UNTABLE
DISABLE CHANGE_TRACKING 

ALTER TABLE UNUNIT
DISABLE CHANGE_TRACKING 

ALTER TABLE LedgerAP
DISABLE CHANGE_TRACKING 

ALTER TABLE LedgerEX
DISABLE CHANGE_TRACKING 

ALTER TABLE Activity
DISABLE CHANGE_TRACKING 

ALTER TABLE CFGConsolidationCompanies
DISABLE CHANGE_TRACKING 

ALTER TABLE CFGConsolidationGroups
DISABLE CHANGE_TRACKING 

ALTER DATABASE CURRENT
SET CHANGE_TRACKING = OFF

END

END -- AnalysisChangeTracking
GO
