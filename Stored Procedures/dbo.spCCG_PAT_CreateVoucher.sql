SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_CreateVoucher] (
	@Company			Nvarchar(14)	,	-- For multicompany scenarios, use ' ' when only one.
	@UsePeriod			int,
	@UseBatch			Nvarchar(32),		
	@CSVPayableSeqs		varchar(max),			-- NULL when batching
	@Employee			Nvarchar(20),
	@User				Nvarchar(32),
	@VoucherDate		datetime,
	@DefaultProjAmtMsg	Nvarchar(max)		-- 'Exported from PAT' (old default)
)
AS
BEGIN
	--Copyright (c) 2020 EleVia Software. All rights reserved.
	--This script will create vouchers for ready payables
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF -- Until we know which field is being truncated, we need to turn off ANSI_WARNINGs [JAM]

	--update CCG_PAT_Payable set Batch = 'PAT TEST' where seq in (2,3)
	--exec spCCG_PAT_CreateVoucher ' ', 200506, '', '', '00001', 'ADMIN', '2005-06-01', ''
	--declare @Company		Nvarchar(14)
	--set @Company = ' '
	declare @debug				int	= 0
	declare @RunDate			datetime = getdate()
	declare @TodaysDate			datetime = CONVERT(datetime, CONVERT(Nvarchar(10), @RunDate, 101))
	declare @Batch				Nvarchar(32)
	--declare @MasterPKey		Nvarchar(32)
	declare @Period				int
	declare @GLAccountDefault	Nvarchar(13)
	declare @BankCodeDefault	Nvarchar(10)
	declare @StageExported		varchar(30)
	declare @RequireVoucher		char(1)
	declare @LastVoucher		decimal(19,4)
	declare @VoucherLen			int
	declare @MultiCurrency		char(1)
	declare @TaxAuditingEnabled char(1)
	declare @CompanyCurrency	Nvarchar(30)
	declare @UseInvForVoucherDate char(1)
	declare @Creator			Nvarchar(32)
	declare @Authorized			char(1)
	declare @UseBatchFile		char(1) = (case when isnull(@UseBatch,'') <> '' and isnull(@CSVPayableSeqs,'') = '' then 'Y' else 'N' end)
	declare @sql				nvarchar(max)

	--declare @CompanyTaxCode   Nvarchar(10)
	--declare @CompanyTax2Code   Nvarchar(10)

	select @StageExported = min(stage) from CCG_PAT_ConfigStages where SubType = 'Exported' and Status = 'A'--should only be one
	select @RequireVoucher = requireVoucherNumbers, @LastVoucher = lastvoucherAP from CFGSystem
	--select requireVoucherNumbers,lastvoucherAP,* from CFGSystem
	select @MultiCurrency = MulticurrencyEnabled from CFGSystem
	select @CompanyCurrency = functionalcurrencycode,@TaxAuditingEnabled = TaxAuditingEnabled
		--,@CompanyTaxCode = case when TaxAuditingEnabled = 'Y' then DefaultTaxCode else null end,
		--@CompanyTax2Code = case when SecondaryTaxEnabled = 'Y' then DefaultTax2Code else null end
		from cfgmaindata where company = @Company

	--select @Creator = ISNULL(UserName,'CCG_PAT') from SEUser where Employee = @Employee
	set @Creator = @User

	select @UseInvForVoucherDate = case when AdvancedOptions like N'%UseInvForVoucherDate%' then 'Y' when CCG_PAT_Config.VoucherDate = 'I' then 'Y' else 'N' end,
			@Authorized = case when AdvancedOptions like N'%ExportAuthorized%' then 'A' else 'N' end
		from CCG_PAT_Config

	if len(@UseBatch) > 0 set @Batch = @UseBatch

	if @UsePeriod > 200000
		set @Period = @UsePeriod
	else
		select @Period  = currentperiod from CFGSystem

	select @GLAccountDefault = GLAccountDefault from CCG_PAT_Config
	select @BankCodeDefault = BankCodeDefault from CCG_PAT_Config

	--added 5/17 for basic MC support.  Use the company of the default bank
	--select @Company = Company from CFGBanks where Code = @BankCodeDefault
	--starting 1.1 use company passed in parameter

	--select * from CFGBanks
	--update CCG_PAT_Config set BankCodeDefault = 'Corporate'
	--select * from CA
	--update CCG_PAT_Config set GLAccountDefault = '210.00'

	if @UseBatchFile = 'Y'
	begin 
		if @Batch IS NULL 
		begin
			SET ANSI_WARNINGS ON
			select -5 as ReturnVal, 'Batch Filename required' as ErrorMessage
			return 1
		end
		if @Batch in (select Batch from apControl)
		begin
			SET ANSI_WARNINGS ON
			select -5 as ReturnVal, 'Unique Filename required' as ErrorMessage
			return 1
		end
	end
	else if isnull(@CSVPayableSeqs, '') <> ''
	begin
		-- Assign PKeys to each payable item for using in Vision tables:		
		set @sql = N'
			update CCG_PAT_Payable set Batch = replace(newid(), ''-'', ''''), ModDate = '''+convert(varchar(40), @RunDate, 121)+''' where Seq in (' + @CSVPayableSeqs + ')'
		print @sql
		exec(@sql)
	end

	--set @MasterPKey =  + '_' + CONVERT(Nvarchar(2),DATEPART(YY, @RunDate)%100)  + CONVERT(Nvarchar(3),DATEPART(dy, @RunDate)) + CONVERT(Nvarchar(2),DATEPART(hh, @RunDate)) +  CONVERT(Nvarchar(2),DATEPART(mi, @RunDate))
	declare @transCount int = @@trancount

	BEGIN TRY
		if @transCount > 0 save transaction outertrans
		else begin transaction

		--assign voucher numbers if configured to do so in Vision, otherwise the user would have been required to enter these
		if @RequireVoucher = 'N'
		BEGIN
			select @VoucherLen = max(len(voucher)) from ledgerAP where voucher is not null and period in (select distinct top 2 period from CFGPostControl where TransType='AP' order by Period desc)
			if @VoucherLen is null select top 1 @VoucherLen = len(voucher) from ledgerAP where voucher is not null order by period desc
			--this is dependant on validation in PAT to avoid numbering a record that is not complete or valid for processing
			update p 
				set Voucher = assignedNum
				from CCG_PAT_Payable p
					inner join (
						select Seq, RIGHT('000000000000' + convert(Nvarchar(12),row_number() over(order by vendor, payabledate desc) + convert(bigint,@LastVoucher)),@VoucherLen) as assignedNum
							from CCG_PAT_Payable p
							--INNER JOIN CCG_PAT_ConfigStages s on CCG_PAT_Payable.stage = s.stage and SubType = 'Ready'
							where ((@UseBatchFile = 'Y' and p.Batch = @Batch) or (@UseBatchFile = 'N' and p.ModDate = @RunDate))
					) a on p.Seq = a.seq

			update CFGSystem set lastvoucherAP = (select max(voucher) from CCG_PAT_Payable p where ((@UseBatchFile = 'Y' and p.Batch = @Batch) or (@UseBatchFile = 'N' and p.ModDate = @RunDate)) and not voucher is null)
		END	

		------------------------------------------------------------------------------------------------
		-------------------------------- apDetail, apMaster, apControl ---------------------------------
		------------------------------------------------------------------------------------------------

		-- apMaster
		---------------------------------------------------------------
		print cast(getdate() as char(20)) + '	apMaster'
		
		insert into apMaster (Batch, MasterPKey, Vendor, InvoiceDate, Invoice, TransDate, LiabCode, BankCode, PayTerms, PayDate, Address,
				Posted, Seq, Voucher, CurrencyCode, PaymentCurrencyCode, Status, AuthorizedBy)
			select (case when @UseBatchFile = 'Y' then @Batch else p.Batch end), 
					convert(Nvarchar(32), p.seq), p.Vendor, p.payabledate,
					p.payablenumber as Invoice,
					case when @UseInvForVoucherDate = 'Y' then ISNULL(p.payabledate, @VoucherDate) else @VoucherDate end,
					case when p.LiabCode is null then liab.LiabCode else p.LiabCode end,
					case when p.BankCode is null then @BankCodeDefault else p.BankCode end,
					case when p.PayTerms is null then VEA.PayTerms else p.PayTerms end,
					p.PayDate,--consider adding logic to ensure payterms = 'Date' when this is not null
					case when not p.Address is null then  p.Address when not veaddress.Address is null then veaddress.Address  else minAddress.Address end
					, 'N', row_number() over(order by VE.Name,p.vendor,p.Voucher) as Seq, p.Voucher
					, case when @MultiCurrency = 'N' then ' ' else ISNULL(BankAcct.AccountCurrencyCode, @CompanyCurrency) end
					, case when @MultiCurrency = 'N' then ' ' else ISNULL(BankAcct.AccountCurrencyCode, @CompanyCurrency) end
					, @Authorized, case when @Authorized = 'A' then @Employee else null end
				FROM CCG_PAT_Payable p
					INNER JOIN VE on p.vendor = ve.vendor
					INNER JOIN VEAccounting vea on p.vendor = vea.vendor and vea.Company = @Company--if there is no vea record it is not approved for processing
					OUTER APPLY (select top 1 * from VEAddress where VEAddress.Vendor=p.Vendor and VEAddress.PrimaryInd='Y') VEAddress
					--LEFT JOIN VEAddress on VEAddress.Vendor=p.Vendor and VEAddress.PrimaryInd='Y'--assume there will only be on primary per vendor [we can't assume anymore on this one; JAM]
					LEFT JOIN (select vendor, min(address) as Address from veaddress group by vendor) minAddress on minAddress.Vendor = p.Vendor
					LEFT JOIN CFGVendorTypeAPLiability liab on ve.[category] = liab.[Type] and liab.Company = @Company
					LEFT JOIN CFGBanks on p.BankCode = CFGBanks.Code
					LEFT JOIN CA BankAcct on CFGBanks.Account = BankAcct.Account
					--INNER JOIN CCG_PAT_ConfigStages s on p.stage = s.stage and SubType = 'Ready'
				Where p.payabletype = 'I'
					and ((@UseBatchFile = 'Y' and p.Batch = @Batch)			--the UI will batch the records to be processed before calling this stored procedure
						or (@UseBatchFile = 'N' and p.ModDate = @RunDate))
				order by VE.Name, p.vendor	--changed to order by Name first per MH request

		if @@ERROR <> 0
		begin
			SET ANSI_WARNINGS ON
			select -3 as ReturnVal, ' (' + Cast(@@ERROR as Nvarchar(20)) + ')' as ErrorMessage
			if @transCount > 0 and XACT_STATE() <> -1 rollback transaction outertrans
			else if @transCount = 0 rollback transaction
			return 1
		end

		if @debug = 1 
			select m.* from apMaster m inner join CCG_PAT_Payable p on m.MasterPKey = convert(Nvarchar(32), p.seq) and m.Vendor = p.Vendor and m.Invoice = p.PayableNumber 
				where ((@UseBatchFile = 'Y' and p.Batch = @Batch) or (@UseBatchFile = 'N' and p.ModDate = @RunDate))

		-- apDetail
		---------------------------------------------------------------
		print cast(getdate() as char(20)) + '	apDetail'

		--temp table matching Vision 7.1
		CREATE TABLE [dbo].[#apDetail](
			[Batch] Nvarchar(32) COLLATE database_default NOT NULL ,
			[MasterPKey] Nvarchar(32) COLLATE database_default NOT NULL ,
			[FileID] [uniqueidentifier] NULL,
			[PKey] Nvarchar(44) COLLATE database_default NOT NULL,
			[Seq] int NOT NULL,
			[Description] Nvarchar(80) COLLATE database_default NULL,
			[WBS1] Nvarchar(30) COLLATE database_default NULL,
			[WBS2] Nvarchar(7) COLLATE database_default NULL,
			[WBS3] Nvarchar(7) COLLATE database_default NULL,
			[Account] Nvarchar(13) COLLATE database_default NULL,
			[Amount] [decimal](19, 4) NOT NULL,			-- This is (19, 5) in PAT. Could be our source of truncation [JAM]
			[SuppressBill] char(1) COLLATE database_default NOT NULL,
			--[TaxCode] Nvarchar(10) NULL,
			[NetAmount] [decimal](19, 4) NOT NULL,		-- This is (19, 5) in PAT. Could be our source of truncation [JAM]
			--[TaxAmount] [decimal](19, 4) NOT NULL,
			--[CurrencyExchangeOverrideRate] [decimal](19, 6) NOT NULL,
			--[PONumber] Nvarchar(30) NULL,
			--[PaymentExchangeRate] [decimal](19, 6) NOT NULL,
			[PaymentAmount] [decimal](19, 4) NOT NULL,
			--[PaymentExchangeInfo] Nvarchar(max) NULL,
			[ExpenseCode] Nvarchar(10) COLLATE database_default NULL,
			--[AmountProjectFunctionalCurrency] [decimal](19, 4) NOT NULL,
			--[TaxAmountProjectFunctionalCurrency] [decimal](19, 4) NOT NULL,
			--[Tax2Code] Nvarchar(10) NULL,
			--[Tax2Amount] [decimal](19, 4) NOT NULL,
			--[CompoundTax] char(1) NOT NULL,
		) ON [PRIMARY]

		insert into #apDetail (Batch, MasterPKey, FileID, PKey, Seq, Description, ExpenseCode, Account, Amount, SuppressBill,
				PaymentAmount, NetAmount, WBS1, WBS2, WBS3)
			select (case when @UseBatchFile = 'Y' then @Batch else p.Batch end), 
					convert(Nvarchar(32), p.seq), p.FileID,		
					isnull(pa.PKey, REPLACE(NewID(), '-', '')),  --ProjectAmount.PKey new in 4.1
					row_number() over(partition by p.Seq order by pa.Seq) as Seq,
					ISNULL(pa.[Description], @DefaultProjAmtMsg) as [Description],
					case when ISNULL(pa.ExpenseCode,'') = '' then null else pa.ExpenseCode end,
					case when not isnull(pa.GLAccount,'') = '' then pa.GLAccount when not isnull(p.GLAccount,'') = '' then p.GLAccount
						 when PR.chargeType = 'R' and not vea.RegAccount is null then vea.RegAccount
						 when PR.chargeType <> 'R' and not vea.OHAccount is null then vea.OHAccount
						 --consider account from liabcode settings as another default option
						 else @GLAccountDefault end as account , Amount, ISNULL(pa.suppressbill,'N'), Amount, NetAmount,
					case when pa.wbs1 is null or RTRIM(pa.wbs1) = '' then NULL else pa.wbs1 end as wbs1,--ensure null when no project info
					case when pa.wbs1 is null or RTRIM(pa.wbs1) = '' then NULL when pa.wbs2 is null or pa.wbs2 = '' then ' ' else pa.wbs2 end as wbs2,
					case when pa.wbs1 is null or RTRIM(pa.wbs1) = '' then NULL when pa.wbs3 is null or pa.wbs3 = '' then ' ' else pa.wbs3 end as wbs3
				from CCG_PAT_Payable p
					INNER JOIN CCG_PAT_ProjectAmount pa on p.seq = pa.payableseq
					LEFT JOIN PR on pa.wbs1 = PR.wbs1 and pa.wbs2 = pr.wbs2 and pa.wbs3 = pr.wbs3
					INNER JOIN VEAccounting vea on p.vendor = vea.vendor and vea.Company = @Company--if there is no vea record it is not approved for processing
					--INNER JOIN CCG_PAT_ConfigStages s on p.stage = s.stage and SubType = 'Ready'
				Where p.payabletype = 'I' 
					and ((@UseBatchFile = 'Y' and p.Batch = @Batch) or (@UseBatchFile = 'N' and p.ModDate = @RunDate))
				order by p.vendor

		if @@ERROR <> 0
		begin
			SET ANSI_WARNINGS ON
			select -4 as ReturnVal, ' (' + Cast(@@ERROR as Nvarchar(20)) + ')' as ErrorMessage
			if @transCount > 0 and XACT_STATE() <> -1 rollback transaction outertrans
			else if @transCount = 0 rollback transaction
			return 1
		end

		print '#apdetail'

		if @debug = 1 select * from #apDetail

		--insert common fields from temp table to main table
		insert into apDetail (Batch, MasterPKey, PKey, Seq, Description, ExpenseCode,Account, Amount, SuppressBill,
				PaymentAmount, NetAmount,WBS1,WBS2,WBS3)
			select Batch, MasterPKey, PKey, Seq, Description, ExpenseCode,Account, Amount, SuppressBill,
					PaymentAmount, NetAmount, WBS1, WBS2, WBS3
				from #apDetail 

		if @TaxAuditingEnabled = 'Y'
		begin
			print '@TaxAuditingEnabled'
			insert into apDetailTax (Batch,MasterPKey,PKey,TaxCode,TaxAmount,TaxAmountProjectFunctionalCurrency,Seq) 
				select Batch,MasterPKey,patax.PKey,patax.TaxCode,patax.TaxAmount,patax.TaxAmount,patax.Seq 
					from #apDetail inner join CCG_PAT_ProjectAmountTax patax on patax.PKey = #apdetail.PKey 
					where ISNULL(patax.TaxCode,'') <> ''
		end --taxcodes

		if @debug = 1 select d.* from #apDetail d0 inner join apDetail d on d.Batch = d0.Batch and d.MasterPKey = d0.MasterPKey and d.PKey = d0.PKey and d.Seq = d0.Seq 

		-- apControl
		---------------------------------------------------------------
		print cast(getdate() as char(20)) + '	apControl'

		--Creator is userid, we only have employee so leave as CCG_PAT or consider looking up user
		insert into apControl (Batch, PostPeriod, PostSeq, Recurring, Selected, Posted, Creator, Period, EndDate, Total,
				DefaultLiab, DefaultBank, DefaultDate, Company, DefaultCurrencyCode)
			select d.Batch, 0 as PostPeriod, 0 as PostSeq, 'N' as Recurring, 'N' as Selected, 'N' as Posted, @Creator as Creator,
					@Period, max(m.TransDate) as EndDate, sum(d.Amount) as Total, max(m.LiabCode), max(m.BankCode), @VoucherDate, @Company, case when @MultiCurrency = 'N' then ' ' else MAX(m.CurrencyCode) end
				from #apDetail d0
					inner join apDetail d on d.Batch = d0.Batch and d.MasterPKey = d0.MasterPKey and d.PKey = d0.PKey 
					inner join apMaster m on d.Batch = m.Batch and d.MasterPKey = m.MasterPKey
				group by d.Batch

		if @@ERROR <> 0
		begin
			select -2 as ReturnVal, ' (' + Cast(@@ERROR as Nvarchar(20)) + ')' as ErrorMessage
			if @transCount > 0 and XACT_STATE() <> -1 rollback transaction outertrans
			else if @transCount = 0 rollback transaction
			return 1
		end

		if @TaxAuditingEnabled = 'Y' 
		begin
			if @UseBatchFile = 'Y'
				insert into apControlDefaultTaxCodes 
					select @Batch, x.TaxCode, x.Seq from CFGMainDataDefaultTaxCodes x where x.Company = @Company 
			else
				insert into apControlDefaultTaxCodes 
					select p.Batch, x.TaxCode, x.Seq 
						from CFGMainDataDefaultTaxCodes x 
							outer apply CCG_PAT_Payable p 
						where x.Company = @Company and not exists (select 1 from apControlDefaultTaxCodes c0 where c0.Batch = p.Batch and c0.TaxCode = x.TaxCode and c0.Seq = x.Seq)
							and ((@UseBatchFile = 'Y' and p.Batch = @Batch) or (@UseBatchFile = 'N' and p.ModDate = @RunDate))					
		end

		if exists (select 'x' from syscolumns a,sysobjects b where a.id = b.id and a.name = 'Description' and b.name = 'apControl')
		begin
			set @sql = N'
				update c set Description = p.Batch 
					from CCG_PAT_Payable p 
						inner join apControl c on c.Batch = p.Batch where '+(case when @UseBatchFile = 'Y' then 'p.Batch = '''+@Batch+'''' else 'p.ModDate = '''+convert(varchar(40),@RunDate,121)+'''' end)
			exec(@sql)
		end

		if @debug = 1 select c.* from CCG_PAT_Payable p inner join apControl c on c.Batch = p.Batch where ((@UseBatchFile = 'Y' and p.Batch = @Batch) or (@UseBatchFile = 'N' and p.ModDate = @RunDate)) order by p.Batch
		
		---TDM
		IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[APDocuments]') AND type in (N'U'))
		BEGIN
			print cast(getdate() as char(20)) + '	APDocuments'

			insert into APDocuments (Batch, MasterPKey, FileID, AssociateNew, Seq)
				select p.Batch, convert(Nvarchar(32), p.seq), p.FileID, 'Y', 1
					from CCG_PAT_Payable p
						inner join VE on p.vendor = ve.vendor
						inner join VEAccounting vea on p.vendor = vea.vendor and vea.Company = @Company--if there is no vea record it is not approved for processing
					Where p.payabletype = 'I'
						and ((@UseBatchFile = 'Y' and p.Batch = @Batch) or (@UseBatchFile = 'N' and p.ModDate = @RunDate))
						and p.FileID is not NULL
					order by VE.Name, p.vendor

			if @debug = 1 
				select dc.* from CCG_PAT_Payable p inner join APDocuments dc on dc.Batch = p.Batch and convert(Nvarchar(32), p.seq) = dc.MasterPKey and dc.FileID = p.FileID 
					where ((@UseBatchFile = 'Y' and p.Batch = @Batch) or (@UseBatchFile = 'N' and p.ModDate = @RunDate)) order by MasterPKey

			insert into APDocumentsDetail (Batch, MasterPKey, FileID, DetailPKey)
				select dc.Batch, dc.MasterPKey, dc.FileID, d.PKey
					from CCG_PAT_Payable p
						inner join APDocuments dc on dc.Batch = p.Batch and convert(Nvarchar(32), p.seq) = dc.MasterPKey and dc.FileID = p.FileID
						inner join apDetail d on dc.Batch = d.Batch and dc.MasterPKey = d.MasterPKey
					where ((@UseBatchFile = 'Y' and p.Batch = @Batch) or (@UseBatchFile = 'N' and p.ModDate = @RunDate))

			if @debug = 1 
				select dd.* from CCG_PAT_Payable p inner join APDocumentsDetail dd on dd.Batch = p.Batch and convert(Nvarchar(32), p.seq) = dd.MasterPKey and dd.FileID = p.FileID 
					where ((@UseBatchFile = 'Y' and p.Batch = @Batch) or (@UseBatchFile = 'N' and p.ModDate = @RunDate)) order by MasterPKey, DetailPKey
		END

		print cast(getdate() as char(20)) + '	Done'

		------------------------------------------------------------------------------------------------
		------------------------------- Update PAT Tables -------------------------------
		------------------------------------------------------------------------------------------------

		if NOT @StageExported IS NULL
		BEGIN
			INSERT INTO CCG_PAT_History (PayableSeq, Stage, ActionDate, ActionTaken, ActionTakenBy, [Description], PriorStage, PriorStageDateSet)
				SELECT p.Seq, @StageExported, getutcdate(), N'Stage Change', @Employee, N'Exported', Stage, dbo.fnCCG_PAT_GetPriorStageChangeActionDateForPayable(p.Seq, getutcdate())
					from CCG_PAT_Payable p
						INNER JOIN apMaster v on p.Batch = v.Batch and convert(Nvarchar(32), p.seq) = v.MasterPKey
					WHERE ((@UseBatchFile = 'Y' and p.Batch = @Batch) or (@UseBatchFile = 'N' and p.ModDate = @RunDate))
						and Stage in (SELECT stage FROM CCG_PAT_CONFIGSTAGES WHERE SUBTYPE ='Ready')		--only update when was in ready stage to allow pending items to continue to be processed

			UPDATE p 
				set Stage = @StageExported, ModUser = @Employee, ModDate = (case when @UseBatchFile = 'Y' then GetUTCDate() else p.ModDate end)
				FROM CCG_PAT_Payable p
					INNER JOIN apMaster v on p.Batch = v.Batch and convert(Nvarchar(32), p.seq) = v.MasterPKey
				WHERE ((@UseBatchFile = 'Y' and p.Batch = @Batch) or (@UseBatchFile = 'N' and p.ModDate = @RunDate))
					and Stage in (SELECT stage FROM CCG_PAT_CONFIGSTAGES WHERE SUBTYPE = 'Ready')		--only update when was in ready stage to allow pending items to continue to be processed
		END

		INSERT INTO CCG_PAT_History (PayableSeq, ActionDate, ActionTaken, ActionTakenBy, [Description])
			SELECT p.Seq, @RunDate, N'Exported', @Employee, p.Batch
				from CCG_PAT_Payable p
			WHERE ((@UseBatchFile = 'Y' and p.Batch = @Batch) or (@UseBatchFile = 'N' and p.ModDate = @RunDate))

		------------------------------------------------------------------------------------------------
		------------------------------- Add'l Error Checking and Results -------------------------------
		------------------------------------------------------------------------------------------------

		-- Return the number of batches found and a return value
		SET ANSI_WARNINGS ON

		if @UseBatchFile = 'Y'
			select count(*) as ReturnVal, @StageExported as ErrorMessage from apControl where Batch=@Batch
		else
			select count(*) as ReturnVal, @StageExported as ErrorMessage from CCG_PAT_Payable p inner join apControl c on c.Batch = p.Batch where p.ModDate = @RunDate

		if @debug = 0 
		begin 
			if @transCount = 0 commit transaction
		end
		else begin
			if @transCount > 0 and XACT_STATE() <> -1 rollback transaction outertrans
			else if @transCount = 0 rollback transaction
		end
	END TRY
	BEGIN CATCH
		SET ANSI_WARNINGS ON

		-- SQL found some error - return the message and number:
		select -1 as ReturnVal, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage
		if @transCount > 0 and XACT_STATE() <> -1 rollback transaction outertrans
		else if @transCount = 0 rollback transaction
	END CATCH
END
GO
