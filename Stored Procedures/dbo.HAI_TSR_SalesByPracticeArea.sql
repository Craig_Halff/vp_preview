SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[HAI_TSR_SalesByPracticeArea]
	@CompanyId VARCHAR(2),
	@Year INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SET TRANSACTION isolation level READ uncommitted; 

IF EXISTS (SELECT NAME 
           FROM   sysobjects 
           WHERE  NAME = 'setContextInfo' 
                  AND type = 'P') 
  EXECUTE dbo.Setcontextinfo 
    @StrCompany = '01', 
    @StrAuditingEnabled = 'N', 
    @strUserName = 'HIT', 
    @StrCultureName = 'en-US'; 

SELECT Isnull(Substring(pr.org, 1, 2), '') 
       AS 
       group1, 
       groupCFGOrgCodes1.label 
       AS groupDesc1, 
       Isnull(projects_sales.custsalesteam, '') 
       AS group2, 
       Organization_Projects_Sales_CustSalesTeam.NAME 
       AS groupDesc2, 
       Isnull(Isnull(pr.NAME, '') + pr.wbs1, '') 
       AS group3, 
       projects_sales.custsalesdate 
       AS gridUDCol_Projects_Sales_CustSalesDate, 
       Isnull(projects_sales.custsalesamount, 0.00) 
       AS 
       gridUDCol_Projects_Sales_CustSalesAmount, 
       LEVEL3.wbs1 
       AS WBS1, 
       LEVEL3.wbs2 
       AS WBS2, 
       LEVEL3.wbs3 
       AS WBS3, 
       LEVEL3.wbs1 + LEVEL3.wbs2 + LEVEL3.wbs3 
       AS PKey, 
       LEVEL3.NAME 
       AS NAME, 
       LEVEL2.NAME 
       AS PHName, 
       LEVEL3.NAME 
       AS TKName, 
       /***STARTCurrencyColumns***/ 
       '' 
       AS currencyCodePres, 
       '' 
       AS currencyCodeProj, 
       '' 
       AS currencyCodeBill, 
       '' 
       AS currencyCodeFunct, 
       /***ENDCurrencyColumns***/ 
       cfgchargetype.label 
       AS ChargeType, 
       LEVEL3.sublevel 
       AS SubLevel, 
       LEVEL3.principal 
       AS Principal, 
       EMPrin.firstname 
       AS prinFirstName, 
       EMPrin.lastname 
       AS prinLastName, 
       '' 
       AS prinName, 
       LEVEL3.projmgr 
       AS ProjMgr, 
       EMMgr.firstname 
       AS prgFirstName, 
       EMMgr.lastname 
       AS prgLastName, 
       '' 
       AS prgName, 
       LEVEL3.supervisor 
       AS Supervisor, 
       Supv.firstname 
       AS supFirstName, 
       Supv.lastname 
       AS supLastName, 
       '' 
       AS supName, 
       LEVEL3.biller 
       AS Biller, 
       EMBiller.firstname 
       AS billerFirstName, 
       EMBiller.lastname 
       AS billerLastName, 
       '' 
       AS billerName, 
       pr.servprocode 
       AS ServiceProfileCode, 
       cfgserviceprofile.description 
       AS ServiceProfileDesc, 
       LEVEL3.tlprojectname 
       AS TLProjectName, 
       LEVEL3.address1 
       AS PRAddress1, 
       LEVEL3.address2 
       AS PRAddress2, 
       LEVEL3.address3 
       AS PRAddress3, 
       LEVEL3.city 
       AS PRCity, 
       LEVEL3.state 
       AS PRState, 
       LEVEL3.zip 
       AS PRZip, 
       LEVEL3.county 
       AS PRCounty, 
       fw_cfgcountry.country 
       AS PRCountry, 
       LEVEL3.phone 
       AS PRPhone, 
       LEVEL3.fax 
       AS PRFax, 
       LEVEL3.email 
       AS PREmail, 
       LEVEL3.clientid 
       AS ClientID, 
       cl.NAME 
       AS ClientName, 
       LEVEL3.claddress 
       AS CLAddress, 
       CLAddress_cl.address1 
       AS CLAddress1, 
       CLAddress_cl.address2 
       AS CLAddress2, 
       CLAddress_cl.address3 
       AS CLAddress3, 
       CLAddress_cl.address4 
       AS CLAddress4, 
       CLAddress_cl.city 
       AS CLCity, 
       CLAddress_cl.state 
       AS CLState, 
       CLAddress_cl.zip 
       AS CLZip, 
       CL_CFGCountry.country 
       AS CLCountry, 
       contacts.phone 
       AS CLPhone, 
       contacts.fax 
       AS CLFax, 
       contacts.email 
       AS CLEMail, 
       LEVEL3.org 
       AS Org, 
       organization.NAME 
       AS OrgName, 
       cfgorganizationstatus.label 
       AS OrganizationStatus, 
       /* When there are multiple grid type rows, populate standard fee/contract columns for the first row only to avoid multiplying total*/
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.fee/*strPresentationCurrencyRate_Project*/ ) 
         ELSE 0 
       END 
       AS Fee, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.reimballow/*strPresentationCurrencyRate_Project*/ ) 
         ELSE 0 
       END 
       AS ReimbAllow, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.consultfee/*strPresentationCurrencyRate_Project*/ ) 
         ELSE 0 
       END 
       AS ConsultFee, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.feedirlab/*strPresentationCurrencyRate_Project*/ ) 
         ELSE 0 
       END 
       AS FeeDirLab, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.feedirexp/*strPresentationCurrencyRate_Project*/ ) 
         ELSE 0 
       END 
       AS FeeDirExp, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.reimballowexp/*strPresentationCurrencyRate_Project*/ ) 
         ELSE 0 
       END 
       AS ReimbAllowExp, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.reimballowcons/*strPresentationCurrencyRate_Project*/ ) 
         ELSE 0 
       END 
       AS ReimbAllowCons, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.feebillingcurrency/*strPresentationCurrencyRate_Billing*/ ) 
         ELSE 0 
       END 
       AS FeeBillingCurrency, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.reimballowbillingcurrency 
         /*strPresentationCurrencyRate_Billing*/ ) 
         ELSE 0 
       END 
       AS ReimbAllowBillingCurrency, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.consultfeebillingcurrency 
         /*strPresentationCurrencyRate_Billing*/ ) 
         ELSE 0 
       END 
       AS ConsultFeeBillingCurrency, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.feedirlabbillingcurrency 
         /*strPresentationCurrencyRate_Project*/ ) 
         ELSE 0 
       END 
       AS FeeDirLabBillingCurrency, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.feedirexpbillingcurrency 
         /*strPresentationCurrencyRate_Project*/ ) 
         ELSE 0 
       END 
       AS FeeDirExpBillingCurrency, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.reimballowexpbillingcurrency 
         /*strPresentationCurrencyRate_Project*/ ) 
         ELSE 0 
       END 
       AS ReimbAllowExpBillingCurrency, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.reimballowconsbillingcurrency 
         /*strPresentationCurrencyRate_Project*/ 
         ) 
         ELSE 0 
       END 
       AS ReimbAllowConsBillingCurrency, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.feefunctionalcurrency 
         /*strPresentationCurrencyRate_Functional*/ ) 
         ELSE 0 
       END 
       AS FeeFunctionalCurrency, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.reimballowfunctionalcurrency 
         /*strPresentationCurrencyRate_Functional*/ 
         ) 
         ELSE 0 
       END 
       AS ReimbAllowFunctionalCurrency, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.consultfeefunctionalcurrency 
         /*strPresentationCurrencyRate_Functional*/ 
         ) 
         ELSE 0 
       END 
       AS ConsultFeeFunctionalCurrency, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.feedirlabfunctionalcurrency 
         /*strPresentationCurrencyRate_Project*/ ) 
         ELSE 0 
       END 
       AS FeeDirLabFunctionalCurrency, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.feedirexpfunctionalcurrency 
         /*strPresentationCurrencyRate_Project*/ ) 
         ELSE 0 
       END 
       AS FeeDirExpFunctionalCurrency, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.reimballowexpfunctionalcurrency 
         /*strPresentationCurrencyRate_Project*/ 
         ) 
         ELSE 0 
       END 
       AS ReimbAllowExpFunctionalCurrency, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.reimballowconsfunctionalcurrency 
         /*strPresentationCurrencyRate_Project*/ 
         ) 
         ELSE 0 
       END 
       AS ReimbAllowConsFunctionalCurrency, 
       CASE 
         WHEN LEVEL3.sublevel = 'N' THEN LEVEL3.budohrate 
         ELSE 0 
       END 
       AS BudOHRate, 
       LEVEL3.status 
       AS Status, 
       cfgprojectstatus.description 
       AS StatusDesc, 
       CASE 
         WHEN LEVEL3.sublevel = 'N' THEN LEVEL3.revtype 
         ELSE '' 
       END 
       AS RevType, 
       CASE 
         WHEN LEVEL3.sublevel = 'N' THEN cfgrgmethods.description 
         ELSE '' 
       END 
       AS RevTypeDesc, 
       CASE 
         WHEN LEVEL3.sublevel = 'N' THEN LEVEL3.multamt 
         ELSE 0 
       END 
       AS MultAmt, 
       LEVEL3.unittable 
       AS UnitTable, 
       LEVEL3.startdate 
       AS StartDate, 
       LEVEL3.enddate 
       AS EndDate, 
       LEVEL3.pctcomp 
       AS PctComp, 
       LEVEL3.labpctcomp 
       AS LabPctComp, 
       LEVEL3.exppctcomp 
       AS ExpPctComp, 
       LEVEL3.billbydefault 
       AS BillByDefault, 
       Isnull(apexpensecodeot.tablename, '') 
       AS billbydefaultortablename, 
       CASE 
         WHEN LEVEL3.billbydefaultconsultants = 'Y' THEN 'Yes' 
         WHEN LEVEL3.billbydefaultconsultants = 'N' THEN 'No' 
         WHEN LEVEL3.billbydefaultconsultants = 'E' THEN 'Expense Code' 
       END 
       AS billbydefaultconsultants, 
       CASE 
         WHEN LEVEL3.billbydefaultotherexp = 'Y' THEN 'Yes' 
         WHEN LEVEL3.billbydefaultotherexp = 'N' THEN 'No' 
         WHEN LEVEL3.billbydefaultotherexp = 'E' THEN 'Expense Code' 
       END 
       AS billbydefaultotherexp, 
       LEVEL3.billablewarning 
       AS BillableWarning, 
       LEVEL3.budgetedflag 
       AS BudgetedFlag, 
       LEVEL3.budgetedlevels 
       AS BudgetedLevels, 
       CASE LEVEL3.sublevel 
         WHEN 'N' THEN LEVEL3.tkcheckrpdate 
         ELSE '' 
       END 
       AS TKCheckRPDate, 
       CASE LEVEL3.sublevel 
         WHEN 'N' THEN LEVEL3.tkcheckrpplannedhrs 
         ELSE '' 
       END 
       AS TKCheckRPPlannedHrs, 
       LEVEL3.billwbs1 
       AS BillWBS1, 
       LEVEL3.billwbs2 
       AS BillWBS2, 
       LEVEL3.billwbs3 
       AS BillWBS3, 
       LEVEL3.xcharge 
       AS XCharge, 
       LEVEL3.xchargemethod 
       AS XChargeMethod, 
       LEVEL3.xchargemult 
       AS XChargeMult, 
       LEVEL3.description 
       AS Description, 
       LEVEL3.closed 
       AS Closed, 
       LEVEL3.readonly 
       AS ReadOnly, 
       LEVEL3.defaulteffortdriven 
       AS DefaultEffortDriven, 
       LEVEL3.defaulttasktype 
       AS DefaultTaskType, 
       LEVEL3.versionid 
       AS VersionID, 
       LEVEL3.contactid 
       AS ContactID, 
       contacts.firstname 
       AS ContactFirstName, 
       contacts.lastname 
       AS ContactLastName, 
       '' 
       AS ContactName, 
       LEVEL3.clbillingaddr 
       AS CLBillingAddr, 
       LEVEL3.longname 
       AS LongName, 
       clAddress_bl.address1 
       AS CLBAddress1, 
       clAddress_bl.address2 
       AS CLBAddress2, 
       clAddress_bl.address3 
       AS CLBAddress3, 
       clAddress_bl.address4 
       AS CLBAddress4, 
       clAddress_bl.city 
       AS CLBCity, 
       clAddress_bl.state 
       AS CLBState, 
       clAddress_bl.zip 
       AS CLBZip, 
       CLB_CFGCountry.country 
       AS CLBCountry, 
       billContacts.phone 
       AS CLBPhone, 
       billContacts.fax 
       AS CLBFax, 
       billContacts.email 
       AS CLBEMail, 
       LEVEL3.federalind 
       AS FederalInd, 
       LEVEL3.projecttype 
       AS ProjectType, 
       cfgprojecttype.description 
       AS projectTypeDesc, 
       LEVEL3.responsibility 
       AS Responsibility, 
       cfgprresponsibility.description 
       AS ResponsibilityDesc, 
       LEVEL3.referable 
       AS Referable, 
       LEVEL3.estcompletiondate 
       AS EstCompletionDate, 
       LEVEL3.actcompletiondate 
       AS ActCompletionDate, 
       LEVEL3.contractdate 
       AS ContractDate, 
       LEVEL3.constcompldate 
       AS ConstComplDate, 
       LEVEL3.profservicescompldate 
       AS ProfServicesComplDate, 
       LEVEL3.biddate 
       AS BidDate, 
       LEVEL3.compldatecomment 
       AS ComplDateComment, 
       LEVEL3.firmcost /*strPresentationCurrencyRate_Project*/ 
       AS FirmCost, 
       LEVEL3.firmcostcomment 
       AS FirmCostComment, 
       CASE 
         WHEN ( Row_number() 
                  OVER( 
                    partition BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3 
                    ORDER BY LEVEL3.wbs1, LEVEL3.wbs2, LEVEL3.wbs3) ) = 1 THEN 
         ( LEVEL3.totalprojectcost/*strPresentationCurrencyRate_Project*/ ) 
         ELSE 0 
       END 
       AS TotalProjectCost, 
       LEVEL3.totalcostcomment 
       AS TotalCostComment, 
       LEVEL3.ajerabilledconsultant /*strPresentationCurrencyRate_Project*/ 
       AS 
       AjeraBilledConsultant, 
       LEVEL3.ajerabilledlabor /*strPresentationCurrencyRate_Project*/ 
       AS 
       AjeraBilledLabor, 
       LEVEL3.ajerabilledreimbursable /*strPresentationCurrencyRate_Project*/ 
       AS 
       AjeraBilledReimbursable, 
       LEVEL3.ajeracostconsultant /*strPresentationCurrencyRate_Project*/ 
       AS 
       AjeraCostConsultant, 
       LEVEL3.ajeracostlabor /*strPresentationCurrencyRate_Project*/ 
       AS AjeraCostLabor, 
       LEVEL3.ajeracostreimbursable /*strPresentationCurrencyRate_Project*/ 
       AS 
       AjeraCostReimbursable, 
       LEVEL3.ajerareceivedconsultant /*strPresentationCurrencyRate_Project*/ 
       AS 
       AjeraReceivedConsultant, 
       LEVEL3.ajerareceivedlabor /*strPresentationCurrencyRate_Project*/ 
       AS 
       AjeraReceivedLabor, 
       LEVEL3.ajerareceivedreimbursable /*strPresentationCurrencyRate_Project*/ 
       AS 
       AjeraReceivedReimbursable, 
       LEVEL3.ajeraspentconsultant /*strPresentationCurrencyRate_Project*/ 
       AS 
       AjeraSpentConsultant, 
       LEVEL3.ajeraspentlabor /*strPresentationCurrencyRate_Project*/ 
       AS 
       AjeraSpentLabor, 
       LEVEL3.ajeraspentreimbursable /*strPresentationCurrencyRate_Project*/ 
       AS 
       AjeraSpentReimbursable, 
       LEVEL3.ajerawipconsultant /*strPresentationCurrencyRate_Project*/ 
       AS 
       AjeraWIPConsultant, 
       LEVEL3.ajerawiplabor /*strPresentationCurrencyRate_Project*/ 
       AS AjeraWIPLabor, 
       LEVEL3.ajerawipreimbursable/*strPresentationCurrencyRate_Project*/ 
       AS 
       AjeraWIPReimbursable, 
       LEVEL3.opportunityid 
       AS OpportunityID, 
       opportunity.NAME 
       AS OpportunityDesc, 
       LEVEL3.clientconfidential 
       AS ClientConfidential, 
       LEVEL3.clientalias 
       AS ClientAlias, 
       LEVEL3.billingclientid 
       AS BillingClientID, 
       billCL.NAME 
       AS BillClientName, 
       LEVEL3.billingcontactid 
       AS BillingContactID, 
       billContacts.lastname 
       AS billContactLastName, 
       billContacts.firstname 
       AS billContactFirstName, 
       '' 
       AS billContactName, 
       LEVEL3.proposalwbs1 
       AS ProposalWBS1, 
       rpplan.planname 
       AS PlanName, 
       LEVEL3.createuser 
       AS CreateUser, 
       Dateadd("hh", CONVERT(INT, '-5'), ( LEVEL3.createdate )) 
       AS CreateDate, 
       LEVEL3.moduser 
       AS ModUser, 
       Dateadd("hh", CONVERT(INT, '-5'), ( LEVEL3.moddate )) 
       AS ModDate, 
       /***START MEMO***/'' 
       AS LMEMO,/***END MEMO***/ 
       LEVEL3.costratemeth 
       AS CostRateMeth, 
       CASE 
         WHEN LEVEL3.costratemeth = 0 THEN '' 
         ELSE 
           CASE 
             WHEN LEVEL3.costratemeth = 1 THEN costrate.tablename 
             ELSE 
               CASE 
                 WHEN LEVEL3.costratemeth = 2 THEN costrateC.tablename 
                 ELSE costrateL.tablename 
               END 
           END 
       END 
       AS CostRateTableNo, 
       LEVEL3.payratemeth 
       AS PayRateMeth, 
       CASE 
         WHEN LEVEL3.payratemeth = 0 THEN '' 
         ELSE 
           CASE 
             WHEN LEVEL3.payratemeth = 1 THEN Payrate.tablename 
             ELSE 
               CASE 
                 WHEN LEVEL3.payratemeth = 2 THEN payrateC.tablename 
                 ELSE payrateL.tablename 
               END 
           END 
       END 
       AS PayRateTableNo, 
       LEVEL3.locale 
       AS Locale, 
       LEVEL3.lineitemapproval 
       AS LineItemApproval, 
       LEVEL3.lineitemapprovalek 
       AS LineItemApprovalEK, 
       LEVEL3.budgetsource 
       AS BudgetSource, 
       LEVEL3.budgetlevel 
       AS BudgetLevel, 
       LEVEL3.revupsetlimits 
       AS RevUpsetLimits, 
       LEVEL3.revupsetwbs2 
       AS RevUpsetWBS2, 
       LEVEL3.revupsetwbs3 
       AS RevUpsetWBS3, 
       LEVEL3.revupsetincludecomp 
       AS RevUpsetIncludeComp, 
       LEVEL3.revupsetincludecons 
       AS RevUpsetIncludeCons, 
       LEVEL3.revupsetincludereimb 
       AS RevUpsetIncludeReimb, 
       LEVEL3.proposalwbs1 
       AS proposalWBS1, 
       CLOwner.client 
       AS OwnerNumber, 
       CLOwner.NAME 
       AS OwnerName, 
       CLAddressOwner.address 
       AS OwnerAddress, 
       CLAddressOwner.address1 
       AS OwnerAddress1, 
       CLAddressOwner.address2 
       AS OwnerAddress2, 
       CLAddressOwner.address3 
       AS OwnerAddress3, 
       CLAddressOwner.address4 
       AS OwnerAddress4, 
       CLAddressOwner.city 
       AS OwnerCity, 
       CLAddressOwner.state 
       AS OwnerState, 
       CLAddressOwner.zip 
       AS OwnerZip, 
       Owner_CFGCountry.country 
       AS OwnerCountry, 
       LEVEL3.requirecomments 
       AS RequireComments, 
       LEVEL3.availableforcrm 
       AS AvailableForCRM, 
       LEVEL3.readyforapproval 
       AS ReadyForApproval, 
       LEVEL3.readyforprocessing 
       AS ReadyForProcessing, 
       LEVEL3.restrictchargecompanies 
       AS RestrictChargeCompanies, 
       CASE LEVEL3.icbillinglab 
         WHEN 'P' THEN 'Project' 
         WHEN 'G' THEN 'Global' 
       END 
       AS ICBillingLab, 
       CASE LEVEL3.icbillinglabmethod 
         WHEN 0 THEN 'Multiplier' 
         WHEN 1 THEN 'Billing Terms' 
         WHEN 2 THEN 'Rate Table' 
         WHEN 3 THEN 'By Category' 
         WHEN 4 THEN 'By Labor Code' 
         ELSE '' 
       END 
       AS ICBillingLabMethod, 
       LEVEL3.icbillinglabtableno, 
       Ltrim(Str(LEVEL3.icbillinglabmult, 10, 2)) 
       AS ICBillingLabMult, 
       CASE LEVEL3.icbillingexp 
         WHEN 'P' THEN 'Project' 
         WHEN 'G' THEN 'Global' 
       END 
       AS ICBillingExp, 
       CASE LEVEL3.icbillingexpmethod 
         WHEN 0 THEN 'Multiplier' 
         WHEN 1 THEN 'Billing Terms' 
         WHEN 5 THEN 'By Account' 
         WHEN 6 THEN 'By Category' 
         WHEN 7 THEN 'By Vendor' 
         ELSE '' 
       END 
       AS ICBillingExpMethod, 
       LEVEL3.icbillingexptableno, 
       Ltrim(Str(LEVEL3.icbillingexpmult, 10, 2)) 
       AS ICBillingExpMult, 
       cfgmaindata.country 
       AS CompanyCountry, 
       CASE 
         WHEN CL_CFGCountry.displaystate IS NULL THEN 
           CASE 
             WHEN CL_StateQuery.displaystate IS NULL THEN 'C' 
             ELSE CL_StateQuery.displaystate 
           END 
         ELSE CL_CFGCountry.displaystate 
       END 
       AS CL_DisplayState, 
       CASE 
         WHEN CL_CFGCountry.addressformat IS NULL THEN 
           CASE 
             WHEN CL_StateQuery.addressformat IS NULL THEN 
               CASE 
                 WHEN cfgmaindata.defaultaddressformat IS NULL THEN '1' 
                 ELSE cfgmaindata.defaultaddressformat 
               END 
             ELSE CL_StateQuery.addressformat 
           END 
         ELSE CL_CFGCountry.addressformat 
       END 
       AS CL_AddressFormat, 
       Isnull(CL_CFGStates.description, '') 
       AS CL_StateDesc, 
       CASE 
         WHEN CLB_CFGCountry.displaystate IS NULL THEN 
           CASE 
             WHEN CLB_StateQuery.displaystate IS NULL THEN 'C' 
             ELSE CLB_StateQuery.displaystate 
           END 
         ELSE CLB_CFGCountry.displaystate 
       END 
       AS CLB_DisplayState, 
       CASE 
         WHEN CLB_CFGCountry.addressformat IS NULL THEN 
           CASE 
             WHEN CLB_StateQuery.addressformat IS NULL THEN 
               CASE 
                 WHEN cfgmaindata.defaultaddressformat IS NULL THEN '1' 
                 ELSE cfgmaindata.defaultaddressformat 
               END 
             ELSE CLB_StateQuery.addressformat 
           END 
         ELSE CLB_CFGCountry.addressformat 
       END 
       AS CLB_AddressFormat, 
       Isnull(CLB_CFGStates.description, '') 
       AS CLB_StateDesc, 
       CASE 
         WHEN Owner_CFGCountry.displaystate IS NULL THEN 
           CASE 
             WHEN Owner_StateQuery.displaystate IS NULL THEN 'C' 
             ELSE Owner_StateQuery.displaystate 
           END 
         ELSE Owner_CFGCountry.displaystate 
       END 
       AS Owner_DisplayState, 
       CASE 
         WHEN Owner_CFGCountry.addressformat IS NULL THEN 
           CASE 
             WHEN Owner_StateQuery.addressformat IS NULL THEN 
               CASE 
                 WHEN cfgmaindata.defaultaddressformat IS NULL THEN '1' 
                 ELSE cfgmaindata.defaultaddressformat 
               END 
             ELSE Owner_StateQuery.addressformat 
           END 
         ELSE Owner_CFGCountry.addressformat 
       END 
       AS Owner_AddressFormat, 
       Isnull(Owner_CFGStates.description, '') 
       AS Owner_StateDesc 
FROM   pr 
       LEFT JOIN projectcustomtabfields AS GROUPWBS1_ProjectCustomTabFields 
              ON ( pr.wbs1 = GROUPWBS1_ProjectCustomTabFields.wbs1 
                   AND pr.wbs2 = GROUPWBS1_ProjectCustomTabFields.wbs2 
                   AND pr.wbs3 = GROUPWBS1_ProjectCustomTabFields.wbs3 ) 
       LEFT JOIN projects_sales 
              ON ( pr.wbs1 = projects_sales.wbs1 
                   AND projects_sales.wbs2 = ' ' 
                   AND projects_sales.wbs3 = ' ' ) 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes1 
              ON Substring(pr.org, 1, 2) = groupCFGOrgCodes1.code 
                 AND groupCFGOrgCodes1.orglevel = 1 
       LEFT JOIN organization AS Organization_Projects_Sales_CustSalesTeam 
              ON projects_sales.custsalesteam = 
                 Organization_Projects_Sales_CustSalesTeam.org 
       LEFT JOIN cfgserviceprofile 
              ON cfgserviceprofile.code = pr.servprocode 
       LEFT JOIN cl cl_addr 
              ON cl_addr.clientid = pr.clientid 
       LEFT JOIN claddress CLAddress_cl 
              ON CLAddress_cl .clientid = cl_addr.clientid 
                 AND pr.claddress = CLAddress_cl.address 
       LEFT JOIN contacts AS BLCT 
              ON pr.billingcontactid = BLCT.contactid 
       LEFT JOIN fw_cfgcountry AS CL_CFGCountry 
              ON CL_CFGCountry.isocountrycode = CLAddress_cl.country 
       LEFT JOIN cfgstates AS CL_CFGStates 
              ON CL_CFGCountry.isocountrycode = CL_CFGStates.isocountrycode 
                 AND CL_CFGStates.code = CLAddress_cl.state 
                 AND CL_CFGCountry.isocountrycode = CLAddress_cl.country 
       LEFT JOIN 
       /* CLQuery */ 
       (SELECT code, 
               Min(displaystate)  AS DisplayState, 
               Min(addressformat) AS AddressFormat, 
               Min(description)   AS StateNoCountry 
        FROM   cfgstates 
               INNER JOIN fw_cfgcountry 
                       ON cfgstates.isocountrycode = 
                          fw_cfgcountry.isocountrycode 
        GROUP  BY code) AS CL_StateQuery 
              ON CL_StateQuery.code = CLAddress_cl.state, 
       pr AS LEVEL2 
       LEFT JOIN projectcustomtabfields AS GROUPWBS2_ProjectCustomTabFields 
              ON ( LEVEL2.wbs1 = GROUPWBS2_ProjectCustomTabFields.wbs1 
                   AND LEVEL2.wbs2 = GROUPWBS2_ProjectCustomTabFields.wbs2 
                   AND LEVEL2.wbs3 = GROUPWBS2_ProjectCustomTabFields.wbs3 ), 
       pr AS LEVEL3 
       LEFT JOIN projectcustomtabfields 
              ON projectcustomtabfields.wbs1 = LEVEL3.wbs1 
                 AND projectcustomtabfields.wbs2 = LEVEL3.wbs2 
                 AND projectcustomtabfields.wbs3 = LEVEL3.wbs3 
       /***MULTICURRENCYFROM***/ 
       LEFT JOIN (organization 
                  LEFT JOIN cfgorganizationstatus 
                         ON organization.status = cfgorganizationstatus.status) 
              ON LEVEL3.org = organization.org 
       /*left join " & buildGroupJoin("CL","CL") & " ON LEVEL3.ClientID = CL.CLientID */ 
       LEFT JOIN fw_cfgcountry 
              ON fw_cfgcountry.isocountrycode = LEVEL3.country 
       LEFT JOIN cl 
              ON LEVEL3.clientid = cl.clientid 
       LEFT JOIN cl billCL 
              ON LEVEL3.billingclientid = billCL.clientid 
       LEFT JOIN cl AS CLParentLevel1 
              ON cl.parentlevel1 = CLParentLevel1.clientid 
       LEFT JOIN cl AS CLParentLevel2 
              ON cl.parentlevel2 = CLParentLevel2.clientid 
       LEFT JOIN cl AS CLParentLevel3 
              ON cl.parentlevel3 = CLParentLevel3.clientid 
       LEFT JOIN cl AS CLParentLevel4 
              ON cl.parentlevel4 = CLParentLevel4.clientid 
       LEFT JOIN claddress clAddress_bl 
              ON LEVEL3.billingclientid = clAddress_bl.clientid 
                 AND LEVEL3.clbillingaddr = clAddress_bl.address 
       LEFT JOIN cfgprojecttype 
              ON LEVEL3.projecttype = cfgprojecttype.code 
       LEFT JOIN cfgprresponsibility 
              ON LEVEL3.responsibility = cfgprresponsibility.code 
       LEFT JOIN em EMPrin 
              ON EMPrin.employee = LEVEL3.principal 
       LEFT JOIN em EMMgr 
              ON EMMgr.employee = LEVEL3.projmgr 
       LEFT JOIN em Supv 
              ON Supv.employee = LEVEL3.supervisor 
       LEFT JOIN em EMBiller 
              ON EMBiller.employee = LEVEL3.biller 
       LEFT JOIN cfgprojectstatus 
              ON cfgprojectstatus.code = LEVEL3.status 
       LEFT JOIN opportunity 
              ON opportunity.opportunityid = LEVEL3.opportunityid 
       LEFT JOIN contacts 
              ON contacts.contactid = LEVEL3.contactid 
       LEFT JOIN contacts billContacts 
              ON billContacts.contactid = LEVEL3.billingcontactid 
       LEFT JOIN cfgchargetype 
              ON cfgchargetype.type = LEVEL3.chargetype 
       LEFT JOIN cfgrgmethods 
              ON cfgrgmethods.method = LEVEL3.revtype 
       LEFT JOIN cl AS CLBill 
              ON CLBill.clientid = LEVEL3.billingclientid 
       LEFT JOIN costrt AS payrate 
              ON payrate.tableno = LEVEL3.payratetableno 
                 AND LEVEL3.payratemeth = 1 
                 AND ( payrate.ratetype = 'P' 
                        OR payrate.ratetype = 'B' ) 
       LEFT JOIN costrt AS costrate 
              ON costrate.tableno = LEVEL3.costratetableno 
                 AND LEVEL3.costratemeth = 1 
                 AND ( costrate.ratetype = 'C' 
                        OR costrate.ratetype = 'B' ) 
       LEFT JOIN costct AS payrateC 
              ON payrateC.tableno = LEVEL3.payratetableno 
                 AND LEVEL3.payratemeth = 2 
                 AND ( payrateC.ratetype = 'P' 
                        OR payrateC.ratetype = 'B' ) 
       LEFT JOIN costct AS costrateC 
              ON costrateC.tableno = LEVEL3.costratetableno 
                 AND LEVEL3.costratemeth = 2 
                 AND ( costrateC.ratetype = 'C' 
                        OR costrateC.ratetype = 'B' ) 
       LEFT JOIN costlt AS payrateL 
              ON payrateL.tableno = LEVEL3.payratetableno 
                 AND LEVEL3.payratemeth = 3 
                 AND ( payrateL.ratetype = 'P' 
                        OR payrateL.ratetype = 'B' ) 
       LEFT JOIN costlt AS costrateL 
              ON costrateL.tableno = LEVEL3.costratetableno 
                 AND LEVEL3.costratemeth = 3 
                 AND ( costrateL.ratetype = 'C' 
                        OR costrateL.ratetype = 'B' ) 
       LEFT JOIN rpplan 
              ON Level3.planid = rpplan.planid 
       LEFT JOIN prclientassoc PRClientAssocOwner 
              ON Level3.wbs1 = PRClientAssocOwner.wbs1 
                 AND PRClientAssocOwner.role = 'sysOwner' 
       LEFT JOIN cl CLOwner 
              ON CLOwner.clientid = PRClientAssocOwner.clientid 
       LEFT JOIN claddress CLAddressOwner 
              ON CLowner.clientid = CLAddressOwner.clientid 
                 AND CLAddressOwner.primaryind = 'Y' 
       LEFT JOIN apexpensecodeot 
              ON apexpensecodeot.tableno = LEVEL3.billbydefaultortable 
       LEFT JOIN fw_cfgcountry AS CLB_CFGCountry 
              ON CLB_CFGCountry.isocountrycode = CLAddress_bl.country 
       LEFT JOIN cfgstates AS CLB_CFGStates 
              ON CLB_CFGCountry.isocountrycode = CLB_CFGStates.isocountrycode 
                 AND CLB_CFGStates.code = CLAddress_bl.state 
                 AND CLB_CFGCountry.isocountrycode = CLAddress_bl.country 
       LEFT JOIN (SELECT code, 
                         Min(displaystate)  AS DisplayState, 
                         Min(addressformat) AS AddressFormat, 
                         Min(description)   AS StateNoCountry 
                  FROM   cfgstates 
                         INNER JOIN fw_cfgcountry 
                                 ON cfgstates.isocountrycode = 
                                    fw_cfgcountry.isocountrycode 
                  GROUP  BY code) AS CLB_StateQuery 
              ON CLB_StateQuery.code = CLAddress_bl.state 
       LEFT JOIN fw_cfgcountry AS Owner_CFGCountry 
              ON Owner_CFGCountry.isocountrycode = CLAddressOwner.country 
       LEFT JOIN cfgstates AS Owner_CFGStates 
              ON Owner_CFGCountry.isocountrycode = 
                 Owner_CFGStates.isocountrycode 
                 AND Owner_CFGStates.code = CLAddressOwner.state 
                 AND Owner_CFGCountry.isocountrycode = CLAddressOwner.country 
       LEFT JOIN (SELECT code, 
                         Min(displaystate)  AS DisplayState, 
                         Min(addressformat) AS AddressFormat, 
                         Min(description)   AS StateNoCountry 
                  FROM   cfgstates 
                         INNER JOIN fw_cfgcountry 
                                 ON cfgstates.isocountrycode = 
                                    fw_cfgcountry.isocountrycode 
                  GROUP  BY code) AS Owner_StateQuery 
              ON Owner_StateQuery.code = CLAddressOwner.state 
       LEFT JOIN cfgmaindata 
              ON cfgmaindata.company = '01' 
/***ACTIVITYFROM***/ 
WHERE  ( ( pr.wbs1 = LEVEL3.wbs1 
           AND pr.wbs2 = ' ' 
           AND pr.wbs3 = ' ' ) 
         AND ( LEVEL2.wbs1 = LEVEL3.wbs1 
               AND LEVEL2.wbs2 = LEVEL3.wbs2 
               AND LEVEL2.wbs3 = ' ' ) ) 
       AND ( Datepart(yy, projects_sales.custsalesdate) = @Year ) 
       /***MULTICURRENCYWHERE***/ 
       AND (( EXISTS (SELECT 'x' 
                      FROM   projects_sales 
                             LEFT JOIN organization AS OrganizationCust_1 
                                    ON OrganizationCust_1.org = 
                                       projects_sales.custsalesteam 
                      WHERE  pr.wbs1 = projects_sales.wbs1 
                             AND pr.wbs2 = projects_sales.wbs2 
                             AND pr.wbs3 = projects_sales.wbs3 
                             AND (( Datepart(yy, projects_sales.custsalesdate) = 
                                    @Year 
                                  ))) 
              AND ( ( Substring(pr.org, 1, 2) = @CompanyId )  ) )) 
       AND LEVEL3.wbs2 = ' ' 
       AND LEVEL3.wbs3 = ' ' 
       AND ( '' = '' 
              OR ( LEVEL3.wbs1 = '' 
                   AND LEVEL3.wbs2 = '' 
                   AND LEVEL3.wbs3 = '' ) ) 
ORDER  BY group1, 
          group2, 
          gridudcol_projects_sales_custsalesdate DESC, 
          group3, 
          LEVEL3.wbs1, 
          LEVEL3.wbs2, 
          LEVEL3.wbs3 /***EXTEND GRIDCOLUMN ORDER BY***/

END
GO
