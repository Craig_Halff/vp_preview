SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_Stamp_Save]
	@fieldsSQL			Nvarchar(max),
	@updateValuesSql	Nvarchar(max),
	@insertValuesSql	Nvarchar(max),
	@deleteSeqs			Nvarchar(max),
	@VISION_LANGUAGE	Nvarchar(10)
AS
BEGIN
	/*
	[DeltekStoredProc_CCG_PAT_Config_Stamp_Save]
		@fieldsSQL = 'Label, StampType, DisplayOrder, Content, PositionType, PageSet, Width, ValidRoles, Status, ModDate, ModUser',
		@updateValuesSql = '(''Another One'', ''Text'', 0, ''C:\Temp\ANOTHER'', ''TopRight'', ''All'', 25, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 28), (''Bill to budget, write off remaining'', ''Text'', 1, ''Bill to budget, write off remaining'', ''Free'', ''All'', 50, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 26), (''Approved'', ''Text'', 2, ''Approved\nby [:FIRSTNAME] [:LASTNAME]\non [:DATE] at [:TIME]'', ''TopRight'', ''First'', 20, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 15), (''Approved As-Is'', ''Text'', 3, ''Approved As-Is\nby [:FIRSTNAME] [:LASTNAME]\non [:DATE] at [:TIME]'', ''TopRight'', ''First'', 20, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 18), (''Hold Indefinitely'', ''Text'', 10, ''Hold Indefinitely'', ''TopRight'', ''First'', 20, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 16), (''Hold Until Next Month'', ''Text'', 11, ''Hold Until Next Month'', ''TopRight'', ''First'', 20, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 19), (''Hold As Noted'', ''Text'', 12, ''Hold As Noted'', ''TopRight'', ''First'', 20, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 20), (''Hold item'', ''Text'', 15, ''Hold item'', ''Free'', ''All'', 9, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 27), (''----------------'', ''Text'', 19, ''-'', ''TopRight'', ''All'', 25, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 25), (''Check'', ''Image'', 20, ''c:\temp\OK32.png'', ''Free'', ''All'', 5, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 17), (''Check - Red - Small'', ''Image'', 21, ''C:\Temp\Red Check 16x16.gif'', ''Free'', ''All'', 2, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 21), (''Check - Green - Small'', ''Image'', 22, ''C:\Temp\Green Check 16x16.png'', ''Free'', ''All'', 2, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 22), (''Accepted'', ''Image'', 23, ''C:\Temp\Accepted.jpg'', ''Free'', ''All'', 8, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 23), (''Rejected'', ''Image'', 24, ''C:\Temp\Rejected.jpg'', ''Free'', ''All'', 8, '''', ''Main'', GetDate(), ''ADMIN'', 24)',
		@insertValuesSql = '',
		@deleteSeqs = ''
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	int;
	DECLARE @ids		TABLE (id int);
	DECLARE @i			int;
	DECLARE @newI		int;
	DECLARE @seq		Nvarchar(50);

	-- Ensure that our inputs are safe (preventing SQL injection)
	SET @safeSql = dbo.fnCCG_PAT_RegexMatch(N'<CSV>', @fieldsSQL);
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch(N'<INSERT VALUES>|', @updateValuesSql) * 2;
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch(N'<INSERT VALUES>|', @insertValuesSql) * 4;
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch(N'(<NUMBER>\|?)*', @deleteSeqs) * 8;
	IF @safeSql < 15 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS Nvarchar(4));
		RETURN;
	END;

	BEGIN TRANSACTION;
	-- Stamps updates
	IF ISNULL(@updateValuesSql, '') <> '' BEGIN
		SET @sSQL = N'
			UPDATE cs
				SET
					[Label]			= temp.[Label],
					[StampType]		= temp.[StampType],
					[DisplayOrder]	= temp.[DisplayOrder],
					[Content]		= temp.[Content],
					[PositionType]	= temp.[PositionType],
					[Width]			= temp.[Width],
					[ValidRoles]	= temp.[ValidRoles],
					[Status]		= temp.[Status],
					[ModDate]		= temp.[ModDate],
					[ModUser]		= temp.[ModUser],
					[PageSet]		= temp.[PageSet]
				FROM CCG_PAT_ConfigStamps cs
				JOIN (
					VALUES ' + @updateValuesSql + '
				) temp (' + @fieldsSQL + ', Seq) ON temp.Seq = cs.Seq';
		--PRINT @sSQL;
		EXEC (@sSQL);
	END;

	-- Stamps inserts
	IF ISNULL(@insertValuesSql, '') <> '' BEGIN
		SET @sSQL = N'
			INSERT INTO CCG_PAT_ConfigStamps
				(' + @fieldsSQL + ')
				VALUES
				' + @insertValuesSql;
		--PRINT @sSQL;
		EXEC (@sSQL);
	END;

	-- Do Stamps deletes
	SET @i = 0;
	BEGIN TRY
		WHILE @i >= 0 And @i < Len(@deleteSeqs) BEGIN
			SET @newI = CHARINDEX('|', @deleteSeqs, @i + 1);
			IF @newI <= 0 SET @newI = Len(@deleteSeqs) + 1;
			SET @seq = SUBSTRING(@deleteSeqs, @i, @newI - @i);

			--PRINT @seq;
			DELETE FROM CCG_PAT_ConfigStamps WHERE Seq = @seq;

			SET @i = @newI + 1;
		END;
	END TRY
	BEGIN CATCH
		SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_MESSAGE() AS ErrorMessage;
	END CATCH;

	-- Update the multilingual descriptions table
	SET @sSQL = N'
		DELETE a FROM CCG_PAT_ConfigStampsDescriptions a LEFT JOIN CCG_PAT_ConfigStamps b ON a.Seq = b.Seq
			WHERE b.Seq IS NULL		-- No match

		MERGE CCG_PAT_ConfigStampsDescriptions WITH (HOLDLOCK) AS Target
		USING CCG_PAT_ConfigStamps AS Source ON Target.Seq = Source.Seq AND Target.UICultureName = '''+@VISION_LANGUAGE+'''
		WHEN MATCHED THEN
			UPDATE SET
				[Label]		= Source.[Label],
				[Content]	= Source.[Content]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (Seq, Label, UICultureName, Content)
				VALUES (Source.Seq, Source.Label, '''+@VISION_LANGUAGE+''', Source.Content)
		WHEN NOT MATCHED BY SOURCE AND Target.UICultureName = '''+@VISION_LANGUAGE+''' THEN DELETE;';
	EXEC (@sSQL);

	COMMIT TRANSACTION;
END;
GO
