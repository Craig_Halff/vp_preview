SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_CheckForDuplicateInvoice] ( @vendor nvarchar(20), @itemNumber nvarchar(50), @itemDate varchar(30), @seq int, @company nvarchar(14), @transType varchar(2), @voucher nvarchar(12))
             AS EXEC spCCG_PAT_CheckForDuplicateInvoice @vendor,@itemNumber,@itemDate,@seq,@company,@transType,@voucher
GO
