SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_EmailUpdate]
	@SMTPServer			Nvarchar(500),
	@SMTPPort			Nvarchar(20),
	@Username			Nvarchar(500),
	@Password			Nvarchar(50),
	@Interval			Nvarchar(20),
	@TLSOptionVisible	bit,
	@useTLS				char(1),
	@EmailLink			Nvarchar(500),
	@EncTimeout			Nvarchar(10),
	@EnfWinUsername		char(1)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);

	SET @sSQL = N'
		UPDATE CCG_Email_Config SET
			SMTPServer = N''' + @SMTPServer + ''',
			SMTPPort = ''' + @SMTPPort + ''',
			Username = N''' + @Username + ''',
			Password = N''' + REPLACE(@Password, '''', '''''') + ''',
			Interval = ' + @Interval +
			(CASE WHEN @TLSOptionVisible=1 THEN ',
			UseTLS = ''' + @useTLS + '''' ELSE '' END);
	EXEC (@sSQL);

    UPDATE CCG_PAT_Config SET
        EmailLink = @EmailLink,
        EncTimeout = @EncTimeout,
        EnforceWindowsUsernameInLink = @EnfWinUsername
END;

GO
