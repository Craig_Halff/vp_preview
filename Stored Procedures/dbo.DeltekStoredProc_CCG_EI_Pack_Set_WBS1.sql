SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Pack_Set_WBS1]
	@wbs1				NVARCHAR(30)
AS
BEGIN
	/*
	[DeltekStoredProc_CCG_EI_Pack_Set_WBS1] @wbs1 = '2003005.00'
	*/
	SET NOCOUNT ON;
	SELECT DISTINCT invoice, invoice + ' - ' + convert(varchar(20), invoicedate,101), invoicedate
		FROM billInvMaster
		WHERE MainWBS1 = @wbs1 and recdSeq = 1
		ORDER BY InvoiceDate desc
END;
GO
