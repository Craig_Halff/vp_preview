SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AddIndustryDefaultsConsultfrCA]
AS
BEGIN

PRINT ' '
PRINT 'Adding Consulting Industry Defaults for fr-CA'

BEGIN TRANSACTION

DELETE FROM BTLaborCatsDescriptions WHERE UICultureName = 'fr-CA';
--..........................................................................................v(50)
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 1, 'fr-CA','Exécutif'             WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 1);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 2, 'fr-CA','Conduite de pratique'         WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 2);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 3, 'fr-CA','Client exécutif'      WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 3);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 4, 'fr-CA','Gestionnaire d''engagement'    WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 4);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 5, 'fr-CA','Sous-traitant'            WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 5);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 6, 'fr-CA','Analyste commercial'      WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 6);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 7, 'fr-CA','Architecte'             WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 7);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 8, 'fr-CA','Concepteur'              WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 8);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 9, 'fr-CA','Responsable des propositions'      WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 9);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 10,'fr-CA','Contrôleur'            WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 10);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 11,'fr-CA','Marketing'             WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 11);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 12,'fr-CA','Ressources humaines'       WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 12);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 13,'fr-CA','Développement commercial'  WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 13);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 14,'fr-CA','Admin'                 WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 14);

DELETE FROM CFGActivitySubjectData WHERE UICultureName = 'fr-CA';
--..................................................................................v(50)
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Appel à froid'                ,'fr-CA',3 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Réunion initiale'         ,'fr-CA',1 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Courriel d''introduction'       ,'fr-CA',4 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Appel de qualification'       ,'fr-CA',2 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Mention des médias sociaux'     ,'fr-CA',6 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Courriel leadership pensé' ,'fr-CA',5 ,'N');

DELETE FROM CFGActivityTypeDescriptions WHERE UICultureName = 'fr-CA';
--.........................................................................................................v(15)
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'EMail'     ,'fr-CA','Courriel'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'EMail');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Event'     ,'fr-CA','Événement'          ,6 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Event');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Mailing'   ,'fr-CA','Publipostage'        ,5 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Mailing');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Meeting'   ,'fr-CA','Réunion'        ,3 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Meeting');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Merge'     ,'fr-CA','Créer un Document',4 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Merge');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Milestone' ,'fr-CA','Jalon'      ,8 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Milestone');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Phone Call','fr-CA','Appel téléphonique'     ,2 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Phone Call');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Task'      ,'fr-CA','Tâche'           ,9 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Task');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Touchpoint','fr-CA','Point de service'     ,7 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Touchpoint');

DELETE FROM CFGARLedgerHeadingsDescriptions WHERE UICultureName = 'fr-CA';
--...............................................................................................v(12)
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 1,'fr-CA','Honoraires'       WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 1);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 2,'fr-CA','Frais remb. de '     WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 2);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 3,'fr-CA','Sous-traitant' WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 3);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 4,'fr-CA','Provision'   WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 4);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 5,'fr-CA','Taxes'      WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 5);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 6,'fr-CA','Intérêt'   WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 6);

DELETE FROM CFGBillMainDescriptions WHERE UICultureName = 'fr-CA';
--..............................................................................................................................................................................................................v(15)..v(15)....v(15)............v(15)......v(15)...v(15).....v(15)..v(15)
INSERT INTO CFGBillMainDescriptions (Company,UICultureName,FooterMessage,FeeLabel,LabLabel,ConLabel,ExpLabel,UnitLabel,AddOnLabel,TaxLabel,InterestLabel,OvtIndicator) SELECT CFGMainData.Company,'fr-CA',NULL,'Honoraires' ,'Main-d’œuvre' ,'Sous-traitant' ,'Frais' ,'Unité' ,'Supplément' ,'Taxe' ,'Intérêt' ,'Ovt'  FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGBillMainData WHERE Company = CFGMainData.Company);

DELETE FROM CFGCampaignActionDescriptions WHERE UICultureName = 'fr-CA';
--..................................................................................................v(50)
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-CA','Développer le contenu' ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '01');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-CA','Envoyer l''invitation' ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '02');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-CA','Envoyer un ensemble de courriels',3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '03');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-CA','Suivi'       ,4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '04');

DELETE FROM CFGCampaignAudienceDescriptions WHERE UICultureName = 'fr-CA';
--....................................................................................................v(50)
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-CA','Cadres supérieurs'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '01');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-CA','Finance'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '02');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-CA','Marketing'           ,3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '03');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-CA','Développement commercial',4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '04');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '05','fr-CA','Gestionnaires d''engagement' ,5 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '05');

DELETE FROM CFGCampaignObjectiveDescriptions WHERE UICultureName = 'fr-CA';
--.....................................................................................................v(50)
INSERT INTO CFGCampaignObjectiveDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-CA','Nouvelle génération d''entreprises' ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignObjectiveData WHERE Code = '01');
INSERT INTO CFGCampaignObjectiveDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-CA','Développer les activités existantes',2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignObjectiveData WHERE Code = '02');

DELETE FROM CFGCampaignStatusDescriptions WHERE UICultureName = 'fr-CA';
--..................................................................................................v(50)
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-CA','Actif'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '01');
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-CA','Planification' ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '02');
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-CA','Exécuté',3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '03');

DELETE FROM CFGCampaignTypeDescriptions WHERE UICultureName = 'fr-CA';
--................................................................................................v(50)
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-CA','Courriel'      ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '01');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-CA','Courrier direct',2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '02');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-CA','Webinaire'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '03');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-CA','Salon professionnel'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '04');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '05','fr-CA','Meetup'     ,5 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '05');

DELETE FROM CFGClientCurrentStatusDescriptions WHERE UICultureName = 'fr-CA';
--...............................................................................................................v(50)
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Existing','fr-CA','Existant',1 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Existing');
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Former'  ,'fr-CA','Ancien'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Former');
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Prospect','fr-CA','Client potentiel',2 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Prospect');

DELETE FROM CFGClientRelationshipDescriptions WHERE UICultureName = 'fr-CA';
--......................................................................................................v(50)
INSERT INTO CFGClientRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-CA','Concurrent',1 WHERE EXISTS (SELECT 'x' FROM CFGClientRelationshipData WHERE Code = '01');

DELETE FROM CFGClientRoleDescriptions WHERE UICultureName = 'fr-CA';
--....................................................................................................v(50)
INSERT INTO CFGClientRoleDescriptions (Code,UICultureName,Description,Seq) SELECT '01'      ,'fr-CA','Sous-traitant',2 WHERE EXISTS (SELECT 'x' FROM CFGClientRoleData WHERE Code = '01');
INSERT INTO CFGClientRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'sysOwner','fr-CA','Client'      ,1 WHERE EXISTS (SELECT 'x' FROM CFGClientRoleData WHERE Code = 'sysOwner');

DELETE FROM CFGClientTypeDescriptions WHERE UICultureName = 'fr-CA';
--..............................................................................................v(50)
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-CA','Formation'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '01');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-CA','Entreprise'          ,2 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '02');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-CA','Finance et Banque' ,3 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '03');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-CA','Santé et bien-être' ,4 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '04');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '05','fr-CA','Hospitalité'         ,5 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '05');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '06','fr-CA','Services d''information',6 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '06');

DELETE FROM CFGContactRelationshipDescriptions WHERE UICultureName = 'fr-CA';
--.......................................................................................................v(50)
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-CA','Ancien client'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '01');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-CA','Ancien collaborateur' ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '02');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-CA','Pair de l''industrie'   ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '03');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-CA','Contact personnel',4 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '04');

DELETE FROM CFGContactRoleDescriptions WHERE UICultureName = 'fr-CA';
--.....................................................................................................v(50)
INSERT INTO CFGContactRoleDescriptions (Code,UICultureName,Description,Seq) SELECT '01'      ,'fr-CA','Sous-traitant',2 WHERE EXISTS (SELECT 'x' FROM CFGContactRoleData WHERE Code = '01');
INSERT INTO CFGContactRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'sysOwner','fr-CA','Client'      ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactRoleData WHERE Code = 'sysOwner');

DELETE FROM CFGContactSourceDescriptions WHERE UICultureName = 'fr-CA';
--...................................................................................................v(50)
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '01','fr-CA','Référence du client'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '01');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '02','fr-CA','Événement'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '02');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '03','fr-CA','Liste'              ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '03');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '04','fr-CA','Contact personnel'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '04');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '05','fr-CA','Médias sociaux'      ,5 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '05');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '06','fr-CA','Leadership éclairé',6 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '06');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '07','fr-CA','Site Web'           ,7 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '07');

DELETE FROM CFGContactTitleDescriptions WHERE UICultureName = 'fr-CA';
--.................................................................................................v(50)
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Director' ,'fr-CA','Directeur' ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Director');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Executive','fr-CA','Exécutif',1 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Executive');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Finance'  ,'fr-CA','Finance'  ,5 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Finance');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Marketing','fr-CA','Marketing',6 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Marketing');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Sales'    ,'fr-CA','Ventes'    ,4 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Sales');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'VP'       ,'fr-CA','VP'       ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'VP');

DELETE FROM CFGContractStatusDescriptions WHERE UICultureName = 'fr-CA';
--..................................................................................................v(50)
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-CA','Examen interne'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '01');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-CA','Envoyé au client'   ,2 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '02');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-CA','Négociation'      ,3 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '03');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-CA','Examen juridique'     ,4 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '04');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '05','fr-CA','Signé et exécuté',5 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '05');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '06','fr-CA','Affaire perdue/aucune affaire'     ,6 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '06');

DELETE FROM CFGContractTypeDescriptions WHERE UICultureName = 'fr-CA';
--................................................................................................v(50)
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-CA','Énoncé des travaux',1 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '01');
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-CA','Amendement'        ,2 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '02');

DELETE FROM CFGCubeTranslationDescriptions WHERE UICultureName = 'fr-CA';
--..............................................................................................................................................v(200)
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'U'                       ,'fr-CA','Inconnu');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'UM'                      ,'fr-CA','Mois inconnu');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'H'                       ,'fr-CA','Historique');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('YesNo'          ,'Y'                       ,'fr-CA','Oui');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('YesNo'          ,'N'                       ,'fr-CA','Non');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('Status'         ,'A'                       ,'fr-CA','Actif');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('Status'         ,'I'                       ,'fr-CA','Inactif');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'1-Asset'                 ,'fr-CA','Actif');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'2-Liability'             ,'fr-CA','Passif');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'3-NetWorth'              ,'fr-CA','Valeur nette');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'4-Revenue'               ,'fr-CA','Revenu');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'5-Reimbursable'          ,'fr-CA','Remboursable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'6-ReimbursableConsultant','fr-CA','Consultant remboursable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'7-Direct'                ,'fr-CA','Direct');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'8-DirectConsultant'      ,'fr-CA','Sous-traitant direct');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'9-Indirect'              ,'fr-CA','Indirect');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'10-OtherCharges'         ,'fr-CA','Autres charges');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'U-Unknown'               ,'fr-CA','Inconnu');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ExpenseType'    ,'5-ReimbursableOther'     ,'fr-CA','Remboursables - Autres');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ExpenseType'    ,'7-DirectOther'           ,'fr-CA','Frais directs - Autres');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ReportType'     ,'BalanceSheet'            ,'fr-CA','Bilan');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ReportType'     ,'IncomeStatement'         ,'fr-CA','Déclaration de revenus');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'1-Asset','fr-CA'         ,'Other Assets');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'2-Liability'             ,'fr-CA','Autres passifs');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'3-NetWorth'              ,'fr-CA','Autre valeur nette');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'4-Revenue'               ,'fr-CA','Autre revenu');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'5-Reimbursable'          ,'fr-CA','Autres remboursements');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'6-Reimbursable'          ,'fr-CA','Autres remboursements');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'7-Direct'                ,'fr-CA','Autres coûts directs');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'8-Direct'                ,'fr-CA','Autres coûts directs');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'9-Indirect'              ,'fr-CA','Autres coûts indirects');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'10-OtherCharges'         ,'fr-CA','Autres charges');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'B'                       ,'fr-CA','Facturable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'H'                       ,'fr-CA','En suspens');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'W'                       ,'fr-CA','À radier');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'X'                       ,'fr-CA','Radié(es)');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'D'                       ,'fr-CA','Marqué pour la suppression');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'N'                       ,'fr-CA','Non facturable');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'F'                       ,'fr-CA','Facturation finale');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'T'                       ,'fr-CA','Transféré');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'R'                       ,'fr-CA','Partiellement suspendu/libéré');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'M'                       ,'fr-CA','Modifié');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'O'                       ,'fr-CA','Supprimé');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AP'                      ,'fr-CA','Documents commerciaux de CF');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BA'                      ,'fr-CA','Traitement des heures des avantages');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BE'                      ,'fr-CA','Transferts de facturation de frais');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BL'                      ,'fr-CA','Transferts de facturation des honoraires');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BU'                      ,'fr-CA','Transferts de facturation d''[Unit]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CD'                      ,'fr-CA','Sorties de fonds');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CP'                      ,'fr-CA','Convertir l''[Organization] du [WBS1]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CR'                      ,'fr-CA','Encaissements en espèces');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CV'                      ,'fr-CA','Sorties CF');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EP'                      ,'fr-CA','Traitement des paiements du [Employee]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','ER'                      ,'fr-CA','Remboursements de l''[Employee]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EX'                      ,'fr-CA','Dépenses de l''[Employee]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IN'                      ,'fr-CA','Factures');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','JE'                      ,'fr-CA','Écritures de journal');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','KC'                      ,'fr-CA','Conversion clé');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','LA'                      ,'fr-CA','Rajustements des honoraires');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','LG'                      ,'fr-CA','Gains/pertes et réévaluations');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','MI'                      ,'fr-CA','Dépenses diverses');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PP'                      ,'fr-CA','Traitement de paiement CF');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PR'                      ,'fr-CA','Impressions et reproductions');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PY'                      ,'fr-CA','Traitement de la paie');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','RG'                      ,'fr-CA','Génération de revenus');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','TS'                      ,'fr-CA','Feuilles de temps');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','UN'                      ,'fr-CA','[Units]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','UP'                      ,'fr-CA','[Units] par [WBS1]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AL'                      ,'fr-CA','Rapport de conversion clé, ouverture d''un nouvel exercice/trimestre/an de régularisation des avantages, fermeture d''une période');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AX'                      ,'fr-CA','Document commercial CF converti');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CN'                      ,'fr-CA','Frais de consultant convertis');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CT'                      ,'fr-CA','Transaction indiquant la date d''exécution de la conversion d''un appel de livraison précédent');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EC'                      ,'fr-CA','Ajustement(s) d''analyse de temps convertie');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IH'                      ,'fr-CA','Facture convertie');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IX'                      ,'fr-CA','Facture(s) convertie(s)');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','JX'                      ,'fr-CA','Conversion MCFMS');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PX'                      ,'fr-CA','Traitement des paiements CF convertis');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','RX'                      ,'fr-CA','Conversion MCFMS');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','XD'                      ,'fr-CA','Données de compte converties');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','XE'                      ,'fr-CA','Frais du [WBS1] convertis');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HA'                      ,'fr-CA','Historiques des positions de [Account]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HB'                      ,'fr-CA','[Employee] - Historique de régularisation des avantages');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HE'                      ,'fr-CA','[WBS1] - Historique main-d''œuvre et frais');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HL'                      ,'fr-CA','[WBS1] - Historique main-d''œuvre et frais');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HU'                      ,'fr-CA','Revenus non facturés');

DELETE FROM CFGEMDegreeDescriptions WHERE UICultureName = 'fr-CA';
--............................................................................................v(50)
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-CA','Baccalauréat en sciences',1 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '01');
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-CA','Baccalauréat ès arts'   ,2 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '02');
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-CA','Masters'            ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '03');

DELETE FROM CFGEmployeeRelationshipDescriptions WHERE UICultureName = 'fr-CA';
--..............................................................................................................v(50)
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01'      ,'fr-CA','Ancien employeur'            ,3 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '01');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '02'      ,'fr-CA','Ancien collaborateur'            ,2 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '02');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '03'      ,'fr-CA','Contact personnel'           ,5 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '03');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '04'      ,'fr-CA','Connexion générale de l''industrie',4 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '04');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT 'SysOwner','fr-CA','Propriétaire'                      ,1 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = 'SysOwner');

DELETE FROM CFGEmployeeRoleDescriptions WHERE UICultureName = 'fr-CA';
--........................................................................................................v(50)
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Admin'     ,'fr-CA','Admin'           ,1  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Admin');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Controller','fr-CA','Contrôleur'      ,4  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Controller');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'DelivTeam' ,'fr-CA','Équipe de livraison'   ,5  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'DelivTeam');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Executive' ,'fr-CA','Exécutif'       ,7  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Executive');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'HumanRes'  ,'fr-CA','Ressources humaines' ,8  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'HumanRes');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ResMgr'    ,'fr-CA','Responsable des ressources',12 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ResMgr');

DELETE FROM CFGEmployeeTitleDescriptions WHERE UICultureName = 'fr-CA';
--...................................................................................................v(50)
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Acct'      ,'fr-CA','Comptable'           ,18 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Acct');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Arch'      ,'fr-CA','Architecte'            ,22 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Arch');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'BA'        ,'fr-CA','Analyste commercial'     ,23 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'BA');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CE'        ,'fr-CA','Client exécutif'     ,9  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CE');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CEO'       ,'fr-CA','PDG'                  ,1  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CEO');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CFO'       ,'fr-CA','DPF'                  ,2  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CFO');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Consultant','fr-CA','Sous-traitant'           ,16 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Consultant');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Controller','fr-CA','Contrôleur'           ,17 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Controller');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'COO'       ,'fr-CA','CO'                  ,3  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'COO');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CP'        ,'fr-CA','Partenaire client'       ,10 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CP');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Dev'       ,'fr-CA','Développeur'            ,20 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Dev');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'DirHR'     ,'fr-CA','Directeur des RH'       ,7  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'DirHR');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'DirMkting' ,'fr-CA','Directeur du Marketing',8  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'DirMkting');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Eng'       ,'fr-CA','Ingénieur'             ,21 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Eng');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'EngageMgr' ,'fr-CA','Gestionnaire d''engagement'   ,11 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'EngageMgr');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Marketing' ,'fr-CA','Associé marketing'  ,25 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Marketing');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'ProdMgr'   ,'fr-CA','Chef de produit'      ,12 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'ProdMgr');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'ProjMgr'   ,'fr-CA','Chef de projet'      ,13 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'ProjMgr');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'PropMgr'   ,'fr-CA','Responsable des propositions'     ,15 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'PropMgr');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Recruiter' ,'fr-CA','Recruteur'            ,19 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Recruiter');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'RM'        ,'fr-CA','Responsable des ressources'     ,14 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'RM');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'UX'        ,'fr-CA','UX'                   ,24 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'UX');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'VPEng'     ,'fr-CA','V.-pr. ingénierie'       ,6  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'VPEng');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'VPSales'   ,'fr-CA','V.-pr. des ventes'          ,4  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'VPSales');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'VPStrat'   ,'fr-CA','V.-pr. de la stratégie'       ,5  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'VPStrat');

DELETE FROM CFGEmployeeTypeDescriptions WHERE UICultureName = 'fr-CA';
--.....................................................................................v(30)
INSERT INTO CFGEmployeeTypeDescriptions (Type,UICultureName,Label) SELECT 'E','fr-CA','Employé'   WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTypeData WHERE Type = 'E');
INSERT INTO CFGEmployeeTypeDescriptions (Type,UICultureName,Label) SELECT 'M','fr-CA','Gestion' WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTypeData WHERE Type = 'M');

DELETE FROM CFGEMRegistrationDescriptions WHERE UICultureName = 'fr-CA';
--.......................................................................................................v(70)
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'ACP'    ,'fr-CA','PMI-ACP'          ,2 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'ACP');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'AWS'    ,'fr-CA','Certification AWS',5 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'AWS');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'CCNA'   ,'fr-CA','CCNA'             ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'CCNA');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'CompTIA','fr-CA','CompTIA'          ,4 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'CompTIA');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'PMP'    ,'fr-CA','PMP'              ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'PMP');

DELETE FROM CFGEMSkillDescriptions WHERE UICultureName = 'fr-CA';
--..........................................................................................v(70)
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '1','fr-CA','Stratégie'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '1');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '2','fr-CA','Coaching exécutif',2 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '2');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '3','fr-CA','Analyse des données'     ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '3');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '4','fr-CA','Analyse financière',4 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '4');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '5','fr-CA','Validation d''idée'   ,5 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '5');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '6','fr-CA','Ingénierie'       ,6 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '6');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '7','fr-CA','Évaluation des risques'   ,7 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '7');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '8','fr-CA','Aller au marché'      ,8 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '8');

DELETE FROM CFGEMSkillLevelDescriptions WHERE UICultureName = 'fr-CA';
--.............................................................................................v(50)
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 1,'fr-CA','Élémentaire'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 1);
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 2,'fr-CA','Avancé',2 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 2);
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 3,'fr-CA','Expert'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 3);

DELETE FROM CFGEMSkillUsageDescriptions WHERE UICultureName = 'fr-CA';
--.............................................................................................v(255)
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 1,'fr-CA','Entrée'        ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 1);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 2,'fr-CA','De 1 à 2 ans'    ,2 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 2);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 3,'fr-CA','De 3 à 5 ans'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 3);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 4,'fr-CA','De 6 à 10 ans'   ,4 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 4);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 5,'fr-CA','Plus de 10 ans',5 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 5);

DELETE FROM CFGLeadRatingDescriptions WHERE UICultureName = 'fr-CA';
--..............................................................................................v(50)
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-CA','Froid',1 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '01');
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-CA','Tiède',2 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '02');
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-CA','Chaud' ,3 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '03');

DELETE FROM CFGOpportunityClosedReasonDescriptions WHERE UICultureName = 'fr-CA';
--...........................................................................................................v(50)
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-CA','Prix'    ,1 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '01');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-CA','Services' ,2 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '02');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-CA','Ressources',3 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '03');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-CA','Calendrier'   ,4 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '04');

DELETE FROM CFGOpportunitySourceDescriptions WHERE UICultureName = 'fr-CA';
--...........................................................................................................v(50)
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'CRef'    ,'fr-CA','Référence du client'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'CRef');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Event'   ,'fr-CA','Événement'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Event');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'List'    ,'fr-CA','Liste'              ,5 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'List');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'PersCont','fr-CA','Contact personnel'  ,6 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'PersCont');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Social'  ,'fr-CA','Médias sociaux'      ,7 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Social');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'TLead'   ,'fr-CA','Leadership éclairé',8 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'TLead');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Web'     ,'fr-CA','Web'               ,9 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Web');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'WIQ'     ,'fr-CA','GovWin IQ'         ,3 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'WIQ');

DELETE FROM CFGPrefixDescriptions WHERE UICultureName = 'fr-CA';
--........................................................................................v(10)
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Dr.'  ,'fr-CA','Dr'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Dr.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Miss' ,'fr-CA','Mlle' ,2 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Miss');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Mr.'  ,'fr-CA','M.'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Mr.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Mrs.' ,'fr-CA','Mme' ,4 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Mrs.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Ms.'  ,'fr-CA','Mme'  ,5 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Ms.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Prof.','fr-CA','Prof.',6 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Prof.');

DELETE FROM CFGProbabilityDescriptions WHERE UICultureName = 'fr-CA';
--.....................................................................................................v(50)
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 5,  'fr-CA','05' ,1 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 5);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 10, 'fr-CA','10' ,2 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 10);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 20, 'fr-CA','20' ,3 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 20);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 30, 'fr-CA','30' ,4 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 30);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 40, 'fr-CA','40' ,5 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 40);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 50, 'fr-CA','50' ,6 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 50);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 60, 'fr-CA','60' ,7 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 60);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 70, 'fr-CA','70' ,8 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 70);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 80, 'fr-CA','80' ,9 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 80);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 90, 'fr-CA','90' ,10 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 90);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 95, 'fr-CA','95' ,11 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 95);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 100,'fr-CA','100',12 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 100);

DELETE FROM CFGProjectMilestoneDescriptions WHERE UICultureName = 'fr-CA';
--.......................................................................................................................v(255)
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysEstStart',       'fr-CA','Commencement estimé',       1 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysEstStart')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysEstCompletion',  'fr-CA','Achèvement estimé',  2 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysEstCompletion')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysContract',       'fr-CA','Contrat attribué',      3 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysContract')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysBidSubmitted',   'fr-CA','Proposition soumise',    4 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysBidSubmitted')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysStart',          'fr-CA','Début effectif',          5 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysStart')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysActualCompl',    'fr-CA','Achèvement réel',     6 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysActualCompl')


DELETE FROM CFGProjectTypeDescriptions WHERE UICultureName = 'fr-CA';
--...............................................................................................v(50)
INSERT INTO CFGProjectTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-CA','Coaching exécutif' ,1 WHERE EXISTS (SELECT 'x' FROM CFGProjectTypeData WHERE Code = '01');
INSERT INTO CFGProjectTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-CA','Développement de produits',2 WHERE EXISTS (SELECT 'x' FROM CFGProjectTypeData WHERE Code = '02');
INSERT INTO CFGProjectTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-CA','Évaluation des risques'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGProjectTypeData WHERE Code = '03');
INSERT INTO CFGProjectTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-CA','Stratégie'           ,4 WHERE EXISTS (SELECT 'x' FROM CFGProjectTypeData WHERE Code = '04');
INSERT INTO CFGProjectTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '05','fr-CA','Atelier'           ,5 WHERE EXISTS (SELECT 'x' FROM CFGProjectTypeData WHERE Code = '05');

DELETE FROM CFGProposalSourceDescriptions WHERE UICultureName = 'fr-CA';
--..................................................................................................v(50)
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-CA','Référence du client'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '01');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-CA','Événement'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '02');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-CA','Liste'              ,3 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '03');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '04','fr-CA','Contact personnel'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '04');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '05','fr-CA','Médias sociaux'      ,5 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '05');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '06','fr-CA','Leadership éclairé',6 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '06');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '07','fr-CA','Site Web'           ,7 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '07');

DELETE FROM CFGProposalStatusDescriptions WHERE UICultureName = 'fr-CA';
--..................................................................................................v(50)
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-CA','En cours',1 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '01');
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-CA','En examen'  ,2 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '02');
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-CA','Soumis'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '03');

DELETE FROM CFGProposalTypeDescriptions WHERE UICultureName = 'fr-CA';
--.........................................................................................v(30)
INSERT INTO CFGProposalTypeDescriptions (Code,UICultureName,Type,Seq) SELECT '01','fr-CA','Devis rapide'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGProposalTypeData WHERE Code = '01');
INSERT INTO CFGProposalTypeDescriptions (Code,UICultureName,Type,Seq) SELECT '02','fr-CA','Proposition complète',2 WHERE EXISTS (SELECT 'x' FROM CFGProposalTypeData WHERE Code = '02');

DELETE FROM CFGPRResponsibilityDescriptions WHERE UICultureName = 'fr-CA';
--....................................................................................................v(80)
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'J' ,'fr-CA','Commune',2 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'J');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'SO','fr-CA','Solo' ,1 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'SO');

DELETE FROM CFGPYAccrualsDescriptions WHERE UICultureName = 'fr-CA';
--............................................................................................................................v(40)
INSERT INTO CFGPYAccrualsDescriptions (Code,Company,UICultureName,Description) SELECT 'Sick Lv.',CFGMainData.Company,'fr-CA','Congé de maladie' FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGPYAccrualsData WHERE Code = 'Sick Lv.' AND Company = CFGMainData.Company)
INSERT INTO CFGPYAccrualsDescriptions (Code,Company,UICultureName,Description) SELECT 'Vacation',CFGMainData.Company,'fr-CA','Vacances'   FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGPYAccrualsData WHERE Code = 'Vacation' AND Company = CFGMainData.Company)

DELETE FROM CFGRGMethodsDescriptions WHERE UICultureName = 'fr-CA';
--..........................................................................................v(40)
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'B','fr-CA','Facturations'                   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'B');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'N','fr-CA','Aucune reconnaissance des revenus'     WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'N');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'P','fr-CA','(Pourcentage de frais de rémun.*) + Frais remb.' WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'P');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'W','fr-CA','Facturations + Travaux en cours selon Facturation'   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'W');

DELETE FROM CFGSuffixDescriptions WHERE UICultureName = 'fr-CA';
--......................................................................................v(150)
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'I'  ,'fr-CA','I'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'I');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'II' ,'fr-CA','II' ,4 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'II');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'III','fr-CA','III',5 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'III');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'Jr.','fr-CA','Débutant',1 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'Jr.');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'Sr.','fr-CA','Senior',2 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'Sr.');

DELETE FROM CFGUnitTypeDescriptions WHERE UICultureName = 'fr-CA';
--...................................................................................................v(50)
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Equipment','fr-CA','Équipement',1 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Equipment');
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Expenses' ,'fr-CA','Dépenses' ,2 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Expenses');
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Labor'    ,'fr-CA','Main-d’œuvre'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Labor');

DELETE FROM CFGVendorRoleDescriptions WHERE UICultureName = 'fr-CA';
--..............................................................................................v(50)
INSERT INTO CFGVendorRoleDescriptions (Code,UICultureName,Description,Seq) SELECT '01','fr-CA','Paie'        ,1 WHERE EXISTS (SELECT 'x' FROM CFGVendorRoleData WHERE Code = '01');
INSERT INTO CFGVendorRoleDescriptions (Code,UICultureName,Description,Seq) SELECT '02','fr-CA','Ressources humaines',2 WHERE EXISTS (SELECT 'x' FROM CFGVendorRoleData WHERE Code = '02');
INSERT INTO CFGVendorRoleDescriptions (Code,UICultureName,Description,Seq) SELECT '03','fr-CA','Agent de voyage'   ,3 WHERE EXISTS (SELECT 'x' FROM CFGVendorRoleData WHERE Code = '03');

DELETE FROM CFGVendorTypeDescriptions WHERE UICultureName = 'fr-CA';
--...................................................................................v(30)
INSERT INTO CFGVendorTypeDescriptions (Type,UICultureName,Label) SELECT 'G','fr-CA','Général'       WHERE EXISTS (SELECT 'x' FROM CFGVendorTypeData WHERE Type = 'G');
INSERT INTO CFGVendorTypeDescriptions (Type,UICultureName,Label) SELECT 'S','fr-CA','Sous-traitant' WHERE EXISTS (SELECT 'x' FROM CFGVendorTypeData WHERE Type = 'S');

DELETE FROM FW_CFGAttachmentCategoryDesc WHERE UICultureName = 'fr-CA';
--................................................................................................................................................v(255)
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Notes'          ,'Activities'         ,'fr-CA','Remarques'                   ,1  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Notes' and Application ='Activities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'NDA'            ,'Contacts'           ,'fr-CA','Accord de non-divulgation',2  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'NDA' and Application ='Contacts');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Notes'          ,'Contacts'           ,'fr-CA','Remarques'                   ,3  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Notes' and Application ='Contacts');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'CompanyOverview','TextLibrary'        ,'fr-CA','Vue d''ensemble de l''entreprise'        ,4  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'CompanyOverview' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Case Study'     ,'TextLibrary'        ,'fr-CA','Étude de cas'              ,5  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Case Study' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Reference'      ,'TextLibrary'        ,'fr-CA','Référence'               ,6  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Reference' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Resume'         ,'Employees'          ,'fr-CA','Reprendre'                  ,7  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Resume' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Award'          ,'Employees'          ,'fr-CA','Prix'                   ,8  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Award' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Leadership'     ,'Employees'          ,'fr-CA','Leadership éclairé'      ,9  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Leadership' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Firms'              ,'fr-CA','Contrat de service principal',10 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Firms'              ,'fr-CA','Énoncé des travaux'       ,11 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'NDA'            ,'Firms'              ,'fr-CA','Accord de non-divulgation',12 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'NDA' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'ClientACH'      ,'Firms'              ,'fr-CA','Client ACH'              ,13 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'ClientACH' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'VendorACH'      ,'Firms'              ,'fr-CA','Fournisseur ACH'              ,14 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'VendorACH' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'DigitalArtifact','Marketing Campaigns','fr-CA','Artefact numérique'        ,15 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'DigitalArtifact' and Application ='Marketing Campaigns');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Estimate'       ,'Opportunities'      ,'fr-CA','Estimation'                ,16 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Estimate' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Proposal'       ,'Opportunities'      ,'fr-CA','Proposition'                ,17 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Proposal' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Opportunities'      ,'fr-CA','Énoncé des travaux'       ,18 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Opportunities'      ,'fr-CA','Contrat de service principal',19 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Estimate'       ,'Projects'           ,'fr-CA','Estimation'                ,20 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Estimate' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Proposal'       ,'Projects'           ,'fr-CA','Proposition'                ,21 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Proposal' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Projects'           ,'fr-CA','Énoncé des travaux'       ,22 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Projects'           ,'fr-CA','Contrat de service principal',23 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Projects');

DELETE FROM FW_CFGLabelData WHERE UICultureName = 'fr-CA';
--............................................................................................................................................................................v(100)
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','accountLabel'                            ,'Compte'                       ,'[Account]'                    ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','accountLabelPlural'                      ,'Comptes'                      ,'[Accounts]'                   ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','clientLabel'                             ,'Client'                        ,'[Client]'                     ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','clientLabelPlural'                       ,'Clients'                       ,'[Clients]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','contactLabel'                            ,'Contact'                       ,'[Contact]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','contactLabelPlural'                      ,'Contacts'                      ,'[Contacts]'                   ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','employeeLabel'                           ,'Employé'                      ,'[Employee]'                   ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','employeeLabelPlural'                     ,'Employés'                     ,'[Employees]'                  ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','labcd1Label'                             ,'Code des honoraires de Niveau 1'            ,'[Labcd Level 1]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','labcd1LabelPlural'                       ,'Code des honoraires de Niveau 1s'           ,'[Labcd Level 1s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','labcd2Label'                             ,'Code des honoraires de Niveau 2'            ,'[Labcd Level 2]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','labcd2LabelPlural'                       ,'Code des honoraires de Niveau 2s'           ,'[Labcd Level 2s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','labcd3Label'                             ,'Code des honoraires de Niveau 3'            ,'[Labcd Level 3]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','labcd3LabelPlural'                       ,'Code des honoraires de Niveau 3s'           ,'[Labcd Level 3s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','labcd4Label'                             ,'Code des honoraires de Niveau 4'            ,'[Labcd Level 4]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','labcd4LabelPlural'                       ,'Code des honoraires de Niveau 4s'           ,'[Labcd Level 4s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','labcd5Label'                             ,'Code des honoraires de Niveau 5'            ,'[Labcd Level 5]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','labcd5LabelPlural'                       ,'Code des honoraires de Niveau 5s'           ,'[Labcd Level 5s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','labcdLabel'                              ,'Code des honoraires'                    ,'[Labor Code]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','labcdLabelPlural'                        ,'Codes des honoraires'                   ,'[Labor Codes]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','firmLabel'                               ,'Entreprise'                          ,'[Firm]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','firmLabelPlural'                         ,'Entreprises'                         ,'[Firms]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','mktLabel'                                ,'Activité de marketing'              ,'[Marketing Campaign]'         ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','mktLabelPlural'                          ,'Activités de marketing'             ,'[Marketing Campaigns]'        ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','org1Label'                               ,'Emplacement'                      ,'[Org Level 1]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','org1LabelPlural'                         ,'Emplacements'                     ,'[Org Level 1s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','org2Label'                               ,'Département'                    ,'[Org Level 2]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','org2LabelPlural'                         ,'Départements'                   ,'[Org Level 2s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','org3Label'                               ,'Organisation 3'                ,'[Org Level 3]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','org3LabelPlural'                         ,'Organisation 3S'               ,'[Org Level 3s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','org4Label'                               ,'Organisation 4'                ,'[Org Level 4]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','org4LabelPlural'                         ,'Organisation 4s'               ,'[Org Level 4s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','org5Label'                               ,'Organisation 5'                ,'[Org Level 5]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','org5LabelPlural'                         ,'Organisation 5s'               ,'[Org Level 5s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','orgLabel'                                ,'Organisation'                  ,'[Organization]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','orgLabelPlural'                          ,'Organisations'                 ,'[Organizations]'              ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','proposalLabel'                           ,'Proposition'                      ,'[Proposal]'                   ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','proposalLabelPlural'                     ,'Propositions'                     ,'[Proposals]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','textLibLabel'                            ,'Expression standard'                   ,'[Text Library]'               ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','textLibLabelPlural'                      ,'Expressions standard'                  ,'[Text Libraries]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','unitLabel'                               ,'Unité'                          ,'[Unit]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','unitLabelPlural'                         ,'Unités'                         ,'[Units]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','vendorLabel'                             ,'Fournisseur'                        ,'[Vendor]'                     ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','vendorLabelPlural'                       ,'Fournisseurs'                       ,'[Vendors]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','wbs1Label'                               ,'Engagement'                    ,'[WBS1]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','wbs1LabelPlural'                         ,'Engagements'                   ,'[WBS1s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','wbs2Label'                               ,'Phase'                         ,'[WBS2]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','wbs2LabelPlural'                         ,'Phases'                        ,'[WBS2s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','wbs3Label'                               ,'Tâche'                          ,'[WBS3]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','wbs3LabelPlural'                         ,'Tâches'                         ,'[WBS3s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','sysPM'                                   ,'Gestionnaire d''engagement'            ,'[Project Manager]'            ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','sysPR'                                   ,'Client exécutif'              ,'[Principal]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','sysSP'                                   ,'Conduite de pratique'                 ,'[Supervisor]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','equipmentLabel'                          ,'Équipement'                     ,'[Equipment]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','equipmentLabelPlural'                    ,'Équipement'                     ,'[Equipments]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','jobToDateLabel'                          ,'Engagement à ce jour'            ,'[JobToDate]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','JTDLabel'                                ,'ETD'                           ,'[JTD]'                        ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','estimateToCompleteLabel'                 ,'Coût estimé pour achèvement (CEPA)'          ,'[EstimateToComplete]'         ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','ETCLabel'                                ,'CEPA'                           ,'[ETC]'                        ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','estimateAtCompletionLabel'               ,'Coûts estimés à l''achèvement (CEAA)'        ,'[EstimateAtCompletion]'       ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','EACLabel'                                ,'CEAA'                           ,'[EAC]'                        ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','sysPRM'                                  ,'Responsable des propositions'              ,'[Proposal Manager]'           ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','sysMC'                                   ,'Coordinateur du marketing'         ,'[Marketing Coordinator]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','sysBDL'                                  ,'Responsable du développement d''activité'     ,'[Business Development Lead]'  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','consultantLabel'                         ,'Sous-traitant'                    ,'[Consultant]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','consultantLabelPlural'                   ,'Sous-traitants'                   ,'[Consultants]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','compensationLabel'                       ,'Rémunération'                  ,'[Compensation]'               ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','revType1Label'                           ,'Main-d’œuvre'                         ,'[Revenue Method Type 1]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','revType2Label'                           ,'Sous-traitant'                    ,'[Revenue Method Type 2]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','revType3Label'                           ,'Remb.'                         ,'[Revenue Method Type 3]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','revType4Label'                           ,'Méthode de revenus 4'              ,'[Revenue Method Type 4]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','revType5Label'                           ,'Méthode de revenus 5'              ,'[Revenue Method Type 5]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','feeEstimatingCostGroupLabel'             ,'Groupe de coûts'                    ,'[Cost Group]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','feeEstimatingCostGroupLabelPlural'       ,'Groupes de coûts'                   ,'[Cost Groups]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','feeEstimatingFunctionalGroupLabel'       ,'Groupe fonctionnel'              ,'[Functional Group]'           ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','feeEstimatingFunctionalGroupLabelPlural' ,'Groupes fonctionnels'             ,'[Functional Groups]'          ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','feeEstimatingServiceProfileLabel'        ,'Profil de service'               ,'[Service Profile]'            ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('fr-CA','feeEstimatingServiceProfileLabelPlural'  ,'Profils de service'              ,'[Service Profiles]'           ,'N','N',NULL);

COMMIT TRANSACTION
    
END
GO
