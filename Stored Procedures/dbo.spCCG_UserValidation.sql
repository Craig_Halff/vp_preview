SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_UserValidation]
    @ModUser VARCHAR(255)
  , @Action VARCHAR(255)
AS
/* 
	Copyright (c) 2021 Central Consulting Group.  All rights reserved.
	09/23/2021	David Springer
				Validate User against a particular Action
				Call this on a project CHANGE workflow

	09/23/2021	Craig H. Anderson
				Added Project Name Change
*/
    BEGIN
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
        SET NOCOUNT ON;

        IF @Action = 'Awarded Stage'
            AND (
                    SELECT Role FROM dbo.SEUser WHERE Username = @ModUser
                ) NOT IN ( 'ACCT-PA', 'ACCT-PA-CASH', 'ACCT-PA-COST', 'ACCT-PA-FLEET', 'ACCT-PAM', 'ACCT-SA', 'ACCT-SUPER' )
            BEGIN
                RAISERROR('<br><h3>You are not authorized to set the Stage to Awarded.</h3>', 16, 1);
            END;

        IF @Action = 'Project Name Change'
            AND (
                    SELECT Role FROM dbo.SEUser WHERE Username = @ModUser
                ) NOT IN ( 'ACCT-PA', 'ACCT-PA-CASH', 'ACCT-PA-COST', 'ACCT-PA-FLEET', 'ACCT-PAM', 'ACCT-SA', 'ACCT-SUPER' )
            BEGIN
                RAISERROR(
                    '<br><h3>You are not authorized to change the project Short or Long name after it has been awarded.</h3><p>Contact your Project Accountant if a change is necessary.</p>', 16, 1
                );
            END;

        IF @Action = 'Billing Client Change'
            AND (
                    SELECT Role FROM dbo.SEUser WHERE Username = @ModUser
                ) NOT IN ( 'ACCT-PA', 'ACCT-PA-CASH', 'ACCT-PA-COST', 'ACCT-PA-FLEET', 'ACCT-PAM', 'ACCT-SA', 'ACCT-SUPER' )
            BEGIN
                RAISERROR('<br><h3>You are not authorized to change the project Billing Client after it has been awarded.</h3><p>Contact your Project Accountant if a change is necessary.</p>', 16, 1);
            END;

    END;
GO
