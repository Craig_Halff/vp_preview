SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Ctrl_DoEntryColumnChange] (
	@EMPLOYEEID				Nvarchar(32),
	@seq					Nvarchar(50),
	@ConfigCustomColumn		Nvarchar(32),
	@ColumnLabel			Nvarchar(500),
	@DatabaseField			Nvarchar(100),
	@DatabaseModEmpField	Nvarchar(100),
	@decValue				bit,
	@DataType				Nvarchar(200),
	@oldValue				Nvarchar(max),
	@newValue				Nvarchar(max)
) AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max);

    SET @sql = N'
        IF exists (SELECT ''x'' FROM CCG_PAT_CustomColumns WHERE PayableSeq = ' + @seq + ')
            UPDATE CCG_PAT_CustomColumns SET
                ' + @DatabaseField + ' = ' + @newValue + ',
                ' + @DatabaseModEmpField + ' = N''' + @EMPLOYEEID + '''
                WHERE PayableSeq = ' + @seq + '
        ELSE
            INSERT INTO CCG_PAT_CustomColumns(PayableSeq, ' + @DatabaseField + ', ' + @DatabaseModEmpField + ')
                VALUES (' + @seq + ', ' + @newValue + ', N''' + @EMPLOYEEID + ''')';
	EXEC(@sql);

    SET @sql = N'
        INSERT INTO CCG_PAT_HistoryUpdateColumns (ActionTaken, ActionDate, ActionTakenBy, PayableSeq, DataType,
                ConfigCustomColumn, ColumnLabel, OldValue,
                NewValue, OldValueDecimal, NewValueDecimal)
            VALUES (N''Change'', getutcdate(), N''' + @EMPLOYEEID + ''', ' + @seq + ', ''' + @DataType + ''',
                ' + @ConfigCustomColumn + ', ''' + Replace(@ColumnLabel, N'''', N'''''') + ''', ' + @oldValue + ',
                ' + @newValue + ', ' + (CASE WHEN @decValue = 1 THEN @oldValue ELSE 'NULL' END) + ', ' + (CASE WHEN @decValue = 1 THEN @newValue ELSE 'NULL' END) + ')';

	EXEC(@sql);
END;

GO
