SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectBillingTermsInsert] (
   @WBS1        varchar(32), 
   @WBS2        varchar(7), 
   @WBS3        varchar(7), 
   @BillingType varchar(225))
AS
/*
Copyright (c) 2017 Central Consulting Group. All rights reserved.
02/27/2017 David Springer
           Create billing terms based on Billing Type.
           Allow rebuild of billing terms.
           Call from Project CHANGE workflow when Billing Type has changed and is not empty.
03/09/2017 David Springer
           Changed Lump Sum to FeeMeth = 3
03/27/2017 David Springer
           Add Phase limits to Sublevel Terms at the project level
		   Set Billing Type ...Rate Mult1 = 1 and Mult2 = 0
04/03/2017 David Springer
           Added Limit at Level Above; added Fee Type as a sub type to Billing Type; added Tax Code
04/10/2017 David Springer
           Cost Plus Fixed Fee to use Add-On and Add-On Limit.  Added WBS3 validation and Show Task Breakdown.
04/25/2017 David Springer
           Set AP Defaults = N when Billing Type is any Lump Sum or Percent Construction
05/18/2017 David Springer
           Added project Reimb Expense Table
01/19/2018 David Springer
           Removed project Reimb Expense Table & added Reimb Mileage Multiplier, Expense & Consultant Tables
01/24/2018 David Springer
           Added expense table validation
03/05/2018 David Springer
           Restricted interest in Fee to Date
06/27/2018 David Springer
           Removed Tax Code from project level when Sublevel Terms
06/29/2018 David Springer
           Added Show Task Breakdown on Lump Sum by Task
06/29/2018 David Springer
		   Added Billing Term Notes & Description
12/4/2020  Craig H. Anderson
           Added code at bottom to set  CustInvoiceTemplate.Template to match BT.Template
04/22/2021 Craig H. Anderson
           Now truncating px.CustBillingTermNotes to 254-characters before assignint to the BT.Note VARCHAR(255) field
		   to avoid truncation errors when running.

*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
DECLARE @Company      varchar (255),
        @BilledToDate money,
        @Fee          money,
        @FeePercent   decimal(19,4),
		@PhaseSeq     integer,
		@PhaseGroup   varchar (10),
		@ExpPhase     varchar (32),
		@ErrorMessage varchar (8000),
		@LimitAtLevelAbove varchar (1)

BEGIN
	exec spCCG_ProjectBillingTermsExpTableValidation @WBS1, @WBS2, @WBS3

   If @BillingType like 'Cost Plus%' and @BillingType not like 'Cost Plus%Max%' and 
      exists (Select 'x' From ProjectCustomTabFields 
	          Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 <> @WBS3 and WBS3 <> ' ' and CustBillingType like 'Cost Plus%Max%')
	  RaisError ('There are tasks with Cost Plus Max.  This task cannot be Cost Plus.                             ' , 16, 1)

   If @BillingType like 'Cost Plus%Max%' and 
      exists (Select 'x' From ProjectCustomTabFields 
	          Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 <> @WBS3 and WBS3 <> ' ' and CustBillingType not like 'Cost Plus%Max%')
	  RaisError ('There are tasks with Cost Plus.  This task cannot be Cost Plus Max.                           ' , 16, 1)

   If @BillingType like 'Cost Plus%Max%' and 
      exists (Select 'x' From PR
	          Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3 and (ConsultFee <> 0 or FeeDirExp <> 0))
	  RaisError ('Cost Plus Max cannot have Direct Expense nor Direct Consultant fees.                           ' , 16, 1)

   Select @Company = Left (Org, (Select Org1Length From CFGFormat)) From PR Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3

-- Project level
   If @WBS2 = ' '
      Begin
      If IsNull (@BillingType, '') <> 'Sublevel Terms'
	     Begin
      -- Delete billing terms at all levels
         Delete From BTA Where WBS1 = @WBS1
         Delete From BTFCategory Where WBS1 = @WBS1
         Delete From BTF Where WBS1 = @WBS1
         Delete From BTTaxCodes Where WBS1 = @WBS1
         Delete From BT Where WBS1 = @WBS1

      -- Clear phase & task level Billing Type if project level Billing Type changed
         Update ProjectCustomTabFields
         Set CustBillingType = null,
		     CustFeeType = null
         Where WBS1 = @WBS1
           and WBS2 <> ' ' 

      -- Assign the phase & task level Fee Type = project level Fee Type
         Update p2
	     Set p2.CustFeeType = p1.CustFeeType
		 From ProjectCustomTabFields p1, ProjectCustomTabFields p2
		 Where p1.WBS1 = @WBS1
		   and p1.WBS2 = ' ' -- project level
		   and p1.WBS1 = p2.WBS1
		   and p2.WBS2 <> ' ' -- phase & task level

		 End
      Else
	     Begin
      -- Delete billing terms at project level only
         Delete From BTA Where WBS1 = @WBS1 and WBS2 = ' '
         Delete From BTFCategory Where WBS1 = @WBS1 and WBS2 = ' '
         Delete From BTF Where WBS1 = @WBS1 and WBS2 = ' '
         Delete From BTTaxCodes Where WBS1 = @WBS1 and WBS2 = ' '
         Delete From BT Where WBS1 = @WBS1 and WBS2 = ' '
		 End

      End -- project level

-- Phase level
   If @WBS2 <> ' ' and @WBS3 = ' '
      Begin
	  If not exists (Select 'x' From ProjectCustomTabFields Where WBS1 = @WBS1 and WBS2 = ' ' and (CustBillingType ='Sublevel Terms' or (CustBillingType like 'Cost Plus%' and CustBillingType not like '%Max%')))
		 Begin
		 Set @ErrorMessage = 'Phase ' + @WBS2 + ' cannot have a Billing Type, because project does not have a Billing Type = Sublevel Terms.'
		 RAISERROR (@ErrorMessage, 16, 1)
		 End

   -- Clear phase & task level billing terms if project level Billing Type changed
      If IsNull (@BillingType, '') <> 'Sublevel Terms'
	     Begin
         Delete From BTA Where WBS1 = @WBS1 and WBS2 = @WBS2
         Delete From BTFCategory Where WBS1 = @WBS1 and WBS2 = @WBS2
         Delete From BTF Where WBS1 = @WBS1 and WBS2 = @WBS2
         Delete From BTTaxCodes Where WBS1 = @WBS1 and WBS2 = @WBS2
         Delete From BT Where WBS1 = @WBS1 and WBS2 = @WBS2

      -- Clear task level Billing Type if phase level Billing Type changed
         Update ProjectCustomTabFields
         Set CustBillingType = null,
		     CustFeeType = null
         Where WBS1 = @WBS1
           and WBS2 = @WBS2
           and WBS3 <> ' ' 

      -- Assign the task level Fee Type = phase level Fee Type
         Update p2
	     Set p2.CustFeeType = p1.CustFeeType
		 From ProjectCustomTabFields p1, ProjectCustomTabFields p2
		 Where p1.WBS1 = @WBS1
		   and p1.WBS2 = @WBS2 -- phase level
		   and p1.WBS3 = ' '
		   and p1.WBS1 = p2.WBS1
		   and p1.WBS2 = p2.WBS2
		   and p2.WBS3 <> ' ' -- task level

         End -- Billing Type <> Sublevel
      Else
	     Begin
         Delete From BTA Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = ' '
         Delete From BTFCategory Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = ' '
         Delete From BTF Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = ' '
         Delete From BTTaxCodes Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = ' '
         Delete From BT Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = ' '
         End 
      End -- phase level

   If @WBS3 <> ' ' -- Task level
      Begin
      If not exists (Select 'x' From ProjectCustomTabFields Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = ' ' and CustBillingType = 'Sublevel Terms')
		Begin
		Set @ErrorMessage = 'Task ' + @WBS3 + ' cannot have a Billing Type, because Phase ' + @WBS2 + ' does not have a Billing Type = Sublevel Terms.'
		RAISERROR (@ErrorMessage, 16, 1)
		End

      Delete From BTA Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
      Delete From BTF Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
      Delete From BTTaxCodes Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
      Delete From BT Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3

   -- Assign the task level Fee Type = phase level Fee Type if task level Billing Type is null
      Update p3
	  Set p3.CustFeeType = p2.CustFeeType
	  From ProjectCustomTabFields p2, ProjectCustomTabFields p3
	  Where p2.WBS1 = @WBS1
		and p2.WBS2 = @WBS2 -- phase level
		and p2.WBS3 = ' '
		and p2.WBS1 = p3.WBS1
		and p2.WBS2 = p3.WBS2
		and p3.WBS3 <> ' ' -- task level
        and p3.CustBillingType is null

      End -- Task level

-- Create Billing Terms for all Billing Types based on Billing Term Defaults
-- then alter the billing terms based on specific Billing Types.
   Insert into BT
    (WBS1, WBS2, WBS3, Template, CreditTemplate, UseClientAddress, 
    Description, Notes, NextInvoice, DraftPrint, 
    LabMeth, Mult1, Mult2, Mult3, SeparateOvt, OvtMult, RateTable, ORTable, LabDetail, ShowComment, ShowRate, ShowDate, ShowMults, ShowOvtMult, 
    FlagOverrides, SortMeth1, SortTable1, SortMeth2, SortTable2, 
    ExpMeth, ExpMult, ExpTable, ExpShowMult, ExpDetail, 
    ConMeth, ConMult, ConTable, ConShowMult, ConDetail, 
    UnitMeth, UnitMult, UnitShowMult, UnitDetail, FeeMeth, 
    FeeBasis, FeeFactor1, FeeFactor2, FeePctCpl, FeeLabel, PriorFee, 
    AddOns, Interest, GracePd, 
    LimitMeth, LabLimit, ConLimit, ExpLimit, UnitLimit, AddOnLimit, FeeLimit, 
    Retainage, RetPct, RetLimit, RetLab, RetExp, RetCon, RetFee, RetUnit, RetAddOn, 
    ConsolidatePrinting, ConsolidateWBS3, SubLevelTerms, ConsolidatePosting, WBS2ToPost, WBS3ToPost, 
    SubLevelRollup, WBS3Rollup, LimitsBySubLevel, InterestBySubLevel, TaxBySubLevel, RetainageBySubLevel, 
    BackupReport, BackupComments, BackupVendorInvoice, 
    LabAccount, ConAccount, ExpAccount, UnitAccount, FeeAccount, AddOnAccount, IntAccount, Footer, 
    LabDefault, ExpDefault, ConDefault, UnitDefault, PostFeesByPhase, SpecialOvtMult, 
    BackupLaborCode, BackupEKDescription, 
    IntercompanyTax, IntercompanyTaxWBS2ToPost, IntercompanyTaxWBS3ToPost, printLaborForFixedFee, 
    BackupLabor, BackupLaborCategory, BackupEmployeeNumber, BackupLaborSort, BackupLaborTable, BackupExpense, 
    BackupAPDescription, BackupMileage, BackupTransType, BackupExpenseSort, BackupExpenseTable, BackupEXDescription, 
    BackupLabDefault, DraftInvoice, ScheduledBilling, ConsolidateTaxPosting, 
    TaxWBS2ToPost, TaxWBS3ToPost, TaxRetainage, 
    FeeByDetailEnabled, OverUnderAccount, 
    FeeToDate, Fee1, FeePctCpl1, PriorFee1, FeeToDate1, Fee2, FeePctCpl2, PriorFee2, FeeToDate2, Fee3, FeePctCpl3, PriorFee3, FeeToDate3, 
    Fee4, FeePctCpl4, PriorFee4, FeeToDate4, Fee5, FeePctCpl5, PriorFee5, FeeToDate5, 
    InvoiceApprovalEnabled, InvoiceApprovalCode,
    DaysBeforeDue, PayTerms, ShowDueDate,
    PrintARBreakout, PrintARWBS2Totals, PrintBTDARBalanceTotal, PrintBTDBreakout, PrintBTDReceivedTotal, PrintBTDWBS2Totals, PrintWBS1AR, PrintWBS1BTD, PrintWBS2AR, PrintWBS2BTD,
    SupportDocuments, SupportDocumentsAP, SupportDocumentsBkupRpt, SupportDocumentsEX)
   Select
    @WBS1, @WBS2, @WBS3, Template, CreditTemplate, UseClientAddress,
    Description, Notes, NextInvoice, DraftPrint, 
    LabMeth, Mult1, Mult2, Mult3, SeparateOvt, OvtMult, RateTable, ORTable, LabDetail, ShowComment, ShowRate, ShowDate, ShowMults, ShowOvtMult, 
    FlagOverrides, SortMeth1, SortTable1, SortMeth2, SortTable2, 
    ExpMeth, ExpMult, ExpTable, ExpShowMult, ExpDetail, 
    ConMeth, ConMult, ConTable, ConShowMult, ConDetail, 
    UnitMeth, UnitMult, UnitShowMult, UnitDetail, FeeMeth, 
    FeeBasis, FeeFactor1, FeeFactor2, FeePctCpl, FeeLabel, PriorFee, 
    AddOns, Interest, GracePd, 
    LimitMeth, LabLimit, ConLimit, ExpLimit, UnitLimit, AddOnLimit, FeeLimit, 
    Retainage, RetPct, RetLimit, RetLab, RetExp, RetCon, RetFee, RetUnit, RetAddOn, 
    ConsolidatePrinting, ConsolidateWBS3, SubLevelTerms, ConsolidatePosting, WBS2ToPost, WBS3ToPost, 
    SubLevelRollup, WBS3Rollup, LimitsBySubLevel, InterestBySubLevel, TaxBySubLevel, RetainageBySubLevel, 
    BackupReport, BackupComments, BackupVendorInvoice, 
    LabAccount, ConAccount, ExpAccount, UnitAccount, FeeAccount, AddOnAccount, IntAccount, Footer, 
    LabDefault, ExpDefault, ConDefault, UnitDefault, PostFeesByPhase, SpecialOvtMult, 
    BackupLaborCode, BackupEKDescription, 
    IntercompanyTax, IntercompanyTaxWBS2ToPost, IntercompanyTaxWBS3ToPost, printLaborForFixedFee, 
    BackupLabor, BackupLaborCategory, BackupEmployeeNumber, BackupLaborSort, BackupLaborTable, BackupExpense, 
    BackupAPDescription, BackupMileage, BackupTransType, BackupExpenseSort, BackupExpenseTable, BackupEXDescription, 
    BackupLabDefault, DraftInvoice, ScheduledBilling, ConsolidateTaxPosting, 
    TaxWBS2ToPost, TaxWBS3ToPost, TaxRetainage, 
    FeeByDetailEnabled, OverUnderAccount, 
    FeeToDate, Fee1, FeePctCpl1, PriorFee1, FeeToDate1, Fee2, FeePctCpl2, PriorFee2, FeeToDate2, Fee3, FeePctCpl3, PriorFee3, FeeToDate3, 
    Fee4, FeePctCpl4, PriorFee4, FeeToDate4, Fee5, FeePctCpl5, PriorFee5, FeeToDate5, 
    InvoiceApprovalEnabled, InvoiceApprovalCode,
    DaysBeforeDue, PayTerms, ShowDueDate,
    PrintARBreakout, PrintARWBS2Totals, PrintBTDARBalanceTotal, PrintBTDBreakout, PrintBTDReceivedTotal, PrintBTDWBS2Totals, PrintWBS1AR, PrintWBS1BTD, PrintWBS2AR, PrintWBS2BTD,
    SupportDocuments, SupportDocumentsAP, SupportDocumentsBkupRpt, SupportDocumentsEX
   From BTDefaults
   Where DefaultType = '<D>'
     and Company = @Company  -- Set Company for multi-company from project Org

   Update b
   Set b.Notes = LEFT(dbo.CCG_StripHTMLTag(px.CustBillingTermNotes,''),254)
   From ProjectCustomTabFields px, BT b
   Where b.WBS1 = @WBS1
     and b.WBS2 = @WBS2
     and b.WBS3 = @WBS3
     and b.WBS1 = px.WBS1
     and b.WBS2 = px.WBS2
     and b.WBS3 = px.WBS3
	 and px.CustBillingTermNotes is not null

-- Set Billing Term Notes
   Update px
   Set px.CustBillingTermNotes = Replace(dbo.fnCCG_ConvertHtml_LeaveNewLines(replace(px.CustBillingTermNotes, CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10), CHAR(13)+CHAR(10))),
			CHAR(13)+CHAR(10), '<br/>')
   From ProjectCustomTabFields px, BT b
   Where b.WBS1 = @WBS1
     and b.WBS2 = @WBS2
     and b.WBS3 = @WBS3
     and b.WBS1 = px.WBS1
     and b.WBS2 = px.WBS2
     and b.WBS3 = px.WBS3
	 and px.CustBillingTermNotes is not null

   Update b
   Set b.Description = Replace(dbo.fnCCG_ConvertHtml_LeaveNewLines(replace(px.CustBillingTermDescription, CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10), CHAR(13)+CHAR(10))),
			CHAR(13)+CHAR(10), '<br/>')
   From ProjectCustomTabFields px, BT b
   Where b.WBS1 = @WBS1
     and b.WBS2 = @WBS2
     and b.WBS3 = @WBS3
     and b.WBS1 = px.WBS1
     and b.WBS2 = px.WBS2
     and b.WBS3 = px.WBS3
	 and px.CustBillingTermDescription is not null

-- Set Billing Term Description
   Update px
   Set px.CustBillingTermDescription = Replace(dbo.fnCCG_ConvertHtml_LeaveNewLines(replace(px.CustBillingTermDescription, CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10), CHAR(13)+CHAR(10))),
			CHAR(13)+CHAR(10), '<br/>')
   From ProjectCustomTabFields px, BT b
   Where b.WBS1 = @WBS1
     and b.WBS2 = @WBS2
     and b.WBS3 = @WBS3
     and b.WBS1 = px.WBS1
     and b.WBS2 = px.WBS2
     and b.WBS3 = px.WBS3
	 and px.CustBillingTermDescription is not null

-- Add Tax Code
   Insert Into BTTaxCodes
   (WBS1, WBS2, WBS3, Seq, TaxCode)
   Select WBS1, WBS2, WBS3, 1, CustTaxCode
   From ProjectCustomTabFields
   Where WBS1 = @WBS1
     and WBS2 = @WBS2
	 and WBS3 = @WBS3
	 and CustTaxCode is not null
	 and CustBillingType <> 'Sublevel Terms'

-- Set Sublevel Terms Tax by Phase = Y when tax codes added
   Update b
   Set TaxBySubLevel = 'Y'
   From BT b, ProjectCustomTabFields px
   Where b.WBS1 = @WBS1
     and b.WBS2 = ' '
	 and b.WBS1 = px.WBS1
	 and b.WBS2 = px.WBS2
  -- Project level is Sublevel Terms
	 and px.CustBillingType = 'Sublevel Terms'
	 and exists (Select 'x' From BTTaxCodes Where WBS1 = px.WBS1 and WBS2 <> ' ')

-- Labor Multipliers
   Update b
   Set b.Mult1 = CASE When px.CustLaborFringeMultiplier = 0 Then 1 Else px.CustLaborFringeMultiplier END,
       b.Mult2 = CASE When px.CustLaborOvhProfitMultiplier = 0 Then 1 Else px.CustLaborOvhProfitMultiplier END
   From BT b, ProjectCustomTabFields px
   Where b.WBS1 = @WBS1
     and b.WBS2 = @WBS2
     and b.WBS3 = @WBS3
     and b.WBS1 = px.WBS1
     and b.WBS2 = px.WBS2
     and b.WBS3 = px.WBS3

-- Reimb Mileage Multiplier
-- This is set at the project level only in the spCCG_OpportunityCreateProjectWBS procedure
-- As billing terms are created, copy this multiplier down from the project to the level with billing terms
-- EnLink Mileage Multiplier
   Update px
   Set px.CustReimbMileageMultiplier = p1.CustReimbMileageMultiplier
   From ProjectCustomTabFields p1, ProjectCustomTabFields px
   Where p1.WBS1 = @WBS1
	 and p1.WBS2 = ' '  -- project level multiplier
     and p1.WBS1 = px.WBS1
	 and px.WBS2 = @WBS2
	 and px.WBS3 = @WBS3

/*  Expense & Consultant expense tables
Expense 1.00, Mileage 1.0000
Expense 1.05, Mileage 1.0000
Expense 1.10, Mileage 1.0000
Expense 1.00, Mileage 1.1009
Expense 1.10, Mileage 1.1009
Consultant 1.00
Consultant 1.05
Consultant 1.10
*/

-- Expense Multiplier
   Update b
   Set b.ExpTable = t.TableNo,
       b.ExpMult = 1
   From BT b, ProjectCustomTabFields px, BTEA t
   Where b.WBS1 = @WBS1
     and b.WBS2 = @WBS2
     and b.WBS3 = @WBS3
     and b.WBS1 = px.WBS1
     and b.WBS2 = px.WBS2
     and b.WBS3 = px.WBS3
	 and t.TableName = 'Expense ' + convert (varchar, convert (decimal (18,4), px.CustReimbExpenseMultiplier)) + ', Mileage ' + convert (varchar, convert (decimal (18,4), px.CustReimbMileageMultiplier))

-- Consultant Multiplier
   Update b
   Set b.ConTable = t.TableNo,
       b.ConMult = 1
   From BT b, ProjectCustomTabFields px, BTEA t
   Where b.WBS1 = @WBS1
     and b.WBS2 = @WBS2
     and b.WBS3 = @WBS3
     and b.WBS1 = px.WBS1
     and b.WBS2 = px.WBS2
     and b.WBS3 = px.WBS3
	 and t.TableName = 'Consultant ' + convert (varchar, convert (decimal (18,4), px.CustReimbConsultantMultiplier))

-- Show Task Breakdown
   If @WBS2 = ' '
      Update b
      Set b.ConsolidateWBS3 = CASE When p.CustShowTaskBreakdown = 'Y' Then 'N' Else 'Y' END
      From BT b, ProjectCustomTabFields p
      Where b.WBS1 = @WBS1
        and b.WBS2 = @WBS2
        and b.WBS3 = @WBS3
        and b.WBS1 = p.WBS1
        and b.WBS2 = p.WBS2
        and b.WBS3 = p.WBS3

   If @WBS2 <> ' '
      Update b
      Set b.ConsolidatePrinting = CASE When p.CustShowTaskBreakdown = 'Y' Then 'N' Else 'Y' END
      From BT b, ProjectCustomTabFields p
      Where b.WBS1 = @WBS1
        and b.WBS2 = @WBS2
        and b.WBS3 = @WBS3
        and b.WBS1 = p.WBS1
        and b.WBS2 = p.WBS2
        and b.WBS3 = p.WBS3

   If @BillingType = 'Sublevel Terms'
      Begin
      Update BT
      Set SubLevelTerms = 'Y',
	      LabDefault = 'Y', ShowComment = 'N', ShowDate = 'N', ShowRate = 'N', ShowMults = 'N',
		  ConDefault = 'Y', ExpDefault = 'Y', UnitDefault = 'Y'
      Where WBS1 = @WBS1
        and WBS2 = @WBS2
        and WBS3 = ' '

   -- Set Fee Type at the level of the Sublevel Terms
	  Update ProjectCustomTabFields
	  Set CustFeeType = 'Sublevel Terms'
      Where WBS1 = @WBS1
        and WBS2 = @WBS2
        and WBS3 = @WBS3

   -- Set Fee Type at the lower levels if they do not have Billing Type
	  Update ProjectCustomTabFields
	  Set CustFeeType = 'Sublevel Terms'
      Where WBS1 = @WBS1
        and WBS2 = CASE When @WBS2 = ' ' Then WBS2 Else @WBS2 END
        and WBS3 = CASE When @WBS3 = ' ' Then WBS3 Else @WBS3 END
		and CustBillingType is null

      End

   If @BillingType like 'Cost Plus%' -- Any Cost Plus Billing Type
      Begin
      Update PR
      Set BillByDefaultConsultants = 'E', 
          BillByDefaultOtherExp = 'E', 
          BillByDefault = 'C' 
      Where WBS1 = @WBS1
        and WBS2 = @WBS2
        and WBS3 = @WBS3

   -- Set at lowest level below level of Billing Type
      Update PR
      Set BillByDefaultConsultants = 'E', 
          BillByDefaultOtherExp = 'E', 
          BillByDefault = 'C' 
      Where WBS1 = @WBS1
        and WBS2 = CASE When @WBS2 = ' ' Then WBS2 Else @WBS2 END
        and WBS3 = CASE When @WBS3 = ' ' Then WBS3 Else @WBS3 END
		and Sublevel = 'N'

	  Update ProjectCustomTabFields
	  Set CustFeeType = 'Cost Plus'
      Where WBS1 = @WBS1
        and WBS2 = CASE When @WBS2 = ' ' Then WBS2 Else @WBS2 END
        and WBS3 = CASE When @WBS3 = ' ' Then WBS3 Else @WBS3 END

      End

-- Show timesheet comments for Tritex
   If @BillingType like 'Cost Plus%'
      Begin
      Update b
      Set b.ShowComment = 'Y'
      From BT b, PR p
      Where b.WBS1 = @WBS1
        and b.WBS2 = @WBS2
        and b.WBS3 = @WBS3
        and b.WBS1 = p.WBS1
        and b.WBS2 = p.WBS2
        and b.WBS3 = p.WBS3
		and p.Org like '03%'
      End

-- Aggregate Limit
   If @BillingType like 'Cost Plus Max%'
      Begin
      Update b
      Set b.LimitMeth = '1',
	      b.LabLimit = p.FeeDirLab + p.FeeDirExp + p.ConsultFee,
          b.ConLimit = p.ReimbAllowCons,
          b.ExpLimit = p.ReimbAllowExp
      From BT b, PR p
      Where b.WBS1 = @WBS1
        and b.WBS2 = @WBS2
        and b.WBS3 = @WBS3
        and b.WBS1 = p.WBS1
        and b.WBS2 = p.WBS2
        and b.WBS3 = p.WBS3

   -- Phase level
      If @WBS2 <> ' ' and @WBS3 = ' '
         Update BT
         Set LimitsBySubLevel = 'Y'
         Where WBS1 = @WBS1
           and WBS2 = ' ' -- project level

   -- Task level
      If @WBS3 <> ' '
         Update BT
         Set LimitsBySubLevel = 'Y'
         Where WBS1 = @WBS1
		   and WBS2 = @WBS2
           and WBS3 = ' ' -- phase level

	  Update ProjectCustomTabFields
	  Set CustFeeType = 'Cost Plus Max'
      Where WBS1 = @WBS1
        and WBS2 = CASE When @WBS2 = ' ' Then WBS2 Else @WBS2 END
        and WBS3 = CASE When @WBS3 = ' ' Then WBS3 Else @WBS3 END

      Select @LimitAtLevelAbove = CustLimitAtLevelAbove From ProjectCustomTabFields Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
      If @LimitAtLevelAbove = 'Y'
	     exec spCCG_ProjectBillingTermsLimitAbove @WBS1, @WBS2, @WBS3, @LimitAtLevelAbove

      End

   If @BillingType like 'Cost Plus%Rate'
      Begin
      Update BT
      Set LabMeth = '3',
	      Mult1 = 1,
		  Mult2 = 0
      Where WBS1 = @WBS1
        and WBS2 = @WBS2
        and WBS3 = @WBS3
	  
	  Update b
      Set b.RateTable = '1943'
      From BT b, PR p
	  Where b.WBS1 = @WBS1
        and b.WBS2 = @WBS2
        and b.WBS3 = @WBS3
		and p.WBS1 = b.WBS1
		and p.WBS2 = b.WBS2
		and p.WBS3 = b.WBS3
		and p.ClientID = '000301'
      End

   If @BillingType like 'Cost Plus%Mult'
      Begin
      Update BT
      Set ShowMults = 'Y'
      Where WBS1 = @WBS1
        and WBS2 = @WBS2
        and WBS3 = @WBS3
      End

   If @BillingType like 'Lump Sum%'
      Begin
      Update PR 
      Set BillByDefaultConsultants = 'N',
          BillByDefaultOtherExp = 'N',
          BillByDefault = 'N'
      Where WBS1 = @WBS1
	    and WBS2 = @WBS2
		and WBS3 = @WBS3

   -- Set at lowest level below level of Billing Type
      Update PR 
      Set BillByDefaultConsultants = 'N',
          BillByDefaultOtherExp = 'N',
          BillByDefault = 'N'
      Where WBS1 = @WBS1
        and WBS2 = CASE When @WBS2 = ' ' Then WBS2 Else @WBS2 END
        and WBS3 = CASE When @WBS3 = ' ' Then WBS3 Else @WBS3 END
		and Sublevel = 'N'

      Update BT Set
   -- Labor
      LabDetail = 'N', LabDefault = 'Y', ShowRate = 'N', ShowMults = 'N', ShowOvtMult = 'N', ShowComment = 'N',
	  ConDefault = 'Y', ExpDefault = 'Y', UnitDefault = 'Y',
      ConsolidatePrinting = 'Y',
   -- Miscellaneous
      DraftPrint = 'Y'
      Where WBS1 = @WBS1
        and WBS2 = @WBS2 
        and WBS3 = @WBS3

      Update BT Set
   -- Project Level
      DraftPrint = 'Y'
      Where WBS1 = @WBS1
        and WBS2 = ' '

	  Update ProjectCustomTabFields
	  Set CustFeeType = 'Lump Sum'
      Where WBS1 = @WBS1
        and WBS2 = CASE When @WBS2 = ' ' Then WBS2 Else @WBS2 END
        and WBS3 = CASE When @WBS3 = ' ' Then WBS3 Else @WBS3 END

	  End -- like Lump Sum

   If @BillingType = 'Lump Sum'  -- Overall Percent Complete
      Begin
      Update BT
      Set FeeMeth = '3', -- 2 = Percent Complete by Phase; 3 = Fixed Amount by Phase
	      PostFeesByPhase = 'Y'
		  --b.FeeMeth = '1',
          --b.FeeBasis = 'L', 
          --b.FeeFactor1 = p.FeeDirLab + p.FeeDirExp + p.ConsultFee
      Where WBS1 = @WBS1
        and WBS2 = @WBS2
        and WBS3 = @WBS3

      If @WBS2 <> ' ' and @WBS3 = ' ' -- Phase billing terms
	     Begin
      -- Insert new Billing Phase records (Fee);  FeeMeth = 3-Percent Complete by Phase as Fixed Amount
         Insert Into BTF (WBS1, WBS2, WBS3, Seq, Phase, Name, Fee, PostWBS1, PostWBS2, PostWBS3)
         Select p.WBS1, p.WBS2, p.WBS3, 
           (Select IsNull (Max (Seq), 0) From BTF Where WBS1 = p.WBS1) + row_number () over (order by p.WBS2) Seq, 
           (Select IsNull (Max (Seq), 0) From BTF Where WBS1 = p.WBS1) + row_number () over (order by p.WBS2) Phase, 
            p.Name, p.FeeDirLab + p.FeeDirExp + p.ConsultFee, 
            p.WBS1 PostWBS1, 
            p.WBS2 PostWBS2,
            (Select Min ( WBS3) From PR Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 <> ' ') PostWBS3
         From PR p
         Where p.WBS1 = @WBS1
	       and p.WBS2 = @WBS2
           and p.WBS3 = ' ' -- phase level fee
           and not exists (Select * From BTF 
                           Where WBS1 = p.WBS1 and WBS2 = p.WBS2 and WBS3 = p.WBS3 and IsNull (PostWBS2, ' ') = p.WBS2)

		  Update b
		  Set b.FeeToDate = i.Amount,
			  b.BilledJTD = i.Amount,
			  b.PctComplete = 
			  CASE When b.Fee = 0 Then 0
				   Else i.Amount / b.Fee * 100
			  END
		  From BTF b, 
				(Select WBS1, WBS2, - sum (Amount) Amount
				 From LedgerAR
				 Where TransType = 'IN'
				   and RefNo <> 'Auto'
				   and IsNull (SubType, '') <> 'R'
				   and InvoiceSection not in ('I', 'T')  -- Interest & Tax
				 Group by WBS1, WBS2) i
		  Where b.WBS1 = @WBS1
			and b.WBS2 = @WBS2
			and b.WBS1 = i.WBS1
			and b.PostWBS2 = i.WBS2
          End -- Phase level

      If @WBS3 <> ' ' -- Task billing terms
	     Begin
      -- Insert new Billing Phase records (Fee);  FeeMeth = 3-Percent Complete by Phase as Fixed Amount
         Insert Into BTF (WBS1, WBS2, WBS3, Seq, Phase, Name, Fee, PostWBS1, PostWBS2, PostWBS3)
         Select p.WBS1, p.WBS2, p.WBS3, 
           (Select IsNull (Max (Seq), 0) From BTF Where WBS1 = p.WBS1) + row_number () over (order by p.WBS2) Seq, 
           (Select IsNull (Max (Seq), 0) From BTF Where WBS1 = p.WBS1) + row_number () over (order by p.WBS2) Phase, 
            p.Name, p.FeeDirLab + p.FeeDirExp + p.ConsultFee, 
            p.WBS1 PostWBS1, 
            p.WBS2 PostWBS2,
            p.WBS3 PostWBS3
         From PR p
         Where p.WBS1 = @WBS1
	       and p.WBS2 = @WBS2
           and p.WBS3 = @WBS3
           and not exists (Select * From BTF 
                           Where WBS1 = p.WBS1 and WBS2 = p.WBS2 and WBS3 = p.WBS3 and IsNull (PostWBS2, ' ') = p.WBS2 and IsNull (PostWBS3, ' ') = p.WBS3)

		  Update b
		  Set b.FeeToDate = i.Amount,
			  b.BilledJTD = i.Amount,
			  b.PctComplete = 
			  CASE When b.Fee = 0 Then 0
				   Else i.Amount / b.Fee * 100
			  END
		  From BTF b, 
				(Select WBS1, WBS2, WBS3, - sum (Amount) Amount
				 From LedgerAR
				 Where TransType = 'IN'
				   and RefNo <> 'Auto'
				   and IsNull (SubType, '') <> 'R'
				   and InvoiceSection not in ('I', 'T')  -- Interest & Tax
				 Group by WBS1, WBS2, WBS3) i
		  Where b.WBS1 = @WBS1
			and b.WBS2 = @WBS2
			and b.WBS3 = @WBS3
			and b.WBS1 = i.WBS1
			and b.PostWBS2 = i.WBS2
			and b.PostWBS3 = i.WBS3
          End -- Task level
      End -- Lump Sum

-- Fee Method 3
   If @BillingType = 'Lump Sum by Phase'
      Begin

   -- Set AP Default for Lump Sum Phase checkbox
      Update p
	  Set BillByDefaultConsultants = 'N',
          BillByDefaultOtherExp = 'N',
          BillByDefault = 'N'
      From PR p, ProjectCustomTabFields px
      Where p.WBS1 = @WBS1
        and p.WBS2 = @WBS2
        and p.WBS3 = @WBS3
        and p.WBS1 = px.WBS1
        and p.WBS2 = px.WBS2
        and p.WBS3 = px.WBS3

   -- Set AP Default for Lump Sum Phase checkbox at lowest level below level of Billing Type
      Update p
      Set BillByDefaultConsultants = 'N',
          BillByDefaultOtherExp = 'N',
          BillByDefault = 'N'
      From PR p, ProjectCustomTabFields px
      Where p.WBS1 = @WBS1
        and p.Sublevel = 'N'
        and p.WBS1 = px.WBS1
        and p.WBS2 = px.WBS2
        and px.WBS3 = ' '  -- phase level for checkbox
		and px.CustLumpSumPhase = 'Y'

	  Update ProjectCustomTabFields
	  Set CustFeeType = 'Lump Sum'
      Where WBS1 = @WBS1
		and CustLumpSumPhase = 'Y'

   -- Propagate Fee Type to lower levels
	  Update p3
	  Set p3.CustFeeType = 'Lump Sum'
	  From ProjectCustomTabFields p2, ProjectCustomTabFields p3
      Where p2.WBS1 = @WBS1
        and p2.WBS2 <> ' '
        and p2.WBS3 = ' ' -- phase level for checkbox
		and p2.CustLumpSumPhase = 'Y'
		and p2.WBS1 = p3.WBS1
		and p2.WBS2 = p3.WBS2
		and p3.WBS3 <> ' '  -- task level for sublevel

      Update BT
      Set FeeMeth = '3',  -- 2 = Percent Complete by Phase; 3 = Fixed Amount by Phase
	      PostFeesByPhase = 'Y'
      Where WBS1 = @WBS1
        and WBS2 = @WBS2
        and WBS3 = @WBS3

   -- Insert new Billing Phase records (Fee);  FeeMeth = 3-Percent Complete by Phase as Fixed Amount
      Insert Into BTF (WBS1, WBS2, WBS3, Seq, Phase, Name, Fee, PostWBS1, PostWBS2, PostWBS3)
      Select p.WBS1, @WBS2, ' ', 
        (Select IsNull (Max (Seq), 0) From BTF Where WBS1 = p.WBS1) + row_number () over (order by p.WBS2) Seq, 
        (Select IsNull (Max (Seq), 0) From BTF Where WBS1 = p.WBS1) + row_number () over (order by p.WBS2) Phase, 
         Left (p.WBS2 + ' - ' + p.Name, 40), p.FeeDirLab + p.FeeDirExp + p.ConsultFee, 
         p.WBS1 PostWBS1, 
         p.WBS2 PostWBS2,
         t.WBS3 PostWBS3
      From PR p, ProjectCustomTabFields px
	       Left Join
	       (Select WBS1, WBS2, Min (WBS3) WBS3 From PR Where WBS3 <> ' ' Group by WBS1, WBS2) t on t.WBS1 = px.WBS1 and t.WBS2 = px.WBS2
      Where p.WBS1 = @WBS1
        and p.WBS1 = px.WBS1
		and p.WBS2 = px.WBS2
		and p.WBS3 = px.WBS3
		and p.WBS2 <> ' '
		and p.WBS3 = ' ' -- Phase level
        and px.CustLumpSumPhase = 'Y'
        and not exists (Select * From BTF 
                        Where WBS1 = p.WBS1 and IsNull (PostWBS2, ' ') = p.WBS2 and IsNull (PostWBS3, ' ') = p.WBS3)

      Update b
      Set b.FeeToDate = i.Amount,
          b.BilledJTD = i.Amount,
          b.PctComplete = 
          CASE When b.Fee = 0 Then 0
		       Else i.Amount / b.Fee * 100
		  END
      From BTF b, 
            (Select WBS1, WBS2, - sum (Amount) Amount
             From LedgerAR
             Where TransType = 'IN'
               and RefNo <> 'Auto'
               and IsNull (SubType, '') <> 'R'
			   and InvoiceSection not in ('I', 'T')  -- Interest & Tax
             Group by WBS1, WBS2) i
      Where b.WBS1 = @WBS1
        and b.WBS2 = @WBS2
        and b.WBS3 = @WBS3
        and b.WBS1 = i.WBS1
        and b.PostWBS2 = i.WBS2
/*
   -- Category (when using Allocate Billed Revenue on Fee Invoices Based on Category
   -- Add  FeeByDetailEnabled = 'Y'
   -- Category
      Insert Into BTFCategory
      (WBS1, WBS2, WBS3, Seq, Category, Fee)
      Select p.WBS1, ' ', ' ', b.Phase, '1', p.FeeDirLab -- Labor
      From PR p, BTF b
      Where p.WBS1 = @WBS1
        and p.WBS2 <> ' '
        and p.WBS3 = ' '
        and p.WBS1 = b.WBS1
        and p.WBS2 = b.PostWBS2
        and p.WBS3 = IsNull (b.PostWBS3, ' ')
        and p.FeeDirLab <> 0
      UNION All
      Select p.WBS1, ' ', ' ', b.Phase, '2', p.ConsultFee -- Direct Consultant
      From PR p, BTF b
      Where p.WBS1 = @WBS1
        and p.WBS2 <> ' '
        and p.WBS3 = ' '
        and p.WBS1 = b.WBS1
        and p.WBS2 = b.PostWBS2
        and p.WBS3 = IsNull (b.PostWBS3, ' ')
        and p.ConsultFee <> 0
      UNION All
      Select p.WBS1, ' ', ' ', b.Phase, '3', p.FeeDirExp -- Direct Expense
      From PR p, BTF b
      Where p.WBS1 = @WBS1
        and p.WBS2 <> ' '
        and p.WBS3 = ' '
        and p.WBS1 = b.WBS1
        and p.WBS2 = b.PostWBS2
        and p.WBS3 = IsNull (b.PostWBS3, ' ')
        and p.FeeDirExp <> 0

   -- Category for Labor
      Update b
      Set b.FeeToDate = i.Amount,
          b.BilledJTD = i.Amount,
          b.PctComplete = 
	      CASE When b.Fee = 0 Then 0
		       Else i.Amount / b.Fee * 100
		  END
      From BTFCategory b, 
         (Select WBS1, WBS2, WBS3, - sum (Amount) Amount
          From LedgerAR
          Where TransType = 'IN'
            and RefNo <> 'Auto'
            and IsNull (SubType, '') <> 'R'
	        and Account = '4010'  -- Revenue Labor
          Group by WBS1, WBS2, WBS3) i
      Where b.WBS1 = @WBS1
        and b.WBS2 = @WBS2
        and b.WBS3 = @WBS3
        and b.WBS1 = i.WBS1
        and b.Seq  = i.WBS2
        and b.Category = '1'

   -- Category for Direct Consultant
      Update b
      Set b.FeeToDate = i.Amount,
          b.BilledJTD = i.Amount,
          b.PctComplete = 
	      CASE When b.Fee = 0 Then 0
		       Else i.Amount / b.Fee * 100
		  END
      From BTFCategory b, 
         (Select WBS1, WBS2, WBS3, - sum (Amount) Amount
          From LedgerAR
          Where TransType = 'IN'
            and RefNo <> 'Auto'
            and IsNull (SubType, '') <> 'R'
	        and Account = '4025'  -- Revenue Consultant
          Group by WBS1, WBS2, WBS3) i
      Where b.WBS1 = @WBS1
        and b.WBS2 = @WBS2
        and b.WBS3 = @WBS3
        and b.WBS1 = i.WBS1
        and b.Seq  = i.WBS2
        and b.Category = '2'

   -- Category for Direct Expense
      Update b
      Set b.FeeToDate = i.Amount,
          b.BilledJTD = i.Amount,
          b.PctComplete = 
	      CASE When b.Fee = 0 Then 0
		       Else i.Amount / b.Fee * 100
		  END
      From BTFCategory b, 
         (Select WBS1, WBS2, WBS3, - sum (Amount) Amount
          From LedgerAR
          Where TransType = 'IN'
            and RefNo <> 'Auto'
            and IsNull (SubType, '') <> 'R'
	        and Account = '4045'  -- Revenue Expense
          Group by WBS1, WBS2, WBS3) i
      Where b.WBS1 = @WBS1
        and b.WBS2 = @WBS2
        and b.WBS3 = @WBS3
        and b.WBS1 = i.WBS1
        and b.Seq  = i.WBS2
        and b.Category = '3'
*/     
      End -- Lump Sum by Phase

   If @BillingType = 'Lump Sum by Task'
      Begin
      Update BT
      Set FeeMeth = '3',  -- 2 = Percent Complete by Phase; 3 = Fixed Amount by Phase
	      PostFeesByPhase = 'Y', 
		  ConsolidatePrinting = 'N'
	  Where WBS1 = @WBS1
        and WBS2 = @WBS2
        and WBS3 = @WBS3

      Update ProjectCustomTabFields
      Set CustShowTaskBreakdown = 'Y'
	  Where WBS1 = @WBS1
        and WBS2 = @WBS2
        and WBS3 = @WBS3

   -- Insert new Billing Phase records (Fee);  FeeMeth = 3-Percent Complete by Phase as Fixed Amount
      Insert Into BTF (WBS1, WBS2, WBS3, Seq, Phase, Name, Fee, PostWBS1, PostWBS2, PostWBS3)
      Select p.WBS1, @WBS2, ' ', 
        (Select IsNull (Max (Seq), 0) From BTF Where WBS1 = p.WBS1) + row_number () over (order by p.WBS2) Seq, 
        (Select IsNull (Max (Seq), 0) From BTF Where WBS1 = p.WBS1) + row_number () over (order by p.WBS2) Phase, 
         Left (p.WBS3 + ' - ' + p.Name, 40), p.FeeDirLab + p.FeeDirExp + p.ConsultFee, 
         p.WBS1 PostWBS1, 
         p.WBS2 PostWBS2,
         p.WBS3 PostWBS3
		From PR p
		Where p.WBS1 = @WBS1
		  and p.WBS2 = @WBS2
		  and p.WBS3 <> ' ' -- Task level
		  and not exists (Select * From BTF 
			  			  Where WBS1 = p.WBS1 and IsNull (PostWBS2, ' ') = p.WBS2 and IsNull (PostWBS3, ' ') = p.WBS3)

      Update b
      Set b.FeeToDate = i.Amount,
          b.BilledJTD = i.Amount,
          b.PctComplete = 
          CASE When b.Fee = 0 Then 0
		       Else i.Amount / b.Fee * 100
		  END
      From BTF b, 
            (Select WBS1, WBS2, WBS3, - sum (Amount) Amount
             From LedgerAR
             Where TransType = 'IN'
               and RefNo <> 'Auto'
               and IsNull (SubType, '') <> 'R'
			   and InvoiceSection not in ('I', 'T')  -- Interest & Tax
             Group by WBS1, WBS2, WBS3) i
      Where b.WBS1 = @WBS1
        and b.WBS2 = @WBS2
        and b.WBS3 = @WBS3
        and b.WBS1 = i.WBS1
        and b.PostWBS2 = i.WBS2
        and IsNull (b.PostWBS3, ' ') = i.WBS3
      End -- Lump Sum by Task

   If @BillingType = 'Cost Plus Fixed Fee'
      Begin

      Update BT
      Set LimitMeth = '2', 
          AddOnLimit = px.CustCostPlusFixedFeeAmount,
		  AddOns = 'Y'
	  From BT b, ProjectCustomTabFields px
      Where b.WBS1 = @WBS1
        and b.WBS2 = @WBS2
        and b.WBS3 = @WBS3
        and b.WBS1 = px.WBS1
		and b.WBS2 = px.WBS2
		and b.WBS3 = px.WBS3

      Insert Into BTA
	  (WBS1, WBS2, WBS3, Sequence, Description, Markup, AddToLab, Account)
	  Select WBS1, WBS2, WBS3, 1, 'Fixed Fee Amount', px.CustCostPlusFixedFeePercent, 'Y', '401.00'
	  From ProjectCustomTabFields px
      Where px.WBS1 = @WBS1
        and px.WBS2 = @WBS2
        and px.WBS3 = @WBS3

	  Update ProjectCustomTabFields
	  Set CustFeeType = 'Cost Plus Fixed Fee'
      Where WBS1 = @WBS1
        and WBS2 = CASE When @WBS2 = ' ' Then WBS2 Else @WBS2 END
        and WBS3 = CASE When @WBS3 = ' ' Then WBS3 Else @WBS3 END

      End -- 'Cost Plus Fixed Fee

   If @BillingType = 'Percent of Construction'  -- Any level
      Begin

      Update PR
	  Set BillByDefaultConsultants = 'N',
          BillByDefaultOtherExp = 'N',
          BillByDefault = 'N'
      Where WBS1 = @WBS1
	    and WBS2 = @WBS2
		and WBS3 = @WBS3

   -- Set at lowest level below level of Billing Type
      Update PR 
      Set BillByDefaultConsultants = 'N',
          BillByDefaultOtherExp = 'N',
          BillByDefault = 'N'
      Where WBS1 = @WBS1
        and WBS2 = CASE When @WBS2 = ' ' Then WBS2 Else @WBS2 END
        and WBS3 = CASE When @WBS3 = ' ' Then WBS3 Else @WBS3 END
		and Sublevel = 'N'

	  Update ProjectCustomTabFields
	  Set CustFeeType = 'Percent of Construction'
      Where WBS1 = @WBS1
        and WBS2 = CASE When @WBS2 = ' ' Then WBS2 Else @WBS2 END
        and WBS3 = CASE When @WBS3 = ' ' Then WBS3 Else @WBS3 END

      Select @Fee = CustConstructionFeeCost
      From ProjectCustomTabFields
      Where WBS1 = @WBS1
        and WBS2 = ' '  -- Project level construction contract

      Select @FeePercent = custConstructionFeePercent
      From ProjectCustomTabFields
      Where WBS1 = @WBS1
        and WBS2 = @WBS2
        and WBS3 = @WBS3  -- Fee Pct comes from any level

      Update BT
      Set UnitDefault = 'Y',
	      PostFeesByPhase = 'N', FeeMeth = '1', FeeBasis = 'P', 
          FeeFactor1 = @Fee, -- Construction Cost using Fee variable
          FeeFactor2 = @FeePercent  -- Fee Percent using Pct Complete variable
      Where WBS1 = @WBS1
        and WBS2 = @WBS2
        and WBS3 = @WBS3
      End  -- Percent of Construction

	  -- Set the ProjectCustomTabFields Invoice Template to match the Billing Terms Template for this project
	  UPDATE x
	  SET x.CustInvoiceTemplate = b.Template
	  FROM  dbo.ProjectCustomTabFields x
	  INNER JOIN dbo.BT  b
		ON x.WBS1 = b.WBS1 AND b.WBS2 = ' '
	  WHERE x.WBS1 = @WBS1 
END
GO
