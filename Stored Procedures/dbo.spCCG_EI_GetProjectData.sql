SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_GetProjectData] (
	@sRole						varchar(32),
	@invGroupFilter				varchar(max),
	@invGroupFilter2			varchar(max),
	@ARMExists					int,			/* 0 or 1 */
	@docsFunction				int,			/* 0 or 1 */
	@sInvoiceGroup				varchar(255),
	@visionLanguage				varchar(10),
	@user						Nvarchar(32),
	@employee					Nvarchar(20),
	@inclCustomCols				int,			/* 0 or 1 */
	@hideDelegatedProjects		int = 0,		/* 0 or 1 */
	@quickLoad					int = 0			/* 0 or 1 */
)
AS
BEGIN
	/* Copyright (c) 2018 EleVia Software.  All rights reserved. */
	/*

	2018-08-17  SPS  Tweaks to allow biller to see project from a different invoice group IF they are a reviewer of that project (requires ProjectFilter too)
	2018-09-20  JAM  Switched to using new CCG_EI_Pending table

	exec spCCG_EI_GetProjectData @sRole = 'ACCT', @invGroupFilter = ' is not null ', @invGroupFilter2 = '',
		@ARMExists = '1', @docsFunction = '0', @sInvoiceGroup = '', @visionLanguage = 'en-US', @user = 'APPLE',
		@employee = '00001', @inclCustomCols = 1, @quickLoad = 0

	-- select * from SEUSER
	-- SELECT * from CCG_EI_SQLPartsCache
	-- delete from CCG_EI_SQLPartsCache
	select FilterBillGroup from CCG_EI_Config
	exec spCCG_EI_GetProjectData '',' is not null ','','1','0','','en-US','GRACEC','00003',1
	update CCG_EI_Config set AdvancedOptions='LinkCmd=iexplore.exe&LinkAutoClose=true&AcctStageChangeCheck=True'
	exec spCCG_EI_GetProjectData 'ACCT','=''Bob''','','1','0','Bob','en-US','APPLE','00001',1
	update CCG_EI_Config set AdvancedOptions='LinkCmd=iexplore.exe&LinkAutoClose=true&AcctStageChangeCheck=True&ShowROSubprjForAcct=Y'
	exec spCCG_EI_GetProjectData 'ACCT','=''Bob''','','1','0','Bob','en-US','APPLE','00001',1
	*/
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	declare @sSQL Nvarchar(max) = '',
			@prnamelen varchar(20),
			@docsEnabled varchar(1),
			@filterBillGrp varchar(1),
			@showROSubprjForAcct varchar(1) = '',
			@filterSQL varchar(1),
			@hideDormant varchar(1),
			@delegation varchar(1),
			@ApprovalRequired varchar(1),
			@SubgridMode varchar(50)
			--@multipleInvGroups char(1) = (case when charindex(',', @sInvoiceGroup) > -1 then 'Y' else 'N' end)

	declare @roleFieldsSql Nvarchar(max),
			@activeColumnsSql Nvarchar(max) = '',
			@custFieldsSelectSql Nvarchar(max) = '',
			@custFieldsGroupSql Nvarchar(max) = ''

	exec [spCCG_EI_GetSQLRoleFields] @sqlPart = @roleFieldsSql OUTPUT
	if @inclCustomCols = 1 exec [spCCG_EI_GetSQLCustomColumns] 0, 1, @sRole, 0, @quickLoad, @sqlPart = @custFieldsSelectSql OUTPUT, @outerApplies = @activeColumnsSql OUTPUT, @sqlGroupBy = @custFieldsGroupSql OUTPUT
	set @roleFieldsSql = replace(replace(@roleFieldsSql, N'[:Employee]', @employee), N'[:User]', @user)
	set @custFieldsSelectSql = replace(replace(@custFieldsSelectSql, N'[:Employee]', @employee), N'[:User]', @user)
	set @activeColumnsSql = replace(replace(@activeColumnsSql, N'[:Employee]', @employee), N'[:User]', @user)

	select @prnamelen = ISNULL(NULLIF(ProjectNameLength,0),30), @docsEnabled = ISNULL(DocsEnabled,'N'), @filterBillGrp = ISNULL(NULLIF(FilterBillGroup,''),'Y'), @filterSQL = ISNULL(FilterSQL,'N'),
			@hideDormant = ISNULL(HideDormant,'N'), @delegation = ISNULL(Delegation,'Y'), @ApprovalRequired = ISNULL(DelegationApproval,'Y'),
			@SubgridMode = ISNULL(SubgridMode, '')
		from CCG_EI_Config

	if @sRole = 'ACCT' or @sRole = 'ROACCT'
	begin
		if exists (select 'x' from CCG_EI_Config where CharIndex('ShowROSubprjForAcct=Y',AdvancedOptions,1) > 0)
		begin
			set @showROSubprjForAcct='Y'
			--print @showROSubprjForAcct
		end
		--set @sSQL = '
		--	select PR.WBS1, N'' '' as WBS2, N'' '' as WBS3, ''Projects'' as TLevel, PR.LongName, EM_PM.LastName+N'', ''+EM_PM.FirstName as ProjMgrName,
		--		''Accounting'' as Role,	(case when isnull(MR.CanModify,''N'')=''Y'' then ''N'' else ''Y'' end) as IsReadOnly,
		set @sSQL = N'
			select PR.WBS1, '' '' as WBS2, '' '' as WBS3, ''Projects'' as TLevel, PR.LongName, EM_PM.LastName+'', ''+EM_PM.FirstName as ProjMgrName,'
		if @filterBillGrp = 'Y' or @showROSubprjForAcct = 'Y'
			set @sSql = @sSQL + N'
				case when ''' + @showROSubprjForAcct + '''=''Y'' and BTBGSubs.MainWBS1 is not null and BTBGSubs.MainWBS1<>BTBGSubs.SubWBS1 then ''Y'' else ''N'' End as SubProject,'
		else
			set @sSql = @sSQL + N'''N'' as SubProject,'

		set @sSql = @sSQL + N'''Accounting'' as Role,'
		if @filterBillGrp = 'Y' or @showROSubprjForAcct = 'Y'
			set @sSql = @sSQL + N'case when ''' + @showROSubprjForAcct + '''=''Y'' and BTBGSubs.MainWBS1 is not null and BTBGSubs.MainWBS1<>BTBGSubs.SubWBS1 then ''Y'' '
		else
			set @sSql = @sSQL + N'case '
		set @sSql = @sSQL + N'
			when isnull(MR.CanModify,''N'')=''Y'' then ''N'' else ''Y'' end as IsReadOnly, '
		set @sSql = @sSQL + N'
			PR.BillingClientId, LEFT(PR.Name, '+@prnamelen+') as Name, PR.SubLevel as SubLevel, ' +
		-- stolz - This would show the multilingual stage flow name, but everything that uses it fails,
		-- so don't do this yet.  We'll likely need to add a hidden column that is the StageFlow field,
		-- then add StageFlowDescription (multilingual) that is the column that appears to the user.
		--		' case When IsNull(csfd.StageFlowDescription,'')='' Then PCTF." + invStageFlowFieldName + " else csfd.StageFlowDescription End as " + invStageFlowFieldName + ",CCG_EI.InvoiceStage as InvoiceStage,CCG_EI.InvoiceStage as InvoiceStageName, "
			N' PCTF.custInvoiceStageFlow, csfd.StageFlowDescription as StageFlowDescription, CCG_EI.InvoiceStage as InvoiceStage,
			CCG_EI.InvoiceStage as InvoiceStageName, ' -- including invoicestage twice, once to keep a hidden key value, the other for display
		-- stolz 2018-08-17 - Allow biller to see project from another biller if that first biller is a reviewer for a project in the other biller's group (requires a fnCCG_EI_ProjectFilter too)
		if @filterSQL = 'Y' and @sInvoiceGroup = Replace(Replace(@invGroupFilter,'=',''),'''','')
			set @sSql = @sSql + N'''' + @sInvoiceGroup + ''' as custInvoiceGroup, invGrp.DataValue as InvoiceGroupDisplay ' + @roleFieldsSql
		else
			set @sSql = @sSql + N' PCTF.custInvoiceGroup, invGrp.DataValue as InvoiceGroupDisplay ' + @roleFieldsSql

		-- 5.0 sps
		--declare @tplTable Nvarchar(1000) = (case when EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Projects_CustFinalInvPkgUserDocuments]')) then 'Projects_CustFinalInvPkgUserDocuments' else 'Projects_FinalInvPkgUserDocuments' end)
		--
		--set @sSQL = @sSQL + N', '''' as HistoryButton, (case 
		--	when isnull(CustFinalInvPkgIncludeExpReceipts, 0) > 0 or isnull(CustFinalInvPkgIncludeDirectExp, '''') = ''Y''
		--		or isnull(CustFinalInvPkgIncludeDirectSub, '''') = ''Y'' or isnull(CustFinalInvPkgIncludeExpenseReport, 0) > 0
		--		or isnull(CustFinalInvPkgIncludeSubInvoices, 0) > 0 or isnull(CustFinalInvPkgIncludeTimesheetData, 0) > 0 then ''Y'' 
		--	when exists (select 1 from '+@tplTable+' tt where tt.wbs1 = PR.wbs1 and tt.wbs2 = PR.wbs2 and tt.wbs3 = PR.wbs3) then ''Y'' 							
		--	else ''N'' end) as PackageButton  '		-- Not ready for this [JAM]

		set @sSQL = @sSQL + N', '''' as HistoryButton, ''Y'' as PackageButton  '

		if @ARMExists = 1
			set @sSQL = @sSQL + N', (select distinct ''x'' from CCG_ARM_ProcessInvoices pinv
					inner join CCG_ARM_Processes p on p.ProcessId = pinv.ProcessId
					inner join CCG_ARM_ConfigProcessStatus cps on cps.ProcessStatusId = p.ProcessStatusId
				where pinv.WBS1 = PR.WBS1 and cps.ProcessIsOpen = ''Y'') as ARMButton '

		if @docsEnabled = 'Y' or @docsEnabled = 'A'
		begin					-- DOCS54
			if @docsFunction = 1
				set @sSQL = @sSQL + N', docs.ColValue as DocsButton, docs.ColDetail as DocsButtonDetail '
			else
				set @sSQL = @sSQL + N', docs.totalCount as DocsButton, docs.daysOld as DocsButtonDetail '
		end

		if @SubgridMode = 'MessageSubGrid' set @sSql += ', case when exists(select 1 from CCG_EI_Messages M where M.WBS1 = PR.WBS1) then ''Y'' else ''N'' end as HasMessages '

		-- sSQL += ", '' as PctButton "
		set @sSQL = @sSQL + @custFieldsSelectSql

		set @sSQL = @sSQL + N'
			from PR
				inner join ProjectCustomTabFields PCTF on PCTF.WBS1 = PR.WBS1 and PCTF.WBS2 = N'' '' and PCTF.WBS3 = N'' ''
				outer apply (select top 1 * from FW_CustomColumnValuesData invGrp
					where invGrp.InfoCenterArea = ''Projects'' and invGrp.ColName = ''CustInvoiceGroup'' and invGrp.Code = PCTF.custInvoiceGroup
					order by CASE WHEN UICultureName = '''+@visionLanguage+''' THEN 1 ELSE 0 END desc
				) as invGrp 
				left join (select distinct StageFlow, StageFlowDescription from CCG_EI_ConfigStageFlowsDescriptions
					where UICultureName = ''' + @visionLanguage + '''
				) as csfd on csfd.StageFlow = PCTF.custInvoiceStageFlow
				left join CCG_EI_CustomColumns EI_Cust on EI_Cust.WBS1 = PR.WBS1 and EI_Cust.WBS2 = N'' '' '
		if @filterBillGrp = 'Y' or @showROSubprjForAcct = 'Y'
			set @sSQL = @sSQL + N'
				left join BTBGSubs on PR.WBS1 = BTBGSubs.SubWBS1 '
		if @filterSQL = 'Y'
			set @sSQL = @sSQL + N'
				inner join (
					select distinct wbs1 from dbo.fnCCG_EI_ProjectFilter('''+ISNULL(@sInvoiceGroup,'')+''', N'''+@user+''')
				) ProjectFilter on ProjectFilter.wbs1 = PR.wbs1'

		set @sSQL = @sSQL + N'
				left join CCG_EI on CCG_EI.wbs1 = PR.wbs1
				left join (select max(CanModify) as CanModify, StageFlow, Stage from CCG_EI_ConfigRights group by StageFlow, Stage) MR
					on MR.StageFlow = PCTF.custInvoiceStageFlow and MR.Stage = CCG_EI.InvoiceStage
				left join EM EM_PM on EM_PM.Employee = PR.ProjMgr '

		if @docsEnabled = 'Y' or @docsEnabled = 'A'
		begin				-- DOCS54
			if @docsFunction = 1
				set @sSQL = @sSQL + N'
				outer apply dbo.fnCCG_EI_DocButtonColumn(PR.WBS1, N''' + @user + ''') as docs '
			else
				set @sSQL = @sSQL + N'
				left join (select WBS1, count(*) as totalCount, max(createdate) as lastDate,
					convert(int, GETUTCDATE() - max(createdate)) as daysOld
					from CCG_DOCS_PR where  Category = ''ADD_DOCS'' group by WBS1) docs on PR.WBS1 = docs.WBS1 '
		end

		set @sSQL = @sSQL + @activeColumnsSql
		set @sSQL = @sSQL + N'
				where PR.WBS2 = N'' '' and PR.WBS3 = N'' '' '

		if @filterBillGrp = 'Y' and @showROSubprjForAcct <> 'Y'
			set @sSQL = @sSQL + N'
					and (BTBGSubs.MainWBS1 is null or BTBGSubs.MainWBS1 = BTBGSubs.SubWBS1) '
		if @filterSQL = 'N'  -- Don't do any unnecessary filtering when filterSQL enabled, so the client has control
			set @sSQL = @sSQL + N'
					and PR.Status <> ''D'' and PR.ChargeType = ''R'' '

		-- stolz 2018-08-17 - If we're loading only a single invoice group AND they have a filter, then no need to restrict here as the ProjectFilter
		--						has already done it (plus we don't want to, because the filter MIGHT bring back a project from another invoice group
		if @filterSQL = 'N' or @sInvoiceGroup <> Replace(Replace(@invGroupFilter,'=',''),'''','')
			set @sSQL = @sSQL + N'
					and PCTF.custInvoiceGroup ' + @invGroupFilter

		if @invGroupFilter2 <> ''
			set @sSQL = @sSQL + N'
					and PCTF.custInvoiceGroup ' + @invGroupFilter2

		set @sSQL = @sSQL + N'
					and not PCTF.custInvoiceStageFlow IS NULL '	-- required fields for EI
		set @sSQL = @sSQL + N'
				order by PR.WBS1'
	end
	else
	begin
		set @sSQL = N'
			select distinct PR.WBS1, N'' '' as WBS2, N'' '' as WBS3, ''Projects'' as TLevel, PR.LongName, EM_PM.LastName+N'', ''+EM_PM.FirstName as ProjMgrName,
				isnull(max(case when isnull(p.CanModify,''Y'')=''Y'' then p.Role else null end), max(case when isnull(p.CanModify,''Y'')<>''Y'' then p.Role else '''' end)) as Role, 
				min(case when isnull(p.CanModify,''Y'')=''Y'' and p.NextInvoiceStage is null then ''N'' else ''Y'' end) as IsReadOnly,
				PR.BillingClientId, LEFT(PR.Name, '+@prnamelen+') as Name, PR.SubLevel as SubLevel,
				PCTF.custInvoiceStageFlow, isnull(max(p.NextInvoiceStage), CCG_EI.InvoiceStage) as InvoiceStage,
				isnull(max(p.NextInvoiceStage), CCG_EI.InvoiceStage) as InvoiceStageName, PCTF.custInvoiceGroup ' + @roleFieldsSql

		-- 5.0 sps
		set @sSQL = @sSQL + N', '''' as HistoryButton, '''' as PackageButton '

		if @ARMExists = 1
			set @sSQL = @sSQL + N', (select distinct ''x'' from CCG_ARM_ProcessInvoices pinv
					inner join CCG_ARM_Processes p on p.ProcessId = pinv.ProcessId
					inner join CCG_ARM_ConfigProcessStatus cps on cps.ProcessStatusId = p.ProcessStatusId
				where pinv.WBS1 = PR.WBS1 and cps.ProcessIsOpen = ''Y'') as ARMButton '

		if @docsEnabled = 'Y'
		begin
			-- DOCS54  TODO config if docs
			if @docsFunction = 1
				set @sSQL = @sSQL + N', docs.ColValue as DocsButton, docs.ColDetail as DocsButtonDetail '
			else
				set @sSQL = @sSQL + N', docs.totalCount as DocsButton, docs.daysOld as DocsButtonDetail '
		end

		if @SubgridMode = 'MessageSubGrid' set @sSql += ', case when exists(select 1 from CCG_EI_Messages M where M.WBS1 = PR.WBS1) then ''Y'' else ''N'' end as HasMessages '

		--sSQL += ", '' as PctButton "
		set @sSQL = @sSQL + @custFieldsSelectSql

		set @sSQL = @sSQL + N'
				from CCG_EI  ' -- We can start with CCG_EI for non-accounting only since we know there will be a record after setting the first stage
		set @sSQL = @sSQL + N'
					inner join PR on PR.WBS1 = CCG_EI.WBS1 and PR.WBS2 = N'' '' and PR.WBS3 = N'' ''
					inner join ProjectCustomTabFields PCTF on PCTF.WBS1 = PR.WBS1 and PCTF.WBS2 = N'' '' and PCTF.WBS3 = N'' ''
					inner join CCG_EI_Pending p on p.WBS1 = CCG_EI.WBS1 and isnull(p.Parallel,'''') <> ''C''
					left join CCG_EI_CustomColumns EI_Cust on EI_Cust.WBS1 = PR.WBS1 and EI_Cust.WBS2 = '' ''
					left join EM EM_PM on EM_PM.Employee = PR.ProjMgr

					' + (case when @delegation = 'Y' then N'
					left join CCG_EI_Delegation  d1 on  d1.Delegate = N'''+@Employee+''' AND (isnull( d1.ApprovedBy,N'''') <> N'''' or '''+@ApprovalRequired+''' = ''N'') AND GETDATE() between  d1.FromDate and dateadd(d, 1,  d1.ToDate) AND isnull( d1.ForWBS1,N'''') = N''''    and isnull( d1.ForClientId,'''') = ''''
					left join CCG_EI_Delegation  d2 on  d2.Delegate = N'''+@Employee+''' AND (isnull( d2.ApprovedBy,N'''') <> N'''' or '''+@ApprovalRequired+''' = ''N'') AND GETDATE() between  d2.FromDate and dateadd(d, 1,  d2.ToDate) AND isnull( d2.ForWBS1,N'''') = N''''    and isnull( d2.ForClientId,'''') = PR.BillingClientID
					left join CCG_EI_Delegation  d3 on  d3.Delegate = N'''+@Employee+''' AND (isnull( d3.ApprovedBy,N'''') <> N'''' or '''+@ApprovalRequired+''' = ''N'') AND GETDATE() between  d3.FromDate and dateadd(d, 1,  d3.ToDate) AND isnull( d3.ForWBS1,N'''') = PR.WBS1		and isnull( d3.ForClientId,'''') = ''''
					left join CCG_EI_Delegation dt1 on dt1.Employee = N'''+@Employee+''' AND (isnull(dt1.ApprovedBy,N'''') <> N'''' or '''+@ApprovalRequired+''' = ''N'') AND GETDATE() between dt1.FromDate and dateadd(d, 1, dt1.ToDate) AND isnull(dt1.ForWBS1,N'''') = N''''    and isnull(dt1.ForClientId,'''') = ''''
					left join CCG_EI_Delegation dt2 on dt2.Employee = N'''+@Employee+''' AND (isnull(dt2.ApprovedBy,N'''') <> N'''' or '''+@ApprovalRequired+''' = ''N'') AND GETDATE() between dt2.FromDate and dateadd(d, 1, dt2.ToDate) AND isnull(dt2.ForWBS1,N'''') = N''''    and isnull(dt2.ForClientId,'''') = PR.BillingClientID
					left join CCG_EI_Delegation dt3 on dt3.Employee = N'''+@Employee+''' AND (isnull(dt3.ApprovedBy,N'''') <> N'''' or '''+@ApprovalRequired+''' = ''N'') AND GETDATE() between dt3.FromDate and dateadd(d, 1, dt3.ToDate) AND isnull(dt3.ForWBS1,N'''') = PR.WBS1		and isnull(dt3.ForClientId,'''') = '''' '
					else N'' end)

		if @filterBillGrp = 'Y'
			set @sSQL = @sSQL + N'
					left join BTBGSubs on PR.WBS1 = BTBGSubs.SubWBS1 '
		if @docsEnabled = 'Y'
		begin					-- DOCS54
			if @docsFunction = 1
				set @sSQL = @sSQL + N'
					outer apply dbo.fnCCG_EI_DocButtonColumn(PR.WBS1, N''' + @user + ''') as docs '
 			else
				set @sSQL = @sSQL + N'
					left join (select WBS1, count(*) as totalCount, max(createdate) as lastDate,
						convert(int, GETUTCDATE() - max(createdate)) as daysOld
						from CCG_DOCS_PR where  Category = ''ADD_DOCS'' group by WBS1) docs on PR.WBS1 = docs.WBS1 '
		end

		set @sSQL = @sSQL + @activeColumnsSql
		set @sSQL = @sSQL + N'
				where PCTF.CustInvoiceGroup IS NOT NULL
					and PCTF.CustInvoiceStageFlow IS NOT NULL
					and PR.WBS2 = '' ''	'

		if @delegation = 'N'
			set @sSQL = @sSQL + N'
					and p.Employee = N'''+@employee+''''
		else 
		begin
			if @hideDelegatedProjects = 0
				set @sSQL = @sSQL + N'
						and (
							   (p.CanModify = ''Y'' and (
									   (p.Employee = d3.Employee AND d3.Employee is not null)
									OR (p.Employee = d2.Employee AND ISNULL(d3.Employee,'''')='''' AND d2.Employee is not null)
									OR (p.Employee = d1.Employee AND ISNULL(d3.Employee,'''')='''' AND ISNULL(d2.Employee,'''')='''' AND d1.Employee is not null)
							))
							OR (p.Employee = N'''+@employee+''' AND
									(ISNULL(dt3.Delegate,'''')=''''	OR dt3.Dual=''Y'')
								AND (ISNULL(dt2.Delegate,'''')='''' OR dt2.Dual=''Y'')
								AND (ISNULL(dt1.Delegate,'''')='''' OR dt1.Dual=''Y''))
						) '
			else
				set @sSQL = @sSQL + N'
						and (p.Employee = N'''+@employee+''' AND ISNULL(dt3.Delegate,'''')='''' AND ISNULL(dt2.Delegate,'''')='''' AND ISNULL(dt1.Delegate,'''')='''') '
		end

		if @filterBillGrp = 'Y'
			set @sSQL = @sSQL + N'
					and (BTBGSubs.MainWBS1 is null or BTBGSubs.MainWBS1 = BTBGSubs.SubWBS1) '

		/*
		For non-acct just let them see all that are assigned to them (EI 4.2)
		if (!filterSQL)	//don't do any unnecessary filtering when filterSQL enabled, so the client has control
		    sSQL += " and PR.Status<>'D' and PR.ChargeType='R' "
		*/

		if @hideDormant = 'Y'
			set @sSQL = @sSQL + N'
					and PR.Status <> ''D'' '

		set @sSQL = @sSQL + N'
				group by
					PR.WBS1, PR.LongName, EM_PM.LastName, EM_PM.FirstName, 
					PR.BillingClientId, PR.Name, PR.SubLevel, PCTF.custInvoiceStageFlow, CCG_EI.InvoiceStage,
					CCG_EI.InvoiceStage, PCTF.custInvoiceGroup ' + @roleFieldsSql + @custFieldsGroupSql +
				(case when @docsEnabled = 'Y' then
					(case when @docsFunction = 1 then N', docs.ColValue, docs.ColDetail '
					else N', docs.totalCount, docs.daysOld ' end) else ''
				end) + N'
				order by PR.WBS1 '
	end

	--Print @sSQL
	exec(@sSQL)
END
GO
