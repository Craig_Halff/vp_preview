SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectBillingTermsUpdateCM1] (
   @WBS1         varchar (32), 
   @WBS2         varchar (32),
   @WBS3         varchar (32),
   @ContractNo   varchar (255))
AS
-- ####################   Contract Management   #####################
/*
Copyright (c) 2017 Central Consulting Group. All rights reserved.
07/12/2017 David Springer
           Update Billing Terms fee.
           Call from Contract CHANGE workflow when Include in Fee is checked.
		   Call from Contract Details INSERT or CHANGE, but check that Contract Include in Fee is checked.
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
DECLARE @BillingType  varchar (255)
BEGIN
-- Project Level 
If @WBS2 = ' '
	Begin
	DECLARE curContract insensitive cursor for
			Select WBS2, WBS3
			From ContractDetails
			Where WBS1 = @WBS1
			  and ContractNumber = @ContractNo
	Open curContract
	Fetch Next from curContract into @WBS2, @WBS3
		While @@Fetch_Status = 0
		Begin

		Select @BillingType = CustBillingType From ProjectCustomTabFields Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
		exec spCCG_ProjectBillingTermsUpdateCM2 @WBS1, @WBS2, @WBS3, @BillingType

		Fetch Next from curContract into @WBS2, @WBS3

		End
	Close curContract
	Deallocate curContract

	End -- project level
Else
	Select @BillingType = CustBillingType From ProjectCustomTabFields Where WBS1 = @WBS1 and WBS2 = @WBS2 and WBS3 = @WBS3
	exec spCCG_ProjectBillingTermsUpdateCM2 @WBS1, @WBS2, @WBS3, @BillingType
END
GO
