SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Save_Email_Config]
	@EmailConfigUpdateSql	Nvarchar(max),
	@EIConfigUpdateSql		Nvarchar(max)
AS
BEGIN
-- MUST INSTALL RegexMatch FIRST FOR THIS TO WORK

	/*
	DeltekStoredProc_CCG_EI_Config_Save_Email_Config
		@EmailConfigUpdateSql = 'SMTPServer = ''smtp.mail.visi.com'', SMTPPort = 25, Username = ''steve.stolz@visi.com'', Password = ''fusion911'', Interval =          1',
		@EIConfigUpdateSql = 'EmailLink = ''<BR><BR>Click <a href="http://ccg7038/VisionClient/CCG/ElectronicInvoicing.application?VisionRev=7.2&ShowRoles=Y&Database=CCGDemo&DbServer=ccg7038&[:USERENC]&[:EDENC]">here</a> to enter Electronic Invoicing'', EncTimeout =          8, EnforceWindowsUsernameInLink = ''N'''
	*/

	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	int;

	-- Ensure that our inputs are safe (preventing SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<SET LIST>', @EmailConfigUpdateSql);
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<SET LIST>', @EIConfigUpdateSql) * 2;
	IF @safeSql < 3 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS varchar(4));
		RETURN;
	END;

	BEGIN TRANSACTION;
	SET @sSQL = N'
		UPDATE CCG_Email_Config SET ' + @EmailConfigUpdateSql + N';';
	EXEC (@sSQL);

	SET @sSQL = N'
		UPDATE CCG_EI_Config SET ' + @EIConfigUpdateSql + N';';
	EXEC (@sSQL);
	COMMIT TRANSACTION;
END;
GO
