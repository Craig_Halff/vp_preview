SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPReCalcPlan]
  @strRowID nvarchar(255),
  @bitReorderOutlineNumber bit = 0,
  @bitCalledFromRM bit = 0,
  @bitCalledFromRevenueForecast bit = 0
AS

BEGIN -- Procedure stRPReCalcPlan

  SET NOCOUNT ON

  DECLARE @strPlanID varchar(32) = ''
  DECLARE @strTaskID varchar(32) = ''
  DECLARE @strInputType varchar(1) = ''

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. E~<PNAssignment.ResourceID>|<PNTask.TaskID>
  --   2. G~<PNAssignment.GenericResourceID>|<PNTask.TaskID>
  --   3. P~<PNPlan.PlanID>
  
  IF (CHARINDEX('~', @strRowID) = 0)
    BEGIN
      SET @strInputType = 'T'
    END
  ELSE
    BEGIN
      SET @strInputType = LEFT(@strRowID, CHARINDEX('~', @strRowID) - 1)
    END

  IF (@strInputType IN ('E', 'G', 'T'))
    BEGIN

      SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

      -- Get various Plan parameters.

      SELECT 
        @strPlanID = T.PlanID
        FROM PNTask AS T
          INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID
        WHERE T.TaskID = @strTaskID

    END
  ELSE IF (@strInputType IN ('P'))
    BEGIN

      SET @strPlanID = REPLACE(@strRowID, 'P~', '')

    END /* END ELSE IF (@strInputType IN ('P')) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Extend date span of WBS rows, if needed.
  IF (@bitCalledFromRevenueForecast = 1)
      BEGIN
			EXECUTE dbo.stRPExtendDateSpan_RevForecast @strPlanID, @bitCalledFromRM
	  END
  ELSE
      BEGIN
			EXECUTE dbo.stRPExtendDateSpan @strPlanID, @bitCalledFromRM
	  END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
 
  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- If this SP was called from Resource Management then need to update RP tables.

    IF (@bitCalledFromRM = 1)
      BEGIN

        UPDATE T SET
          ChildrenCount = (SELECT COUNT(*) FROM RPTask WHERE PlanID = @strPlanID AND ParentOutlineNumber = T.OutlineNUmber)
          FROM RPTask AS T 
          WHERE T.PlanID = @strPlanID 
            AND T.ChildrenCount <> (SELECT COUNT(*) FROM RPTask WHERE PlanID = @strPlanID AND ParentOutlineNumber = T.OutlineNUmber)

      END /* END IF (@bitCalledFromRM = 1) */

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  COMMIT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Check to see if there is a need to reorder OutlineNumber.

  IF (@bitReorderOutlineNumber = 1)
    BEGIN
      EXECUTE dbo.stRPReorderOutlineNumber @strPlanID, @bitCalledFromRM
    END


	 -- Update ChildrenCount for the whole plan, if needed.

    UPDATE T SET
      ChildrenCount = (SELECT COUNT(*) FROM PNTask WHERE PlanID = @strPlanID AND ParentOutlineNumber = T.OutlineNUmber)
      FROM PNTask AS T 
      WHERE T.PlanID = @strPlanID 
        AND T.ChildrenCount <> (SELECT COUNT(*) FROM PNTask WHERE PlanID = @strPlanID AND ParentOutlineNumber = T.OutlineNUmber)
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF
     
END -- stRPReCalcPlan
GO
