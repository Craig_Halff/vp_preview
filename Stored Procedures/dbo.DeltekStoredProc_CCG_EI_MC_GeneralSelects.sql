SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_MC_GeneralSelects] (
	@type		varchar(50),
	@param1		varchar(max) = ''
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @type = 'GetLanguageLabels'
		SELECT Group1, ID, Product, UICultureName, Label FROM CCG_Language_Labels
			WHERE Product IN ('','EI','MD') and (UICultureName = @param1 OR ISNULL(@param1,'') = '')
	ELSE IF @type = 'GetLanguageLabels_General'
		SELECT Id, MAX(Label) as Label, CASE WHEN UICultureName = @param1 THEN 1 ELSE 0 END as Specificity
	        FROM CCG_Language_Labels
	        WHERE Product IN ('','EI','MD') and UICultureName like LEFT(@param1, 2)+'%'
	        GROUP BY Id, (CASE WHEN UICultureName = @param1 THEN 1 ELSE 0 END)
	        ORDER BY Id, Specificity Desc
END

GO
