SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_ConfigSaveRoles] ( @fieldsSQL varchar(max), @newValuesSql varchar(max), @deleteRolesSql varchar(max), @visionLanguage varchar(10))
             AS EXEC spCCG_EI_ConfigSaveRoles @fieldsSQL,@newValuesSql,@deleteRolesSql,@visionLanguage
GO
