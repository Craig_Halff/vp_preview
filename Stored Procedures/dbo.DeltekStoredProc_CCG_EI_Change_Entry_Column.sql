SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Change_Entry_Column]
	@KEY_EMPLOYEEID			Nvarchar(32),
	@isInputColumn			bit,
	@wbs1					Nvarchar(30),
	@wbs2					Nvarchar(30),
	@wbs3					Nvarchar(30),
	@databaseField			varchar(500),
	@oldValue				Nvarchar(max),
	@newValue				Nvarchar(max),
	@databaseModEmpField	Nvarchar(500),
	@columnDataType			varchar(100),
	@customColumnId			varchar(100),
	@customColumnLabel		Nvarchar(100),
	@isDecimalValue			bit
AS
BEGIN
	/*
	DEBUG TEST:
	DeltekStoredProc_CCG_EI_Change_Entry_Column @KEY_EMPLOYEEID = '00001',
		@isInputColumn = '1',
		@wbs1 = '2002010.00',
		@wbs2 = ' ',
		@wbs3 = ' ',
		@databaseField = 'HoldUntilDate',
		@oldValue = '''12/31/2015 12:00:00 AM''',
		@newValue = '''12/16/2015 12:00:00 AM''',
		@databaseModEmpField = 'HoldUntilDate_ModEmp',
		@columnDataType = 'D',
		@customColumnId = '404',
		@customColumnLabel = 'Hold Until Date',
		@isDecimalValue = '0'
	*/
	SET NOCOUNT ON;

	DECLARE @sSQL			Nvarchar(max);
	DECLARE @whereClause	Nvarchar(max);
	DECLARE @safeSql		int;

	-- Ensure that our inputs are safe (prevent SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @wbs1);
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @wbs2) * 2;
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @wbs3) * 4;
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<VALUE>', @oldValue) * 8;
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<VALUE>', @newValue) * 16;
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @KEY_EMPLOYEEID) * 32;
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @columnDataType) * 64;
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<NUMBER>', @customColumnId) * 128;
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @customColumnLabel) * 256;
	IF @safeSql < 511 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS varchar(4));
		RETURN;
	END;

	IF @isInputColumn = 1 BEGIN
		-- Write through not handled here, but using the check stored procedure during validation
        SET @whereClause = N' WHERE WBS1 = ''' + @wbs1 + N''' and WBS2 = ''' + @wbs2 + N''' and WBS3 = ''' + @wbs3 + N''' ';

		SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<FIELD>', @databaseField);
		SET @safeSql = @safeSql & dbo.fnCCG_EI_RegexMatch('<FIELD>', @databaseModEmpField);
		IF @safeSql = 0 BEGIN
			SELECT 'ERROR: Unsafe parameters used! Execution cancelled.';
			RETURN;
		END;

		IF EXISTS (SELECT 'x' FROM CCG_EI_CustomColumns WHERE WBS1 = @wbs1 and WBS2 = @wbs2 and WBS3 = @wbs3)
			SET @sSQL = N'
				UPDATE CCG_EI_CustomColumns SET ' + @databaseField + N' = ' + @newValue + N',
				' + @databaseModEmpField + N' = ''' + @KEY_EMPLOYEEID + N'''' + @whereClause;
        ELSE
			SET @sSQL = N'
				INSERT INTO CCG_EI_CustomColumns
					(WBS1, WBS2, WBS3, ' + @databaseField + N', ' + @databaseModEmpField + N')
					VALUES (''' + @wbs1 + N''', ''' + @wbs2 + N''', ''' + @wbs3 + N''', ' + @newValue + N',
					''' + @KEY_EMPLOYEEID + N''')';

		--PRINT @sSQL;
		EXEC (@sSQL);
	END;

    -- Log in history table
    SET @sSQL = N'
		INSERT INTO CCG_EI_HistoryUpdateColumns
			(ActionTaken, ActionDate, ActionTakenBy,
				WBS1, WBS2, WBS3, DataType, ConfigCustomColumn, ColumnLabel,
				OldValue, NewValue,
				OldValueDecimal, NewValueDecimal)
			VALUES (''Change'', getutcdate(), ''' + @KEY_EMPLOYEEID + N''',
				''' + @wbs1 + N''', ''' + @wbs2 + N''', ''' + @wbs3 + N''',
				''' + @columnDataType + N''', ' + @customColumnId + N', ''' + @customColumnLabel + N''',
				' + @oldValue + N', ' + @newValue + N',
				' + CASE
						WHEN @isDecimalValue = 1 THEN @oldValue
						ELSE N'NULL'
					END + N', ' +
					CASE
						WHEN @isDecimalValue = 1 THEN @newValue
						ELSE N'NULL'
					END + N')';

	--PRINT @sSQL;
	EXEC (@sSQL);
END;
GO
