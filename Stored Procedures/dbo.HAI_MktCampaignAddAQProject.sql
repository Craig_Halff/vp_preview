SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[HAI_MktCampaignAddAQProject] @WBS1 VARCHAR(12)
AS
    /* 
		Copyright (c) 2021 Halff Associates, Inc.  All rights reserved.
		9/7/2021	Craig H. Anderson

		Add Project to Mkt Campaigns if they match
	*/

    BEGIN
        INSERT INTO dbo.MktCampaigns_CustAssociatedProjects (
            CampaignID
          , Seq
          , CreateUser
          , CreateDate
          , ModUser
          , ModDate
          , CustAssociatedProjectCode
          , CustAssociatedProject
          , CustAQCategory
          , CustAssociatedProjectTotalFee
        )
        --DECLARE @WBS1 VARCHAR (12)= '045560.001'
        SELECT '' -- CampaignID - varchar(32)
             , REPLACE(NEWID(), '-', '') -- Seq - varchar(32)
             , 'Automatic' -- CreateUser - varchar(32)
             , GETDATE() -- CreateDate - datetime
             , 'Automatic' -- ModUser - varchar(32)
             , GETDATE() -- ModDate - datetime
             , @WBS1 -- CustAssociatedProjectCode - nvarchar(255)
             , p.Name -- CustAssociatedProject - nvarchar(30)
             , NULL -- CustAQCategory - nvarchar(255)
             , px.CustTotalCompensation -- CustAssociatedProjectTotalFee - decimal(19, 5)
        FROM dbo.MktCampaign                          AS mc
            INNER JOIN dbo.MktCampaignCustomTabFields AS mcx
                ON mc.CampaignID = mcx.CampaignID
            INNER JOIN dbo.PR                         p
                ON p.WBS1 = @WBS1
                    AND p.WBS2 = ' '
                    AND mcx.CustCampaignClient = p.ClientID
            INNER JOIN dbo.ProjectCustomTabFields     AS px
                ON p.WBS1 = px.WBS1
                    AND p.WBS2 = px.WBS2
                    AND p.WBS3 = px.WBS3
        WHERE
            mc.RecordStatus = 'A'
            AND NOT EXISTS (
            SELECT 1
            FROM dbo.MktCampaigns_CustAssociatedProjects m1
            WHERE
                m1.CampaignID = mc.CampaignID
                AND m1.CustAssociatedProjectCode = @WBS1
        );

    END;
GO
