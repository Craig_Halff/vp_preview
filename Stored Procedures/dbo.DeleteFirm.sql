SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteFirm] @clientID varchar(32)
AS
    DELETE FROM ARC WHERE ClientID = @clientID
    DELETE FROM CLAddress WHERE ClientID = @clientID
    DELETE FROM ClientCustomTabFields WHERE ClientID = @clientID
    DELETE FROM ClientFileLinks WHERE ClientID = @clientID
    DELETE FROM ClientToClientAssoc WHERE FromClientID = @clientID
    DELETE FROM ClientToClientAssoc WHERE ToClientID = @clientID
    DELETE FROM CLSubscr WHERE ClientID = @clientID
    DELETE FROM CustomProposalClient WHERE ClientID = @clientID
    DELETE FROM CustomProposalClientGraphics WHERE ClientID = @clientID
    DELETE FROM EMClientAssoc WHERE ClientID = @clientID
    --DELETE FROM OpportunityClientAssoc WHERE ClientID = @clientID
    DELETE FROM ClendorProjectAssoc WHERE ClientID = @clientID
    DELETE FROM ClendorProjectAssocTemplate WHERE ClientID = @clientID
    DELETE FROM MktCampaignClientAssoc WHERE ClientID = @clientID
    DELETE FROM CLActivity WHERE ClientID = @clientID
    DELETE FROM ClientFromContacts WHERE ClientID = @clientID

    EXEC DeleteCustomGridTabData 'Firms',@clientID, '', ''

    UPDATE Activity SET ClientID = null WHERE ClientID = @clientID
    UPDATE WorkflowActivity SET ClientID = null WHERE ClientID = @clientID
    UPDATE Contacts SET ClientID = null,CLAddress = null WHERE ClientID = @clientID
    UPDATE CustomProposal SET ClientID = null WHERE ClientID = @clientID
    UPDATE CustomProposalContact SET ClientID = null WHERE ClientID = @clientID
    --UPDATE Opportunity SET ClientID = null WHERE ClientID = @clientID
    UPDATE PR SET BillingClientID = null,CLBillingAddr = null WHERE BillingClientID = @clientID
    UPDATE PR SET ClientID = null,CLAddress = null WHERE ClientID = @clientID
    UPDATE PRTemplate SET BillingClientID = null,CLBillingAddr = null WHERE BillingClientID = @clientID
    UPDATE PRTemplate SET ClientID = null,CLAddress = null WHERE ClientID = @clientID
    UPDATE RPPlan SET ClientID = null WHERE ClientID = @clientID
    UPDATE PNPlan SET ClientID = null WHERE ClientID = @clientID
    UPDATE RPTask SET ClientID = null WHERE ClientID = @clientID
    UPDATE PNTask SET ClientID = null WHERE ClientID = @clientID

GO
