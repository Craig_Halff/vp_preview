SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_GetUsers]
AS
BEGIN
	/* Copyright (c) 2020 EleVia Software.  All rights reserved. */
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	declare @sql varchar(max)

	IF not exists (select 'x' from syscolumns a,sysobjects b where a.id = b.id and a.name = 'DisplayUsername' and b.name = 'SEUser')
	BEGIN
		-- Can we get EI users only?
		select u.Username, u.Username as DisplayUsername
			from SEUser u 
				left join EM on EM.Employee = u.Employee
			where EM.Status is null or EM.Status <> 'T'
			order by 1
	END
	ELSE
	BEGIN
		set @sql = '
			select u.Username, u.DisplayUsername
			from SEUser u 
				left join EM on EM.Employee = u.Employee
			where EM.Status is null or EM.Status <> ''T''
			order by 1'
		exec(@sql)
	END
END
GO
