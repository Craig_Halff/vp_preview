SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE    Procedure [dbo].[spCCG_ProjectFeeAdjustment] @Period varchar (6)
AS
/*
Copyright (c) 2017 Central Consulting Group. All rights reserved.
09/08/2017 David Springer
           Reconcile Cost Plus project fees with invoiced amounts.
           Call from Project scheduled workflow.
		   Pass period as:  (Select Period From CFGDates Where convert (date, getDate()) between AccountPdStart and AccountPdEnd)
10/18/2019 David Springer
           Added contract auto numbering
06/25/2020 David Springer
           Set Approved Date = today and Period = period of the invoice
07/31/2020 Craig H. Anderson
			Changed SalesDate from d.AccountPdEnd to getdate() so that sales date reflects the date that the process was run, just like the contract Approved date.
08/20/2020 Craig H. Anderson
			Sales now getting total for new Sales entry directly from the latest contract entry.
*/
DECLARE @Date datetime = getDate()
BEGIN
SET NOCOUNT ON

--	Update Contract Mgmt
--	Create one contract per project
	Insert Into Contracts
	(ContractNumber, ContractDescription, WBS1, RequestDate, ApprovedDate, ContractStatus, ContractType, FeeIncludeInd, Period)
	Select distinct c.ContractNo, c.ContractNo, pt.WBS1, getdate() RequestDate , getdate() ApprovedDate, '04' Status, '05' Type, 'Y', d.Period
	From PR pt,
		(Select Period, AccountPdEnd From CFGDates Where Period = @Period) d,
		(Select WBS1, Right ('00' + ltrim (str (Max (IIF (IsNumeric (ContractNumber) = 1, ContractNumber, 0)) + 1)), 3) ContractNo
		From Contracts
		Group by WBS1) c,
		(Select p.WBS1, p.WBS2, Sum (p.FeeDirLab + p.ReimbAllowCons + p.ReimbAllowExp) As FeeTotal
		From PR p, ProjectCustomTabFields px
		Where p.WBS1 = px.WBS1
		  and p.WBS2 = px.WBS2
		  and p.WBS3 = px.WBS3
		  and p.Sublevel = 'N' -- lowest level fee
		  and px.CustFeeType = 'Cost Plus'
		Group by p.WBS1, p.WBS2) phf,
		(Select WBS1, WBS2, -Sum (Amount) BilledTotal
		From LedgerAR
		Where TransType = 'IN'
		  and Invoice is not null
		  and IsNull (SubType, '') <> 'R'
		  and InvoiceSection <> 'T'
		  and Period <= @Period
		Group by WBS1, WBS2) phi
	Where pt.WBS1 = c.WBS1
	  and pt.WBS2 = ' '   -- project level
	  and pt.Status = 'A'
	  and pt.Sublevel = 'Y'  -- don't want any projects without phases
	  and pt.WBS1 = phf.WBS1
	  and phf.WBS1 = phi.WBS1
	  and phf.WBS2 = phi.WBS2
	  and phf.FeeTotal < phi.BilledTotal
	Order by 1, 2, 3

--	Create project level contract details where project has phases
	Insert Into ContractDetails
	(ContractNumber, WBS1, WBS2, WBS3, FeeDirLab, ReimbAllowCons, ReimbAllowExp)
	Select c.ContractNo, pt.WBS1, ' ' WBS2, ' ' WBS3,
		IIF (Sum (f.FeeLabor) < Sum (i.BilledLabor), Sum (i.BilledLabor)- Sum (f.FeeLabor), 0) FeeDirLab,
		IIF (Sum (f.FeeCons) + Sum (f.FeeExp) < Sum (i.BilledCons) + Sum (i.BilledExp), Sum (i.BilledCons) - Sum (f.FeeCons), 0) ReimbCons, 
		IIF (Sum (f.FeeCons) + Sum (f.FeeExp) < Sum (i.BilledCons) + Sum (i.BilledExp), Sum (i.BilledExp) - Sum (f.FeeExp), 0) ReimbExp
	From PR pt,
		(Select WBS1, Max (ContractNumber) ContractNo
		From Contracts
		Where IsNumeric (ContractNumber) = 1
		Group by WBS1) c,
	--	Compare lowest level
		(Select p.WBS1, p.WBS2, p.WBS3, Sum (FeeDirLab) FeeLabor, Sum (ReimbAllowCons) FeeCons, Sum (ReimbAllowExp) FeeExp, Sum (FeeDirLab + ReimbAllowCons + ReimbAllowExp) FeeTotal
		From PR p, ProjectCustomTabFields px
		Where p.WBS1 = px.WBS1
		  and p.WBS2 = px.WBS2
		  and p.WBS3 = px.WBS3
		  and p.Sublevel = 'N' -- lowest level fee
		  and px.CustFeeType = 'Cost Plus'
		Group by p.WBS1, p.WBS2, p.WBS3) f,
		(Select WBS1, WBS2, WBS3,
			-Sum (CASE When Account = '401.00' Then Amount Else 0 END) BilledLabor,
			-Sum (CASE When Account = '403.00' Then Amount Else 0 END) BilledCons,
			-Sum (CASE When Account = '402.00' Then Amount Else 0 END) BilledExp,
			-Sum (Amount) BilledTotal
		From LedgerAR
		Where TransType = 'IN'
		  and Invoice is not null
		  and IsNull (SubType, '') <> 'R'
		  and InvoiceSection <> 'T'
		  and Period <= @Period
		Group by WBS1, WBS2, WBS3) i
	Where pt.WBS1 = c.WBS1
	  and pt.WBS2 = ' '
	  and pt.Status = 'A'
	  and pt.Sublevel = 'Y'  -- don't want any projects without phases
	  and pt.WBS1 = f.WBS1
	  and f.WBS1 = i.WBS1
	  and f.WBS2 = i.WBS2
	  and f.WBS3 = i.WBS3
	  and f.FeeTotal < i.BilledTotal -- compare lowest level second
	  and exists
		(Select 'x' 
		From 
			--	Compare phase level
			(Select p.WBS1, p.WBS2, Sum (FeeDirLab + ReimbAllowCons + ReimbAllowExp) FeeTotal
			From PR p, ProjectCustomTabFields px
			Where p.WBS1 = px.WBS1
			  and p.WBS2 = px.WBS2
			  and p.WBS3 = px.WBS3
			  and p.Sublevel = 'N' -- lowest level fee
			  and px.CustFeeType = 'Cost Plus'
			Group by p.WBS1, p.WBS2) phf,
			(Select WBS1, WBS2, -Sum (Amount) BilledTotal
			From LedgerAR
			Where TransType = 'IN'
			  and Invoice is not null
			  and IsNull (SubType, '') <> 'R'
			  and InvoiceSection <> 'T'
			  and Period <= @Period
			Group by WBS1, WBS2) phi
			Where pt.WBS1 = phf.WBS1 -- phase fees
			  and phf.WBS1 = phi.WBS1 -- phase invoicing
			  and phf.WBS2 = phi.WBS2
			  and phf.FeeTotal < phi.BilledTotal -- compare phase level first
		)
	Group by c.ContractNo, pt.WBS1
	Order by 1, 2, 3

--	Create contract details at lowest level
	Insert Into ContractDetails
	(ContractNumber, WBS1, WBS2, WBS3, FeeDirLab, ReimbAllowCons, ReimbAllowExp)
	Select c.ContractNo, f.WBS1, f.WBS2, f.WBS3, 
		IIF (f.FeeLabor < i.BilledLabor, i.BilledLabor - f.FeeLabor, 0) FeeDirLab,
		IIF (f.FeeCons + f.FeeExp < i.BilledCons + i.BilledExp, i.BilledCons - f.FeeCons, 0) ReimbCons, 
		IIF (f.FeeCons + f.FeeExp < i.BilledCons + i.BilledExp, i.BilledExp - f.FeeExp, 0) ReimbExp
	From PR pt,
		(Select WBS1, Max (ContractNumber) ContractNo
		From Contracts
		Where IsNumeric (ContractNumber) = 1
		Group by WBS1) c,
	--	Compare lowest level
		(Select p.WBS1, p.WBS2, p.WBS3, FeeDirLab FeeLabor, ReimbAllowCons FeeCons, ReimbAllowExp FeeExp, FeeDirLab + ReimbAllowCons + ReimbAllowExp FeeTotal
		From PR p, ProjectCustomTabFields px
		Where p.WBS1 = px.WBS1
		  and p.WBS2 = px.WBS2
		  and p.WBS3 = px.WBS3
		  and p.Sublevel = 'N' -- lowest level fee
		  and px.CustFeeType = 'Cost Plus') f,
		(Select WBS1, WBS2, WBS3, 
			-Sum (CASE When Account = '401.00' Then Amount Else 0 END) BilledLabor,
			-Sum (CASE When Account = '403.00' Then Amount Else 0 END) BilledCons,
			-Sum (CASE When Account = '402.00' Then Amount Else 0 END) BilledExp,
			-Sum (Amount) BilledTotal
		From LedgerAR
		Where TransType = 'IN'
		  and Invoice is not null
		  and IsNull (SubType, '') <> 'R'
		  and InvoiceSection <> 'T'
		  and Period <= @Period
		Group by WBS1, WBS2, WBS3) i
	Where pt.WBS1 = c.WBS1
	  and pt.WBS2 = ' '
	  and pt.Status = 'A'
	  and pt.Sublevel = 'Y'  -- don't want any projects without phases
	  and pt.WBS1 = f.WBS1
	  and f.WBS1 = i.WBS1
	  and f.WBS2 = i.WBS2
	  and f.WBS3 = i.WBS3
	  and f.FeeTotal < i.BilledTotal -- compare lowest level second
	  and exists
		(Select 'x' 
		From 
			--	Compare phase level
			(Select p.WBS1, p.WBS2, Sum (FeeDirLab + ReimbAllowCons + ReimbAllowExp) FeeTotal
			From PR p, ProjectCustomTabFields px
			Where p.WBS1 = px.WBS1
			  and p.WBS2 = px.WBS2
			  and p.WBS3 = px.WBS3
			  and p.Sublevel = 'N' -- lowest level fee
			  and px.CustFeeType = 'Cost Plus'
			Group by p.WBS1, p.WBS2) phf,
			(Select WBS1, WBS2, -Sum (Amount) BilledTotal
			From LedgerAR
			Where TransType = 'IN'
			  and Invoice is not null
			  and IsNull (SubType, '') <> 'R'
			  and InvoiceSection <> 'T'
			  and Period <= @Period
			Group by WBS1, WBS2) phi
			Where pt.WBS1 = phf.WBS1 -- phase fees
			  and phf.WBS1 = phi.WBS1 -- phase invoicing
			  and phf.WBS2 = phi.WBS2
			  and phf.FeeTotal < phi.BilledTotal -- compare phase level first
		)
	Order by 1, 2, 3

	Update ContractDetails
	Set Fee = FeeDirExp + FeeDirLab
	Where Fee <> FeeDirExp + FeeDirLab

	Update ContractDetails
	Set ReimbAllow = ReimbAllowExp + ReimbAllowCons
	Where ReimbAllow <> ReimbAllowExp + ReimbAllowCons

--	Set Billing & functional Currency at all levels
	Update c
	Set FeeDirLabBillingCurrency = c.FeeDirlab, 
		FeeDirLabFunctionalCurrency = c.FeeDirLab, 
		FeeDirExpBillingCurrency = c.FeeDirExp, 
		FeeDirExpFunctionalCurrency = c.FeeDirExp, 
		FeeBillingCurrency = c.Fee, 
		FeeFunctionalCurrency = c.Fee, 
		ConsultFeeBillingCurrency = c.ConsultFee, 
		ConsultFeeFunctionalCurrency = c.ConsultFee,
		ReimbAllowConsBillingCurrency = c.ReimbAllowCons,
		ReimbAllowConsFunctionalCurrency = c.ReimbAllowCons,
		ReimbAllowExpBillingCurrency = c.ReimbAllowExp,
		ReimbAllowExpFunctionalCurrency = c.ReimbAllowExp,
		ReimbAllowBillingCurrency = c.ReimbAllow,
		ReimbAllowFunctionalCurrency = c.ReimbAllow,
		TotalBillingCurrency = c.Total,
		TotalFunctionalCurrency = c.Total
	From PR p, ContractDetails c
	Where p.WBS1 = c.WBS1
	  and p.WBs2 = ' ' 
	  and p.Status = 'A'

--	Lowest level Fee
	Update p
	Set p.FeeDirLab = c.FeeLabor,
		p.ReimbAllowCons = c.FeeCons,
		p.ReimbAllowExp = c.FeeExp
	From PR p,
		(Select d.WBS1, d.WBS2, d.WBS3, Sum (d.FeeDirLab) FeeLabor, Sum (d.ReimbAllowCons) FeeCons, Sum (d.ReimbAllowExp) FeeExp
		From Contracts c, ContractDetails d
		Where c.WBS1 = d.WBS1
		  and c.ContractNumber = d.ContractNumber
		  and c.FeeIncludeInd = 'Y'
		  and d.WBS2 <> ' ' -- lowest level fee
		 Group by d.WBS1, d.WBS2, d.WBS3) c
	Where p.WBS1 = c.WBS1
	  and p.WBS2 = c.WBS2
	  and p.WBS3 = c.WBS3
	  and p.Sublevel = 'N'
	  and (p.FeeDirLab <> c.FeeLabor
	   or p.ReimbAllowCons <> c.FeeCons
	   or p.ReimbAllowExp <> c.FeeExp)

	Update PR
	Set Fee = FeeDirExp + FeeDirLab
	Where Fee <> FeeDirExp + FeeDirLab

	Update PR
	Set ReimbAllow = ReimbAllowExp + ReimbAllowCons
	Where ReimbAllow <> ReimbAllowExp + ReimbAllowCons

--	Rollup task fees to phase
	Update p
	Set p.FeeDirLab = t.FeeDirLab, 
		p.FeeDirExp = t.FeeDirExp, 
		p.Fee = t.Fee, 
		p.ConsultFee = t.ConsultFee, 
		p.ReimbAllowCons = t.ReimbAllowCons, 
		p.ReimbAllowExp = t.ReimbAllowExp, 
		p.ReimbAllow = t.ReimbAllow
	From PR p,
	(Select WBS1, WBS2,
			sum (FeeDirLab) FeeDirLab, 
			sum (FeeDirExp) FeeDirExp,
			sum (Fee) Fee, 
			sum (ConsultFee) ConsultFee,
			sum (ReimbAllowCons) ReimbAllowCons,
			sum (ReimbAllowExp) ReimbAllowExp,
			sum (ReimbAllow) ReimbAllow
	From PR
	Where WBS3 <> ' ' -- Task records
		and FeeDirLab + FeeDirExp + ConsultFee + ReimbAllowCons + ReimbAllowExp <> 0
	Group by WBS1, WBS2) t -- Group by Phase
	Where p.WBS1 = t.WBS1 
		and p.WBS2 = t.WBS2
		and p.WBS3 = ' '  -- Phase level only
		and p.FeeDirLab + p.FeeDirExp + p.ConsultFee + p.ReimbAllowCons + p.ReimbAllowExp <> t.FeeDirLab + t.FeeDirExp + t.ConsultFee + t.ReimbAllowCons + t.ReimbAllowExp 

--	Rollup phase fees to project
	Update p
	Set p.FeeDirLab = t.FeeDirLab, 
		p.FeeDirExp = t.FeeDirExp, 
		p.Fee = t.Fee, 
		p.ConsultFee = t.ConsultFee, 
		p.ReimbAllowCons = t.ReimbAllowCons, 
		p.ReimbAllowExp = t.ReimbAllowExp, 
		p.ReimbAllow = t.ReimbAllow
	From PR p,
	(Select WBS1, 
			sum (FeeDirLab) FeeDirLab, 
			sum (FeeDirExp) FeeDirExp,
			sum (Fee) Fee, 
			sum (ConsultFee) ConsultFee,
			sum (ReimbAllowCons) ReimbAllowCons,
			sum (ReimbAllowExp) ReimbAllowExp,
			sum (ReimbAllow) ReimbAllow
	From PR
	Where WBS2 <> ' ' -- Phase records
		and WBS3 = ' '  -- Exclude tasks
	Group by WBS1) t -- Group by Project
	Where p.WBS1 = t.WBS1 
		and p.WBS2 = ' '  -- Project level only
		and p.FeeDirLab + p.FeeDirExp + p.ConsultFee + p.ReimbAllowCons + p.ReimbAllowExp <> t.FeeDirLab + t.FeeDirExp + t.ConsultFee + t.ReimbAllowCons + t.ReimbAllowExp 

--	Set Billing & functional Currency at all levels
	Update PR
	Set FeeDirLabBillingCurrency = FeeDirlab, 
		FeeDirLabFunctionalCurrency = FeeDirLab, 
		FeeDirExpBillingCurrency = FeeDirExp, 
		FeeDirExpFunctionalCurrency = FeeDirExp, 
		FeeBillingCurrency = Fee, 
		FeeFunctionalCurrency = Fee, 
		ConsultFeeBillingCurrency = ConsultFee, 
		ConsultFeeFunctionalCurrency = ConsultFee,
		ReimbAllowConsBillingCurrency = ReimbAllowCons,
		ReimbAllowConsFunctionalCurrency = ReimbAllowCons,
		ReimbAllowExpBillingCurrency = ReimbAllowExp,
		ReimbAllowExpFunctionalCurrency = ReimbAllowExp,
		ReimbAllowBillingCurrency = ReimbAllow,
		ReimbAllowFunctionalCurrency = ReimbAllow
	Where exists (Select 'x' From PR x Where x.WBS1 = PR.WBS1 and WBS2 = ' ' and Status = 'A')

	-- Create Sales
	Insert Into Projects_Sales
	(Seq, CustSalesContract, WBS1, WBS2, WBS3, CustSalesTeam, CustSalesDate, CustSalesAmount, CreateUser)
	Select Replace (NEWID(), '-', '') Seq, ContractNo, WBS1, ' ' WBS2, ' ' WBS3, Org, @Date as SalesDate, Amount, 'spCCG_FeeAdjustment'
	From 
    --  Use the Org from the project lowest level, not the invoice org, so the current project org gets the sales.
		(Select c.ContractNo, p.WBS1, p.Org, Sum (d.FeeDirLab + d.FeeDirExp + d.ConsultFee + d.ReimbAllowExp + d.ReimbAllowCons) Amount
		From PR p, ContractDetails d,
			(Select WBS1, Max (ContractNumber) ContractNo
			From Contracts
			Where ContractType = '05'
			  and ApprovedDate = @Date
			Group by WBS1) c
		Where p.WBS1 = c.WBS1
		  and c.ContractNo = d.ContractNumber
		  and p.WBS1 = d.WBS1
		  and p.WBS2 = d.WBS2
		  and p.WBS3 = d.WBS3
		  and p.Sublevel = 'N'
		Group by c.ContractNo, p.WBS1, p.Org) x
	Order by 1, 2, 3

	exec spCCG_ProjectTotalCompensation '%'
END
GO
