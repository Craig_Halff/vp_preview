SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Get_Functions]
	@excludeGetD_E		bit = 1
AS
BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_EI_Config_Get_Functions] 0
	SET NOCOUNT ON;

	SELECT 'dbo.'+name, name
		FROM sys.objects
		WHERE name like '%fnCCG_EI_%'
			AND type in (N'FN', N'IF', N'TF', N'FS', N'FT')
			AND name not in ('fnCCG_EI_projectfilter', 'fnCCG_EI_RegexMatch')
			AND (name not in ('fnCCG_EI_GetD', 'fnCCG_EI_GetE') OR @excludeGetD_E = 0)
		ORDER BY 1
END;
GO
