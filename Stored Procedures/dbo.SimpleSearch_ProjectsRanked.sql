SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SimpleSearch_ProjectsRanked]
  @criteria VARCHAR(50) = NULL
AS
BEGIN
  ------------------------------------------------------------------------
  SET NOCOUNT ON;

  ------------------------------------------------------------------------
  DECLARE @search_criteria VARCHAR(25) = '%' 
                                       + COALESCE(@criteria, '') 
                                       + '%'

  ------------------------------------------------------------------------
  SELECT
    R.WBS1                      AS "WBS1"
   ,Project.[Name]              AS "ProjectName"
   ,Marketing.CustMarketingName AS "MarketingName"
   ,R.[Rank]                    AS "Rank"
  FROM
     (SELECT
        SearchResults.WBS1
       ,SUM(SearchResults.[Rank]) AS "Rank"
      FROM
        (SELECT
           P.WBS1
          ,4 AS "Rank"
         FROM
           dbo.PR AS P
         WHERE
           P.WBS1 LIKE @search_criteria
         ----------------------------------------------------------------
         UNION
         ----------------------------------------------------------------
         SELECT
           P.WBS1
          ,2 AS "Rank"
         FROM
           dbo.PR AS P
         WHERE
           P.Name LIKE @search_criteria
         ----------------------------------------------------------------
         UNION
         ----------------------------------------------------------------
         SELECT
           PX.WBS1
          ,1 AS "Rank"
         FROM
           dbo.ProjectCustomTabFields AS PX
         WHERE
           PX.CustMarketingName LIKE @search_criteria) AS SearchResults
      GROUP BY
        SearchResults.WBS1) AS R

      INNER JOIN dbo.PR AS Project
        ON R.WBS1                 = Project.WBS1
          AND Project.WBS2        = ' '
          AND Project.ChargeType  = 'R'

      INNER JOIN dbo.ProjectCustomTabFields AS Marketing
        ON Project.WBS1       = Marketing.WBS1
          AND Marketing.WBS2  = ' '

  ORDER BY
    R.[Rank]                    DESC
   ,R.WBS1                      ASC
   ,Project.[Name]              ASC
   ,Marketing.CustMarketingName ASC
--------------------------------------------------------------------------
END
GO
