SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[spCCG_ProjectContractNoValidation] @ContractNumber varchar (255)
AS
/*
Copyright 2019 (c) Central Consulting Group, Inc.  All rights reserved.
09/19/2019	David Springer
			Validate Contract Number is 3-digit numeric
			Call this on a Contract INSERT workflow
			Call this on a Contract CHANGE workflow when Contract Number has changed
*/
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

	If len (@ContractNumber) <> 3
		RAISERROR ('Contract Number must be 3 digits.                                                             ', 16, 1)

	If IsNumeric (@ContractNumber) = 0 -- non-numeric
		RAISERROR ('Contract Number must be numeric.                                                             ', 16, 1)
END
GO
