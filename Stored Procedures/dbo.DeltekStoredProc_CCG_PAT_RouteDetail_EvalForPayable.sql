SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_RouteDetail_EvalForPayable]
	@RouteValue	Nvarchar(500),
	@wbs1		Nvarchar(30),
	@wbs2		Nvarchar(7),
	@wbs3		Nvarchar(7),
	@lowest		bit
AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_RouteDetail_EvalForPayable] 'Name', '2003005.00', '2SD', '--', 1
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	DECLARE @sql	Nvarchar(max);
	DECLARE @rows	int = 0;

	SET @sql = N'
		SELECT ' + @RouteValue + '
			FROM PR
				LEFT JOIN ProjectCustomTabFields c on PR.WBS1 = c.WBS1 and PR.WBS2 = c.WBS2 and PR.WBS3 = c.WBS3
			WHERE PR.WBS1 = N''' + @wbs1 + ''' and PR.WBS2 = N''' + @wbs2 + ''' and PR.WBS3 = N''' + @wbs3 + '''
				AND ISNULL(' + @RouteValue + ', '''') <> '''';

		SELECT ' + @RouteValue + '
			FROM PR
				LEFT JOIN ProjectCustomTabFields c on PR.WBS1 = c.WBS1 and PR.WBS2 = c.WBS2 and PR.WBS3 = c.WBS3
			WHERE PR.WBS1 = N''' + @wbs1 + ''' and PR.WBS2 = N''' + @wbs2 + ''' and PR.WBS3 = N'' ''
				AND ISNULL(' + @RouteValue + ', '''') <> '''';

		SELECT ' + @RouteValue + '
			FROM PR
				LEFT JOIN ProjectCustomTabFields c on PR.WBS1 = c.WBS1 and PR.WBS2 = c.WBS2 and PR.WBS3 = c.WBS3
			WHERE PR.WBS1 = N''' + @wbs1 + ''' and PR.WBS2 = N'' '' and PR.WBS3 = N'' ''
				AND ISNULL(' + @RouteValue + ', '''') <> ''''';
	EXEC (@sql);
END;

GO
