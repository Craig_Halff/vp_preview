SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_VEAddress_Lookup] (
	@vendor		Nvarchar(20),
	@address	Nvarchar(20)
) AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SET NOCOUNT ON;

	SELECT Address, PrimaryInd, Accounting, Address1, Address2, Address3, Address4, City, State, Zip
        FROM VEAddress
        WHERE Vendor = @vendor and address = @address
END;

GO
