SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_PctCompleteUpdateHistory]
	@insertValuesSql	Nvarchar(max)
AS
BEGIN
	-- Was Called DeltekStoredProc_CCG_EI_Pct_Complete_Update_History
	SET NOCOUNT ON
	DECLARE @safeSql	bit

	-- Save a history of this update
	IF LEN(@insertValuesSql) > 0
		EXEC (N'
		INSERT INTO CCG_EI_HistoryUpdateColumns
			(ActionTaken, ActionDate, ActionTakenBy, WBS1, WBS2, WBS3, BTFSeq,
				DataType, ConfigCustomColumn, ColumnLabel, OldValue, NewValue, OldValueDecimal, NewValueDecimal)
			VALUES ' + @insertValuesSql)

END
GO
