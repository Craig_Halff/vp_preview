SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_PackageUpdate] (
	@wbs1			Nvarchar(32),	
	@documentId		varchar(300),
	@newOrder		int
)
AS
BEGIN
	-- Copyright (c) 2020 EleVia Software. All rights reserved. 
	declare @sql nvarchar(max) = ' where wbs1 = N'''+@wbs1+''' and wbs2 = N'' '' and wbs3 = N'' '''
	if @documentId = 'CustFinalInvPkgIncludeInvoice'
		set @sql = N'update ProjectCustomTabFields set CustFinalInvPkgIncludeInvoice = '+cast(@newOrder as varchar(20)) + @sql
	else if @documentId = 'CustFinalInvPkgIncludeTimesheetData'
		set @sql = N'update ProjectCustomTabFields set CustFinalInvPkgIncludeTimesheetData = '+cast(@newOrder as varchar(20)) + @sql
	else if @documentId = 'CustFinalInvPkgIncludeSubInvoices'
		set @sql = N'update ProjectCustomTabFields set CustFinalInvPkgIncludeSubInvoices = '+cast(@newOrder as varchar(20)) + @sql
	else if @documentId = 'CustFinalInvPkgIncludeExpReceipts'
		set @sql = N'update ProjectCustomTabFields set CustFinalInvPkgIncludeExpReceipts = '+cast(@newOrder as varchar(20)) + @sql
	else if @documentId = 'CustFinalInvPkgIncludeExpenseReport'
		set @sql = N'update ProjectCustomTabFields set CustFinalInvPkgIncludeExpenseReport = '+cast(@newOrder as varchar(20)) + @sql
	else if left(@documentId, 4) <> 'Cust'
		set @sql = N'update Projects_FinalInvPkgUserDocuments set CustOrder = '+cast(@newOrder as varchar(20))+' where Seq = '''+replace(@documentId, '''', '''''')+''''
	exec(@sql)
END
GO
