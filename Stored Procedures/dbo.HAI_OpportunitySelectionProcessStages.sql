SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_OpportunitySelectionProcessStages]
    @OpportunityID varchar(32), @SelectionProcess varchar(25), @Stage varchar(25)
AS
/* 
	Copyright (c) 2020 Halff Associates, Inc.  All rights reserved.
	10/04/2020	Craig H. Anderson

	Compare selected Stage against Selection Process.
*/
DECLARE @error varchar(255) = '';
SET NOCOUNT ON;
BEGIN
    DECLARE @stageName varchar(25) = (
                SELECT  Description
                FROM    dbo.CFGOpportunityStageDescriptions
                WHERE   Code = @Stage
            );
    IF @stageName IN ( '03', '04' )
       AND  @SelectionProcess NOT IN ( 'RFQ Process', 'RFQ/RFP Process' )
    BEGIN
        SET @error
            = CONCAT(
                  'Opportunity Stage: '
                , @stageName
                , ' is only valid for the "RFQ" or "RFQ+RFP" Selection Process.                                           '
              );
        RAISERROR(@error, 16, 1);
    END;
    ELSE IF @stageName IN ( '05', '06' )
            AND @SelectionProcess NOT IN ( 'RFQ/RFP Process' )
    BEGIN
        SET @error
            = CONCAT(
                  'Opportunity Stage: '
                , @stageName
                , ' is only valid for the "RFQ+RFP" Selection Process.                                           '
              );
        RAISERROR(@error, 16, 1);
    END;
    ELSE IF @stageName IN ( '07', '08' )
            AND @SelectionProcess NOT IN ( 'Fee Proposal', 'Task Work Order' )
    BEGIN
        SET @error
            = CONCAT(
                  'Opportunity Stage: '
                , @stageName
                , ' is only valid for the "Fee Proposal" or "Task Work Order" Selection Process.                                           '
              );
        RAISERROR(@error, 16, 1);
    END;
END;
GO
