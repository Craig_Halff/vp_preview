SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ClearCustomFKColumns] @dataType varchar(30), @keyValue nvarchar(max), @isUnicode varchar(1) --Y/N
AS

  /* This procedure finds all the custom fields in all custom tables with a particular data type that are foreign keys.
     It then clears out any of them that have a particular key value.  This function is called when deleting Hub records. */

  DECLARE @keyVarchar varchar(max)
  DECLARE @tableName varchar(100)
  DECLARE @colName varchar(100)
  DECLARE @stmt nvarchar(500)

  IF (@isUnicode = 'N')
	SET @keyVarchar = CONVERT(varchar,@keyValue)

  DECLARE curTableList CURSOR LOCAL FOR
	SELECT CT.TableName, 
			CC.Name 
	FROM FW_CustomColumnsData CC 
		INNER JOIN CustomTablesWithUDIC CT ON CC.InfoCenterArea = CT.InfoCenterArea AND CC.GridID = CT.GridID 
	WHERE CT.TableName IS NOT NULL AND CC.DataType = @dataType

  OPEN curTableList;

  FETCH NEXT FROM curTableList INTO @tableName, @colName

   WHILE @@FETCH_STATUS = 0            
      BEGIN
	IF (@isUnicode = 'Y')
		BEGIN
			SET @stmt = 'UPDATE ' + @tableName + ' SET ' + @colName + ' = NULL WHERE ' + @colName + ' = @key'
			EXECUTE sp_executesql @stmt,N'@key nvarchar(max)',@key = @keyValue
		END
	ELSE
		BEGIN
			SET @stmt = 'UPDATE ' + @tableName + ' SET ' + @colName + ' = NULL WHERE ' + @colName + ' = @key'
			EXECUTE sp_executesql @stmt,N'@key varchar(max)',@key = @keyVarchar
		END

	FETCH NEXT FROM curTableList INTO @tableName, @colName
   END;

   CLOSE curTableList;
   DEALLOCATE curTableList;
GO
