SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpSpreadVar]
  @strPlanID varchar(32),
  @strJTDDate varchar(10), -- Date must be in format: 'yyyy-mm-dd'
  @strSpreadLab varchar(1) = 'Y',
  @strSpreadExp varchar(1) = 'N',
  @strSpreadCon varchar(1) = 'N',
  @strIgnoreVariance varchar(1) = 'N'
AS

BEGIN -- Procedure rpSpreadVar

  SET NOCOUNT ON

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  /* If this is a Navigator Plan, then exit this stored procedure. */

  IF ((SELECT COUNT(*) FROM PNPlan WHERE PlanID = @strPlanID) > 0) RETURN

  PRINT 'This is a Vision Plan. Proceed with Spread Variance...'
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  
  DECLARE @intTaskDependency int
  
  DECLARE @strPlanName Nvarchar(255)
  
  DECLARE @dtJTDDate datetime
  DECLARE @dtETCDate datetime
  
  DECLARE @strCompany Nvarchar(14)
  
  DECLARE @strMatchWBS1Wildcard varchar(1)
  DECLARE @strUnpostedFlg varchar(1)
  DECLARE @strCommitmentFlg varchar(1)
  DECLARE @strExcludeUNFlg varchar(1)
  DECLARE @strCalcExpBill varchar(1)
  DECLARE @strCalcConBill varchar(1)
  
  DECLARE @sintGRMethod smallint
  DECLARE @sintCostRtMethod smallint
  DECLARE @sintBillingRtMethod smallint
  
  DECLARE @intCostRtTableNo int
  DECLARE @intBillingRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int
  
  SELECT @intTaskDependency = COUNT(*) FROM RPDependency WHERE PlanID = @strPlanID
  
  IF (@intTaskDependency > 0)
    BEGIN
      -- SELECT @strPlanName = PlanName FROM RPPlan WHERE PlanID = @strPlanID
      -- RAISERROR('Plan {%s} has Task Dependency, therefore, cannot be processed by Spread Variance.', 9, 1, @strPlanName)
      RETURN
    END -- If-Then

  -- Compute JTD and ETC Dates.

  SET @dtJTDDate = CONVERT(datetime, @strJTDDate)
  SET @dtETCDate = DATEADD(d, 1, @dtJTDDate)
  
  -- Get Current Company setting.
  
  SELECT @strCompany = dbo.GetActiveCompany()

  -- Create a temporary Calender to catch the overflow data behind the regular calendar.
  
  SELECT StartDate, EndDate, PeriodScale INTO #Calendar FROM 
  (SELECT PlanID AS PlanID, StartDate AS StartDate, EndDate AS EndDate, PeriodScale AS PeriodScale 
     FROM RPCalendarInterval WHERE PlanID = @strPlanID 
   UNION ALL 
   SELECT PlanID AS PlanID, DATEADD(d, 1, MAX(EndDate)) AS StartDate, 
     DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate))) AS EndDate, 
     'o' AS PeriodScale 
     FROM RPCalendarInterval WHERE PlanID = @strPlanID 
     GROUP BY PlanID) AS CI
     
  -- Get Parameters from CFGResourcePlanning
  -- When Unit Tab is in use, need to exclude TransType = 'UN' from JTD of Expense & Consultant.
  
  SELECT
    @strMatchWBS1Wildcard = MatchWBS1Wildcard,
    @strExcludeUNFlg = UntTab 
    FROM CFGResourcePlanning WHERE Company = @strCompany
    
  -- Get Parameters from RPPlan
  
  SELECT
    @strUnpostedFlg = UnpostedFlg,
    @strCommitmentFlg = CommitmentFlg,
    @strCalcExpBill = CalcExpBillAmtFlg,
    @strCalcConBill = CalcConBillAmtFlg,
    @sintGRMethod = GRMethod,
    @intGenResTableNo = GenResTableNo,
    @intGRBillTableNo = GRBillTableNo,
    @sintCostRtMethod = CostRtMethod,
    @sintBillingRtMethod = BillingRtMethod,
    @intCostRtTableNo = CostRtTableNo,
    @intBillingRtTableNo = BillingRtTableNo
    FROM RPPlan WHERE PlanID = @strPlanID
    
  -- Spread Labor (if needed).
  
  IF (@strSpreadLab = 'Y')
    BEGIN
    
      EXECUTE dbo.rpSpreadLabVar
        @strPlanID,
        @dtETCDate,
        @strMatchWBS1Wildcard,
        @strUnpostedFlg,
        @sintGRMethod,
        @intGenResTableNo,
        @intGRBillTableNo,
        @sintCostRtMethod,
        @sintBillingRtMethod,
        @intCostRtTableNo,
        @intBillingRtTableNo,
        @strIgnoreVariance
    
    END -- If-Then
    
  -- Spread Expense (if needed).
  
  IF (@strSpreadExp = 'Y')
    BEGIN
    
      EXECUTE dbo.rpSpreadExpConVar
        @strPlanID,
        @dtETCDate,
        @strMatchWBS1Wildcard,
        @strCommitmentFlg,
        @strExcludeUNFlg,
        @strCalcExpBill,
        'E',
        @strIgnoreVariance
          
    END -- If-Then

  -- Spread Expense (if needed).
  
  IF (@strSpreadCon = 'Y')
    BEGIN
    
      EXECUTE dbo.rpSpreadExpConVar
        @strPlanID,
        @dtETCDate,
        @strMatchWBS1Wildcard,
        @strCommitmentFlg,
        @strExcludeUNFlg,
        @strCalcConBill,
        'C',
        @strIgnoreVariance
          
    END -- If-Then

  -- Recalculate the Plan
  
  EXECUTE dbo.rpDeleteSummaryTPD @strPlanID
  EXECUTE dbo.rpConsolidateOverflowTPD @strPlanID
  EXECUTE dbo.rpCalcTPD @strPlanID
  EXECUTE dbo.rpSumUpTPD @strPlanID
  
  -- Update StartDate and EndDate for Task rows.
  
  UPDATE RPTask
    SET 
      StartDate = CASE WHEN XTPD.StartDate < T.StartDate THEN XTPD.StartDate ELSE T.StartDate END, 
      EndDate = CASE WHEN XTPD.EndDate > T.EndDate THEN XTPD.EndDate ELSE T.EndDate END
    FROM RPTask AS T
      INNER JOIN
        (SELECT TaskID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate
           FROM
             (SELECT TaskID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate
                FROM RPPlannedLabor WHERE PlanID = @strPlanID AND AssignmentID IS NULL
                GROUP BY TaskID
              UNION ALL
              SELECT TaskID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate
                FROM RPPlannedExpenses WHERE PlanID = @strPlanID AND ExpenseID IS NULL
                GROUP BY TaskID
              UNION ALL
              SELECT TaskID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate
                FROM RPPlannedConsultant WHERE PlanID = @strPlanID AND ConsultantID IS NULL
                GROUP BY TaskID
              UNION ALL
              SELECT TaskID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate
                FROM RPPlannedUnit WHERE PlanID = @strPlanID AND UnitID IS NULL
                GROUP BY TaskID) AS YTPD
           GROUP BY TaskID) AS XTPD
        ON T.PlanID = @strPlanID AND T.TaskID = XTPD.TaskID
        WHERE T.StartDate != XTPD.StartDate OR T.EndDate != XTPD.EndDate
    

  SET NOCOUNT OFF

END -- rpSpreadVar
GO
