SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_CollectionsUpdate3]
AS 
/* 
Copyright (c) 2020 Central Consulting Group.  All rights reserved.
07/01/2020	David Springer
			Update UDIC_Collections 
			Notify Operations Manager, one email per team, from: PA, cc: TeamLeader, bcc: PA
			Call this from a Collections scheduled workflow
			Schedule every Monday (4th Monday is critical to code below)
10/17/2020	Craig H. Anderson
			Removed the Testing IF
02/22/2021	Craig H. Anderson
			Fix for single invoice per project issue
*/
DECLARE @Subject		varchar (120), -- length of the CCG_Email subject field
		@SubjectTemplate varchar (120),
		@HistoryAction	varchar (255),
		@Intro			varchar (max),
		@OMFirstName	varchar (50),
		@OMEmail		varchar (255),
		@TeamCode		varchar (10),
		@Team			varchar (255),
		@TLEmpNo		varchar (32),
		@TLEmail		varchar (255),
		@PAEmail		varchar (255),
		@PAEmailAll		varchar (1000),
		@WBS1			varchar (32),
		@Project		varchar (2000),
		@UDIC_UID		varchar (32),
		@InvoiceNo		varchar (255),
		@InvoiceDate	Date,
		@Balance		float,
		@ARTotal		float,
		@DaysOut		integer,
		@ResponseDate	date,
		@Response		varchar (255),
		@HistoryDate	date,
		@HistoryActivity varchar (255),
		@HistoryNotes	varchar (max),
		@CSS			varchar (max),
        @Body			varchar (max),
		@Loop1			integer,
		@Loop2			integer,
		@Loop3			integer,
		@nRank			int,
		@Contactor	    varchar(50)

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON



	Begin

	Set @SubjectTemplate = 'Summary of Collections Over 90-Days  '
	Set @HistoryAction = 'OM Monthly Collections Notification'
		

	DECLARE curTeam insensitive cursor for
			Select distinct IsNull (om.PreferredName, om.FirstName), om.Email, o.Code, o.Label Team, tl.Employee, tl.Email
			FROM UDIC_Collection c, PR p, EM om, EM tl, CFGFormat f, CFGOrgCodes o
			Where c.CustCollectionStatus in ('Open', 'Legal Involvement')
				AND c.CustBalance > 0
				and IsNull (c.CustRequestedActionStart, '01/01/1900') <= convert (date, getDate())
				and datediff(day, c.CustInvoiceDate, getdate()) > 90
				and c.CustProject = p.WBS1
				and p.WBS2 = ' '
				and Right (p.Org, f.Org3Length) = o.Code
				and o.OrgLevel = '3'
				and o.Status = 'A'
				and c.CustOperationsManager = om.Employee
				and c.CustPrincipal = tl.Employee
				and exists (Select 'x' From UDIC_Collection_History Where UDIC_UID = c.UDIC_UID and CustHistoryResponse IN ('Verified Invoice Received', 'Expect Pay within 30 days', 'Expect Pay within 60 days', 'Expect Pay within 90 days'))
			Order by 1

	End

-- CSS style rules
	Set @CSS = 
'
<style type="text/css">
    table {width:100%;border-collapse: collapse; font-size:10pt;}
	p {padding-top:10pt;font-family: Calibri, Arial, sans-serif; font-size:10pt; margin-top:5px; margin-bottom:5px;}
	td, th {border: 1pt solid black;padding: 5px;font-family:Calibri, Arial, sans-serif; font-size:10pt;}
	th {background-color: rbg(68,114,96);color: white; text-align:center;font-size:10.5pt;padding-left:5px;padding-right:5px;}
	.aLeft {text-align:left;}
	.aCenter {text-align:center;}
	.aRight {text-align:right;}
	.Table_Header tr th {background-color: #4472c4;color: white;}
	.Project_Header {background-color:#CCCCCC; color:black;}
	tfoot tr td {border:none;font-weight:bold;}
    tfoot tr td.aRight {border-bottom: 3pt double;black;}
	p.hangingIndent {margin-left:.75in;text-indent:-.75in; }
    td.Response {background-color:#ffffcc;}
    td {max-height:5em;}
    div.scrollable {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        overflow-y: scroll;
    }
	.noteDiv {margin-left:20px; clear:left;width:100%;}
</style>
';

--	############  Team  ##############
	OPEN curTeam
	FETCH NEXT FROM curTeam INTO @OMFirstName, @OMEmail, @TeamCode, @Team, @TLEmpNo, @TLEmail
	Set @Loop1 = @@FETCH_STATUS
	While @Loop1 = 0
		BEGIN
		
		Set @Subject = @SubjectTemplate + ' ' + @TeamCode +': ' + @Team

		--raiserror('Starting Loop 1',0, 1) with nowait;
		Set @PAEmailAll = ''
		Set @ARTotal = 0
	--	Open the table with headers
	Set @Intro = '<p>This is a summary of invoices for '+ @TeamCode +': ' + @Team+'.</p>'
		Set @Body = '<p>' + @OMFirstName + ',</p>
    <p>' + @Intro + '</p><br>'+
   '<table cellpadding="5" cellspacing="0" style="border:1px solid black;border-collapse:collapse;">
	<thead class="Table_Header">
		<tr>
			<th rowspan="2" style="width:3em;" >Invoice<br>Number</th>
			<th rowspan="2" style="width:3em;" >Invoice<br>Date</th>
			<th rowspan="2" style="width:4em;" >Invoice<br>Balance</th>
			<th rowspan="2" style="width:2em;" >Days<br>Out</th>
			<th colspan="2">Last Completed Collection Activity</th>
		</tr>
		<tr> 
			<th>Client Response<br>Classification</th>
			<th>Additional Collections Notes</th>
		</tr>
	</thead>
	<tbody>
';
	--	############  Project  ##############
		DECLARE curProject insensitive cursor for
				Select distinct p.WBS1,
					bc.Name + ' (' + IsNull (bcc.PreferredName, bcc.FirstName) + ' ' + bcc.LastName + '): ' + p.WBS1 + ' - ' + p.Name + ' (PM ' + IsNull (pm.PreferredName, pm.FirstName)  + ' ' + pm.LastName + ')' Project,
					pa.EMail
				From UDIC_Collection c, PR p, EM pm, EM pa, CL bc, Contacts bcc
				Where c.CustCollectionStatus in ('Open', 'Legal Involvement')
					AND c.CustBalance > 0
					and IsNull (c.CustRequestedActionStart, '01/01/1900') <= convert (date, getDate())
					and datediff(day, c.CustInvoiceDate, getdate()) > 90
					and c.CustPrincipal = @TLEmpNo
					and c.CustProjMgr = pm.Employee
					and c.CustBiller = pa.Employee
					and c.CustProject = p.WBS1
					and p.WBS2 = ' '
					and c.CustBillingClient = bc.ClientID
					and c.CustBillingContact = bcc.ContactID
                    and exists (Select 'x' From UDIC_Collection_History Where UDIC_UID = c.UDIC_UID and CustHistoryResponse IN ('Verified Invoice Received', 'Expect Pay within 30 days', 'Expect Pay within 60 days', 'Expect Pay within 90 days'))
				Order by 2

		OPEN curProject
		FETCH NEXT FROM curProject INTO @WBS1, @Project, @PAEmail
		Set @Loop2 = @@FETCH_STATUS
		While @Loop2 = 0
			BEGIN
			Set @PAEmailAll = @PAEmailAll + @PAEmail + ';'
			--	############  Invoice  ##############
			DECLARE curInvoice insensitive cursor for
					SELECT c.UDIC_UID, c.CustInvoiceNumber, c.CustInvoiceDate, c.CustBalance, c.CustDaysOutstanding, h.ResponseDate, h.Response
					FROM dbo.UDIC_Collection AS c
						LEFT OUTER JOIN
							(
								SELECT uch.UDIC_UID, uch.CustHistoryResponseDate AS ResponseDate, uch.CustHistoryResponse AS Response
									FROM dbo.UDIC_Collection_History AS uch
										INNER JOIN
											(
												SELECT hm.UDIC_UID, MAX(hm.CustHistoryResponseDate) AS maxDate
													FROM dbo.UDIC_Collection_History hm
													WHERE hm.CustHistoryResponseDate IS NOT NULL
													GROUP BY hm.UDIC_UID
											) AS uchm
											ON uch.UDIC_UID = uchm.UDIC_UID
											   AND uch.CustHistoryResponseDate = uchm.maxDate
							) h
							ON c.UDIC_UID = h.UDIC_UID
					WHERE
					c.CustCollectionStatus IN ( 'Open', 'Legal Involvement' )
					AND c.CustBalance > 0
					AND DATEDIFF(DAY, c.CustInvoiceDate, GETDATE()) > 90
					AND EXISTS
						(
							SELECT 'x'
								FROM dbo.UDIC_Collection_History
								WHERE
								UDIC_UID = c.UDIC_UID
								AND CustHistoryResponse IN ( 'Verified Invoice Received', 'Expect Pay within 30 days', 'Expect Pay within 60 days', 'Expect Pay within 90 days' )
						)
					AND c.CustProject = @WBS1
					ORDER BY c.CustInvoiceNumber;
			Set @Body =  @Body +
				'<tr>
					<td class="Project_Header" colspan="6">' + @Project + '</td>
				</tr>'
			OPEN curInvoice
			FETCH NEXT FROM curInvoice INTO @UDIC_UID, @InvoiceNo, @InvoiceDate, @Balance, @DaysOut, @ResponseDate, @Response
			Set @Loop3 = @@FETCH_STATUS
			While @Loop3 = 0
				BEGIN
				--	############  History  ##############
				DECLARE curHistory insensitive cursor for
						SELECT distinct top 5 CAST(HistoryDate AS date) AS HistoryDate, Activity, Notes, Contactor, nRank
						From
						(
						Select CustHistoryActionStart HistoryDate, CustHistoryAction Activity, null Notes,NULL AS Contactor, 2 AS nRank
						From UDIC_Collection_History
						Where UDIC_UID = @UDIC_UID
						  and CustHistoryActionStart is not NULL AND CustHistoryAction NOT IN ('Verify Invoice Received','PM to Follow Up')
						Union
						Select CustHistoryResponseDate, CustHistoryResponse, CustHistoryResponseNotes, CONCAT(ISNULL(e.PreferredName,e.FirstName),' ', e.LastName) AS Contactor, 1 AS nRank
						From UDIC_Collection_History
						LEFT OUTER JOIN dbo.EM AS e ON CustHistoryContactedbyEmployee = e.Employee
						Where UDIC_UID = @UDIC_UID
						  and CustHistoryResponseDate is not null
						) a WHERE a.Activity NOT IN ( 'OM Monthly Collections Notification','OTM Monthly Collections Notification', '')
						Order by 1 desc, nRank
		
				Set @ARTotal = @ARTotal + @Balance
				Set @Body = @Body +
					'<tr>
						<td class="aRight">' + @InvoiceNo + '</td>
						<td class="aCenter">' + convert (varchar (10), @InvoiceDate, 101) + '</td>
						<td class="aRight">$' + convert (varchar, cast (@Balance as money), 1)+ '</td>
						<td class="aRight">' + convert (varchar, cast (@DaysOut as integer), 1)+ '</td>
						<td class="aLeft"'+IIF(@ResponseDate is NULL OR DATEDIFF(DAY,@ResponseDate,getdate()) > 30,'style="color:red;" ', '' )+'>' + IIF (@ResponseDate is null, 'No followup recorded.', convert (varchar (10), @ResponseDate, 101) + ' - ' + @Response) + '</td>
						<td class="aLeft">'
				OPEN curHistory
				FETCH NEXT FROM curHistory INTO @HistoryDate, @HistoryActivity, @HistoryNotes, @Contactor, @nRank
				While @@FETCH_STATUS = 0
					BEGIN
						SET @Body = @Body +	'<p>' + Convert(varchar(10), @HistoryDate, 101) + ': ' + @HistoryActivity + IIF(@Contactor IS NOT NULL, ' ('+@Contactor+') ','') + IIF (@HistoryNotes is null, '</p>', ' </p><div class="noteDiv"> ' + @HistoryNotes)+ '</div>'
					--	End History
					FETCH NEXT FROM curHistory INTO @HistoryDate, @HistoryActivity, @HistoryNotes, @Contactor, @nRank
					END
				CLOSE curHistory
				DEALLOCATE curHistory
				Set @Body = @Body +
							'</td>
					</tr>'

				Insert Into UDIC_Collection_History
				(UDIC_UID, Seq, CustHistoryActionStatus, CustHistoryAction, CustHistoryActionStart)
				Values (@UDIC_UID, Replace (NewID(), '-', ''), 'Sent', @HistoryAction, getDate())

			--	End Invoice
				FETCH NEXT FROM curInvoice INTO @UDIC_UID, @InvoiceNo, @InvoiceDate, @Balance, @DaysOut, @ResponseDate, @Response
				Set @Loop3 = @@FETCH_STATUS
				END
			CLOSE curInvoice
			DEALLOCATE curInvoice

		--	End Project
			FETCH NEXT FROM curProject INTO @WBS1, @Project, @PAEmail
			Set @Loop2 = @@FETCH_STATUS
			END
		CLOSE curProject
		DEALLOCATE curProject

		IF @Body IS NOT NULL AND @CSS IS NOT NULL 
		BEGIN
			Set @Body = @Body +
				'</tbody>
				<tfoot><tr><td colspan="2" class="aRight">Outstanding &gt; 90:</td><td class="aRight">$' + convert (varchar, cast (@ARTotal as money), 1)+ '</td><td></td><td></td><td></td></tr></tfoot>
				</table>
				<p>Please let me know if I can be of assistance.</p>
				<p>Thank you,</p>'
			
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, BCCList, [Subject], Body, CreateDateTime, MaxDelay)
			--Values ('Collections System', @PAEmail, /*@OMEmail*/ 'canderson@halff.com', /*@TLEmail*/ 'canderson@halff.com', /*@PAEmailAll*/ 'canderson@halff.com', @Subject, CONCAT(@CSS , @Body), getDate(), 0)
			VALUES ('Collections System', /*@PAEmail*/ 'jKubik@Halff.com', @OMEmail, @TLEmail + ';rChampaign@Halff.com' , /* @PAEmailAll + */ 'jKubik@Halff.com;cAnderson@Halff.com' , @Subject, CONCAT(@CSS , @Body), getDate(), 0)
        END
       --WAITFOR	DELAY '00:01:01';
	--	End Team

		FETCH NEXT FROM curTeam INTO @OMFirstName, @OMEmail, @TeamCode, @Team, @TLEmpNo, @TLEmail
		Set @Loop1 = @@FETCH_STATUS

		END
	CLOSE curTeam
	DEALLOCATE curTeam
END
GO
