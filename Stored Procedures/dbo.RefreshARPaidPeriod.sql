SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RefreshARPaidPeriod] AS 
BEGIN
   DECLARE @CheckPWPLevel varchar(1)

   SET NOCOUNT ON

/* add option to check wbs level when looking for the AR balance of the invoice */
/* Store InvBalanceSourceCurrency for invoice balance < 0 so that it can be used to determine PWP payments */

   Select @CheckPWPLevel = CheckPWPLevel From FW_CFGSystem
   UPDATE AR SET AR.PaidPeriod = 999999 WHERE AR.PaidPeriod <> 999999

   IF @CheckPWPLevel = '1' /* Only check to WBS1 level (ignore WBS2/WBS3) */
   BEGIN
		UPDATE AR SET AR.PaidPeriod = CASE WHEN L.Balance BETWEEN -0.00005 AND 0.00005 THEN L.MaxPeriod ELSE AR.PaidPeriod END
		, AR.InvBalanceSourceCurrency = L.Balance 
		From AR Inner Join (
			SELECT LedgerAR.WBS1, N'' AS WBS2,  N'' AS WBS3, LedgerAR.Invoice, Max(LedgerAR.Period) as MaxPeriod,
			SUM(CASE WHEN LedgerAR.TransType='IN' THEN -LedgerAR.AmountSourceCurrency 
			WHEN LedgerAR.TransType = 'CR' AND LedgerAR.SubType = 'T' 
			THEN -LedgerAR.AmountSourceCurrency ELSE LedgerAR.AmountSourceCurrency END) AS Balance 
			FROM LedgerAR
			WHERE ((LedgerAR.TransType = 'IN' AND (LedgerAR.SubType <> 'X' Or LedgerAR.SubType Is Null)) OR
				(LedgerAR.TransType ='CR' AND (LedgerAR.SubType='R' OR LedgerAR.SubType='T'))) 
			AND LedgerAR.AutoEntry = 'N'
			GROUP BY LedgerAR.WBS1, LedgerAR.Invoice
			HAVING SUM(CASE WHEN LedgerAR.TransType='IN' THEN -LedgerAR.AmountSourceCurrency 
			WHEN LedgerAR.TransType = 'CR' AND LedgerAR.SubType = 'T' 
			THEN -LedgerAR.AmountSourceCurrency ELSE LedgerAR.AmountSourceCurrency END) <= 0.00005
		) L on AR.WBS1=L.WBS1 and AR.Invoice=L.Invoice
   END
   ELSE
	IF @CheckPWPLevel = '2'  /* Only check to WBS2 level (ignore WBS3) */
	BEGIN
		UPDATE AR SET AR.PaidPeriod = CASE WHEN L.Balance BETWEEN -0.00005 AND 0.00005 THEN L.MaxPeriod ELSE AR.PaidPeriod END
		, AR.InvBalanceSourceCurrency = L.Balance 
		From AR Inner Join (
			SELECT LedgerAR.WBS1, LedgerAR.WBS2, N'' AS WBS3, LedgerAR.Invoice, Max(LedgerAR.Period) as MaxPeriod,
			SUM(CASE WHEN LedgerAR.TransType='IN' THEN -LedgerAR.AmountSourceCurrency 
			WHEN LedgerAR.TransType = 'CR' AND LedgerAR.SubType = 'T' 
			THEN -LedgerAR.AmountSourceCurrency ELSE LedgerAR.AmountSourceCurrency END) AS Balance 
			FROM LedgerAR
			WHERE ((LedgerAR.TransType = 'IN' AND (LedgerAR.SubType <> 'X' Or LedgerAR.SubType Is Null)) OR
				(LedgerAR.TransType ='CR' AND (LedgerAR.SubType='R' OR LedgerAR.SubType='T'))) 
			AND LedgerAR.AutoEntry = 'N'
			GROUP BY LedgerAR.WBS1, LedgerAR.WBS2, LedgerAR.Invoice
			HAVING SUM(CASE WHEN LedgerAR.TransType='IN' THEN -LedgerAR.AmountSourceCurrency 
			WHEN LedgerAR.TransType = 'CR' AND LedgerAR.SubType = 'T' 
			THEN -LedgerAR.AmountSourceCurrency ELSE LedgerAR.AmountSourceCurrency END) <= 0.00005
		) L on AR.WBS1=L.WBS1 and AR.WBS2=L.WBS2 and AR.Invoice=L.Invoice
	END
	ELSE
	BEGIN /* CheckPWPLevel = 3  Check all WBS level */
		UPDATE AR SET AR.PaidPeriod = CASE WHEN L.Balance BETWEEN -0.00005 AND 0.00005 THEN L.MaxPeriod ELSE AR.PaidPeriod END
		, AR.InvBalanceSourceCurrency = L.Balance 
		From AR Inner Join (
			SELECT LedgerAR.WBS1, LedgerAR.WBS2, LedgerAR.WBS3, LedgerAR.Invoice, Max(LedgerAR.Period) as MaxPeriod,
			SUM(CASE WHEN LedgerAR.TransType='IN' THEN -LedgerAR.AmountSourceCurrency 
			WHEN LedgerAR.TransType = 'CR' AND LedgerAR.SubType = 'T' 
			THEN -LedgerAR.AmountSourceCurrency ELSE LedgerAR.AmountSourceCurrency END) AS Balance 
			FROM LedgerAR
			WHERE ((LedgerAR.TransType = 'IN' AND (LedgerAR.SubType <> 'X' Or LedgerAR.SubType Is Null)) OR
				(LedgerAR.TransType ='CR' AND (LedgerAR.SubType='R' OR LedgerAR.SubType='T'))) 
			AND LedgerAR.AutoEntry = 'N'
			GROUP BY LedgerAR.WBS1, LedgerAR.WBS2, LedgerAR.WBS3, LedgerAR.Invoice
			HAVING SUM(CASE WHEN LedgerAR.TransType='IN' THEN -LedgerAR.AmountSourceCurrency 
			WHEN LedgerAR.TransType = 'CR' AND LedgerAR.SubType = 'T' 
			THEN -LedgerAR.AmountSourceCurrency ELSE LedgerAR.AmountSourceCurrency END) <= 0.00005
		) L on AR.WBS1=L.WBS1 and AR.WBS2=L.WBS2 and AR.WBS3=L.WBS3 and AR.Invoice=L.Invoice
	END
END
GO
