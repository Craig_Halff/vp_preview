SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Check_Vendor_Enabled] (
	@Company		Nvarchar(14)
) AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SET NOCOUNT ON;

	SELECT OriginatingVendorEnabled
		FROM CFGMainData
		WHERE Company = @Company;
END;

GO
