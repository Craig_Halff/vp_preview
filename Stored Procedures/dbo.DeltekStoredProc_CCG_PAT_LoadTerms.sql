SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_LoadTerms] (
	@visionMultiCompany		bit,
	@hasVendor				bit,
	@vendor					Nvarchar(20),
	@company				Nvarchar(14)
) AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_LoadTerms] 1, 1, '', ''
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max);
	DECLARE @hasCompany bit = (CASE WHEN @visionMultiCompany = 1 AND ISNULL(@company, N'') <> N'' THEN 1 ELSE 0 END);

	SET @sql = N'
		SELECT code, description, max(vedef), min(sortorder)
			FROM ( '
	IF @hasVendor = 1
		SET @sql = @sql + '
				SELECT UPPER(payterms) as code,
					case when UPPER(payterms) like ''[0-9]%'' then ''DATE '' + UPPER(payterms)
						else UPPER(payterms) end as description,
					max(case when vendor = N''' + @vendor + '''
							then UPPER(payterms)
						else '''' end) as vedef,
					case when UPPER(payterms) like ''[0-9]%'' then 10 + LEN(UPPER(payterms)) else 1 end as sortorder
					FROM VEAccounting
                    WHERE not payterms is null and vendor = N''' + @vendor + ''' ' +
						(CASE WHEN @hasCompany = 1 THEN ' and VEAccounting.company = N''' + @company + '''' ELSE '' END) + '
					GROUP BY UPPER(payterms)
				UNION ';

    SET @sql = @sql + '
				SELECT ''Date'' as code, ''DATE'' as description, '''' as vedef, 1 as sortorder
				UNION
				SELECT ''Hold'' as code, ''HOLD'' as description, '''' as vedef, 1 as sortorder
				UNION
				SELECT ''Next'' as code, ''NEXT'' as description, '''' as vedef, 1 as sortorder
				UNION
				SELECT ''PWP'' as code, ''PWP'' as description, '''' as vedef, 1 as sortorder
			) vedef
		GROUP BY code, description
		ORDER BY 4, 2';

	PRINT @sql
	EXEC(@sql);
END;

GO
