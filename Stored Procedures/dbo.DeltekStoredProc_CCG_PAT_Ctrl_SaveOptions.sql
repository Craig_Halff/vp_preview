SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Ctrl_SaveOptions] (
	@EMPLOYEEID				Nvarchar(32),
	@description			Nvarchar(50),
	@type					varchar(50),
	@options				Nvarchar(max),
	@seq					int
) AS
BEGIN
	SET NOCOUNT ON;

	IF @seq <= 0 BEGIN		-- this is a new message.
		INSERT INTO CCG_PAT_ConfigOptions (DateChanged, ChangedBy, Description, Type, Detail)
                VALUES (getdate(), @EMPLOYEEID, ISNULL(@description, N''), @type, CAST(ISNULL(@options, N'') AS Nvarchar(max)));

		SELECT SCOPE_IDENTITY();
    END
    ELSE BEGIN -- this is an update
        IF ISNULL(@options, N'') <> N''
            UPDATE CCG_PAT_ConfigOptions
                SET DateChanged = getdate(),
                    ChangedBy = @EMPLOYEEID,
                    Description = ISNULL(@description, N''),
                    Detail = CAST(ISNULL(@options, N'') AS Nvarchar(max))
				WHERE Seq = @seq;
        ELSE -- if no options passed in it is a delete
			DELETE FROM CCG_PAT_ConfigOptions
   				WHERE Seq = @seq;
    END;
END;

GO
