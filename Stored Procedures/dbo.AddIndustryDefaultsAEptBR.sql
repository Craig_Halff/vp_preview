SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AddIndustryDefaultsAEptBR]
AS
BEGIN

PRINT ' '
PRINT 'Adding AE Industry Defaults for pt-BR'

BEGIN TRANSACTION

DELETE FROM BTLaborCatsDescriptions WHERE UICultureName = 'pt-BR';
--..........................................................................................v(50)
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 1, 'pt-BR','Diretor'         WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 1);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 2, 'pt-BR','Gerente de projetos'   WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 2);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 3, 'pt-BR','Consultor sênior' WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 3);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 4, 'pt-BR','Arquiteto sênior'  WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 4);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 5, 'pt-BR','Engenheiro sênior'   WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 5);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 6, 'pt-BR','Engenheiro'          WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 6);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 7, 'pt-BR','Arquiteto'         WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 7);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 8, 'pt-BR','Designer'          WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 8);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 9, 'pt-BR','Relator'      WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 9);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 10,'pt-BR','Técnico'          WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 10);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 11,'pt-BR','Administrativo'    WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 11);
INSERT INTO BTLaborCatsDescriptions (Category,UICultureName,Description) SELECT 12,'pt-BR','Marketing'         WHERE EXISTS (SELECT 'x' FROM BTLaborCatsData WHERE Category = 12);

DELETE FROM CFGActivitySubjectData WHERE UICultureName = 'pt-BR';
--..................................................................................v(50)
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Chamada de vendas'               ,'pt-BR',3 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Reunião inicial'        ,'pt-BR',1 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('E-mail de apresentação'      ,'pt-BR',4 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Chamada de qualificação'      ,'pt-BR',2 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('Menção nas redes sociais'    ,'pt-BR',6 ,'N');
INSERT INTO CFGActivitySubjectData (Subject,UICultureName,Seq,isMilestone) VALUES ('E-mail de liderança em ideias','pt-BR',5 ,'N');

DELETE FROM CFGActivityTypeDescriptions WHERE UICultureName = 'pt-BR';
--.........................................................................................................v(15)
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'EMail'     ,'pt-BR','E-mail'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'EMail');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Event'     ,'pt-BR','Evento'          ,4 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Event');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Mailing'   ,'pt-BR','Correspondência'        ,2 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Mailing');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Meeting'   ,'pt-BR','Reunião'        ,3 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Meeting');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Merge'     ,'pt-BR','Criar documento',5 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Merge');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Milestone' ,'pt-BR','Marco'      ,6 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Milestone');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Phone Call','pt-BR','Chamada telefônica'     ,7 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Phone Call');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Task'      ,'pt-BR','Tarefa'           ,8 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Task');
INSERT INTO CFGActivityTypeDescriptions (Code,UICultureName,ActivityType,Seq) SELECT 'Touchpoint','pt-BR','Ponto de contato'     ,9 WHERE EXISTS (SELECT 'x' FROM CFGActivityTypeData WHERE Code = 'Touchpoint');

DELETE FROM CFGARLedgerHeadingsDescriptions WHERE UICultureName = 'pt-BR';
--...............................................................................................v(12)
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 1,'pt-BR','Tarifas'       WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 1);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 2,'pt-BR','Reemb.'     WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 2);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 3,'pt-BR','Consultor' WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 3);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 4,'pt-BR','Adiantamento'   WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 4);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 5,'pt-BR','Impostos'      WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 5);
INSERT INTO CFGARLedgerHeadingsDescriptions (ReportColumn,UICultureName,Label) SELECT 6,'pt-BR','Juros'   WHERE EXISTS (SELECT 'x' FROM CFGARLedgerHeadingsData WHERE ReportColumn = 6);

DELETE FROM CFGAwardTypeDescriptions WHERE UICultureName = 'pt-BR';
--............................................................................................v(255)
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '1','pt-BR','IDIQ - GWAC'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 1);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '2','pt-BR','IDIQ - Agência específica',2 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 2);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '4','pt-BR','BPA'                   ,3 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 4);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '5','pt-BR','Outro'                 ,4 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 5);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '6','pt-BR','Tarefa / pedido de entrega' ,5 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 6);
INSERT INTO CFGAwardTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '7','pt-BR','Indeterminado'          ,6 WHERE EXISTS (SELECT 'x' FROM CFGAwardTypeData WHERE Code = 7);

DELETE FROM CFGBillMainDescriptions WHERE UICultureName = 'pt-BR';
--..............................................................................................................................................................................................................v(15)..v(15)....v(15).........v(15)......v(15)...v(15).....v(15)..v(15)
INSERT INTO CFGBillMainDescriptions (Company,UICultureName,FooterMessage,FeeLabel,LabLabel,ConLabel,ExpLabel,UnitLabel,AddOnLabel,TaxLabel,InterestLabel,OvtIndicator) SELECT CFGMainData.Company,'pt-BR',NULL,'Tarifa' ,'Mão de obra' ,'Consultor' ,'Despesa' ,'Unidade' ,'Complemento' ,'Imposto' ,'Juros' ,'Ovt' FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGBillMainData WHERE Company = CFGMainData.Company);

DELETE FROM CFGCampaignActionDescriptions WHERE UICultureName = 'pt-BR';
--..................................................................................................v(50)
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '01','pt-BR','Desenvolver conteúdo' ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '01');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '02','pt-BR','Enviar convite' ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '02');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '03','pt-BR','Enviar e-mail informativo',3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '03');
INSERT INTO CFGCampaignActionDescriptions (Code,UICultureName,Description,Seq) SELECT '04','pt-BR','Acompanhamento'       ,4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignActionData WHERE Code = '04');

DELETE FROM CFGCampaignAudienceDescriptions WHERE UICultureName = 'pt-BR';
--....................................................................................................v(50)
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '01','pt-BR','Executivos'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '01');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '02','pt-BR','Finanças'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '02');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '03','pt-BR','Marketing'           ,3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '03');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '04','pt-BR','Desenvolvimento empresarial',4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '04');
INSERT INTO CFGCampaignAudienceDescriptions (Code,UICultureName,Description,Seq) SELECT '05','pt-BR','Gerentes de projetos'    ,5 WHERE EXISTS (SELECT 'x' FROM CFGCampaignAudienceData WHERE Code = '05');

DELETE FROM CFGCampaignObjectiveDescriptions WHERE UICultureName = 'pt-BR';
--.....................................................................................................v(50)
INSERT INTO CFGCampaignObjectiveDescriptions (Code,UICultureName,Description,Seq) SELECT '01','pt-BR','Geração de novos negócios' ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignObjectiveData WHERE Code = '01');
INSERT INTO CFGCampaignObjectiveDescriptions (Code,UICultureName,Description,Seq) SELECT '02','pt-BR','Expandir negócios existentes',2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignObjectiveData WHERE Code = '02');

DELETE FROM CFGCampaignStatusDescriptions WHERE UICultureName = 'pt-BR';
--..................................................................................................v(50)
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','pt-BR','Ativo'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '01');
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','pt-BR','Planejamento' ,2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '02');
INSERT INTO CFGCampaignStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','pt-BR','Concluído',3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignStatusData WHERE Code = '03');

DELETE FROM CFGCampaignTypeDescriptions WHERE UICultureName = 'pt-BR';
--................................................................................................v(50)
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','pt-BR','E-mail'      ,1 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '01');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','pt-BR','Mala direta',2 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '02');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','pt-BR','Webinar'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '03');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','pt-BR','Exposição'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '04');
INSERT INTO CFGCampaignTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '05','pt-BR','Encontro'     ,5 WHERE EXISTS (SELECT 'x' FROM CFGCampaignTypeData WHERE Code = '05');

DELETE FROM CFGClientCurrentStatusDescriptions WHERE UICultureName = 'pt-BR';
--...............................................................................................................v(50)
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Existing','pt-BR','Existente',1 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Existing');
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Former'  ,'pt-BR','Antigo'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Former');
INSERT INTO CFGClientCurrentStatusDescriptions (Code,UICultureName,CurrentStatus,Seq) SELECT 'Prospect','pt-BR','Prospecto',2 WHERE EXISTS (SELECT 'x' FROM CFGClientCurrentStatusData WHERE Code = 'Prospect');

DELETE FROM CFGClientRelationshipDescriptions WHERE UICultureName = 'pt-BR';
--......................................................................................................v(50)
INSERT INTO CFGClientRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01','pt-BR','Concorrência',1 WHERE EXISTS (SELECT 'x' FROM CFGClientRelationshipData WHERE Code = '01');

DELETE FROM CFGClientRoleDescriptions WHERE UICultureName = 'pt-BR';
--....................................................................................................v(50)
INSERT INTO CFGClientRoleDescriptions (Code,UICultureName,Description,Seq) SELECT '01'      ,'pt-BR','Subempreiteiro',2 WHERE EXISTS (SELECT 'x' FROM CFGClientRoleData WHERE Code = '01');
INSERT INTO CFGClientRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'sysOwner','pt-BR','Cliente'       ,1 WHERE EXISTS (SELECT 'x' FROM CFGClientRoleData WHERE Code = 'sysOwner');

DELETE FROM CFGClientTypeDescriptions WHERE UICultureName = 'pt-BR';
--..............................................................................................v(50)
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','pt-BR','Educação'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '01');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','pt-BR','Empresa'          ,2 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '02');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','pt-BR','Financeiro e Bancário' ,3 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '03');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','pt-BR','Saúde e Bem-estar' ,4 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '04');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '05','pt-BR','Hospitalidade'         ,5 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '05');
INSERT INTO CFGClientTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '06','pt-BR','Serviços de informação',6 WHERE EXISTS (SELECT 'x' FROM CFGClientTypeData WHERE Code = '06');

DELETE FROM CFGCompetitionTypeDescriptions WHERE UICultureName = 'pt-BR';
--...................................................................................................v(255)
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '37','pt-BR','Reserva de 8(a)'                                       ,2  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '37')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '38','pt-BR','Reserva de SDB'                                        ,3  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '38')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '39','pt-BR','Reserva de pequena empresa'                                  ,4  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '39')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '40','pt-BR','Completo e aberto/irrestrito'                         ,5  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '40')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '44','pt-BR','Hubzone'                                              ,6  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '44')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '45','pt-BR','Pequena empresa de propriedade de ex-combatente afastado por invalidez'        ,7  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '45')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '47','pt-BR','Indeterminado'                                         ,1  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '47')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '48','pt-BR','Outro'                                                ,8  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '48')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '64','pt-BR','Fornecedor exclusivo'                                          ,9  WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '64')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '65','pt-BR','Pequena empresa de propriedade de mulher da reserva'                 ,10 WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '65')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '66','pt-BR','Pequena empresa de propriedade de ex-combatente'                         ,11 WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '66')
INSERT INTO CFGCompetitionTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '67','pt-BR','Pequena empresa de propriedade de mulheres economicamente desfavorecidas',12 WHERE EXISTS (SELECT 'x' FROM CFGCompetitionTypeData WHERE Code = '67')

DELETE FROM CFGContactRelationshipDescriptions WHERE UICultureName = 'pt-BR';
--.......................................................................................................v(50)
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01','pt-BR','Antigo cliente'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '01');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '02','pt-BR','Antigo colega de trabalho' ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '02');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '03','pt-BR','Empresas similares do setor'   ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '03');
INSERT INTO CFGContactRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '04','pt-BR','Contato pessoal',4 WHERE EXISTS (SELECT 'x' FROM CFGContactRelationshipData WHERE Code = '04');

DELETE FROM CFGContactRoleDescriptions WHERE UICultureName = 'pt-BR';
--.......................................................................................................v(50)
INSERT INTO CFGContactRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Contractor','pt-BR','Subempreiteiro',1 WHERE EXISTS (SELECT 'x' FROM CFGContactRoleData WHERE Code = 'Contractor');
INSERT INTO CFGContactRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'sysOwner'  ,'pt-BR','Proprietário'        ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactRoleData WHERE Code = 'sysOwner');

DELETE FROM CFGContactSourceDescriptions WHERE UICultureName = 'pt-BR';
--...................................................................................................v(50)
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '01','pt-BR','Referência do cliente'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '01');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '02','pt-BR','Evento'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '02');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '03','pt-BR','Lista'              ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '03');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '04','pt-BR','Contato pessoal'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '04');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '05','pt-BR','Redes sociais'      ,5 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '05');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '06','pt-BR','Liderança em ideias',6 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '06');
INSERT INTO CFGContactSourceDescriptions (Code,UICultureName,ContactSource,Seq) SELECT '07','pt-BR','Site'           ,7 WHERE EXISTS (SELECT 'x' FROM CFGContactSourceData WHERE Code = '07');

DELETE FROM CFGContactTitleDescriptions WHERE UICultureName = 'pt-BR';
--.................................................................................................v(50)
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Director' ,'pt-BR','Diretor' ,2 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Director');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Executive','pt-BR','Executivo',1 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Executive');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Finance'  ,'pt-BR','Finanças'  ,5 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Finance');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Marketing','pt-BR','Marketing',6 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Marketing');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Sales'    ,'pt-BR','Vendas'    ,4 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'Sales');
INSERT INTO CFGContactTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'VP'       ,'pt-BR','Vice-presidente'       ,3 WHERE EXISTS (SELECT 'x' FROM CFGContactTitleData WHERE Code = 'VP');

DELETE FROM CFGContactTypeDescriptions WHERE UICultureName = 'pt-BR';
--..........................................................................................v(100)
INSERT INTO CFGContactTypeDescriptions (Code,UICultureName,Description) SELECT 'C','pt-BR','Cliente' WHERE EXISTS (SELECT 'x' FROM CFGContactTypeData WHERE Code = 'C');

DELETE FROM CFGContractStatusDescriptions WHERE UICultureName = 'pt-BR';
--..................................................................................................v(50)
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','pt-BR','Revisão interna'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '01');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','pt-BR','Enviado ao cliente'   ,2 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '02');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','pt-BR','Negociação'      ,3 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '03');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '04','pt-BR','Revisão jurídica'     ,4 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '04');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '05','pt-BR','Assinado e executado',5 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '05');
INSERT INTO CFGContractStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '06','pt-BR','Perdido/não fechado'     ,6 WHERE EXISTS (SELECT 'x' FROM CFGContractStatusData WHERE Code = '06');

DELETE FROM CFGContractTypeDescriptions WHERE UICultureName = 'pt-BR';
--................................................................................................v(50)
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','pt-BR','Original'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '01');
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','pt-BR','Alterar pedido'       ,2 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '02');
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','pt-BR','Serviços adicionais',3 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '03');
INSERT INTO CFGContractTypeDescriptions (Code,UICultureName,Description,Seq) SELECT '04','pt-BR','Memorando de acordo',4 WHERE EXISTS (SELECT 'x' FROM CFGContractTypeData WHERE Code = '04');

DELETE FROM CFGCubeTranslationDescriptions WHERE UICultureName = 'pt-BR';
--..............................................................................................................................................v(200)
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'U'                       ,'pt-BR','Desconhecido');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'UM'                      ,'pt-BR','Mês desconhecido');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('General'        ,'H'                       ,'pt-BR','Histórico');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('YesNo'          ,'Y'                       ,'pt-BR','Sim');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('YesNo'          ,'N'                       ,'pt-BR','Não');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('Status'         ,'A'                       ,'pt-BR','Ativo');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('Status'         ,'I'                       ,'pt-BR','Inativo');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'1-Asset'                 ,'pt-BR','Ativo');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'2-Liability'             ,'pt-BR','Passivo');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'3-NetWorth'              ,'pt-BR','Patrimônio líquido');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'4-Revenue'               ,'pt-BR','Receita');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'5-Reimbursable'          ,'pt-BR','Reembolsável');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'6-ReimbursableConsultant','pt-BR','Consultor reembolsável');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'7-Direct'                ,'pt-BR','Direto');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'8-DirectConsultant'      ,'pt-BR','Consultor direto');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'9-Indirect'              ,'pt-BR','Indireto');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'10-OtherCharges'         ,'pt-BR','Outros encargos');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountType'    ,'U-Unknown'               ,'pt-BR','Desconhecido');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ExpenseType'    ,'5-ReimbursableOther'     ,'pt-BR','Outro reembolsável');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ExpenseType'    ,'7-DirectOther'           ,'pt-BR','Outro direto');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ReportType'     ,'BalanceSheet'            ,'pt-BR','Balanço patrimonial');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('ReportType'     ,'IncomeStatement'         ,'pt-BR','Declaração de renda');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'1-Asset','pt-BR'         ,'Other Assets');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'2-Liability'             ,'pt-BR','Outros passivos');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'3-NetWorth'              ,'pt-BR','Outro patrimônio líquido');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'4-Revenue'               ,'pt-BR','Outra receita');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'5-Reimbursable'          ,'pt-BR','Outro reembolsável');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'6-Reimbursable'          ,'pt-BR','Outro reembolsável');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'7-Direct'                ,'pt-BR','Outro direto');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'8-Direct'                ,'pt-BR','Outro direto');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'9-Indirect'              ,'pt-BR','Outro indireto');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('AccountGroup'   ,'10-OtherCharges'         ,'pt-BR','Outros encargos');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'B'                       ,'pt-BR','Faturável');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'H'                       ,'pt-BR','Retido');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'W'                       ,'pt-BR','Para dar baixa');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'X'                       ,'pt-BR','Com baixa');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'D'                       ,'pt-BR','Marcado para exclusão');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'N'                       ,'pt-BR','Não cobrável');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'F'                       ,'pt-BR','Final cobrado');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'T'                       ,'pt-BR','Transferido');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'R'                       ,'pt-BR','Parcialmente retido/liberado');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'M'                       ,'pt-BR','Modificado');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('BillingStatus'  ,'O'                       ,'pt-BR','Excluído');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AP'                      ,'pt-BR','Comprovantes de CP');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BA'                      ,'pt-BR','Processamento de horas de benefício');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BE'                      ,'pt-BR','Transferências de despesas de faturamento');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BL'                      ,'pt-BR','Transferências de mão de obra de faturamento');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','BU'                      ,'pt-BR','Transferências de [Unit] de faturamento');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CD'                      ,'pt-BR','Desembolsos de caixa');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CP'                      ,'pt-BR','Converter [WBS1] do [Organization]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CR'                      ,'pt-BR','Recebimentos de caixa');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CV'                      ,'pt-BR','Desembolsos de CP');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EP'                      ,'pt-BR','Processamento de pagamento do [Employee]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','ER'                      ,'pt-BR','Reembolsos de [Employee]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EX'                      ,'pt-BR','Despesas de [Employee]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IN'                      ,'pt-BR','Faturas');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','JE'                      ,'pt-BR','Lançamentos');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','KC'                      ,'pt-BR','Conversão chave');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','LA'                      ,'pt-BR','Ajustes de mão de obra');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','LG'                      ,'pt-BR','Ganhos/perdas e reavaliações');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','MI'                      ,'pt-BR','Despesas diversas');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PP'                      ,'pt-BR','Processamento de pagamento de CP');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PR'                      ,'pt-BR','Impressões e reproduções');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PY'                      ,'pt-BR','Processamento de folha de pagamento');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','RG'                      ,'pt-BR','Geração de receita');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','TS'                      ,'pt-BR','Controles de ponto');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','UN'                      ,'pt-BR','[Units]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','UP'                      ,'pt-BR','[Units] por [WBS1]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AL'                      ,'pt-BR','Lançamento de conversão de chaves, abertura de um novo período/W2 trimestre/ano de provisão de benefício, encerramento de um período');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','AX'                      ,'pt-BR','Comprovante convertido de Contas a pagar');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CN'                      ,'pt-BR','Despesas convertidas dos consultores');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','CT'                      ,'pt-BR','Transação indicando quando a conversão de uma liberação anterior foi executada');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','EC'                      ,'pt-BR','Ajuste(s) de análise de tempo convertido(s)');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IH'                      ,'pt-BR','Fatura convertida');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','IX'                      ,'pt-BR','Fatura(s) convertida(s)');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','JX'                      ,'pt-BR','Conversão de MCFMS');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','PX'                      ,'pt-BR','Processamento de pagamento de Contas a pagar convertidas');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','RX'                      ,'pt-BR','Conversão de MCFMS');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','XD'                      ,'pt-BR','Dados da conta convertida');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','XE'                      ,'pt-BR','Despesas [WBS1] convertidas');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HA'                      ,'pt-BR','Histórico de saldos da [Account]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HB'                      ,'pt-BR','Histórico de provisão de benefício do [Employee]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HE'                      ,'pt-BR','Histórico de mão de obra e despesa do [WBS1]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HL'                      ,'pt-BR','Histórico de mão de obra e despesa do [WBS1]');
INSERT INTO CFGCubeTranslationDescriptions (Usage,Code,UICultureName,Description) VALUES ('TransactionType','HU'                      ,'pt-BR','Receita não cobrada');

DELETE FROM CFGEMDegreeDescriptions WHERE UICultureName = 'pt-BR';
--............................................................................................v(50)
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '01','pt-BR','Bacharel em Ciências',1 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '01');
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '02','pt-BR','Bacharel em Artes'   ,2 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '02');
INSERT INTO CFGEMDegreeDescriptions (Code,UICultureName,Description,Seq) SELECT '03','pt-BR','Mestre'            ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMDegreeData WHERE Code = '03');

DELETE FROM CFGEmployeeRelationshipDescriptions WHERE UICultureName = 'pt-BR';
--..............................................................................................................v(50)
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '01'      ,'pt-BR','Antigo empregador'            ,3 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '01');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '02'      ,'pt-BR','Antigo colega de trabalho'            ,2 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '02');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '03'      ,'pt-BR','Contato pessoal'           ,5 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '03');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT '04'      ,'pt-BR','Conexão geral do setor',4 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = '04');
INSERT INTO CFGEmployeeRelationshipDescriptions (Code,UICultureName,Description,Seq) SELECT 'SysOwner','pt-BR','Proprietário'                      ,1 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRelationshipData WHERE Code = 'SysOwner');

DELETE FROM CFGEmployeeRoleDescriptions WHERE UICultureName = 'pt-BR';
--........................................................................................................v(50)
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Admin'     ,'pt-BR','Administrativo'         ,7  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Admin');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'AirQuality','pt-BR','Especialista em qualidade do ar' ,8  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'AirQuality');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Architect' ,'pt-BR','Arquiteto'              ,9  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Architect');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Associate' ,'pt-BR','Associado'              ,10 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Associate');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Biologist' ,'pt-BR','Biólogo'              ,11 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Biologist');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'CADDTechi' ,'pt-BR','Técnico de CADD'        ,12 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'CADDTechi');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'CivilEngr' ,'pt-BR','Engenheiro civil'         ,13 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'CivilEngr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ConstIns'  ,'pt-BR','Inspetor de construção' ,14 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ConstIns');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ConstMgr'  ,'pt-BR','Gerente de obra'   ,15 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ConstMgr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ElectEngr' ,'pt-BR','Engenheiro elétrico'    ,16 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ElectEngr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'EntitleSpl','pt-BR','Especialista em direito' ,17 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'EntitleSpl');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'EnvirPlnr' ,'pt-BR','Planejador ambiental'  ,18 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'EnvirPlnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Estimator' ,'pt-BR','Avaliador'              ,19 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Estimator');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'GISSpecial','pt-BR','Especialista em GIS'         ,20 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'GISSpecial');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Hydrolgst' ,'pt-BR','Hidrólogo'            ,21 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Hydrolgst');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'InfoTech'  ,'pt-BR','Tecnologia da informação' ,22 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'InfoTech');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'InteDsnr'  ,'pt-BR','Designer de interiores'      ,23 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'InteDsnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'LandArch'  ,'pt-BR','Arquiteto paisagista'    ,24 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'LandArch');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'MechEng'   ,'pt-BR','Engenheiro mecânico'    ,25 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'MechEng');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'MEPCoordin','pt-BR','Coordenador de MEP'        ,26 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'MEPCoordin');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Planner'   ,'pt-BR','Planejador'                ,27 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Planner');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjAdmin' ,'pt-BR','Administrador de projetos'  ,28 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjAdmin');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjArch'  ,'pt-BR','Arquiteto de projetos'      ,29 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjArch');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjDsnr'  ,'pt-BR','Designer de projetos'       ,30 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjDsnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjEng'   ,'pt-BR','Engenheiro de projetos'       ,31 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjEng');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjPlnr'  ,'pt-BR','Planejador de projetos'        ,32 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjPlnr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'ProjSvor'  ,'pt-BR','Técnico de projetos'       ,33 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'ProjSvor');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'QAQCMgr'   ,'pt-BR','Gerente de QA/QC'          ,34 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'QAQCMgr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'SpecWriter','pt-BR','Redator de especificações'  ,35 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'SpecWriter');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'StructEng' ,'pt-BR','Engenheiro estrutural'    ,36 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'StructEng');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'Surveyor'  ,'pt-BR','Técnico'               ,37 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'Surveyor');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TechEditor','pt-BR','Editor técnico'       ,38 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TechEditor');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TrafficSpe','pt-BR','Especialista em tráfego'     ,39 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TrafficSpe');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TransEngr' ,'pt-BR','Engenheiro de transportes',40 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TransEngr');
INSERT INTO CFGEmployeeRoleDescriptions (Code,UICultureName,Description,Seq) SELECT 'TransPlnr' ,'pt-BR','Planejador de transportes' ,41 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeRoleData WHERE Code = 'TransPlnr');

DELETE FROM CFGEmployeeTitleDescriptions WHERE UICultureName = 'pt-BR';
--..................................................................................................v(50)
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Architect','pt-BR','Arquiteto'              ,1  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Architect');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CFO'      ,'pt-BR','Diretor Executivo Financeiro',2  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CFO');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'CivilEng' ,'pt-BR','Engenheiro civil'         ,3  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'CivilEng');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Designer' ,'pt-BR','Designer'               ,4  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Designer');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Director' ,'pt-BR','Diretor'               ,5  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Director');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'EE'       ,'pt-BR','Engenheiro ambiental' ,8  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'EE');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'EIT'      ,'pt-BR','Engenheiro em treinamento'   ,7  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'EIT');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Engineer' ,'pt-BR','Engenheiro'               ,6  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Engineer');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'GE'       ,'pt-BR','Engenheiro geotécnico'  ,10 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'GE');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'GM'       ,'pt-BR','Gerente geral'        ,9  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'GM');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'ID'       ,'pt-BR','Designer de interiores'      ,12 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'ID');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'LA'       ,'pt-BR','Arquiteto paisagista'    ,13 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'LA');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'PIC'      ,'pt-BR','Diretor encarregado'    ,17 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'PIC');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Planner'  ,'pt-BR','Planejador'                ,14 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Planner');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'PM'       ,'pt-BR','Gerente de projetos'        ,18 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'PM');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'President','pt-BR','Presidente'              ,15 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'President');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'Principal','pt-BR','Diretor'              ,16 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'Principal');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'SUP'      ,'pt-BR','Supervisor'            ,20 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'SUP');
INSERT INTO CFGEmployeeTitleDescriptions (Code,UICultureName,Title,Seq) SELECT 'TC'       ,'pt-BR','Consultor em treinamento'    ,19 WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTitleData WHERE Code = 'TC');

DELETE FROM CFGEmployeeTypeDescriptions WHERE UICultureName = 'pt-BR';
--.....................................................................................v(30)
INSERT INTO CFGEmployeeTypeDescriptions (Type,UICultureName,Label) SELECT 'E','pt-BR','Funcionário'  WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTypeData WHERE Type = 'E');
INSERT INTO CFGEmployeeTypeDescriptions (Type,UICultureName,Label) SELECT 'P','pt-BR','Diretor' WHERE EXISTS (SELECT 'x' FROM CFGEmployeeTypeData WHERE Type = 'P');

DELETE FROM CFGEMRegistrationDescriptions WHERE UICultureName = 'pt-BR';
--...................................................................................................v(70)
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'AIA','pt-BR','Instituto Americano de Arquitetos',2 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'AIA');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'EIT','pt-BR','Engenheiro em treinamento'            ,5 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'EIT');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'PE' ,'pt-BR','Engenheiro profissional'           ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'PE');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'PLS','pt-BR','Topógrafo agrário profissional'      ,4 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'PLS');
INSERT INTO CFGEMRegistrationDescriptions (Code,UICultureName,Description,Seq) SELECT 'RLS','pt-BR','Topógrafo agrário registrado'        ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMRegistrationData WHERE Code = 'RLS');

DELETE FROM CFGEMSkillDescriptions WHERE UICultureName = 'pt-BR';
--.............................................................................................v(70)
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '01'  ,'pt-BR','Administrativo'                          ,1  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '01SF','pt-BR','Engenheiro acústico'                     ,2  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '02'  ,'pt-BR','Arquitetos'                              ,3  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '02');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '03SF','pt-BR','Fotógrafo aéreo'                     ,4  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '03SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '04SF','pt-BR','Engenheiro aeronáutico'                   ,5  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '04SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '05'  ,'pt-BR','Projetistas'                               ,6  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '05');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '05SF','pt-BR','Arqueólogo'                            ,7  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '05SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '07'  ,'pt-BR','Fiscal de obras'                 ,8  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '07');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '07SF','pt-BR','Biólogo'                               ,9  WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '07SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '08'  ,'pt-BR','Avaliadores'                              ,10 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '08');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '08SF','pt-BR','Técnico de CADD'                         ,11 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '08SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '09SF','pt-BR','Cartógrafo'                            ,12 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '09SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '10'  ,'pt-BR','Engenheiros sanitários'                      ,13 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '10');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '11'  ,'pt-BR','Economistas'                              ,14 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '11');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '11SF','pt-BR','Químico'                                 ,15 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '11SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '12'  ,'pt-BR','Ecologistas'                              ,16 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '12');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '13SF','pt-BR','Engenheiro de comunicações'                 ,17 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '13SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '14'  ,'pt-BR','Engenheiros químicos'                      ,18 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '14');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '14SF','pt-BR','Programador de computador'                     ,19 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '14SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '15'  ,'pt-BR','Engenheiros civis'                         ,20 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '15');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '16SF','pt-BR','Gerente de obra'                    ,21 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '16SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '17SF','pt-BR','Engenheiro de corrosão'                      ,22 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '17SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '18SF','pt-BR','Engenheiro/Avaliador de custos'                 ,23 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '18SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '19'  ,'pt-BR','Engenheiros eletricistas'                    ,24 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '19');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '21'  ,'pt-BR','Peritos'                               ,25 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '21');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '22SF','pt-BR','Engenheiro eletrônico'                    ,26 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '22SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '23SF','pt-BR','Engenheiro ambiental'                  ,27 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '23SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '24SF','pt-BR','Cientista ambiental'                 ,28 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '24SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '25SF','pt-BR','Engenheiro de proteção contra incêndios'                ,29 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '25SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '26SF','pt-BR','Engenheiro forense'                       ,30 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '26SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '27'  ,'pt-BR','Engenheiros mecânicos'                    ,31 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '27');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '27SF','pt-BR','Engenheiro de fundações/geotécnico'        ,32 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '27SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '28'  ,'pt-BR','Engenheiros de minas'                        ,33 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '28');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '28SF','pt-BR','Perito geodésico'                       ,34 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '28SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '29SF','pt-BR','Especialista em sistemas de informação geográfica',35 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '29SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '30'  ,'pt-BR','Engenheiros de solos'                         ,36 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '30');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '31'  ,'pt-BR','Escritores de especificações'                  ,37 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '31');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '31SF','pt-BR','Planejador em instituições de saúde'                 ,38 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '31SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '32'  ,'pt-BR','Engenheiros estruturais'                    ,39 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '32');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '32SF','pt-BR','Engenheiro hidráulico'                      ,40 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '32SF');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '33'  ,'pt-BR','Engenheiros de transportes'                ,41 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '33SF','pt-BR','Perito hidrográfico'                   ,42 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '35SF','pt-BR','Engenheiro industrial'                     ,43 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '36'  ,'pt-BR','Geólogo'                               ,44 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '36SF','pt-BR','Higienista industrial'                    ,45 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '38SF','pt-BR','Topógrafo'                           ,46 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '40'  ,'pt-BR','Hidrólogos'                            ,47 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '40SF','pt-BR','Engenheiro de materiais'                      ,48 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '41SF','pt-BR','Engenheiro de manipulação de materiais'             ,49 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '42'  ,'pt-BR','Designers de interiores'                      ,50 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '43'  ,'pt-BR','Arquitetos paisagistas'                    ,51 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '44'  ,'pt-BR','Oceanógrafos'                          ,52 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '45'  ,'pt-BR','Planejadores: Urbano/Regional'                ,53 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '45SF','pt-BR','Intérprete de fotos'                       ,54 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '46SF','pt-BR','Fotogrametrista'                        ,55 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '48SF','pt-BR','Gerente de projetos'                         ,56 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '49SF','pt-BR','Especialista em sensoriamento remoto'               ,57 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '50SF','pt-BR','Avaliador de riscos'                           ,58 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '51SF','pt-BR','Engenheiro de segurança/saúde ocupacional'     ,59 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '53SF','pt-BR','Programador'                               ,60 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '54SF','pt-BR','Especialista de segurança'                     ,61 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '58SF','pt-BR','Técnico/analista'                      ,62 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '59SF','pt-BR','Toxicologista'                            ,63 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '61SF','pt-BR','Engenheiro de valor'                          ,64 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');
INSERT INTO CFGEMSkillDescriptions (Code,UICultureName,Description,Seq) SELECT '62SF','pt-BR','Engenheiro de recursos hídricos'                ,65 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillData WHERE Code = '01');

DELETE FROM CFGEMSkillLevelDescriptions WHERE UICultureName = 'pt-BR';
--.............................................................................................v(50)
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 1,'pt-BR','Básico'   ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 1);
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 2,'pt-BR','Avançado',2 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 2);
INSERT INTO CFGEMSkillLevelDescriptions (Code,UICultureName,Description,Seq) SELECT 3,'pt-BR','Perito'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillLevelData WHERE Code = 3);

DELETE FROM CFGEMSkillUsageDescriptions WHERE UICultureName = 'pt-BR';
--.............................................................................................v(255)
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 1,'pt-BR','Entrada'        ,1 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 1);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 2,'pt-BR','1 a 2 anos'    ,2 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 2);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 3,'pt-BR','3 a 5 anos'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 3);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 4,'pt-BR','6 a 10 anos'   ,4 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 4);
INSERT INTO CFGEMSkillUsageDescriptions (Code,UICultureName,Description,Seq) SELECT 5,'pt-BR','Mais de 10 anos',5 WHERE EXISTS (SELECT 'x' FROM CFGEMSkillUsageData WHERE Code = 5);

DELETE FROM CFGLeadRatingDescriptions WHERE UICultureName = 'pt-BR';
--..............................................................................................v(50)
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '01','pt-BR','Quente' ,1 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '01');
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '02','pt-BR','Morno',2 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '02');
INSERT INTO CFGLeadRatingDescriptions (Code,UICultureName,Description,Seq) SELECT '03','pt-BR','Frio',3 WHERE EXISTS (SELECT 'x' FROM CFGLeadRatingData WHERE Code = '03');

DELETE FROM CFGOpportunityClosedReasonDescriptions WHERE UICultureName = 'pt-BR';
--...........................................................................................................v(50)
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '01','pt-BR','Preço'    ,1 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '01');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '02','pt-BR','Serviços' ,2 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '02');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '03','pt-BR','Recursos',3 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '03');
INSERT INTO CFGOpportunityClosedReasonDescriptions (Code,UICultureName,Description,Seq) SELECT '04','pt-BR','Tempo'   ,4 WHERE EXISTS (SELECT 'x' FROM CFGOpportunityClosedReasonData WHERE Code = '04');

DELETE FROM CFGOpportunitySourceDescriptions WHERE UICultureName = 'pt-BR';
--...........................................................................................................v(50)
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'CRef'    ,'pt-BR','Referência do cliente'          ,1 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'CRef');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Event'   ,'pt-BR','Evento'                     ,2 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Event');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'List'    ,'pt-BR','Lista'                      ,5 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'List');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'PersCont','pt-BR','Contato pessoal'          ,6 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'PersCont');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Social'  ,'pt-BR','Redes sociais'              ,7 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Social');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'TLead'   ,'pt-BR','Liderança em ideias'        ,8 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'TLead');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'Web'     ,'pt-BR','Web'                       ,9 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'Web');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'WIQ'     ,'pt-BR','GovWin IQ'                 ,3 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'WIQ');
INSERT INTO CFGOpportunitySourceDescriptions (Code,UICultureName,Description,Seq) SELECT 'WOM'     ,'pt-BR','Gerente de oportunidade GovWin',4 WHERE EXISTS (SELECT 'x' FROM CFGOpportunitySourceData WHERE Code = 'WOM');

DELETE FROM CFGPrefixDescriptions WHERE UICultureName = 'pt-BR';
--........................................................................................v(10)
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Dr.'  ,'pt-BR','Dr.'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Dr.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Miss' ,'pt-BR','Senhorita' ,2 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Miss');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Mr.'  ,'pt-BR','Sr.'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Mr.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Mrs.' ,'pt-BR','Sra.' ,4 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Mrs.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Ms.'  ,'pt-BR','Sra.'  ,5 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Ms.');
INSERT INTO CFGPrefixDescriptions (Code,UICultureName,Prefix,Seq) SELECT 'Prof.','pt-BR','Prof.',6 WHERE EXISTS (SELECT 'x' FROM CFGPrefixData WHERE Code = 'Prof.');

DELETE FROM CFGProbabilityDescriptions WHERE UICultureName = 'pt-BR';
--.....................................................................................................v(50)
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 5  ,'pt-BR','05' ,1  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 5);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 10 ,'pt-BR','10' ,2  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 10);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 20 ,'pt-BR','20' ,3  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 20);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 30 ,'pt-BR','30' ,4  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 30);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 40 ,'pt-BR','40' ,5  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 40);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 50 ,'pt-BR','50' ,6  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 50);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 60 ,'pt-BR','60' ,7  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 60);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 70 ,'pt-BR','70' ,8  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 70);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 80 ,'pt-BR','80' ,9  WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 80);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 90 ,'pt-BR','90' ,10 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 90);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 95 ,'pt-BR','95' ,11 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 95);
INSERT INTO CFGProbabilityDescriptions (Probability,UICultureName,Description,Seq) SELECT 100,'pt-BR','100',12 WHERE EXISTS (SELECT 'x' FROM CFGProbabilityData WHERE Probability = 100);

DELETE FROM CFGProjectCodeDescriptions WHERE UICultureName = 'pt-BR';
--.......................................................................................................v(100)
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '001','pt-BR','Acústica; redução de ruído'                                                                    ,1   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '001');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '002','pt-BR','Fotografia aérea; dados aerotransportados e coleta e análise de imagens'                         ,2   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '002');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '003','pt-BR','Desenvolvimento agrícola; armazenamento de grãos; mecanização agrícola'                                   ,3   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '003');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '004','pt-BR','Controle de poluição do ar'                                                                         ,4   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '004');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '005','pt-BR','Aeroportos; auxílios à navegação; iluminação aeroportuária; abastecimento de aeronaves'                                         ,5   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '005');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '006','pt-BR','Aeroportos; terminais; e suspensores; manuseio de cargas'                                              ,6   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '006');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '007','pt-BR','Instalações articuladas'                                                                             ,7   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '007');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '008','pt-BR','Auditórios e teatros'                                                                        ,8   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '008');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '009','pt-BR','Automação; controles; instrumentação'                                                         ,9   WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '009');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '010','pt-BR','Alojamentos; dormitórios'                                                                         ,10  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '010');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '011','pt-BR','Pontes'                                                                                       ,11  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '011');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '012','pt-BR','Cemitérios (planejamento e relocação)'                                                            ,12  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '012');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '013','pt-BR','Processamento e armazenamento de produtos químicos'                                                                 ,13  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '013');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '014','pt-BR','Igrejas; capelas'                                                                             ,14  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '014');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '015','pt-BR','Códigos; padrões; portarias'                                                                  ,15  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '015');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '016','pt-BR','Armazenamento frio; refrigeração; congelamento rápido'                                                      ,16  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '016');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '017','pt-BR','Prédio comercial; (prédios com poucos andares); centros comerciais'                                             ,17  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '017');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '018','pt-BR','Sistemas de comunicações; televisão; microondas'                                                         ,18  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '018');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '019','pt-BR','Instalações de computador; serviço de informática'                                                         ,19  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '019');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '020','pt-BR','Conservação e gerenciamento de recursos'                                                          ,20  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '020');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '021','pt-BR','Gerenciamento de construções'                                                                       ,21  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '021');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '022','pt-BR','Controle de corrosão; proteção catódica; eletrólise'                                          ,22  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '022')
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '023','pt-BR','Estimativa de custos; engenharia e análise de custos; custo paramétrico; previsão'               ,23  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '023');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '024','pt-BR','Barragens (concreto; arco)'                                                                         ,24  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '024');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '025','pt-BR','Barragens (terra; rocha); diques'                                                             ,25  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '025');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '026','pt-BR','Dessalinização (processo e instalações)'                                                         ,26  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '026');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '027','pt-BR','Salões de jantar; clubes; restaurantes'                                                              ,27  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '027');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '028','pt-BR','Investigações ecológicas e arqueológicas'                                                     ,28  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '028');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '029','pt-BR','Instalações educacionais; salas de aula'                                                            ,29  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '029');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '030','pt-BR','Eletrônicos'                                                                                   ,30  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '030');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '031','pt-BR','Elevadores; escadas rolantes; transporte'                                                          ,31  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '031');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '032','pt-BR','Conservação de energia; novas fontes de energia'                                                       ,32  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '032');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '033','pt-BR','Estudos de impacto ambiental, avaliações ou declarações'                                       ,33  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '033');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '034','pt-BR','Abrigos de partículas radiativas liberadas em explosão nuclear; design resistente a explosões'                                                      ,34  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '034');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '035','pt-BR','Casas de campo; ginásios; estádios'                                                            ,35  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '035');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '036','pt-BR','Proteção contra incêndios'                                                                               ,36  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '036');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '037','pt-BR','Pescas; escadas passa peixe'                                                                       ,37  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '037');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '038','pt-BR','Produtos florestais e de silvicultura'                                                                    ,38  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '038');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '039','pt-BR','Garagens; instalações de manutenção de veículos; plataformas de estacionamento'                                       ,39  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '039');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '040','pt-BR','Sistemas de gás (propano, natural, etc.)'                                                          ,40  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '040');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '041','pt-BR','Design gráfico'                                                                                ,41  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '041');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '042','pt-BR','Portos; quebra-mar; cais; instalações do terminal marítimo'                                             ,42  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '042');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '043','pt-BR','Aquecimento, ventilação, ar condicionado'                                                        ,43  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '043');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '044','pt-BR','Planejamento de sistemas de saúde'                                                                       ,44  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '044');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '045','pt-BR','Arranha-céus; edifícios com direitos aéreos'                                                           ,45  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '045');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '046','pt-BR','Rodovias; ruas; pavimentação de campos de pouso; estacionamentos'                                              ,46  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '046');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '047','pt-BR','Preservação histórica'                                                                       ,47  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '047');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '048','pt-BR','Hospitais e instalações médicas'                                                                ,48  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '048');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '049','pt-BR','Hotéis; motéis'                                                                                ,49  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '049');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '050','pt-BR','Moradias (residenciais, multifamiliares, apartamentos, condomínios'                                   ,50  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '050');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '051','pt-BR','Hidráulica e pneumática'                                                                       ,51  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '051');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '052','pt-BR','Edifícios industriais; plantas de fabricação'                                                    ,52  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '052');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '053','pt-BR','Processo industrial; controle de qualidade'                                                         ,53  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '053');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '054','pt-BR','Tratamento de resíduos industriais'                                                                     ,54  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '054');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '055','pt-BR','Design de interiores; planejamento de espaço'                                                               ,55  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '055');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '056','pt-BR','Irrigação; drenagem'                                                                          ,56  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '056');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '057','pt-BR','Instalações judiciais e de tribunal'                                                             ,57  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '057');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '058','pt-BR','Laboratórios; instalações de pesquisa médica'                                                      ,58  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '058');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '059','pt-BR','Arquitetura paisagística'                                                                        ,59  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '059');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '060','pt-BR','Bibliotecas; museus; galerias'                                                                 ,60  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '060');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '061','pt-BR','Iluminação (interior; displays; teatros; etc.)'                                                 ,61  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '061');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '062','pt-BR','Iluminação (exterior; rua; memoriais; campos de atletismo)'                                      ,62  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '062');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '063','pt-BR','Sistemas de manipulação de materiais; esteiras; classificadores'                                                ,63  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '063');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '064','pt-BR','Metalurgia'                                                                                    ,64  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '064');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '065','pt-BR','Microclimatologia; engenharia tropical'                                                        ,65  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '065');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '066','pt-BR','Padrões de design militar'                                                                     ,66  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '066');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '067','pt-BR','Mineração e mineralogia'                                                                         ,67  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '067');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '068','pt-BR','Instalações de mísseis (silos; combustíveis; transportes)'                                                  ,68  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '068');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '069','pt-BR','Design de sistemas modulares; estruturas ou componentes pré-fabricados'                                      ,69  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '069');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '070','pt-BR','Arquitetura naval; plataformas marítimas'                                                       ,70  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '070');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '071','pt-BR','Instalações nucleares; abrigos nucleares'                                                         ,71  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '071');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '072','pt-BR','Prédio comercial; parques industriais'                                                             ,72  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '072');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '073','pt-BR','Engenharia oceanográfica'                                                                     ,73  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '073');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '074','pt-BR','Artilharia; munições; armas especiais'                                                       ,74  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '074');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '075','pt-BR','Exploração de petróleo; refino'                                                               ,75  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '075');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '076','pt-BR','Petróleo e combustível (armazenagem e distribuição)'                                                 ,76  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '076');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '077','pt-BR','Tubulações (de ponta a ponta - liquefeito e gás)'                                                       ,77  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '077');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '078','pt-BR','Planejamento (comunitário; regional; área total e estadual)'                                              ,78  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '078');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '079','pt-BR','Planejamento (local, instalação e projeto)'                                                     ,79  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '079');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '080','pt-BR','Design de encanamento e tubulação'                                                                        ,80  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '080');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '081','pt-BR','Estruturas pneumáticas; edifícios com suporte aéreo'                                                   ,81  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '081');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '082','pt-BR','Instalações postais'                                                                             ,82  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '082');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '083','pt-BR','Geração, transmissão, distribuição de energia'                                                  ,83  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '083');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '084','pt-BR','Prisões e instalações correcionais'                                                             ,84  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '084');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '085','pt-BR','Design de produtos, máquinas e equipamentos'                                                           ,85  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '085');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '086','pt-BR','Radar; sonar; telescópios de rádio e radar'                                                        ,86  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '086');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '087','pt-BR','Ferrovia e trânsito rápido'                                                                    ,87  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '087');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '088','pt-BR','Instalações recreativas (parques; marinas; etc.)'                                                ,88  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '088');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '089','pt-BR','Reabilitação (edifícios; estruturas; instalações)'                                            ,89  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '089');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '090','pt-BR','Recuperação de recursos; reciclagem'                                                                 ,90  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '090');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '091','pt-BR','Sistemas e blindagens de radiofrequência'                                                          ,91  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '091');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '092','pt-BR','Canais de rios; vias navegáveis; controle de inundação'                                                       ,92  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '092');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '093','pt-BR','Engenharia de segurança; estudos de acidentes; estudos da CIPA'                                            ,93  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '093');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '094','pt-BR','Sistemas de segurança; detecção de intrusos e de fumaça'                                                  ,94  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '094');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '095','pt-BR','Design e estudos sísmicos'                                                                      ,95  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '095');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '096','pt-BR','Coleta, tratamento e eliminação de esgotos'                                                       ,96  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '096');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '097','pt-BR','Estudos de solos e geológicos para fundações'                                                         ,97  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '097');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '098','pt-BR','Utilização de energia solar'                                                                      ,98  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '098');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '099','pt-BR','Resíduos sólidos; incineração; aterro'                                                          ,99  WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '099');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '100','pt-BR','Ambientes especiais; espaços higienizados; etc.'                                                       ,100 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '100');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '101','pt-BR','Design estrutural; estruturas especiais'                                                         ,101 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '101');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '102','pt-BR','Levantamento; cartografia; mapeamento; estudos de planície de inundação'                                             ,102 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '102');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '103','pt-BR','Piscinas'                                                                                ,103 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '103');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '104','pt-BR','Manuseio e instalações de águas pluviais'                                                              ,104 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '104');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '105','pt-BR','Sistemas de telefonia (rural; móvel; intercomunicador; etc.)'                                             ,105 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '105');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '106','pt-BR','Serviços de teste e inspeção'                                                                 ,106 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '106');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '107','pt-BR','Engenharia de tráfego e transportes'                                                          ,107 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '107');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '108','pt-BR','Torres (sistemas autossustentáveis e orientados)'                                                      ,108 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '108');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '109','pt-BR','Túneis e passagens subterrâneas'                                                                             ,109 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '109');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '110','pt-BR','Renovações urbanas; desenvolvimento comunitário'                                                         ,110 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '110');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '111','pt-BR','Utilitários (gás e vapor)'                                                                     ,111 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '111');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '112','pt-BR','Análise de valor; custo de vida útil'                                                            ,112 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '112');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '113','pt-BR','Armazéns e depósitos'                                                                           ,113 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '113');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '114','pt-BR','Recursos hídricos; hidrologia; lençóis freáticos'                                                      ,114 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '114');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '115','pt-BR','Fornecimento, tratamento e distribuição de água'                                                      ,115 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '115');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '116','pt-BR','Túneis aerodinâmicos; design/teste de instalações de pesquisa'                                              ,116 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '116');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT '117','pt-BR','Zoneamento; estudos de uso da terra'                                                                      ,117 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = '117');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'A08','pt-BR','Instalações para animais'                                                                             ,118 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'A08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'A09','pt-BR','Antiterrorismo/proteção de Força'                                                               ,119 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'A09');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'A10','pt-BR','Redução do amianto'                                                                            ,120 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'A10');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C01','pt-BR','Cartografia'                                                                                   ,121 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C01');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C03','pt-BR','Cartografia: náutica e aeronáutica'                                                           ,122 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C05','pt-BR','Instalações de cuidados/desenvolvimento infantil'                                                             ,123 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C07','pt-BR','Engenheiro de águas litorâneas'                                                                           ,124 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C07');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C11','pt-BR','Instalações comunitárias'                                                                          ,125 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C11');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C16','pt-BR','Levantamento de construções'                                                                        ,126 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C16');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'C19','pt-BR','Instalações criogênicas'                                                                          ,127 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'C19');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D04','pt-BR','Empresa de arquitetura e construção - Preparação de solicitações de propostas'                                          ,128 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D05','pt-BR','Elevação digital e desenvolvimento de modelos de terreno'                                               ,129 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D06','pt-BR','Ortofotografia digital'                                                                      ,130 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D06');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'D08','pt-BR','Estudos e design de dragagem'                                                                   ,131 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'D08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E03','pt-BR','Estudos e design elétricos'                                                                 ,132 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E06','pt-BR','Embaixadas e chancelarias'                                                                      ,133 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E06');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E08','pt-BR','Economia de engenharia'                                                                         ,134 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E10','pt-BR','Mapeamento ambiental e de recursos naturais'                                                    ,135 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E10');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E11','pt-BR','Planejamento ambiental'                                                                        ,136 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E11');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E12','pt-BR','Remediação ambiental'                                                                     ,137 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E12');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'E13','pt-BR','Testes e análises ambientais'                                                            ,138 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'E13');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'F05','pt-BR','Engenharia pericial'                                                                          ,139 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'F05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'G03','pt-BR','Levantamento geodésico: terra e aerotransportado'                                                       ,140 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'G03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'G04','pt-BR','Serviços do sistema de informações geográficas: desenvolvimento, análise e coleta de dados'            ,141 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'G04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'G05','pt-BR','Conversão de dados geoespaciais: digitalização, compilação, atribuição, desenhos, esboços',142 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'G05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'H02','pt-BR','Manuseio e armazenamento de materiais perigosos'                                                      ,143 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'H02');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'H03','pt-BR','Remediação de resíduos perigosos, tóxicos e radioativos'                                               ,144 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'H03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'H13','pt-BR','Levantamento hidrográfico'                                                                        ,145 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'H13');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'I04','pt-BR','Sistemas inteligentes de transporte'                                                            ,146 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'I04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'L02','pt-BR','Agrimensura'                                                                                ,147 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'L02');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'M01','pt-BR','Localização de mapeamento/sistemas de endereçamento'                                                           ,148 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'M01');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'N02','pt-BR','Estruturas de navegação; bloqueios'                                                                  ,149 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'N02');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'P03','pt-BR','Fotogrametria'                                                                                ,150 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'P03');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'P13','pt-BR','Instalações de segurança pública'                                                                      ,151 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'P13');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R05','pt-BR','Plantas/sistemas de refrigeração'                                                                  ,152 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R05');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R07','pt-BR','Sensoriamento remoto'                                                                                ,153 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R07');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R08','pt-BR','Instalações de pesquisa'                                                                           ,154 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R08');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R10','pt-BR','Análise de riscos'                                                                                 ,155 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R10');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'R12','pt-BR','Telhamentos'                                                                                       ,156 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'R12');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'S11','pt-BR','Design sustentável'                                                                            ,157 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'S11');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'T04','pt-BR','Levantamento e mapeamento topográfico'                                                             ,158 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'T04');
INSERT INTO CFGProjectCodeDescriptions (ProjectCode,UICultureName,Description,Seq) SELECT 'U01','pt-BR','Remediação de artilharia não detonada'                                                               ,159 WHERE EXISTS (SELECT 'x' FROM CFGProjectCodeData WHERE ProjectCode = 'U01');

DELETE FROM CFGProjectMilestoneDescriptions WHERE UICultureName = 'pt-BR';
--...........................................................................................................................v(255)
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysEstStart',           'pt-BR','Início estimado',                   1 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysEstStart')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysEstCompletion',      'pt-BR','Conclusão estimada',              2 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysEstCompletion')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysContract',           'pt-BR','Contrato concedido',                  3 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysContract')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysBidSubmitted',       'pt-BR','Oferta enviada',                     4 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysBidSubmitted')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysStart',              'pt-BR','Ínicio efetivo',                      5 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysStart')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysActualCompl',        'pt-BR','Conclusão atual',                 6 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysActualCompl')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysProfServicesCompl',  'pt-BR','Conclusão de serviços profissionais',  7 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysProfServicesCompl')
INSERT INTO CFGProjectMilestoneDescriptions (Code, UICultureName, Description, Seq) SELECT 'SysConstCompl',         'pt-BR','Conclusão da construção',           8 WHERE EXISTS (SELECT 'x' FROM CFGProjectMilestoneData WHERE Code = 'SysConstCompl')


DELETE FROM CFGProposalSourceDescriptions WHERE UICultureName = 'pt-BR';
--..................................................................................................v(50)
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '01','pt-BR','Referência do cliente'  ,1 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '01');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '02','pt-BR','Evento'             ,2 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '02');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '03','pt-BR','Lista'              ,3 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '03');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '04','pt-BR','Contato pessoal'  ,4 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '04');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '05','pt-BR','Redes sociais'      ,5 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '05');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '06','pt-BR','Liderança em ideias',6 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '06');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '07','pt-BR','Site'           ,7 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '07');
INSERT INTO CFGProposalSourceDescriptions (Code,UICultureName,Description,Seq) SELECT '08','pt-BR','GovWinIQ'          ,8 WHERE EXISTS (SELECT 'x' FROM CFGProposalSourceData WHERE Code = '08');

DELETE FROM CFGProposalStatusDescriptions WHERE UICultureName = 'pt-BR';
--..................................................................................................v(50)
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '01','pt-BR','Em andamento',1 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '01');
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '02','pt-BR','Em análise'  ,2 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '02');
INSERT INTO CFGProposalStatusDescriptions (Code,UICultureName,Description,Seq) SELECT '03','pt-BR','Enviado'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGProposalStatusData WHERE Code = '03');

DELETE FROM CFGPRResponsibilityDescriptions WHERE UICultureName = 'pt-BR';
--....................................................................................................v(80)
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'C' ,'pt-BR','Consultor'           ,3 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'C');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'IE','pt-BR','Experiência individual',4 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'IE');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'JV','pt-BR','Joint venture'        ,2 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'JV');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'P' ,'pt-BR','Operação a termo'                ,1 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'P');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'S' ,'pt-BR','Subempreiteiro'        ,5 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'S');
INSERT INTO CFGPRResponsibilityDescriptions (Code,UICultureName,Description,Seq) SELECT 'U' ,'pt-BR','Indeterminado'         ,6 WHERE EXISTS (SELECT 'x' FROM CFGPRResponsibilityData WHERE Code = 'U');

DELETE FROM CFGPYAccrualsDescriptions WHERE UICultureName = 'pt-BR';
--............................................................................................................................v(40)
INSERT INTO CFGPYAccrualsDescriptions (Code,Company,UICultureName,Description) SELECT 'Sick Lv.',CFGMainData.Company,'pt-BR','Afastamento por doença' FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGPYAccrualsData WHERE Code = 'Sick Lv.' AND Company = CFGMainData.Company)
INSERT INTO CFGPYAccrualsDescriptions (Code,Company,UICultureName,Description) SELECT 'Vacation',CFGMainData.Company,'pt-BR','Férias'   FROM CFGMainData WHERE EXISTS (SELECT 'x' FROM CFGPYAccrualsData WHERE Code = 'Vacation' AND Company = CFGMainData.Company)

DELETE FROM CFGRGMethodsDescriptions WHERE UICultureName = 'pt-BR';
--..........................................................................................v(40)
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'B','pt-BR','Cobranças JTD'                   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'B');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'M','pt-BR','(JTD DL*Mult) + Desp. reemb. JTD'  WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'M');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'N','pt-BR','Sem reconhecimento de receita'         WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'N');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'P','pt-BR','(Pct Comp*Fee) + Desp. reemb. JTD' WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'P');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'R','pt-BR','Recebimentos JTD'                   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'R');
INSERT INTO CFGRGMethodsDescriptions (Method,UICultureName,Description) SELECT 'W','pt-BR','Cobranças JTD + WIP no faturamento'   WHERE EXISTS (SELECT 'x' FROM CFGRGMethodsData WHERE Method = 'W');

DELETE FROM CFGServiceProfileDescriptions WHERE UICultureName = 'pt-BR';
--..................................................................................................................v(40)
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.18' ,'pt-BR','Utilização da terra/plano de desenvolvimento','Area in hectares'               WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.18');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.19' ,'pt-BR','Plano de uso do solo/construção','Area in hectares'                          WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.19');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.23' ,'pt-BR','Plano de paisagismo','Area in hectares'                                  WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.23');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.24' ,'pt-BR','Plano de espaço aberto/verde','Area in hectares'                           WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.24');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.25' ,'pt-BR','Plano de estrutura de paisagismo','Area in hectares'                        WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.25');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.26' ,'pt-BR','Plano de conservação de paisagismo','Area in hectares'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.26');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.27' ,'pt-BR','Plano de manutenção e desenvolvimento','Area in hectares'                WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.27');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.34' ,'pt-BR','Edifícios e interiores','Chargeable costs in Euro'                 WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.34');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.39' ,'pt-BR','Instalações ao ar livre','Chargeable costs in Euro'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.39');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.43' ,'pt-BR','Estruturas de engenharia civil','Chargeable costs in Euro'            WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.43');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.47' ,'pt-BR','Instalações de transporte','Chargeable costs in Euro'               WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.47');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.51' ,'pt-BR','Planejamento estrutural','Chargeable costs in Euro'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.51');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.55' ,'pt-BR','Equipamentos técnicos','Chargeable costs in Euro'                     WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.55');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A01','pt-BR','Estudos de impacto ambiental','Area in hectares'                    WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A01');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A02','pt-BR','Isolamento térmico e balanceamento de energia','Chargeable costs in Euro' WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A02');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A03','pt-BR','Acústica de edifícios','Chargeable costs in Euro'                      WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A03');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A04','pt-BR','Acústica do espaço','Chargeable costs in Euro'                          WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A04');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A05','pt-BR','Geotécnica','Chargeable costs in Euro'                             WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A05');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A06','pt-BR','Operações de medição/levantamento','Accounting units'                  WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A06');
INSERT INTO CFGServiceProfileDescriptions (Code,UICultureName,Description,FeeBasisLabel) SELECT '2013.A07','pt-BR','Levantamento de engenharia','Chargeable costs in Euro'                   WHERE EXISTS (SELECT 'x' FROM CFGServiceProfileData WHERE Code = '2013.A07');

DELETE FROM CFGSuffixDescriptions WHERE UICultureName = 'pt-BR';
--......................................................................................v(150)
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'I'  ,'pt-BR','I'  ,3 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'I');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'II' ,'pt-BR','II' ,4 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'II');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'III','pt-BR','III',5 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'III');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'Jr.','pt-BR','Jr.',1 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'Jr.');
INSERT INTO CFGSuffixDescriptions (Code,UICultureName,Suffix,Seq) SELECT 'Sr.','pt-BR','Sr.',2 WHERE EXISTS (SELECT 'x' FROM CFGSuffixData WHERE Code = 'Sr.');

DELETE FROM CFGUnitTypeDescriptions WHERE UICultureName = 'pt-BR';
--...................................................................................................v(50)
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Equipment','pt-BR','Equipamento',1 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Equipment');
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Expenses ','pt-BR','Despesas' ,2 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Expenses');
INSERT INTO CFGUnitTypeDescriptions (Code,UICultureName,Description,Seq) SELECT 'Labor'    ,'pt-BR','Mão de obra'    ,3 WHERE EXISTS (SELECT 'x' FROM CFGUnitTypeData WHERE Code = 'Labor');

DELETE FROM CFGVendorTypeDescriptions WHERE UICultureName = 'pt-BR';
--...................................................................................v(30)
INSERT INTO CFGVendorTypeDescriptions (Type,UICultureName,Label) SELECT 'C','pt-BR','Consultor' WHERE EXISTS (SELECT 'x' FROM CFGVendorTypeData WHERE Type = 'C');
INSERT INTO CFGVendorTypeDescriptions (Type,UICultureName,Label) SELECT 'T','pt-BR','Comércio'      WHERE EXISTS (SELECT 'x' FROM CFGVendorTypeData WHERE Type = 'T');

DELETE FROM FW_CFGAttachmentCategoryDesc WHERE UICultureName = 'pt-BR';
--................................................................................................................................................v(255)
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Notes'          ,'Activities'         ,'pt-BR','Notas'                   ,1  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Notes' and Application ='Activities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'NDA'            ,'Contacts'           ,'pt-BR','Acordo de confidencialidade',2  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'NDA' and Application ='Contacts');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Notes'          ,'Contacts'           ,'pt-BR','Notas'                   ,3  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Notes' and Application ='Contacts');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'CompanyOverview','TextLibrary'        ,'pt-BR','Visão geral da empresa'        ,4  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'CompanyOverview' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Case Study'     ,'TextLibrary'        ,'pt-BR','Estudo de caso'              ,5  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Case Study' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Reference'      ,'TextLibrary'        ,'pt-BR','Referência'               ,6  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Reference' and Application ='TextLibrary');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Resume'         ,'Employees'          ,'pt-BR','Currículo'                  ,7  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Resume' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Award'          ,'Employees'          ,'pt-BR','Concessão'                   ,8  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Award' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Leadership'     ,'Employees'          ,'pt-BR','Liderança em ideias'      ,9  WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Leadership' and Application ='Employees');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Firms'              ,'pt-BR','Contrato geral de serviços (MSA)',10 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Firms'              ,'pt-BR','Declaração de trabalho (SOW)'       ,11 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'NDA'            ,'Firms'              ,'pt-BR','Acordo de confidencialidade',12 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'NDA' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'ClientACH'      ,'Firms'              ,'pt-BR','Cliente ACH'              ,13 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'ClientACH' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'VendorACH'      ,'Firms'              ,'pt-BR','Fornecedor ACH'              ,14 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'VendorACH' and Application ='Firms');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'DigitalArtifact','Marketing Campaigns','pt-BR','Artigo digital'        ,15 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'DigitalArtifact' and Application ='Marketing Campaigns');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Estimate'       ,'Opportunities'      ,'pt-BR','Estimativa'                ,16 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Estimate' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Proposal'       ,'Opportunities'      ,'pt-BR','Proposta'                ,17 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Proposal' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Opportunities'      ,'pt-BR','Declaração de trabalho (SOW)'       ,18 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Opportunities'      ,'pt-BR','Contrato geral de serviços (MSA)',19 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Opportunities');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Estimate'       ,'Projects'           ,'pt-BR','Estimativa'                ,20 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Estimate' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'Proposal'       ,'Projects'           ,'pt-BR','Proposta'                ,21 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'Proposal' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'SOW'            ,'Projects'           ,'pt-BR','Declaração de trabalho (SOW)'       ,22 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'SOW' and Application ='Projects');
INSERT INTO FW_CFGAttachmentCategoryDesc (Code,Application,UICultureName,Description,Seq) SELECT 'MSA'            ,'Projects'           ,'pt-BR','Contrato geral de serviços (MSA)',23 WHERE EXISTS (SELECT 'x' FROM FW_CFGAttachmentCategoryData WHERE Code = 'MSA' and Application ='Projects');

DELETE FROM FW_CFGLabelData WHERE UICultureName = 'pt-BR';
--............................................................................................................................................................................v(100)
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','accountLabel'                            ,'Conta'                       ,'[Account]'                    ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','accountLabelPlural'                      ,'Contas'                      ,'[Accounts]'                   ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','clientLabel'                             ,'Cliente'                        ,'[Client]'                     ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','clientLabelPlural'                       ,'Clientes'                       ,'[Clients]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','contactLabel'                            ,'Contato'                       ,'[Contact]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','contactLabelPlural'                      ,'Contatos'                      ,'[Contacts]'                   ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','employeeLabel'                           ,'Funcionário'                      ,'[Employee]'                   ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','employeeLabelPlural'                     ,'Funcionários'                     ,'[Employees]'                  ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','labcd1Label'                             ,'Nível 1 do Código do trabalho'            ,'[Labcd Level 1]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','labcd1LabelPlural'                       ,'Nível 1s do Código do trabalho'           ,'[Labcd Level 1s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','labcd2Label'                             ,'Nível 2 do Código do trabalho'            ,'[Labcd Level 2]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','labcd2LabelPlural'                       ,'Nível 2s do Código do trabalho'           ,'[Labcd Level 2s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','labcd3Label'                             ,'Nível 3 do Código do trabalho'            ,'[Labcd Level 3]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','labcd3LabelPlural'                       ,'Nível 3s do Código do trabalho'           ,'[Labcd Level 3s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','labcd4Label'                             ,'Nível 4 do Código do trabalho'            ,'[Labcd Level 4]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','labcd4LabelPlural'                       ,'Nível 4s do Código do trabalho'           ,'[Labcd Level 4s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','labcd5Label'                             ,'Nível 5 do Código do trabalho'            ,'[Labcd Level 5]'              ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','labcd5LabelPlural'                       ,'Nível 5s do Código do trabalho'           ,'[Labcd Level 5s]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','labcdLabel'                              ,'Código do trabalho'                    ,'[Labor Code]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','labcdLabelPlural'                        ,'Códigos do trabalho'                   ,'[Labor Codes]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','firmLabel'                               ,'Firma'                          ,'[Firm]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','firmLabelPlural'                         ,'Firmas'                         ,'[Firms]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','mktLabel'                                ,'Campanha de Marketing'            ,'[Marketing Campaign]'         ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','mktLabelPlural'                          ,'Campanhas de Marketing'           ,'[Marketing Campaigns]'        ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','org1Label'                               ,'Organização 1'                ,'[Org Level 1]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','org1LabelPlural'                         ,'Organização 1s'               ,'[Org Level 1s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','org2Label'                               ,'Organização 2'                ,'[Org Level 2]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','org2LabelPlural'                         ,'Organização 2s'               ,'[Org Level 2s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','org3Label'                               ,'Organização 3'                ,'[Org Level 3]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','org3LabelPlural'                         ,'Organização 3s'               ,'[Org Level 3s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','org4Label'                               ,'Organização 4'                ,'[Org Level 4]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','org4LabelPlural'                         ,'Organização 4s'               ,'[Org Level 4s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','org5Label'                               ,'Organização 5'                ,'[Org Level 5]'                ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','org5LabelPlural'                         ,'Organização 5s'               ,'[Org Level 5s]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','orgLabel'                                ,'Organização'                  ,'[Organization]'               ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','orgLabelPlural'                          ,'Organizações'                 ,'[Organizations]'              ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','proposalLabel'                           ,'Proposta'                      ,'[Proposal]'                   ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','proposalLabelPlural'                     ,'Propostas'                     ,'[Proposals]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','textLibLabel'                            ,'Histórico padrão'                   ,'[Text Library]'               ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','textLibLabelPlural'                      ,'Históricos padrão'                  ,'[Text Libraries]'             ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','unitLabel'                               ,'Unidade'                          ,'[Unit]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','unitLabelPlural'                         ,'Unidades'                         ,'[Units]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','vendorLabel'                             ,'Fornecedor'                        ,'[Vendor]'                     ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','vendorLabelPlural'                       ,'Fornecedores'                       ,'[Vendors]'                    ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','wbs1Label'                               ,'Projeto'                       ,'[WBS1]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','wbs1LabelPlural'                         ,'Projetos'                      ,'[WBS1s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','wbs2Label'                               ,'Fase'                         ,'[WBS2]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','wbs2LabelPlural'                         ,'Fases'                        ,'[WBS2s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','wbs3Label'                               ,'Tarefa'                          ,'[WBS3]'                       ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','wbs3LabelPlural'                         ,'Tarefas'                         ,'[WBS3s]'                      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','sysPM'                                   ,'Gerente de projetos'               ,'[Project Manager]'            ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','sysPR'                                   ,'Diretor'                     ,'[Principal]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','sysSP'                                   ,'Supervisor'                    ,'[Supervisor]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','equipmentLabel'                          ,'Equipamento'                     ,'[Equipment]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','equipmentLabelPlural'                    ,'Equipamento'                     ,'[Equipments]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','jobToDateLabel'                          ,'Desde o início do trabalho'                   ,'[JobToDate]'                  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','JTDLabel'                                ,'JTD'                           ,'[JTD]'                        ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','estimateToCompleteLabel'                 ,'Estimativa até a conclusão'          ,'[EstimateToComplete]'         ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','ETCLabel'                                ,'ETC'                           ,'[ETC]'                        ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','estimateAtCompletionLabel'               ,'Estimativa na conclusão'        ,'[EstimateAtCompletion]'       ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','EACLabel'                                ,'EAC'                           ,'[EAC]'                        ,'N','Y',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','sysPRM'                                  ,'Gerente de proposta'              ,'[Proposal Manager]'           ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','sysMC'                                   ,'Coordenador de marketing'         ,'[Marketing Coordinator]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','sysBDL'                                  ,'Liderança no desenvolvimento de negócios'     ,'[Business Development Lead]'  ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','consultantLabel'                         ,'Consultor'                    ,'[Consultant]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','consultantLabelPlural'                   ,'Consultores'                   ,'[Consultants]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','compensationLabel'                       ,'Pagamento'                  ,'[Compensation]'               ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','revType1Label'                           ,'Mão de obra'                         ,'[Revenue Method Type 1]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','revType2Label'                           ,'Consultor'                    ,'[Revenue Method Type 2]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','revType3Label'                           ,'Reemb.'                         ,'[Revenue Method Type 3]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','revType4Label'                           ,'Método de receita 4'              ,'[Revenue Method Type 4]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','revType5Label'                           ,'Método de receita 5'              ,'[Revenue Method Type 5]'      ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','feeEstimatingCostGroupLabel'             ,'Grupo de custo'                    ,'[Cost Group]'                 ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','feeEstimatingCostGroupLabelPlural'       ,'Grupos de custo'                   ,'[Cost Groups]'                ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','feeEstimatingFunctionalGroupLabel'       ,'Grupo funcional'              ,'[Functional Group]'           ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','feeEstimatingFunctionalGroupLabelPlural' ,'Grupos funcionais'             ,'[Functional Groups]'          ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','feeEstimatingServiceProfileLabel'        ,'Perfil do serviço'               ,'[Service Profile]'            ,'N','N',NULL);
INSERT INTO FW_CFGLabelData (UICultureName,LabelName,LabelValue,PlaceHolder,Gender,LeadingVowelTreatment,UDIC_ID) VALUES ('pt-BR','feeEstimatingServiceProfileLabelPlural'  ,'Perfis do serviço'              ,'[Service Profiles]'           ,'N','N',NULL);

COMMIT TRANSACTION

END
GO
