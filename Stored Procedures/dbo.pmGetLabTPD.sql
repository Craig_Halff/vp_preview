SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[pmGetLabTPD]
  @strPlanID VARCHAR(32),
  @strETCDate VARCHAR(10), -- Date must be in format: 'yyyy-mm-dd',
  @strRetrievalMode VARCHAR(1) = '3',
  @strGetPlanned VARCHAR(1) = 'Y',
  @strGetBaseline VARCHAR(1) = 'Y',
  @strGetJTD VARCHAR(1) = 'Y',
  @strGetEVT VARCHAR(1) = 'Y',
  @strGetTD VARCHAR(1) = 'Y',
  @strGetComp VARCHAR(1) = 'Y',
  @strGetCons VARCHAR(1) = 'Y',
  @strGetReim VARCHAR(1) = 'Y',
  @strGetCachedRate VARCHAR(1) = 'Y'
AS

BEGIN -- Procedure pmGetLabTPD

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  
  DECLARE @startTime datetime
  DECLARE @totalTime int
  
  SET @startTime = GetDate()
  
  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
  
  DECLARE @intHrDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intRtCostDecimals int
  DECLARE @intRtBillDecimals int
  DECLARE @intLabRevDecimals int
  
  DECLARE @intBaselineLabCount int
  DECLARE @intPlannedLabCount int
  
  DECLARE @intEVCount int

  DECLARE @intTDCount int

  DECLARE @intJTDLabCount int
  
  DECLARE @intCompCount int
  DECLARE @intConsCount int
  DECLARE @intReimCount int
    
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strUserName Nvarchar(32)

  DECLARE @strOutlineNumber varchar(255)

  DECLARE @strTaskID varchar(32)
  DECLARE @strAssignmentID varchar(32)
  
  DECLARE @strFeesByPeriod varchar(1)
  
  DECLARE @intCostRtTableNo int
  DECLARE @intBillingRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int

  DECLARE @siCostRtMethod smallint
  DECLARE @siBillingRtMethod smallint
  DECLARE @siGRMethod smallint
  DECLARE @siCategory smallint
  
  DECLARE @strResourceID Nvarchar(20)
  DECLARE @strLaborCode Nvarchar(14)
  DECLARE @strGRLBCD Nvarchar(14)
  
  DECLARE @strUnposted varchar(1)
  
  DECLARE @strHasJTDLab varchar(1)
  
  -- Declare Temp tables.
           
   DECLARE @tabTopTask 
    TABLE (TaskID varchar(32) COLLATE database_default,
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(7) COLLATE database_default,
           WBS3 Nvarchar(7) COLLATE database_default,
           LaborCode Nvarchar(14) COLLATE database_default
           PRIMARY KEY(TaskID))

	DECLARE @tabJTDCalendar 
	  TABLE (StartDate datetime,
           EndDate datetime
           PRIMARY KEY(StartDate)) 

	DECLARE @tabLD 
	  TABLE (RowID int IDENTITY,
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(7) COLLATE database_default,
           WBS3 Nvarchar(7) COLLATE database_default,
           LaborCode Nvarchar(14) COLLATE database_default,
           Employee Nvarchar(20) COLLATE database_default,
           Category smallint,
           StartDate datetime,
           EndDate datetime,
           PeriodHrs decimal(19,4), 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4),
           PostedFlg smallint
           PRIMARY KEY(RowID, WBS1, WBS2, WBS3, LaborCode, Employee, Category, StartDate))

	DECLARE @tabJTDLab
	  TABLE (RowID int IDENTITY,
	         PlanID varchar(32) COLLATE database_default,
	         TaskID varchar(32) COLLATE database_default,
	         AssignmentID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodHrs decimal(19,4), 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PostedFlg smallint 
           PRIMARY KEY(RowID, PlanID, TaskID, StartDate, EndDate)) 

  -- Save Active Company string for use later.
  
  SET @strCompany = dbo.GetActiveCompany()
  SET @strUserName = dbo.GetVisionAuditUserName()
          
  -- Set JTD/ETC Dates.
  
  SET @dtETCDate = CONVERT(datetime, REPLACE(@strETCDate, '-', ''))
  SET @dtJTDDate = DATEADD(d, -1, @dtETCDate) 

  -- Get decimal settings.
  
  SELECT @intHrDecimals = HrDecimals,
         @intAmtCostDecimals = AmtCostDecimals,
         @intAmtBillDecimals = AmtBillDecimals,
         @intRtCostDecimals = RtCostDecimals,
         @intRtBillDecimals = RtBillDecimals,
         @intLabRevDecimals = LabRevDecimals
    FROM dbo.RP$tabDecimals(@strPlanID)
    
  -- Get flags to determine which features are being used.
  
  SELECT
     @strFeesByPeriod = FeesByPeriodFlg
     FROM CFGResourcePlanning
     WHERE CFGResourcePlanning.Company = @strCompany
     
  -- Get Plan parameters to be used later.
  
  SELECT
    @siCostRtMethod = CostRtMethod, 
    @siBillingRtMethod = BillingRtMethod,
    @intCostRtTableNo = CostRtTableNo,
    @intBillingRtTableNo = BillingRtTableNo,
    @siGRMethod = GRMethod,
    @intGenResTableNo = GenResTableNo,
    @intGRBillTableNo = GRBillTableNo,
    @strUnposted = UnpostedFlg
    FROM RPPlan WHERE PlanID = @strPlanID
    
  -- Initialize counting variables.
  
  SELECT 
    @intBaselineLabCount = 0,
    @intPlannedLabCount = 0,
    @intEVCount = 0,
    @intTDCount = 0,
    @intJTDLabCount = 0,
    @intCompCount = 0,
    @intConsCount = 0,
    @intReimCount = 0
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strGetJTD = 'Y' AND @strRetrievalMode != '1')
    BEGIN
 
      INSERT @tabTopTask(TaskID, WBS1, WBS2, WBS3, LaborCode)
        SELECT DISTINCT TaskID, WBS1, WBS2, WBS3, LaborCode
          FROM RPTask AS T
          INNER JOIN (SELECT MIN(OutlineLevel) AS OutlineLevel
              FROM RPTask AS XT INNER JOIN RPWBSLevelFormat AS F ON XT.PlanID = F.PlanID
              AND XT.WBSType = F.WBSType AND F.WBSMatch = 'Y'
              WHERE XT.PlanID = @strPlanID) AS ZT ON T.OutlineLevel = ZT.OutlineLevel
          WHERE T.PlanID = @strPlanID AND T.WBS1 IS NOT NULL AND T.WBS1 != '<none>'
      
	    IF (@@ROWCOUNT > 0)
	      BEGIN
	      
	        -- Compute data for JTD Calendar table with overflow periods before and after the regular calendar.

          IF (@strRetrievalMode = '3')
	          INSERT @tabJTDCalendar(StartDate, EndDate)
		          SELECT StartDate, EndDate FROM
			          (SELECT StartDate AS StartDate, EndDate AS EndDate
                   FROM RPCalendarInterval WHERE PlanID = @strPlanID
                 UNION ALL
                 SELECT CAST('19300101' AS datetime) AS StartDate,
                   DATEADD(d, -1, MIN(StartDate)) AS EndDate
                   FROM RPCalendarInterval WHERE PlanID = @strPlanID
                   GROUP BY PlanID
                 UNION ALL
                 SELECT DATEADD(d, 1, MAX(EndDate)) AS StartDate,
                   DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate))) AS EndDate
                   FROM RPCalendarInterval WHERE PlanID = @strPlanID
                 ) AS CI
          ELSE IF (@strRetrievalMode = '2')
	          INSERT @tabJTDCalendar(StartDate, EndDate)
		          SELECT CAST('19300101' AS datetime) AS StartDate,
                DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate))) AS EndDate
                FROM RPCalendarInterval WHERE PlanID = @strPlanID
                GROUP BY PlanID
          
          -- Initialize control variables to indicate whether we have JTD data of certain type.

	        SET @strHasJTDLab = 'N'
    
          --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	        -- Save Labor JTD for this Plan into temp table.

	        INSERT @tabLD
            (WBS1,
             WBS2,
             WBS3,
             LaborCode,
             Employee,
             Category,
             StartDate,
             EndDate,
             PeriodHrs, 
             PeriodCost, 
             PeriodBill,
             PostedFlg)
		        SELECT
			        LD.WBS1,
			        LD.WBS2,
			        LD.WBS3,
			        ISNULL(LD.LaborCode, '%') AS LaborCode,
			        ISNULL(LD.Employee, '') AS Employee,
			        LD.Category,
			        CI.StartDate, 
			        CI.EndDate, 
			        SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs, 
			        SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost, 
			        SUM(BillExt) AS PeriodBill, 
			        1 AS PostedFlg
			        FROM LD WITH (INDEX(LDWBS1TransDateIDX))
				        INNER JOIN @tabTopTask AS T ON (LD.WBS1 = T.WBS1
                  AND LD.ProjectCost = 'Y' AND LD.TransDate <= @dtJTDDate
                  AND LD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
                  AND LD.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                  AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%')))
				        INNER JOIN @tabJTDCalendar AS CI ON LD.TransDate BETWEEN CI.StartDate AND CI.EndDate 
			        GROUP BY LD.WBS1, LD.WBS2, LD.WBS3, LD.LaborCode, LD.Employee, LD.Category, CI.StartDate, CI.EndDate
        			
	        IF (@@ROWCOUNT > 0) SET @strHasJTDLab = 'Y'

	        -- Save Unposted Labor JTD into temp table.

          IF (@strUnposted = 'Y')
            BEGIN
            
	            INSERT @tabLD
                (WBS1,
                 WBS2,
                 WBS3,
                 LaborCode,
                 Employee,
                 Category,
                 StartDate,
                 EndDate,
                 PeriodHrs, 
                 PeriodCost, 
                 PeriodBill,
                 PostedFlg)
		            SELECT
			            WBS1,
			            WBS2,
			            WBS3,
			            ISNULL(LaborCode, '%') AS LaborCode,
			            ISNULL(Employee, '') AS Employee,
			            Category,
			            StartDate, 
			            EndDate, 
			            SUM(PeriodHrs) AS PeriodHrs, 
			            SUM(PeriodCost) AS PeriodCost, 
			            SUM(PeriodBill) AS PeriodBill, 
			            -1 AS PostedFlg FROM
				            (SELECT
                       TD.WBS1,
                       TD.WBS2,
                       TD.WBS3,
                       TD.LaborCode,
                       TD.Employee,
                       TD.BillCategory AS Category,
                       CI.StartDate, 
                       CI.EndDate, 
                       SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs, 
                       SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost, 
                       SUM(BillExt) AS PeriodBill
                       FROM tkDetail AS TD
                         INNER JOIN @tabTopTask AS T ON (TD.WBS1 = T.WBS1
                           AND TD.TransDate <= @dtJTDDate
                           AND TD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
                           AND TD.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                           AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%')))
                         INNER JOIN @tabJTDCalendar AS CI ON TD.TransDate BETWEEN CI.StartDate AND CI.EndDate
                         INNER JOIN EM ON TD.Employee = EM.Employee  
                         INNER JOIN tkMaster AS TM ON (TD.Employee = TM.Employee AND TM.Submitted <> 'P' AND TD.EndDate = TM.EndDate)
                       GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.LaborCode, TD.Employee, TD.BillCategory, CI.StartDate, CI.EndDate
                     UNION ALL
                     SELECT
                       TD.WBS1,
                       TD.WBS2,
                       TD.WBS3,
                       TD.LaborCode,
                       TD.Employee,
                       TD.BillCategory AS Category,
                       CI.StartDate, 
                       CI.EndDate, 
                       SUM(RegHrs + OvtHrs + SpecialOvtHrs) AS PeriodHrs, 
                       SUM(RegAmtProjectCurrency + OvtAmtProjectCurrency + SpecialOvtAmtProjectCurrency) AS PeriodCost, 
                       SUM(BillExt) AS PeriodBill
                       FROM tsDetail AS TD
                         INNER JOIN @tabTopTask AS T ON (TD.WBS1 = T.WBS1
                           AND TD.TransDate <= @dtJTDDate
                           AND TD.WBS2 LIKE (ISNULL(T.WBS2, '%'))
                           AND TD.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                           AND ISNULL(TD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%')))
                         INNER JOIN @tabJTDCalendar AS CI ON TD.TransDate BETWEEN CI.StartDate AND CI.EndDate
                         INNER JOIN EM ON TD.Employee = EM.Employee  
                         INNER JOIN tsControl AS TC ON (TC.Posted = 'N' AND TD.Batch = TC.Batch)
                       GROUP BY TD.WBS1, TD.WBS2, TD.WBS3, TD.LaborCode, TD.Employee, TD.BillCategory, CI.StartDate, CI.EndDate
					          )  AS X
                    GROUP BY WBS1, WBS2, WBS3, LaborCode, Employee, Category, StartDate, EndDate
                    
              IF (@@ROWCOUNT > 0) SET @strHasJTDLab = 'Y'
              
            END -- If-Then
        	
        -->>>>>

          IF (@strHasJTDLab = 'Y')
            BEGIN
            
              INSERT @tabJTDLab
                (PlanID,
                 TaskID,  
                 AssignmentID,  
                 StartDate,  
                 EndDate,  
                 PeriodHrs,  
                 PeriodCost,  
                 PeriodBill,  
                 PostedFlg)
                SELECT
                  @strPlanID AS PlanID,
                  TaskID,  
                  AssignmentID,  
                  StartDate,  
                  EndDate,  
                  SUM(ISNULL(PeriodHrs, 0)) AS PeriodHrs,  
                  SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,  
                  SUM(ISNULL(PeriodBill, 0)) AS PeriodBill,  
                  SIGN(MIN(PostedFlg)) AS PostedFlg
                  FROM
                    (SELECT A.AssignmentID,  
                      A.TaskID,  
                      LD.StartDate,  
                      LD.EndDate,  
                      SUM(PeriodHrs) AS PeriodHrs,  
                      SUM(PeriodCost) AS PeriodCost,  
                      SUM(PeriodBill) AS PeriodBill,  
                      PostedFlg
                      FROM @tabLD AS LD
                        INNER JOIN RPAssignment AS A ON (LD.WBS1 = A.WBS1
                          AND LD.WBS2 LIKE (ISNULL(A.WBS2, '%'))  
                          AND LD.WBS3 LIKE (ISNULL(A.WBS3, '%'))  
                          AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(A.LaborCode, '%'))  
                          AND LD.Employee = A.ResourceID)  
						  INNER JOIN RPTask AS T ON T.PlanID = A.PlanID AND T.TaskID = A.TaskID 					        
						  INNER JOIN RPWBSLevelFormat AS F ON T.PlanID = F.PlanID AND T.WBSType = F.WBSType AND F.WBSMatch = 'Y'
                        INNER JOIN (SELECT MIN(AssignmentID) AS AssignmentID FROM RPAssignment 						 
                          WHERE PlanID = @strPlanID AND ResourceID IS NOT NULL  
                          GROUP BY TaskID, ResourceID) AS A1 ON A.AssignmentID = A1.AssignmentID  
                      WHERE A.PlanID = @strPlanID  
                      GROUP BY A.AssignmentID, A.TaskID, LD.StartDate, LD.EndDate, PostedFlg
                    UNION ALL -- Category as Generic Resources.
                    SELECT AC.AssignmentID,  
                      AC.TaskID,  
                      LD.StartDate,  
                      LD.EndDate,  
                      SUM(PeriodHrs) AS PeriodHrs,  
                      SUM(PeriodCost) AS PeriodCost,  
                      SUM(PeriodBill) AS PeriodBill,  
                      PostedFlg
                      FROM @tabLD AS LD 
                        INNER JOIN RPAssignment AS AC ON (LD.WBS1 = AC.WBS1 
                          AND AC.ResourceID IS NULL AND AC.GRLBCD IS NULL  
                          AND LD.WBS2 LIKE (ISNULL(AC.WBS2, '%'))  
                          AND LD.WBS3 LIKE (ISNULL(AC.WBS3, '%'))  
                          AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(AC.LaborCode, '%'))  
                          AND LD.Category = AC.Category)  
						  INNER JOIN RPTask AS T ON T.PlanID = AC.PlanID AND T.TaskID = AC.TaskID  
						  INNER JOIN RPWBSLevelFormat AS F ON T.PlanID = F.PlanID AND T.WBSType = F.WBSType AND F.WBSMatch = 'Y'

                        INNER JOIN (SELECT MIN(AssignmentID) AS AssignmentID FROM RPAssignment  
                          WHERE PlanID = @strPlanID AND ResourceID IS NULL AND GRLBCD IS NULL  
                          GROUP BY TaskID, Category) AS A1 ON AC.AssignmentID = A1.AssignmentID  
                      WHERE AC.PlanID = @strPlanID
                        AND (LD.Employee NOT IN (SELECT A2.ResourceID FROM RPAssignment AS A2  
                          WHERE A2.TaskID = AC.TaskID AND A2.ResourceID IS NOT NULL AND A2.PlanID = @strPlanID))  
                      GROUP BY AC.AssignmentID, AC.TaskID, LD.StartDate, LD.EndDate, PostedFlg
                    UNION ALL -- Labor Code as Generic Resources.
                    SELECT AL.AssignmentID,  
                      AL.TaskID,  
                      LD.StartDate,  
                      LD.EndDate,  
                      SUM(PeriodHrs) AS PeriodHrs,  
                      SUM(PeriodCost) AS PeriodCost,  
                      SUM(PeriodBill) AS PeriodBill,  
                      PostedFlg
                      FROM @tabLD AS LD 
                        INNER JOIN RPAssignment AS AL ON (LD.WBS1 = AL.WBS1 
                          AND AL.ResourceID IS NULL AND AL.Category = 0  
                          AND LD.WBS2 LIKE (ISNULL(AL.WBS2, '%'))  
                          AND LD.WBS3 LIKE (ISNULL(AL.WBS3, '%'))  
                          AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(AL.LaborCode, '%'))  
                          AND ISNULL(LD.LaborCode, '%') LIKE AL.GRLBCD)  
						 INNER JOIN RPTask AS T ON T.PlanID = AL.PlanID AND T.TaskID = AL.TaskID  
						 INNER JOIN RPWBSLevelFormat AS F ON T.PlanID = F.PlanID AND T.WBSType = F.WBSType AND F.WBSMatch = 'Y'
                        INNER JOIN (SELECT MIN(AssignmentID) AS AssignmentID FROM RPAssignment  
                          WHERE PlanID = @strPlanID AND ResourceID IS NULL AND Category = 0  
                          GROUP BY TaskID, GRLBCD) AS A1 ON AL.AssignmentID = A1.AssignmentID  
                      WHERE AL.PlanID = @strPlanID
                        AND (LD.Employee NOT IN (SELECT A2.ResourceID FROM RPAssignment AS A2  
                          WHERE A2.TaskID = AL.TaskID AND A2.ResourceID IS NOT NULL AND A2.PlanID = @strPlanID))  
                      GROUP BY AL.AssignmentID, AL.TaskID, LD.StartDate, LD.EndDate, PostedFlg
                    UNION ALL
                    SELECT NULL AS AssignmentID,  
                      T.TaskID,  
                      LD.StartDate,  
                      LD.EndDate,  
                      SUM(PeriodHrs) AS PeriodHrs,  
                      SUM(PeriodCost) AS PeriodCost,  
                      SUM(PeriodBill) AS PeriodBill,  
                      PostedFlg
                      FROM @tabLD AS LD
                        INNER JOIN RPTask AS T ON (LD.WBS1 = T.WBS1
                          AND LD.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                          AND LD.WBS3 LIKE (ISNULL(T.WBS3, '%'))  
                          AND ISNULL(LD.LaborCode, '%') LIKE (ISNULL(T.LaborCode, '%')))
                        INNER JOIN RPWBSLevelFormat AS F ON T.PlanID = F.PlanID AND T.WBSType = F.WBSType AND F.WBSMatch = 'Y'
                      WHERE T.PlanID = @strPlanID  
                      GROUP BY T.TaskID, LD.StartDate, LD.EndDate, PostedFlg) AS X
                  GROUP BY TaskID, AssignmentID, StartDate, EndDate
                  HAVING SUM(ISNULL(PeriodHrs, 0)) != 0 OR SUM(ISNULL(PeriodCost, 0)) != 0 OR SUM(ISNULL(PeriodBill, 0)) != 0
                                
            END -- If-Then (@strHasJTDLab = 'Y')

	      END -- If-Then (@@ROWCOUNT > 0)
    
    END -- If-Then (@strRetrievalMode != '1')
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Check to see if there is any time-phased data.
  -- We don't want to do any work if the plan is empty or if the client side does not request the data type.
  
  IF (@strRetrievalMode != '1')
    BEGIN
      IF (@strGetJTD = 'Y') SET @intJTDLabCount = CASE WHEN EXISTS (SELECT 'X' FROM @tabJTDLab) THEN 1 ELSE 0 END
    END
    
  IF (@strRetrievalMode = '3')
    BEGIN
      IF (@strGetBaseline = 'Y') SET @intBaselineLabCount = CASE WHEN EXISTS (SELECT 'X' FROM RPBaselineLabor WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
    END

  IF (@strGetPlanned = 'Y') SET @intPlannedLabCount = CASE WHEN EXISTS (SELECT 'X' FROM RPPlannedLabor WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
  
  IF (@strGetEVT = 'Y') SET @intEVCount = CASE WHEN EXISTS (SELECT 'X' FROM RPEVT WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
  
  IF (@strGetTD = 'Y') SET @intTDCount = CASE WHEN EXISTS (SELECT 'X' FROM RPDependency WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END

  IF (@strFeesByPeriod = 'Y')
    BEGIN
      IF (@strGetComp = 'Y') SET @intCompCount = CASE WHEN EXISTS (SELECT 'X' FROM RPCompensationFee WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
      IF (@strGetCons = 'Y') SET @intConsCount = CASE WHEN EXISTS (SELECT 'X' FROM RPConsultantFee WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
      IF (@strGetReim = 'Y') SET @intReimCount = CASE WHEN EXISTS (SELECT 'X' FROM RPReimbAllowance WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
    END
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -->>> Planned Labor TPD.

  IF (@intPlannedLabCount > 0)
    BEGIN
    
      SELECT DISTINCT 1 AS Tag, NULL AS Parent,
        A.AssignmentID AS [PL!1!P],
        NULL AS [R!2!I],
        NULL AS [R!2!SD],
        NULL AS [R!2!ED],
        NULL AS [R!2!!Element]
        FROM RPAssignment AS A -- Need INNER JOIN to weed out Assignments w/o TPD...
          INNER JOIN RPPlannedLabor AS TPD ON A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID
        WHERE A.PlanID = @strPlanID
      UNION ALL
      SELECT 2 AS Tag, 1 AS Parent,
        TPD.AssignmentID AS [PL!1!P],
        TimePhaseID AS [R!2!I],
        CONVERT(INT, TPD.StartDate) AS [R!2!SD],
        CONVERT(INT, TPD.EndDate) AS [R!2!ED],
        LTRIM(ISNULL(STR(TPD.PeriodHrs, 19, @intHrDecimals), '')) + '|' +  
        CASE WHEN TPD.CostRate = 0
             THEN ''
             ELSE CONVERT(VARCHAR, TPD.CostRate) END + '|' +  
        CASE WHEN TPD.BillingRate = 0
             THEN ''
             ELSE CONVERT(VARCHAR, TPD.BillingRate) END AS [R!2!!Element]
        FROM RPPlannedLabor AS TPD
        WHERE TPD.PlanID = @strPlanID AND TPD.AssignmentID IS NOT NULL
      UNION ALL -- ONLY Leaf Task Labor TPD.
      SELECT DISTINCT 1 AS Tag, NULL AS Parent,
        T.TaskID AS [PL!1!P],
        NULL AS [R!2!I],
        NULL AS [R!2!SD],
        NULL AS [R!2!ED],
        NULL AS [R!2!!Element]
        FROM RPTask AS T -- Need INNER JOIN to weed out Tasks w/o TPD...
          INNER JOIN RPPlannedLabor AS TPD ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL
        WHERE T.PlanID = @strPlanID AND T.ChildrenCount = 0 AND T.LabParentState = 'N'
      UNION ALL
      SELECT 2 AS Tag, 1 AS Parent,
        T.TaskID AS [PL!1!P],
        TimePhaseID AS [R!2!I],
        CONVERT(INT, TPD.StartDate) AS [R!2!SD],
        CONVERT(INT, TPD.EndDate) AS [R!2!ED],
        LTRIM(ISNULL(STR(TPD.PeriodHrs, 19, @intHrDecimals), '')) + '|' +  
        CASE WHEN TPD.CostRate = 0
             THEN ''
             ELSE CONVERT(VARCHAR, TPD.CostRate) END + '|' +  
        CASE WHEN TPD.BillingRate = 0
             THEN ''
             ELSE CONVERT(VARCHAR, TPD.BillingRate) END AS [R!2!!Element]
        FROM  RPTask AS T
          INNER JOIN RPPlannedLabor AS TPD ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID AND TPD.AssignmentID IS NULL
        WHERE T.PlanID = @strPlanID AND T.ChildrenCount = 0 AND T.LabParentState = 'N'
        ORDER BY [PL!1!P], Tag, [R!2!SD]
        FOR XML EXPLICIT
    
    END --IF (@intPlannedLabCount > 0)
    
  -->>> Get Actual Labor TPD (only when Retrieval Mode is not "Planned Only").
  
  IF (@strRetrievalMode != '1' AND @intJTDLabCount > 0)
    BEGIN
     
      SELECT DISTINCT 1 AS Tag, NULL AS Parent,
        ISNULL(TPD.AssignmentID, T.TaskID) AS [JL!1!P],
        NULL AS [R!2!SD],
        NULL AS [R!2!!Element]
        FROM RPTask AS T -- Need INNER JOIN to weed out Tasks w/o TPD...
          INNER JOIN @tabJTDLab AS TPD ON T.TaskID = TPD.TaskID
        WHERE T.PlanID = @strPlanID
      UNION ALL
      SELECT 2 AS Tag, 1 AS Parent,
        ISNULL(TPD.AssignmentID, TPD.TaskID) AS [JL!1!P], 
        CONVERT(INT, TPD.StartDate) AS [R!2!SD], 
        LTRIM(ISNULL(STR(TPD.PeriodHrs, 19, 2), '')) + '|' +  
        LTRIM(ISNULL(STR(TPD.PeriodCost, 19, 2), '')) + '|' +  
        LTRIM(ISNULL(STR(TPD.PeriodBill, 19, 2), '')) + '|' + 
        LTRIM(STR(ISNULL(TPD.PostedFlg, 1))) AS [R!2!!Element]
        FROM  @tabJTDLab AS TPD
        ORDER BY [JL!1!P], Tag, [R!2!SD]
        FOR XML EXPLICIT

    END --IF (@strRetrievalMode != '1' AND @intJTDLabCount > 0)
    
  -->>> Get Baseline Labor TPD (only when needed).

  IF (@strRetrievalMode = '3' AND @intBaselineLabCount > 0)
    BEGIN

      SELECT DISTINCT 1 AS Tag, NULL AS Parent,
        ISNULL(TPD.AssignmentID, T.TaskID) AS [BL!1!P],
        NULL AS [R!2!SD],
        NULL AS [R!2!!Element]
        FROM RPTask AS T -- Need INNER JOIN to weed out Tasks w/o TPD...
          INNER JOIN RPBaselineLabor AS TPD ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID 
        WHERE T.PlanID = @strPlanID
      UNION ALL
      SELECT 2 AS Tag, 1 AS Parent,
        ISNULL(TPD.AssignmentID, TPD.TaskID) AS [BL!1!P],
        CONVERT(INT, TPD.StartDate) AS [R!2!SD], 
        LTRIM(ISNULL(STR(TPD.PeriodHrs, 19, @intHrDecimals), '')) + '|' +  
        LTRIM(ISNULL(STR(TPD.PeriodCost, 19, @intAmtCostDecimals), '')) + '|' +  
        LTRIM(ISNULL(STR(TPD.PeriodBill, 19, @intAmtBillDecimals), '')) AS [R!2!!Element]
        FROM  RPBaselineLabor AS TPD
        WHERE TPD.PlanID = @strPlanID
        ORDER BY [BL!1!P], Tag, [R!2!SD]
        FOR XML EXPLICIT
        
    END --IF (@strRetrievalMode = '3' AND @intBaselineLabCount > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   
    
  -- Get Compensation Fee, Consultant Fee, and Reimbursable Allowance by period when applicable.

  IF (@strFeesByPeriod = 'Y')
    BEGIN
    
      IF (@intCompCount > 0)
        SELECT DISTINCT 1 AS Tag, NULL AS Parent,
          T.TaskID AS [CP!1!P],
          NULL AS [R!2!I],
          NULL AS [R!2!SD],
          NULL AS [R!2!ED],
          NULL AS [R!2!!Element]
          FROM RPTask AS T -- Need INNER JOIN to weed out Tasks w/o TPD...
            INNER JOIN RPCompensationFee AS TPD ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID
          WHERE T.PlanID = @strPlanID AND T.ChildrenCount = 0
        UNION ALL
          SELECT 2 AS Tag, 1 AS Parent,
            T.TaskID AS [CP!1!P],
            TPD.TimePhaseID AS [R!2!I],
            CONVERT(INT, TPD.StartDate) AS [R!2!SD],
            CONVERT(INT, TPD.EndDate) AS [R!2!ED],
            LTRIM(ISNULL(STR(TPD.PeriodCost, 19, @intAmtCostDecimals), '')) + '|' +  
            LTRIM(ISNULL(STR(TPD.PeriodBill, 19, @intAmtBillDecimals), '')) + '|' +  
            LTRIM(ISNULL(STR(TPD.PeriodDirLabCost, 19, @intAmtCostDecimals), '')) + '|' +  
            LTRIM(ISNULL(STR(TPD.PeriodDirLabBill, 19, @intAmtBillDecimals), '')) + '|' +  
            LTRIM(ISNULL(STR(TPD.PeriodDirExpCost, 19, @intAmtCostDecimals), '')) + '|' +  
            LTRIM(ISNULL(STR(TPD.PeriodDirExpBill, 19, @intAmtBillDecimals), '')) AS [R!2!!Element]
          FROM RPTask AS T -- Need INNER JOIN to weed out Tasks w/o TPD...
            INNER JOIN RPCompensationFee AS TPD ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID
          WHERE T.PlanID = @strPlanID AND T.ChildrenCount = 0
          ORDER BY [CP!1!P], Tag, [R!2!SD]
        FOR XML EXPLICIT        
      
      IF (@intConsCount > 0)
        SELECT DISTINCT 1 AS Tag, NULL AS Parent,
          T.TaskID AS [CS!1!P],
          NULL AS [R!2!I],
          NULL AS [R!2!SD],
          NULL AS [R!2!ED],
          NULL AS [R!2!!Element]
          FROM RPTask AS T -- Need INNER JOIN to weed out Tasks w/o TPD...
            INNER JOIN RPConsultantFee AS TPD ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID
          WHERE T.PlanID = @strPlanID AND T.ChildrenCount = 0
        UNION ALL
          SELECT 2 AS Tag, 1 AS Parent,
            T.TaskID AS [CS!1!P],
            TPD.TimePhaseID AS [R!2!I],
            CONVERT(INT, TPD.StartDate) AS [R!2!SD],
            CONVERT(INT, TPD.EndDate) AS [R!2!ED],
            LTRIM(ISNULL(STR(TPD.PeriodCost, 19, @intAmtCostDecimals), '')) + '|' +  
            LTRIM(ISNULL(STR(TPD.PeriodBill, 19, @intAmtBillDecimals), '')) AS [R!2!!Element]
          FROM RPTask AS T -- Need INNER JOIN to weed out Tasks w/o TPD...
            INNER JOIN RPConsultantFee AS TPD ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID
          WHERE T.PlanID = @strPlanID AND T.ChildrenCount = 0
          ORDER BY [CS!1!P], Tag, [R!2!SD]
        FOR XML EXPLICIT        

      IF (@intReimCount > 0)
        SELECT DISTINCT 1 AS Tag, NULL AS Parent,
          T.TaskID AS [RA!1!P],
          NULL AS [R!2!I],
          NULL AS [R!2!SD],
          NULL AS [R!2!ED],
          NULL AS [R!2!!Element]
          FROM RPTask AS T -- Need INNER JOIN to weed out Tasks w/o TPD...
            INNER JOIN RPReimbAllowance AS TPD ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID
          WHERE T.PlanID = @strPlanID AND T.ChildrenCount = 0
        UNION ALL
          SELECT 2 AS Tag, 1 AS Parent,
            T.TaskID AS [RA!1!P],
            TPD.TimePhaseID AS [R!2!I],
            CONVERT(INT, TPD.StartDate) AS [R!2!SD],
            CONVERT(INT, TPD.EndDate) AS [R!2!ED],
            LTRIM(ISNULL(STR(TPD.PeriodCost, 19, @intAmtCostDecimals), '')) + '|' +  
            LTRIM(ISNULL(STR(TPD.PeriodBill, 19, @intAmtBillDecimals), '')) + '|' +  
            LTRIM(ISNULL(STR(TPD.PeriodConCost, 19, @intAmtCostDecimals), '')) + '|' +  
            LTRIM(ISNULL(STR(TPD.PeriodConBill, 19, @intAmtBillDecimals), '')) + '|' +  
            LTRIM(ISNULL(STR(TPD.PeriodExpCost, 19, @intAmtCostDecimals), '')) + '|' +  
            LTRIM(ISNULL(STR(TPD.PeriodExpBill, 19, @intAmtBillDecimals), '')) AS [R!2!!Element]
          FROM RPTask AS T -- Need INNER JOIN to weed out Tasks w/o TPD...
            INNER JOIN RPReimbAllowance AS TPD ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID
          WHERE T.PlanID = @strPlanID AND T.ChildrenCount = 0
          ORDER BY [RA!1!P], Tag, [R!2!SD]
        FOR XML EXPLICIT        

    END --IF (@strFeesByPeriod = 'Y')

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   

  -- Get Task Dependencies.
  
  IF (@intTDCount > 0)
    BEGIN
    
      SELECT DISTINCT 1 AS Tag, NULL AS Parent, TaskID AS [TD!1!P], NULL AS [D!2!!Element] 
        FROM RPDependency WHERE PlanID = @strPlanID
      UNION ALL
      SELECT DISTINCT 2 AS Tag, 1 AS Parent, TaskID AS [TD!1!P], PredecessorID AS [D!2!!Element] 
        FROM RPDependency WHERE PlanID = @strPlanID
        ORDER BY [TD!1!P]
        FOR XML EXPLICIT
    
    END --IF (@intTDCount > 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   

  -- Get Task EV Pct.
  
  IF (@intEVCount > 0)
    BEGIN
    
      SELECT DISTINCT 1 AS Tag, NULL AS Parent, 
        TaskID AS [EV!1!P], 
        NULL AS [R!2!I],
        NULL AS [R!2!SD],
        NULL AS [R!2!ED],
        NULL AS [R!2!!Element] 
        FROM RPEVT WHERE PlanID = @strPlanID
      UNION ALL
      SELECT DISTINCT 2 AS Tag, 1 AS Parent, 
        TaskID AS [EV!1!P], 
        TimePhaseID AS [R!2!I],
        CONVERT(INT, StartDate) AS [R!2!SD],
        CONVERT(INT, EndDate) AS [R!2!ED],
        CONVERT(VARCHAR, PeriodPct) AS [R!2!!Element] 
        FROM RPEVT WHERE PlanID = @strPlanID
        ORDER BY [EV!1!P], Tag, [R!2!SD]
        FOR XML EXPLICIT
           
    END --IF (@intEVCount > 0
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get rates to be cached on the client side.
  
  -- Employee & Leaf Task (only when Rate Method is Labor Code) Rates.
  
  IF (@strGetCachedRate = 'Y')
    BEGIN
  
      --> Cost.

      IF (@siCostRtMethod = 2) -- Labor Rate Table
        BEGIN
        
          SELECT DISTINCT 1 AS Tag, NULL AS Parent,
            A.AssignmentID AS [CR!1!P],
            'C' AS [CR!1!T],
            NULL AS [R!2!ED!Hide],
            NULL AS [R!2!!Element]
            FROM RPAssignment AS A
              INNER JOIN BTRRTEmpls AS R ON A.ResourceID = R.Employee AND R.TableNo = @intCostRtTableNo 
            WHERE PlanID = @strPlanID
          UNION ALL
          SELECT 2 AS Tag, 1 AS Parent,
            AssignmentID AS [CR!1!P],
            'C' AS [CR!1!T],
            R.EffectiveDate AS [R!2!ED!Hide],
            CASE WHEN R.StartDate = '19000101' AND R.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '|' +
            CONVERT(VARCHAR, CONVERT(INT, R.StartDate)) + '|' +
            CONVERT(VARCHAR, CONVERT(INT, R.EndDate)) + '|' +
            LTRIM(STR(R.Rate, 19, 4)) [R!2!!Element]
            FROM RPAssignment AS A
              INNER JOIN BTRRTEmpls AS R ON A.ResourceID = R.Employee AND R.TableNo = @intCostRtTableNo 
            WHERE PlanID = @strPlanID
            ORDER BY [CR!1!P], Tag, [R!2!ED!Hide]
            FOR XML EXPLICIT
                      
        END -- IF (@siCostRtMethod = 2)
        
      ELSE IF (@siCostRtMethod = 3) -- Labor Category Table
        BEGIN

          SELECT DISTINCT 1 AS Tag, NULL AS Parent,
            A.AssignmentID AS [CR!1!P],
            'C' AS [CR!1!T],
            NULL AS [R!2!ED!Hide],
            NULL AS [R!2!!Element]
            FROM RPAssignment AS A
              INNER JOIN EM ON A.ResourceID = EM.Employee
		          LEFT JOIN BTRCTEmpls AS OC ON EM.Employee = OC.Employee AND OC.TableNo = @intCostRtTableNo
		          INNER JOIN BTRCTCats AS R ON R.TableNo = @intCostRtTableNo AND R.Category = ISNULL(OC.Category, EM.BillingCategory)
            WHERE PlanID = @strPlanID AND ResourceID IS NOT NULL
          UNION ALL   
          SELECT 2 AS Tag, 1 AS Parent,
            AssignmentID AS [CR!1!P],
            'C' AS [CR!1!T],
            R.EffectiveDate AS [R!2!ED!Hide],
            CASE WHEN R.StartDate = '19000101' AND R.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '|' +
            CONVERT(VARCHAR, CONVERT(INT, R.StartDate)) + '|' +
            CONVERT(VARCHAR, CONVERT(INT, R.EndDate)) + '|' +
            LTRIM(STR(R.Rate, 19, 4)) [R!2!!Element]
            FROM RPAssignment AS A
              INNER JOIN EM ON A.ResourceID = EM.Employee
		          LEFT JOIN BTRCTEmpls AS OC ON EM.Employee = OC.Employee AND OC.TableNo = @intCostRtTableNo
		          INNER JOIN BTRCTCats AS R ON R.TableNo = @intCostRtTableNo AND R.Category = ISNULL(OC.Category, EM.BillingCategory)
            WHERE PlanID = @strPlanID AND ResourceID IS NOT NULL
            ORDER BY [CR!1!P], Tag, [R!2!ED!Hide]
            FOR XML EXPLICIT
            
        END -- IF (@siCostRtMethod = 3)
        
      ELSE IF (@siCostRtMethod = 4) -- Labor Code Table
        BEGIN
        
          SELECT DISTINCT 1 AS Tag, NULL AS Parent, -- Employee
            A.AssignmentID AS [CR!1!P],
            'C' AS [CR!1!T],
            NULL AS [R!2!ED!Hide],
            NULL AS [R!2!!Element]
            FROM RPAssignment AS A
              INNER JOIN CFGFormat ON 1 = 1
              INNER JOIN BTRLTCodes ON TableNo = @intCostRtTableNo
                AND 
                  ((SUBSTRING(LaborCode, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
                    SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(LaborCode, LC1Start, LC1Length)) AND  
                   (SUBSTRING(LaborCode, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
                    SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(LaborCode, LC2Start, LC2Length)) AND  
                   (SUBSTRING(LaborCode, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
                    SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(LaborCode, LC3Start, LC3Length)) AND  
                   (SUBSTRING(LaborCode, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
                    SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(LaborCode, LC4Start, LC4Length)) AND  
                   (SUBSTRING(LaborCode, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
                    SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(LaborCode, LC5Start, LC5Length)))
            WHERE PlanID = @strPlanID AND ResourceID IS NOT NULL
              AND (A.LaborCode IS NOT NULL AND A.LaborCode != '<none>')
          UNION ALL
          SELECT DISTINCT 1 AS Tag, NULL AS Parent, -- Leaf Tasks
            T.TaskID AS [CR!1!P],
            'C' AS [CR!1!T],
            NULL AS [R!2!ED!Hide],
            NULL AS [R!2!!Element]
            FROM RPTask AS T
              INNER JOIN CFGFormat ON 1 = 1
              INNER JOIN BTRLTCodes ON TableNo = @intCostRtTableNo
                AND 
                  ((SUBSTRING(LaborCode, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
                    SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(LaborCode, LC1Start, LC1Length)) AND  
                   (SUBSTRING(LaborCode, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
                    SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(LaborCode, LC2Start, LC2Length)) AND  
                   (SUBSTRING(LaborCode, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
                    SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(LaborCode, LC3Start, LC3Length)) AND  
                   (SUBSTRING(LaborCode, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
                    SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(LaborCode, LC4Start, LC4Length)) AND  
                   (SUBSTRING(LaborCode, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
                    SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(LaborCode, LC5Start, LC5Length)))
            WHERE T.PlanID = @strPlanID AND T.ChildrenCount = 0 AND T.LabParentState = 'N'
              AND (T.LaborCode IS NOT NULL AND T.LaborCode != '<none>')
          UNION ALL
          SELECT 2 AS Tag, 1 AS Parent, -- Employee
            AssignmentID AS [CR!1!P],
            'C' AS [CR!1!T],
            RA.EffectiveDate AS [R!2!ED!Hide],
            CASE WHEN RA.StartDate = '19000101' AND RA.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '|' +
            CONVERT(VARCHAR, CONVERT(INT, StartDate)) + '|' +
            CONVERT(VARCHAR, CONVERT(INT, EndDate)) + '|' +
            LTRIM(STR(RA.Rate, 19, 4)) [R!2!!Element]
            FROM BTRLTCodes AS RA
              INNER JOIN
                (SELECT A.AssignmentID, MIN(LaborCodeMask) AS LaborCodeMask
                  FROM RPAssignment AS A
                    INNER JOIN CFGFormat ON 1 = 1
                    INNER JOIN BTRLTCodes ON TableNo = @intCostRtTableNo
                      AND 
                        ((SUBSTRING(LaborCode, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
                          SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(LaborCode, LC1Start, LC1Length)) AND  
                         (SUBSTRING(LaborCode, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
                          SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(LaborCode, LC2Start, LC2Length)) AND  
                         (SUBSTRING(LaborCode, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
                          SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(LaborCode, LC3Start, LC3Length)) AND  
                         (SUBSTRING(LaborCode, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
                          SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(LaborCode, LC4Start, LC4Length)) AND  
                         (SUBSTRING(LaborCode, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
                          SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(LaborCode, LC5Start, LC5Length)))
                  WHERE PlanID = @strPlanID AND ResourceID IS NOT NULL
                    AND (A.LaborCode IS NOT NULL AND A.LaborCode != '<none>')
                  GROUP BY AssignmentID
                ) AS RB ON RA.LaborCodeMask = RB.LaborCodeMask AND TableNo = @intCostRtTableNo
          UNION ALL
          SELECT 2 AS Tag, 1 AS Parent, -- Leaf Tasks
            TaskID AS [CR!1!P],
            'C' AS [CR!1!T],
            RA.EffectiveDate AS [R!2!ED!Hide],
            CASE WHEN RA.StartDate = '19000101' AND RA.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '|' +
            CONVERT(VARCHAR, CONVERT(INT, StartDate)) + '|' +
            CONVERT(VARCHAR, CONVERT(INT, EndDate)) + '|' +
            LTRIM(STR(RA.Rate, 19, 4)) [R!2!!Element]
            FROM BTRLTCodes AS RA
              INNER JOIN
                (SELECT T.TaskID, MIN(LaborCodeMask) AS LaborCodeMask
                  FROM RPTask AS T
                    INNER JOIN CFGFormat ON 1 = 1
                    INNER JOIN BTRLTCodes ON TableNo = @intCostRtTableNo
                      AND 
                        ((SUBSTRING(LaborCode, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
                          SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(LaborCode, LC1Start, LC1Length)) AND  
                         (SUBSTRING(LaborCode, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
                          SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(LaborCode, LC2Start, LC2Length)) AND  
                         (SUBSTRING(LaborCode, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
                          SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(LaborCode, LC3Start, LC3Length)) AND  
                         (SUBSTRING(LaborCode, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
                          SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(LaborCode, LC4Start, LC4Length)) AND  
                         (SUBSTRING(LaborCode, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
                          SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(LaborCode, LC5Start, LC5Length)))
                  WHERE T.PlanID = @strPlanID AND T.ChildrenCount = 0 AND T.LabParentState = 'N'
                    AND (T.LaborCode IS NOT NULL AND T.LaborCode != '<none>')
                  GROUP BY TaskID
                ) AS RB ON RA.LaborCodeMask = RB.LaborCodeMask AND TableNo = @intCostRtTableNo
              ORDER BY [CR!1!P], Tag, [R!2!ED!Hide]
              FOR XML EXPLICIT

        END -- IF (@siCostRtMethod = 4)
        
      --> Bill

      IF (@siBillingRtMethod = 2) -- Labor Rate Table
        BEGIN
              
          SELECT DISTINCT 1 AS Tag, NULL AS Parent,
            A.AssignmentID AS [CR!1!P],
            'B' AS [CR!1!T],
            NULL AS [R!2!ED!Hide],
            NULL AS [R!2!!Element]
            FROM RPAssignment AS A
              INNER JOIN BTRRTEmpls AS R ON A.ResourceID = R.Employee AND R.TableNo = @intBillingRtTableNo 
            WHERE PlanID = @strPlanID
          UNION ALL
          SELECT 2 AS Tag, 1 AS Parent,
            AssignmentID AS [CR!1!P],
            'B' AS [CR!1!T],
            R.EffectiveDate AS [R!2!ED!Hide],
            CASE WHEN R.StartDate = '19000101' AND R.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '|' +
            CONVERT(VARCHAR, CONVERT(INT, R.StartDate)) + '|' +
            CONVERT(VARCHAR, CONVERT(INT, R.EndDate)) + '|' +
            LTRIM(STR(R.Rate, 19, 4)) [R!2!!Element]
            FROM RPAssignment AS A
              INNER JOIN BTRRTEmpls AS R ON A.ResourceID = R.Employee AND R.TableNo = @intBillingRtTableNo 
            WHERE PlanID = @strPlanID
            ORDER BY [CR!1!P], Tag, [R!2!ED!Hide]
            FOR XML EXPLICIT
                
        END -- IF (@siBillingRtMethod = 2)
        
      ELSE IF (@siBillingRtMethod = 3) -- Labor Category Table
        BEGIN

          SELECT DISTINCT 1 AS Tag, NULL AS Parent,
            A.AssignmentID AS [CR!1!P],
            'B' AS [CR!1!T],
            NULL AS [R!2!ED!Hide],
            NULL AS [R!2!!Element]
            FROM RPAssignment AS A
              INNER JOIN EM ON A.ResourceID = EM.Employee
		          LEFT JOIN BTRCTEmpls AS OC ON EM.Employee = OC.Employee AND OC.TableNo = @intBillingRtTableNo
		          INNER JOIN BTRCTCats AS R ON R.TableNo = @intBillingRtTableNo AND R.Category = ISNULL(OC.Category, EM.BillingCategory)
            WHERE PlanID = @strPlanID AND ResourceID IS NOT NULL
          UNION ALL   
          SELECT 2 AS Tag, 1 AS Parent,
            AssignmentID AS [CR!1!P],
            'B' AS [CR!1!T],
            R.EffectiveDate AS [R!2!ED!Hide],
            CASE WHEN R.StartDate = '19000101' AND R.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '|' +
            CONVERT(VARCHAR, CONVERT(INT, R.StartDate)) + '|' +
            CONVERT(VARCHAR, CONVERT(INT, R.EndDate)) + '|' +
            LTRIM(STR(R.Rate, 19, 4)) [R!2!!Element]
            FROM RPAssignment AS A
              INNER JOIN EM ON A.ResourceID = EM.Employee
		          LEFT JOIN BTRCTEmpls AS OC ON EM.Employee = OC.Employee AND OC.TableNo = @intBillingRtTableNo
		          INNER JOIN BTRCTCats AS R ON R.TableNo = @intBillingRtTableNo AND R.Category = ISNULL(OC.Category, EM.BillingCategory)
            WHERE PlanID = @strPlanID AND ResourceID IS NOT NULL
            ORDER BY [CR!1!P], Tag, [R!2!ED!Hide]
            FOR XML EXPLICIT
        
        END -- IF (@siBillingRtMethod = 3)
        
      ELSE IF (@siBillingRtMethod = 4) -- Labor Code Table
        BEGIN
        
          SELECT DISTINCT 1 AS Tag, NULL AS Parent, -- Employee
            A.AssignmentID AS [CR!1!P],
            'B' AS [CR!1!T],
            NULL AS [R!2!ED!Hide],
            NULL AS [R!2!!Element]
            FROM RPAssignment AS A
              INNER JOIN CFGFormat ON 1 = 1
              INNER JOIN BTRLTCodes ON TableNo = @intBillingRtTableNo
                AND 
                  ((SUBSTRING(LaborCode, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
                    SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(LaborCode, LC1Start, LC1Length)) AND  
                   (SUBSTRING(LaborCode, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
                    SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(LaborCode, LC2Start, LC2Length)) AND  
                   (SUBSTRING(LaborCode, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
                    SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(LaborCode, LC3Start, LC3Length)) AND  
                   (SUBSTRING(LaborCode, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
                    SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(LaborCode, LC4Start, LC4Length)) AND  
                   (SUBSTRING(LaborCode, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
                    SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(LaborCode, LC5Start, LC5Length)))
            WHERE PlanID = @strPlanID AND ResourceID IS NOT NULL
              AND (A.LaborCode IS NOT NULL AND A.LaborCode != '<none>')
          UNION ALL
          SELECT DISTINCT 1 AS Tag, NULL AS Parent, -- Leaf Tasks
            T.TaskID AS [CR!1!P],
            'B' AS [CR!1!T],
            NULL AS [R!2!ED!Hide],
            NULL AS [R!2!!Element]
            FROM RPTask AS T
              INNER JOIN CFGFormat ON 1 = 1
              INNER JOIN BTRLTCodes ON TableNo = @intBillingRtTableNo
                AND 
                  ((SUBSTRING(LaborCode, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
                    SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(LaborCode, LC1Start, LC1Length)) AND  
                   (SUBSTRING(LaborCode, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
                    SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(LaborCode, LC2Start, LC2Length)) AND  
                   (SUBSTRING(LaborCode, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
                    SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(LaborCode, LC3Start, LC3Length)) AND  
                   (SUBSTRING(LaborCode, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
                    SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(LaborCode, LC4Start, LC4Length)) AND  
                   (SUBSTRING(LaborCode, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
                    SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(LaborCode, LC5Start, LC5Length)))
            WHERE T.PlanID = @strPlanID AND T.ChildrenCount = 0 AND T.LabParentState = 'N'
              AND (T.LaborCode IS NOT NULL AND T.LaborCode != '<none>')
          UNION ALL
          SELECT 2 AS Tag, 1 AS Parent,
            AssignmentID AS [CR!1!P],
            'B' AS [CR!1!T],
            RA.EffectiveDate AS [R!2!ED!Hide],
            CASE WHEN RA.StartDate = '19000101' AND RA.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '|' +
            CONVERT(VARCHAR, CONVERT(INT, StartDate)) + '|' +
            CONVERT(VARCHAR, CONVERT(INT, EndDate)) + '|' +
            LTRIM(STR(RA.Rate, 19, 4)) [R!2!!Element]
            FROM BTRLTCodes AS RA
              INNER JOIN
                (SELECT A.AssignmentID, MIN(LaborCodeMask) AS LaborCodeMask
                  FROM RPAssignment AS A
                    INNER JOIN CFGForMat ON 1 = 1
                    INNER JOIN BTRLTCodes ON TableNo = @intBillingRtTableNo
                      AND 
                        ((SUBSTRING(LaborCode, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
                          SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(LaborCode, LC1Start, LC1Length)) AND  
                         (SUBSTRING(LaborCode, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
                          SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(LaborCode, LC2Start, LC2Length)) AND  
                         (SUBSTRING(LaborCode, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
                          SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(LaborCode, LC3Start, LC3Length)) AND  
                         (SUBSTRING(LaborCode, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
                          SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(LaborCode, LC4Start, LC4Length)) AND  
                         (SUBSTRING(LaborCode, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
                          SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(LaborCode, LC5Start, LC5Length)))
                  WHERE PlanID = @strPlanID AND ResourceID IS NOT NULL
                    AND (A.LaborCode IS NOT NULL AND A.LaborCode != '<none>')
                  GROUP BY AssignmentID
                ) AS RB ON RA.LaborCodeMask = RB.LaborCodeMask AND TableNo = @intBillingRtTableNo
          UNION ALL
          SELECT 2 AS Tag, 1 AS Parent,
            TaskID AS [CR!1!P],
            'B' AS [CR!1!T],
            RA.EffectiveDate AS [R!2!ED!Hide],
            CASE WHEN RA.StartDate = '19000101' AND RA.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '|' +
            CONVERT(VARCHAR, CONVERT(INT, StartDate)) + '|' +
            CONVERT(VARCHAR, CONVERT(INT, EndDate)) + '|' +
            LTRIM(STR(RA.Rate, 19, 4)) [R!2!!Element]
            FROM BTRLTCodes AS RA
              INNER JOIN
                (SELECT T.TaskID, MIN(LaborCodeMask) AS LaborCodeMask
                  FROM RPTask AS T
                    INNER JOIN CFGForMat ON 1 = 1
                    INNER JOIN BTRLTCodes ON TableNo = @intBillingRtTableNo
                      AND 
                        ((SUBSTRING(LaborCode, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
                          SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(LaborCode, LC1Start, LC1Length)) AND  
                         (SUBSTRING(LaborCode, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
                          SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(LaborCode, LC2Start, LC2Length)) AND  
                         (SUBSTRING(LaborCode, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
                          SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(LaborCode, LC3Start, LC3Length)) AND  
                         (SUBSTRING(LaborCode, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
                          SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(LaborCode, LC4Start, LC4Length)) AND  
                         (SUBSTRING(LaborCode, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
                          SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(LaborCode, LC5Start, LC5Length)))
                  WHERE T.PlanID = @strPlanID AND T.ChildrenCount = 0 AND T.LabParentState = 'N'
                    AND (T.LaborCode IS NOT NULL AND T.LaborCode != '<none>')
                  GROUP BY TaskID
                ) AS RB ON RA.LaborCodeMask = RB.LaborCodeMask AND TableNo = @intBillingRtTableNo
              ORDER BY [CR!1!P], Tag, [R!2!ED!Hide]
              FOR XML EXPLICIT
              
        END -- IF (@siBillingRtMethod = 4)
            
      --> Generic Resource Rates.
      
      IF (@siGRMethod = 0) -- Labor Category
        BEGIN
        
          -- Cost
          
          SELECT DISTINCT 1 AS Tag, NULL AS Parent,
            A.AssignmentID AS [CR!1!P],
            'C' AS [CR!1!T],
            NULL AS [R!2!ED!Hide],
            NULL AS [R!2!!Element]
            FROM RPAssignment AS A
		          INNER JOIN BTRCTCats AS R ON R.TableNo = @intGenResTableNo AND A.Category = R.Category
            WHERE PlanID = @strPlanID AND ResourceID IS NULL
          UNION ALL   
          SELECT 2 AS Tag, 1 AS Parent,
            AssignmentID AS [CR!1!P],
            'C' AS [CR!1!T],
            R.EffectiveDate AS [R!2!ED!Hide],
            CASE WHEN R.StartDate = '19000101' AND R.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '|' +
            CONVERT(VARCHAR, CONVERT(INT, R.StartDate)) + '|' +
            CONVERT(VARCHAR, CONVERT(INT, R.EndDate)) + '|' +
            LTRIM(STR(R.Rate, 19, 4)) [R!2!!Element]
            FROM RPAssignment AS A
		          INNER JOIN BTRCTCats AS R ON R.TableNo = @intGenResTableNo AND A.Category = R.Category
            WHERE PlanID = @strPlanID AND ResourceID IS NULL
          ORDER BY [CR!1!P], Tag, [R!2!ED!Hide]
          FOR XML EXPLICIT

        -- Bill
          
          SELECT DISTINCT 1 AS Tag, NULL AS Parent,
            A.AssignmentID AS [CR!1!P],
            'B' AS [CR!1!T],
            NULL AS [R!2!ED!Hide],
            NULL AS [R!2!!Element]
            FROM RPAssignment AS A
		          INNER JOIN BTRCTCats AS R ON R.TableNo = @intGRBillTableNo AND A.Category = R.Category
            WHERE PlanID = @strPlanID AND ResourceID IS NULL
          UNION ALL   
          SELECT 2 AS Tag, 1 AS Parent,
            AssignmentID AS [CR!1!P],
            'B' AS [CR!1!T],
            R.EffectiveDate AS [R!2!ED!Hide],
            CASE WHEN R.StartDate = '19000101' AND R.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '|' +
            CONVERT(VARCHAR, CONVERT(INT, R.StartDate)) + '|' +
            CONVERT(VARCHAR, CONVERT(INT, R.EndDate)) + '|' +
            LTRIM(STR(R.Rate, 19, 4)) [R!2!!Element]
            FROM RPAssignment AS A
		          INNER JOIN BTRCTCats AS R ON R.TableNo = @intGRBillTableNo AND A.Category = R.Category
            WHERE PlanID = @strPlanID AND ResourceID IS NULL
          ORDER BY [CR!1!P], Tag, [R!2!ED!Hide]
          FOR XML EXPLICIT
        
        END -- IF (@siGRMethod = 0)
        
      ELSE IF (@siGRMethod = 1) -- Labor Code
        BEGIN
        
          -- Cost
          
          SELECT DISTINCT 1 AS Tag, NULL AS Parent,
            A.AssignmentID AS [CR!1!P],
            'C' AS [CR!1!T],
            NULL AS [R!2!ED!Hide],
            NULL AS [R!2!!Element]
            FROM RPAssignment AS A
              INNER JOIN CFGFormat ON 1 = 1
              INNER JOIN BTRLTCodes ON TableNo = @intGenResTableNo
                AND 
                  ((SUBSTRING(GRLBCD, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
                    SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(GRLBCD, LC1Start, LC1Length)) AND  
                   (SUBSTRING(GRLBCD, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
                    SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(GRLBCD, LC2Start, LC2Length)) AND  
                   (SUBSTRING(GRLBCD, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
                    SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(GRLBCD, LC3Start, LC3Length)) AND  
                   (SUBSTRING(GRLBCD, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
                    SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(GRLBCD, LC4Start, LC4Length)) AND  
                   (SUBSTRING(GRLBCD, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
                    SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(GRLBCD, LC5Start, LC5Length)))
            WHERE PlanID = @strPlanID AND ResourceID IS NULL
          UNION ALL
          SELECT 2 AS Tag, 1 AS Parent,
            AssignmentID AS [CR!1!P],
            'C' AS [CR!1!T],
            RA.EffectiveDate AS [R!2!ED!Hide],
            CASE WHEN RA.StartDate = '19000101' AND RA.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '|' +
            CONVERT(VARCHAR, CONVERT(INT, StartDate)) + '|' +
            CONVERT(VARCHAR, CONVERT(INT, EndDate)) + '|' +
            LTRIM(STR(RA.Rate, 19, 4)) [R!2!!Element]
            FROM BTRLTCodes AS RA
              INNER JOIN
                (SELECT A.AssignmentID, MIN(LaborCodeMask) AS LaborCodeMask
                  FROM RPAssignment AS A
                    INNER JOIN CFGForMat ON 1 = 1
                    INNER JOIN BTRLTCodes ON TableNo = @intGenResTableNo
                      AND 
                        ((SUBSTRING(GRLBCD, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
                          SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(GRLBCD, LC1Start, LC1Length)) AND  
                         (SUBSTRING(GRLBCD, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
                          SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(GRLBCD, LC2Start, LC2Length)) AND  
                         (SUBSTRING(GRLBCD, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
                          SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(GRLBCD, LC3Start, LC3Length)) AND  
                         (SUBSTRING(GRLBCD, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
                          SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(GRLBCD, LC4Start, LC4Length)) AND  
                         (SUBSTRING(GRLBCD, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
                          SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(GRLBCD, LC5Start, LC5Length)))
                WHERE PlanID = @strPlanID AND ResourceID IS NULL
                GROUP BY AssignmentID
              ) AS RB
            ON RA.LaborCodeMask = RB.LaborCodeMask AND TableNo = @intGenResTableNo
          ORDER BY [CR!1!P], Tag, [R!2!ED!Hide]
          FOR XML EXPLICIT

          -- Bill
          
          SELECT DISTINCT 1 AS Tag, NULL AS Parent,
            A.AssignmentID AS [CR!1!P],
            'B' AS [CR!1!T],
            NULL AS [R!2!ED!Hide],
            NULL AS [R!2!!Element]
            FROM RPAssignment AS A
              INNER JOIN CFGFormat ON 1 = 1
              INNER JOIN BTRLTCodes ON TableNo = @intGRBillTableNo
                AND 
                  ((SUBSTRING(GRLBCD, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
                    SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(GRLBCD, LC1Start, LC1Length)) AND  
                   (SUBSTRING(GRLBCD, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
                    SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(GRLBCD, LC2Start, LC2Length)) AND  
                   (SUBSTRING(GRLBCD, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
                    SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(GRLBCD, LC3Start, LC3Length)) AND  
                   (SUBSTRING(GRLBCD, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
                    SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(GRLBCD, LC4Start, LC4Length)) AND  
                   (SUBSTRING(GRLBCD, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
                    SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(GRLBCD, LC5Start, LC5Length)))
            WHERE PlanID = @strPlanID AND ResourceID IS NULL
          UNION ALL
          SELECT 2 AS Tag, 1 AS Parent,
            AssignmentID AS [CR!1!P],
            'B' AS [CR!1!T],
            RA.EffectiveDate AS [R!2!ED!Hide],
            CASE WHEN RA.StartDate = '19000101' AND RA.EndDate = '99990101' THEN 'N' ELSE 'Y' END + '|' +
            CONVERT(VARCHAR, CONVERT(INT, StartDate)) + '|' +
            CONVERT(VARCHAR, CONVERT(INT, EndDate)) + '|' +
            LTRIM(STR(RA.Rate, 19, 4)) [R!2!!Element]
            FROM BTRLTCodes AS RA
              INNER JOIN
                (SELECT A.AssignmentID, MIN(LaborCodeMask) AS LaborCodeMask
                  FROM RPAssignment AS A
                    INNER JOIN CFGForMat ON 1 = 1
                    INNER JOIN BTRLTCodes ON TableNo = @intGRBillTableNo
                      AND 
                        ((SUBSTRING(GRLBCD, LC1Start, LC1Length) LIKE SUBSTRING(LaborCodeMask, LC1Start, LC1Length) OR  
                          SUBSTRING(LaborCodeMask, LC1Start, LC1Length) LIKE SUBSTRING(GRLBCD, LC1Start, LC1Length)) AND  
                         (SUBSTRING(GRLBCD, LC2Start, LC2Length) LIKE SUBSTRING(LaborCodeMask, LC2Start, LC2Length) OR  
                          SUBSTRING(LaborCodeMask, LC2Start, LC2Length) LIKE SUBSTRING(GRLBCD, LC2Start, LC2Length)) AND  
                         (SUBSTRING(GRLBCD, LC3Start, LC3Length) LIKE SUBSTRING(LaborCodeMask, LC3Start, LC3Length) OR  
                          SUBSTRING(LaborCodeMask, LC3Start, LC3Length) LIKE SUBSTRING(GRLBCD, LC3Start, LC3Length)) AND  
                         (SUBSTRING(GRLBCD, LC4Start, LC4Length) LIKE SUBSTRING(LaborCodeMask, LC4Start, LC4Length) OR  
                          SUBSTRING(LaborCodeMask, LC4Start, LC4Length) LIKE SUBSTRING(GRLBCD, LC4Start, LC4Length)) AND  
                         (SUBSTRING(GRLBCD, LC5Start, LC5Length) LIKE SUBSTRING(LaborCodeMask, LC5Start, LC5Length) OR  
                          SUBSTRING(LaborCodeMask, LC5Start, LC5Length) LIKE SUBSTRING(GRLBCD, LC5Start, LC5Length)))
                WHERE PlanID = @strPlanID AND ResourceID IS NULL
                GROUP BY AssignmentID
              ) AS RB
            ON RA.LaborCodeMask = RB.LaborCodeMask AND TableNo = @intGRBillTableNo
          ORDER BY [CR!1!P], Tag, [R!2!ED!Hide]
          FOR XML EXPLICIT
        
        END -- IF (@siGRMethod = 1)
        
    END -- IF (@strGetCachedRate = 'Y')

  SET @totalTime = DATEDIFF(ms,@startTime,GetDate())

  Select 1 Tag,null Parent,@totalTime [time!1] for xml explicit

  SET NOCOUNT OFF

END -- pmGetLabTPD
GO
