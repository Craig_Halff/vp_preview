SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteMktCampaign] @CampaignID varchar(32)
AS
	DELETE FROM CustomProposalMktCampaign WHERE CampaignID = @CampaignID
	DELETE FROM CustomProposalMktCampaignGraphics WHERE CampaignID = @CampaignID
	DELETE FROM MktCampaignContactAssoc WHERE CampaignID = @CampaignID
	DELETE FROM MktCampaignCustomTabFields WHERE CampaignID = @CampaignID
	DELETE FROM MktCampaignFileLinks WHERE CampaignID = @CampaignID
	DELETE FROM MktCampaignLeads WHERE CampaignID = @CampaignID
	--DELETE FROM MktCampaignOppAssoc WHERE CampaignID = @CampaignID
	DELETE FROM MktCampaignProjectAssoc WHERE CampaignID = @CampaignID
	DELETE FROM MktCampaignProjectAssocTemplate WHERE CampaignID = @CampaignID
    DELETE FROM MktCampaignClientAssoc WHERE CampaignID = @CampaignID   
	EXEC DeleteCustomGridTabData 'MktCampaign',@CampaignID, '', ''
	UPDATE Activity SET CampaignID = null WHERE CampaignID = @CampaignID
	UPDATE WorkflowActivity SET CampaignID = null WHERE CampaignID = @CampaignID
GO
