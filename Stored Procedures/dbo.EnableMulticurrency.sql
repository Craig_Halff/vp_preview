SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[EnableMulticurrency]
    @FunctionalCurrencyCode Nvarchar(3)
AS
BEGIN
	begin tran

	UPDATE APApprovalMaster SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE apControl SET DefaultCurrencyCode = @FunctionalCurrencyCode
	UPDATE apMaster SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE apMaster SET PaymentCurrencyCode = @FunctionalCurrencyCode
	UPDATE BIED SET TransactionCurrencyCode = @FunctionalCurrencyCode
	UPDATE BILD SET EMCurrencyCode = @FunctionalCurrencyCode
	UPDATE billInvSums SET TaxCurrencyCode = @FunctionalCurrencyCode
	UPDATE billTaxDetail SET TaxCurrencyCode = @FunctionalCurrencyCode
	UPDATE BTRCT SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE BTRLT SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE BTROT SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE BTRRT SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE CA SET AccountCurrencyCode = @FunctionalCurrencyCode WHERE EXISTS (SELECT 'x' FROM CFGBanks WHERE CFGBanks.Account = CA.Account)
	UPDATE CA SET CustomCurrencyCode = @FunctionalCurrencyCode
	UPDATE CFGBillTaxesData SET TaxCurrencyCode = @FunctionalCurrencyCode
	UPDATE CFGConsolidationGroups SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE CFGCreditCard SET CurrencyCode = @FunctionalCurrencyCode
	INSERT INTO FW_CFGEnabledCurrencies (Company, CurrencyCode) SELECT Company, @FunctionalCurrencyCode From CFGMainData
	UPDATE CFGMainData SET FunctionalCurrencyCode = @FunctionalCurrencyCode
	UPDATE CFGServiceProfileData SET CurrencyCode = @FunctionalCurrencyCode WHERE SystemProfile = 'N'
	UPDATE CFGServiceProfileData SET CurrencyCode = 'EUR' WHERE SystemProfile = 'Y' -- pre-configured system profiles reserved for HOAI (Germany)
	UPDATE FW_CFGSystem SET MulticurrencyEnabled = 'Y'
	UPDATE Contacts SET CustomCurrencyCode = @FunctionalCurrencyCode
	UPDATE CostCT SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE CostLT SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE CostRT SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE CR SET PaymentCurrencyCode = @FunctionalCurrencyCode
	UPDATE CreditCardTransaction SET AmountCurrencyCode = @FunctionalCurrencyCode
	UPDATE cvControl SET DefaultCurrencyCode = @FunctionalCurrencyCode
	UPDATE cvMaster SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE ekDetail SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE ekMaster SET DefaultCurrencyCode = @FunctionalCurrencyCode
	UPDATE EmployeeRealizationWk SET BillingCurrencyCode = @FunctionalCurrencyCode
	UPDATE Equipment SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE exControl SET DefaultCurrencyCode = @FunctionalCurrencyCode
	UPDATE exDetail SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE exMaster SET DefaultCurrencyCode = @FunctionalCurrencyCode
	UPDATE ICBillExpDetail SET ICBillInvoiceCurrencyCode = @FunctionalCurrencyCode
	UPDATE ICBillingWK SET CreditFunctionalCurrency = @FunctionalCurrencyCode
	UPDATE ICBillingWK SET DebitFunctionalCurrency = @FunctionalCurrencyCode
	UPDATE ICBillInvMaster SET ICBillInvoiceCurrencyCode = @FunctionalCurrencyCode
	UPDATE ICBillLabDetail SET ICBillInvoiceCurrencyCode = @FunctionalCurrencyCode
	UPDATE ICBillTaxDetail SET ICBillInvoiceCurrencyCode = @FunctionalCurrencyCode
	UPDATE jeControl SET DefaultCurrencyCode = @FunctionalCurrencyCode
	UPDATE jeMaster SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE LD SET EMCurrencyCode = @FunctionalCurrencyCode
	UPDATE LedgerAP SET TransactionCurrencyCode = @FunctionalCurrencyCode
	UPDATE LedgerAR SET TransactionCurrencyCode = @FunctionalCurrencyCode
	UPDATE LedgerEX SET TransactionCurrencyCode = @FunctionalCurrencyCode
	UPDATE LedgerMisc SET TransactionCurrencyCode = @FunctionalCurrencyCode
	UPDATE miControl SET DefaultCurrencyCode = @FunctionalCurrencyCode
	UPDATE miMaster SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE MktCampaign SET CustomCurrencyCode = @FunctionalCurrencyCode
	UPDATE PNPlan SET CostCurrencyCode = @FunctionalCurrencyCode
	UPDATE PNPlan SET BillingCurrencyCode = @FunctionalCurrencyCode
	UPDATE POCommitment SET TransactionCurrencyCode = @FunctionalCurrencyCode
	UPDATE POMaster SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE POPQMaster SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE POPRMaster SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE POVoucherMaster SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE POVoucherMaster SET PaymentCurrencyCode = @FunctionalCurrencyCode
	UPDATE PR SET ProjectCurrencyCode = @FunctionalCurrencyCode
	UPDATE PR SET BillingCurrencyCode = @FunctionalCurrencyCode
	UPDATE prControl SET DefaultCurrencyCode = @FunctionalCurrencyCode
	UPDATE PRDefaults SET ProjectCurrencyCode = NULL
	UPDATE PRDefaults SET BillingCurrencyCode = NULL
	UPDATE prMaster SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE PRTemplate SET ProjectCurrencyCode = @FunctionalCurrencyCode
	UPDATE PRTemplate SET BillingCurrencyCode = @FunctionalCurrencyCode
	UPDATE RPPlan SET CostCurrencyCode = @FunctionalCurrencyCode
	UPDATE RPPlan SET BillingCurrencyCode = @FunctionalCurrencyCode
	UPDATE TextLibrary SET CustomCurrencyCode = @FunctionalCurrencyCode
	UPDATE UNTable SET ProjectCurrencyCode = @FunctionalCurrencyCode
	UPDATE UNTable SET BillingCurrencyCode = @FunctionalCurrencyCode
	UPDATE CLENDOR SET CustomCurrencyCode = @FunctionalCurrencyCode
	UPDATE VendorItem SET LastPriceCurrencyCode = @FunctionalCurrencyCode
	UPDATE VO SET CurrencyCode = @FunctionalCurrencyCode
	UPDATE VO SET PaymentCurrencyCode = @FunctionalCurrencyCode
	--OriginalPaymentCurrencyCode only applies to AP/L transactions associated with vouchers:
	UPDATE BIED SET 
	BIED.OriginalPaymentCurrencyCode = @FunctionalCurrencyCode
	FROM BIED Inner Join VO ON BIED.Vendor = VO.Vendor and BIED.Voucher = VO.Voucher
	WHERE BIED.TransType = 'AP' and BIED.SubType = 'L'
	UPDATE LedgerAP SET 
	LedgerAP.OriginalPaymentCurrencyCode = @FunctionalCurrencyCode
	FROM LedgerAP Inner Join VO ON LedgerAP.Vendor = VO.Vendor and LedgerAP.Voucher = VO.Voucher
	WHERE LedgerAP.TransType = 'AP' and LedgerAP.SubType = 'L'

	-- the following UPDATEs to RPGrd and RPGridCfg are needed to trigger resetting of RP grid configurations:
    UPDATE RPGrd SET CreateDate = '19900101'
    UPDATE RPGridCfg SET CreateDate = '19900101'

	
	commit transaction
END
GO
