SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_LoadEmployeeObjects] ( @orderByName bit, @rolesToInclude nvarchar(max)= '')
             AS EXEC spCCG_PAT_LoadEmployeeObjects @orderByName,@rolesToInclude
GO
