SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_EI_GetVisionStageFlows]
	@VISION_LANGUAGE varchar(10)
AS
BEGIN
	-- EXEC [dbo].[spCCG_EI_GetVisionStageFlows] ''
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	declare @mainLang varchar(10) = (select top 1 UICultureName
		from FW_CustomColumnValuesData 
		where InfoCenterArea = 'Projects' and ColName = 'CustInvoiceStageFlow' 
		group by UICultureName
		order by count(*) desc)
	select *
		from FW_CustomColumnValuesData  
		where InfoCenterArea = 'Projects' and ColName = 'CustInvoiceStageFlow' and 
			((isnull(@VISION_LANGUAGE, '') <> '' and UICultureName = @VISION_LANGUAGE) or (isnull(@VISION_LANGUAGE, '') = '' and UICultureName = @mainLang))
		order by Seq
END
GO
