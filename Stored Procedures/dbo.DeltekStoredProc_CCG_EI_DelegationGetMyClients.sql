SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_DelegationGetMyClients] ( @employee nvarchar(20))
             AS EXEC spCCG_EI_DelegationGetMyClients @employee
GO
