SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyConvertTax]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg Nvarchar(1000) output,
@UserName Nvarchar(32) = 'VisionKeyConvert' -- Add by DP 4.0
,@Period integer = 0,
@EntityType Nvarchar(20)
as
begin -- Procedure
	set nocount on
	declare @OldValue Nvarchar(20)
	declare @NewValue Nvarchar(20)
	Declare @Existing integer
	declare @Entity Nvarchar(20)
	declare @length integer
	declare @CvtType integer
	declare @KeysFetch integer
	declare @RetVal integer
	declare @diag smallint
	declare @Errmsg2 Nvarchar(1000),
			@custlabel	Nvarchar(40),
			@Custlabelplural Nvarchar(40)
	declare @message nvarchar(max)
	set @diag = 0
	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'Tax'
	set @length = 10
	set @Existing = 0
	set @KeysFetch = 0

	execute InsertKeyCvtDriver @Entity -- populate driver table

-- Add by DP 4.0
	delete from keyconvertDriver where entity = N'Tax' and TableName = N'CFGBillTaxesData' and ColumnName =  N'Code'
	delete from keyconvertDriver where entity = N'Tax' and TableName = N'CFGBillTaxesDescriptions'
	DECLARE @OldName Nvarchar(40)
	DECLARE @NewName Nvarchar(40)
	DECLARE @PKey Nvarchar(50)
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0
--

	select @custlabel = 'Tax Code',
			 @Custlabelplural = 'Tax Codes'
	declare KeysCursor cursor for
		select OldKey, NewKey, Pkey from KeyConvertWork where Entity = @EntityType
	open KeysCursor
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue, 
		@PKey

	set @KeysFetch = @@Fetch_Status

While (@KeysFetch = 0)
	begin
		begin tran
-- Add by DP 4.0
		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany Nvarchar(14)
			DECLARE @RecordCount integer

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				set @RecordCount = (select count(*) from KeyConvertWork where Entity = @EntityType)

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),'KC','Y',  @custlabel + ' Key Convert ('+rtrim(convert(Nvarchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, @custlabel + ' Key Convert', @custlabel);
			end

			Set @PostControlFlag = 1
		end

		set @Existing = 0
		select @Existing =  1, @NewName = Description from CFGBillTaxes where Code = @NewValue
		select @OldName = Description from CFGBillTaxes where Code = @OldValue
--

	If (@Existing = 1)
		begin
		  if (@DeleteExisting = 0)
			begin
			SET @message = dbo.GetMessage('MsgNumAnExistingConvWillNotCont',@custlabel,@NewValue,@custlabel,@custLabelPlural,'','','','','')
			RAISERROR(@message,16,3)
				close keysCursor
				deallocate keysCursor
				if (@@Trancount > 0)
				   ROLLBACK TRANSACTION
				return(50002) -- user defined error
			end
			if (@NewValue = @OldValue)
				begin
				SET @message = dbo.GetMessage('MsgAreSameNumConvNotCont',@custlabelPlural,'','','','','','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end
			declare @MulticompanyEnabled Nvarchar(1)
			declare @MulticurrencyEnabled Nvarchar(1)
			declare @NewFunctionalCurrencyCode Nvarchar(3)
			declare @OldFunctionalCurrencyCode Nvarchar(3)
			declare @oldCompay Nvarchar(14)
			declare @newCompay Nvarchar(14)
			select @MulticompanyEnabled = MulticompanyEnabled,
		   		 @MulticurrencyEnabled = MulticurrencyEnabled from FW_CFGSystem
			
			if (@MulticompanyEnabled = 'Y')
				begin
					select @oldCompay = old.Company,
							 @newCompay = new.Company from CFGBillTaxes old,CFGBillTaxes new, cfgformat 
							where old.Code = @OldValue and new.Code = @NewValue
					if (@oldCompay <> @newCompay)
						begin
						SET @message = dbo.GetMessage('MsgWithMultiCmpYouCannotMerge',@custlabelPlural,@custlabel,@OldValue,@oldCompay,@custlabel,@NewValue,@newCompay,'','')
						RAISERROR(@message,16,3)
							close keysCursor
							deallocate keysCursor
							if (@@Trancount > 0)
							   ROLLBACK TRANSACTION
							return(50003) -- user defined error
						end
					end

/*new for 7.2 MTC*/
--BTDefaultsTaxCodes (DefaultType, Company, TaxCode)
			delete from BTDefaultsTaxCodes where TaxCode = @OldValue 
				and exists (select 'x' from BTDefaultsTaxCodes new where new.TaxCode = @newValue and new.DefaultType = BTDefaultsTaxCodes.DefaultType and new.Company = BTDefaultsTaxCodes.Company)
--apDetailTax (Batch, MasterPKey, PKey, TaxCode)
			Update apDetailTax Set apDetailTax.TaxAmount = apDetailTax.TaxAmount + old.TaxAmount, apDetailTax.TaxAmountProjectFunctionalCurrency = apDetailTax.TaxAmountProjectFunctionalCurrency + old.TaxAmountProjectFunctionalCurrency
				from apDetailTax old inner join apDetailTax on (old.Batch = apDetailTax.Batch and old.MasterPKey = apDetailTax.MasterPKey and old.PKey = apDetailTax.PKey )
				Where apDetailTax.TaxCode = @newValue And old.TaxCode = @OldValue
			delete from apDetailTax where TaxCode = @OldValue 
				and exists (select 'x' from apDetailTax new where new.TaxCode = @newValue and new.Batch = apDetailTax.Batch	and new.MasterPKey = apDetailTax.MasterPKey	and new.PKey = apDetailTax.PKey	)
--cdDetailTax (Batch, CheckNo, PKey, TaxCode)
			Update cdDetailTax Set cdDetailTax.TaxAmount = cdDetailTax.TaxAmount + old.TaxAmount
				from cdDetailTax old inner join cdDetailTax on (old.Batch = cdDetailTax.Batch and old.CheckNo = cdDetailTax.CheckNo and old.PKey = cdDetailTax.PKey )
				Where cdDetailTax.TaxCode = @newValue And old.TaxCode = @OldValue
			delete from cdDetailTax where TaxCode = @OldValue 
				and exists (select 'x' from cdDetailTax new where new.TaxCode = @newValue and new.Batch = cdDetailTax.Batch	and new.CheckNo = cdDetailTax.CheckNo and new.PKey = cdDetailTax.PKey )
--cvDetailTax (Batch, CheckNo, PKey, TaxCode)
			Update cvDetailTax Set cvDetailTax.TaxAmount = cvDetailTax.TaxAmount + old.TaxAmount
				from cvDetailTax old inner join cvDetailTax on (old.Batch = cvDetailTax.Batch and old.CheckNo = cvDetailTax.CheckNo and old.PKey = cvDetailTax.PKey )
				Where cvDetailTax.TaxCode = @newValue And old.TaxCode = @OldValue
			delete from cvDetailTax where TaxCode = @OldValue 
				and exists (select 'x' from cvDetailTax new where new.TaxCode = @newValue and new.Batch = cvDetailTax.Batch	and new.CheckNo = cvDetailTax.CheckNo and new.PKey = cvDetailTax.PKey )
--exDetailTax (Batch, MasterPKey, PKey, TaxCode)
			Update exDetailTax Set exDetailTax.TaxAmount = exDetailTax.TaxAmount + old.TaxAmount
				from exDetailTax old inner join exDetailTax on (old.Batch = exDetailTax.Batch and old.MasterPKey = exDetailTax.MasterPKey and old.PKey = exDetailTax.PKey )
				Where exDetailTax.TaxCode = @newValue And old.TaxCode = @OldValue
			delete from exDetailTax where TaxCode = @OldValue 
				and exists (select 'x' from exDetailTax new where new.TaxCode = @newValue and new.Batch = exDetailTax.Batch	and new.MasterPKey = exDetailTax.MasterPKey	and new.PKey = exDetailTax.PKey	)
--jeDetailTax (Batch, RefNo, PKey, TaxCode)
			Update jeDetailTax Set jeDetailTax.TaxDebitAmount = jeDetailTax.TaxDebitAmount + old.TaxDebitAmount, jeDetailTax.TaxCreditAmount = jeDetailTax.TaxCreditAmount + old.TaxCreditAmount
				from jeDetailTax old inner join jeDetailTax on (old.Batch = jeDetailTax.Batch and old.RefNo = jeDetailTax.RefNo and old.PKey = jeDetailTax.PKey )
				Where jeDetailTax.TaxCode = @newValue And old.TaxCode = @OldValue
			delete from jeDetailTax where TaxCode = @OldValue 
				and exists (select 'x' from jeDetailTax new where new.TaxCode = @newValue and new.Batch = jeDetailTax.Batch	and new.RefNo = jeDetailTax.RefNo and new.PKey = jeDetailTax.PKey )
--ekDetailTax (Employee, ReportDate, ReportName, Seq, TaxCode)
			Update ekDetailTax Set ekDetailTax.TaxAmount = ekDetailTax.TaxAmount + old.TaxAmount
				from ekDetailTax old inner join ekDetailTax on (old.Employee = ekDetailTax.Employee and old.ReportDate = ekDetailTax.ReportDate and old.ReportName = ekDetailTax.ReportName and old.Seq = ekDetailTax.Seq )
				Where ekDetailTax.TaxCode = @newValue And old.TaxCode = @OldValue
			delete from ekDetailTax where TaxCode = @OldValue 
				and exists (select 'x' from ekDetailTax new where new.TaxCode = @newValue and new.Employee = ekDetailTax.Employee	and new.ReportDate = ekDetailTax.ReportDate and new.ReportName = ekDetailTax.ReportName and new.Seq = ekDetailTax.Seq )
--BIEDTax		(Period, PostSeq, PKey, TaxCode)
			Update BIEDTax Set BIEDTax.TaxAmount = BIEDTax.TaxAmount + old.TaxAmount, BIEDTax.TaxCBAmount = BIEDTax.TaxCBAmount + old.TaxCBAmount, 
				BIEDTax.AmountTaxCurrency = BIEDTax.AmountTaxCurrency + old.AmountTaxCurrency, BIEDTax.CBAmountTaxCurrency = BIEDTax.CBAmountTaxCurrency + old.CBAmountTaxCurrency, 
				BIEDTax.TaxAmountTaxCurrency = BIEDTax.TaxAmountTaxCurrency + old.TaxAmountTaxCurrency, BIEDTax.TaxCBAmountTaxCurrency = BIEDTax.TaxCBAmountTaxCurrency + old.TaxCBAmountTaxCurrency, 
				BIEDTax.TaxAmountFunctionalCurrency = BIEDTax.TaxAmountFunctionalCurrency + old.TaxAmountFunctionalCurrency, BIEDTax.TaxCBAmountFunctionalCurrency = BIEDTax.TaxCBAmountFunctionalCurrency + old.TaxCBAmountFunctionalCurrency, 
				BIEDTax.TaxAmountSourceCurrency = BIEDTax.TaxAmountSourceCurrency + old.TaxAmountSourceCurrency, BIEDTax.NonRecoverTaxPercent = BIEDTax.NonRecoverTaxPercent + old.NonRecoverTaxPercent
				from BIEDTax old inner join BIEDTax on (old.Period = BIEDTax.Period and old.PostSeq = BIEDTax.PostSeq and old.PKey = BIEDTax.PKey )
				Where BIEDTax.TaxCode = @newValue And old.TaxCode = @OldValue
			delete from BIEDTax where TaxCode = @OldValue 
				and exists (select 'x' from BIEDTax new where new.TaxCode = @newValue and new.Period = BIEDTax.Period	and new.PostSeq = BIEDTax.PostSeq	and new.PKey = BIEDTax.PKey	)
--LedgerAPTax	(Period, PostSeq, PKey, TaxCode)
			Update LedgerAPTax Set LedgerAPTax.TaxAmount = LedgerAPTax.TaxAmount + old.TaxAmount, LedgerAPTax.TaxCBAmount = LedgerAPTax.TaxCBAmount + old.TaxCBAmount, 
				LedgerAPTax.AmountTaxCurrency = LedgerAPTax.AmountTaxCurrency + old.AmountTaxCurrency, LedgerAPTax.CBAmountTaxCurrency = LedgerAPTax.CBAmountTaxCurrency + old.CBAmountTaxCurrency, 
				LedgerAPTax.TaxAmountTaxCurrency = LedgerAPTax.TaxAmountTaxCurrency + old.TaxAmountTaxCurrency, LedgerAPTax.TaxCBAmountTaxCurrency = LedgerAPTax.TaxCBAmountTaxCurrency + old.TaxCBAmountTaxCurrency, 
				LedgerAPTax.TaxAmountFunctionalCurrency = LedgerAPTax.TaxAmountFunctionalCurrency + old.TaxAmountFunctionalCurrency, LedgerAPTax.TaxCBAmountFunctionalCurrency = LedgerAPTax.TaxCBAmountFunctionalCurrency + old.TaxCBAmountFunctionalCurrency, 
				LedgerAPTax.TaxAmountSourceCurrency = LedgerAPTax.TaxAmountSourceCurrency + old.TaxAmountSourceCurrency, LedgerAPTax.NonRecoverTaxPercent = LedgerAPTax.NonRecoverTaxPercent + old.NonRecoverTaxPercent
				from LedgerAPTax old inner join LedgerAPTax on (old.Period = LedgerAPTax.Period and old.PostSeq = LedgerAPTax.PostSeq and old.PKey = LedgerAPTax.PKey )
				Where LedgerAPTax.TaxCode = @newValue And old.TaxCode = @OldValue
			delete from LedgerAPTax where TaxCode = @OldValue 
				and exists (select 'x' from LedgerAPTax new where new.TaxCode = @newValue and new.Period = LedgerAPTax.Period	and new.PostSeq = LedgerAPTax.PostSeq	and new.PKey = LedgerAPTax.PKey	)
--LedgerEXTax	(Period, PostSeq, PKey, TaxCode)
			Update LedgerEXTax Set LedgerEXTax.TaxAmount = LedgerEXTax.TaxAmount + old.TaxAmount, LedgerEXTax.TaxCBAmount = LedgerEXTax.TaxCBAmount + old.TaxCBAmount, 
				LedgerEXTax.AmountTaxCurrency = LedgerEXTax.AmountTaxCurrency + old.AmountTaxCurrency, LedgerEXTax.CBAmountTaxCurrency = LedgerEXTax.CBAmountTaxCurrency + old.CBAmountTaxCurrency, 
				LedgerEXTax.TaxAmountTaxCurrency = LedgerEXTax.TaxAmountTaxCurrency + old.TaxAmountTaxCurrency, LedgerEXTax.TaxCBAmountTaxCurrency = LedgerEXTax.TaxCBAmountTaxCurrency + old.TaxCBAmountTaxCurrency, 
				LedgerEXTax.TaxAmountFunctionalCurrency = LedgerEXTax.TaxAmountFunctionalCurrency + old.TaxAmountFunctionalCurrency, LedgerEXTax.TaxCBAmountFunctionalCurrency = LedgerEXTax.TaxCBAmountFunctionalCurrency + old.TaxCBAmountFunctionalCurrency, 
				LedgerEXTax.TaxAmountSourceCurrency = LedgerEXTax.TaxAmountSourceCurrency + old.TaxAmountSourceCurrency, LedgerEXTax.NonRecoverTaxPercent = LedgerEXTax.NonRecoverTaxPercent + old.NonRecoverTaxPercent
				from LedgerEXTax old inner join LedgerEXTax on (old.Period = LedgerEXTax.Period and old.PostSeq = LedgerEXTax.PostSeq and old.PKey = LedgerEXTax.PKey )
				Where LedgerEXTax.TaxCode = @newValue And old.TaxCode = @OldValue
			delete from LedgerEXTax where TaxCode = @OldValue 
				and exists (select 'x' from LedgerEXTax new where new.TaxCode = @newValue and new.Period = LedgerEXTax.Period	and new.PostSeq = LedgerEXTax.PostSeq	and new.PKey = LedgerEXTax.PKey	)
--LedgerMiscTax (Period, PostSeq, PKey, TaxCode)
			Update LedgerMiscTax Set LedgerMiscTax.TaxAmount = LedgerMiscTax.TaxAmount + old.TaxAmount, LedgerMiscTax.TaxCBAmount = LedgerMiscTax.TaxCBAmount + old.TaxCBAmount, 
				LedgerMiscTax.AmountTaxCurrency = LedgerMiscTax.AmountTaxCurrency + old.AmountTaxCurrency, LedgerMiscTax.CBAmountTaxCurrency = LedgerMiscTax.CBAmountTaxCurrency + old.CBAmountTaxCurrency, 
				LedgerMiscTax.TaxAmountTaxCurrency = LedgerMiscTax.TaxAmountTaxCurrency + old.TaxAmountTaxCurrency, LedgerMiscTax.TaxCBAmountTaxCurrency = LedgerMiscTax.TaxCBAmountTaxCurrency + old.TaxCBAmountTaxCurrency, 
				LedgerMiscTax.TaxAmountFunctionalCurrency = LedgerMiscTax.TaxAmountFunctionalCurrency + old.TaxAmountFunctionalCurrency, LedgerMiscTax.TaxCBAmountFunctionalCurrency = LedgerMiscTax.TaxCBAmountFunctionalCurrency + old.TaxCBAmountFunctionalCurrency, 
				LedgerMiscTax.TaxAmountSourceCurrency = LedgerMiscTax.TaxAmountSourceCurrency + old.TaxAmountSourceCurrency, LedgerMiscTax.NonRecoverTaxPercent = LedgerMiscTax.NonRecoverTaxPercent + old.NonRecoverTaxPercent
				from LedgerMiscTax old inner join LedgerMiscTax on (old.Period = LedgerMiscTax.Period and old.PostSeq = LedgerMiscTax.PostSeq and old.PKey = LedgerMiscTax.PKey )
				Where LedgerMiscTax.TaxCode = @newValue And old.TaxCode = @OldValue
			delete from LedgerMiscTax where TaxCode = @OldValue 
				and exists (select 'x' from LedgerMiscTax new where new.TaxCode = @newValue and new.Period = LedgerMiscTax.Period	and new.PostSeq = LedgerMiscTax.PostSeq	and new.PKey = LedgerMiscTax.PKey	)
--LedgerARTax	(Period, PostSeq, PKey, TaxCode)
			Update LedgerARTax Set LedgerARTax.TaxAmount = LedgerARTax.TaxAmount + old.TaxAmount, LedgerARTax.TaxCBAmount = LedgerARTax.TaxCBAmount + old.TaxCBAmount, 
				LedgerARTax.AmountTaxCurrency = LedgerARTax.AmountTaxCurrency + old.AmountTaxCurrency, LedgerARTax.CBAmountTaxCurrency = LedgerARTax.CBAmountTaxCurrency + old.CBAmountTaxCurrency, 
				LedgerARTax.TaxAmountTaxCurrency = LedgerARTax.TaxAmountTaxCurrency + old.TaxAmountTaxCurrency, LedgerARTax.TaxCBAmountTaxCurrency = LedgerARTax.TaxCBAmountTaxCurrency + old.TaxCBAmountTaxCurrency, 
				LedgerARTax.TaxAmountFunctionalCurrency = LedgerARTax.TaxAmountFunctionalCurrency + old.TaxAmountFunctionalCurrency, LedgerARTax.TaxCBAmountFunctionalCurrency = LedgerARTax.TaxCBAmountFunctionalCurrency + old.TaxCBAmountFunctionalCurrency, 
				LedgerARTax.TaxAmountSourceCurrency = LedgerARTax.TaxAmountSourceCurrency + old.TaxAmountSourceCurrency, LedgerARTax.NonRecoverTaxPercent = LedgerARTax.NonRecoverTaxPercent + old.NonRecoverTaxPercent
				from LedgerARTax old inner join LedgerARTax on (old.Period = LedgerARTax.Period and old.PostSeq = LedgerARTax.PostSeq and old.PKey = LedgerARTax.PKey )
				Where LedgerARTax.TaxCode = @newValue And old.TaxCode = @OldValue
			delete from LedgerARTax where TaxCode = @OldValue 
				and exists (select 'x' from LedgerARTax new where new.TaxCode = @newValue and new.Period = LedgerARTax.Period	and new.PostSeq = LedgerARTax.PostSeq	and new.PKey = LedgerARTax.PKey	)

	    END --Existing
-- Add by DP 4.0
		Else	-- Not Existing
		begin
			select * into #TempKeyConvert from CFGBillTaxesData where Code = @OldValue
			Update #TempKeyConvert set Code=@NewValue
			Insert CFGBillTaxesData select * from #TempKeyConvert
			Drop Table #TempKeyConvert
			
			select * into #TempKeyConvert1 from CFGBillTaxesDescriptions where Code = @OldValue
			Update #TempKeyConvert1 set Code=@NewValue
			Insert CFGBillTaxesDescriptions select * from #TempKeyConvert1
			Drop Table #TempKeyConvert1
			
			set @NewName = @OldName
		end

		exec @RetVal = KeyCvt @Entity = @Entity,
					 @CvtType = @CvtType,
					 @Length = @Length,
					 @ErrMsg = @ErrMsg2 output,
					 @Diag = @Diag,
					 @NewValue = @NewValue,
					 @OldValue = @OldValue,
					 @ConstrFlag = 3 -- UpdateTables
					 ,@NeedTransaction = 'N'
		Delete from CFGBillTaxesData where Code = @OldValue
		Delete from CFGBillTaxesDescriptions where Code = @OldValue
		
--
		if (@RetVal = 0)
			begin
-- Add by DP 4.0
				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq)
					select PKey, Entity, OldKey, NewKey, @OldName, @NewName, @Period, @LastPostSeq from keyConvertWork where Pkey = @PKey
				end
--
				delete from keyConvertWork where current of keysCursor
				commit Transaction
			end
		else
			begin
				set @ErrMsg = @ErrMsg + @ErrMsg2
				rollback transaction
			end
	fetch next from KeysCursor into 
		@OldValue,
		@NewValue,
		@PKey
	set @KeysFetch = @@Fetch_Status
	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	return(0)
End -- Procedure
GO
