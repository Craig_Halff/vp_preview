SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_PAT_UMF_GetData] (
	@stripHTML			int,				/* 0 or 1 */
	@product			varchar(20),		/* PAT/EI/etc. */
	@cacheField			varchar(100),
	@updateTable		varchar(200),
	@updateField		varchar(100),
	@wbs1				Nvarchar(30),
	@wbs2				Nvarchar(7),
	@wbs3				Nvarchar(7),
	@payableSeq			int = 0				/* Only provided if product = PAT */
)
AS
BEGIN
	/* Copyright (c) 2020 EleVia Software.  All rights reserved. */
	/*
		exec [spCCG_PAT_UMF_GetData] 0, 'PAT', '', 'ProjectCustomTabFields', 'CustMemorandumField', '2002010.00', ' ', ' '
	*/
	SET NOCOUNT ON
	declare @sql varchar(max)

	if @product = 'PAT'
		-- Are we using a cache field in PAT?
		--	If the field is in PAT, get the value from CCG_PAT_CustomColumns if there, otherwise the Vision table
		if isnull(@cacheField,'') <> ''
			if isnull(@wbs1,'') = '' or isnull(@updateField,'') = '' or isnull(@updateTable,'') = ''
				set @sql = N'
					select '+(case when @stripHTML=1 then 'dbo.fnCCG_StripHtml(' else '' end)+' IsNull(cc.'+@cacheField+', '''') '+(case when @stripHTML=1 then ')' else '' end)+'
						from CCG_PAT_CustomColumns cc
						where cc.PayableSeq = '+cast(@payableSeq as varchar(20))
			else
				set @sql = N'
					select '+(case when @stripHTML=1 then 'dbo.fnCCG_StripHtml(' else '' end)+' IsNull(t.'+@updateField+', cc.'+@cacheField+') '+(case when @stripHTML=1 then ')' else '' end)+'
						from '+@updateTable+' t
							outer apply (select * from CCG_PAT_CustomColumns cc where cc.PayableSeq = '+cast(@payableSeq as varchar(20))+') cc
						where t.WBS1 = N'''+@wbs1+''' and t.WBS2 = N'''+@wbs2+''' and t.WBS3 = N'''+@wbs3+''''
		else
			set @sql = N'
				select '+(case when @stripHTML=1 then 'dbo.fnCCG_StripHtml(' else '' end) + @updateField + (case when @stripHTML=1 then ')' else '' end)+'
					from '+@updateTable+' t
					where t.WBS1 = N'''+@wbs1+''' and t.WBS2 = N'''+@wbs2+''' and t.WBS3 = N'''+@wbs3+''''

	exec (@sql)
END
GO
