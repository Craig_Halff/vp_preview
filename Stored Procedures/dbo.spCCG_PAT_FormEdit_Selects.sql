SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_PAT_FormEdit_Selects]
	@selectType		varchar(100),
	@param			Nvarchar(max) = '',
	@param2			Nvarchar(max) = '',
	@boolCase		bit = 0
AS
BEGIN
	-- Copyright (c) 2021 EleVia Software. All rights reserved.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	DECLARE @sql Nvarchar(max)

	IF @selectType = 'getOriginatingVendorEnabled'
		SELECT OriginatingVendorEnabled
			FROM CFGMainData
			WHERE Company = @param

	ELSE IF @selectType = 'LoadContracts' BEGIN
		SET @sql = N'
		SELECT Seq, ' +
			(Case WHEN isnull(@param, '') <> '' THEN '' ELSE 'Vendor + '' - '' + ' END) + 'PayableNumber, Address
			FROM CCG_PAT_Payable
			WHERE PayableType = ''C'' ' +
			(Case When isnull(@param, '') <> '' THEN ' and Vendor = N''' + @param + ''' ' ELSE '' END) + '
			ORDER BY 2'
		EXEC (@sql)
	END

	ELSE IF @selectType = 'LoadVEAddress'
		SELECT Address, Address, case when PrimaryInd = 'Y' then Address else '' end
			FROM VEAddress
			WHERE Vendor = @param
            ORDER BY 2

	ELSE IF @selectType = 'doVEAddressLookup'
		SELECT Address, PrimaryInd, Accounting, Address1, Address2, Address3, Address4, City, State, Zip
            FROM VEAddress
            WHERE Vendor = @param and address = @param2

	ELSE IF @selectType = 'getVendorValueListItems' BEGIN
		SET @sql = N'
		SELECT Vendor, ' + @param + '
			FROM VE
			WHERE ' +
			(CASE
				WHEN ISNULL(@param2, '') = '' THEN ' Status <> ''D'' '
				ELSE ' (Vendor like N''%' + Replace(@param2, N'''', N'''''') + '%''
					OR name like N''%' + Replace(@param2, N'''', N'''''') + ''') '
			END) + '
		UNION ALL
		SELECT VE.Vendor, Alias
			From VendorAlias a
				INNER JOIN VE on VE.Vendor = a.Vendor and VE.Status <> ''D''
		ORDER BY 1'
		EXEC (@sql)
	END
END
GO
