SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Role_Save_GetRoles]
AS
BEGIN
	SET NOCOUNT ON

	SELECT ltrim(rtrim([Role])) FROM CCG_PAT_ConfigRoles
END;

GO
