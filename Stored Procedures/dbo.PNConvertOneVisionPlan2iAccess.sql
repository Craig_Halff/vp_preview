SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PNConvertOneVisionPlan2iAccess]
  @strPlanID varchar(32)
AS

BEGIN -- Procedure PNConvertOneVisionPlan2iAccess

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

  DECLARE @strCompany Nvarchar(14)
  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)
  DECLARE @strMinorScale varchar(1)
  DECLARE @strAccordionFormatID varchar(32)
  DECLARE @strEMCostCurrencyCode Nvarchar(3)
  DECLARE @strEMBillCurrencyCode Nvarchar(3)
  DECLARE @strGRCostCurrencyCode Nvarchar(3)
  DECLARE @strGRBillCurrencyCode Nvarchar(3)
  DECLARE @strCalcExpBillAmtFlg varchar(1)
  DECLARE @strCalcConBillAmtFlg varchar(1)
  DECLARE @strExpTab varchar(1)
  DECLARE @strConTab varchar(1)
  DECLARE @strNextTaskID varchar(32)

  DECLARE @siGRMethod smallint
  DECLARE @siCostRtMethod smallint
  DECLARE @siBillRtMethod smallint
  DECLARE @siStartingDayOfWeek smallint

  DECLARE @intPlanCount int
  DECLARE @intCostRtTableNo int
  DECLARE @intBillRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int

  DECLARE @tiDefExpWBSLevel tinyint
  DECLARE @tiMinExpWBSLevel tinyint
  DECLARE @tiDefConWBSLevel tinyint
  DECLARE @tiMinConWBSLevel tinyint

  DECLARE @tabExpWBSTree TABLE(
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    WBSType varchar(4) COLLATE database_default,
    WBSLevel tinyint,
    StartDate datetime,
    EndDate datetime
    UNIQUE(PlanID, TaskID, WBS1, WBS2, WBS3)
  )

  DECLARE @tabConWBSTree TABLE(
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    WBSType varchar(4) COLLATE database_default,
    WBSLevel tinyint,
    StartDate datetime,
    EndDate datetime
    UNIQUE(PlanID, TaskID, WBS1, WBS2, WBS3)
  )

  DECLARE @tabExpense TABLE(
    PlanID varchar(32) COLLATE database_default,
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    Account Nvarchar(13) COLLATE database_default,
    Vendor Nvarchar(30) COLLATE database_default,
    DirectAcctFlg varchar(1) COLLATE database_default,
    PlannedExpCost decimal,
    PlannedExpBill decimal,
    BaselineExpCost decimal,
    BaselineExpBill decimal
    UNIQUE(PlanID, WBS1, WBS2, WBS3, Account, Vendor)
  )

  DECLARE @tabConsultant TABLE(
    PlanID varchar(32) COLLATE database_default,
    WBS1 Nvarchar(30) COLLATE database_default,
    WBS2 Nvarchar(7) COLLATE database_default,
    WBS3 Nvarchar(7) COLLATE database_default,
    Account Nvarchar(13) COLLATE database_default,
    Vendor Nvarchar(30) COLLATE database_default,
    DirectAcctFlg varchar(1) COLLATE database_default,
    PlannedConCost decimal,
    PlannedConBill decimal,
    BaselineConCost decimal,
    BaselineConBill decimal
    UNIQUE(PlanID, WBS1, WBS2, WBS3, Account, Vendor)
  )

  DECLARE csrPNExpense CURSOR LOCAL FAST_FORWARD FOR
    SELECT DISTINCT TaskID FROM PNExpense WHERE PlanID = @strPlanID

  DECLARE csrPNConsultant CURSOR LOCAL FAST_FORWARD FOR
    SELECT DISTINCT TaskID FROM PNConsultant WHERE PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  /* If a Plan with the same PlanID already exists in PNPlan, then exit this stored procedure. */

  SET @intPlanCount = CASE WHEN EXISTS (SELECT 'X' FROM PNPlan WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END

  IF (@intPlanCount > 0) RETURN
  
  BEGIN TRANSACTION
  
  DELETE PNCalendarInterval WHERE PlanID = @strPlanID
  DELETE PNAccordionFormat WHERE PlanID = @strPlanID
  DELETE PNWBSLevelFormat WHERE PlanID = @strPlanID

  DELETE PNPlannedLabor WHERE PlanID = @strPlanID
  DELETE PNBaselineLabor WHERE PlanID = @strPlanID
  DELETE PNAssignment WHERE PlanID = @strPlanID
  DELETE PNPlannedExpenses WHERE PlanID = @strPlanID
  DELETE PNBaselineExpenses WHERE PlanID = @strPlanID
  DELETE PNPlannedConsultant WHERE PlanID = @strPlanID
  DELETE PNBaselineConsultant WHERE PlanID = @strPlanID
  DELETE PNConsultant WHERE PlanID = @strPlanID
  DELETE PNTask WHERE PlanID = @strPlanID
  DELETE PNPlan WHERE PlanID = @strPlanID
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Delete data in RP tables that will not be carried over to PN tables.

  DELETE RPCompensationFee WHERE PlanID = @strPlanID
  DELETE RPConsultantFee WHERE PlanID = @strPlanID
  DELETE RPReimbAllowance WHERE PlanID = @strPlanID
  DELETE RPEVT WHERE PlanID = @strPlanID
  DELETE RPDependency WHERE PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get the RABIBC flag

  SELECT 
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled 
    FROM FW_CFGSystem

  -- Get Staring Day of Week from Vision Plan record to be used later.

  SELECT 
    @strCompany = Company,
    @siStartingDayOfWeek = StartingDayOfWeek,
    @siGRMethod = GRMethod,
    @intGenResTableNo = GenResTableNo,
    @intGRBillTableNo = GRBillTableNo,
    @siCostRtMethod = CostRtMethod,
    @siBillRtMethod = BillingRtMethod,
    @intCostRtTableNo = CostRtTableNo,
    @intBillRtTableNo = BillingRtTableNo,
    @strCalcExpBillAmtFlg = 'Y',
    @strCalcConBillAmtFlg = 'Y'
    FROM RPPlan WHERE PlanID = @strPlanID

  -- Get MinorScale and AccordionFormatID from Vision RPAccordionFormat.
  -- There should be only one RPAccordionFormat record for a Vision Plan.

  SELECT 
    @strAccordionFormatID = AccordionFormatID,
    @strMinorScale = MinorScale
    FROM RPAccordionFormat WHERE PlanID = @strPlanID

  -- Get Default Plan Settings from CFGResourcePlanning

  SELECT
    @tiDefExpWBSLevel = ExpWBSLevel,
    @tiDefConWBSLevel = ConWBSLevel,
    @strExpTab = ExpTab,
    @strConTab = ConTab
    FROM CFGResourcePlanning WHERE Company = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN @strCompany ELSE ' ' END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine the minimum WBS level where Expense rows need to be attached.
  -- Start with the default ExpWBSLevel. If there is no Expense row in the Vision Plan then @tiMinExpWBSLevel = ExpWBSLevel.
  -- If the Vision Plan has Expense rows, then the highest level found will be used as ExpWBSLevel.

  SELECT @tiMinExpWBSLevel = ISNULL(MIN(WBSLevel), @tiDefExpWBSLevel) 
    FROM ( /* X */
      SELECT 1 AS WBSLevel
        FROM RPExpense AS C
          INNER JOIN PR ON C.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND C.WBS2 IS NULL AND C.WBS3 IS NULL AND PR.SubLevel = 'Y'
        WHERE C.PlanID = @strPlanID
      UNION
      SELECT 2 AS WBSLevel
        FROM RPExpense AS C
          INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND PR.WBS3 = ' ' AND C.WBS3 IS NULL AND PR.SubLevel = 'Y'
        WHERE C.PlanID = @strPlanID
      UNION
      SELECT 3 AS WBSLevel
        FROM RPExpense AS C
          INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND PR.WBS3 = ' ' AND C.WBS3 IS NULL AND PR.SubLevel = 'N'
        WHERE C.PlanID = @strPlanID
      UNION
      SELECT 3 AS WBSLevel
        FROM RPExpense AS C
          INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND C.WBS3 = PR.WBS3
        WHERE C.PlanID = @strPlanID
    ) AS X

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Determine the minimum WBS level where Consultant rows need to be attached.
  -- Start with the default ConWBSLevel. If there is no Consultant row in the Vision Plan then @tiMinConWBSLevel = ConWBSLevel.
  -- If the Vision Plan has Consultant rows, then the highest level found will be used as ConWBSLevel.

  SELECT @tiMinConWBSLevel = ISNULL(MIN(WBSLevel), @tiDefConWBSLevel) 
    FROM ( /* X */
      SELECT 1 AS WBSLevel
        FROM RPConsultant AS C
          INNER JOIN PR ON C.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND C.WBS2 IS NULL AND C.WBS3 IS NULL AND PR.SubLevel = 'Y'
        WHERE C.PlanID = @strPlanID
      UNION
      SELECT 2 AS WBSLevel
        FROM RPConsultant AS C
          INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND PR.WBS3 = ' ' AND C.WBS3 IS NULL AND PR.SubLevel = 'Y'
        WHERE C.PlanID = @strPlanID
      UNION
      SELECT 3 AS WBSLevel
        FROM RPConsultant AS C
          INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND PR.WBS3 = ' ' AND C.WBS3 IS NULL AND PR.SubLevel = 'N'
        WHERE C.PlanID = @strPlanID
      UNION
      SELECT 3 AS WBSLevel
        FROM RPConsultant AS C
          INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND C.WBS3 = PR.WBS3
        WHERE C.PlanID = @strPlanID
    ) AS X

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  INSERT @tabExpWBSTree(
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    WBSType,
    WBSLevel,
    StartDate,
    EndDate
  )
    SELECT
      T.PlanID,
      T.TaskID,
      T.WBS1,
      T.WBS2,
      T.WBS3,
      T.WBSType,
      CAST(RIGHT(T.WBSType, 1) AS tinyint) AS WBSLevel,
      T.StartDate,
      T.EndDate
      FROM RPTask AS T
        INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND
          ((T.WBSType = 'WBS1' AND T.WBS1 <> '<none>') OR
           (T.WBSType = 'WBS2' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>') OR
           (T.WBSType = 'WBS3' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>' AND T.WBS3 <> '<none>'))
      WHERE P.PlanID = @strPlanID AND CAST(RIGHT(T.WBSType, 1) AS tinyint) <= @tiMinExpWBSLevel

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  INSERT @tabConWBSTree(
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    WBSType,
    WBSLevel,
    StartDate,
    EndDate
  )
    SELECT
      T.PlanID,
      T.TaskID,
      T.WBS1,
      T.WBS2,
      T.WBS3,
      T.WBSType,
      CAST(RIGHT(T.WBSType, 1) AS tinyint) AS WBSLevel,
      T.StartDate,
      T.EndDate
      FROM RPTask AS T
        INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID AND P.UtilizationIncludeFlg = 'Y' AND
          ((T.WBSType = 'WBS1' AND T.WBS1 <> '<none>') OR
           (T.WBSType = 'WBS2' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>') OR
           (T.WBSType = 'WBS3' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>' AND T.WBS3 <> '<none>'))
      WHERE P.PlanID = @strPlanID AND CAST(RIGHT(T.WBSType, 1) AS tinyint) <= @tiMinConWBSLevel

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Collecting existing RPExpense rows to be used latter.
  -- Combine RPExpense rows together by WBS1, WBS2, WBS3, Account, Vendor.

  INSERT @tabExpense(
    PlanID,
    WBS1,
    WBS2,
    WBS3,
    Account,
    Vendor,
    DirectAcctFlg,
    PlannedExpCost,
    PlannedExpBill,
    BaselineExpCost,
    BaselineExpBill
  )
    SELECT
      C.PlanID,
      C.WBS1,
      C.WBS2,
      C.WBS3,
      C.Account AS Account,
      C.Vendor AS Vendor,
      CASE WHEN CA.Type = 7 THEN 'Y' ELSE 'N' END AS DirectAcctFlg,
      SUM(C.PlannedExpCost) AS PlannedExpCost,
      SUM(C.PlannedExpBill) AS PlannedExpBill,
      SUM(C.BaselineExpCost) AS BaselineExpCost,
      SUM(C.BaselineExpBill) AS BaselineExpBill
      FROM RPExpense AS C
        INNER JOIN CA ON C.Account = CA.Account
      WHERE C.PlanID = @strPlanID
      GROUP BY C.PlanID, C.WBS1, C.WBS2, C.WBS3, C.Account, C.Vendor, CA.Type

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Collecting existing RPConsultant rows to be used latter.
  -- Combine RPConsultant rows together by WBS1, WBS2, WBS3, Account, Vendor.

  INSERT @tabConsultant(
    PlanID,
    WBS1,
    WBS2,
    WBS3,
    Account,
    Vendor,
    DirectAcctFlg,
    PlannedConCost,
    PlannedConBill,
    BaselineConCost,
    BaselineConBill
  )
    SELECT
      C.PlanID,
      C.WBS1,
      C.WBS2,
      C.WBS3,
      C.Account AS Account,
      C.Vendor AS Vendor,
      CASE WHEN CA.Type = 8 THEN 'Y' ELSE 'N' END AS DirectAcctFlg,
      SUM(C.PlannedConCost) AS PlannedConCost,
      SUM(C.PlannedConBill) AS PlannedConBill,
      SUM(C.BaselineConCost) AS BaselineConCost,
      SUM(C.BaselineConBill) AS BaselineConBill
      FROM RPConsultant AS C
        INNER JOIN CA ON C.Account = CA.Account
      WHERE C.PlanID = @strPlanID
      GROUP BY C.PlanID, C.WBS1, C.WBS2, C.WBS3, C.Account, C.Vendor, CA.Type

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determining the Currency Code of Rate Tables based on Rate Methods

  SELECT @strEMCostCurrencyCode = 
    CASE 
      WHEN @siCostRtMethod = 2 THEN (SELECT CurrencyCode FROM BTRRT WHERE TableNo = @intCostRtTableNo)
      WHEN @siCostRtMethod = 3 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intCostRtTableNo)
      WHEN @siCostRtMethod = 4 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intCostRtTableNo)
    END

  SELECT @strEMBillCurrencyCode = 
    CASE 
      WHEN @siBillRtMethod = 2 THEN (SELECT CurrencyCode FROM BTRRT WHERE TableNo = @intBillRtTableNo)
      WHEN @siBillRtMethod = 3 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intBillRtTableNo)
      WHEN @siBillRtMethod = 4 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intBillRtTableNo)
    END

  SELECT @strGRCostCurrencyCode = 
    CASE 
      WHEN @siGRMethod = 0 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intGenResTableNo)
      WHEN @siGRMethod = 1 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intGenResTableNo)
    END

  SELECT @strGRBillCurrencyCode = 
    CASE 
      WHEN @siGRMethod = 0 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intGRBillTableNo)
      WHEN @siGRMethod = 1 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intGRBillTableNo)
    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  
  INSERT PNPlan( 
    PlanID,
    PlanName,
    PlanNumber,
    OpportunityID,
    ClientID,
    ProjMgr,
    Principal,
    Supervisor,
    Org,
    StartDate,
    EndDate,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    BaselineExpCost,
    BaselineExpBill,
    BaselineConCost,
    BaselineConBill,
    BaselineRevenue,
    BaselineStart,
    BaselineFinish,
    PlannedLaborHrs,
    PlannedLabCost,
    PlannedLabBill,
    PlannedExpCost,
    PlannedExpBill,
    PlannedConCost,
    PlannedConBill,
    LabRevenue,
    PlannedDirExpCost,
    PlannedDirExpBill,
    PlannedDirConCost,
    PlannedDirConBill,
    CostRtMethod,
    BillingRtMethod,
    CostRtTableNo,
    BillingRtTableNo,
    GRMethod,
    GenResTableNo,
    GRBillTableNo,
    ContingencyPct,
    ContingencyAmt,
    OverheadPct,
    AvailableFlg,
    UnPostedFlg,
    BudgetType,
    PctCompleteFormula,
    PctComplete,
    PctCompleteBill,
    PctCompleteLabCost,
    PctCompleteLabBill,
    PctCompleteExpCost,
    PctCompleteExpBill,
    PctCompleteConCost,
    PctCompleteConBill,
    WeightedLabCost,
    WeightedLabBill,
    WeightedExpCost,
    WeightedExpBill,
    WeightedConCost,
    WeightedConBill,
    UtilizationIncludeFlg,
    WBS1,
    Probability,
    LabMultType,
    Multiplier,
    ProjectedMultiplier,
    ProjectedRatio,
    CalcExpBillAmtFlg,
    ExpBillRtMethod,
    ExpBillRtTableNo,
    ExpBillMultiplier,
    CalcConBillAmtFlg,
    ConBillRtMethod,
    ConBillRtTableNo,
    ConBillMultiplier,
    ReimbMethod,
    ExpRevenue,
    ConRevenue,
    TargetMultCost,
    TargetMultBill,
    BaselineUntQty,
    BaselineUntCost,
    BaselineUntBill,
    PlannedUntQty,
    PlannedUntCost,
    PlannedUntBill,
    PctCompleteUntCost,
    PctCompleteUntBill,
    PlannedDirUntCost,
    PlannedDirUntBill,
    WeightedUntCost,
    WeightedUntBill,
    UntRevenue,
    Status,
    PctComplByPeriodFlg,
    RevenueMethod,
    AnalysisBasis,
    Company,
    StartingDayOfWeek,
    CostCurrencyCode,
    BillingCurrencyCode,
    CompensationFee,
    ConsultantFee,
    ReimbAllowance,
    CompensationFeeBill,
    ConsultantFeeBill,
    ReimbAllowanceBill,
    CompensationFeeDirLab,
    CompensationFeeDirExp,
    ReimbAllowanceExp,
    ReimbAllowanceCon,
    CompensationFeeDirLabBill,
    CompensationFeeDirExpBill,
    ReimbAllowanceExpBill,
    ReimbAllowanceConBill,
    EVFormula,
    LabBillMultiplier,
    UntBillMultiplier,
    BaselineDirExpCost,
    BaselineDirExpBill,
    BaselineDirConCost,
    BaselineDirConBill,
    BaselineDirUntCost,
    BaselineDirUntBill,
    CommitmentFlg,
    TopdownFee,
    NeedsRecalc,
    ExpWBSLevel,
    ConWBSLevel,
    CheckedOutUser,
    CheckedOutDate,
    CheckedOutID,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      P.PlanID,
      P.PlanName,
      P.PlanNumber,
      P.OpportunityID,
      P.ClientID,
      P.ProjMgr,
      P.Principal,
      P.Supervisor,
      P.Org,
      P.StartDate,
      P.EndDate,
      BaselineLaborHrs,
      BaselineLabCost,
      BaselineLabBill,
      BaselineExpCost,
      BaselineExpBill,
      BaselineConCost,
      BaselineConBill,
      BaselineRevenue,
      BaselineStart,
      BaselineFinish,
      PlannedLaborHrs,
      PlannedLabCost,
      PlannedLabBill,
      PlannedExpCost,
      PlannedExpBill,
      PlannedConCost,
      PlannedConBill,
      LabRevenue,
      PlannedDirExpCost,
      PlannedDirExpBill,
      PlannedDirConCost,
      PlannedDirConBill,

      CASE WHEN P.CostRtMethod = 4 THEN 2 ELSE P.CostRtMethod END AS CostRtMethod,
      CASE WHEN P.BillingRtMethod = 4 THEN 2 ELSE P.BillingRtMethod END AS BillingRtMethod,

      CASE WHEN P.CostRtMethod = 4 THEN 0
      ELSE
        CASE WHEN @strMultiCurrencyEnabled <> 'Y' THEN P.CostRtTableNo
        ELSE
          CASE WHEN @strEMCostCurrencyCode = PR.ProjectCurrencyCode THEN P.CostRtTableNo ELSE 0 END
        END
      END AS CostRtTableNo,

      CASE WHEN P.BillingRtMethod = 4 THEN 0
      ELSE
        CASE WHEN @strMultiCurrencyEnabled <> 'Y' THEN P.BillingRtTableNo
        ELSE
          CASE WHEN @strEMBillCurrencyCode = CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.BillingCurrencyCode ELSE PR.ProjectCurrencyCode END
               THEN P.BillingRtTableNo
               ELSE 0
          END
        END
      END AS BillingRtTableNo,
      
      GRMethod,

      CASE WHEN @strMultiCurrencyEnabled <> 'Y' THEN P.GenResTableNo
      ELSE
        CASE WHEN @strGRCostCurrencyCode = PR.ProjectCurrencyCode THEN P.GenResTableNo ELSE 0 END 
      END AS GenResTableNo,

      CASE WHEN @strMultiCurrencyEnabled <> 'Y' THEN P.GRBillTableNo
      ELSE
        CASE WHEN @strGRBillCurrencyCode = CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.BillingCurrencyCode ELSE PR.ProjectCurrencyCode END
             THEN P.GRBillTableNo
             ELSE 0
        END 
      END AS GRBillTableNo,

      ContingencyPct,
      ContingencyAmt,
      OverheadPct,
      'Y' AS AvailableFlg,
      'Y' AS UnPostedFlg,
      BudgetType,
      2 AS PctCompleteFormula, -- JTD / (JTD + ETC)
      PctComplete,
      PctCompleteBill,
      PctCompleteLabCost,
      PctCompleteLabBill,
      PctCompleteExpCost,
      PctCompleteExpBill,
      PctCompleteConCost,
      PctCompleteConBill,
      WeightedLabCost,
      WeightedLabBill,
      WeightedExpCost,
      WeightedExpBill,
      WeightedConCost,
      WeightedConBill,
      'Y' AS UtilizationIncludeFlg,
      P.WBS1,
      P.Probability,
      CASE BudgetType
        WHEN 'C' THEN 1 /* Planned Multiplier */
        WHEN 'B' THEN 3 /* Planned Ratio */
        WHEN 'A' THEN
          CASE 
            WHEN LabMultType IN (0, 1) THEN 1 /* Planned Multiplier */
            WHEN LabMultType IN (2, 3) THEN 3 /* Planned Ratio */
          END
      END AS LabMultType,
      Multiplier,
      ProjectedMultiplier,
      ProjectedRatio,
      @strCalcExpBillAmtFlg AS CalcExpBillAmtFlg,
      CASE WHEN P.CalcExpBillAmtFlg = 'N' THEN 0 ELSE ExpBillRtMethod END AS ExpBillRtMethod,
      CASE WHEN P.CalcExpBillAmtFlg = 'N' THEN 0 ELSE ExpBillRtTableNo END AS ExpBillRtTableNo,
      CASE WHEN P.CalcExpBillAmtFlg = 'N' THEN 1.0000 ELSE ExpBillMultiplier END AS ExpBillMultiplier,
      @strCalcConBillAmtFlg AS CalcConBillAmtFlg,
      CASE WHEN P.CalcConBillAmtFlg = 'N' THEN 0 ELSE ConBillRtMethod END AS ConBillRtMethod,
      CASE WHEN P.CalcConBillAmtFlg = 'N' THEN 0 ELSE ConBillRtTableNo END AS ConBillRtTableNo,
      CASE WHEN P.CalcConBillAmtFlg = 'N' THEN 1.0000 ELSE ConBillMultiplier END AS ConBillMultiplier,
      ReimbMethod,
      ExpRevenue,
      ConRevenue,
      TargetMultCost,
      TargetMultBill,
      BaselineUntQty,
      BaselineUntCost,
      BaselineUntBill,
      PlannedUntQty,
      PlannedUntCost,
      PlannedUntBill,
      PctCompleteUntCost,
      PctCompleteUntBill,
      PlannedDirUntCost,
      PlannedDirUntBill,
      WeightedUntCost,
      WeightedUntBill,
      UntRevenue,
      PR.Status AS Status,
      PctComplByPeriodFlg,
      P.RevenueMethod,
      AnalysisBasis,
      P.Company,
      2 AS StartingDayOfWeek, -- Always set to Monday
      PR.ProjectCurrencyCode AS CostCurrencyCode,
      CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.BillingCurrencyCode ELSE PR.ProjectCurrencyCode END AS BillingCurrencyCode,

      PR.Fee AS CompensationFee,
      PR.ConsultFee AS ConsultantFee,
      PR.ReimbAllow AS ReimbAllowance,

      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeBillingCurrency ELSE PR.Fee END) AS CompensationFeeBill,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ConsultFeeBillingCurrency ELSE PR.ConsultFee END) AS ConsultantFeeBill,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowBillingCurrency ELSE PR.ReimbAllow END) AS ReimbAllowanceBill,

      PR.FeeDirLab AS CompensationFeeDirLab,
      PR.FeeDirExp AS CompensationFeeDirExp,
      PR.ReimbAllowExp AS ReimbAllowanceExp,
      PR.ReimbAllowCons AS ReimbAllowanceCon,

      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirLabBillingCurrency ELSE PR.FeeDirLab END) AS CompensationFeeDirLabBill,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirExpBillingCurrency ELSE PR.FeeDirExp END) AS CompensationFeeDirExpBill,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowExpBillingCurrency ELSE PR.ReimbAllowExp END) AS ReimbAllowanceExpBill,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowConsBillingCurrency ELSE PR.ReimbAllowCons END) AS ReimbAllowanceConBill,

      EVFormula,
      LabBillMultiplier,
      UntBillMultiplier,
      BaselineDirExpCost,
      BaselineDirExpBill,
      BaselineDirConCost,
      BaselineDirConBill,
      BaselineDirUntCost,
      BaselineDirUntBill,
      CommitmentFlg,
      TopdownFee,
      NeedsRecalc,
      @tiMinExpWBSLevel AS ExpWBSLevel,
      @tiMinConWBSLevel AS ConWBSLevel,
      P.CheckedOutUser,
      P.CheckedOutDate,
      P.CheckedOutID,
      P.CreateUser,
      P.CreateDate,
      P.ModUser,
      P.ModDate
      FROM RPPlan AS P
        LEFT JOIN PR ON P.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
      WHERE P.PlanID = @strPlanID

  INSERT PNTask(
    TaskID,
    PlanID,
    Name,
    LaborCode,
    WBS1,
    WBS2,
    WBS3,
    WBSType,
    ParentOutlineNumber,
    OutlineNumber,
    ChildrenCount,
    OutlineLevel,
    Priority,
    PctComplete,
    PctCompleteLabCost,
    PctCompleteLabBill,
    PctCompleteExpCost,
    PctCompleteExpBill,
    PctCompleteConCost,
    PctCompleteConBill,
    OverheadPct,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    BaselineExpCost,
    BaselineExpBill,
    BaselineConCost,
    BaselineConBill,
    BaselineRevenue,
    PlannedLaborHrs,
    PlannedLabCost,
    PlannedLabBill,
    PlannedExpCost,
    PlannedExpBill,
    PlannedConCost,
    PlannedConBill,
    LabRevenue,
    PlannedDirExpCost,
    PlannedDirExpBill,
    PlannedDirConCost,
    PlannedDirConBill,
    WeightedLabCost,
    WeightedLabBill,
    WeightedExpCost,
    WeightedExpBill,
    WeightedConCost,
    WeightedConBill,
    Duration,
    ActualDuration,
    BaselineDuration,
    RemainingDuration,
    ScheduleConstraint,
    StartDate,
    ActualStart,
    BaselineStart,
    EarlyStart,
    LateStart,
    ScheduledStart,
    EndDate,
    ActualFinish,
    BaselineFinish,
    EarlyFinish,
    LateFinish,
    ScheduledFinish,
    CostRate,
    BillingRate,
    ExpBillRate,
    ConBillRate,
    ExpRevenue,
    ConRevenue,
    UntCostRate,
    UntBillRate,
    BaselineUntQty,
    BaselineUntCost,
    BaselineUntBill,
    PlannedUntQty,
    PlannedUntCost,
    PlannedUntBill,
    PctCompleteUntCost,
    PctCompleteUntBill,
    PlannedDirUntCost,
    PlannedDirUntBill,
    WeightedUntCost,
    WeightedUntBill,
    UntRevenue,
    LabParentState,
    ExpParentState,
    ConParentState,
    UntParentState,
    ConVState,
    ExpVState,
    LabVState,
    UntVState,
    BaselineDirExpCost,
    BaselineDirExpBill,
    BaselineDirConCost,
    BaselineDirConBill,
    BaselineDirUntCost,
    BaselineDirUntBill,
    CompensationFee,
    ConsultantFee,
    ReimbAllowance,
    CompensationFeeBill,
    ConsultantFeeBill,
    ReimbAllowanceBill,
    CompensationFeeDirLab,
    CompensationFeeDirExp,
    ReimbAllowanceExp,
    ReimbAllowanceCon,
    CompensationFeeDirLabBill,
    CompensationFeeDirExpBill,
    ReimbAllowanceExpBill,
    ReimbAllowanceConBill,
    UnmatchedFlg,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      TaskID,
      PlanID,
      Name,
      LaborCode,
      WBS1,
      WBS2,
      WBS3,
      WBSType,
      ParentOutlineNumber,
      OutlineNumber,
      ChildrenCount,
      OutlineLevel,
      Priority,
      PctComplete,
      PctCompleteLabCost,
      PctCompleteLabBill,
      PctCompleteExpCost,
      PctCompleteExpBill,
      PctCompleteConCost,
      PctCompleteConBill,
      OverheadPct,
      BaselineLaborHrs,
      BaselineLabCost,
      BaselineLabBill,
      BaselineExpCost,
      BaselineExpBill,
      BaselineConCost,
      BaselineConBill,
      BaselineRevenue,
      PlannedLaborHrs,
      PlannedLabCost,
      PlannedLabBill,
      PlannedExpCost,
      PlannedExpBill,
      PlannedConCost,
      PlannedConBill,
      LabRevenue,
      PlannedDirExpCost,
      PlannedDirExpBill,
      PlannedDirConCost,
      PlannedDirConBill,
      WeightedLabCost,
      WeightedLabBill,
      WeightedExpCost,
      WeightedExpBill,
      WeightedConCost,
      WeightedConBill,
      Duration,
      ActualDuration,
      BaselineDuration,
      RemainingDuration,
      ScheduleConstraint,
      StartDate,
      ActualStart,
      BaselineStart,
      EarlyStart,
      LateStart,
      ScheduledStart,
      EndDate,
      ActualFinish,
      BaselineFinish,
      EarlyFinish,
      LateFinish,
      ScheduledFinish,
      CostRate,
      BillingRate,
      ExpBillRate,
      ConBillRate,
      ExpRevenue,
      ConRevenue,
      UntCostRate,
      UntBillRate,
      BaselineUntQty,
      BaselineUntCost,
      BaselineUntBill,
      PlannedUntQty,
      PlannedUntCost,
      PlannedUntBill,
      PctCompleteUntCost,
      PctCompleteUntBill,
      PlannedDirUntCost,
      PlannedDirUntBill,
      WeightedUntCost,
      WeightedUntBill,
      UntRevenue,
      LabParentState,
      ExpParentState,
      ConParentState,
      UntParentState,
      ConVState,
      ExpVState,
      LabVState,
      UntVState,
      BaselineDirExpCost,
      BaselineDirExpBill,
      BaselineDirConCost,
      BaselineDirConBill,
      BaselineDirUntCost,
      BaselineDirUntBill,
      CompensationFee,
      ConsultantFee,
      ReimbAllowance,
      CompensationFeeBill,
      ConsultantFeeBill,
      ReimbAllowanceBill,
      CompensationFeeDirLab,
      CompensationFeeDirExp,
      ReimbAllowanceExp,
      ReimbAllowanceCon,
      CompensationFeeDirLabBill,
      CompensationFeeDirExpBill,
      ReimbAllowanceExpBill,
      ReimbAllowanceConBill,
      UnmatchedFlg,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPTask AS T WHERE T.PlanID = @strPlanID
  
  INSERT PNAssignment( 
    AssignmentID,
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    LaborCode,
    ResourceID,
    GenericResourceID,
    Category,
    GRLBCD,
    ActivityID,
    CostRate,
    BillingRate,
    PctCompleteLabCost,
    PctCompleteLabBill,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    BaselineRevenue,
    BaselineStart,
    BaselineFinish,
    PlannedLaborHrs,
    PlannedLabCost,
    PlannedLabBill,
    LabRevenue,
    WeightedLabCost,
    WeightedLabBill,
    StartDate,
    EndDate,
    SortSeq,
    LabParentState,
    ExpParentState,
    ConParentState,
    UntParentState,
    ConVState,
    ExpVState,
    LabVState,
    UntVState,
    JTDLabHrs,
    JTDLabCost,
    JTDLabBill,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      AssignmentID,
      PlanID,
      TaskID,
      WBS1,
      WBS2,
      WBS3,
      LaborCode,
      ResourceID,
      GenericResourceID,
      Category,
      GRLBCD,
      ActivityID,
      CostRate,
      BillingRate,
      PctCompleteLabCost,
      PctCompleteLabBill,
      BaselineLaborHrs,
      BaselineLabCost,
      BaselineLabBill,
      BaselineRevenue,
      BaselineStart,
      BaselineFinish,
      PlannedLaborHrs,
      PlannedLabCost,
      PlannedLabBill,
      LabRevenue,
      WeightedLabCost,
      WeightedLabBill,
      StartDate,
      EndDate,
      SortSeq,
      LabParentState,
      ExpParentState,
      ConParentState,
      UntParentState,
      ConVState,
      ExpVState,
      LabVState,
      UntVState,
      JTDLabHrs,
      JTDLabCost,
      JTDLabBill,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPAssignment AS A WHERE A.PlanID = @strPlanID

  INSERT PNPlannedLabor(
    TimePhaseID,
    TaskID,
    PlanID,
    AssignmentID,
    StartDate,
    EndDate,
    PeriodScale,
    PeriodCount,
    PeriodHrs,
    PeriodCost,
    PeriodBill,
    CostRate,
    BillingRate,
    PeriodRev,
    HardBooked,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      TimePhaseID,
      TaskID,
      PlanID,
      AssignmentID,
      StartDate,
      EndDate,
      PeriodScale,
      PeriodCount,
      PeriodHrs,
      PeriodCost,
      PeriodBill,
      CostRate,
      BillingRate,
      PeriodRev,
      HardBooked,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPPlannedLabor AS PL WHERE PL.PlanID = @strPlanID
  
  INSERT PNBaselineLabor(
    TimePhaseID,
    TaskID,
    PlanID,
    AssignmentID,
    StartDate,
    EndDate,
    PeriodScale,
    PeriodCount,
    PeriodHrs,
    PeriodCost,
    PeriodBill,
    CostRate,
    BillingRate,
    PeriodRev,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      TimePhaseID,
      TaskID,
      PlanID,
      AssignmentID,
      StartDate,
      EndDate,
      PeriodScale,
      PeriodCount,
      PeriodHrs,
      PeriodCost,
      PeriodBill,
      CostRate,
      BillingRate,
      PeriodRev,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPBaselineLabor AS BL WHERE BL.PlanID = @strPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strExpTab = 'Y')
    BEGIN

      INSERT PNExpense(
        ExpenseID,
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        StartDate,
        EndDate,
        Account,
        Vendor,
        DirectAcctFlg,
        ExpBillRate,
        PlannedExpCost,
        PlannedExpBill,
        BaselineExpCost,
        BaselineExpBill,
        LabParentState,
        ExpParentState,
        ConParentState,
        UntParentState,
        LabVState,
        ExpVState,
        ConVState,
        UntVState,
        CreateUser,
        CreateDate,
        ModUser,
        ModDate
      )
        SELECT
          REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS ExpenseID,
          E.PlanID,
          T.TaskID,
          T.WBS1,
          T.WBS2,
          T.WBS3,
          T.StartDate,
          T.EndDate,
          E.Account,
          E.Vendor,
          E.DirectAcctFlg,
          CASE 
            WHEN E.DirectAcctFlg = 'Y' 
            THEN 1.0
            ELSE
              CASE
                WHEN @strCalcExpBillAmtFlg = 'Y'
                THEN
                  CASE 
                    WHEN E.PlannedExpCost = 0.0
                    THEN 0.0
                    ELSE (E.PlannedExpBill / E.PlannedExpCost)
                  END
                ELSE 0.0
              END
          END AS ExpBillRate,
          E.PlannedExpCost,
          E.PlannedExpBill,
          E.BaselineExpCost,
          E.BaselineExpBill,
          'N' AS LabParentState,
          'N' AS ExpParentState,
          'N' AS ConParentState,
          'N' AS UntParentState,
          'N' AS LabVState,
          'Y' AS ExpVState,
          'N' AS ConVState,
          'N' AS UntVState,
          NULL AS CreateUser,
          CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
          NULL AS ModUser,
          CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate
          FROM @tabExpense AS E
            INNER JOIN @tabExpWBSTree AS T ON E.PlanID = T.PlanID AND
              E.WBS1 = T.WBS1 AND ISNULL(E.WBS2, '$') = ISNULL(T.WBS2, '$') AND ISNULL(E.WBS3, '$') = ISNULL(T.WBS3, '$')

      -- Baseline Time-Phased data for the summary Task rows.

      INSERT PNBaselineExpenses(
        TimePhaseID,
        TaskID,
        PlanID,
        ExpenseID,
        StartDate,
        EndDate,
        PeriodScale,
        PeriodCount,
        PeriodCost,
        PeriodBill,
        PeriodRev,
        CreateUser,
        CreateDate,
        ModUser,
        ModDate
      )
        SELECT
          TimePhaseID,
          TaskID,
          PlanID,
          ExpenseID,
          StartDate,
          EndDate,
          PeriodScale,
          PeriodCount,
          PeriodCost,
          PeriodBill,
          PeriodRev,
          CreateUser,
          CreateDate,
          ModUser,
          ModDate
          FROM RPBaselineExpenses AS TPD 
          WHERE TPD.PlanID = @strPlanID AND TPD.ExpenseID IS NULL
 
      -- Baseline Time-Phased data for the Expense rows.

      INSERT PNBaselineExpenses(
        TimePhaseID,
        TaskID,
        PlanID,
        ExpenseID,
        StartDate,
        EndDate,
        PeriodScale,
        PeriodCount,
        PeriodCost,
        PeriodBill,
        PeriodRev,
        CreateUser,
        CreateDate,
        ModUser,
        ModDate
      )
        SELECT
          REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
          NC.TaskID,
          NC.PlanID,
          NC.ExpenseID,
          TPD.StartDate,
          TPD.EndDate,
          MIN(TPD.PeriodScale),
          MIN(TPD.PeriodCount),
          SUM(TPD.PeriodCost),
          SUM(TPD.PeriodBill),
          SUM(TPD.PeriodRev),
          NULL AS CreateUser,
          CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
          NULL AS ModUser,
          CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate
          FROM RPBaselineExpenses AS TPD
            INNER JOIN RPExpense AS VC ON TPD.PlanID = VC.PlanID AND TPD.TaskID = VC.TaskID AND TPD.ExpenseID = VC.ExpenseID
            INNER JOIN PNExpense AS NC ON 
              VC.WBS1 = NC.WBS1 AND ISNULL(VC.WBS2, ' ') = ISNULL(NC.WBS2, ' ') AND ISNULL(VC.WBS3, ' ') = ISNULL(NC.WBS3, ' ') AND
              VC.Account = NC.Account AND ISNULL(VC.Vendor, ' ') = ISNULL(NC.Vendor, ' ')
          WHERE TPD.PlanID = @strPlanID AND TPD.ExpenseID IS NOT NULL
          GROUP BY NC.PlanID, NC.TaskID, NC.ExpenseID, NC.WBS1, NC.WBS2, NC.WBS3, NC.Account, NC.Vendor, TPD.StartDate, TPD.EndDate

    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strConTab = 'Y')
    BEGIN

      INSERT PNConsultant(
        ConsultantID,
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        StartDate,
        EndDate,
        Account,
        Vendor,
        DirectAcctFlg,
        ConBillRate,
        PlannedConCost,
        PlannedConBill,
        BaselineConCost,
        BaselineConBill,
        LabParentState,
        ExpParentState,
        ConParentState,
        UntParentState,
        LabVState,
        ExpVState,
        ConVState,
        UntVState,
        CreateUser,
        CreateDate,
        ModUser,
        ModDate
      )
        SELECT
          REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS ConsultantID,
          C.PlanID,
          T.TaskID,
          T.WBS1,
          T.WBS2,
          T.WBS3,
          T.StartDate,
          T.EndDate,
          C.Account,
          C.Vendor,
          C.DirectAcctFlg,
          CASE 
            WHEN C.DirectAcctFlg = 'Y' 
            THEN 1.0
            ELSE
              CASE
                WHEN @strCalcConBillAmtFlg = 'Y'
                THEN
                  CASE 
                    WHEN C.PlannedConCost = 0.0
                    THEN 0.0
                    ELSE (C.PlannedConBill / C.PlannedConCost)
                  END
                ELSE 0.0
              END
          END AS ConBillRate,
          C.PlannedConCost,
          C.PlannedConBill,
          C.BaselineConCost,
          C.BaselineConBill,
          'N' AS LabParentState,
          'N' AS ExpParentState,
          'N' AS ConParentState,
          'N' AS UntParentState,
          'N' AS LabVState,
          'N' AS ExpVState,
          'Y' AS ConVState,
          'N' AS UntVState,
          NULL AS CreateUser,
          CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
          NULL AS ModUser,
          CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate
          FROM @tabConsultant AS C
            INNER JOIN @tabConWBSTree AS T ON C.PlanID = T.PlanID AND
              C.WBS1 = T.WBS1 AND ISNULL(C.WBS2, '$') = ISNULL(T.WBS2, '$') AND ISNULL(C.WBS3, '$') = ISNULL(T.WBS3, '$')

      -- Baseline Time-Phased data for the summary Task rows.

      INSERT PNBaselineConsultant(
        TimePhaseID,
        TaskID,
        PlanID,
        ConsultantID,
        StartDate,
        EndDate,
        PeriodScale,
        PeriodCount,
        PeriodCost,
        PeriodBill,
        PeriodRev,
        CreateUser,
        CreateDate,
        ModUser,
        ModDate
      )
        SELECT
          TimePhaseID,
          TaskID,
          PlanID,
          ConsultantID,
          StartDate,
          EndDate,
          PeriodScale,
          PeriodCount,
          PeriodCost,
          PeriodBill,
          PeriodRev,
          CreateUser,
          CreateDate,
          ModUser,
          ModDate
          FROM RPBaselineConsultant AS TPD 
          WHERE TPD.PlanID = @strPlanID AND TPD.ConsultantID IS NULL
 
      -- Baseline Time-Phased data for the ConsultantS rows.

      INSERT PNBaselineConsultant(
        TimePhaseID,
        TaskID,
        PlanID,
        ConsultantID,
        StartDate,
        EndDate,
        PeriodScale,
        PeriodCount,
        PeriodCost,
        PeriodBill,
        PeriodRev,
        CreateUser,
        CreateDate,
        ModUser,
        ModDate
      )
        SELECT
          REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
          NC.TaskID,
          NC.PlanID,
          NC.ConsultantID,
          TPD.StartDate,
          TPD.EndDate,
          MIN(TPD.PeriodScale),
          MIN(TPD.PeriodCount),
          SUM(TPD.PeriodCost),
          SUM(TPD.PeriodBill),
          SUM(TPD.PeriodRev),
          NULL AS CreateUser,
          CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
          NULL AS ModUser,
          CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate
          FROM RPBaselineConsultant AS TPD
            INNER JOIN RPConsultant AS VC ON TPD.PlanID = VC.PlanID AND TPD.TaskID = VC.TaskID AND TPD.ConsultantID = VC.ConsultantID
            INNER JOIN PNConsultant AS NC ON 
              VC.WBS1 = NC.WBS1 AND ISNULL(VC.WBS2, ' ') = ISNULL(NC.WBS2, ' ') AND ISNULL(VC.WBS3, ' ') = ISNULL(NC.WBS3, ' ') AND
              VC.Account = NC.Account AND ISNULL(VC.Vendor, ' ') = ISNULL(NC.Vendor, ' ')
          WHERE TPD.PlanID = @strPlanID AND TPD.ConsultantID IS NOT NULL
          GROUP BY NC.PlanID, NC.TaskID, NC.ConsultantID, NC.WBS1, NC.WBS2, NC.WBS3, NC.Account, NC.Vendor, TPD.StartDate, TPD.EndDate

    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT PNWBSLevelFormat(
    WBSFormatID,
    PlanID,
    FmtLevel,
    WBSType,
    WBSMatch,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT 
      WBSFormatID,
      PlanID,
      FmtLevel,
      WBSType,
      WBSMatch,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPWBSLevelFormat WHERE PlanID = @strPlanID

  INSERT PNAccordionFormat(
    AccordionFormatID,
    PlanID,
    StartDate,
    EndDate,
    MajorScale,
    MinorScale,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
     SELECT 
       AccordionFormatID,
       PlanID,
       StartDate,
       EndDate,
       MajorScale,
       MinorScale,
       CreateUser,
       CreateDate,
       ModUser,
       ModDate
       FROM RPAccordionFormat WHERE PlanID = @strPlanID

  INSERT PNCalendarInterval(
    PlanID,
    StartDate,
    AccordionFormatID,
    EndDate,
    PeriodScale,
    PeriodCount,
    NumWorkingDays,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT 
      PlanID,
      StartDate,
      AccordionFormatID,
      EndDate,
      PeriodScale,
      PeriodCount,
      NumWorkingDays,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM RPCalendarInterval WHERE PlanID = @strPlanID      

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- If the Navigator Plan has Calendar Scale of "Weekly" or "Bi-Weekly",
  -- and the Vision Plan originally had "Starting Day of Week" other than "Monday",
  -- then the Calendar Intervals need to be regenerated.

  EXECUTE dbo.PNDeleteSummaryTPD @strPlanID, 'Y', 'Y', 'Y'

  IF (@strMinorScale IN ('w', 'b') AND @siStartingDayOfWeek <> 2)
    BEGIN

      -- Recreate Calendar Interval with the new "Starting Day of Week" = Monday
      
      EXEC dbo.PNMakeCI @strAccordionFormatID

      -- Realign TPD.
          
      EXECUTE dbo.PNAlignTPD @strPlanID

    END -- IF (@strMinorScale IN ('w', 'b') AND @siStartingDayOfWeek <> 2)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Spread Expense Amounts

  OPEN csrPNExpense

  FETCH NEXT FROM csrPNExpense INTO @strNextTaskID

  WHILE (@@FETCH_STATUS = 0)
    BEGIN
      EXECUTE dbo.PNSpreadExpTPD @strNextTaskID
      FETCH NEXT FROM csrPNExpense INTO @strNextTaskID
    END -- While

  CLOSE csrPNExpense
  DEALLOCATE csrPNExpense

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Spread Consultant Amounts

  OPEN csrPNConsultant

  FETCH NEXT FROM csrPNConsultant INTO @strNextTaskID

  WHILE (@@FETCH_STATUS = 0)
    BEGIN
      EXECUTE dbo.PNSpreadConTPD @strNextTaskID
      FETCH NEXT FROM csrPNConsultant INTO @strNextTaskID
    END -- While

  CLOSE csrPNConsultant
  DEALLOCATE csrPNConsultant

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Recalculate Navigator Plan data.

  EXECUTE dbo.PNCalcTPD @strPlanID, 'Y', 'Y', 'Y'
  EXECUTE dbo.PNSumUpTPD @strPlanID, 'Y', 'Y', 'Y'

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  COMMIT

  SET NOCOUNT OFF

END -- PNConvertOneVisionPlan2iAccess
GO
