SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[buildPRSummarySub]
	@visionuser		Nvarchar(32),
	@sessionID		Nvarchar(32)
AS
BEGIN
   DECLARE @acctTypeRevenue		smallint
   DECLARE @acctTypeReimb		smallint
   DECLARE @acctTypeReimbCons	smallint
   DECLARE @acctTypeDirect		smallint
   DECLARE @acctTypeDirectCons	smallint
   DECLARE @acctTypeIndirect	smallint
   DECLARE @acctTypeOtherCharges	smallint
   DECLARE @Org1Start		int
   DECLARE @Org1Length		int
   DECLARE @MulticompanyEnabled   varchar(1)
   
   SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
   SET NOCOUNT ON

	SELECT	@acctTypeRevenue = 4, @acctTypeReimb = 5, @acctTypeReimbCons = 6, 
		@acctTypeDirect = 7, @acctTypeDirectCons = 8, 
		@acctTypeInDirect = 9, @acctTypeOtherCharges = 10

	SELECT @Org1Start = Org1Start, @Org1Length = Org1Length FROM CFGFormat
	
	SELECT @MulticompanyEnabled = MulticompanyEnabled FROM FW_CFGSystem
	------------------------------------------------------------------
	--Labor: set LaborCost, LaborBilling
	UPDATE PRSummarySub
	SET 	PRSummarySub.LaborCost = myQry.RegAmt+myQry.OvtAmt+myQry.SpecialOvtAmt,
		PRSummarySub.LaborCostProjectCurrency = myQry.RegAmtProjectCurrency+myQry.OvtAmtProjectCurrency+myQry.SpecialOvtAmtProjectCurrency,
		PRSummarySub.LaborCostBillingCurrency = myQry.RegAmtBillingCurrency+myQry.OvtAmtBillingCurrency+myQry.SpecialOvtAmtBillingCurrency,
		PRSummarySub.Hours = myQry.RegHours+myQry.OvtHours+myQry.SpecialOvtHours,

		PRSummarySub.RegHours = myQry.RegHours,
		PRSummarySub.OvtHours = myQry.OvtHours,
		PRSummarySub.SpecialOvtHours = myQry.SpecialOvtHours,

		PRSummarySub.LaborRegAmt = myQry.RegAmt,
		PRSummarySub.LaborOvtAmt = myQry.OvtAmt,
		PRSummarySub.LaborSpecialOvtAmt = myQry.SpecialOvtAmt,

		PRSummarySub.LaborRegAmtProjectCurrency = myQry.RegAmtProjectCurrency,
		PRSummarySub.LaborOvtAmtProjectCurrency = myQry.OvtAmtProjectCurrency,
		PRSummarySub.LaborSpecialOvtAmtProjectCurrency = myQry.SpecialOvtAmtProjectCurrency,

		PRSummarySub.LaborRegAmtBillingCurrency = myQry.RegAmtBillingCurrency,
		PRSummarySub.LaborOvtAmtBillingCurrency = myQry.OvtAmtBillingCurrency,
		PRSummarySub.LaborSpecialOvtAmtBillingCurrency = myQry.SpecialOvtAmtBillingCurrency,

		PRSummarySub.LaborBilling = myQry.billamt
	FROM (
		SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3,
		sum(IsNull(RegHrs,0)) AS RegHours,
		sum(IsNull(RegAmt,0)) AS RegAmt,
		sum(IsNull(OvtHrs,0)) AS OvtHours,
		sum(IsNull(OvtAmt,0)) AS OvtAmt,
		sum(IsNull(SpecialOvtHrs,0)) AS SpecialOvtHours,
		sum(IsNull(SpecialOvtAmt,0)) AS SpecialOvtAmt,

		sum(IsNull(RegAmtProjectCurrency,0)) AS RegAmtProjectCurrency,
		sum(IsNull(OvtAmtProjectCurrency,0)) AS OvtAmtProjectCurrency,
		sum(IsNull(SpecialOvtAmtProjectCurrency,0)) AS SpecialOvtAmtProjectCurrency,

		sum(IsNull(RegAmtBillingCurrency,0)) AS RegAmtBillingCurrency,
		sum(IsNull(OvtAmtBillingCurrency,0)) AS OvtAmtBillingCurrency,
		sum(IsNull(SpecialOvtAmtBillingCurrency,0)) AS SpecialOvtAmtBillingCurrency,
		sum(BillExt) AS billamt
		FROM LD mySource 
		INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
		AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
		WHERE psl.sessionID = @sessionID
			AND mySource.ProjectCost = 'Y'
		GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period
	) AS myQry inner join PRSummarySub 
	on (PRSummarySub.WBS1 = myQry.WBS1 AND PRSummarySub.WBS2 = myQry.WBS2 
		AND PRSummarySub.WBS3 = myQry.WBS3 AND PRSummarySub.Period = myQry.Period)

	------------------------------------------------------------------
	--Overhead: set Overhead, OverheadProjectCurrency
	-- Uncomment setting of OverheadBillingCurrency after RegOHBillingCurrency is added to PRF table
	UPDATE PRSummarySub
	SET 	PRSummarySub.Overhead = myQry.RegOH,
		--PRSummarySub.OverheadBillingCurrency = myQry.RegOHBillingCurrency,
		PRSummarySub.OverheadProjectCurrency = myQry.RegOHProjectCurrency
	FROM (
		SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3,
		sum(IsNull(RegOH, 0)) AS RegOH, 
		--sum(IsNull(RegOHBillingCurrency, 0)) AS RegOHBillingCurrency,
		sum(IsNull(RegOHProjectCurrency, 0)) AS RegOHProjectCurrency 
		FROM PRF mySource 
		INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
		AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
		WHERE psl.sessionID = @sessionID
		GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period
	) AS myQry inner join PRSummarySub 
	on (PRSummarySub.WBS1 = myQry.WBS1 AND PRSummarySub.WBS2 = myQry.WBS2 
		AND PRSummarySub.WBS3 = myQry.WBS3 AND PRSummarySub.Period = myQry.Period)

	------------------------------------------------------------------
	--Direct Expenses: set DirectConsCost, DirectOtherCost, DirectConsBilling, DirectOtherBilling
		-- For Testing purposes..
		--sum(CASE WHEN Account >= '600.00' AND Account <= '619.99' THEN Amount ELSE 0 END) AS consAmt, 
		--sum(CASE WHEN Account >= '600.00' AND Account <= '619.99' THEN BillExt ELSE 0 END) AS billconsAmt 
		--WHERE psl.sessionID = 'NoUser20060221593466'
		--AND (Account >= '600.00' AND Account < '700.00') 

	UPDATE PRSummarySub
	SET 	PRSummarySub.DirectConsCost = myQry.DirectConsCost,
		PRSummarySub.DirectOtherCost = myQry.DirectOtherCost,
		PRSummarySub.DirectConsCostProjectCurrency = myQry.DirectConsCostProjectCurrency,
		PRSummarySub.DirectOtherCostProjectCurrency = myQry.DirectOtherCostProjectCurrency,
		PRSummarySub.DirectConsCostBillingCurrency = myQry.DirectConsCostBillingCurrency,
		PRSummarySub.DirectOtherCostBillingCurrency = myQry.DirectOtherCostBillingCurrency,
		PRSummarySub.DirectConsBilling = myQry.DirectConsBilling,
		PRSummarySub.DirectOtherBilling = myQry.DirectOtherBilling
	FROM (
		SELECT Period, WBS1, WBS2, WBS3,
		sum(consAmt) As DirectConsCost, sum(direct - consAmt) As DirectOtherCost, 
		sum(consAmtProjectCurrency) As DirectConsCostProjectCurrency, sum(directProjectCurrency - consAmtProjectCurrency) As DirectOtherCostProjectCurrency, 
		sum(consAmtBillingCurrency) As DirectConsCostBillingCurrency, sum(directBillingCurrency - consAmtBillingCurrency) As DirectOtherCostBillingCurrency, 
		sum(billconsAmt) As DirectConsBilling, sum(billdirect - billconsAmt) As DirectOtherBilling 
		FROM (
			SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3, 
			sum(Amount) AS direct, sum(AmountProjectCurrency) AS directProjectCurrency, 
			sum(AmountBillingCurrency) AS directBillingCurrency, 
			sum(BillExt) AS billdirect, 
			sum(CASE WHEN CA.Type = @acctTypeDirectCons THEN Amount ELSE 0 END) AS consAmt, 
			sum(CASE WHEN CA.Type = @acctTypeDirectCons THEN AmountProjectCurrency ELSE 0 END) AS consAmtProjectCurrency, 
			sum(CASE WHEN CA.Type = @acctTypeDirectCons THEN AmountBillingCurrency ELSE 0 END) AS consAmtBillingCurrency, 
			sum(CASE WHEN CA.Type = @acctTypeDirectCons THEN BillExt ELSE 0 END) AS billconsAmt 
			FROM LedgerAR mySource
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
			AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
			INNER JOIN CA On mySource.Account = CA.Account
				WHERE psl.sessionID = @sessionID
			AND (CA.Type >= @acctTypeDirect AND CA.Type < @acctTypeIndirect)
			AND ProjectCost = 'Y'
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period
	
			UNION ALL
			SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3, 
			sum(Amount) AS direct, sum(AmountProjectCurrency) AS directProjectCurrency, 
			sum(AmountBillingCurrency) AS directBillingCurrency, 
			sum(BillExt) AS billdirect, 
			sum(CASE WHEN CA.Type = @acctTypeDirectCons THEN Amount ELSE 0 END) AS consAmt, 
			sum(CASE WHEN CA.Type = @acctTypeDirectCons THEN AmountProjectCurrency ELSE 0 END) AS consAmtProjectCurrency, 
			sum(CASE WHEN CA.Type = @acctTypeDirectCons THEN AmountBillingCurrency ELSE 0 END) AS consAmtBillingCurrency, 
			sum(CASE WHEN CA.Type = @acctTypeDirectCons THEN BillExt ELSE 0 END) AS billconsAmt 
			FROM LedgerAP mySource
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
			AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
			INNER JOIN CA On mySource.Account = CA.Account
				WHERE psl.sessionID = @sessionID
			AND (CA.Type >= @acctTypeDirect AND CA.Type < @acctTypeIndirect)
			AND ProjectCost = 'Y'
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period
	
			UNION ALL
			SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3, 
			sum(Amount) AS direct, sum(AmountProjectCurrency) AS directProjectCurrency, 
			sum(AmountBillingCurrency) AS directBillingCurrency, 
			sum(BillExt) AS billdirect, 
			sum(CASE WHEN CA.Type = @acctTypeDirectCons THEN Amount ELSE 0 END) AS consAmt, 
			sum(CASE WHEN CA.Type = @acctTypeDirectCons THEN AmountProjectCurrency ELSE 0 END) AS consAmtProjectCurrency, 
			sum(CASE WHEN CA.Type = @acctTypeDirectCons THEN AmountBillingCurrency ELSE 0 END) AS consAmtBillingCurrency, 
			sum(CASE WHEN CA.Type = @acctTypeDirectCons THEN BillExt ELSE 0 END) AS billconsAmt 
			FROM LedgerEX mySource
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
			AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
			INNER JOIN CA On mySource.Account = CA.Account
				WHERE psl.sessionID = @sessionID
			AND (CA.Type >= @acctTypeDirect AND CA.Type < @acctTypeIndirect)
			AND ProjectCost = 'Y'
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period

			UNION ALL
			SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3, 
			sum(Amount) AS direct, sum(AmountProjectCurrency) AS directProjectCurrency, 
			sum(AmountBillingCurrency) AS directBillingCurrency, 
			sum(BillExt) AS billdirect, 
			sum(CASE WHEN CA.Type = @acctTypeDirectCons THEN Amount ELSE 0 END) AS consAmt, 
			sum(CASE WHEN CA.Type = @acctTypeDirectCons THEN AmountProjectCurrency ELSE 0 END) AS consAmtProjectCurrency, 
			sum(CASE WHEN CA.Type = @acctTypeDirectCons THEN AmountBillingCurrency ELSE 0 END) AS consAmtBillingCurrency, 
			sum(CASE WHEN CA.Type = @acctTypeDirectCons THEN BillExt ELSE 0 END) AS billconsAmt 
			FROM LedgerMisc mySource
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
			AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
			INNER JOIN CA On mySource.Account = CA.Account
				WHERE psl.sessionID = @sessionID
			AND (CA.Type >= @acctTypeDirect AND CA.Type < @acctTypeIndirect)
			AND ProjectCost = 'Y'
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period
		) AS myMiddleQry 
		GROUP BY WBS1, WBS2, WBS3, Period
	) AS myQry inner join PRSummarySub 
	on (PRSummarySub.WBS1 = myQry.WBS1 AND PRSummarySub.WBS2 = myQry.WBS2 
		AND PRSummarySub.WBS3 = myQry.WBS3 AND PRSummarySub.Period = myQry.Period)


	------------------------------------------------------------------
	--InDirect Expenses: set InDirectCost, InDirectBilling
		-- For Testing purposes..
		--WHERE psl.sessionID = 'NoUser20060221593466'
		--AND (Account >= '700.00' AND Account < '800.00') 

	UPDATE PRSummarySub
	SET 	PRSummarySub.InDirectCost = myQry.InDirectCost,
		PRSummarySub.InDirectCostProjectCurrency = myQry.InDirectCostProjectCurrency,
		PRSummarySub.InDirectCostBillingCurrency = myQry.InDirectCostBillingCurrency,
		PRSummarySub.InDirectBilling = myQry.InDirectBilling
	FROM (
		SELECT Period, WBS1, WBS2, WBS3,
		sum(InDirect) As InDirectCost,
		sum(InDirectProjectCurrency) As InDirectCostProjectCurrency,
		sum(InDirectBillingCurrency) As InDirectCostBillingCurrency,
		sum(billInDirect) As InDirectBilling
		FROM (
			SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3, 
			sum(Amount) AS InDirect, sum(AmountProjectCurrency) AS InDirectProjectCurrency, 
			sum(AmountBillingCurrency) AS InDirectBillingCurrency, sum(BillExt) AS billInDirect
			FROM LedgerAR mySource
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
				AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
			INNER JOIN CA On mySource.Account = CA.Account
			WHERE psl.sessionID = @sessionID
				AND (CA.Type >= @acctTypeInDirect AND CA.Type < @acctTypeOtherCharges)
				AND ProjectCost = 'Y'
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period
	
			UNION ALL
			SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3, 
			sum(Amount) AS InDirect, sum(AmountProjectCurrency) AS InDirectProjectCurrency, 
			sum(AmountBillingCurrency) AS InDirectBillingCurrency, sum(BillExt) AS billInDirect
			FROM LedgerAP mySource
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
				AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
			INNER JOIN CA On mySource.Account = CA.Account
			WHERE psl.sessionID = @sessionID
				AND (CA.Type >= @acctTypeInDirect AND CA.Type < @acctTypeOtherCharges)
				AND ProjectCost = 'Y'
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period
	
			UNION ALL
			SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3, 
			sum(Amount) AS InDirect, sum(AmountProjectCurrency) AS InDirectProjectCurrency, 
			sum(AmountBillingCurrency) AS InDirectBillingCurrency, sum(BillExt) AS billInDirect
			FROM LedgerEX mySource
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
				AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
			INNER JOIN CA On mySource.Account = CA.Account
			WHERE psl.sessionID = @sessionID
				AND (CA.Type >= @acctTypeInDirect AND CA.Type < @acctTypeOtherCharges)
				AND ProjectCost = 'Y'
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period

			UNION ALL
			SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3, 
			sum(Amount) AS InDirect, sum(AmountProjectCurrency) AS InDirectProjectCurrency, 
			sum(AmountBillingCurrency) AS InDirectBillingCurrency, sum(BillExt) AS billInDirect
			FROM LedgerMisc mySource
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
				AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
			INNER JOIN CA On mySource.Account = CA.Account
			WHERE psl.sessionID = @sessionID
				AND (CA.Type >= @acctTypeInDirect AND CA.Type < @acctTypeOtherCharges)
				AND ProjectCost = 'Y'
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period
		) AS myMiddleQry 
		GROUP BY WBS1, WBS2, WBS3, Period
	) AS myQry inner join PRSummarySub 
	on (PRSummarySub.WBS1 = myQry.WBS1 AND PRSummarySub.WBS2 = myQry.WBS2 
		AND PRSummarySub.WBS3 = myQry.WBS3 AND PRSummarySub.Period = myQry.Period)


	------------------------------------------------------------------
	--Reimbursable Expenses: set ReimbConsCost, ReimbOtherCost, ReimbConsBilling, ReimbOtherBilling
		-- For Testing purposes..
		--sum(CASE WHEN Account >= '500.00' AND Account <= '519.99' THEN Amount ELSE 0 END) AS consAmt, 
		--sum(CASE WHEN Account >= '500.00' AND Account <= '519.99' THEN BillExt ELSE 0 END) AS billconsAmt 
		--WHERE psl.sessionID = 'NoUser20060221593466'
		--AND (Account >= '700.00' OR Account < '600.00') 

	UPDATE PRSummarySub
	SET 	PRSummarySub.ReimbConsCost = myQry.ReimbConsCost,
		PRSummarySub.ReimbOtherCost = myQry.ReimbOtherCost,
		PRSummarySub.ReimbConsCostProjectCurrency = myQry.ReimbConsCostProjectCurrency,
		PRSummarySub.ReimbOtherCostProjectCurrency = myQry.ReimbOtherCostProjectCurrency,
		PRSummarySub.ReimbConsCostBillingCurrency = myQry.ReimbConsCostBillingCurrency,
		PRSummarySub.ReimbOtherCostBillingCurrency = myQry.ReimbOtherCostBillingCurrency,
		PRSummarySub.ReimbConsBilling = myQry.ReimbConsBilling,
		PRSummarySub.ReimbOtherBilling = myQry.ReimbOtherBilling
	FROM (
		SELECT Period, WBS1, WBS2, WBS3,
		sum(consAmt) As ReimbConsCost, sum(amt - consAmt) As ReimbOtherCost,
		sum(consAmtProjectCurrency) As ReimbConsCostProjectCurrency, sum(AmtProjectCurrency - consAmtProjectCurrency) As ReimbOtherCostProjectCurrency,
		sum(consAmtBillingCurrency) As ReimbConsCostBillingCurrency, sum(AmtBillingCurrency - consAmtBillingCurrency) As ReimbOtherCostBillingCurrency,
		sum(billconsAmt) As ReimbConsBilling, sum(billamt - billconsAmt) As ReimbOtherBilling
		FROM (
			SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3, 
			sum(Amount) AS amt, sum(AmountProjectCurrency) AS amtProjectCurrency,
			sum(AmountBillingCurrency) AS amtBillingCurrency,
			sum(billExt) AS billamt,
			sum(CASE WHEN CA.Type = @acctTypeReimbCons THEN Amount ELSE 0 END) AS consAmt, 
			sum(CASE WHEN CA.Type = @acctTypeReimbCons THEN AmountProjectCurrency ELSE 0 END) AS consAmtProjectCurrency, 
			sum(CASE WHEN CA.Type = @acctTypeReimbCons THEN AmountBillingCurrency ELSE 0 END) AS consAmtBillingCurrency, 
			sum(CASE WHEN CA.Type = @acctTypeReimbCons THEN BillExt ELSE 0 END) AS billconsAmt 
			FROM LedgerAR mySource
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
			AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
			INNER JOIN CA On mySource.Account = CA.Account
				WHERE psl.sessionID = @sessionID
			AND (CA.Type >= @acctTypeReimb AND CA.Type < @acctTypeDirect)
			AND ProjectCost = 'Y'
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period

			UNION ALL
			SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3, 
			sum(Amount) AS amt, sum(AmountProjectCurrency) AS amtProjectCurrency,
			sum(AmountBillingCurrency) AS amtBillingCurrency,
			sum(billExt) AS billamt,
			sum(CASE WHEN CA.Type = @acctTypeReimbCons THEN Amount ELSE 0 END) AS consAmt, 
			sum(CASE WHEN CA.Type = @acctTypeReimbCons THEN AmountProjectCurrency ELSE 0 END) AS consAmtProjectCurrency, 
			sum(CASE WHEN CA.Type = @acctTypeReimbCons THEN AmountBillingCurrency ELSE 0 END) AS consAmtBillingCurrency, 
			sum(CASE WHEN CA.Type = @acctTypeReimbCons THEN BillExt ELSE 0 END) AS billconsAmt 
			FROM LedgerAP mySource
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
			AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
			INNER JOIN CA On mySource.Account = CA.Account
				WHERE psl.sessionID = @sessionID
			AND (CA.Type >= @acctTypeReimb AND CA.Type < @acctTypeDirect)
			AND ProjectCost = 'Y'
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period
	
			UNION ALL
			SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3, 
			sum(Amount) AS amt, sum(AmountProjectCurrency) AS amtProjectCurrency,
			sum(AmountBillingCurrency) AS amtBillingCurrency,
			sum(billExt) AS billamt,
			sum(CASE WHEN CA.Type = @acctTypeReimbCons THEN Amount ELSE 0 END) AS consAmt, 
			sum(CASE WHEN CA.Type = @acctTypeReimbCons THEN AmountProjectCurrency ELSE 0 END) AS consAmtProjectCurrency, 
			sum(CASE WHEN CA.Type = @acctTypeReimbCons THEN AmountBillingCurrency ELSE 0 END) AS consAmtBillingCurrency, 
			sum(CASE WHEN CA.Type = @acctTypeReimbCons THEN BillExt ELSE 0 END) AS billconsAmt 
			FROM LedgerEX mySource
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
			AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
			INNER JOIN CA On mySource.Account = CA.Account
				WHERE psl.sessionID = @sessionID
			AND (CA.Type >= @acctTypeReimb AND CA.Type < @acctTypeDirect)
			AND ProjectCost = 'Y'
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period
	
			UNION ALL
			SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3, 
			sum(Amount) AS amt, sum(AmountProjectCurrency) AS amtProjectCurrency,
			sum(AmountBillingCurrency) AS amtBillingCurrency,
			sum(billExt) AS billamt,
			sum(CASE WHEN CA.Type = @acctTypeReimbCons THEN Amount ELSE 0 END) AS consAmt, 
			sum(CASE WHEN CA.Type = @acctTypeReimbCons THEN AmountProjectCurrency ELSE 0 END) AS consAmtProjectCurrency, 
			sum(CASE WHEN CA.Type = @acctTypeReimbCons THEN AmountBillingCurrency ELSE 0 END) AS consAmtBillingCurrency, 
			sum(CASE WHEN CA.Type = @acctTypeReimbCons THEN BillExt ELSE 0 END) AS billconsAmt 
			FROM LedgerMisc mySource
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
			AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
			INNER JOIN CA On mySource.Account = CA.Account
				WHERE psl.sessionID = @sessionID
			AND (CA.Type >= @acctTypeReimb AND CA.Type < @acctTypeDirect)
			AND ProjectCost = 'Y'
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period

		) AS myMiddleQry 
		GROUP BY WBS1, WBS2, WBS3, Period
	) AS myQry inner join PRSummarySub 
	on (PRSummarySub.WBS1 = myQry.WBS1 AND PRSummarySub.WBS2 = myQry.WBS2 
		AND PRSummarySub.WBS3 = myQry.WBS3 AND PRSummarySub.Period = myQry.Period)


	------------------------------------------------------------------
	--LedgerAR: AR
	UPDATE PRSummarySub
	SET 	PRSummarySub.AR = myQry.ar,
		PRSummarySub.ARProjectCurrency = myQry.arProjectCurrency,
		PRSummarySub.ARBillingCurrency = myQry.arBillingCurrency
	FROM (
		SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3,
		sum(CASE WHEN TransType = N'CR' AND SubType = 'R' THEN Amount ELSE -Amount END) AS ar,
		sum(CASE WHEN TransType = N'CR' AND SubType = 'R' THEN AmountProjectCurrency ELSE -AmountProjectCurrency END) AS arProjectCurrency,
		sum(CASE WHEN TransType = N'CR' AND SubType = 'R' THEN AmountBillingCurrency ELSE -AmountBillingCurrency END) AS arBillingCurrency
		FROM LedgerAR mySource 
		INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
		AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
		WHERE psl.sessionID = @sessionID
		AND ((TransType = N'IN' AND AutoEntry = 'N' AND IsNull(SubType,' ') <> 'X') 
			OR (TransType = N'CR' AND (SubType = 'R' OR (SubType = 'T' AND IsNull(Invoice,N' ') <> N' ')))) 
		GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period
	) AS myQry inner join PRSummarySub 
	on (PRSummarySub.WBS1 = myQry.WBS1 AND PRSummarySub.WBS2 = myQry.WBS2 
		AND PRSummarySub.WBS3 = myQry.WBS3 AND PRSummarySub.Period = myQry.Period)


	------------------------------------------------------------------
	--LedgerAR: Received
	UPDATE PRSummarySub
	SET 	PRSummarySub.Received = myQry.Received,
		PRSummarySub.ReceivedProjectCurrency = myQry.ReceivedProjectCurrency,
		PRSummarySub.ReceivedBillingCurrency = myQry.ReceivedBillingCurrency
	FROM (
		SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3,
		sum(-Amount) AS Received,
		sum(-AmountProjectCurrency) AS ReceivedProjectCurrency,
		sum(-AmountBillingCurrency) AS ReceivedBillingCurrency
		FROM LedgerAR mySource 
		INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
		AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
		WHERE psl.sessionID = @sessionID
		AND TransType = N'CR' AND AutoEntry = 'N'
		AND (SubType IN ('R', 'X') OR SubType = 'T' AND IsNull(Invoice, N' ') = N' ') 
		GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period
	) AS myQry inner join PRSummarySub 
	on (PRSummarySub.WBS1 = myQry.WBS1 AND PRSummarySub.WBS2 = myQry.WBS2 
		AND PRSummarySub.WBS3 = myQry.WBS3 AND PRSummarySub.Period = myQry.Period)


	------------------------------------------------------------------
	--Revenue
	UPDATE PRSummarySub
	SET 	PRSummarySub.Revenue = myQry.rev,
		PRSummarySub.RevenueProjectCurrency = myQry.revProjectCurrency,
		PRSummarySub.RevenueBillingCurrency = myQry.revBillingCurrency
	FROM (
		SELECT Max(Period) AS Period, Max(WBS1) AS WBS1, Max(WBS2) AS WBS2, 
		Max(WBS3) AS WBS3, sum(rev) AS rev, 
		sum(revProjectCurrency) AS revProjectCurrency,
		sum(revBillingCurrency) AS revBillingCurrency
		FROM (
			SELECT Max(psl.Period) AS Period, Max(psl.WBS1) AS WBS1, Max(psl.WBS2) AS WBS2, 
			Max(psl.WBS3) AS WBS3, sum(-Amount) AS rev,
			sum(-AmountProjectCurrency) AS revProjectCurrency,
			sum(-AmountBillingCurrency) AS revBillingCurrency
			FROM LedgerAR mySource
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
				AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
			INNER JOIN CA On mySource.Account = CA.Account
			WHERE psl.sessionID = @sessionID
				AND (CA.Type >= @acctTypeRevenue AND CA.Type < @acctTypeReimb)
				AND (NOT (TransType = N'IN' AND LinkWBS1 IS NOT NULL))
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period

			UNION ALL
			SELECT Max(psl.Period) AS Period, Max(psl.WBS1) AS WBS1, Max(psl.WBS2) AS WBS2, 
			Max(psl.WBS3) AS WBS3, sum(-Amount) AS rev,
			sum(-AmountProjectCurrency) AS revProjectCurrency,
			sum(-AmountBillingCurrency) AS revBillingCurrency
			FROM LedgerAP mySource
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
				AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
			INNER JOIN CA On mySource.Account = CA.Account
			WHERE psl.sessionID = @sessionID
				AND (CA.Type >= @acctTypeRevenue AND CA.Type < @acctTypeReimb)
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period

			UNION ALL
			SELECT Max(psl.Period) AS Period, Max(psl.WBS1) AS WBS1, Max(psl.WBS2) AS WBS2, 
			Max(psl.WBS3) AS WBS3, sum(-Amount) AS rev,
			sum(-AmountProjectCurrency) AS revProjectCurrency,
			sum(-AmountBillingCurrency) AS revBillingCurrency
			FROM LedgerEX mySource
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
				AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
			INNER JOIN CA On mySource.Account = CA.Account
			WHERE psl.sessionID = @sessionID
				AND (CA.Type >= @acctTypeRevenue AND CA.Type < @acctTypeReimb)
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period

			UNION ALL
			SELECT Max(psl.Period) AS Period, Max(psl.WBS1) AS WBS1, Max(psl.WBS2) AS WBS2, 
			Max(psl.WBS3) AS WBS3, sum(-Amount) AS rev,
			sum(-AmountProjectCurrency) AS revProjectCurrency,
			sum(-AmountBillingCurrency) AS revBillingCurrency
			FROM LedgerMisc mySource
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
				AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
			INNER JOIN CA On mySource.Account = CA.Account
			WHERE psl.sessionID = @sessionID
				AND (CA.Type >= @acctTypeRevenue AND CA.Type < @acctTypeReimb)
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period
		) AS myMiddleQry 
		GROUP BY WBS1, WBS2, WBS3, Period
	) AS myQry inner join PRSummarySub 
	on (PRSummarySub.WBS1 = myQry.WBS1 AND PRSummarySub.WBS2 = myQry.WBS2 
		AND PRSummarySub.WBS3 = myQry.WBS3 AND PRSummarySub.Period = myQry.Period)


	------------------------------------------------------------------
	--LedgerAR: Billed Columns
	UPDATE PRSummarySub
	SET 	PRSummarySub.Billed = myQry.bill,
		PRSummarySub.BilledLab = myQry.billLab,
		PRSummarySub.BilledCons = myQry.billCons,
		PRSummarySub.BilledExp = myQry.billExp,
		PRSummarySub.BilledFee = myQry.billFee,
		PRSummarySub.BilledUnit = myQry.billUnit,
		PRSummarySub.BilledAddOn = myQry.billAddOn,
		PRSummarySub.BilledTaxes = myQry.billTaxes,
		PRSummarySub.BilledInterest = myQry.billInterest,
		PRSummarySub.BilledOther = myQry.billOther,
		PRSummarySub.BilledProjectCurrency = myQry.billProjectCurrency,
		PRSummarySub.BilledLabProjectCurrency = myQry.billLabProjectCurrency,
		PRSummarySub.BilledConsProjectCurrency = myQry.billConsProjectCurrency,
		PRSummarySub.BilledExpProjectCurrency = myQry.billExpProjectCurrency,
		PRSummarySub.BilledFeeProjectCurrency = myQry.billFeeProjectCurrency,
		PRSummarySub.BilledUnitProjectCurrency = myQry.billUnitProjectCurrency,
		PRSummarySub.BilledAddOnProjectCurrency = myQry.billAddOnProjectCurrency,
		PRSummarySub.BilledTaxesProjectCurrency = myQry.billTaxesProjectCurrency,
		PRSummarySub.BilledInterestProjectCurrency = myQry.billInterestProjectCurrency,
		PRSummarySub.BilledOtherProjectCurrency = myQry.billOtherProjectCurrency,
		PRSummarySub.BilledBillingCurrency = myQry.billBillingCurrency,
		PRSummarySub.BilledLabBillingCurrency = myQry.billLabBillingCurrency,
		PRSummarySub.BilledConsBillingCurrency = myQry.billConsBillingCurrency,
		PRSummarySub.BilledExpBillingCurrency = myQry.billExpBillingCurrency,
		PRSummarySub.BilledFeeBillingCurrency = myQry.billFeeBillingCurrency,
		PRSummarySub.BilledUnitBillingCurrency = myQry.billUnitBillingCurrency,
		PRSummarySub.BilledAddOnBillingCurrency = myQry.billAddOnBillingCurrency,
		PRSummarySub.BilledTaxesBillingCurrency = myQry.billTaxesBillingCurrency,
		PRSummarySub.BilledInterestBillingCurrency = myQry.billInterestBillingCurrency,
		PRSummarySub.BilledOtherBillingCurrency = myQry.billOtherBillingCurrency

	FROM (
		SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3, 
		sum(-Amount) AS bill,
		sum(CASE WHEN WBS1Column = 4 THEN -Amount ELSE 0 END) AS billLab,
		sum(CASE WHEN WBS1Column = 2 THEN -Amount ELSE 0 END) AS billCons,
		sum(CASE WHEN WBS1Column = 3 THEN -Amount ELSE 0 END) AS billExp,
		sum(CASE WHEN WBS1Column = 1 THEN -Amount ELSE 0 END) AS billFee,
		sum(CASE WHEN WBS1Column = 5 THEN -Amount ELSE 0 END) AS billUnit,
		sum(CASE WHEN WBS1Column = 6 THEN -Amount ELSE 0 END) AS billAddOn,
		sum(CASE WHEN WBS1Column = 7 THEN -Amount ELSE 0 END) AS billTaxes,
		sum(CASE WHEN SubType is null AND mySource.Account is null THEN -Amount ELSE 0 END) AS billInterest,
		sum(CASE WHEN mySource.Account is not null and (WBS1Column = 0 OR WBS1Column is null) THEN -Amount ELSE 0 END) AS billOther, 

		sum(-AmountProjectCurrency) AS billProjectCurrency,
		sum(CASE WHEN WBS1Column = 4 THEN -AmountProjectCurrency ELSE 0 END) AS billLabProjectCurrency,
		sum(CASE WHEN WBS1Column = 2 THEN -AmountProjectCurrency ELSE 0 END) AS billConsProjectCurrency,
		sum(CASE WHEN WBS1Column = 3 THEN -AmountProjectCurrency ELSE 0 END) AS billExpProjectCurrency,
		sum(CASE WHEN WBS1Column = 1 THEN -AmountProjectCurrency ELSE 0 END) AS billFeeProjectCurrency,
		sum(CASE WHEN WBS1Column = 5 THEN -AmountProjectCurrency ELSE 0 END) AS billUnitProjectCurrency,
		sum(CASE WHEN WBS1Column = 6 THEN -AmountProjectCurrency ELSE 0 END) AS billAddOnProjectCurrency,
		sum(CASE WHEN WBS1Column = 7 THEN -AmountProjectCurrency ELSE 0 END) AS billTaxesProjectCurrency,
		sum(CASE WHEN SubType is null AND mySource.Account is null THEN -AmountProjectCurrency ELSE 0 END) AS billInterestProjectCurrency,
		sum(CASE WHEN mySource.Account is not null and (WBS1Column = 0 OR WBS1Column is null) THEN -AmountProjectCurrency ELSE 0 END) AS billOtherProjectCurrency, 

		sum(-AmountBillingCurrency) AS billBillingCurrency,
		sum(CASE WHEN WBS1Column = 4 THEN -AmountBillingCurrency ELSE 0 END) AS billLabBillingCurrency,
		sum(CASE WHEN WBS1Column = 2 THEN -AmountBillingCurrency ELSE 0 END) AS billConsBillingCurrency,
		sum(CASE WHEN WBS1Column = 3 THEN -AmountBillingCurrency ELSE 0 END) AS billExpBillingCurrency,
		sum(CASE WHEN WBS1Column = 1 THEN -AmountBillingCurrency ELSE 0 END) AS billFeeBillingCurrency,
		sum(CASE WHEN WBS1Column = 5 THEN -AmountBillingCurrency ELSE 0 END) AS billUnitBillingCurrency,
		sum(CASE WHEN WBS1Column = 6 THEN -AmountBillingCurrency ELSE 0 END) AS billAddOnBillingCurrency,
		sum(CASE WHEN WBS1Column = 7 THEN -AmountBillingCurrency ELSE 0 END) AS billTaxesBillingCurrency,
		sum(CASE WHEN SubType is null AND mySource.Account is null THEN -AmountBillingCurrency ELSE 0 END) AS billInterestBillingCurrency,
		sum(CASE WHEN mySource.Account is not null and (WBS1Column = 0 OR WBS1Column is null) THEN -AmountBillingCurrency ELSE 0 END) AS billOtherBillingCurrency 

		FROM LedgerAR mySource
		INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
			AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
		LEFT JOIN CA On mySource.Account = CA.Account
		LEFT JOIN CFGInvMap ON mySource.Account = CFGInvMap.Account
			AND (SELECT CASE WHEN (SELECT MulticompanyEnabled FROM FW_CFGSystem) = 'Y' 
				THEN subString(mySource.Org,(SELECT org1Start FROM CFGFormat),(SELECT org1Length FROM CFGFormat))
				ELSE ' ' END)=CFGInvMap.Company
		WHERE psl.sessionID = @sessionID
			AND TransType = N'IN' AND IsNull(SubType, ' ') <> 'I'
			AND IsNull(SubType, ' ') <> 'R' AND AutoEntry = 'N'
			AND (CA.Type >= @acctTypeRevenue OR mySource.Account IS NULL)
			AND (NOT (TransType = N'IN' AND LinkWBS1 IS NOT NULL))	
		GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period
	) AS myQry inner join PRSummarySub 
	on (PRSummarySub.WBS1 = myQry.WBS1 AND PRSummarySub.WBS2 = myQry.WBS2 
		AND PRSummarySub.WBS3 = myQry.WBS3 AND PRSummarySub.Period = myQry.Period)

	UPDATE PRSummarySub
	SET 	PRSummarySub.ModUser =@visionuser, PRSummarySub.ModDate = getDate()
	FROM PRSummarySub
	INNER JOIN PRSummaryWBSList AS psl ON psl.Period = PRSummarySub.Period 
	AND psl.WBS1 = PRSummarySub.WBS1 AND psl.WBS2 = PRSummarySub.WBS2 AND psl.WBS3 = PRSummarySub.WBS3
	WHERE psl.sessionID = @sessionID

	------------------------------------------------------------------
	--LedgerMisc: Unbilled Gains/Losses
	IF @MulticompanyEnabled = 'Y'
	 Begin
		UPDATE PRSummarySub
		SET 	PRSummarySub.UnbillGainLoss = myQry.UnbillGainLoss,
			PRSummarySub.UnbillGainLossProjectCurrency = myQry.UnbillGainLossProjectCurrency
		FROM (
			SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3,
			sum(Amount) AS UnbillGainLoss,
			sum(AmountProjectCurrency) AS UnbillGainLossProjectCurrency
			FROM LedgerMisc mySource 
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
			AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3
			INNER JOIN CFGAutoPosting on SubString(mySource.Org, @Org1Start, @Org1Length) = CFGAutoPosting.Company
			WHERE psl.sessionID = @sessionID
				AND ((CFGAutoPosting.UnbilledServices = mySource.Account)
					OR (CFGAutoPosting.UnbilledServices1 = mySource.Account)
					OR (CFGAutoPosting.UnbilledServices2 = mySource.Account)
					OR (CFGAutoPosting.UnbilledServices3 = mySource.Account)
					OR (CFGAutoPosting.UnbilledServices4 = mySource.Account)
					OR (CFGAutoPosting.UnbilledServices5 = mySource.Account))
			  AND mySource.GainsAndLossesType = 'U'
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period
		) AS myQry inner join PRSummarySub 
		on (PRSummarySub.WBS1 = myQry.WBS1 AND PRSummarySub.WBS2 = myQry.WBS2 
			AND PRSummarySub.WBS3 = myQry.WBS3 AND PRSummarySub.Period = myQry.Period)
	 End
	ELSE
	 Begin
		UPDATE PRSummarySub
		SET 	PRSummarySub.UnbillGainLoss = myQry.UnbillGainLoss,
			PRSummarySub.UnbillGainLossProjectCurrency = myQry.UnbillGainLossProjectCurrency
		FROM (
			SELECT psl.Period, psl.WBS1, psl.WBS2, psl.WBS3,
			sum(Amount) AS UnbillGainLoss,
			sum(AmountProjectCurrency) AS UnbillGainLossProjectCurrency
			FROM LedgerMisc mySource 
			INNER JOIN PRSummaryWBSList AS psl ON psl.Period = mySource.Period AND psl.WBS1 = mySource.WBS1
			AND psl.WBS2 = mySource.WBS2 AND psl.WBS3 = mySource.WBS3,
			CFGAutoPosting
			WHERE psl.sessionID = @sessionID
				AND ((CFGAutoPosting.UnbilledServices = mySource.Account)
					OR (CFGAutoPosting.UnbilledServices1 = mySource.Account)
					OR (CFGAutoPosting.UnbilledServices2 = mySource.Account)
					OR (CFGAutoPosting.UnbilledServices3 = mySource.Account)
					OR (CFGAutoPosting.UnbilledServices4 = mySource.Account)
					OR (CFGAutoPosting.UnbilledServices5 = mySource.Account))
			  AND mySource.GainsAndLossesType = 'U'
			GROUP BY psl.WBS1, psl.WBS2, psl.WBS3, psl.Period
		) AS myQry inner join PRSummarySub 
		on (PRSummarySub.WBS1 = myQry.WBS1 AND PRSummarySub.WBS2 = myQry.WBS2 
			AND PRSummarySub.WBS3 = myQry.WBS3 AND PRSummarySub.Period = myQry.Period)
	 End
END

GO
