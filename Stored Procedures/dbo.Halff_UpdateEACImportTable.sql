SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Halff_UpdateEACImportTable]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Update the EAC from the Backlog application
	execute Halff_UpdateEAC

	-- Update the fee types custom fields
	execute Halff_UpdateFeeTypes

	-- Update fields in the custom fields table for ETC & percent complete
	execute Halff_UpdateETC_PCTComp

END
GO
