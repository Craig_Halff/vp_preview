SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
create Procedure [dbo].[InsertKeyCvtDriver] 
@Entity varchar(50)
as
begin
declare @InfoCenter varchar(50)
declare @custDataType varchar(50)
Delete from keyConvertDriver where Entity = @Entity
set @InfoCenter = null
set @custDataType = null
if (@Entity = 'Employee')
begin
set @InfoCenter = 'Employees'
set @custDataType = 'Employee'
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'Activity',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'ADPExport',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'AlertsDashboard',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'BIED',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'BILD',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'billLabDetail',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'BTRCTEmpls',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'BTROTEmpls',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'BTRRTEmpls',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'CustomProposalEmployee',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'CustomProposalEmployeeGraphics',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'CustomProposalProjTeam',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'ekDetail',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'ekMaster',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMMain',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMCompany',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMAccrual',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMAccrualDetail',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMActivity',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMClientAssoc',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMContactAssoc',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMDegree',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMEKGroups',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EmployeeFileLinks',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMProjectAssoc',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMRegistration',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMResume',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMSkills',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMSubscr',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMTKGroups',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'erDetail',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EX',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EXAdvance',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'exChecks',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'exMaster',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'laDetail',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'laMaster',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'LD',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'LedgerAP',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'LedgerAR',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'LedgerEX',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'LedgerMisc',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'SEUser',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'SF255Employees',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'tkDetail',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'tkMaster',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'tsDetail',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'tsMaster',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'XChargeWk',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMDirectDeposit',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMDirectDepositDetail',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'exChecksE',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PR',N'ProjMgr','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'RPPlan',N'ProjMgr','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PR',N'Principal','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'RPPlan',N'Principal','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'tkMaster',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'tkMaster',N'ApprovedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'ekMaster',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'ekMaster',N'ApprovedBy','N');

/*new for 2.0*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'CostCTEmpls',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'CostRTEmpls',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'EMLocale',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'EMPayroll',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'EMPayrollDetail',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'EMPayrollWithholding',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'EMPayrollWithholdingDetail',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'EMPhoto',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'EMProjectAssocTemplate',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'PYChecks',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'PYChecksA',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'PYChecksE',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'PYChecksW',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'SECalendar',N'AccessEmployee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'SECalendar',N'PrimaryEmployee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'SF255EmployeeGraphics',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'PRTemplate',N'ProjMgr','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'PRTemplate',N'Principal','N');
/*New for 2.0 beta 2*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'MktCampaign',N'CampaignMgr','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'MktCampaign',N'Manager3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'MktCampaign',N'MktgMgr','N');

/*added supervisor fields*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EMCompany',N'Supervisor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PR',N'Supervisor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PRTemplate',N'Supervisor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'RPPlan',N'Supervisor','N');
/**/
/*new for 3.0*/
--sf330personnel
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'CFGPOBuyer',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'DocumentsEM',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'FormW2Data',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'ICBillingWK',N'Employee','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'Leads',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'POMaster',N'Buyer','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'POMaster',N'AuthorizedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'POMaster',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'POCOMaster',N'Buyer','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'POCOMaster',N'AuthorizedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'POCOMaster',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'POPRMaster',N'RequestedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'POPRMaster',N'AuthorizedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'POPRMaster',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'POPQMaster',N'RequestedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'POPQMaster',N'AuthorizedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'POPQMaster',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PODetail',N'RequestedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'RPAssignment',N'ResourceID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'SF330',N'SignorEmployee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'SF330FirmsPart2',N'SignorEmployee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'SF330Personnel',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'WorkflowEMActivity',N'Employee','N');

/*added 3.0 sp5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'ICBillLabDetail',N'Employee','N');

/*added 4.1*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'EMPayrollWithholdingWage',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'EMPayrollWithholdingDetailWage',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'PYChecksWWage',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'EMPayrollContribution',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'EMPayrollContributionDetail',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'EMPayrollContributionWage',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'EMPayrollContributionDetailWage',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'PYChecksC',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'PYChecksCWage',N'Employee','N');

/*new for 5.0*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'tkUnitDetail',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'unDetail',N'Employee','N');
/*new for 7.0 - 07/15/09 UpDetail now also has an employee field */
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'upDetail',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'CFGInvApprover',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'InvMaster',N'PerformedBy','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'InvMaster',N'SubmittedBy','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'ItemRequestMaster',N'RequestedBy','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'ItemRequestMaster',N'AuthorizedBy','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'ItemRequestMaster',N'SubmittedBy','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'tkBreakTime',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'ReceiveMaster',N'Employee','N');
/*end 5.0*/

/*new for 5.1*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTRRT',N'FilterProjMgr','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTRRT',N'FilterPrincipal','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTRRT',N'FilterSupervisor','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTRCT',N'FilterProjMgr','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTRCT',N'FilterPrincipal','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTRCT',N'FilterSupervisor','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTRLT',N'FilterProjMgr','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTRLT',N'FilterPrincipal','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTRLT',N'FilterSupervisor','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTROT',N'FilterProjMgr','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTROT',N'FilterPrincipal','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTROT',N'FilterSupervisor','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTEA',N'FilterProjMgr','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTEA',N'FilterPrincipal','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTEA',N'FilterSupervisor','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTEC',N'FilterProjMgr','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTEC',N'FilterPrincipal','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTEC',N'FilterSupervisor','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTEV',N'FilterProjMgr','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTEV',N'FilterPrincipal','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BTEV',N'FilterSupervisor','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BillingTemplate',N'FilterProjMgr','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BillingTemplate',N'FilterPrincipal','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'BillingTemplate',N'FilterSupervisor','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'UNTable',N'FilterProjMgr','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'UNTable',N'FilterPrincipal','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'UNTable',N'FilterSupervisor','N');
/*end 5.1*/

/*new for 6.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'apControl',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'cdControl',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'crControl',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'cvControl',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'erControl',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'exControl',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'inControl',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'jeControl',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'laControl',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'miControl',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'prControl',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'tsControl',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'unControl',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'upControl',N'SubmittedBy','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'apMaster',N'AuthorizedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'cdMaster',N'AuthorizedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'crMaster',N'AuthorizedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'cvMaster',N'AuthorizedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'erMaster',N'AuthorizedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'exMaster',N'AuthorizedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'inMaster',N'AuthorizedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'jeMaster',N'AuthorizedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'laMaster',N'AuthorizedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'miMaster',N'AuthorizedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'prMaster',N'AuthorizedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'tsMaster',N'AuthorizedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'unMaster',N'AuthorizedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'upMaster',N'AuthorizedBy','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'CFGTransApprover',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'CFGTransEmployee',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'CFGTransEmployee',N'Approval','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'exChecksB',N'Employee','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'ekDetail',N'LineItemApprover','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'tkDetail',N'LineItemApprover','N');
/*end 6.0*/

/*new for 6.2*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PNAssignment',N'ResourceID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PNPlan',N'ProjMgr','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PNPlan',N'Principal','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PNPlan',N'Supervisor','N');
/*end 6.2*/

/*new for 7.0 - 07/15/09 */
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'ContractCredit',N'Employee','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'ContractCreditTemplate',N'Employee','N');
/*end mod 7.0*/

/*new for 7.0 - 6/20/2011*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'GR',N'Supervisor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EmployeeRealizationWk',N'Employee','N');
/*end mod 7.0*/

/*new for 7.1*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Employee',N'SEUser',N'DelegateEmployee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PR',N'Biller','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PRTemplate',N'Biller','N');
/*end 7.1*/

/*new for 7.1*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'BillingXferAudit',N'ApprovedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'tkRevisionMaster',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'tkRevisionMaster',N'ModUser','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'tkRevisionDetail',N'Employee','N');
/*end 7.1*/

/*new for 7.2*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'exChecksTax',N'Employee','N');
/*end 7.2*/

/*new for 7.3*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'CFGCreditCardSecondaryEmployee',N'Employee','N');
/*end 7.3*/

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EKDocuments',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EKDocumentsDetail',N'Employee','N');

/*new for 7.4*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'ApprovalItem',N'AssignUSR_ID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'ApprovalItemHistory',N'EmployeeID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'ApprovalRoleAssignment',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'Equipment',N'Buyer','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'ApprovalWorkflow',N'EmployeeAssignto','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'WorkflowActionCreateItem',N'EmployeeAssignto','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'CFGApprovalType',N'ApprovalAdmin','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'Equipment',N'ShopInspectionBy','N');
/*end 7.4*/

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'ekDetailTax',N'Employee','N');

/*new for 7.5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EQICEMAssoc',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'APApprovalMaster',N'RouteToEmployee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'APApprovalMaster',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'APApprovalNotes',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'AbsenceRequests',N'RequestedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'AbsenceRequests',N'SubmittedBy','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'CAB',N'SubmittedBy','N');
/*end 7.5*/

/*new for 7.6*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'FW_Attachments',N'Key1','N');
--JYC
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'ApprovalItem',N'ApplicationKey','N');
/*end 7.6*/

/*new for 1.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'Contacts',N'Owner','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'Employee',N'PNTask',N'ProjMgr','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'Employee',N'EMActivityOccurrence',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'EmCitizenship',N'Employee','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'Clendor',N'Owner','N');
/*end 1.0*/


/*new for 2.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PRDefaults',N'ProjMgr','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PRDefaults',N'Principal','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PRDefaults',N'Supervisor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PRDefaults',N'Biller','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'Employee',N'PRProposals',N'Employee','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'Employee',N'PRProposalsTemplate',N'Employee','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'tkCustomFields',N'Employee','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'InvoiceApprover',N'Employee','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PR',N'ProposalManager','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PR',N'MarketingCoordinator','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PR',N'BusinessDeveloperLead','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PRTemplate',N'ProposalManager','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PRTemplate',N'MarketingCoordinator','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PRTemplate',N'BusinessDeveloperLead','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PRDefaults',N'ProposalManager','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PRDefaults',N'MarketingCoordinator','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'PRDefaults',N'BusinessDeveloperLead','N');
/*end for 2.0*/

/*new for 3.5*/
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'Employee',N'ApprovalErrorLog',N'Employee','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'Employee',N'laDetailCustomFields',N'Employee','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'Employee',N'tsDetailCustomFields',N'Employee','N');
/*end for 3.5*/

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Employee',N'AnalysisCubesKPIOverride',N'DimensionRec','N');

end -- end employee insert

if (@Entity = 'Client')
begin
	set @InfoCenter = 'Clients'
	set @custDataType = 'client'
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'Clendor',N'Client','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'Activity',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'ARC',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'Clendor',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'CLAddress',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'ClientFileLinks',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'ClientToClientAssoc',N'FromClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'ClientToClientAssoc',N'ToClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'CLSubscr',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'Contacts',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'CustomProposal',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'CustomProposalClient',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'CustomProposalClientGraphics',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'CustomProposalContact',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'EMClientAssoc',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'PR',N'BillingClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'PR',N'ClientID','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'PRClientAssoc',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'RPPlan',N'ClientID','N');
/*3.0*/
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'ClientsFromLeads',N'ClientID','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'PRClientAssocTemplate',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'PRTemplate',N'BillingClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'PRTemplate',N'ClientID','N');

/*added 4.1*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'EMMain',N'ClientID','N');

/*added 5.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'Clendor',N'ParentID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'Clendor',N'ParentLevel1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'Clendor',N'ParentLevel2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'Clendor',N'ParentLevel3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'Clendor',N'ParentLevel4','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'SF255Consultant',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'SF330ProposedFirms',N'ClientID','N');

/*new for 6.2*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'PNPlan',N'ClientID','N');
/*end 6.2*/
/*new for 7.4*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Client',N'ClientPhoto',N'ClientID','N');
/*end 7.4 */

/*new for 7.6*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Client',N'FW_Attachments',N'Key1','N');
/*end 7.6 */

/*new for 1.0*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Client',N'PNTask',N'ClientID','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Client',N'MktCampaignClientAssoc',N'ClientID','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Client',N'CLActivity',N'ClientID','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Client',N'ClendorProjectAssoc',N'ClientID','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Client',N'ClendorProjectAssocTemplate',N'ClientID','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Client',N'ClientFromContacts',N'ClientID','N');
/*end 1.0 */

/*new for 2.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'PRDefaults',N'BillingClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'PRDefaults',N'ClientID','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'Client',N'PRCompetitionAssoc',N'ClientID','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'Client',N'PRCompetitionAssocTemplate',N'ClientID','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'Client',N'WorkflowCLActivity',N'ClientID','N');
/*end for 2.0*/

/*new for 3.5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'AR',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'ARCreditMemo',N'ClientID','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'Client',N'ARPreInvoice',N'ClientID','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'Client',N'billINMaster',N'ClientID','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'Client',N'billInvMaster',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'ICBillInvMaster',N'ClientID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Client',N'inMaster',N'ClientID','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'Client',N'TaxReporting',N'ClientID','N');
/*end for 3.5*/

end -- end Client insert

if (@Entity = 'Account')
begin
set @InfoCenter = 'ChartOfAccounts'
set @custDataType = 'account'
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'AccountCustomTabFields',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'apDetail',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'apppChecks',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'apppChecksW',N'FeeDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBanks',N'WireTransferFeeDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BIED',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BIED',N'XferAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'billINDetail',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'billUnitDetail',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BT',N'LabAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BT',N'ConAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BT',N'ExpAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BT',N'UnitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BT',N'FeeAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BT',N'AddOnAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BT',N'IntAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BTA',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BTEAAccts',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BTECAccts',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CA',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CA',N'CashBasisAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CABDetail',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'cdDetail',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAPDiscount',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAPLiability',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGARMap',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'LaborCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'MiscCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'PrintsCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'ConsAccrAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'EmplExpCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'EmplAdvCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBanks',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBillMainData',N'LabAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBillMainData',N'ConAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBillMainData',N'ExpAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBillMainData',N'UnitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBillMainData',N'FeeAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBillMainData',N'AddOnAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBillMainData',N'IntAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBillTaxesData',N'CreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBillTaxesData',N'DebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGEKCategory',N'DirectAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGEKCategory',N'ReimbAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGEKCategory',N'IndirectAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGEKCategory',N'CompanyPaidAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGEKMain',N'CompanyPaidAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGInvMap',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGInvMap',N'ARAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGInvMap',N'RetainageAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGOHAccounts',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGTimeAnalysis',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGXCharge',N'XChargeRegCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGXCharge',N'XChargeRegDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGXCharge',N'XChargeOHCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGXCharge',N'XChargeOHDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'crDetail',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'cvDetail',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'EB',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'ekDetail',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'exDetail',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'GLGroupDetail',N'StartAccount','Y');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'GLGroupDetail',N'EndAccount','Y');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'inDetail',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'jeDetail',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'LedgerAP',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'LedgerAP',N'XferAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'LedgerAR',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'LedgerAR',N'XferAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'LedgerEX',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'LedgerEX',N'XferAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'LedgerMisc',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'LedgerMisc',N'XferAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'miDetail',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'prDetail',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'RPConsultant',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'RPExpense',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'UNUnit',N'PostAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'UNUnit',N'RegAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'UNUnit',N'OHAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'UNUnit',N'CreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'unDetail',N'Account','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'XChargeWk',N'DebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'XChargeWk',N'CreditAccount','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'RetainedEarnCurrent','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'RetainedEarnPrevYrs','N');
/*new for 2.0*/

Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Account',N'BudgetMilestoneSeq',N'Account','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Account',N'CFGPYMain',N'BonusCostAccount','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Account',N'CFGPYMain',N'FICAAccount','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Account',N'CFGPYMain',N'OtherPayCostAccount','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Account',N'CFGPYMain',N'PayrollCreditAccount','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Account',N'CFGPYWHCodes',N'Account','N');
/*end 2.0*/
/*NEW for 3.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'apppChecksE',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BIED',N'AutoEntryAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BTDefaults',N'AddOnAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BTDefaults',N'ConAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BTDefaults',N'ExpAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BTDefaults',N'FeeAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BTDefaults',N'IntAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BTDefaults',N'LabAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'BTDefaults',N'UnitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CACompanyMapping',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'AcctsReceivableIntercompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'AcctsPayableIntercompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'AcctsReceivable','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UnbilledServices','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UninvoicedRevenue','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGConsolidationEliminations',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGConsolidationGroups',N'GainsAndLossesAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGConsolidationTranslationMethodOverrides',N'Account','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values ('Account','CFGEmployeeType','DirectAccount','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values ('Account','CFGEmployeeType','IndirectAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGEmployeeTypeAccounts',N'DirectAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGEmployeeTypeAccounts',N'IndirectAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingBSOtherCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingBSOtherDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingExpOHCostCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingExpOHCostDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingExpOHCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingExpOHDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingExpPromoCostCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingExpPromoCostDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingExpPromoCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingExpPromoDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingExpRegCostCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingExpRegCostDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingExpRegCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingExpRegDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingLabOHCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingLabOHDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingLabPromoCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingLabPromoDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingLabRegCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBilling',N'ICBillingLabRegDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'TaxReimbAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'TaxDirectAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'TaxIndirectAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'TaxBalanceSheetAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'ShipReimbAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'ShipDirectAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'ShipIndirectAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'ShipBalanceSheetAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscReimbAccount1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscDirectAccount1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscIndirectAccount1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscBalanceSheetAccount1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscReimbAccount2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscDirectAccount2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscIndirectAccount2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscBalanceSheetAccount2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscReimbAccount3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscDirectAccount3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscIndirectAccount3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscBalanceSheetAccount3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscReimbAccount4','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscDirectAccount4','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscIndirectAccount4','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscBalanceSheetAccount4','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscReimbAccount5','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscDirectAccount5','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscIndirectAccount5','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPOAccounts',N'MiscBalanceSheetAccount5','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CompanyPOCategory',N'BalanceSheetAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CompanyPOCategory',N'DirectAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CompanyPOCategory',N'IndirectAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CompanyPOCategory',N'ReimbAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'ConsolidationBudgetsDetail',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'Consolidations',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'Eliminations',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'ICBillingWK',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'ICBillingWK',N'CostCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'ICBillingWK',N'CostDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'ICBillingWK',N'CreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'ICBillingWK',N'DebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'ICBillingWK',N'ICAPCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'ICBillingWK',N'ICAPDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'ICBillingWK',N'ICARCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'ICBillingWK',N'ICARDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'ICBillingWK',N'ICSuspenseCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'ICBillingWK',N'ICSuspenseDebitAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'KeyConvertWorkPROrg',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'LedgerAP',N'AutoEntryAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'LedgerAR',N'AutoEntryAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'LedgerEX',N'AutoEntryAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'LedgerMisc',N'AutoEntryAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'POCommitment',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'RPUnit',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'VEAccounting',N'OHAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'VEAccounting',N'RegAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'VEAccounting',N'ReimbAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'VEEFTDetail',N'Account','N');

/*end 3.0*/
/*added 3.0 sp2*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGEmployeeTypeAccounts',N'OtherPayCostAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGEmployeeTypeAccounts',N'OtherPay2CostAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGEmployeeTypeAccounts',N'OtherPay3CostAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGEmployeeTypeAccounts',N'OtherPay4CostAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGEmployeeTypeAccounts',N'OtherPay5CostAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPYMain',N'OtherPay2CostAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPYMain',N'OtherPay3CostAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPYMain',N'OtherPay4CostAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPYMain',N'OtherPay5CostAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'SuspenseIntercompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'ForeignEMTypeDirectAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'ForeignEMTypeIndirectAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'RealizedGainsAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'RealizedLossesAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UnrealizedGainsAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UnrealizedLossesAccount','N');

/*added 3.0 sp5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'ICBillingLabRevenueAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'ICBillingExpRevenueAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBillingTerms',N'ICBillingVoucherExpenseAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGICBillingTerms',N'ICBillingVoucherBSOtherAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'ICBillingWK',N'VoucherExpenseAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'ICBillingWK',N'InvoiceLabRevenueAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'ICBillingWK',N'InvoiceExpRevenueAccount','N');

/*added 6.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'ICCashBasisSuspenseAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBillTaxesData',N'ICBillingARAcct','N');

/*added 4.1 */
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPYContribution',N'CreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGPYContribution',N'DebitAccount','N');

/*added 5.0 */
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAPExpenseCodeAccounts',N'ReimbAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAPExpenseCodeAccounts',N'DirectAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAPExpenseCodeAccounts',N'OverheadAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAPExpenseCodeAccounts',N'PromotionalAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UninvoicedRevenue1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UninvoicedRevenue2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UninvoicedRevenue3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UninvoicedRevenue4','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UninvoicedRevenue5','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UnbilledServices1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UnbilledServices2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UnbilledServices3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UnbilledServices4','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UnbilledServices5','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CompanyPOCategory',N'InventoryAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'InvDetail',N'Account','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGCashAcctSetup',N'StartAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGCashAcctSetup',N'EndAccount','N');

/*new for 5.1*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'upDetail',N'Account','N');
/*end 5.1*/

/*new for 6.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CA',N'UnrealizedGainAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CA',N'UnrealizedLossAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBillTaxesData',N'NonRecoverCreditAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBillTaxesData',N'NonRecoverDebitAccount','N');
/*end 6.0*/

/*new for 6.2*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'PNConsultant',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'PNExpense',N'Account','N');
/*end 6.2*/

/*new for 7.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGRealizationAllocationAccounts',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBanks',N'InProcessAccount','N');

/*end 7.0*/

/*7.0 SP1 changes*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UnbilledRealizedGainsAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UnbilledRealizedLossesAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UnbilledUnrealizedGainsAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAutoPosting',N'UnbilledUnrealizedLossesAccount','N');
/*end 7.0 SP1*/
/*7.2 changes*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBillTaxesICBillingExclusionAccounts',N'Account','N');
/*end 7.2*/
/*7.3 changes*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGCreditCardSecondary',N'CompanyPaidCreditAccount','N');
/*end 7.3*/

/*7.4 changes*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'ARPreInvoiceDetail',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBillMainData',N'FeeByDetailOverUnderAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBillMainData',N'PreInvoiceRecAccount','N');
/*end 7.4*/

/*7.5 changes*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGBankEntriesCodeData',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGAssetMain',N'FAAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGFAAssetTypeData',N'AssetAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGFAAssetTypeData',N'AccuDepAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGFAAssetTypeData',N'DepExpAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'Equipment',N'AccumulatedDeprAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'Equipment',N'DepreciationExpAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'EQICCost',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'TaxReporting',N'TaxAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'TaxReporting',N'BaseAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'APApprovalWBS',N'Account','N');
/*end 7.5*/

/*1.0 changes*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGEKCategory',N'PromotionalAccount','N');
/*end 1.0*/

/*2.0 changes*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'CFGEKCategory',N'PromotionalAccount','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'PRConsultant',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'PRConsultantTemplate',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'PRExpense',N'Account','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Account',N'PRExpenseTemplate',N'Account','N');
/*end 2.0*/

 
end -- end Account insert
if (@Entity = 'Vendor')
begin
set @InfoCenter = 'Vendors'
set @custDataType = 'vendor'
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'apMaster',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'apppChecks',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'apppChecksW',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'BIED',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'billConDetail',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'billExpDetail',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'BTEVVends',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'Contacts',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'CustomProposalVendor',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'CustomProposalVendorGraphics',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'cvMaster',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'EB',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'EMMain',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'Form1099Data',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'jeDetail',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'LedgerAP',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'LedgerAR',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'LedgerEX',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'LedgerMisc',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'RPConsultant',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'RPExpense',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'Clendor',N'Vendor','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'VEAddress',N'Vendor','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'VEFileLinks',N'Vendor','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'VEProjectAssoc',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'VESubscr',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'VO',N'Vendor','N');
/*new for 2.0*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Vendor',N'BudgetMilestoneSeq',N'Vendor','N');
--Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Vendor',N'VEProjectAssocTemplate',N'Vendor','N');
/*end 2.0*/
/*new for 3.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'apppChecksE',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'CFGPYMain',N'FICAVendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'CompanyItem',N'LastVendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'CompanyItem',N'PrimaryVendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'DocumentsVE',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'ICBillingWK',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'POMaster',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'POPQVendorDetail',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'POPRVendorDetail',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'POVoucherMaster',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'SF330ProposedFirms',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'VEAccounting',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'VEEFTDetail',N'Vendor','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'VendorAlias',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'VendorItem',N'Vendor','N');
/*end 3.0*/

/*added 3.0 sp5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'CFGICBilling',N'ICBillingVendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'ICBillingWK',N'InvoiceVendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'ICBillExpDetail',N'Vendor','N');

/*added 5.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'ItemRequestVendorDetail',N'Vendor','N');

/*new for 6.2*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'PNConsultant',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'PNExpense',N'Vendor','N');
/*end 6.2*/

/*new for 7.2*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'apDetail',N'OriginatingVendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'BIED',N'OriginatingVendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'cvDetail',N'OriginatingVendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'ekDetail',N'OriginatingVendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'exDetail',N'OriginatingVendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'LedgerAP',N'OriginatingVendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'LedgerAR',N'OriginatingVendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'LedgerEX',N'OriginatingVendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'LedgerMisc',N'OriginatingVendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'apppChecksTax',N'Vendor','N');
/*end for 7.2*/

/*new for 7.4*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'POPQVendorDocuments',N'Vendor','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Vendor',N'POPQVendorDocumentsDetail',N'Vendor','N');
/*end for 7.4*/
/*new for 7.5*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Vendor',N'Activity',N'Vendor','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Vendor',N'WorkflowActivity',N'Vendor','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Vendor',N'Equipment',N'LeaseVendor','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Vendor',N'EQICCost',N'Vendor','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Vendor',N'APApprovalMaster',N'Vendor','N');
/*end for 7.5 */

/*new for 7.6*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Vendor',N'FW_Attachments',N'Key1','N');
/*end for 7.6 */

end --insert Vendor insert
if (@Entity = 'WBS1')
begin
set @InfoCenter = 'Projects'
set @custDataType = 'WBS1'
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Activity',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ADPCode',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'apDetail','WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'apppChecks',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'apppChecksW',N'FeeDebitWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGBanks',N'WireTransferFeeDebitWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'AR',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ARC',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BIED',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BIED',N'BilledWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BIED',N'XferWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BILD',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BILD',N'BilledWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BILD',N'XferWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billAddDetail',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billAddDetail',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billARDetail',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billARDetail',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billBTDDetail',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billBTDDetail',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billConDetail',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billConDetail',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billConDetail',N'PostWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billConDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billExpDetail',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billExpDetail',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billExpDetail',N'PostWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billExpDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billFeeDetail',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billFeeDetail',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billFeeDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billFeeDetail',N'PostWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billINDetail',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billINDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billINMaster',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billINMaster',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billIntDetail',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billIntDetail',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billInvMaster',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billInvMaster',N'ByGroupWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billInvMaster',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billInvSums',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billInvSums',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billLabDetail',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billLabDetail',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billLabDetail',N'PostWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billLabDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billLimDetail',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billLimDetail',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billRetDetail',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billRetDetail',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billTaxDetail',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billTaxDetail',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billUnitDetail',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billUnitDetail',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billUnitDetail',N'PostWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billUnitDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BT',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BTA',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BTBG',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BTBGSubs',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BTBGSubs',N'SubWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BTF',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BTF',N'PostWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'cdDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGAPDiscount',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGAutoPosting',N'LaborCreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGAutoPosting',N'MiscCreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGAutoPosting',N'PrintsCreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGAutoPosting',N'ConsAccrWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGAutoPosting',N'EmplExpCreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGAutoPosting',N'EmplAdvCreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGEKCategory',N'CompanyPaidWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGEKMain',N'CompanyPaidWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGOHMain',N'OHVarianceWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGTimeAnalysis',N'StartWBS1','Y');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGTimeAnalysis',N'EndWBS1','Y');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGTKCategory',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'crDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CustomProposalProject',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CustomProposalProjectGraphics',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CustomProposalProjTeam',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'cvDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'EB',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ekDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'EMProjectAssoc',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'exDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'inDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'inMaster',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'jeDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'laDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LB',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LD',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LD',N'BilledWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LD',N'XferWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LedgerAP',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LedgerAP',N'BilledWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LedgerAP',N'XferWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LedgerAR',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LedgerAR',N'BilledWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LedgerAR',N'XferWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LedgerEX',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LedgerEX',N'BilledWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LedgerEX',N'XferWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LedgerMisc',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LedgerMisc',N'BilledWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LedgerMisc',N'XferWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'miDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'LabDistWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'MiscWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'UnitsBSWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'UnitsIndWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'UnitsOtherWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'OHVarianceWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'PayrollWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'FICAWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'PrintsWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'EmplExpWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'EmplAdvWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'XChargeRegDebitWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'XChargeRegCreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'XChargeOHDebitWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'XChargeOHCreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'XChargeRegFromWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'XChargeRegToWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'XChargeOHFromWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'XChargeOHToWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ConsAccrWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'OverheadAllocation',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PR',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PR',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PR',N'ProposalWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRAwards',N'WBS1','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRClientAssoc',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRContactAssoc',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRDescriptions',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'prDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRF',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRFileLinks',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRProjectCodes',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRSubscr',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ReportWBSList',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'RPAssignment',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'RPConsultant',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'RPExpense',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'RPTask',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'SF254Projects',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'SF255Projects',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'tkDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'tsDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'UNUnit',N'CreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'unDetail',N'WBS1','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'VEProjectAssoc',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'XChargeWk',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'XChargeWk',N'CreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'XChargeWk',N'DebitWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'XChargeWk',N'FromWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'XChargeWk',N'ToWBS1','N');
/*2.0 dbChanges */
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'CFGPYMain',N'BonusCostWBS1','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'CFGPYMain',N'FICAWBS1','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'CFGPYWHCodes',N'PayrollWBS1','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'PRProjectCodesTemplate',N'WBS1','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'PRTemplate',N'BillWBS1','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'PRTemplate',N'ProposalWBS1','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'RPPlan',N'WBS1','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'SF255ProjectGraphics',N'WBS1','N');
/*end 2.0 db changes*/
/*2.0 beta2*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'MktCampaign',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'MktCampaignProjectAssoc',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'MktCampaignProjectAssocTemplate',N'WBS1','N');

/*3.0 changes */
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values ('WBS1','CFGAutoPosting','GenOHWBS1','N');
/*6.0 Changes */
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGAutoPosting',N'RealizedGainsWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGAutoPosting',N'RealizedLossesWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGAutoPosting',N'UnrealizedGainsWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGAutoPosting',N'UnrealizedLossesWBS1','N');


Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGICBilling',N'ICBillingReclassWBS1','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS1',N'CFGPYMain',N'OtherPayCostWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGPYMain',N'OtherPay2CostWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGPYMain',N'OtherPay3CostWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGPYMain',N'OtherPay4CostWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGPYMain',N'OtherPay5CostWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillingWK',N'CreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillingWK',N'DebitWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillingWK',N'FromWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillingWK',N'ReclassCreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillingWK',N'ReclassDebitWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillingWK',N'ToWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillingWK',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'KeyConvertWorkPrOrg',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingBSOtherCreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingBSOtherDebitWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingExpOHCreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingExpOHDebitWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingExpOHFromWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingExpOHToWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingExpPromoCreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingExpPromoDebitWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingExpPromoFromWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingExpPromoToWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingExpRegCreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingExpRegDebitWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingExpRegFromWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingExpRegToWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingLabOHCreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingLabOHDebitWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingLabOHFromWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingLabOHToWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingLabPromoCreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingLabPromoDebitWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingLabPromoFromWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingLabPromoToWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingLabRegCreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingLabRegDebitWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingLabRegFromWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'ICBillingLabRegToWBS1','N');

/*6.0 Changes */
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'RealizedGainsWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'RealizedLossesWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'UnrealizedGainsWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'UnrealizedLossesWBS1','N');

--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values ('WBS1','PMKOrgCvt','WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'POCommitment',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'POCostDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'POPQCostDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'POPQWBS',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'POPRCostDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'POPRWBS',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'POWBS',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRChargeCompanies',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRDefaults',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRDefaults',N'ProposalWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRSummarySub',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRSummaryMain',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRSummaryWBSList',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'RPUnit',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'SavedInvoices',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'SF330PersonnelProjects',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'SF330Projects',N'WBS1','N');
/*end 3.0 changes*/

/*added 3.0 sp5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGICBillingTerms',N'ICBillingInvoiceWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGICBillingTerms',N'ICBillingVoucherWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LedgerAP',N'LinkWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LedgerAR',N'LinkWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LedgerEX',N'LinkWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'LedgerMisc',N'LinkWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BIED',N'LinkWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'inDetail',N'LinkWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillingWK',N'InvoiceWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillingWK',N'VoucherWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillInvMaster',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillInvMaster',N'ByGroupWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillInvMaster',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillInvMaster',N'ICBillLinkWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillLabDetail',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillLabDetail',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillLabDetail',N'PostWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillLabDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillExpDetail',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillExpDetail',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillExpDetail',N'PostWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillExpDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillTaxDetail',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillTaxDetail',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillInvSums',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ICBillInvSums',N'BillWBS1','N');

/*added 4.1 */
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGPYContribution',N'CreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGPYContribution',N'DebitWBS1','N');

/*new for 5.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'tkUnitDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGInventoryMain',N'InventoryWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'InvDetail',N'WBS1','N');
/*end 5.0*/

/*new for 5.1*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'upDetail',N'WBS1','N');
/*end 5.1*/

/*new for 6.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'RevenueGroup',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'RevenueGroup',N'RevWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'RevenueGroupSubs',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'RevenueGroupSubs',N'SubWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'DocumentsPR',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ItemRequestDetail',N'WBS1','N');
/*end 6.0*/
/*new for 6.1*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BTSchedule',N'WBS1','N');
/*end 6.1*/

Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'CFGBillTaxesData',N'NonRecoverCreditWBS1','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'CFGBillTaxesData',N'NonRecoverDebitWBS1','N');

/*new for 6.2*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'PRAdditionalData',N'WBS1','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'PNPlan',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PNAssignment',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PNConsultant',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PNExpense',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PNTask',N'WBS1','N');
/*end 6.2*/

/*new for 7.0 - 07/15/09 */
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'Contracts',N'WBS1','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'ContractCredit',N'WBS1','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'ContractDetails',N'WBS1','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'ContractsTemplate',N'WBS1','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'ContractCreditTemplate',N'WBS1','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'ContractDetailsTemplate',N'WBS1','N');
/*end mod 7.0*/

/*new for 7.0*/
/* 11/14 Added WBS1, WBS2, WBS3, CreditMemoRefNo, OriginalInvoice in ARCreditMemo table*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS1',N'ARCreditMemo',N'WBS1','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BTFCategory',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'EmployeeRealizationWk',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'EmployeeRealizationDetailWk',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'EmployeeRealizationDetailWk',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billFeeAllocation',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billFeeAllocation',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billExpWriteOffDelete',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billExpWriteOffDelete',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billExpWriteOffDelete',N'PostWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billLabWriteOffDelete',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billLabWriteOffDelete',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billLabWriteOffDelete',N'PostWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billUnitWriteOffDelete',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billUnitWriteOffDelete',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billUnitWriteOffDelete',N'PostWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billTaxDetail',N'PostWBS1','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'PayrollContributionCreditWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'PayrollContributionDebitWBS1','N');
/*end for 7.0*/

/*7.0 SP1 changes*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGAutoPosting',N'UnbilledRealizedGainsWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGAutoPosting',N'UnbilledRealizedLossesWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGAutoPosting',N'UnbilledUnrealizedGainsWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGAutoPosting',N'UnbilledUnrealizedLossesWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'UnbilledRealizedGainsWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'UnbilledRealizedLossesWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'UnbilledUnrealizedGainsWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Organization',N'UnbilledUnrealizedLossesWBS1','N');
/*end 7.0 SP1*/

/*new for 7.2*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'tkRevisionDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BTTaxCodes',N'WBS1','N');
/*end for 7.2*/

/*new for 7.3*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGCreditCardSecondary',N'WBS1','N');
/*end for 7.3*/


/*new for 7.4*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ARPreInvoice',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ARPreInvoiceDetail',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billPreInvoice',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billPriorInvoices',N'BillWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'billPriorInvoices',N'MainWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BTFPhaseGroupData',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'BTFPhaseGroupDescriptions',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'Equipment',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRFECostGroups',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRFEFuncGroups',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRFEPhases',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRFESpecialServices',N'WBS1','N');
/*end for 7.4*/

/*new for 7.5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGBankEntriesCodeData',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGAssetMain',N'FAWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'CFGAssetMain',N'DOWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'TaxReporting',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'APApprovalWBS',N'WBS1','N');
/*end for 7.5*/

/*new for 7.6*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'FW_Attachments',N'Key1','N');
/*end for 7.6*/

/*new for 1.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ClendorProjectAssoc',N'WBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'ClendorProjectAssocTemplate',N'WBS1','N');
/*end for 1.0*/

/*new for 2.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'RPPlan',N'PlanNumber','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PNPlan',N'PlanNumber','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PR',N'SiblingWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRTemplate',N'SiblingWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRDefaults',N'SiblingWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PR',N'PreAwardWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRTemplate',N'PreAwardWBS1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS1',N'PRDefaults',N'PreAwardWBS1','N');

Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS1',N'PRCompetitionAssoc',N'WBS1','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS1',N'PRProposals',N'WBS1','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS1',N'PRMilestone',N'WBS1','N');

Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS1',N'ProjectFromContacts',N'WBS1','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS1',N'PRRevenue',N'WBS1','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS1',N'CustomProposal',N'WBS1','N');

Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS1',N'PRConsultant',N'WBS1','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS1',N'PRLabor',N'WBS1','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS1',N'PRExpense',N'WBS1','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS1',N'PRUnit',N'WBS1','N');

Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS1',N'InvoiceApprover',N'WBS1','N');
/*end for 2.0*/

/*new for 3.5*/
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS1',N'inDetailCustomFields',N'WBS1','N');
/*end for 3.5*/

Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS1',N'InvoiceApproval',N'WBS1','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS1',N'InvoiceApprovalActions',N'WBS1','N');

/*new for 4.0*/
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS1',N'InvoiceVoid',N'WBS1','N');
/*end for 4.0*/

end --WBS1 insert
if (@Entity = 'WBS2')
begin
set @InfoCenter = 'Projects'
set @custDataType = 'WBS2'
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Activity',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ADPCode',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'apDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'apppChecks',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'apppChecksW',N'FeeDebitWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGBanks',N'WireTransferFeeDebitWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'AR',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BIED',N'BilledWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BIED',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BIED',N'XferWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BILD',N'BilledWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BILD',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BILD',N'XferWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billAddDetail',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billARDetail',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billBTDDetail',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billConDetail',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billConDetail',N'PostWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billConDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billExpDetail',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billExpDetail',N'PostWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billExpDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billFeeDetail',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billFeeDetail',N'PostWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billFeeDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billINDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billINMaster',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billIntDetail',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billInvMaster',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billInvSums',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billLabDetail',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billLabDetail',N'PostWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billLabDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billLimDetail',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billRetDetail',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billTaxDetail',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billUnitDetail',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billUnitDetail',N'PostWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billUnitDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BT',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BT',N'WBS2ToPost','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BTA',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BTF',N'PostWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BTF',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'cdDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGAPDiscount',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGAutoPosting',N'ConsAccrWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGAutoPosting',N'EmplAdvCreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGAutoPosting',N'EmplExpCreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGAutoPosting',N'LaborCreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGAutoPosting',N'MiscCreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGAutoPosting',N'PrintsCreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGEKCategory',N'CompanyPaidWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGEKMain',N'CompanyPaidWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGOHMain',N'OHVarianceWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGTKCategory',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'crDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CustomProposalProject',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CustomProposalProjectGraphics',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CustomProposalProjTeam',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'cvDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'EB',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ekDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'EMProjectAssoc',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'exDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'inDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'inMaster',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'jeDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'laDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LB',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LD',N'BilledWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LD',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LD',N'XferWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LedgerAP',N'BilledWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LedgerAP',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LedgerAP',N'XferWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LedgerAR',N'BilledWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LedgerAR',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LedgerAR',N'XferWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LedgerEX',N'BilledWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LedgerEX',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LedgerEX',N'XferWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LedgerMisc',N'BilledWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LedgerMisc',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LedgerMisc',N'XferWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'miDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ConsAccrWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'EmplAdvWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'EmplExpWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'FICAWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'LabDistWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'MiscWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'OHVarianceWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'PayrollWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'PrintsWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'UnitsBSWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'UnitsIndWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'UnitsOtherWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'XChargeOHCreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'XChargeOHDebitWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'XChargeOHFromWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'XChargeOHToWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'XChargeRegCreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'XChargeRegDebitWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'XChargeRegFromWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'XChargeRegToWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'OverheadAllocation',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PR',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PR',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PRAwards',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PRDescriptions',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'prDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PRF',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PRFileLinks',N'WBS2','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Projects_Custom',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PRProjectCodes',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PRSubscr',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ReportWBSList',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'RPAssignment',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'RPConsultant',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'RPExpense',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'RPTask',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'tkDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'tsDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'UNUnit',N'CreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'unDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'XChargeWk',N'CreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'XChargeWk',N'DebitWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'XChargeWk',N'FromWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'XChargeWk',N'ToWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'XChargeWk',N'WBS2','N');

/*new for 2.0*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'CFGPYMain',N'BonusCostWBS2','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'CFGPYMain',N'FICAWBS2','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'CFGPYMain',N'OtherPayCostWBS2','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'CFGPYWHCodes',N'PayrollWBS2','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'EMProjectAssocTemplate',N'WBS2','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'PRAwardsTemplate',N'WBS2','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'PRDescriptionsTemplate',N'WBS2','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'ProjectCustomTabFieldsTemplate',N'WBS2','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'PRProjectCodesTemplate',N'WBS2','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'PRTemplate',N'BillWBS2','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'PRTemplate',N'WBS2','N');
--Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'VEProjectAssoc',N'WBS2','N');
--Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'VEProjectAssocTemplate',N'WBS2','N');
/*end 2.0*/
/*2.0 beta2*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'MktCampaign',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'MktCampaignProjectAssoc',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'MktCampaignProjectAssocTemplate',N'WBS2','N');
/*new for 3.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BT',N'IntercompanyTaxWBS2ToPost','N');
--don't convert the following no corresponding wbs1
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values ('WBS2','BTDefaults','IntercompanyTaxWBS2ToPost','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values ('WBS2','BTDefaults','WBS2ToPost','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values ('WBS3','PRDefaults','RevUpsetWBS2','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values ('WBS3','PRTemplate','RevUpsetWBS2','N');

--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values ('WBS2','CFGAutoPosting','GenOHWBS2','N');

/*6.0 changes*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGAutoPosting',N'RealizedGainsWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGAutoPosting',N'RealizedLossesWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGAutoPosting',N'UnrealizedGainsWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGAutoPosting',N'UnrealizedLossesWBS2','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGICBilling',N'ICBillingReclassWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGPYMain',N'OtherPay2CostWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGPYMain',N'OtherPay3CostWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGPYMain',N'OtherPay4CostWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGPYMain',N'OtherPay5CostWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillingWK',N'CreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillingWK',N'DebitWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillingWK',N'FromWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillingWK',N'ReclassCreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillingWK',N'ReclassDebitWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillingWK',N'ToWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillingWK',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'KeyConvertWorkPrOrg',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingBSOtherCreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingBSOtherDebitWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingExpOHCreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingExpOHDebitWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingExpOHFromWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingExpOHToWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingExpPromoCreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingExpPromoDebitWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingExpPromoFromWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingExpPromoToWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingExpRegCreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingExpRegDebitWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingExpRegFromWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingExpRegToWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingLabOHCreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingLabOHDebitWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingLabOHFromWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingLabOHToWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingLabPromoCreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingLabPromoDebitWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingLabPromoFromWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingLabPromoToWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingLabRegCreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingLabRegDebitWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingLabRegFromWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'ICBillingLabRegToWBS2','N');

/*6.0 changes*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'RealizedGainsWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'RealizedLossesWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'UnrealizedGainsWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'UnrealizedLossesWBS2','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'POCommitment',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'POCostDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'POPQCostDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'POPQWBS',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'POPRCostDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'POPRWBS',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'POWBS',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PR',N'RevUpsetWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PRChargeCompanies',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PRChargeCompaniesTemplate',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PRDefaults',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PRSummarySub',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PRSummaryMain',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PRSummaryWBSList',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'RPUnit',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'SF330PersonnelProjects',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'SF330Projects',N'WBS2','N');
/*end 3.0*/

/*added 3.0 sp5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGICBillingTerms',N'ICBillingInvoiceWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGICBillingTerms',N'ICBillingVoucherWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LedgerAP',N'LinkWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LedgerAR',N'LinkWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LedgerEX',N'LinkWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'LedgerMisc',N'LinkWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BIED',N'LinkWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'inDetail',N'LinkWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillingWK',N'InvoiceWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillingWK',N'VoucherWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillInvMaster',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillInvMaster',N'ICBillLinkWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillLabDetail',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillLabDetail',N'PostWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillLabDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillExpDetail',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillExpDetail',N'PostWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillExpDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillTaxDetail',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ICBillInvSums',N'BillWBS2','N');

/*added 4.1 */
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGPYContribution',N'CreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGPYContribution',N'DebitWBS2','N');

/*new for 5.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'tkUnitDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGInventoryMain',N'InventoryWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'InvDetail',N'WBS2','N');
/*end 5.0*/

/*new for 5.1*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'upDetail',N'WBS2','N');
/*end 5.1*/

/*new for 6.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'RevenueGroup',N'RevWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'DocumentsPR',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ItemRequestDetail',N'WBS2','N');
/*end 6.0*/
/*new for 6.1*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BTSchedule',N'WBS2','N');
/*end 6.1*/

Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'CFGBillTaxesData',N'NonRecoverCreditWBS2','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'CFGBillTaxesData',N'NonRecoverDebitWBS2','N');

/*new for 6.2*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'PRAdditionalData',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PNAssignment',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PNConsultant',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PNExpense',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'PNTask',N'WBS2','N');
/*end 6.2*/

/*new for 7.0 - 07/15/09 */
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'ContractDetails',N'WBS2','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'ContractDetailsTemplate',N'WBS2','N');
/*end mod 7.0*/

/*new for 7.0*/
/* 11/14 Added WBS1, WBS2, WBS3, CreditMemoRefNo, OriginalInvoice in ARCreditMemo table*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS2',N'ARCreditMemo',N'WBS2','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BTFCategory',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'EmployeeRealizationWk',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'EmployeeRealizationDetailWk',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billFeeAllocation',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billExpWriteOffDelete',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billExpWriteOffDelete',N'PostWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billLabWriteOffDelete',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billLabWriteOffDelete',N'PostWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billUnitWriteOffDelete',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billUnitWriteOffDelete',N'PostWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billTaxDetail',N'PostWBS2','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'PayrollContributionCreditWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'PayrollContributionDebitWBS2','N');
/*end for 7.0*/

/*7.0 SP1 changes*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGAutoPosting',N'UnbilledRealizedGainsWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGAutoPosting',N'UnbilledRealizedLossesWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGAutoPosting',N'UnbilledUnrealizedGainsWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGAutoPosting',N'UnbilledUnrealizedLossesWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'UnbilledRealizedGainsWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'UnbilledRealizedLossesWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'UnbilledUnrealizedGainsWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Organization',N'UnbilledUnrealizedLossesWBS2','N');
/*end 7.0 SP1*/

/*new for 7.2*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'tkRevisionDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BTTaxCodes',N'WBS2','N');
/*end for 7.2*/

/*new for 7.3*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGCreditCardSecondary',N'WBS2','N');
/*end for 7.3*/

/*new for 7.4*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ARPreInvoice',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ARPreInvoiceDetail',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ARPreInvoiceDetail',N'WBS2ToPost','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'billPriorInvoices',N'BillWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BTFPhaseGroupData',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BTFPhaseGroupDescriptions',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'Equipment',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'BT',N'PreInvWBS2ToPost','N');
/*end for 7.4*/

/*new for 7.5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGBankEntriesCodeData',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGAssetMain',N'FAWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'CFGAssetMain',N'DOWBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'TaxReporting',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'APApprovalWBS',N'WBS2','N');
/*end for 7.5*/

/*new for 7.6*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'FW_Attachments',N'Key2','N');
/*end for 7.6*/

/*new for 1.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ClendorProjectAssoc',N'WBS2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS2',N'ClendorProjectAssocTemplate',N'WBS2','N');
/*end for 1.0*/

/*new for 2.0*/
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS2',N'PRCompetitionAssoc',N'WBS2','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS2',N'PRCompetitionAssocTemplate',N'WBS2','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS2',N'PRProposals',N'WBS2','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS2',N'PRProposalsTemplate',N'WBS2','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS2',N'PRMilestone',N'WBS2','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS2',N'PRMilestoneTemplate',N'WBS2','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS2',N'PRRevenue',N'WBS2','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS2',N'PRRevenueTemplate',N'WBS2','N');
/*end for 2.0*/

/*new for 3.5*/
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS2',N'inDetailCustomFields',N'WBS2','N');
/*end for 3.5*/

end --WBS2 insert

if (@Entity = 'WBS3')
begin
set @InfoCenter = 'Projects'
set @custDataType = 'WBS3'
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Activity',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ADPCode',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'apDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'apppChecks',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'apppChecksW',N'FeeDebitWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGBanks',N'WireTransferFeeDebitWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'AR',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BIED',N'BilledWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BIED',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BIED',N'XferWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BILD',N'BilledWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BILD',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BILD',N'XferWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billAddDetail',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billARDetail',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billBTDDetail',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billConDetail',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billConDetail',N'PostWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billConDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billExpDetail',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billExpDetail',N'PostWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billExpDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billFeeDetail',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billFeeDetail',N'PostWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billFeeDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billINDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billINMaster',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billIntDetail',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billInvMaster',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billInvSums',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billLabDetail',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billLabDetail',N'PostWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billLabDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billLimDetail',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billRetDetail',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billTaxDetail',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billUnitDetail',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billUnitDetail',N'PostWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billUnitDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BT',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BT',N'WBS3ToPost','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BTA',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BTF',N'PostWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BTF',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'cdDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGAPDiscount',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGAutoPosting',N'ConsAccrWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGAutoPosting',N'EmplAdvCreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGAutoPosting',N'EmplExpCreditWBS3','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGAutoPosting',N'LaborCreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGAutoPosting',N'MiscCreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGAutoPosting',N'PrintsCreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGEKCategory',N'CompanyPaidWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGEKMain',N'CompanyPaidWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGOHMain',N'OHVarianceWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGTKCategory',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'crDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CustomProposalProject',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CustomProposalProjectGraphics',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CustomProposalProjTeam',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'cvDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'EB',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ekDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'EMProjectAssoc',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'exDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'inDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'inMaster',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'jeDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'laDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LB',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LD',N'BilledWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LD',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LD',N'XferWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LedgerAP',N'BilledWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LedgerAP',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LedgerAP',N'XferWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LedgerAR',N'BilledWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LedgerAR',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LedgerAR',N'XferWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LedgerEX',N'BilledWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LedgerEX',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LedgerEX',N'XferWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LedgerMisc',N'BilledWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LedgerMisc',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LedgerMisc',N'XferWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'miDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ConsAccrWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'EmplAdvWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'EmplExpWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'FICAWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'LabDistWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'MiscWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'OHVarianceWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'PayrollWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'PrintsWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'UnitsBSWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'UnitsIndWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'UnitsOtherWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'XChargeOHCreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'XChargeOHDebitWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'XChargeOHFromWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'XChargeOHToWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'XChargeRegCreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'XChargeRegDebitWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'XChargeRegFromWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'XChargeRegToWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'OverheadAllocation',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PR',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PR',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PRAwards',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PRDescriptions',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'prDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PRF',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PRFileLinks',N'WBS3','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Projects_Custom',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PRProjectCodes',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PRSubscr',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ReportWBSList',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'RPAssignment',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'RPConsultant',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'RPExpense',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'RPTask',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'tkDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'tsDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'UNUnit',N'CreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'unDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'XChargeWk',N'CreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'XChargeWk',N'DebitWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'XChargeWk',N'FromWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'XChargeWk',N'ToWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'XChargeWk',N'WBS3','N');
/*New for 2.0*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'CFGPYMain',N'BonusCostWBS3','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'CFGPYMain',N'FICAWBS3','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'CFGPYMain',N'OtherPayCostWBS3','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'CFGPYWHCodes',N'PayrollWBS3','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'EMProjectAssocTemplate',N'WBS3','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'PRAwardsTemplate',N'WBS3','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'PRDescriptionsTemplate',N'WBS3','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'ProjectCustomTabFieldsTemplate',N'WBS3','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'PRProjectCodesTemplate',N'WBS3','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'PRTemplate',N'BillWBS3','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'PRTemplate',N'WBS3','N');
--Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'VEProjectAssoc',N'WBS3','N');
--Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'VEProjectAssocTemplate',N'WBS3','N');
/*end 2.0*/
/*2.0 beta2*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'MktCampaign',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'MktCampaignProjectAssoc',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'MktCampaignProjectAssocTemplate',N'WBS3','N');
/*3.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BT',N'IntercompanyTaxWBS3ToPost','N');
--don't convert the following no corresponding wbs1
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values ('WBS3','BTDefaults','IntercompanyTaxWBS3ToPost','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values ('WBS3','BTDefaults','WBS3ToPost','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values ('WBS3','PRDefaults','RevUpsetWBS3','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values ('WBS3','PRTemplate','RevUpsetWBS3','N');

--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values ('WBS3','CFGAutoPosting','GenOHWBS3','N');

/*6.0 changes*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGAutoPosting',N'RealizedGainsWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGAutoPosting',N'RealizedLossesWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGAutoPosting',N'UnrealizedGainsWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGAutoPosting',N'UnrealizedLossesWBS3','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGICBilling',N'ICBillingReclassWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGPYMain',N'OtherPay2CostWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGPYMain',N'OtherPay3CostWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGPYMain',N'OtherPay4CostWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGPYMain',N'OtherPay5CostWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillingWK',N'CreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillingWK',N'DebitWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillingWK',N'FromWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillingWK',N'ReclassCreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillingWK',N'ReclassDebitWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillingWK',N'ToWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillingWK',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'KeyConvertWorkPrOrg',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingBSOtherCreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingBSOtherDebitWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingExpOHCreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingExpOHDebitWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingExpOHFromWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingExpOHToWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingExpPromoCreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingExpPromoDebitWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingExpPromoFromWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingExpPromoToWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingExpRegCreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingExpRegDebitWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingExpRegFromWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingExpRegToWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingLabOHCreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingLabOHDebitWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingLabOHFromWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingLabOHToWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingLabPromoCreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingLabPromoDebitWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingLabPromoFromWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingLabPromoToWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingLabRegCreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingLabRegDebitWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingLabRegFromWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'ICBillingLabRegToWBS3','N');

/*6.0 changes*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'RealizedGainsWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'RealizedLossesWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'UnrealizedGainsWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'UnrealizedLossesWBS3','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'POCommitment',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'POCostDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'POPQCostDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'POPQWBS',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'POPRCostDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'POPRWBS',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'POWBS',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PR',N'RevUpsetWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PRChargeCompanies',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PRChargeCompaniesTemplate',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PRDefaults',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PRSummarySub',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PRSummaryMain',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PRSummaryWBSList',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'RPUnit',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'SF330PersonnelProjects',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'SF330Projects',N'WBS3','N');
/*end 3.0*/

/*added 3.0 sp5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGICBillingTerms',N'ICBillingInvoiceWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGICBillingTerms',N'ICBillingVoucherWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LedgerAP',N'LinkWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LedgerAR',N'LinkWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LedgerEX',N'LinkWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'LedgerMisc',N'LinkWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BIED',N'LinkWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'inDetail',N'LinkWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillingWK',N'InvoiceWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillingWK',N'VoucherWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillInvMaster',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillInvMaster',N'ICBillLinkWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillLabDetail',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillLabDetail',N'PostWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillLabDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillExpDetail',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillExpDetail',N'PostWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillExpDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillTaxDetail',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ICBillInvSums',N'BillWBS3','N');

/*added 4.1 */
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGPYContribution',N'CreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGPYContribution',N'DebitWBS3','N');

/*new for 5.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'tkUnitDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGInventoryMain',N'InventoryWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'InvDetail',N'WBS3','N');
/*end 5.0*/

/*new for 5.1*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'upDetail',N'WBS3','N');
/*end 5.1*/

/*new for 6.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'RevenueGroup',N'RevWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'DocumentsPR',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ItemRequestDetail',N'WBS3','N');
/*end 6.0*/
/*new for 6.1*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BTSchedule',N'WBS3','N');
/*end 6.1*/

Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'CFGBillTaxesData',N'NonRecoverCreditWBS3','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'CFGBillTaxesData',N'NonRecoverDebitWBS3','N');

/*new for 6.2*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'PRAdditionalData',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PNAssignment',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PNConsultant',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PNExpense',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'PNTask',N'WBS3','N');
/*end 6.2*/

/*new for 7.0 - 07/15/09 */
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'ContractDetails',N'WBS3','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'ContractDetailsTemplate',N'WBS3','N');
/*end mod 7.0*/

/*new for 7.0*/
/* 11/14 Added WBS1, WBS2, WBS3, CreditMemoRefNo, OriginalInvoice in ARCreditMemo table*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'WBS3',N'ARCreditMemo',N'WBS3','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BTFCategory',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'EmployeeRealizationWk',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'EmployeeRealizationDetailWk',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billFeeAllocation',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billExpWriteOffDelete',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billExpWriteOffDelete',N'PostWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billLabWriteOffDelete',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billLabWriteOffDelete',N'PostWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billUnitWriteOffDelete',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billUnitWriteOffDelete',N'PostWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billTaxDetail',N'PostWBS3','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'PayrollContributionCreditWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'PayrollContributionDebitWBS3','N');
/*end for 7.0*/

/*7.0 SP1 changes*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGAutoPosting',N'UnbilledRealizedGainsWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGAutoPosting',N'UnbilledRealizedLossesWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGAutoPosting',N'UnbilledUnrealizedGainsWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGAutoPosting',N'UnbilledUnrealizedLossesWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'UnbilledRealizedGainsWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'UnbilledRealizedLossesWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'UnbilledUnrealizedGainsWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Organization',N'UnbilledUnrealizedLossesWBS3','N');
/*end 7.0 SP1*/

/*new for 7.2*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'tkRevisionDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BTTaxCodes',N'WBS3','N');
/*end for 7.2*/

/*new for 7.3*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGCreditCardSecondary',N'WBS3','N');
/*end for 7.3*/

/*new for 7.4*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ARPreInvoice',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ARPreInvoiceDetail',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ARPreInvoiceDetail',N'WBS3ToPost','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'billPriorInvoices',N'BillWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BTFPhaseGroupData',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BTFPhaseGroupDescriptions',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'Equipment',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'BT',N'PreInvWBS3ToPost','N');
/*end for 7.4*/

/*new for 7.5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGBankEntriesCodeData',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGAssetMain',N'FAWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'CFGAssetMain',N'DOWBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'TaxReporting',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'APApprovalWBS',N'WBS3','N');
/*end for 7.5*/

/*new for 7.6*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'FW_Attachments',N'Key3','N');
/*end for 7.6*/

/*new for 1.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ClendorProjectAssoc',N'WBS3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WBS3',N'ClendorProjectAssocTemplate',N'WBS3','N');
/*end for 1.0*/

/*new for 2.0*/
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS3',N'PRCompetitionAssoc',N'WBS3','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS3',N'PRCompetitionAssocTemplate',N'WBS3','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS3',N'PRProposals',N'WBS3','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS3',N'PRProposalsTemplate',N'WBS3','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS3',N'PRMilestone',N'WBS3','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS3',N'PRMilestoneTemplate',N'WBS3','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS3',N'PRRevenue',N'WBS3','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS3',N'PRRevenueTemplate',N'WBS3','N');
/*end for 2.0*/

/*new for 3.5*/
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'WBS3',N'inDetailCustomFields',N'WBS3','N');
/*end for 3.5*/

end --WBS3

if (@Entity = 'Bank')
begin
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'apControl',N'DefaultBank','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'apMaster',N'BankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'apppChecks',N'BankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'BIED',N'BankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'cdControl',N'DefaultBank','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'cdMaster',N'BankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'CFGAutoPosting',N'EmplDefaultBank','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'CFGBanks',N'Code','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'CR',N'BankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'crControl',N'DefaultBank','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'crMaster',N'BankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'cvControl',N'DefaultBank','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'cvMaster',N'BankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'EMDirectDepositDetail',N'SourceBankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'erDetail',N'BankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'EX',N'Bank','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'EXAdvance',N'Bank','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'exChecks',N'BankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'exChecksE',N'SourceBankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'LedgerAP',N'BankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'LedgerAR',N'BankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'LedgerEX',N'BankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'LedgerMisc',N'BankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'Organization',N'EmplDefaultBank','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'VO',N'BankCode','N');
/*new for 2.0*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Bank',N'CFGPYMain',N'PayrollBankCode','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Bank',N'EMPayroll',N'BankCode','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Bank',N'EMPayrollDetail',N'BankCode','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Bank',N'Organization',N'PayrollDefaultBank','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Bank',N'PYChecks',N'BankCode','N');
/*end 2.0*/
/* 3.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'VEEFTDetail',N'SourceBankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'POVoucherMaster',N'BankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'apppChecksE',N'SourceBankCode','N');

/*added 3.0 sp5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'CFGICBillingTerms',N'ICBillingVoucherBankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'ICBillingWK',N'VoucherBankCode','N');

/*new for 5.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'CFGBankTextFormat',N'Code','N');
/*end 5.0*/

/*new for 6.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'exChecksB',N'BankCode','N');
/*end 6.0*/

/*new for 7.1*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'CFGTransAutoNumBank',N'BankCode','N');
/*end 7.1*/

/*added 7.5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'CFGPYMain',N'FICAAPBank','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'CFGBankImport',N'Code','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'CFGBankImportDetailData',N'Code','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'CFGBankImportDetailDescriptions',N'Code','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'CFGBankPaymentMatchingRule',N'Code','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'CFGBankReceiptMatchingRule',N'Code','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'CFGBankTypeMapping',N'Code','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'BankRecImport',N'BankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'BankRecTransaction',N'BankCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'CFGBankEntriesCodeData',N'Code','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'CFGBankEntriesCodeDescriptions',N'Code','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Bank',N'APApprovalMaster',N'opBank','N');

end -- bank
if (@Entity = 'Discount')
begin
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Discount',N'apppChecks',N'DiscCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Discount',N'CFGAPDiscount',N'Code','N');
/*moved from ve to veAccounting*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Discount',N'VEAccounting',N'DiscCode','N');
end --Discount
if (@Entity = 'Liability')
begin
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Liability',N'apControl',N'DefaultLiab','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Liability',N'apMaster',N'LiabCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Liability',N'apppChecks',N'LiabCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Liability',N'CFGAPLiability',N'Code','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Liability',N'cvControl',N'DefaultLiab','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Liability',N'cvMaster',N'LiabCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Liability',N'VO',N'LiabCode','N');
/*new for 3.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Liability',N'CFGVendorTypeAPLiability',N'LiabCode','N');
--removed
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Liability',N'CFGVendorType',N'LiabCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Liability',N'POVoucherMaster',N'LiabCode','N');

/*added 3.0 sp5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Liability',N'CFGICBillingTerms',N'ICBillingVoucherLiabCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Liability',N'ICBillingWK',N'VoucherLiabCode','N');

/*added 7.5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Liability',N'CFGPYMain',N'FICAAPLiability','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Liability',N'APApprovalMaster',N'LiabCode','N');

end --Liability
if (@Entity = 'Tax')
begin
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'BIED',N'BillTaxCodeOverride','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'BIED',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'BILD',N'BillTaxCodeOverride','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'billINDetail',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'CFGBillTaxesData',N'Code','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'CFGBillTaxesDescriptions',N'Code','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'crDetail',N'TaxCode','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'inControl',N'DefaultTaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'inDetail',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LD',N'BillTaxCodeOverride','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerAP',N'BillTaxCodeOverride','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerAP',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerAR',N'BillTaxCodeOverride','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerAR',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerEX',N'BillTaxCodeOverride','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerEX',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerMisc',N'BillTaxCodeOverride','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerMisc',N'TaxCode','N');
/*new for 3.0*/
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'CFGMain',N'DefaultTaxCode','N');
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'VEAddress',N'DefaultTaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'VEAccounting',N'DefaultTaxCode','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Tax',N'POVoucherDetail',N'TaxCode','N');
/*end 3.0*/


/*added 3.0 sp5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'CFGICBillingTerms',N'ICBillingInvoiceTaxCode1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'CFGICBillingTerms',N'ICBillingInvoiceTaxCode2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'CFGICBillingTerms',N'ICBillingInvoiceTaxCode3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'CFGICBillingTerms',N'ICBillingVoucherTaxCode1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'CFGICBillingTerms',N'ICBillingVoucherTaxCode2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'CFGICBillingTerms',N'ICBillingVoucherTaxCode3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'ICBillingWK',N'InvoiceTaxCode1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'ICBillingWK',N'InvoiceTaxCode2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'ICBillingWK',N'InvoiceTaxCode3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'ICBillingWK',N'VoucherTaxCode1','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'ICBillingWK',N'VoucherTaxCode2','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'ICBillingWK',N'VoucherTaxCode3','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'ICBillTaxDetail',N'Code','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'ICBillInvSums',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'ICBillInvSums',N'VoucherTaxCode','N');

/*JYC-added for 6.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'CFGBillTaxesData',N'CompoundOnTaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'CFGPOShipTo',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'CFGPOShipTo',N'Tax2Code','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'POVoucherDetail',N'Tax2Code','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'BIED',N'BillTax2CodeOverride','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'BILD',N'BillTax2CodeOverride','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LD',N'BillTax2CodeOverride','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerAP',N'BillTax2CodeOverride','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerAR',N'BillTax2CodeOverride','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerEX',N'BillTax2CodeOverride','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerMisc',N'BillTax2CodeOverride','N');

/*added for 7.2*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'CFGEKCategoryTaxCodes',N'TaxCode','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'BTDefaultsTaxCodes',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'BTTaxCodes',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'CFGMainDataDefaultTaxCodes',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'apControlDefaultTaxCodes',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'cdControlDefaultTaxCodes',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'cvControlDefaultTaxCodes',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'exControlDefaultTaxCodes',N'TaxCode','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'apDetailTax',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'apDetailTax',N'CompoundOnTaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'cdDetailTax',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'cdDetailTax',N'CompoundOnTaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'cvDetailTax',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'cvDetailTax',N'CompoundOnTaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'exDetailTax',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'exDetailTax',N'CompoundOnTaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'jeDetailTax',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'jeDetailTax',N'CompoundOnTaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'ekDetailTax',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'ekDetailTax',N'CompoundOnTaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'BIEDTax',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'BIEDTax',N'CompoundOnTaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerARTax',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerARTax',N'CompoundOnTaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerAPTax',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerAPTax',N'CompoundOnTaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerEXTax',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerEXTax',N'CompoundOnTaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerMiscTax',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'LedgerMiscTax',N'CompoundOnTaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'exChecksTax',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'apppChecksTax',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'CFGBillTaxesVATGridNumber',N'Code','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'CFGBillTaxesICBillingExclusionAccounts',N'Code','N');
/*end 7.2*/

/*added for 7.4*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'ARPreInvoiceDetail',N'TaxCode','N');
/*end 7.4*/
/*added for 7.5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'TaxReporting',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'APApprovalWBSTax',N'TaxCode','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'APApprovalWBSTax',N'CompoundOnTaxCode','N');
/*end 7.5*/

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Tax',N'billTaxDetail',N'Code','N');

end --Tax
if (@Entity = 'Unit')
begin
set @InfoCenter = 'Units'
set @custDataType = 'unit'
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Unit',N'BIED',N'Unit','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Unit',N'billUnitDetail',N'Unit','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Unit',N'LedgerAP',N'Unit','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Unit',N'LedgerAR',N'Unit','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Unit',N'LedgerEX',N'Unit','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Unit',N'LedgerMisc',N'Unit','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Unit',N'UNUnit',N'Unit','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Unit',N'unDetail',N'Unit','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Unit',N'unMaster',N'Unit','N');
/*new for 3.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Unit',N'RPUnit',N'Unit','N');
/*end 3.0*/
/*new for 5.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Unit',N'tkUnitDetail',N'Unit','N');
/*end 5.0*/

/*new for 5.1*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Unit',N'upDetail',N'Unit','N');
/*end 5.1*/

/*new for 7.5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Unit',N'EQUnit',N'Unit','N');
/*end 7.5*/

/*new for 2.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Unit',N'PRUnit',N'Unit','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Unit',N'PRUnitTemplate',N'Unit','N');
/*end 2.0*/

/*new for 3.5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Unit',N'unDetailCustomFields',N'Unit','N');
/*end for 3.5*/

end --unit
if (@Entity = 'Org')
begin
set @custDataType = 'Org'
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'apppChecks',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'BIED',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'BILD',N'EmOrg','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'BILD',N'PrOrg','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'CAB',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'CFGAPLiability',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'CFGBanks',N'Org','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'CFGOrgSetup',N'PayrollDefOrg','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'CustomProposal',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'CustomProposalEmployee',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'CustomProposalProject',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'EMCompany',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'exChecks',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'FirmOrgAssociations',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'LD',N'EmOrg','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'LD',N'PrOrg','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'LedgerAP',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'LedgerAR',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'LedgerEX',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'LedgerMisc',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'Organization',N'DistributeTarget','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'Organization',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'OverheadAllocation',N'DistributionTarget','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'OverheadAllocation',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'OverheadAllocation',N'OrgToDistribute','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'PR',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'RPPlan',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'Clendor',N'Org','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'XChargeWk',N'EmpOrg','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'XChargeWk',N'ProjOrg','N');
/*new for 2.0*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'MktCampaign',N'Org','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'PRTemplate',N'Org','N');
/*end 2.0*/
/*2.0 beta2*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'CustomProposalMktCampaign',N'Org','N');
/*new for 3.0*/
--Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'CFGOrgSetup',N'DefaultOrg','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'FW_CFGSystem',N'DefaultOrg','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'CFGOrgSetup',N'OrgIntercompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'ICBillingWK',N'OriginatingOrg','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'ICBillingWK',N'TargetOrg','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'KeyConvertWorkPrOrg',N'newOrg','N');
/*end 3.0*/
/*new for 4.1*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'BIED',N'AutoEntryOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'LedgerAP',N'AutoEntryOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'LedgerAR',N'AutoEntryOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'LedgerEX',N'AutoEntryOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'LedgerMisc',N'AutoEntryOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'PRDefaults',N'Org','N');
/*end 4.1*/

/*new for 5.1*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'BTRRT',N'FilterOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'BTRCT',N'FilterOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'BTRLT',N'FilterOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'BTROT',N'FilterOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'BTEA',N'FilterOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'BTEC',N'FilterOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'BTEV',N'FilterOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'BillingTemplate',N'FilterOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'UNTable',N'FilterOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'GLTable',N'FilterOrg','N');
/*end 5.1*/

/*new for 6.2*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Org',N'PNPlan',N'Org','N');
/*end 6.2*/

/*new for 7.0 - 6/20/2011*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'GR',N'Org','N');
/*end mod 7.0*/

/*new for 7.4*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'ApprovalRoleAssignment',N'Org','N');
/*end for 7.4*/

/*new for 7.5*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'APApprovalMaster',N'RouteToOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'BIED',N'EmOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'LedgerAP',N'EmOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'LedgerAR',N'EmOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'LedgerEX',N'EmOrg','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'LedgerMisc',N'EmOrg','N');

/*new for 1.0*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'PNTask',N'Org','N');
/*end for 1.0*/

/*new for 2.0*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'PRProposals',N'Org','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Org',N'PRProposalsTemplate',N'Org','N');
/*end for 2.0*/

/*end for 7.5*/
end --org
if (@Entity = 'OrgSubCode')
begin
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'AlertsData',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'apControl',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'BIED',N'LinkCompany','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'BTDefaults',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CACompanyMapping',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'cdControl',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGAccrualScheduleMasterData',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGAccrualScheduleMasterDescriptions',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGAPDiscount',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGAPForm1099',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGAPLiability',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGAPMain',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGARMap',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGAutoPosting',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGBanks',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGBillMainData',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGBillMainDescriptions',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGBillTaxesData',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGConsolidationCompanies',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGDatesStatus',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGEKCategory',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGEKEmployeeGroup',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGEKMain',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGEmployeeTypeAccounts',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'FW_CFGEnabledCurrencies',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGHoliday',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGICBilling',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGInvMap',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGMainData',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGNonWorkingDay',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGOHAccounts',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGOHMain',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGOrgSetup',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPOAccounts',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPOBillTo',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPOBuyer',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPOClause',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPOFOB',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPOMain',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPOShipTo',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPOShipVia',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPostControl',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPostControlIntercompany',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPYAccrualsData',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPYAccrualsDescriptions',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPYFormQuarterly',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPYFormW2',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPYMain',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPYWHCodes',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGResourcePlanning',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGTimeAnalysis',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGTKCategory',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGTKEmployeeGroup',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGTKMain',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGVendorTypeAPLiability',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGXCharge',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CompanyItem',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CompanyPOCategory',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'ConsolidationBudgets',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'Consolidations',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'crControl',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'cvControl',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'FW_DetailJobs',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'FW_DetailProfiles',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'Eliminations',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'EliminationsDetail',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'erControl',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'exControl',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'Form1099Data',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'FormQuarterlyData',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'FormW2Data',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'ICBillingWK',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'inControl',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'jeControl',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'jeDetail',N'LinkCompany','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'laControl',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'LedgerAP',N'LinkCompany','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'LedgerAR',N'LinkCompany','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'LedgerEX',N'LinkCompany','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'LedgerMisc',N'LinkCompany','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'miControl',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'OverheadAllocation',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'POMaster',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'POPQMaster',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'POPRMaster',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'PRChargeCompanies',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'PRChargeCompaniesTemplate',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'prControl',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'RPPlan',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'SECompany',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'tkComments',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'tkControl',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'tsControl',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'UNTable',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'unControl',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'VEAccounting',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'VendorItem',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'VO',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'XChargeWk',N'Company','N');

/*added 3.0 sp5*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGICBillingTerms',N'ICBillingInvoiceCompany','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGICBillingTerms',N'ICBillingVoucherCompany','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'ICBillInvMaster',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'ICBillLabDetail',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'ICBillExpDetail',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'ICBillTaxDetail',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'ICBillInvSums',N'Company','N');
 
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'AR',N'LinkCompany','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'VO',N'LinkCompany','N');
/*added 4.0 */
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGStateID',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'EMTKGroups',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'EMEKGroups',N'Company','N');

/*added 4.1*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPYWHCodesWage',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPYContribution',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPYContributionWage',N'Company','N');

	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'cdDetail',N'LinkCompany','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGCashAcctSetup',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGCashSetupData',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGCashSetupDescriptions',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'crDetail',N'LinkCompany','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'VisionSystemAnalysisLog',N'Company','N');
/*end 4.1*/

/*added 5.0*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGAccrualScheduleDetail',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGAPExpenseCodeAccounts',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGBankTextFormat',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGInventoryMain',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGInvLocations',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGInvApprover',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'InvMaster',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'ItemRequestMaster',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'ItemUMConversion',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'Quantities',N'Company','N');
/*end 5.0*/


/*added 5.1*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'upControl',N'Company','N');
/*end 5.1*/

/*added 6.0*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGApproveTrans',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGTransApprover',N'Company','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGTransEmployee',N'Company','N');

	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'laDetail',N'LinkCompany','N');

/*end 6.0*/

/*new for 6.2*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'PNPlan',N'Company','N');
/*end 6.2*/

/*new for 7.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGRealizationAllocationAccounts',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGICBillingOverrides',N'TargetCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGICBillingOverrides',N'OriginatingCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'ADPCode',N'Company','N');
/*end 7.0*/

/*added for 7.1*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGTransAutoNumBank',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGTransAutoNumType',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'ReportPresentationCurrency',N'Company','N');
/*end 7.1*/

/*added for 7.2*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGMainDataDefaultTaxCodes',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'BTDefaultsTaxCodes',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGEKCategoryTaxCodes',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'tkBillingXferReasons',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'tkRevExplanations',N'Company','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGDiaryAutoNum',N'Company','N');
/*end 7.2*/

/*added for 7.3*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGCreditCard',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGCreditCardImport',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGCreditCardImportDetailData',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGCreditCardImportDetailDescriptions',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGCreditCardSecondary',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGCreditCardSecondaryEmployee',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CreditCardImport',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CreditCardTransaction',N'Company','N');
/*end 7.3*/

/*added for 7.4*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGServiceProfileCompanyMapping',N'Company','N');
/*end 7.4*/

/*added for 7.5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'EMMain',N'HomeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'EMCompany',N'EmployeeCompany','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'EMAccrual',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'EMAccrualDetail',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'EMPayroll',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'EMPayrollContributionWage',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'EMPayrollWithholdingWage',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'EMPayrollContribution',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'EMPayrollWithholding',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'EMDirectDeposit',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'EMLocale',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'ekMaster',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'tkBreakTime',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'tkDetail',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'tkMaster',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'tkRevisionDetail',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'tkRevisionMaster',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'tkUnitDetail',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'unDetail',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'upDetail',N'EmployeeCompany','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGAssetMain',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGFABookData',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGFABookDescriptions',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGFADepMethodData',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGFADepMethodDescriptions',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGFADepMethodSum',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGFAPropertyTypeData',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGFAPropertyTypeDescriptions',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGFARepairTypeData',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGFARepairTypeDescriptions',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGFAAssetTypeDescriptions',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGFASec179Main',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGFAAssetTypeData',N'Company','N');

Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'APApprovalMaster',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'AbsenceRequests',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CABGroup',N'Company','N');
/*end 7.5*/

/*added for 7.6*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'SETransTypes',N'Company','N');
/*end 7.6*/

/*added for Storm 2.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGRMSettings',N'DefaultCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPREstClause',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGPREstService',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGProjRevenueDefaults',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'TransactionUDFData',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'TransactionUDFLabels',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'tkCustomFields',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGQuickBooks',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGProjRevenueTemplateMaster',N'Company','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGIntegrationCompany',N'Company','N');
/*end 2.0*/

/*new for 3.5*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'ApprovalErrorLog',N'EmployeeCompany','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'OrgSubCode',N'CFGTransCustomFields',N'Company','N');
/*end for 3.5*/

end
if (@Entity = 'labcd')
begin
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'ADPCode',N'LaborCodeMask','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'BILD',N'LaborCode','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'BILD',N'XferLaborCode','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'billLabDetail',N'LaborCode','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'BTRLTCodes',N'LaborCodeMask','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'CFGBillMainData',N'NonBillLaborCode','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'CFGTKCategory',N'LaborCode','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'ICBillingWK',N'LaborCode','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'laDetail',N'LaborCode','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'LB',N'LaborCode','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'LD',N'LaborCode','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'LD',N'XferLaborCode','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'RPAssignment',N'LaborCode','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'RPTask',N'LaborCode','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'tkDetail',N'LaborCode','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'tsDetail',N'LaborCode','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'XChargeWk',N'LaborCode','N');
/*new for 2.0*/
	Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'labcd',N'CostLTCodes',N'LaborCodeMask','N');
/*end 2.0*/
/*3.0 */
/*no 3.0 changes*/
/*end 3.0*/

/*added 3.0 sp5*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'ICBillLabDetail',N'LaborCode','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'RPTopDownGR',N'LaborCode','N');

/*new for 6.2*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'PNAssignment',N'LaborCode','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'PNTask',N'LaborCode','N');
/*end 6.2*/

/* added 7.0 - 6/20/2011*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'GR',N'LaborCode','N');

	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'RPAssignment',N'GRLBCD','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'PNAssignment',N'GRLBCD','N');
/*new for 7.2*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'labcd',N'tkRevisionDetail',N'LaborCode','N');
/*end for 7.2*/
end --labcd
if (@Entity = 'Contact')
begin
	set @InfoCenter = 'Contacts'
	set @custDataType = 'contact'
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Contact',N'Activity',N'ContactID','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Contact',N'ContactFileLinks',N'ContactID','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Contact',N'ContactMoreInfo',N'ContactID','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Contact',N'Contacts',N'ContactID','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Contact',N'ContactSubscr',N'ContactID','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Contact',N'ContactToContactAssoc',N'FromContactID','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Contact',N'ContactToContactAssoc',N'ToContactID','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Contact',N'CustomProposalContact',N'ContactID','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Contact',N'EMContactAssoc',N'ContactID','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Contact',N'PR',N'BillingContactID','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Contact',N'PR',N'ContactID','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Contact',N'PRContactAssoc',N'ContactID','N');
/*new for 2.0*/
	Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Contact',N'ContactActivity',N'ContactID','N');
	Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Contact',N'ContactsFromLeads',N'ContactID','N');
	Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Contact',N'PRContactAssocTemplate',N'ContactID','N');
	Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Contact',N'PRTemplate',N'BillingContactID','N');
	Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Contact',N'PRTemplate',N'ContactID','N');
/*end 2.0*/
/*new for 2.0 beta 2*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Contact',N'MktCampaignContactAssoc',N'ContactID','N');
/*3.0 */
/*no 3.0 changes*/
/*end 3.0*/
/*new for 7.4*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Contact',N'VEAccounting',N'PurchasingContactID','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Contact',N'ContactPhoto',N'ContactID','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Contact',N'WorkflowContactActivity',N'ContactID','N');
/*7.4 */
/*new for 7.5*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Contact',N'Activity',N'ContactIDForVendor','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Contact',N'WorkflowActivity',N'ContactIDForVendor','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Contact',N'Equipment',N'LeaseContactID','N');
/*7.5 */
/*new for 7.6*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Contact',N'FW_Attachments',N'Key1','N');
/*7.6 */
/*new for 1.0*/
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Contact',N'MktCampaignClientAssoc',N'ContactID','N');
Insert into KeyConvertDriver (Entity,TableName,ColumnName, SkipValue) values (N'Contact',N'ClientFromContacts',N'ContactID','N');
/*1.0 */

/*new for 2.0*/
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Contact',N'PRDefaults',N'BillingContactID','N');
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Contact',N'PRDefaults',N'ContactID','N');
Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'Contact',N'ProjectFromContacts',N'ContactID','N');
/*2.0 */

end
if (@Entity = 'WHCODE')
begin
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'CFGPYWHCodes',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'EMPayrollWithholding',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'EMPayrollWithholdingDetail',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'PYChecksW',N'Code','N');

/*added 4.1*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'CFGPYWHCodesWage',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'EMPayrollWithholdingWage',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'EMPayrollWithholdingDetailWage',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'PYChecksWWage',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'CFGPYWHCodesWage',N'CodeWage','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'EMPayrollWithholdingWage',N'CodeWage','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'EMPayrollWithholdingDetailWage',N'CodeWage','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'PYChecksWWage',N'CodeWage','N');

	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'CFGPYContribution',N'Withholding','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'CFGPYContributionWage',N'CodeWage','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'EMPayrollContributionWage',N'CodeWage','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'EMPayrollContributionDetailWage',N'CodeWage','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'PYChecksCWage',N'CodeWage','N');

/*added 5.0*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'WHCODE',N'CFGPYWHCodes',N'LimitLinkCode','N');
end

/*added 5.0*/
if (@Entity = 'ContributionCode')
begin
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'ContributionCode',N'CFGPYContribution',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'ContributionCode',N'CFGPYContributionWage',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'ContributionCode',N'EMPayrollContribution',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'ContributionCode',N'EMPayrollContributionDetail',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'ContributionCode',N'EMPayrollContributionWage',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'ContributionCode',N'EMPayrollContributionDetailWage',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'ContributionCode',N'PYChecksC',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'ContributionCode',N'PYChecksCWage',N'Code','N');
end
/*end  5.0*/

if (@Entity = 'Accrual')
begin
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Accrual',N'CFGPYAccrualsData',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Accrual',N'CFGPYAccrualsDescriptions',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Accrual',N'EMAccrual',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Accrual',N'EMAccrualDetail',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Accrual',N'PYChecksA',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Accrual',N'CFGTimeAnalysis',N'AccrualCode','N');

	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'Accrual',N'AbsenceRequests',N'AccrualCode','N');
end
/* DW Add for 6.2 (Europe) */
if (@Entity = 'GenericResource')
begin
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'GenericResource',N'RPAssignment',N'GenericResourceID','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'GenericResource',N'RPTopdownGR',N'GenericResourceID','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'GenericResource',N'GRSkills',N'Code','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'GenericResource',N'PNAssignment',N'GenericResourceID','N');
end
/* end  Add for 6.2 (Europe) */
if (@Entity = 'RefNo')
begin
	set @InfoCenter = 'RefNo'
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'apMaster',N'Voucher','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'apppChecks',N'Voucher','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'AR',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'ARC',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'BIED',N'BilledInvoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'BIED',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'BIED',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'BIED',N'Voucher','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'BILD',N'BilledInvoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billAddDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billARDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billARDetail',N'InvoiceNumber','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billBTDDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billConDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billConDetail',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billExpDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billExpDetail',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billFeeDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billINDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billINMaster',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billIntDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billInvMaster',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billInvSums',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billLabDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billLimDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billRetDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billTaxDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billUnitDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'cdDetailTax',N'CheckNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'cdDetail',N'CheckNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'cdMaster',N'CheckNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'CR',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'crDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'crDetail',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'crMaster',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'cvDetailTax',N'CheckNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'cvDetail',N'CheckNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'cvMaster',N'CheckNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'cvMaster',N'Voucher','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'EMDirectDepositDetail',N'CheckRefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'EMPayrollDetail',N'CheckNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'erDetail',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'erMaster',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'EX',N'Voucher','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'exChecks',N'Voucher','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'inDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'inMaster',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'jeDetailTax',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'jeDetail',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'jeMaster',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LD',N'BilledInvoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerAP',N'BilledInvoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerAP',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerAP',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerAP',N'Voucher','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerAR',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerAR',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerAR',N'Voucher','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerEX',N'BilledInvoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerEX',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerEX',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerEX',N'Voucher','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerMisc',N'BilledInvoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerMisc',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerMisc',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerMisc',N'Voucher','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'miDetail',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'miMaster',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'POVoucherMaster',N'Voucher','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'prDetail',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'prMaster',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'SavedInvoices',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'VEEFTDetail',N'CheckRefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'VO',N'Voucher','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'POVoucherMaster',N'Invoice','N');		
	
/*added 3.0 sp5*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'ICBillInvMaster',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'ICBillLabDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'ICBillExpDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'ICBillExpDetail',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'ICBillTaxDetail',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'ICBillInvSums',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'ICBillingWK',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'ICBillingWK',N'Invoice','N');

/*new for 5.1*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'upDetail',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'upMaster',N'RefNo','N');
/*end 5.1*/

/*new for 7.0*/
/*add CheckNoRef in apppChecks, exchecks, and PYChecks tables*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'apppChecks',N'CheckNoRef','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'exchecks',N'CheckNoRef','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'PYChecks',N'CheckNoRef','N');

/*new for 7.0*/
/* 11/14 Added OriginalInvoice in billInvMaster table*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billInvMaster',N'OriginalInvoice','N');

/* 11/14 Added WBS1, WBS2, WBS3, CreditMemoRefNo, OriginalInvoice in ARCreditMemo table*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'ARCreditMemo',N'CreditMemoRefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'ARCreditMemo',N'OriginalInvoice','N');

/*add Credit Memo Reference in ARC, BIED, billINDetail, inDetail, LedgerAP, LedgerAR, LedgerEX and LedgerMisc tables*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'ARC',N'CreditMemoRefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'BIED',N'CreditMemoRefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billINDetail',N'CreditMemoRefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'inDetail',N'CreditMemoRefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerAP',N'CreditMemoRefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerAR',N'CreditMemoRefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerEX',N'CreditMemoRefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerMisc',N'CreditMemoRefNo','N');
/*end 7.0*/

/*new for 7.1*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'CFGTransAutoNumBank',N'NextRefNoReceipts','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'CFGTransAutoNumBank',N'NextRefNoPayments','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'CFGTransAutoNumType',N'NextRefNo','N');
/*end 7.1*/

/*new for 7.4*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'BIED',N'PreInvoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'crDetail',N'PreInvoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerAP',N'PreInvoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerAR',N'PreInvoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerEX',N'PreInvoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'LedgerMisc',N'PreInvoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'ARPreInvoice',N'AppliedInvoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'ARPreInvoice',N'PreInvoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'ARPreInvoiceDetail',N'PreInvoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billPreInvoice',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billPreInvoice',N'PreInvoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billPriorInvoices',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'billPriorInvoices',N'InvoiceNumber','N');
/*end 7.4*/

/*new for 7.5*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) 
		Values (N'RefNo',N'APApprovalMaster',N'opNextVoucherNumber','N');
/*end 7.5*/

/*new for 3.5*/
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'RefNo',N'cdDetailCustomFields',N'CheckNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'RefNo',N'crDetailCustomFields',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'RefNo',N'cvDetailCustomFields',N'CheckNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'RefNo',N'erDetailCustomFields',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'RefNo',N'inDetailCustomFields',N'Invoice','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'RefNo',N'jeDetailCustomFields',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'RefNo',N'miDetailCustomFields',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'RefNo',N'prDetailCustomFields',N'RefNo','N');
	Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values (N'RefNo',N'upDetailCustomFields',N'RefNo','N');
/*end for 3.5*/

	Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'RefNo',N'InvoiceApproval',N'Invoice','N');
	Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'RefNo',N'InvoiceApprovalActions',N'Invoice','N');

/*new for 4.0*/
	Insert into KeyConvertDriver (Entity, TableName, ColumnName, SkipValue) values (N'RefNo',N'InvoiceVoid',N'Invoice','N');
/*end for 4.0*/

end
--Handle custom tables and columns
-- first insert is for key column only
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values(N'Contact',N'ContactCustomTabFields',N'ContactID','N')
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values(N'Client',N'ClientCustomTabFields',N'ClientID','N')
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values(N'Employee',N'EmployeeCustomTabFields',N'Employee','N')
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values(N'WBS1',N'ProjectCustomTabFields',N'WBS1','N')
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values(N'WBS2',N'ProjectCustomTabFields',N'WBS2','N')
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values(N'WBS3',N'ProjectCustomTabFields',N'WBS3','N')
Insert into keyConvertDriver (Entity, TableName, ColumnName, SkipValue) Values(N'Unit',N'UnitCustomTabFields',N'Unit','N')
Insert into keyconvertDriver 
(  Entity,
  TableName,
  ColumnName,
  SkipValue)
select
		@Entity, 
		TableName,
case infoCenterArea 
			when 'ChartOfAccounts' then 'Account'
			when 'Clients' then 'ClientID'
			when 'Contacts' then 'ContactID'
			when 'Employees' then 'Employee'
			when 'Projects' then @CustDataType
			when 'TextLibrary' then 'Name'
			--when 'Units' then 'Unit'
			when 'Vendors' then 'Vendor'
			end as columnName,
'N' as SkipValue
from FW_CustomGrids where infoCenterArea = @InfoCenter 
--('ChartOfAccounts','Clients','Employees','Projects','Units','Vendors')
-- this insert is for all custom columns
Insert into keyconvertDriver 
(  Entity,
  TableName,
  ColumnName,
  SkipValue)
select case FW_CustomColumns.DataType
			when 'account' then 'Account'
			when 'client' then 'Client'
			when 'contact' then 'Contact'
			when 'employee' then 'Employee'
			when 'wbs1' then 'WBS1'
			when 'vendor' then 'Vendor'
			when 'org' then 'Org'
			end as Entity,
		case when CustomTables.TableName is null and FW_CustomColumns.InfoCenterArea='Firms' and FW_CustomColumns.GridID='X' Then 'ClientCustomTabFields' 
		else CustomTables.TableName End as TableName,
		FW_CustomColumns.Name as columnName,
		'N' as skipValue
from FW_CustomColumns inner join CustomTables on CustomTables.InfoCenterArea = FW_CustomColumns.InfoCenterArea AND CustomTables.GridID = FW_CustomColumns.GridID
where FW_CustomColumns.DataType = @custDataType
-- ('account',	'wbs1',	'vendor',	'employee')

-- this insert is for all custom columns in UDIC
Insert into keyconvertDriver 
(  Entity,
  TableName,
  ColumnName,
  SkipValue)
select case FW_CustomColumns.DataType
			when 'account' then 'Account'
			when 'client' then 'Client'
			when 'contact' then 'Contact'
			when 'employee' then 'Employee'
			when 'wbs1' then 'WBS1'
			when 'vendor' then 'Vendor'
			when 'org' then 'Org'
			end as Entity,
		Case When FW_CustomColumns.GridID = 'X' then FW_UDIC.TableName else FW_CustomGrids.TableName end ,
		FW_CustomColumns.Name as columnName,
		'N' as skipValue
from FW_CustomColumns inner join FW_UDIC on FW_UDIC.UDIC_ID = FW_CustomColumns.InfoCenterArea 
Left join FW_CustomGrids on FW_CustomColumns.InfocenterArea=FW_CustomGrids.InfocenterArea and FW_CustomColumns.GridID=FW_CustomGrids.GridID
where FW_CustomColumns.DataType = @custDataType

end
GO
