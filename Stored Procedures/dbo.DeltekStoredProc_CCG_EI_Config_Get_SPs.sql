SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Get_SPs]
AS
BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_EI_Config_Get_SPs]
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT name, name
        FROM sys.objects
        WHERE name like '%spCCG_EI_%'
			AND type in (N'P', N'PC')
			AND name not like '%_CHECK'
            AND name not in (
				select ProcedureName from CCG_AppSPList
			/*	'spCCG_EI_SetStage', 'spCCG_EI_CheckNotifications', 'spCCG_EI_SetUserLicense',
                'spCCG_EI_GetInvoiceFolder', 'spCCG_EI_AddDisplayColumn', 'spCCG_EI_AddInputColumn' */
			)
        ORDER BY 1;
END;
GO
