SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Get_Invoice_Stage] ( @sDateDifference varchar(100), @sRole varchar(32), @user nvarchar(32), @employee nvarchar(20))
             AS EXEC spCCG_EI_Get_Invoice_Stage @sDateDifference,@sRole,@user,@employee
GO
