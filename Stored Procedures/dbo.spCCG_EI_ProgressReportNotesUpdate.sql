SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_EI_ProgressReportNotesUpdate] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7))
AS BEGIN
/*
	Copyright (c) 2020 EleVia Software.  All rights reserved.
*/
	update ProjectCustomTabFields set CustProgressReportEditDate = GETDATE() where wbs1 = @wbs1 and wbs2 = @wbs2 and wbs3 = @wbs3
END
GO
