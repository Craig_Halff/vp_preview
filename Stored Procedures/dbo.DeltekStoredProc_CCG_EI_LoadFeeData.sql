SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_LoadFeeData] ( @rolesFieldsSql nvarchar(max), @rolesJoinsSql nvarchar(max), @filterBillGrp bit, @filterSQL bit, @isRoleAcct bit, @supportBuiltInFeePctCpl varchar(10), @isSingleProject bit, @invoiceGroup nvarchar(255), @User nvarchar(32), @wbs1List nvarchar(max), @includeDormant varchar(5), @langOverallPercentComplete nvarchar(255), @langCumulativeUnitOrFee nvarchar(255), @langCurrentInitOrFee nvarchar(255), @langLumpSum nvarchar(255), @langPercentOfConstruction nvarchar(255), @langUnitBased nvarchar(255), @langPercentCompleteByPhase nvarchar(255), @langPercentCompleteByPhaseFixed nvarchar(255), @VISION_LANGUAGE varchar(10)= null)
             AS EXEC spCCG_EI_LoadFeeData @rolesFieldsSql,@rolesJoinsSql,@filterBillGrp,@filterSQL,@isRoleAcct,@supportBuiltInFeePctCpl,@isSingleProject,@invoiceGroup,@User,@wbs1List,@includeDormant,@langOverallPercentComplete,@langCumulativeUnitOrFee,@langCurrentInitOrFee,@langLumpSum,@langPercentOfConstruction,@langUnitBased,@langPercentCompleteByPhase,@langPercentCompleteByPhaseFixed,@VISION_LANGUAGE
GO
