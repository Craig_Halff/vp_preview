SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_AddHistoryRecord]
	@actionTaken		varchar(20),
	@takenBy			Nvarchar(32),
	@recipient			Nvarchar(32),
	@WBS1				Nvarchar(32),
	@invoiceStage		varchar(50),
	@priorInvoiceStage	varchar(50),
	@autoloadStage		bit,
	@delegateFor		Nvarchar(32),
	@invoice			Nvarchar(20)
AS BEGIN
	/*
	[spCCG_EI_AddHistoryRecord]
		@actionTaken = 'Email Sent', @takenBy = '00001', @recipient = '00001',
		@WBS1 = '1999015.00', @invoiceStage = 'Write-off approval', @priorInvoiceStage = '', @autoloadStage = '0'
	*/
	SET NOCOUNT ON

	If @recipient = N'' SET @recipient = NULL
	If @WBS1 = N'' SET @WBS1 = NULL
	If @invoiceStage = '' SET @invoiceStage = NULL
	If @priorInvoiceStage = '' SET @priorInvoiceStage = NULL
	If @delegateFor = N'' SET @delegateFor = NULL

	-- Look for override
	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCCG_EI_AddHistoryRecordOverride65]') AND type in (N'P', N'PC'))
	BEGIN
		declare @sql65 Nvarchar(max) = N'EXEC [spCCG_EI_AddHistoryRecordOverride65]
			@actionTaken='''+@actionTaken+N''', @takenBy=N'''+@takenBy+N''', @recipient=N'''+isnull(@recipient,'')+N''', @WBS1=N'''+isnull(@WBS1,'')+N''',
			@invoiceStage='''+isnull(@invoiceStage,'')+N''', @priorInvoiceStage='''+isnull(@priorInvoiceStage,'')+N''', @autoloadStage='''+cast(@autoloadStage as Nvarchar(1))+N''',
			@delegateFor=N'''+isnull(@delegateFor,'')+N''', @invoice='''+isnull(@Invoice,'')+N''''
		exec(@sql65)
		return
	END
	ELSE IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCCG_EI_AddHistoryRecordOverride]') AND type in (N'P', N'PC'))
	BEGIN
		declare @sql Nvarchar(max) = N'EXEC [spCCG_EI_AddHistoryRecordOverride]
			@actionTaken='''+@actionTaken+N''', @takenBy=N'''+@takenBy+N''', @recipient=N'''+isnull(@recipient,'')+N''', @WBS1=N'''+isnull(@WBS1,'')+N''',
			@invoiceStage='''+isnull(@invoiceStage,'')+N''', @priorInvoiceStage='''+isnull(@priorInvoiceStage,'')+N''', @autoloadStage='''+cast(@autoloadStage as Nvarchar(1))+N''',
			@delegateFor=N'''+isnull(@delegateFor,'')+N''''
		exec(@sql)
		return
	END

	IF @autoloadStage = 0 or isnull(@WBS1, '') = ''
		INSERT INTO CCG_EI_History
			(ActionTaken, ActionDate, ActionTakenBy, ActionRecipient, WBS1, InvoiceStage, PriorInvoiceStage, DelegateFor, Invoice)
			VALUES (@actionTaken, getutcdate(), @takenBy, @recipient, @WBS1, @invoiceStage, @priorInvoiceStage, @delegateFor, nullif(@invoice,''))
	ELSE
		INSERT INTO CCG_EI_History
			(ActionTaken, ActionDate, ActionTakenBy, ActionRecipient, WBS1, InvoiceStage, PriorInvoiceStage, DelegateFor, Invoice)
			SELECT @actionTaken, getutcdate(), @takenBy, @recipient, @WBS1, e.InvoiceStage, NULL, @delegateFor, nullif(@invoice,'')
				FROM (select InvoiceStage from CCG_EI e full outer join PR on e.WBS1 = PR.WBS1 where PR.WBS1 = @WBS1 and PR.WBS2 = N' ' and PR.WBS3 = N' ') e

	Select @@ROWCOUNT
END
GO
