SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_All_ObjectExists] ( @objectName varchar(512), @columnName nvarchar(500))
             AS EXEC spCCG_All_ObjectExists @objectName,@columnName
GO
