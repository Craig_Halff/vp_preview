SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_Ctrl_LoadPayables] (
	@custFields						Nvarchar(max),
	@outerApplies					Nvarchar(max),
	@filter							Nvarchar(max),
	@employeeId						Nvarchar(32),
	@hasDisbursement				bit,
	@joinVE							bit,
	@joinVEAccounting				bit,
	@joinVECustom					bit,
	@getVisionAmounts				bit,
	@useVisionAmountField			Nvarchar(100),
	@useSqlFilter					bit = 1,
	@showAllRelevantContracts		bit = 0,
	@payableFilterIsTableFunction	bit = 0,
	@applyPayableFilterLast			bit = 1
) AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max) = N''
	DECLARE @useMasterPKey bit = 1
	DECLARE @filterColumnSql Nvarchar(max) = (CASE WHEN @useSqlFilter=1 AND @payableFilterIsTableFunction=0 THEN
		N'dbo.fnCCG_PAT_PayableFilter(N'''+@employeeId+''', PAT.Seq)' ELSE N'''1''' END);
	DECLARE @filterJoinSql Nvarchar(max) = (CASE WHEN @useSqlFilter=1 AND @payableFilterIsTableFunction=1 THEN N'
			INNER JOIN dbo.fnCCG_PAT_PayableFilter(N'''+@employeeId+''') sqlFilter on PAT.Seq = sqlFilter.payableSeq ' ELSE N'' END)

	DECLARE @allSelectFields Nvarchar(max) = N'PAT.ParentSeq, PAT0.PayableNumber as ParentPayableNumber, PAT.Seq, PAT.Seq as PayableSeq, PAT.PayableType, PAT.Company,
                PAT.PayableNumber, CONVERT(Nvarchar(10), PAT.PayableDate, 120) as PayableDate, CONVERT(Nvarchar(10), PAT.TransDate, 120) as TransDate, PAT.Vendor,
                PAT.Voucher, PAT.Stage, PAT.GLAccount, PAT.ControlAmount, PASummary.Amount,
                PASummary.TotalCount, PASummary.MinWBS1, PASummary.WBS1Count,
                PAT.PayableFileName, PAT.FileID, PAT.LiabCode, PAT.BankCode, PAT.PayTerms,
                CONVERT(Nvarchar(10), PAT.PayDate, 120) as PayDate, PAT.Address, PAT.CheckNumber,
                PAT.SourceBatch, PAT.SourceType, PAT.SourcePKey, PAT.SourceSeq,
                PAT.PONumber, PAT.POPKey, PAT.POSeq, PAT.StorageSeq, PAT.StoragePathOrID,
                PAT.Batch, case
					when not ap.Posted IS NULL then
						case when ap.Posted = ''N'' then ''UnPosted''
						when ap.Posted = ''Y'' then ''Posted''
						else ap.Posted
					end
					when not PAT.Batch is null then ''Deleted''
				else null
			end as TransFileStatus, PASummary.Amount as VisionAmount, convert(varchar(23), PAT.ModDate, 121) as ModDate, 
			PASummary.MinWBS1 as VisionMinWBS1, PASummary.WBS1Count as VisionWBS1Count' + (CASE WHEN @applyPayableFilterLast=0 THEN ', '+@filterColumnSql+' as Include' ELSE '' END) +
			@custFields

	DECLARE @PASummary Nvarchar(max) = N'
				SELECT PayableSeq, Sum(Amount) as Amount, Min(WBS1) as MinWBS1, Max(WBS1) as MaxWBS1,
                        Count(*) as TotalCount, Count(distinct WBS1) as WBS1Count
                    FROM CCG_PAT_ProjectAmount
                    GROUP BY PayableSeq'

	SET @sql = N'
        SELECT '+@allSelectFields+'
		INTO #PayablesQuery
		FROM CCG_PAT_Payable PAT ' + (CASE WHEN @applyPayableFilterLast=0 then @filterJoinSql else '' END) + '
			LEFT JOIN CCG_PAT_Payable PAT0 ON PAT0.Seq = PAT.ParentSeq
			LEFT JOIN ('+@PASummary+') PASummary on PASummary.PayableSeq = PAT.Seq
			LEFT JOIN CCG_PAT_CustomColumns PAT_Cust on PAT_Cust.PayableSeq = PAT.Seq
			LEFT JOIN (
				SELECT apMaster.MasterPKey, vendor, voucher, Batch, invoice, MAX(posted) as posted,
						''I'' as PayableType
					FROM apMaster
					GROUP BY apMaster.MasterPKey, Vendor, Voucher, batch, invoice ';

	IF @hasDisbursement = 1			-- disbursements
		SET @sql = @sql + '
				UNION ALL
				SELECT CheckNo as MasterPKey, vendor, voucher, Batch, invoice, MAX(posted) as posted, ''D'' as PayableType
					FROM cvMaster
					GROUP BY CheckNo, Vendor, Voucher, batch, invoice ';

	SET @sql = @sql + '
			) ap on ';

	-- this method is much more efficient, but does not work with some history loading.
	--      TBD if we can use this by default.  Need to check AddInc
	IF @useMasterPKey = 1 AND @hasDisbursement = 0
		SET @sql = @sql + 'ap.MasterPKey = convert(Nvarchar(20), PAT.Seq) and ap.Batch = PAT.Batch ';
	-- this method is less efficient but works with disbursements
	ELSE IF @useMasterPKey = 1
		SET @sql = @sql + 'ap.MasterPKey = case
					when PAT.PayableType = ''D'' then PAT.CheckNumber
					else convert(Nvarchar(20), PAT.Seq)
				end and ap.Batch = PAT.Batch ';
	ELSE
		SET @sql = @sql + 'PAT.Vendor = ap.Vendor and PAT.PayableType = ap.PayableType
				and (PAT.Voucher = ap.Voucher or (
					ap.Voucher is null and PAT.PayableNumber = ap.Invoice and PAT.Batch = ap.Batch)) '; -- ap.Voucher is null on history loaded items

	IF @joinVE = 1
		SET @sql = @sql + '
			LEFT JOIN VE on PAT.Vendor = VE.Vendor ';
	IF @joinVEAccounting = 1
		SET @sql = @sql + '
			LEFT JOIN VEAccounting on PAT.Vendor = VEAccounting.Vendor ';
	if @joinVECustom = 1
		SET @sql = @sql + '
			LEFT JOIN VendorCustomTabFields on PAT.Vendor = VendorCustomTabFields.Vendor ';

	SET @sql = @sql + @outerApplies + '
		WHERE 1=1 ' + @filter;
		-- consider filtering out contracts and/or disbursments if not enabled

	-- Instead we need a workflow to insert a PO record into CCG_PAT_Payable
	IF @showAllRelevantContracts = 1 
		SET @sql = @sql + '

			INSERT INTO #PayablesQuery
				SELECT '+@allSelectFields+'
					FROM CCG_PAT_Payable PAT
					LEFT JOIN CCG_PAT_Payable PAT0 ON PAT0.Seq = PAT.ParentSeq
						LEFT JOIN ('+@PASummary+') PASummary on PASummary.PayableSeq = PAT.Seq
						LEFT JOIN CCG_PAT_CustomColumns PAT_Cust on PAT_Cust.PayableSeq = PAT.Seq
						' + @outerApplies + '
						OUTER APPLY (SELECT NULL as Posted) ap
					WHERE PAT.PayableType = ''C'' AND PAT.Seq IN (SELECT DISTINCT p.ParentSeq FROM #PayablesQuery p LEFT JOIN CCG_PAT_Pending pn ON pn.PayableSeq = p.Seq)
						AND PAT.Seq NOT IN (SELECT DISTINCT p.Seq FROM #PayablesQuery p) ';

	IF @getVisionAmounts = 1
		SET @sql = @sql + '

			UPDATE p SET
					p.VisionAmount = ap.VisionAmount,
					p.VisionMinWBS1 = ap.VisionMinWBS1,
					p.VisionWBS1Count = ap.VisionWBS1Count
				FROM #PayablesQuery p
					LEFT JOIN (
						SELECT vo.vendor, vo.voucher, sum(l.' + @useVisionAmountField + ') as VisionAmount,
								count(distinct l.WBS1) as VisionWBS1Count, min(l.WBS1) as VisionMinWBS1
							FROM VO
								INNER JOIN LedgerAP l on vo.Vendor = l.Vendor and vo.Voucher = l.Voucher
							WHERE l.TransType = N''AP'' and SubType = ''L'' and l.PostSeq <> 0
							GROUP BY vo.vendor, vo.voucher
					) ap on ap.Vendor = p.Vendor and ap.Voucher = p.Voucher ';

	SET @sql = @sql + '

		SELECT PAT.*
			FROM #PayablesQuery PAT ' + (CASE WHEN @applyPayableFilterLast=1 then @filterJoinSql else '' END) + '
			' + (CASE WHEN @useSqlFilter=1 AND @payableFilterIsTableFunction=0 AND @applyPayableFilterLast=0 THEN '
			WHERE Include = ''1'' '
			WHEN @useSqlFilter=1 AND @payableFilterIsTableFunction=0 AND @applyPayableFilterLast=1 THEN '
			WHERE '+@filterColumnSql+' = ''1'' '
			ELSE '' END) + '
            ORDER BY case when PayableType = ''C'' then Seq else ParentSeq end, PayableType, Seq
		DROP TABLE #PayablesQuery ';

	EXEC(@sql);
	--SELECT @sql

END;
GO
