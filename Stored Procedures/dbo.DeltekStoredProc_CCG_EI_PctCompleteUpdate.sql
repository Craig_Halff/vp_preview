SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_PctCompleteUpdate] ( @wbs1 nvarchar(32), @wbs2 nvarchar(7), @wbs3 nvarchar(7), @seq int, @newFeePctCpl decimal(19, 4), @sNewFeeToDate varchar(30), @sNewFeePctCpl_TopLevel varchar(30), @sNewFeeToDate_TopLevel varchar(30), @KEY_EMPLOYEEID nvarchar(30), @DataType varchar(20), @ColumnLabel varchar(500), @OldValue decimal(19, 4), @NewValue decimal(19, 4), @saveFeePctCplImmediately bit, @feeMeth varchar(20), @feeBasis varchar(10), @category int)
             AS EXEC spCCG_EI_PctCompleteUpdate @wbs1,@wbs2,@wbs3,@seq,@newFeePctCpl,@sNewFeeToDate,@sNewFeePctCpl_TopLevel,@sNewFeeToDate_TopLevel,@KEY_EMPLOYEEID,@DataType,@ColumnLabel,@OldValue,@NewValue,@saveFeePctCplImmediately,@feeMeth,@feeBasis,@category
GO
