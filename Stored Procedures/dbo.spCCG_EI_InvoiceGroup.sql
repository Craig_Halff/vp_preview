SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_InvoiceGroup] @WBS1 varchar(32)
AS 
/*
Copyright (c) 2017 EleVia Software  All rights reserved.
05/02/2017 David Springer
           Populate Invoice Group if the value exists in the drop down list.
		   Called from spCCG_OpportunityPhasesCreateProjectWBS procedure
*/
Declare @invoiceGroup varchar(50)
BEGIN
		
	Select @invoiceGroup = em.firstname + ' ' + em.lastname + ' Team ' + Right(pr.org, (Select Org3Length From CFGFormat)) 
		from pr
		inner join em 
		on pr.biller = em.employee
		inner join CFGOrgCodesDescriptions ocd 
		on right(pr.org, (Select Org3Length From CFGFormat)) = Ocd.code
	where WBS1=@WBS1 and WBS2=' '  and ocd.orglevel = 3

		if Exists (Select 'x' from FW_CustomColumnValuesData where InfoCenterArea='Projects' and colname='custInvoiceGroup' and code=@invoiceGroup)
			Update ProjectCustomTabFields
			Set custInvoiceGroup = @invoiceGroup
			Where WBS1 = @WBS1
			  and WBS2  = ' ' -- project level
END
GO
