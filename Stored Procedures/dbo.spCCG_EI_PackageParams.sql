SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_PackageParams] (
	@WBS1				Nvarchar(30),
	@Invoice			Nvarchar(12) = NULL --optional, when null will figure out the last final
)
AS BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	-- Copyright (c) 2020 EleVia Software. All rights reserved. 

	/* Returns parameters available to the content of the package building software in XML format.
		This will need to modified for custom templates when template parameters are not adequate. */
	/*
		exec spCCG_EI_PackageParams '0001743.11'
		exec spCCG_EI_PackageParams '2003005.00'
		exec spCCG_EI_PackageParams '2002010.00','<Draft>'
	*/
	--declare @WBS1 varchar(20)
	--declare @Invoice varchar(20)
	declare @UseInvoice Nvarchar(12)
	declare  @BillingClientID varchar(50)
	declare  @InvoiceDate datetime
	declare  @billFromDate datetime
	declare  @billToDate datetime
	declare @InvoiceAmount decimal (19,5)
	declare  @InvoiceFolder varchar(1000)
	declare  @InvoiceFolderFinals varchar(1000)
	declare @BillingCurrencyCode varchar(3)
	declare @ClientCulture varchar(10)
	declare @MulticompanyEnabled varchar(1)
	declare @MulticurrencyEnabled varchar(1)
	declare @VisionVersion varchar(11)
	declare @CurrentPeriod int
	declare @Org1Length int
	declare @ProjectCompany Nvarchar(20)

	set @ClientCulture = 'en-US'--multi language setups require client specific logic to set culture which can control date and number formats in templates

	select @VisionVersion=[version],@CurrentPeriod=currentperiod,@MulticompanyEnabled=MulticompanyEnabled,@MulticurrencyEnabled=MulticurrencyEnabled from CFGSystem
	--set @WBS1 = '2003005.00'

	set @ProjectCompany = ' '
	set @Org1Length = -1
	if @MulticompanyEnabled = 'Y'
	BEGIN
		select @Org1Length= Org1Length from CFGFormat
	END

	select @BillingClientID = BillingClientID,@BillingCurrencyCode = PR.BillingCurrencyCode,
		@InvoiceFolder = custInvoiceFolder, @InvoiceFolderFinals = custInvoiceFolderFinals,
		@ProjectCompany = case when @MulticompanyEnabled = 'Y' and @Org1Length > 0 then LEFT(PR.Org,@Org1Length) else ' ' end
	from PR
	--left join CL BCL on PR.BillingClientID = BCL.ClientID
	--left join CL on PR.ClientID = CL.Client
	left join ProjectCustomTabFields c on c.WBS1 = PR.WBS1 and c.WBS2 = N' '
	where PR.WBS1 = @WBS1 and PR.WBS2 = N' '

	IF @Invoice is null --default to the newest non '<Draft>' invoice
		SELECT top 1 @InvoiceDate = invoicedate, @UseInvoice = invoice,
				@billFromDate = FromDate, @billToDate = ToDate
			FROM billInvMaster
			WHERE MainWBS1 = @WBS1 And Left(invoice,1) <> '<'
		order by InvoiceDate desc, Invoice desc, RecdSeq
	ELSE
		SELECT top 1 @InvoiceDate = invoicedate, @UseInvoice = invoice,
				@billFromDate = FromDate, @billToDate = ToDate
			FROM billInvMaster where MainWBS1 = @WBS1 and Invoice = @Invoice -- and RecdSeq = 1
			order by InvoiceDate desc, Invoice desc, RecdSeq

	--print @WBS1
	--print ISNULL(@Invoice,@UseInvoice)
	--print @InvoiceDate
	select @InvoiceAmount = SUM(finalamount) from billInvSums where MainWBS1 = @WBS1 and Invoice = ISNULL(@Invoice,@UseInvoice)
		and ArrayType = 'I'
	--print @InvoiceAmount

		select p.SourceType as '@source'
			, p.[Type] as '@dataType'
			, p.Name as '@name'
			, p.BlankWarn '@blankWarn'
			, p.Value as 'value'
		from (
			select 'static' as SourceType,'string' as [Type],'WBS1' as Name,'BlankNull' as BlankWarn,@WBS1 as value
			union all
			select 'static','string','invoiceFolder','BlankNull',@InvoiceFolder
			union all
			select 'static','string','invoiceFolderFinals',null,@InvoiceFolderFinals
			union all
			select 'static','currency','invoiceTotal','Null' as BlankWarn,convert(varchar(20),convert(money,@InvoiceAmount))
			union all
			select 'static','string','invoice','BlankNull' as BlankWarn,@UseInvoice
			union all
			select 'static','date','invoiceDate','Null' as BlankWarn,convert(varchar(24),@InvoiceDate,126)
			union all
			select 'static','date','billFromDate','Null' as BlankWarn,convert(varchar(24),@billFromDate,126)
			union all
			select 'static','date','billToDate','Null' as BlankWarn,convert(varchar(24),@billToDate,126)
			union all
			select 'static','string' as dataType,'company' as name,'BlankNull' as BlankWarn,@ProjectCompany where @MulticompanyEnabled = 'Y'
			union all
			select 'static','string' as dataType,'companyName' as name,'BlankNull' as BlankWarn,firmName from CFGMainData where Company = @ProjectCompany
			union all
			select 'static','string' as dataType,'billingCurrencyCode' as name,null,@BillingCurrencyCode where @MulticurrencyEnabled = 'Y'
			union all
			select 'static','string' as dataType,'clientCulture' as name,null,@ClientCulture --where @MulticurrencyEnabled = 'Y'
			union all
			select 'static','date' as dataType,'packageDate' as name,null,convert(varchar(24),GETDATE(),126)

		) p
		FOR XML PATH('parameter'),TYPE
END
GO
