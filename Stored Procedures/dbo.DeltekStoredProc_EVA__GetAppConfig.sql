SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_EVA__GetAppConfig] (@app varchar(50), @ccgPre varchar(1) = null)
AS EXEC spEVA__GetAppConfig @app, @ccgPre
GO
