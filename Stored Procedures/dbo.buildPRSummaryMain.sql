SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[buildPRSummaryMain]
	@visionuser		Nvarchar(32),
	@sessionID		varchar(32)
AS
BEGIN
   SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
   SET NOCOUNT ON

	------------------------------------------------------------------
	--Calculate PRSummaryMain columns based on PRSummarySub columns.
	UPDATE PRSummaryMain  SET
		PRSummaryMain.Revenue		= Sub.Revenue,
		PRSummaryMain.RevenueBillingCurrency	= Sub.RevenueBillingCurrency,
		PRSummaryMain.RevenueProjectCurrency	= Sub.RevenueProjectCurrency,
		PRSummaryMain.Billed		= Sub.Billed,
		PRSummaryMain.BilledLab		= Sub.BilledLab,
		PRSummaryMain.BilledCons	= Sub.BilledCons,
		PRSummaryMain.BilledExp		= Sub.BilledExp,
		PRSummaryMain.BilledFee		= Sub.BilledFee,
		PRSummaryMain.BilledUnit	= Sub.BilledUnit,
		PRSummaryMain.BilledAddOn	= Sub.BilledAddOn,
		PRSummaryMain.BilledTaxes	= Sub.BilledTaxes,
		PRSummaryMain.BilledInterest	= Sub.BilledInterest,
		PRSummaryMain.BilledOther	= Sub.BilledOther,
		PRSummaryMain.AR		= Sub.AR,
		PRSummaryMain.ARProjectCurrency		= Sub.ARProjectCurrency,
		PRSummaryMain.ARBillingCurrency		= Sub.ARBillingCurrency,
		PRSummaryMain.Received		= Sub.Received,
		PRSummaryMain.ReceivedProjectCurrency	= Sub.ReceivedProjectCurrency,
		PRSummaryMain.ReceivedBillingCurrency	= Sub.ReceivedBillingCurrency,
	 	PRSummaryMain.Overhead = Sub.Overhead,
		PRSummaryMain.OverheadProjectCurrency 	= Sub.OverheadProjectCurrency,
		PRSummaryMain.LaborCost		= Sub.LaborCost,
		PRSummaryMain.LaborCostProjectCurrency	= Sub.LaborCostProjectCurrency,
		PRSummaryMain.LaborCostBillingCurrency	= Sub.LaborCostBillingCurrency,
		PRSummaryMain.LaborBilling	= Sub.LaborBilling,

--		PRSummaryMain.SpentCost		= Sub.LaborCost + 0000 + Sub.ReimbConsCost + Sub.ReimbOtherCost + Sub.DirectConsCost + Sub.DirectOtherCost,
--		PRSummaryMain.ProfitCost	= Sub.Revenue - (Sub.LaborCost + 0000 + Sub.ReimbConsCost + Sub.ReimbOtherCost + Sub.DirectConsCost + Sub.DirectOtherCost),

		PRSummaryMain.Unbilled		= Sub.RevenueBillingCurrency - Sub.Billed,
		PRSummaryMain.SpentCostLessOH	= Sub.LaborCost + Sub.ReimbConsCost + Sub.ReimbOtherCost + Sub.DirectConsCost + Sub.DirectOtherCost,
		PRSummaryMain.SpentCostLessOHProjectCurrency	= Sub.LaborCostProjectCurrency + Sub.ReimbConsCostProjectCurrency + Sub.ReimbOtherCostProjectCurrency + Sub.DirectConsCostProjectCurrency + Sub.DirectOtherCostProjectCurrency,
		PRSummaryMain.SpentCostLessOHBillingCurrency	= Sub.LaborCostBillingCurrency + Sub.ReimbConsCostBillingCurrency + Sub.ReimbOtherCostBillingCurrency + Sub.DirectConsCostBillingCurrency + Sub.DirectOtherCostBillingCurrency,
		PRSummaryMain.SpentBilling	= Sub.LaborBilling + Sub.ReimbConsBilling + Sub.ReimbOtherBilling + Sub.DirectConsBilling + Sub.DirectOtherBilling,
		PRSummaryMain.GrossMargin	= Sub.Revenue - (Sub.LaborCost + Sub.ReimbConsCost + Sub.ReimbOtherCost + Sub.DirectConsCost + Sub.DirectOtherCost),
		PRSummaryMain.GrossMarginProjectCurrency	= Sub.RevenueProjectCurrency - (Sub.LaborCostProjectCurrency + Sub.ReimbConsCostProjectCurrency + Sub.ReimbOtherCostProjectCurrency + Sub.DirectConsCostProjectCurrency + Sub.DirectOtherCostProjectCurrency),
		PRSummaryMain.GrossMarginBillingCurrency	= Sub.RevenueBillingCurrency - (Sub.LaborCostBillingCurrency + Sub.ReimbConsCostBillingCurrency + Sub.ReimbOtherCostBillingCurrency + Sub.DirectConsCostBillingCurrency + Sub.DirectOtherCostBillingCurrency),
		PRSummaryMain.NetRevenueCost	= Sub.Revenue - (Sub.ReimbConsCost + Sub.ReimbOtherCost + Sub.DirectConsCost + Sub.DirectOtherCost),
		PRSummaryMain.NetRevenueCostProjectCurrency	= Sub.RevenueProjectCurrency - (Sub.ReimbConsCostProjectCurrency + Sub.ReimbOtherCostProjectCurrency + Sub.DirectConsCostProjectCurrency + Sub.DirectOtherCostProjectCurrency),
		PRSummaryMain.NetRevenueCostBillingCurrency	= Sub.RevenueBillingCurrency - (Sub.ReimbConsCostBillingCurrency + Sub.ReimbOtherCostBillingCurrency + Sub.DirectConsCostBillingCurrency + Sub.DirectOtherCostBillingCurrency),
		PRSummaryMain.NetRevenueBilling	= Sub.Revenue - (Sub.ReimbConsBilling + Sub.ReimbOtherBilling + Sub.DirectConsBilling + Sub.DirectOtherBilling),
		PRSummaryMain.ProfitCostLessOH	= Sub.Revenue - (Sub.LaborCost + Sub.ReimbConsCost + Sub.ReimbOtherCost + Sub.DirectConsCost + Sub.DirectOtherCost),
		PRSummaryMain.ProfitCostLessOHProjectCurrency	= Sub.RevenueProjectCurrency - (Sub.LaborCostProjectCurrency + Sub.ReimbConsCostProjectCurrency + Sub.ReimbOtherCostProjectCurrency + Sub.DirectConsCostProjectCurrency + Sub.DirectOtherCostProjectCurrency),
		PRSummaryMain.ProfitCostLessOHBillingCurrency	= Sub.RevenueBillingCurrency - (Sub.LaborCostBillingCurrency + Sub.ReimbConsCostBillingCurrency + Sub.ReimbOtherCostBillingCurrency + Sub.DirectConsCostBillingCurrency + Sub.DirectOtherCostBillingCurrency),
		PRSummaryMain.ProfitBilling	= Sub.Revenue - (Sub.LaborBilling + Sub.ReimbConsBilling + Sub.ReimbOtherBilling + Sub.DirectConsBilling + Sub.DirectOtherBilling)

	FROM PRSummaryMain
	INNER JOIN PRSummarySub AS Sub ON PRSummaryMain.Period = Sub.Period AND PRSummaryMain.WBS1 = Sub.WBS1 
	AND PRSummaryMain.WBS2 = Sub.WBS2 AND PRSummaryMain.WBS3 = Sub.WBS3
	INNER JOIN PRSummaryWBSList AS psl ON psl.Period = Sub.Period AND psl.WBS1 = Sub.WBS1 
	AND psl.WBS2 = Sub.WBS2 AND psl.WBS3 = Sub.WBS3
	WHERE psl.sessionID = @sessionID

--	UPDATE PRSummaryMain
--	SET 	PRSummaryMain.Overhead = PRF.RegOH
--	FROM PRSummaryMain
--	INNER JOIN PRSummaryWBSList AS psl ON psl.Period = PRSummaryMain.Period 
--	AND psl.WBS1 = PRSummaryMain.WBS1 AND psl.WBS2 = PRSummaryMain.WBS2 AND psl.WBS3 = PRSummaryMain.WBS3
--	INNER JOIN PRF ON 
--	WHERE psl.sessionID = @sessionID

	UPDATE PRSummaryMain
	SET 	PRSummaryMain.ModUser =@visionuser, PRSummaryMain.ModDate = getDate()
	FROM PRSummaryMain
	INNER JOIN PRSummaryWBSList AS psl ON psl.Period = PRSummaryMain.Period 
	AND psl.WBS1 = PRSummaryMain.WBS1 AND psl.WBS2 = PRSummaryMain.WBS2 AND psl.WBS3 = PRSummaryMain.WBS3
	WHERE psl.sessionID = @sessionID
END

GO
