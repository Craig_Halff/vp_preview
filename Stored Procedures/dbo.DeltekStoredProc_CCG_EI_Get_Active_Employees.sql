SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Get_Active_Employees]
AS
BEGIN
	-- TEST: EXEC [DeltekStoredProc_CCG_EI_Get_Active_Employees]
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- Check series against number of non-terminated employees
	SELECT count(*) as cnt FROM EM WHERE Status <> 'T'
END;
GO
