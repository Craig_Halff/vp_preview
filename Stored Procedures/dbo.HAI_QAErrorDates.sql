SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[HAI_QAErrorDates]
    @CheckDate DATE
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
 
    DECLARE @Error1 NVARCHAR(255) = N'Unable to select future QA Review Date.                                                                          ';
    DECLARE @ComputeDate DATE;
 
    IF @CheckDate > GETDATE()+3
        RAISERROR(@Error1, 16, 1);
 
END
GO
