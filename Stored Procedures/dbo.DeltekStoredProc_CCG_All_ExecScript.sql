SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_All_ExecScript] ( @Script varchar(max))
             AS EXEC spCCG_All_ExecScript @Script
GO
