SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Status_CHECK] ( @WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7), @NewValue varchar(255))
             AS EXEC spCCG_EI_Status_CHECK @WBS1,@WBS2,@WBS3,@NewValue
GO
