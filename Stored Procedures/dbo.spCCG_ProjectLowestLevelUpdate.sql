SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectLowestLevelUpdate] @WBS1 varchar (32)
AS
/*
Copyright 2019 (c) Central Consulting Group.   All rights reserved.
07/29/2019	David Springer
			Update Project Lowest Level Name
			Call from Project CHANGE workflow when Name has changed.
*/
BEGIN
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	Update l
	Set l.CustName = p1.WBS1 + 
			CASE When p2.WBS2 is null Then '' Else ' | ' + p2.WBS2 END + 
			CASE When p3.WBS3 is null Then '' Else ' | ' + p3.WBS3 END +
			' ' + p1.Name + 
			CASE When p2.WBS2 is null Then '' Else ' | ' + p2.Name END + 
			CASE When p3.WBS3 is null Then '' Else ' | ' + p3.Name END,
		l.CustWBS1Name = p1.Name,
		l.CustWBS2Name = p2.Name,
		l.CustWBS3Name = p3.Name
	From UDIC_ProjectLowestLevel l, PR p1
		Left Join PR p2 on p2.WBS1 = p1.WBS1 and p2.WBS2 <> ' ' and p2.WBS3 = ' '
		Left Join PR p3 on p3.WBS1 = p2.WBS1 and p3.WBS2 = p2.WBS2 and p3.WBS3 <> ' '
	Where p1.WBS1 = @WBS1
	  and p1.WBS2 = ' ' -- project level
	  and p1.WBS1 = l.CustWBS1
	  and IsNull (p2.WBS2, l.CustWBS2) = l.CustWBS2
	  and IsNull (p3.WBS3, l.CustWBS3) = l.CustWBS3
END
GO
