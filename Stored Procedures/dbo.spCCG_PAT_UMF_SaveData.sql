SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_UMF_SaveData] (
	@product			varchar(20),		/* PAT/EI/etc. */
	@cacheField			varchar(100),
	@updateTable		varchar(200),
	@updateField		varchar(100),
	@emp				varchar(32),
	@wbs1				Nvarchar(30),
	@wbs2				Nvarchar(7),
	@wbs3				Nvarchar(7),
	@newValue			varchar(max),
	@payableSeq			int = 0				/* Only provided if product = PAT */
)
AS
BEGIN
	/* Copyright (c) 2020 EleVia Software.  All rights reserved. */
	/*
		exec [spCCG_PAT_UMF_SaveData] 'EI', '', 'ProjectCustomTabFields', 'CustMemorandumField', '', '2002010.00', ' ', ' ', '<Test>'
	*/
	SET NOCOUNT ON
	declare @sql varchar(max), @sqlHist varchar(max)
	set @newValue = Replace(@newValue,'''','''''')

	if @product = 'PAT'
	begin
		set @sqlHist = '
			insert into CCG_PAT_HistoryUpdateColumns (ActionTaken, ActionDate, ActionTakenBy, PayableSeq, DataType, ConfigCustomColumn, ColumnLabel, OldValue, NewValue)
				values (''Change'', getutcdate(), '''+@emp+''', '+cast(@payableSeq as varchar(20))+', ''A'', null, '''+@updateField+''', '''', '''+@newValue+''')'

		if isnull(@cacheField,'') <> '' and (isnull(@wbs1,'') = '' or isnull(@updateTable,'') = '' or isnull(@updateField,'') = '')
		begin
			set @sql = N'
				if exists (select ''x'' from CCG_PAT_CustomColumns where PayableSeq = '+cast(@payableSeq as varchar(20))+')
					update CCG_PAT_CustomColumns set
							'+@cacheField+' = '''+@newValue+''',
							'+@cacheField+'_ModEmp = '''+@emp+'''
						where PayableSeq = '+cast(@payableSeq as varchar(20))+'
				else
					insert into CCG_PAT_CustomColumns (PayableSeq, '+@cacheField+', '+@cacheField+'_ModEmp)
						values ('+cast(@payableSeq as varchar(20))+', '''+@newValue+''', '''+@emp+''')'
		end else
		begin
			set @sql = N'
				update '+@updateTable+' set '+@updateField+' = '''+@newValue+'''
					where WBS1 = N'''+@wbs1+''' and WBS2 = N'''+@wbs2+''' and WBS3 = N'''+@wbs3+''''

			set @sqlHist = Replace(@sqlHist, '''Change''', '''Update''')
		end
	end

	--print @sql
	exec (@sql)
	exec (@sqlHist)
END
GO
