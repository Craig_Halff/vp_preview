SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Route_Load]
	@VISION_LANGUAGE	varchar(10)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT T1.Route, IsNull(T2.RouteLabel, IsNull(T1.RouteLabel, '')) as RouteLabel,
            IsNull(T2.RouteDescription, IsNull(T1.RouteDescription, '')) as RouteDescription, Status
        FROM CCG_PAT_ConfigRoutes T1
            LEFT JOIN CCG_PAT_ConfigRoutesDescriptions T2 ON T2.Route = T1.Route
				and T2.UICultureName = @VISION_LANGUAGE
        ORDER BY RouteLabel;
END;
GO
