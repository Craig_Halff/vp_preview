SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE   PROCEDURE [dbo].[HAI_ProjectPursuitDateSync]
    @WBS1 VARCHAR(15)

AS
    /**********************************************************************************
			Copyright (c) 2021 Halff Associates Inc. All rights reserved.

			Update the Cooresponding dbo.ProjectCustomTabFields date when date is 
			entered or adjusted on the Milestone Grid.

			08/23/2021	Jeremy Baummer - Created
			09/03/2021  Jeremy Baummer - Added Contract Awarded

	***********************************************************************************/
	BEGIN
		SET NOCOUNT ON;
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

		UPDATE px SET px.CustFeeProposalSubmitted = m.EndDate, px.ModUser = 'Automated', px.ModDate = GETDATE()
		FROM dbo.ProjectCustomTabFields px
			INNER JOIN dbo.PRMilestone m
				ON px.WBS1 = m.WBS1 AND px.WBS2 = m.WBS2 AND px.WBS3 = m.WBS3 AND m.Code = 'CustFeeSubmitted'
		WHERE px.WBS1 = @WBS1 AND px.WBS2 = ' ' AND ISNULL(px.CustFeeProposalSubmitted,CAST(1999-01-01 AS DATETIME)) <> m.EndDate
		UPDATE px SET px.CustInterviewDate = m.EndDate, px.ModUser = 'Automated', px.ModDate = GETDATE()
		FROM dbo.ProjectCustomTabFields px
			INNER JOIN dbo.PRMilestone m
				ON px.WBS1 = m.WBS1 AND px.WBS2 = m.WBS2 AND px.WBS3 = m.WBS3 AND m.Code = 'CustInterviewedDate'
		WHERE px.WBS1 = @WBS1 AND px.WBS2 = ' ' AND ISNULL(px.CustInterviewDate,CAST(1999-01-01 AS DATETIME)) <> m.EndDate
		UPDATE px SET px.CustRFQAnticipated = m.EndDate, px.ModUser = 'Automated', px.ModDate = GETDATE()
		FROM dbo.ProjectCustomTabFields px
			INNER JOIN dbo.PRMilestone m
				ON px.WBS1 = m.WBS1 AND px.WBS2 = m.WBS2 AND px.WBS3 = m.WBS3 AND m.Code = 'CustRFQAnticipated'
		WHERE px.WBS1 = @WBS1 AND px.WBS2 = ' ' AND ISNULL(px.CustRFQAnticipated,CAST(1999-01-01 AS DATETIME)) <> m.EndDate
		UPDATE px SET px.CustRFQRelease = m.EndDate, px.ModUser = 'Automated', px.ModDate = GETDATE()
		FROM dbo.ProjectCustomTabFields px
			INNER JOIN dbo.PRMilestone m
				ON px.WBS1 = m.WBS1 AND px.WBS2 = m.WBS2 AND px.WBS3 = m.WBS3 AND m.Code = 'CustRFQRelease'
		WHERE px.WBS1 = @WBS1 AND px.WBS2 = ' ' AND ISNULL(px.CustRFQRelease,CAST(1999-01-01 AS DATETIME)) <> m.EndDate
		UPDATE px SET px.CustRFQSubmitted = m.EndDate, px.ModUser = 'Automated', px.ModDate = GETDATE()
		FROM dbo.ProjectCustomTabFields px
			INNER JOIN dbo.PRMilestone m
				ON px.WBS1 = m.WBS1 AND px.WBS2 = m.WBS2 AND px.WBS3 = m.WBS3 AND m.Code = 'CustRFQSubmitted'
		WHERE px.WBS1 = @WBS1 AND px.WBS2 = ' ' AND ISNULL(px.CustRFQSubmitted,CAST(1999-01-01 AS DATETIME)) <> m.EndDate
		UPDATE px SET px.CustSubmittalDeadline = m.EndDate, px.ModUser = 'Automated', px.ModDate = GETDATE()
		FROM dbo.ProjectCustomTabFields px
			INNER JOIN dbo.PRMilestone m
				ON px.WBS1 = m.WBS1 AND px.WBS2 = m.WBS2 AND px.WBS3 = m.WBS3 AND m.Code = 'CustSubmittal'
		WHERE px.WBS1 = @WBS1 AND px.WBS2 = ' ' AND ISNULL(px.CustSubmittalDeadline,CAST(1999-01-01 AS DATETIME)) <> m.EndDate
		UPDATE px SET px.CustRFQSelected = m.EndDate, px.ModUser = 'Automated', px.ModDate = GETDATE()
		FROM dbo.ProjectCustomTabFields px
			INNER JOIN dbo.PRMilestone m
				ON px.WBS1 = m.WBS1 AND px.WBS2 = m.WBS2 AND px.WBS3 = m.WBS3 AND m.Code = 'CustSelectNeg'
		WHERE px.WBS1 = @WBS1 AND px.WBS2 = ' ' AND ISNULL(px.CustRFQSelected,CAST(1999-01-01 AS DATETIME)) <> m.EndDate
		UPDATE px SET px.CustContractAwarded = m.EndDate, px.ModUser = 'Automated', px.ModDate = GETDATE()
		FROM dbo.ProjectCustomTabFields px
			INNER JOIN dbo.PRMilestone m
				ON px.WBS1 = m.WBS1 AND px.WBS2 = m.WBS2 AND px.WBS3 = m.WBS3 AND m.Code = 'SysContract'
		WHERE px.WBS1 = @WBS1 AND px.WBS2 = ' ' AND ISNULL(px.CustRFQSelected,CAST(1999-01-01 AS DATETIME)) <> m.EndDate
	END
GO
