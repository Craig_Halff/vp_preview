SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_FormRouting_LoadGrid]
	@ROLE_ACCT					Nvarchar(200),
	@seq						Nvarchar(20),
	@history					bit,
	@showAll					bit,
	@POSeq						Nvarchar(20),
	@VISION_LANGUAGE			Nvarchar(10) = 'en-US'
AS
BEGIN
	-- [spCCG_PAT_FormRouting_LoadGrid] '', '4112', 1, 1, 1, 'en-US', 0
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	DECLARE @sql Nvarchar(max) = N''
	DECLARE @GetInactiveEmployeeData bit = 1

	SET @sql += N'
        SELECT 3 as TypeOrder, ''Pending'' as RowType, SortOrder,
				case when Parallel = ''Y'' then 1 else 0 end as Parallel, Convert(Nvarchar, DateLastNotificationSent, 120) as ActionDate,
                case when ISNull(p.Employee, '''') = '''' then N''' + @ROLE_ACCT + ''' else p.Employee end as Employee,
                ISNULL(rdesc.RouteLabel, r.RouteLabel) as Stage, Description, Seq, null as DelegateFor, null as ActionRecipient
				'+(CASE WHEN @GetInactiveEmployeeData=1 THEN ', EM.LastName, EM.FirstName, EM.PreferredName, null as D_LastName, null as D_FirstName,
					null as D_PreferredName, null as AR_LastName, null as AR_FirstName, null as AR_PreferredName' ELSE '' END)+'
            FROM CCG_PAT_Pending p'+
			(CASE WHEN @GetInactiveEmployeeData=1 THEN '
				LEFT JOIN EM ON EM.Employee = p.Employee' ELSE '' END)+'
                LEFT JOIN CCG_PAT_ConfigRoutes r on p.[Route] = r.[Route]
				LEFT JOIN CCG_PAT_ConfigRoutesDescriptions rdesc on rdesc.[Route] = r.[Route] AND rdesc.UICultureName = ''' + @VISION_LANGUAGE + '''
            WHERE PayableSeq = ' + @seq +
            (CASE WHEN @history = 1 THEN '
        UNION
        SELECT 2 as TypeOrder, ''Completed'' as RowType, ParallelSort,
				case when ParallelSort is null then 0 else 1 end, Convert(Nvarchar, ActionDate, 120), ActionTakenBy as Employee,
				ISNULL(sdesc.StageLabel, s.StageLabel) as Stage, Description, Seq, h.DelegateFor, h.ActionRecipient
				'+(CASE WHEN @GetInactiveEmployeeData=1 THEN ', EM.LastName, EM.FirstName, EM.PreferredName, DelF.LastName as D_LastName, DelF.FirstName as D_FirstName,
					DelF.PreferredName as D_PreferredName, ActRec.LastName as AR_LastName, ActRec.FirstName as AR_FirstName, ActRec.PreferredName as AR_PreferredName' ELSE '' END)+'
            FROM CCG_PAT_History h'+
			(CASE WHEN @GetInactiveEmployeeData=1 THEN '
				LEFT JOIN EM ON EM.Employee = h.ActionTakenBy
				LEFT JOIN EM DelF ON DelF.Employee = h.DelegateFor
				LEFT JOIN EM ActRec ON ActRec.Employee = h.ActionRecipient' ELSE '' END)+'
                LEFT JOIN CCG_PAT_ConfigStages s on h.Stage = s.Stage
                LEFT JOIN CCG_PAT_ConfigStagesDescriptions sdesc on sdesc.[Stage] = s.[Stage] AND sdesc.UICultureName = ''' + @VISION_LANGUAGE + '''
            WHERE ActionTaken = ''Stage Change'' and PayableSeq = ' + @seq ELSE '' END) +
            (CASE WHEN @history = 1 AND @showAll = 1 THEN '
        UNION
        SELECT 2 as TypeOrder, ''Completed'' as RowType, ParallelSort,
				case when ParallelSort is null then 0 else 1 end, Convert(Nvarchar, ActionDate, 120), ActionTakenBy as Employee,
				'''' as Stage, case when ActionTaken = ''Stamp'' then Description else ActionTaken end as Description, Seq, h.DelegateFor,
				case when ActionTaken = ''Stamp'' then null else h.ActionRecipient end as ActionRecipient
				'+(CASE WHEN @GetInactiveEmployeeData=1 THEN ', EM.LastName, EM.FirstName, EM.PreferredName, DelF.LastName as D_LastName, DelF.FirstName as D_FirstName,
					DelF.PreferredName as D_PreferredName, ActRec.LastName as AR_LastName, ActRec.FirstName as AR_FirstName, ActRec.PreferredName as AR_PreferredName' ELSE '' END)+'
            FROM CCG_PAT_History h'+
			(CASE WHEN @GetInactiveEmployeeData=1 THEN '
				LEFT JOIN EM ON EM.Employee = h.ActionTakenBy
				LEFT JOIN EM DelF ON DelF.Employee = h.DelegateFor
				LEFT JOIN EM ActRec ON ActRec.Employee = h.ActionRecipient' ELSE '' END)+'
            WHERE ActionTaken <> ''Stage Change'' and PayableSeq = ' + @seq ELSE '' END) +
            (CASE WHEN @history = 1 AND ISNULL(@POSeq, '') <> '' THEN '
        UNION
        SELECT 1 as TypeOrder, ''PO'' as RowType, ParallelSort,
				case when ParallelSort is null then 0 else 1 end, Convert(Nvarchar, ActionDate, 120), ActionTakenBy as Employee,
				ISNULL(sdesc.StageLabel, s.StageLabel) as Stage, Description, Seq, h.DelegateFor, h.ActionRecipient
				'+(CASE WHEN @GetInactiveEmployeeData=1 THEN ', EM.LastName, EM.FirstName, EM.PreferredName, DelF.LastName as D_LastName, DelF.FirstName as D_FirstName,
					DelF.PreferredName as D_PreferredName, ActRec.LastName as AR_LastName, ActRec.FirstName as AR_FirstName, ActRec.PreferredName as AR_PreferredName' ELSE '' END)+'
            FROM CCG_PAT_History h'+
			(CASE WHEN @GetInactiveEmployeeData=1 THEN '
				LEFT JOIN EM ON EM.Employee = h.ActionTakenBy
				LEFT JOIN EM DelF ON DelF.Employee = h.DelegateFor
				LEFT JOIN EM ActRec ON ActRec.Employee = h.ActionRecipient' ELSE '' END)+'
                LEFT JOIN CCG_PAT_ConfigStages s on h.Stage = s.Stage
				LEFT JOIN CCG_PAT_ConfigStagesDescriptions sdesc on sdesc.[Stage] = s.[Stage] AND sdesc.UICultureName = ''' + @VISION_LANGUAGE + '''
            WHERE ActionTaken = ''Stage Change'' and PayableSeq = ' + @POSeq ELSE '' END) + '
        ORDER BY 1, 3, 5'
	EXEC(@sql)
END
GO
