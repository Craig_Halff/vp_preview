SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create   Procedure [dbo].[spCCG_ProjectStatus] @WBS1 varchar(32), @Status varchar (1)
AS 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
/* 
Copyright (c) 2017 Central Consulting Group.  All rights reserved.
03/09/2017 David Springer
           Set Project Status
*/
BEGIN

   Update PR
   Set Status = @Status
   Where WBS1 = @WBS1
     and WBS2 = ' ' 

END
GO
