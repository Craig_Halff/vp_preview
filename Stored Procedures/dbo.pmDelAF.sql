SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[pmDelAF]
  @strAccordionFormatID varchar(32)
AS

BEGIN -- Procedure pmDelAF

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to delete an Accordion Calendar row and its Calendar Interval rows.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    
  DELETE RPCalendarInterval WHERE AccordionFormatID = @strAccordionFormatID
  DELETE RPAccordionFormat WHERE AccordionFormatID = @strAccordionFormatID

  SET NOCOUNT OFF

END -- pmDelAF
GO
