SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_PDFImposition_SideHistory] (
	@seq			int,
	@userLanguage	varchar(10),
	@mode			varchar(20),
	@stageFilterCSV	varchar(1000) = ''
) AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @sql Nvarchar(max)

	IF @mode = 'approved'
	BEGIN
		SET @sql = N'
		SELECT h.ActionDate, h.ActionTaken, h.ActionTakenBy,
				EM.LastName + '', '' + ISNULL(EM.PreferredName, EM.FirstName) as ActionTakenByName, h.Stage,
				CASE
					WHEN h.ApprovedDetail=p.ApprovedDetail then ''Y''
					WHEN not isnull(h.ApprovedDetail, '''') = '''' then ''N''
					ELSE null
				END as ApprovedDetailMatch,
				h.ApprovedDetail as historyAD, p.ApprovedDetail as currentAD,
				rd.RouteDescription,
				null as StampSeq, h.DelegateFor
			FROM CCG_PAT_Payable p
				INNER JOIN CCG_PAT_History h on p.Seq = h.PayableSeq
				LEFT JOIN EM on h.ActionTakenBy = EM.Employee
				LEFT JOIN CCG_PAT_ConfigRoutesDescriptions rd ON rd.Route = p.Route AND rd.UICultureName = ''' + @userLanguage + '''
			WHERE p.seq = ' + CAST(@seq as Nvarchar(20)) + ' and 
				(h.ApprovedDetail is not null' + (
					CASE WHEN @stageFilterCSV = '' THEN ')' ELSE ' or p.PayableType = ''P'') AND h.Stage in (' + @stageFilterCSV + ')' END)
	END
	ELSE IF @mode = 'stamp'
	BEGIN
		SET @sql = N'
		SELECT h.ActionDate, h.ActionTaken, h.ActionTakenBy,
				EM.LastName + '', '' + ISNULL(EM.PreferredName, EM.FirstName) as ActionTakenByName, h.Stage,
				CASE
					WHEN h.ApprovedDetail = p.ApprovedDetail then ''Y''
					WHEN not isnull(h.ApprovedDetail, '''') = '''' then ''N''
					ELSE null
				END as ApprovedDetailMatch,
				h.ApprovedDetail as historyAD, p.ApprovedDetail as currentAD,
				rd.RouteDescription,
				sp.Seq as StampSeq, h.DelegateFor
			FROM CCG_PAT_Payable p
				INNER JOIN CCG_PAT_History h on p.Seq = h.PayableSeq
				INNER JOIN CCG_PAT_ConfigStages s ON s.Stage = h.Stage
				INNER JOIN CCG_PAT_ConfigStamps sp ON sp.Seq = s.RequireStamp
				LEFT JOIN EM on h.ActionTakenBy = EM.Employee
				LEFT JOIN CCG_PAT_ConfigRoutesDescriptions rd ON rd.Route = p.Route AND rd.UICultureName = ''' + @userLanguage + '''
			WHERE p.seq = ' + CAST(@seq as Nvarchar(20)) + ' and (s.RequireStamp is not NULL) and h.ActionTaken IN (''Stage Change'',''Create'')'
				+ (CASE WHEN @stageFilterCSV = '' THEN '' ELSE ' AND h.Stage in (' + @stageFilterCSV + ')' END);
	END;
	EXEC(@sql);
END;
GO
