SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EMAIL_Process_Failed]
	@maxMessageTries int,
	@type			varchar(20)--FailedAttempt
AS
BEGIN
	/*
	DeltekStoredProc_CCG_EMAIL_Process_Failed
		@maxMessageTries = 2,
		@type = 'FailedAttempt'
 	*/
	SET NOCOUNT ON;
	declare @NumRowsAffected int
	declare @ErrMsg varchar(200)
	
	set @NumRowsAffected=0
	set @ErrMsg = ''
	
	BEGIN TRY
		BEGIN TRAN

			INSERT INTO CCG_Email_History (MessageSeq,MessageGroupSeq,SourceApplication,Sender,ToList,CCList,BCCList,Subject,Body,Priority,MaxDelay,Type,Description) 
                SELECT Seq,NULL ,SourceApplication,Sender,ToList,CCList,BCCList,Subject 
                 ,Body,Priority,MaxDelay,'Canceled','Max tries exceeded:' + convert(varchar(20),@maxMessageTries) + ' Requested:'+ Convert(varchar(20),CreateDateTime) 
                from CCG_Email_Messages where Seq in (	select messageseq --keep this subquery in sync with delete below
                     FROM CCG_EMAIL_HISTORY 
                     WHERE Type = @type and ActionDateTime > GetDate() - 30 --only go back 30 days to hopefully help with query performance
                     GROUP BY messageseq HAVING count(*) >=  @maxMessageTries )
			set @NumRowsAffected=@@ROWCOUNT
			if @NumRowsAffected > 0
			BEGIN
				DELETE from CCG_Email_Messages where Seq in (	select messageseq --keep this subquery in sync with insert above
						 FROM CCG_EMAIL_HISTORY 
						 WHERE Type = @type AND ActionDateTime > GetDate() - 30 --only go back 30 days to hopefully help with query performance
						 GROUP BY messageseq HAVING count(*) >=  @maxMessageTries )
				if @NumRowsAffected <> @@ROWCOUNT
				BEGIN--consider raising an error if we want to rollback
					set @ErrMsg = 'Count mismatch ' + convert(varchar(20),@@ROWCOUNT)
				END          
			END         
		COMMIT TRAN
		select @NumRowsAffected as ReturnValue, @ErrMsg as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		select -1 as  ReturnValue, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage
	END CATCH

	BEGIN TRY
		IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CCG_Email_Log]') AND type in (N'U'))
		BEGIN
		 exec('insert into CCG_Email_Log (Action,Detail) values (''PROCESS_FAILED'','''+  @type+''')')
		END
	END TRY
	BEGIN CATCH
		PRINT 'TRY LOG ERR:' + ERROR_MESSAGE()
	END CATCH

END;
GO
