SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpAlignStartDayOfWeek]
  @strPlanID VARCHAR(32)
AS

BEGIN -- Procedure rpAlignStartDayOfWeek

  SET NOCOUNT ON

  DECLARE @strNextPlanID varchar(32)
  DECLARE @strAccordionFormatID varchar(32)
  
  DECLARE @strPlanName Nvarchar(255)
  
  DECLARE @intTPDCount int
  
  DECLARE csrRPPlan CURSOR FOR
    SELECT DISTINCT CI.PlanID, P.PlanName FROM CFGResourcePlanning AS CFG, 
      RPCalendarInterval AS CI INNER JOIN RPPlan AS P ON CI.PlanID = P.PlanID
      WHERE (PeriodScale = 'w' OR PeriodScale = 'b') 
        AND CFG.Company = dbo.GetActiveCompany()
        AND P.StartingDayOfWeek != CFG.StartingDayOfWeek
        AND CI.PlanID LIKE @strPlanID
        
  OPEN csrRPPlan

  FETCH NEXT FROM csrRPPlan INTO @strNextPlanID, @strPlanName

  WHILE (@@FETCH_STATUS = 0)
    BEGIN
    
      PRINT '... Processing Plan "' + @strPlanName + '" '
      
      -- Update Plan's StartingDayOfWeek to match with System Setting in CFGResourcePlanning.
      
      UPDATE RPPlan SET StartingDayOfWeek = CFGResourcePlanning.StartingDayOfWeek
        FROM RPPlan, CFGResourcePlanning
        WHERE RPPlan.PlanID = @strNextPlanID
          AND CFGResourcePlanning.Company = dbo.GetActiveCompany()

      -- Re-make Calendar Intervals with the current setting of Start Day of Week.

      DECLARE csrAF CURSOR FOR
        SELECT DISTINCT AccordionFormatID FROM RPCalendarInterval, CFGResourcePlanning
          WHERE (PeriodScale = 'w' OR PeriodScale = 'b')
            AND CFGResourcePlanning.Company = dbo.GetActiveCompany()
            AND DATEPART(dw, StartDate) != StartingDayOfWeek
            AND PlanID = @strNextPlanID    

      OPEN csrAF

      FETCH NEXT FROM csrAF INTO @strAccordionFormatID

      WHILE (@@FETCH_STATUS = 0)
        BEGIN

          EXEC dbo.rpMakeCI @strAccordionFormatID
          FETCH NEXT FROM csrAF INTO @strAccordionFormatID

        END -- While

      CLOSE csrAF
      DEALLOCATE csrAF

      -- Check to see if there is any time-phased data.
      
      SELECT @intTPDCount = SUM(TPDCount) FROM 
        (SELECT COUNT(*) AS TPDCount FROM RPPlannedLabor WHERE PlanID  = @strNextPlanID
         UNION ALL
         SELECT COUNT(*) AS TPDCount FROM RPPlannedExpenses WHERE PlanID  = @strNextPlanID
         UNION ALL
         SELECT COUNT(*) AS TPDCount FROM RPPlannedConsultant WHERE PlanID  = @strNextPlanID
         UNION ALL
         SELECT COUNT(*) AS TPDCount FROM RPPlannedUnit WHERE PlanID  = @strNextPlanID
         UNION ALL
         SELECT COUNT(*) AS TPDCount FROM RPBaselineLabor WHERE PlanID  = @strNextPlanID
         UNION ALL
         SELECT COUNT(*) AS TPDCount FROM RPBaselineExpenses WHERE PlanID  = @strNextPlanID
         UNION ALL
         SELECT COUNT(*) AS TPDCount FROM RPBaselineConsultant WHERE PlanID  = @strNextPlanID
         UNION ALL
         SELECT COUNT(*) AS TPDCount FROM RPBaselineUnit WHERE PlanID  = @strNextPlanID
         UNION ALL
         SELECT COUNT(*) AS TPDCount FROM RPEVT WHERE PlanID  = @strNextPlanID
         UNION ALL
         SELECT COUNT(*) AS TPDCount FROM RPCompensationFee WHERE PlanID  = @strNextPlanID
         UNION ALL
         SELECT COUNT(*) AS TPDCount FROM RPConsultantFee WHERE PlanID  = @strNextPlanID
         UNION ALL
         SELECT COUNT(*) AS TPDCount FROM RPReimbAllowance WHERE PlanID  = @strNextPlanID
         ) AS X

      IF (@intTPDCount > 0)
        BEGIN
      
          -- Realign TPD.
          
          EXECUTE dbo.rpAlignTPD @strNextPlanID

          -- Recalculate Plan.

          EXEC dbo.rpDeleteSummaryTPD @strNextPlanID, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'
          EXEC dbo.rpConsolidateOverflowTPD @strNextPlanID
          EXEC dbo.rpCalcTPD @strNextPlanID, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'
          EXEC dbo.rpSumUpTPD @strNextPlanID, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'
          
        END -- If
      
      DROP Table #Decimals
      
      FETCH NEXT FROM csrRPPlan INTO @strNextPlanID, @strPlanName

    END -- While

  CLOSE csrRPPlan
  DEALLOCATE csrRPPlan
 
  SET NOCOUNT OFF

END -- rpAlignStartDayOfWeek
GO
