SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteProject] @WBS1 Nvarchar(30),@PhaseID Nvarchar(32), @TaskID Nvarchar(32)
AS
    declare @strSQL Nvarchar(max)
    declare @stmt nvarchar(max)

    SET @strSQL = ' WHERE WBS1=N''' + @WBS1 + '''' 
    If @PhaseID <> ' '
       begin
          SET @strSQL = @strSQL + ' AND WBS2 = N''' + @PhaseID + ''''
          If @TaskID <> ' '
             SET @strSQL = @strSQL + ' AND WBS3 = N''' + @TaskID + ''''
       end

    If @PhaseID = ' ' And @TaskID = ' '
       begin
          DELETE FROM PRClientAssoc WHERE WBS1=@WBS1
          DELETE FROM PRContactAssoc WHERE WBS1=@WBS1
          DELETE FROM Contracts WHERE WBS1=@WBS1
          DELETE FROM ContractCredit WHERE WBS1=@WBS1
          Update SF254Projects Set WBS1 = NULL WHERE WBS1=@WBS1
          Update SF255Projects Set WBS1 = NULL WHERE WBS1=@WBS1
       end

    SET @stmt = 'DELETE FROM PRChargeCompanies ' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM EMProjectAssoc ' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM ClendorProjectAssoc ' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM ClendorProjectAssocTemplate ' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM PRAwards ' + @strSQL    
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM PRDescriptions ' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM PRProjectCodes ' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM MktCampaignProjectAssoc ' + @strSQL
    EXEC sp_executesql @stmt

    Exec DeleteCustomGridTabData 'Projects',@WBS1,@PhaseID,@TaskID

    SET @stmt = 'DELETE FROM CustomProposalProjTeam ' + @strSQL    
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM CustomProposalProjectGraphics ' + @strSQL    
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM CustomProposalProject ' + @strSQL    
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM PRFileLinks ' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM ProjectCustomTabFields ' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM BTA' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM BTF' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM BT' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM ReportWBSList' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM PRSummarySub' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM PRSummaryMain' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM PRSummaryWBSList' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'UPDATE Activity SET WBS1 = null, WBS2 = null, WBS3 = null ' + @strSQL
    EXEC sp_executesql @stmt
    
    SET @stmt = 'UPDATE WorkflowActivity SET WBS1 = null, WBS2 = null, WBS3 = null ' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'UPDATE SF330PersonnelProjects SET WBS1 = null, WBS2 = null, WBS3 = null ' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'UPDATE SF330Projects SET WBS1 = null, WBS2 = null, WBS3 = null ' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM PRAdditionalData' + @strSQL
    EXEC sp_executesql @stmt

    SET @stmt = 'DELETE FROM ContractDetails' + @strSQL
    EXEC sp_executesql @stmt

	SET @stmt = 'DELETE FROM BTFCategory' + @strSQL
    EXEC sp_executesql @stmt

	SET @stmt = 'DELETE FROM ProjectCustomTabFields' + @strSQL
    EXEC sp_executesql @stmt

	SET @stmt = 'DELETE FROM LB' + @strSQL
    EXEC sp_executesql @stmt

	SET @stmt = 'DELETE FROM EB' + @strSQL
    EXEC sp_executesql @stmt

	SET @stmt = 'DELETE FROM PRMilestone' + @strSQL
    EXEC sp_executesql @stmt

	SET @stmt = 'DELETE FROM BTTaxCodes' + @strSQL
    EXEC sp_executesql @stmt

	SET @stmt = 'DELETE FROM PRChargeCompanies' + @strSQL
    EXEC sp_executesql @stmt

	SET @stmt = 'DELETE PNPlannedRevenueLabor FROM PNPlannedRevenueLabor
                   INNER JOIN PNTask ON PNPlannedRevenueLabor.PlanID = PNTask.PlanID AND PNPlannedRevenueLabor.TaskID = PNTask.TaskID' + @strSQL
    EXEC sp_executesql @stmt

	SET @stmt = 'DELETE RPPlannedRevenueLabor FROM RPPlannedRevenueLabor
                   INNER JOIN PNTask ON RPPlannedRevenueLabor.PlanID = PNTask.PlanID AND RPPlannedRevenueLabor.TaskID = PNTask.TaskID' + @strSQL
    EXEC sp_executesql @stmt

	SET @stmt = 'DELETE FROM PNTask' + @strSQL
    EXEC sp_executesql @stmt

	SET @stmt = 'DELETE FROM RPTask' + @strSQL
    EXEC sp_executesql @stmt
	
	DECLARE @PlanID VARCHAR(32)
	SELECT @PlanID = PlanID FROM PR WHERE WBS1 = @WBS1 AND WBS2 = ' '
    IF @PlanID > ''
    BEGIN
		IF @PhaseID = ' ' AND @TaskID = ' '
		BEGIN
			EXEC stRPDelPlan @PlanID
		END
		UPDATE PNTask SET ChildrenCount =  
        (SELECT COUNT(*) FROM PNTask P WHERE P.ParentOutlineNumber = PNTask.OutlineNumber  
        AND P.PlanID = PNTask.PlanID)  
        WHERE PlanID = @PlanID
    END

    --Delete PR
	SET @stmt = 'DELETE FROM PR' + @strSQL
    EXEC sp_executesql @stmt

    IF @PhaseID <> ' ' AND @TaskID = ' '
    BEGIN
        DELETE FROM PR Where WBS1 = @WBS1 AND WBS2 = @PhaseID AND WBS3 <> N' '
        SET @stmt = 'UPDATE PR SET SubLevel = ''N'' WHERE WBS1=N''' + @WBS1 + ''' AND WBS2 = N'' '' AND NOT Exists ( Select ''x'' FROM PR WHERE WBS1=N''' + @WBS1 + ''' AND WBS2 <> N'' '')'
        EXEC sp_executesql @stmt
    END
    IF @PhaseID <> ' ' AND @TaskID <> ' '
    BEGIN
        SET @stmt = 'UPDATE PR SET SubLevel = ''N'' WHERE WBS1=N''' + @WBS1 + ''' AND WBS2 = N''' + @PhaseID + ''' AND WBS3 = N'' '' AND NOT Exists ( Select ''x'' FROM PR WHERE WBS1=N''' + @WBS1 + ''' AND WBS2=N''' + @PhaseID + ''' AND WBS3 <> N'' '')'
        EXEC sp_executesql @stmt
    END
GO
