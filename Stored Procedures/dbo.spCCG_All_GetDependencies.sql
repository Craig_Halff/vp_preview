SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_All_GetDependencies] (@depLevel int)
AS BEGIN
	set nocount on
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	declare @ObjectName varchar(1000)

	declare @cur as cursor
	SET @cur = CURSOR FOR select ObjectName from #tableDep where DependencyLevel = @depLevel
	SET @depLevel = @depLevel + 1

	open @cur
	fetch next from @cur into @ObjectName
	while @@FETCH_STATUS = 0
	begin
		begin try
		insert into #tableDep (DependencyLevel, ObjectName)
			select distinct @depLevel, referenced_entity_name
				from sys.dm_sql_referenced_entities('dbo.'+replace(@ObjectName,'dbo.',''), 'OBJECT')
				where referenced_entity_name like '%CCG%' and referenced_entity_name not in (select ProcedureName from CCG_AppSPList)
					and referenced_entity_name not in (select replace(ObjectName,'dbo.','') from #tableDep)
		end try
		begin catch end catch

		fetch next from @cur into @ObjectName
	end
	if exists (select 'x' from #tableDep where DependencyLevel = @depLevel)
		exec spCCG_All_GetDependencies @depLevel

	close @cur
	deallocate @cur
END
GO
