SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPDelConsultant]
  @strRowID nvarchar(255),
  @bitCalledFromRM bit = 0
AS

BEGIN -- Procedure stRPDelConsultant

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to delete a Assignment row and its associated rows.
-- If bCalledFromRM = True then use RP* tables, else PN* tables.
-- If the Assignment has JTD then only delete planned labor rows, if the Assignment does not also delete the Assignment.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @_SectionSign nchar = NCHAR(167) -- N'§'
  DECLARE @_AccountVendorSign nchar = NCHAR(10132) -- N'➔'

  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strConsultantID varchar(32)
  DECLARE @strIDPrefix nvarchar(255)
  DECLARE @strAccount nvarchar(26)
  DECLARE @strVendor nvarchar(40)
  DECLARE @strWBS1 nvarchar(30)
  DECLARE @strWBS2 nvarchar(30) = ' '
  DECLARE @strWBS3 nvarchar(30) = ' '

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime

  DECLARE @intAccountVendorIndex int
  DECLARE @intRowIDSeperatorIndex int

  DECLARE @bitJTDExists bit
 
  DECLARE @bitPNBaselineExists bit

  -- Get JTD Date
   
  SELECT 
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE())))
    FROM CFGRMSettings

  -- Parse RowID

      SET @intAccountVendorIndex = CHARINDEX(@_AccountVendorSign,@strRowID)
      SET @intRowIDSeperatorIndex = CHARINDEX('|',@strRowID)
  
      IF @intAccountVendorIndex > 0
        BEGIN
          SET @strAccount = LEFT(@strRowID,@intAccountVendorIndex - 1)
        END

      SET @strVendor =
        CASE
          WHEN @intRowIDSeperatorIndex = @intAccountVendorIndex + 1
          THEN NULL
          ELSE SUBSTRING(@strRowID,@intAccountVendorIndex + 1,@intRowIDSeperatorIndex - @intAccountVendorIndex - 1)
        END
      SET @strTaskID = SUBSTRING(@strRowID,@intRowIDSeperatorIndex + 1,LEN(@strRowID) - @intRowIDSeperatorIndex)

  -- Get WBS123 from task
  
  SELECT 
    @strPlanID = PlanID,
    @strWBS1 = WBS1,
    @strWBS2 = ISNULL(WBS2, ' '),
    @strWBS3 = ISNULL(WBS3, ' ')
    FROM PNTask
    WHERE TaskID = @strTaskID

  -- Figure out if there are any JTD for this assignment

  SELECT 
    @bitJTDExists = 
	  CASE 
	    WHEN 
		  EXISTS(
		    SELECT
              'x'
			  FROM (
			  SELECT sum(LG.AmountProjectCurrency)  AS JTDCost,	sum(LG.BillExt) AS JTDBill    
              FROM LedgerAR AS LG
                INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
              WHERE LG.ProjectCost = 'Y' AND 
			        LG.TransDate <= @dtJTDDate AND 
					LG.WBS1 = @strWBS1 AND 
					(LG.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
                    (LG.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 + '%' END) AND 
					CA.Type IN (6, 8) AND 
					LG.Account = @strAccount AND 
					ISNULL(LG.Vendor,'x') = ISNULL(@strVendor,'x')

				UNION ALL
				SELECT sum(LG.AmountProjectCurrency)  AS JTDCost,	sum(LG.BillExt) AS JTDBill   
                FROM LedgerAP AS LG
                INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
                WHERE LG.ProjectCost = 'Y' AND 
			        LG.TransDate <= @dtJTDDate AND 
					LG.WBS1 = @strWBS1 AND 
					(LG.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
                    (LG.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 + '%' END) AND 
					CA.Type IN (6, 8) AND 
					LG.Account = @strAccount AND 
					ISNULL(LG.Vendor,'x') = ISNULL(@strVendor,'x')

				UNION ALL
				SELECT sum(LG.AmountProjectCurrency)  AS JTDCost,	sum(LG.BillExt) AS JTDBill   
                FROM LedgerEX AS LG
                INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
                WHERE LG.ProjectCost = 'Y' AND 
			        LG.TransDate <= @dtJTDDate AND 
					LG.WBS1 = @strWBS1 AND 
					(LG.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
                    (LG.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 + '%' END) AND 
					CA.Type IN (6, 8) AND 
					LG.Account = @strAccount AND 
					ISNULL(LG.Vendor,'x') = ISNULL(@strVendor,'x')

				UNION ALL
				SELECT sum(LG.AmountProjectCurrency)  AS JTDCost,	sum(LG.BillExt) AS JTDBill   
                FROM LedgerMISC AS LG
                INNER JOIN CA ON LG.Account = CA.Account -- Only pick up records in Ledger which has legitimate Account.
                WHERE LG.ProjectCost = 'Y' AND 
			        LG.TransDate <= @dtJTDDate AND 
					LG.WBS1 = @strWBS1 AND 
					(LG.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
                    (LG.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 + '%' END) AND 
					CA.Type IN (6, 8) AND 
					LG.Account = @strAccount AND 
					ISNULL(LG.Vendor,'x') = ISNULL(@strVendor,'x')

				UNION ALL
				SELECT sum(POC.AmountProjectCurrency)  AS JTDCost,	sum(POC.BillExt) AS JTDBill   
                FROM POCommitment AS POC
                 INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND POD.COPKey IS NULL)
                 INNER JOIN POMaster AS POM ON POD.MasterPKey = POM.MasterPKey
                 INNER JOIN CA ON POC.Account = CA.Account
                WHERE POM.OrderDate <= @dtJTDDate AND 
			        POC.WBS1 = @strWBS1 AND 
					(POC.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
                    (POC.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 + '%' END) AND 
					CA.Type IN (6, 8) AND 
					POC.Account = @strAccount AND 
					ISNULL(POM.Vendor,'x') = ISNULL(@strVendor,'x')
		  
		       UNION ALL
				SELECT sum(POC.AmountProjectCurrency)  AS JTDCost,	sum(POC.BillExt) AS JTDBill   
                FROM POCommitment AS POC
                 INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND POD.COPKey IS NOT NULL)
				 INNER JOIN POCOMaster AS POCOM ON POD.COPKey = POCOM.PKey
                 INNER JOIN POMaster AS POM ON POD.MasterPKey = POM.MasterPKey
                 INNER JOIN CA ON POC.Account = CA.Account
                WHERE POCOM.OrderDate <= @dtJTDDate AND 
			        POC.WBS1 = @strWBS1 AND 
					(POC.WBS2 LIKE CASE WHEN @strWBS2 = ' ' THEN '%' ELSE @strWBS2 END) AND 
                    (POC.WBS3 LIKE CASE WHEN @strWBS3 = ' ' THEN '%' ELSE @strWBS3 + '%' END) AND 
					CA.Type IN (6, 8) AND 
					POC.Account = @strAccount AND 
					ISNULL(POM.Vendor,'x') = ISNULL(@strVendor,'x')
		  ) AS tabLG
		  Having sum(tabLG.JTDCost) > 0 or sum(tabLG.JTDBill) > 0 )
        THEN 1 
		ELSE 0
      END

  SELECT @strConsultantID = A.ConsultantID
  FROM PNConsultant A
  WHERE A.TaskID = @strTaskID  AND
        A.Account = @strAccount AND 
        ISNULL(A.Vendor, 'x') = ISNULL(@strVendor, 'x')

  SELECT @bitPNBaselineExists = CASE WHEN EXISTS(
    SELECT TimePhaseID FROM PNBaselineConsultant BE WHERE BE.ConsultantID = @strConsultantID)					
    THEN 1 ELSE 0 END

--******************************
--			Do deletes
--******************************

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

  BEGIN TRANSACTION

  DELETE FROM PNPlannedConsultant WHERE ConsultantID = @strConsultantID

  IF (@bitJTDExists = 0 AND @bitPNBaselineExists = 0)
    BEGIN
      DELETE FROM PNConsultant WHERE ConsultantID = @strConsultantID
    END

  IF (@bitCalledFromRM = 1)
    BEGIN

      DELETE FROM RPPlannedConsultant WHERE ConsultantID = @strConsultantID

      IF (@bitJTDExists = 0 AND @bitPNBaselineExists = 0)
        BEGIN
          DELETE FROM RPConsultant WHERE ConsultantID = @strConsultantID
        END

    END

  -- Update VesionID.

  EXECUTE dbo.stRPUpdateVersionID @strPlanID, @bitCalledFromRM 

  SET NOCOUNT OFF

  COMMIT

END -- stRPDelConsultant
GO
