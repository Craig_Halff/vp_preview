SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[pmGetExpTPD]
  @strPlanID VARCHAR(32),
  @strETCDate VARCHAR(10), -- Date must be in format: 'yyyy-mm-dd'
  @strRetrievalMode VARCHAR(1) = '3',
  @strGetPlanned VARCHAR(1) = 'Y',
  @strGetBaseline VARCHAR(1) = 'Y',
  @strGetJTD VARCHAR(1) = 'Y'
AS

BEGIN -- Procedure pmGetExpTPD

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  
  DECLARE @startTime datetime
  DECLARE @totalTime int
  
  SET @startTime = GetDate()
  
  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime
  
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intECURevDecimals int
  
  DECLARE @intBaselineExpCount int
  DECLARE @intPlannedExpCount int
  DECLARE @intJTDExpCount int

  DECLARE @strCompany Nvarchar(14)
  DECLARE @strUserName Nvarchar(32)

  DECLARE @strOutlineNumber varchar(255)

  DECLARE @strTaskID varchar(32)
  DECLARE @strExpenseID varchar(32)
  
  DECLARE @strExpTab varchar(1)
  DECLARE @strUntTab varchar(1)

  DECLARE @strCommitmentFlg varchar(1)
  
  DECLARE @strHasExp varchar(1)
  
  -- Declare Temp tables.
           
  DECLARE @tabTopTask 
    TABLE (TaskID varchar(32) COLLATE database_default,
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(7) COLLATE database_default,
           WBS3 Nvarchar(7) COLLATE database_default,
           LaborCode Nvarchar(14) COLLATE database_default
           PRIMARY KEY(TaskID))

  DECLARE @tabJTDCalendar 
    TABLE (StartDate datetime,
           EndDate datetime,
           PeriodScale varchar(1) COLLATE database_default
           PRIMARY KEY(StartDate)) 

  DECLARE @tabLedger 
    TABLE (RowID varchar(32) COLLATE database_default,
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(7) COLLATE database_default,
           WBS3 Nvarchar(7) COLLATE database_default,
           Account Nvarchar(13) COLLATE database_default,
           Vendor Nvarchar(30) COLLATE database_default,
           StartDate datetime,
           EndDate datetime,
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4),
           PostedFlg smallint
           PRIMARY KEY(RowID, WBS1, WBS2, WBS3, StartDate))

  DECLARE @tabJTDExp 
    TABLE (RowID varchar(32) COLLATE database_default,
	       PlanID varchar(32) COLLATE database_default,
	       TaskID varchar(32) COLLATE database_default,
	       ExpenseID varchar(32) COLLATE database_default,
           StartDate datetime, 
           EndDate datetime, 
           PeriodCost decimal(19,4), 
           PeriodBill decimal(19,4), 
           PostedFlg smallint 
           PRIMARY KEY(RowID, PlanID, TaskID, StartDate, EndDate)) 

  -- Save Active Company string for use later.
  
  SET @strCompany = dbo.GetActiveCompany()
  SET @strUserName = dbo.GetVisionAuditUserName()
          
  -- Set JTD/ETC Dates.
  
  SET @dtETCDate = CONVERT(datetime, REPLACE(@strETCDate, '-', ''))
  SET @dtJTDDate = DATEADD(d, -1, @dtETCDate) 

  -- Get decimal settings.
  
  SELECT @intAmtCostDecimals = AmtCostDecimals,
         @intAmtBillDecimals = AmtBillDecimals,
         @intECURevDecimals = ECURevDecimals
    FROM dbo.RP$tabDecimals(@strPlanID)
    
  -- Get flags to determine which features are being used.
  
  SELECT
     @strExpTab = ExpTab,
     @strUntTab = UntTab
     FROM CFGResourcePlanning
     WHERE CFGResourcePlanning.Company = @strCompany
     
  -- Get Plan parameters to be used later.
  
  SELECT
    @strCommitmentFlg = CommitmentFlg
    FROM RPPlan WHERE PlanID = @strPlanID

  -- Initialize counting variables.
  
  SELECT 
    @intBaselineExpCount = 0,
    @intPlannedExpCount = 0,
    @intJTDExpCount = 0
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strGetJTD = 'Y' AND @strRetrievalMode != '1')
    BEGIN
 
      INSERT @tabTopTask(TaskID, WBS1, WBS2, WBS3, LaborCode)
        SELECT DISTINCT TaskID, WBS1, WBS2, WBS3, LaborCode
          FROM RPTask AS T
          INNER JOIN (SELECT MIN(OutlineLevel) AS OutlineLevel
              FROM RPTask AS XT INNER JOIN RPWBSLevelFormat AS F ON XT.PlanID = F.PlanID
              AND XT.WBSType = F.WBSType AND F.WBSMatch = 'Y'
              WHERE XT.PlanID = @strPlanID) AS ZT ON T.OutlineLevel = ZT.OutlineLevel
          WHERE T.PlanID = @strPlanID AND T.WBS1 IS NOT NULL AND T.WBS1 != '<none>'
      
	    IF (@@ROWCOUNT > 0)
	      BEGIN
	      
	        -- Compute data for JTD Calendar table with overflow periods before and after the regular calendar.

          IF (@strRetrievalMode = '3')
	          INSERT @tabJTDCalendar(StartDate, EndDate, PeriodScale)
		          SELECT StartDate, EndDate, PeriodScale FROM
			          (SELECT StartDate AS StartDate, EndDate AS EndDate, PeriodScale AS PeriodScale
                   FROM RPCalendarInterval WHERE PlanID = @strPlanID
                 UNION ALL
                 SELECT CAST('19300101' AS datetime) AS StartDate,
                   DATEADD(d, -1, MIN(StartDate)) AS EndDate,
                   'o' AS PeriodScale
                   FROM RPCalendarInterval WHERE PlanID = @strPlanID
                   GROUP BY PlanID
                 UNION ALL
                 SELECT DATEADD(d, 1, MAX(EndDate)) AS StartDate,
                   DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate))) AS EndDate,
                   'o' AS PeriodScale
                   FROM RPCalendarInterval WHERE PlanID = @strPlanID
                 ) AS CI
          ELSE
	          INSERT @tabJTDCalendar(StartDate, EndDate, PeriodScale)
		          SELECT MIN(StartDate), MAX(EndDate), 'o' FROM
			          (SELECT StartDate AS StartDate, EndDate AS EndDate
                   FROM RPCalendarInterval WHERE PlanID = @strPlanID
                 UNION ALL
                 SELECT CAST('19300101' AS datetime) AS StartDate,
                   DATEADD(d, -1, MIN(StartDate)) AS EndDate
                   FROM RPCalendarInterval WHERE PlanID = @strPlanID
                   GROUP BY PlanID
                 UNION ALL
                 SELECT DATEADD(d, 1, MAX(EndDate)) AS StartDate,
                   DATEADD(yyyy, 100, DATEADD(d, 1, MAX(EndDate))) AS EndDate
                   FROM RPCalendarInterval WHERE PlanID = @strPlanID
                 ) AS CI
            
          -- Initialize control variables to indicate whether we have JTD data of certain type.

	        SET @strHasExp = 'N'
    
        -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

          -- Save Expense JTD for this Plan into temp table.
          
  	        INSERT @tabLedger
		        SELECT
		          REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
			        WBS1,
			        WBS2,
			        WBS3,
			        Account,
			        Vendor,
			        StartDate, 
			        EndDate, 
			        SUM(ISNULL(PeriodCost, 0)) AS PeriodCost, 
			        SUM(ISNULL(PeriodBill, 0)) AS PeriodBill, 
			        1 AS PostedFlg
			        FROM
			          (SELECT
                   Ledger.WBS1 AS WBS1,
                   Ledger.WBS2 AS WBS2,
                   Ledger.WBS3 AS WBS3,
                   Ledger.Account AS Account,
                   Ledger.Vendor AS Vendor,
                   CI.StartDate AS StartDate,
                   CI.EndDate AS EndDate,
                   SUM(AmountProjectCurrency) AS PeriodCost,  
                   SUM(BillExt) AS PeriodBill
                   FROM LedgerAR AS Ledger
                     INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                       AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                       AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                       AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                       AND Ledger.TransType != (CASE WHEN @strUntTab = 'Y' THEN 'UN' ELSE '' END)
                     INNER JOIN @tabJTDCalendar AS CI ON Ledger.TransDate BETWEEN CI.StartDate AND CI.EndDate 
                     INNER JOIN CA ON Ledger.Account = CA.Account
                   WHERE CA.Type IN (5, 7)
			             GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, Ledger.Account, Ledger.Vendor, CI.StartDate, CI.EndDate
			           UNION ALL
			           SELECT
                   Ledger.WBS1 AS WBS1,
                   Ledger.WBS2 AS WBS2,
                   Ledger.WBS3 AS WBS3,
                   Ledger.Account AS Account,
                   Ledger.Vendor AS Vendor,
                   CI.StartDate AS StartDate,
                   CI.EndDate AS EndDate,
                   SUM(AmountProjectCurrency) AS PeriodCost,  
                   SUM(BillExt) AS PeriodBill
                   FROM LedgerAP AS Ledger
                     INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                       AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                       AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                       AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                       AND Ledger.TransType != (CASE WHEN @strUntTab = 'Y' THEN 'UN' ELSE '' END)
                     INNER JOIN @tabJTDCalendar AS CI ON Ledger.TransDate BETWEEN CI.StartDate AND CI.EndDate 
                     INNER JOIN CA ON Ledger.Account = CA.Account
                   WHERE CA.Type IN (5, 7)
			             GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, Ledger.Account, Ledger.Vendor, CI.StartDate, CI.EndDate
			           UNION ALL
			           SELECT
                   Ledger.WBS1 AS WBS1,
                   Ledger.WBS2 AS WBS2,
                   Ledger.WBS3 AS WBS3,
                   Ledger.Account AS Account,
                   Ledger.Vendor AS Vendor,
                   CI.StartDate AS StartDate,
                   CI.EndDate AS EndDate,
                   SUM(AmountProjectCurrency) AS PeriodCost,  
                   SUM(BillExt) AS PeriodBill
                   FROM LedgerEX AS Ledger
                     INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                       AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                       AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                       AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                       AND Ledger.TransType != (CASE WHEN @strUntTab = 'Y' THEN 'UN' ELSE '' END)
                     INNER JOIN @tabJTDCalendar AS CI ON Ledger.TransDate BETWEEN CI.StartDate AND CI.EndDate 
                     INNER JOIN CA ON Ledger.Account = CA.Account
                   WHERE CA.Type IN (5, 7)
			             GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, Ledger.Account, Ledger.Vendor, CI.StartDate, CI.EndDate
			           UNION ALL
			           SELECT
                   Ledger.WBS1 AS WBS1,
                   Ledger.WBS2 AS WBS2,
                   Ledger.WBS3 AS WBS3,
                   Ledger.Account AS Account,
                   Ledger.Vendor AS Vendor,
                   CI.StartDate AS StartDate,
                   CI.EndDate AS EndDate,
                   SUM(AmountProjectCurrency) AS PeriodCost,  
                   SUM(BillExt) AS PeriodBill
                   FROM LedgerMISC AS Ledger
                     INNER JOIN @tabTopTask AS T ON (Ledger.WBS1 = T.WBS1
                       AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate 
                       AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                       AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))
                       AND Ledger.TransType != (CASE WHEN @strUntTab = 'Y' THEN 'UN' ELSE '' END)
                     INNER JOIN @tabJTDCalendar AS CI ON Ledger.TransDate BETWEEN CI.StartDate AND CI.EndDate 
                     INNER JOIN CA ON Ledger.Account = CA.Account
                   WHERE CA.Type IN (5, 7)
			             GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, Ledger.Account, Ledger.Vendor, 
			               CI.StartDate, CI.EndDate
			          ) AS X
			        GROUP BY WBS1, WBS2, WBS3, Account, Vendor, StartDate, EndDate

	        IF (@@ROWCOUNT > 0) SET @strHasExp = 'Y'

	        -- Save Unposted Expense into temp table.

          IF (@strCommitmentFlg = 'Y')
            BEGIN
            
  	          INSERT @tabLedger
		            SELECT
		              REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
			            WBS1,
			            WBS2,
			            WBS3,
			            Account,
			            Vendor,
			            StartDate, 
			            EndDate, 
			            SUM(ISNULL(PeriodCost, 0)) AS PeriodCost, 
			            SUM(ISNULL(PeriodBill, 0)) AS PeriodBill, 
			            -1 AS PostedFlg
			            FROM
			              (SELECT -- POC with no Change Orders.
                       POC.WBS1 AS WBS1,
                       POC.WBS2 AS WBS2,
                       POC.WBS3 AS WBS3,
                       POC.Account AS Account,
                       POM.Vendor AS Vendor,
		                   CI.StartDate AS StartDate,
			                 CI.EndDate AS EndDate,
                       SUM(AmountProjectCurrency) AS PeriodCost,  
                       SUM(BillExt) AS PeriodBill
                       FROM POCommitment AS POC 
                         INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
                         INNER JOIN POMaster AS POM ON POD.MasterPKey = POM.MasterPKey
                         INNER JOIN @tabTopTask AS T ON POC.WBS1 = T.WBS1
                           AND POC.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                           AND POC.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                        INNER JOIN @tabJTDCalendar AS CI ON POM.OrderDate BETWEEN CI.StartDate AND CI.EndDate
                        INNER JOIN CA ON POC.Account = CA.Account
                       WHERE POM.OrderDate <= @dtJTDDate
                         AND AmountProjectCurrency != 0 AND BillExt != 0
                         AND CA.Type IN (5, 7) 
 			                 GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor, CI.StartDate, CI.EndDate
			               UNION ALL
			               SELECT -- POC with Change Orders.
                       POC.WBS1 AS WBS1,
                       POC.WBS2 AS WBS2,
                       POC.WBS3 AS WBS3,
                       POC.Account AS Account,
                       POM.Vendor AS Vendor,
		                   CI.StartDate AS StartDate,
			                 CI.EndDate AS EndDate,
                       SUM(AmountProjectCurrency) AS PeriodCost,  
                       SUM(BillExt) AS PeriodBill
                       FROM POCommitment AS POC 
                         INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
                         INNER JOIN POCOMaster AS POCOM ON POD.COPKey = POCOM.PKey
                         INNER JOIN POMaster AS POM ON POCOM.MasterPKey = POM.MasterPKey
                         INNER JOIN @tabTopTask AS T ON POC.WBS1 = T.WBS1
                           AND POC.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                           AND POC.WBS3 LIKE (ISNULL(T.WBS3, '%'))
                        INNER JOIN @tabJTDCalendar AS CI ON POCOM.OrderDate BETWEEN CI.StartDate AND CI.EndDate
                        INNER JOIN CA ON POC.Account = CA.Account
                       WHERE POCOM.OrderDate <= @dtJTDDate
                         AND AmountProjectCurrency != 0 AND BillExt != 0
                         AND CA.Type IN (5, 7) 
			                 GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor, CI.StartDate, CI.EndDate
                    ) AS X
			            GROUP BY WBS1, WBS2, WBS3, Account, Vendor, StartDate, EndDate
                   
              IF (@@ROWCOUNT > 0) SET @strHasExp = 'Y'
              
            END -- If-Then (@strUnposted = 'Y')

        -->>>>>

          IF (@strHasExp = 'Y')
            BEGIN
          
              IF (@strExpTab = 'Y')
                BEGIN
                
                  INSERT @tabJTDExp
                    (RowID,
                     PlanID,
                     TaskID,  
                     ExpenseID,  
                     StartDate,  
                     EndDate,  
                     PeriodCost,  
                     PeriodBill,  
                     PostedFlg)
                    SELECT
                      REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
                      @strPlanID AS PlanID,
                      TaskID,  
                      ExpenseID,  
                      StartDate,  
                      EndDate,  
                      SUM(ISNULL(PeriodCost, 0)) AS PeriodCost,  
                      SUM(ISNULL(PeriodBill, 0)) AS PeriodBill,  
                      SIGN(MIN(PostedFlg)) AS PostedFlg
                      FROM
                        (SELECT E.ExpenseID,  
                           E.TaskID,  
                           Ledger.StartDate,  
                           Ledger.EndDate,  
                           SUM(PeriodCost) AS PeriodCost,  
                           SUM(PeriodBill) AS PeriodBill,  
                           PostedFlg
                           FROM @tabLedger AS Ledger
						   INNER JOIN RPTask AS T ON Ledger.WBS1 = T.WBS1
                          AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                               AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))
						    
                          INNER JOIN RPWBSLevelFormat AS F ON T.PlanID = F.PlanID AND T.WBSType = F.WBSType
                              AND F.FmtLevel != 0 AND F.WBSMatch = 'Y' AND F.WBSType != 'LBCD'
                             INNER JOIN RPExpense AS E ON (Ledger.WBS1 = E.WBS1 AND E.TaskID = T.TaskID
                               AND Ledger.WBS2 LIKE (ISNULL(E.WBS2, '%'))  
                               AND Ledger.WBS3 LIKE (ISNULL(E.WBS3, '%'))
                               AND Ledger.Account = E.Account
                               AND (ISNULL(Ledger.Vendor, '%')) LIKE (ISNULL(E.Vendor, '%')))  
                             INNER JOIN (SELECT MIN(ExpenseID) AS ExpenseID FROM RPExpense  
                               WHERE PlanID = @strPlanID  
                               GROUP BY TaskID, Account, Vendor) AS E1 ON E.ExpenseID = E1.ExpenseID
                           WHERE E.PlanID = @strPlanID  
                           GROUP BY E.ExpenseID, E.TaskID, Ledger.StartDate, Ledger.EndDate, PostedFlg
                         UNION ALL -- For Expense rows with Vendor IS NULL, back out Vendors that have already been summed up.
                         SELECT E.ExpenseID,  
                           E.TaskID,  
                           Ledger.StartDate,  
                           Ledger.EndDate,  
                           -SUM(PeriodCost) AS PeriodCost,  
                           -SUM(PeriodBill) AS PeriodBill,  
                           PostedFlg
                           FROM @tabLedger AS Ledger
						    INNER JOIN RPTask AS T ON Ledger.WBS1 = T.WBS1
                           AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                               AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%'))
						    
                          INNER JOIN RPWBSLevelFormat AS F ON T.PlanID = F.PlanID AND T.WBSType = F.WBSType
                              AND F.FmtLevel != 0 AND F.WBSMatch = 'Y' AND F.WBSType != 'LBCD'
                          INNER JOIN 
                               (SELECT V0.TaskID, V0.ExpenseID, VX.WBS1, VX.WBS2, VX.WBS3, VX.Account, VX.Vendor FROM
                                  (SELECT TaskID, ExpenseID, WBS1, WBS2, WBS3, Account FROM RPExpense WHERE PlanID = @strPlanID AND Vendor IS NULL) AS V0
                                  INNER JOIN
                                  (SELECT TaskID, WBS1, WBS2, WBS3, Account, Vendor FROM RPExpense WHERE PlanID = @strPlanID AND Vendor IS NOT NULL) AS VX
                                    ON V0.TaskID = VX.TaskID AND V0.WBS1 = VX.WBS1 
                                      AND (ISNULL(V0.WBS2, '%')) LIKE (ISNULL(VX.WBS2, '%')) 
                                      AND (ISNULL(V0.WBS3, '%')) LIKE (ISNULL(VX.WBS3, '%')) AND V0.Account = VX.Account) AS E ON (Ledger.WBS1 = E.WBS1
							   AND E.TaskID = T.TaskID
                               AND Ledger.WBS2 LIKE (ISNULL(E.WBS2, '%'))  
                               AND Ledger.WBS3 LIKE (ISNULL(E.WBS3, '%'))
                               AND Ledger.Account = E.Account
                               AND (ISNULL(Ledger.Vendor, '%')) LIKE (ISNULL(E.Vendor, '%')))  
                           GROUP BY E.ExpenseID, E.TaskID, Ledger.StartDate, Ledger.EndDate, PostedFlg                           
                         UNION ALL
                         SELECT NULL AS ExpenseID,  
                           T.TaskID,  
                           Ledger.StartDate,  
                           Ledger.EndDate,  
                           SUM(PeriodCost) AS PeriodCost,  
                           SUM(PeriodBill) AS PeriodBill,  
                           PostedFlg
                           FROM @tabLedger AS Ledger
                             INNER JOIN RPTask AS T ON (Ledger.WBS1 = T.WBS1
                               AND Ledger.WBS2 LIKE (ISNULL(T.WBS2, '%'))  
                               AND Ledger.WBS3 LIKE (ISNULL(T.WBS3, '%')))  
                               AND T.LaborCode IS NULL
                             INNER JOIN RPWBSLevelFormat AS F ON T.PlanID = F.PlanID AND T.WBSType = F.WBSType
                               AND F.FmtLevel != 0 AND F.WBSMatch = 'Y' AND F.WBSType != 'LBCD'
                           WHERE T.PlanID = @strPlanID
                           GROUP BY T.TaskID, Ledger.StartDate, Ledger.EndDate, PostedFlg) AS X
                      GROUP BY TaskID, ExpenseID, StartDate, EndDate
                      HAVING SUM(ISNULL(PeriodCost, 0)) != 0 OR SUM(ISNULL(PeriodBill, 0)) != 0
                      
                END -- If-Then (@strExpTab = 'Y')
                                
            END -- If-Then (@strHasExp = 'Y')

	      END -- If-Then (@@ROWCOUNT > 0)
    
    END -- (@strGetJTD = 'Y' AND @strRetrievalMode != '1')
    
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Check to see if there is any time-phased data.
  -- We don't want to do any work if the plan is empty.
  
  IF (@strRetrievalMode != '1')
    BEGIN
      IF (@strGetJTD = 'Y') SET @intJTDExpCount = CASE WHEN EXISTS (SELECT 'X' FROM @tabJTDExp) THEN 1 ELSE 0 END
    END
    
  IF (@strRetrievalMode = '3')
    BEGIN
      IF (@strGetBaseline = 'Y') SET @intBaselineExpCount = CASE WHEN EXISTS (SELECT 'X' FROM RPBaselineExpenses WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
    END

  IF (@strGetPlanned = 'Y') SET @intPlannedExpCount = CASE WHEN EXISTS (SELECT 'X' FROM RPPlannedExpenses WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        
  IF (@strExpTab = 'Y')
    BEGIN 

      -->>> Planned Expense TPD.
      
      IF (@intPlannedExpCount > 0)
        BEGIN
        
          SELECT DISTINCT 1 AS Tag, NULL AS Parent,
            E.ExpenseID AS [PE!1!P],
            NULL AS [R!2!I],
            NULL AS [R!2!SD],
            NULL AS [R!2!ED],
            NULL AS [R!2!!Element]
            FROM RPExpense AS E -- Need INNER JOIN to weed out Expenses w/o TPD...
              INNER JOIN RPPlannedExpenses AS TPD ON E.PlanID = TPD.PlanID AND E.TaskID = TPD.TaskID AND E.ExpenseID = TPD.ExpenseID
            WHERE E.PlanID = @strPlanID
          UNION ALL
          SELECT 2 AS Tag, 1 AS Parent,
            TPD.ExpenseID AS [PE!1!P],
            TimePhaseID AS [R!2!I],
            CONVERT(INT, TPD.StartDate) AS [R!2!SD],
            CONVERT(INT, TPD.EndDate) AS [R!2!ED],
            LTRIM(ISNULL(STR(PeriodCost, 19, @intAmtCostDecimals), '')) + '|' +  
            LTRIM(ISNULL(STR(PeriodBill, 19, @intAmtBillDecimals), '')) AS [R!2!!Element]
            FROM RPPlannedExpenses AS TPD
            WHERE TPD.PlanID = @strPlanID AND TPD.ExpenseID IS NOT NULL
          UNION ALL -- ONLY Leaf Task Expense TPD.
          SELECT DISTINCT 1 AS Tag, NULL AS Parent,
            T.TaskID AS [TPE!1!P],
            NULL AS [R!2!I],
            NULL AS [R!2!SD],
            NULL AS [R!2!ED],
            NULL AS [R!2!!Element]
            FROM RPTask AS T -- Need INNER JOIN to weed out Tasks w/o TPD...
              INNER JOIN RPPlannedExpenses AS TPD ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID AND TPD.ExpenseID IS NULL
            WHERE T.PlanID = @strPlanID AND T.ChildrenCount = 0 AND T.ExpParentState = 'N'
          UNION ALL
          SELECT 2 AS Tag, 1 AS Parent,
            T.TaskID AS [PE!1!P], 
            TimePhaseID AS [R!2!I],
            CONVERT(INT, TPD.StartDate) AS [R!2!SD],
            CONVERT(INT, TPD.EndDate) AS [R!2!ED],
            LTRIM(ISNULL(STR(PeriodCost, 19, @intAmtCostDecimals), '')) + '|' +  
            LTRIM(ISNULL(STR(PeriodBill, 19, @intAmtBillDecimals), '')) AS [R!2!!Element]
            FROM  RPTask AS T
              INNER JOIN RPPlannedExpenses AS TPD ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID AND TPD.ExpenseID IS NULL
            WHERE T.PlanID = @strPlanID AND T.ChildrenCount = 0 AND T.ExpParentState = 'N'
            ORDER BY [PE!1!P], Tag, [R!2!SD]
            FOR XML EXPLICIT
        
        END --IF (@intPlannedExpCount > 0)
        
      -->>> Get Actual Expense TPD (only when Retrieval Mode is not "Planned Only").
      
      IF (@strRetrievalMode != '1' AND @intJTDExpCount > 0)
        BEGIN
        
          SELECT DISTINCT 1 AS Tag, NULL AS Parent,
            ISNULL(TPD.ExpenseID, T.TaskID) AS [JE!1!P],
            NULL AS [R!2!SD],
            NULL AS [R!2!!Element]
            FROM RPTask AS T -- Need INNER JOIN to weed out Tasks w/o TPD...
              INNER JOIN @tabJTDExp AS TPD ON T.TaskID = TPD.TaskID
            WHERE T.PlanID = @strPlanID
          UNION ALL
          SELECT 2 AS Tag, 1 AS Parent,
            ISNULL(TPD.ExpenseID, TPD.TaskID) AS [JE!1!P], 
            CONVERT(INT, TPD.StartDate) AS [R!2!SD], 
            LTRIM(ISNULL(STR(TPD.PeriodCost, 19, 2), '')) + '|' +  
            LTRIM(ISNULL(STR(TPD.PeriodBill, 19, 2), '')) + '|' + 
            LTRIM(STR(ISNULL(TPD.PostedFlg, 1))) AS [R!2!!Element]
            FROM  @tabJTDExp AS TPD
            ORDER BY [JE!1!P], Tag, [R!2!SD]
            FOR XML EXPLICIT

        END --IF (@strRetrievalMode != '1' AND @intJTDExpCount > 0)
        
      -->>> Get Baseline Expenses TPD (only when needed).

      IF (@strRetrievalMode = '3' AND @intBaselineExpCount > 0)
        BEGIN

          SELECT DISTINCT 1 AS Tag, NULL AS Parent,
            ISNULL(TPD.ExpenseID, T.TaskID) AS [BE!1!P],
            NULL AS [R!2!SD],
            NULL AS [R!2!!Element]
            FROM RPTask AS T -- Need INNER JOIN to weed out Tasks w/o TPD...
              INNER JOIN RPBaselineExpenses AS TPD ON T.PlanID = TPD.PlanID AND T.TaskID = TPD.TaskID
            WHERE T.PlanID = @strPlanID
          UNION ALL
          SELECT 2 AS Tag, 1 AS Parent,
            ISNULL(TPD.ExpenseID, TPD.TaskID) AS [BE!1!P],
            CONVERT(INT, TPD.StartDate) AS [R!2!SD], 
            LTRIM(ISNULL(STR(TPD.PeriodCost, 19, @intAmtCostDecimals), '')) + '|' +  
            LTRIM(ISNULL(STR(TPD.PeriodBill, 19, @intAmtBillDecimals), '')) AS [R!2!!Element]
            FROM  RPBaselineExpenses AS TPD
            WHERE TPD.PlanID = @strPlanID
            ORDER BY [BE!1!P], Tag, [R!2!SD]
            FOR XML EXPLICIT
            
        END --IF (@strRetrievalMode = '3' AND @intBaselineExpCount > 0)

    END --IF (@strExpTab = 'Y')

  SET @totalTime = DATEDIFF(ms,@startTime,GetDate())

  Select 1 Tag,null Parent,@totalTime [time!1] for xml explicit

  SET NOCOUNT OFF

END -- pmGetExpTPD
GO
