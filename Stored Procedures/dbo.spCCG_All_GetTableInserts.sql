SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_All_GetTableInserts] (@tableName Nvarchar(200), @inserts varchar(max) output, @singleInsert char(1) = 'Y', @checkExistence char(1) = 'N')
AS BEGIN
	set nocount on
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	declare @tableCC_Cols table (RowNum int, ColName varchar(1000), DataType varchar(200),
		IsNullable varchar(10), MaxLen int, IsIdentityColumn varchar(10), IsUnique int, ColDefault varchar(2000))
	insert into @tableCC_Cols
		select row_number() over (Order by IsIdentityColumn desc, ColName asc) as RowNum, * from dbo.[fnCCG_All_GetTableColumns](@tableName)

	declare @i int = 1
	declare @cols int = (select count('x') from @tableCC_Cols)
	declare @hasIdentity char(1) = (case when exists (select 'x' from @tableCC_Cols where IsIdentityColumn = 1) then 'Y' else 'N' end)
	declare @insertSql varchar(max) = '', @values varchar(max) = ''
	declare @hasUniqueCond char(1) = (case when @checkExistence = 'Y' and exists (select 'x' from @tableCC_Cols where IsUnique = 1) then 'Y' else 'N' end)
	declare @checkSql varchar(2000) = ''

	set @inserts = ''
	if @hasUniqueCond = 'N' set @checkExistence = 'N'
	if @checkExistence = 'Y' set @checkSql = 'if not exists (select ''x'' from '+@tableName+' where [[[-REPLACE-]]]) '+char(13)+char(10)+'	'

	while exists (select 'x' from #tableCCs where RowNum = @i)
	begin
		declare @j int = 1
		declare @colName varchar(200) = '', @type varchar(200), @unique int, @checkSqlSpecific varchar(max)
		set @checkSqlSpecific = ''

		if @singleInsert = 'N' and @checkExistence = 'Y' set @insertSql += @checkSql
		if @singleInsert = 'N' or @i = 1 set @insertSql += 'insert into '+@tableName+' ('
		set @values = ''

		select @colName = ColName, @type = DataType, @unique = IsUnique from @tableCC_Cols where RowNum = @j
		while isnull(@colName, '') <> ''
		begin
			declare @t varchar(max)
			declare @sql nvarchar(2000) = 'select @t = ['+@colName+'] from #tableCCs where RowNum = '+cast(@i as varchar)
			exec sp_executesql @sql, N'@t varchar(max) output', @t output

			if @j > 1 begin
				if @singleInsert = 'N' or @i = 1 set @insertSql = @insertSql + ', '
				set @values = @values + ', '
			end
			if @singleInsert = 'N' or @i = 1 set @insertSql = @insertSql + '['+@colName+']'
			if @t is null
				set @values = @values + 'NULL'
			else
				set @values = @values + (case when @type in ('varchar', 'nvarchar', 'char', 'nchar', 'text', 'datetime') then ''''+replace(@t, '''', '''''')+'''' else @t end)

			if @checkExistence = 'Y' and @unique = 1
			begin
				if @checkSqlSpecific <> '' set @checkSqlSpecific += ' and '
				set @checkSqlSpecific += @colName + ' = ' + (case when @type in ('varchar', 'nvarchar', 'char', 'nchar', 'text', 'datetime') then ''''+replace(@t, '''', '''''')+'''' else @t end)
			end

			set @j = @j + 1
			set @colName = ''
			select @colName = ColName, @type = DataType, @unique = IsUnique from @tableCC_Cols where RowNum = @j
		end
		if @checkExistence = 'Y' set @insertSql = replace(@insertSql, '[[[-REPLACE-]]]', @checkSqlSpecific)

		if @singleInsert = 'N'
			set @insertSql = @insertSql + ') values (' + @values + ')' + char(13)+char(10)
		else
		begin
			if @i = 1 set @insertSql = @insertSql + ') values (' + @values + ')'
			else set @insertSql = @insertSql + ',
				(' + @values + ')'
		end
		set @i = @i + 1
	end

	if @hasIdentity = 'N' or @singleInsert = 'N' set @inserts = @insertSql
	else if isnull(@insertSql, '') <> '' set @inserts = '
set identity_insert dbo.'+@tableName+' on
' + @insertSql + '
set identity_insert dbo.'+@tableName+' off '
END
GO
