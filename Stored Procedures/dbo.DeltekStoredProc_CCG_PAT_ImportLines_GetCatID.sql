SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_ImportLines_GetCatID]
AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_ImportLines_GetCatID]
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SET NOCOUNT ON;
	SELECT convert(Nvarchar(10), category) + N'|' + ekgroup + N'|' + description
		FROM cfgekcategory;
END;

GO
