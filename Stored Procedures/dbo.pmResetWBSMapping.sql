SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[pmResetWBSMapping]
  @strPlanID VARCHAR(32)
AS

BEGIN -- Procedure pmResetWBSMapping

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to reset WBS Mapping information.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  
  DECLARE @iWBS1Lv INTEGER
  DECLARE @iWBS2Lv INTEGER
  DECLARE @iWBS3Lv INTEGER
  DECLARE @iLBCDLv INTEGER
  DECLARE @MaxInt INTEGER
  DECLARE @intExp INTEGER
  DECLARE @intCon INTEGER
  DECLARE @intUnt INTEGER
  DECLARE @intExpTPD INTEGER
  DECLARE @intConTPD INTEGER
  DECLARE @intUntTPD INTEGER
  
  DECLARE @strExpTab VARCHAR(1)
  DECLARE @strConTab VARCHAR(1)
  DECLARE @strUntTab VARCHAR(1)
  
  DECLARE @strActiveCompany Nvarchar(14)
  
  DECLARE @UpdatedExp 
    TABLE (ExpenseID VARCHAR(32) COLLATE database_default
           PRIMARY KEY(ExpenseID))

  DECLARE @UpdatedCon 
    TABLE (ConsultantID VARCHAR(32) COLLATE database_default
           PRIMARY KEY(ConsultantID))

  DECLARE @UpdatedUnt 
    TABLE (UnitID VARCHAR(32) COLLATE database_default
           PRIMARY KEY(UnitID))
           
  DECLARE @tabTask 
    TABLE (OutlineNumber VARCHAR(255) COLLATE database_default,
           OutlineLevel INTEGER,
           WBSType VARCHAR(4) COLLATE database_default,
           WBS1 Nvarchar(30) COLLATE database_default,
           WBS2 Nvarchar(7) COLLATE database_default,
           WBS3 Nvarchar(7) COLLATE database_default,
           LaborCode Nvarchar(14) COLLATE database_default
           PRIMARY KEY(OutlineNumber, OutlineLevel, WBSType))

  DECLARE @tabWBS1 
    TABLE (OutlineNumber VARCHAR(255) COLLATE database_default,
           WBS1 Nvarchar(30) COLLATE database_default
           PRIMARY KEY(OutlineNumber))
           
  DECLARE @tabWBS2 
    TABLE (OutlineNumber VARCHAR(255) COLLATE database_default,
           WBS2 Nvarchar(7) COLLATE database_default
           PRIMARY KEY(OutlineNumber))

  DECLARE @tabWBS3 
    TABLE (OutlineNumber VARCHAR(255) COLLATE database_default,
           WBS3 Nvarchar(7) COLLATE database_default
           PRIMARY KEY(OutlineNumber))

  DECLARE @tabLBCD 
    TABLE (OutlineNumber VARCHAR(255) COLLATE database_default,
           LaborCode Nvarchar(14) COLLATE database_default
           PRIMARY KEY(OutlineNumber))

  SET @MaxInt = CONVERT(INTEGER, 0x7FFFFFFF)
  
  SET @strActiveCompany = dbo.GetActiveCompany()
  
  SELECT @iWBS1Lv = CASE WHEN FmtLevel > 0 THEN FmtLevel - 1 ELSE @MaxInt END FROM RPWBSLevelFormat WHERE PlanID = @strPlanID AND WBSType = 'WBS1'
  SELECT @iWBS2Lv = CASE WHEN FmtLevel > 0 THEN FmtLevel - 1 ELSE @MaxInt END FROM RPWBSLevelFormat WHERE PlanID = @strPlanID AND WBSType = 'WBS2'
  SELECT @iWBS3Lv = CASE WHEN FmtLevel > 0 THEN FmtLevel - 1 ELSE @MaxInt END FROM RPWBSLevelFormat WHERE PlanID = @strPlanID AND WBSType = 'WBS3'
  SELECT @iLBCDLv = CASE WHEN FmtLevel > 0 THEN FmtLevel - 1 ELSE @MaxInt END FROM RPWBSLevelFormat WHERE PlanID = @strPlanID AND WBSType = 'LBCD'

  -- 2011-02-09, Luong Vu
  -- Coming out of the above statements, the value of @iWBS1Lv, @iWBS2Lv, @iWBS3Lv, @iLBCDLv could be NULL 
  -- when RPWBSLevelFormat does not have a row for the particular Plan. 
  -- The following statements compensate for that condition.

  SELECT @iWBS1Lv = ISNULL(@iWBS1Lv, @MaxInt)
  SELECT @iWBS2Lv = ISNULL(@iWBS2Lv, @MaxInt)
  SELECT @iWBS3Lv = ISNULL(@iWBS3Lv, @MaxInt)
  SELECT @iLBCDLv = ISNULL(@iLBCDLv, @MaxInt)
    
  -- Get flags to determine which features are being used.
  
  SELECT
     @strExpTab = ExpTab,
     @strConTab = ConTab,
     @strUntTab = UntTab
     FROM CFGResourcePlanning
     WHERE CFGResourcePlanning.Company = @strActiveCompany
     
  -- Determine the existence of Expense/Consultant/Unit and corresponding TPD rows.
  
  IF (@strExpTab = 'Y') SET @intExp = CASE WHEN EXISTS (SELECT 'X' FROM RPExpense WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
  IF (@strConTab = 'Y') SET @intCon = CASE WHEN EXISTS (SELECT 'X' FROM RPConsultant WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
  IF (@strUntTab = 'Y') SET @intUnt = CASE WHEN EXISTS (SELECT 'X' FROM RPUnit WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END 

  IF (@strExpTab = 'Y') SET @intExpTPD = CASE WHEN EXISTS (SELECT 'X' FROM RPPlannedExpenses WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
  IF (@strConTab = 'Y') SET @intConTPD = CASE WHEN EXISTS (SELECT 'X' FROM RPPlannedConsultant WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
  IF (@strUntTab = 'Y') SET @intUntTPD = CASE WHEN EXISTS (SELECT 'X' FROM RPPlannedUnit WHERE PlanID = @strPlanID) THEN 1 ELSE 0 END
  
  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabTask(OutlineNumber, OutlineLevel, WBSType, WBS1, WBS2, WBS3, LaborCode)
    SELECT OutlineNumber, OutlineLevel, ISNULL(WBSType, ''), WBS1, WBS2, WBS3, LaborCode
      FROM RPTask WHERE PlanID = @strPlanID
      
  -->>> WBS1

  INSERT @tabWBS1(OutlineNumber, WBS1)
    SELECT Z1.OutlineNumber, T.WBS1 FROM
      (SELECT OutlineLevel, OutlineNumber, MIN(WBS1_ON) AS WBS1_ON FROM
        (SELECT T1.OutlineLevel, T1.OutlineNumber AS OutlineNumber, T2.OutlineNumber AS WBS1_ON
           FROM @tabTask AS T1 INNER JOIN @tabTask AS T2 ON T2.OutlineNumber LIKE (T1.OutlineNumber + '%') AND T2.WBSType = 'WBS1'
         UNION
         SELECT T1.OutlineLevel, T1.OutlineNumber AS OutlineNumber, T2.OutlineNumber AS WBS1_ON
           FROM @tabTask AS T1 INNER JOIN @tabTask AS T2 ON T1.OutlineNumber LIKE (T2.OutlineNumber + '%') AND T2.WBSType = 'WBS1'
         ) AS X1
        GROUP BY OutlineNumber, OutlineLevel) AS Z1
      INNER JOIN @tabTask AS T ON Z1.WBS1_ON = T.OutlineNumber
      WHERE Z1.OutlineLevel = @iWBS1Lv
      
  -->>> WBS2

  INSERT @tabWBS2(OutlineNumber, WBS2)
    SELECT Z1.OutlineNumber, T.WBS2 FROM
      (SELECT OutlineLevel, OutlineNumber, MIN(WBS2_ON) AS WBS2_ON FROM
        (SELECT T1.OutlineLevel, T1.OutlineNumber AS OutlineNumber, T2.OutlineNumber AS WBS2_ON
           FROM @tabTask AS T1 INNER JOIN @tabTask AS T2 ON T2.OutlineNumber LIKE (T1.OutlineNumber + '%') AND T2.WBSType = 'WBS2'
         UNION
         SELECT T1.OutlineLevel, T1.OutlineNumber AS OutlineNumber, T2.OutlineNumber AS WBS2_ON
           FROM @tabTask AS T1 INNER JOIN @tabTask AS T2 ON T1.OutlineNumber LIKE (T2.OutlineNumber + '%') AND T2.WBSType = 'WBS2'
         ) AS X1
        GROUP BY OutlineNumber, OutlineLevel) AS Z1
      INNER JOIN @tabTask AS T ON Z1.WBS2_ON = T.OutlineNumber
      WHERE Z1.OutlineLevel = @iWBS2Lv

  -->>> WBS3

  INSERT @tabWBS3(OutlineNumber, WBS3)
    SELECT Z1.OutlineNumber, T.WBS3 FROM
      (SELECT OutlineLevel, OutlineNumber, MIN(WBS3_ON) AS WBS3_ON FROM
        (SELECT T1.OutlineLevel, T1.OutlineNumber AS OutlineNumber, T2.OutlineNumber AS WBS3_ON
           FROM @tabTask AS T1 INNER JOIN @tabTask AS T2 ON T2.OutlineNumber LIKE (T1.OutlineNumber + '%') AND T2.WBSType = 'WBS3'
         UNION
         SELECT T1.OutlineLevel, T1.OutlineNumber AS OutlineNumber, T2.OutlineNumber AS WBS3_ON
           FROM @tabTask AS T1 INNER JOIN @tabTask AS T2 ON T1.OutlineNumber LIKE (T2.OutlineNumber + '%') AND T2.WBSType = 'WBS3'
         ) AS X1
        GROUP BY OutlineNumber, OutlineLevel) AS Z1
      INNER JOIN @tabTask AS T ON Z1.WBS3_ON = T.OutlineNumber
      WHERE Z1.OutlineLevel = @iWBS3Lv

  -->>> LBCD

  INSERT @tabLBCD(OutlineNumber, LaborCode)
    SELECT Z1.OutlineNumber, T.LaborCode FROM
      (SELECT OutlineLevel, OutlineNumber, MIN(LBCD_ON) AS LBCD_ON FROM
        (SELECT T1.OutlineLevel, T1.OutlineNumber AS OutlineNumber, T2.OutlineNumber AS LBCD_ON
           FROM @tabTask AS T1 INNER JOIN @tabTask AS T2 ON T2.OutlineNumber LIKE (T1.OutlineNumber + '%') AND T2.WBSType = 'LBCD'
         UNION
         SELECT T1.OutlineLevel, T1.OutlineNumber AS OutlineNumber, T2.OutlineNumber AS LBCD_ON
           FROM @tabTask AS T1 INNER JOIN @tabTask AS T2 ON T1.OutlineNumber LIKE (T2.OutlineNumber + '%') AND T2.WBSType = 'LBCD'
         ) AS X1
        GROUP BY OutlineNumber, OutlineLevel) AS Z1
      INNER JOIN @tabTask AS T ON Z1.LBCD_ON = T.OutlineNumber
      WHERE Z1.OutlineLevel = @iLBCDLv

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION
  
  UPDATE RPTask
    SET WBSType = CASE WHEN OutlineLevel = @iWBS1Lv THEN 'WBS1'
                       WHEN OutlineLevel = @iWBS2Lv THEN 'WBS2'
                       WHEN OutlineLevel = @iWBS3Lv THEN 'WBS3'
                       WHEN OutlineLevel = @iLBCDLv THEN 'LBCD'
                       ELSE NULL END,
        WBS1 = CASE WHEN OutlineLevel < @iWBS1Lv THEN NULL ELSE ISNULL(X1.WBS1, '<none>') END,
        WBS2 = CASE WHEN OutlineLevel < @iWBS2Lv THEN NULL ELSE ISNULL(X2.WBS2, '<none>') END,
        WBS3 = CASE WHEN OutlineLevel < @iWBS3Lv THEN NULL ELSE ISNULL(X3.WBS3, '<none>') END,
        LaborCode = CASE WHEN OutlineLevel < @iLBCDLv THEN NULL ELSE ISNULL(XL.LaborCode, '<none>') END,
        PlannedExpCost = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PlannedExpCost, 0) END,
        PlannedExpBill = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PlannedExpBill, 0) END,
        PlannedDirExpCost = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PlannedDirExpCost, 0) END,
        PlannedDirExpBill = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PlannedDirExpBill, 0) END,
        PctCompleteExpCost = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PctCompleteExpCost, 0) END,
        PctCompleteExpBill = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PctCompleteExpBill, 0) END,
        WeightedExpCost = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(WeightedExpCost, 0) END,
        WeightedExpBill = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(WeightedExpBill, 0) END,
        ExpBillRate = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(ExpBillRate, 0) END,
        ExpRevenue = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(ExpRevenue, 0) END,
        PlannedConCost = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PlannedConCost, 0) END,
        PlannedConBill = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PlannedConBill, 0) END,
        PlannedDirConCost = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PlannedDirConCost, 0) END,
        PlannedDirConBill = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PlannedDirConBill, 0) END,
        PctCompleteConCost = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PctCompleteConCost, 0) END,
        PctCompleteConBill = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PctCompleteConBill, 0) END,
        WeightedConCost = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(WeightedConCost, 0) END,
        WeightedConBill = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(WeightedConBill, 0) END,
        ConBillRate = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(ConBillRate, 0) END,
        ConRevenue = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(ConRevenue, 0) END,
        PlannedUntQty = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PlannedUntQty, 0) END,
        PlannedUntCost = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PlannedUntCost, 0) END,
        PlannedUntBill = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PlannedUntBill, 0) END,
        PlannedDirUntCost = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PlannedDirUntCost, 0) END,
        PlannedDirUntBill = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PlannedDirUntBill, 0) END,
        PctCompleteUntCost = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PctCompleteUntCost, 0) END,
        PctCompleteUntBill = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(PctCompleteUntBill, 0) END,
        WeightedUntCost = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(WeightedUntCost, 0) END,
        WeightedUntBill = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(WeightedUntBill, 0) END,
        UntCostRate = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(UntCostRate, 0) END,
        UntBillRate = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(UntBillRate, 0) END,
        UntRevenue = CASE WHEN OutlineLevel >= @iLBCDLv THEN 0 ELSE ISNULL(UntRevenue, 0) END,
        ExpVState = CASE WHEN OutlineLevel >= @iLBCDLv THEN 'N' ELSE 'Y' END,
        ConVState = CASE WHEN OutlineLevel >= @iLBCDLv THEN 'N' ELSE 'Y' END,
        UntVState = CASE WHEN OutlineLevel >= @iLBCDLv THEN 'N' ELSE 'Y' END
    FROM RPTask AS T 
      LEFT JOIN @tabWBS1 AS X1 ON T.OutlineNumber LIKE (X1.OutlineNumber + '%')
      LEFT JOIN @tabWBS2 AS X2 ON T.OutlineNumber LIKE (X2.OutlineNumber + '%')
      LEFT JOIN @tabWBS3 AS X3 ON T.OutlineNumber LIKE (X3.OutlineNumber + '%')
      LEFT JOIN @tabLBCD AS XL ON T.OutlineNumber LIKE (XL.OutlineNumber + '%')
    WHERE T.PlanID = @strPlanID
    
  UPDATE RPAssignment SET WBS1 = T.WBS1, WBS2 = T.WBS2, WBS3 = T.WBS3, LaborCode = T.LaborCode
    FROM RPAssignment AS A INNER JOIN RPTask AS T ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
    WHERE A.PlanID = @strPlanID
    
  UPDATE Activity 
    SET WBS1 = PR1.WBS1,
        WBS2 = PR2.WBS2,
        WBS3 = PR3.WBS3
    FROM Activity AS AC 
      INNER JOIN RPAssignment AS A ON AC.AssignmentID = A.AssignmentID
      LEFT JOIN PR AS PR1 ON A.WBS1 = PR1.WBS1 AND PR1.WBS2 = ' ' AND PR1.WBS3 = ' '
      LEFT JOIN PR AS PR2 ON A.WBS1 = PR2.WBS1 AND A.WBS2 = PR2.WBS2 AND PR2.WBS3 = ' '
      LEFT JOIN PR AS PR3 ON A.WBS1 = PR3.WBS1 AND A.WBS2 = PR3.WBS2 AND A.WBS3 = PR3.WBS3
    WHERE A.PlanID = @strPlanID
      
  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- If there are Labor Code rows in the Plan then:
  --   Move Expense/Consultant/Unit rows (and their TPD, if any) that were hung at or below Labor Code level up to a level above Labor Code.
  --   Reset WBS1, WBS2, WBS3 of Expense/Consultant/Unit rows.
  
  IF (@iLBCDLv < @MaxInt)
    BEGIN
    
      -->>> Expense
      
      IF (@strExpTab = 'Y')
        BEGIN
        
          IF (@intExp > 0)
            BEGIN
            
              UPDATE RPExpense
                SET TaskID = X.TaskID, WBS1 = X.WBS1, WBS2 = X.WBS2, WBS3 = X.WBS3
                OUTPUT INSERTED.ExpenseID INTO @UpdatedExp
                FROM RPExpense AS E 
                  INNER JOIN RPTask AS T ON E.PlanID = T.PlanID AND E.TaskID = T.TaskID
                  INNER JOIN RPTask AS LBCD ON T.PlanID = LBCD.PlanID AND T.OutlineNumber LIKE (LBCD.OutlineNumber + '%') AND LBCD.WBSType = 'LBCD' AND T.OutlineLevel >= LBCD.OutlineLevel
                  INNER JOIN RPTask AS X ON LBCD.PlanID = X.PlanID AND LBCD.OutlineNumber LIKE (X.OutlineNumber + '%') AND X.OutlineLevel = LBCD.OutlineLevel - 1
                WHERE E.PlanID = @strPlanID
    
            END -- IF (@intExp > 0)
            
          IF (@intExpTPD > 0)
            BEGIN

              UPDATE RPPlannedExpenses
                SET TaskID = E.TaskID
                FROM RPPlannedExpenses AS TPD
                  INNER JOIN RPExpense AS E ON TPD.PlanID = E.PlanID AND TPD.ExpenseID = E.ExpenseID
                  INNER JOIN RPTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID
                  INNER JOIN RPTask AS LBCD ON T.PlanID = LBCD.PlanID AND T.OutlineNumber LIKE (LBCD.OutlineNumber + '%') AND LBCD.WBSType = 'LBCD' AND T.OutlineLevel >= LBCD.OutlineLevel
                WHERE TPD.PlanID = @strPlanID
    
              DELETE RPPlannedExpenses
                FROM RPPlannedExpenses AS TPD
                  INNER JOIN RPTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID
                  INNER JOIN RPTask AS LBCD ON T.PlanID = LBCD.PlanID AND T.OutlineNumber LIKE (LBCD.OutlineNumber + '%') AND LBCD.WBSType = 'LBCD' AND T.OutlineLevel >= LBCD.OutlineLevel
                WHERE TPD.PlanID = @strPlanID AND TPD.ExpenseID IS NULL
    
            END -- IF (@intExpTPD > 0)
        
        END -- IF ((@strExpTab = 'Y')
    
      -->>> Consultant
      
      IF (@strConTab = 'Y')
        BEGIN
        
          IF (@intCon > 0)
            BEGIN
            
              UPDATE RPConsultant
                SET TaskID = X.TaskID, WBS1 = X.WBS1, WBS2 = X.WBS2, WBS3 = X.WBS3
                OUTPUT INSERTED.ConsultantID INTO @UpdatedCon
                FROM RPConsultant AS C 
                  INNER JOIN RPTask AS T ON C.PlanID = T.PlanID AND C.TaskID = T.TaskID
                  INNER JOIN RPTask AS LBCD ON T.PlanID = LBCD.PlanID AND T.OutlineNumber LIKE (LBCD.OutlineNumber + '%') AND LBCD.WBSType = 'LBCD' AND T.OutlineLevel >= LBCD.OutlineLevel
                  INNER JOIN RPTask AS X ON LBCD.PlanID = X.PlanID AND LBCD.OutlineNumber LIKE (X.OutlineNumber + '%') AND X.OutlineLevel = LBCD.OutlineLevel - 1
                WHERE C.PlanID = @strPlanID
    
            END -- IF (@intCon > 0)
            
          IF (@intConTPD > 0)
            BEGIN
            
              UPDATE RPPlannedConsultant
                SET TaskID = C.TaskID
                FROM RPPlannedConsultant AS TPD
                  INNER JOIN RPConsultant AS C ON TPD.PlanID = C.PlanID AND TPD.ConsultantID = C.ConsultantID
                  INNER JOIN RPTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID
                  INNER JOIN RPTask AS LBCD ON T.PlanID = LBCD.PlanID AND T.OutlineNumber LIKE (LBCD.OutlineNumber + '%') AND LBCD.WBSType = 'LBCD' AND T.OutlineLevel >= LBCD.OutlineLevel
                WHERE TPD.PlanID = @strPlanID
    
              DELETE RPPlannedConsultant
                FROM RPPlannedConsultant AS TPD
                  INNER JOIN RPTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID
                  INNER JOIN RPTask AS LBCD ON T.PlanID = LBCD.PlanID AND T.OutlineNumber LIKE (LBCD.OutlineNumber + '%') AND LBCD.WBSType = 'LBCD' AND T.OutlineLevel >= LBCD.OutlineLevel
                WHERE TPD.PlanID = @strPlanID AND TPD.ConsultantID IS NULL
            
            END -- IF (@intConTPD > 0)
        
        END -- IF ((@strConTab = 'Y')
    
      -->>> Unit
      
      IF (@strUntTab = 'Y')
        BEGIN
        
          IF (@intUnt > 0)
            BEGIN
            
              UPDATE RPUnit
                SET TaskID = X.TaskID, WBS1 = X.WBS1, WBS2 = X.WBS2, WBS3 = X.WBS3
                OUTPUT INSERTED.UnitID INTO @UpdatedUnt
                FROM RPUnit AS U 
                  INNER JOIN RPTask AS T ON U.PlanID = T.PlanID AND U.TaskID = T.TaskID
                  INNER JOIN RPTask AS LBCD ON T.PlanID = LBCD.PlanID AND T.OutlineNumber LIKE (LBCD.OutlineNumber + '%') AND LBCD.WBSType = 'LBCD' AND T.OutlineLevel >= LBCD.OutlineLevel
                  INNER JOIN RPTask AS X ON LBCD.PlanID = X.PlanID AND LBCD.OutlineNumber LIKE (X.OutlineNumber + '%') AND X.OutlineLevel = LBCD.OutlineLevel - 1
                WHERE U.PlanID = @strPlanID
            
            END -- IF (@intUnt > 0)
            
          IF (@intUntTPD > 0)
            BEGIN
            
              UPDATE RPPlannedUnit
                SET TaskID = U.TaskID
                FROM RPPlannedUnit AS TPD
                  INNER JOIN RPUnit AS U ON TPD.PlanID = U.PlanID AND TPD.UnitID = U.UnitID
                  INNER JOIN RPTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID
                  INNER JOIN RPTask AS LBCD ON T.PlanID = LBCD.PlanID AND T.OutlineNumber LIKE (LBCD.OutlineNumber + '%') AND LBCD.WBSType = 'LBCD' AND T.OutlineLevel >= LBCD.OutlineLevel
                WHERE TPD.PlanID = @strPlanID
    
              DELETE RPPlannedUnit
                FROM RPPlannedUnit AS TPD
                  INNER JOIN RPTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID
                  INNER JOIN RPTask AS LBCD ON T.PlanID = LBCD.PlanID AND T.OutlineNumber LIKE (LBCD.OutlineNumber + '%') AND LBCD.WBSType = 'LBCD' AND T.OutlineLevel >= LBCD.OutlineLevel
                WHERE TPD.PlanID = @strPlanID AND TPD.UnitID IS NULL

            END -- IF (@intUntTPD > 0)
        
        END -- IF ((@strUntTab = 'Y')

    END -- IF (@iLBCDLv < @MaxInt)

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   
  -->>> Expense
  
  IF (@strExpTab = 'Y')
    BEGIN
    
      IF (@intExp > 0)
        BEGIN
            
          UPDATE RPExpense SET WBS1 = T.WBS1, WBS2 = T.WBS2, WBS3 = T.WBS3
            FROM RPExpense AS E
              LEFT JOIN @UpdatedExp AS UE ON E.ExpenseID = UE.ExpenseID
              INNER JOIN RPTask AS T ON E.PlanID = T.PlanID AND E.TaskID = T.TaskID
            WHERE E.PlanID = @strPlanID AND UE.ExpenseID IS NULL

        END -- IF (@intExp > 0)
               
    END -- IF ((@strExpTab = 'Y')

  -->>> Consultant
  
  IF (@strConTab = 'Y')
    BEGIN
    
      IF (@intCon > 0)
        BEGIN
        
          UPDATE RPConsultant SET WBS1 = T.WBS1, WBS2 = T.WBS2, WBS3 = T.WBS3
            FROM RPConsultant AS C
              LEFT JOIN @UpdatedCon AS UC ON C.ConsultantID = UC.ConsultantID
              INNER JOIN RPTask AS T ON C.PlanID = T.PlanID AND C.TaskID = T.TaskID
            WHERE C.PlanID = @strPlanID AND UC.ConsultantID IS NULL
           
        END -- IF (@intCon > 0)
               
    END -- IF ((@strConTab = 'Y')

  -->>> Unit
  
  IF (@strUntTab = 'Y')
    BEGIN
    
      IF (@intUnt > 0)
        BEGIN
        
          UPDATE RPUnit SET WBS1 = T.WBS1, WBS2 = T.WBS2, WBS3 = T.WBS3
            FROM RPUnit AS U
              LEFT JOIN @UpdatedUnt AS UU ON U.UnitID = UU.UnitID
              INNER JOIN RPTask AS T ON U.PlanID = T.PlanID AND U.TaskID = T.TaskID
            WHERE U.PlanID = @strPlanID AND UU.UnitID IS NULL
        
        END -- IF (@intUnt > 0)
                
    END -- IF ((@strUntTab = 'Y')

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  
  COMMIT

  SET NOCOUNT OFF

END -- pmResetWBSMapping
GO
