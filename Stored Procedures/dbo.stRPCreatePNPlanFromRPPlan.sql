SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPCreatePNPlanFromRPPlan]
  @strWBS1 nvarchar(30)
AS

  BEGIN -- Procedure stRPCreatePNPlanFromRPPlan

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON

  --SET ANSI_WARNINGS OFF

  DECLARE @strCompany nvarchar(14)
  DECLARE @strOldPlanID varchar(32)
  DECLARE @strNewPlanID varchar(32)
  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)
  DECLARE @strMinorScale varchar(1)
  DECLARE @strAccordionFormatID varchar(32)
  DECLARE @strEMCostCurrencyCode nvarchar(3)
  DECLARE @strEMBillCurrencyCode nvarchar(3)
  DECLARE @strGRCostCurrencyCode nvarchar(3)
  DECLARE @strGRBillCurrencyCode nvarchar(3)
  DECLARE @strCalcExpBillAmtFlg varchar(1)
  DECLARE @strCalcConBillAmtFlg varchar(1)
  DECLARE @strExpTab varchar(1)
  DECLARE @strConTab varchar(1)
  DECLARE @strUntTab varchar(1)
  DECLARE @strNextTaskID varchar(32)
  DECLARE @strUserName nvarchar(20)

  DECLARE @siGRMethod smallint
  DECLARE @siCostGRRtMethod smallint
  DECLARE @siBillGRRtMethod smallint
  DECLARE @siCostRtMethod smallint
  DECLARE @siBillRtMethod smallint

  DECLARE @intCostRtTableNo int
  DECLARE @intBillRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int

  DECLARE @tiDefExpWBSLevel tinyint
  DECLARE @tiMinExpWBSLevel tinyint
  DECLARE @tiDefConWBSLevel tinyint
  DECLARE @tiMinConWBSLevel tinyint

  DECLARE @dtPRMinDate datetime
  DECLARE @dtPRMaxDate datetime
  DECLARE @dtToday datetime 
  DECLARE @dtTomorrow datetime
  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime

  DECLARE @strTaskID varchar (32)
  DECLARE @strAssignmentID varchar(32)

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  ----The following variables are defined for the revenue conversion 
  DECLARE @strRevenueSetting varchar(1) -- G = Gross Revenue, I = Net Revenue Include Direct Consultants, E = Net Revenue Exclude Direct Consultants, L = Labor Revenue
  DECLARE @strProjectCurrencyCode nvarchar(3)
  DECLARE @strBillingCurrencyCode nvarchar(3)
  DECLARE @strStoredRevenueCurrency varchar(1)
  DECLARE @siPlannedRevenueMethod smallint
  DECLARE @strReimbMethod char
  DECLARE @strFeeByRowAndPeriod varchar(1)

  DECLARE @strWBS2 nvarchar(30)
  DECLARE @strWBS3 nvarchar(30) 
  
  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @tabPR TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    Name nvarchar(255) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int,
    ChargeType varchar(1) COLLATE database_default,
    ProjectType nvarchar(10) COLLATE database_default,
    WBSType varchar(4) COLLATE database_default,
    Org nvarchar(30) COLLATE database_default,
    ClientID varchar(32) COLLATE database_default,
    ProjMgr nvarchar(20) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    Status varchar(1) COLLATE database_default,
    Fee decimal(19, 4),
    ConsultFee decimal(19, 4),
    ReimbAllow decimal(19, 4),
    FeeBillingCurrency decimal(19, 4),
    ConsultFeeBillingCurrency decimal(19, 4),
    ReimbAllowBillingCurrency decimal(19, 4),
    FeeDirLab decimal(19, 4),
    FeeDirExp decimal(19, 4),
    ReimbAllowExp decimal(19, 4),
    ReimbAllowCons decimal(19, 4),
    FeeDirLabBillingCurrency decimal(19, 4),
    FeeDirExpBillingCurrency decimal(19, 4),
    ReimbAllowExpBillingCurrency decimal(19, 4),
    ReimbAllowConsBillingCurrency decimal(19, 4)
    UNIQUE (RowID, WBS1, WBS2, WBS3, ParentOutlineNumber, OutlineNumber, Status)
  )

  DECLARE @tabTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
	LaborCode nvarchar(14) COLLATE database_default,
    Name nvarchar(255) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChildrenCount int,
    OutlineLevel int,
    ChargeType varchar(1) COLLATE database_default,
    ProjectType nvarchar(10) COLLATE database_default,
    WBSType varchar(4) COLLATE database_default,
    Org nvarchar(30) COLLATE database_default,
    ClientID varchar(32) COLLATE database_default,
    ProjMgr nvarchar(20) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    Status varchar(1) COLLATE database_default,
    BaselineLaborHrs decimal(19,4),
    BaselineLabCost decimal(19,4),
    BaselineLabBill decimal(19,4),
    BaselineExpCost decimal(19,4),
    BaselineExpBill decimal(19,4),
    BaselineConCost decimal(19,4),
    BaselineConBill decimal(19,4),
    BaselineUntQty decimal(19,4),
    BaselineUntCost decimal(19,4),
    BaselineUntBill decimal(19,4),
    BaselineRevenue decimal(19,4),
    BaselineDirExpCost decimal(19,4),
    BaselineDirExpBill decimal(19,4),
    BaselineDirConCost decimal(19,4),
    BaselineDirConBill decimal(19,4),
    BaselineDirUntCost decimal(19,4),
    BaselineDirUntBill decimal(19,4),
    BaselineStart datetime,
    BaselineFinish datetime,
    CompensationFee decimal(19, 4),
    ConsultantFee decimal(19, 4),
    ReimbAllowance decimal(19, 4),
    CompensationFeeBill decimal(19, 4),
    ConsultantFeeBill decimal(19, 4),
    ReimbAllowanceBill decimal(19, 4),
    CompensationFeeDirLab decimal(19, 4),
    CompensationFeeDirExp decimal(19, 4),
    ReimbAllowanceExp decimal(19, 4),
    ReimbAllowanceCon decimal(19, 4),
    CompensationFeeDirLabBill decimal(19, 4),
    CompensationFeeDirExpBill decimal(19, 4),
    ReimbAllowanceExpBill decimal(19, 4),
    ReimbAllowanceConBill decimal(19, 4)
    UNIQUE (PlanID, TaskID, WBS1, WBS2, WBS3, LaborCode, ParentOutlineNumber, OutlineNumber, ChildrenCount, Status)
  )

  DECLARE @tabNonLeafAssignment TABLE (
    OldPlanID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
	LaborCode nvarchar(14) COLLATE database_default,
    ResourceID nvarchar(20) COLLATE database_default,
    GenericResourceID nvarchar(20) COLLATE database_default,
    Leaf_OutlineNumber varchar(255) COLLATE database_default,
    Leaf_WBS2 nvarchar(30) COLLATE database_default,
    Leaf_WBS3 nvarchar(30) COLLATE database_default,
	Leaf_LaborCode nvarchar(14) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    CostRate decimal(19, 4),
    BillingRate decimal(19, 4),
    PlannedLaborHrs decimal(19, 4),
    PlannedLabCost decimal(19, 4),
    PlannedLabBill decimal(19, 4),
    BaselineLaborHrs decimal(19, 4),
    BaselineLabCost decimal(19, 4),
    BaselineLabBill decimal(19, 4)
    UNIQUE(OldPlanID, AssignmentID, WBS1, WBS2, WBS3, LaborCode,ResourceID, GenericResourceID, Leaf_OutlineNumber)
  )
  DECLARE @tabInsertedAssignment TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
	LaborCode nvarchar(14) COLLATE database_default,
    ResourceID nvarchar(20) COLLATE database_default,
    GenericResourceID nvarchar(20) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    CostRate decimal(19, 4),
    BillingRate decimal(19, 4),
    PlannedLaborHrs decimal(19, 4),
    BaselineLaborHrs decimal(19, 4),
    BaselineLabCost decimal(19, 4),
    BaselineLabBill decimal(19, 4)
    UNIQUE(PlanID, TaskID, AssignmentID, WBS1, WBS2, WBS3, LaborCode, ResourceID, GenericResourceID, StartDate, EndDate)
  )

  DECLARE @tabExpWBSTree TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    WBSType varchar(4) COLLATE database_default,
    StartDate datetime,
    EndDate datetime
    UNIQUE(PlanID, TaskID, WBS1, WBS2, WBS3)
  )

  DECLARE @tabConWBSTree TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    WBSType varchar(4) COLLATE database_default,
    StartDate datetime,
    EndDate datetime
    UNIQUE(PlanID, TaskID, WBS1, WBS2, WBS3)
  )

  DECLARE @tabExpense TABLE (
    OldPlanID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    Account nvarchar(13) COLLATE database_default,
    Vendor nvarchar(30) COLLATE database_default,
    DirectAcctFlg varchar(1) COLLATE database_default,
    PlannedExpCost decimal(19, 4),
    PlannedExpBill decimal(19, 4),
    BaselineExpCost decimal(19, 4),
    BaselineExpBill decimal(19, 4)
    UNIQUE(OldPlanID, WBS1, WBS2, WBS3, Account, Vendor)
  )

  DECLARE @tabConsultant TABLE (
    OldPlanID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    Account nvarchar(13) COLLATE database_default,
    Vendor nvarchar(30) COLLATE database_default,
    DirectAcctFlg varchar(1) COLLATE database_default,
    PlannedConCost decimal(19, 4),
    PlannedConBill decimal(19, 4),
    BaselineConCost decimal(19, 4),
    BaselineConBill decimal(19, 4)
    UNIQUE(OldPlanID, WBS1, WBS2, WBS3, Account, Vendor)
  )

  DECLARE @tabLaborCodeAssignment TABLE (
    OldPlanID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
	LaborCode nvarchar(14) COLLATE database_default,
	AssignmentID varchar(32) COLLATE database_default,
	ResourceID nvarchar(20) COLLATE database_default,
    GenericResourceID nvarchar(20) COLLATE database_default,
	StartDate datetime,
    EndDate datetime,
    CostRate decimal(19, 4),
    BillingRate decimal(19, 4),
    PlannedLaborHrs decimal(19, 4),
    PlannedLabCost decimal(19, 4),
    PlannedLabBill decimal(19, 4),
    BaselineLaborHrs decimal(19, 4),
    BaselineLabCost decimal(19, 4),
    BaselineLabBill decimal(19, 4),
	bCombined bit
    UNIQUE(OldPlanID, WBS1, WBS2, WBS3, LaborCode, ResourceID, GenericResourceID,bCombined)
  )

    DECLARE @tabCombinedNonLeafAssignment TABLE (
    OldPlanID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
	LaborCode nvarchar(14) COLLATE database_default,
	AssignmentID varchar(32) COLLATE database_default,
	ResourceID nvarchar(20) COLLATE database_default,
    GenericResourceID nvarchar(20) COLLATE database_default,
	StartDate datetime,
    EndDate datetime,
    CostRate decimal(19, 4),
    BillingRate decimal(19, 4),
    PlannedLaborHrs decimal(19, 4),
    PlannedLabCost decimal(19, 4),
    PlannedLabBill decimal(19, 4),
    BaselineLaborHrs decimal(19, 4),
    BaselineLabCost decimal(19, 4),
    BaselineLabBill decimal(19, 4)
    UNIQUE(OldPlanID, WBS1, WBS2, WBS3, LaborCode, ResourceID, GenericResourceID)
  )
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- The following is for TPD planned hours  conversion
  DECLARE @tabPlannedLabor TABLE (
    PlanID varchar(32) COLLATE database_default,
    TimePhaseID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
	LaborCode nvarchar(14) COLLATE database_default,
	ResourceID nvarchar(20) COLLATE database_default,
    GenericResourceID nvarchar(20) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
	PeriodHrs decimal(19, 4),
	PeriodCost decimal(19, 4),
    PeriodBill decimal(19, 4),
    CostRate decimal(19, 4),
    BillingRate decimal(19, 4),

    UNIQUE(PlanID, TimePhaseID, WBS1, WBS2, WBS3,LaborCode )
  )

  DECLARE @tabBaselineLabor TABLE (
    PlanID varchar(32) COLLATE database_default,
    TimePhaseID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
	LaborCode nvarchar(14) COLLATE database_default,
	ResourceID nvarchar(20) COLLATE database_default,
    GenericResourceID nvarchar(20) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
	CostRate decimal(19, 4),
    BillingRate decimal(19, 4),
    PeriodHrs decimal(19, 4),
    PeriodCost decimal(19, 4),
    PeriodBill decimal(19, 4)
    UNIQUE(PlanID, TimePhaseID, WBS1, WBS2, WBS3,LaborCode )
  )

  -- The following is for revenue conversion
  DECLARE @tabRevenue TABLE (
    OldPlanID varchar(32) COLLATE database_default,
    TimePhaseID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    PeriodCost decimal(19, 4),
    PeriodBill decimal(19, 4),
	StoredRevenueCurrency varchar(1)
    UNIQUE(OldPlanID, TimePhaseID, WBS1, WBS2, WBS3 )
  )
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET @strUserName = 'Vision2DVP35'

  SELECT 
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE())))
    FROM CFGRMSettings

  SET @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)

  -- Find RPPlan.

  SELECT 
    @strCompany = RP.Company,
    @strOldPlanID = RP.PlanID,
    @siGRMethod = RP.GRMethod,
    @siCostGRRtMethod = RP.GRMethod, /* Vision did not have the concept of CostGRRtMethod */
    @siBillGRRtMethod = RP.GRMethod, /* Vision did not have the concept of BillGRRtMethod */
    @intGenResTableNo = RP.GenResTableNo,
    @intGRBillTableNo = RP.GRBillTableNo,
    @siCostRtMethod = RP.CostRtMethod,
    @siBillRtMethod = RP.BillingRtMethod,
    @intCostRtTableNo = RP.CostRtTableNo,
    @intBillRtTableNo = RP.BillingRtTableNo,
    @strCalcExpBillAmtFlg = 'Y',
    @strCalcConBillAmtFlg = 'Y',
	@strProjectCurrencyCode = PR.ProjectCurrencyCode,
	@strBillingCurrencyCode = PR.BillingCurrencyCode,
	@strReimbMethod = RP.ReimbMethod,
	@siPlannedRevenueMethod = RP.LabMultType
    FROM RPPlan AS RP
      INNER JOIN PR ON RP.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
      LEFT JOIN PNPlan AS PN ON RP.PlanID = PN.PlanID 
    WHERE RP.WBS1 = @strWBS1 AND RP.UtilizationIncludeFlg = 'Y'
      AND PN.PlanID IS NULL

  -- Exit if the Plan is already an iAccess Plan.
  
  IF (@strOldPlanID IS NULL) RETURN

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  --The following part is for dealing with labor code conversion from Vision 7.6 to DVP 3.0
  DECLARE @siLCLevels smallint = 0

  DECLARE @strLCLevel1Enabled nvarchar(14) = 'Y'
  DECLARE @strLCLevel2Enabled nvarchar(14) = 'Y'
  DECLARE @strLCLevel3Enabled nvarchar(14) = 'Y'
  DECLARE @strLCLevel4Enabled nvarchar(14) = 'Y'
  DECLARE @strLCLevel5Enabled nvarchar(14) = 'Y'

  DECLARE @bNeedUpdateLaborCode bit = 0

  SELECT
    @siLCLevels = LCLevels
    FROM CFGFormat

  SELECT @bNeedUpdateLaborCode = bNeedUpdateLaborCode,
         @strLCLevel1Enabled = LCLevel1Enabled,
         @strLCLevel2Enabled = LCLevel2Enabled,
         @strLCLevel3Enabled = LCLevel3Enabled,
         @strLCLevel4Enabled = LCLevel4Enabled,
         @strLCLevel5Enabled = LCLevel5Enabled
  FROM dbo.stRP$tabNeedUpgradeLaborCode (@strOldPlanID)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
----Determine the variable values used for the revenue conversion 
  SELECT @strRevenueSetting = dbo.stRP$GetGlobalRevenueSetting()

  IF EXISTS (SELECT DefaultValue FROM FW_CustomColumnsData 
	WHERE (InfocenterArea = 'ProjectPlan' OR InfocenterArea = 'Projects') AND NAME = 'CustGlobalRevenueSetting' AND GridID = 'X' )
  BEGIN
    -- need to save the custom setting for resolving the support issue about revenue conversion
	UPDATE CFGRMSettings SET CustGlobalRevenueSetting = (SELECT DefaultValue FROM FW_CustomColumnsData 
	WHERE (InfocenterArea = 'ProjectPlan' OR InfocenterArea = 'Projects') AND NAME = 'CustGlobalRevenueSetting' AND GridID = 'X' )
  END

  --Set the plan compensation values by row and period for the plan's company  	 
  SELECT @strFeeByRowAndPeriod = FeesByPeriodFlg FROM CFGResourcePlanning WHERE Company = @strCompany

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  

  --delete the existing PNTask data.

	DELETE FROM PNTask WHERE PlanID = @strOldPlanID

	--Set all Task level WBS and labor code value as their parent
	UPDATE CT SET CT.WBS1 = PT.WBS1, CT.WBS2 = PT.WBS2, CT.LaborCode = NULL
	FROM RPTask CT INNER JOIN RPTask PT ON CT.PlanID = PT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber
	WHERE CT.PlanID = @strOldPlanID
		  AND CT.WBSTYPE = 'WBS3'

	UPDATE CT SET  CT.WBS1 = PT.WBS1, CT.WBS2 = PT.WBS2,CT.WBS3 = PT.WBS3 
	FROM RPTask CT INNER JOIN RPTask PT ON CT.PlanID = PT.PlanID AND CT.ParentOutlineNumber = PT.OutlineNumber
	WHERE CT.PlanID = @strOldPlanID
		  AND CT.WBSTYPE = 'LBCD'

	UPDATE A SET A.WBS1 = T.WBS1, A.WBS2 = T.WBS2, A.WBS3 = T.WBS3,  A.LaborCode = T.LaborCode  
	FROM RPAssignment A INNER JOIN RPTask T ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID
	WHERE A.PlanID = @strOldPlanID

 --Check if the plan contains Planned and/or Baseline TPD Hours Only On Mapped WBS or Labor Code Row  
 BEGIN TRANSACTION _PlanConversion_76To30_

 EXECUTE dbo.sp_stRPFixTPDOnlyOnWBSLevel @strOldPlanID  
  

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  


  -- Newly created Plan in PN tables will have a new PlanID.
  
  SET @strNewPlanID = REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')

  -- Get the current date in current local and not the UTC date.
  
  SET @dtToday = DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)
  SET @dtTomorrow = DATEADD(dd, 1, @dtToday)  

  -- Get Project data.

  SELECT 
    @dtPRMinDate = COALESCE(StartDate, EstCompletionDate, @dtTomorrow),
    @dtPRMaxDate = CASE WHEN (EstCompletionDate IS NOT NULL AND EstCompletionDate >= @dtPRMinDate) THEN EstCompletionDate ELSE @dtPRMinDate END
    FROM PR WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get the RABIBC flag

  SELECT 
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled 
    FROM FW_CFGSystem

  -- Get MinorScale and AccordionFormatID from RPAccordionFormat.
  -- There should be only one RPAccordionFormat record for a Plan.

  SELECT 
    @strAccordionFormatID = AccordionFormatID,
    @strMinorScale = MinorScale
    FROM RPAccordionFormat WHERE PlanID = @strOldPlanID

  -- Get Default Plan Settings from CFGResourcePlanning

  SELECT
    @tiDefExpWBSLevel = ExpWBSLevel,
    @tiDefConWBSLevel = ConWBSLevel,
    @strExpTab = ExpTab,
    @strConTab = ConTab,
    @strUntTab = UntTab
    FROM CFGResourcePlanning WHERE Company = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN @strCompany ELSE ' ' END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collect all WBS rows from PR table.

  -- WBS1 Row.

  INSERT @tabPR(
    WBS1,
    WBS2,
    WBS3,
    Name,
    ParentOutlineNumber,
    OutlineNumber,
    OutlineLevel,
    ChargeType,
    ProjectType,
    WBSType,
    Org,
    ClientID,
    ProjMgr,
    StartDate,
    EndDate,
    Status,
    Fee,
    ConsultFee,
    ReimbAllow,
    FeeBillingCurrency,
    ConsultFeeBillingCurrency,
    ReimbAllowBillingCurrency,
    FeeDirLab,
    FeeDirExp,
    ReimbAllowExp,
    ReimbAllowCons,
    FeeDirLabBillingCurrency,
    FeeDirExpBillingCurrency,
    ReimbAllowExpBillingCurrency,
    ReimbAllowConsBillingCurrency
  )
    SELECT
      PR.WBS1 AS WBS1,
      PR.WBS2 AS WBS2,
      PR.WBS3 AS WBS3,
      PR.Name AS Name,
      NULL AS ParentOutlineNumber,
      '001' AS OutlineNumber,
      0 AS OutlineLevel,
      PR.ChargeType AS ChargeType,
      PR.ProjectType AS ProjectType,
      'WBS1' AS WBSType,
      PR.Org AS Org,
      PR.ClientID AS ClientID,
      PR.ProjMgr AS ProjMgr,
      COALESCE(T.StartDate, @dtPRMinDate) AS StartDate,
      COALESCE(T.EndDate, @dtPRMaxDate)  AS EndDate,
      PR.Status AS Status,
      PR.Fee AS Fee,
      PR.ConsultFee AS ConsultFee,
      PR.ReimbAllow AS ReimbAllow,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeBillingCurrency ELSE PR.Fee END) AS FeeBillingCurrency,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ConsultFeeBillingCurrency ELSE PR.ConsultFee END) AS ConsultFeeBillingCurrency,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowBillingCurrency ELSE PR.ReimbAllow END) AS ReimbAllowBillingCurrency,
      PR.FeeDirLab AS CompensationFeeDirLab,
      PR.FeeDirExp AS CompensationFeeDirExp,
      PR.ReimbAllowExp AS ReimbAllowanceExp,
      PR.ReimbAllowCons AS ReimbAllowanceCon,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirLabBillingCurrency ELSE PR.FeeDirLab END) AS FeeDirLabBillingCurrency,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirExpBillingCurrency ELSE PR.FeeDirExp END) AS FeeDirExpBillingCurrency,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowExpBillingCurrency ELSE PR.ReimbAllowExp END) AS ReimbAllowExpBillingCurrency,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowConsBillingCurrency ELSE PR.ReimbAllowCons END) AS ReimbAllowConsBillingCurrency
      FROM PR
	  INNER JOIN RPTask AS T ON PR.WBS1 = T.WBS1 AND  T.WBS2 IS NULL AND T.WBS3 IS NULL AND
          T.PlanID = @strOldPlanID AND T.WBSType <> 'LBCD' AND
          ((T.WBSType = 'WBS1' AND T.WBS1 <> '<none>') OR
           (T.WBSType = 'WBS2' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>') OR
           (T.WBSType = 'WBS3' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>' AND T.WBS3 <> '<none>'))
      WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '

  -- WBS2 Rows.
  INSERT @tabPR(
    WBS1,
    WBS2,
    WBS3,
    Name,
    ParentOutlineNumber,
    OutlineNumber,
    OutlineLevel,
    ChargeType,
    ProjectType,
    WBSType,
    Org,
    ClientID,
    ProjMgr,
    StartDate,
    EndDate,
    Status,
    Fee,
    ConsultFee,
    ReimbAllow,
    FeeBillingCurrency,
    ConsultFeeBillingCurrency,
    ReimbAllowBillingCurrency,
    FeeDirLab,
    FeeDirExp,
    ReimbAllowExp,
    ReimbAllowCons,
    FeeDirLabBillingCurrency,
    FeeDirExpBillingCurrency,
    ReimbAllowExpBillingCurrency,
    ReimbAllowConsBillingCurrency
  )
    SELECT
      X.WBS1 AS WBS1,
      X.WBS2 AS WBS2,
      X.WBS3 AS WBS3,
      X.Name,
      '001' AS ParentOutlineNumber,
      '001' + '.' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
      1 AS OutlineLevel,
      X.ChargeType AS ChargeType,
      X.ProjectType AS ProjectType,
      'WBS2' AS WBSType,
      X.Org,
      X.ClientID,
      X.ProjMgr,
      X.StartDate,
      X.EndDate,
      X.Status,
      X.Fee,
      X.ConsultFee,
      X.ReimbAllow,
      X.FeeBillingCurrency,
      X.ConsultFeeBillingCurrency,
      X.ReimbAllowBillingCurrency,
      X.FeeDirLab,
      X.FeeDirExp,
      X.ReimbAllowExp,
      X.ReimbAllowCons,
      X.FeeDirLabBillingCurrency,
      X.FeeDirExpBillingCurrency,
      X.ReimbAllowExpBillingCurrency,
      X.ReimbAllowConsBillingCurrency
      FROM (
        SELECT
          PR.WBS1 AS WBS1,
          PR.WBS2 AS WBS2,
          PR.WBS3 AS WBS3,
          PR.Name AS Name,
          PR.ChargeType AS ChargeType,
          PR.ProjectType AS ProjectType,
          PR.Org AS Org,
          PR.ClientID AS ClientID,
          PR.ProjMgr AS ProjMgr,
          COALESCE(T.StartDate, PT.StartDate, @dtPRMinDate) AS StartDate,
          COALESCE(T.EndDate,PT.EndDate, @dtPRMaxDate)  AS EndDate,
          PR.Status AS Status,
          PR.Fee AS Fee,
          PR.ConsultFee AS ConsultFee,
          PR.ReimbAllow AS ReimbAllow,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeBillingCurrency ELSE PR.Fee END) AS FeeBillingCurrency,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ConsultFeeBillingCurrency ELSE PR.ConsultFee END) AS ConsultFeeBillingCurrency,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowBillingCurrency ELSE PR.ReimbAllow END) AS ReimbAllowBillingCurrency,
          PR.FeeDirLab AS FeeDirLab,
          PR.FeeDirExp AS FeeDirExp,
          PR.ReimbAllowExp AS ReimbAllowExp,
          PR.ReimbAllowCons AS ReimbAllowCons,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirLabBillingCurrency ELSE PR.FeeDirLab END) AS FeeDirLabBillingCurrency,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirExpBillingCurrency ELSE PR.FeeDirExp END) AS FeeDirExpBillingCurrency,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowExpBillingCurrency ELSE PR.ReimbAllowExp END) AS ReimbAllowExpBillingCurrency,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowConsBillingCurrency ELSE PR.ReimbAllowCons END) AS ReimbAllowConsBillingCurrency,
          ROW_NUMBER() OVER (PARTITION BY PR.WBS1 ORDER BY PR.WBS1, PR.WBS2) AS RowID
          FROM PR 
		  LEFT JOIN RPTask AS T ON PR.WBS1 = T.WBS1 AND PR.WBS2 = ISNULL(T.WBS2, ' ') AND T.WBS3 IS NULL AND
          T.PlanID = @strOldPlanID AND T.WBSType <> 'LBCD'
		  LEFT JOIN @tabPR AS PT ON PR.WBS1 = PT.WBS1 AND PT.WBS2 = ' ' AND  PT.WBS3 = ' '    
          WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 <> ' ' AND PR.WBS3 = ' '
      ) AS X

  -- WBS3 Rows.

  INSERT @tabPR(
    WBS1,
    WBS2,
    WBS3,
    Name,
    ParentOutlineNumber,
    OutlineNumber,
    OutlineLevel,
    ChargeType,
    ProjectType,
    WBSType,
    Org,
    ClientID,
    ProjMgr,
    StartDate,
    EndDate,
    Status,
    Fee,
    ConsultFee,
    ReimbAllow,
    FeeBillingCurrency,
    ConsultFeeBillingCurrency,
    ReimbAllowBillingCurrency,
    FeeDirLab,
    FeeDirExp,
    ReimbAllowExp,
    ReimbAllowCons,
    FeeDirLabBillingCurrency,
    FeeDirExpBillingCurrency,
    ReimbAllowExpBillingCurrency,
    ReimbAllowConsBillingCurrency
  )
    SELECT
      X.WBS1 AS WBS1,
      X.WBS2 AS WBS2,
      X.WBS3 AS WBS3,
      X.Name,
      PX.OutlineNumber AS ParentOutlineNumber,
      PX.OutlineNumber + '.' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(X.RowID, 36)), 3) AS OutlineNumber,
      PX.OutlineLevel + 1 AS OutlineLevel,
      X.ChargeType AS ChargeType,
      X.ProjectType AS ProjectType,
      'WBS3' AS WBSType,
      X.Org,
      X.ClientID,
      X.ProjMgr,
      X.StartDate,
      X.EndDate,
      X.Status,
      X.Fee,
      X.ConsultFee,
      X.ReimbAllow,
      X.FeeBillingCurrency,
      X.ConsultFeeBillingCurrency,
      X.ReimbAllowBillingCurrency,
      X.FeeDirLab,
      X.FeeDirExp,
      X.ReimbAllowExp,
      X.ReimbAllowCons,
      X.FeeDirLabBillingCurrency,
      X.FeeDirExpBillingCurrency,
      X.ReimbAllowExpBillingCurrency,
      X.ReimbAllowConsBillingCurrency
      FROM (
        SELECT
          PR.WBS1 AS WBS1,
          PR.WBS2 AS WBS2,
          PR.WBS3 AS WBS3,
          PR.Name AS Name,
          PR.ChargeType AS ChargeType,
          PR.ProjectType AS ProjectType,
          PR.Org AS Org,
          PR.ClientID AS ClientID,
          PR.ProjMgr AS ProjMgr,
          COALESCE(T.StartDate,PT.StartDate, @dtPRMinDate) AS StartDate,
          COALESCE(T.EndDate,PT.EndDate, @dtPRMaxDate)  AS EndDate,
          PR.Status AS Status,
          PR.Fee AS Fee,
          PR.ConsultFee AS ConsultFee,
          PR.ReimbAllow AS ReimbAllow,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeBillingCurrency ELSE PR.Fee END) AS FeeBillingCurrency,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ConsultFeeBillingCurrency ELSE PR.ConsultFee END) AS ConsultFeeBillingCurrency,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowBillingCurrency ELSE PR.ReimbAllow END) AS ReimbAllowBillingCurrency,
          PR.FeeDirLab AS FeeDirLab,
          PR.FeeDirExp AS FeeDirExp,
          PR.ReimbAllowExp AS ReimbAllowExp,
          PR.ReimbAllowCons AS ReimbAllowCons,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirLabBillingCurrency ELSE PR.FeeDirLab END) AS FeeDirLabBillingCurrency,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirExpBillingCurrency ELSE PR.FeeDirExp END) AS FeeDirExpBillingCurrency,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowExpBillingCurrency ELSE PR.ReimbAllowExp END) AS ReimbAllowExpBillingCurrency,
          (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowConsBillingCurrency ELSE PR.ReimbAllowCons END) AS ReimbAllowConsBillingCurrency,
          ROW_NUMBER() OVER (PARTITION BY PR.WBS2 ORDER BY PR.WBS1, PR.WBS2, PR.WBS3) AS RowID
          FROM PR 
		  LEFT JOIN RPTask AS T ON PR.WBS1 = T.WBS1 AND PR.WBS2 = ISNULL(T.WBS2, ' ') AND PR.WBS3 = ISNULL(T.WBS3, ' ') AND
          T.PlanID = @strOldPlanID AND T.WBSType <> 'LBCD'
		  LEFT JOIN @tabPR AS PT ON PR.WBS1 = PT.WBS1 AND PR.WBS2 = PT.WBS2  AND  PT.WBS3 = ' '    
          WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 <> ' ' AND PR.WBS3 <> ' '

      ) AS X
        INNER JOIN @tabPR AS PX ON X.WBS1 = PX.WBS1 AND X.WBS2 = PX.WBS2 AND PX.WBS3 = ' '

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Merge WBS rows from PR and RPTask tables to form the new combined WBS tree.

  INSERT @tabTask(
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    Name,
    ParentOutlineNumber,
    OutlineNumber,
    ChildrenCount,
    OutlineLevel,
    ChargeType,
    ProjectType,
    WBSType,
    Org,
    ClientID,
    ProjMgr,
    StartDate,
    EndDate,
    Status,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    BaselineExpCost,
    BaselineExpBill,
    BaselineConCost,
    BaselineConBill,
    BaselineUntQty,
    BaselineUntCost,
    BaselineUntBill,
    BaselineRevenue,
    BaselineDirExpCost,
    BaselineDirExpBill,
    BaselineDirConCost,
    BaselineDirConBill,
    BaselineDirUntCost,
    BaselineDirUntBill,
    BaselineStart,
    BaselineFinish,
    CompensationFee,
    ConsultantFee,
    ReimbAllowance,
    CompensationFeeBill,
    ConsultantFeeBill,
    ReimbAllowanceBill,
    CompensationFeeDirLab,
    CompensationFeeDirExp,
    ReimbAllowanceExp,
    ReimbAllowanceCon,
    CompensationFeeDirLabBill,
    CompensationFeeDirExpBill,
    ReimbAllowanceExpBill,
    ReimbAllowanceConBill
  )
    SELECT
      @strNewPlanID AS PlanID,
      COALESCE(T.TaskID, REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')) AS TaskID,
      PR.WBS1 AS WBS1,
      CASE WHEN PR.WBS2 = ' ' THEN NULL ELSE PR.WBS2 END AS WBS2,
      CASE WHEN PR.WBS3 = ' ' THEN NULL ELSE PR.WBS3 END AS WBS3,
      COALESCE(T.Name, PR.Name) AS Name,
      PR.ParentOutlineNumber AS ParentOutlineNumber,
      PR.OutlineNumber AS OutlineNumber,
      0 AS ChildrenCount,
      PR.OutlineLevel AS OutlineLevel,
      PR.ChargeType AS ChargeType,
      PR.ProjectType AS ProjectType,
      PR.WBSType AS WBSType,
      PR.Org AS Org,
      PR.ClientID AS ClientID,
      PR.ProjMgr AS ProjMgr,
      COALESCE(T.StartDate, PR.StartDate, @dtPRMinDate) AS StartDate,
      COALESCE(T.EndDate, PR.EndDate, @dtPRMaxDate)  AS EndDate,
      PR.Status AS Status,
      ISNULL(T.BaselineLaborHrs, 0) AS BaselineLaborHrs,
      ISNULL(T.BaselineLabCost, 0) AS BaselineLabCost,
      ISNULL(T.BaselineLabBill, 0) AS BaselineLabBill,
      ISNULL(T.BaselineExpCost, 0) AS BaselineExpCost,
      ISNULL(T.BaselineExpBill, 0) AS BaselineExpBill,
      ISNULL(T.BaselineConCost, 0) AS BaselineConCost,
      ISNULL(T.BaselineConBill, 0) AS BaselineConBill,
      ISNULL(T.BaselineUntQty, 0) AS BaselineUntQty,
      ISNULL(T.BaselineUntCost, 0) AS BaselineUntCost,
      ISNULL(T.BaselineUntBill, 0) AS BaselineUntBill,
      ISNULL(T.BaselineRevenue, 0) AS BaselineRevenue, 
      ISNULL(T.BaselineDirExpCost, 0) AS BaselineDirExpCost,
      ISNULL(T.BaselineDirExpBill, 0) AS BaselineDirExpBill,
      ISNULL(T.BaselineDirConCost, 0) AS BaselineDirConCost,
      ISNULL(T.BaselineDirConBill, 0) AS BaselineDirConBill,
      ISNULL(T.BaselineDirUntCost, 0) AS BaselineDirUntCost,
      ISNULL(T.BaselineDirUntBill, 0) AS BaselineDirUntBill,
      T.BaselineStart AS BaselineStart,
      T.BaselineFinish AS BaselineFinish,
      PR.Fee AS CompensationFee,
      PR.ConsultFee AS ConsultantFee,
      PR.ReimbAllow AS ReimbAllowance,
      PR.FeeBillingCurrency AS CompensationFeeBill,
      PR.ConsultFeeBillingCurrency AS ConsultantFeeBill,
      PR.ReimbAllowBillingCurrency AS ReimbAllowanceBill,
      PR.FeeDirLab AS CompensationFeeDirLab,
      PR.FeeDirExp AS CompensationFeeDirExp,
      PR.ReimbAllowExp AS ReimbAllowanceExp,
      PR.ReimbAllowCons AS ReimbAllowanceCon,
      PR.FeeDirLabBillingCurrency AS CompensationFeeDirLabBill,
      PR.FeeDirExpBillingCurrency AS CompensationFeeDirExpBill,
      PR.ReimbAllowExpBillingCurrency AS ReimbAllowanceExpBill,
      PR.ReimbAllowConsBillingCurrency AS ReimbAllowanceConBill
      FROM @tabPR AS PR
        LEFT JOIN RPTask AS T ON PR.WBS1 = T.WBS1 AND PR.WBS2 = ISNULL(T.WBS2, ' ') AND PR.WBS3 = ISNULL(T.WBS3, ' ') AND
          T.PlanID = @strOldPlanID AND
          ((T.WBSType = 'WBS1' AND T.WBS1 <> '<none>') OR
           (T.WBSType = 'WBS2' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>') OR
           (T.WBSType = 'WBS3' AND T.WBS1 <> '<none>' AND T.WBS2 <> '<none>' AND T.WBS3 <> '<none>'))

    UPDATE T SET
    ChildrenCount = (SELECT COUNT(*) FROM @tabTask WHERE ParentOutlineNumber = T.OutlineNUmber)
    FROM @tabTask AS T 
    WHERE T.ChildrenCount <> (SELECT COUNT(*) FROM @tabTask WHERE ParentOutlineNumber = T.OutlineNUmber)

  IF  @siLCLevels > 0 AND @bNeedUpdateLaborCode = 1 --Need to upgrade the labor code level task
  BEGIN
	-- Need to collect the labor code level task
	INSERT @tabTask(
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
	LaborCode,
    Name,
    ParentOutlineNumber,
    OutlineNumber,
    ChildrenCount,
    OutlineLevel,
    ChargeType,
    ProjectType,
    WBSType,
    Org,
    ClientID,
    ProjMgr,
    StartDate,
    EndDate,
    Status,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    BaselineExpCost,
    BaselineExpBill,
    BaselineConCost,
    BaselineConBill,
    BaselineUntQty,
    BaselineUntCost,
    BaselineUntBill,
    BaselineRevenue,
    BaselineDirExpCost,
    BaselineDirExpBill,
    BaselineDirConCost,
    BaselineDirConBill,
    BaselineDirUntCost,
    BaselineDirUntBill,
    BaselineStart,
    BaselineFinish,
    CompensationFee,
    ConsultantFee,
    ReimbAllowance,
    CompensationFeeBill,
    ConsultantFeeBill,
    ReimbAllowanceBill,
    CompensationFeeDirLab,
    CompensationFeeDirExp,
    ReimbAllowanceExp,
    ReimbAllowanceCon,
    CompensationFeeDirLabBill,
    CompensationFeeDirExpBill,
    ReimbAllowanceExpBill,
    ReimbAllowanceConBill  
  )
    SELECT
      @strNewPlanID AS PlanID,
      MAX(COALESCE(TaskID, REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', ''))) AS TaskID,
      WBS1,
      WBS2,
      WBS3,
	  LaborCode,
      MAX(NAME) AS Name,
      MIN(ParentOutlineNumber) AS ParentOutlineNumber,
      MIN(OutlineNumber) AS OutlineNumber,
      0 AS ChildrenCount,
      MIN(OutlineLevel) AS OutlineLevel,
      MAX(ChargeType) AS ChargeType,
      MAX(ProjectType) AS ProjectType,
      MAX(WBSType) AS WBSType,
      MAX(Org) AS Org,
      MAX(ClientID) AS ClientID,
      MAX(ProjMgr) AS ProjMgr,
      Min(StartDate) AS StartDate,
      Max(EndDate) AS EndDate,
      Max(Status) AS Status ,
      SUM(ISNULL(BaselineLaborHrs, 0)) AS BaselineLaborHrs,
      SUM(ISNULL(BaselineLabCost, 0)) AS BaselineLabCost,
      SUM(ISNULL(BaselineLabBill, 0)) AS BaselineLabBill,
      SUM(ISNULL(BaselineExpCost, 0)) AS BaselineExpCost,
      SUM(ISNULL(BaselineExpBill, 0)) AS BaselineExpBill,
      SUM(ISNULL(BaselineConCost, 0)) AS BaselineConCost,
      SUM(ISNULL(BaselineConBill, 0)) AS BaselineConBill,
      SUM(ISNULL(BaselineUntQty, 0)) AS BaselineUntQty,
      SUM(ISNULL(BaselineUntCost, 0)) AS BaselineUntCost,
      SUM(ISNULL(BaselineUntBill, 0)) AS BaselineUntBill,
      SUM(ISNULL(BaselineRevenue, 0)) AS BaselineRevenue, 
      SUM(ISNULL(BaselineDirExpCost, 0)) AS BaselineDirExpCost,
      SUM(ISNULL(BaselineDirExpBill, 0)) AS BaselineDirExpBill,
      SUM(ISNULL(BaselineDirConCost, 0)) AS BaselineDirConCost,
      SUM(ISNULL(BaselineDirConBill, 0)) AS BaselineDirConBill,
      SUM(ISNULL(BaselineDirUntCost, 0)) AS BaselineDirUntCost,
      SUM(ISNULL(BaselineDirUntBill, 0)) AS BaselineDirUntBill,
      Min(BaselineStart) AS BaselineStart,
      Max(BaselineFinish) AS BaselineFinish,
      SUM(ISNULL(CompensationFee,0)) AS  CompensationFee,
      SUM(ISNULL(ConsultantFee,0)) AS ConsultantFee,
      SUM(ISNULL(ReimbAllowance,0)) AS ReimbAllowance,
      SUM(ISNULL(CompensationFeeBill,0)) AS CompensationFeeBill,
      SUM(ISNULL(ConsultantFeeBill,0)) AS ConsultantFeeBill,
      SUM(ISNULL(ReimbAllowanceBill,0)) AS ReimbAllowanceBill,
      SUM(ISNULL(CompensationFeeDirLab,0)) AS CompensationFeeDirLab,
      SUM(ISNULL(CompensationFeeDirExp,0)) AS CompensationFeeDirExp,
      SUM(ISNULL(ReimbAllowanceExp,0)) AS ReimbAllowanceExp,
      SUM(ISNULL(ReimbAllowanceCon,0)) AS ReimbAllowanceCon,
      SUM(ISNULL(CompensationFeeDirLabBill,0)) AS CompensationFeeDirLabBill,
      SUM(ISNULL(CompensationFeeDirExpBill,0)) AS CompensationFeeDirExpBill,
      SUM(ISNULL(ReimbAllowanceExpBill,0)) AS ReimbAllowanceExpBill,
      SUM(ISNULL(ReimbAllowanceConBill,0)) AS ReimbAllowanceConBill  
	  FROM RPTask WHERE PlanID = @strOldPlanID AND WBSType = 'LBCD'
	  AND WBS1 <> '<none>' 
	  AND ISNULL(LaborCode, ' ') <> '<none>' 
	  AND ISNULL(WBS2, ' ') <> '<none>'  AND ISNULL(WBS3, ' ') <> '<none>' 
	  GROUP BY PlanID, WBS1, WBS2, WBS3, LaborCode


	--Need to reset the labor code task parent outline number based their WBS1/WBS2/WBS3 value.
	--Because the WBS level task outline number is now order by WBS2/WBS3 value, but Vision does not follow the rule
	 UPDATE  T SET T.ParentOutlineNumber = ParentTask.OutlineNumber 
			 FROM  @tabTask AS T INNER JOIN @tabTask ParentTask 
			 ON T.WBS1 = ParentTask.WBS1 
			 AND ISNULL(T.WBS2,' ') = ISNULL(ParentTask.WBS2,' ') 
			 AND ISNULL(T.WBS3,' ') = ISNULL(ParentTask.WBS3,' ') 
			 WHERE T.WBSType = 'LBCD' AND ParentTask.WBSType <> 'LBCD' 
   
   --Move Labor code level Tasks to the lowest WBS Level
   -- 1. The Labor code under WBS1 Level need to move under the first WBS2 Level or under WBS3 Level
	 UPDATE  T SET T.ParentOutlineNumber = ParentTask.OutlineNumber, 
					   T.WBS2 = ParentTask.WBS2, 
					   T.WBS3 = ParentTask.WBS3,
					   T.OutlineLevel = CASE WHEN ParentTask.WBS3 IS NULL THEN 2 ELSE 3 END FROM 
					   @tabTask AS T INNER JOIN
	 (SELECT TOP 1 PlanID,OutlineNumber,WBS2, WBS3 FROM @tabTask WHERE PlanID = @strNewPlanID AND
	    WBS2 IS NOT NULL AND ChildrenCount = 0 ORDER BY OutlineNumber ) AS ParentTask 
		  ON ParentTask.PlanID = T.PlanID
     Where T.PlanID = @strNewPlanID AND WBStype = 'LBCD' AND T.WBS2 IS NULL

	 ---- 2. Labor code under WBS2 Level and need to move under WBS3 Level
	 -- if no labor code under WBS1 are updated. then we need to move the labor code under WBS2 to WBS3 level
	DECLARE laborCodeCursor CURSOR FOR 
	SELECT  TaskID  FROM @tabTask WHERE WBS2 IS NOT NULL AND WBSType = 'LBCD'

	OPEN laborCodeCursor
	FETCH NEXT FROM laborCodeCursor INTO @strTaskID 
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		  
		UPDATE T SET T.ParentOutlineNumber = ParentTask.OutlineNumber, 
						T.WBS3 = ParentTask.WBS3,
						T.OutlineLevel = 3 							    
						FROM @tabTask AS T INNER JOIN
		(SELECT TOP 1 T.PlanID AS PlanID,T.OutlineNumber AS OutlineNumber, T.WBS3 AS WBS3 FROM @tabTask T
		INNER JOIN @tabTask laborCodeTask ON laborCodeTask.PlanID = T.PlanID AND
		T.WBS2 = laborCodeTask.WBS2 AND T.WBSType <> 'LBCD' WHERE laborCodeTask.TaskID = @strTaskID
		AND T.WBS3 IS NOT NULL ORDER BY OutlineNumber) AS ParentTask ON ParentTask.PlanID = T.PlanID					
		Where T.PlanID = @strNewPlanID AND T.TaskID = @strTaskID AND T.OutlineLevel = 2

		FETCH NEXT FROM laborCodeCursor INTO @strTaskID
	END --WHILE @@FETCH_STATUS = 0 
	CLOSE laborCodeCursor
	DEALLOCATE laborCodeCursor
	
	--Because the WBS3 Level Outline number may be changed after sync with PR, 
	--we need to reset the parent outline number for the labor code under those WBS3 level.
	UPDATE T 
    SET ParentOutlineNumber = PT.OutlineNumber
        FROM @tabTask T INNER JOIN @tabTask PT 
		     ON T.PlanID = PT.PlanID AND T.WBS1 = PT.WBS1 AND T.WBS2 = PT.WBS2 AND T.WBS3 = PT.WBS3    
    WHERE  T.WBS3 IS NOT NULL AND T.WBSType = 'LBCD' AND PT.WBS3 IS NOT NULL AND PT.WBSType = 'WBS3' 

	--Insert the data in @tabTask into PNTask so that we can reorder outline number
	INSERT PNTask(
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
	LaborCode,
    Name,
    ParentOutlineNumber,
    OutlineNumber,
    ChildrenCount,
    OutlineLevel,
    ChargeType,
    ProjectType,
    WBSType,
    Org,
    ClientID,
    ProjMgr,
    StartDate,
    EndDate,
    Status,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    BaselineExpCost,
    BaselineExpBill,
    BaselineConCost,
    BaselineConBill,
    BaselineUntQty,
    BaselineUntCost,
    BaselineUntBill,
    BaselineRevenue,
    BaselineDirExpCost,
    BaselineDirExpBill,
    BaselineDirConCost,
    BaselineDirConBill,
    BaselineDirUntCost,
    BaselineDirUntBill,
    BaselineStart,
    BaselineFinish,
    CompensationFee,
    ConsultantFee,
    ReimbAllowance,
    CompensationFeeBill,
    ConsultantFeeBill,
    ReimbAllowanceBill,
    CompensationFeeDirLab,
    CompensationFeeDirExp,
    ReimbAllowanceExp,
    ReimbAllowanceCon,
    CompensationFeeDirLabBill,
    CompensationFeeDirExpBill,
    ReimbAllowanceExpBill,
    ReimbAllowanceConBill,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      PlanID,
      TaskID,
      WBS1,
      WBS2,
      WBS3,
	  LaborCode,
      Name,
      ParentOutlineNumber,
      OutlineNumber,
      ChildrenCount,
      OutlineLevel,
      ChargeType,
      ProjectType,
      WBSType,
      Org,
      ClientID,
      ProjMgr,
      StartDate,
      EndDate,
      Status,
      BaselineLaborHrs,
      BaselineLabCost,
      BaselineLabBill,
      BaselineExpCost,
      BaselineExpBill,
      BaselineConCost,
      BaselineConBill,
      BaselineUntQty,
      BaselineUntCost,
      BaselineUntBill,
      BaselineRevenue,
      BaselineDirExpCost,
      BaselineDirExpBill,
      BaselineDirConCost,
      BaselineDirConBill,
      BaselineDirUntCost,
      BaselineDirUntBill,
      BaselineStart,
      BaselineFinish,
      CompensationFee,
      ConsultantFee,
      ReimbAllowance,
      CompensationFeeBill,
      ConsultantFeeBill,
      ReimbAllowanceBill,
      CompensationFeeDirLab,
      CompensationFeeDirExp,
      ReimbAllowanceExp,
      ReimbAllowanceCon,
      CompensationFeeDirLabBill,
      CompensationFeeDirExpBill,
      ReimbAllowanceExpBill,
      ReimbAllowanceConBill,
      @strUserName AS CreateUser,
      GETUTCDATE() AS CreateDate,
      @strUserName AS ModUser,
      GETUTCDATE() AS ModDate
      FROM @tabTask AS T

	 --3 Reorder the outline number and childrenCount for the plan	 
	EXECUTE dbo.stRPReorderOutlineNumber @strNewPlanID 

	--now insert PNTask back to @tabTask
	
  -- Need to update ChildrenCount in @tabTask.

    DELETE FROM @tabTask

	INSERT @tabTask(
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
	LaborCode,
    Name,
    ParentOutlineNumber,
    OutlineNumber,
    ChildrenCount,
    OutlineLevel,
    ChargeType,
    ProjectType,
    WBSType,
    Org,
    ClientID,
    ProjMgr,
    StartDate,
    EndDate,
    Status,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    BaselineExpCost,
    BaselineExpBill,
    BaselineConCost,
    BaselineConBill,
    BaselineUntQty,
    BaselineUntCost,
    BaselineUntBill,
    BaselineRevenue,
    BaselineDirExpCost,
    BaselineDirExpBill,
    BaselineDirConCost,
    BaselineDirConBill,
    BaselineDirUntCost,
    BaselineDirUntBill,
    BaselineStart,
    BaselineFinish,
    CompensationFee,
    ConsultantFee,
    ReimbAllowance,
    CompensationFeeBill,
    ConsultantFeeBill,
    ReimbAllowanceBill,
    CompensationFeeDirLab,
    CompensationFeeDirExp,
    ReimbAllowanceExp,
    ReimbAllowanceCon,
    CompensationFeeDirLabBill,
    CompensationFeeDirExpBill,
    ReimbAllowanceExpBill,
    ReimbAllowanceConBill
  )
    SELECT
      PlanID,
      TaskID,
      WBS1,
      WBS2,
      WBS3,
	  LaborCode,
      Name,
      ParentOutlineNumber,
      OutlineNumber,
      ChildrenCount,
      OutlineLevel,
      ChargeType,
      ProjectType,
      WBSType,
      Org,
      ClientID,
      ProjMgr,
      StartDate,
      EndDate,
      Status,
      BaselineLaborHrs,
      BaselineLabCost,
      BaselineLabBill,
      BaselineExpCost,
      BaselineExpBill,
      BaselineConCost,
      BaselineConBill,
      BaselineUntQty,
      BaselineUntCost,
      BaselineUntBill,
      BaselineRevenue,
      BaselineDirExpCost,
      BaselineDirExpBill,
      BaselineDirConCost,
      BaselineDirConBill,
      BaselineDirUntCost,
      BaselineDirUntBill,
      BaselineStart,
      BaselineFinish,
      CompensationFee,
      ConsultantFee,
      ReimbAllowance,
      CompensationFeeBill,
      ConsultantFeeBill,
      ReimbAllowanceBill,
      CompensationFeeDirLab,
      CompensationFeeDirExp,
      ReimbAllowanceExp,
      ReimbAllowanceCon,
      CompensationFeeDirLabBill,
      CompensationFeeDirExpBill,
      ReimbAllowanceExpBill,
      ReimbAllowanceConBill
      FROM PNTask AS T  WHERE PlanID = @strNewPlanID

      DELETE FROM PNTASK Where PlanID = @strNewPlanID

  UPDATE T SET
    ChildrenCount = (SELECT COUNT(*) FROM @tabTask WHERE ParentOutlineNumber = T.OutlineNUmber)
    FROM @tabTask AS T 
    WHERE T.ChildrenCount <> (SELECT COUNT(*) FROM @tabTask WHERE ParentOutlineNumber = T.OutlineNUmber)

  END --  IF  @siLCLevels > 0 AND @bNeedUpdateLaborCode = 1 --Need to upgrade the labor code level task
 
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  -- Save all TPD planned hours and baseline hours
  INSERT @tabPlannedLabor (
		PlanID ,
		TimePhaseID ,
		WBS1,
		WBS2,
		WBS3,
		LaborCode,
		ResourceID,
		GenericResourceID,
		StartDate,
		EndDate,
		PeriodHrs,
		PeriodCost,
		PeriodBill,
		CostRate,
		BillingRate
	)
	SELECT  @strNewPlanID,
			P.TimePhaseID,
			A.WBS1,
			A.WBS2,
			A.WBS3,
			A.LaborCode,
			A.ResourceID,
			A.GenericResourceID,
			P.StartDate,
			P.EndDate,
			P.PeriodHrs,
			P.PeriodCost,
			P.PeriodBill,
			P.CostRate,
			P.BillingRate
			FROM  RPPlannedLabor P 
			      INNER JOIN RPAssignment A 
			      ON P.PlanID = A.PlanID AND P.TaskID = A.TaskID AND P.AssignmentID = A.AssignmentID
				  WHERE P.PlanID = @strOldPlanID AND (A.GenericResourceID IS NOT NULL OR A.ResourceID IS NOT NULL)  

-- Move the @tabPlannedLabor to the lowest WBS Level
  -- 1. Planned labor under WBS1 Level need to move under the first WBS2 Level 
  ------OR Labor Code Level if there is no WBS2 level for the plan
     UPDATE  P SET P.WBS2 = childTask.WBS2, P.LaborCode = ISNULL(P.LaborCode, childTask.LaborCode)
     FROM @tabPlannedLabor AS P INNER JOIN
	 (SELECT TOP 1 WBS1,WBS2, WBS3,LaborCode,OutlineNumber FROM @tabTask WHERE PlanID = @strNewPlanID AND
	    (WBS2 IS NOT NULL OR (WBS2 IS NULL AND LaborCode IS NOT NULL ))
		  ORDER BY OutlineNumber ) AS childTask 
		  ON childTask.WBS1 = P.WBS1
     Where P.WBS2 IS NULL AND P.WBS3 IS NULL 



  -- 2. Planned labor under WBS2 Level need to move under the first WBS3 Level 
   ------OR First Labor Code Level if there is no WBS3 level under the phase

    DECLARE plannedLaborWBS2Cursor CURSOR FOR 
	SELECT WBS2 FROM @tabPlannedLabor 
	   WHERE WBS2 IS NOT NULL AND WBS3 IS NULL	   

	OPEN plannedLaborWBS2Cursor
	FETCH NEXT FROM plannedLaborWBS2Cursor INTO @strWBS2 
	WHILE @@FETCH_STATUS = 0 
	BEGIN			  
		 UPDATE  P SET P.WBS3 = childTask.WBS3, P.LaborCode = ISNULL(P.LaborCode, childTask.LaborCode)
		 FROM @tabPlannedLabor AS P 
        INNER JOIN
 		(SELECT TOP 1 WBS1,WBS2, WBS3, LaborCode,OutlineNumber FROM @tabTask WHERE PlanID = @strNewPlanID AND
		WBS2 = @strWBS2 AND
	    ((WBS2 IS NOT NULL AND WBS3 IS NOT NULL ) OR (WBS2 IS NOT NULL AND WBS3 IS NULL AND LaborCode IS NOT NULL ))
		  ORDER BY OutlineNumber ) AS childTask 
			  ON childTask.WBS1 = P.WBS1 AND  childTask.WBS2= P.WBS2
		 Where P.WBS2 = @strWBS2 AND P.WBS3 IS NULL  

 		 FETCH NEXT FROM plannedLaborWBS2Cursor INTO @strWBS2
    END --WHILE @@FETCH_STATUS = 0 
	CLOSE plannedLaborWBS2Cursor
	DEALLOCATE plannedLaborWBS2Cursor

  -- 3. Planned labor under WBS3 Level need to move under the first Labor code Level 

    DECLARE plannedLaborLaborCodeCursor CURSOR FOR 
	SELECT WBS3 FROM @tabPlannedLabor WHERE WBS3 IS NOT NULL AND LaborCode IS NULL

	OPEN plannedLaborLaborCodeCursor
	FETCH NEXT FROM plannedLaborLaborCodeCursor INTO @strWBS3 
	WHILE @@FETCH_STATUS = 0 
	BEGIN			  
		 UPDATE  P SET P.LaborCode = FirstTask.LaborCode
		 FROM @tabPlannedLabor AS P INNER JOIN
		 (SELECT TOP 1 WBS1,WBS2,WBS3,LaborCode FROM @tabTask WHERE PlanID = @strNewPlanID AND
			WBS3 = @strWBS3 AND LaborCode IS NOT NULL ORDER BY OutlineNumber ) AS FirstTask 
			  ON FirstTask.WBS1 = P.WBS1 AND  FirstTask.WBS2= P.WBS2 AND  FirstTask.WBS3= P.WBS3
		 Where P.WBS3 = @strWBS3 AND P.LaborCode IS NULL
		 FETCH NEXT FROM plannedLaborLaborCodeCursor INTO @strWBS3
    END --WHILE @@FETCH_STATUS = 0 
	CLOSE plannedLaborLaborCodeCursor
	DEALLOCATE plannedLaborLaborCodeCursor

------------------------------------------------------------------------------------------------------------------
	INSERT @tabBaselineLabor (
		PlanID ,
		TimePhaseID ,
		WBS1,
		WBS2,
		WBS3,
		LaborCode,
		ResourceID,
		GenericResourceID,
		StartDate,
		EndDate,
		CostRate,
		BillingRate,
		PeriodHrs,
		PeriodCost,
		PeriodBill	
	)
	SELECT  @strNewPlanID,
			P.TimePhaseID,
			A.WBS1,
			A.WBS2,
			A.WBS3,
			A.LaborCode,
			A.ResourceID,
			A.GenericResourceID,
			P.StartDate,
			P.EndDate,
			P.CostRate,
			P.BillingRate,
			P.PeriodHrs,
			P.PeriodCost,
			P.PeriodBill
			FROM  RPBaselineLabor P 
			      INNER JOIN RPAssignment A 
			      ON P.PlanID = A.PlanID AND P.TaskID = A.TaskID AND P.AssignmentID = A.AssignmentID
				  WHERE P.PlanID = @strOldPlanID AND (A.GenericResourceID IS NOT NULL OR A.ResourceID IS NOT NULL)  

  -- 1. Baseline labor under WBS1 Level need to move under the first WBS2 Level 
  ------OR Labor Code Level if there is no WBS2 level for the plan
     UPDATE  P SET P.WBS2 = childTask.WBS2, P.LaborCode = ISNULL(P.LaborCode, childTask.LaborCode)
     FROM @tabBaselineLabor AS P INNER JOIN
	 (SELECT TOP 1 WBS1,WBS2, WBS3,LaborCode,OutlineNumber FROM @tabTask WHERE PlanID = @strNewPlanID AND
	    (WBS2 IS NOT NULL OR (WBS2 IS NULL AND LaborCode IS NOT NULL ))
		  ORDER BY OutlineNumber ) AS childTask 
		  ON childTask.WBS1 = P.WBS1
     Where P.WBS2 IS NULL AND P.WBS3 IS NULL 

  -- 2. Baseline labor under WBS2 Level need to move under the first WBS3 Level 
   ------OR First Labor Code Level if there is no WBS3 level under the phase 
     
    DECLARE baseLineLaborWBS2Cursor CURSOR FOR 
	SELECT WBS2 FROM @tabBaselineLabor 
		 WHERE WBS2 IS NOT NULL AND WBS3 IS NULL	   

	OPEN baseLineLaborWBS2Cursor
	FETCH NEXT FROM baseLineLaborWBS2Cursor INTO @strWBS2 
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		 UPDATE  P SET P.WBS3 = childTask.WBS3, P.LaborCode = ISNULL(P.LaborCode, childTask.LaborCode)
		 FROM @tabBaselineLabor AS P 
        INNER JOIN
 		(SELECT TOP 1 WBS1,WBS2, WBS3,LaborCode,OutlineNumber FROM @tabTask WHERE PlanID = @strNewPlanID AND
		WBS2 = @strWBS2 AND
	    ((WBS2 IS NOT NULL AND WBS3 IS NOT NULL ) OR (WBS2 IS NOT NULL AND WBS3 IS NULL AND LaborCode IS NOT NULL ))
		 ORDER BY OutlineNumber ) AS childTask 
			  ON childTask.WBS1 = P.WBS1 AND  childTask.WBS2= P.WBS2
		 Where P.WBS2 = @strWBS2 AND P.WBS3 IS NULL  
	
		 FETCH NEXT FROM baseLineLaborWBS2Cursor INTO @strWBS2
    END --WHILE @@FETCH_STATUS = 0 
	CLOSE baseLineLaborWBS2Cursor
	DEALLOCATE baseLineLaborWBS2Cursor
 
   -- 3. Baseline labor under WBS3 Level need to move under the first Labor code Level 

    DECLARE baseLineLaborLaborCodeCursor CURSOR FOR 
	SELECT WBS3 FROM @tabBaselineLabor WHERE WBS3 IS NOT NULL AND LaborCode IS NULL

	OPEN baseLineLaborLaborCodeCursor
	FETCH NEXT FROM baseLineLaborLaborCodeCursor INTO @strWBS3 
	WHILE @@FETCH_STATUS = 0 
	BEGIN			  
		 UPDATE  P SET P.LaborCode = FirstTask.LaborCode
		 FROM @tabBaselineLabor AS P INNER JOIN
		 (SELECT TOP 1 WBS1,WBS2,WBS3,LaborCode FROM @tabTask WHERE PlanID = @strNewPlanID AND
			WBS3 = @strWBS3 AND LaborCode IS NOT NULL ORDER BY OutlineNumber ) AS FirstTask 
			  ON FirstTask.WBS1 = P.WBS1 AND  FirstTask.WBS2= P.WBS2 AND  FirstTask.WBS3= P.WBS3
		 Where P.WBS3 = @strWBS3 AND P.LaborCode IS NULL
		 FETCH NEXT FROM baseLineLaborLaborCodeCursor INTO @strWBS3
    END --WHILE @@FETCH_STATUS = 0 
	CLOSE baseLineLaborLaborCodeCursor
	DEALLOCATE baseLineLaborLaborCodeCursor
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  
  -- Save Revenue data into @tabRevenue
  IF @strMultiCurrencyEnabled = 'Y' AND @strReportAtBillingInBillingCurr = 'Y' AND @strProjectCurrencyCode <> @strBillingCurrencyCode	    
  BEGIN
  	INSERT @tabRevenue (
		OldPlanID ,
		TimePhaseID ,
		WBS1,
		WBS2,
		WBS3,
		StartDate,
		EndDate,
		PeriodCost,
		PeriodBill,
		StoredRevenueCurrency
	)
	SELECT  PlanID,
			TimePhaseID,
			WBS1,
			WBS2,
			WBS3,
			StartDate,
			EndDate,
			PeriodCost,
			PeriodBill,
			StoredRevenueCurrency
			FROM dbo.stRP$tabConvertedRevenue (@strOldPlanID,@strRevenueSetting,@strFeeByRowAndPeriod,'Y',@siPlannedRevenueMethod,@strReimbMethod)

  END
  
  ELSE --IF @strMultiCurrencyEnabled <> 'Y' -->Single Currency 
  BEGIN
  INSERT @tabRevenue (
		OldPlanID ,
		TimePhaseID ,
		WBS1,
		WBS2,
		WBS3,
		StartDate,
		EndDate,
		PeriodCost,
		PeriodBill,
		StoredRevenueCurrency
	)
	SELECT  PlanID,
			TimePhaseID,
			WBS1,
			WBS2,
			WBS3,
			StartDate,
			EndDate,
			PeriodCost,
			PeriodBill,
			StoredRevenueCurrency
			FROM dbo.stRP$tabConvertedRevenue (@strOldPlanID,@strRevenueSetting,@strFeeByRowAndPeriod,'N',@siPlannedRevenueMethod,@strReimbMethod)
  END

  -- Set the stored currency for the plan
  IF (SELECT COUNT(*) FROM @tabRevenue) > 0
  BEGIN
	SELECT TOP 1 @strStoredRevenueCurrency =  StoredRevenueCurrency FROM @tabRevenue
  END
  ELSE
  BEGIN
	SET @strStoredRevenueCurrency = 'P'
  END
  --
  -- Move the @tabRevenue to the lowest WBS Level
  -- 1. Revenue under WBS1 Level need to move under the first WBS2 Level 
     UPDATE  V SET V.WBS2 = FirstPhase.WBS2
     FROM @tabRevenue AS V INNER JOIN
	 (SELECT TOP 1 WBS1,WBS2 FROM @tabTask WHERE PlanID = @strNewPlanID AND
	    WBS2 IS NOT NULL AND WBS3 IS NULL ORDER BY OutlineNumber ) AS FirstPhase 
		  ON FirstPhase.WBS1 = V.WBS1
     Where V.WBS2 IS NULL AND V.WBS3 IS NULL

  -- 2. Revenue under WBS2 Level need to move under the first WBS3 Level 
     
    DECLARE revenueWBS2Cursor CURSOR FOR 
	SELECT WBS2 FROM @tabRevenue WHERE WBS2 IS NOT NULL AND WBS3 IS NULL

	OPEN revenueWBS2Cursor
	FETCH NEXT FROM revenueWBS2Cursor INTO @strWBS2 
	WHILE @@FETCH_STATUS = 0 
	BEGIN			  
		 UPDATE  V SET V.WBS3 = FirstTask.WBS3
		 FROM @tabRevenue AS V INNER JOIN
		 (SELECT TOP 1 WBS1,WBS2,WBS3 FROM @tabTask WHERE PlanID = @strNewPlanID AND
			WBS2 = @strWBS2 AND WBS3 IS NOT NULL ORDER BY OutlineNumber ) AS FirstTask 
			  ON FirstTask.WBS1 = V.WBS1 AND  FirstTask.WBS2= V.WBS2
		 Where V.WBS2 = @strWBS2 AND V.WBS3 IS NULL
		 FETCH NEXT FROM revenueWBS2Cursor INTO @strWBS2
    END --WHILE @@FETCH_STATUS = 0 
	CLOSE revenueWBS2Cursor
	DEALLOCATE revenueWBS2Cursor

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determine the minimum WBS level where Expense rows need to be attached.
  -- Start with the default ExpWBSLevel. If there is no Expense row in the Plan then @tiMinExpWBSLevel = ExpWBSLevel.
  -- If the Plan has Expense rows, then the highest level found will be used as ExpWBSLevel.

  SELECT @tiMinExpWBSLevel = ISNULL(MIN(WBSLevel), @tiDefExpWBSLevel) FROM (
    SELECT 1 AS WBSLevel
      FROM RPExpense AS C
        INNER JOIN PR ON C.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND C.WBS2 IS NULL AND C.WBS3 IS NULL AND PR.SubLevel = 'Y'
      WHERE C.PlanID = @strOldPlanID
    UNION
    SELECT 2 AS WBSLevel
      FROM RPExpense AS C
        INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND PR.WBS3 = ' ' AND C.WBS3 IS NULL AND PR.SubLevel = 'Y'
      WHERE C.PlanID = @strOldPlanID
    UNION
    SELECT 3 AS WBSLevel
      FROM RPExpense AS C
        INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND PR.WBS3 = ' ' AND C.WBS3 IS NULL AND PR.SubLevel = 'N'
      WHERE C.PlanID = @strOldPlanID
    UNION
    SELECT 3 AS WBSLevel
      FROM RPExpense AS C
        INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND C.WBS3 = PR.WBS3
      WHERE C.PlanID = @strOldPlanID
    ) AS X

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Determine the minimum WBS level where Consultant rows need to be attached.
  -- Start with the default ConWBSLevel. If there is no Consultant row in the Plan then @tiMinConWBSLevel = ConWBSLevel.
  -- If the Plan has Consultant rows, then the highest level found will be used as ConWBSLevel.

  SELECT @tiMinConWBSLevel = ISNULL(MIN(WBSLevel), @tiDefConWBSLevel) FROM (
    SELECT 1 AS WBSLevel
      FROM RPConsultant AS C
        INNER JOIN PR ON C.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' AND C.WBS2 IS NULL AND C.WBS3 IS NULL AND PR.SubLevel = 'Y'
      WHERE C.PlanID = @strOldPlanID
    UNION
    SELECT 2 AS WBSLevel
      FROM RPConsultant AS C
        INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND PR.WBS3 = ' ' AND C.WBS3 IS NULL AND PR.SubLevel = 'Y'
      WHERE C.PlanID = @strOldPlanID
    UNION
    SELECT 3 AS WBSLevel
      FROM RPConsultant AS C
        INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND PR.WBS3 = ' ' AND C.WBS3 IS NULL AND PR.SubLevel = 'N'
      WHERE C.PlanID = @strOldPlanID
    UNION
    SELECT 3 AS WBSLevel
      FROM RPConsultant AS C
        INNER JOIN PR ON C.WBS1 = PR.WBS1 AND C.WBS2 = PR.WBS2 AND C.WBS3 = PR.WBS3
      WHERE C.PlanID = @strOldPlanID
    ) AS X

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  INSERT @tabExpWBSTree(
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    WBSType,
    StartDate,
    EndDate
  )
    SELECT
      T.PlanID AS PlanID,
      T.TaskID,
      T.WBS1,
      T.WBS2,
      T.WBS3,
      T.WBSType,
      T.StartDate,
      T.EndDate
      FROM @tabTask AS T
      WHERE (OutlineLevel + 1) <= @tiMinExpWBSLevel AND T.WBSType <> 'LBCD'

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  INSERT @tabConWBSTree(
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    WBSType,
    StartDate,
    EndDate
  )
    SELECT
      T.PlanID AS PlanID,
      T.TaskID,
      T.WBS1,
      T.WBS2,
      T.WBS3,
      T.WBSType,
      T.StartDate,
      T.EndDate
      FROM @tabTask AS T
      WHERE (OutlineLevel + 1) <= @tiMinConWBSLevel AND T.WBSType <> 'LBCD'

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Collecting existing RPExpense rows to be used latter.
  -- Combine RPExpense rows together by WBS1, WBS2, WBS3, Account, Vendor.

  INSERT @tabExpense(
    OldPlanID,
    WBS1,
    WBS2,
    WBS3,
    Account,
    Vendor,
    DirectAcctFlg,
    PlannedExpCost,
    PlannedExpBill,
    BaselineExpCost,
    BaselineExpBill
  )
    SELECT
      E.OldPlanID AS OldPlanID,
      E.WBS1,
      E.WBS2,
      E.WBS3,
      E.Account AS Account,
      E.Vendor AS Vendor,
      E.DirectAcctFlg AS DirectAcctFlg,
      SUM(E.PlannedExpCost) AS PlannedExpCost,
      SUM(E.PlannedExpBill) AS PlannedExpBill,
      SUM(E.BaselineExpCost) AS BaselineExpCost,
      SUM(E.BaselineExpBill) AS BaselineExpBill
      FROM (
        SELECT
          XE.PlanID AS OldPlanID,
          XE.WBS1 AS WBS1,
          XE.WBS2 AS WBS2,
          XE.WBS3 AS WBS3,
          XE.Account AS Account,
          XE.Vendor AS Vendor,
          CASE WHEN CA.Type = 7 THEN 'Y' ELSE 'N' END AS DirectAcctFlg,
          SUM(XE.PlannedExpCost) AS PlannedExpCost,
          SUM(XE.PlannedExpBill) AS PlannedExpBill,
          SUM(XE.BaselineExpCost) AS BaselineExpCost,
          SUM(XE.BaselineExpBill) AS BaselineExpBill
          FROM RPExpense AS XE
            INNER JOIN CA ON XE.Account = CA.Account AND CA.Type IN (5, 7)
          WHERE XE.PlanID = @strOldPlanID 
          GROUP BY XE.PlanID, XE.WBS1, XE.WBS2, XE.WBS3, XE.Account, XE.Vendor, CA.Type
        UNION ALL          
        SELECT
          XU.PlanID AS OldPlanID,
          XU.WBS1 AS WBS1,
          XU.WBS2 AS WBS2,
          XU.WBS3 AS WBS3,
          XU.Account AS Account,
          NULL AS Vendor,
          CASE WHEN CA.Type = 7 THEN 'Y' ELSE 'N' END AS DirectAcctFlg,
          SUM(XU.PlannedUntCost) AS PlannedExpCost,
          SUM(XU.PlannedUntBill) AS PlannedExpBill,
          SUM(XU.BaselineUntCost) AS BaselineExpCost,
          SUM(XU.BaselineUntBill) AS BaselineExpBill
          FROM RPUnit AS XU
            INNER JOIN CA ON XU.Account = CA.Account AND CA.Type IN (5, 7)
          WHERE XU.PlanID = @strOldPlanID 
          GROUP BY XU.PlanID, XU.WBS1, XU.WBS2, XU.WBS3, XU.Account, CA.Type
      ) AS E
      GROUP BY E.OldPlanID, E.WBS1, E.WBS2, E.WBS3, E.Account, E.Vendor, E.DirectAcctFlg

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  -- Collecting existing RPConsultant rows to be used latter.
  -- Combine RPConsultant rows together by WBS1, WBS2, WBS3, Account, Vendor.

  INSERT @tabConsultant(
    OldPlanID,
    WBS1,
    WBS2,
    WBS3,
    Account,
    Vendor,
    DirectAcctFlg,
    PlannedConCost,
    PlannedConBill,
    BaselineConCost,
    BaselineConBill
  )
    SELECT
      C.OldPlanID AS OldPlanID,
      C.WBS1,
      C.WBS2,
      C.WBS3,
      C.Account AS Account,
      C.Vendor AS Vendor,
      C.DirectAcctFlg AS DirectAcctFlg,
      SUM(C.PlannedConCost) AS PlannedConCost,
      SUM(C.PlannedConBill) AS PlannedConBill,
      SUM(C.BaselineConCost) AS BaselineConCost,
      SUM(C.BaselineConBill) AS BaselineConBill
      FROM (
        SELECT
          XC.PlanID AS OldPlanID,
          XC.WBS1 AS WBS1,
          XC.WBS2 AS WBS2,
          XC.WBS3 AS WBS3,
          XC.Account AS Account,
          XC.Vendor AS Vendor,
          CASE WHEN CA.Type = 8 THEN 'Y' ELSE 'N' END AS DirectAcctFlg,
          SUM(XC.PlannedConCost) AS PlannedConCost,
          SUM(XC.PlannedConBill) AS PlannedConBill,
          SUM(XC.BaselineConCost) AS BaselineConCost,
          SUM(XC.BaselineConBill) AS BaselineConBill
          FROM RPConsultant AS XC
            INNER JOIN CA ON XC.Account = CA.Account AND CA.Type IN (6, 8)
          WHERE XC.PlanID = @strOldPlanID 
          GROUP BY XC.PlanID, XC.WBS1, XC.WBS2, XC.WBS3, XC.Account, XC.Vendor, CA.Type
        UNION ALL          
        SELECT
          XU.PlanID AS OldPlanID,
          XU.WBS1 AS WBS1,
          XU.WBS2 AS WBS2,
          XU.WBS3 AS WBS3,
          XU.Account AS Account,
          NULL AS Vendor,
          CASE WHEN CA.Type = 8 THEN 'Y' ELSE 'N' END AS DirectAcctFlg,
          SUM(XU.PlannedUntCost) AS PlannedConCost,
          SUM(XU.PlannedUntBill) AS PlannedConBill,
          SUM(XU.BaselineUntCost) AS BaselineConCost,
          SUM(XU.BaselineUntBill) AS BaselineConBill
          FROM RPUnit AS XU
            INNER JOIN CA ON XU.Account = CA.Account AND CA.Type IN (6, 8)
          WHERE XU.PlanID = @strOldPlanID 
          GROUP BY XU.PlanID, XU.WBS1, XU.WBS2, XU.WBS3, XU.Account, CA.Type
      ) AS C
      GROUP BY C.OldPlanID, C.WBS1, C.WBS2, C.WBS3, C.Account, C.Vendor, C.DirectAcctFlg

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Determining the Currency Code of Rate Tables based on Rate Methods

  SELECT @strEMCostCurrencyCode = 
    CASE 
      WHEN @siCostRtMethod = 2 THEN (SELECT CurrencyCode FROM BTRRT WHERE TableNo = @intCostRtTableNo)
      WHEN @siCostRtMethod = 3 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intCostRtTableNo)
      WHEN @siCostRtMethod = 4 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intCostRtTableNo)
    END

  SELECT @strEMBillCurrencyCode = 
    CASE 
      WHEN @siBillRtMethod = 2 THEN (SELECT CurrencyCode FROM BTRRT WHERE TableNo = @intBillRtTableNo)
      WHEN @siBillRtMethod = 3 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intBillRtTableNo)
      WHEN @siBillRtMethod = 4 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intBillRtTableNo)
    END

  SELECT @strGRCostCurrencyCode = 
    CASE 
      WHEN @siCostGRRtMethod = 0 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intGenResTableNo)
      WHEN @siCostGRRtMethod = 1 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intGenResTableNo)
    END

  SELECT @strGRBillCurrencyCode = 
    CASE 
      WHEN @siBillGRRtMethod = 0 THEN (SELECT CurrencyCode FROM BTRCT WHERE TableNo = @intGRBillTableNo)
      WHEN @siBillGRRtMethod = 1 THEN (SELECT CurrencyCode FROM BTRLT WHERE TableNo = @intGRBillTableNo)
    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collect RPAssignment rows that were hung at non-leaf level excluding the assignment under labor code level task, 
  -- the assignment under labor code level will be collected in different way based on if we need upgrade the labor code
  -- T.ChildrenCount > 0 will filter out the labor code assignment, since labor code level always has childrenCount = 0 , not > 0
    INSERT @tabNonLeafAssignment(
    OldPlanID,
	AssignmentID,
    WBS1,
    WBS2,
    WBS3,
	LaborCode,
    ResourceID,
    GenericResourceID,
    Leaf_OutlineNumber,
    Leaf_WBS2,
    Leaf_WBS3,
	Leaf_LaborCode,
    StartDate,
    EndDate,
    CostRate,
    BillingRate,
    PlannedLaborHrs,
    PlannedLabCost,
    PlannedLabBill,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill
  )
    SELECT DISTINCT
        A.PlanID AS OldPlanID,
		A.AssignmentID,
        A.WBS1 AS WBS1,
        A.WBS2 AS WBS2,
        A.WBS3 AS WBS3,
		A.LaborCode AS LaborCode,
        A.ResourceID AS ResourceID,
        A.GenericResourceID AS GenericResourceID,
        NULL AS Leaf_OutlineNumber,
        NULL AS Leaf_WBS2,
        NULL AS Leaf_WBS3,
		NULL AS Leaf_LaborCode,
        A.StartDate AS StartDate,
        A.EndDate AS EndDate,
		A.CostRate,
		A.BillingRate,
        A.PlannedLaborHrs AS PlannedLaborHrs,
        A.PlannedLabCost AS PlannedLabCost,
        A.PlannedLabBill AS PlannedLabBill,
        A.BaselineLaborHrs AS BaselineLaborHrs,
        A.BaselineLabCost AS BaselineLabCost,
        A.BaselineLabBill AS BaselineLabBill
        FROM RPAssignment AS A
                /* Select Assignment rows that were hung at non-leaf level */
                INNER JOIN @tabTask AS T ON T.ChildrenCount > 0
                  AND A.WBS1 = T.WBS1 AND ISNULL(A.WBS2, ' ') = ISNULL(T.WBS2, ' ') 
				  AND ISNULL(A.WBS3, ' ') = ISNULL(T.WBS3, ' ') AND ISNULL(A.LaborCode, ' ') = ISNULL(T.LaborCode, ' ')
				  WHERE A.PlanID = @strOldPlanID
                /* Assignment rows must always have either ResourceID or GenericResourceID */
                AND ((ResourceID IS NOT NULL AND GenericResourceID IS NULL) OR (ResourceID IS NULL AND GenericResourceID IS NOT NULL))

				
  --Now we need to set leaf WBS2/WBS3/LaborCode for all above non-leaf assignments  
  --SET Leaf_WBS2/Leaf_WBS3/Leaf_LaborCode for All non leaf Assignment under WBS1   
	UPDATE A SET A.Leaf_WBS2 = childTask.WBS2, 
				 A.Leaf_WBS3 = childTask.WBS3,
				 A.Leaf_LaborCode = childTask.LaborCode,
				 A.leaf_OutLineNumber = childTask.OutLineNumber
	FROM @tabNonLeafAssignment AS A INNER JOIN
	 (SELECT TOP 1 WBS1,WBS2, WBS3,LaborCode,OutlineNumber FROM @tabTask WHERE PlanID = @strNewPlanID AND
	    (WBS2 IS NOT NULL OR (WBS2 IS NULL AND LaborCode IS NOT NULL ))
		AND ChildrenCount = 0 ORDER BY OutlineNumber ) AS childTask 
		  ON childTask.WBS1 = A.WBS1
     Where A.OldPlanID = @strOldPlanID  AND A.WBS2 IS NULL AND A.LaborCode IS NULL


	--SET Leaf_WBS3/Leaf_LaborCode for All non leaf Assignment under WBS2 

		DECLARE AssignmentCursor CURSOR FOR 
		SELECT  AssignmentID FROM @tabNonLeafAssignment WHERE WBS2 IS NOT NULL AND WBS3 IS NULL

		OPEN AssignmentCursor
		FETCH NEXT FROM AssignmentCursor INTO @strAssignmentID 
		WHILE @@FETCH_STATUS = 0 
		BEGIN	
			UPDATE A SET  A.Leaf_WBS2 = childTask.WBS2, 
				          A.Leaf_WBS3 = childTask.WBS3,
				          A.Leaf_LaborCode = childTask.LaborCode,
						  A.leaf_OutLineNumber = childTask.OutLineNumber
			FROM @tabNonLeafAssignment AS A 
			INNER JOIN
 			(SELECT TOP 1 T.WBS1,T.WBS2, T.WBS3,T.LaborCode,T.OutLineNumber FROM @tabTask T
			INNER JOIN @tabNonLeafAssignment A ON  
			A.WBS2 = T.WBS2 WHERE A.AssignmentID = @strAssignmentID
			AND (T.WBS2 IS NOT NULL  AND T.WBS3 IS NOT NULL 
			     OR (T.WBS2 IS NOT NULL  AND T.WBS3 IS NULL AND T.LaborCode IS NOT NULL)
				)
			AND T.ChildrenCount = 0 ORDER BY T.OutlineNumber) AS childTask 	         
		    ON childTask.WBS1 = A.WBS1 AND childTask.WBS2 = A.WBS2  		
			Where A.AssignmentID = @strAssignmentID --AND A.OldPlanID = @strOldPlanID  		
			FETCH NEXT FROM AssignmentCursor INTO @strAssignmentID
		END --WHILE @@FETCH_STATUS = 0 
	  CLOSE AssignmentCursor
	  DEALLOCATE AssignmentCursor	 
	  

	-- if need to upgrade labor code, need to move the non leaf task level to labor code level
	IF  @siLCLevels > 0 AND @bNeedUpdateLaborCode = 1
	BEGIN

		--SET Leaf_LaborCode for All non leaf Assignment under WBS2   
		DECLARE AssignmentCursor CURSOR FOR 
		SELECT  AssignmentID FROM @tabNonLeafAssignment WHERE WBS2 IS NOT NULL AND WBS3 IS NOT NULL

		OPEN AssignmentCursor
		FETCH NEXT FROM AssignmentCursor INTO @strAssignmentID 
		WHILE @@FETCH_STATUS = 0 
		BEGIN			 			  
			UPDATE A SET  A.Leaf_WBS2 = ParentTask.WBS2, 
				          A.Leaf_WBS3 = ParentTask.WBS3,
				          A.Leaf_LaborCode = ParentTask.LaborCode				
			FROM @tabNonLeafAssignment AS A 
			INNER JOIN
 			(SELECT TOP 1 T.WBS1,T.WBS2, T.WBS3,T.LaborCode FROM @tabTask T
			INNER JOIN @tabNonLeafAssignment A ON  
			A.WBS2 = T.WBS2 AND A.WBS3 = T.WBS3 WHERE A.AssignmentID = @strAssignmentID
			AND T.WBS2 IS NOT NULL  AND T.WBS3 IS NOT NULL AND T.LaborCode IS NOT NULL AND T.ChildrenCount = 0 ORDER BY T.OutlineNumber) AS ParentTask 	         
		    ON ParentTask.WBS1 = A.WBS1 AND ParentTask.WBS2 = A.WBS2 AND ParentTask.WBS3 = A.WBS3			
			Where A.AssignmentID = @strAssignmentID --AND A.OldPlanID = @strOldPlanID  			 
			FETCH NEXT FROM AssignmentCursor INTO @strAssignmentID
		END --WHILE @@FETCH_STATUS = 0 
	  CLOSE AssignmentCursor
	  DEALLOCATE AssignmentCursor	 
	END --IF  @siLCLevels > 0 AND @bNeedUpdateLaborCode = 1

  IF  NOT (@siLCLevels > 0 AND @bNeedUpdateLaborCode = 1) --no need to upgrade the labor code level task
  -- if not upgrade labor code level assignment, treat them as non leaf assignment also	
  -- Collect RPAssignment rows that were hung at LBCD level.
  BEGIN
  INSERT @tabNonLeafAssignment(
    OldPlanID,
	AssignmentID,
    WBS1,
    WBS2,
    WBS3,
    ResourceID,
    GenericResourceID,
    Leaf_OutlineNumber,
    Leaf_WBS2,
    Leaf_WBS3,
    StartDate,
    EndDate,
    CostRate,
    BillingRate,
    PlannedLaborHrs,
    PlannedLabCost,
    PlannedLabBill,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill
  )

    SELECT DISTINCT
      ZA.OldPlanID AS OldPlanID,
	  MAX(ZA.AssignmentID) AS AssignmentID,
      ZA.WBS1 AS WBS1,
      MAX(ZA.WBS2) AS WBS2,
      MAX(ZA.WBS3) AS WBS3,
      ZA.ResourceID AS ResourceID,
      ZA.GenericResourceID AS GenericResourceID,
      ZA.Leaf_OutlineNumber AS Leaf_OutlineNumber,
      ZA.Leaf_WBS2 AS Leaf_WBS2,
      ZA.Leaf_WBS3 AS Leaf_WBS3,
      MIN(ZA.StartDate) AS StartDate,
      MAX(ZA.EndDate) AS EndDate,
      CASE
        WHEN SUM(ZA.PlannedLaborHrs) <> 0
        THEN SUM(ZA.PlannedLabCost) / SUM(ZA.PlannedLaborHrs)
        ELSE 0
      END AS CostRate,
      CASE
        WHEN SUM(ZA.PlannedLaborHrs) <> 0
        THEN SUM(ZA.PlannedLabBill) / SUM(ZA.PlannedLaborHrs)
        ELSE 0
      END AS BillingRate,
      SUM(ZA.PlannedLaborHrs) AS PlannedLaborHrs,
      SUM(ZA.PlannedLabCost) AS PlannedLabCost,
      SUM(ZA.PlannedLabBill) AS PlannedLabBill,
      SUM(ZA.BaselineLaborHrs) AS BaselineLaborHrs,
      SUM(ZA.BaselineLabCost) AS BaselineLabCost,
      SUM(ZA.BaselineLabBill) AS BaselineLabBill
      FROM ( /* ZA */
        /* Collect RPAssignment rows that were hung at LBCD level. */
        SELECT 
          XA.OldPlanID AS OldPlanID,
		  MAX(XA.AssignmentID) AS AssignmentID,
          XA.WBS1 AS WBS1,
          XA.WBS2 AS WBS2,
          XA.WBS3 AS WBS3,
          XA.ResourceID AS ResourceID,
          XA.GenericResourceID AS GenericResourceID,
          XA.Leaf_OutlineNumber AS Leaf_OutlineNumber,
          XA.Leaf_WBS2 AS Leaf_WBS2,
          XA.Leaf_WBS3 AS Leaf_WBS3,
          MIN(XA.StartDate) AS StartDate,
          MAX(XA.EndDate) AS EndDate,
          SUM(XA.PlannedLaborHrs) AS PlannedLaborHrs,
          SUM(XA.PlannedLabCost) AS PlannedLabCost,
          SUM(XA.PlannedLabBill) AS PlannedLabBill,
          SUM(XA.BaselineLaborHrs) AS BaselineLaborHrs,
          SUM(XA.BaselineLabCost) AS BaselineLabCost,
          SUM(XA.BaselineLabBill) AS BaselineLabBill
          FROM ( /* XA */
            SELECT --DISTINCT
              A.PlanID AS OldPlanID,
			  A.AssignmentID AS AssignmentID,
              A.WBS1 AS WBS1,
              A.WBS2 AS WBS2,
              A.WBS3 AS WBS3,
              A.ResourceID AS ResourceID,
              A.GenericResourceID AS GenericResourceID,
              PT.OutlineNumber AS Leaf_OutlineNumber,
              PT.WBS2 AS Leaf_WBS2,
              PT.WBS3 AS Leaf_WBS3,
              A.StartDate AS StartDate,
              A.EndDate AS EndDate,
              A.PlannedLaborHrs AS PlannedLaborHrs,
              A.PlannedLabCost AS PlannedLabCost,
              A.PlannedLabBill AS PlannedLabBill,
              A.BaselineLaborHrs AS BaselineLaborHrs,
              A.BaselineLabCost AS BaselineLabCost,
              A.BaselineLabBill AS BaselineLabBill
              FROM RPAssignment AS A
                /* Select Assignment rows that were hung at LBCD level */
                INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID 
                  AND AT.WBSType = 'LBCD' AND AT.LaborCode IS NOT NULL
                /* Determine OutlineNumber of parent row for a branch to move Assignment rows to */
                /* Note that the latest Project Structure could be different than the structure in RPTask, */
                /* therefore, must use the latest structure in @tabTask to determine the new OutlineNumber. */
                INNER JOIN @tabTask AS PT ON PT.WBSType IN ('WBS1', 'WBS2', 'WBS3')
                  AND A.WBS1 = PT.WBS1 AND ISNULL(A.WBS2, ' ') = ISNULL(PT.WBS2, ' ') AND ISNULL(A.WBS3, ' ') = ISNULL(PT.WBS3, ' ')
              WHERE A.PlanID = @strOldPlanID
                /* Assignment rows must always have either ResourceID or GenericResourceID */
                AND ((ResourceID IS NOT NULL AND GenericResourceID IS NULL) OR (ResourceID IS NULL AND GenericResourceID IS NOT NULL))
          ) AS XA
          GROUP BY XA.OldPlanID, XA.WBS1, XA.WBS2, XA.WBS3, XA.ResourceID, XA.GenericResourceID,
            XA.Leaf_OutlineNumber, XA.Leaf_WBS2, XA.Leaf_WBS3

      ) AS ZA
        GROUP BY ZA.OldPlanID, ZA.WBS1, /*ZA.WBS2, ZA.WBS3,*/ ZA.ResourceID, ZA.GenericResourceID,
          ZA.Leaf_OutlineNumber, ZA.Leaf_WBS2, ZA.Leaf_WBS3

				
  --Now we need to set leaf WBS2/WBS3/LaborCode for all above non-leaf assignments  
  --SET Leaf_WBS2/Leaf_WBS3/Leaf_LaborCode for All non leaf Assignment under WBS1   
	UPDATE A SET A.Leaf_WBS2 = childTask.WBS2, 
				 A.Leaf_WBS3 = childTask.WBS3,
				 A.Leaf_LaborCode = childTask.LaborCode,
				 A.leaf_OutLineNumber = childTask.OutLineNumber
	FROM @tabNonLeafAssignment AS A INNER JOIN
	 (SELECT TOP 1 WBS1,WBS2, WBS3,LaborCode,OutlineNumber FROM @tabTask WHERE PlanID = @strNewPlanID AND
	    (WBS2 IS NOT NULL OR (WBS2 IS NULL AND LaborCode IS NOT NULL ))
		AND ChildrenCount = 0 ORDER BY OutlineNumber ) AS childTask 
		  ON childTask.WBS1 = A.WBS1
     Where A.OldPlanID = @strOldPlanID  AND A.WBS2 IS NULL AND A.LaborCode IS NULL


	--SET Leaf_WBS3/Leaf_LaborCode for All non leaf Assignment under WBS2 

		DECLARE AssignmentCursor CURSOR FOR 
		SELECT  AssignmentID FROM @tabNonLeafAssignment WHERE WBS2 IS NOT NULL AND WBS3 IS NULL

		OPEN AssignmentCursor
		FETCH NEXT FROM AssignmentCursor INTO @strAssignmentID 
		WHILE @@FETCH_STATUS = 0 
		BEGIN	
			UPDATE A SET  A.Leaf_WBS2 = childTask.WBS2, 
				          A.Leaf_WBS3 = childTask.WBS3,
				          A.Leaf_LaborCode = childTask.LaborCode			
			FROM @tabNonLeafAssignment AS A 
			INNER JOIN
 			(SELECT TOP 1 T.WBS1,T.WBS2, T.WBS3,T.LaborCode FROM @tabTask T
			INNER JOIN @tabNonLeafAssignment A ON  
			A.WBS2 = T.WBS2 WHERE A.AssignmentID = @strAssignmentID
			AND (T.WBS2 IS NOT NULL  AND T.WBS3 IS NOT NULL 
			     OR (T.WBS2 IS NOT NULL  AND T.WBS3 IS NULL AND T.LaborCode IS NOT NULL)
				)
			AND T.ChildrenCount = 0 ORDER BY T.OutlineNumber) AS childTask 	         
		    ON childTask.WBS1 = A.WBS1 AND childTask.WBS2 = A.WBS2  		
			Where A.AssignmentID = @strAssignmentID --AND A.OldPlanID = @strOldPlanID  		
			FETCH NEXT FROM AssignmentCursor INTO @strAssignmentID
		END --WHILE @@FETCH_STATUS = 0 
	  CLOSE AssignmentCursor
	  DEALLOCATE AssignmentCursor	 

	END --IF  NOT (@siLCLevels > 0 AND @bNeedUpdateLaborCode = 1 )

	--Now we combine all non leaf assignment with the same leaf wbs1/wbs2/wbs3/laborcode, resourceid, genericrResourceID.
	INSERT @tabCombinedNonLeafAssignment(
	OldPlanID,
    WBS1,
    WBS2,
    WBS3,
	LaborCode,
	AssignmentID,
	ResourceID,
	GenericResourceID,
    StartDate,
    EndDate,
    CostRate,
    BillingRate,
    PlannedLaborHrs,
    PlannedLabCost,
    PlannedLabBill,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill
	)
	SELECT 
	Max(A.OldPlanID) AS PlanID,
	Max(A.WBS1) AS WBS1,
    Max(A.Leaf_WBS2) AS WBS2,
    Max(A.Leaf_WBS3) AS WBS3,
	Max(A.Leaf_LaborCode) AS LaborCode,
	Max(A.AssignmentID) AS AssignmentID,
	Max(A.ResourceID) AS ResourceID,
	Max(A.GenericResourceID) AS GenericResourceID,
    MIN(A.StartDate) AS StartDate,
    MAX(A.EndDate) AS EndDate,
    CASE
    WHEN SUM(A.PlannedLaborHrs) <> 0
    THEN SUM(A.PlannedLabCost) / SUM(A.PlannedLaborHrs)
    ELSE 0
    END AS CostRate,
    CASE
    WHEN SUM(A.PlannedLaborHrs) <> 0
    THEN SUM(A.PlannedLabBill) / SUM(A.PlannedLaborHrs)
    ELSE 0
    END AS BillingRate,
    SUM(A.PlannedLaborHrs) AS PlannedLaborHrs,
    SUM(A.PlannedLabCost) AS PlannedLabCost,
    SUM(A.PlannedLabBill) AS PlannedLabBill,
    SUM(A.BaselineLaborHrs) AS BaselineLaborHrs,
    SUM(A.BaselineLabCost) AS BaselineLabCost,
    SUM(A.BaselineLabBill) AS BaselineLabBill
	FROM @tabNonLeafAssignment A 
	GROUP BY A.OldPlanID,A.WBS1,A.Leaf_WBS2,A.Leaf_WBS3,A.Leaf_LaborCode,A.ResourceID,A.GenericResourceID		  		 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  
  
  DELETE PNCalendarInterval WHERE PlanID = @strOldPlanID
  DELETE PNAccordionFormat WHERE PlanID = @strOldPlanID
  DELETE PNWBSLevelFormat WHERE PlanID = @strOldPlanID

  DELETE PNPlannedLabor WHERE PlanID = @strOldPlanID
  DELETE PNBaselineLabor WHERE PlanID = @strOldPlanID
  DELETE PNAssignment WHERE PlanID = @strOldPlanID
  DELETE PNPlannedExpenses WHERE PlanID = @strOldPlanID
  DELETE PNBaselineExpenses WHERE PlanID = @strOldPlanID
  DELETE PNExpense WHERE PlanID = @strOldPlanID
  DELETE PNPlannedConsultant WHERE PlanID = @strOldPlanID
  DELETE PNBaselineConsultant WHERE PlanID = @strOldPlanID
  DELETE PNConsultant WHERE PlanID = @strOldPlanID
  DELETE PNTask WHERE PlanID = @strOldPlanID
  DELETE PNPlan WHERE PlanID = @strOldPlanID
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Delete data in RP tables that will not be carried over to PN tables.

  DELETE RPCompensationFee WHERE PlanID = @strOldPlanID
  DELETE RPConsultantFee WHERE PlanID = @strOldPlanID
  DELETE RPReimbAllowance WHERE PlanID = @strOldPlanID
  DELETE RPEVT WHERE PlanID = @strOldPlanID
  DELETE RPDependency WHERE PlanID = @strOldPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT PNPlan(
    PlanID,
    PlanName,
    PlanNumber,
    OpportunityID,
    ClientID,
    ProjMgr,
    Principal,
    Supervisor,
    Org,
    StartDate,
    EndDate,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    BaselineExpCost,
    BaselineExpBill,
    BaselineConCost,
    BaselineConBill,
    BaselineRevenue,
    BaselineStart,
    BaselineFinish,
    PlannedLaborHrs,
    PlannedLabCost,
    PlannedLabBill,
    PlannedExpCost,
    PlannedExpBill,
    PlannedConCost,
    PlannedConBill,
    LabRevenue,
    PlannedDirExpCost,
    PlannedDirExpBill,
    PlannedDirConCost,
    PlannedDirConBill,
    CostRtMethod,
    BillingRtMethod,
    CostRtTableNo,
    BillingRtTableNo,
    GRMethod,
    CostGRRtMethod,
    BillGRRtMethod,
    GenResTableNo,
    GRBillTableNo,
    ContingencyPct,
    ContingencyAmt,
    OverheadPct,
    AvailableFlg,
    UnPostedFlg,
    BudgetType,
    PctCompleteFormula,
    PctComplete,
    PctCompleteBill,
    PctCompleteLabCost,
    PctCompleteLabBill,
    PctCompleteExpCost,
    PctCompleteExpBill,
    PctCompleteConCost,
    PctCompleteConBill,
    WeightedLabCost,
    WeightedLabBill,
    WeightedExpCost,
    WeightedExpBill,
    WeightedConCost,
    WeightedConBill,
    UtilizationIncludeFlg,
    WBS1,
    Probability,
    LabMultType,
    Multiplier,
    ProjectedMultiplier,
    ProjectedRatio,
    CalcExpBillAmtFlg,
    ExpBillRtMethod,
    ExpBillRtTableNo,
    ExpBillMultiplier,
    CalcConBillAmtFlg,
    ConBillRtMethod,
    ConBillRtTableNo,
    ConBillMultiplier,
    ReimbMethod,
    ExpRevenue,
    ConRevenue,
    TargetMultCost,
    TargetMultBill,
    BaselineUntQty,
    BaselineUntCost,
    BaselineUntBill,
    PlannedUntQty,
    PlannedUntCost,
    PlannedUntBill,
    PctCompleteUntCost,
    PctCompleteUntBill,
    PlannedDirUntCost,
    PlannedDirUntBill,
    WeightedUntCost,
    WeightedUntBill,
    UntRevenue,
    Status,
    PctComplByPeriodFlg,
    RevenueMethod,
    AnalysisBasis,
    Company,
    CostCurrencyCode,
    BillingCurrencyCode,
    CompensationFee,
    ConsultantFee,
    ReimbAllowance,
    CompensationFeeBill,
    ConsultantFeeBill,
    ReimbAllowanceBill,
    CompensationFeeDirLab,
    CompensationFeeDirExp,
    ReimbAllowanceExp,
    ReimbAllowanceCon,
    CompensationFeeDirLabBill,
    CompensationFeeDirExpBill,
    ReimbAllowanceExpBill,
    ReimbAllowanceConBill,
    EVFormula,
    LabBillMultiplier,
    UntBillMultiplier,
    BaselineDirExpCost,
    BaselineDirExpBill,
    BaselineDirConCost,
    BaselineDirConBill,
    BaselineDirUntCost,
    BaselineDirUntBill,
    CommitmentFlg,
    TopdownFee,
    NeedsRecalc,
    ExpWBSLevel,
    ConWBSLevel,
    CheckedOutUser,
    CheckedOutDate,
    CheckedOutID,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      @strNewPlanID AS PlanID,
      P.PlanName,
      P.PlanNumber,
      NULL AS OpportunityID, -- Project Plan shall not have OpportunityID.
      P.ClientID,
      P.ProjMgr,
      P.Principal,
      P.Supervisor,
      P.Org,
      P.StartDate,
      P.EndDate,
      BaselineLaborHrs,
      BaselineLabCost,
      BaselineLabBill,
      BaselineExpCost,
      BaselineExpBill,
      BaselineConCost,
      BaselineConBill,
      BaselineRevenue,
      BaselineStart,
      BaselineFinish,
      PlannedLaborHrs,
      PlannedLabCost,
      PlannedLabBill,
      PlannedExpCost,
      PlannedExpBill,
      PlannedConCost,
      PlannedConBill,
      LabRevenue,
      PlannedDirExpCost,
      PlannedDirExpBill,
      PlannedDirConCost,
      PlannedDirConBill,

      CASE WHEN P.CostRtMethod = 4 THEN 2 ELSE P.CostRtMethod END AS CostRtMethod,
      CASE WHEN P.BillingRtMethod = 4 THEN 2 ELSE P.BillingRtMethod END AS BillingRtMethod,

      CASE WHEN P.CostRtMethod = 4 THEN 0
      ELSE
        CASE WHEN @strMultiCurrencyEnabled <> 'Y' THEN P.CostRtTableNo
        ELSE
          CASE WHEN @strEMCostCurrencyCode = PR.ProjectCurrencyCode THEN P.CostRtTableNo ELSE 0 END
        END
      END AS CostRtTableNo,

      CASE WHEN P.BillingRtMethod = 4 THEN 0
      ELSE
        CASE WHEN @strMultiCurrencyEnabled <> 'Y' THEN P.BillingRtTableNo
        ELSE
          CASE WHEN @strEMBillCurrencyCode = CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.BillingCurrencyCode ELSE PR.ProjectCurrencyCode END
               THEN P.BillingRtTableNo
               ELSE 0
          END
        END
      END AS BillingRtTableNo,
      
      @siGRMethod AS GRMethod,
      @siCostGRRtMethod AS CostGRRtMethod,
      @siBillGRRtMethod AS BillGRRtMethod,

      CASE WHEN @strMultiCurrencyEnabled <> 'Y' THEN P.GenResTableNo
      ELSE
        CASE WHEN @strGRCostCurrencyCode = PR.ProjectCurrencyCode THEN P.GenResTableNo ELSE 0 END 
      END AS GenResTableNo,

      CASE WHEN @strMultiCurrencyEnabled <> 'Y' THEN P.GRBillTableNo
      ELSE
        CASE WHEN @strGRBillCurrencyCode = CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.BillingCurrencyCode ELSE PR.ProjectCurrencyCode END
             THEN P.GRBillTableNo
             ELSE 0
        END 
      END AS GRBillTableNo,

      ContingencyPct,
      ContingencyAmt,
      OverheadPct,
      'Y' AS AvailableFlg,
      'Y' AS UnPostedFlg,
      BudgetType,
      2 AS PctCompleteFormula, -- JTD / (JTD + ETC)
      PctComplete,
      PctCompleteBill,
      PctCompleteLabCost,
      PctCompleteLabBill,
      PctCompleteExpCost,
      PctCompleteExpBill,
      PctCompleteConCost,
      PctCompleteConBill,
      WeightedLabCost,
      WeightedLabBill,
      WeightedExpCost,
      WeightedExpBill,
      WeightedConCost,
      WeightedConBill,
      'Y' AS UtilizationIncludeFlg,
      P.WBS1,
      P.Probability,
      CASE BudgetType
        WHEN 'C' THEN 1 /* Planned Multiplier */
        WHEN 'B' THEN 3 /* Planned Ratio */
        WHEN 'A' THEN
          CASE 
            WHEN LabMultType IN (0, 1) THEN 1 /* Planned Multiplier */
            WHEN LabMultType IN (2, 3) THEN 3 /* Planned Ratio */
          END
      END AS LabMultType,
      Multiplier,
      ProjectedMultiplier,
      ProjectedRatio,
      @strCalcExpBillAmtFlg AS CalcExpBillAmtFlg,
      CASE WHEN P.CalcExpBillAmtFlg = 'N' THEN 0 ELSE ExpBillRtMethod END AS ExpBillRtMethod,
      CASE WHEN P.CalcExpBillAmtFlg = 'N' THEN 0 ELSE ExpBillRtTableNo END AS ExpBillRtTableNo,
      CASE WHEN P.CalcExpBillAmtFlg = 'N' THEN 1.0000 ELSE ExpBillMultiplier END AS ExpBillMultiplier,
      @strCalcConBillAmtFlg AS CalcConBillAmtFlg,
      CASE WHEN P.CalcConBillAmtFlg = 'N' THEN 0 ELSE ConBillRtMethod END AS ConBillRtMethod,
      CASE WHEN P.CalcConBillAmtFlg = 'N' THEN 0 ELSE ConBillRtTableNo END AS ConBillRtTableNo,
      CASE WHEN P.CalcConBillAmtFlg = 'N' THEN 1.0000 ELSE ConBillMultiplier END AS ConBillMultiplier,
      ReimbMethod,
      ExpRevenue,
      ConRevenue,
      TargetMultCost,
      TargetMultBill,
      BaselineUntQty,
      BaselineUntCost,
      BaselineUntBill,
      PlannedUntQty,
      PlannedUntCost,
      PlannedUntBill,
      PctCompleteUntCost,
      PctCompleteUntBill,
      PlannedDirUntCost,
      PlannedDirUntBill,
      WeightedUntCost,
      WeightedUntBill,
      UntRevenue,
      PR.Status AS Status,
      PctComplByPeriodFlg,
      P.RevenueMethod,
      AnalysisBasis,
      P.Company,
      PR.ProjectCurrencyCode AS CostCurrencyCode,
      CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.BillingCurrencyCode ELSE PR.ProjectCurrencyCode END AS BillingCurrencyCode,

      PR.Fee AS CompensationFee,
      PR.ConsultFee AS ConsultantFee,
      PR.ReimbAllow AS ReimbAllowance,

      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeBillingCurrency ELSE PR.Fee END) AS CompensationFeeBill,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ConsultFeeBillingCurrency ELSE PR.ConsultFee END) AS ConsultantFeeBill,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowBillingCurrency ELSE PR.ReimbAllow END) AS ReimbAllowanceBill,

      PR.FeeDirLab AS CompensationFeeDirLab,
      PR.FeeDirExp AS CompensationFeeDirExp,
      PR.ReimbAllowExp AS ReimbAllowanceExp,
      PR.ReimbAllowCons AS ReimbAllowanceCon,

      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirLabBillingCurrency ELSE PR.FeeDirLab END) AS CompensationFeeDirLabBill,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.FeeDirExpBillingCurrency ELSE PR.FeeDirExp END) AS CompensationFeeDirExpBill,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowExpBillingCurrency ELSE PR.ReimbAllowExp END) AS ReimbAllowanceExpBill,
      (CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN PR.ReimbAllowConsBillingCurrency ELSE PR.ReimbAllowCons END) AS ReimbAllowanceConBill,

      EVFormula,
      LabBillMultiplier,
      UntBillMultiplier,
      BaselineDirExpCost,
      BaselineDirExpBill,
      BaselineDirConCost,
      BaselineDirConBill,
      BaselineDirUntCost,
      BaselineDirUntBill,
      CommitmentFlg,
      TopdownFee,
      NeedsRecalc,
      @tiMinExpWBSLevel AS ExpWBSLevel,
      @tiMinConWBSLevel AS ConWBSLevel,
      P.CheckedOutUser,
      P.CheckedOutDate,
      P.CheckedOutID,
      P.CreateUser,
      P.CreateDate,
      @strUserName AS ModUser,
      GETUTCDATE() AS ModDate
      FROM RPPlan AS P
        LEFT JOIN PR ON P.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
      WHERE P.PlanID = @strOldPlanID

	  IF  @siLCLevels > 0 AND @bNeedUpdateLaborCode = 1 --Need to upgrade the labor code level task
		BEGIN
		-- set the plan setting about labor code level allowing wildcard.
		UPDATE PNPlan SET LCLevel1Enabled = @strLCLevel1Enabled,
						  LCLevel2Enabled = @strLCLevel2Enabled,
						  LCLevel3Enabled = @strLCLevel3Enabled,
						  LCLevel4Enabled = @strLCLevel4Enabled,
						  LCLevel5Enabled = @strLCLevel5Enabled
			WHERE PlanID = @strNewPlanID
		END
  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  INSERT PNTask(
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
	LaborCode,
    Name,
    ParentOutlineNumber,
    OutlineNumber,
    ChildrenCount,
    OutlineLevel,
    ChargeType,
    ProjectType,
    WBSType,
    Org,
    ClientID,
    ProjMgr,
    StartDate,
    EndDate,
    Status,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    BaselineExpCost,
    BaselineExpBill,
    BaselineConCost,
    BaselineConBill,
    BaselineUntQty,
    BaselineUntCost,
    BaselineUntBill,
    BaselineRevenue,
    BaselineDirExpCost,
    BaselineDirExpBill,
    BaselineDirConCost,
    BaselineDirConBill,
    BaselineDirUntCost,
    BaselineDirUntBill,
    BaselineStart,
    BaselineFinish,
    CompensationFee,
    ConsultantFee,
    ReimbAllowance,
    CompensationFeeBill,
    ConsultantFeeBill,
    ReimbAllowanceBill,
    CompensationFeeDirLab,
    CompensationFeeDirExp,
    ReimbAllowanceExp,
    ReimbAllowanceCon,
    CompensationFeeDirLabBill,
    CompensationFeeDirExpBill,
    ReimbAllowanceExpBill,
    ReimbAllowanceConBill,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT
      PlanID,
      TaskID,
      WBS1,
      WBS2,
      WBS3,
	  LaborCode,
      Name,
      ParentOutlineNumber,
      OutlineNumber,
      ChildrenCount,
      OutlineLevel,
      ChargeType,
      ProjectType,
      WBSType,
      Org,
      ClientID,
      ProjMgr,
      StartDate,
      EndDate,
      Status,
      BaselineLaborHrs,
      BaselineLabCost,
      BaselineLabBill,
      BaselineExpCost,
      BaselineExpBill,
      BaselineConCost,
      BaselineConBill,
      BaselineUntQty,
      BaselineUntCost,
      BaselineUntBill,
      BaselineRevenue,
      BaselineDirExpCost,
      BaselineDirExpBill,
      BaselineDirConCost,
      BaselineDirConBill,
      BaselineDirUntCost,
      BaselineDirUntBill,
      BaselineStart,
      BaselineFinish,
      CompensationFee,
      ConsultantFee,
      ReimbAllowance,
      CompensationFeeBill,
      ConsultantFeeBill,
      ReimbAllowanceBill,
      CompensationFeeDirLab,
      CompensationFeeDirExp,
      ReimbAllowanceExp,
      ReimbAllowanceCon,
      CompensationFeeDirLabBill,
      CompensationFeeDirExpBill,
      ReimbAllowanceExpBill,
      ReimbAllowanceConBill,
      @strUserName AS CreateUser,
      GETUTCDATE() AS CreateDate,
      @strUserName AS ModUser,
      GETUTCDATE() AS ModDate
      FROM @tabTask AS T
  
  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  -- Inserting non-leaf Assignments at new leaf locations using Leaf-OutlineNumber.
  -- Need to move non-leaf Assignments to PNAssignment first to avoid duplicating at both leaf and non-leaf locations.

  
  INSERT PNAssignment(
    AssignmentID,
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
	LaborCode,
    ResourceID,
    GenericResourceID,
    CostRate,
    BillingRate,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    PlannedLaborHrs,
    StartDate,
    EndDate,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    /* Save off data from the inserted Assignment rows to create Labor TPD rows later */
    OUTPUT
      INSERTED.PlanID,
      INSERTED.TaskID,
      INSERTED.AssignmentID,
      INSERTED.WBS1,
      INSERTED.WBS2,
      INSERTED.WBS3,
	  INSERTED.LaborCode,
      INSERTED.ResourceID,
      INSERTED.GenericResourceID,
      INSERTED.StartDate,
      INSERTED.EndDate,
      INSERTED.CostRate,
      INSERTED.BillingRate,
      INSERTED.PlannedLaborHrs,
      INSERTED.BaselineLaborHrs,
      INSERTED.BaselineLabCost,
      INSERTED.BaselineLabBill
      INTO @tabInsertedAssignment(
        PlanID,
        TaskID,
        AssignmentID,
        WBS1,
        WBS2,
        WBS3,
		LaborCode,
        ResourceID,
        GenericResourceID,
        StartDate,
        EndDate,
        CostRate,
        BillingRate,
        PlannedLaborHrs,
        BaselineLaborHrs,
        BaselineLabCost,
        BaselineLabBill
      )
    SELECT
      REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS AssignmentID,
      @strNewPlanID AS PlanID,
      T.TaskID AS TaskID,
      A.WBS1 AS WBS1,
      A.WBS2 AS WBS2,
      A.WBS3 AS WBS3,
	  A.LaborCode AS LaborCode,
      A.ResourceID AS ResourceID,
      A.GenericResourceID AS GenericResourceID,
      A.CostRate AS CostRate,
      A.BillingRate AS BillingRate,
      A.BaselineLaborHrs AS BaselineLaborHrs,
      A.BaselineLabCost AS BaselineLabCost,
      A.BaselineLabBill AS BaselineLabBill,
      A.PlannedLaborHrs AS PlannedLaborHrs,
      A.StartDate AS StartDate,
      A.EndDate AS EndDate,
      @strUserName AS CreateUser,
      GETUTCDATE() AS CreateDate,
      @strUserName AS ModUser,
      GETUTCDATE() AS ModDate
      FROM @tabCombinedNonLeafAssignment AS A
        INNER JOIN @tabTask AS T ON T.WBS1 = A.WBS1 AND ISNULL(T.WBS2, ' ') = ISNULL(A.WBS2, ' ') AND ISNULL(T.WBS3, ' ') = ISNULL(A.WBS3, ' ')
		  AND ISNULL(T.LaborCode, ' ') = ISNULL(A.LaborCode, ' ')

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 

  --Add those assignment and its TPD data under labor code level task
  IF  @siLCLevels > 0 AND @bNeedUpdateLaborCode = 1
  BEGIN

    -- Set the labor code in assignment the same as it's parent task
	UPDATE A SET A.LaborCode = T.LaborCode
	FROM RPAssignment A 
	INNER JOIN RPTask T ON T.PlanID = A.PlanID AND T.TaskID = A.TaskID
     WHERE T.PlanID = @strOldPlanID  
	AND T.WBSType = 'LBCD'
	AND T.WBS1 <> '<none>' 
	AND ISNULL(T.LaborCode, ' ') <> '<none>' 
	AND ISNULL(T.WBS2, ' ') <> '<none>'  AND ISNULL(T.WBS3, ' ') <> '<none>'    

	-- retrieve all Assignment under labor code level task 
	--(with the number of the same wbs1,wbs2,wbs3,laborcode, resourceID, GenericResourceID > 1)
	--set bCombined = 1
	INSERT @tabLaborCodeAssignment(
	OldPlanID,
    WBS1,
    WBS2,
    WBS3,
	LaborCode,
	AssignmentID,
	ResourceID,
	GenericResourceID,
    StartDate,
    EndDate,
    CostRate,
    BillingRate,
    PlannedLaborHrs,
    PlannedLabCost,
    PlannedLabBill,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
	bCombined
	)
	SELECT 
	Max(A.PlanID) AS PlanID,
	Max(A.WBS1) AS WBS1,
    Max(A.WBS2) AS WBS2,
    Max(A.WBS3) AS WBS3,
	Max(A.LaborCode) AS LaborCode,
	Max(A.AssignmentID) AS AssignmentID,
	Max(A.ResourceID) AS ResourceID,
	Max(A.GenericResourceID) AS GenericResourceID,
    MIN(A.StartDate) AS StartDate,
    MAX(A.EndDate) AS EndDate,
    CASE
    WHEN SUM(A.PlannedLaborHrs) <> 0
    THEN SUM(A.PlannedLabCost) / SUM(A.PlannedLaborHrs)
    ELSE 0
    END AS CostRate,
    CASE
    WHEN SUM(A.PlannedLaborHrs) <> 0
    THEN SUM(A.PlannedLabBill) / SUM(A.PlannedLaborHrs)
    ELSE 0
    END AS BillingRate,
    SUM(A.PlannedLaborHrs) AS PlannedLaborHrs,
    SUM(A.PlannedLabCost) AS PlannedLabCost,
    SUM(A.PlannedLabBill) AS PlannedLabBill,
    SUM(A.BaselineLaborHrs) AS BaselineLaborHrs,
    SUM(A.BaselineLabCost) AS BaselineLabCost,
    SUM(A.BaselineLabBill) AS BaselineLabBill,
	1
	FROM RPAssignment A 
	INNER JOIN RPTask T ON T.PlanID = A.PlanID AND T.TaskID = A.TaskID
    WHERE T.PlanID = @strOldPlanID  
	AND T.WBSType = 'LBCD'
	AND T.WBS1 <> '<none>' 
	AND ISNULL(T.LaborCode, ' ') <> '<none>' 
	AND ISNULL(T.WBS2, ' ') <> '<none>'  AND ISNULL(T.WBS3, ' ') <> '<none>' 
	GROUP BY A.PlanID,A.WBS1,A.WBS2,A.WBS3,A.LaborCode,A.ResourceID,A.GenericResourceID Having count (*) > 1

	-- retrieve all Assignment under labor code level task 
	--(with only one record having the same wbs1,wbs2,wbs3,laborcode, resourceID, GenericResourceID )
	--set bCombined = 0

	INSERT @tabLaborCodeAssignment(
	OldPlanID,
    WBS1,
    WBS2,
    WBS3,
	LaborCode,
	AssignmentID,
	ResourceID,
	GenericResourceID,
    StartDate,
    EndDate,
    CostRate,
    BillingRate,
    PlannedLaborHrs,
    PlannedLabCost,
    PlannedLabBill,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
	bCombined
	)
	SELECT 
	Max(A.PlanID) AS PlanID,
	Max(A.WBS1) AS WBS1,
    Max(A.WBS2) AS WBS2,
    Max(A.WBS3) AS WBS3,
	Max(A.LaborCode) AS LaborCode,
	Max(A.AssignmentID) AS AssignmentID,
	Max(A.ResourceID) AS ResourceID,
	Max(A.GenericResourceID) AS GenericResourceID,
    MIN(A.StartDate) AS StartDate,
    MAX(A.EndDate) AS EndDate,
    CASE
    WHEN SUM(A.PlannedLaborHrs) <> 0
    THEN SUM(A.PlannedLabCost) / SUM(A.PlannedLaborHrs)
    ELSE 0
    END AS CostRate,
    CASE
    WHEN SUM(A.PlannedLaborHrs) <> 0
    THEN SUM(A.PlannedLabBill) / SUM(A.PlannedLaborHrs)
    ELSE 0
    END AS BillingRate,
    SUM(A.PlannedLaborHrs) AS PlannedLaborHrs,
    SUM(A.PlannedLabCost) AS PlannedLabCost,
    SUM(A.PlannedLabBill) AS PlannedLabBill,
    SUM(A.BaselineLaborHrs) AS BaselineLaborHrs,
    SUM(A.BaselineLabCost) AS BaselineLabCost,
    SUM(A.BaselineLabBill) AS BaselineLabBill,
	0
	FROM RPAssignment A 
	INNER JOIN RPTask T ON T.PlanID = A.PlanID AND T.TaskID = A.TaskID
    WHERE T.PlanID = @strOldPlanID  
	AND T.WBSType = 'LBCD'
	AND T.WBS1 <> '<none>' 
	AND ISNULL(T.LaborCode, ' ') <> '<none>' 
	AND ISNULL(T.WBS2, ' ') <> '<none>'  AND ISNULL(T.WBS3, ' ') <> '<none>' 
	GROUP BY A.PlanID,A.WBS1,A.WBS2,A.WBS3,A.LaborCode,A.ResourceID,A.GenericResourceID Having count (*) = 1

	--SET All Assignment  WBS2 and WBS3 if necessary
	UPDATE A SET A.WBS2 = ParentTask.WBS2, 
				 A.WBS3 = ParentTask.WBS3
	FROM @tabLaborCodeAssignment AS A INNER JOIN
	 (SELECT TOP 1 WBS1,WBS2, WBS3 FROM @tabTask WHERE PlanID = @strNewPlanID AND
	    WBS2 IS NOT NULL AND ChildrenCount = 0 ORDER BY OutlineNumber ) AS ParentTask 
		  ON ParentTask.WBS1 = A.WBS1
     Where A.OldPlanID = @strOldPlanID  AND A.WBS2 IS NULL
	 	 
		DECLARE AssignmentCursor CURSOR FOR 
		SELECT  AssignmentID  FROM @tabLaborCodeAssignment WHERE WBS2 IS NOT NULL AND WBS3 IS NULL

		OPEN AssignmentCursor
		FETCH NEXT FROM AssignmentCursor INTO @strAssignmentID 
		WHILE @@FETCH_STATUS = 0 
		BEGIN		  
			UPDATE A SET  A.WBS3 = ParentTask.WBS3			
			FROM @tabLaborCodeAssignment AS A 
			INNER JOIN
			(SELECT TOP 1 T.WBS1 AS WBS1, T.WBS3 AS WBS3 FROM @tabTask T
			INNER JOIN @tabLaborCodeAssignment A ON  
			A.WBS2 = T.WBS2 WHERE A.AssignmentID = @strAssignmentID
			AND T.WBS3 IS NOT NULL ORDER BY T.OutlineNumber) AS ParentTask 
			ON ParentTask.WBS1 = A.WBS1 			
			Where A.AssignmentID = @strAssignmentID --AND A.OldPlanID = @strOldPlanID  
			FETCH NEXT FROM AssignmentCursor INTO @strAssignmentID
		END --WHILE @@FETCH_STATUS = 0 
	  CLOSE AssignmentCursor
	  DEALLOCATE AssignmentCursor	 

	--Now we can insert those RPAssignment records under labor code	
	DELETE FROM @tabInsertedAssignment	 --clean the records added from possible non leaf assignments

	--Insert the labor code assignment into PN, notice, when the assignment is combined, we will set a new assignmentID, if not, use original RPAssignment.AssignmentID
	INSERT PNAssignment(
    AssignmentID,
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
	LaborCode,
    ResourceID,
    GenericResourceID,
    CostRate,
    BillingRate,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    PlannedLaborHrs,
    StartDate,
    EndDate,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    /* Save off data from the inserted Assignment rows to create Labor TPD rows later */
    OUTPUT
      INSERTED.PlanID,
      INSERTED.TaskID,
      INSERTED.AssignmentID,
      INSERTED.WBS1,
      INSERTED.WBS2,
      INSERTED.WBS3,
	  INSERTED.LaborCode,
      INSERTED.ResourceID,
      INSERTED.GenericResourceID,
      INSERTED.StartDate,
      INSERTED.EndDate,
      INSERTED.CostRate,
      INSERTED.BillingRate,
      INSERTED.PlannedLaborHrs,
      INSERTED.BaselineLaborHrs,
      INSERTED.BaselineLabCost,
      INSERTED.BaselineLabBill
      INTO @tabInsertedAssignment(
        PlanID,
        TaskID,
        AssignmentID,
        WBS1,
        WBS2,
        WBS3,
		LaborCode,
        ResourceID,
        GenericResourceID,
        StartDate,
        EndDate,
        CostRate,
        BillingRate,
        PlannedLaborHrs,
        BaselineLaborHrs,
        BaselineLabCost,
        BaselineLabBill
      )
    SELECT
	  CASE WHEN A.bCombined = 1 THEN
      REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')
	  ELSE A.AssignmentID END AS AssignmentID,
      @strNewPlanID AS PlanID,
      T.TaskID AS TaskID,
      A.WBS1 AS WBS1,
      A.WBS2 AS WBS2,
      A.WBS3 AS WBS3,
      A.LaborCode AS LaborCode,
      A.ResourceID AS ResourceID,
      A.GenericResourceID AS GenericResourceID,
      A.CostRate AS CostRate,
      A.BillingRate AS BillingRate,
      A.BaselineLaborHrs AS BaselineLaborHrs,
      A.BaselineLabCost AS BaselineLabCost,
      A.BaselineLabBill AS BaselineLabBill,
      A.PlannedLaborHrs AS PlannedLaborHrs,
      A.StartDate AS StartDate,
      A.EndDate AS EndDate,
      @strUserName AS CreateUser,
      GETUTCDATE() AS CreateDate,
      @strUserName AS ModUser,
      GETUTCDATE() AS ModDate
      FROM @tabLaborCodeAssignment AS A
	  INNER JOIN @tabTask AS T ON 
          T.WBS1 = A.WBS1 AND ISNULL(T.WBS2, ' ') = ISNULL(A.WBS2, ' ') AND ISNULL(T.WBS3, ' ') = ISNULL(A.WBS3, ' ')
		  AND ISNULL(T.LaborCode, ' ') = ISNULL(A.LaborCode, ' ') --AND ISNULL(A.LaborCode, '%') like ISNULL(T.LaborCode, '%')
        /* Except those Assignments that have already been moved downward from the non-leaf nodes */
        LEFT JOIN @tabCombinedNonLeafAssignment AS XA ON 
          A.WBS1 = XA.WBS1 AND ISNULL(A.WBS2, ' ') = ISNULL(XA.WBS2, ' ') AND ISNULL(A.WBS3, ' ') = ISNULL(XA.WBS3, ' ')
		  AND ISNULL(A.LaborCode, ' ') = ISNULL(XA.LaborCode, ' ')
          AND ISNULL(A.ResourceID, '|') = ISNULL(XA.ResourceID, '|') AND ISNULL(A.GenericResourceID, '|') = ISNULL(XA.GenericResourceID, '|')		  		 
	  WHERE XA.OldPlanID IS NULL --AND ZA.OldPlanID IS NOT NULL 
  END --IF  @siLCLevels > 0 AND @bNeedUpdateLaborCode = 1


  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  -- Move only leaf Assignment rows from RPAssignment to PNAssignment.
  -- Except those Assignments that have already been moved downward from the non-leaf nodes...

    DELETE FROM @tabInsertedAssignment	 --clean the records added from possible non leaf assignments and labor code level assignments

	INSERT PNAssignment(
    AssignmentID,
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    LaborCode,
    ResourceID,
    GenericResourceID,
    Category,
    GRLBCD,
    ActivityID,
    CostRate,
    BillingRate,
    BaselineLaborHrs,
    BaselineLabCost,
    BaselineLabBill,
    BaselineRevenue,
    BaselineStart,
    BaselineFinish,
    PlannedLaborHrs,
    PlannedLabCost,
    PlannedLabBill,
    StartDate,
    EndDate,
    SortSeq,
    HardBooked,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    /* Save off data from the inserted Assignment rows to create Labor TPD rows later */
    OUTPUT
      INSERTED.PlanID,
      INSERTED.TaskID,
      INSERTED.AssignmentID,
      INSERTED.WBS1,
      INSERTED.WBS2,
      INSERTED.WBS3,
	  INSERTED.LaborCode,
      INSERTED.ResourceID,
      INSERTED.GenericResourceID,
      INSERTED.StartDate,
      INSERTED.EndDate,
      INSERTED.CostRate,
      INSERTED.BillingRate,
      INSERTED.PlannedLaborHrs,
      INSERTED.BaselineLaborHrs,
      INSERTED.BaselineLabCost,
      INSERTED.BaselineLabBill
      INTO @tabInsertedAssignment(
        PlanID,
        TaskID,
        AssignmentID,
        WBS1,
        WBS2,
        WBS3,
		LaborCode,
        ResourceID,
        GenericResourceID,
        StartDate,
        EndDate,
        CostRate,
        BillingRate,
        PlannedLaborHrs,
        BaselineLaborHrs,
        BaselineLabCost,
        BaselineLabBill
      )
	      SELECT
      A.AssignmentID,
      @strNewPlanID AS PlanID,
      A.TaskID,
      A.WBS1,
      A.WBS2,
      A.WBS3,
      A.LaborCode,
      A.ResourceID,
      A.GenericResourceID,
      A.Category,
      A.GRLBCD,
      A.ActivityID,
      A.CostRate,
      A.BillingRate,
      A.BaselineLaborHrs,
      A.BaselineLabCost,
      A.BaselineLabBill,
      A.BaselineRevenue,
      A.BaselineStart,
      A.BaselineFinish,
      A.PlannedLaborHrs,
      A.PlannedLabCost,
      A.PlannedLabBill,
      A.StartDate,
      A.EndDate,
      A.SortSeq,
      CASE
        WHEN XTPD.AssignmentID = A.AssignmentID
        THEN 'Y'
        ELSE 'N'
      END AS HardBooked,
      @strUserName AS CreateUser,
      GETUTCDATE() AS CreateDate,
      @strUserName AS ModUser,
      GETUTCDATE() AS ModDate
      FROM RPAssignment AS A
        /* Select Assignment rows that were hung at leaf level */
        INNER JOIN @tabTask AS T ON T.ChildrenCount = 0
          AND A.WBS1 = T.WBS1 AND ISNULL(A.WBS2, ' ') = ISNULL(T.WBS2, ' ') AND ISNULL(A.WBS3, ' ') = ISNULL(T.WBS3, ' ')
		  AND ISNULL(A.LaborCode, ' ') = ISNULL(T.LaborCode, ' ')
        /* Except those Assignments that have already been moved downward from the non-leaf nodes */
        LEFT JOIN @tabNonLeafAssignment AS XA ON T.OutlineNumber = XA.Leaf_OutlineNumber
          AND A.WBS1 = XA.WBS1 AND ISNULL(A.WBS2, ' ') = ISNULL(XA.Leaf_WBS2, ' ') AND ISNULL(A.WBS3, ' ') = ISNULL(XA.Leaf_WBS3, ' ')
		  AND ISNULL(A.LaborCode, ' ') = ISNULL(XA.LaborCode, ' ')
          AND ISNULL(A.ResourceID, '|') = ISNULL(XA.ResourceID, '|') AND ISNULL(A.GenericResourceID, '|') = ISNULL(XA.GenericResourceID, '|')
        LEFT JOIN (
          SELECT DISTINCT
            TPD.PlanID,
            TPD.TaskID,
            TPD.AssignmentID
            FROM RPPlannedLabor AS TPD
              WHERE TPD.EndDate >= @dtETCDate AND TPD.HardBooked = 'Y' AND TPD.AssignmentID IS NOT NULL
                AND TPD.PlanID = @strOldPlanID
          UNION
          SELECT DISTINCT
            ZTPD.PlanID,
            ZTPD.TaskID,
            ZTPD.AssignmentID
            FROM (
              SELECT DISTINCT
                TPD.PlanID,
                TPD.TaskID,
                TPD.AssignmentID,
                MAX(TPD.HardBooked) OVER (PARTITION BY TPD.PlanID, TPD.TaskID, TPD.AssignmentID) AS MaxHardBooked,
                MAX(TPD.EndDate) OVER (PARTITION BY TPD.PlanID, TPD.TaskID, TPD.AssignmentID) AS MaxTPDEnd
                FROM RPPlannedLabor AS TPD
                WHERE TPD.AssignmentID IS NOT NULL AND TPD.PlanID = @strOldPlanID
            ) AS ZTPD
            WHERE ZTPD.MaxTPDEnd < @dtETCDate AND ZTPD.MaxHardBooked = 'Y'
        ) AS XTPD ON A.PlanID = XTPD.PlanID AND A.TaskID = XTPD.TaskID AND A.AssignmentID = XTPD.AssignmentID
      WHERE A.PlanID = @strOldPlanID
        /* Except those Assignments that have already been moved downward from the non-leaf nodes and assignment under laborcode is not null*/
        AND A.LaborCode is NULL AND XA.OldPlanID IS NULL  --AND ZA.OldPlanID IS NULL 


	------------------------------		
	
	

	--Create TPD planned hour and baseline data
	  INSERT PNPlannedLabor(
		TimePhaseID,
		PlanID, 
		TaskID,
		AssignmentID,
		StartDate, 
		EndDate, 
		PeriodHrs,
		PeriodCost,
		PeriodBill,
		CostRate,
		BillingRate,
		CreateUser,
		CreateDate,
		ModUser,
		ModDate
  )
      SELECT
      TPD.TimePhaseID,
      TPD.PlanID AS PlanID, 
      A.TaskID AS TaskID,
      A.AssignmentID AS AssignmentID,
      TPD.StartDate AS StartDate, 
      TPD.EndDate AS EndDate, 
      TPD.PeriodHrs AS PeriodHrs,
	  TPD.PeriodCost AS PeriodCost,
      TPD.PeriodBill AS PeriodBill,
	  TPD.CostRate AS PeriodBill,
	  TPD.BillingRate AS PeriodBill,
      @strUserName AS CreateUser,
      GETUTCDATE() AS CreateDate,
      @strUserName AS ModUser,
      GETUTCDATE() AS ModDate
      FROM @tabPlannedLabor AS TPD 
	  INNER JOIN PNAssignment A 
	  ON TPD.PlanID = A.PlanID AND TPD.WBS1 = A.WBS1 AND ISNULL(A.WBS2, ' ') = ISNULL(TPD.WBS2, ' ') 
	    AND ISNULL(A.WBS3, ' ') = ISNULL(TPD.WBS3, ' ') 
		AND 1 = CASE WHEN  
			(
			 @bNeedUpdateLaborCode = 1 AND ISNULL(A.LaborCode, '%') =  ISNULL(TPD.LaborCode, '%') 			 
			 OR @bNeedUpdateLaborCode = 0
			)
		THEN 1
		ELSE 0 END		 
		AND ISNULL(A.ResourceID, '|') = ISNULL(TPD.ResourceID, '|') AND ISNULL(A.GenericResourceID, '|') = ISNULL(TPD.GenericResourceID, '|')

	--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  -- Inserting one row of Baseline Labor TPD for each inserted Assignment row at the leaf level.

  INSERT PNBaselineLabor(
    TimePhaseID,
    PlanID, 
    TaskID,
    AssignmentID,
    StartDate, 
    EndDate, 
    CostRate,
    BillingRate,
    PeriodHrs,
    PeriodCost,
    PeriodBill,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
      SELECT
      TPD.TimePhaseID,
      TPD.PlanID AS PlanID, 
      A.TaskID AS TaskID,
      A.AssignmentID AS AssignmentID,
      TPD.StartDate AS StartDate, 
      TPD.EndDate AS EndDate, 
      TPD.CostRate AS CostRate,
      TPD.BillingRate AS BillingRate,
      TPD.PeriodHrs AS PeriodHrs,
      TPD.PeriodCost AS PeriodCost,
      TPD.PeriodCost AS PeriodBill,
      @strUserName AS CreateUser,
      GETUTCDATE() AS CreateDate,
      @strUserName AS ModUser,
      GETUTCDATE() AS ModDate
      FROM @tabBaselineLabor AS TPD 
	  INNER JOIN PNAssignment A 
	  ON TPD.PlanID = A.PlanID AND TPD.WBS1 = A.WBS1 AND ISNULL(A.WBS2, ' ') = ISNULL(TPD.WBS2, ' ') 
	    AND ISNULL(A.WBS3, ' ') = ISNULL(TPD.WBS3, ' ') 
		AND 1 = CASE WHEN  
			(
			 @bNeedUpdateLaborCode = 1 AND ISNULL(A.LaborCode, '%') =  ISNULL(TPD.LaborCode, '%') 			 
			 OR @bNeedUpdateLaborCode = 0
			)
		THEN 1
		ELSE 0 END		
		AND ISNULL(A.ResourceID, '|') = ISNULL(TPD.ResourceID, '|') AND ISNULL(A.GenericResourceID, '|') = ISNULL(TPD.GenericResourceID, '|')

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strExpTab = 'Y')
    BEGIN

      INSERT PNExpense(
        ExpenseID,
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        StartDate,
        EndDate,
        Account,
        Vendor,
        DirectAcctFlg,
        ExpBillRate,
        PlannedExpCost,
        PlannedExpBill,
        BaselineExpCost,
        BaselineExpBill,
        CreateUser,
        CreateDate,
        ModUser,
        ModDate
      )
        SELECT
          REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS ExpenseID,
          @strNewPlanID AS PlanID,
          T.TaskID,
          T.WBS1,
          T.WBS2,
          T.WBS3,
          T.StartDate,
          T.EndDate,
          E.Account,
          E.Vendor,
          E.DirectAcctFlg,
          CASE 
            WHEN E.DirectAcctFlg = 'Y' 
            THEN 1.0
            ELSE
              CASE
                WHEN @strCalcExpBillAmtFlg = 'Y'
                THEN
                  CASE 
                    WHEN E.PlannedExpCost = 0.0
                    THEN 0.0
                    ELSE (E.PlannedExpBill / E.PlannedExpCost)
                  END
                ELSE 0.0
              END
          END AS ExpBillRate,
          E.PlannedExpCost,
          E.PlannedExpBill,
          E.BaselineExpCost,
          E.BaselineExpBill,
          @strUserName AS CreateUser,
          GETUTCDATE() AS CreateDate,
          @strUserName AS ModUser,
          GETUTCDATE() AS ModDate
          FROM @tabExpense AS E
            INNER JOIN @tabExpWBSTree AS T ON 
              E.WBS1 = T.WBS1 AND ISNULL(E.WBS2, '$') = ISNULL(T.WBS2, '$') AND ISNULL(E.WBS3, '$') = ISNULL(T.WBS3, '$')

      -- Need to create one Planned TPD Expense row for each Expense row.

      INSERT PNPlannedExpenses(
        TimePhaseID,
        TaskID,
        PlanID,
        ExpenseID,
        StartDate,
        EndDate,
        PeriodCost,
        PeriodBill,
        CreateUser,
        CreateDate,
        ModUser,
        ModDate
      )
        SELECT
          REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
          E.TaskID AS TaskID,
          E.PlanID AS PlanID,
          E.ExpenseID AS ExpenseID,
          E.StartDate AS StartDate,
          E.EndDate AS EndDate,
          E.PlannedExpCost AS PeriodCost,
          E.PlannedExpBill AS PeriodBill,
          @strUserName AS CreateUser,
          GETUTCDATE() AS CreateDate,
          @strUserName AS ModUser,
          GETUTCDATE() AS ModDate
          FROM PNExpense AS E 
          WHERE E.PlanID = @strNewPlanID

      -- Baseline Time-Phased data for the summary Task rows.

      INSERT PNBaselineExpenses(
        TimePhaseID,
        TaskID,
        PlanID,
        ExpenseID,
        StartDate,
        EndDate,
        PeriodCost,
        PeriodBill,
        CreateUser,
        CreateDate,
        ModUser,
        ModDate
      )
        SELECT
          TimePhaseID,
          TaskID,
          @strNewPlanID AS PlanID,
          ExpenseID,
          StartDate,
          EndDate,
          PeriodCost,
          PeriodBill,
          @strUserName AS CreateUser,
          GETUTCDATE() AS CreateDate,
          @strUserName AS ModUser,
          GETUTCDATE() AS ModDate
          FROM RPBaselineExpenses AS TPD 
          WHERE TPD.PlanID = @strOldPlanID AND TPD.ExpenseID IS NULL
 
      -- Baseline Time-Phased data for the Expense rows.

      INSERT PNBaselineExpenses(
        TimePhaseID,
        TaskID,
        PlanID,
        ExpenseID,
        StartDate,
        EndDate,
        PeriodCost,
        PeriodBill,
        CreateUser,
        CreateDate,
        ModUser,
        ModDate
      )
        SELECT
          REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
          NC.TaskID,
          @strNewPlanID AS PlanID,
          NC.ExpenseID,
          TPD.StartDate,
          TPD.EndDate,
          SUM(TPD.PeriodCost),
          SUM(TPD.PeriodBill),
          @strUserName AS CreateUser,
          GETUTCDATE() AS CreateDate,
          @strUserName AS ModUser,
          GETUTCDATE() AS ModDate
          FROM RPBaselineExpenses AS TPD
            INNER JOIN RPExpense AS VC ON TPD.PlanID = VC.PlanID AND TPD.TaskID = VC.TaskID AND TPD.ExpenseID = VC.ExpenseID
            INNER JOIN PNExpense AS NC ON 
              VC.WBS1 = NC.WBS1 AND ISNULL(VC.WBS2, ' ') = ISNULL(NC.WBS2, ' ') AND ISNULL(VC.WBS3, ' ') = ISNULL(NC.WBS3, ' ') AND
              VC.Account = NC.Account AND ISNULL(VC.Vendor, ' ') = ISNULL(NC.Vendor, ' ')
          WHERE TPD.PlanID = @strOldPlanID AND TPD.ExpenseID IS NOT NULL
          GROUP BY NC.PlanID, NC.TaskID, NC.ExpenseID, NC.WBS1, NC.WBS2, NC.WBS3, NC.Account, NC.Vendor, TPD.StartDate, TPD.EndDate

    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@strConTab = 'Y')
    BEGIN

      INSERT PNConsultant(
        ConsultantID,
        PlanID,
        TaskID,
        WBS1,
        WBS2,
        WBS3,
        StartDate,
        EndDate,
        Account,
        Vendor,
        DirectAcctFlg,
        ConBillRate,
        PlannedConCost,
        PlannedConBill,
        BaselineConCost,
        BaselineConBill,
        CreateUser,
        CreateDate,
        ModUser,
        ModDate
      )
        SELECT
          REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS ConsultantID,
          @strNewPlanID AS PlanID,
          T.TaskID,
          T.WBS1,
          T.WBS2,
          T.WBS3,
          T.StartDate,
          T.EndDate,
          C.Account,
          C.Vendor,
          C.DirectAcctFlg,
          CASE 
            WHEN C.DirectAcctFlg = 'Y' 
            THEN 1.0
            ELSE
              CASE
                WHEN @strCalcConBillAmtFlg = 'Y'
                THEN
                  CASE 
                    WHEN C.PlannedConCost = 0.0
                    THEN 0.0
                    ELSE (C.PlannedConBill / C.PlannedConCost)
                  END
                ELSE 0.0
              END
          END AS ConBillRate,
          C.PlannedConCost,
          C.PlannedConBill,
          C.BaselineConCost,
          C.BaselineConBill,
          @strUserName AS CreateUser,
          GETUTCDATE() AS CreateDate,
          @strUserName AS ModUser,
          GETUTCDATE() AS ModDate
          FROM @tabConsultant AS C
            INNER JOIN @tabConWBSTree AS T ON 
              C.WBS1 = T.WBS1 AND ISNULL(C.WBS2, '$') = ISNULL(T.WBS2, '$') AND ISNULL(C.WBS3, '$') = ISNULL(T.WBS3, '$')

      -- Need to create one Planned TPD Expense row for each Expense row.

      INSERT PNPlannedConsultant(
        TimePhaseID,
        TaskID,
        PlanID,
        ConsultantID,
        StartDate,
        EndDate,
        PeriodCost,
        PeriodBill,
        CreateUser,
        CreateDate,
        ModUser,
        ModDate
      )
        SELECT
          REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
          C.TaskID AS TaskID,
          C.PlanID AS PlanID,
          C.ConsultantID AS ConsultantID,
          C.StartDate AS StartDate,
          C.EndDate AS EndDate,
          C.PlannedConCost AS PeriodCost,
          C.PlannedConBill AS PeriodBill,
          @strUserName AS CreateUser,
          GETUTCDATE() AS CreateDate,
          @strUserName AS ModUser,
          GETUTCDATE() AS ModDate
          FROM PNConsultant AS C
          WHERE C.PlanID = @strNewPlanID

      -- Baseline Time-Phased data for the summary Task rows.

      INSERT PNBaselineConsultant(
        TimePhaseID,
        TaskID,
        PlanID,
        ConsultantID,
        StartDate,
        EndDate,
        PeriodCost,
        PeriodBill,
        CreateUser,
        CreateDate,
        ModUser,
        ModDate
      )
        SELECT
          TimePhaseID,
          TaskID,
          @strNewPlanID AS PlanID,
          ConsultantID,
          StartDate,
          EndDate,
          PeriodCost,
          PeriodBill,
          @strUserName AS CreateUser,
          GETUTCDATE() AS CreateDate,
          @strUserName AS ModUser,
          GETUTCDATE() AS ModDate
          FROM RPBaselineConsultant AS TPD 
          WHERE TPD.PlanID = @strOldPlanID AND TPD.ConsultantID IS NULL
 
      -- Baseline Time-Phased data for the ConsultantS rows.

      INSERT PNBaselineConsultant(
        TimePhaseID,
        TaskID,
        PlanID,
        ConsultantID,
        StartDate,
        EndDate,
        PeriodCost,
        PeriodBill,
        CreateUser,
        CreateDate,
        ModUser,
        ModDate
      )
        SELECT
          REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
          NC.TaskID,
          @strNewPlanID AS PlanID,
          NC.ConsultantID,
          TPD.StartDate,
          TPD.EndDate,
          SUM(TPD.PeriodCost),
          SUM(TPD.PeriodBill),
          @strUserName AS CreateUser,
          GETUTCDATE() AS CreateDate,
          @strUserName AS ModUser,
          GETUTCDATE() AS ModDate
          FROM RPBaselineConsultant AS TPD
            INNER JOIN RPConsultant AS VC ON TPD.PlanID = VC.PlanID AND TPD.TaskID = VC.TaskID AND TPD.ConsultantID = VC.ConsultantID
            INNER JOIN PNConsultant AS NC ON 
              VC.WBS1 = NC.WBS1 AND ISNULL(VC.WBS2, ' ') = ISNULL(NC.WBS2, ' ') AND ISNULL(VC.WBS3, ' ') = ISNULL(NC.WBS3, ' ') AND
              VC.Account = NC.Account AND ISNULL(VC.Vendor, ' ') = ISNULL(NC.Vendor, ' ')
          WHERE TPD.PlanID = @strOldPlanID AND TPD.ConsultantID IS NOT NULL
          GROUP BY NC.PlanID, NC.TaskID, NC.ConsultantID, NC.WBS1, NC.WBS2, NC.WBS3, NC.Account, NC.Vendor, TPD.StartDate, TPD.EndDate

    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT PNWBSLevelFormat(
    WBSFormatID,
    PlanID,
    FmtLevel,
    WBSType,
    WBSMatch,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT 
      WBSFormatID,
      @strNewPlanID AS PlanID,
      FmtLevel,
      WBSType,
      WBSMatch,
      @strUserName AS CreateUser,
      GETUTCDATE() AS CreateDate,
      @strUserName AS ModUser,
      GETUTCDATE() AS ModDate
      FROM RPWBSLevelFormat WHERE PlanID = @strOldPlanID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  -- Create Revenue Forecast TPD data
   INSERT PNPlannedRevenueLabor(
        TimePhaseID,
        TaskID,
        PlanID,
        StartDate,
        EndDate,
        PeriodCost,
        PeriodBill,
        CreateUser,
        CreateDate,
        ModUser,
        ModDate
      )
        SELECT
          V.TimePhaseID,
          T.TaskID AS TaskID,
          @strNewPlanID,
          V.StartDate AS StartDate,
          V.EndDate AS EndDate,
          V.PeriodCost AS PeriodCost,
          V.PeriodBill AS PeriodBill,
          @strUserName AS CreateUser,
          GETUTCDATE() AS CreateDate,
          @strUserName AS ModUser,
          GETUTCDATE() AS ModDate
          FROM @tabRevenue AS V INNER JOIN @tabTask T ON T.WBS1 = V.WBS1 AND ISNULL (T.WBS2, ' ') = ISNULL (V.WBS2, ' ') AND ISNULL (T.WBS3, ' ') = ISNULL (V.WBS3, ' ') 
          WHERE T.WBSType <> 'LBCD' AND (V.PeriodCost > 0 OR V.PeriodBill > 0)

   INSERT RPPlannedRevenueLabor(
        TimePhaseID,
        TaskID,
        PlanID,
        StartDate,
        EndDate,
        PeriodCost,
        PeriodBill,
        CreateUser,
        CreateDate,
        ModUser,
        ModDate
      )
        SELECT
          V.TimePhaseID AS TimePhaseID,
          T.TaskID AS TaskID,
          @strNewPlanID,
          V.StartDate AS StartDate,
          V.EndDate AS EndDate,
          V.PeriodCost AS PeriodCost,
          V.PeriodBill AS PeriodBill,
          @strUserName AS CreateUser,
          GETUTCDATE() AS CreateDate,
          @strUserName AS ModUser,
          GETUTCDATE() AS ModDate
          FROM @tabRevenue AS V INNER JOIN @tabTask T ON T.WBS1 = V.WBS1 AND ISNULL (T.WBS2, ' ') = ISNULL (V.WBS2, ' ') AND ISNULL (T.WBS3, ' ') = ISNULL (V.WBS3, ' ') 
          WHERE T.WBSType <> 'LBCD' AND (V.PeriodCost > 0 OR V.PeriodBill > 0)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


  -- Update all WBS levels in PR table with PlanID.

  UPDATE PR SET
    PR.PlanID = @strNewPlanID
    FROM PR
    WHERE PR.WBS1 = @strWBS1

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- At this point we have a "new" Plan created in PN tables.
  -- It is time to delete the "old" plan from RP tables and publish the "new" Plan to RP tables.
  EXECUTE dbo.stRPExtendDateSpan @strNewPlanID,0	
  EXECUTE dbo.PMDelPlan @strOldPlanID
  EXECUTE dbo.stRPPublish @strNewPlanID

 -- if there is revenue forecast, we need extend revenue dates and PR.Allocation Method
  IF (SELECT COUNT(*) FROM PNPlannedRevenueLabor WHERE PlanID = @strNewPlanID) > 0 
  BEGIN 
    UPDATE PR SET AllocMethod = 'r' WHERE WBS1 = @strWBS1
	UPDATE PNPlan SET StoredCurrency =  @strStoredRevenueCurrency WHERE PlanID = @strNewPlanID 
	UPDATE RPPlan SET StoredCurrency =  @strStoredRevenueCurrency WHERE PlanID = @strNewPlanID 
	UPDATE PNTask SET RevenueStartDate = StartDate, RevenueEndDate = EndDate WHERE PlanID = @strNewPlanID 
	UPDATE RPTask SET RevenueStartDate = StartDate, RevenueEndDate = EndDate  WHERE PlanID = @strNewPlanID 

	--Set the revenue calculation method the same as conversion method.
	IF 	@strRevenueSetting = 'G' UPDATE CFGRMSettings SET RevenueCalculationsOption = 0
	IF 	@strRevenueSetting = 'I' UPDATE CFGRMSettings SET RevenueCalculationsOption = 1
	IF 	@strRevenueSetting = 'E' UPDATE CFGRMSettings SET RevenueCalculationsOption = 2
	IF 	@strRevenueSetting = 'L' UPDATE CFGRMSettings SET RevenueCalculationsOption = 3	
	
	EXECUTE dbo.stRPExtendDateSpan_RevForecast @strNewPlanID,1	
  END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  COMMIT TRANSACTION _PlanConversion_76To30_

  SET NOCOUNT OFF

END -- stRPCreatePNPlanFromRPPlan
GO
