SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[HAI_TSR_MarketingCostsAllOffices]
	@CompanyId VARCHAR(2),
	@MinPeriod INT,
	@Period INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SET TRANSACTION isolation level READ uncommitted; 

-- CREATE A UNIQUE ID
DECLARE @SessionId VARCHAR(32) = REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')

IF EXISTS (SELECT NAME 
           FROM   sysobjects 
           WHERE  NAME = 'setContextInfo' 
                  AND type = 'P') 
  EXECUTE dbo.Setcontextinfo 
    @StrCompany = /*N*/'01', 
    @StrAuditingEnabled = 'N', 
    @strUserName = /*N*/'HIT', 
    @StrCultureName = 'en-US'; 

DECLARE @s1 VARCHAR(100) = 'AND (LD.Period BETWEEN ' + CAST(@MinPeriod AS VARCHAR(10)) + ' AND ' + CAST(@Period AS VARCHAR(10)) + ') '
DECLARE @s2 VARCHAR(MAX) = '';
DECLARE @s3 VARCHAR(10) = CAST(@Period AS VARCHAR(10))

IF @CompanyId = '01' 
	SET @s2 = '((((
	(PR.WBS1 = /*N*/''NC1008.000'') 
	OR (PR.WBS1 = /*N*/''NC1010.000'')
	OR (PR.WBS1 = /*N*/''NC1010.001 '')
	OR (PR.WBS1 = /*N*/''NC1010.002 '')
	OR (PR.WBS1 = /*N*/''NC1010.003'')
	OR (PR.WBS1 = /*N*/''NC1011.001'')
	)) 
	OR ((PR.WBS1 LIKE /*N*/''P%'') 
	AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Promotional''))))))'
ELSE IF @CompanyId = '03' 
	SET @s2 = '((((
	(PR.WBS1 = /*N*/''NC3008.000'') 
	OR (PR.WBS1 = /*N*/''NC3010.000'')
	OR (PR.WBS1 = /*N*/''NC3010.001'')
	OR (PR.WBS1 = /*N*/''NC3010.002'')
	OR (PR.WBS1 = /*N*/''NC3010.003'')
	OR (PR.WBS1 = /*N*/''NC3011.001'')
	)) 
	OR ((PR.WBS1 LIKE /*N*/''P%'') 
	AND  EXISTS (Select ''x'' FROM CFGChargeType WHERE CFGChargeType.type = PR.ChargeType AND ((CFGChargeType.Label = /*N*/''Promotional''))))))'

EXEC Setcurrentactivityonly 
@SessionId, 
' PR LEFT JOIN CL AS CLBill ON CLBill.ClientID = PR.BillingClientID  
LEFT JOIN CL ON CL.ClientID = PR.ClientID  
LEFT JOIN Contacts AS BLCT 
ON PR.BillingContactID = BLCT.ContactID,  PR AS LEVEL2, PR AS LEVEL3 ', 
@s1, 
' ', 
@s2
, 
@s3, 
' ', 
'358' 

DECLARE @sql NVARCHAR(MAX) = N'DECLARE @tabWBSList TABLE 
  ( 
     wbs1 NVARCHAR(30) COLLATE database_default, 
     wbs2 NVARCHAR(7) COLLATE database_default, 
     wbs3 NVARCHAR(7) COLLATE database_default 
     PRIMARY KEY(wbs1, wbs2, wbs3) 
  ) 

INSERT @tabWBSList 
       (wbs1, 
        wbs2, 
        wbs3) 
SELECT pr.wbs1, 
       level2.wbs2, 
       level3.wbs3 
FROM   pr AS level3, 
       pr AS LEVEL2, 
       pr, 
       ##rwl_' + @SessionId + ' 
WHERE  LEVEL3.sublevel = ''N'' 
       AND pr.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs2 = LEVEL3.wbs2 
       AND pr.wbs2 = '''' 
       AND pr.wbs3 = '''' 
       AND LEVEL2.wbs3 = '''' 
       AND (( (( ( pr.wbs1 = ''NC1008.000'' ) 
					OR ( pr.wbs1 = ''NC1010.000'' ) 
					OR ( pr.wbs1 = ''NC1010.001'' )  
					OR ( pr.wbs1 = ''NC1010.002'' )  
					OR ( pr.wbs1 = ''NC1010.003'' )  
					OR ( pr.wbs1 = ''NC1011.001'' )  
					OR ( pr.wbs1 = ''NC3008.000'' ) 
					OR ( pr.wbs1 = ''NC3010.000'' ) 
					OR ( pr.wbs1 = ''NC3010.001'' )  
					OR ( pr.wbs1 = ''NC3010.002'' )  
					OR ( pr.wbs1 = ''NC3010.003'' )  
					OR ( pr.wbs1 = ''NC3011.001'' )  
				  )) 
               OR ( ( pr.wbs1 LIKE ''P%'' ) 
                    AND EXISTS (SELECT ''x'' 
                                FROM   cfgchargetype 
                                WHERE  cfgchargetype.type = pr.chargetype 
                                       AND (( cfgchargetype.label =  
                                              ''Promotional'' 
                                            ))) 
                  ) )) 
       AND LEVEL3.wbs1 = ##rwl_' + @SessionId + '.wbs1 
       AND ##rwl_' + @SessionId + '.wbs2 = '' '' 
       AND ##rwl_' + @SessionId + '.wbs3 = '' '' 
       AND ##rwl_' + @SessionId + '.sessionid = 
           ''' + @SessionId + ''' 

DECLARE @tabContract TABLE 
  ( 
     contractwbs1                 NVARCHAR(30) COLLATE database_default, 
     contractwbs2                 NVARCHAR(7) COLLATE database_default, 
     contractwbs3                 NVARCHAR(7) COLLATE database_default, 
     contractfee                  DECIMAL(19, 4), 
     contractreimballow           DECIMAL(19, 4), 
     contractconsultfee           DECIMAL(19, 4), 
     contractfeedirectlabor       DECIMAL(19, 4), 
     contractfeedirectexpense     DECIMAL(19, 4), 
     contractreimballowexpense    DECIMAL(19, 4), 
     contractreimballowconsultant DECIMAL(19, 4) 
     PRIMARY KEY(contractwbs1, contractwbs2, contractwbs3) 
  ) 

INSERT @tabContract 
       (contractwbs1, 
        contractwbs2, 
        contractwbs3, 
        contractfee, 
        contractreimballow, 
        contractconsultfee, 
        contractfeedirectlabor, 
        contractfeedirectexpense, 
        contractreimballowexpense, 
        contractreimballowconsultant) 
SELECT contractdetails.wbs1                AS contractWBS1, 
       '' ''                                 AS contractWBS2, 
       '' ''                                 AS contractWBS3, 
       Sum(contractdetails.fee)            AS contractFee, 
       Sum(contractdetails.reimballow)     AS contractReimbAllow, 
       Sum(contractdetails.consultfee)     AS contractConsultFee, 
       Sum(contractdetails.feedirlab)      AS contractFeeDirectLabor, 
       Sum(contractdetails.feedirexp)      AS contractFeeDirectExpense, 
       Sum(contractdetails.reimballowexp)  AS contractReimbAllowExpense, 
       Sum(contractdetails.reimballowcons) AS contractReimbAllowConsultant 
FROM   contractdetails 
       JOIN contracts 
         ON contractdetails.wbs1 = contracts.wbs1 
            AND contractdetails.contractnumber = contracts.contractnumber 
WHERE  contracts.period <= @Period 
       AND contractdetails.wbs2 = '' '' 
       AND EXISTS (SELECT * 
                   FROM   @tabWBSList wbsList 
                   WHERE  wbsList.wbs1 = contractdetails.wbs1) 
GROUP  BY contractdetails.wbs1 
UNION ALL 
SELECT contractdetails.wbs1                AS contractWBS1, 
       contractdetails.wbs2                AS contractWBS2, 
       '' ''                                 AS contractWBS3, 
       Sum(contractdetails.fee)            AS contractFee, 
       Sum(contractdetails.reimballow)     AS contractReimbAllow, 
       Sum(contractdetails.consultfee)     AS contractConsultFee, 
       Sum(contractdetails.feedirlab)      AS contractFeeDirectLabor, 
       Sum(contractdetails.feedirexp)      AS contractFeeDirectExpense, 
       Sum(contractdetails.reimballowexp)  AS contractReimbAllowExpense, 
       Sum(contractdetails.reimballowcons) AS contractReimbAllowConsultant 
FROM   contractdetails 
       JOIN contracts 
         ON contractdetails.wbs1 = contracts.wbs1 
            AND contractdetails.contractnumber = contracts.contractnumber 
WHERE  contracts.period <= @Period 
       AND NOT contractdetails.wbs2 = '' '' 
       AND EXISTS (SELECT * 
                   FROM   @tabWBSList wbsList 
                   WHERE  wbsList.wbs1 = contractdetails.wbs1 
                          AND wbsList.wbs2 = contractdetails.wbs2) 
GROUP  BY contractdetails.wbs1, 
          contractdetails.wbs2 
UNION ALL 
SELECT contractdetails.wbs1                AS contractWBS1, 
       contractdetails.wbs2                AS contractWBS2, 
       contractdetails.wbs3                AS contractWBS3, 
       Sum(contractdetails.fee)            AS contractFee, 
       Sum(contractdetails.reimballow)     AS contractReimbAllow, 
       Sum(contractdetails.consultfee)     AS contractConsultFee, 
       Sum(contractdetails.feedirlab)      AS contractFeeDirectLabor, 
       Sum(contractdetails.feedirexp)      AS contractFeeDirectExpense, 
       Sum(contractdetails.reimballowexp)  AS contractReimbAllowExpense, 
       Sum(contractdetails.reimballowcons) AS contractReimbAllowConsultant 
FROM   contractdetails 
       JOIN contracts 
         ON contractdetails.wbs1 = contracts.wbs1 
            AND contractdetails.contractnumber = contracts.contractnumber 
WHERE  contracts.period <= @Period 
       AND NOT contractdetails.wbs3 = '' '' 
       AND EXISTS (SELECT * 
                   FROM   @tabWBSList wbsList 
                   WHERE  wbsList.wbs1 = contractdetails.wbs1 
                          AND wbsList.wbs2 = contractdetails.wbs2 
                          AND wbsList.wbs3 = contractdetails.wbs3) 
GROUP  BY contractdetails.wbs1, 
          contractdetails.wbs2, 
          contractdetails.wbs3 

SELECT Isnull(Substring(LEVEL3.org, 1, 2), '''') 
       AS 
       group1, 
       Min(groupCFGOrgCodes1.label) 
       AS groupDesc1, 
       Isnull(Substring(LEVEL3.org, 7, 3), '''') 
       AS group2, 
       Min(groupCFGOrgCodes3.label) 
       AS groupDesc2, 
       Isnull(pr.wbs1, '''') 
       AS group3, 
       Min(pr.NAME) 
       AS groupDesc3, 
       ''0'' 
       AS recordType, 
       '''' 
       subType, 
       0 
       AS hrsCur, 
       0 
       hrsCurLabReg, 
       0 
       hrsCurLabOvt, 
       0 
       AS hrsYTD, 
       0 
       hrsYTDLabReg, 
       0 
       hrsYTDLabOvt, 
       0 
       AS hrsJTD, 
       0 
       hrsJTDLabReg, 
       0 
       hrsJTDLabOvt, 
       0 
       AS hrsCustom, 
       0 
       hrsCustomLabReg, 
       0 
       hrsCustomLabOvt, 
       0 
       AS hrsBud, 
       0 
       AS hrsPctComp_c, 
       0 
       AS hrsPctComp_b, 
       0 
       AS hrsETC, 
       0 
       AS hrsEAC, 
       0 
       amtCurLabReg_c, 
       0 
       amtCurLabReg_b, 
       0 
       amtCurLabOvt_c, 
       0 
       amtCurLabOvt_b, 
       0 
       amtYTDLabReg_c, 
       0 
       amtYTDLabReg_b, 
       0 
       amtYTDLabOvt_c, 
       0 
       amtYTDLabOvt_b, 
       0 
       amtJTDLabReg_c, 
       0 
       amtJTDLabReg_b, 
       0 
       amtJTDLabOvt_c, 
       0 
       amtJTDLabOvt_b, 
       0 
       amtCustomLabReg_c, 
       0 
       amtCustomLabReg_b, 
       0 
       amtCustomLabOvt_c, 
       0 
       amtCustomLabOvt_b, 
       0 
       amtCur_c, 
       0 
       amtCur_b, 
       0 
       amtYTD_c, 
       0 
       amtYTD_b, 
       0 
       amtJTD_c, 
       0 
       amtJTD_b, 
       0 
       amtCustom_c, 
       0 
       amtCustom_b, 
       0 
       AS amtBud_c, 
       0 
       amtBud_b, 
       0 
       AS amtEAC_c, 
       0 
       amtEAC_b, 
       0 
       AS amtETC_c, 
       0 
       amtETC_b, 
       0 
       AS amtPctComp_c, 
       0 
       amtPctComp_b, 
       0 
       AS amtPctCompLab_c, 
       0 
       amtPctCompLab_b, 
       0 
       AS amtPctCompExp_c, 
       0 
       amtPctCompExp_b, 
       0 
       AS arCur_c, 
       0 
       AS arCur_b, 
       0 
       AS arYTD_c, 
       0 
       AS arYTD_b, 
       0 
       AS arJTD_c, 
       0 
       AS arJTD_b, 
       0 
       AS arCustom_c, 
       0 
       AS arCustom_b, 
       0 
       AS billCur_c, 
       0 
       AS billCur_b, 
       0 
       AS billYTD_c, 
       0 
       AS billYTD_b, 
       0 
       AS billJTD_c, 
       0 
       AS billJTD_b, 
       0 
       AS billCustom_c, 
       0 
       AS billCustom_b, 
       0 
       AS recdCur_c, 
       0 
       AS recdCur_b, 
       0 
       AS recdYTD_c, 
       0 
       AS recdYTD_b, 
       0 
       AS recdJTD_c, 
       0 
       AS recdJTD_b, 
       0 
       AS recdCustom_c, 
       0 
       AS recdCustom_b, 
       0 
       AS directCur_c, 
       0 
       directCur_b, 
       0 
       AS directYTD_c, 
       0 
       directYTD_b, 
       0 
       AS directJTD_c, 
       0 
       directJTD_b, 
       0 
       AS directCustom_c, 
       0 
       directCustom_b, 
       0 
       AS reimbCur_c, 
       0 
       reimbCur_b, 
       0 
       AS reimbYTD_c, 
       0 
       reimbYTD_b, 
       0 
       AS reimbJTD_c, 
       0 
       reimbJTD_b, 
       0 
       AS reimbCustom_c, 
       0 
       reimbCustom_b, 
       0 
       AS revCur_c, 
       0 
       AS revCur_b, 
       0 
       AS revYTD_c, 
       0 
       AS revYTD_b, 
       0 
       AS revJTD_c, 
       0 
       AS revJTD_b, 
       0 
       AS revCustom_c, 
       0 
       AS revCustom_b, 
       0.00 
       AS ubGLCur_c, 
       0.00 
       AS ubGLYTD_c, 
       0.00 
       AS ubGLJTD_c, 
       0.00 
       AS ubGLCustom_c, 
       0 
       althrsBud, 
       0 
       altamtBud_c, 
       0 
       altamtBud_b, 
       0.0 
       poAmt_c, 
       0.00 
       poAmt_b, 
       0.0 
       rppctcmpcalc, 
       0.00 
       targetMult_c, 
       0.00 
       targetMult_b, 
       0.00 
       rpfee_c, 
       0.00 
       rpfee_b, 
       0.00 
       rpconsultfee_c, 
       0.00 
       rpconsultfee_b, 
       0.00 
       rpreimballow_c, 
       0.00 
       rpreimballow_b, 
       '''' 
       AS currencyCodeProj, 
       '''' 
       AS currencyCodeBill, 
       '''' 
       AS currencyCodeFunct, 
       '''' 
       AS currencyCodePres_Cost, 
       '''' 
       AS currencyCodePres_Bill, 
       '''' 
       AS currencyCodePres_ARFee, 
       '''' 
       AS currencyCode_Opportunity, 
       0 
       AS currencyCodeProjCount, 
       0 
       AS currencyCodeBillCount, 
       0 
       AS currencyCodeFunctCount, 
       0 
       AS currencyCodePresCount_Cost, 
       0 
       AS currencyCodePresCount_Bill, 
       0 
       AS currencyCodePresCount_ARFee, 
       0 
       AS currencyCodeCount_Opportunity, 
       Min(LEVEL3.wbs1) 
       WBS1, 
       Min(LEVEL3.wbs2) 
       WBS2, 
       Min(LEVEL3.wbs3) 
       WBS3, 
       Min(pr.NAME) 
       w1Name, 
       Min(LEVEL2.NAME) 
       w2Name, 
       Min(LEVEL3.NAME) 
       w3Name, 
       '''' 
       ChargeType, 
       Max(pr.chargetype) 
       ChargeTypeCode, 
       Min(pr.sublevel) 
       SubLevel1, 
       Min(LEVEL2.sublevel) 
       SubLevel2, 
       Min(LEVEL3.sublevel) 
       SubLevel3, 
       Min(pr.principal) 
       w1principal, 
       Min(LEVEL2.principal) 
       w2principal, 
       Min(LEVEL3.principal) 
       w3principal, 
       '''' 
       w1prinName, 
       '''' 
       w2prinName, 
       '''' 
       w3prinName, 
       Min(pr.projmgr) 
       w1projMgr, 
       Min(LEVEL2.projmgr) 
       w2projMgr, 
       Min(LEVEL3.projmgr) 
       w3projMgr, 
       '''' 
       w1prgName, 
       '''' 
       w2prgName, 
       '''' 
       w3prgName, 
       Min(pr.supervisor) 
       w1supervisor, 
       Min(LEVEL2.supervisor) 
       w2supervisor, 
       Min(LEVEL3.supervisor) 
       w3supervisor, 
       '''' 
       w1supName, 
       '''' 
       w2supName, 
       '''' 
       w3supName, 
       Min(pr.address1) 
       PRAddress1, 
       Min(pr.address2) 
       PRAddress2, 
       Min(pr.address3) 
       PRAddress3, 
       Min(pr.city) 
       PRCity, 
       Min(pr.state) 
       PRState, 
       Min(pr.zip) 
       PRZip, 
       Min(pr.county) 
       PRCounty, 
       Min(Country_PR.country) 
       AS PRCountry, 
       Min(pr.phone) 
       PRPhone, 
       Min(pr.fax) 
       PRFax, 
       Min(pr.email) 
       PREmail, 
       Min(pr.clientid) 
       ClientID, 
       '''' 
       ClientName, 
       Min(pr.claddress) 
       CLAddress, 
       '''' 
       CLAddress1, 
       '''' 
       CLAddress2, 
       '''' 
       CLAddress3, 
       '''' 
       CLAddress4, 
       '''' 
       CLCity, 
       '''' 
       CLState, 
       '''' 
       CLZip, 
       '''' 
       CLCountry, 
       '''' 
       CLPhone, 
       '''' 
       CLFax, 
       '''' 
       CLEMail, 
       Min(pr.org) 
       w1Org, 
       Min(LEVEL2.org) 
       w2Org, 
       Min(LEVEL3.org) 
       w3Org, 
       '''' 
       w1OrgName, 
       '''' 
       w2OrgName, 
       '''' 
       w3OrgName, 
       Min(pr.tlprojectname) 
       AS w1TLProjectName, 
       Min(Level2.tlprojectname) 
       AS w2TLProjectName, 
       Min(Level3.tlprojectname) 
       AS w3TLProjectName, 
       Max(pr.servprocode) 
       AS ServiceProfileCode, 
       Max(cfgserviceprofile.description) 
       AS ServiceProfileDesc, 
       Max(LEVEL3.multamt) 
       mult, 
       Sum(LEVEL3.multamt) 
       mult_sum, 
       Sum(LEVEL3.fee) 
       AS fee_c, 
       Sum(LEVEL3.fee) 
       AS fee_b, 
       Sum(LEVEL3.consultfee) 
       AS consultFee_c, 
       Sum(LEVEL3.consultfee) 
       AS consultFee_b, 
       Sum(LEVEL3.reimballow) 
       AS reimbAllow_c, 
       Sum(LEVEL3.reimballow) 
       AS reimbAllow_b, 
       Sum(LEVEL3.feedirlab) 
       AS feeDirectLabor, 
       Sum(LEVEL3.feedirexp) 
       AS feeDirectExpense, 
       Sum(LEVEL3.reimballowexp) 
       AS reimbAllowExpense, 
       Sum(LEVEL3.reimballowcons) 
       AS reimbAllowConsultant, 
       Sum(Isnull(WBSLevelSumQuery.levelsumcontractfee, 0.0)) 
       AS contractFee, 
       Sum(Isnull(WBSLevelSumQuery.levelsumcontractconsultfee, 0.0)) 
       AS 
       contractConsultFee, 
       Sum(Isnull(WBSLevelSumQuery.levelsumcontractreimballow, 0.0)) 
       AS 
       contractReimbAllow, 
       Sum(Isnull(WBSLevelSumQuery.levelsumcontractfeedirectlabor, 0.0)) 
       AS 
       contractFeeDirectLabor, 
       Sum(Isnull(WBSLevelSumQuery.levelsumcontractfeedirectexpense, 0.0)) 
       AS 
       contractFeeDirectExpense, 
       Sum(Isnull(WBSLevelSumQuery.levelsumcontractreimballowexpense, 0.0)) 
       AS 
       contractReimbAllowExpense, 
       Sum(Isnull(WBSLevelSumQuery.levelsumcontractreimballowconsultant, 0.0)) 
       AS 
       contractReimbAllowConsultant, 
       Min(CASE 
             WHEN LEVEL3.sublevel = ''N'' THEN LEVEL3.budohrate 
             ELSE 0 
           END) 
       BudOHRate, 
       Min(pr.status) 
       w1Status, 
       Min(LEVEL2.status) 
       w2Status, 
       Min(LEVEL3.status) 
       w3Status, 
       '''' 
       w1StatusDesc, 
       '''' 
       w2StatusDesc, 
       '''' 
       w3StatusDesc, 
       Max(pr.revtype) 
       wbs1RevType, 
       Max(LEVEL2.revtype) 
       wbs2RevType, 
       Max(LEVEL3.revtype) 
       wbs3RevType, 
       '''' 
       wbs1RevTypeDesc, 
       '''' 
       wbs2RevTypeDesc, 
       '''' 
       wbs3RevTypeDesc, 
       Min(CASE 
             WHEN LEVEL3.sublevel = ''N'' THEN LEVEL3.unittable 
             ELSE '''' 
           END) 
       UnitTable, 
       Min(pr.startdate) 
       w1StartDate, 
       Min(LEVEL2.startdate) 
       w2StartDate, 
       Min(LEVEL3.startdate) 
       w3StartDate, 
       Min(pr.enddate) 
       w1EndDate, 
       Min(LEVEL2.enddate) 
       w2EndDate, 
       Min(LEVEL3.enddate) 
       w3EndDate, 
       Min(pr.pctcomp) 
       w1PctComp, 
       Min(LEVEL2.pctcomp) 
       w2PctComp, 
       Min(LEVEL3.pctcomp) 
       w3PctComp, 
       Min(pr.labpctcomp) 
       w1LabPctComp, 
       Min(LEVEL2.labpctcomp) 
       w2LabPctComp, 
       Min(LEVEL3.labpctcomp) 
       w3LabPctComp, 
       Min(pr.exppctcomp) 
       w1ExpPctComp, 
       Min(LEVEL2.exppctcomp) 
       w2ExpPctComp, 
       Min(LEVEL3.exppctcomp) 
       w3ExpPctComp, 
       Min(CASE 
             WHEN LEVEL3.sublevel = ''N'' THEN 
               CASE LEVEL3.billbydefault 
                 WHEN ''Y'' THEN ''Yes'' 
                 WHEN ''N'' THEN ''No'' 
                 ELSE ''Category'' 
               END 
             ELSE '''' 
           END) 
       BillByDefault, 
       Min(CASE 
             WHEN LEVEL3.sublevel = ''N'' THEN 
               CASE LEVEL3.billablewarning 
                 WHEN ''N'' THEN ''None'' 
                 WHEN ''W'' THEN ''Warning'' 
                 ELSE ''Error'' 
               END 
             ELSE '''' 
           END) 
       BillableWarning, 
       Min(CASE 
             WHEN LEVEL3.sublevel = ''N'' THEN Isnull(CASE LEVEL3.budgetedflag 
                                                      WHEN ''N'' THEN ''None'' 
                                                      WHEN ''W'' THEN ''Warning'' 
                                                      ELSE ''Error'' 
                                                    END, '''') 
             ELSE '''' 
           END) 
       BudgetedFlag, 
       Min(CASE 
             WHEN LEVEL3.sublevel = ''N'' THEN Isnull(CASE LEVEL3.budgetsource 
             WHEN ''B'' THEN ''Budget Worksheet'' 
             WHEN ''P'' THEN ''Resource Planning'' 
             ELSE '''' 
                                                    END, '''') 
             ELSE '''' 
           END) 
       BudgetSource, 
       Min(CASE 
             WHEN LEVEL3.sublevel = ''N'' THEN Isnull(CASE LEVEL3.budgetlevel 
             WHEN ''E'' THEN ''Employee Only'' 
             WHEN ''L'' THEN ''Labor Code Only'' 
             WHEN ''B'' THEN ''Both'' 
             ELSE '''' 
                                                    END, '''') 
             ELSE '''' 
           END) 
       BudgetLevel, 
       Min(CASE 
             WHEN LEVEL3.sublevel = ''N'' THEN 
             Isnull(LEVEL3.budgetedlevels, '''') 
             ELSE '''' 
           END) 
       BudgetedLevels, 
       Min(CASE 
             WHEN LEVEL3.sublevel = ''N'' THEN 
               CASE LEVEL3.xcharge 
                 WHEN ''G'' THEN ''Global'' 
                 WHEN ''N'' THEN ''None'' 
                 ELSE ''Project'' 
               END 
             ELSE '''' 
           END) 
       XCharge, 
       Min(CASE 
             WHEN LEVEL3.sublevel = ''N'' THEN 
               CASE LEVEL3.xchargemethod 
                 WHEN ''1'' THEN ''BillingTerms'' 
                 ELSE ''Multiplier'' 
               END 
             ELSE '''' 
           END) 
       XChargeMethod, 
       Min(CASE 
             WHEN LEVEL3.sublevel = ''N'' THEN LEVEL3.xchargemult 
             ELSE 0 
           END) 
       XChargeMult, 
       Min(pr.contactid) 
       ContactID, 
       '''' 
       ContactFirstName, 
       '''' 
       ContactLastName, 
       Min(pr.clbillingaddr) 
       CLBillingAddr, 
       Min(pr.longname) 
       w1LongName, 
       Min(LEVEL2.longname) 
       w2LongName, 
       Min(LEVEL3.longname) 
       w3LongName, 
       '''' 
       CLBAddress1, 
       '''' 
       CLBAddress2, 
       '''' 
       CLBAddress3, 
       '''' 
       CLBAddress4, 
       '''' 
       CLBCity, 
       '''' 
       CLBState, 
       '''' 
       CLBZip, 
       '''' 
       CLBCountry, 
       '''' 
       CLBPhone, 
       '''' 
       CLBFax, 
       '''' 
       CLBEMail, 
       Min(pr.federalind) 
       w1FederalInd, 
       Min(LEVEL2.federalind) 
       w2FederalInd, 
       Min(LEVEL3.federalind) 
       w3FederalInd, 
       Min(pr.projecttype) 
       w1ProjectType, 
       Min(LEVEL2.projecttype) 
       w2ProjectType, 
       Min(LEVEL3.projecttype) 
       w3ProjectType, 
       '''' 
       w1projectTypeDesc, 
       '''' 
       w2projectTypeDesc, 
       '''' 
       w3projectTypeDesc, 
       Min(pr.responsibility) 
       w1Responsibility, 
       Min(LEVEL2.responsibility) 
       w2Responsibility, 
       Min(LEVEL3.responsibility) 
       w3Responsibility, 
       '''' 
       w1ResponsibilityDesc, 
       '''' 
       w2ResponsibilityDesc, 
       '''' 
       w3ResponsibilityDesc, 
       Min(pr.referable) 
       w1Referable, 
       Min(LEVEL2.referable) 
       w2Referable, 
       Min(LEVEL3.referable) 
       w3Referable, 
       Min(pr.estcompletiondate) 
       w1EstCompletionDate, 
       Min(LEVEL2.estcompletiondate) 
       w2EstCompletionDate, 
       Min(LEVEL3.estcompletiondate) 
       w3EstCompletionDate, 
       Min(pr.actcompletiondate) 
       w1ActCompletionDate, 
       Min(LEVEL2.actcompletiondate) 
       w2ActCompletionDate, 
       Min(LEVEL3.actcompletiondate) 
       w3ActCompletionDate, 
       Min(pr.contractdate) 
       w1ContractDate, 
       Min(LEVEL2.contractdate) 
       w2ContractDate, 
       Min(LEVEL3.contractdate) 
       w3ContractDate, 
       Min(pr.constcompldate) 
       w1ConstComplDate, 
       Min(LEVEL2.constcompldate) 
       w2ConstComplDate, 
       Min(LEVEL3.constcompldate) 
       w3ConstComplDate, 
       Min(pr.profservicescompldate) 
       w1ProfServicesComplDate, 
       Min(LEVEL2.profservicescompldate) 
       w2ProfServicesComplDate, 
       Min(LEVEL3.profservicescompldate) 
       w3ProfServicesComplDate, 
       Min(pr.biddate) 
       w1BidDate, 
       Min(LEVEL2.biddate) 
       w2BidDate, 
       Min(LEVEL3.biddate) 
       w3BidDate, 
       Min(pr.compldatecomment) 
       w1ComplDateComment, 
       Min(LEVEL2.compldatecomment) 
       w2ComplDateComment, 
       Min(LEVEL3.compldatecomment) 
       w3ComplDateComment, 
       Min(pr.firmcost) 
       w1FirmCost, 
       Min(LEVEL2.firmcost) 
       w2FirmCost, 
       Min(LEVEL3.firmcost) 
       w3FirmCost, 
       Min(pr.firmcostcomment) 
       w1FirmCostComment, 
       Min(LEVEL2.firmcostcomment) 
       w2FirmCostComment, 
       Min(LEVEL3.firmcostcomment) 
       w3FirmCostComment, 
       Min(pr.totalprojectcost) 
       w1TotalProjectCost, 
       Min(LEVEL2.totalprojectcost) 
       w2TotalProjectCost, 
       Min(LEVEL3.totalprojectcost) 
       w3TotalProjectCost, 
       Min(pr.totalcostcomment) 
       w1TotalCostComment, 
       Min(LEVEL2.totalcostcomment) 
       w2TotalCostComment, 
       Min(LEVEL3.totalcostcomment) 
       w3TotalCostComment, 
       Min(pr.clientconfidential) 
       ClientConfidential, 
       Min(pr.clientalias) 
       ClientAlias, 
       Min(pr.billingclientid) 
       BillingClientID, 
       '''' 
       BillClientName, 
       Min(pr.billingcontactid) 
       BillingContactID, 
       '''' 
       billContactLastName, 
       '''' 
       billContactFirstName, 
       Min(pr.proposalwbs1) 
       ProposalWBS1, 
       Min(pr.createuser) 
       w1CreateUser, 
       Min(LEVEL2.createuser) 
       w2CreateUser, 
       Min(LEVEL3.createuser) 
       w3CreateUser, 
       Min(Dateadd("hh", -5, pr.createdate)) 
       w1CreateDate, 
       Min(Dateadd("hh", -5, LEVEL2.createdate)) 
       w2CreateDate, 
       Min(Dateadd("hh", -5, LEVEL3.createdate)) 
       w3CreateDate, 
       Min(pr.moduser) 
       w1ModUser, 
       Min(LEVEL2.moduser) 
       w2ModUser, 
       Min(LEVEL3.moduser) 
       w3ModUser, 
       Min(Dateadd("hh", -5, pr.moddate)) 
       w1ModDate, 
       Min(Dateadd("hh", -5, LEVEL2.moddate)) 
       w2ModDate, 
       Min(Dateadd("hh", -5, LEVEL3.moddate)) 
       w3ModDate, 
       Min(CASE pr.costratemeth 
             WHEN 0 THEN ''None'' 
             WHEN 4 THEN ''From Employee Cost Rate'' 
             WHEN 1 THEN ''From Labor Rate Table'' 
             WHEN 2 THEN ''From Category Rate Table'' 
             WHEN 3 THEN ''From Labor Code Table'' 
             ELSE '''' 
           END) 
       w1CostRateMeth, 
       Min(CASE LEVEL2.costratemeth 
             WHEN 0 THEN ''None'' 
             WHEN 4 THEN ''From Employee Cost Rate'' 
             WHEN 1 THEN ''From Labor Rate Table'' 
             WHEN 2 THEN ''From Category Rate Table'' 
             WHEN 3 THEN ''From Labor Code Table'' 
             ELSE '''' 
           END) 
       w2CostRateMeth, 
       Min(CASE LEVEL3.costratemeth 
             WHEN 0 THEN ''None'' 
             WHEN 4 THEN ''From Employee Cost Rate'' 
             WHEN 1 THEN ''From Labor Rate Table'' 
             WHEN 2 THEN ''From Category Rate Table'' 
             WHEN 3 THEN ''From Labor Code Table'' 
             ELSE '''' 
           END) 
       w3CostRateMeth, 
       '''' 
       w1CostRateTableNo, 
       '''' 
       w2CostRateTableNo, 
       '''' 
       w3CostRateTableNo, 
       Min(CASE pr.payratemeth 
             WHEN 0 THEN ''None'' 
             WHEN 4 THEN ''From Employee Cost Rate'' 
             WHEN 1 THEN ''From Labor Rate Table'' 
             WHEN 2 THEN ''From Category Rate Table'' 
             WHEN 3 THEN ''From Labor Code Table'' 
             ELSE '''' 
           END) 
       w1PayRateMeth, 
       Min(CASE LEVEL2.payratemeth 
             WHEN 0 THEN ''None'' 
             WHEN 4 THEN ''From Employee Cost Rate'' 
             WHEN 1 THEN ''From Labor Rate Table'' 
             WHEN 2 THEN ''From Category Rate Table'' 
             WHEN 3 THEN ''From Labor Code Table'' 
             ELSE '''' 
           END) 
       w2PayRateMeth, 
       Min(CASE LEVEL3.payratemeth 
             WHEN 0 THEN ''None'' 
             WHEN 4 THEN ''From Employee Cost Rate'' 
             WHEN 1 THEN ''From Labor Rate Table'' 
             WHEN 2 THEN ''From Category Rate Table'' 
             WHEN 3 THEN ''From Labor Code Table'' 
             ELSE '''' 
           END) 
       w3PayRateMeth, 
       '''' 
       w1PayRateTableNo, 
       '''' 
       w2PayRateTableNo, 
       '''' 
       w3PayRateTableNo, 
       Min(pr.locale) 
       w1Locale, 
       Min(LEVEL2.locale) 
       w2Locale, 
       Min(LEVEL3.locale) 
       w3Locale, 
       Min(CASE pr.lineitemapproval 
             WHEN ''Y'' THEN ''Yes'' 
             WHEN ''N'' THEN ''No'' 
             ELSE ''System'' 
           END) 
       w1LineItemApproval, 
       Min(CASE LEVEL2.lineitemapproval 
             WHEN ''Y'' THEN ''Yes'' 
             WHEN ''N'' THEN ''No'' 
             ELSE ''System'' 
           END) 
       w2LineItemApproval, 
       Min(CASE LEVEL3.lineitemapproval 
             WHEN ''Y'' THEN ''Yes'' 
             WHEN ''N'' THEN ''No'' 
             ELSE ''System'' 
           END) 
       w3LineItemApproval, 
       Min(CASE pr.lineitemapprovalek 
             WHEN ''Y'' THEN ''Yes'' 
             WHEN ''N'' THEN ''No'' 
             ELSE ''System'' 
           END) 
       w1LineItemApprovalEK, 
       Min(CASE LEVEL2.lineitemapprovalek 
             WHEN ''Y'' THEN ''Yes'' 
             WHEN ''N'' THEN ''No'' 
             ELSE ''System'' 
           END) 
       w2LineItemApprovalEK, 
       Min(CASE LEVEL3.lineitemapprovalek 
             WHEN ''Y'' THEN ''Yes'' 
             WHEN ''N'' THEN ''No'' 
             ELSE ''System'' 
           END) 
       w3LineItemApprovalEK, 
       '''' 
       opp_name, 
       '''' 
       opp_number, 
       '''' 
       opp_prproposalwbs1, 
       '''' 
       opp_prproposalname, 
       '''' 
       opp_org, 
       '''' 
       opp_orgname, 
       '''' 
       opp_clname, 
       '''' 
       opp_contactname, 
       '''' 
       opp_projmgrname, 
       '''' 
       opp_principalname, 
       '''' 
       opp_supervisorname, 
       0 
       opp_revenue, 
       0 
       opp_probability, 
       0 
       opp_weightedrevenue, 
       '''' 
       opp_opendate, 
       '''' 
       opp_closedate, 
       0 
       opp_daysopen, 
       '''' 
       opp_stage, 
       '''' 
       opp_type, 
       '''' 
       opp_source, 
       '''' 
       opp_status, 
       '''' 
       opp_eststartdate, 
       '''' 
       opp_estcompletiondate, 
       '''' 
       opp_address1, 
       '''' 
       opp_address2, 
       '''' 
       opp_address3, 
       '''' 
       opp_city, 
       '''' 
       opp_state, 
       '''' 
       opp_zip, 
       '''' 
       opp_country, 
       '''' 
       opp_createuser, 
       '''' 
       opp_createdate, 
       '''' 
       opp_moduser, 
       '''' 
       opp_moddate, 
       '''' 
       opp_oppdesc 
FROM   pr AS LEVEL3 
       LEFT JOIN projectcustomtabfields 
              ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 
                 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 
                 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes1 
              ON Substring(LEVEL3.org, 1, 2) = groupCFGOrgCodes1.code 
                 AND groupCFGOrgCodes1.orglevel = 1 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes3 
              ON Substring(LEVEL3.org, 7, 3) = groupCFGOrgCodes3.code 
                 AND groupCFGOrgCodes3.orglevel = 3 
       LEFT JOIN (SELECT LEVEL3.wbs1, 
                         LEVEL3.wbs2 AS MaxWBS2, 
                         LEVEL3.wbs3 AS MaxWBS3, 
                         Max(CASE 
                               WHEN LEVEL3.sublevel = ''N'' THEN 
                               Isnull(tabContract.contractfee, 0.0) 
                               ELSE -999999999.99 
                             END)    AS LevelSumContractFee, 
                         Max(CASE 
                               WHEN LEVEL3.sublevel = ''N'' THEN 
                               Isnull(tabContract.contractreimballow, 0.0) 
                               ELSE -999999999.99 
                             END)    AS LevelSumContractReimbAllow, 
                         Max(CASE 
                               WHEN LEVEL3.sublevel = ''N'' THEN 
                               Isnull(tabContract.contractconsultfee, 0.0) 
                               ELSE -999999999.99 
                             END)    AS LevelSumContractConsultFee, 
                         Max(CASE 
                               WHEN LEVEL3.sublevel = ''N'' THEN 
                               Isnull(tabContract.contractfeedirectlabor, 0.0) 
                               ELSE -999999999.99 
                             END)    AS LevelSumContractFeeDirectLabor, 
                         Max(CASE 
                               WHEN LEVEL3.sublevel = ''N'' THEN 
                               Isnull(tabContract.contractfeedirectexpense, 0.0) 
                               ELSE -999999999.99 
                             END)    AS LevelSumContractFeeDirectExpense, 
                         Max(CASE 
                               WHEN LEVEL3.sublevel = ''N'' THEN 
Isnull(tabContract.contractreimballowexpense, 0.0) 
ELSE -999999999.99 
END)    AS LevelSumContractReimbAllowExpense, 
Max(CASE 
WHEN LEVEL3.sublevel = ''N'' THEN Isnull( 
tabContract.contractreimballowconsultant, 0.0) 
ELSE -999999999.99 
END)    AS LevelSumContractReimbAllowConsultant 
FROM   pr AS LEVEL3 
LEFT JOIN projectcustomtabfields 
 ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 
    AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 
    AND LEVEL3.wbs3 = projectcustomtabfields.wbs3 
LEFT JOIN @tabContract tabContract 
 ON tabContract.contractwbs1 = LEVEL3.wbs1 
    AND tabContract.contractwbs2 = LEVEL3.wbs2 
    AND tabContract.contractwbs3 = LEVEL3.wbs3, 
pr AS LEVEL2, 
pr, 
##rwl_' + @SessionId + ' 
WHERE  LEVEL3.wbs1 = pr.wbs1 
AND LEVEL3.wbs1 = LEVEL2.wbs1 
AND LEVEL3.wbs2 = LEVEL2.wbs2 
AND pr.wbs2 = '' '' 
AND pr.wbs3 = '' '' 
AND LEVEL2.wbs3 = '' '' 
AND (( (( ( pr.wbs1 = ''NC1008.000'' ) 
		OR ( pr.wbs1 = ''NC1010.000'' ) 
		OR ( pr.wbs1 = ''NC1010.001'' )  
		OR ( pr.wbs1 = ''NC1010.002'' )  
		OR ( pr.wbs1 = ''NC1010.003'' )  
		OR ( pr.wbs1 = ''NC1011.001'' )  
		OR ( pr.wbs1 = ''NC3008.000'' ) 
		OR ( pr.wbs1 = ''NC3010.000'' ) 
		OR ( pr.wbs1 = ''NC3010.001'' )  
		OR ( pr.wbs1 = ''NC3010.002'' )  
		OR ( pr.wbs1 = ''NC3010.003'' )  
		OR ( pr.wbs1 = ''NC3011.001'' )  
	 )) 
  OR ( ( pr.wbs1 LIKE ''P%'' ) 
       AND EXISTS (SELECT ''x'' 
                   FROM   cfgchargetype 
                   WHERE  cfgchargetype.type = 
                          pr.chargetype 
                          AND (( cfgchargetype.label = 
                                  
                                 ''Promotional'' 
                               ))) 
     ) )) 
AND LEVEL3.wbs1 = ##rwl_' + @SessionId + '.wbs1 
AND ##rwl_' + @SessionId + '.wbs2 = '' '' 
AND ##rwl_' + @SessionId + '.wbs3 = '' '' 
AND ##rwl_' + @SessionId + '.sessionid = 
''' + @SessionId + ''' 
GROUP  BY LEVEL3.wbs1, 
LEVEL3.wbs2, 
LEVEL3.wbs3) AS WBSLevelSumQuery 
ON LEVEL3.wbs1 = WBSLevelSumQuery.wbs1 
AND LEVEL3.wbs2 = WBSLevelSumQuery.maxwbs2 
AND LEVEL3.wbs3 = WBSLevelSumQuery.maxwbs3, 
pr AS LEVEL2, 
pr 
LEFT JOIN cfgserviceprofile 
ON cfgserviceprofile.code = pr.servprocode 
LEFT JOIN fw_cfgcountry Country_PR 
ON pr.country = Country_PR.isocountrycode, 
##rwl_' + @SessionId + ' 
WHERE  LEVEL3.sublevel = ''N'' 
       AND (( (( ( pr.wbs1 = ''NC1008.000'' ) 
					OR ( pr.wbs1 = ''NC1010.000'' ) 
					OR ( pr.wbs1 = ''NC1010.001'' )  
					OR ( pr.wbs1 = ''NC1010.002'' )  
					OR ( pr.wbs1 = ''NC1010.003'' )  
					OR ( pr.wbs1 = ''NC1011.001'' )  
					OR ( pr.wbs1 = ''NC3008.000'' ) 
					OR ( pr.wbs1 = ''NC3010.000'' ) 
					OR ( pr.wbs1 = ''NC3010.001'' )  
					OR ( pr.wbs1 = ''NC3010.002'' )  
					OR ( pr.wbs1 = ''NC3010.003'' )  
					OR ( pr.wbs1 = ''NC3011.001'' )  
				  )) 
               OR ( ( pr.wbs1 LIKE ''P%'' ) 
                    AND EXISTS (SELECT ''x'' 
                                FROM   cfgchargetype 
                                WHERE  cfgchargetype.type = pr.chargetype 
                                       AND (( cfgchargetype.label =  
                                              ''Promotional'' 
                                            ))) 
                  ) )) 
       AND LEVEL3.wbs1 = ##rwl_' + @SessionId + '.wbs1 
       AND ##rwl_' + @SessionId + '.wbs2 = '' '' 
       AND ##rwl_' + @SessionId + '.wbs3 = '' '' 
       AND ##rwl_' + @SessionId + '.sessionid = 
           ''' + @SessionId + ''' 
       AND pr.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs2 = LEVEL3.wbs2 
       AND pr.wbs2 = '''' 
       AND pr.wbs3 = '''' 
       AND LEVEL2.wbs3 = '''' 
GROUP  BY Isnull(Substring(LEVEL3.org, 1, 2), ''''), 
          Isnull(Substring(LEVEL3.org, 7, 3), ''''), 
          Isnull(pr.wbs1, '''') 
/***Labor***/ 
UNION ALL 
SELECT Isnull(Substring(LEVEL3.org, 1, 2), '''') AS group1, 
       Min(groupCFGOrgCodes1.label)            AS groupDesc1, 
       Isnull(Substring(LEVEL3.org, 7, 3), '''') AS group2, 
       Min(groupCFGOrgCodes3.label)            AS groupDesc2, 
       Isnull(pr.wbs1, '''')                     AS group3, 
       Min(pr.NAME)                            AS groupDesc3, 
       ''1''                                     AS recordType, 
       ''''                                      subType, 
       Sum(CASE 
             WHEN period = @Period THEN hours 
             ELSE 0 
           END)                                hrsCur, 
       Sum(CASE 
             WHEN period = @Period THEN reghours 
             ELSE 0 
           END)                                hrsCurLabReg, 
       Sum(CASE 
             WHEN period = @Period THEN ovthours 
             ELSE 0 
           END)                                hrsCurLabOvt, 
       Sum(CASE 
             WHEN period >= @MinPeriod THEN hours 
             ELSE 0 
           END)                                hrsYTD, 
       Sum(CASE 
             WHEN period >= @MinPeriod THEN reghours 
             ELSE 0 
           END)                                hrsYTDLabReg, 
       Sum(CASE 
             WHEN period >= @MinPeriod THEN ovthours 
             ELSE 0 
           END)                                hrsYTDLabOvt, 
       Sum(hours)                              hrsJTD, 
       Sum(reghours)                           hrsJTDLabReg, 
       Sum(ovthours)                           hrsJTDLabOvt, 
       Sum(CASE 
             WHEN period >= 201801 
                  AND period <= 201801 THEN hours 
             ELSE 0 
           END)                                hrsCustom, 
       Sum(CASE 
             WHEN period >= 201801 
                  AND period <= 201801 THEN reghours 
             ELSE 0 
           END)                                hrsCustomLabReg, 
       Sum(CASE 
             WHEN period >= 201801 
                  AND period <= 201801 THEN ovthours 
             ELSE 0 
           END)                                hrsCustomLabOvt, 
       0                                       AS hrsBud, 
       0                                       AS hrsPctComp_c, 
       0                                       AS hrsPctComp_b, 
       0                                       AS hrsETC, 
       0                                       AS hrsEAC, 
       Sum(( CASE 
               WHEN period = @Period THEN Isnull(laborregamt, 0) 
               ELSE 0 
             END ))                            amtCurLabReg_c, 
       Sum(CASE 
             WHEN period = @Period THEN laborbilling 
             ELSE 0 
           END)                                amtCurLabReg_b, 
       Sum(( CASE 
               WHEN period = @Period THEN Isnull(laborovtamt, 0) 
               ELSE 0 
             END ))                            amtCurLabOvt_c, 
       0                                       AS amtCurLabOvt_b, 
       Sum(( CASE 
               WHEN period >= @MinPeriod THEN Isnull(laborregamt, 0) 
               ELSE 0 
             END ))                            amtYTDLabReg_c, 
       Sum(CASE 
             WHEN period >= @MinPeriod THEN laborbilling 
             ELSE 0 
           END)                                amtYTDLabReg_b, 
       Sum(( CASE 
               WHEN period >= @MinPeriod THEN Isnull(laborovtamt, 0) 
               ELSE 0 
             END ))                            amtYTDLabOvt_c, 
       0                                       AS amtYTDLabOvt_b, 
       Sum(Isnull(laborregamt, 0))             amtJTDLabReg_c, 
       Sum(laborbilling)                       amtJTDLabReg_b, 
       Sum(Isnull(laborovtamt, 0))             amtJTDLabOvt_c, 
       0                                       AS amtJTDLabOvt_b, 
       Sum(( CASE 
               WHEN period >= 201801 
                    AND period <= 201801 THEN Isnull(laborregamt, 0) 
               ELSE 0 
             END ))                            amtCustomLabReg_c, 
       Sum(CASE 
             WHEN period >= 201801 
                  AND period <= 201801 THEN laborbilling 
             ELSE 0 
           END)                                amtCustomLabReg_b, 
       Sum(( CASE 
               WHEN period >= 201801 
                    AND period <= 201801 THEN Isnull(laborovtamt, 0) 
               ELSE 0 
             END ))                            amtCustomLabOvt_c, 
       0                                       AS amtCustomLabOvt_b, 
       Sum(( CASE 
               WHEN period = @Period THEN Isnull(laborcost, 0) 
               ELSE 0 
             END ))                            amtCur_c, 
       Sum(CASE 
             WHEN period = @Period THEN laborbilling 
             ELSE 0 
           END)                                amtCur_b, 
       Sum(( CASE 
               WHEN period >= @MinPeriod THEN Isnull(laborcost, 0) 
               ELSE 0 
             END ))                            amtYTD_c, 
       Sum(CASE 
             WHEN period >= @MinPeriod THEN laborbilling 
             ELSE 0 
           END)                                amtYTD_b, 
       Sum(Isnull(laborcost, 0))               AS amtJTD_c, 
       Sum(laborbilling)                       amtJTD_b, 
       Sum(( CASE 
               WHEN period >= 201801 
                    AND period <= 201801 THEN Isnull(laborcost, 0) 
               ELSE 0 
             END ))                            AS amtCustom_c, 
       Sum(CASE 
             WHEN period >= 201801 
                  AND period <= 201801 THEN laborbilling 
             ELSE 0 
           END)                                amtCustom_b, 
       0                                       AS amtBud_c, 
       0                                       amtBud_b, 
       0                                       AS amtEAC_c, 
       0                                       amtEAC_b, 
       0                                       AS amtETC_c, 
       0                                       amtETC_b, 
       0                                       AS amtPctComp_c, 
       0                                       amtPctComp_b, 
       0                                       AS amtPctCompLab_c, 
       0                                       amtPctCompLab_b, 
       0                                       AS amtPctCompExp_c, 
       0                                       amtPctCompExp_b, 
       0                                       AS arCur_c, 
       0                                       AS arCur_b, 
       0                                       AS arYTD_c, 
       0                                       AS arYTD_b, 
       0                                       AS arJTD_c, 
       0                                       AS arJTD_b, 
       0                                       AS arCustom_c, 
       0                                       AS arCustom_b, 
       0                                       AS billCur_c, 
       0                                       AS billCur_b, 
       0                                       AS billYTD_c, 
       0                                       AS billYTD_b, 
       0                                       AS billJTD_c, 
       0                                       AS billJTD_b, 
       0                                       AS billCustom_c, 
       0                                       AS billCustom_b, 
       0                                       AS recdCur_c, 
       0                                       AS recdCur_b, 
       0                                       AS recdYTD_c, 
       0                                       AS recdYTD_b, 
       0                                       AS recdJTD_c, 
       0                                       AS recdJTD_b, 
       0                                       AS recdCustom_c, 
       0                                       AS recdCustom_b, 
       0                                       AS directCur_c, 
       0                                       directCur_b, 
       0                                       AS directYTD_c, 
       0                                       directYTD_b, 
       0                                       AS directJTD_c, 
       0                                       directJTD_b, 
       0                                       AS directCustom_c, 
       0                                       directCustom_b, 
       0                                       AS reimbCur_c, 
       0                                       reimbCur_b, 
       0                                       AS reimbYTD_c, 
       0                                       reimbYTD_b, 
       0                                       AS reimbJTD_c, 
       0                                       reimbJTD_b, 
       0                                       AS reimbCustom_c, 
       0                                       reimbCustom_b, 
       0                                       AS revCur_c, 
       0                                       AS revCur_b, 
       0                                       AS revYTD_c, 
       0                                       AS revYTD_b, 
       0                                       AS revJTD_c, 
       0                                       AS revJTD_b, 
       0                                       AS revCustom_c, 
       0                                       AS revCustom_b, 
       0.00                                    AS ubGLCur_c, 
       0.00                                    AS ubGLYTD_c, 
       0.00                                    AS ubGLJTD_c, 
       0.00                                    AS ubGLCustom_c, 
       0                                       althrsBud, 
       0                                       altamtBud_c, 
       0                                       altamtBud_b, 
       0.0                                     poAmt_c, 
       0.00                                    poAmt_b, 
       0.0                                     rppctcmpcalc, 
       0.00                                    targetMult_c, 
       0.00                                    targetMult_b, 
       0.00                                    rpfee_c, 
       0.00                                    rpfee_b, 
       0.00                                    rpconsultfee_c, 
       0.00                                    rpconsultfee_b, 
       0.00                                    rpreimballow_c, 
       0.00                                    rpreimballow_b, 
       ''''                                      AS currencyCodeProj, 
       ''''                                      AS currencyCodeBill, 
       ''''                                      AS currencyCodeFunct, 
       ''''                                      AS currencyCodePres_Cost, 
       ''''                                      AS currencyCodePres_Bill, 
       ''''                                      AS currencyCodePres_ARFee, 
       ''''                                      AS currencyCode_Opportunity, 
       0                                       AS currencyCodeProjCount, 
       0                                       AS currencyCodeBillCount, 
       0                                       AS currencyCodeFunctCount, 
       0                                       AS currencyCodePresCount_Cost, 
       0                                       AS currencyCodePresCount_Bill, 
       0                                       AS currencyCodePresCount_ARFee, 
       0                                       AS currencyCodeCount_Opportunity, 
       Min(pr.wbs1)                            WBS1, 
       Min(LEVEL2.wbs2)                        WBS2, 
       Min(LEVEL3.wbs3)                        WBS3, 
       ''''                                      w1Name, 
       ''''                                      w2Name, 
       ''''                                      w3Name, 
       ''''                                      ChargeType, 
       Max(pr.chargetype)                      ChargeTypeCode, 
       Min(pr.sublevel)                        SubLevel1, 
       Min(LEVEL2.sublevel)                    SubLevel2, 
       Min(LEVEL3.sublevel)                    SubLevel3, 
       ''''                                      w1principal, 
       ''''                                      w2principal, 
       ''''                                      w3principal, 
       ''''                                      w1prinName, 
       ''''                                      w2prinName, 
       ''''                                      w3prinName, 
       ''''                                      w1projMgr, 
       ''''                                      w2projMgr, 
       ''''                                      w3projMgr, 
       ''''                                      w1prgName, 
       ''''                                      w2prgName, 
       ''''                                      w3prgName, 
       ''''                                      w1supervisor, 
       ''''                                      w2supervisor, 
       ''''                                      w3supervisor, 
       ''''                                      w1supName, 
       ''''                                      w2supName, 
       ''''                                      w3supName, 
       ''''                                      PRAddress1, 
       ''''                                      PRAddress2, 
       ''''                                      PRAddress3, 
       ''''                                      PRCity, 
       ''''                                      PRState, 
       ''''                                      PRZip, 
       ''''                                      PRCounty, 
       ''''                                      PRCountry, 
       ''''                                      PRPhone, 
       ''''                                      PRFax, 
       ''''                                      PREmail, 
       ''''                                      ClientID, 
       ''''                                      ClientName, 
       ''''                                      CLAddress, 
       ''''                                      CLAddress1, 
       ''''                                      CLAddress2, 
       ''''                                      CLAddress3, 
       ''''                                      CLAddress4, 
       ''''                                      CLCity, 
       ''''                                      CLState, 
       ''''                                      CLZip, 
       ''''                                      CLCountry, 
       ''''                                      CLPhone, 
       ''''                                      CLFax, 
       ''''                                      CLEMail, 
       ''''                                      w1Org, 
       ''''                                      w2Org, 
       ''''                                      w3Org, 
       ''''                                      w1OrgName, 
       ''''                                      w2OrgName, 
       ''''                                      w3OrgName, 
       ''''                                      AS w1TLProjectName, 
       ''''                                      AS w2TLProjectName, 
       ''''                                      AS w3TLProjectName, 
       ''''                                      ServiceProfileCode, 
       ''''                                      ServiceProfileDesc, 
       0                                       mult, 
       0                                       mult_sum, 
       0                                       fee_c, 
       0                                       fee_b, 
       0                                       consultFee_c, 
       0                                       consultFee_b, 
       0                                       reimbAllow_c, 
       0                                       reimbAllow_b, 
       0.0                                     feeDirectLabor, 
       0.0                                     feeDirectExpense, 
       0.0                                     reimbAllowExpense, 
       0.0                                     reimbAllowConsultant, 
       0                                       contractfee, 
       0                                       contractconsultFee, 
       0                                       contractreimbAllow, 
       0.0                                     contractFeeDirectLabor, 
       0.0                                     contractFeeDirectExpense, 
       0.0                                     contractReimbAllowExpense, 
       0.0                                     contractReimbAllowConsultant, 
       0                                       BudOHRate, 
       ''''                                      w1Status, 
       ''''                                      w2Status, 
       ''''                                      w3Status, 
       ''''                                      w1StatusDesc, 
       ''''                                      w2StatusDesc, 
       ''''                                      w3StatusDesc, 
       ''''                                      wbs1RevType, 
       ''''                                      wbs2RevType, 
       ''''                                      wbs3RevType, 
       ''''                                      wbs1RevTypeDesc, 
       ''''                                      wbs2RevTypeDesc, 
       ''''                                      wbs3RevTypeDesc, 
       ''''                                      UnitTable, 
       NULL                                    w1StartDate, 
       NULL                                    w2StartDate, 
       NULL                                    w3StartDate, 
       NULL                                    w1EndDate, 
       NULL                                    w2EndDate, 
       NULL                                    w3EndDate, 
       0.00                                    w1PctComp, 
       0.00                                    w2PctComp, 
       0.00                                    w3PctComp, 
       0.00                                    w1LabPctComp, 
       0.00                                    w2LabPctComp, 
       0.00                                    w3LabPctComp, 
       0.00                                    w1ExpPctComp, 
       0.00                                    w2ExpPctComp, 
       0.00                                    w3ExpPctComp, 
       ''''                                      BillByDefault, 
       ''''                                      BillableWarning, 
       ''''                                      BudgetedFlag, 
       ''''                                      BudgetSource, 
       ''''                                      BudgetLevel, 
       ''''                                      BudgetedLevels, 
       ''''                                      XCharge, 
       ''''                                      XChargeMethod, 
       0                                       XChargeMult, 
       ''''                                      ContactID, 
       ''''                                      ContactFirstName, 
       ''''                                      ContactLastName, 
       ''''                                      CLBillingAddr, 
       ''''                                      w1LongName, 
       ''''                                      w2LongName, 
       ''''                                      w3LongName, 
       ''''                                      CLBAddress1, 
       ''''                                      CLBAddress2, 
       ''''                                      CLBAddress3, 
       ''''                                      CLBAddress4, 
       ''''                                      CLBCity, 
       ''''                                      CLBState, 
       ''''                                      CLBZip, 
       ''''                                      CLBCountry, 
       ''''                                      CLBPhone, 
       ''''                                      CLBFax, 
       ''''                                      CLBEMail, 
       ''''                                      w1FederalInd, 
       ''''                                      w2FederalInd, 
       ''''                                      w3FederalInd, 
       ''''                                      w1ProjectType, 
       ''''                                      w2ProjectType, 
       ''''                                      w3ProjectType, 
       ''''                                      w1projectTypeDesc, 
       ''''                                      w2projectTypeDesc, 
       ''''                                      w3projectTypeDesc, 
       ''''                                      w1Responsibility, 
       ''''                                      w2Responsibility, 
       ''''                                      w3Responsibility, 
       ''''                                      w1ResponsibilityDesc, 
       ''''                                      w2ResponsibilityDesc, 
       ''''                                      w3ResponsibilityDesc, 
       ''''                                      w1Referable, 
       ''''                                      w2Referable, 
       ''''                                      w3Referable, 
       NULL                                    w1EstCompletionDate, 
       NULL                                    w2EstCompletionDate, 
       NULL                                    w3EstCompletionDate, 
       NULL                                    w1ActCompletionDate, 
       NULL                                    w2ActCompletionDate, 
       NULL                                    w3ActCompletionDate, 
       NULL                                    w1ContractDate, 
       NULL                                    w2ContractDate, 
       NULL                                    w3ContractDate, 
       NULL                                    w1ConstComplDate, 
       NULL                                    w2ConstComplDate, 
       NULL                                    w3ConstComplDate, 
       NULL                                    w1ProfServicesComplDate, 
       NULL                                    w2ProfServicesComplDate, 
       NULL                                    w3ProfServicesComplDate, 
       NULL                                    w1BidDate, 
       NULL                                    w2BidDate, 
       NULL                                    w3BidDate, 
       ''''                                      w1ComplDateComment, 
       ''''                                      w2ComplDateComment, 
       ''''                                      w3ComplDateComment, 
       0                                       w1FirmCost, 
       0                                       w2FirmCost, 
       0                                       w3FirmCost, 
       ''''                                      w1FirmCostComment, 
       ''''                                      w2FirmCostComment, 
       ''''                                      w3FirmCostComment, 
       0                                       w1TotalProjectCost, 
       0                                       w2TotalProjectCost, 
       0                                       w3TotalProjectCost, 
       ''''                                      w1TotalCostComment, 
       ''''                                      w2TotalCostComment, 
       ''''                                      w3TotalCostComment, 
       ''''                                      ClientConfidential, 
       ''''                                      ClientAlias, 
       ''''                                      BillingClientID, 
       ''''                                      BillClientName, 
       ''''                                      BillingContactID, 
       ''''                                      billContactLastName, 
       ''''                                      billContactFirstName, 
       ''''                                      ProposalWBS1, 
       ''''                                      w1CreateUser, 
       ''''                                      w2CreateUser, 
       ''''                                      w3CreateUser, 
       NULL                                    w1CreateDate, 
       NULL                                    w2CreateDate, 
       NULL                                    w3CreateDate, 
       ''''                                      w1ModUser, 
       ''''                                      w2ModUser, 
       ''''                                      w3ModUser, 
       NULL                                    w1ModDate, 
       NULL                                    w2ModDate, 
       NULL                                    w3ModDate, 
       ''''                                      w1CostRateMeth, 
       ''''                                      w2CostRateMeth, 
       ''''                                      w3CostRateMeth, 
       ''''                                      w1CostRateTableNo, 
       ''''                                      w3CostRateTableNo, 
       ''''                                      w3CostRateTableNo, 
       ''''                                      w1PayRateMeth, 
       ''''                                      w2PayRateMeth, 
       ''''                                      w3PayRateMeth, 
       ''''                                      w1PayRateTableNo, 
       ''''                                      w2PayRateTableNo, 
       ''''                                      w3PayRateTableNo, 
       ''''                                      w1Locale, 
       ''''                                      w1Locale, 
       ''''                                      w1Locale, 
       ''''                                      w1LineItemApproval, 
       ''''                                      w2LineItemApproval, 
       ''''                                      w3LineItemApproval, 
       ''''                                      w1LineItemApprovalEK, 
       ''''                                      w2LineItemApprovalEK, 
       ''''                                      w3LineItemApprovalEK, 
       ''''                                      opp_name, 
       ''''                                      opp_number, 
       ''''                                      opp_prproposalwbs1, 
       ''''                                      opp_prproposalname, 
       ''''                                      opp_org, 
       ''''                                      opp_orgname, 
       ''''                                      opp_clname, 
       ''''                                      opp_contactname, 
       ''''                                      opp_projmgrname, 
       ''''                                      opp_principalname, 
       ''''                                      opp_supervisorname, 
       0                                       opp_revenue, 
       ''''                                      opp_probability, 
       0                                       opp_weightedrevenue, 
       NULL                                    opp_opendate, 
       NULL                                    opp_closedate, 
       ''''                                      opp_daysopen, 
       ''''                                      opp_stage, 
       ''''                                      opp_type, 
       ''''                                      opp_source, 
       ''''                                      opp_status, 
       NULL                                    opp_eststartdate, 
       NULL                                    opp_estcompletiondate, 
       ''''                                      opp_address1, 
       ''''                                      opp_address2, 
       ''''                                      opp_address3, 
       ''''                                      opp_city, 
       ''''                                      opp_state, 
       ''''                                      opp_zip, 
       ''''                                      opp_country, 
       ''''                                      opp_createuser, 
       NULL                                    opp_createdate, 
       ''''                                      opp_moduser, 
       NULL                                    opp_moddate, 
       ''''                                      opp_oppdesc 
FROM   pr AS LEVEL3 
       LEFT JOIN projectcustomtabfields 
              ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 
                 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 
                 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes1 
              ON Substring(LEVEL3.org, 1, 2) = groupCFGOrgCodes1.code 
                 AND groupCFGOrgCodes1.orglevel = 1 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes3 
              ON Substring(LEVEL3.org, 7, 3) = groupCFGOrgCodes3.code 
                 AND groupCFGOrgCodes3.orglevel = 3, 
       pr AS LEVEL2, 
       pr, 
       ##rwl_' + @SessionId + ', 
       prsummarysub AS LD 
WHERE  LD.wbs1 = LEVEL3.wbs1 
       AND LD.wbs2 = LEVEL3.wbs2 
       AND LD.wbs3 = LEVEL3.wbs3 
       AND LD.period <= @Period 
       AND ( LD.period >= @MinPeriod 
              OR pr.chargetype <> ''H'' ) 
       AND level3.sublevel = ''N'' 
       AND (( (( ( pr.wbs1 = ''NC1008.000'' ) 
					OR ( pr.wbs1 = ''NC1010.000'' ) 
					OR ( pr.wbs1 = ''NC1010.001'' )  
					OR ( pr.wbs1 = ''NC1010.002'' )  
					OR ( pr.wbs1 = ''NC1010.003'' )  
					OR ( pr.wbs1 = ''NC1011.001'' )  
					OR ( pr.wbs1 = ''NC3008.000'' ) 
					OR ( pr.wbs1 = ''NC3010.000'' ) 
					OR ( pr.wbs1 = ''NC3010.001'' )  
					OR ( pr.wbs1 = ''NC3010.002'' )  
					OR ( pr.wbs1 = ''NC3010.003'' )  
					OR ( pr.wbs1 = ''NC3011.001'' )  
				  )) 
               OR ( ( pr.wbs1 LIKE ''P%'' ) 
                    AND EXISTS (SELECT ''x'' 
                                FROM   cfgchargetype 
                                WHERE  cfgchargetype.type = pr.chargetype 
                                       AND (( cfgchargetype.label =  
                                              ''Promotional'' 
                                            ))) 
                  ) )) 
       AND LEVEL3.wbs1 = ##rwl_' + @SessionId + '.wbs1 
       AND ##rwl_' + @SessionId + '.wbs2 = '' '' 
       AND ##rwl_' + @SessionId + '.wbs3 = '' '' 
       AND ##rwl_' + @SessionId + '.sessionid = 
           ''' + @SessionId + ''' 
       AND pr.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs2 = LEVEL3.wbs2 
       AND pr.wbs2 = '''' 
       AND pr.wbs3 = '''' 
       AND LEVEL2.wbs3 = '''' 
GROUP  BY Isnull(Substring(LEVEL3.org, 1, 2), ''''), 
          Isnull(Substring(LEVEL3.org, 7, 3), ''''), 
          Isnull(pr.wbs1, '''') 
UNION ALL 
SELECT Isnull(Substring(LEVEL3.org, 1, 2), '''') AS group1, 
       Min(groupCFGOrgCodes1.label)            AS groupDesc1, 
       Isnull(Substring(LEVEL3.org, 7, 3), '''') AS group2, 
       Min(groupCFGOrgCodes3.label)            AS groupDesc2, 
       Isnull(pr.wbs1, '''')                     AS group3, 
       Min(pr.NAME)                            AS groupDesc3, 
       myRecord.rectype                        AS recordType, 
       mySubType.subtype                       AS subType, 
       0                                       AS hrsCur, 
       0                                       hrsCurLabReg, 
       0                                       hrsCurLabOvt, 
       0                                       AS hrsYTD, 
       0                                       hrsYTDLabReg, 
       0                                       hrsYTDLabOvt, 
       0                                       AS hrsJTD, 
       0                                       hrsJTDLabReg, 
       0                                       hrsJTDLabOvt, 
       0                                       AS hrsCustom, 
       0                                       hrsCustomLabReg, 
       0                                       hrsCustomLabOvt, 
       0                                       AS hrsBud, 
       0                                       AS hrsPctComp_c, 
       0                                       AS hrsPctComp_b, 
       0                                       AS hrsETC, 
       0                                       AS hrsEAC, 
       0                                       amtCurLabReg_c, 
       0                                       amtCurLabReg_b, 
       0                                       amtCurLabOvt_c, 
       0                                       amtCurLabOvt_b, 
       0                                       amtYTDLabReg_c, 
       0                                       amtYTDLabReg_b, 
       0                                       amtYTDLabOvt_c, 
       0                                       amtYTDLabOvt_b, 
       0                                       amtJTDLabReg_c, 
       0                                       amtJTDLabReg_b, 
       0                                       amtJTDLabOvt_c, 
       0                                       amtJTDLabOvt_b, 
       0                                       amtCustomLabReg_c, 
       0                                       amtCustomLabReg_b, 
       0                                       amtCustomLabOvt_c, 
       0                                       amtCustomLabOvt_b, 
       Sum(CASE 
             WHEN period = @Period THEN 
               CASE 
                 WHEN subtype = 0 THEN 
                   CASE 
                     WHEN myRecord.rectype = ''5'' THEN reimbconscost 
                     WHEN myRecord.rectype = ''3'' THEN directconscost 
                     WHEN myRecord.rectype = ''6'' THEN indirectcost 
                     ELSE 0 
                   END 
                 ELSE 
                   CASE 
                     WHEN myRecord.rectype = ''5'' THEN reimbothercost 
                     WHEN myRecord.rectype = ''3'' THEN directothercost 
                     ELSE 0 
                   END 
               END 
             ELSE 0 
           END)                                amtCur_c, 
       Sum(CASE 
             WHEN period = @Period THEN 
               CASE 
                 WHEN subtype = 0 THEN 
                   CASE 
                     WHEN myRecord.rectype = ''5'' THEN reimbconsbilling 
                     WHEN myRecord.rectype = ''3'' THEN directconsbilling 
                     WHEN myRecord.rectype = ''6'' THEN indirectbilling 
                     ELSE 0 
                   END 
                 ELSE 
                   CASE 
                     WHEN myRecord.rectype = ''5'' THEN reimbotherbilling 
                     WHEN myRecord.rectype = ''3'' THEN directotherbilling 
                     ELSE 0 
                   END 
               END 
             ELSE 0 
           END)                                amtCur_b, 
       Sum(CASE 
             WHEN period >= @MinPeriod THEN 
               CASE 
                 WHEN subtype = 0 THEN 
                   CASE 
                     WHEN myRecord.rectype = ''5'' THEN reimbconscost 
                     WHEN myRecord.rectype = ''3'' THEN directconscost 
                     WHEN myRecord.rectype = ''6'' THEN indirectcost 
                     ELSE 0 
                   END 
                 ELSE 
                   CASE 
                     WHEN myRecord.rectype = ''5'' THEN reimbothercost 
                     WHEN myRecord.rectype = ''3'' THEN directothercost 
                     ELSE 0 
                   END 
               END 
             ELSE 0 
           END)                                amtYTD_c, 
       Sum(CASE 
             WHEN period >= @MinPeriod THEN 
               CASE 
                 WHEN subtype = 0 THEN 
                   CASE 
                     WHEN myRecord.rectype = ''5'' THEN reimbconsbilling 
                     WHEN myRecord.rectype = ''3'' THEN directconsbilling 
                     WHEN myRecord.rectype = ''6'' THEN indirectbilling 
                     ELSE 0 
                   END 
                 ELSE 
                   CASE 
                     WHEN myRecord.rectype = ''5'' THEN reimbotherbilling 
                     WHEN myRecord.rectype = ''3'' THEN directotherbilling 
                     ELSE 0 
                   END 
               END 
             ELSE 0 
           END)                                amtYTD_b, 
       Sum(CASE 
             WHEN subtype = 0 THEN 
               CASE 
                 WHEN myRecord.rectype = ''5'' THEN reimbconscost 
                 WHEN myRecord.rectype = ''3'' THEN directconscost 
                 WHEN myRecord.rectype = ''6'' THEN indirectcost 
                 ELSE 0 
               END 
             ELSE 
               CASE 
                 WHEN myRecord.rectype = ''5'' THEN reimbothercost 
                 WHEN myRecord.rectype = ''3'' THEN directothercost 
                 ELSE 0 
               END 
           END)                                amtJTD_c, 
       Sum(CASE 
             WHEN subtype = 0 THEN 
               CASE 
                 WHEN myRecord.rectype = ''5'' THEN reimbconsbilling 
                 WHEN myRecord.rectype = ''3'' THEN directconsbilling 
                 WHEN myRecord.rectype = ''6'' THEN indirectbilling 
                 ELSE 0 
               END 
             ELSE 
               CASE 
                 WHEN myRecord.rectype = ''5'' THEN reimbotherbilling 
                 WHEN myRecord.rectype = ''3'' THEN directotherbilling 
                 ELSE 0 
               END 
           END)                                amtJTD_b, 
       Sum(CASE 
             WHEN period >= 201801 
                  AND period <= 201801 THEN 
               CASE 
                 WHEN subtype = 0 THEN 
                   CASE 
                     WHEN myRecord.rectype = ''5'' THEN reimbconscost 
                     WHEN myRecord.rectype = ''3'' THEN directconscost 
                     WHEN myRecord.rectype = ''6'' THEN indirectcost 
                     ELSE 0 
                   END 
                 ELSE 
                   CASE 
                     WHEN myRecord.rectype = ''5'' THEN reimbothercost 
                     WHEN myRecord.rectype = ''3'' THEN directothercost 
                     ELSE 0 
                   END 
               END 
             ELSE 0 
           END)                                amtCustom_c, 
       Sum(CASE 
             WHEN period >= 201801 
                  AND period <= 201801 THEN 
               CASE 
                 WHEN subtype = 0 THEN 
                   CASE 
                     WHEN myRecord.rectype = ''5'' THEN reimbconsbilling 
                     WHEN myRecord.rectype = ''3'' THEN directconsbilling 
                     WHEN myRecord.rectype = ''6'' THEN indirectbilling 
                     ELSE 0 
                   END 
                 ELSE 
                   CASE 
                     WHEN myRecord.rectype = ''5'' THEN reimbotherbilling 
                     WHEN myRecord.rectype = ''3'' THEN directotherbilling 
                     ELSE 0 
                   END 
               END 
             ELSE 0 
           END)                                amtCustom_b, 
       0                                       AS amtBud_c, 
       0                                       amtBud_b, 
       0                                       AS amtEAC_c, 
       0                                       amtEAC_b, 
       0                                       AS amtETC_c, 
       0                                       amtETC_b, 
       0                                       AS amtPctComp_c, 
       0                                       amtPctComp_b, 
       0                                       AS amtPctCompLab_c, 
       0                                       amtPctCompLab_b, 
       0                                       AS amtPctCompExp_c, 
       0                                       amtPctCompExp_b, 
       0                                       AS arCur_c, 
       0                                       AS arCur_b, 
       0                                       AS arYTD_c, 
       0                                       AS arYTD_b, 
       0                                       AS arJTD_c, 
       0                                       AS arJTD_b, 
       0                                       AS arCustom_c, 
       0                                       AS arCustom_b, 
       0                                       AS billCur_c, 
       0                                       AS billCur_b, 
       0                                       AS billYTD_c, 
       0                                       AS billYTD_b, 
       0                                       AS billJTD_c, 
       0                                       AS billJTD_b, 
       0                                       AS billCustom_c, 
       0                                       AS billCustom_b, 
       0                                       AS recdCur_c, 
       0                                       AS recdCur_b, 
       0                                       AS recdYTD_c, 
       0                                       AS recdYTD_b, 
       0                                       AS recdJTD_c, 
       0                                       AS recdJTD_b, 
       0                                       AS recdCustom_c, 
       0                                       AS recdCustom_b, 
       Sum(CASE 
             WHEN myRecord.rectype = ''3'' 
                  AND subtype = 0 
                  AND period = @Period THEN directconscost + directothercost 
             ELSE 0 
           END)                                directCur_c, 
       Sum(CASE 
             WHEN myRecord.rectype = ''3'' 
                  AND subtype = 0 
                  AND period = @Period THEN 
             directconsbilling + directotherbilling 
             ELSE 0 
           END)                                directCur_b, 
       Sum(CASE 
             WHEN myRecord.rectype = ''3'' 
                  AND subtype = 0 
                  AND period >= @MinPeriod THEN directconscost + directothercost 
             ELSE 0 
           END)                                directYTD_c, 
       Sum(CASE 
             WHEN myRecord.rectype = ''3'' 
                  AND subtype = 0 
                  AND period >= @MinPeriod THEN 
             directconsbilling + directotherbilling 
             ELSE 0 
           END)                                directYTD_b, 
       Sum(CASE 
             WHEN myRecord.rectype = ''3'' 
                  AND subtype = 0 THEN directconscost + directothercost 
             ELSE 0 
           END)                                directJTD_c, 
       Sum(CASE 
             WHEN myRecord.rectype = ''3'' 
                  AND subtype = 0 THEN directconsbilling + directotherbilling 
             ELSE 0 
           END)                                directJTD_b, 
       Sum(CASE 
             WHEN myRecord.rectype = ''3'' 
                  AND subtype = 0 
                  AND period >= 201801 
                  AND period <= 201801 THEN directconscost + directothercost 
             ELSE 0 
           END)                                directCustom_c, 
       Sum(CASE 
             WHEN myRecord.rectype = ''3'' 
                  AND subtype = 0 
                  AND period >= 201801 
                  AND period <= 201801 THEN 
             directconsbilling + directotherbilling 
             ELSE 0 
           END)                                directCustom_b, 
       Sum(CASE 
             WHEN myRecord.rectype = ''5'' 
                  AND subtype = 0 
                  AND period = @Period THEN reimbconscost + reimbothercost 
             ELSE 0 
           END)                                reimbCur_c, 
       Sum(CASE 
             WHEN myRecord.rectype = ''5'' 
                  AND subtype = 0 
                  AND period = @Period THEN reimbconsbilling + reimbotherbilling 
             ELSE 0 
           END)                                reimbCur_b, 
       Sum(CASE 
             WHEN myRecord.rectype = ''5'' 
                  AND subtype = 0 
                  AND period >= @MinPeriod THEN reimbconscost + reimbothercost 
             ELSE 0 
           END)                                reimbYTD_c, 
       Sum(CASE 
             WHEN myRecord.rectype = ''5'' 
                  AND subtype = 0 
                  AND period >= @MinPeriod THEN reimbconsbilling + reimbotherbilling 
             ELSE 0 
           END)                                reimbYTD_b, 
       Sum(CASE 
             WHEN myRecord.rectype = ''5'' 
                  AND subtype = 0 
                  AND period >= 201801 
                  AND period <= 201801 THEN reimbconscost + reimbothercost 
             ELSE 0 
           END)                                reimbCustom_c, 
       Sum(CASE 
             WHEN myRecord.rectype = ''5'' 
                  AND subtype = 0 
                  AND period >= 201801 
                  AND period <= 201801 THEN reimbconsbilling + reimbotherbilling 
             ELSE 0 
           END)                                reimbCustom_b, 
       Sum(CASE 
             WHEN myRecord.rectype = ''5'' 
                  AND subtype = 0 THEN reimbconscost + reimbothercost 
             ELSE 0 
           END)                                reimbJTD_c, 
       Sum(CASE 
             WHEN myRecord.rectype = ''5'' 
                  AND subtype = 0 THEN reimbconsbilling + reimbotherbilling 
             ELSE 0 
           END)                                reimbJTD_b, 
       0                                       AS revCur_c, 
       0                                       AS revCur_b, 
       0                                       AS revYTD_c, 
       0                                       AS revYTD_b, 
       0                                       AS revJTD_c, 
       0                                       AS revJTD_b, 
       0                                       AS revCustom_c, 
       0                                       AS revCustom_b, 
       0.00                                    AS ubGLCur_c, 
       0.00                                    AS ubGLYTD_c, 
       0.00                                    AS ubGLJTD_c, 
       0.00                                    AS ubGLCustom_c, 
       0                                       althrsBud, 
       0                                       altamtBud_c, 
       0                                       altamtBud_b, 
       0.0                                     poAmt_c, 
       0.00                                    poAmt_b, 
       0.0                                     rppctcmpcalc, 
       0.00                                    targetMult_c, 
       0.00                                    targetMult_b, 
       0.00                                    rpfee_c, 
       0.00                                    rpfee_b, 
       0.00                                    rpconsultfee_c, 
       0.00                                    rpconsultfee_b, 
       0.00                                    rpreimballow_c, 
       0.00                                    rpreimballow_b, 
       ''''                                      AS currencyCodeProj, 
       ''''                                      AS currencyCodeBill, 
       ''''                                      AS currencyCodeFunct, 
       ''''                                      AS currencyCodePres_Cost, 
       ''''                                      AS currencyCodePres_Bill, 
       ''''                                      AS currencyCodePres_ARFee, 
       ''''                                      AS currencyCode_Opportunity, 
       0                                       AS currencyCodeProjCount, 
       0                                       AS currencyCodeBillCount, 
       0                                       AS currencyCodeFunctCount, 
       0                                       AS currencyCodePresCount_Cost, 
       0                                       AS currencyCodePresCount_Bill, 
       0                                       AS currencyCodePresCount_ARFee, 
       0                                       AS currencyCodeCount_Opportunity, 
       Min(pr.wbs1)                            WBS1, 
       Min(LEVEL2.wbs2)                        WBS2, 
       Min(LEVEL3.wbs3)                        WBS3, 
       ''''                                      w1Name, 
       ''''                                      w2Name, 
       ''''                                      w3Name, 
       ''''                                      ChargeType, 
       Max(pr.chargetype)                      ChargeTypeCode, 
       Min(pr.sublevel)                        SubLevel1, 
       Min(LEVEL2.sublevel)                    SubLevel2, 
       Min(LEVEL3.sublevel)                    SubLevel3, 
       ''''                                      w1principal, 
       ''''                                      w2principal, 
       ''''                                      w3principal, 
       ''''                                      w1prinName, 
       ''''                                      w2prinName, 
       ''''                                      w3prinName, 
       ''''                                      w1projMgr, 
       ''''                                      w2projMgr, 
       ''''                                      w3projMgr, 
       ''''                                      w1prgName, 
       ''''                                      w2prgName, 
       ''''                                      w3prgName, 
       ''''                                      w1supervisor, 
       ''''                                      w2supervisor, 
       ''''                                      w3supervisor, 
       ''''                                      w1supName, 
       ''''                                      w2supName, 
       ''''                                      w3supName, 
       ''''                                      PRAddress1, 
       ''''                                      PRAddress2, 
       ''''                                      PRAddress3, 
       ''''                                      PRCity, 
       ''''                                      PRState, 
       ''''                                      PRZip, 
       ''''                                      PRCounty, 
       ''''                                      PRCountry, 
       ''''                                      PRPhone, 
       ''''                                      PRFax, 
       ''''                                      PREmail, 
       ''''                                      ClientID, 
       ''''                                      ClientName, 
       ''''                                      CLAddress, 
       ''''                                      CLAddress1, 
       ''''                                      CLAddress2, 
       ''''                                      CLAddress3, 
       ''''                                      CLAddress4, 
       ''''                                      CLCity, 
       ''''                                      CLState, 
       ''''                                      CLZip, 
       ''''                                      CLCountry, 
       ''''                                      CLPhone, 
       ''''                                      CLFax, 
       ''''                                      CLEMail, 
       ''''                                      w1Org, 
       ''''                                      w2Org, 
       ''''                                      w3Org, 
       ''''                                      w1OrgName, 
       ''''                                      w2OrgName, 
       ''''                                      w3OrgName, 
       ''''                                      AS w1TLProjectName, 
       ''''                                      AS w2TLProjectName, 
       ''''                                      AS w3TLProjectName, 
       ''''                                      ServiceProfileCode, 
       ''''                                      ServiceProfileDesc, 
       0                                       mult, 
       0                                       mult_sum, 
       0                                       fee_c, 
       0                                       fee_b, 
       0                                       consultFee_c, 
       0                                       consultFee_b, 
       0                                       reimbAllow_c, 
       0                                       reimbAllow_b, 
       0.0                                     feeDirectLabor, 
       0.0                                     feeDirectExpense, 
       0.0                                     reimbAllowExpense, 
       0.0                                     reimbAllowConsultant, 
       0                                       contractfee, 
       0                                       contractconsultFee, 
       0                                       contractreimbAllow, 
       0.0                                     contractFeeDirectLabor, 
       0.0                                     contractFeeDirectExpense, 
       0.0                                     contractReimbAllowExpense, 
       0.0                                     contractReimbAllowConsultant, 
       0                                       BudOHRate, 
       ''''                                      w1Status, 
       ''''                                      w2Status, 
       ''''                                      w3Status, 
       ''''                                      w1StatusDesc, 
       ''''                                      w2StatusDesc, 
       ''''                                      w3StatusDesc, 
       ''''                                      wbs1RevType, 
       ''''                                      wbs2RevType, 
       ''''                                      wbs3RevType, 
       ''''                                      wbs1RevTypeDesc, 
       ''''                                      wbs2RevTypeDesc, 
       ''''                                      wbs3RevTypeDesc, 
       ''''                                      UnitTable, 
       NULL                                    w1StartDate, 
       NULL                                    w2StartDate, 
       NULL                                    w3StartDate, 
       NULL                                    w1EndDate, 
       NULL                                    w2EndDate, 
       NULL                                    w3EndDate, 
       0.00                                    w1PctComp, 
       0.00                                    w2PctComp, 
       0.00                                    w3PctComp, 
       0.00                                    w1LabPctComp, 
       0.00                                    w2LabPctComp, 
       0.00                                    w3LabPctComp, 
       0.00                                    w1ExpPctComp, 
       0.00                                    w2ExpPctComp, 
       0.00                                    w3ExpPctComp, 
       ''''                                      BillByDefault, 
       ''''                                      BillableWarning, 
       ''''                                      BudgetedFlag, 
       ''''                                      BudgetSource, 
       ''''                                      BudgetLevel, 
       ''''                                      BudgetedLevels, 
       ''''                                      XCharge, 
       ''''                                      XChargeMethod, 
       0                                       XChargeMult, 
       ''''                                      ContactID, 
       ''''                                      ContactFirstName, 
       ''''                                      ContactLastName, 
       ''''                                      CLBillingAddr, 
       ''''                                      w1LongName, 
       ''''                                      w2LongName, 
       ''''                                      w3LongName, 
       ''''                                      CLBAddress1, 
       ''''                                      CLBAddress2, 
       ''''                                      CLBAddress3, 
       ''''                                      CLBAddress4, 
       ''''                                      CLBCity, 
       ''''                                      CLBState, 
       ''''                                      CLBZip, 
       ''''                                      CLBCountry, 
       ''''                                      CLBPhone, 
       ''''                                      CLBFax, 
       ''''                                      CLBEMail, 
       ''''                                      w1FederalInd, 
       ''''                                      w2FederalInd, 
       ''''                                      w3FederalInd, 
       ''''                                      w1ProjectType, 
       ''''                                      w2ProjectType, 
       ''''                                      w3ProjectType, 
       ''''                                      w1projectTypeDesc, 
       ''''                                      w2projectTypeDesc, 
       ''''                                      w3projectTypeDesc, 
       ''''                                      w1Responsibility, 
       ''''                                      w2Responsibility, 
       ''''                                      w3Responsibility, 
       ''''                                      w1ResponsibilityDesc, 
       ''''                                      w2ResponsibilityDesc, 
       ''''                                      w3ResponsibilityDesc, 
       ''''                                      w1Referable, 
       ''''                                      w2Referable, 
       ''''                                      w3Referable, 
       NULL                                    w1EstCompletionDate, 
       NULL                                    w2EstCompletionDate, 
       NULL                                    w3EstCompletionDate, 
       NULL                                    w1ActCompletionDate, 
       NULL                                    w2ActCompletionDate, 
       NULL                                    w3ActCompletionDate, 
       NULL                                    w1ContractDate, 
       NULL                                    w2ContractDate, 
       NULL                                    w3ContractDate, 
       NULL                                    w1ConstComplDate, 
       NULL                                    w2ConstComplDate, 
       NULL                                    w3ConstComplDate, 
       NULL                                    w1ProfServicesComplDate, 
       NULL                                    w2ProfServicesComplDate, 
       NULL                                    w3ProfServicesComplDate, 
       NULL                                    w1BidDate, 
       NULL                                    w2BidDate, 
       NULL                                    w3BidDate, 
       ''''                                      w1ComplDateComment, 
       ''''                                      w2ComplDateComment, 
       ''''                                      w3ComplDateComment, 
       0                                       w1FirmCost, 
       0                                       w2FirmCost, 
       0                                       w3FirmCost, 
       ''''                                      w1FirmCostComment, 
       ''''                                      w2FirmCostComment, 
       ''''                                      w3FirmCostComment, 
       0                                       w1TotalProjectCost, 
       0                                       w2TotalProjectCost, 
       0                                       w3TotalProjectCost, 
       ''''                                      w1TotalCostComment, 
       ''''                                      w2TotalCostComment, 
       ''''                                      w3TotalCostComment, 
       ''''                                      ClientConfidential, 
       ''''                                      ClientAlias, 
       ''''                                      BillingClientID, 
       ''''                                      BillClientName, 
       ''''                                      BillingContactID, 
       ''''                                      billContactLastName, 
       ''''                                      billContactFirstName, 
       ''''                                      ProposalWBS1, 
       ''''                                      w1CreateUser, 
       ''''                                      w2CreateUser, 
       ''''                                      w3CreateUser, 
       NULL                                    w1CreateDate, 
       NULL                                    w2CreateDate, 
       NULL                                    w3CreateDate, 
       ''''                                      w1ModUser, 
       ''''                                      w2ModUser, 
       ''''                                      w3ModUser, 
       NULL                                    w1ModDate, 
       NULL                                    w2ModDate, 
       NULL                                    w3ModDate, 
       ''''                                      w1CostRateMeth, 
       ''''                                      w2CostRateMeth, 
       ''''                                      w3CostRateMeth, 
       ''''                                      w1CostRateTableNo, 
       ''''                                      w3CostRateTableNo, 
       ''''                                      w3CostRateTableNo, 
       ''''                                      w1PayRateMeth, 
       ''''                                      w2PayRateMeth, 
       ''''                                      w3PayRateMeth, 
       ''''                                      w1PayRateTableNo, 
       ''''                                      w2PayRateTableNo, 
       ''''                                      w3PayRateTableNo, 
       ''''                                      w1Locale, 
       ''''                                      w1Locale, 
       ''''                                      w1Locale, 
       ''''                                      w1LineItemApproval, 
       ''''                                      w2LineItemApproval, 
       ''''                                      w3LineItemApproval, 
       ''''                                      w1LineItemApprovalEK, 
       ''''                                      w2LineItemApprovalEK, 
       ''''                                      w3LineItemApprovalEK, 
       ''''                                      opp_name, 
       ''''                                      opp_number, 
       ''''                                      opp_prproposalwbs1, 
       ''''                                      opp_prproposalname, 
       ''''                                      opp_org, 
       ''''                                      opp_orgname, 
       ''''                                      opp_clname, 
       ''''                                      opp_contactname, 
       ''''                                      opp_projmgrname, 
       ''''                                      opp_principalname, 
       ''''                                      opp_supervisorname, 
       0                                       opp_revenue, 
       ''''                                      opp_probability, 
       0                                       opp_weightedrevenue, 
       NULL                                    opp_opendate, 
       NULL                                    opp_closedate, 
       ''''                                      opp_daysopen, 
       ''''                                      opp_stage, 
       ''''                                      opp_type, 
       ''''                                      opp_source, 
       ''''                                      opp_status, 
       NULL                                    opp_eststartdate, 
       NULL                                    opp_estcompletiondate, 
       ''''                                      opp_address1, 
       ''''                                      opp_address2, 
       ''''                                      opp_address3, 
       ''''                                      opp_city, 
       ''''                                      opp_state, 
       ''''                                      opp_zip, 
       ''''                                      opp_country, 
       ''''                                      opp_createuser, 
       NULL                                    opp_createdate, 
       ''''                                      opp_moduser, 
       NULL                                    opp_moddate, 
       ''''                                      opp_oppdesc 
FROM   pr AS LEVEL3 
       LEFT JOIN projectcustomtabfields 
              ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 
                 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 
                 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes1 
              ON Substring(LEVEL3.org, 1, 2) = groupCFGOrgCodes1.code 
                 AND groupCFGOrgCodes1.orglevel = 1 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes3 
              ON Substring(LEVEL3.org, 7, 3) = groupCFGOrgCodes3.code 
                 AND groupCFGOrgCodes3.orglevel = 3, 
       pr AS LEVEL2, 
       pr, 
       ##rwl_' + @SessionId + ', 
       prsummarysub, 
       (SELECT ''3'' AS recType 
        UNION 
        SELECT ''5'' 
        UNION 
        SELECT ''6'' 
        UNION 
        SELECT ''7'') myRecord, 
       (SELECT ''0'' AS subType 
        UNION 
        SELECT ''1'') mySubType 
WHERE  ( ( prsummarysub.wbs1 = LEVEL3.wbs1 
           AND prsummarysub.wbs2 = LEVEL3.wbs2 
           AND prsummarysub.wbs3 = LEVEL3.wbs3 ) 
         AND ( prsummarysub.wbs1 = LEVEL2.wbs1 
               AND prsummarysub.wbs2 = LEVEL2.wbs2 ) 
         AND ( prsummarysub.wbs1 = pr.wbs1 ) 
         AND ( pr.wbs2 = '' '' 
               AND pr.wbs3 = '' '' 
               AND LEVEL2.wbs3 = '' '' ) ) 
       AND period <= @Period 
       AND ( period >= @MinPeriod 
              OR pr.chargetype <> ''H'' ) 
       AND level3.sublevel = ''N'' 
       AND (( (( ( pr.wbs1 = ''NC1008.000'' ) 
					OR ( pr.wbs1 = ''NC1010.000'' ) 
					OR ( pr.wbs1 = ''NC1010.001'' )  
					OR ( pr.wbs1 = ''NC1010.002'' )  
					OR ( pr.wbs1 = ''NC1010.003'' )  
					OR ( pr.wbs1 = ''NC1011.001'' )  
					OR ( pr.wbs1 = ''NC3008.000'' ) 
					OR ( pr.wbs1 = ''NC3010.000'' ) 
					OR ( pr.wbs1 = ''NC3010.001'' )  
					OR ( pr.wbs1 = ''NC3010.002'' )  
					OR ( pr.wbs1 = ''NC3010.003'' )  
					OR ( pr.wbs1 = ''NC3011.001'' )  
				  )) 
               OR ( ( pr.wbs1 LIKE ''P%'' ) 
                    AND EXISTS (SELECT ''x'' 
                                FROM   cfgchargetype 
                                WHERE  cfgchargetype.type = pr.chargetype 
                                       AND (( cfgchargetype.label =  
                                              ''Promotional'' 
                                            ))) 
                  ) )) 
       AND LEVEL3.wbs1 = ##rwl_' + @SessionId + '.wbs1 
       AND ##rwl_' + @SessionId + '.wbs2 = '' '' 
       AND ##rwl_' + @SessionId + '.wbs3 = '' '' 
       AND ##rwl_' + @SessionId + '.sessionid = 
           ''' + @SessionId + ''' 
       AND pr.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs2 = LEVEL3.wbs2 
       AND pr.wbs2 = '''' 
       AND pr.wbs3 = '''' 
       AND LEVEL2.wbs3 = '''' 
GROUP  BY Isnull(Substring(LEVEL3.org, 1, 2), ''''), 
          Isnull(Substring(LEVEL3.org, 7, 3), ''''), 
          Isnull(pr.wbs1, ''''), 
          myRecord.rectype, 
          mySubType.subtype 
/***LedgerAR: Billed and Received Columns***/ 
UNION ALL 
SELECT Isnull(Substring(LEVEL3.org, 1, 2), '''') AS group1, 
       Min(groupCFGOrgCodes1.label)            AS groupDesc1, 
       Isnull(Substring(LEVEL3.org, 7, 3), '''') AS group2, 
       Min(groupCFGOrgCodes3.label)            AS groupDesc2, 
       Isnull(pr.wbs1, '''')                     AS group3, 
       Min(pr.NAME)                            AS groupDesc3, 
       ''9''                                     AS recordType, 
       ''''                                      subType, 
       0                                       AS hrsCur, 
       0                                       hrsCurLabReg, 
       0                                       hrsCurLabOvt, 
       0                                       AS hrsYTD, 
       0                                       hrsYTDLabReg, 
       0                                       hrsYTDLabOvt, 
       0                                       AS hrsJTD, 
       0                                       hrsJTDLabReg, 
       0                                       hrsJTDLabOvt, 
       0                                       AS hrsCustom, 
       0                                       hrsCustomLabReg, 
       0                                       hrsCustomLabOvt, 
       0                                       AS hrsBud, 
       0                                       AS hrsPctComp_c, 
       0                                       AS hrsPctComp_b, 
       0                                       AS hrsETC, 
       0                                       AS hrsEAC, 
       0                                       amtCurLabReg_c, 
       0                                       amtCurLabReg_b, 
       0                                       amtCurLabOvt_c, 
       0                                       amtCurLabOvt_b, 
       0                                       amtYTDLabReg_c, 
       0                                       amtYTDLabReg_b, 
       0                                       amtYTDLabOvt_c, 
       0                                       amtYTDLabOvt_b, 
       0                                       amtJTDLabReg_c, 
       0                                       amtJTDLabReg_b, 
       0                                       amtJTDLabOvt_c, 
       0                                       amtJTDLabOvt_b, 
       0                                       amtCustomLabReg_c, 
       0                                       amtCustomLabReg_b, 
       0                                       amtCustomLabOvt_c, 
       0                                       amtCustomLabOvt_b, 
       0                                       AS amtCur_c, 
       0                                       AS amtCur_b, 
       0                                       AS amtYTD_c, 
       0                                       amtYTD_b, 
       0                                       AS amtJTD_c, 
       0                                       amtJTD_b, 
       0                                       AS amtCustom_c, 
       0                                       amtCustom_b, 
       0                                       AS amtBud_c, 
       0                                       amtBud_b, 
       0                                       AS amtEAC_c, 
       0                                       amtEAC_b, 
       0                                       AS amtETC_c, 
       0                                       amtETC_b, 
       0                                       AS amtPctComp_c, 
       0                                       amtPctComp_b, 
       0                                       AS amtPctCompLab_c, 
       0                                       amtPctCompLab_b, 
       0                                       AS amtPctCompExp_c, 
       0                                       amtPctCompExp_b, 
       0                                       AS arCur_c, 
       0                                       AS arCur_b, 
       0                                       AS arYTD_c, 
       0                                       AS arYTD_b, 
       0                                       AS arJTD_c, 
       0                                       AS arJTD_b, 
       0                                       AS arCustom_c, 
       0                                       AS arCustom_b, 
       Sum(CASE 
             WHEN period = @Period 
                  AND myRecord.rectype = ''9'' THEN billed 
             ELSE 0 
           END)                                billCur_c, 
       Sum(CASE 
             WHEN period = @Period 
                  AND myRecord.rectype = ''9'' THEN billed 
             ELSE 0 
           END)                                billCur_b, 
       Sum(CASE 
             WHEN period >= @MinPeriod 
                  AND myRecord.rectype = ''9'' THEN billed 
             ELSE 0 
           END)                                billYTD_c, 
       Sum(CASE 
             WHEN period >= @MinPeriod 
                  AND myRecord.rectype = ''9'' THEN billed 
             ELSE 0 
           END)                                billYTD_b, 
       Sum(CASE 
             WHEN myRecord.rectype = ''9'' THEN billed 
             ELSE 0 
           END)                                billJTD_c, 
       Sum(CASE 
             WHEN myRecord.rectype = ''9'' THEN billed 
             ELSE 0 
           END)                                billJTD_b, 
       Sum(CASE 
             WHEN period >= 201801 
                  AND period <= 201801 
                  AND myRecord.rectype = ''9'' THEN billed 
             ELSE 0 
           END)                                billCustom_c, 
       Sum(CASE 
             WHEN period >= 201801 
                  AND period <= 201801 
                  AND myRecord.rectype = ''9'' THEN billed 
             ELSE 0 
           END)                                billCustom_b, 
       Sum(CASE 
             WHEN period = @Period 
                  AND myRecord.rectype = ''9'' THEN received 
             ELSE 0 
           END)                                recdCur_c, 
       Sum(CASE 
             WHEN period = @Period 
                  AND myRecord.rectype = ''9'' THEN received 
             ELSE 0 
           END)                                recdCur_b, 
       Sum(CASE 
             WHEN period >= @MinPeriod 
                  AND myRecord.rectype = ''9'' THEN received 
             ELSE 0 
           END)                                recdYTD_c, 
       Sum(CASE 
             WHEN period >= @MinPeriod 
                  AND myRecord.rectype = ''9'' THEN received 
             ELSE 0 
           END)                                recdYTD_b, 
       Sum(CASE 
             WHEN myRecord.rectype = ''9'' THEN received 
             ELSE 0 
           END)                                recdJTD_c, 
       Sum(CASE 
             WHEN myRecord.rectype = ''9'' THEN received 
             ELSE 0 
           END)                                recdJTD_b, 
       Sum(CASE 
             WHEN period >= 201801 
                  AND period <= 201801 
                  AND myRecord.rectype = ''9'' THEN received 
             ELSE 0 
           END)                                recdCustom_c, 
       Sum(CASE 
             WHEN period >= 201801 
                  AND period <= 201801 
                  AND myRecord.rectype = ''9'' THEN received 
             ELSE 0 
           END)                                recdCustom_b, 
       0                                       AS directCur_c, 
       0                                       directCur_b, 
       0                                       AS directYTD_c, 
       0                                       directYTD_b, 
       0                                       AS directJTD_c, 
       0                                       directJTD_b, 
       0                                       AS directCustom_c, 
       0                                       directCustom_b, 
       0                                       AS reimbCur_c, 
       0                                       reimbCur_b, 
       0                                       AS reimbYTD_c, 
       0                                       reimbYTD_b, 
       0                                       AS reimbJTD_c, 
       0                                       reimbJTD_b, 
       0                                       AS reimbCustom_c, 
       0                                       reimbCustom_b, 
       0                                       AS revCur_c, 
       0                                       AS revCur_b, 
       0                                       AS revYTD_c, 
       0                                       AS revYTD_b, 
       0                                       AS revJTD_c, 
       0                                       AS revJTD_b, 
       0                                       AS revCustom_c, 
       0                                       AS revCustom_b, 
       Sum(CASE 
             WHEN myRecord.rectype = ''9'' 
                  AND period = @Period THEN prsummarysub.unbillgainloss 
             ELSE 0 
           END)                                ubGLCur_c, 
       Sum(CASE 
             WHEN myRecord.rectype = ''9'' 
                  AND period >= @MinPeriod THEN prsummarysub.unbillgainloss 
             ELSE 0 
           END)                                ubGLYTD_c, 
       Sum(CASE 
             WHEN myRecord.rectype = ''9'' THEN prsummarysub.unbillgainloss 
             ELSE 0 
           END)                                ubGLJTD_c, 
       Sum(CASE 
             WHEN myRecord.rectype = ''9'' 
                  AND period >= 201801 
                  AND period <= 201801 THEN prsummarysub.unbillgainloss 
             ELSE 0 
           END)                                ubGLCustom_c, 
       0                                       althrsBud, 
       0                                       altamtBud_c, 
       0                                       altamtBud_b, 
       0.0                                     poAmt_c, 
       0.00                                    poAmt_b, 
       0.0                                     rppctcmpcalc, 
       0.00                                    targetMult_c, 
       0.00                                    targetMult_b, 
       0.00                                    rpfee_c, 
       0.00                                    rpfee_b, 
       0.00                                    rpconsultfee_c, 
       0.00                                    rpconsultfee_b, 
       0.00                                    rpreimballow_c, 
       0.00                                    rpreimballow_b, 
       ''''                                      AS currencyCodeProj, 
       ''''                                      AS currencyCodeBill, 
       ''''                                      AS currencyCodeFunct, 
       ''''                                      AS currencyCodePres_Cost, 
       ''''                                      AS currencyCodePres_Bill, 
       ''''                                      AS currencyCodePres_ARFee, 
       ''''                                      AS currencyCode_Opportunity, 
       0                                       AS currencyCodeProjCount, 
       0                                       AS currencyCodeBillCount, 
       0                                       AS currencyCodeFunctCount, 
       0                                       AS currencyCodePresCount_Cost, 
       0                                       AS currencyCodePresCount_Bill, 
       0                                       AS currencyCodePresCount_ARFee, 
       0                                       AS currencyCodeCount_Opportunity, 
       Min(pr.wbs1)                            WBS1, 
       Min(LEVEL2.wbs2)                        WBS2, 
       Min(LEVEL3.wbs3)                        WBS3, 
       ''''                                      w1Name, 
       ''''                                      w2Name, 
       ''''                                      w3Name, 
       ''''                                      ChargeType, 
       Max(pr.chargetype)                      ChargeTypeCode, 
       Min(pr.sublevel)                        SubLevel1, 
       Min(LEVEL2.sublevel)                    SubLevel2, 
       Min(LEVEL3.sublevel)                    SubLevel3, 
       ''''                                      w1principal, 
       ''''                                      w2principal, 
       ''''                                      w3principal, 
       ''''                                      w1prinName, 
       ''''                                      w2prinName, 
       ''''                                      w3prinName, 
       ''''                                      w1projMgr, 
       ''''                                      w2projMgr, 
       ''''                                      w3projMgr, 
       ''''                                      w1prgName, 
       ''''                                      w2prgName, 
       ''''                                      w3prgName, 
       ''''                                      w1supervisor, 
       ''''                                      w2supervisor, 
       ''''                                      w3supervisor, 
       ''''                                      w1supName, 
       ''''                                      w2supName, 
       ''''                                      w3supName, 
       ''''                                      PRAddress1, 
       ''''                                      PRAddress2, 
       ''''                                      PRAddress3, 
       ''''                                      PRCity, 
       ''''                                      PRState, 
       ''''                                      PRZip, 
       ''''                                      PRCounty, 
       ''''                                      PRCountry, 
       ''''                                      PRPhone, 
       ''''                                      PRFax, 
       ''''                                      PREmail, 
       ''''                                      ClientID, 
       ''''                                      ClientName, 
       ''''                                      CLAddress, 
       ''''                                      CLAddress1, 
       ''''                                      CLAddress2, 
       ''''                                      CLAddress3, 
       ''''                                      CLAddress4, 
       ''''                                      CLCity, 
       ''''                                      CLState, 
       ''''                                      CLZip, 
       ''''                                      CLCountry, 
       ''''                                      CLPhone, 
       ''''                                      CLFax, 
       ''''                                      CLEMail, 
       ''''                                      w1Org, 
       ''''                                      w2Org, 
       ''''                                      w3Org, 
       ''''                                      w1OrgName, 
       ''''                                      w2OrgName, 
       ''''                                      w3OrgName, 
       ''''                                      AS w1TLProjectName, 
       ''''                                      AS w2TLProjectName, 
       ''''                                      AS w3TLProjectName, 
       ''''                                      ServiceProfileCode, 
       ''''                                      ServiceProfileDesc, 
       0                                       mult, 
       0                                       mult_sum, 
       0                                       fee_c, 
       0                                       fee_b, 
       0                                       consultFee_c, 
       0                                       consultFee_b, 
       0                                       reimbAllow_c, 
       0                                       reimbAllow_b, 
       0.0                                     feeDirectLabor, 
       0.0                                     feeDirectExpense, 
       0.0                                     reimbAllowExpense, 
       0.0                                     reimbAllowConsultant, 
       0                                       contractfee, 
       0                                       contractconsultFee, 
       0                                       contractreimbAllow, 
       0.0                                     contractFeeDirectLabor, 
       0.0                                     contractFeeDirectExpense, 
       0.0                                     contractReimbAllowExpense, 
       0.0                                     contractReimbAllowConsultant, 
       0                                       BudOHRate, 
       ''''                                      w1Status, 
       ''''                                      w2Status, 
       ''''                                      w3Status, 
       ''''                                      w1StatusDesc, 
       ''''                                      w2StatusDesc, 
       ''''                                      w3StatusDesc, 
       ''''                                      wbs1RevType, 
       ''''                                      wbs2RevType, 
       ''''                                      wbs3RevType, 
       ''''                                      wbs1RevTypeDesc, 
       ''''                                      wbs2RevTypeDesc, 
       ''''                                      wbs3RevTypeDesc, 
       ''''                                      UnitTable, 
       NULL                                    w1StartDate, 
       NULL                                    w2StartDate, 
       NULL                                    w3StartDate, 
       NULL                                    w1EndDate, 
       NULL                                    w2EndDate, 
       NULL                                    w3EndDate, 
       0.00                                    w1PctComp, 
       0.00                                    w2PctComp, 
       0.00                                    w3PctComp, 
       0.00                                    w1LabPctComp, 
       0.00                                    w2LabPctComp, 
       0.00                                    w3LabPctComp, 
       0.00                                    w1ExpPctComp, 
       0.00                                    w2ExpPctComp, 
       0.00                                    w3ExpPctComp, 
       ''''                                      BillByDefault, 
       ''''                                      BillableWarning, 
       ''''                                      BudgetedFlag, 
       ''''                                      BudgetSource, 
       ''''                                      BudgetLevel, 
       ''''                                      BudgetedLevels, 
       ''''                                      XCharge, 
       ''''                                      XChargeMethod, 
       0                                       XChargeMult, 
       ''''                                      ContactID, 
       ''''                                      ContactFirstName, 
       ''''                                      ContactLastName, 
       ''''                                      CLBillingAddr, 
       ''''                                      w1LongName, 
       ''''                                      w2LongName, 
       ''''                                      w3LongName, 
       ''''                                      CLBAddress1, 
       ''''                                      CLBAddress2, 
       ''''                                      CLBAddress3, 
       ''''                                      CLBAddress4, 
       ''''                                      CLBCity, 
       ''''                                      CLBState, 
       ''''                                      CLBZip, 
       ''''                                      CLBCountry, 
       ''''                                      CLBPhone, 
       ''''                                      CLBFax, 
       ''''                                      CLBEMail, 
       ''''                                      w1FederalInd, 
       ''''                                      w2FederalInd, 
       ''''                                      w3FederalInd, 
       ''''                                      w1ProjectType, 
       ''''                                      w2ProjectType, 
       ''''                                      w3ProjectType, 
       ''''                                      w1projectTypeDesc, 
       ''''                                      w2projectTypeDesc, 
       ''''                                      w3projectTypeDesc, 
       ''''                                      w1Responsibility, 
       ''''                                      w2Responsibility, 
       ''''                                      w3Responsibility, 
       ''''                                      w1ResponsibilityDesc, 
       ''''                                      w2ResponsibilityDesc, 
       ''''                                      w3ResponsibilityDesc, 
       ''''                                      w1Referable, 
       ''''                                      w2Referable, 
       ''''                                      w3Referable, 
       NULL                                    w1EstCompletionDate, 
       NULL                                    w2EstCompletionDate, 
       NULL                                    w3EstCompletionDate, 
       NULL                                    w1ActCompletionDate, 
       NULL                                    w2ActCompletionDate, 
       NULL                                    w3ActCompletionDate, 
       NULL                                    w1ContractDate, 
       NULL                                    w2ContractDate, 
       NULL                                    w3ContractDate, 
       NULL                                    w1ConstComplDate, 
       NULL                                    w2ConstComplDate, 
       NULL                                    w3ConstComplDate, 
       NULL                                    w1ProfServicesComplDate, 
       NULL                                    w2ProfServicesComplDate, 
       NULL                                    w3ProfServicesComplDate, 
       NULL                                    w1BidDate, 
       NULL                                    w2BidDate, 
       NULL                                    w3BidDate, 
       ''''                                      w1ComplDateComment, 
       ''''                                      w2ComplDateComment, 
       ''''                                      w3ComplDateComment, 
       0                                       w1FirmCost, 
       0                                       w2FirmCost, 
       0                                       w3FirmCost, 
       ''''                                      w1FirmCostComment, 
       ''''                                      w2FirmCostComment, 
       ''''                                      w3FirmCostComment, 
       0                                       w1TotalProjectCost, 
       0                                       w2TotalProjectCost, 
       0                                       w3TotalProjectCost, 
       ''''                                      w1TotalCostComment, 
       ''''                                      w2TotalCostComment, 
       ''''                                      w3TotalCostComment, 
       ''''                                      ClientConfidential, 
       ''''                                      ClientAlias, 
       ''''                                      BillingClientID, 
       ''''                                      BillClientName, 
       ''''                                      BillingContactID, 
       ''''                                      billContactLastName, 
       ''''                                      billContactFirstName, 
       ''''                                      ProposalWBS1, 
       ''''                                      w1CreateUser, 
       ''''                                      w2CreateUser, 
       ''''                                      w3CreateUser, 
       NULL                                    w1CreateDate, 
       NULL                                    w2CreateDate, 
       NULL                                    w3CreateDate, 
       ''''                                      w1ModUser, 
       ''''                                      w2ModUser, 
       ''''                                      w3ModUser, 
       NULL                                    w1ModDate, 
       NULL                                    w2ModDate, 
       NULL                                    w3ModDate, 
       ''''                                      w1CostRateMeth, 
       ''''                                      w2CostRateMeth, 
       ''''                                      w3CostRateMeth, 
       ''''                                      w1CostRateTableNo, 
       ''''                                      w3CostRateTableNo, 
       ''''                                      w3CostRateTableNo, 
       ''''                                      w1PayRateMeth, 
       ''''                                      w2PayRateMeth, 
       ''''                                      w3PayRateMeth, 
       ''''                                      w1PayRateTableNo, 
       ''''                                      w2PayRateTableNo, 
       ''''                                      w3PayRateTableNo, 
       ''''                                      w1Locale, 
       ''''                                      w1Locale, 
       ''''                                      w1Locale, 
       ''''                                      w1LineItemApproval, 
       ''''                                      w2LineItemApproval, 
       ''''                                      w3LineItemApproval, 
       ''''                                      w1LineItemApprovalEK, 
       ''''                                      w2LineItemApprovalEK, 
       ''''                                      w3LineItemApprovalEK, 
       ''''                                      opp_name, 
       ''''                                      opp_number, 
       ''''                                      opp_prproposalwbs1, 
       ''''                                      opp_prproposalname, 
       ''''                                      opp_org, 
       ''''                                      opp_orgname, 
       ''''                                      opp_clname, 
       ''''                                      opp_contactname, 
       ''''                                      opp_projmgrname, 
       ''''                                      opp_principalname, 
       ''''                                      opp_supervisorname, 
       0                                       opp_revenue, 
       ''''                                      opp_probability, 
       0                                       opp_weightedrevenue, 
       NULL                                    opp_opendate, 
       NULL                                    opp_closedate, 
       ''''                                      opp_daysopen, 
       ''''                                      opp_stage, 
       ''''                                      opp_type, 
       ''''                                      opp_source, 
       ''''                                      opp_status, 
       NULL                                    opp_eststartdate, 
       NULL                                    opp_estcompletiondate, 
       ''''                                      opp_address1, 
       ''''                                      opp_address2, 
       ''''                                      opp_address3, 
       ''''                                      opp_city, 
       ''''                                      opp_state, 
       ''''                                      opp_zip, 
       ''''                                      opp_country, 
       ''''                                      opp_createuser, 
       NULL                                    opp_createdate, 
       ''''                                      opp_moduser, 
       NULL                                    opp_moddate, 
       ''''                                      opp_oppdesc 
FROM   pr AS LEVEL3 
       LEFT JOIN projectcustomtabfields 
              ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 
                 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 
                 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes1 
              ON Substring(LEVEL3.org, 1, 2) = groupCFGOrgCodes1.code 
                 AND groupCFGOrgCodes1.orglevel = 1 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes3 
              ON Substring(LEVEL3.org, 7, 3) = groupCFGOrgCodes3.code 
                 AND groupCFGOrgCodes3.orglevel = 3, 
       pr AS LEVEL2, 
       pr, 
       ##rwl_' + @SessionId + ', 
       prsummarysub, 
       (SELECT ''9'' AS recType) myRecord 
WHERE  ( ( prsummarysub.wbs1 = LEVEL3.wbs1 
           AND prsummarysub.wbs2 = LEVEL3.wbs2 
           AND prsummarysub.wbs3 = LEVEL3.wbs3 ) 
         AND ( prsummarysub.wbs1 = LEVEL2.wbs1 
               AND prsummarysub.wbs2 = LEVEL2.wbs2 ) 
         AND ( prsummarysub.wbs1 = pr.wbs1 ) 
         AND ( pr.wbs2 = '' '' 
               AND pr.wbs3 = '' '' 
               AND LEVEL2.wbs3 = '' '' ) ) 
       AND period <= @Period 
       AND ( period >= @MinPeriod 
              OR pr.chargetype <> ''H'' ) 
       AND level3.sublevel = ''N'' 
       AND (( (( ( pr.wbs1 = ''NC1008.000'' ) 
					OR ( pr.wbs1 = ''NC1010.000'' ) 
					OR ( pr.wbs1 = ''NC1010.001'' )  
					OR ( pr.wbs1 = ''NC1010.002'' )  
					OR ( pr.wbs1 = ''NC1010.003'' )  
					OR ( pr.wbs1 = ''NC1011.001'' )  
					OR ( pr.wbs1 = ''NC3008.000'' ) 
					OR ( pr.wbs1 = ''NC3010.000'' ) 
					OR ( pr.wbs1 = ''NC3010.001'' )  
					OR ( pr.wbs1 = ''NC3010.002'' )  
					OR ( pr.wbs1 = ''NC3010.003'' )  
					OR ( pr.wbs1 = ''NC3011.001'' )  
				  )) 
               OR ( ( pr.wbs1 LIKE ''P%'' ) 
                    AND EXISTS (SELECT ''x'' 
                                FROM   cfgchargetype 
                                WHERE  cfgchargetype.type = pr.chargetype 
                                       AND (( cfgchargetype.label =  
                                              ''Promotional'' 
                                            ))) 
                  ) )) 
       AND LEVEL3.wbs1 = ##rwl_' + @SessionId + '.wbs1 
       AND ##rwl_' + @SessionId + '.wbs2 = '' '' 
       AND ##rwl_' + @SessionId + '.wbs3 = '' '' 
       AND ##rwl_' + @SessionId + '.sessionid = 
           ''' + @SessionId + ''' 
       AND pr.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs2 = LEVEL3.wbs2 
       AND pr.wbs2 = '''' 
       AND pr.wbs3 = '''' 
       AND LEVEL2.wbs3 = '''' 
GROUP  BY Isnull(Substring(LEVEL3.org, 1, 2), ''''), 
          Isnull(Substring(LEVEL3.org, 7, 3), ''''), 
          Isnull(pr.wbs1, '''') 
/***LedgerAR: AR, Revenue Columns***/ 
UNION ALL 
SELECT Isnull(Substring(LEVEL3.org, 1, 2), '''') AS group1, 
       Min(groupCFGOrgCodes1.label)            AS groupDesc1, 
       Isnull(Substring(LEVEL3.org, 7, 3), '''') AS group2, 
       Min(groupCFGOrgCodes3.label)            AS groupDesc2, 
       Isnull(pr.wbs1, '''')                     AS group3, 
       Min(pr.NAME)                            AS groupDesc3, 
       ''8''                                     AS recordType, 
       ''''                                      subType, 
       0                                       AS hrsCur, 
       0                                       hrsCurLabReg, 
       0                                       hrsCurLabOvt, 
       0                                       AS hrsYTD, 
       0                                       hrsYTDLabReg, 
       0                                       hrsYTDLabOvt, 
       0                                       AS hrsJTD, 
       0                                       hrsJTDLabReg, 
       0                                       hrsJTDLabOvt, 
       0                                       AS hrsCustom, 
       0                                       hrsCustomLabReg, 
       0                                       hrsCustomLabOvt, 
       0                                       AS hrsBud, 
       0                                       AS hrsPctComp_c, 
       0                                       AS hrsPctComp_b, 
       0                                       AS hrsETC, 
       0                                       AS hrsEAC, 
       0                                       amtCurLabReg_c, 
       0                                       amtCurLabReg_b, 
       0                                       amtCurLabOvt_c, 
       0                                       amtCurLabOvt_b, 
       0                                       amtYTDLabReg_c, 
       0                                       amtYTDLabReg_b, 
       0                                       amtYTDLabOvt_c, 
       0                                       amtYTDLabOvt_b, 
       0                                       amtJTDLabReg_c, 
       0                                       amtJTDLabReg_b, 
       0                                       amtJTDLabOvt_c, 
       0                                       amtJTDLabOvt_b, 
       0                                       amtCustomLabReg_c, 
       0                                       amtCustomLabReg_b, 
       0                                       amtCustomLabOvt_c, 
       0                                       amtCustomLabOvt_b, 
       0                                       AS amtCur_c, 
       0                                       amtCur_b, 
       0                                       AS amtYTD_c, 
       0                                       amtYTD_b, 
       0                                       AS amtJTD_c, 
       0                                       amtJTD_b, 
       0                                       AS amtCustom_c, 
       0                                       amtCustom_b, 
       0                                       AS amtBud_c, 
       0                                       amtBud_b, 
       0                                       AS amtEAC_c, 
       0                                       amtEAC_b, 
       0                                       AS amtETC_c, 
       0                                       amtETC_b, 
       0                                       AS amtPctComp_c, 
       0                                       amtPctComp_b, 
       0                                       AS amtPctCompLab_c, 
       0                                       amtPctCompLab_b, 
       0                                       AS amtPctCompExp_c, 
       0                                       amtPctCompExp_b, 
       Sum(CASE 
             WHEN period = @Period 
                  AND myRecord.rectype = ''8'' THEN ar 
             ELSE 0 
           END)                                arCur_c, 
       Sum(CASE 
             WHEN period = @Period 
                  AND myRecord.rectype = ''8'' THEN ar 
             ELSE 0 
           END)                                arCur_b, 
       Sum(CASE 
             WHEN period >= @MinPeriod 
                  AND myRecord.rectype = ''8'' THEN ar 
             ELSE 0 
           END)                                arYTD_c, 
       Sum(CASE 
             WHEN period >= @MinPeriod 
                  AND myRecord.rectype = ''8'' THEN ar 
             ELSE 0 
           END)                                arYTD_b, 
       Sum(CASE 
             WHEN myRecord.rectype = ''8'' THEN ar 
             ELSE 0 
           END)                                arJTD_c, 
       Sum(CASE 
             WHEN myRecord.rectype = ''8'' THEN ar 
             ELSE 0 
           END)                                arJTD_b, 
       Sum(CASE 
             WHEN period >= 201801 
                  AND period <= 201801 
                  AND myRecord.rectype = ''8'' THEN ar 
             ELSE 0 
           END)                                arCustom_c, 
       Sum(CASE 
             WHEN period >= 201801 
                  AND period <= 201801 
                  AND myRecord.rectype = ''8'' THEN ar 
             ELSE 0 
           END)                                arCustom_b, 
       0                                       AS billCur_c, 
       0                                       AS billCur_b, 
       0                                       AS billYTD_c, 
       0                                       AS billYTD_b, 
       0                                       AS billJTD_c, 
       0                                       AS billJTD_b, 
       0                                       AS billCustom_c, 
       0                                       AS billCustom_b, 
       0                                       AS recdCur_c, 
       0                                       AS recdCur_b, 
       0                                       AS recdYTD_c, 
       0                                       AS recdYTD_b, 
       0                                       AS recdJTD_c, 
       0                                       AS recdJTD_b, 
       0                                       AS recdCustom_c, 
       0                                       AS recdCustom_b, 
       0                                       AS directCur_c, 
       0                                       directCur_b, 
       0                                       AS directYTD_c, 
       0                                       directYTD_b, 
       0                                       AS directJTD_c, 
       0                                       directJTD_b, 
       0                                       AS directCustom_c, 
       0                                       directCustom_b, 
       0                                       AS reimbCur_c, 
       0                                       reimbCur_b, 
       0                                       AS reimbYTD_c, 
       0                                       reimbYTD_b, 
       0                                       AS reimbJTD_c, 
       0                                       reimbJTD_b, 
       0                                       AS reimbCustom_c, 
       0                                       reimbCustom_b, 
       Sum(CASE 
             WHEN period = @Period 
                  AND myRecord.rectype = ''8'' THEN revenue 
             ELSE 0 
           END)                                revCur_c, 
       Sum(CASE 
             WHEN period = @Period 
                  AND myRecord.rectype = ''8'' THEN revenue 
             ELSE 0 
           END)                                revCur_b, 
       Sum(CASE 
             WHEN period >= @MinPeriod 
                  AND myRecord.rectype = ''8'' THEN revenue 
             ELSE 0 
           END)                                revYTD_c, 
       Sum(CASE 
             WHEN period >= @MinPeriod 
                  AND myRecord.rectype = ''8'' THEN revenue 
             ELSE 0 
           END)                                revYTD_b, 
       Sum(CASE 
             WHEN myRecord.rectype = ''8'' THEN revenue 
             ELSE 0 
           END)                                revJTD_c, 
       Sum(CASE 
             WHEN myRecord.rectype = ''8'' THEN revenue 
             ELSE 0 
           END)                                revJTD_b, 
       Sum(CASE 
             WHEN period >= 201801 
                  AND period <= 201801 
                  AND myRecord.rectype = ''8'' THEN revenue 
             ELSE 0 
           END)                                revCustom_c, 
       Sum(CASE 
             WHEN period >= 201801 
                  AND period <= 201801 
                  AND myRecord.rectype = ''8'' THEN revenue 
             ELSE 0 
           END)                                revCustom_b, 
       0.00                                    AS ubGLCur_c, 
       0.00                                    AS ubGLYTD_c, 
       0.00                                    AS ubGLJTD_c, 
       0.00                                    AS ubGLCustom_c, 
       0                                       althrsBud, 
       0                                       altamtBud_c, 
       0                                       altamtBud_b, 
       0.0                                     poAmt_c, 
       0.00                                    poAmt_b, 
       0.0                                     rppctcmpcalc, 
       0.00                                    targetMult_c, 
       0.00                                    targetMult_b, 
       0.00                                    rpfee_c, 
       0.00                                    rpfee_b, 
       0.00                                    rpconsultfee_c, 
       0.00                                    rpconsultfee_b, 
       0.00                                    rpreimballow_c, 
       0.00                                    rpreimballow_b, 
       ''''                                      AS currencyCodeProj, 
       ''''                                      AS currencyCodeBill, 
       ''''                                      AS currencyCodeFunct, 
       ''''                                      AS currencyCodePres_Cost, 
       ''''                                      AS currencyCodePres_Bill, 
       ''''                                      AS currencyCodePres_ARFee, 
       ''''                                      AS currencyCode_Opportunity, 
       0                                       AS currencyCodeProjCount, 
       0                                       AS currencyCodeBillCount, 
       0                                       AS currencyCodeFunctCount, 
       0                                       AS currencyCodePresCount_Cost, 
       0                                       AS currencyCodePresCount_Bill, 
       0                                       AS currencyCodePresCount_ARFee, 
       0                                       AS currencyCodeCount_Opportunity, 
       Min(pr.wbs1)                            WBS1, 
       Min(LEVEL2.wbs2)                        WBS2, 
       Min(LEVEL3.wbs3)                        WBS3, 
       ''''                                      w1Name, 
       ''''                                      w2Name, 
       ''''                                      w3Name, 
       ''''                                      ChargeType, 
       Max(pr.chargetype)                      ChargeTypeCode, 
       Min(pr.sublevel)                        SubLevel1, 
       Min(LEVEL2.sublevel)                    SubLevel2, 
       Min(LEVEL3.sublevel)                    SubLevel3, 
       ''''                                      w1principal, 
       ''''                                      w2principal, 
       ''''                                      w3principal, 
       ''''                                      w1prinName, 
       ''''                                      w2prinName, 
       ''''                                      w3prinName, 
       ''''                                      w1projMgr, 
       ''''                                      w2projMgr, 
       ''''                                      w3projMgr, 
       ''''                                      w1prgName, 
       ''''                                      w2prgName, 
       ''''                                      w3prgName, 
       ''''                                      w1supervisor, 
       ''''                                      w2supervisor, 
       ''''                                      w3supervisor, 
       ''''                                      w1supName, 
       ''''                                      w2supName, 
       ''''                                      w3supName, 
       ''''                                      PRAddress1, 
       ''''                                      PRAddress2, 
       ''''                                      PRAddress3, 
       ''''                                      PRCity, 
       ''''                                      PRState, 
       ''''                                      PRZip, 
       ''''                                      PRCounty, 
       ''''                                      PRCountry, 
       ''''                                      PRPhone, 
       ''''                                      PRFax, 
       ''''                                      PREmail, 
       ''''                                      ClientID, 
       ''''                                      ClientName, 
       ''''                                      CLAddress, 
       ''''                                      CLAddress1, 
       ''''                                      CLAddress2, 
       ''''                                      CLAddress3, 
       ''''                                      CLAddress4, 
       ''''                                      CLCity, 
       ''''                                      CLState, 
       ''''                                      CLZip, 
       ''''                                      CLCountry, 
       ''''                                      CLPhone, 
       ''''                                      CLFax, 
       ''''                                      CLEMail, 
       ''''                                      w1Org, 
       ''''                                      w2Org, 
       ''''                                      w3Org, 
       ''''                                      w1OrgName, 
       ''''                                      w2OrgName, 
       ''''                                      w3OrgName, 
       ''''                                      AS w1TLProjectName, 
       ''''                                      AS w2TLProjectName, 
       ''''                                      AS w3TLProjectName, 
       ''''                                      ServiceProfileCode, 
       ''''                                      ServiceProfileDesc, 
       0                                       mult, 
       0                                       mult_sum, 
       0                                       fee_c, 
       0                                       fee_b, 
       0                                       consultFee_c, 
       0                                       consultFee_b, 
       0                                       reimbAllow_c, 
       0                                       reimbAllow_b, 
       0.0                                     feeDirectLabor, 
       0.0                                     feeDirectExpense, 
       0.0                                     reimbAllowExpense, 
       0.0                                     reimbAllowConsultant, 
       0                                       contractfee, 
       0                                       contractconsultFee, 
       0                                       contractreimbAllow, 
       0.0                                     contractFeeDirectLabor, 
       0.0                                     contractFeeDirectExpense, 
       0.0                                     contractReimbAllowExpense, 
       0.0                                     contractReimbAllowConsultant, 
       0                                       BudOHRate, 
       ''''                                      w1Status, 
       ''''                                      w2Status, 
       ''''                                      w3Status, 
       ''''                                      w1StatusDesc, 
       ''''                                      w2StatusDesc, 
       ''''                                      w3StatusDesc, 
       ''''                                      wbs1RevType, 
       ''''                                      wbs2RevType, 
       ''''                                      wbs3RevType, 
       ''''                                      wbs1RevTypeDesc, 
       ''''                                      wbs2RevTypeDesc, 
       ''''                                      wbs3RevTypeDesc, 
       ''''                                      UnitTable, 
       NULL                                    w1StartDate, 
       NULL                                    w2StartDate, 
       NULL                                    w3StartDate, 
       NULL                                    w1EndDate, 
       NULL                                    w2EndDate, 
       NULL                                    w3EndDate, 
       0.00                                    w1PctComp, 
       0.00                                    w2PctComp, 
       0.00                                    w3PctComp, 
       0.00                                    w1LabPctComp, 
       0.00                                    w2LabPctComp, 
       0.00                                    w3LabPctComp, 
       0.00                                    w1ExpPctComp, 
       0.00                                    w2ExpPctComp, 
       0.00                                    w3ExpPctComp, 
       ''''                                      BillByDefault, 
       ''''                                      BillableWarning, 
       ''''                                      BudgetedFlag, 
       ''''                                      BudgetSource, 
       ''''                                      BudgetLevel, 
       ''''                                      BudgetedLevels, 
       ''''                                      XCharge, 
       ''''                                      XChargeMethod, 
       0                                       XChargeMult, 
       ''''                                      ContactID, 
       ''''                                      ContactFirstName, 
       ''''                                      ContactLastName, 
       ''''                                      CLBillingAddr, 
       ''''                                      w1LongName, 
       ''''                                      w2LongName, 
       ''''                                      w3LongName, 
       ''''                                      CLBAddress1, 
       ''''                                      CLBAddress2, 
       ''''                                      CLBAddress3, 
       ''''                                      CLBAddress4, 
       ''''                                      CLBCity, 
       ''''                                      CLBState, 
       ''''                                      CLBZip, 
       ''''                                      CLBCountry, 
       ''''                                      CLBPhone, 
       ''''                                      CLBFax, 
       ''''                                      CLBEMail, 
       ''''                                      w1FederalInd, 
       ''''                                      w2FederalInd, 
       ''''                                      w3FederalInd, 
       ''''                                      w1ProjectType, 
       ''''                                      w2ProjectType, 
       ''''                                      w3ProjectType, 
       ''''                                      w1projectTypeDesc, 
       ''''                                      w2projectTypeDesc, 
       ''''                                      w3projectTypeDesc, 
       ''''                                      w1Responsibility, 
       ''''                                      w2Responsibility, 
       ''''                                      w3Responsibility, 
       ''''                                      w1ResponsibilityDesc, 
       ''''                                      w2ResponsibilityDesc, 
       ''''                                      w3ResponsibilityDesc, 
       ''''                                      w1Referable, 
       ''''                                      w2Referable, 
       ''''                                      w3Referable, 
       NULL                                    w1EstCompletionDate, 
       NULL                                    w2EstCompletionDate, 
       NULL                                    w3EstCompletionDate, 
       NULL                                    w1ActCompletionDate, 
       NULL                                    w2ActCompletionDate, 
       NULL                                    w3ActCompletionDate, 
       NULL                                    w1ContractDate, 
       NULL                                    w2ContractDate, 
       NULL                                    w3ContractDate, 
       NULL                                    w1ConstComplDate, 
       NULL                                    w2ConstComplDate, 
       NULL                                    w3ConstComplDate, 
       NULL                                    w1ProfServicesComplDate, 
       NULL                                    w2ProfServicesComplDate, 
       NULL                                    w3ProfServicesComplDate, 
       NULL                                    w1BidDate, 
       NULL                                    w2BidDate, 
       NULL                                    w3BidDate, 
       ''''                                      w1ComplDateComment, 
       ''''                                      w2ComplDateComment, 
       ''''                                      w3ComplDateComment, 
       0                                       w1FirmCost, 
       0                                       w2FirmCost, 
       0                                       w3FirmCost, 
       ''''                                      w1FirmCostComment, 
       ''''                                      w2FirmCostComment, 
       ''''                                      w3FirmCostComment, 
       0                                       w1TotalProjectCost, 
       0                                       w2TotalProjectCost, 
       0                                       w3TotalProjectCost, 
       ''''                                      w1TotalCostComment, 
       ''''                                      w2TotalCostComment, 
       ''''                                      w3TotalCostComment, 
       ''''                                      ClientConfidential, 
       ''''                                      ClientAlias, 
       ''''                                      BillingClientID, 
       ''''                                      BillClientName, 
       ''''                                      BillingContactID, 
       ''''                                      billContactLastName, 
       ''''                                      billContactFirstName, 
       ''''                                      ProposalWBS1, 
       ''''                                      w1CreateUser, 
       ''''                                      w2CreateUser, 
       ''''                                      w3CreateUser, 
       NULL                                    w1CreateDate, 
       NULL                                    w2CreateDate, 
       NULL                                    w3CreateDate, 
       ''''                                      w1ModUser, 
       ''''                                      w2ModUser, 
       ''''                                      w3ModUser, 
       NULL                                    w1ModDate, 
       NULL                                    w2ModDate, 
       NULL                                    w3ModDate, 
       ''''                                      w1CostRateMeth, 
       ''''                                      w2CostRateMeth, 
       ''''                                      w3CostRateMeth, 
       ''''                                      w1CostRateTableNo, 
       ''''                                      w3CostRateTableNo, 
       ''''                                      w3CostRateTableNo, 
       ''''                                      w1PayRateMeth, 
       ''''                                      w2PayRateMeth, 
       ''''                                      w3PayRateMeth, 
       ''''                                      w1PayRateTableNo, 
       ''''                                      w2PayRateTableNo, 
       ''''                                      w3PayRateTableNo, 
       ''''                                      w1Locale, 
       ''''                                      w1Locale, 
       ''''                                      w1Locale, 
       ''''                                      w1LineItemApproval, 
       ''''                                      w2LineItemApproval, 
       ''''                                      w3LineItemApproval, 
       ''''                                      w1LineItemApprovalEK, 
       ''''                                      w2LineItemApprovalEK, 
       ''''                                      w3LineItemApprovalEK, 
       ''''                                      opp_name, 
       ''''                                      opp_number, 
       ''''                                      opp_prproposalwbs1, 
       ''''                                      opp_prproposalname, 
       ''''                                      opp_org, 
       ''''                                      opp_orgname, 
       ''''                                      opp_clname, 
       ''''                                      opp_contactname, 
       ''''                                      opp_projmgrname, 
       ''''                                      opp_principalname, 
       ''''                                      opp_supervisorname, 
       0                                       opp_revenue, 
       ''''                                      opp_probability, 
       0                                       opp_weightedrevenue, 
       NULL                                    opp_opendate, 
       NULL                                    opp_closedate, 
       ''''                                      opp_daysopen, 
       ''''                                      opp_stage, 
       ''''                                      opp_type, 
       ''''                                      opp_source, 
       ''''                                      opp_status, 
       NULL                                    opp_eststartdate, 
       NULL                                    opp_estcompletiondate, 
       ''''                                      opp_address1, 
       ''''                                      opp_address2, 
       ''''                                      opp_address3, 
       ''''                                      opp_city, 
       ''''                                      opp_state, 
       ''''                                      opp_zip, 
       ''''                                      opp_country, 
       ''''                                      opp_createuser, 
       NULL                                    opp_createdate, 
       ''''                                      opp_moduser, 
       NULL                                    opp_moddate, 
       ''''                                      opp_oppdesc 
FROM   pr AS LEVEL3 
       LEFT JOIN projectcustomtabfields 
              ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 
                 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 
                 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes1 
              ON Substring(LEVEL3.org, 1, 2) = groupCFGOrgCodes1.code 
                 AND groupCFGOrgCodes1.orglevel = 1 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes3 
              ON Substring(LEVEL3.org, 7, 3) = groupCFGOrgCodes3.code 
                 AND groupCFGOrgCodes3.orglevel = 3, 
       pr AS LEVEL2, 
       pr, 
       ##rwl_' + @SessionId + ', 
       prsummarysub, 
       (SELECT ''8'' AS recType) myRecord 
WHERE  ( ( prsummarysub.wbs1 = LEVEL3.wbs1 
           AND prsummarysub.wbs2 = LEVEL3.wbs2 
           AND prsummarysub.wbs3 = LEVEL3.wbs3 ) 
         AND ( prsummarysub.wbs1 = LEVEL2.wbs1 
               AND prsummarysub.wbs2 = LEVEL2.wbs2 ) 
         AND ( prsummarysub.wbs1 = pr.wbs1 ) 
         AND ( pr.wbs2 = '' '' 
               AND pr.wbs3 = '' '' 
               AND LEVEL2.wbs3 = '' '' ) ) 
       AND period <= @Period 
       AND level3.sublevel = ''N'' 
       AND (( (( ( pr.wbs1 = ''NC1008.000'' ) 
					OR ( pr.wbs1 = ''NC1010.000'' ) 
					OR ( pr.wbs1 = ''NC1010.001'' )  
					OR ( pr.wbs1 = ''NC1010.002'' )  
					OR ( pr.wbs1 = ''NC1010.003'' )  
					OR ( pr.wbs1 = ''NC1011.001'' )  
					OR ( pr.wbs1 = ''NC3008.000'' ) 
					OR ( pr.wbs1 = ''NC3010.000'' ) 
					OR ( pr.wbs1 = ''NC3010.001'' )  
					OR ( pr.wbs1 = ''NC3010.002'' )  
					OR ( pr.wbs1 = ''NC3010.003'' )  
					OR ( pr.wbs1 = ''NC3011.001'' )  
				  )) 
               OR ( ( pr.wbs1 LIKE ''P%'' ) 
                    AND EXISTS (SELECT ''x'' 
                                FROM   cfgchargetype 
                                WHERE  cfgchargetype.type = pr.chargetype 
                                       AND (( cfgchargetype.label =  
                                              ''Promotional'' 
                                            ))) 
                  ) )) 
       AND LEVEL3.wbs1 = ##rwl_' + @SessionId + '.wbs1 
       AND ##rwl_' + @SessionId + '.wbs2 = '' '' 
       AND ##rwl_' + @SessionId + '.wbs3 = '' '' 
       AND ##rwl_' + @SessionId + '.sessionid = 
           ''' + @SessionId + ''' 
       AND pr.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs2 = LEVEL3.wbs2 
       AND pr.wbs2 = '''' 
       AND pr.wbs3 = '''' 
       AND LEVEL2.wbs3 = '''' 
GROUP  BY Isnull(Substring(LEVEL3.org, 1, 2), ''''), 
          Isnull(Substring(LEVEL3.org, 7, 3), ''''), 
          Isnull(pr.wbs1, '''') 
/***Labor Budgets***/ 
UNION ALL 
SELECT Isnull(Substring(LEVEL3.org, 1, 2), '''') AS group1, 
       Min(groupCFGOrgCodes1.label)            AS groupDesc1, 
       Isnull(Substring(LEVEL3.org, 7, 3), '''') AS group2, 
       Min(groupCFGOrgCodes3.label)            AS groupDesc2, 
       Isnull(pr.wbs1, '''')                     AS group3, 
       Min(pr.NAME)                            AS groupDesc3, 
       ''1''                                     AS recordType, 
       ''''                                      subType, 
       0                                       AS hrsCur, 
       0                                       hrsCurLabReg, 
       0                                       hrsCurLabOvt, 
       0                                       AS hrsYTD, 
       0                                       hrsYTDLabReg, 
       0                                       hrsYTDLabOvt, 
       0                                       AS hrsJTD, 
       0                                       hrsJTDLabReg, 
       0                                       hrsJTDLabOvt, 
       0                                       AS hrsCustom, 
       0                                       hrsCustomLabReg, 
       0                                       hrsCustomLabOvt, 
       Sum(hrsbud)                             AS hrsBud, 
       Sum(COALESCE(NULLIF(pr.pctcomp, 0), NULLIF(pr.labpctcomp, 0), 
               NULLIF(LEVEL2.pctcomp, 0), NULLIF(LEVEL2.labpctcomp, 0), 
               NULLIF(LEVEL3.pctcomp, 0), 
               NULLIF(LEVEL3.labpctcomp, 0), NULLIF(lb.pct, 0), 0) * Isnull( 
           hrsbud, 0)) 
                                               AS hrsPctComp_c, 
       Sum(COALESCE(NULLIF(pr.pctcomp, 0), NULLIF(pr.labpctcomp, 0), 
               NULLIF(LEVEL2.pctcomp, 0), NULLIF(LEVEL2.labpctcomp, 0), 
               NULLIF(LEVEL3.pctcomp, 0), 
               NULLIF(LEVEL3.labpctcomp, 0), NULLIF(lb.pct, 0), 0) * Isnull( 
           hrsbud, 0)) 
                                               AS hrsPctComp_b, 
       Sum(etchrs)                             AS hrsETC, 
       Sum(eachrs)                             AS hrsEAC, 
       0                                       amtCurLabReg_c, 
       0                                       amtCurLabReg_b, 
       0                                       amtCurLabOvt_c, 
       0                                       amtCurLabOvt_b, 
       0                                       amtYTDLabReg_c, 
       0                                       amtYTDLabReg_b, 
       0                                       amtYTDLabOvt_c, 
       0                                       amtYTDLabOvt_b, 
       0                                       amtJTDLabReg_c, 
       0                                       amtJTDLabReg_b, 
       0                                       amtJTDLabOvt_c, 
       0                                       amtJTDLabOvt_b, 
       0                                       amtCustomLabReg_c, 
       0                                       amtCustomLabReg_b, 
       0                                       amtCustomLabOvt_c, 
       0                                       amtCustomLabOvt_b, 
       0                                       AS amtCur_c, 
       0                                       amtCur_b, 
       0                                       AS amtYTD_c, 
       0                                       amtYTD_b, 
       0                                       AS amtJTD_c, 
       0                                       amtJTD_b, 
       0                                       AS amtCustom_c, 
       0                                       amtCustom_b, 
       Sum(amtbud)                             AS amtBud_c, 
       Sum(billbud)                            AS amtBud_b, 
       Sum(eacamt)                             AS amtEAC_c, 
       Sum(billeacamt)                         AS amtEAC_b, 
       Sum(etcamt)                             AS amtETC_c, 
       Sum(billetcamt)                         AS amtETC_b, 
       Sum(COALESCE(NULLIF(pr.pctcomp, 0), NULLIF(pr.labpctcomp, 0), 
               NULLIF(LEVEL2.pctcomp, 0), NULLIF(LEVEL2.labpctcomp, 0), 
               NULLIF(LEVEL3.pctcomp, 0), 
               NULLIF(LEVEL3.labpctcomp, 0), NULLIF(lb.pct, 0), 0) * Isnull( 
           amtbud, 0)) 
                                               AS amtPctComp_c, 
       Sum(COALESCE(NULLIF(pr.pctcomp, 0), NULLIF(pr.labpctcomp, 0), 
               NULLIF(LEVEL2.pctcomp, 0), NULLIF(LEVEL2.labpctcomp, 0), 
               NULLIF(LEVEL3.pctcomp, 0), 
               NULLIF(LEVEL3.labpctcomp, 0), NULLIF(lb.pct, 0), 0) * Isnull( 
           billbud, 0) 
       )                                       AS amtPctComp_b, 
       Sum(COALESCE(NULLIF(pr.pctcomp, 0), NULLIF(pr.labpctcomp, 0), 
               NULLIF(LEVEL2.pctcomp, 0), NULLIF(LEVEL2.labpctcomp, 0), 
               NULLIF(LEVEL3.pctcomp, 0), 
               NULLIF(LEVEL3.labpctcomp, 0), NULLIF(lb.pct, 0), 0) * Isnull( 
           amtbud, 0)) 
                                               AS amtPctCompLab_c, 
       Sum(COALESCE(NULLIF(pr.pctcomp, 0), NULLIF(pr.labpctcomp, 0), 
               NULLIF(LEVEL2.pctcomp, 0), NULLIF(LEVEL2.labpctcomp, 0), 
               NULLIF(LEVEL3.pctcomp, 0), 
               NULLIF(LEVEL3.labpctcomp, 0), NULLIF(lb.pct, 0), 0) * Isnull( 
           billbud, 0) 
       )                                       AS amtPctCompLab_b, 
       0.00                                    AS amtPctCompExp_c, 
       0.00                                    AS amtPctCompExp_b, 
       0                                       AS arCur_c, 
       0                                       AS arCur_b, 
       0                                       AS arYTD_c, 
       0                                       AS arYTD_b, 
       0                                       AS arJTD_c, 
       0                                       AS arJTD_b, 
       0                                       AS arCustom_c, 
       0                                       AS arCustom_b, 
       0                                       AS billCur_c, 
       0                                       AS billCur_b, 
       0                                       AS billYTD_c, 
       0                                       AS billYTD_b, 
       0                                       AS billJTD_c, 
       0                                       AS billJTD_b, 
       0                                       AS billCustom_c, 
       0                                       AS billCustom_b, 
       0                                       AS recdCur_c, 
       0                                       AS recdCur_b, 
       0                                       AS recdYTD_c, 
       0                                       AS recdYTD_b, 
       0                                       AS recdJTD_c, 
       0                                       AS recdJTD_b, 
       0                                       AS recdCustom_c, 
       0                                       AS recdCustom_b, 
       0                                       AS directCur_c, 
       0                                       directCur_b, 
       0                                       AS directYTD_c, 
       0                                       directYTD_b, 
       0                                       AS directJTD_c, 
       0                                       directJTD_b, 
       0                                       AS directCustom_c, 
       0                                       directCustom_b, 
       0                                       AS reimbCur_c, 
       0                                       reimbCur_b, 
       0                                       AS reimbYTD_c, 
       0                                       reimbYTD_b, 
       0                                       AS reimbJTD_c, 
       0                                       reimbJTD_b, 
       0                                       AS reimbCustom_c, 
       0                                       reimbCustom_b, 
       0                                       AS revCur_c, 
       0                                       AS revCur_b, 
       0                                       AS revYTD_c, 
       0                                       AS revYTD_b, 
       0                                       AS revJTD_c, 
       0                                       AS revJTD_b, 
       0                                       AS revCustom_c, 
       0                                       AS revCustom_b, 
       0.00                                    AS ubGLCur_c, 
       0.00                                    AS ubGLYTD_c, 
       0.00                                    AS ubGLJTD_c, 
       0.00                                    AS ubGLCustom_c, 
       0                                       althrsBud, 
       0                                       altamtBud_c, 
       0                                       altamtBud_b, 
       0.0                                     poAmt_c, 
       0.00                                    poAmt_b, 
       0.0                                     rppctcmpcalc, 
       0.00                                    targetMult_c, 
       0.00                                    targetMult_b, 
       0.00                                    rpfee_c, 
       0.00                                    rpfee_b, 
       0.00                                    rpconsultfee_c, 
       0.00                                    rpconsultfee_b, 
       0.00                                    rpreimballow_c, 
       0.00                                    rpreimballow_b, 
       ''''                                      AS currencyCodeProj, 
       ''''                                      AS currencyCodeBill, 
       ''''                                      AS currencyCodeFunct, 
       ''''                                      AS currencyCodePres_Cost, 
       ''''                                      AS currencyCodePres_Bill, 
       ''''                                      AS currencyCodePres_ARFee, 
       ''''                                      AS currencyCode_Opportunity, 
       0                                       AS currencyCodeProjCount, 
       0                                       AS currencyCodeBillCount, 
       0                                       AS currencyCodeFunctCount, 
       0                                       AS currencyCodePresCount_Cost, 
       0                                       AS currencyCodePresCount_Bill, 
       0                                       AS currencyCodePresCount_ARFee, 
       0                                       AS currencyCodeCount_Opportunity, 
       Min(pr.wbs1)                            WBS1, 
       Min(LEVEL2.wbs2)                        WBS2, 
       Min(LEVEL3.wbs3)                        WBS3, 
       ''''                                      w1Name, 
       ''''                                      w2Name, 
       ''''                                      w3Name, 
       ''''                                      ChargeType, 
       Max(pr.chargetype)                      ChargeTypeCode, 
       Min(pr.sublevel)                        SubLevel1, 
       Min(LEVEL2.sublevel)                    SubLevel2, 
       Min(LEVEL3.sublevel)                    SubLevel3, 
       ''''                                      w1principal, 
       ''''                                      w2principal, 
       ''''                                      w3principal, 
       ''''                                      w1prinName, 
       ''''                                      w2prinName, 
       ''''                                      w3prinName, 
       ''''                                      w1projMgr, 
       ''''                                      w2projMgr, 
       ''''                                      w3projMgr, 
       ''''                                      w1prgName, 
       ''''                                      w2prgName, 
       ''''                                      w3prgName, 
       ''''                                      w1supervisor, 
       ''''                                      w2supervisor, 
       ''''                                      w3supervisor, 
       ''''                                      w1supName, 
       ''''                                      w2supName, 
       ''''                                      w3supName, 
       ''''                                      PRAddress1, 
       ''''                                      PRAddress2, 
       ''''                                      PRAddress3, 
       ''''                                      PRCity, 
       ''''                                      PRState, 
       ''''                                      PRZip, 
       ''''                                      PRCounty, 
       ''''                                      PRCountry, 
       ''''                                      PRPhone, 
       ''''                                      PRFax, 
       ''''                                      PREmail, 
       ''''                                      ClientID, 
       ''''                                      ClientName, 
       ''''                                      CLAddress, 
       ''''                                      CLAddress1, 
       ''''                                      CLAddress2, 
       ''''                                      CLAddress3, 
       ''''                                      CLAddress4, 
       ''''                                      CLCity, 
       ''''                                      CLState, 
       ''''                                      CLZip, 
       ''''                                      CLCountry, 
       ''''                                      CLPhone, 
       ''''                                      CLFax, 
       ''''                                      CLEMail, 
       ''''                                      w1Org, 
       ''''                                      w2Org, 
       ''''                                      w3Org, 
       ''''                                      w1OrgName, 
       ''''                                      w2OrgName, 
       ''''                                      w3OrgName, 
       ''''                                      AS w1TLProjectName, 
       ''''                                      AS w2TLProjectName, 
       ''''                                      AS w3TLProjectName, 
       ''''                                      ServiceProfileCode, 
       ''''                                      ServiceProfileDesc, 
       0                                       mult, 
       0                                       mult_sum, 
       0                                       fee_c, 
       0                                       fee_b, 
       0                                       consultFee_c, 
       0                                       consultFee_b, 
       0                                       reimbAllow_c, 
       0                                       reimbAllow_b, 
       0.0                                     feeDirectLabor, 
       0.0                                     feeDirectExpense, 
       0.0                                     reimbAllowExpense, 
       0.0                                     reimbAllowConsultant, 
       0                                       contractfee, 
       0                                       contractconsultFee, 
       0                                       contractreimbAllow, 
       0.0                                     contractFeeDirectLabor, 
       0.0                                     contractFeeDirectExpense, 
       0.0                                     contractReimbAllowExpense, 
       0.0                                     contractReimbAllowConsultant, 
       0                                       BudOHRate, 
       ''''                                      w1Status, 
       ''''                                      w2Status, 
       ''''                                      w3Status, 
       ''''                                      w1StatusDesc, 
       ''''                                      w2StatusDesc, 
       ''''                                      w3StatusDesc, 
       ''''                                      wbs1RevType, 
       ''''                                      wbs2RevType, 
       ''''                                      wbs3RevType, 
       ''''                                      wbs1RevTypeDesc, 
       ''''                                      wbs2RevTypeDesc, 
       ''''                                      wbs3RevTypeDesc, 
       ''''                                      UnitTable, 
       NULL                                    w1StartDate, 
       NULL                                    w2StartDate, 
       NULL                                    w3StartDate, 
       NULL                                    w1EndDate, 
       NULL                                    w2EndDate, 
       NULL                                    w3EndDate, 
       0.00                                    w1PctComp, 
       0.00                                    w2PctComp, 
       0.00                                    w3PctComp, 
       0.00                                    w1LabPctComp, 
       0.00                                    w2LabPctComp, 
       0.00                                    w3LabPctComp, 
       0.00                                    w1ExpPctComp, 
       0.00                                    w2ExpPctComp, 
       0.00                                    w3ExpPctComp, 
       ''''                                      BillByDefault, 
       ''''                                      BillableWarning, 
       ''''                                      BudgetedFlag, 
       ''''                                      BudgetSource, 
       ''''                                      BudgetLevel, 
       ''''                                      BudgetedLevels, 
       ''''                                      XCharge, 
       ''''                                      XChargeMethod, 
       0                                       XChargeMult, 
       ''''                                      ContactID, 
       ''''                                      ContactFirstName, 
       ''''                                      ContactLastName, 
       ''''                                      CLBillingAddr, 
       ''''                                      w1LongName, 
       ''''                                      w2LongName, 
       ''''                                      w3LongName, 
       ''''                                      CLBAddress1, 
       ''''                                      CLBAddress2, 
       ''''                                      CLBAddress3, 
       ''''                                      CLBAddress4, 
       ''''                                      CLBCity, 
       ''''                                      CLBState, 
       ''''                                      CLBZip, 
       ''''                                      CLBCountry, 
       ''''                                      CLBPhone, 
       ''''                                      CLBFax, 
       ''''                                      CLBEMail, 
       ''''                                      w1FederalInd, 
       ''''                                      w2FederalInd, 
       ''''                                      w3FederalInd, 
       ''''                                      w1ProjectType, 
       ''''                                      w2ProjectType, 
       ''''                                      w3ProjectType, 
       ''''                                      w1projectTypeDesc, 
       ''''                                      w2projectTypeDesc, 
       ''''                                      w3projectTypeDesc, 
       ''''                                      w1Responsibility, 
       ''''                                      w2Responsibility, 
       ''''                                      w3Responsibility, 
       ''''                                      w1ResponsibilityDesc, 
       ''''                                      w2ResponsibilityDesc, 
       ''''                                      w3ResponsibilityDesc, 
       ''''                                      w1Referable, 
       ''''                                      w2Referable, 
       ''''                                      w3Referable, 
       NULL                                    w1EstCompletionDate, 
       NULL                                    w2EstCompletionDate, 
       NULL                                    w3EstCompletionDate, 
       NULL                                    w1ActCompletionDate, 
       NULL                                    w2ActCompletionDate, 
       NULL                                    w3ActCompletionDate, 
       NULL                                    w1ContractDate, 
       NULL                                    w2ContractDate, 
       NULL                                    w3ContractDate, 
       NULL                                    w1ConstComplDate, 
       NULL                                    w2ConstComplDate, 
       NULL                                    w3ConstComplDate, 
       NULL                                    w1ProfServicesComplDate, 
       NULL                                    w2ProfServicesComplDate, 
       NULL                                    w3ProfServicesComplDate, 
       NULL                                    w1BidDate, 
       NULL                                    w2BidDate, 
       NULL                                    w3BidDate, 
       ''''                                      w1ComplDateComment, 
       ''''                                      w2ComplDateComment, 
       ''''                                      w3ComplDateComment, 
       0                                       w1FirmCost, 
       0                                       w2FirmCost, 
       0                                       w3FirmCost, 
       ''''                                      w1FirmCostComment, 
       ''''                                      w2FirmCostComment, 
       ''''                                      w3FirmCostComment, 
       0                                       w1TotalProjectCost, 
       0                                       w2TotalProjectCost, 
       0                                       w3TotalProjectCost, 
       ''''                                      w1TotalCostComment, 
       ''''                                      w2TotalCostComment, 
       ''''                                      w3TotalCostComment, 
       ''''                                      ClientConfidential, 
       ''''                                      ClientAlias, 
       ''''                                      BillingClientID, 
       ''''                                      BillClientName, 
       ''''                                      BillingContactID, 
       ''''                                      billContactLastName, 
       ''''                                      billContactFirstName, 
       ''''                                      ProposalWBS1, 
       ''''                                      w1CreateUser, 
       ''''                                      w2CreateUser, 
       ''''                                      w3CreateUser, 
       NULL                                    w1CreateDate, 
       NULL                                    w2CreateDate, 
       NULL                                    w3CreateDate, 
       ''''                                      w1ModUser, 
       ''''                                      w2ModUser, 
       ''''                                      w3ModUser, 
       NULL                                    w1ModDate, 
       NULL                                    w2ModDate, 
       NULL                                    w3ModDate, 
       ''''                                      w1CostRateMeth, 
       ''''                                      w2CostRateMeth, 
       ''''                                      w3CostRateMeth, 
       ''''                                      w1CostRateTableNo, 
       ''''                                      w3CostRateTableNo, 
       ''''                                      w3CostRateTableNo, 
       ''''                                      w1PayRateMeth, 
       ''''                                      w2PayRateMeth, 
       ''''                                      w3PayRateMeth, 
       ''''                                      w1PayRateTableNo, 
       ''''                                      w2PayRateTableNo, 
       ''''                                      w3PayRateTableNo, 
       ''''                                      w1Locale, 
       ''''                                      w1Locale, 
       ''''                                      w1Locale, 
       ''''                                      w1LineItemApproval, 
       ''''                                      w2LineItemApproval, 
       ''''                                      w3LineItemApproval, 
       ''''                                      w1LineItemApprovalEK, 
       ''''                                      w2LineItemApprovalEK, 
       ''''                                      w3LineItemApprovalEK, 
       ''''                                      opp_name, 
       ''''                                      opp_number, 
       ''''                                      opp_prproposalwbs1, 
       ''''                                      opp_prproposalname, 
       ''''                                      opp_org, 
       ''''                                      opp_orgname, 
       ''''                                      opp_clname, 
       ''''                                      opp_contactname, 
       ''''                                      opp_projmgrname, 
       ''''                                      opp_principalname, 
       ''''                                      opp_supervisorname, 
       0                                       opp_revenue, 
       ''''                                      opp_probability, 
       0                                       opp_weightedrevenue, 
       NULL                                    opp_opendate, 
       NULL                                    opp_closedate, 
       ''''                                      opp_daysopen, 
       ''''                                      opp_stage, 
       ''''                                      opp_type, 
       ''''                                      opp_source, 
       ''''                                      opp_status, 
       NULL                                    opp_eststartdate, 
       NULL                                    opp_estcompletiondate, 
       ''''                                      opp_address1, 
       ''''                                      opp_address2, 
       ''''                                      opp_address3, 
       ''''                                      opp_city, 
       ''''                                      opp_state, 
       ''''                                      opp_zip, 
       ''''                                      opp_country, 
       ''''                                      opp_createuser, 
       NULL                                    opp_createdate, 
       ''''                                      opp_moduser, 
       NULL                                    opp_moddate, 
       ''''                                      opp_oppdesc 
FROM   pr AS LEVEL3 
       LEFT JOIN projectcustomtabfields 
              ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 
                 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 
                 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes1 
              ON Substring(LEVEL3.org, 1, 2) = groupCFGOrgCodes1.code 
                 AND groupCFGOrgCodes1.orglevel = 1 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes3 
              ON Substring(LEVEL3.org, 7, 3) = groupCFGOrgCodes3.code 
                 AND groupCFGOrgCodes3.orglevel = 3, 
       pr AS LEVEL2, 
       pr, 
       ##rwl_' + @SessionId + ', 
       lb 
WHERE  ( ( lb.wbs1 = LEVEL3.wbs1 
           AND lb.wbs2 = LEVEL3.wbs2 
           AND lb.wbs3 = LEVEL3.wbs3 ) 
         AND ( lb.wbs1 = LEVEL2.wbs1 
               AND lb.wbs2 = LEVEL2.wbs2 ) 
         AND ( lb.wbs1 = pr.wbs1 ) 
         AND ( pr.wbs2 = '' '' 
               AND pr.wbs3 = '' '' 
               AND LEVEL2.wbs3 = '' '' ) ) 
       AND (( (( ( pr.wbs1 = ''NC1008.000'' ) 
					OR ( pr.wbs1 = ''NC1010.000'' ) 
					OR ( pr.wbs1 = ''NC1010.001'' )  
					OR ( pr.wbs1 = ''NC1010.002'' )  
					OR ( pr.wbs1 = ''NC1010.003'' )  
					OR ( pr.wbs1 = ''NC1011.001'' )  
					OR ( pr.wbs1 = ''NC3008.000'' ) 
					OR ( pr.wbs1 = ''NC3010.000'' ) 
					OR ( pr.wbs1 = ''NC3010.001'' )  
					OR ( pr.wbs1 = ''NC3010.002'' )  
					OR ( pr.wbs1 = ''NC3010.003'' )  
					OR ( pr.wbs1 = ''NC3011.001'' )  
				  )) 
               OR ( ( pr.wbs1 LIKE ''P%'' ) 
                    AND EXISTS (SELECT ''x'' 
                                FROM   cfgchargetype 
                                WHERE  cfgchargetype.type = pr.chargetype 
                                       AND (( cfgchargetype.label =  
                                              ''Promotional'' 
                                            ))) 
                  ) )) 
       AND LEVEL3.wbs1 = ##rwl_' + @SessionId + '.wbs1 
       AND ##rwl_' + @SessionId + '.wbs2 = '' '' 
       AND ##rwl_' + @SessionId + '.wbs3 = '' '' 
       AND ##rwl_' + @SessionId + '.sessionid = 
           ''' + @SessionId + ''' 
       AND pr.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs2 = LEVEL3.wbs2 
       AND pr.wbs2 = '''' 
       AND pr.wbs3 = '''' 
       AND LEVEL2.wbs3 = '''' 
GROUP  BY Isnull(Substring(LEVEL3.org, 1, 2), ''''), 
          Isnull(Substring(LEVEL3.org, 7, 3), ''''), 
          Isnull(pr.wbs1, '''') 
/***Direct and Reimbursable Budgets***/ 
UNION ALL 
SELECT Isnull(Substring(LEVEL3.org, 1, 2), '''') AS group1, 
       Min(groupCFGOrgCodes1.label)            AS groupDesc1, 
       Isnull(Substring(LEVEL3.org, 7, 3), '''') AS group2, 
       Min(groupCFGOrgCodes3.label)            AS groupDesc2, 
       Isnull(pr.wbs1, '''')                     AS group3, 
       Min(pr.NAME)                            AS groupDesc3, 
       CASE 
         WHEN ca.type < 7 THEN ''5'' 
         WHEN ca.type >= 7 
              AND ca.type < 9 THEN ''3'' 
         WHEN ca.type >= 9 
              AND ca.type < 10 THEN ''6'' 
         ELSE ''7'' 
       END                                     AS recordType, 
       Min(CASE 
             WHEN ( ca.type = 6 ) 
                   OR ( ca.type = 8 ) THEN ''0'' 
             ELSE ''1'' 
           END)                                subType, 
       0                                       AS hrsCur, 
       0                                       hrsCurLabReg, 
       0                                       hrsCurLabOvt, 
       0                                       AS hrsYTD, 
       0                                       hrsYTDLabReg, 
       0                                       hrsYTDLabOvt, 
       0                                       AS hrsJTD, 
       0                                       hrsJTDLabReg, 
       0                                       hrsJTDLabOvt, 
       0                                       AS hrsCustom, 
       0                                       hrsCustomLabReg, 
       0                                       hrsCustomLabOvt, 
       0                                       AS hrsBud, 
       0                                       AS hrsPctComp_c, 
       0                                       AS hrsPctComp_b, 
       0                                       AS hrsETC, 
       0                                       AS hrsEAC, 
       0                                       amtCurLabReg_c, 
       0                                       amtCurLabReg_b, 
       0                                       amtCurLabOvt_c, 
       0                                       amtCurLabOvt_b, 
       0                                       amtYTDLabReg_c, 
       0                                       amtYTDLabReg_b, 
       0                                       amtYTDLabOvt_c, 
       0                                       amtYTDLabOvt_b, 
       0                                       amtJTDLabReg_c, 
       0                                       amtJTDLabReg_b, 
       0                                       amtJTDLabOvt_c, 
       0                                       amtJTDLabOvt_b, 
       0                                       amtCustomLabReg_c, 
       0                                       amtCustomLabReg_b, 
       0                                       amtCustomLabOvt_c, 
       0                                       amtCustomLabOvt_b, 
       0                                       AS amtCur_c, 
       0                                       amtCur_b, 
       0                                       AS amtYTD_c, 
       0                                       amtYTD_b, 
       0                                       AS amtJTD_c, 
       0                                       amtJTD_b, 
       0                                       AS amtCustom_c, 
       0                                       amtCustom_b, 
       Sum(amtbud)                             AS amtBud_c, 
       Sum(billbud)                            AS amtBud_b, 
       Sum(eacamt)                             AS amtEAC_c, 
       Sum(billeacamt)                         AS amtEAC_b, 
       Sum(etcamt)                             AS amtETC_c, 
       Sum(billetcamt)                         AS amtETC_b, 
       Sum(COALESCE(NULLIF(pr.pctcomp, 0), NULLIF(pr.exppctcomp, 0), 
               NULLIF(LEVEL2.pctcomp, 0), NULLIF(LEVEL2.exppctcomp, 0), 
               NULLIF(LEVEL3.pctcomp, 0), 
               NULLIF(LEVEL3.exppctcomp, 0), NULLIF(eb.pct, 0), 0) * Isnull( 
           amtbud, 0)) 
                                               AS amtPctComp_c, 
       Sum(COALESCE(NULLIF(pr.pctcomp, 0), NULLIF(pr.exppctcomp, 0), 
               NULLIF(LEVEL2.pctcomp, 0), NULLIF(LEVEL2.exppctcomp, 0), 
               NULLIF(LEVEL3.pctcomp, 0), 
               NULLIF(LEVEL3.exppctcomp, 0), NULLIF(eb.pct, 0), 0) * Isnull( 
           billbud, 0) 
       )                                       AS amtPctComp_b, 
       0.00                                    AS amtPctCompLab_c, 
       0.00                                    amtPctCompLab_b, 
       Sum(COALESCE(NULLIF(pr.pctcomp, 0), NULLIF(pr.exppctcomp, 0), 
               NULLIF(LEVEL2.pctcomp, 0), NULLIF(LEVEL2.exppctcomp, 0), 
               NULLIF(LEVEL3.pctcomp, 0), 
               NULLIF(LEVEL3.exppctcomp, 0), NULLIF(eb.pct, 0), 0) * Isnull( 
           amtbud, 0)) 
                                               AS amtPctCompExp_c, 
       Sum(COALESCE(NULLIF(pr.pctcomp, 0), NULLIF(pr.exppctcomp, 0), 
               NULLIF(LEVEL2.pctcomp, 0), NULLIF(LEVEL2.exppctcomp, 0), 
               NULLIF(LEVEL3.pctcomp, 0), 
               NULLIF(LEVEL3.exppctcomp, 0), NULLIF(eb.pct, 0), 0) * Isnull( 
           billbud, 0) 
       )                                       AS amtPctCompExp_b, 
       0                                       AS arCur_c, 
       0                                       AS arCur_b, 
       0                                       AS arYTD_c, 
       0                                       AS arYTD_b, 
       0                                       AS arJTD_c, 
       0                                       AS arJTD_b, 
       0                                       AS arCustom_c, 
       0                                       AS arCustom_b, 
       0                                       AS billCur_c, 
       0                                       AS billCur_b, 
       0                                       AS billYTD_c, 
       0                                       AS billYTD_b, 
       0                                       AS billJTD_c, 
       0                                       AS billJTD_b, 
       0                                       AS billCustom_c, 
       0                                       AS billCustom_b, 
       0                                       AS recdCur_c, 
       0                                       AS recdCur_b, 
       0                                       AS recdYTD_c, 
       0                                       AS recdYTD_b, 
       0                                       AS recdJTD_c, 
       0                                       AS recdJTD_b, 
       0                                       AS recdCustom_c, 
       0                                       AS recdCustom_b, 
       0                                       AS directCur_c, 
       0                                       directCur_b, 
       0                                       AS directYTD_c, 
       0                                       directYTD_b, 
       0                                       AS directJTD_c, 
       0                                       directJTD_b, 
       0                                       AS directCustom_c, 
       0                                       directCustom_b, 
       0                                       AS reimbCur_c, 
       0                                       reimbCur_b, 
       0                                       AS reimbYTD_c, 
       0                                       reimbYTD_b, 
       0                                       AS reimbJTD_c, 
       0                                       reimbJTD_b, 
       0                                       AS reimbCustom_c, 
       0                                       reimbCustom_b, 
       0                                       AS revCur_c, 
       0                                       AS revCur_b, 
       0                                       AS revYTD_c, 
       0                                       AS revYTD_b, 
       0                                       AS revJTD_c, 
       0                                       AS revJTD_b, 
       0                                       AS revCustom_c, 
       0                                       AS revCustom_b, 
       0.00                                    AS ubGLCur_c, 
       0.00                                    AS ubGLYTD_c, 
       0.00                                    AS ubGLJTD_c, 
       0.00                                    AS ubGLCustom_c, 
       0                                       althrsBud, 
       0                                       altamtBud_c, 
       0                                       altamtBud_b, 
       0.0                                     poAmt_c, 
       0.00                                    poAmt_b, 
       0.0                                     rppctcmpcalc, 
       0.00                                    targetMult_c, 
       0.00                                    targetMult_b, 
       0.00                                    rpfee_c, 
       0.00                                    rpfee_b, 
       0.00                                    rpconsultfee_c, 
       0.00                                    rpconsultfee_b, 
       0.00                                    rpreimballow_c, 
       0.00                                    rpreimballow_b, 
       ''''                                      AS currencyCodeProj, 
       ''''                                      AS currencyCodeBill, 
       ''''                                      AS currencyCodeFunct, 
       ''''                                      AS currencyCodePres_Cost, 
       ''''                                      AS currencyCodePres_Bill, 
       ''''                                      AS currencyCodePres_ARFee, 
       ''''                                      AS currencyCode_Opportunity, 
       0                                       AS currencyCodeProjCount, 
       0                                       AS currencyCodeBillCount, 
       0                                       AS currencyCodeFunctCount, 
       0                                       AS currencyCodePresCount_Cost, 
       0                                       AS currencyCodePresCount_Bill, 
       0                                       AS currencyCodePresCount_ARFee, 
       0                                       AS currencyCodeCount_Opportunity, 
       Min(pr.wbs1)                            WBS1, 
       Min(LEVEL2.wbs2)                        WBS2, 
       Min(LEVEL3.wbs3)                        WBS3, 
       ''''                                      w1Name, 
       ''''                                      w2Name, 
       ''''                                      w3Name, 
       ''''                                      ChargeType, 
       Max(pr.chargetype)                      ChargeTypeCode, 
       Min(pr.sublevel)                        SubLevel1, 
       Min(LEVEL2.sublevel)                    SubLevel2, 
       Min(LEVEL3.sublevel)                    SubLevel3, 
       ''''                                      w1principal, 
       ''''                                      w2principal, 
       ''''                                      w3principal, 
       ''''                                      w1prinName, 
       ''''                                      w2prinName, 
       ''''                                      w3prinName, 
       ''''                                      w1projMgr, 
       ''''                                      w2projMgr, 
       ''''                                      w3projMgr, 
       ''''                                      w1prgName, 
       ''''                                      w2prgName, 
       ''''                                      w3prgName, 
       ''''                                      w1supervisor, 
       ''''                                      w2supervisor, 
       ''''                                      w3supervisor, 
       ''''                                      w1supName, 
       ''''                                      w2supName, 
       ''''                                      w3supName, 
       ''''                                      PRAddress1, 
       ''''                                      PRAddress2, 
       ''''                                      PRAddress3, 
       ''''                                      PRCity, 
       ''''                                      PRState, 
       ''''                                      PRZip, 
       ''''                                      PRCounty, 
       ''''                                      PRCountry, 
       ''''                                      PRPhone, 
       ''''                                      PRFax, 
       ''''                                      PREmail, 
       ''''                                      ClientID, 
       ''''                                      ClientName, 
       ''''                                      CLAddress, 
       ''''                                      CLAddress1, 
       ''''                                      CLAddress2, 
       ''''                                      CLAddress3, 
       ''''                                      CLAddress4, 
       ''''                                      CLCity, 
       ''''                                      CLState, 
       ''''                                      CLZip, 
       ''''                                      CLCountry, 
       ''''                                      CLPhone, 
       ''''                                      CLFax, 
       ''''                                      CLEMail, 
       ''''                                      w1Org, 
       ''''                                      w2Org, 
       ''''                                      w3Org, 
       ''''                                      w1OrgName, 
       ''''                                      w2OrgName, 
       ''''                                      w3OrgName, 
       ''''                                      AS w1TLProjectName, 
       ''''                                      AS w2TLProjectName, 
       ''''                                      AS w3TLProjectName, 
       ''''                                      ServiceProfileCode, 
       ''''                                      ServiceProfileDesc, 
       0                                       mult, 
       0                                       mult_sum, 
       0                                       fee_c, 
       0                                       fee_b, 
       0                                       consultFee_c, 
       0                                       consultFee_b, 
       0                                       reimbAllow_c, 
       0                                       reimbAllow_b, 
       0.0                                     feeDirectLabor, 
       0.0                                     feeDirectExpense, 
       0.0                                     reimbAllowExpense, 
       0.0                                     reimbAllowConsultant, 
       0                                       contractfee, 
       0                                       contractconsultFee, 
       0                                       contractreimbAllow, 
       0.0                                     contractFeeDirectLabor, 
       0.0                                     contractFeeDirectExpense, 
       0.0                                     contractReimbAllowExpense, 
       0.0                                     contractReimbAllowConsultant, 
       0                                       BudOHRate, 
       ''''                                      w1Status, 
       ''''                                      w2Status, 
       ''''                                      w3Status, 
       ''''                                      w1StatusDesc, 
       ''''                                      w2StatusDesc, 
       ''''                                      w3StatusDesc, 
       ''''                                      wbs1RevType, 
       ''''                                      wbs2RevType, 
       ''''                                      wbs3RevType, 
       ''''                                      wbs1RevTypeDesc, 
       ''''                                      wbs2RevTypeDesc, 
       ''''                                      wbs3RevTypeDesc, 
       ''''                                      UnitTable, 
       NULL                                    w1StartDate, 
       NULL                                    w2StartDate, 
       NULL                                    w3StartDate, 
       NULL                                    w1EndDate, 
       NULL                                    w2EndDate, 
       NULL                                    w3EndDate, 
       0.00                                    w1PctComp, 
       0.00                                    w2PctComp, 
       0.00                                    w3PctComp, 
       0.00                                    w1LabPctComp, 
       0.00                                    w2LabPctComp, 
       0.00                                    w3LabPctComp, 
       0.00                                    w1ExpPctComp, 
       0.00                                    w2ExpPctComp, 
       0.00                                    w3ExpPctComp, 
       ''''                                      BillByDefault, 
       ''''                                      BillableWarning, 
       ''''                                      BudgetedFlag, 
       ''''                                      BudgetSource, 
       ''''                                      BudgetLevel, 
       ''''                                      BudgetedLevels, 
       ''''                                      XCharge, 
       ''''                                      XChargeMethod, 
       0                                       XChargeMult, 
       ''''                                      ContactID, 
       ''''                                      ContactFirstName, 
       ''''                                      ContactLastName, 
       ''''                                      CLBillingAddr, 
       ''''                                      w1LongName, 
       ''''                                      w2LongName, 
       ''''                                      w3LongName, 
       ''''                                      CLBAddress1, 
       ''''                                      CLBAddress2, 
       ''''                                      CLBAddress3, 
       ''''                                      CLBAddress4, 
       ''''                                      CLBCity, 
       ''''                                      CLBState, 
       ''''                                      CLBZip, 
       ''''                                      CLBCountry, 
       ''''                                      CLBPhone, 
       ''''                                      CLBFax, 
       ''''                                      CLBEMail, 
       ''''                                      w1FederalInd, 
       ''''                                      w2FederalInd, 
       ''''                                      w3FederalInd, 
       ''''                                      w1ProjectType, 
       ''''                                      w2ProjectType, 
       ''''                                      w3ProjectType, 
       ''''                                      w1projectTypeDesc, 
       ''''                                      w2projectTypeDesc, 
       ''''                                      w3projectTypeDesc, 
       ''''                                      w1Responsibility, 
       ''''                                      w2Responsibility, 
       ''''                                      w3Responsibility, 
       ''''                                      w1ResponsibilityDesc, 
       ''''                                      w2ResponsibilityDesc, 
       ''''                                      w3ResponsibilityDesc, 
       ''''                                      w1Referable, 
       ''''                                      w2Referable, 
       ''''                                      w3Referable, 
       NULL                                    w1EstCompletionDate, 
       NULL                                    w2EstCompletionDate, 
       NULL                                    w3EstCompletionDate, 
       NULL                                    w1ActCompletionDate, 
       NULL                                    w2ActCompletionDate, 
       NULL                                    w3ActCompletionDate, 
       NULL                                    w1ContractDate, 
       NULL                                    w2ContractDate, 
       NULL                                    w3ContractDate, 
       NULL                                    w1ConstComplDate, 
       NULL                                    w2ConstComplDate, 
       NULL                                    w3ConstComplDate, 
       NULL                                    w1ProfServicesComplDate, 
       NULL                                    w2ProfServicesComplDate, 
       NULL                                    w3ProfServicesComplDate, 
       NULL                                    w1BidDate, 
       NULL                                    w2BidDate, 
       NULL                                    w3BidDate, 
       ''''                                      w1ComplDateComment, 
       ''''                                      w2ComplDateComment, 
       ''''                                      w3ComplDateComment, 
       0                                       w1FirmCost, 
       0                                       w2FirmCost, 
       0                                       w3FirmCost, 
       ''''                                      w1FirmCostComment, 
       ''''                                      w2FirmCostComment, 
       ''''                                      w3FirmCostComment, 
       0                                       w1TotalProjectCost, 
       0                                       w2TotalProjectCost, 
       0                                       w3TotalProjectCost, 
       ''''                                      w1TotalCostComment, 
       ''''                                      w2TotalCostComment, 
       ''''                                      w3TotalCostComment, 
       ''''                                      ClientConfidential, 
       ''''                                      ClientAlias, 
       ''''                                      BillingClientID, 
       ''''                                      BillClientName, 
       ''''                                      BillingContactID, 
       ''''                                      billContactLastName, 
       ''''                                      billContactFirstName, 
       ''''                                      ProposalWBS1, 
       ''''                                      w1CreateUser, 
       ''''                                      w2CreateUser, 
       ''''                                      w3CreateUser, 
       NULL                                    w1CreateDate, 
       NULL                                    w2CreateDate, 
       NULL                                    w3CreateDate, 
       ''''                                      w1ModUser, 
       ''''                                      w2ModUser, 
       ''''                                      w3ModUser, 
       NULL                                    w1ModDate, 
       NULL                                    w2ModDate, 
       NULL                                    w3ModDate, 
       ''''                                      w1CostRateMeth, 
       ''''                                      w2CostRateMeth, 
       ''''                                      w3CostRateMeth, 
       ''''                                      w1CostRateTableNo, 
       ''''                                      w3CostRateTableNo, 
       ''''                                      w3CostRateTableNo, 
       ''''                                      w1PayRateMeth, 
       ''''                                      w2PayRateMeth, 
       ''''                                      w3PayRateMeth, 
       ''''                                      w1PayRateTableNo, 
       ''''                                      w2PayRateTableNo, 
       ''''                                      w3PayRateTableNo, 
       ''''                                      w1Locale, 
       ''''                                      w1Locale, 
       ''''                                      w1Locale, 
       ''''                                      w1LineItemApproval, 
       ''''                                      w2LineItemApproval, 
       ''''                                      w3LineItemApproval, 
       ''''                                      w1LineItemApprovalEK, 
       ''''                                      w2LineItemApprovalEK, 
       ''''                                      w3LineItemApprovalEK, 
       ''''                                      opp_name, 
       ''''                                      opp_number, 
       ''''                                      opp_prproposalwbs1, 
       ''''                                      opp_prproposalname, 
       ''''                                      opp_org, 
       ''''                                      opp_orgname, 
       ''''                                      opp_clname, 
       ''''                                      opp_contactname, 
       ''''                                      opp_projmgrname, 
       ''''                                      opp_principalname, 
       ''''                                      opp_supervisorname, 
       0                                       opp_revenue, 
       ''''                                      opp_probability, 
       0                                       opp_weightedrevenue, 
       NULL                                    opp_opendate, 
       NULL                                    opp_closedate, 
       ''''                                      opp_daysopen, 
       ''''                                      opp_stage, 
       ''''                                      opp_type, 
       ''''                                      opp_source, 
       ''''                                      opp_status, 
       NULL                                    opp_eststartdate, 
       NULL                                    opp_estcompletiondate, 
       ''''                                      opp_address1, 
       ''''                                      opp_address2, 
       ''''                                      opp_address3, 
       ''''                                      opp_city, 
       ''''                                      opp_state, 
       ''''                                      opp_zip, 
       ''''                                      opp_country, 
       ''''                                      opp_createuser, 
       NULL                                    opp_createdate, 
       ''''                                      opp_moduser, 
       NULL                                    opp_moddate, 
       ''''                                      opp_oppdesc 
FROM   pr AS LEVEL3 
       LEFT JOIN projectcustomtabfields 
              ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 
                 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 
                 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes1 
              ON Substring(LEVEL3.org, 1, 2) = groupCFGOrgCodes1.code 
                 AND groupCFGOrgCodes1.orglevel = 1 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes3 
              ON Substring(LEVEL3.org, 7, 3) = groupCFGOrgCodes3.code 
                 AND groupCFGOrgCodes3.orglevel = 3, 
       pr AS LEVEL2, 
       pr, 
       ##rwl_' + @SessionId + ', 
       eb 
       LEFT JOIN ca 
              ON eb.account = ca.account 
WHERE  ( ( eb.wbs1 = LEVEL3.wbs1 
           AND eb.wbs2 = LEVEL3.wbs2 
           AND eb.wbs3 = LEVEL3.wbs3 ) 
         AND ( eb.wbs1 = LEVEL2.wbs1 
               AND eb.wbs2 = LEVEL2.wbs2 ) 
         AND ( eb.wbs1 = pr.wbs1 ) 
         AND ( pr.wbs2 = '' '' 
               AND pr.wbs3 = '' '' 
               AND LEVEL2.wbs3 = '' '' ) ) 
       AND (( (( ( pr.wbs1 = ''NC1008.000'' ) 
					OR ( pr.wbs1 = ''NC1010.000'' ) 
					OR ( pr.wbs1 = ''NC1010.001'' )  
					OR ( pr.wbs1 = ''NC1010.002'' )  
					OR ( pr.wbs1 = ''NC1010.003'' )  
					OR ( pr.wbs1 = ''NC1011.001'' )  
					OR ( pr.wbs1 = ''NC3008.000'' ) 
					OR ( pr.wbs1 = ''NC3010.000'' ) 
					OR ( pr.wbs1 = ''NC3010.001'' )  
					OR ( pr.wbs1 = ''NC3010.002'' )  
					OR ( pr.wbs1 = ''NC3010.003'' )  
					OR ( pr.wbs1 = ''NC3011.001'' )  
				  )) 
               OR ( ( pr.wbs1 LIKE ''P%'' ) 
                    AND EXISTS (SELECT ''x'' 
                                FROM   cfgchargetype 
                                WHERE  cfgchargetype.type = pr.chargetype 
                                       AND (( cfgchargetype.label =  
                                              ''Promotional'' 
                                            ))) 
                  ) )) 
       AND LEVEL3.wbs1 = ##rwl_' + @SessionId + '.wbs1 
       AND ##rwl_' + @SessionId + '.wbs2 = '' '' 
       AND ##rwl_' + @SessionId + '.wbs3 = '' '' 
       AND ##rwl_' + @SessionId + '.sessionid = 
           ''' + @SessionId + ''' 
       AND pr.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs2 = LEVEL3.wbs2 
       AND pr.wbs2 = '''' 
       AND pr.wbs3 = '''' 
       AND LEVEL2.wbs3 = '''' 
GROUP  BY Isnull(Substring(LEVEL3.org, 1, 2), ''''), 
          Isnull(Substring(LEVEL3.org, 7, 3), ''''), 
          Isnull(pr.wbs1, ''''), 
          CASE 
            WHEN ca.type < 7 THEN ''5'' 
            WHEN ca.type >= 7 
                 AND ca.type < 9 THEN ''3'' 
            WHEN ca.type >= 9 
                 AND ca.type < 10 THEN ''6'' 
            ELSE ''7'' 
          END, 
          CASE 
            WHEN ( ca.type = 6 ) 
                  OR ( ca.type = 8 ) THEN ''0'' 
            ELSE ''1'' 
          END 
UNION ALL 
SELECT Isnull(Substring(LEVEL3.org, 1, 2), '''') AS group1, 
       Min(groupCFGOrgCodes1.label)            AS groupDesc1, 
       Isnull(Substring(LEVEL3.org, 7, 3), '''') AS group2, 
       Min(groupCFGOrgCodes3.label)            AS groupDesc2, 
       Isnull(pr.wbs1, '''')                     AS group3, 
       Min(pr.NAME)                            AS groupDesc3, 
       Min(CASE 
             WHEN ca.type < 7 THEN ''5'' 
             WHEN ca.type >= 7 
                  AND ca.type < 9 THEN ''3'' 
             WHEN ca.type >= 9 
                  AND ca.type < 10 THEN ''6'' 
             ELSE ''7'' 
           END)                                AS recordType, 
       Min(CASE 
             WHEN ( ca.type = 6 ) 
                   OR ( ca.type = 8 ) THEN ''0'' 
             ELSE ''1'' 
           END)                                subType, 
       0                                       AS hrsCur, 
       0                                       hrsCurLabReg, 
       0                                       hrsCurLabOvt, 
       0                                       AS hrsYTD, 
       0                                       hrsYTDLabReg, 
       0                                       hrsYTDLabOvt, 
       0                                       AS hrsJTD, 
       0                                       hrsJTDLabReg, 
       0                                       hrsJTDLabOvt, 
       0                                       AS hrsCustom, 
       0                                       hrsCustomLabReg, 
       0                                       hrsCustomLabOvt, 
       0                                       AS hrsBud, 
       0                                       AS hrsPctComp_c, 
       0                                       AS hrsPctComp_b, 
       0                                       AS hrsETC, 
       0                                       AS hrsEAC, 
       0                                       amtCurLabReg_c, 
       0                                       amtCurLabReg_b, 
       0                                       amtCurLabOvt_c, 
       0                                       amtCurLabOvt_b, 
       0                                       amtYTDLabReg_c, 
       0                                       amtYTDLabReg_b, 
       0                                       amtYTDLabOvt_c, 
       0                                       amtYTDLabOvt_b, 
       0                                       amtJTDLabReg_c, 
       0                                       amtJTDLabReg_b, 
       0                                       amtJTDLabOvt_c, 
       0                                       amtJTDLabOvt_b, 
       0                                       amtCustomLabReg_c, 
       0                                       amtCustomLabReg_b, 
       0                                       amtCustomLabOvt_c, 
       0                                       amtCustomLabOvt_b, 
       0                                       AS amtCur_c, 
       0                                       amtCur_b, 
       0                                       AS amtYTD_c, 
       0                                       amtYTD_b, 
       0                                       AS amtJTD_c, 
       0                                       amtJTD_b, 
       0                                       AS amtCustom_c, 
       0                                       amtCustom_b, 
       0                                       AS amtBud_c, 
       0                                       amtBud_b, 
       0                                       AS amtEAC_c, 
       0                                       amtEAC_b, 
       0                                       AS amtETC_c, 
       0                                       amtETC_b, 
       0                                       AS amtPctComp_c, 
       0                                       amtPctComp_b, 
       0                                       AS amtPctCompLab_c, 
       0                                       amtPctCompLab_b, 
       0                                       AS amtPctCompExp_c, 
       0                                       amtPctCompExp_b, 
       0                                       AS arCur_c, 
       0                                       AS arCur_b, 
       0                                       AS arYTD_c, 
       0                                       AS arYTD_b, 
       0                                       AS arJTD_c, 
       0                                       AS arJTD_b, 
       0                                       AS arCustom_c, 
       0                                       AS arCustom_b, 
       0                                       AS billCur_c, 
       0                                       AS billCur_b, 
       0                                       AS billYTD_c, 
       0                                       AS billYTD_b, 
       0                                       AS billJTD_c, 
       0                                       AS billJTD_b, 
       0                                       AS billCustom_c, 
       0                                       AS billCustom_b, 
       0                                       AS recdCur_c, 
       0                                       AS recdCur_b, 
       0                                       AS recdYTD_c, 
       0                                       AS recdYTD_b, 
       0                                       AS recdJTD_c, 
       0                                       AS recdJTD_b, 
       0                                       AS recdCustom_c, 
       0                                       AS recdCustom_b, 
       0                                       AS directCur_c, 
       0                                       directCur_b, 
       0                                       AS directYTD_c, 
       0                                       directYTD_b, 
       0                                       AS directJTD_c, 
       0                                       directJTD_b, 
       0                                       AS directCustom_c, 
       0                                       directCustom_b, 
       0                                       AS reimbCur_c, 
       0                                       reimbCur_b, 
       0                                       AS reimbYTD_c, 
       0                                       reimbYTD_b, 
       0                                       AS reimbJTD_c, 
       0                                       reimbJTD_b, 
       0                                       AS reimbCustom_c, 
       0                                       reimbCustom_b, 
       0                                       AS revCur_c, 
       0                                       AS revCur_b, 
       0                                       AS revYTD_c, 
       0                                       AS revYTD_b, 
       0                                       AS revJTD_c, 
       0                                       AS revJTD_b, 
       0                                       AS revCustom_c, 
       0                                       AS revCustom_b, 
       0.00                                    AS ubGLCur_c, 
       0.00                                    AS ubGLYTD_c, 
       0.00                                    AS ubGLJTD_c, 
       0.00                                    AS ubGLCustom_c, 
       0                                       althrsBud, 
       0                                       altamtBud_c, 
       0                                       altamtBud_b, 
       Sum(Isnull(pocommitment.amount, 0.0))   AS poAmt_c, 
       Sum(Isnull(pocommitment.billext, 0))    AS poAmt_b, 
       0.0                                     rppctcmpcalc, 
       0.00                                    targetMult_c, 
       0.00                                    targetMult_b, 
       0.00                                    rpfee_c, 
       0.00                                    rpfee_b, 
       0.00                                    rpconsultfee_c, 
       0.00                                    rpconsultfee_b, 
       0.00                                    rpreimballow_c, 
       0.00                                    rpreimballow_b, 
       ''''                                      AS currencyCodeProj, 
       ''''                                      AS currencyCodeBill, 
       ''''                                      AS currencyCodeFunct, 
       ''''                                      AS currencyCodePres_Cost, 
       ''''                                      AS currencyCodePres_Bill, 
       ''''                                      AS currencyCodePres_ARFee, 
       ''''                                      AS currencyCode_Opportunity, 
       0                                       AS currencyCodeProjCount, 
       0                                       AS currencyCodeBillCount, 
       0                                       AS currencyCodeFunctCount, 
       0                                       AS currencyCodePresCount_Cost, 
       0                                       AS currencyCodePresCount_Bill, 
       0                                       AS currencyCodePresCount_ARFee, 
       0                                       AS currencyCodeCount_Opportunity, 
       Min(pr.wbs1)                            WBS1, 
       Min(LEVEL2.wbs2)                        WBS2, 
       Min(LEVEL3.wbs3)                        WBS3, 
       ''''                                      w1Name, 
       ''''                                      w2Name, 
       ''''                                      w3Name, 
       ''''                                      ChargeType, 
       Max(pr.chargetype)                      ChargeTypeCode, 
       Min(pr.sublevel)                        SubLevel1, 
       Min(LEVEL2.sublevel)                    SubLevel2, 
       Min(LEVEL3.sublevel)                    SubLevel3, 
       ''''                                      w1principal, 
       ''''                                      w2principal, 
       ''''                                      w3principal, 
       ''''                                      w1prinName, 
       ''''                                      w2prinName, 
       ''''                                      w3prinName, 
       ''''                                      w1projMgr, 
       ''''                                      w2projMgr, 
       ''''                                      w3projMgr, 
       ''''                                      w1prgName, 
       ''''                                      w2prgName, 
       ''''                                      w3prgName, 
       ''''                                      w1supervisor, 
       ''''                                      w2supervisor, 
       ''''                                      w3supervisor, 
       ''''                                      w1supName, 
       ''''                                      w2supName, 
       ''''                                      w3supName, 
       ''''                                      PRAddress1, 
       ''''                                      PRAddress2, 
       ''''                                      PRAddress3, 
       ''''                                      PRCity, 
       ''''                                      PRState, 
       ''''                                      PRZip, 
       ''''                                      PRCounty, 
       ''''                                      PRCountry, 
       ''''                                      PRPhone, 
       ''''                                      PRFax, 
       ''''                                      PREmail, 
       ''''                                      ClientID, 
       ''''                                      ClientName, 
       ''''                                      CLAddress, 
       ''''                                      CLAddress1, 
       ''''                                      CLAddress2, 
       ''''                                      CLAddress3, 
       ''''                                      CLAddress4, 
       ''''                                      CLCity, 
       ''''                                      CLState, 
       ''''                                      CLZip, 
       ''''                                      CLCountry, 
       ''''                                      CLPhone, 
       ''''                                      CLFax, 
       ''''                                      CLEMail, 
       ''''                                      w1Org, 
       ''''                                      w2Org, 
       ''''                                      w3Org, 
       ''''                                      w1OrgName, 
       ''''                                      w2OrgName, 
       ''''                                      w3OrgName, 
       ''''                                      AS w1TLProjectName, 
       ''''                                      AS w2TLProjectName, 
       ''''                                      AS w3TLProjectName, 
       ''''                                      ServiceProfileCode, 
       ''''                                      ServiceProfileDesc, 
       0                                       mult, 
       0                                       mult_sum, 
       0                                       fee_c, 
       0                                       fee_b, 
       0                                       consultFee_c, 
       0                                       consultFee_b, 
       0                                       reimbAllow_c, 
       0                                       reimbAllow_b, 
       0.0                                     feeDirectLabor, 
       0.0                                     feeDirectExpense, 
       0.0                                     reimbAllowExpense, 
       0.0                                     reimbAllowConsultant, 
       0                                       contractfee, 
       0                                       contractconsultFee, 
       0                                       contractreimbAllow, 
       0.0                                     contractFeeDirectLabor, 
       0.0                                     contractFeeDirectExpense, 
       0.0                                     contractReimbAllowExpense, 
       0.0                                     contractReimbAllowConsultant, 
       0                                       BudOHRate, 
       ''''                                      w1Status, 
       ''''                                      w2Status, 
       ''''                                      w3Status, 
       ''''                                      w1StatusDesc, 
       ''''                                      w2StatusDesc, 
       ''''                                      w3StatusDesc, 
       ''''                                      wbs1RevType, 
       ''''                                      wbs2RevType, 
       ''''                                      wbs3RevType, 
       ''''                                      wbs1RevTypeDesc, 
       ''''                                      wbs2RevTypeDesc, 
       ''''                                      wbs3RevTypeDesc, 
       ''''                                      UnitTable, 
       NULL                                    w1StartDate, 
       NULL                                    w2StartDate, 
       NULL                                    w3StartDate, 
       NULL                                    w1EndDate, 
       NULL                                    w2EndDate, 
       NULL                                    w3EndDate, 
       0.00                                    w1PctComp, 
       0.00                                    w2PctComp, 
       0.00                                    w3PctComp, 
       0.00                                    w1LabPctComp, 
       0.00                                    w2LabPctComp, 
       0.00                                    w3LabPctComp, 
       0.00                                    w1ExpPctComp, 
       0.00                                    w2ExpPctComp, 
       0.00                                    w3ExpPctComp, 
       ''''                                      BillByDefault, 
       ''''                                      BillableWarning, 
       ''''                                      BudgetedFlag, 
       ''''                                      BudgetSource, 
       ''''                                      BudgetLevel, 
       ''''                                      BudgetedLevels, 
       ''''                                      XCharge, 
       ''''                                      XChargeMethod, 
       0                                       XChargeMult, 
       ''''                                      ContactID, 
       ''''                                      ContactFirstName, 
       ''''                                      ContactLastName, 
       ''''                                      CLBillingAddr, 
       ''''                                      w1LongName, 
       ''''                                      w2LongName, 
       ''''                                      w3LongName, 
       ''''                                      CLBAddress1, 
       ''''                                      CLBAddress2, 
       ''''                                      CLBAddress3, 
       ''''                                      CLBAddress4, 
       ''''                                      CLBCity, 
       ''''                                      CLBState, 
       ''''                                      CLBZip, 
       ''''                                      CLBCountry, 
       ''''                                      CLBPhone, 
       ''''                                      CLBFax, 
       ''''                                      CLBEMail, 
       ''''                                      w1FederalInd, 
       ''''                                      w2FederalInd, 
       ''''                                      w3FederalInd, 
       ''''                                      w1ProjectType, 
       ''''                                      w2ProjectType, 
       ''''                                      w3ProjectType, 
       ''''                                      w1projectTypeDesc, 
       ''''                                      w2projectTypeDesc, 
       ''''                                      w3projectTypeDesc, 
       ''''                                      w1Responsibility, 
       ''''                                      w2Responsibility, 
       ''''                                      w3Responsibility, 
       ''''                                      w1ResponsibilityDesc, 
       ''''                                      w2ResponsibilityDesc, 
       ''''                                      w3ResponsibilityDesc, 
       ''''                                      w1Referable, 
       ''''                                      w2Referable, 
       ''''                                      w3Referable, 
       NULL                                    w1EstCompletionDate, 
       NULL                                    w2EstCompletionDate, 
       NULL                                    w3EstCompletionDate, 
       NULL                                    w1ActCompletionDate, 
       NULL                                    w2ActCompletionDate, 
       NULL                                    w3ActCompletionDate, 
       NULL                                    w1ContractDate, 
       NULL                                    w2ContractDate, 
       NULL                                    w3ContractDate, 
       NULL                                    w1ConstComplDate, 
       NULL                                    w2ConstComplDate, 
       NULL                                    w3ConstComplDate, 
       NULL                                    w1ProfServicesComplDate, 
       NULL                                    w2ProfServicesComplDate, 
       NULL                                    w3ProfServicesComplDate, 
       NULL                                    w1BidDate, 
       NULL                                    w2BidDate, 
       NULL                                    w3BidDate, 
       ''''                                      w1ComplDateComment, 
       ''''                                      w2ComplDateComment, 
       ''''                                      w3ComplDateComment, 
       0                                       w1FirmCost, 
       0                                       w2FirmCost, 
       0                                       w3FirmCost, 
       ''''                                      w1FirmCostComment, 
       ''''                                      w2FirmCostComment, 
       ''''                                      w3FirmCostComment, 
       0                                       w1TotalProjectCost, 
       0                                       w2TotalProjectCost, 
       0                                       w3TotalProjectCost, 
       ''''                                      w1TotalCostComment, 
       ''''                                      w2TotalCostComment, 
       ''''                                      w3TotalCostComment, 
       ''''                                      ClientConfidential, 
       ''''                                      ClientAlias, 
       ''''                                      BillingClientID, 
       ''''                                      BillClientName, 
       ''''                                      BillingContactID, 
       ''''                                      billContactLastName, 
       ''''                                      billContactFirstName, 
       ''''                                      ProposalWBS1, 
       ''''                                      w1CreateUser, 
       ''''                                      w2CreateUser, 
       ''''                                      w3CreateUser, 
       NULL                                    w1CreateDate, 
       NULL                                    w2CreateDate, 
       NULL                                    w3CreateDate, 
       ''''                                      w1ModUser, 
       ''''                                      w2ModUser, 
       ''''                                      w3ModUser, 
       NULL                                    w1ModDate, 
       NULL                                    w2ModDate, 
       NULL                                    w3ModDate, 
       ''''                                      w1CostRateMeth, 
       ''''                                      w2CostRateMeth, 
       ''''                                      w3CostRateMeth, 
       ''''                                      w1CostRateTableNo, 
       ''''                                      w3CostRateTableNo, 
       ''''                                      w3CostRateTableNo, 
       ''''                                      w1PayRateMeth, 
       ''''                                      w2PayRateMeth, 
       ''''                                      w3PayRateMeth, 
       ''''                                      w1PayRateTableNo, 
       ''''                                      w2PayRateTableNo, 
       ''''                                      w3PayRateTableNo, 
       ''''                                      w1Locale, 
       ''''                                      w1Locale, 
       ''''                                      w1Locale, 
       ''''                                      w1LineItemApproval, 
       ''''                                      w2LineItemApproval, 
       ''''                                      w3LineItemApproval, 
       ''''                                      w1LineItemApprovalEK, 
       ''''                                      w2LineItemApprovalEK, 
       ''''                                      w3LineItemApprovalEK, 
       ''''                                      opp_name, 
       ''''                                      opp_number, 
       ''''                                      opp_prproposalwbs1, 
       ''''                                      opp_prproposalname, 
       ''''                                      opp_org, 
       ''''                                      opp_orgname, 
       ''''                                      opp_clname, 
       ''''                                      opp_contactname, 
       ''''                                      opp_projmgrname, 
       ''''                                      opp_principalname, 
       ''''                                      opp_supervisorname, 
       0                                       opp_revenue, 
       ''''                                      opp_probability, 
       0                                       opp_weightedrevenue, 
       NULL                                    opp_opendate, 
       NULL                                    opp_closedate, 
       ''''                                      opp_daysopen, 
       ''''                                      opp_stage, 
       ''''                                      opp_type, 
       ''''                                      opp_source, 
       ''''                                      opp_status, 
       NULL                                    opp_eststartdate, 
       NULL                                    opp_estcompletiondate, 
       ''''                                      opp_address1, 
       ''''                                      opp_address2, 
       ''''                                      opp_address3, 
       ''''                                      opp_city, 
       ''''                                      opp_state, 
       ''''                                      opp_zip, 
       ''''                                      opp_country, 
       ''''                                      opp_createuser, 
       NULL                                    opp_createdate, 
       ''''                                      opp_moduser, 
       NULL                                    opp_moddate, 
       ''''                                      opp_oppdesc 
FROM   pr AS LEVEL3 
       LEFT JOIN projectcustomtabfields 
              ON LEVEL3.wbs1 = projectcustomtabfields.wbs1 
                 AND LEVEL3.wbs2 = projectcustomtabfields.wbs2 
                 AND LEVEL3.wbs3 = projectcustomtabfields.wbs3 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes1 
              ON Substring(LEVEL3.org, 1, 2) = groupCFGOrgCodes1.code 
                 AND groupCFGOrgCodes1.orglevel = 1 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes3 
              ON Substring(LEVEL3.org, 7, 3) = groupCFGOrgCodes3.code 
                 AND groupCFGOrgCodes3.orglevel = 3, 
       pr AS LEVEL2, 
       pr, 
       ##rwl_' + @SessionId + ', 
       pocommitment 
       LEFT JOIN pomaster 
              ON pomaster.masterpkey = pocommitment.masterpkey 
       LEFT JOIN ca 
              ON ca.account = pocommitment.account 
       LEFT JOIN ve 
              ON ve.vendor = pomaster.vendor 
WHERE  ( ( pocommitment.wbs1 = LEVEL3.wbs1 
           AND pocommitment.wbs2 = LEVEL3.wbs2 
           AND pocommitment.wbs3 = LEVEL3.wbs3 ) 
         AND ( pocommitment.wbs1 = LEVEL2.wbs1 
               AND pocommitment.wbs2 = LEVEL2.wbs2 ) 
         AND ( pocommitment.wbs1 = pr.wbs1 ) 
         AND ( pr.wbs2 = '' '' 
               AND pr.wbs3 = '' '' 
               AND LEVEL2.wbs3 = '' '' ) ) 
       AND (( (( ( pr.wbs1 = ''NC1008.000'' ) 
					OR ( pr.wbs1 = ''NC1010.000'' ) 
					OR ( pr.wbs1 = ''NC1010.001'' )  
					OR ( pr.wbs1 = ''NC1010.002'' )  
					OR ( pr.wbs1 = ''NC1010.003'' )  
					OR ( pr.wbs1 = ''NC1011.001'' )  
					OR ( pr.wbs1 = ''NC3008.000'' ) 
					OR ( pr.wbs1 = ''NC3010.000'' ) 
					OR ( pr.wbs1 = ''NC3010.001'' )  
					OR ( pr.wbs1 = ''NC3010.002'' )  
					OR ( pr.wbs1 = ''NC3010.003'' )  
					OR ( pr.wbs1 = ''NC3011.001'' )  
				  )) 
               OR ( ( pr.wbs1 LIKE ''P%'' ) 
                    AND EXISTS (SELECT ''x'' 
                                FROM   cfgchargetype 
                                WHERE  cfgchargetype.type = pr.chargetype 
                                       AND (( cfgchargetype.label =  
                                              ''Promotional'' 
                                            ))) 
                  ) )) 
       AND LEVEL3.wbs1 = ##rwl_' + @SessionId + '.wbs1 
       AND ##rwl_' + @SessionId + '.wbs2 = '' '' 
       AND ##rwl_' + @SessionId + '.wbs3 = '' '' 
       AND ##rwl_' + @SessionId + '.sessionid = 
           ''' + @SessionId + ''' 
       AND pr.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs1 = LEVEL3.wbs1 
       AND LEVEL2.wbs2 = LEVEL3.wbs2 
       AND pr.wbs2 = '''' 
       AND pr.wbs3 = '''' 
       AND LEVEL2.wbs3 = '''' 
GROUP  BY Isnull(Substring(LEVEL3.org, 1, 2), ''''), 
          Isnull(Substring(LEVEL3.org, 7, 3), ''''), 
          Isnull(pr.wbs1, ''''), 
          CASE 
            WHEN ca.type < 7 THEN ''5'' 
            WHEN ca.type >= 7 
                 AND ca.type < 9 THEN ''3'' 
            WHEN ca.type >= 9 
                 AND ca.type < 10 THEN ''6'' 
            ELSE ''7'' 
          END, 
          CASE 
            WHEN ( ca.type = 6 ) 
                  OR ( ca.type = 8 ) THEN ''0'' 
            ELSE ''1'' 
          END 
ORDER  BY group1, 
          group2, 
          group3, 
          recordtype, 
          subtype
		  
OPTION (RECOMPILE)' 

DECLARE @ParmDefinition NVARCHAR(MAX) = N'@MinPeriod INT, @Period INT'; 
EXECUTE sp_executesql @sql, @ParmDefinition, @MinPeriod, @Period

END
GO
