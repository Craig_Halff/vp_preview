SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteUDICData]
  @strUDICName Nvarchar(32),
  @strUDICPK varchar(32)
AS

BEGIN -- Procedure DeleteUDICData

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to delete a Plan.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  
  DECLARE @strSQL nvarchar(max)
  DECLARE @strTableName nvarchar(128)
  DECLARE @strPKColName nvarchar(128)
  DECLARE @STR_NonGrid varchar(1)
  
  -- Determine the PK Column Name for the Primary table.
  -- The Primary table has Table_Name = UDIC_ID
  
  SELECT @strPKColName = tc.name
    FROM 
    sys.schemas s 
    inner join sys.tables t on s.schema_id=t.schema_id
    inner join sys.indexes i on t.object_id=i.object_id
    inner join sys.index_columns ic on i.object_id=ic.object_id and i.index_id=ic.index_id
    inner join sys.columns tc on ic.object_id=tc.object_id and ic.column_id=tc.column_id
    where i.is_primary_key=1 and t.name=@strUDICName

  -- Declare Cursor to traverse list of UDIC tables for the given UDIC.
  -- Need to delete the Grid tables first before the main UDIC table.
  -- Need to use Cursor to guarantee the sort sequence when tables are deleted.
  
  DECLARE curTableList CURSOR FOR
    SELECT Name AS Table_Name
      FROM sys.tables  
      WHERE Name LIKE '%' + @strUDICName + '%'  
      ORDER BY Name DESC  
      
  -->>> Loop through the list of tables.
  
  OPEN curTableList;

  FETCH NEXT FROM curTableList INTO @strTableName

  WHILE @@FETCH_STATUS = 0            
    BEGIN
      SET @strSQL = 'DELETE [' + @strTableName + '] WHERE [' + @strPKColName + '] = ''' + @strUDICPK + ''''
      EXEC(@strSQL)
      SET @strSQL = ''
	  FETCH NEXT FROM curTableList INTO @strTableName
    END -- End While

   CLOSE curTableList;
   DEALLOCATE curTableList;  
  
  --<<< End Loop.

  SET NOCOUNT OFF

END -- DeleteUDICData
GO
