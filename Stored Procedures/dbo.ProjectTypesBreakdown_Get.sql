SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ProjectTypesBreakdown_Get]
	@WBS1	NVARCHAR(30)
AS
BEGIN
  ------------------------------------------------------------------------
  SET NOCOUNT ON;

  ------------------------------------------------------------------------
SELECT
	PTB.WBS1
	,PTB.WBS2
	,PTB.WBS3
	,PTB.Seq
	,PTB.CreateUser
	,PTB.CreateDate
	,PTB.ModUser
	,PTB.ModDate
	,PTB.CustBreakdownProjectType
	,PTB.CustBreakdownFee
	,PTB.CustBreakdownUnitType
	,PTB.CustBreakdownUnitValue
	,PT.CustomCurrencyCode
    ,PT.CustProjectTypeCode
    ,PT.CustProjectTypeName
    ,PT.CustProjectTypeCategory
    ,PT.CustProjectTypeSubCategory
  FROM
    dbo.Projects_ProjectTypesBreakdown AS PTB
	INNER JOIN dbo.UDIC_ProjectType AS PT
		ON PT.UDIC_UID = PTB.CustBreakdownProjectType
  WHERE
	PTB.WBS1 = @WBS1
		AND PTB.WBS2 = ' '
		AND PTB.WBS3 = ' '
  ORDER BY
    PTB.CustBreakdownProjectType    ASC
  ------------------------------------------------------------------------
END
GO
