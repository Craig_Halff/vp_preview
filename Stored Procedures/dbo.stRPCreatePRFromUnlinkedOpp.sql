SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPCreatePRFromUnlinkedOpp]
AS

BEGIN -- Procedure stRPCreatePRFromUnlinkedOpp

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- The purpose of this stored procedure is to create project from unlinked opportunities. 
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  DECLARE @strOpportunityID varchar(32)
  DECLARE @strOpportunity nvarchar(30) /* aka Opportunity Number */
  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @intOutlineLevel int
  DECLARE @intWBS1SeqNo as int = 0
  DECLARE @intWBS2SeqNo as int
  DECLARE @intWBS3SeqNo as int
  DECLARE @strWBS1 as nvarchar(30)
  DECLARE @strWBS2 as nvarchar(30)
  DECLARE @strWBS3 as nvarchar(30)    
  DECLARE @intChildrenCount as int
  
  DECLARE @strDefaultFunctionCurrencyCode as nvarchar(3)  
  DECLARE @ReportAtBillingInBillingCurr as varchar(1)
  DECLARE @ServiceEst as varchar(1)

  /*Following for the use of creating formated new WBS1 */
  declare
  @NewWBS1                  nvarchar(30),
  @WBS1Length               smallint,
  @WBS1LeadZeros            varchar(1),
  @WBS1Delimiter1           nvarchar(1),
  @WBS1Delimiter1Position   smallint,
  @WBS1Delimiter2           nvarchar(1),
  @WBS1Delimiter2Position   smallint

  /*Following used to decide the company of the opportunity Company*/
  DECLARE @siOrg1Start smallint
  DECLARE @siOrg1Length smallint
  DECLARE @strCompany nvarchar(14)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strRevenueMethod nvarchar(1)
  DECLARE @strRevByCategoryEnabled nvarchar(1)
  DECLARE @nRevenueCategories int 


  declare @strSeq varchar(10)

  select 
    @siOrg1Start = Org1Start, 
    @siOrg1Length = Org1Length,
    @WBS1Length = WBS1Length,
    @WBS1LeadZeros = WBS1LeadZeros,
    @WBS1Delimiter1 = WBS1Delimiter1,
    @WBS1Delimiter1Position = WBS1Delimiter1Position,
    @WBS1Delimiter2 = WBS1Delimiter2,
    @WBS1Delimiter2Position = WBS1Delimiter2Position
  from CFGFormat


  SET NOCOUNT ON

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
--loop through all opportunities with no link to project

select @strDefaultFunctionCurrencyCode = FunctionalCurrencyCode from CFGMainData 
  Inner Join CFGRMSettings ON CFGMainData.Company = CFGRMSettings.DefaultCompany

SELECT @ReportAtBillingInBillingCurr = ReportAtBillingInBillingCurr FROM FW_CFGSystem

SELECT @ServiceEst = EstServiceFlg FROM CFGPREstService

-- An Opportunity could be linked 2 different Projects:
-- 1. Regular Project: PR.ChargeType = 'R' AND O.OpportunityID = PR.OpportunityID AND O.PRWBS1 = PR.WBS1.
-- 2. Proposal Project: PR.ChargeType = 'P' AND O.OpportunityID = PR.OpportunityID AND O.PRProposalWBS1 = PR.WBS1.
-- Furthermore, an Opportunity could be linked to no Project, to one Project, or two Projects.

-- For each Opportunity, if it is not linked to a Regular Project, 
-- then we need to create a new Regular Project to represent the Opportunity in the new world of Vantagepoint
-- where there is no longer Opportunities but only Projects.

DECLARE noLinkOppCursor CURSOR FOR 
SELECT DISTINCT OpportunityID, PlanID, Opportunity FROM Opportunity WHERE OpportunityID NOT IN
  (SELECT DISTINCT OpportunityID FROM PR WHERE WBS2 = ' ' and WBS3 = ' ' AND OpportunityID IS NOT NULL AND ChargeType = 'R')

if exists (select 'x' from sys.objects where name = 'PRUnlinked' and type = 'U')
  drop table dbo.PRUnlinked
select WBS1, WBS2, WBS3, Name, LongName, Status, Stage, ChargeType, SubLevel, AvailableForCRM, ReadyForApproval, ReadyForProcessing, 
       ProjectCurrencyCode, BillingCurrencyCode, RevType,OpportunityID, CreateUser, CreateDate, ModUser, ModDate
  into dbo.PRUnlinked from PR where WBS1 is NULL

BEGIN
OPEN noLinkOppCursor
  FETCH NEXT FROM noLinkOppCursor INTO @strOpportunityID, @strPlanID, @strOpportunity
  WHILE @@FETCH_STATUS = 0 
  BEGIN

  -- Defect #1073045, 2019-02-19, Luong Vu.
  -- If @strOpportunity is a valid WBS1 Number, then use @strOpportunity as PR.WBS1 of the newly created Project.
  -- Else, create a new Z-Number to be used as PR.WBS1 of the newly created Project.

  IF(
    dbo.PR$ValidateWBS1(@strOpportunity) = 1 AND
    NOT EXISTS(SELECT 'X' FROM dbo.PRUnlinked WHERE WBS1 = @strOpportunity AND WBS2 = ' ' AND WBS3 = ' ')
  )
    BEGIN
      SET @NewWBS1 = @strOpportunity
    END /* IF(dbo.PR$ValidateWBS1(@strOpportunity) = 1) */

  ELSE /* ELSE IF(dbo.PR$ValidateWBS1(@strOpportunity) = 0) */
    BEGIN

      CREATEWBS1: 
    -- Increment counter.
      SET @intWBS1SeqNo = @intWBS1SeqNo + 1

      --first create WBS1 Number
      SET @strSeq = RIGHT('0000' + CONVERT(VARCHAR(4), dbo.DLTK$DecToBase36(@intWBS1SeqNo, 36)), 4) 

      if @WBS1LeadZeros = 'N' or @WBS1Delimiter1Position = 0
        set @NewWBS1 = 'Z' + right(replicate('0',30) +  @strSeq, @WBS1Length-1)

      if @WBS1LeadZeros = 'Y'
        begin
        if @WBS1Delimiter1Position <> 0 and  @WBS1Delimiter2Position = 0
        begin
          set @NewWBS1 = 'Z' + right(replicate('0',30) + @strSeq, @WBS1Length-2)
          set @NewWBS1 = left(@NewWBS1,@WBS1Delimiter1Position-1) + @WBS1Delimiter1 + right(@NewWBS1,@WBS1Length-@WBS1Delimiter1Position)
          --print @NewWBS1
        end
        if @WBS1Delimiter1Position <> 0 and  @WBS1Delimiter2Position <> 0
        begin
          set @NewWBS1 = 'Z' + right(replicate('0',30) + @strSeq, @WBS1Length-3)
          set @NewWBS1 = left(@NewWBS1,@WBS1Delimiter1Position-1) + @WBS1Delimiter1 + substring(@NewWBS1,@WBS1Delimiter1Position,@WBS1Delimiter2Position-@WBS1Delimiter1Position-1)
          + @WBS1Delimiter2 + right(@NewWBS1,@WBS1Length-@WBS1Delimiter2Position)
          --print @NewWBS1
        end
        end
    IF (SELECT COUNT(*) FROM PR WHERE WBS1 = @NewWBS1) > 0 GOTO CREATEWBS1 -- IF WBS1 EXISTS, INCREMENT AND TRY AGAIN

    END /* END ELSE IF(dbo.PR$ValidateWBS1(@strOpportunity) = 0) */

    SET @strWBS1 = @NewWBS1

  /*Following determine what revenue method for the newly created project*/
  SELECT @strMultiCompanyEnabled = MultiCompanyEnabled FROM FW_CFGSystem

  Select    @strCompany = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN COALESCE(CM.Company, '#@$') ELSE ' ' END
    FROM Opportunity AS O 
      LEFT JOIN (
        SELECT DISTINCT Company FROM CFGMainData
      ) AS CM
      ON SUBSTRING(O.Org, @siOrg1Start, @siOrg1Length) = CM.Company
    WHERE O.OpportunityID = @strOpportunityID

  Set @strRevenueMethod = 'N'

  Select @strRevByCategoryEnabled =  RevByCategoryEnabled, @nRevenueCategories = RevenueCategories
  From CFGMainData Where Company = @strCompany

  if @strRevByCategoryEnabled = 'N' 
  Begin
  IF Exists ( SELECT 'X' FROM CFGRGMethodsData  WHERE Active = 'Y' And Method = 'B' )
  Begin
    Set @strRevenueMethod = 'B'
  End
  End

  if @strRevByCategoryEnabled = 'Y' And @nRevenueCategories = 1
  Begin
  IF Exists ( SELECT 'X' FROM CFGRGMethodsData  WHERE Active = 'Y' And Method = 'B' And Category = 1 )
  Begin
    Set @strRevenueMethod = 'B'
  End
  End

    if @strPlanID is NULL
    BEGIN	   
      INSERT INTO PRUnlinked (
        WBS1,
        WBS2,
        WBS3,
        Name,
        LongName,
        Status,
        Stage,
        ChargeType,
        SubLevel,
        AvailableForCRM,
        ReadyForApproval,
        ReadyForProcessing,
        ProjectCurrencyCode,
        BillingCurrencyCode,
        RevType,
        CreateUser,
        CreateDate,
        ModUser,
        ModDate)
      SELECT 
        @strWBS1 AS WBS1,
        ' ' AS WBS2,
        ' ' AS WBS3,
        LEFT(O.NAME,40) AS Name,
        O.NAME AS LongName,
        ISNULL(O.Status,'A') AS Status,
        O.Stage AS Stage,
        'R' AS ChargeType,
        'N' AS SubLevel,
        'Y' AS AvailableForCRM,
        'Y' AS ReadyForApproval,
        'N' AS ReadyForProcessing,
        Case When @ServiceEst = 'Y' then ISNULL(O.OppCurrencyCode, @strDefaultFunctionCurrencyCode)     				   				     
        Else ISNULL(O.CustomCurrencyCode, @strDefaultFunctionCurrencyCode)   End As ProjectCurrencyCode,
        Case When @ServiceEst = 'Y' then   
          Case When @ReportAtBillingInBillingCurr = 'Y' then  ISNULL(O.Billingcurrencycode, @strDefaultFunctionCurrencyCode)  
          Else ISNULL(O.OppCurrencyCode, @strDefaultFunctionCurrencyCode)  End
        Else ISNULL(O.CustomCurrencyCode, @strDefaultFunctionCurrencyCode)   End As BillingCurrencyCode,	
        @strRevenueMethod,			  				  
        'DPS20UPDPR' AS CreateUser,
        GETUTCDATE() AS CreateDate,
        'DPS20UPDPR' AS ModUser,
        GETUTCDATE() AS ModDate  
        FROM Opportunity O  Where O.OpportunityID = @strOpportunityID
      END --strPlanID is null
      ELSE --@strPlanID is not null
      BEGIN
      SET @intWBS2SeqNo = 1
      SET @intWBS3SeqNo = 1
  
      DECLARE noTaskCursor CURSOR FOR 	  
      SELECT OutlineLevel,ChildrenCount,TaskID from RPTask WHERE PlanID =  @strPlanID Order By OutlineNumber
        
      BEGIN
      OPEN noTaskCursor
        FETCH NEXT FROM noTaskCursor INTO @intOutlineLevel,@intChildrenCount,@strTaskID
        WHILE @@FETCH_STATUS = 0 
        BEGIN
          if @intOutlineLevel = 0
             begin
            set @strWBS2 = ' '
            set @strWBS3 = ' '
             end 
          if @intOutlineLevel = 1
             begin
            set @strWBS2 = RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(@intWBS2SeqNo, 36)), 3)  
            set @strWBS3 = ' '
            set @intWBS2SeqNo = @intWBS2SeqNo + 1
             end 
          if @intOutlineLevel = 2
             begin
            set @strWBS3 = RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(@intWBS3SeqNo, 36)), 3)  
            set @intWBS3SeqNo = @intWBS3SeqNo + 1
             end 

             -- insert a new record in PR table
              INSERT INTO PRUnlinked (WBS1,
                  WBS2,
                  WBS3,
                  Name,
                  LongName,
                  Status,
                  Stage,
                  ChargeType,
                  SubLevel,
                  AvailableForCRM,
                  ReadyForApproval,
                  ReadyForProcessing,
                  ProjectCurrencyCode,
                  BillingCurrencyCode,
          RevType,		
                  CreateUser,
                  CreateDate,
                  ModUser,
                  ModDate)
          SELECT @strWBS1 AS WBS1,
               @strWBS2 AS WBS2,
               @strWBS3 AS WBS3,
               LEFT(T.NAME,40) AS Name,
               T.NAME AS LongName,
               ISNULL(O.Status,'A') AS Status,
               O.Stage AS Stage,
               'R' AS ChargeType,
               Case When @intChildrenCount > 0 Then 'Y' Else 'N' End AS SubLevel,
               'Y' AS AvailableForCRM,
               'Y' AS ReadyForApproval,
               'N' AS ReadyForProcessing,
               Case When @ServiceEst = 'Y' then ISNULL(O.OppCurrencyCode, @strDefaultFunctionCurrencyCode)     				   				     
               Else ISNULL(O.CustomCurrencyCode, @strDefaultFunctionCurrencyCode)   End As ProjectCurrencyCode,
               Case When @ServiceEst = 'Y' then   
                 Case When @ReportAtBillingInBillingCurr = 'Y' then  ISNULL(O.Billingcurrencycode, @strDefaultFunctionCurrencyCode)  
                 Else ISNULL(O.OppCurrencyCode, @strDefaultFunctionCurrencyCode)  End
               Else ISNULL(O.CustomCurrencyCode, @strDefaultFunctionCurrencyCode)   End As BillingCurrencyCode,			
         @strRevenueMethod,			  				  
               'DPS20UPDPR' AS CreateUser,
                GETUTCDATE() AS CreateDate,
                'DPS20UPDPR' AS ModUser,
                GETUTCDATE() AS ModDate  
                FROM RPTask T INNER JOIN Opportunity O On O.PlanID = T.PlanID
                Where T.TaskID = @strTaskID and O.OpportunityID = @strOpportunityID
        
        -- Set WBS1,WBS2,WBS3 in Task,Expense,Consultant,Assignment
        UPDATE PNTask set WBS1 = @strWBS1,
                    WBS2 = case when @strWBS2 = ' ' then null else @strWBS2 end,
                    WBS3 = case when @strWBS3 = ' ' then null else @strWBS3 end
             Where TaskID = @strTaskID and PlanID =  @strPlanID

        UPDATE RPTask set WBS1 = @strWBS1,
                    WBS2 = case when @strWBS2 = ' ' then null else @strWBS2 end,
                    WBS3 = case when @strWBS3 = ' ' then null else @strWBS3 end
             Where TaskID = @strTaskID and PlanID =  @strPlanID
        ----------------------------------
        UPDATE PNAssignment set WBS1 = @strWBS1,
                    WBS2 = case when @strWBS2 = ' ' then null else @strWBS2 end,
                    WBS3 = case when @strWBS3 = ' ' then null else @strWBS3 end
             Where TaskID = @strTaskID and PlanID =  @strPlanID

        UPDATE RPAssignment set WBS1 = @strWBS1,
                    WBS2 = case when @strWBS2 = ' ' then null else @strWBS2 end,
                    WBS3 = case when @strWBS3 = ' ' then null else @strWBS3 end
             Where TaskID = @strTaskID and PlanID =  @strPlanID

----------------------------------
        UPDATE PNExpense set WBS1 = @strWBS1,
                    WBS2 = case when @strWBS2 = ' ' then null else @strWBS2 end,
                    WBS3 = case when @strWBS3 = ' ' then null else @strWBS3 end
             Where TaskID = @strTaskID and PlanID =  @strPlanID

        UPDATE RPExpense set WBS1 = @strWBS1,
                    WBS2 = case when @strWBS2 = ' ' then null else @strWBS2 end,
                    WBS3 = case when @strWBS3 = ' ' then null else @strWBS3 end
             Where TaskID = @strTaskID and PlanID =  @strPlanID

----------------------------------
        UPDATE PNConsultant set WBS1 = @strWBS1,
                    WBS2 = case when @strWBS2 = ' ' then null else @strWBS2 end,
                    WBS3 = case when @strWBS3 = ' ' then null else @strWBS3 end
             Where TaskID = @strTaskID and PlanID =  @strPlanID

        UPDATE RPConsultant set WBS1 = @strWBS1,
                    WBS2 = case when @strWBS2 = ' ' then null else @strWBS2 end,
                    WBS3 = case when @strWBS3 = ' ' then null else @strWBS3 end
             Where TaskID = @strTaskID and PlanID =  @strPlanID

        FETCH NEXT FROM noTaskCursor INTO @intOutlineLevel,@intChildrenCount,@strTaskID

        END
        END
        CLOSE noTaskCursor
        DEALLOCATE noTaskCursor	
        
        ---- need to link the existing plan to the new project		 
        UPDATE PNPlan Set WBS1 = @strWBS1, OpportunityID = NULL Where PlanID = @strPlanID
        UPDATE RPPlan Set WBS1 = @strWBS1, OpportunityID = NULL Where PlanID = @strPlanID
      END --@strPlanID is NOT null

      UPDATE Opportunity SET PRWBS1 = @strWBS1
      Where OpportunityID = @strOpportunityID

      UPDATE PRUnlinked Set OpportunityID = @strOpportunityID Where WBS1 = @strWBS1

  FETCH NEXT FROM noLinkOppCursor INTO @strOpportunityID, @strPlanID, @strOpportunity

  END
  END
  CLOSE noLinkOppCursor
  DEALLOCATE noLinkOppCursor

  Exec('INSERT INTO PR (
    WBS1,
    WBS2,
    WBS3,
    Name,
    LongName,
    Status,
    Stage,
    ChargeType,
    SubLevel,
    AvailableForCRM,
    ReadyForApproval,
    ReadyForProcessing,
    ProjectCurrencyCode,
    BillingCurrencyCode,
    RevType,		
    OpportunityID,
    CreateUser,
    CreateDate,
    ModUser,
    ModDate
  )
    SELECT 
      WBS1,
      WBS2,
      WBS3,
      Name,
      LongName,
      Status,
      Stage,
      ChargeType,
      SubLevel,
      AvailableForCRM,
      ReadyForApproval,
      ReadyForProcessing,
      ProjectCurrencyCode,
      BillingCurrencyCode,
      RevType,
      OpportunityID,
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
      FROM PRUnlinked')

  COMMIT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF
  END
GO
