SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[HAI_MktCampaign_AdminQualCreate] @WBS1 AS VARCHAR(12)
AS
    /* 
	Copyright (c) 2021 Halff Associates, Inc.  All rights reserved.
	8/24/2021	Craig H. Anderson

	Create a Marketing Campaign for an AQ project that has been won.
	*/
    BEGIN
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
        DECLARE @CampaignID VARCHAR(32) = REPLACE(NEWID(), '-', '');
        IF NOT EXISTS (SELECT 1 FROM dbo.MktCampaign WHERE WBS1 = @WBS1)
            BEGIN
                /**********************************************************
					Create MktCampaign record
				***********************************************************/
                BEGIN TRY
                    INSERT INTO dbo.MktCampaign (
                        CampaignID
                      , Name
                      , Number
                      , Description
                      , Org
                      , Type
                      , Budget
                      , ActualCost
                      , CampaignMgr
                      , MktgMgr
                      , LaunchDate
                      , EndDate
                      , WBS1
                      , WBS2
                      , WBS3
                      , RecordStatus
                      , CreateUser
                      , CreateDate
                      , ModUser
                      , ModDate
                      , PotentialRevenue
                    )
                    SELECT @CampaignID -- CampaignID - varchar(32)
                         , px.CustMarketingName -- Name - nvarchar(100)
                         , @WBS1 -- Number - nvarchar(50)
                         , p.Memo -- Description - nvarchar(max)
                         , p.Org -- Org - nvarchar(30)
                         , '010' -- Type - nvarchar(10) - Administrative Qualification
                         , ISNULL(px2.CustJTDSpentBilling, 0) -- Budget - decimal(19, 4)
                         , ISNULL(px2.CustJTDSpentBilling, 0) -- ActualCost - decimal(19, 4)
                         , p.ProjMgr -- CampaignMgr - nvarchar(20)
                         , px.CustMarketingManager -- MktgMgr - nvarchar(20)
                         , p.EstStartDate -- LaunchDate - datetime
                         , p.EstCompletionDate -- EndDate - datetime
                         , @WBS1 -- WBS1 - nvarchar(30)
                         , ' ' -- WBS2 - nvarchar(30)
                         , ' ' -- WBS3 - nvarchar(30)
                         , 'A' -- RecordStatus - nvarchar(10)
                         , p.ModUser -- CreateUser - nvarchar(32)
                         , p.ModDate -- CreateDate - datetime
                         , p.ModUser -- ModUser - nvarchar(32)
                         , p.ModDate -- ModDate - datetime
                         , p.WeightedRevenue -- PotentialRevenue - decimal(19, 4)
                    FROM dbo.PR                                    AS p
                        INNER JOIN dbo.ProjectCustomTabFields      AS px
                            ON p.WBS1 = px.WBS1
                                AND p.WBS2 = px.WBS2
                        LEFT OUTER JOIN dbo.ProjectCustomTabFields AS px2
                            ON p.SiblingWBS1 = px2.WBS1
                                AND px2.WBS2 = ' '
                    WHERE
                        p.WBS1 = @WBS1
                        AND p.WBS2 = ' ';
                END TRY
                BEGIN CATCH
                    RAISERROR('Could not insert Marketing campaign.', 16, 1);
                END CATCH;

                /**********************************************************
					Create  MktCampaignCustomTabFields record
				***********************************************************/
                IF EXISTS (SELECT 1 FROM dbo.MktCampaign AS mc WHERE mc.CampaignID = @CampaignID)
                    BEGIN
                        INSERT INTO dbo.MktCampaignCustomTabFields (
                            CampaignID
                          , CreateUser
                          , CreateDate
                          , ModUser
                          , ModDate
                          , CustCampaignClient
                        )
                        SELECT @CampaignID
                             , p.ModUser
                             , p.ModDate
                             , p.ModUser
                             , p.ModDate
                             , p.ClientID
                        FROM dbo.PR AS p
                        WHERE
                            p.WBS1 = @WBS1
                            AND p.WBS2 = ' ';

                        /**********************************************************
							Alert Marketing	
						***********************************************************/
                        -- DECLARE @WBS1 VARCHAR(32)= '045558.001'
                        -- DECLARE @CampaignID VARCHAR(32) = 'CA6330224F984E4B9749471B3F79A0E7'
                        DECLARE @prjURL VARCHAR(255) = CONCAT('https://vantagepoint.halff.com/vantagepoint/app/#!Projects/view/project/overview/', @WBS1, '/presentation');
                        DECLARE @mcURL VARCHAR(255) = CONCAT('https://vantagepoint.halff.com/vantagepoint/app/#!MktCampaigns/view/0/0/', @CampaignID, '/presentation');

                        INSERT INTO dbo.CCG_Email_Messages (
                            SourceApplication
                          , Sender
                          , ToList
                          , CCList
                          , BCCList
                          , Subject
                          , GroupSubject
                          , Body
                          , MaxDelay
                        )
                        SELECT 'Vantagepoint'                                  AS SourceApplication -- SourceApplication - varchar(40)
                             , 'Deltek@Halff.com'                              AS Sender -- Sender - varchar(80)
                             , mm.EMail                                        AS ToList -- ToList - varchar(2000)
                             , NULL                                            AS CCList -- CCList - varchar(2000)
                             , NULL                                            AS BCCList -- BCCList - varchar(2000)
                             , CONCAT('AQ Campaign for ', c.Name, ' Created ') AS Subject -- Subject - varchar(120)
                             , NULL                                            AS GroupSubject -- GroupSubject - varchar(120)
                             , CONCAT(
                                   '<br><p>'
                                 , ISNULL(mm.PreferredName, mm.FirstName)
                                 , ',</p><p>A Marketing Campaign has been created automatically for the won Administrative Qualification: <a href="'
                                 , @prjURL
                                 , '">'
                                 , @WBS1
                                 , ': '
                                 , m.Name
                                 , '</a>.</p>'
                                 , '<table><tbody><tr><th scope="row">Client:</th><td>'
                                 , c.Name
                                 , '</td></tr>'
                                 , '<tr><th scope="row">Start: </th><td>'
                                 , CONVERT(VARCHAR(25), m.LaunchDate, 110)
                                 , '</td></tr>'
                                 , '<tr><th scope="row">End: </th><td>'
                                 , CONVERT(VARCHAR(25), m.EndDate, 110)
                                 , '</td></tr>'
                                 , '<tr><th scope="row">PM: </th><td>'
                                 , ISNULL(pm.PreferredName, pm.FirstName)
                                 , ' '
                                 , pm.LastName
                                 , ', Team '
                                 , RIGHT(pm.Org, 3)
                                 , '</td></tr>'
                                 , '</tbody></table>'
                                 , '<p>Please make your edits to the <a href="'
                                 , @mcURL
                                 , '">Marketing Campaign</a> as necessary.</p>'
                               )                                               AS Body -- Body - varchar(max)
                             , 1                                               AS MaxDelay -- MaxDelay - int
                        FROM dbo.MktCampaign                          AS m
                            INNER JOIN dbo.MktCampaignCustomTabFields AS mx
                                ON m.CampaignID = mx.CampaignID
                            INNER JOIN dbo.PR                         AS p
                                ON m.WBS1 = p.WBS1
                                    AND p.WBS2 = ' '
                            LEFT OUTER JOIN dbo.EM                    AS mm
                                ON m.MktgMgr = mm.Employee
                            LEFT OUTER JOIN dbo.EM                    AS pm
                                ON p.ProjMgr = pm.Employee
                            LEFT OUTER JOIN dbo.Clendor               AS c
                                ON mx.CustCampaignClient = c.ClientID
                        WHERE m.CampaignID = @CampaignID;
                    END;
            END;

        /**********************************************************
			Add the AQ Categories to the Mkt. Campaign
		***********************************************************/
        IF EXISTS (SELECT 1 FROM dbo.MktCampaign AS mc WHERE mc.CampaignID = @CampaignID)
            BEGIN
                INSERT INTO dbo.MktCampaigns_CustAdministrativeQualificationCategories (
                    CampaignID
                  , Seq
                  , CreateUser
                  , CreateDate
                  , ModUser
                  , ModDate
                  , CustAQCategory
                  , CustAQCategoryProjectsCount
                  , CustAQCategoryDescription
                  , CustSubmitStatus
                  , CustResult
                )
                SELECT @CampaignID -- CampaignID - varchar(32)
                     , REPLACE(NEWID(), '-', '') -- Seq - varchar(32)
                     , c.CreateUser
                     , GETDATE() -- CreateDate - datetime
                     , c.ModUser -- ModUser - varchar(32)
                     , GETDATE() -- ModDate - datetime
                     , c.CustAQCategory -- CustAQCategory - nvarchar(255)
                     , 0 -- CustAQCategoryProjectsCount - decimal(19, 5)
                     , c.CustAQCategoryDescription -- CustAQCategoryDescription - nvarchar(max)
                     , c.CustSubmitStatus -- CustSubmitStatus - nvarchar(255)
                     , c.CustResult -- CustResult - nvarchar(255)
                FROM dbo.Projects_CustAQCategories AS c
                WHERE c.WBS1 = @WBS1;
            END;
    END;
GO
