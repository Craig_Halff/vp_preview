SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyConvertWBSDisable]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg Nvarchar(1000) output,
@UserName Nvarchar(32) = 'VisionKeyConvert', -- Add by DP 4.0
@Period integer = 0,
@EntityType Nvarchar(20)
as
begin -- Procedure
	set nocount on
	SET ANSI_WARNINGS off
	declare @OldWBS1 			Nvarchar(30)
	declare @OldWBS2 			Nvarchar(30)
	declare @OldWBS3 			Nvarchar(30)
	declare @NewWBS1 			Nvarchar(30)
	declare @NewWBS2 			Nvarchar(30)
	declare @NewWBS3 			Nvarchar(30)
	declare @name				Nvarchar(40)
	Declare @Existing 		integer
	declare @Entity 			Nvarchar(20)
	declare @length 			integer
	declare @CvtType 			integer
	declare @KeysFetch 		integer
	declare @RetVal 			integer
	declare @MainWBS1	Nvarchar(30),
			@Invoice	Nvarchar(12),
			@WBS1	Nvarchar(30),
			@WBS2	Nvarchar(30),
			@WBS3	Nvarchar(30),
			@PKey	Nvarchar(32)
	declare @Errmsg2 			Nvarchar(1000),
			@custlabel		Nvarchar(40),
			@CustLabelProj 	Nvarchar(40),
			@custLabelProjPlural	Nvarchar(40),
			@CustLabelPhase 	Nvarchar(40),
			@custLabelPhasePlural	Nvarchar(40),
			@Custlabelplural 	Nvarchar(40)
	declare @NewChargeType 	Nvarchar(1)
	declare @OldChargeType 	Nvarchar(1)
	declare @newSubLevel 	Nvarchar(1)
	declare @oldSubLevel 	Nvarchar(1)
	declare @badProject	Nvarchar(1)
	declare @RevType Nvarchar(10)
	declare 	@ErrorNum		integer
	declare @diag integer
	declare @tmpWBS2 Nvarchar(30)
	declare @PlanID		varchar(32)
	declare @PlanFetch 	integer
	declare @TaskFetch 	integer
	declare @TaskFetch2 	integer
	declare @OldTaskID		varchar(32)
	declare @NewTaskID		varchar(32)
	declare @ResourceID		Nvarchar(20),
			@OldAssignmentID	Nvarchar(32),
			@NewAssignmentID	Nvarchar(32)
	set @ErrorNum = 0
	set @diag = 0
	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'WBSDisable'
	set @length = 30
	set @Existing = 0
	set @KeysFetch = 0

	execute InsertKeyCvtDriver 'WBS3'
	execute InsertKeyCvtDriver 'WBS2'

	delete from keyconvertdriver 
	where entity in (N'wbs2', N'wbs3') 
	and tableName + ',' + columnName in 
							(N'RPTask,WBS2',
							N'RPTask,WBS3',
							N'PNTask,WBS2',
							N'PNTask,WBS3')

-- Add by DP 4.0
	delete from keyconvertDriver where entity = N'WBS2' and TableName = N'PR' and ColumnName = N'WBS2'
	delete from keyconvertDriver where entity = N'wbs3' and TableName = N'PR' and ColumnName = N'WBS3'
	DECLARE @OldName Nvarchar(40)
	DECLARE @NewName Nvarchar(40)
	DECLARE @OldName2 Nvarchar(40)
	DECLARE @NewName2 Nvarchar(40)
	DECLARE @OldName3 Nvarchar(40)
	DECLARE @NewName3 Nvarchar(40)
	DECLARE @PKeyWork Nvarchar(50)
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0
--

/* new for 5.0 add checking for multi-company phase and task --- Start*/
	declare @test Nvarchar(4)
	declare @MulticompanyEnabled Nvarchar(1)
	select @MulticompanyEnabled = MulticompanyEnabled from FW_CFGSystem

/* new for 5.0 add checking for multi-company phase and task --- End*/
	select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'wbs3Label'
	select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'wbs3LabelPlural'
	select @custLabelPhase = LabelValue from FW_CFGLabels where LabelName = 'wbs2Label'
	select @custLabelPhasePlural = LabelValue from FW_CFGLabels where LabelName = 'wbs2LabelPlural'
	select @custLabelProj = LabelValue from FW_CFGLabels where LabelName = 'wbs1Label'
	select @custLabelProjPlural = LabelValue from FW_CFGLabels where LabelName = 'wbs1LabelPlural'

	declare KeysCursor cursor for
		select OldKey,
			OldKey2,
			OldKey3,
			OldKey,
			OldKey2,
			OldKey3, PKey from KeyConvertWork where Entity = @Entity
	open KeysCursor
	fetch next from KeysCursor into 
		@OldWBS1,
		@OldWBS2,
		@OldWBS3,
		@NewWBS1,
		@NewWBS2,
		@NewWBS3, 
		@PKeyWork

	set @KeysFetch = @@Fetch_Status

While (@KeysFetch = 0)
	begin
		begin tran
-- Add by DP 4.0
		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany Nvarchar(14)
			DECLARE @RecordCount integer

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				set @RecordCount = (select count(*) from KeyConvertWork where Entity = @Entity)

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),'KC','Y',  'Disable ' + @CustLabelPhasePlural + '/' + @Custlabelplural + ' ('+rtrim(convert(Nvarchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, left('Disable ' + @CustLabelPhasePlural + '/' + @Custlabelplural, 40), @CustLabelProj + '/' + @CustLabelPhase + '/' + @Custlabel);
			end

			Set @PostControlFlag = 1
		end

		select @OldName = isnull(Name, N'') from PR where WBS1 = @OldWBS1 and WBS2=N' ' and WBS3 = N' '
		select @OldName2 = isnull(Name, N'') from PR where WBS1 = @OldWBS1 and WBS2=@OldWBS2 and WBS3 = N' '
		select @OldName3 = isnull(Name, N'') from PR where WBS1 = @OldWBS1 and WBS2=@OldWBS2 and WBS3 = @OldWBS3
		set @NewName = @OldName
		set @NewName2 = @OldName2
		set @NewName3 = @OldName3

/* Removed by DP 4.0
		exec @RetVal = KeyCvt @Entity = 'WBS3',
									@CvtType = @CvtType,
									@Length = 20,
									@ErrMsg = @ErrMsg2 output,
									@Diag = @Diag,
									@ConstrFlag = 1 -- Drop Constraints		
*/
			if (@Diag = 1)
				print '|' + @oldWBS1 + '|' + '|' + @oldWBS2 + '|'
		if (isNull(@oldWBS2,' ') = ' ') --turning off phases for project
		begin
/* new for 5.0 add checking for multi-company phase and task --- Start*/
			if (@MulticompanyEnabled = 'Y')
				begin
					if (@Diag <> 0)
						print 'declare countCursor cursor for select convert(Nvarchar(4), count(*)) from (select distinct left(org, cfgformat.Org1Length) as Company from PR, cfgformat where wbs1 = N''' + @oldWBS1 + ''' and wbs2<>N'' '') a'
					execute('declare countCursor cursor for select convert(Nvarchar(4), count(*)) from (select distinct left(org, cfgformat.Org1Length) as Company from PR, cfgformat where wbs1 = N''' + @oldWBS1 + ''') a')

					open countCursor
					fetch next from countCursor into @test
					close countCursor
					deallocate countCursor
					if (@@FETCH_STATUS = 0 and @test > 1) -- multiple org in sublevel
						begin
							RAISERROR ('The %s %s and the %s below that %s belong to more than one Company. The conversion will not be processed.',
							16, 3, @CustLabelProj, @oldWBS1, @CustLabelPhasePlural, @CustLabelProj)

							if (@@Trancount > 0)
								ROLLBACK TRANSACTION
							close keysCursor
							deallocate keysCursor
							return(50001) -- user defined error
						end
				end
/* new for 5.0 add checking for multi-company phase and task --- End*/

			if (@Diag = 1)
				print 'turning off phases for project ' + @oldWBS1

/*new for 2.0*/
	
	Insert into EMProjectAssocTemplate 
	(  WBS1,
	  WBS2,
	  WBS3,
	  Employee,
	  Role,
	  RoleDescription,
	  TeamStatus,
	  StartDate,
	  EndDate,
	  CreateUser,
	  CreateDate,
	  ModUser,
	  ModDate,
	  RecordID)
	select   WBS1,
	  ' ',
	  ' ',
	  Employee,
	  Role,
	  max(convert(Nvarchar,RoleDescription)),
	  max(TeamStatus),
	  max(StartDate),
	  max(EndDate),
	  max(CreateUser),
	  max(CreateDate),
	  max(ModUser),
	  max(ModDate),
	 replace(newid(),'-','')
	  from EMProjectAssocTemplate
	 Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
				and  not exists (select 'x' from EMProjectAssocTemplate tmp where
				 tmp.wbs1 = EMProjectAssocTemplate.wbs1 
				 and tmp.wbs2 = N' '
				 and tmp.wbs3 = N' ' 
				 and tmp.employee = EMProjectAssocTemplate.employee 
				 and tmp.role = EMProjectAssocTemplate.role)
				group by wbs1,employee,Role
	
	delete from EMProjectAssocTemplate where 
		WBS1 = @OldWBS1 and 
		WBS2  <> N' '
	
	Insert into PRAwardsTemplate 
	(  WBS1,
	  WBS2,
	  WBS3,
	  RecordID,
	  Description,
	  Institution,
	  AwardDate,
	  CreateUser,
	  CreateDate,
	  ModUser,
	  ModDate)
	select   WBS1,
	  ' ',
	  ' ',
	  RecordID,
	  max(Description),
	  max(Institution),
	  max(AwardDate),
	  max(CreateUser),
	  max(CreateDate),
	  max(ModUser),
	  max(ModDate) 
	  from PRAwardsTemplate
	 Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
				and  not exists (select 'x' from PRAwardsTemplate tmp 
				where tmp.wbs1 = PRAwardsTemplate.wbs1 
				and tmp.wbs2 = N' '
				and tmp.wbs3 = N' ' 
				and tmp.RecordID = PRAwardsTemplate.RecordID)
				group by wbs1,RecordID
				
	delete from PRAwardsTemplate where 
		WBS1 = @OldWBS1 and 
		WBS2  <> N' '
	
				
	Insert into PRDescriptionsTemplate 
	(  WBS1,
	  WBS2,
	  WBS3,
	  DescCategory,
	  Description,
	  CreateUser,
	  CreateDate,
	  ModUser,
	  ModDate)
	select   WBS1,
	  ' ',
	  ' ',
	  DescCategory,
	  max(convert(Nvarchar,Description)),
	  max(CreateUser),
	  max(CreateDate),
	  max(ModUser),
	  max(ModDate) 
	  from PRDescriptionsTemplate
	 Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
				and  not exists (select 'x' from PRDescriptionsTemplate tmp 
				where tmp.wbs1 = PRDescriptionsTemplate.wbs1 
				and tmp.wbs2 = N' '
				and tmp.wbs3 = N' ' 
				and tmp.DescCategory = PRDescriptionsTemplate.DescCategory)
				group by wbs1,DescCategory
				
	delete from PRDescriptionsTemplate where 
		WBS1 = @OldWBS1 and 
		WBS2  <> N' '
	
	Insert into prsummarySub 
	 (WBS1,
	  WBS2,
	  WBS3,
	  Period,
	  ChargeType,
	  Revenue,
	  DirectConsCost,
	  DirectOtherCost,
	  DirectConsBilling,
	  DirectOtherBilling,
	  InDirectCost,
	  InDirectBilling,
	  ReimbConsCost,
	  ReimbOtherCost,
	  ReimbConsBilling,
	  ReimbOtherBilling,
	  Hours,
	  LaborCost,
	  LaborBilling,
	  Billed,
	  BilledLab,
	  BilledCons,
	  BilledExp,
	  BilledFee,
	  BilledUnit,
	  BilledAddOn,
	  BilledTaxes,
	  BilledInterest,
	  BilledOther,
	  AR,
	  Received,
	  Overhead,
	  RevenueBillingCurrency,
	  RevenueProjectCurrency,
	  DirectConsCostProjectCurrency,
	  DirectOtherCostProjectCurrency,
	  ReimbConsCostProjectCurrency,
	  ReimbOtherCostProjectCurrency,
	  InDirectCostProjectCurrency,
	  LaborCostProjectCurrency,
	  BilledProjectCurrency,
	  BilledLabProjectCurrency,
	  BilledConsProjectCurrency,
	  BilledExpProjectCurrency,
	  BilledFeeProjectCurrency,
	  BilledUnitProjectCurrency,
	  BilledAddOnProjectCurrency,
	  BilledTaxesProjectCurrency,
	  BilledInterestProjectCurrency,
	  BilledOtherProjectCurrency,
	  BilledBillingCurrency,
	  BilledLabBillingCurrency,
	  BilledConsBillingCurrency,
	  BilledExpBillingCurrency,
	  BilledFeeBillingCurrency,
	  BilledUnitBillingCurrency,
	  BilledAddOnBillingCurrency,
	  BilledTaxesBillingCurrency,
	  BilledInterestBillingCurrency,
	  BilledOtherBillingCurrency,
	  ARProjectCurrency,
	  ARBillingCurrency,
	  ReceivedProjectCurrency,
	  ReceivedBillingCurrency,
	  OverheadProjectCurrency,
	  LaborCostBillingCurrency,
	  DirectConsCostBillingCurrency,
	  DirectOtherCostBillingCurrency,
	  ReimbConsCostBillingCurrency,
	  ReimbOtherCostBillingCurrency,
	  InDirectCostBillingCurrency,
	  RegHours,
	  OvtHours,
	  SpecialOvtHours,
	  LaborRegAmt,
	  LaborOvtAmt,
	  LaborSpecialOvtAmt,
	  LaborRegAmtProjectCurrency,
	  LaborOvtAmtProjectCurrency,
	  LaborSpecialOvtAmtProjectCurrency,
	  LaborRegAmtBillingCurrency,
	  LaborOvtAmtBillingCurrency,
	  LaborSpecialOvtAmtBillingCurrency,
	  ModUser,
	  ModDate)
	select WBS1,
		' ',
		' ',
		Period,
		min(ChargeType),
		sum(Revenue),
		sum(DirectConsCost),
		sum(DirectOtherCost),
		sum(DirectConsBilling),
		sum(DirectOtherBilling),
		sum(InDirectCost),
		sum(InDirectBilling),
		sum(ReimbConsCost),
		sum(ReimbOtherCost),
		sum(ReimbConsBilling),
		sum(ReimbOtherBilling),
		sum(Hours),
		sum(LaborCost),
		sum(LaborBilling),
		sum(Billed),
		sum(BilledLab),
		sum(BilledCons),
		sum(BilledExp),
		sum(BilledFee),
		sum(BilledUnit),
		sum(BilledAddOn),
		sum(BilledTaxes),
		sum(BilledInterest),
		sum(BilledOther),
		sum(AR),
		sum(Received),
		sum(Overhead),
		sum(RevenueBillingCurrency),
		sum(RevenueProjectCurrency),
		sum(DirectConsCostProjectCurrency),
		sum(DirectOtherCostProjectCurrency),
		sum(ReimbConsCostProjectCurrency),
		sum(ReimbOtherCostProjectCurrency),
		sum(InDirectCostProjectCurrency),
		sum(LaborCostProjectCurrency),
		sum(BilledProjectCurrency),
		sum(BilledLabProjectCurrency),
		sum(BilledConsProjectCurrency),
		sum(BilledExpProjectCurrency),
		sum(BilledFeeProjectCurrency),
		sum(BilledUnitProjectCurrency),
		sum(BilledAddOnProjectCurrency),
		sum(BilledTaxesProjectCurrency),
		sum(BilledInterestProjectCurrency),
		sum(BilledOtherProjectCurrency),
		sum(BilledBillingCurrency),
		sum(BilledLabBillingCurrency),
		sum(BilledConsBillingCurrency),
		sum(BilledExpBillingCurrency),
		sum(BilledFeeBillingCurrency),
		sum(BilledUnitBillingCurrency),
		sum(BilledAddOnBillingCurrency),
		sum(BilledTaxesBillingCurrency),
		sum(BilledInterestBillingCurrency),
		sum(BilledOtherBillingCurrency),
		sum(ARProjectCurrency),
		sum(ARBillingCurrency),
		sum(ReceivedProjectCurrency),
		sum(ReceivedBillingCurrency),
		sum(OverheadProjectCurrency),
		sum(LaborCostBillingCurrency),
		sum(DirectConsCostBillingCurrency),
		sum(DirectOtherCostBillingCurrency),
		sum(ReimbConsCostBillingCurrency),
		sum(ReimbOtherCostBillingCurrency),
		sum(InDirectCostBillingCurrency),
		sum(RegHours),
		sum(OvtHours),
		sum(SpecialOvtHours),
		sum(LaborRegAmt),
		sum(LaborOvtAmt),
		sum(LaborSpecialOvtAmt),
		sum(LaborRegAmtProjectCurrency),
		sum(LaborOvtAmtProjectCurrency),
		sum(LaborSpecialOvtAmtProjectCurrency),
		sum(LaborRegAmtBillingCurrency),
		sum(LaborOvtAmtBillingCurrency),
		sum(LaborSpecialOvtAmtBillingCurrency),
		max(ModUser),
		max(ModDate)
	  	from prsummarySub
	 Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
				and  not exists (select 'x' from prsummarySub tmp 
				where tmp.wbs1 = prsummarySub.wbs1 
				and tmp.wbs2 = N' '
				and tmp.wbs3 = N' ' 
				and tmp.Period = prsummarySub.Period)
				group by wbs1,Period

	delete from PRSummarySub where 
		WBS1 = @OldWBS1 and 
		WBS2  <> N' '


	Insert into prsummaryMain 
	 (WBS1,
	  WBS2,
	  WBS3,
	  Period,
	  ChargeType,
	  Revenue,
	  Billed,
	  BilledLab,
	  BilledCons,
	  BilledExp,
	  BilledFee,
	  BilledUnit,
	  BilledAddOn,
	  BilledTaxes,
	  BilledInterest,
	  BilledOther,
	  AR,
	  Received,
	  LaborCost,
	  LaborBilling,
	  Unbilled,
	  SpentCostLessOH,
	  SpentBilling,
	  GrossMargin,
	  NetRevenueCost,
	  NetRevenueBilling,
	  ProfitCostLessOH,
	  ProfitBilling,
	  Overhead,
	  RevenueBillingCurrency,
	  RevenueProjectCurrency,
	  ARProjectCurrency,
	  ARBillingCurrency,
	  ReceivedProjectCurrency,
	  ReceivedBillingCurrency,
	  LaborCostProjectCurrency,
	  LaborCostBillingCurrency,
	  SpentCostLessOHProjectCurrency,
	  SpentCostLessOHBillingCurrency,
	  GrossMarginProjectCurrency,
	  GrossMarginBillingCurrency,
	  NetRevenueCostProjectCurrency,
	  NetRevenueCostBillingCurrency,
	  ProfitCostLessOHProjectCurrency,
	  ProfitCostLessOHBillingCurrency,
	  OverheadProjectCurrency,
	  ModUser,
	  ModDate)
	select WBS1,
		' ',
		' ',
		Period,
		min(ChargeType),
		sum(Revenue),
		sum(Billed),
		sum(BilledLab),
		sum(BilledCons),
		sum(BilledExp),
		sum(BilledFee),
		sum(BilledUnit),
		sum(BilledAddOn),
		sum(BilledTaxes),
		sum(BilledInterest),
		sum(BilledOther),
		sum(AR),
		sum(Received),
		sum(LaborCost),
		sum(LaborBilling),
		sum(Unbilled),
		sum(SpentCostLessOH),
		sum(SpentBilling),
		sum(GrossMargin),
		sum(NetRevenueCost),
		sum(NetRevenueBilling),
		sum(ProfitCostLessOH),
		sum(ProfitBilling),
		sum(Overhead),
		sum(RevenueBillingCurrency),
		sum(RevenueProjectCurrency),
		sum(ARProjectCurrency),
		sum(ARBillingCurrency),
		sum(ReceivedProjectCurrency),
		sum(ReceivedBillingCurrency),
		sum(LaborCostProjectCurrency),
		sum(LaborCostBillingCurrency),
		sum(SpentCostLessOHProjectCurrency),
		sum(SpentCostLessOHBillingCurrency),
		sum(GrossMarginProjectCurrency),
		sum(GrossMarginBillingCurrency),
		sum(NetRevenueCostProjectCurrency),
		sum(NetRevenueCostBillingCurrency),
		sum(ProfitCostLessOHProjectCurrency),
		sum(ProfitCostLessOHBillingCurrency),
		sum(OverheadProjectCurrency),
		max(ModUser),
		max(ModDate)
	  	from prsummaryMain
	 Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
				and  not exists (select 'x' from prsummaryMain tmp 
				where tmp.wbs1 = prsummaryMain.wbs1 
				and tmp.wbs2 = N' '
				and tmp.wbs3 = N' ' 
				and tmp.Period = prsummaryMain.Period)
				group by wbs1,Period

	delete from PRSummaryMain where 
		WBS1 = @OldWBS1 and 
		WBS2  <> N' '


							
	Insert into ClendorProjectAssoc 
	(  ClientID,
	  WBS1,
	  Role,
	  CreateUser,
	  CreateDate,
	  ModUser,
	  ModDate,
	  WBS2,
	  WBS3,
	  RoleDescription,
	  TeamStatus,Address,PrimaryInd,ClientConfidential,ClientInd,VendorInd)
	select   ClientID,
	  WBS1,
	  max(Role),
	  max(CreateUser),
	  max(CreateDate),
	  max(ModUser),
	  max(ModDate),
	  ' ',
	  ' ',
	  max(convert(Nvarchar,RoleDescription)),
	  max(TeamStatus), max(Address), max(PrimaryInd), max(ClientConfidential), max(ClientInd), max(VendorInd)
	   from ClendorProjectAssoc
	 Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
				and  not exists (select 'x' from ClendorProjectAssoc tmp 
				where tmp.wbs1 = ClendorProjectAssoc.wbs1 
				and tmp.wbs2 = N' '
				and tmp.wbs3 = N' ' 
				and tmp.ClientID = ClendorProjectAssoc.ClientID)
				group by ClientID,wbs1,wbs2
				
	delete from ClendorProjectAssoc where 
		WBS1 = @OldWBS1 and 
		WBS2  <> N' '
	
		   
	Insert into ClendorProjectAssocTemplate 
	(  ClientID,
	  WBS1,
	  WBS2,
	  WBS3,
	  Role,
	  RoleDescription,
	  TeamStatus,Address,PrimaryInd,ClientConfidential,ClientInd,VendorInd,
	  CreateUser,
	  CreateDate,
	  ModUser,
	  ModDate)
	select   ClientID,
	  WBS1,
	  ' ',
	  ' ',
	  max(Role),
	  max(convert(Nvarchar,RoleDescription)),
	  max(TeamStatus), max(Address), max(PrimaryInd), max(ClientConfidential), max(ClientInd), max(VendorInd),
	  max(CreateUser),
	  max(CreateDate),
	  max(ModUser),
	  max(ModDate) 
	  from ClendorProjectAssocTemplate
	 Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
				and  not exists (select 'x' from ClendorProjectAssocTemplate tmp 
				where tmp.wbs1 = ClendorProjectAssocTemplate.wbs1 
				and tmp.wbs2 = N' '
				and tmp.wbs3 = N' ' 
				and tmp.ClientID = ClendorProjectAssocTemplate.ClientID)
				group by ClientID,wbs1,wbs2
	
	delete from ClendorProjectAssocTemplate where 
		WBS1 = @OldWBS1 and 
		WBS2  <> N' '

			Insert into PRProjectCodesTemplate 
			 (WBS1,
			  WBS2,
			  WBS3,
			  ProjectCode,
			  Fees,
			  Seq,
			  CreateUser,
			  CreateDate,
			  ModUser,
			  ModDate)
			select
			  WBS1,
			  ' ',
			  ' ',
			  ProjectCode,
			  sum(Fees),
			  max(Seq),
			  max(CreateUser),
			  max(CreateDate),
			  max(ModUser),
			  max(ModDate)
			from PRProjectCodesTemplate where PRProjectCodesTemplate.WBS1 = @OldWBS1
			and not exists (select 'x' from PRProjectCodesTemplate tmp where tmp.WBS1 = PRProjectCodesTemplate.WBS1 
									and tmp.WBS2 = N' ' 
									and tmp.WBS3 = N' ' 
									and tmp.ProjectCode = PRProjectCodesTemplate.ProjectCode)
			group by WBS1,ProjectCode
			
			delete from PRProjectCodesTemplate where PRProjectCodesTemplate.WBS1 = @OldWBS1 and PRProjectCodesTemplate.WBS2  <> N' '

			delete from prTemplate where wbs2 <> N' ' and wbs1 = @oldwbs1
			delete from PRAdditionalData where wbs2 <> N' ' and wbs1 = @oldwbs1

Insert into MktCampaignProjectAssoc 
 (CampaignID,
  WBS1,
  WBS2,
  WBS3,
  ModUser,
  ModDate,
  CreateUser,
  CreateDate)
select CampaignID,
  WBS1,
  ' ',
  ' ',
  max(ModUser),
  max(ModDate),
  max(CreateUser),
  max(CreateDate)
from MktCampaignProjectAssoc where MktCampaignProjectAssoc.WBS1 = @OldWBS1
and not exists (select 'x' from MktCampaignProjectAssoc tmp where tmp.WBS1 = MktCampaignProjectAssoc.WBS1 
									and tmp.WBS2 = N' ' 
									and tmp.WBS3 = N' ' 
									and tmp.CampaignID = MktCampaignProjectAssoc.CampaignID)
group by CampaignID,WBS1

delete from MktCampaignProjectAssoc where WBS1 = @OldWBS1 and WBS2 <> N' '


Insert into MktCampaignProjectAssocTemplate 
 (CampaignID,
  WBS1,
  WBS2,
  WBS3,
  ModUser,
  ModDate,
  CreateUser,
  CreateDate)
select CampaignID,
  WBS1,
  ' ',
  ' ',
  max(ModUser),
  max(ModDate),
  max(CreateUser),
  max(CreateDate)
from MktCampaignProjectAssocTemplate where MktCampaignProjectAssocTemplate.WBS1 = @OldWBS1
and not exists (select 'x' from MktCampaignProjectAssocTemplate tmp where tmp.WBS1 = MktCampaignProjectAssocTemplate.WBS1 
									and tmp.WBS2 = N' ' 
									and tmp.WBS3 = N' ' 
									and tmp.CampaignID = MktCampaignProjectAssocTemplate.CampaignID)
group by CampaignID,WBS1

delete from MktCampaignProjectAssocTemplate where WBS1 = @OldWBS1 and WBS2 <> N' '

/*end 2.0*/			
			delete from reportWBSList where wbs1 = @oldwbs1 and wbs2 <> N' '
			Insert into emProjectAssoc 
			( WBS1,
			  WBS2,
			  WBS3,
			  Employee,
			  Role,
			  RoleDescription,
			  TeamStatus,
			  CreateUser,
			  CreateDate,
			  ModUser,
			  ModDate,
			  RecordID)
			select
			  WBS1,
			  ' ',
			  ' ',
			  Employee,
			  Role,
			  max(convert(Nvarchar,RoleDescription)),
			  max(TeamStatus),
			  max(CreateUser),
			  max(CreateDate),
			  max(ModUser),
			  max(ModDate),
		     replace(newid(),'-','')
			from emProjectAssoc  Where WBS1 = @OldWBS1
			and  not exists (select 'x' from emProjectAssoc tmp where tmp.wbs1 = emProjectAssoc.wbs1 and tmp.wbs2 = N' ' and tmp.wbs3 = N' ' and tmp.employee = emProjectAssoc.employee and tmp.role = emProjectAssoc.role)
			group by wbs1,employee,Role
			
			delete from emProjectAssoc where WBS1 = @OldWBS1 and wbs2 <> N' ' 


			Insert into PRAwards 
			 (WBS1,
			  WBS2,
			  WBS3,
			  RecordID,
			  Description,
			  Institution,
			  AwardDate,
			  CreateUser,
			  CreateDate,
			  ModUser,
			  ModDate)
			select   
			  WBS1,
			  ' ',
			  ' ',
			  RecordID,
			  max(Description),
			  max(Institution),
			  max(AwardDate),
			  max(CreateUser),
			  max(CreateDate),
			  max(ModUser),
			  max(ModDate) from PRAwards
			where PRAwards.WBS1 = @OldWBS1
			and  not exists (select 'x' from PRAwards tmp where tmp.wbs1 = PRAwards.wbs1 and tmp.wbs2 = N' ' and tmp.wbs3 = N' ' and tmp.RecordID = PRAwards.RecordID)
			group by wbs1,RecordID
			
			delete from PRAwards where PRAwards.WBS1 = @OldWBS1 and PRAwards.WBS2 <> N' ' 
					
			Insert into PRDescriptions 
			 (WBS1,
			  WBS2,
			  WBS3,
			  DescCategory,
			  Description,
			  CreateUser,
			  CreateDate,
			  ModUser,
			  ModDate)
			select
			  WBS1,
			  ' ',
			  ' ',
			  DescCategory,
			  max(convert(Nvarchar,Description)),
			  max(CreateUser),
			  max(CreateDate),
			  max(ModUser),
			  max(ModDate)
			from PRDescriptions
			where WBS1 = @OldWBS1
			and not exists (select 'x' from PRDescriptions tmp where tmp.WBS1 = PRDescriptions.WBS1 and tmp.WBS2 = N' ' and tmp.WBS3 = N' ' and tmp.DescCategory = PRDescriptions.DescCategory)
			group by WBS1,DescCategory
			
			delete from PRDescriptions where PRDescriptions.WBS1 = @OldWBS1 and PRDescriptions.WBS2  <> N' '
			
			Insert into PRFileLinks 
			(  LinkID,
			  WBS1,
			  WBS2,
			  WBS3,
			  Description,
			  FilePath,
			  Graphic,
			  CreateUser,
			  CreateDate,
			  ModUser,
			  ModDate)
			select 
			  LinkID,
			  WBS1,
			  ' ',
			  ' ',
			  max(Description),
			  max(FilePath),
			  max(Graphic),
			  max(CreateUser),
			  max(CreateDate),
			  max(ModUser),
			  max(ModDate)
			from PRFileLinks where PRFileLinks.WBS1 = @OldWBS1
			and not exists (select 'x' from PRFileLinks tmp where tmp.WBS1 = PRFileLinks.WBS1 and tmp.WBS2 = N' ' and tmp.WBS3 = N' ' and tmp.LinkID = PRFileLinks.LinkID)
			group by WBS1,LinkID
			
			delete from PRFileLinks where WBS1 = @OldWBS1 and WBS2 <> N' '
			
			Insert into PRProjectCodes 
			 (WBS1,
			  WBS2,
			  WBS3,
			  ProjectCode,
			  Fees,
			  Seq,
			  CreateUser,
			  CreateDate,
			  ModUser,
			  ModDate)
			select
			  WBS1,
			  ' ',
			  ' ',
			  ProjectCode,
			  sum(Fees),
			  max(Seq),
			  max(CreateUser),
			  max(CreateDate),
			  max(ModUser),
			  max(ModDate)
			from PRProjectCodes where PRProjectCodes.WBS1 = @OldWBS1
			and not exists (select 'x' from PRProjectCodes tmp where tmp.WBS1 = PRProjectCodes.WBS1 and tmp.WBS2 = N' ' and tmp.WBS3 = N' ' and tmp.ProjectCode = PRProjectCodes.ProjectCode)
			group by WBS1,ProjectCode
			
			delete from PRProjectCodes where PRProjectCodes.WBS1 = @OldWBS1 and PRProjectCodes.WBS2  <> N' '

			Insert into billinmaster 
			 (MainWBS1,
			  Invoice,
			  WBS1,
			  WBS2,
			  WBS3,
			  TransDate,
			  TransComment)
			select
			  MainWBS1,
			  Invoice,
			  WBS1,
			  ' ',
			  ' ',
			  max(TransDate),
			  max(TransComment)
			 from billinmaster where billinmaster.wbs1 = @oldWBS1
				and not exists (select 'x' from billINMaster tmp where tmp.wbs1 = BillINMaster.wbs1 and tmp.WBS2 = N' ' and tmp.WBS3 = N' ' and tmp.MainWBS1 = billinmaster.mainWBS1 and  tmp.Invoice = billINMaster.Invoice)
			group by mainWBS1,Invoice,WBS1

			delete from billInMaster where billInMaster.WBS1 = @OldWBS1 and billInMaster.WBS2  <> N' '
/*start billINDetail*/

		declare billInCursor cursor for
		select 
			MainWBS1,
			Invoice,
			WBS1,
		   WBS2,
			WBS3,
			PKey
		 from billindetail bid where wbs1 = @oldWBS1 or MainWBS1 = @oldWBS1
		and exists (select 'x' from billindetail tmp
							where tmp.MainWBS1 = bid.MainWBS1 and
							tmp.Invoice = bid.Invoice and
							tmp.WBS1 = bid.WBS1 and
							tmp.PKey = bid.Pkey
							group by 
								MainWBS1,
								Invoice,
								WBS1,
								PKey
							having count(*) > 1)
		
		open billInCursor
		fetch next from billinCursor into 
			@MainWBS1,
			@Invoice,
			@Wbs1,
			@WBS2,
			@WBS3,
			@PKey
		
		while (@@Fetch_status = 0)
		 begin
			update billInDetail set pKey = convert(Nvarchar(32),replace(newid(),'-',''))
			where MainWBS1 = @MainWBS1 and
					Invoice = @Invoice and
					WBS1 = @WBS1 and
					WBS2 = @WBS2 and
					WBS3 = @WBS3 and
					Pkey = @Pkey
					
		
			fetch next from billinCursor into 
				@MainWBS1,
				@Invoice,
				@Wbs1,
				@WBS2,
				@WBS3,
				@PKey
		 end
		close billInCursor
		
		deallocate billincursor
/*end billINDetail*/			
			Insert into inMaster 
			(  Batch,
			  Invoice,
			  WBS1,
			  WBS2,
			  WBS3,
			  TransDate,
			  TransComment,
			  Posted,
			  Seq)
			select   
			  Batch,
			  Invoice,
			  WBS1,
			  ' ',
			  ' ',
			  max(TransDate),
			  max(TransComment),
			  max(Posted),
			  max(Seq)
			from inMaster
			where inMaster.WBS1 = @OldWBS1
			and not exists (select 'x' from inMaster tmp where tmp.WBS1 = inMaster.WBS1 and tmp.WBS2 = N' ' and tmp.WBS3 = N' ' and tmp.Batch = inMaster.Batch and tmp.Invoice = inMaster.Invoice)
			group by Batch,Invoice,WBS1
			
			delete from inMaster where inMaster.WBS1 = @OldWBS1 and inMaster.WBS2  <> N' '
			IF EXISTS (SELECT 'x' FROM sys.objects WHERE name = 'ProjectCustomTabFields' AND type = 'U')
				begin	
					delete from ProjectCustomTabFields where ProjectCustomTabFields.WBS1 = @OldWBS1 and ProjectCustomTabFields.WBS2  <> N' ' 
					--and exists (select 'x' from ProjectCustomTabFields new where new.WBS1 = @OldWBS1 and new.wbs2 = @NewWBS2 and new.wbs3 = ProjectCustomTabFields.wbs3)
				end

			delete from prf where wbs1 = @oldWbs1 and wbs2 = N' '
									
			Insert into prf 
			(  WBS1,
			  WBS2,
			  WBS3,
			  Period,
			  RegOH,
			  GandAOH)
			select 
			  tmp.WBS1,
			  ' ',
			  ' ',
			  tmp.Period,
			  0,
			  0 from prf tmp 
			where wbs1 = @oldWbs1 and wbs2 <> N' ' 
			and not exists (select 'x' from prf tmp2 where tmp2.wbs1 = tmp.wbs1 and tmp2.wbs2 = N' ' and tmp2.period = tmp.period)
			group by tmp.wbs1, tmp.period
			
			update prf
				set PRF.RegOH =  sumRegOH,
				PRF.GandAOH = sumGandAOH,
				 PRF.RegOHProjectCurrency =  sumRegOHProjectCurrency,
				 PRF.GandAOHProjectCurrency =  sumGandAOHProjectCurrency
				from (select wbs1, min(tmp.wbs2) wbs2, min(tmp.wbs3) wbs3, period ,
						sum(tmp.RegOH) as sumRegOH,
						sum(tmp.GandAOH) as sumGandAOH,
						sum(tmp.RegOHProjectCurrency) as sumRegOHProjectCurrency,
						sum(tmp.GandAOHProjectCurrency) as sumGandAOHProjectCurrency
						from prf  tmp
						where wbs1 = @oldWBS1 and WBS2 <> N' '
						group by WBS1,period) as tmp
				inner join prf on tmp.wbs1 = prf.wbs1 and tmp.period = prf.period 
				where
					prf.wbs2 = N' ' and prf.wbs3 = N' '
			
			delete from prf where wbs1 = @oldwbs1 and wbs2 <> N' '
			
			Insert into ar 
			 (WBS1,
			  WBS2,
			  WBS3,
			  Invoice,
			  Period,
			  InvoiceDate,
			  LinkCompany,
			  InvBalanceSourceCurrency,
			  DueDate,
			  PaidPeriod)
			select
			  tmp.WBS1,
			  ' ',
			  ' ',
			  tmp.Invoice,
			  min(tmp.Period) period,
			  min(tmp.InvoiceDate) InvoiceDate,
			  min(tmp.LinkCompany) LinkCompany,
			  min(tmp.InvBalanceSourceCurrency) InvBalanceSourceCurrency,
			  min(tmp.DueDate) DueDate,
			  min(tmp.PaidPeriod) PaidPeriod 
			from AR as tmp 
			where tmp.WBS2 <> N' ' and tmp.WBS1 = @oldWBS1 
			group by tmp.wbs1,tmp.invoice
			
			delete from AR where wbs2 <> N' ' and wbs1 = @oldWBS1

/*new for 7.4*/
			Insert into ARPreInvoice 
			 (WBS1,
			  WBS2,
			  WBS3,
			  PreInvoice,
			  Period,
			  InvoiceDate,
			  Cancelled,
			  AppliedInvoice,
			  AppliedPeriod,
			  AppliedPostSeq,
			  Note)
			select
			  tmp.WBS1,
			  ' ',
			  ' ',
			  tmp.PreInvoice,
			  min(tmp.Period) period,
			  min(tmp.InvoiceDate) InvoiceDate,
			  min(tmp.Cancelled) Cancelled,
			  min(tmp.AppliedInvoice) AppliedInvoice,
			  min(tmp.AppliedPeriod) AppliedPeriod,
			  min(tmp.AppliedPostSeq) AppliedPostSeq,
			  max(tmp.Note) as Note 
			from ARPreInvoice as tmp 
			where tmp.WBS2 <> N' ' and tmp.WBS1 = @oldWBS1 
			and not exists (select 'x' from ARPreInvoice tmp2 where tmp2.WBS1 = tmp.WBS1 and tmp2.WBS2 = N' ' and tmp2.WBS3 = N' ' 
			and tmp2.PreInvoice = tmp.PreInvoice)
			group by tmp.wbs1,tmp.PreInvoice
			delete from ARPreInvoice where wbs2 <> N' ' and wbs1 = @oldWBS1

			Insert into ARPreInvoiceDetail 
			 (WBS1,
				WBS2,
				WBS3,
				PreInvoice,
				Seq,
				InvoiceSection,
				Amount,
				PaidAmount,
				Account,
				TaxCode,
				TaxBasis,
				WBS2ToPost,
				WBS3ToPost)
			select
			  tmp.WBS1,
			  ' ',
			  ' ',
			  tmp.PreInvoice,
			  tmp.Seq,
			  min(tmp.InvoiceSection) InvoiceSection,
			  sum(tmp.Amount) Amount,
			  sum(tmp.PaidAmount) PaidAmount,
			  min(tmp.Account) Account,
			  min(tmp.TaxCode) TaxCode,
			  min(tmp.TaxBasis) TaxBasis,
			  ' ',
			  ' ' 
			from ARPreInvoiceDetail as tmp 
			where tmp.WBS2 <> N' ' and tmp.WBS1 = @oldWBS1 
			and not exists (select 'x' from ARPreInvoiceDetail tmp2 where tmp2.WBS1 = tmp.WBS1 and tmp2.WBS2 = N' ' and tmp2.WBS3 = N' ' 
			and tmp2.PreInvoice = tmp.PreInvoice and tmp2.Seq = tmp.Seq)
			group by tmp.wbs1,tmp.PreInvoice,tmp.Seq
			delete from ARPreInvoiceDetail where wbs2 <> N' ' and wbs1 = @oldWBS1

			Insert into BTFPhaseGroupData 
			 (WBS1,
				WBS2,
				WBS3,
				Seq,
				PhaseGroup,
				PrintHeader,
				PrintSubTotal,
				HideDetail,
				AddSpaceAfter)
			select
			  tmp.WBS1,
			  ' ',
			  ' ',
			  tmp.Seq,
			  tmp.PhaseGroup,
			  min(tmp.PrintHeader) PrintHeader,
			  min(tmp.PrintSubTotal) PrintSubTotal,
			  min(tmp.HideDetail) HideDetail,
			  min(tmp.AddSpaceAfter) AddSpaceAfter 
			from BTFPhaseGroupData as tmp 
			where tmp.WBS2 <> N' ' and tmp.WBS1 = @oldWBS1 
			and not exists (select 'x' from BTFPhaseGroupData tmp2 where tmp2.WBS1 = tmp.WBS1 and tmp2.WBS2 = N' ' and tmp2.WBS3 = N' ' 
			and tmp2.Seq = tmp.Seq and tmp2.PhaseGroup = tmp.PhaseGroup)
			group by tmp.wbs1,tmp.Seq,tmp.PhaseGroup
			delete from BTFPhaseGroupData where wbs2 <> N' ' and wbs1 = @oldWBS1

			Insert into BTFPhaseGroupDescriptions 
			 (WBS1,
				WBS2,
				WBS3,
				Seq,
				PhaseGroup,
				UICultureName,
				Description)
			select
			  tmp.WBS1,
			  ' ',
			  ' ',
			  tmp.Seq,
			  tmp.PhaseGroup,
			  tmp.UICultureName,
			  max(tmp.Description) Description
			from BTFPhaseGroupDescriptions as tmp 
			where tmp.WBS2 <> N' ' and tmp.WBS1 = @oldWBS1 
			and not exists (select 'x' from BTFPhaseGroupDescriptions tmp2 where tmp2.WBS1 = tmp.WBS1 and tmp2.WBS2 = N' ' and tmp2.WBS3 = N' ' 
			and tmp2.Seq = tmp.Seq and tmp2.PhaseGroup = tmp.PhaseGroup and tmp2.UICultureName = tmp.UICultureName)
			group by tmp.wbs1,tmp.Seq,tmp.PhaseGroup,tmp.UICultureName
			delete from BTFPhaseGroupDescriptions where wbs2 <> N' ' and wbs1 = @oldWBS1
/*end 7.4*/
			
-- Added by DP
			Delete from PRSummaryWBSList where WBS1 = @OldWBS1 and WBS2 <> N' '
--
			Delete from BTA where WBS1 = @OldWBS1 and WBS2 <> N' '
			Delete from BTF where WBS1 = @OldWBS1 and WBS2 <> N' '
			Delete from BTSchedule where WBS1 = @OldWBS1 and WBS2 <> N' '
			Delete from BTTaxCodes where WBS1 = @OldWBS1 and WBS2 <> N' '
			Delete from BT where WBS1 = @OldWBS1 and WBS2 <> N' '
/*MJU changes per DAK 1/15/03*/
			UPDATE BT SET BT.SubLevelTerms = 'N',
			ConsolidatePrinting = 'N',
			ConsolidatePosting = 'N',
			WBS2ToPost = null,
			wbs3toPost = null,
			PreInvWBS2ToPost = null,
			PreInvwbs3toPost = null,
			PrintWBS2AR = 'N',
			PrintWBS2BTD = 'N',
			SubLevelRollup =0,
			LimitsBySublevel = 'N',
			InterestBySubLevel = 'N',
			TaxBySubLevel = 'N',
			RetainageBySubLevel = 'N'
			Where WBS2 = N' ' and WBS1 = @OldWBS1
			

			--MJU Change billINMaster to work like INMaster per DAK
			--Delete from billINMaster where WBS1 = @OldWBS1 and WBS2 <> ' '
			--Delete from billINDetail where WBS1 = @OldWBS1 and WBS2 <> ' '

			delete from billInvMaster where MainWBS1 = @OldWBS1 and BillWBS2 <> N' '

/*added 3.0 sp5 */
			delete from ICBillInvMaster where MainWBS1 = @OldWBS1 and BillWBS2 <> N' '

			Insert into lb 
			(  WBS1,
			  WBS2,
			  WBS3,
			  LaborCode,
			  Pct,
			  Rate,
			  BillRate,
			  HrsBud,
			  AmtBud,
			  BillBud,
			  StartDate,
			  EndDate,
			  EtcHrs,
			  EtcAmt,
			  EacHrs,
			  EacAmt)
			select 
				tmp.WBS1,
				' ',
				' ',
				tmp.LaborCode,
				min(tmp.Pct),
				min(tmp.Rate),
				min(tmp.BillRate),
				0,
				0,
				0,
				min(tmp.StartDate),
				min(tmp.EndDate),
				0,
				0,
				0,
				0 
			from lb tmp 
			where tmp.wbs1 = @oldWBS1 and tmp.WBS2 <> N' ' 
			group by tmp.WBS1, tmp.laborCode
			
			update LB
				set LB.AmtBud =  sumAmtBud,
				LB.BillBud = sumBillBud,
			   LB.HrsBud = sumHrsBud,
				LB.EtcAmt = sumEtcAmt,
				LB.EtcHrs = sumEtcHrs,
				LB.EacHrs = sumEacHrs,
				LB.EacAmt = sumEacAmt
				from (select wbs1, min(tmp.wbs2) wbs2, min(tmp.wbs3) wbs3, LaborCode, 
						sum(tmp.AmtBud) as sumAmtBud,
						sum(tmp.HrsBud) as sumHrsBud,
						sum(tmp.EtcHrs) as sumEtcHrs,
						sum(tmp.EacHrs) as sumEacHrs,
						sum(tmp.BillBud) as sumBillBud,
						sum(tmp.EtcAmt) as sumEtcAmt,
						sum(tmp.EacAmt) as sumEacAmt
						from lb  tmp
						where wbs1 = @oldWBS1 and WBS2 <> N' '
						group by WBS1,laborCode) as tmp
				inner join lb on tmp.wbs1 = LB.wbs1 and tmp.laborcode = LB.LaborCode
				where
					 LB.wbs2 = N' ' and LB.wbs3 = N' '
			
			delete from lb where WBS1 = @oldWBS1 and WBS2 <> N' '
			
			Insert into eb 
			( WBS1,
			  WBS2,
			  WBS3,
			  Account,
			  Vendor,
			  Pct,
			  AmtBud,
			  BillBud,
			  StartDate,
			  EndDate,
			  EtcAmt,
			  EacAmt)
			select
				tmp.WBS1,
				' ',
				' ',
				tmp.Account,
				tmp.Vendor,
				min(tmp.Pct) Pct,
				min(tmp.AmtBud) AmtBud,
				min(tmp.BillBud) BillBud,
				min(tmp.StartDate) StartDate,
				min(tmp.EndDate) EndDate,
				min(tmp.EtcAmt) EtcAmt,
				min(tmp.EacAmt) EacAmt
			from EB  tmp
			where tmp.wbs1 = @oldWBS1 and tmp.WBS2 <> N' ' 
			group by tmp.WBS1, tmp.Account,
				tmp.Vendor
			
			update EB
				set EB.AmtBud =  sumAmtBud,
				EB.BillBud = sumBillBud,
				EB.EtcAmt = sumEtcAmt,
				EB.EacAmt = sumEacAmt
				from (select wbs1, min(tmp.wbs2) wbs2, min(tmp.wbs3) wbs3, Account,Vendor, 
						sum(tmp.AmtBud) as sumAmtBud,
						sum(tmp.BillBud) as sumBillBud,
						sum(tmp.EtcAmt) as sumEtcAmt,
						sum(tmp.EacAmt) as sumEacAmt
						from eb  tmp
						where wbs1 = @oldWBS1 and WBS2 <> N' '
						group by WBS1,Account,Vendor) as tmp
				inner join eb on tmp.wbs1 = eb.wbs1 and tmp.vendor = eb.vendor and tmp.account = eb.account
				where
					eb.wbs2 = N' ' and eb.wbs3 = N' '
			
			delete from eb where wbs1 = @OldWBs1 and WBS2 <> N' '
			
--Reset Contracts.FeeIncludeInd to 'N'
			Update Contracts set FeeIncludeInd='N'
			from Contracts C 
			Where C.WBS1=@OldWBs1 and C.FeeIncludeInd='Y'

			if (Select SyncProjToContractFees from FW_CFGSystem) = 'Y'
				update PR set Fee = 0, ConsultFee = 0, ReimbAllow = 0, FeeBillingCurrency = 0, ConsultFeeBillingCurrency =0, 
					ReimbAllowBillingCurrency =0,   FeeFunctionalCurrency = 0, ConsultFeeFunctionalCurrency = 0, ReimbAllowFunctionalCurrency = 0 , 
					FeeDirLab = 0 , FeeDirExp = 0, ReimbAllowExp = 0 , ReimbAllowCons = 0, 
					FeeDirLabBillingCurrency = 0 , FeeDirExpBillingCurrency = 0, ReimbAllowExpBillingCurrency = 0 , ReimbAllowConsBillingCurrency = 0,   
					FeeDirLabFunctionalCurrency = 0 , FeeDirExpFunctionalCurrency = 0, ReimbAllowExpFunctionalCurrency = 0 , ReimbAllowConsFunctionalCurrency = 0  
					where WBS1 in (@OldWBs1)
--
-- RonK 07/15/09: Update ContractDetails for Rubik re: turning off Phases for a Project
			delete from ContractDetails where wbs1 = @OldWBS1 and WBS2 <> N' '

			Insert into PRChargeCompanies 
			 (WBS1,
			  WBS2,
			  WBS3,
			  Company,
			  CreateUser,
			  CreateDate,
			  ModUser,
			  ModDate
			  )
			select
			  tmp.WBS1,
			  ' ',
			  ' ',
			  tmp.Company,
			  max(CreateUser),
			  max(CreateDate),
			  max(ModUser),
			  max(ModDate)
			from PRChargeCompanies as tmp 
			where tmp.WBS1 = @oldWBS1 and tmp.WBS2 <> N' '
			group by tmp.wbs1,tmp.Company

	        Delete FROM PRChargeCompanies WHERE PRChargeCompanies.WBS1 = @OldWBS1 and PRChargeCompanies.WBS2 <> N' '

		end -- end turn off phases for project
		else if (isNull(@oldWBS2,' ') <> ' ') --Turning off tasks for phase
		begin
/* new for 5.0 add checking for multi-company phase and task --- Start*/
			if (@MulticompanyEnabled = 'Y')
				begin
					if (@Diag <> 0)
						print 'declare countCursor cursor for select convert(Nvarchar(4), count(*)) from (select distinct left(org, cfgformat.Org1Length) as Company from PR, cfgformat where wbs1 = N''' + @oldWBS1 + ''' and wbs2 = N''' + @oldWBS2 + ''' and wbs3<>N'' '') a'
					execute('declare countCursor cursor for select convert(Nvarchar(4), count(*)) from (select distinct left(org, cfgformat.Org1Length) as Company from PR, cfgformat where wbs1 = N''' + @oldWBS1 + ''' and wbs2 = N''' + @oldWBS2 + ''' ) a')

					open countCursor
					fetch next from countCursor into @test
					close countCursor
					deallocate countCursor
					if (@@FETCH_STATUS = 0 and @test > 1) -- multiple org in sublevel
						begin
							RAISERROR ('The %s/%s %s/%s and the %s below that %s belong to more than one Company. The conversion will not be processed.',
							16, 3,  @CustLabelProj, @CustLabelPhase, @oldWBS1, @oldWBS2, @Custlabelplural, @CustLabelPhase)

							if (@@Trancount > 0)
								ROLLBACK TRANSACTION
							close keysCursor
							deallocate keysCursor
							return(50001) -- user defined error
						end
				end
/* new for 5.0 add checking for multi-company phase and task --- End*/


			if (@Diag = 1)
				print 'Turning off tasks for phase:' + @OldWBS2 + ' project:' + @oldWBS1

/*new for 2.0*/

Insert into EMProjectAssocTemplate 
(  WBS1,
  WBS2,
  WBS3,
  Employee,
  Role,
  RoleDescription,
  TeamStatus,
  StartDate,
  EndDate,
  CreateUser,
  CreateDate,
  ModUser,
  ModDate,
  RecordID)
select   WBS1,
  WBS2,
  ' ',
  Employee,
  Role,
  max(convert(Nvarchar,RoleDescription)),
  max(TeamStatus),
  max(StartDate),
  max(EndDate),
  max(CreateUser),
  max(CreateDate),
  max(ModUser),
  max(ModDate),
  replace(newid(),'-','')
  from EMProjectAssocTemplate
 Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
			and  not exists (select 'x' from EMProjectAssocTemplate tmp where tmp.wbs1 = EMProjectAssocTemplate.wbs1 and tmp.wbs2 = EMProjectAssocTemplate.wbs2 and tmp.wbs3 = N' ' and tmp.employee = EMProjectAssocTemplate.employee and tmp.role = EMProjectAssocTemplate.role)
			group by wbs1,wbs2,employee,Role

delete from EMProjectAssocTemplate where 
	WBS1 = @OldWBS1 and 
	WBS2  = @oldWBS2 and 
	wbs3 <> N' '

Insert into PRAwardsTemplate 
(  WBS1,
  WBS2,
  WBS3,
  RecordID,
  Description,
  Institution,
  AwardDate,
  CreateUser,
  CreateDate,
  ModUser,
  ModDate)
select   WBS1,
  WBS2,
  ' ',
  RecordID,
  max(Description),
  max(Institution),
  max(AwardDate),
  max(CreateUser),
  max(CreateDate),
  max(ModUser),
  max(ModDate) 
  from PRAwardsTemplate
 Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
			and  not exists (select 'x' from PRAwardsTemplate tmp 
			where tmp.wbs1 = PRAwardsTemplate.wbs1 
			and tmp.wbs2 = PRAwardsTemplate.wbs2 
			and tmp.wbs3 = N' ' 
			and tmp.RecordID = PRAwardsTemplate.RecordID)
			group by wbs1,wbs2,RecordID
			
delete from PRAwardsTemplate where 
	WBS1 = @OldWBS1 and 
	WBS2  = @oldWBS2 and 
	wbs3 <> N' '
			
Insert into PRDescriptionsTemplate 
(  WBS1,
  WBS2,
  WBS3,
  DescCategory,
  Description,
  CreateUser,
  CreateDate,
  ModUser,
  ModDate)
select   WBS1,
  WBS2,
  ' ',
  DescCategory,
  max(convert(Nvarchar,Description)),
  max(CreateUser),
  max(CreateDate),
  max(ModUser),
  max(ModDate) 
  from PRDescriptionsTemplate
 Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
			and  not exists (select 'x' from PRDescriptionsTemplate tmp 
			where tmp.wbs1 = PRDescriptionsTemplate.wbs1 
			and tmp.wbs2 = PRDescriptionsTemplate.wbs2 
			and tmp.wbs3 = N' ' 
			and tmp.DescCategory = PRDescriptionsTemplate.DescCategory)
			group by wbs1,wbs2,DescCategory
			
delete from PRDescriptionsTemplate where 
	WBS1 = @OldWBS1 and 
	WBS2  = @oldWBS2 and 
	wbs3 <> N' '

	Insert into prsummarySub 
	 (WBS1,
	  WBS2,
	  WBS3,
	  Period,
	  ChargeType,
	  Revenue,
	  DirectConsCost,
	  DirectOtherCost,
	  DirectConsBilling,
	  DirectOtherBilling,
	  InDirectCost,
	  InDirectBilling,
	  ReimbConsCost,
	  ReimbOtherCost,
	  ReimbConsBilling,
	  ReimbOtherBilling,
	  Hours,
	  LaborCost,
	  LaborBilling,
	  Billed,
	  BilledLab,
	  BilledCons,
	  BilledExp,
	  BilledFee,
	  BilledUnit,
	  BilledAddOn,
	  BilledTaxes,
	  BilledInterest,
	  BilledOther,
	  AR,
	  Received,
	  Overhead,
	  RevenueBillingCurrency,
	  RevenueProjectCurrency,
	  DirectConsCostProjectCurrency,
	  DirectOtherCostProjectCurrency,
	  ReimbConsCostProjectCurrency,
	  ReimbOtherCostProjectCurrency,
	  InDirectCostProjectCurrency,
	  LaborCostProjectCurrency,
	  BilledProjectCurrency,
	  BilledLabProjectCurrency,
	  BilledConsProjectCurrency,
	  BilledExpProjectCurrency,
	  BilledFeeProjectCurrency,
	  BilledUnitProjectCurrency,
	  BilledAddOnProjectCurrency,
	  BilledTaxesProjectCurrency,
	  BilledInterestProjectCurrency,
	  BilledOtherProjectCurrency,
	  BilledBillingCurrency,
	  BilledLabBillingCurrency,
	  BilledConsBillingCurrency,
	  BilledExpBillingCurrency,
	  BilledFeeBillingCurrency,
	  BilledUnitBillingCurrency,
	  BilledAddOnBillingCurrency,
	  BilledTaxesBillingCurrency,
	  BilledInterestBillingCurrency,
	  BilledOtherBillingCurrency,
	  ARProjectCurrency,
	  ARBillingCurrency,
	  ReceivedProjectCurrency,
	  ReceivedBillingCurrency,
	  OverheadProjectCurrency,
	  LaborCostBillingCurrency,
	  DirectConsCostBillingCurrency,
	  DirectOtherCostBillingCurrency,
	  ReimbConsCostBillingCurrency,
	  ReimbOtherCostBillingCurrency,
	  InDirectCostBillingCurrency,
	  RegHours,
	  OvtHours,
	  SpecialOvtHours,
	  LaborRegAmt,
	  LaborOvtAmt,
	  LaborSpecialOvtAmt,
	  LaborRegAmtProjectCurrency,
	  LaborOvtAmtProjectCurrency,
	  LaborSpecialOvtAmtProjectCurrency,
	  LaborRegAmtBillingCurrency,
	  LaborOvtAmtBillingCurrency,
	  LaborSpecialOvtAmtBillingCurrency,
	  ModUser,
	  ModDate)
	select WBS1,
		WBS2,
		' ',
		Period,
		min(ChargeType),
		sum(Revenue),
		sum(DirectConsCost),
		sum(DirectOtherCost),
		sum(DirectConsBilling),
		sum(DirectOtherBilling),
		sum(InDirectCost),
		sum(InDirectBilling),
		sum(ReimbConsCost),
		sum(ReimbOtherCost),
		sum(ReimbConsBilling),
		sum(ReimbOtherBilling),
		sum(Hours),
		sum(LaborCost),
		sum(LaborBilling),
		sum(Billed),
		sum(BilledLab),
		sum(BilledCons),
		sum(BilledExp),
		sum(BilledFee),
		sum(BilledUnit),
		sum(BilledAddOn),
		sum(BilledTaxes),
		sum(BilledInterest),
		sum(BilledOther),
		sum(AR),
		sum(Received),
		sum(Overhead),
		sum(RevenueBillingCurrency),
		sum(RevenueProjectCurrency),
		sum(DirectConsCostProjectCurrency),
		sum(DirectOtherCostProjectCurrency),
		sum(ReimbConsCostProjectCurrency),
		sum(ReimbOtherCostProjectCurrency),
		sum(InDirectCostProjectCurrency),
		sum(LaborCostProjectCurrency),
		sum(BilledProjectCurrency),
		sum(BilledLabProjectCurrency),
		sum(BilledConsProjectCurrency),
		sum(BilledExpProjectCurrency),
		sum(BilledFeeProjectCurrency),
		sum(BilledUnitProjectCurrency),
		sum(BilledAddOnProjectCurrency),
		sum(BilledTaxesProjectCurrency),
		sum(BilledInterestProjectCurrency),
		sum(BilledOtherProjectCurrency),
		sum(BilledBillingCurrency),
		sum(BilledLabBillingCurrency),
		sum(BilledConsBillingCurrency),
		sum(BilledExpBillingCurrency),
		sum(BilledFeeBillingCurrency),
		sum(BilledUnitBillingCurrency),
		sum(BilledAddOnBillingCurrency),
		sum(BilledTaxesBillingCurrency),
		sum(BilledInterestBillingCurrency),
		sum(BilledOtherBillingCurrency),
		sum(ARProjectCurrency),
		sum(ARBillingCurrency),
		sum(ReceivedProjectCurrency),
		sum(ReceivedBillingCurrency),
		sum(OverheadProjectCurrency),
		sum(LaborCostBillingCurrency),
		sum(DirectConsCostBillingCurrency),
		sum(DirectOtherCostBillingCurrency),
		sum(ReimbConsCostBillingCurrency),
		sum(ReimbOtherCostBillingCurrency),
		sum(InDirectCostBillingCurrency),
		sum(RegHours),
		sum(OvtHours),
		sum(SpecialOvtHours),
		sum(LaborRegAmt),
		sum(LaborOvtAmt),
		sum(LaborSpecialOvtAmt),
		sum(LaborRegAmtProjectCurrency),
		sum(LaborOvtAmtProjectCurrency),
		sum(LaborSpecialOvtAmtProjectCurrency),
		sum(LaborRegAmtBillingCurrency),
		sum(LaborOvtAmtBillingCurrency),
		sum(LaborSpecialOvtAmtBillingCurrency),
		max(ModUser),
		max(ModDate)
	  	from prsummarySub
	 Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
				and  not exists (select 'x' from prsummarySub tmp 
				where tmp.wbs1 = prsummarySub.wbs1 
				and tmp.wbs2 = prsummarySub.wbs2
				and tmp.wbs3 = N' ' 
				and tmp.Period = prsummarySub.Period)
				group by wbs1,wbs2, Period


	delete from PRSummarySub where 
		WBS1 = @OldWBS1 and 
		WBS2 = @OldWBS2 and 
		WBS3  <> N' '


	Insert into prsummaryMain 
	 (WBS1,
	  WBS2,
	  WBS3,
	  Period,
	  ChargeType,
	  Revenue,
	  Billed,
	  BilledLab,
	  BilledCons,
	  BilledExp,
	  BilledFee,
	  BilledUnit,
	  BilledAddOn,
	  BilledTaxes,
	  BilledInterest,
	  BilledOther,
	  AR,
	  Received,
	  LaborCost,
	  LaborBilling,
	  Unbilled,
	  SpentCostLessOH,
	  SpentBilling,
	  GrossMargin,
	  NetRevenueCost,
	  NetRevenueBilling,
	  ProfitCostLessOH,
	  ProfitBilling,
	  Overhead,
	  RevenueBillingCurrency,
	  RevenueProjectCurrency,
	  ARProjectCurrency,
	  ARBillingCurrency,
	  ReceivedProjectCurrency,
	  ReceivedBillingCurrency,
	  LaborCostProjectCurrency,
	  LaborCostBillingCurrency,
	  SpentCostLessOHProjectCurrency,
	  SpentCostLessOHBillingCurrency,
	  GrossMarginProjectCurrency,
	  GrossMarginBillingCurrency,
	  NetRevenueCostProjectCurrency,
	  NetRevenueCostBillingCurrency,
	  ProfitCostLessOHProjectCurrency,
	  ProfitCostLessOHBillingCurrency,
	  OverheadProjectCurrency,
	  ModUser,
	  ModDate)
	select WBS1,
		WBS2,
		' ',
		Period,
		min(ChargeType),
		sum(Revenue),
		sum(Billed),
		sum(BilledLab),
		sum(BilledCons),
		sum(BilledExp),
		sum(BilledFee),
		sum(BilledUnit),
		sum(BilledAddOn),
		sum(BilledTaxes),
		sum(BilledInterest),
		sum(BilledOther),
		sum(AR),
		sum(Received),
		sum(LaborCost),
		sum(LaborBilling),
		sum(Unbilled),
		sum(SpentCostLessOH),
		sum(SpentBilling),
		sum(GrossMargin),
		sum(NetRevenueCost),
		sum(NetRevenueBilling),
		sum(ProfitCostLessOH),
		sum(ProfitBilling),
		sum(Overhead),
		sum(RevenueBillingCurrency),
		sum(RevenueProjectCurrency),
		sum(ARProjectCurrency),
		sum(ARBillingCurrency),
		sum(ReceivedProjectCurrency),
		sum(ReceivedBillingCurrency),
		sum(LaborCostProjectCurrency),
		sum(LaborCostBillingCurrency),
		sum(SpentCostLessOHProjectCurrency),
		sum(SpentCostLessOHBillingCurrency),
		sum(GrossMarginProjectCurrency),
		sum(GrossMarginBillingCurrency),
		sum(NetRevenueCostProjectCurrency),
		sum(NetRevenueCostBillingCurrency),
		sum(ProfitCostLessOHProjectCurrency),
		sum(ProfitCostLessOHBillingCurrency),
		sum(OverheadProjectCurrency),
		max(ModUser),
		max(ModDate)
	  	from prsummaryMain
	 Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
				and  not exists (select 'x' from prsummaryMain tmp 
				where tmp.wbs1 = prsummaryMain.wbs1 
				and tmp.wbs2 = prsummaryMain.wbs2
				and tmp.wbs3 = N' ' 
				and tmp.Period = prsummaryMain.Period)
				group by wbs1,wbs2, Period

	delete from PRSummaryMain where 
		WBS1 = @OldWBS1 and 
		WBS2 = @OldWBS2 and 
		WBS3  <> N' '


						
Insert into ClendorProjectAssoc 
(  ClientID,
  WBS1,
  Role,
  CreateUser,
  CreateDate,
  ModUser,
  ModDate,
  WBS2,
  WBS3,
  RoleDescription,
  TeamStatus,Address,PrimaryInd,ClientConfidential,ClientInd,VendorInd)
select   ClientID,
  WBS1,
  max(Role),
  max(CreateUser),
  max(CreateDate),
  max(ModUser),
  max(ModDate),
  WBS2,
  ' ',
  max(convert(Nvarchar,RoleDescription)),
  max(TeamStatus), max(Address), max(PrimaryInd), max(ClientConfidential), max(ClientInd), max(VendorInd)
   from ClendorProjectAssoc
 Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
			and  not exists (select 'x' from ClendorProjectAssoc tmp 
			where tmp.wbs1 = ClendorProjectAssoc.wbs1 
			and tmp.wbs2 = ClendorProjectAssoc.wbs2 
			and tmp.wbs3 = N' ' 
			and tmp.ClientID = ClendorProjectAssoc.ClientID)
			group by ClientID,wbs1,wbs2
			
delete from ClendorProjectAssoc where 
	WBS1 = @OldWBS1 and 
	WBS2  = @oldWBS2 and 
	wbs3 <> N' '
	   
Insert into ClendorProjectAssocTemplate 
(  ClientID,
  WBS1,
  WBS2,
  WBS3,
  Role,
  RoleDescription,
  TeamStatus,Address,PrimaryInd,ClientConfidential,ClientInd,VendorInd,
  CreateUser,
  CreateDate,
  ModUser,
  ModDate)
select   ClientID,
  WBS1,
  WBS2,
  ' ',
  max(Role),
  max(convert(Nvarchar,RoleDescription)),
  max(TeamStatus), max(Address), max(PrimaryInd), max(ClientConfidential), max(ClientInd), max(VendorInd),
  max(CreateUser),
  max(CreateDate),
  max(ModUser),
  max(ModDate) 
  from ClendorProjectAssocTemplate
 Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
			and  not exists (select 'x' from ClendorProjectAssocTemplate tmp 
			where tmp.wbs1 = ClendorProjectAssocTemplate.wbs1 
			and tmp.wbs2 = ClendorProjectAssocTemplate.wbs2 
			and tmp.wbs3 = N' ' 
			and tmp.ClientID = ClendorProjectAssocTemplate.ClientID)
			group by ClientID,wbs1,wbs2

delete from ClendorProjectAssocTemplate where 
	WBS1 = @OldWBS1 and 
	WBS2  = @oldWBS2 and 
	wbs3 <> N' '
			Insert into PRProjectCodesTemplate 
			 (WBS1,
			  WBS2,
			  WBS3,
			  ProjectCode,
			  Fees,
			  Seq,
			  CreateUser,
			  CreateDate,
			  ModUser,
			  ModDate)
			select
			  WBS1,
			  WBS2,
			  ' ',
			  ProjectCode,
			  sum(Fees),
			  max(Seq),
			  max(CreateUser),
			  max(CreateDate),
			  max(ModUser),
			  max(ModDate)
			from PRProjectCodesTemplate where 
					PRProjectCodesTemplate.WBS1 = @OldWBS1 and 
					PRProjectCodesTemplate.WBS2 = @OldWBS2 and 
					WBS3 <> N' '
			and not exists (select 'x' from PRProjectCodesTemplate tmp where 
									tmp.WBS1 = PRProjectCodesTemplate.WBS1 
									and tmp.WBS2 = PRProjectCodesTemplate.WBS2 
									and tmp.WBS3 = N' ' 
									and tmp.ProjectCode = PRProjectCodesTemplate.ProjectCode)
			group by WBS1,wbs2, ProjectCode
			
			delete from PRProjectCodesTemplate where PRProjectCodesTemplate.WBS1 = @OldWBS1 and PRProjectCodesTemplate.WBS2  = @oldWBS2 and PRProjectCodesTemplate.wbs3 <> N' '

			delete from prTemplate where wbs2 = @oldWBS2 and wbs1 = @oldwbs1 and wbs3 <> N' '
			delete from PRAdditionalData where wbs2 = @oldWBS2 and wbs1 = @oldwbs1 and wbs3 <> N' '

        Delete FROM ClendorProjectAssoc  where wbs1 = @OldWBS1 and wbs3 <> N' '
					and exists (select 'x' from ClendorProjectAssoc new where 
					new.WBS1 = @NewWBS1
					and new.wbs2 = @OldWBS2
					and new.wbs3 = ClendorProjectAssoc.wbs3
					and new.ClientID = ClendorProjectAssoc.ClientID)

        Delete FROM ClendorProjectAssocTemplate  where wbs1 = @OldWBS1 and wbs3 <> N' '
					and exists (select 'x' from ClendorProjectAssocTemplate new where 
					new.WBS1 = @NewWBS1
					and new.wbs2 = @OldWBS2
					and new.wbs3 = ClendorProjectAssocTemplate.wbs3
					and new.ClientID = ClendorProjectAssocTemplate.ClientID)
/*end 2.0*/			
			delete from reportWBSList where wbs1 = @oldwbs1 and wbs2 = @oldwbs2 and wbs3 <> N' '
			Insert into emProjectAssoc 
			( WBS1,
			  WBS2,
			  WBS3,
			  Employee,
			  Role,
			  RoleDescription,
			  TeamStatus,
			  CreateUser,
			  CreateDate,
			  ModUser,
			  ModDate,
			  RecordID)
			select
			  WBS1,
			  WBS2,
			  ' ',
			  Employee,
			  Role,
			  max(convert(Nvarchar,RoleDescription)),
			  max(TeamStatus),
			  max(CreateUser),
			  max(CreateDate),
			  max(ModUser),
			  max(ModDate),
			  replace(newid(),'-','')
			from emProjectAssoc  Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
			and  not exists (select 'x' from emProjectAssoc tmp where tmp.wbs1 = emProjectAssoc.wbs1 and tmp.wbs2 = emProjectAssoc.wbs2 and tmp.wbs3 = N' ' and tmp.employee = emProjectAssoc.employee and tmp.role = emProjectAssoc.role)
			group by wbs1,wbs2,employee,Role
			
			delete from emProjectAssoc Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
			
			Insert into PRAwards 
			 (WBS1,
			  WBS2,
			  WBS3,
			  RecordID,
			  Description,
			  Institution,
			  AwardDate,
			  CreateUser,
			  CreateDate,
			  ModUser,
			  ModDate)
			select   
			  WBS1,
			  WBS2,
			  ' ',
			  RecordID,
			  max(Description),
			  max(Institution),
			  max(AwardDate),
			  max(CreateUser),
			  max(CreateDate),
			  max(ModUser),
			  max(ModDate) from PRAwards
			where PRAwards.WBS1 = @OldWBS1 and PRAwards.WBS2 = @OldWBS2 and PRAwards.WBS3 <> N' '
			and  not exists (select 'x' from PRAwards tmp where tmp.wbs1 = PRAwards.wbs1 and tmp.wbs2 = PRAwards.WBS2 and tmp.wbs3 = N' ' and tmp.RecordID = PRAwards.RecordID)
			group by wbs1,WBS2,RecordID
			
			delete from PRAwards where PRAwards.WBS1 = @OldWBS1 and PRAwards.WBS2 = @OldWBS2 and PRAwards.WBS3 <> N' '
					
			Insert into PRDescriptions 
			 (WBS1,
			  WBS2,
			  WBS3,
			  DescCategory,
			  Description,
			  CreateUser,
			  CreateDate,
			  ModUser,
			  ModDate)
			select
			  WBS1,
			  WBS2,
			  ' ',
			  DescCategory,
			  max(convert(Nvarchar,Description)),
			  max(CreateUser),
			  max(CreateDate),
			  max(ModUser),
			  max(ModDate)
			from PRDescriptions
			 Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
			and not exists (select 'x' from PRDescriptions tmp where tmp.WBS1 = PRDescriptions.WBS1 and tmp.WBS2 = PRDescriptions.WBS2 and tmp.WBS3 = N' ' and tmp.DescCategory = PRDescriptions.DescCategory)
			group by WBS1,WBS2,DescCategory
			
			delete from PRDescriptions where PRDescriptions.WBS1 = @OldWBS1 and PRDescriptions.WBS2  <> N' '
			
			Insert into PRFileLinks 
			(  LinkID,
			  WBS1,
			  WBS2,
			  WBS3,
			  Description,
			  FilePath,
			  Graphic,
			  CreateUser,
			  CreateDate,
			  ModUser,
			  ModDate)
			select 
			  LinkID,
			  WBS1,
			  WBS2,
			  ' ',
			  max(Description),
			  max(FilePath),
			  max(Graphic),
			  max(CreateUser),
			  max(CreateDate),
			  max(ModUser),
			  max(ModDate)
			from PRFileLinks where PRFileLinks.WBS1 = @OldWBS1 and PRFileLinks.WBS2 = @OldWBS2 and WBS3 <> N' '
			and not exists (select 'x' from PRFileLinks tmp where tmp.WBS1 = PRFileLinks.WBS1 and tmp.WBS2 = PRFileLinks.WBS2 and tmp.WBS3 = N' ' and tmp.LinkID = PRFileLinks.LinkID)
			group by WBS1,WBS2,LinkID
			
			delete from PRFileLinks Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
			
			Insert into PRProjectCodes 
			 (WBS1,
			  WBS2,
			  WBS3,
			  ProjectCode,
			  Fees,
			  Seq,
			  CreateUser,
			  CreateDate,
			  ModUser,
			  ModDate)
			select
			  WBS1,
			  WBS2,
			  ' ',
			  ProjectCode,
			  sum(Fees),
			  max(Seq),
			  max(CreateUser),
			  max(CreateDate),
			  max(ModUser),
			  max(ModDate)
			from PRProjectCodes where PRProjectCodes.WBS1 = @OldWBS1 and PRProjectCodes.WBS2 = @OldWBS2 and WBS3 <> N' '
			and not exists (select 'x' from PRProjectCodes tmp where tmp.WBS1 = PRProjectCodes.WBS1 and tmp.WBS2 = PRProjectCodes.WBS2 and tmp.WBS3 = N' ' and tmp.ProjectCode = PRProjectCodes.ProjectCode)
			group by WBS1,wbs2, ProjectCode
			
			delete from PRProjectCodes Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '

			Insert into billinmaster 
			 (MainWBS1,
			  Invoice,
			  WBS1,
			  WBS2,
			  WBS3,
			  TransDate,
			  TransComment)
			select
			  MainWBS1,
			  Invoice,
			  WBS1,
			  WBS2,
			  ' ',
			  max(TransDate),
			  max(TransComment)
			 from billinmaster where billinmaster.wbs1 = @oldWBS1
				and not exists (select 'x' from billINMaster tmp where tmp.wbs1 = BillINMaster.wbs1 and tmp.WBS2 = tmp.WBS2 and tmp.WBS3 = N' ' and tmp.MainWBS1 = billinmaster.mainWBS1 and  tmp.Invoice = billINMaster.Invoice)
			group by mainWBS1,Invoice,WBS1,WBS2

			delete from billInMaster where billInMaster.WBS1 = @OldWBS1 and billInMaster.WBS2 = @OldWBS2 and billInMaster.WBS3  <> N' '
/*start billINDetail*/

		declare billInCursor cursor for
		select 
			MainWBS1,
			Invoice,
			WBS1,
		   WBS2,
			WBS3,
			PKey
		 from billindetail bid where (wbs1 = @oldWBS1 or MainWBS1 = @oldWBS1) and wbs2 = @oldWBS2
		and exists (select 'x' from billindetail tmp
							where tmp.MainWBS1 = bid.MainWBS1 and
							tmp.Invoice = bid.Invoice and
							tmp.WBS1 = bid.WBS1 and
					      tmp.WBS2 = bid.WBS2 and
							tmp.PKey = bid.Pkey
							group by 
								MainWBS1,
								Invoice,
								WBS1,
								PKey
							having count(*) > 1)
		
		open billInCursor
		fetch next from billinCursor into 
			@MainWBS1,
			@Invoice,
			@Wbs1,
			@WBS2,
			@WBS3,
			@PKey
		
		while (@@Fetch_status = 0)
		 begin
			update billInDetail set pKey = convert(Nvarchar(32),replace(newid(),'-',''))
			where MainWBS1 = @MainWBS1 and
					Invoice = @Invoice and
					WBS1 = @WBS1 and
					WBS2 = @WBS2 and
					WBS3 = @WBS3 and
					Pkey = @Pkey
					
		
			fetch next from billinCursor into 
				@MainWBS1,
				@Invoice,
				@Wbs1,
				@WBS2,
				@WBS3,
				@PKey
		 end
		close billInCursor
		
		deallocate billincursor
/*end billINDetail*/			
			Insert into inMaster 
			 (Batch,
			  Invoice,
			  WBS1,
			  WBS2,
			  WBS3,
			  TransDate,
			  TransComment,
			  Posted,
			  Seq)
			select   
			  Batch,
			  Invoice,
			  WBS1,
			  WBS2,
			  ' ',
			  max(TransDate),
			  max(TransComment),
			  max(Posted),
			  max(Seq) 
			from inMaster
			where inMaster.WBS1 = @OldWBS1 and inMaster.WBS2 = @OldWBS2 and inMaster.WBS3 <> N' '
			and not exists (select 'x' from inMaster tmp where tmp.WBS1 = inMaster.WBS1 and tmp.WBS2 = inMaster.WBS2 and tmp.WBS3 = N' ' and tmp.Batch = inMaster.Batch and tmp.Invoice = inMaster.Invoice)
			group by Batch,Invoice,WBS1,WBS2
			
			delete from inMaster Where WBS1 = @OldWBS1 and WBS2 = @OldWBS2 and WBS3 <> N' '
			IF EXISTS (SELECT 'x' FROM sys.objects WHERE name = 'ProjectCustomTabFields' AND type = 'U')
				begin	
					delete from ProjectCustomTabFields where ProjectCustomTabFields.WBS1 = @OldWBS1 and ProjectCustomTabFields.WBS2  = @oldWBS2 and ProjectCustomTabFields.WBS3 <> N' '
					--and exists (select 'x' from ProjectCustomTabFields new where new.WBS1 = @OldWBS1 and new.wbs2 = @NewWBS2 and new.wbs3 = ProjectCustomTabFields.wbs3)
				end
			
			Insert into prf 
			(  WBS1,
			  WBS2,
			  WBS3,
			  Period,
			  RegOH,
			  GandAOH)
			select 
			  tmp.WBS1,
			  tmp.WBS2,
			  ' ',
			  tmp.Period,
			  0,
			  0 from prf tmp 
			where tmp.wbs1 = @oldWbs1 and tmp.wbs2 = @oldWBS2 and tmp.wbs3 <> N' ' 
			group by tmp.wbs1, tmp.wbs2, tmp.period
			
			update prf
				set PRF.RegOH =  sumRegOH,
				PRF.GandAOH = sumGandAOH,
				 PRF.RegOHProjectCurrency =  sumRegOHProjectCurrency,
				 PRF.GandAOHProjectCurrency =  sumGandAOHProjectCurrency
				from (select wbs1, min(tmp.wbs2) wbs2, min(tmp.wbs3) wbs3, period ,
						sum(tmp.RegOH) as sumRegOH,
						sum(tmp.GandAOH) as sumGandAOH,
						sum(tmp.RegOHProjectCurrency) as sumRegOHProjectCurrency,
						sum(tmp.GandAOHProjectCurrency) as sumGandAOHProjectCurrency
						from prf  tmp
						where wbs1 = @oldWBS1 and WBS2  = @oldWBS2 and  WBS3 <> N' '
						group by WBS1,WBS2,period) as tmp
				inner join prf on tmp.wbs1 = prf.wbs1 and tmp.wbs2 = prf.wbs2 and tmp.period = prf.period 
				where
					prf.wbs3 = N' '
			
			delete from prf where wbs1 = @oldwbs1 and wbs2 = @oldWBS2 and wbs3 <> N' '
			
			Insert into ar 
			 (WBS1,
			  WBS2,
			  WBS3,
			  Invoice,
			  Period,
			  InvoiceDate,
			  LinkCompany,
			  InvBalanceSourceCurrency,
			  DueDate,
			  PaidPeriod)
			select
			  tmp.WBS1,
			  tmp.WBS2,
			  ' ',
			  tmp.Invoice,
			  min(tmp.Period) period,
			  min(tmp.InvoiceDate) InvoiceDate,
			  min(tmp.LinkCompany) LinkCompany,
			  min(tmp.InvBalanceSourceCurrency) InvBalanceSourceCurrency,
			  min(tmp.DueDate) DueDate,
			  min(tmp.PaidPeriod) PaidPeriod 
			from AR as tmp 
			where tmp.WBS1 = @oldWBS1 and tmp.WBS2 = @oldWBS2 and tmp.WBS3 <> N' '
			group by tmp.wbs1,tmp.WBS2,tmp.invoice
			
			delete from AR where  wbs3 <> N' ' and wbs2  = @oldWBS2 and wbs1 = @oldWBS1

/*new for 7.4*/
			Insert into ARPreInvoice 
			 (WBS1,
			  WBS2,
			  WBS3,
			  PreInvoice,
			  Period,
			  InvoiceDate,
			  Cancelled,
			  AppliedInvoice,
			  AppliedPeriod,
			  AppliedPostSeq,
			  Note)
			select
			  tmp.WBS1,
			  tmp.WBS2,
			  ' ',
			  tmp.PreInvoice,
			  min(tmp.Period) period,
			  min(tmp.InvoiceDate) InvoiceDate,
			  min(tmp.Cancelled) Cancelled,
			  min(tmp.AppliedInvoice) AppliedInvoice,
			  min(tmp.AppliedPeriod) AppliedPeriod,
			  min(tmp.AppliedPostSeq) AppliedPostSeq,
			  max(tmp.Note) as Note 
			from ARPreInvoice as tmp 
			where tmp.WBS1 = @oldWBS1 and tmp.WBS2 = @oldWBS2 and tmp.WBS3 <> N' '
			and not exists (select 'x' from ARPreInvoice tmp2 where tmp2.WBS1 = tmp.WBS1 and tmp2.WBS2 = tmp.WBS2 and tmp2.WBS3 = N' ' 
			and tmp2.PreInvoice = tmp.PreInvoice)
			group by tmp.wbs1,tmp.WBS2,tmp.PreInvoice
			delete from ARPreInvoice where  wbs3 <> N' ' and wbs2  = @oldWBS2 and wbs1 = @oldWBS1

			Insert into ARPreInvoiceDetail 
			 (WBS1,
				WBS2,
				WBS3,
				PreInvoice,
				Seq,
				InvoiceSection,
				Amount,
				PaidAmount,
				Account,
				TaxCode,
				TaxBasis,
				WBS2ToPost,
				WBS3ToPost)
			select
			  tmp.WBS1,
			  tmp.WBS2,
			  ' ',
			  tmp.PreInvoice,
			  tmp.Seq,
			  min(tmp.InvoiceSection) InvoiceSection,
			  sum(tmp.Amount) Amount,
			  sum(tmp.PaidAmount) PaidAmount,
			  min(tmp.Account) Account,
			  min(tmp.TaxCode) TaxCode,
			  min(tmp.TaxBasis) TaxBasis,
			  max(tmp.WBS2ToPost) WBS2ToPost,
			  ' ' 
			from ARPreInvoiceDetail as tmp 
			where tmp.WBS1 = @oldWBS1 and tmp.WBS2 = @oldWBS2 and tmp.WBS3 <> N' '
			and not exists (select 'x' from ARPreInvoiceDetail tmp2 where tmp2.WBS1 = tmp.WBS1 and tmp2.WBS2 = tmp.WBS2 and tmp2.WBS3 = N' ' 
			and tmp2.PreInvoice = tmp.PreInvoice and tmp2.Seq = tmp.Seq)
			group by tmp.wbs1,tmp.WBS2,tmp.PreInvoice,tmp.Seq
			delete from ARPreInvoiceDetail where  wbs3 <> N' ' and wbs2  = @oldWBS2 and wbs1 = @oldWBS1

			Insert into BTFPhaseGroupData 
			 (WBS1,
				WBS2,
				WBS3,
				Seq,
				PhaseGroup,
				PrintHeader,
				PrintSubTotal,
				HideDetail,
				AddSpaceAfter)
			select
			  tmp.WBS1,
			  tmp.WBS2,
			  ' ',
			  tmp.Seq,
			  tmp.PhaseGroup,
			  min(tmp.PrintHeader) PrintHeader,
			  min(tmp.PrintSubTotal) PrintSubTotal,
			  min(tmp.HideDetail) HideDetail,
			  min(tmp.AddSpaceAfter) AddSpaceAfter 
			from BTFPhaseGroupData as tmp 
			where tmp.WBS1 = @oldWBS1 and tmp.WBS2 = @oldWBS2 and tmp.WBS3 <> N' '
			and not exists (select 'x' from BTFPhaseGroupData tmp2 where tmp2.WBS1 = tmp.WBS1 and tmp2.WBS2 = tmp.WBS2 and tmp2.WBS3 = N' ' 
			and tmp2.Seq = tmp.Seq and tmp2.PhaseGroup = tmp.PhaseGroup)
			group by tmp.wbs1,tmp.WBS2,tmp.Seq,tmp.PhaseGroup
			delete from BTFPhaseGroupData where wbs3 <> N' ' and wbs2  = @oldWBS2 and wbs1 = @oldWBS1

			Insert into BTFPhaseGroupDescriptions 
			 (WBS1,
				WBS2,
				WBS3,
				Seq,
				PhaseGroup,
				UICultureName,
				Description)
			select
			  tmp.WBS1,
			  tmp.WBS2,
			  ' ',
			  tmp.Seq,
			  tmp.PhaseGroup,
			  tmp.UICultureName,
			  max(tmp.Description) Description
			from BTFPhaseGroupDescriptions as tmp 
			where tmp.WBS1 = @oldWBS1 and tmp.WBS2 = @oldWBS2 and tmp.WBS3 <> N' '
			and not exists (select 'x' from BTFPhaseGroupDescriptions tmp2 where tmp2.WBS1 = tmp.WBS1 and tmp2.WBS2 = tmp.WBS2 and tmp2.WBS3 = N' ' 
			and tmp2.Seq = tmp.Seq and tmp2.PhaseGroup = tmp.PhaseGroup and tmp2.UICultureName = tmp.UICultureName)
			group by tmp.wbs1,tmp.WBS2,tmp.Seq,tmp.PhaseGroup,tmp.UICultureName
			delete from BTFPhaseGroupDescriptions where wbs3 <> N' ' and wbs2  = @oldWBS2 and wbs1 = @oldWBS1
/*end 7.4*/
			
-- Added by DP
			Delete from PRSummaryWBSList where wbs3 <> N' ' and wbs2  = @oldWBS2 and wbs1 = @oldWBS1
--
			Delete from BTA where wbs3 <> N' ' and wbs2  = @oldWBS2 and wbs1 = @oldWBS1
			Delete from BTF where wbs3 <> N' ' and wbs2  = @oldWBS2 and wbs1 = @oldWBS1
			Delete from BTSchedule where wbs3 <> N' ' and wbs2  = @oldWBS2 and wbs1 = @oldWBS1
			Delete from BTTaxCodes where wbs3 <> N' ' and wbs2  = @oldWBS2 and wbs1 = @oldWBS1
			Delete from BT where wbs3 <> N' ' and wbs2  = @oldWBS2 and wbs1 = @oldWBS1
			/*MJU BT changes Per DAK 1/15/03*/
			UPDATE BT SET BT.SubLevelTerms = 'N',
			ConsolidatePrinting = 'N',
			ConsolidatePosting = 'N',
			WBS2ToPost = null,
			wbs3toPost = null,
			PreInvWBS2ToPost = null,
			PreInvwbs3toPost = null,
			SubLevelRollup =0,
			LimitsBySublevel = 'N',
			InterestBySubLevel = 'N',
			TaxBySubLevel = 'N',
			RetainageBySubLevel = 'N'
			Where WBS3 = N' ' and WBS2 = @oldWBS2 and WBS1 = @OldWBS1
			

			UPDATE BT SET 
			PrintBTDBreakout = 'N',
			PrintARWBS2Totals = 'N',
			PrintBTDWBS2Totals = 'N',
			PrintARBreakout = 'N'
			Where WBS3 = N' ' and WBS2 = N' ' and WBS1 = @OldWBS1

			--MJU Change billINMaster to work like INMaster per DAK
			--Delete from billINMaster where wbs3 <> ' ' and wbs2  = @oldWBS2 and wbs1 = @oldWBS1
			--Delete from billINDetail where wbs3 <> ' ' and wbs2  = @oldWBS2 and wbs1 = @oldWBS1

			delete from billInvMaster where MainWBS1 = @OldWBS1 and BillWBS2 = @OldWbs2 and BillWBS3 <> N' '
/*added 3.0 sp5 */
			delete from ICBillInvMaster where MainWBS1 = @OldWBS1 and BillWBS2 = @OldWbs2 and BillWBS3 <> N' '
			
			Insert into lb 
			(  WBS1,
			  WBS2,
			  WBS3,
			  LaborCode,
			  Pct,
			  Rate,
			  BillRate,
			  HrsBud,
			  AmtBud,
			  BillBud,
			  StartDate,
			  EndDate,
			  EtcHrs,
			  EtcAmt,
			  EacHrs,
			  EacAmt)
			select 
				tmp.WBS1,
				tmp.WBS2,
				' ',
				tmp.LaborCode,
				min(tmp.Pct),
				min(tmp.Rate),
				min(tmp.BillRate),
				0,
				0,
				0,
				min(tmp.StartDate),
				min(tmp.EndDate),
				0,
				0,
				0,
				0 
			from lb tmp 
			where tmp.wbs1 = @oldWBS1 and tmp.WBS2  = @oldWBS2 and tmp.WBS3 <> N' ' 
			group by tmp.WBS1,tmp.WBS2,tmp.laborCode
			
			update LB
				set LB.AmtBud =  sumAmtBud,
				LB.BillBud = sumBillBud,
				LB.EtcAmt = sumEtcAmt,
				LB.EacAmt = sumEacAmt
				from (select wbs1, wbs2, min(tmp.wbs3) wbs3, LaborCode, 
						sum(tmp.AmtBud) as sumAmtBud,
						sum(tmp.BillBud) as sumBillBud,
						sum(tmp.EtcAmt) as sumEtcAmt,
						sum(tmp.EacAmt) as sumEacAmt
						from lb  tmp
						where tmp.wbs1 = @oldWBS1 and tmp.WBS2 = @oldWBS2 and tmp.WBS3 <> N' '
						group by tmp.WBS1,tmp.WBS2,tmp.laborCode) as tmp
				inner join lb on tmp.wbs1 = LB.wbs1 and tmp.wbs2 = LB.wbs2 and tmp.laborcode = LB.LaborCode
				where
					 LB.wbs3 = N' '
			
			delete from lb where WBS1 = @oldWBS1 and WBS2  = @oldWBS2 and WBS3 <> N' '
			
			Insert into eb 
			( WBS1,
			  WBS2,
			  WBS3,
			  Account,
			  Vendor,
			  Pct,
			  AmtBud,
			  BillBud,
			  StartDate,
			  EndDate,
			  EtcAmt,
			  EacAmt)
			select
				tmp.WBS1,
				tmp.WBS2,
				' ',
				tmp.Account,
				tmp.Vendor,
				min(tmp.Pct) Pct,
				min(tmp.AmtBud) AmtBud,
				min(tmp.BillBud) BillBud,
				min(tmp.StartDate) StartDate,
				min(tmp.EndDate) EndDate,
				min(tmp.EtcAmt) EtcAmt,
				min(tmp.EacAmt) EacAmt
			from EB tmp
			where tmp.wbs1 = @oldWBS1 and tmp.WBS2 = @oldWBS2 and tmp.WBS3 <> N' ' 
			group by tmp.WBS1, tmp.WBS2, tmp.Account, tmp.Vendor
			
			update EB
				set EB.AmtBud =  sumAmtBud,
				EB.BillBud = sumBillBud,
				EB.EtcAmt = sumEtcAmt,
				EB.EacAmt = sumEacAmt
				from (select wbs1, wbs2, min(tmp.wbs3) wbs3, Account,Vendor, 
						sum(tmp.AmtBud) as sumAmtBud,
						sum(tmp.BillBud) as sumBillBud,
						sum(tmp.EtcAmt) as sumEtcAmt,
						sum(tmp.EacAmt) as sumEacAmt
						from eb  tmp
						where wbs1 = @oldWBS1 and WBS2 = @oldWBS2 and WBS3 <> N' '
						group by WBS1,WBS2,Account,Vendor) as tmp
				inner join eb on tmp.wbs1 = eb.wbs1 and tmp.wbs2 = eb.wbs2 and tmp.vendor = eb.vendor and tmp.account = eb.account
				where
					eb.wbs3 = N' '
			
			delete from eb where wbs1 = @OldWBs1 and WBS2 = @oldWBS2 and WBS3 <> N' '


--Reset Contracts.FeeIncludeInd to 'N'
			Update Contracts set FeeIncludeInd='N'
			from Contracts C 
			Where C.WBS1=@OldWBs1 and C.FeeIncludeInd='Y'

			if (Select SyncProjToContractFees from FW_CFGSystem) = 'Y'
				update PR set Fee = 0, ConsultFee = 0, ReimbAllow = 0, FeeBillingCurrency = 0, ConsultFeeBillingCurrency =0, 
					ReimbAllowBillingCurrency =0,   FeeFunctionalCurrency = 0, ConsultFeeFunctionalCurrency = 0, ReimbAllowFunctionalCurrency = 0 , 
					FeeDirLab = 0 , FeeDirExp = 0, ReimbAllowExp = 0 , ReimbAllowCons = 0, 
					FeeDirLabBillingCurrency = 0 , FeeDirExpBillingCurrency = 0, ReimbAllowExpBillingCurrency = 0 , ReimbAllowConsBillingCurrency = 0,   
					FeeDirLabFunctionalCurrency = 0 , FeeDirExpFunctionalCurrency = 0, ReimbAllowExpFunctionalCurrency = 0 , ReimbAllowConsFunctionalCurrency = 0  
					where WBS1 in (@OldWBs1)
--
-- RonK 07/15/09: Update ContractDetails for Rubik re: turning off Tasks for a Phase
			-- Must create a summarized Phase ContractDetails record for all Task records, if any.
			declare @taskCount as Integer
			set @taskCount = (select count(*) from ContractDetails where wbs1 = @OldWBs1 and WBS2 = @oldWBS2 and WBS3 <> N' ')
			If (@taskCount = 1)
			begin
				-- If none, do nothing. If one, just remove the Task ID.
				update ContractDetails set wbs3 = N' ' 
					from ContractDetails old
					where wbs1 = @OldWBs1 and WBS2 = @oldWBS2 and WBS3 <> N' '
					and not exists(select 'x' from ContractDetails tmp where
							tmp.wbs1 = old.wbs1 
							and tmp.wbs2 = old.wbs2
							and tmp.wbs3 = N' '
							and tmp.ContractNumber = old.ContractNumber)
			end
			else if (@taskCount > 1)
			begin
				-- More than one, so must merge task items at WBS2 and then remove them.
				Insert into ContractDetails (ContractNumber, WBS1, WBS2, WBS3, Fee, ReimbAllow, ConsultFee, Total, 
							FeeBillingCurrency, ReimbAllowBillingCurrency, ConsultFeeBillingCurrency, TotalBillingCurrency,
							FeeFunctionalCurrency, ReimbAllowFunctionalCurrency, ConsultFeeFunctionalCurrency, TotalFunctionalCurrency,
							FeeDirLab, FeeDirExp, ReimbAllowExp, ReimbAllowCons, FeeDirLabBillingCurrency, FeeDirExpBillingCurrency,
							ReimbAllowExpBillingCurrency, ReimbAllowConsBillingCurrency, FeeDirLabFunctionalCurrency, 
							FeeDirExpFunctionalCurrency, ReimbAllowExpFunctionalCurrency, ReimbAllowConsFunctionalCurrency,
							CreateUser, CreateDate, ModUser, ModDate) 
						SELECT ContractNumber, WBS1, WBS2, N' ', Sum(Fee), Sum(ReimbAllow), Sum(ConsultFee), Sum(Total), 
							Sum(FeeBillingCurrency), Sum(ReimbAllowBillingCurrency), Sum(ConsultFeeBillingCurrency), Sum(TotalBillingCurrency),
							Sum(FeeFunctionalCurrency), Sum(ReimbAllowFunctionalCurrency), Sum(ConsultFeeFunctionalCurrency), Sum(TotalFunctionalCurrency),
							Sum(FeeDirLab), Sum(FeeDirExp), Sum(ReimbAllowExp), Sum(ReimbAllowCons), Sum(FeeDirLabBillingCurrency), Sum(FeeDirExpBillingCurrency),
							Sum(ReimbAllowExpBillingCurrency), Sum(ReimbAllowConsBillingCurrency), Sum(FeeDirLabFunctionalCurrency), 
							Sum(FeeDirExpFunctionalCurrency), Sum(ReimbAllowExpFunctionalCurrency), Sum(ReimbAllowConsFunctionalCurrency),
							Max(CreateUser), Max(CreateDate), Max(ModUser), Max(ModDate)
						from ContractDetails old Where old.WBS1 = @OldWBS1 and old.WBS2 = @oldWBS2 and old.WBS3 <> N' '
						and not exists(select 'x' from ContractDetails tmp where
							 tmp.wbs1 = old.wbs1 
							 and tmp.wbs2 = old.wbs2
							 and tmp.wbs3 = N' '
							 and tmp.ContractNumber = old.ContractNumber)
						group by ContractNumber, WBS1, WBS2	
				
			end
			delete from ContractDetails where wbs1 = @OldWBs1 and WBS2 = @oldWBS2 and WBS3 <> N' '
-- RonK 07/15/09 End mod


			Insert into PRChargeCompanies 
			 (WBS1,
			  WBS2,
			  WBS3,
			  Company,
			  CreateUser,
			  CreateDate,
			  ModUser,
			  ModDate
			  )
			select
			  tmp.WBS1,
			  tmp.WBS2,
			  ' ',
			  tmp.Company,
			  max(CreateUser),
			  max(CreateDate),
			  max(ModUser),
			  max(ModDate)
			from PRChargeCompanies as tmp 
			where tmp.WBS1 = @oldWBS1 and tmp.WBS2 = @oldWBS2 and tmp.WBS3 <> N' '
			group by tmp.wbs1,tmp.WBS2,tmp.Company

	        Delete FROM PRChargeCompanies WHERE PRChargeCompanies.WBS1 = @OldWBS1 and PRChargeCompanies.WBS2 = @oldWBS2 and PRChargeCompanies.WBS3 <> N' '
			
		end --Turning off tasks for phase


		if (isNull(@OldWBS2,' ') = ' ')
		  begin
			 set @Entity = 'WBS2'
			 set @OldWBS2 = '.task.'
			 set @NewWBS2 = ' '
			 set @OldWBS3 = null
			 set @NewWBS3 = null
		  end
		else
		  begin
			 set @Entity = 'WBS3'
			 --set @OldWBS2 = '.task.'
			 --set @NewWBS2 = ' '
			 set @OldWBS3 = '.task.'
			 set @NewWBS3 = ' '				
		  end

		exec @RetVal = KeyCvt @Entity = @Entity,
									 @CvtType = @CvtType,
									 @Length = 20,
									 @ErrMsg = @ErrMsg2 output,
									 @Diag = @Diag,
									 @NewValue = @NewWBS1,
									 @NewValue2 = @NewWBS2,
									 @NewValue3 = @NewWBS3,
									 @OldValue = @OldWBS1,
									 @OldValue2 = @OldWBS2,  --Equivilant of UpdateTables in Advantage
									 @OldValue3 = @OldWBS3,
									 @ConstrFlag = 3 --Update tables

		if (@RetVal = 0)
			begin
				declare @newOutline	integer
				set @newOutline = 0
				if (@OldWBS2 = '.task.') --turning off phases for project
				begin
					-- Move the non-WBS2 (Labor Code) tasks to upper level
					set @TaskFetch = 0
					declare TaskCursor1 cursor for
						select PlanID, TaskID from PNTask where wbs1=@oldWbs1 and wbs2 is not null and WBSType<>'WBS2' order by OutlineNumber
					open TaskCursor1
					fetch next from TaskCursor1 into 
						@PlanID,
						@OldTaskID
					set @TaskFetch = @@Fetch_Status

					While (@TaskFetch = 0)
						begin
							set @newOutline = @newOutline + 1
							Update PNTask Set WBS2=null, WBS3=null, OutlineLevel = OutlineLevel-1, ParentOutlineNumber=Left(ParentOutlineNumber, 3)
							, OutlineNumber = Left(ParentOutlineNumber, 3)+'.'+right('000'+convert(varchar, @newOutline),3)
							Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPTask Set WBS2=null, WBS3=null, OutlineLevel = OutlineLevel-1, ParentOutlineNumber=Left(ParentOutlineNumber, 3)
							, OutlineNumber = Left(ParentOutlineNumber, 3)+'.'+right('000'+convert(varchar, @newOutline),3)
							Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPAssignment Set WBS2=null, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPExpense Set WBS2=null, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPConsultant Set WBS2=null, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPUnit Set WBS2=null, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNAssignment Set WBS2=null, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNExpense Set WBS2=null, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNConsultant Set WBS2=null, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID

							fetch next from TaskCursor1 into 
								@PlanID,
								@OldTaskID
							set @TaskFetch = @@Fetch_Status
						end
					close TaskCursor1
					deallocate TaskCursor1

					-- Move the WBS2 tasks to upper level
					set @TaskFetch = 0
					declare TaskCursor2 cursor for
						select PlanID, TaskID from PNTask where wbs1=@oldWbs1 and wbs2 is not null and WBSType='WBS2'
					open TaskCursor2
					fetch next from TaskCursor2 into 
						@PlanID,
						@OldTaskID
					set @TaskFetch = @@Fetch_Status

					While (@TaskFetch = 0)
						begin
							Set @NewTaskID = (Select TaskID from PNTask where wbs1=@oldWbs1 and wbs2 is null and WBSType='WBS1')
					
							Update RPAssignment Set TaskID=@NewTaskID, WBS2=null, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPPlannedLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPBaselineLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPBaselineRevenue Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPExpense Set TaskID=@NewTaskID, WBS2=null, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPPlannedExpenses Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPBaselineExpenses Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPConsultant Set TaskID=@NewTaskID, WBS2=null, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPPlannedConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPBaselineConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPUnit Set TaskID=@NewTaskID, WBS2=null, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPPlannedUnit Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPBaselineUnit Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID

							Update PNAssignment Set TaskID=@NewTaskID, WBS2=null, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNPlannedLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNBaselineLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNExpense Set TaskID=@NewTaskID, WBS2=null, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNPlannedExpenses Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNBaselineExpenses Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNConsultant Set TaskID=@NewTaskID, WBS2=null, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNPlannedConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNBaselineConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID

							Update RPPlannedRevenueLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPBaselineRevenueLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNPlannedRevenueLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNBaselineRevenueLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID

							PRINT '>>> Start Processing PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '"'
							EXECUTE dbo.stRPDelTask @PlanID, @OldTaskID
							PRINT '<<< End Processing PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '"'

							fetch next from TaskCursor2 into 
								@PlanID,
								@OldTaskID
							set @TaskFetch = @@Fetch_Status
						end
					close TaskCursor2
					deallocate TaskCursor2

					set @RevType = 'B'
					select @RevType = isNull(revType,'B') from pr where WBS2 = N' ' and wbs1 = @oldWBS1 group by revType having count(*) = 1
					set @tmpWBS2 = ' '
					update pr set revType = @revType, subLevel = 'N' where wbs2 = N' ' and wbs1 = @oldwbs1
					delete from pr where wbs2 <> N' ' and wbs1 = @oldwbs1

					set @OldWBS2 = '*'
					set @OldName2 = 'All ' + @CustLabelPhasePlural
					set @OldName3 = ''
					set @NewWBS2 = ''
					set @NewName2 = ''
					set @NewWBS3 = ''
					set @NewName3 = ''

				end
				else if (isNull(@oldWBS2,' ') <> ' ') --Turning off tasks for phase
				begin
					-- Move the non-WBS3 (Labor Code) tasks to upper level
					set @TaskFetch = 0
					declare TaskCursor1 cursor for
						select PlanID, TaskID from PNTask where wbs1=@oldWbs1 and wbs2=@oldwbs2 and wbs3 is not null and WBSType<>'WBS3' order by OutlineNumber
					open TaskCursor1
					fetch next from TaskCursor1 into 
						@PlanID,
						@OldTaskID
					set @TaskFetch = @@Fetch_Status

					While (@TaskFetch = 0)
						begin
							set @newOutline = @newOutline + 1
							Update PNTask Set WBS3=null, OutlineLevel = OutlineLevel-1, ParentOutlineNumber=Left(ParentOutlineNumber, 7)
							, OutlineNumber = Left(ParentOutlineNumber, 7)+'.'+right('000'+convert(varchar, @newOutline),3)
							Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPTask Set WBS3=null, OutlineLevel = OutlineLevel-1, ParentOutlineNumber=Left(ParentOutlineNumber, 7)
							, OutlineNumber = Left(ParentOutlineNumber, 7)+'.'+right('000'+convert(varchar, @newOutline),3)
							Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPAssignment Set WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPExpense Set WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPConsultant Set WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPUnit Set WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNAssignment Set WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNExpense Set WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNConsultant Set WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID

							fetch next from TaskCursor1 into 
								@PlanID,
								@OldTaskID
							set @TaskFetch = @@Fetch_Status
						end
					close TaskCursor1
					deallocate TaskCursor1

					-- Move the WBS3 tasks to upper level
					set @TaskFetch = 0
					declare TaskCursor2 cursor for
						select PlanID, TaskID from PNTask where wbs1=@oldWbs1 and wbs2=@oldwbs2 and wbs3 is not null and WBSType='WBS3'
					open TaskCursor2
					fetch next from TaskCursor2 into 
						@PlanID,
						@OldTaskID
					set @TaskFetch = @@Fetch_Status

					While (@TaskFetch = 0)
						begin
							Set @NewTaskID = (Select TaskID from PNTask where wbs1=@oldWbs1 and wbs2=@oldwbs2 and wbs3 is null and WBSType='WBS2')
					
							Update RPAssignment Set TaskID=@NewTaskID, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPPlannedLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPBaselineLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPBaselineRevenue Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPExpense Set TaskID=@NewTaskID, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPPlannedExpenses Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPBaselineExpenses Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPConsultant Set TaskID=@NewTaskID, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPPlannedConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPBaselineConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPUnit Set TaskID=@NewTaskID, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPPlannedUnit Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPBaselineUnit Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID

							Update PNAssignment Set TaskID=@NewTaskID, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNPlannedLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNBaselineLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNExpense Set TaskID=@NewTaskID, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNPlannedExpenses Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNBaselineExpenses Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNConsultant Set TaskID=@NewTaskID, WBS3=null Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNPlannedConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNBaselineConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID

							Update RPPlannedRevenueLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update RPBaselineRevenueLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNPlannedRevenueLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
							Update PNBaselineRevenueLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID

							PRINT '>>> Start Processing PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '"'
							EXECUTE dbo.stRPDelTask @PlanID, @OldTaskID
							PRINT '<<< End Processing PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '"'

							fetch next from TaskCursor2 into 
								@PlanID,
								@OldTaskID
							set @TaskFetch = @@Fetch_Status
						end
					close TaskCursor2
					deallocate TaskCursor2

					set @RevType = 'B'
					select @RevType = isNull(revType,'B') from pr where WBS3 <> N' ' and WBS2 = @oldWBS2 and wbs1 = @oldWBS1 group by revType having count(*) = 1
					if (@Diag = 1)
						print 'update pr set revType = @revType, subLevel = ''N'' where  WBS3 = N'' '' and wbs2 =N''' + @oldWBS2 + ''' and wbs1 =N''' + @oldwbs1 + ''''
					update pr set revType = @revType, subLevel = 'N' where  WBS3 = N' ' and wbs2 = @oldWBS2 and wbs1 = @oldwbs1
					delete from pr where WBS3 <> N' ' and wbs2 = @oldWBS2 and wbs1 = @oldwbs1

					set @OldWBS3 = '*'
					set @OldName3 = 'All ' + @Custlabelplural
					set @NewWBS2 = @OldWBS2
					set @NewName2 = @OldName2
					set @NewWBS3 = ''
					set @NewName3 = ''
				end


				declare @NewPlanID varchar(32)
				select @NewPlanID=PlanID from RPPlan where wbs1=@NewWBS1
				PRINT '>>> Start Processing PlanID = "' + @NewPlanID + '", WBS1 = "' + @NewWBS1 + '"'
				Update RPTask Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update RPAssignment Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update RPExpense Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update RPConsultant Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update RPUnit Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update PNTask Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update PNAssignment Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update PNExpense Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update PNConsultant Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 

							
--*****************************************************************************************************************
-- ****************************************
-- check duplicate tasks including labor code and merge them
-- ****************************************

				declare @TempFetch integer
				declare @TempFetch2 integer
				declare @LaborCode	Nvarchar(14),
						@GenericResourceID		Nvarchar(20),
						@WBSType	Nvarchar(4)
				declare @Account		Nvarchar(13),
						@Vendor		Nvarchar(20),
						@OldExpenseID	varchar(32),
						@NewExpenseID	varchar(32)
				declare @OldConsultantID	varchar(32),
						@NewConsultantID	varchar(32)
				declare @OldUnitID	varchar(32),
						@NewUnitID	varchar(32),
						@Unit		Nvarchar(30)

				declare TaskCursor cursor for
					select A.PlanID, A.WBS1, A.WBS2, A.WBS3, A.LaborCode, A.WBSType, PNTask.TaskID as TaskID From
					(
					select PlanID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, isnull(WBSType,'') as WBSType, MIN(OutlineNumber) as OutlineNumber  
					from PNTask where WBS1=@NewWBS1
					group by PlanID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(LaborCode,''), isnull(WBSType,'')
					Having count(TaskID) > 1
					) A
					Left Join PNTask on PNTask.PlanID=A.PlanID and PNTask.WBS1=A.WBS1 and isnull(PNTask.WBS2,'')=A.WBS2 and isnull(PNTask.WBS3,'')=A.WBS3 and isnull(PNTask.LaborCode,'')=A.LaborCode 
					and isnull(PNTask.WBSType,'')=A.WBSType and PNTask.OutlineNumber=A.OutlineNumber 

				open TaskCursor
				fetch next from TaskCursor into @PlanID, @WBS1, @WBS2, @WBS3, @LaborCode, @WBSType, @NewTaskID
				set @TempFetch = @@Fetch_Status
				While (@TempFetch = 0)
				begin
if (@diag = 1) Print 'Check the duplicate Tasks ' + @PlanID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+@WBSType
					declare DupTaskCursor cursor for
						select TaskID from PNTask 
						where PlanID=@PlanID and WBS1=@WBS1 
						and isnull(WBS2,'')=@WBS2 
						and isnull(WBS3,'')=@WBS3 
						and isnull(LaborCode,'')=@LaborCode 
						and isnull(WBSType,'')=@WBSType
						and TaskID <> @NewTaskID 
					open DupTaskCursor
					fetch next from DupTaskCursor into @OldTaskID
					set @TempFetch2 = @@Fetch_Status
					While (@TempFetch2 = 0)
					begin
if (@diag = 1) Print 'Merging the duplicate Tasks ' + @OldTaskID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+@WBSType
						Update RPAssignment Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPPlannedLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPBaselineLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPBaselineRevenue Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPExpense Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPPlannedExpenses Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPBaselineExpenses Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPPlannedConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPBaselineConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPUnit Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPPlannedUnit Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPBaselineUnit Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID

						Update PNAssignment Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNPlannedLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNBaselineLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNExpense Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNPlannedExpenses Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNBaselineExpenses Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNPlannedConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNBaselineConsultant Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID

						Update RPPlannedRevenueLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update RPBaselineRevenueLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNPlannedRevenueLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID
						Update PNBaselineRevenueLabor Set TaskID=@NewTaskID Where PlanID=@PlanID and TaskID=@OldTaskID

						PRINT '>>> Start Processing PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '"'
						EXECUTE dbo.stRPDelTask @PlanID, @OldTaskID
						PRINT '<<< End Processing PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '"'

						fetch next from DupTaskCursor into @OldTaskID
						set @TempFetch2 = @@Fetch_Status
					end
					close DupTaskCursor
					deallocate DupTaskCursor

					fetch next from TaskCursor into @PlanID, @WBS1, @WBS2, @WBS3, @LaborCode, @WBSType, @NewTaskID
					set @TempFetch = @@Fetch_Status
				end
				close TaskCursor
				deallocate TaskCursor

-- Move Planned and Basedline for the duplicate RPAssignment/PNAssignment, Delete duplicate RPAssignment/PNAssignment
				declare AssignmentCursor cursor for
					select PlanID, TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, isnull(ResourceID,'') as ResourceID
					, isnull(GenericResourceID,'') as GenericResourceID, MIN(AssignmentID) as AssignmentID  
					from RPAssignment where WBS1=@NewWBS1
					group by PlanID, TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(LaborCode,''), isnull(ResourceID,''), isnull(GenericResourceID,'')
					Having count(AssignmentID) > 1
				open AssignmentCursor
				fetch next from AssignmentCursor into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @LaborCode, @ResourceID, @GenericResourceID, @NewAssignmentID
				set @TempFetch = @@Fetch_Status
				While (@TempFetch = 0)
				begin
if (@diag = 1) Print 'Check the duplicate Assignment ' + @PlanID +'/' + @OldTaskID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+@ResourceID +'/'+@GenericResourceID
					declare DupAssignmentCursor cursor for
						select AssignmentID from RPAssignment 
						where PlanID=@PlanID and TaskID=@OldTaskID and WBS1=@WBS1 
						and isnull(WBS2,'')=@WBS2 
						and isnull(WBS3,'')=@WBS3 
						and isnull(LaborCode,'')=@LaborCode 
						and isnull(ResourceID,'')=@ResourceID
						and isnull(GenericResourceID,'')=@GenericResourceID
						and AssignmentID <> @NewAssignmentID
					open DupAssignmentCursor
					fetch next from DupAssignmentCursor into @OldAssignmentID
					set @TempFetch2 = @@Fetch_Status
					While (@TempFetch2 = 0)
					begin
if (@diag = 1) Print 'Merging the duplicate Assignment ' + @PlanID +'/' + @OldTaskID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+@ResourceID +'/'+@GenericResourceID
						Update RPPlannedLabor Set AssignmentID=@NewAssignmentID Where PlanID=@PlanID and TaskID=@OldTaskID and AssignmentID=@OldAssignmentID
						Update RPBaselineLabor Set AssignmentID=@NewAssignmentID Where PlanID=@PlanID and TaskID=@OldTaskID and AssignmentID=@OldAssignmentID
						Update RPBaselineRevenue Set AssignmentID=@NewAssignmentID Where PlanID=@PlanID and TaskID=@OldTaskID and AssignmentID=@OldAssignmentID

						Update PNPlannedLabor Set AssignmentID=@NewAssignmentID Where PlanID=@PlanID and TaskID=@OldTaskID and AssignmentID=@OldAssignmentID
						Update PNBaselineLabor Set AssignmentID=@NewAssignmentID Where PlanID=@PlanID and TaskID=@OldTaskID and AssignmentID=@OldAssignmentID

						PRINT '>>> Start Deleting Assignment PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '", AssignmentID = "' + @OldAssignmentID + '"'
						delete RPAssignment Where PlanID=@PlanID and TaskID=@OldTaskID and AssignmentID=@OldAssignmentID
						delete PNAssignment Where PlanID=@PlanID and TaskID=@OldTaskID and AssignmentID=@OldAssignmentID
						PRINT '<<< End Deleting Assignment PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '", AssignmentID = "' + @OldAssignmentID + '"'

						fetch next from DupAssignmentCursor into @OldAssignmentID
						set @TempFetch2 = @@Fetch_Status
					end
					close DupAssignmentCursor
					deallocate DupAssignmentCursor

					fetch next from AssignmentCursor into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @LaborCode, @ResourceID, @GenericResourceID, @NewAssignmentID
					set @TempFetch = @@Fetch_Status
				end
				close AssignmentCursor
				deallocate AssignmentCursor

-- Move Planned and Basedline for the duplicate RPExpense/PNExpense, Delete duplicate RPExpense/PNExpense
				declare ExpenseCursor cursor for
					select PlanID, TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Account,'') as Account, isnull(Vendor,'') as Vendor, MIN(ExpenseID) as ExpenseID  
					from RPExpense where WBS1=@NewWBS1
					group by PlanID, TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
					Having count(ExpenseID) > 1
				open ExpenseCursor
				fetch next from ExpenseCursor into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @NewExpenseID
				set @TempFetch = @@Fetch_Status
				While (@TempFetch = 0)
				begin
if (@diag = 1) Print 'Check the duplicate Expense ' + @PlanID +'/' + @OldTaskID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @Account +'/'+@Vendor
					declare DupExpenseCursor cursor for
						select ExpenseID from RPExpense 
						where PlanID=@PlanID and TaskID=@OldTaskID and WBS1=@WBS1 
						and isnull(WBS2,'')=@WBS2 
						and isnull(WBS3,'')=@WBS3 
						and isnull(Account,'')=@Account 
						and isnull(Vendor,'')=@Vendor
						and ExpenseID <> @NewExpenseID
					open DupExpenseCursor
					fetch next from DupExpenseCursor into @OldExpenseID
					set @TempFetch2 = @@Fetch_Status
					While (@TempFetch2 = 0)
					begin
if (@diag = 1) Print 'Merging the duplicate Expense ' + @PlanID +'/' + @OldTaskID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @Account +'/'+@Vendor
						Update RPPlannedExpenses Set ExpenseID=@NewExpenseID Where PlanID=@PlanID and TaskID=@OldTaskID and ExpenseID=@OldExpenseID
						Update RPBaselineExpenses Set ExpenseID=@NewExpenseID Where PlanID=@PlanID and TaskID=@OldTaskID and ExpenseID=@OldExpenseID

						Update PNPlannedExpenses Set ExpenseID=@NewExpenseID Where PlanID=@PlanID and TaskID=@OldTaskID and ExpenseID=@OldExpenseID
						Update PNBaselineExpenses Set ExpenseID=@NewExpenseID Where PlanID=@PlanID and TaskID=@OldTaskID and ExpenseID=@OldExpenseID

						PRINT '>>> Start Deleting Expense PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '", ExpenseID = "' + @OldExpenseID + '"'
						delete RPExpense Where PlanID=@PlanID and TaskID=@OldTaskID and ExpenseID=@OldExpenseID
						delete PNExpense Where PlanID=@PlanID and TaskID=@OldTaskID and ExpenseID=@OldExpenseID
						PRINT '<<< End Deleting Expense PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '", ExpenseID = "' + @OldExpenseID + '"'

						fetch next from DupExpenseCursor into @OldExpenseID
						set @TempFetch2 = @@Fetch_Status
					end
					close DupExpenseCursor
					deallocate DupExpenseCursor

					fetch next from ExpenseCursor into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @NewExpenseID
					set @TempFetch = @@Fetch_Status
				end
				close ExpenseCursor
				deallocate ExpenseCursor

-- Move Planned and Basedline for the duplicate RPConsultant/PNConsultant, Delete duplicate RPConsultant/PNConsultant
				declare ConsultantCursor cursor for
					select PlanID, TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Account,'') as Account, isnull(Vendor,'') as Vendor, MIN(ConsultantID) as ConsultantID  
					from RPConsultant where WBS1=@NewWBS1
					group by PlanID, TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
					Having count(ConsultantID) > 1
				open ConsultantCursor
				fetch next from ConsultantCursor into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @NewConsultantID
				set @TempFetch = @@Fetch_Status
				While (@TempFetch = 0)
				begin
if (@diag = 1) Print 'Check the duplicate Consultant ' + @PlanID +'/' + @OldTaskID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @Account +'/'+@Vendor
					declare DupConsultantCursor cursor for
						select ConsultantID from RPConsultant 
						where PlanID=@PlanID and TaskID=@OldTaskID and WBS1=@WBS1 
						and isnull(WBS2,'')=@WBS2 
						and isnull(WBS3,'')=@WBS3 
						and isnull(Account,'')=@Account 
						and isnull(Vendor,'')=@Vendor
						and ConsultantID <> @NewConsultantID
					open DupConsultantCursor
					fetch next from DupConsultantCursor into @OldConsultantID
					set @TempFetch2 = @@Fetch_Status
					While (@TempFetch2 = 0)
					begin
if (@diag = 1) Print 'Merging the duplicate Consultant ' + @PlanID +'/' + @OldTaskID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @Account +'/'+@Vendor
						Update RPPlannedConsultant Set ConsultantID=@NewConsultantID Where PlanID=@PlanID and TaskID=@OldTaskID and ConsultantID=@OldConsultantID
						Update RPBaselineConsultant Set ConsultantID=@NewConsultantID Where PlanID=@PlanID and TaskID=@OldTaskID and ConsultantID=@OldConsultantID

						Update PNPlannedConsultant Set ConsultantID=@NewConsultantID Where PlanID=@PlanID and TaskID=@OldTaskID and ConsultantID=@OldConsultantID
						Update PNBaselineConsultant Set ConsultantID=@NewConsultantID Where PlanID=@PlanID and TaskID=@OldTaskID and ConsultantID=@OldConsultantID

						PRINT '>>> Start Deleting Consultant PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '", ConsultantID = "' + @OldConsultantID + '"'
						delete RPConsultant Where PlanID=@PlanID and TaskID=@OldTaskID and ConsultantID=@OldConsultantID
						delete PNConsultant Where PlanID=@PlanID and TaskID=@OldTaskID and ConsultantID=@OldConsultantID
						PRINT '<<< End Deleting Consultant PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '", ConsultantID = "' + @OldConsultantID + '"'

						fetch next from DupConsultantCursor into @OldConsultantID
						set @TempFetch2 = @@Fetch_Status
					end
					close DupConsultantCursor
					deallocate DupConsultantCursor

					fetch next from ConsultantCursor into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @NewConsultantID
					set @TempFetch = @@Fetch_Status
				end
				close ConsultantCursor
				deallocate ConsultantCursor

-- Move Planned and Basedline for the duplicate RPUnit, Delete duplicate RPUnit
				declare UnitCursor cursor for
					select PlanID, TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Unit,'') as Unit, MIN(UnitID) as UnitID  
					from RPUnit where WBS1=@NewWBS1
					group by PlanID, TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(Unit,'')
					Having count(UnitID) > 1
				open UnitCursor
				fetch next from UnitCursor into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Unit, @NewUnitID
				set @TempFetch = @@Fetch_Status
				While (@TempFetch = 0)
				begin
if (@diag = 1) Print 'Check the duplicate Unit ' + @PlanID +'/' + @OldTaskID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @Unit
					declare DupUnitCursor cursor for
						select UnitID from RPUnit 
						where PlanID=@PlanID and TaskID=@OldTaskID and WBS1=@WBS1 
						and isnull(WBS2,'')=@WBS2 
						and isnull(WBS3,'')=@WBS3 
						and isnull(Unit,'')=@Unit
						and UnitID <> @NewUnitID
					open DupUnitCursor
					fetch next from DupUnitCursor into @OldUnitID
					set @TempFetch2 = @@Fetch_Status
					While (@TempFetch2 = 0)
					begin
if (@diag = 1) Print 'Merging the duplicate Unit ' + @PlanID +'/' + @OldTaskID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @Unit
						Update RPPlannedUnit Set UnitID=@NewUnitID Where PlanID=@PlanID and TaskID=@OldTaskID and UnitID=@OldUnitID
						Update RPBaselineUnit Set UnitID=@NewUnitID Where PlanID=@PlanID and TaskID=@OldTaskID and UnitID=@OldUnitID

						PRINT '>>> Start Deleting Unit PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '", UnitID = "' + @OldUnitID + '"'
						delete RPUnit Where PlanID=@PlanID and TaskID=@OldTaskID and UnitID=@OldUnitID
						PRINT '<<< End Deleting Unit PlanID = "' + @PlanID + '", TaskID = "' + @OldTaskID + '", UnitID = "' + @OldUnitID + '"'

						fetch next from DupUnitCursor into @OldUnitID
						set @TempFetch2 = @@Fetch_Status
					end
					close DupUnitCursor
					deallocate DupUnitCursor

					fetch next from UnitCursor into @PlanID, @OldTaskID, @WBS1, @WBS2, @WBS3, @Unit, @NewUnitID
					set @TempFetch = @@Fetch_Status
				end
				close UnitCursor
				deallocate UnitCursor

--*****************************************************************************************************************


				EXECUTE dbo.stRPSyncWBSStructure @NewPlanID
				PRINT '<<< End Processing PlanID = "' + @NewPlanID + '", WBS1 = "' + @NewWBS1 + '"'

				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq, OldKey2, OldKey3, NewKey2, NewKey3, OldName2, OldName3, NewName2, NewName3, WBSName)
					select PKey, Entity, OldKey, NewKey, @OldName, @NewName, @Period, @LastPostSeq, @OldWBS2, @OldWBS3, @NewWBS2, @NewWBS3, @OldName2, @OldName3, @NewName2, @NewName3, WBSName from keyConvertWork where Pkey = @PKeyWork
				end
--
				delete from keyConvertWork where current of keysCursor
				if ((@ErrorNum + @RetVal) <> 0)
					begin
						if (@Diag <> 0)
							print 'Error :' + convert(Nvarchar,@ErrorNum)
							begin
								set @ErrMsg = @ErrMsg + @ErrMsg2
								rollback transaction
							end
						return(@ErrorNum)
					end
				commit Transaction
			end
		else
			begin
				set @ErrMsg = @ErrMsg + @ErrMsg2
				rollback transaction
			end
	fetch next from KeysCursor into 
		@OldWBS1,
		@OldWBS2,
		@OldWBS3,
		@NewWBS1,
		@NewWBS2,
		@NewWBS3, 
		@PKeyWork
	set @KeysFetch = @@Fetch_Status
	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	SET ANSI_WARNINGS on
	return(0)
End -- Procedure
GO
