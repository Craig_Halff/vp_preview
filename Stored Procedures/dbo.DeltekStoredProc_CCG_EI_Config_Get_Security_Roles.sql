SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Get_Security_Roles]
AS
BEGIN
	-- EXEC [dbo].[DeltekStoredProc_CCG_EI_Config_Get_Security_Roles]
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT *
		FROM CCG_EI_ConfigSecurity
		ORDER BY Role
END;
GO
