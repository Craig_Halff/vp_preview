SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_RevenueRecognition2]
    @context AS VARCHAR(12) = 'current' -- Valid choices are 'current' or 'prior'
  , @shortrun VARCHAR(1) = 'Y' -- Valid Choices are 'Y' or 'N'
  , @targetPeriod INT = 202106 -- For what period (int) are we calculating revenue? Note that this does not determine whether we do prior-period style adjustments, the @context paramneter does
  , @notify VARCHAR(1) = 'Y'
AS
    /****************************************************************************
			Copyright (c) 2021 Halff Associates Inc. All rights reserved.
			Calculate Revenue
			01/31/2021	Craig H. Anderson - Created
	*****************************************************************************/
    BEGIN
        --SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
        SET NOCOUNT ON;
        DECLARE @msg       VARCHAR(255)
              , @BatchID   UNIQUEIDENTIFIER = NEWID()
              , @Result    NVARCHAR(MAX)
              , @taskStart DATETIME2
              , @log       VARCHAR(MAX)     = '<pre style="font-family: ''Ubuntu Mono'', Consolas;">';

        DECLARE @recipients VARCHAR(100) = 'canderson@halff.com;jbaummer@halff.com;tkoum@halff.com;jsagel@halff.com';
        DECLARE @errorRecipients VARCHAR(100) = 'canderson@halff.com;jbaummer@halff.com';

        DECLARE @startTime DATETIME2 = GETDATE();
        DECLARE @currentPeriod INT = DATEPART(YEAR, GETDATE()) * 100 + DATEPART(MONTH, GETDATE());
        DECLARE @currentPeriodstr VARCHAR(8) = CAST(@currentPeriod AS VARCHAR(8));

        DECLARE @firstDayOfPeriod DATE = LEFT(CAST(@targetPeriod AS VARCHAR(8)), 4) + RIGHT(CAST(@targetPeriod AS VARCHAR(8)), 2) + '01';
        DECLARE @lastDayOfPeriod DATE = EOMONTH(@firstDayOfPeriod);
        DECLARE @BadPhaseETC INT;
        DECLARE @BadProjectETC INT;

        DECLARE @PRSumTime VARCHAR(4) = ISNULL((
                                                   SELECT FORMAT(AVG(dme.DurationInMinutes), 'N0') AS avgMinutes
                                                   FROM DataMart.dbo.DataMartEvents AS dme
                                                   WHERE
                                                       dme.EventType LIKE '%buildPRSummary%'
                                                       AND DATEDIFF(DAY, dme.StartTime, GETDATE()) <= 60
                                               )
                                             , 'unk'
                                        );

        DECLARE @UpdateEACTime VARCHAR(4) = ISNULL((
                                                       SELECT FORMAT(AVG(dme.DurationInMinutes), 'N0') AS avgMinutes
                                                       FROM DataMart.dbo.DataMartEvents AS dme
                                                       WHERE
                                                           dme.EventType LIKE '%EAC%'
                                                           AND DATEDIFF(DAY, dme.StartTime, GETDATE()) <= 60
                                                   )
                                                 , 'unk'
                                            );
        SET @log =
            @log + 'Server: ' + @@SERVERNAME + '<br><br><u>Parameters:</u><br>    Context:       ' + @context + '<br>' + '    Short Run:     ' + @shortrun + '<br>' + '    Target Period: '
            + CAST(@targetPeriod AS VARCHAR(8)) + '<br><br><hr>';


        /******************************************************************************************************************************
			Step 1. Update PRSummary Tables.
        ******************************************************************************************************************************/
        IF @shortrun <> 'Y'
            BEGIN
                SET @msg = DataMart.dbo.AlertMessage('1.  Update PRSummary Tables, should take about ' + @PRSumTime + ' minutes.', @startTime);
                SET @log = @log + @msg + '<br>';
                RAISERROR(@msg, 0, 1) WITH NOWAIT;
                BEGIN TRY
                    SET @taskStart = GETDATE();

                    EXEC dbo.buildPRSummary @visionuser = 'DataMart'
                                          , @forceRefresh = 'Y'
                                          , @prWhere = ''
                                          , @period = '' -- Do not pass period.
                                          , @postSeq = '';

                    INSERT INTO DataMart.dbo.DataMartEvents (
                        StartTime
                      , EndTime
                      , EventType
                      , Details
                      , BatchID
                    )
                    VALUES
                    (@taskStart, GETDATE(), 'buildPRSummary', 'OK', @BatchID);
                END TRY
                BEGIN CATCH
                    SET @msg = DataMart.dbo.AlertMessage('Error updating PRSummary!', @startTime);
                    RAISERROR(@msg, 0, 1) WITH NOWAIT;
                    SET @msg = @log + '<br>' + @msg + '<br>Error Number: ' + ISNULL((
                                                                                        SELECT CAST(ERROR_NUMBER() AS VARCHAR(255))
                                                                                    )
                                                                                  , ''
                                                                             );
                    SET @msg = @msg + '<br>Error Severity: ' + ISNULL((
                                                                          SELECT CAST(ERROR_SEVERITY() AS VARCHAR(255))
                                                                      )
                                                                    , ''
                                                               );
                    SET @msg = @msg + '<br>Error State: ' + ISNULL((
                                                                       SELECT CAST(ERROR_STATE() AS VARCHAR(255))
                                                                   )
                                                                 , ''
                                                            );
                    SET @msg = @msg + '<br>Error Procedure: ' + ISNULL((
                                                                           SELECT ERROR_PROCEDURE()
                                                                       )
                                                                     , ''
                                                                );
                    SET @msg = @msg + '<br>Error Line: ' + ISNULL((
                                                                      SELECT CAST(ERROR_LINE() AS VARCHAR(255))
                                                                  )
                                                                , ''
                                                           );
                    SET @msg = @msg + '<br>Error Message: ' + ISNULL((
                                                                         SELECT ERROR_MESSAGE()
                                                                     )
                                                                   , ''
                                                              ) + '</pre>';
                    INSERT INTO HalffImport.dbo.CCG_Email_Messages (
                        SourceApplication
                      , Sender
                      , ToList
                      , CCList
                      , BCCList
                      , Subject
                      , GroupSubject
                      , Body
                      , MaxDelay
                      , Priority
                      , CreateDateTime
                    )
                    VALUES
                    ('Revenue Recognition', 'cAnderson@Halff.com', @errorRecipients, '', '', 'Revenue Recognition Failed', '', @msg, 0, 'H', GETDATE());
                    SET NOEXEC ON;
                END CATCH;
            END;
        ELSE
            BEGIN
                SET @msg = DataMart.dbo.AlertMessage('1.  Skipping Update PRSummary Tables (shortrun)', @startTime);
                SET @log = @log + @msg + '<br>';
                RAISERROR(@msg, 0, 1) WITH NOWAIT;
            END;


        /******************************************************************************************************************************
			Step 2. Update EAC, ETC
        ******************************************************************************************************************************/
        IF @shortrun <> 'Y'
            BEGIN
                SET @msg = DataMart.dbo.AlertMessage('2.  Update EAC & ETC, should take about ' + @UpdateEACTime + ' minutes.', @startTime);
                SET @log = @log + @msg + '<br>';
                RAISERROR(@msg, 0, 1) WITH NOWAIT;
                BEGIN TRY
                    DECLARE @myProjects NVARCHAR(MAX) = STUFF((
                                                                  SELECT ',' + ps.WBS1
                                                                  FROM DataMart.dbo.HAI_ProjectStatus    AS ps WITH (NOLOCK)
                                                                      INNER JOIN DataMart.dbo.ProjectJTD AS p WITH (NOLOCK)
                                                                          ON ps.WBS1 = p.WBS1
                                                                              AND p.ProjectStatus = 'A'
                                                                              AND p.ChargeType = 'R'
                                                                  WHERE
                                                                      ps.WBS2 = ' '
                                                                      AND ps.GroupingType IS NULL
                                                                      AND ps.WBS1 NOT LIKE '[GZ]%'
                                                                      AND ps.WBS1 NOT LIKE '01CAT%.REG'
                                                                  FOR XML PATH('')
                                                              )
                                                            , 1
                                                            , 1
                                                            , ''
                                                        );

                    DECLARE @i              INT         = 0
                          , @status         VARCHAR(20) = 'submitted' -- submitted, completed, processing, timeout
                          , @timeoutSeconds INT         = (60 * 120);

                    SET @taskStart = GETDATE();
                    BEGIN TRY
                        BEGIN TRANSACTION; -- #CHANGE: Put the insert inside of a transaction with a commit to try to ensure it writes before moving on.
                        INSERT INTO DataMart.dbo.HAI_EACandETCUpdaterQueue (
                            Projects
                          , Status
                          , SubmittedDate
                          , StartedDate
                          , CompletedDate
                          , ElapsedTime
                          , Details
                        )
                        VALUES
                        (   CAST(@myProjects AS NTEXT) -- Projects - ntext
                          , @status -- Status - nvarchar(50)
                          , @taskStart -- SubmittedDate - datetime2(7)
                          , NULL, NULL, NULL, NULL);
                        COMMIT TRANSACTION;
                    END TRY
                    BEGIN CATCH
                        SET @msg = DataMart.dbo.AlertMessage('Error inserting work request record into HAI_EACandETCUpdaterQueue', @startTime);
                        SET @log = @log + @msg + '<br>';
                        RAISERROR(@msg, 0, 1) WITH NOWAIT;
                        INSERT INTO HalffImport.dbo.CCG_Email_Messages (
                            SourceApplication
                          , Sender
                          , ToList
                          , CCList
                          , BCCList
                          , Subject
                          , GroupSubject
                          , Body
                          , MaxDelay
                          , Priority
                          , CreateDateTime
                        )
                        VALUES
                        ('Revenue Recognition', 'cAnderson@Halff.com', @errorRecipients, '', '', 'Revenue Recognition Failed', '', @log, 0, 'H', GETDATE());
                        SET NOEXEC ON;
                    END CATCH;
                    WHILE @i < @timeoutSeconds AND @status <> 'completed' AND @status IS NOT NULL -- #CHANGE: now checking for NULL status (no record returned?)
                        BEGIN
                            WAITFOR DELAY '00:00:10';
                            SET @i = @i + 10;

                            SET @status = (
                                SELECT q.Status FROM DataMart.dbo.HAI_EACandETCUpdaterQueue q WHERE q.SubmittedDate = @taskStart
                            );
                            IF @status IS NULL
                                BEGIN
                                    SET @msg = DataMart.dbo.AlertMessage('No record written to queue HAI_EACandETCUpdaterQueue!', @startTime);
                                    SET @log = @log + '<span style="color:red;font-weight:bold;">' + @msg + '</span>' + '<br>';

                                    RAISERROR(@msg, 0, 1) WITH NOWAIT;
                                    INSERT INTO HalffImport.dbo.CCG_Email_Messages (
                                        SourceApplication
                                      , Sender
                                      , ToList
                                      , CCList
                                      , BCCList
                                      , Subject
                                      , GroupSubject
                                      , Body
                                      , MaxDelay
                                      , Priority
                                      , CreateDateTime
                                    )
                                    VALUES
                                    ('Revenue Recognition', 'cAnderson@Halff.com', @errorRecipients, '', '', 'Revenue Recognition Failed', '', @log, 0, 'H', GETDATE());
                                    SET NOEXEC ON;
                                END;
                        END;

                    IF @status <> 'completed' -- We timed out
                        BEGIN
                            SET @msg = DataMart.dbo.AlertMessage('Timeout Running UpdateVision_EAC_ETC_WriteOffs!', @startTime);
                            SET @log = @log + @msg + '<br>';

                            RAISERROR(@msg, 0, 1) WITH NOWAIT;
                            INSERT INTO HalffImport.dbo.CCG_Email_Messages (
                                SourceApplication
                              , Sender
                              , ToList
                              , CCList
                              , BCCList
                              , Subject
                              , GroupSubject
                              , Body
                              , MaxDelay
                              , Priority
                              , CreateDateTime
                            )
                            VALUES
                            ('Revenue Recognition', 'cAnderson@Halff.com', @errorRecipients, '', '', 'Revenue Recognition Failed', '', @log, 0, 'H', GETDATE());
                            SET NOEXEC ON;
                        END;

                    INSERT INTO DataMart.dbo.DataMartEvents (
                        StartTime
                      , EndTime
                      , EventType
                      , Details
                      , BatchID
                    )
                    VALUES
                    (@taskStart, GETDATE(), 'HAI_PSP Update EAC/ETC', @Result, @BatchID);

                END TRY
                BEGIN CATCH
                    SET @msg = DataMart.dbo.AlertMessage('Error Running UpdateVision_EAC_ETC_WriteOffs!', @startTime);
                    SET @log = @log + @msg + '<br>';
                    RAISERROR(@msg, 0, 1) WITH NOWAIT;

                    INSERT INTO HalffImport.dbo.CCG_Email_Messages (
                        SourceApplication
                      , Sender
                      , ToList
                      , CCList
                      , BCCList
                      , Subject
                      , GroupSubject
                      , Body
                      , MaxDelay
                      , Priority
                      , CreateDateTime
                    )
                    VALUES
                    ('Revenue Recognition', 'cAnderson@Halff.com', @errorRecipients, '', '', 'Revenue Recognition Failed', '', @log, 0, 'H', GETDATE());
                    SET NOEXEC ON;
                END CATCH;
            END;
        ELSE
            BEGIN
                SET @msg = DataMart.dbo.AlertMessage('2.  Skip Update EAC, ETC, & WriteOffs', @startTime);
                SET @log = @log + @msg + '<br>';
                RAISERROR(@msg, 0, 1) WITH NOWAIT;

            END;

        /******************************************************************************************************************************
			Step 3. Update FeeType Rollups
        ******************************************************************************************************************************/
        SET @msg = DataMart.dbo.AlertMessage('3.  Update FeeTypeRollups', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        IF @shortrun <> 'Y'
            BEGIN

                EXEC dbo.HAI_UpdateCustFeeTypeRollup;
                -- Push the Rollups back down the wbs tree
                EXEC dbo.HAI_MaintProjectFeeType;
                PRINT ' ';

            END;
        ELSE
            BEGIN
                SET @msg = DataMart.dbo.AlertMessage('3.  Skipping FeeTypeRollups (shortrun)', @startTime);
                SET @log = @log + @msg + '<br>';
                RAISERROR(@msg, 0, 1) WITH NOWAIT;
            END;

        /******************************************************************************************************************************
			Step 4. Generate Revenue Table
					Use ProjectSummary for pertinent numbers such as revenue based on target period
					
        ******************************************************************************************************************************/
        SET @msg = DataMart.dbo.AlertMessage('4.  Prepping the _Revenue table.', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;

        IF OBJECT_ID('dbo._Revenue') IS NOT NULL
            BEGIN
                SET @msg = DataMart.dbo.AlertMessage('    4a. Dropping old _Revenue Table', @startTime);
                SET @log = @log + @msg + '<br>';
                RAISERROR(@msg, 0, 1) WITH NOWAIT;
                DROP TABLE dbo._Revenue;
            END;
        SET @msg = DataMart.dbo.AlertMessage('    4b. Generate Revenue Table', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        BEGIN TRY; -- Revenue Table
            WITH
                wbs
                AS
                    (SELECT p.WBS1
                          , p.WBS2
                          , p.WBS3
                          , p.SubLevel
                          , px.CustFeeType
                          , px.CustFeeTypeRollup
                          , CAST(ISNULL(pss.Billed, 0.0) - ISNULL(pss.TotalBilling, 0.0) + ubjtd.UnBilledJTDThru AS DECIMAL(19, 4))                                                        AS CustWriteoffAdjmt
                          ---
                          , CAST((ISNULL(p.FeeDirLab, 0) + ISNULL(p.FeeDirExp, 0) + ISNULL(p.ConsultFee, 0) + ISNULL(p.ReimbAllowCons, 0) + ISNULL(p.ReimbAllowExp, 0)) AS DECIMAL(19, 4)) AS TotalCompensation
                          , CAST(ISNULL(p.FeeDirLab, 0) AS DECIMAL(19, 4))                                                                                                                 AS TotalLaborCompensation
                          , CAST((ISNULL(p.FeeDirExp, 0) + ISNULL(p.ConsultFee, 0) + ISNULL(p.ReimbAllowCons, 0) + ISNULL(p.ReimbAllowExp, 0)) AS DECIMAL(19, 4))                          AS TotalExpConsCompensation
                          , CAST(ISNULL(pss.TotalBilling, 0.0) AS DECIMAL(19, 4))                                                                                                          AS TotalSpent
                          , CAST(ISNULL(pss.LaborBilling, 0.0) AS DECIMAL(19, 4))                                                                                                          AS LaborSpent
                          , CAST(ISNULL(pss.DirectOtherBilling, 0.0) + ISNULL(pss.InDirectBilling, 0.0) + ISNULL(pss.ReimbOtherBilling, 0.0) AS DECIMAL(19, 4))                            AS ExpenseSpent
                          , CAST(ISNULL(pss.DirectConsBilling, 0.0) + ISNULL(pss.ReimbConsBilling, 0.0) AS DECIMAL(19, 4))                                                                 AS ConsultantSpent
                          --
                          , CAST(ISNULL(pss.TotalCost, 0.0) AS DECIMAL(19, 4))                                                                                                             AS JTDCost
                          --
                          , CAST(ISNULL(ps.EAC, ISNULL(DataMart.dbo.Text2Decimal(px.CustEffortatCompletion), 0)) AS DECIMAL(19, 4))                                                        AS EAC
                          , CAST(ISNULL(ps.ETC, ISNULL(DataMart.dbo.Text2Decimal(px.CustEfforttoComplete), 0)) AS DECIMAL(19, 4))                                                          AS ETC
                          , px.CustPercentComplete                                                                                                                                         AS CustPercentComplete
                          ---
                          , CAST(ISNULL(pss.Revenue, 0) AS DECIMAL(19, 4))                                                                                                                 AS TotalRevenue
                          , CAST(ISNULL(px.CustRevenueLabor, 0) AS DECIMAL(19, 4))                                                                                                         AS CustRevenueLabor
                          , CAST(ISNULL(px.CustRevenueConsultant, 0) AS DECIMAL(19, 4))                                                                                                    AS CustRevenueConsultant
                          , CAST(ISNULL(px.CustRevenueExpense, 0) AS DECIMAL(19, 4))                                                                                                       AS CustRevenueExpense
                          --
                          , CAST(ISNULL(pss.Billed, 0.0) AS DECIMAL(19, 4))                                                                                                                AS JTDBilled
                          , CAST(ISNULL(pss.BilledLab, 0.0) AS DECIMAL(19, 4))                                                                                                             AS JTDBilledLab
                          , CAST(ISNULL(pss.BilledCons, 0.0) AS DECIMAL(19, 4))                                                                                                            AS JTDBilledCons
                          , CAST(ISNULL(pss.BilledExp, 0.0) AS DECIMAL(19, 4))                                                                                                             AS JTDBilledExp
                          , CAST(ISNULL(ubjtd.UnBilledJTDThru, 0.0) AS DECIMAL(19, 4))                                                                                                     AS JTDUnbilled
                     FROM dbo.PR                               p WITH (NOWAIT)
                         INNER JOIN dbo.ProjectCustomTabFields AS px WITH (NOWAIT)
                             ON p.WBS1 = px.WBS1
                                 AND p.WBS2 = px.WBS2
                                 AND p.WBS3 = px.WBS3
                         INNER JOIN dbo.ProjectCustomTabFields AS px_p WITH (NOWAIT)
                             ON p.WBS1 = px_p.WBS1
                                 AND px_p.WBS2 = ' '
                                 AND px_p.WBS3 = ' '
                         INNER JOIN (
                             SELECT WBS1
                             FROM dbo.PR WITH (NOWAIT)
                             WHERE
                                 WBS2 = ' '
                                 AND WBS3 = ' '
                                 AND ChargeType = 'R'
                                 AND Status = 'A'
                                 AND WBS1 NOT LIKE 'ZZ%'
                         )                                     p1
                             ON p.WBS1 = p1.WBS1
                         LEFT OUTER JOIN (
                             SELECT sx.WBS1
                                  , sx.WBS2
                                  , sx.WBS3
                                  , SUM(sx.Revenue)                                                                                                                                                 AS Revenue
                                  , SUM(sx.Billed)                                                                                                                                                  AS Billed -- for task rev calc
                                  , SUM(sx.BilledLab)                                                                                                                                               AS BilledLab
                                  , SUM(sx.BilledCons)                                                                                                                                              AS BilledCons
                                  , SUM(sx.BilledExp)                                                                                                                                               AS BilledExp
                                  , SUM(sx.BilledUnit)                                                                                                                                              AS BilledUnit
                                  , SUM(sx.BilledOther)                                                                                                                                             AS BilledOther
                                  --
                                  , SUM(sx.DirectConsBilling)                                                                                                                                       AS DirectConsBilling
                                  , SUM(sx.DirectOtherBilling)                                                                                                                                      AS DirectOtherBilling
                                  , SUM(sx.InDirectBilling)                                                                                                                                         AS InDirectBilling
                                  , SUM(sx.ReimbConsBilling)                                                                                                                                        AS ReimbConsBilling
                                  , SUM(sx.ReimbOtherBilling)                                                                                                                                       AS ReimbOtherBilling
                                  , SUM(sx.LaborBilling)                                                                                                                                            AS LaborBilling
                                  , SUM(sx.DirectConsBilling) + SUM(sx.DirectOtherBilling) + SUM(sx.InDirectBilling) + SUM(sx.ReimbConsBilling) + SUM(sx.ReimbOtherBilling) + SUM(sx.LaborBilling)  AS TotalBilling
                                  , SUM(sx.DirectOtherCost) + SUM(sx.InDirectCost) + SUM(sx.ReimbOtherCost) + SUM(sx.LaborCost) + SUM(sx.DirectConsCost) + SUM(sx.ReimbConsCost) + SUM(sx.Overhead) AS TotalCost
                             FROM dbo.PRSummarySub sx WITH (NOWAIT)
                             WHERE sx.Period <= @targetPeriod
                             GROUP BY
                                 sx.WBS1
                               , sx.WBS2
                               , sx.WBS3
                         )                                     AS pss
                             ON p.WBS1 = pss.WBS1
                                 AND p.WBS2 = pss.WBS2
                                 AND p.WBS3 = pss.WBS3
                         LEFT OUTER JOIN (
                             SELECT WBS1
                                  , WBS2
                                  , ' ' AS WBS3
                                  , EAC
                                  , ETC
                             FROM DataMart.dbo.HAI_ProjectStatus WITH (NOWAIT)
                             WHERE GroupingType IS NULL
                         )                                     AS ps -- Use EAC from ProjectStatus - More up-to-date and stored as number
                             ON p.WBS1 = ps.WBS1
                                 AND p.WBS2 = ps.WBS2
                                 AND p.WBS3 = ps.WBS3
                         LEFT OUTER JOIN (
                             SELECT ub.WBS1
                                  , ub.WBS2
                                  , ub.WBS3
                                  , CAST(ISNULL(SUM(ub.BillExt), 0.00) AS DECIMAL(19, 4)) AS UnBilledJTDThru
                             FROM (
                                 SELECT WBS1
                                      , WBS2
                                      , WBS3
                                      , BillExt
                                 FROM dbo.LD WITH (NOLOCK)
                                 WHERE
                                     ProjectCost = 'Y'
                                     AND SuppressBill = 'N'
                                     AND BillExt <> 0
                                     AND Period <= @targetPeriod
                                     AND (
                                         TransDate <= @lastDayOfPeriod
                                             OR TransDate IS NULL
                                     )
                                     AND (
                                         (
                                             -- removes all History-Loaded related transactions
                                             BillStatus IN ( 'B', 'H', 'F', 'W', 'X', 'D', 'O' )
                                             AND TransType <> 'HL'
                                             AND (
                                                 BilledPeriod > @targetPeriod
                                                     OR BilledPeriod = 0
                                             )
                                         )
                                             OR (
                                                 -- includes History-Loaded Transactions
                                                 BillStatus IN ( 'B', 'H' )
                                                    AND TransType = 'HL'
                                                    AND (
                                                        BilledPeriod > @targetPeriod
                                                            OR BilledPeriod = 0
                                                    )
                                             )
                                             OR (
                                                 -- includes History-Loaded Transactions
                                                 BillStatus IN ( 'F' )
                                                    AND TransType = 'HL'
                                                    AND (BilledPeriod > @targetPeriod)
                                             )
                                     )
                                 UNION ALL
                                 SELECT L.WBS1
                                      , L.WBS2
                                      , L.WBS3
                                      , L.BillExt
                                 FROM dbo.LedgerMisc L WITH (NOLOCK)
                                 WHERE
                                     L.ProjectCost = 'Y'
                                     AND L.SuppressBill = 'N'
                                     AND L.BillExt <> 0
                                     AND L.Period <= @targetPeriod
                                     AND (
                                         L.TransDate <= @lastDayOfPeriod
                                             OR L.TransDate IS NULL
                                     )
                                     AND (
                                         (
                                             -- removes all History-Loaded related transactions
                                             L.BillStatus IN ( 'B', 'H', 'F', 'W', 'X', 'D', 'O' )
                                             AND L.TransType <> 'HE'
                                             AND (
                                                 L.BilledPeriod > @targetPeriod
                                                     OR L.BilledPeriod = 0
                                             )
                                         )
                                             OR (
                                                 -- include History-Loaded Transactions
                                                 L.BillStatus IN ( 'B', 'H' )
                                                    AND L.TransType = 'HE'
                                                    AND (
                                                        L.BilledPeriod > @targetPeriod
                                                            OR L.BilledPeriod = 0
                                                    )
                                             )
                                             OR (
                                                 -- include History-Loaded Transactions
                                                 L.BillStatus IN ( 'F' )
                                                    AND L.TransType = 'HE'
                                                    AND (L.BilledPeriod > @targetPeriod)
                                             )
                                     )
                                 UNION ALL
                                 SELECT L.WBS1
                                      , L.WBS2
                                      , L.WBS3
                                      , L.BillExt
                                 FROM dbo.LedgerAP L WITH (NOLOCK)
                                 WHERE
                                     L.ProjectCost = 'Y'
                                     AND L.SuppressBill = 'N'
                                     AND L.BillExt <> 0
                                     AND L.Period <= @targetPeriod
                                     AND (
                                         L.TransDate <= @lastDayOfPeriod
                                             OR L.TransDate IS NULL
                                     )
                                     AND (
                                         (
                                             -- removes all History-Loaded related transactions
                                             L.BillStatus IN ( 'B', 'H', 'F', 'W', 'X', 'D', 'O' )
                                             AND L.TransType <> 'HE'
                                             AND (
                                                 L.BilledPeriod > @targetPeriod
                                                     OR L.BilledPeriod = 0
                                             )
                                         )
                                             OR (
                                                 -- include History-Loaded Transactions
                                                 L.BillStatus IN ( 'B', 'H' )
                                                    AND L.TransType = 'HE'
                                                    AND (
                                                        L.BilledPeriod > @targetPeriod
                                                            OR L.BilledPeriod = 0
                                                    )
                                             )
                                             OR (
                                                 -- include History-Loaded Transactions
                                                 L.BillStatus IN ( 'F' )
                                                    AND L.TransType = 'HE'
                                                    AND (L.BilledPeriod > @targetPeriod)
                                             )
                                     )
                                 UNION ALL
                                 SELECT L.WBS1
                                      , L.WBS2
                                      , L.WBS3
                                      , L.BillExt
                                 FROM dbo.LedgerEX L WITH (NOLOCK)
                                 WHERE
                                     L.ProjectCost = 'Y'
                                     AND L.SuppressBill = 'N'
                                     AND L.BillExt <> 0
                                     AND L.Period <= @targetPeriod
                                     AND (
                                         L.TransDate <= @lastDayOfPeriod
                                             OR L.TransDate IS NULL
                                     )
                                     AND (
                                         (
                                             -- removes all History-Loaded related transactions
                                             L.BillStatus IN ( 'B', 'H', 'F', 'W', 'X', 'D', 'O' )
                                             AND L.TransType <> 'HE'
                                             AND (
                                                 L.BilledPeriod > @targetPeriod
                                                     OR L.BilledPeriod = 0
                                             )
                                         )
                                             OR (
                                                 -- include History-Loaded Transactions
                                                 L.BillStatus IN ( 'B', 'H' )
                                                    AND L.TransType = 'HE'
                                                    AND (
                                                        L.BilledPeriod > @targetPeriod
                                                            OR L.BilledPeriod = 0
                                                    )
                                             )
                                             OR (
                                                 -- include History-Loaded Transactions
                                                 L.BillStatus IN ( 'F' )
                                                    AND L.TransType = 'HE'
                                                    AND (L.BilledPeriod > @targetPeriod)
                                             )
                                     )
                                 UNION ALL
                                 SELECT L.WBS1
                                      , L.WBS2
                                      , L.WBS3
                                      , L.BillExt
                                 FROM dbo.LedgerAR L WITH (NOLOCK)
                                 WHERE
                                     L.ProjectCost = 'Y'
                                     AND L.SuppressBill = 'N'
                                     AND L.BillExt <> 0
                                     AND L.Period <= @targetPeriod
                                     AND (
                                         L.TransDate <= @lastDayOfPeriod
                                             OR L.TransDate IS NULL
                                     )
                                     AND (
                                         (
                                             -- removes all History-Loaded related transactions
                                             L.BillStatus IN ( 'B', 'H', 'F', 'W', 'X', 'D', 'O' )
                                             AND L.TransType <> 'HE'
                                             AND (
                                                 L.BilledPeriod > @targetPeriod
                                                     OR L.BilledPeriod = 0
                                             )
                                         )
                                             OR (
                                                 -- include History-Loaded Transactions
                                                 L.BillStatus IN ( 'B', 'H' )
                                                    AND L.TransType = 'HE'
                                                    AND (
                                                        L.BilledPeriod > @targetPeriod
                                                            OR L.BilledPeriod = 0
                                                    )
                                             )
                                             OR (
                                                 -- include History-Loaded Transactions
                                                 L.BillStatus IN ( 'F' )
                                                    AND L.TransType = 'HE'
                                                    AND (L.BilledPeriod > @targetPeriod)
                                             )
                                     )
                                 UNION ALL
                                 SELECT L.WBS1
                                      , L.WBS2
                                      , L.WBS3
                                      , L.BillExt
                                 FROM dbo.BILD L WITH (NOLOCK)
                                 WHERE
                                     L.ProjectCost = 'Y'
                                     AND L.SuppressBill = 'N'
                                     AND L.BillExt <> 0
                                     AND L.Period <= @targetPeriod
                                     AND (
                                         L.TransDate <= @lastDayOfPeriod
                                             OR L.TransDate IS NULL
                                     )
                                     AND (
                                         (
                                             -- removes all History-Loaded related transactions
                                             L.BillStatus IN ( 'B', 'H', 'F', 'W', 'X', 'D', 'O' )
                                             AND L.TransType <> 'HL'
                                             AND (
                                                 L.BilledPeriod > @targetPeriod
                                                     OR L.BilledPeriod = 0
                                             )
                                         )
                                             OR (
                                                 -- include History-Loaded Transactions
                                                 L.BillStatus IN ( 'B', 'H' )
                                                    AND L.TransType = 'HL'
                                                    AND (
                                                        L.BilledPeriod > @targetPeriod
                                                            OR L.BilledPeriod = 0
                                                    )
                                             )
                                             OR (
                                                 -- include History-Loaded Transactions
                                                 L.BillStatus IN ( 'F' )
                                                    AND L.TransType = 'HL'
                                                    AND (L.BilledPeriod > @targetPeriod)
                                             )
                                     )
                                 UNION ALL
                                 SELECT L.WBS1
                                      , L.WBS2
                                      , L.WBS3
                                      , L.BillExt
                                 FROM dbo.BIED L WITH (NOLOCK)
                                 WHERE
                                     L.ProjectCost = 'Y'
                                     AND L.SuppressBill = 'N'
                                     AND L.BillExt <> 0
                                     AND L.Period <= @targetPeriod
                                     AND (
                                         L.TransDate <= @lastDayOfPeriod
                                             OR L.TransDate IS NULL
                                     )
                                     AND (
                                         (
                                             -- removes all History-Loaded related transactions
                                             L.BillStatus IN ( 'B', 'H', 'F', 'W', 'X', 'D', 'O' )
                                             AND L.TransType <> 'HE'
                                             AND (
                                                 L.BilledPeriod > @targetPeriod
                                                     OR L.BilledPeriod = 0
                                             )
                                         )
                                             OR (
                                                 -- include History-Loaded Transactions
                                                 L.BillStatus IN ( 'B', 'H' )
                                                    AND L.TransType = 'HE'
                                                    AND (
                                                        L.BilledPeriod > @targetPeriod
                                                            OR L.BilledPeriod = 0
                                                    )
                                             )
                                             OR (
                                                 -- include History-Loaded Transactions
                                                 L.BillStatus IN ( 'F' )
                                                    AND L.TransType = 'HE'
                                                    AND (L.BilledPeriod > @targetPeriod)
                                             )
                                     )
                             )                     AS ub
                                 INNER JOIN dbo.PR AS p
                                     ON ub.WBS1 = p.WBS1
                                         AND p.WBS2 = ' '
                                         AND p.WBS3 = ' '
                             WHERE
                                 p.ChargeType = 'R' -- Chargeables only
                                 AND p.Status = 'A'
                                 AND p.WBS1 NOT LIKE 'ZZ%'
                                 AND p.WBS1 NOT IN ( 'IC1900.001', 'IC1900.003', 'IC1900.006' )
                             GROUP BY
                                 ub.WBS1
                               , ub.WBS2
                               , ub.WBS3
                         )                                     ubjtd
                             ON p.WBS1 = ubjtd.WBS1
                                 AND p.WBS2 = ubjtd.WBS2
                                 AND p.WBS3 = ubjtd.WBS3)
            SELECT wbs.WBS1
                 , wbs.WBS2
                 , wbs.WBS3
                 , wbs.SubLevel
                 , wbs.CustFeeType
                 , wbs.CustFeeTypeRollup
                 , wbs.CustWriteoffAdjmt
                 , wbs.TotalCompensation
                 , wbs.TotalLaborCompensation
                 , wbs.TotalExpConsCompensation
                 , wbs.EAC
                 , wbs.TotalSpent
                 , wbs.LaborSpent
                 , wbs.ExpenseSpent
                 , wbs.ConsultantSpent
                 , wbs.JTDCost
                 , wbs.ETC
                 , wbs.CustPercentComplete
                 , wbs.TotalRevenue
                 , wbs.CustRevenueLabor
                 , wbs.CustRevenueConsultant
                 , wbs.CustRevenueExpense
                 , CAST(0 AS DECIMAL(19, 4)) AS CustRevenueExpenseAndConsultant
                 , wbs.JTDBilled
                 , wbs.JTDBilledLab
                 , wbs.JTDBilledCons
                 , wbs.JTDBilledExp
                 , wbs.JTDUnbilled
            INTO dbo._Revenue
            FROM wbs;

            UPDATE dbo._Revenue SET EAC = ISNULL(TotalSpent, 0)WHERE ISNULL(EAC, 0) < ISNULL(TotalSpent, 0);
            UPDATE dbo._Revenue
            SET ETC = ISNULL(EAC, 0) - ISNULL(TotalSpent, 0)
            WHERE ISNULL(ETC, 0) <> (ISNULL(EAC, 0) - ISNULL(TotalSpent, 0));
        END TRY
        BEGIN CATCH
            SET @msg = DataMart.dbo.AlertMessage('_Revenue Table Generation Failed!', @startTime);
            RAISERROR(@msg, 0, 1) WITH NOWAIT;
            SET @msg = @log + '<br>' + @msg + '<br>Error Number: ' + ISNULL((
                                                                                SELECT CAST(ERROR_NUMBER() AS VARCHAR(255))
                                                                            )
                                                                          , ''
                                                                     );
            SET @msg = @msg + '<br>Error Severity: ' + ISNULL((
                                                                  SELECT CAST(ERROR_SEVERITY() AS VARCHAR(255))
                                                              )
                                                            , ''
                                                       );
            SET @msg = @msg + '<br>Error State: ' + ISNULL((
                                                               SELECT CAST(ERROR_STATE() AS VARCHAR(255))
                                                           )
                                                         , ''
                                                    );
            SET @msg = @msg + '<br>Error Procedure: ' + ISNULL((
                                                                   SELECT ERROR_PROCEDURE()
                                                               )
                                                             , ''
                                                        );
            SET @msg = @msg + '<br>Error Line: ' + ISNULL((
                                                              SELECT CAST(ERROR_LINE() AS VARCHAR(255))
                                                          )
                                                        , ''
                                                   );
            SET @msg = @msg + '<br>Error Message: ' + ISNULL((
                                                                 SELECT ERROR_MESSAGE()
                                                             )
                                                           , ''
                                                      ) + '</pre>';
            INSERT INTO HalffImport.dbo.CCG_Email_Messages (
                SourceApplication
              , Sender
              , ToList
              , CCList
              , BCCList
              , Subject
              , GroupSubject
              , Body
              , MaxDelay
              , Priority
              , CreateDateTime
            )
            VALUES
            ('Revenue Recognition', 'cAnderson@Halff.com', @errorRecipients, '', '', 'Revenue Recognition Failed', '', @log, 0, 'H', GETDATE());
            SET NOEXEC ON;
        END CATCH;

        /******************************************************************************************************************************
			Step 5. Update JTDBilled to account for unposted invoices
		******************************************************************************************************************************/
        SET @msg = DataMart.dbo.AlertMessage('5.  Update JTDBilled to account for unposted invoices.', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        IF @context = 'current'
            BEGIN
                UPDATE r
                SET r.JTDBilled = r.JTDBilled + ui.UnpostedInvAmt
                FROM dbo._Revenue r
                    INNER JOIN (
                        SELECT A.WBS1
                             , A.WBS2
                             , A.WBS3
                             , SUM(A.Amount) AS UnpostedInvAmt
                        FROM (
                            SELECT id.WBS1
                                 , id.WBS2
                                 , id.WBS3
                                 , id.Amount
                            FROM dbo.inControl ic WITH (NOLOCK)
                               , dbo.inDetail id
                            WHERE
                                ic.Batch = id.Batch
                                AND ic.Posted = 'N'
                        ) AS A
                        GROUP BY
                            A.WBS1
                          , A.WBS2
                          , A.WBS3
                    )             ui
                        ON r.WBS1 = ui.WBS1
                            AND r.WBS2 = ui.WBS2
                            AND r.WBS3 = ui.WBS3;
            END;
        ELSE
            BEGIN
                SET @msg = DataMart.dbo.AlertMessage('     Skipped - we are working in Prior period', @startTime);
                SET @log = @log + @msg + '<br>';
                RAISERROR(@msg, 0, 1) WITH NOWAIT;
            END;

        /******************************************************************************************************************************
			Step 6. Update phase level values as PRSummary tables only store values at the lowest level.
		******************************************************************************************************************************/
        SET @msg = DataMart.dbo.AlertMessage('6.  Update phase level values as  PRSummary tables only store values at the lowest level.', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        BEGIN

            UPDATE rph
            SET rph.TotalSpent = rtk.TotalSpent
              , rph.LaborSpent = rtk.LaborSpent
              , rph.ExpenseSpent = rtk.ExpenseSpent
              , rph.ConsultantSpent = rtk.ConsultantSpent
              , rph.JTDBilled = rtk.JTDBilled
              , rph.JTDBilledLab = rtk.JTDBilledLab
              , rph.JTDBilledExp = rtk.JTDBilledExp
              , rph.JTDBilledCons = rtk.JTDBilledCons
              , rph.JTDUnbilled = rtk.JTDUnbilled
            FROM dbo._Revenue rph
                INNER JOIN (
                    SELECT WBS1
                         , WBS2
                         , SUM(ISNULL(TotalSpent, 0.0000))      AS TotalSpent
                         , SUM(ISNULL(LaborSpent, 0.0000))      AS LaborSpent
                         , SUM(ISNULL(ExpenseSpent, 0.0000))    AS ExpenseSpent
                         , SUM(ISNULL(ConsultantSpent, 0.0000)) AS ConsultantSpent
                         , SUM(ISNULL(JTDCost, 0.0000))         AS JTDCost
                         , SUM(ISNULL(JTDBilled, 0.0000))       AS JTDBilled
                         , SUM(ISNULL(JTDBilledLab, 0.0000))    AS JTDBilledLab
                         , SUM(ISNULL(JTDBilledExp, 0.0000))    AS JTDBilledExp
                         , SUM(ISNULL(JTDBilledCons, 0.0000))   AS JTDBilledCons
                         , SUM(ISNULL(JTDUnbilled, 0.0000))     AS JTDUnbilled
                    FROM dbo._Revenue WITH (NOLOCK)
                    WHERE WBS3 <> ' '
                    GROUP BY
                        WBS1
                      , WBS2
                )             rtk
                    ON rph.WBS1 = rtk.WBS1
                        AND rph.WBS2 = rtk.WBS2
            WHERE
                rph.WBS3 = ' '
                AND rph.SubLevel = 'Y';
        END;
        SET @msg = DataMart.dbo.AlertMessage('    6a.  Update project level values as  PRSummary tables only store values at the lowest level.', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        BEGIN
            UPDATE rp
            SET rp.TotalSpent = rtk.TotalSpent
              , rp.LaborSpent = rtk.LaborSpent
              , rp.ExpenseSpent = rtk.ExpenseSpent
              , rp.ConsultantSpent = rtk.ConsultantSpent
              , rp.JTDBilled = rtk.JTDBilled
              , rp.JTDBilledLab = rtk.JTDBilledLab
              , rp.JTDBilledExp = rtk.JTDBilledExp
              , rp.JTDBilledCons = rtk.JTDBilledCons
              , rp.JTDUnbilled = rtk.JTDUnbilled
            FROM dbo._Revenue rp
                INNER JOIN (
                    SELECT WBS1
                         , SUM(ISNULL(TotalSpent, 0.0000))      AS TotalSpent
                         , SUM(ISNULL(LaborSpent, 0.0000))      AS LaborSpent
                         , SUM(ISNULL(ExpenseSpent, 0.0000))    AS ExpenseSpent
                         , SUM(ISNULL(ConsultantSpent, 0.0000)) AS ConsultantSpent
                         , SUM(ISNULL(JTDCost, 0.0000))         AS JTDCost
                         , SUM(ISNULL(JTDBilled, 0.0000))       AS JTDBilled
                         , SUM(ISNULL(JTDBilledLab, 0.0000))    AS JTDBilledLab
                         , SUM(ISNULL(JTDBilledExp, 0.0000))    AS JTDBilledExp
                         , SUM(ISNULL(JTDBilledCons, 0.0000))   AS JTDBilledCons
                         , SUM(ISNULL(JTDUnbilled, 0.0000))     AS JTDUnbilled
                    FROM dbo._Revenue WITH (NOLOCK)
                    WHERE WBS2 = ' '
                    GROUP BY WBS1
                )             rtk
                    ON rp.WBS1 = rtk.WBS1
            WHERE
                rp.WBS2 = ' '
                AND rp.SubLevel = 'Y';
        END;

        SET @msg = DataMart.dbo.AlertMessage('    6b.  Re-Evaluate EAC & ETC, now that JTD Billings have been updated at Phase & Project levels', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;

        UPDATE dbo._Revenue SET EAC = ISNULL(TotalSpent, 0)WHERE ISNULL(EAC, 0) < ISNULL(TotalSpent, 0);
        UPDATE dbo._Revenue
        SET ETC = ISNULL(EAC, 0) - ISNULL(TotalSpent, 0)
        WHERE ETC <> ISNULL(EAC, 0) - ISNULL(TotalSpent, 0);

        /******************************************************************************************************************************
			Step 7. If context is Prior Month, calculate JTD Billed adjustments.
		******************************************************************************************************************************/
        SET @msg = DataMart.dbo.AlertMessage('7.  Calculate and apply JTD Billed adjustments for prior month use only, the Baummer way!', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        IF @context = 'prior'
            BEGIN -- BilledJTD Adjustment
                UPDATE r
                SET r.JTDBilled = r.JTDBilled + jb.PrevBilled
                FROM dbo._Revenue r
                    INNER JOIN (
                        SELECT adj.WBS1
                             , adj.WBS2
                             , adj.WBS3
                             , ISNULL(SUM(adj.PrevBilled), 0) AS PrevBilled
                        FROM (
                            SELECT a.WBS1
                                 , a.WBS2
                                 , a.WBS3
                                 , a.PrevBilled
                            FROM (
                                SELECT a.Period
                                     , a.WBS1
                                     , a.WBS2
                                     , a.WBS3
                                     , a.Invoice
                                     , a.InvoiceSection
                                     , a.TransType
                                     , a.TransDate
                                     , ISNULL(SUM(a.Amount), 0) AS PrevBilled
                                FROM dbo.LedgerAR a WITH (NOLOCK)
                                WHERE
                                    a.Period <= @targetPeriod
                                    AND a.TransType = 'IN'
                                    AND a.InvoiceSection <> 'T'
                                GROUP BY
                                    a.WBS1
                                  , a.WBS2
                                  , a.WBS3
                                  , a.Invoice
                                  , a.InvoiceSection
                                  , a.TransType
                                  , a.TransDate
                                  , a.Period
                            )     AS a
                                LEFT JOIN (
                                    SELECT a.Period
                                         , a.WBS1
                                         , a.WBS2
                                         , a.WBS3
                                         , a.Invoice
                                         , a.InvoiceSection
                                         , a.TransType
                                         , a.TransDate
                                         , ISNULL(SUM(a.Amount), 0) AS CurrBilled
                                    FROM dbo.LedgerAR a WITH (NOLOCK)
                                    WHERE
                                        a.Period > @targetPeriod
                                        AND a.TransType = 'IN'
                                        AND a.InvoiceSection <> 'T'
                                    GROUP BY
                                        a.WBS1
                                      , a.WBS2
                                      , a.WBS3
                                      , a.Invoice
                                      , a.InvoiceSection
                                      , a.TransType
                                      , a.TransDate
                                      , a.Period
                                ) AS b
                                    ON a.WBS1 = b.WBS1
                                        AND a.WBS2 = b.WBS2
                                        AND a.WBS3 = b.WBS3
                                        AND a.Invoice = b.Invoice
                                        AND a.TransType = b.TransType
                                        AND b.Period > @targetPeriod
                                        AND a.InvoiceSection = b.InvoiceSection
                                        AND b.InvoiceSection <> 'T'
                            WHERE
                                a.Period <= @targetPeriod
                                AND a.TransType = 'IN'
                                AND a.InvoiceSection <> 'T'
                            GROUP BY
                                a.WBS1
                              , a.WBS2
                              , a.WBS3
                              , a.TransDate
                              , a.Invoice
                              , a.Period
                              , a.PrevBilled
                              , b.CurrBilled
                            HAVING SUM(b.CurrBilled) IS NOT NULL
                        ) adj
                        GROUP BY
                            adj.WBS1
                          , adj.WBS2
                          , adj.WBS3
                    )             jb
                        ON r.WBS1 = jb.WBS1
                            AND r.WBS2 = jb.WBS2
                            AND r.WBS3 = jb.WBS3;
            END;
        ELSE
            BEGIN
                SET @msg = DataMart.dbo.AlertMessage('		Skipped (current)', @startTime);
                SET @log = @log + @msg + '<br>';
                RAISERROR(@msg, 0, 1) WITH NOWAIT;
            END;

        /******************************************************************************************************************************
			Step 8. Roll up adjusted JTDBilled amounts to Phase level.
		******************************************************************************************************************************/
        SET @msg = DataMart.dbo.AlertMessage('8.  Roll up adjusted JTDBilled amounts to Phase level.', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        BEGIN
            UPDATE rph
            SET rph.JTDBilled = rtk.JTDBilled
            FROM dbo._Revenue rph
                INNER JOIN (
                    SELECT WBS1
                         , WBS2
                         , SUM(ISNULL(JTDBilled, 0.0000)) AS JTDBilled
                    FROM dbo._Revenue WITH (NOLOCK)
                    WHERE WBS3 <> ' '
                    GROUP BY
                        WBS1
                      , WBS2
                )             rtk
                    ON rph.WBS1 = rtk.WBS1
                        AND rph.WBS2 = rtk.WBS2
            WHERE
                rph.WBS3 = ' '
                AND rph.SubLevel = 'Y';
        END;


        /****************************************************************************************************
			Step 9. Update WriteOffAdj
		*****************************************************************************************************/
        SET @msg = DataMart.dbo.AlertMessage('9.  Update WriteOffAdj at all levels', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        BEGIN
            -- Calculate WO for Phases only, and specifically set Tasks to zero.
            UPDATE r
            SET r.CustWriteoffAdjmt = CASE WHEN r.WBS3 = ' ' THEN (ISNULL(r.JTDBilled, 0) - ISNULL(r.TotalSpent, 0) + ISNULL(r.JTDUnbilled, 0)) ELSE 0 END
            FROM dbo._Revenue AS r;

            -- Set WO to zero for LS or PC fee types at any level
            UPDATE r
            SET r.CustWriteoffAdjmt = 0
            FROM dbo._Revenue AS r
            WHERE
                r.CustFeeTypeRollup LIKE '%LS%'
                OR r.CustFeeTypeRollup LIKE '%PC%';

            -- Roll up WO to Project level
            UPDATE p
            SET p.CustWriteoffAdjmt = ph.CustWriteoffAdjmt
            FROM dbo._Revenue AS p
                INNER JOIN (
                    SELECT WBS1
                         , SUM(CustWriteoffAdjmt) AS CustWriteoffAdjmt
                    FROM dbo._Revenue WITH (NOLOCK)
                    WHERE WBS2 <> ' '
                    GROUP BY WBS1
                )             AS ph
                    ON p.WBS1 = ph.WBS1
            WHERE p.WBS2 = ' ';
        END;


        /******************************************************************************************************************************
			Step 10. Calculate Phase Total Revenue
		******************************************************************************************************************************/
        SET @msg = DataMart.dbo.AlertMessage('10. Calculate Phase Revenue', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        BEGIN
            UPDATE r
            SET r.TotalRevenue = CAST(CASE
                                          WHEN r.CustFeeTypeRollup IN ( 'LS', 'PC' )
                                              AND r.EAC > 0.0
                                              THEN
                                              r.TotalCompensation * r.TotalSpent / r.EAC
                                          WHEN r.CustFeeTypeRollup IN ( 'CP', 'CPFF' )
                                              THEN
                                              r.TotalSpent + r.CustWriteoffAdjmt
                                          WHEN (r.CustFeeTypeRollup = 'CPM')
                                              AND r.EAC > 0
                                              THEN
                                              IIF(r.TotalSpent + r.CustWriteoffAdjmt < r.TotalCompensation * (r.TotalSpent / r.EAC)
                                                  , r.TotalSpent + r.CustWriteoffAdjmt
                                                  , r.TotalCompensation * (r.TotalSpent / r.EAC))
                                          ELSE
                                              0.0
                                      END AS DECIMAL(19, 4))
            FROM dbo._Revenue AS r
            WHERE
                (
                r.WBS3 = ' '
                    AND r.WBS2 <> ' '
            )
                OR (
                    r.WBS2 = ' '
                       AND r.SubLevel = 'N'
                );
        END;

        /******************************************************************************************************************************
			Step 11. Distribute Phase TotalRevenue to Tasks
		******************************************************************************************************************************/
        SET @msg = DataMart.dbo.AlertMessage('11. Distribute Phase TotalRevenue to Tasks', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
		--SQL Prompt Formatting Off
        BEGIN
            UPDATE tsk
                SET tsk.TotalRevenue = CAST(
						CASE
							WHEN ph.TotalSpent <> 0 
								THEN (tsk.TotalSpent / ph.TotalSpent) * ph.TotalRevenue
							WHEN ph.TotalRevenue <> 0 AND ph.JTDBilled <> 0 
								THEN (tsk.JTDBilled / ph.JTDBilled) * ph.TotalRevenue
							ELSE 0
						END
						AS DECIMAL(19, 4))
                FROM dbo._Revenue       tsk
                INNER JOIN dbo._Revenue ph WITH (NOLOCK)
                    ON tsk.WBS1 = ph.WBS1
                        AND tsk.WBS2 = ph.WBS2
                        AND ph.WBS3 = ' '
                WHERE tsk.WBS3 <> ' ';
        END;
		--SQL Prompt Formatting On
        /******************************************************************************************************************************
			Step 12. Allocate Task TotalRevenue to Task revenue Buckets
		******************************************************************************************************************************/
        SET @msg = DataMart.dbo.AlertMessage('12. Allocate lowest level TotalRevenue to Labor, Expense, and Consultant buckets', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        BEGIN
            UPDATE bk
            SET bk.CustRevenueConsultant = CASE
                                               WHEN bk.TotalRevenue <> 0
                                                   THEN
                                                   CASE
                                                       WHEN bk.TotalSpent <> 0
                                                           THEN
                                                           CASE
                                                               WHEN bk.TotalRevenue >= bk.ConsultantSpent + bk.ExpenseSpent
                                                                   THEN
                                                                   bk.ConsultantSpent
                                                               WHEN (bk.ConsultantSpent + bk.ExpenseSpent) <> 0
                                                                   THEN
                                                                   bk.ConsultantSpent / (bk.ConsultantSpent + bk.ExpenseSpent) * bk.TotalRevenue
                                                               ELSE
                                                                   0
                                                           END
                                                       ELSE
                                                           0
                                                   END
                                               ELSE
                                                   0
                                           END
              , bk.CustRevenueExpense = CASE
                                            WHEN bk.TotalRevenue <> 0
                                                THEN
                                                CASE
                                                    WHEN bk.TotalSpent <> 0
                                                        THEN
                                                        CASE
                                                            WHEN bk.TotalRevenue >= bk.ConsultantSpent + bk.ExpenseSpent
                                                                THEN
                                                                bk.ExpenseSpent
                                                            WHEN (bk.ConsultantSpent + bk.ExpenseSpent) <> 0
                                                                THEN
                                                                bk.ExpenseSpent / (bk.ConsultantSpent + bk.ExpenseSpent) * bk.TotalRevenue
                                                            ELSE
                                                                0
                                                        END
                                                    ELSE
                                                        0
                                                END
                                            ELSE
                                                0
                                        END
            FROM dbo._Revenue AS bk
            WHERE bk.SubLevel = 'N';

            UPDATE bk
            SET bk.CustRevenueLabor = bk.TotalRevenue - (bk.CustRevenueExpense + bk.CustRevenueConsultant)
            FROM dbo._Revenue AS bk
            WHERE bk.SubLevel = 'N';
        END;

        /******************************************************************************************************************************
			Step 13. Roll up task level revenue to phases
		******************************************************************************************************************************/
        SET @msg = DataMart.dbo.AlertMessage('13. Roll up task level revenue to phases', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        BEGIN
            UPDATE ph
            SET ph.TotalRevenue = tsk.SumTotalRevenue
              , ph.CustRevenueLabor = tsk.SumRevenueLabor
              , ph.CustRevenueExpense = tsk.SumRevenueExpense
              , ph.CustRevenueConsultant = tsk.SumRevenueConsultant
              , ph.JTDCost = tsk.SumJTDCost
              , ph.JTDBilled = tsk.SumJTDBilled
              , ph.JTDUnbilled = tsk.SumJTDUnBilled
              , ph.TotalSpent = tsk.SumTotalSpent
            FROM dbo._Revenue ph
                INNER JOIN (
                    SELECT WBS1
                         , WBS2
                         , ISNULL(SUM(TotalRevenue), 0)          AS SumTotalRevenue
                         , ISNULL(SUM(CustRevenueLabor), 0)      AS SumRevenueLabor
                         , ISNULL(SUM(CustRevenueExpense), 0)    AS SumRevenueExpense
                         , ISNULL(SUM(CustRevenueConsultant), 0) AS SumRevenueConsultant
                         , ISNULL(SUM(JTDCost), 0)               AS SumJTDCost
                         , ISNULL(SUM(JTDBilled), 0)             AS SumJTDBilled
                         , ISNULL(SUM(JTDUnbilled), 0)           AS SumJTDUnBilled
                         , ISNULL(SUM(TotalSpent), 0)            AS SumTotalSpent
                    FROM dbo._Revenue WITH (NOLOCK)
                    WHERE WBS3 <> ' '
                    GROUP BY
                        WBS1
                      , WBS2
                )             tsk
                    ON ph.WBS1 = tsk.WBS1
                        AND ph.WBS2 = tsk.WBS2
            WHERE
                ph.WBS3 = ' '
                AND ph.WBS2 <> ' ';
        END;
        SET @msg = DataMart.dbo.AlertMessage('    13a. Roll up phase level revenue to Projects', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        -- Roll up to Projects
        BEGIN
            UPDATE p
            SET p.TotalRevenue = ph.SumTotalRevenue
              , p.CustRevenueLabor = ph.SumRevenueLabor
              , p.CustRevenueExpense = ph.SumRevenueExpense
              , p.CustRevenueConsultant = ph.SumRevenueConsultant
              , p.JTDCost = ph.SumJTDCost
              , p.JTDBilled = ph.SumJTDBilled
              , p.JTDUnbilled = ph.SumJTDUnBilled
              , p.TotalSpent = ph.SumTotalSpent
            FROM dbo._Revenue p
                INNER JOIN (
                    SELECT WBS1
                         , ISNULL(SUM(TotalRevenue), 0)          AS SumTotalRevenue
                         , ISNULL(SUM(CustRevenueLabor), 0)      AS SumRevenueLabor
                         , ISNULL(SUM(CustRevenueExpense), 0)    AS SumRevenueExpense
                         , ISNULL(SUM(CustRevenueConsultant), 0) AS SumRevenueConsultant
                         , ISNULL(SUM(JTDCost), 0)               AS SumJTDCost
                         , ISNULL(SUM(JTDBilled), 0)             AS SumJTDBilled
                         , ISNULL(SUM(JTDUnbilled), 0)           AS SumJTDUnBilled
                         , ISNULL(SUM(TotalSpent), 0)            AS SumTotalSpent
                    FROM dbo._Revenue WITH (NOLOCK)
                    WHERE
                        WBS2 <> ' '
                        AND WBS3 = ' '
                    GROUP BY WBS1
                )             ph
                    ON p.WBS1 = ph.WBS1
            WHERE p.WBS2 = ' ';
        END;

        UPDATE dbo._Revenue SET EAC = ISNULL(TotalSpent, 0)WHERE ISNULL(EAC, 0) < ISNULL(TotalSpent, 0);
        UPDATE dbo._Revenue SET ETC = ISNULL(EAC, 0) - ISNULL(TotalSpent, 0);

        -- Set Percent Complete at Phase and Project levels.
        SET @msg = DataMart.dbo.AlertMessage('    13b. Set Percent Complete at Phase and Project levels.', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        UPDATE dbo._Revenue
        SET CustPercentComplete = CASE
                                      WHEN (
                                          EAC IS NOT NULL
                                               AND EAC <> 0
                                      )
                                          THEN
                                          CASE WHEN TotalSpent >= EAC THEN '100%' ELSE REPLACE(FORMAT(TotalSpent / EAC, 'P0'), ' ', '')END
                                      ELSE
                                          NULL
                                  END
        WHERE WBS3 = ' ';

        -- Zero out Writeoffs, EAC, & ETC at the Task Level.
        SET @msg = DataMart.dbo.AlertMessage('    13b. Zero out Writeoffs, EAC, & ETC at the Task Level.', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        UPDATE wbs
        SET wbs.CustWriteoffAdjmt = 0
          , wbs.EAC = 0
          , wbs.ETC = 0
        FROM dbo._Revenue AS wbs
        WHERE wbs.WBS3 <> ' ';
        /******************************************************************************************************************************
			Step 14. Add/update project log records.
		******************************************************************************************************************************/
        SET @msg = DataMart.dbo.AlertMessage('14. Add/update project log records.', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        BEGIN TRY
            EXEC dbo.HAI_UpdateProjectLog @targetPeriod = @targetPeriod; -- int
        END TRY
        BEGIN CATCH
            SET @msg = DataMart.dbo.AlertMessage('Add/update project log records Failed!', @startTime);

            SET @msg = @log + '<br>' + @msg + '<br>Error Number: ' + ISNULL((
                                                                                SELECT CAST(ERROR_NUMBER() AS VARCHAR(255))
                                                                            )
                                                                          , ''
                                                                     );
            SET @msg = @msg + '<br>Error Severity: ' + ISNULL((
                                                                  SELECT CAST(ERROR_SEVERITY() AS VARCHAR(255))
                                                              )
                                                            , ''
                                                       );
            SET @msg = @msg + '<br>Error State: ' + ISNULL((
                                                               SELECT CAST(ERROR_STATE() AS VARCHAR(255))
                                                           )
                                                         , ''
                                                    );
            SET @msg = @msg + '<br>Error Procedure: ' + ISNULL((
                                                                   SELECT ERROR_PROCEDURE()
                                                               )
                                                             , ''
                                                        );
            SET @msg = @msg + '<br>Error Line: ' + ISNULL((
                                                              SELECT CAST(ERROR_LINE() AS VARCHAR(255))
                                                          )
                                                        , ''
                                                   );
            SET @msg = @msg + '<br>Error Message: ' + ISNULL((
                                                                 SELECT ERROR_MESSAGE()
                                                             )
                                                           , ''
                                                      ) + '</pre>';
            RAISERROR(@msg, 0, 1) WITH NOWAIT;
            SET @log = @log + @msg + '<br>';
        END CATCH;
        /******************************************************************************************************************************
			Step 15. Zero-out TotalRevenue, CustRevenueLabor, CustRevenueConsultant, CustRevenueExpense for WBS levels with sub-levels
		******************************************************************************************************************************/

        SET @msg = DataMart.dbo.AlertMessage('15. Zero-out CustRevenueLabor, CustRevenueConsultant, CustRevenueExpense for WBS records with sub-levels', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        BEGIN
            UPDATE wbs
            SET wbs.CustRevenueLabor = 0
              , wbs.CustRevenueConsultant = 0
              , wbs.CustRevenueExpense = 0
            FROM dbo._Revenue AS wbs
            WHERE wbs.SubLevel = 'Y';
        END;

        /****************************************************************************************************
			Step 16. Update (lowest level) ProjectCustomTabFields with New Revenue and WriteOff Amounts
		*****************************************************************************************************/
        SET @msg = DataMart.dbo.AlertMessage('16. Writing to dbo.ProjectCustomTabFields.', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        DECLARE @commitChanges CHAR(1) = 'Y';
        IF @context = 'current'
            BEGIN
                BEGIN TRY
                    BEGIN TRANSACTION;
                    UPDATE px
                    SET px.CustJTDRevenue = ROUND(r.TotalRevenue, 2)
                      , px.CustRevenueLabor = ROUND(r.CustRevenueLabor, 2)
                      , px.CustRevenueExpense = ROUND(r.CustRevenueExpense, 2)
                      , px.CustRevenueConsultant = ROUND(r.CustRevenueConsultant, 2)
                      , px.CustWriteoffAdjmt = IIF(r.CustWriteoffAdjmt <> 0, DataMart.dbo.Decimal2Text(ROUND(r.CustWriteoffAdjmt, 0)), '')
                      , px.CustPercentComplete = r.CustPercentComplete
                      , px.CustEffortatCompletion = IIF(ISNULL(r.EAC, 0) <> 0, DataMart.dbo.Decimal2Text(ROUND(ISNULL(r.EAC, 0), 0)), '')
                      , px.CustEfforttoComplete = IIF(ISNULL(r.ETC, 0) <> 0, DataMart.dbo.Decimal2Text(ROUND(ISNULL(r.ETC, 0), 0)), '')
                    FROM dbo.ProjectCustomTabFields AS px
                        INNER JOIN dbo._Revenue     AS r WITH (NOLOCK)
                            ON px.WBS1 = r.WBS1
                                AND px.WBS2 = r.WBS2
                                AND px.WBS3 = r.WBS3;
                END TRY
                BEGIN CATCH
                    SET @msg = DataMart.dbo.AlertMessage('Error updating ProjectCustomTabFields!', @startTime);
                    RAISERROR(@msg, 0, 1) WITH NOWAIT;
                    SELECT ERROR_NUMBER()    AS ErrorNumber
                         , ERROR_SEVERITY()  AS ErrorSeverity
                         , ERROR_STATE()     AS ErrorState
                         , ERROR_PROCEDURE() AS ErrorProcedure
                         , ERROR_LINE()      AS ErrorLine
                         , ERROR_MESSAGE()   AS ErrorMessage;
                    SET @commitChanges = 'N';
                END CATCH;

                IF @commitChanges = 'Y' COMMIT TRANSACTION; ELSE ROLLBACK TRANSACTION;
            END;
        ELSE -- Prior Period: Do not write RevenueJTD or WriteOffAdj if prior.
            BEGIN
                BEGIN TRY
                    BEGIN TRANSACTION;
                    UPDATE px
                    SET px.CustRevenueLabor = ROUND(r.CustRevenueLabor, 2)
                      , px.CustRevenueExpense = ROUND(r.CustRevenueExpense, 2)
                      , px.CustRevenueConsultant = ROUND(r.CustRevenueConsultant, 2)
                    FROM dbo.ProjectCustomTabFields AS px
                        INNER JOIN dbo._Revenue     AS r WITH (NOLOCK)
                            ON px.WBS1 = r.WBS1
                                AND px.WBS2 = r.WBS2
                                AND px.WBS3 = r.WBS3;
                END TRY
                BEGIN CATCH
                    SET @msg = DataMart.dbo.AlertMessage('Error updating ProjectCustomTabFields!', @startTime);
                    RAISERROR(@msg, 0, 1) WITH NOWAIT;
                    SET @msg = @log + '<br>' + @msg + '<br>Error Number: ' + ISNULL((
                                                                                        SELECT CAST(ERROR_NUMBER() AS VARCHAR(255))
                                                                                    )
                                                                                  , ''
                                                                             );
                    SET @msg = @msg + '<br>Error Severity: ' + ISNULL((
                                                                          SELECT CAST(ERROR_SEVERITY() AS VARCHAR(255))
                                                                      )
                                                                    , ''
                                                               );
                    SET @msg = @msg + '<br>Error State: ' + ISNULL((
                                                                       SELECT CAST(ERROR_STATE() AS VARCHAR(255))
                                                                   )
                                                                 , ''
                                                            );
                    SET @msg = @msg + '<br>Error Procedure: ' + ISNULL((
                                                                           SELECT ERROR_PROCEDURE()
                                                                       )
                                                                     , ''
                                                                );
                    SET @msg = @msg + '<br>Error Line: ' + ISNULL((
                                                                      SELECT CAST(ERROR_LINE() AS VARCHAR(255))
                                                                  )
                                                                , ''
                                                           );
                    SET @msg = @msg + '<br>Error Message: ' + ISNULL((
                                                                         SELECT ERROR_MESSAGE()
                                                                     )
                                                                   , ''
                                                              ) + '</pre>';
                    SET @commitChanges = 'N';
                END CATCH;

                IF @commitChanges = 'Y' COMMIT TRANSACTION; ELSE ROLLBACK TRANSACTION;
            END;

        --*********************************************************************************************************
        --*****         TESTS        ******************************************************************************
        --*********************************************************************************************************

        DECLARE @revRecords INT = (
                    SELECT COUNT(*) AS RevRecords FROM dbo._Revenue AS r
                );
        SET @msg = DataMart.dbo.AlertMessage('Test 1: ' + CAST(@revRecords AS VARCHAR(12)) + ' rev records generated', @startTime);
        SET @log = @log + @msg + '<br>';
        RAISERROR(@msg, 0, 1) WITH NOWAIT;
        SET @BadPhaseETC = (
            SELECT COUNT(*) AS qty
            FROM dbo._Revenue AS r
            WHERE
                r.WBS2 <> ' '
                AND r.WBS3 = ' '
                AND r.ETC <> r.EAC - r.TotalSpent
        );
        SET @BadProjectETC = (
            SELECT COUNT(*) AS qty
            FROM dbo._Revenue AS r
            WHERE
                r.WBS2 = ' '
                AND ISNULL(r.ETC, 0) <> ISNULL(r.EAC, 0) - ISNULL(r.TotalSpent, 0)
        );
        PRINT CONCAT('*****  Bad Project ETC: ', @BadProjectETC, ' at end.');
        PRINT CONCAT('*****  Bad Phase ETC:   ', @BadPhaseETC, ' at end.');
    END;

    /****************************************************************************************************
			 Send email with results
	*****************************************************************************************************/

    -- If we are running on the test server, no need to mail anybody but Craig
    IF @@SERVERNAME <> 'ACCTSQL14\VISION' SET @recipients = 'cAnderson@Halff.com';

    --
    IF @notify = 'Y'
        INSERT INTO HalffImport.dbo.CCG_Email_Messages (
            SourceApplication
          , Sender
          , ToList
          , CCList
          , BCCList
          , Subject
          , GroupSubject
          , Body
          , MaxDelay
          , Priority
          , CreateDateTime
        )
        VALUES
        ('Revenue Recognition', 'cAnderson@Halff.com', @recipients, '', '', 'Revenue Recognition Finished', '', @log + '</pre>', 0, 'H', GETDATE());
    SET NOEXEC OFF;

/*
EXEC dbo.HAI_RevenueRecognition @context = 'current' -- varchar(7)
	                              , @shortrun = 'Y' -- varchar(1)
	                              , @targetPeriod = 202106 -- int
								  , @notify = 'Y'
*/
GO
