SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_LoadDocs] ( @wbs1 nvarchar(30), @context varchar(30)= 'DRAFT', @newest bit= 0)
             AS EXEC spCCG_EI_LoadDocs @wbs1,@context,@newest
GO
