SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_DelegationHistoryGet] (@Username Nvarchar(32), @BeginDate datetime, @EndDate datetime)
AS
BEGIN
/*
	Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved.

	select * from CCG_EI_Delegation
	exec spCCG_EI_DelegationHistoryGet 'ADMIN','',''
	exec spCCG_EI_DelegationHistoryGet 'ADMIN','2/21/2017','2/28/2017'
*/
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	if isnull(@BeginDate,'') <> '' AND isnull(@EndDate,'') <> ''
		select top 1000 dh.Seq, dh.Id, dh.Delegate, dh.Employee, convert(varchar(10), dh.FromDate, 120), convert(varchar(10), dh.ToDate, 120),
				CL.Name as ForClient, PR.WBS1+N' - '+PR.Name as ForProject, dh.ActionTaken, dh.ActionTakenBy, dh.ActionDate, dh.ActionRecipient, dh.Description
			from CCG_EI_HistoryDelegation dh
				left join CL on CL.ClientId = dh.ForClientId
				left join PR on PR.WBS1 = dh.ForWBS1 and PR.WBS2 = N' ' and PR.WBS3 = N' '
			where @BeginDate <= ToDate and FromDate <= @EndDate
			order by dh.ActionDate desc
	else
		select top 1000 dh.Seq, dh.Id, dh.Delegate, dh.Employee, convert(varchar(10), dh.FromDate, 120), convert(varchar(10), dh.ToDate, 120),
				CL.Name as ForClient, PR.WBS1+N' - '+PR.Name as ForProject, dh.ActionTaken, dh.ActionTakenBy, dh.ActionDate, dh.ActionRecipient, dh.Description
			from CCG_EI_HistoryDelegation dh
				left join CL on CL.ClientId = dh.ForClientId
				left join PR on PR.WBS1 = dh.ForWBS1 and PR.WBS2 = N' ' and PR.WBS3 = N' '
			order by dh.ActionDate desc
END
GO
