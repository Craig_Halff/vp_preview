SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_PctCompleteSaveToVision]
	@wbs1			Nvarchar(30),
	@wbs2			Nvarchar(7),
	@wbs3			Nvarchar(7),
	@seq			varchar(20),
	@wbsList		Nvarchar(max),			-- For saving multiple phases/tasks at once
	@removeFromEI	int							-- 0 or 1
AS
BEGIN
	/*
	-- WAS CALLED: DeltekStoredProc_CCG_EI_Pct_Complete_Save_To_Vision
	select * from CCG_EI_FeePctCpl eip
	spCCG_EI_PctCompleteSaveToVision @seq = '27', @wbs1 = '2003005.00', @wbs2 = '6CA', @wbs3 = ' ', @wbsList = '', @removeFromEI = 1
	spCCG_EI_PctCompleteSaveToVision @wbs1='',
		@wbs2='',
		@wbs3='',
		@seq='',
		@wbsList='''2003005.00| | |0'',''2003005.00|1PD| |0'',''2003005.00|5BD| |0''',
		@removeFromEI='1'
	*/
	SET NOCOUNT ON
	declare @sql varchar(max) = ''

	declare @FeeByCategoryEnabled varchar(1) = 'N'
	select @FeeByCategoryEnabled = [FeeByCategoryEnabled] from CFGBillMainData

	IF ISNULL(@wbs2,'') = '' set @wbs2 = ' '
	IF ISNULL(@wbs3,'') = '' set @wbs3 = ' '

	set @wbsList = isnull(@wbsList,'')
	set @wbs1 = isnull(@wbs1,'')
	if isnull(@wbsList,'') <> '' and left(@wbsList, 1) <> '''' set @wbsList = ''''+@wbsList+''''

	declare @where varchar(max) = (case
			when @wbsList <> '' then '(eip.WBS1+''|''+eip.WBS2+''|''+eip.WBS3+''|''+cast(eip.Seq as varchar(20)) in ('+@wbsList+')) and '
			when @wbs1 <> '' then '(eip.WBS1 = N'''+@wbs1+''' and eip.WBS2 = N'''+@wbs2+''' and eip.WBS3 = N'''+@wbs3+''') and '
			else ''
		end) + (case when @wbsList = '' and isnull(@seq,'') <> '' then 'eip.Seq = '+cast(@seq as varchar(20))+ ' and ' else '' end)

	-- Update Records without Billing Phases
	-- Fee Method 1, 2, 3
	set @sql = '
	Update BT set
			BT.FeePctCpl = case when BT.FeeByDetailEnabled = ''N'' or BT.FeeMeth < 2 then isnull(eip.FeePctCpl, BT.FeePctCpl) else BT.FeePctCpl end,
			BT.FeeToDate = case when BT.FeeByDetailEnabled = ''N'' or BT.FeeMeth < 2 then isnull(eip.FeeToDate, BT.FeeToDate) else BT.FeeToDate end,
			BT.FeePctCpl1 = isnull(eip.FeePctCpl1, BT.FeePctCpl1), BT.FeeToDate1 = isnull(eip.FeeToDate1, isnull(eip.FeePctCpl1, BT.FeePctCpl1) * BT.Fee1 / 100.0),
			BT.FeePctCpl2 = isnull(eip.FeePctCpl2, BT.FeePctCpl2), BT.FeeToDate2 = isnull(eip.FeeToDate2, isnull(eip.FeePctCpl2, BT.FeePctCpl2) * BT.Fee2 / 100.0),
			BT.FeePctCpl3 = isnull(eip.FeePctCpl3, BT.FeePctCpl3), BT.FeeToDate3 = isnull(eip.FeeToDate3, isnull(eip.FeePctCpl3, BT.FeePctCpl3) * BT.Fee3 / 100.0),
			BT.FeePctCpl4 = isnull(eip.FeePctCpl4, BT.FeePctCpl4), BT.FeeToDate4 = isnull(eip.FeeToDate4, isnull(eip.FeePctCpl4, BT.FeePctCpl4) * BT.Fee4 / 100.0),
			BT.FeePctCpl5 = isnull(eip.FeePctCpl5, BT.FeePctCpl5), BT.FeeToDate5 = isnull(eip.FeeToDate5, isnull(eip.FeePctCpl5, BT.FeePctCpl5) * BT.Fee5 / 100.0)
		from CCG_EI_FeePctCpl eip
			inner join BT on BT.WBS1 = eip.WBS1 and BT.WBS2 = eip.WBS2 and BT.WBS3 = eip.WBS3
		where '+@where + ' BT.FeeMeth <= 3 and eip.Seq = 0'
	exec(@sql)

	-- Update Billing Phases
	-- Fee Method 2, 3
	set @sql = '
	Update BTF set
			BTF.PctComplete = isnull(eip.FeePctCpl, BTF.PctComplete), BTF.FeeToDate = eip.FeeToDate
		from CCG_EI_FeePctCpl eip
			inner join BT on BT.WBS1 = eip.WBS1 and BT.WBS2 = eip.WBS2 and BT.WBS3 = eip.WBS3
			inner join BTF on BTF.WBS1 = eip.WBS1 and BTF.WBS2 = eip.WBS2 and BTF.WBS3 = eip.WBS3 and BTF.Seq = eip.Seq
			left join BTFCategory bc on bc.WBS1 = eip.WBS1 and bc.WBS2 = eip.WBS2 and bc.WBS3 = eip.WBS3 and bc.Seq = eip.Seq
		where '+@where + ' BT.FeeMeth <= 3 ' 
		if @FeeByCategoryEnabled = 'Y'
			set @sql = @sql +  ' and (BT.FeeMeth = 2 or BT.FeeByDetailEnabled = ''N'' or bc.Fee is null)'
	exec(@sql)
			
	if @FeeByCategoryEnabled = 'Y' --added this since FeeByDetailEnabled as multiple meaning and BTFCategory can have records in it if it was one time enabled, but later disabled
	begin			
		-- Update Categories
		-- Fee Method 3
		set @sql = '
		Update bc set
				bc.PctComplete = case bc.Category
					when 1 then isnull(eip.FeePctCpl1, bc.PctComplete) when 2 then isnull(eip.FeePctCpl2, bc.PctComplete)
					when 3 then isnull(eip.FeePctCpl3, bc.PctComplete) when 4 then isnull(eip.FeePctCpl4, bc.PctComplete)
					when 5 then isnull(eip.FeePctCpl5, bc.PctComplete) end,
				bc.FeeToDate =
					case bc.Category
						when 1 then isnull(eip.FeeToDate1, isnull(eip.FeePctCpl1, bc.PctComplete) * bc.Fee / 100.0)
						when 2 then isnull(eip.FeeToDate2, isnull(eip.FeePctCpl2, bc.PctComplete) * bc.Fee / 100.0)
						when 3 then isnull(eip.FeeToDate3, isnull(eip.FeePctCpl3, bc.PctComplete) * bc.Fee / 100.0)
						when 4 then isnull(eip.FeeToDate4, isnull(eip.FeePctCpl4, bc.PctComplete) * bc.Fee / 100.0)
						when 5 then isnull(eip.FeeToDate5, isnull(eip.FeePctCpl5, bc.PctComplete) * bc.Fee / 100.0) end
			from CCG_EI_FeePctCpl eip
				inner join BT on BT.WBS1 = eip.WBS1 and BT.WBS2 = eip.WBS2 and BT.WBS3 = eip.WBS3
				inner join BTF on BTF.WBS1 = eip.WBS1 and BTF.WBS2 = eip.WBS2 and BTF.WBS3 = eip.WBS3 and BTF.Seq = eip.Seq
				inner join BTFCategory bc on bc.WBS1 = eip.WBS1 and bc.WBS2 = eip.WBS2 and bc.WBS3 = eip.WBS3 and bc.Seq = eip.Seq
			where '+@where + ' BT.FeeMeth = 3 and BT.FeeByDetailEnabled = ''Y'' '
		exec(@sql)

		-- Fee Method 3 / Categories - Update Billing Phase Totals
		set @sql = '
		Update BTF set
				BTF.PctComplete = bc.FeeToDate / bc.Fee * 100.0, BTF.FeeToDate = bc.FeeToDate
			from CCG_EI_FeePctCpl eip
				inner join BT on BT.WBS1 = eip.WBS1 and BT.WBS2 = eip.WBS2 and BT.WBS3 = eip.WBS3
				inner join BTF on BTF.WBS1 = eip.WBS1 and BTF.WBS2 = eip.WBS2 and BTF.WBS3 = eip.WBS3 and BTF.Seq = eip.Seq
				outer apply (select sum(bc.Fee) as Fee, sum(bc.FeeToDate) as FeeToDate
					from BTFCategory bc
					where bc.WBS1 = eip.WBS1 and bc.WBS2 = eip.WBS2 and bc.WBS3 = eip.WBS3 and bc.Seq = eip.Seq) bc
			where '+@where + ' BT.FeeMeth = 3 and BT.FeeByDetailEnabled = ''Y'' and bc.Fee is not null '
		exec(@sql)
	end

	-- Fee Method 4, 5 (Unit or fee)
	set @sql = '
	Update BT set
			BT.FeeFactor1 =
				case when BT.FeeBasis = ''L'' then eip.FeeToDate
					when BT.FeeBasis = ''U''  then eip.FeeToDate / BT.FeeFactor2 /*== UnitsToDate */
				end
		from CCG_EI_FeePctCpl eip
			inner join BT on BT.WBS1 = eip.WBS1 and BT.WBS2 = eip.WBS2 and BT.WBS3 = eip.WBS3
		where '+@where + ' BT.FeeMeth in (4,5) and BT.FeeBasis in (''U'',''L'') --and BT.FeeByDetailEnabled = ''N'' '
	exec(@sql)
	

	if @removeFromEI = 1
	begin
		-- Now remove from EI table
		set @sql = 'DELETE eip FROM CCG_EI_FeePctCpl as eip WHERE '+@where + ' 1=1'
		exec(@sql)
	end
END
GO
