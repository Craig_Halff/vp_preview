SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Note - this version is for a multi-company database as it incorporates the Company value into the Batch to keep things unique
--Need to run for each company separately

--DHB - Edited 7/8/2019
--		Removing heavily commented lines to keep SQL line under 8000 characters
--		Also removing Begin\Commit Transaction as that appears to hang the script

CREATE PROCEDURE [dbo].[ccgGLJournalEntriesDirectWrite72] (
--Create PROCEDURE ccgGLJournalEntriesDirectWrite72 (
   @dest        varchar(255), 
   @Company     varchar(50), 
   @PeriodStart varchar(6), 
   @PeriodEnd   varchar(6), 
   @TableName   varchar(255)) 
AS
/*
02/26/2013 David Springer
           Revised to loop through periods
   For use in conjunction with QuickBooks Import General Ledger script
   
   @Dest = Vision Database Name
   @Company = pass empty string if not multi-company
   @Tablename = Journal Entry Import Table Name (JE)

****************************************************************   
   Instructions
****************************************************************   
1.   Insert transactions from all periods to VisionImportDEJournalEntry_JE.
2.   Validate all transactions and verify that there are no errors. - DO NOT IMPORT!!!
2.b	 If running for multi-company database, break company data into separate companies
		Select * into VisionImportDEJournalEntry_JE_XXX from VisionImportDEJournalEntry_JE where left(org,3)='XXX'
3.   Determine periods to import:

     Select 
        Min (convert (varchar (6), TransDate, 112)) MinPeriod,
        Max (convert (varchar (6), TransDate, 112)) MaxPeriod
     From VisionImportDEJournalEntry_JE_04
   Order by 1, 2

	 Select left(org,3),convert (varchar (6), TransDate, 112) Period  From VisionImportDEJournalEntry_JE group by left(org,3),convert (varchar (6), TransDate, 112) order by left(org,3),convert (varchar (6), TransDate, 112)
     
4.   Execute this procedure.  The parameters to be passed are as follows.  If the database does not have multi-company enabled then pass two singel apostrophes for company.

                                Target DB, Company, Start, End, Table

Exec ccgGLJournalEntriesDirectWrite72 'HalffImport', '04', '201901', '201910', 'JE_04'
go

Exec ccgGLJournalEntriesDirectWrite72 'HalffImport', '05', '201901', '201910', 'JE_05'
go

****************************************************************   

Select * from VisionImportDEJournalEntry_JE where errormessage<>' '
	Select * from PR where left(wbs1,1)='Z'
	Select * from CA order by account
	Update VisionImportDEJournalEntry_JE set wbs1='ZZZC-ATCH.RE', wbs2='000', wbs3=' ' 
		from VisionImportDEJournalEntry_JE JE join CA on je.account=ca.account
		where errormessage like '%WBS1 is require%' and left(ca.type,1) in (4,5,6,7,8)
	Update VisionImportDEJournalEntry_JE set wbs1='ZZZC-ATCH.OH', wbs2='000', wbs3=' ' 
		from VisionImportDEJournalEntry_JE JE join CA on je.account=ca.account
		where errormessage like '%WBS1 is require%' and left(ca.type,1) in (9,10)
Select errormessage from VisionImportDEJournalEntry_JE where errormessage<>' ' group by errormessage
	Update VisionImportDEJournalEntry_JE set account='199.00' where account='UNDEPO'
	Update VisionImportDEJournalEntry_JE set account='621.10' where account='621.00'

Select * from VisionImportDEJournalEntry_JE where  (convert (varchar (6), TransDate, 112))>201411
	

***** - To Start Over and remove previous import
Delete LedgerMisc where SkipGL='N' and transtype='JE' and Desc1<>'Account Opening Balance'
Delete jeMaster where Description<>'Account Opening Balance'
Delete jeControl where Batch not in (Select batch from jemaster)
Delete jeDetail where Batch not in (Select batch from jemaster)
Delete CFGPostControl where transtype='JE' and Batch not in (Select batch from jemaster)


--GL
Select * from LedgerMisc order by period
Select * into zLedgerMisc_2014110 from LedgerMisc
go

Delete LedgerMisc where TransType='JE' and Period>201312
go

Delete jeMaster where Batch>201312
Delete jeControl where Batch>201312
Delete jeDetail where Batch>201312

*****
*/

Set NoCount OFF

Declare @Period varchar(6)
Declare @sql    varchar(8000)
Declare @batch varchar(100)

WHILE @PeriodStart <= @PeriodEnd
BEGIN

Set @Period = @PeriodStart
Print @Period
Set @Batch = @period+'_'+@Company
Print @Batch

-- One error record exists in the VisionImportDEJournalEntry_JE table, so it will not be deleted
-- Therefore, first IF statement will equate to 1, not 0. 
-- DHB - altered to 0 as there should be no errors my way of importing
set @sql=
' USE ' + @dest + '' +
' IF (select sum(case when (ErrorMessage is null or convert(varchar(10),ErrorMessage)='''') then 0 else 1 end)' +
' from VisionImportDEJournalEntry_' + @TableName + ')=0' +
' BEGIN' +

--Vision 7
--select * from GW_Vision..jemaster
' insert into JEMaster(Batch, Refno, TransDate, Type, Description, Posted, Seq, UpdateBilled,' +
' CurrencyCode, CurrencyExchangeOverrideMethod, CurrencyExchangeOverrideDate, CurrencyExchangeOverrideRate,' +
'	Status, AuthorizedBy, RejectReason, ModUser, ModDate)' +
' select ''' + @batch + ''', V.Refno, Max(V.TransDate) AS TransDate,' +
' Max(case when V.PostType = ''C'' then ''2'' else case when V.PostType = ''B'' then ''3'' else ''1'' end end) as Type,' +
' Max(case when V.MasterDescription is null then ''Journal Entry from CCG Import Module''' +
' else left(V.MasterDescription,40) end) as Description, ''N'', 1, Max(ISNULL(V.UpdateBilled,''N'')),' +
' '' '' CurrencyCode,' +
'	''N'' as CurrencyExchangeOverrideMethod, NULL as CurrencyExchangeOverrideDate, 0 as CurrencyExchangeOverrideRate,' +
'	''N'' as Status, NULL, NULL, NULL, getdate() as ModDate' +
' From VisionImportDEJournalEntry_' + @TableName + ' V' +
' where (ErrorMessage is null or convert(varchar(10),ErrorMessage)='''')' +
'   and convert (varchar (6), v.TransDate, 112) = ''' + @Period + '''' +
' GROUP BY V.Refno' +
' ORDER BY V.Refno' +



--Vision 7
--select * from GW_Vision..jedetail
' INSERT INTO JEDetail ( Batch, Refno, PKey, Seq, WBS1, WBS2, WBS3, Account, DebitAmount, CreditAmount,' +
' Description, TaxType, SuppressBill) ' +
' select ''' + @batch + ''', V.Refno, V.PKey, 1, V.WBS1, V.WBS2,' +
' V.WBS3, V.Account, ROUND(isnull(V.DebitAmount, 0), 2), ROUND(isnull(V.CreditAmount, 0), 2),' +
' left(V.DetailDescription,40), V.TaxType, isnull(V.SuppressBill, ''N'')' +
' From VisionImportDEJournalEntry_' + @tablename + ' V' +
' where (ErrorMessage is null or convert(varchar(10),ErrorMessage)='''')' +
'     and convert (varchar (6), v.TransDate, 112) = ''' + @Period + '''' +

--Vision 7
--select * from GW_Vision..jecontrol
' INSERT INTO jeControl (Company, Batch, Recurring, Selected, Posted, Creator, Period, EndDate, Total,' +
' AllowTaxEntries, DefaultCurrencyCode,PostSeq)' +
' SELECT ''' + @Company + ''', ''' + @batch + ''' AS Batch,' +
' ''N'' AS Recurring, ''N'' AS Selected, ''N'' AS Posted,' + 
' ''CCG Import Module'' AS Creator, ' + @Period + ' AS Period, max(AccountPdEnd) AS EndDate,' +
' Sum(ROUND(isnull(V.CreditAmount, 0),  2)) AS Total,' +
' ''N'' AS AllowTaxEntries,' +
' '' '' CurrencyCode, 1' +
' FROM VisionImportDEJournalEntry_' + @TableName + ' V' +
'	LEFT JOIN CFGDates CFGDates on cfgDates.Period=' + @Period + '' +
' where (ErrorMessage is null or convert(varchar(10),ErrorMessage)='''')' +
'     and convert (varchar (6), v.TransDate, 112) = ''' + @Period + '''' +

' END'
print @sql execute(@sql)

--Post Transactions
set @sql=
' USE ' + @dest + '' +
--' SET TRANSACTION ISOLATION LEVEL READ COMMITTED;BEGIN TRANSACTION' +

' Update JEControl Set Selected = ''Y'' Where Batch = ''' + @batch + ''' And Selected = ''N''' 

-- + ' COMMIT TRANSACTION'
print @sql execute(@sql)

set @sql=
' USE ' + @dest + '' +
--' SET TRANSACTION ISOLATION LEVEL READ COMMITTED;BEGIN TRANSACTION' +

' Update CFGDates Set LastPostSeq=LastPostSeq + 1  WHERE Period = ' + @Period + '' +

--select * from GW_Vision..cfgpostcontrol
' Insert Into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed,' +
' Canceled, Company , Batch)' +
' SELECT ' + @Period + ', IsNull ((select LastPostSeq from CFGDates WHERE Period = ' + @Period + '), 1),' +
' ''CCG'', getdate(), ''JE'', ''N'', ''N'',''' + @Company + ''', ''' + @Batch + '''' 
-- + ' COMMIT TRANSACTION'
print @sql execute(@sql)

set @sql=
' USE ' + @dest + '' +
' Update jeControl Set PostPeriod = ' + @Period + ',' +
' PostSeq = (select LastPostSeq from CFGDates WHERE Period = ' + @Period + '),' +
' Posted = ''F'' Where Batch = ''' + @Batch + ''''
print @sql execute(@sql)

set @sql=
' USE ' + @dest + '' +
--' SET TRANSACTION ISOLATION LEVEL READ COMMITTED;BEGIN TRANSACTION' +
--select * from GW_Vision..LedgerMisc
' INSERT INTO LedgerMisc(Period, PostSeq, PKey, WBS1, WBS2, WBS3, Account, Org, TransType ,' +
' SubType, RefNo, TransDate, Desc1, Desc2, Amount, CBAmount, BillExt, ProjectCost, SuppressBill ,' +
' BillStatus, BankCode, Invoice, InvoiceSection, Employee, Vendor, Line, PartialPayment, Discount ,' +
' Voucher, BilledWBS1, BilledWBS2, BilledWBS3, BilledInvoice, BilledPeriod, Unit, UnitTable ,' +
' UnitQuantity, UnitCostRate, UnitBillingRate, UnitBillExt, XferWBS1, XferWBS2, XferWBS3, XferAccount ,' +
' TaxCode, ' +
' TaxBasis, TaxCBBasis, BillTaxCodeOverride, WrittenOffPeriod ,' +
' TransactionAmount, TransactionCurrencyCode, ExchangeInfo, AmountProjectCurrency, ProjectExchangeInfo ,' +
' AmountBillingCurrency, BillingExchangeInfo, AutoEntryAmount, AutoEntryExchangeInfo, AutoEntryOrg ,' +
' AutoEntryAccount, AmountSourceCurrency, SourceExchangeInfo, PONumber, UnitCostRateBillingCurrency ,' +
' LinkCompany, LinkWBS1, LinkWBS2, LinkWBS3,' +
' RealizationAmountEmployeeCurrency, RealizationAmountProjectCurrency,' +
' RealizationAmountBillingCurrency, NonBill, CreditMemoRefNo,' +
' OriginalAmountSourceCurrency, OriginalPaymentCurrencyCode )' +
--END 7.0
' SELECT c.Period as Period, (select PostSeq from jeControl WHERE Batch=''' + @batch + ''') as PostSeq,' +
' ''CCG''+d.PKEY as PKEY, d.WBS1, d.WBS2, d.WBS3, d.Account,' +
--' CASE WHEN pr3.Org IS NOT NULL THEN pr3.Org WHEN pr2.Org IS NOT NULL THEN pr2.Org ELSE ISNULL(pr1.Org,(select DefaultOrg from ' + @dest + '..CFGSystem)) END,' +
' CASE WHEN pr3.Org IS NOT NULL THEN pr3.Org ELSE (select DefaultOrg from ' + @dest + '..CFGSystem) END,' +
' ''JE'' as TransType, NULL as SubType,' +
' RIGHT(''0000000000''+cast(d.RefNo as varchar(50)),(select refnolength from ' + @dest + '..CFGFormat)), v.TransDate, m.Description as Desc1, d.Description as Desc2,' +
' CASE WHEN m.Type<>1 THEN 0 ELSE CASE WHEN DebitAmount=0 THEN -CreditAmount ELSE DebitAmount END END as Amount,' +
' CASE WHEN m.Type=1 THEN 0 ELSE CASE WHEN DebitAmount=0 THEN -CreditAmount ELSE DebitAmount END END as CBAmount,' +
' CASE WHEN m.Type<>1 THEN 0 ELSE CASE WHEN ca.Type BETWEEN 7 and 9 and PR3.WBS1 is not null THEN' +
'	CASE WHEN DebitAmount=0 THEN -CreditAmount ELSE DebitAmount END ELSE 0 END END as BillExt,' +
' CASE WHEN m.Type<>1 THEN ''N'' ELSE CASE WHEN ca.Type BETWEEN 7 and 9 and PR3.WBS1 is not null THEN ''Y'' ELSE ''N'' END END as ProjectCost,' +
' ''Y'' as SuppressBill,' +
' CASE WHEN ca.Type BETWEEN 7 and 9 and PR3.WBS1 is not null THEN ''B'' ELSE NULL END as BillStatus,' +
' NULL as BankCode, NULL as Invoice, NULL as InvoiceSection, NULL as Employee, NULL as Vendor, 0 as Line,' +
' 0 as PartialPayment, 0 as Discount, NULL as Voucher, NULL as BilledWBS1, NULL as BilledWBS2, NULL as BilledWBS3,' +
' NULL as BilledInvoice, 0 as BilledPeriod, NULL as Unit, NULL as UnitTable, 0 as UnitQuantity, 0 as UnitCostRate,' +
' 0 as UnitBillingRate, 0 as UnitBillExt, NULL as XferWBS1, NULL as XferWBS2, NULL as XferWBS3, NULL as XferAccount,' +
' NULL as TaxCode, ' +

'  0 as TaxBasis, 0 as TaxCBBasis, NULL as BillTaxCodeOverride,' +
'  0 as WrittenOffPeriod, CASE WHEN DebitAmount=0 THEN -CreditAmount ELSE DebitAmount END as TransactionAmount,' +
' CurrencyCode as TransactionCurrencyCode,' +
' ''<parms><ExchgRate>1</ExchgRate><OvrDateUsed>N</OvrDateUsed><OvrRateUsed>N</OvrRateUsed><OvrAmtUsed>N</OvrAmtUsed><InvUsed>N</InvUsed><TriUsed>N</TriUsed><ExchgRate2>0</ExchgRate2><InvUsed2>N</InvUsed2></parms>'' as ExchangeInfo,' +
' CASE WHEN /*ca.Type BETWEEN 7 and 9 and*/ PR3.WBS1 is not null THEN' + 
'	CASE WHEN DebitAmount=0 THEN -CreditAmount ELSE DebitAmount END ELSE 0 END as AmountProjectCurrency,' +
' CASE WHEN /*ca.Type BETWEEN 7 and 9 and*/ PR3.WBS1 is not null THEN' + 
'   ''<parms><Memo>Direct from amount</Memo></parms>'' ELSE NULL END as ProjectExchangeInfo,' +
' CASE WHEN /*ca.Type BETWEEN 7 and 9 and*/ PR3.WBS1 is not null THEN ' +
' 	CASE WHEN DebitAmount=0 THEN -CreditAmount ELSE DebitAmount END ELSE 0 END as AmountBillingCurrency,' +
' CASE WHEN /*ca.Type BETWEEN 7 and 9 and*/ PR3.WBS1 is not null THEN' + 
' ''<parms><Memo>Direct from amount</Memo></parms>'' ELSE NULL END as BillingExchangeInfo,' +
' -CASE WHEN m.Type<>1 THEN 0 ELSE CASE WHEN DebitAmount=0 THEN -CreditAmount ELSE DebitAmount END END as AutoEntryAmount,' +
' ''<parms><ExchgRate>1</ExchgRate><OvrDateUsed>N</OvrDateUsed><OvrRateUsed>N</OvrRateUsed><OvrAmtUsed>N</OvrAmtUsed><InvUsed>N</InvUsed><TriUsed>N</TriUsed><ExchgRate2>0</ExchgRate2><InvUsed2>N</InvUsed2></parms>'' as AutoEntryExchangeInfo,' +
--' CASE WHEN pr3.Org IS NOT NULL THEN pr3.Org WHEN pr2.Org IS NOT NULL THEN pr2.Org ELSE ISNULL(pr1.Org,(select DefaultOrg from ' + @dest + '..CFGSystem)) END as AutoEntryOrg, NULL as AutoEntryAccount, ' +
' CASE WHEN pr3.Org IS NOT NULL THEN pr3.Org ELSE (select DefaultOrg from ' + @dest + '..CFGSystem) END as AutoEntryOrg, NULL as AutoEntryAccount, ' +
' 0 as AmountSourceCurrency, NULL as SourceCurrencyInfo, NULL as PONumber, 0 as UnitCostRateBillingCurrency,' +
' NULL as LinkCompany, NULL as LinkWBS1, NULL as LinkWBS2, NULL as LinkWBS3,' +
 ' 0 as RealizationAmountEmployeeCurrency, 0 as RealizationAmountProjectCurrency,' +
' 0 as RealizationAmountBillingCurrency, ''N'' as NonBill, NULL as CreditMemoRefNo,' +
' 0 as OriginalAmountSourceCurrency, NULL as OriginalPaymentCurrencyCode' +
--End 7.0
' FROM jecontrol c' +
' inner join jemaster m on m.Batch=c.Batch' +
' inner join jedetail d on d.Batch=c.Batch and d.RefNo = m.RefNo' +
' INNER JOIN ca ca on ca.Account=d.Account' +
--' LEFT JOIN PR As PR1 ON d.WBS1 = PR1.WBS1 AND PR1.WBS2 = '' '' And PR1.WBS3 = '' ''' +
--' LEFT JOIN PR As PR2 ON d.WBS1 = PR2.WBS1 AND d.WBS2 = PR2.WBS2 And PR2.WBS3 = '' ''' +
' LEFT JOIN PR As PR3 ON d.WBS1 = PR3.WBS1 AND d.WBS2 = PR3.WBS2 And d.WBS3 = PR3.WBS3' +
' LEFT JOIN (SELECT Pkey, TransDate FROM VisionImportDEJournalEntry_' + @TableName + ') v on v.Pkey=d.Pkey' +
' WHERE c.Batch = ''' + @Batch + ''' And m.Posted <> ''Y'' AND d.Account Is Not Null' +
' ORDER BY m.Seq, d.Seq' +

' Update jeMaster set Posted=''Y'' Where Batch=''' + @batch + '''' +

' UPDATE CFGPostControl ' +
' SET PostComment=' +
' cast((select count(*) from jedetail where Batch=''' + @batch + ''') as varchar(10))' +
'	+'' recs, Pd end ''+ (select convert(varchar(10),EndDate,101) from jeControl WHERE Batch=''' + @batch + '''), ' +
' RecordCount=(select count(*) from jeDetail where Period=' + @Period + ' and Batch=''' + @batch + ''')' +
' WHERE (Period=' + @Period + ') AND (PostSeq=(select PostSeq from jeControl WHERE Batch=''' + @batch + '''))' 

-- + ' COMMIT TRANSACTION'
print @sql execute(@sql)

set @sql=
' USE ' + @dest + '' +
--' SET TRANSACTION ISOLATION LEVEL READ COMMITTED;BEGIN TRANSACTION' +

' Update jeControl Set Posted = ''Y'' Where Batch = ''' + @batch + '''' +

' UPDATE CFGPostControl SET Completed = ''Y'' , ' +
' EndDate = (select EndDate from jeControl WHERE Batch=''' + @batch + ''')' +
' WHERE (Period = ' + @Period + ') AND (PostSeq = (select PostSeq from jeControl WHERE Batch=''' + @batch + '''))'

-- + ' COMMIT TRANSACTION'
print @sql execute(@sql)

set @sql=
' USE ' + @dest + '' +
--' SET TRANSACTION ISOLATION LEVEL READ COMMITTED;BEGIN TRANSACTION' +

' Update CFGDates Set OHAllocNeeded = ''Y'' Where Period = ' + @Period + '' 

-- + ' COMMIT TRANSACTION'
print @sql execute(@sql)

set @sql=
' USE ' + @dest + '' +
' Update JEControl Set Selected = ''N'' Where Batch = ''' + @batch + ''''
print @sql execute(@sql)

IF @PeriodStart=@PeriodEnd
      BREAK
   ELSE
	IF RIGHT(@PeriodStart, 2)='12'
	Set @PeriodStart=@PeriodStart+89 
	ELSE Set @PeriodStart=@PeriodStart+1
      CONTINUE
END
GO
