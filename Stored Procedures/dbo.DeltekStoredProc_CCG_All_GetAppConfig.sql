SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_All_GetAppConfig] ( @app varchar(50))
             AS EXEC spCCG_All_GetAppConfig @app
GO
