SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_CollectionsBillerChange]
    @wbs VARCHAR(11), @oldBiller VARCHAR(8), @newBiller VARCHAR(8)
AS
    /* 
Copyright (c) 2020 Halff Associates, Inc.  All rights reserved.
Update the Biller and RequestedActionOwner when PA changes for a project.

09/30/2020  Craig Anderson (left the name with the CCG prefix just to keep the stored procedures together alphabetically)

*/
    BEGIN
        SET NOCOUNT ON;

        BEGIN TRANSACTION;
        UPDATE dbo.UDIC_Collection
        SET CustRequestedActionOwner = @newBiller
        WHERE
            CustProject = @wbs
            AND CustRequestedActionOwner = @oldBiller
            AND CustCollectionStatus = 'Open'
            AND CustRequestedActionStatus = 'Incomplete';

        UPDATE dbo.UDIC_Collection
        SET CustBiller = @newBiller
        WHERE
            CustProject = @wbs
            AND CustBiller = @oldBiller
            AND CustCollectionStatus = 'Open';
        COMMIT TRANSACTION;
    END;
GO
