SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_Status](@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7)) 
AS BEGIN
/*
	Copyright (c) 2016 EleVia Software  All rights reserved.
*/
	declare @ErrNum int, @ErrMsg varchar(255), @NewValue varchar(255)

	BEGIN TRY
		-- Do our error checking:
		select @NewValue=Upper(Status) from CCG_EI_CustomColumns where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3
		if isnull(@newvalue,' ') = 'C' set @newvalue = 'D'
		--exec spCCG_EI_Status_CHECK @WBS1, @WBS2, @WBS3, @NewValue
		--if @ReturnValue < 0 return
		if @NewValue not in ('A','I','D')
			select -1 as ReturnValue, 'Invalid value.  Status must be A, I or C.' as ReturnMessage
		else if @WBS2 = ' '
             begin--top level, update all sublevels
                    --update PR set Status=@NewValue where WBS1=@WBS1
					update ProjectCustomTabFields set CustBillingStatus=@NewValue where WBS1=@WBS1
                    select 1 as ReturnValue, 'Successful update' as ReturnMessage
             end
        else if @WBS3 = ' '
             begin--update levels 2 and 3
                    --update PR set Status=@NewValue where WBS1=@WBS1 and WBS2=@WBS2
					update ProjectCustomTabFields set CustBillingStatus=@NewValue where WBS1=@WBS1 and WBS2=@WBS2
                    select 1 as ReturnValue, 'Successful update' as ReturnMessage
             end

		else begin
			--update PR set Status=@NewValue where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3
			update ProjectCustomTabFields set CustBillingStatus=@NewValue where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3
			select 1 as ReturnValue, 'Successful update' as ReturnMessage
			end
	END TRY
	BEGIN CATCH
		set @ErrNum=ERROR_NUMBER()
		set @ErrMsg=ERROR_MESSAGE()
		select -1 as ReturnValue, 'Unknown error: ' + CAST(@ErrNum as varchar) + ' - ' + @ErrMsg as ReturnMessage
	END CATCH
END
GO
