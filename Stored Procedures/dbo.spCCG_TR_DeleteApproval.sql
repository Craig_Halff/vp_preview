SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_TR_DeleteApproval](@TableName varchar(10), @Period int, @PostSeq int, @PKey varchar(32))
AS BEGIN

	declare @sql varchar(255)
	set @sql = 'delete from CCG_TR_Approvals where TableName=''' + @TableName + ''' and Period=' + Cast(@Period as varchar) + 
		' and PostSeq=' + Cast(@PostSeq as varchar) + ' and PKey=''' + @PKey + ''''
	print @sql
	execute(@sql)
END
GO
