SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_GetSQLRoleFields] ( @sqlPart varchar(max))
             AS EXEC spCCG_EI_GetSQLRoleFields @sqlPart
GO
