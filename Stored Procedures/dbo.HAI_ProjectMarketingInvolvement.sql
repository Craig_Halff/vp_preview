SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[HAI_ProjectMarketingInvolvement]
AS
/*
2021-07-27	David Springer 
			Created for Vantagepoint, and added Marketing Manager
*/
BEGIN
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    -- Set CustCorporateMarketing = 'Yes' if the Marketing Team has charged to the Opportunity
    BEGIN TRANSACTION;
    UPDATE  px
    SET px.CustCorporateMarketing = 'Yes',
		px.CustMarketingManager = IsNull (o.CustMarketingManager, px.CustMarketingManager)
    FROM    dbo.ProjectCustomTabFields AS px
    INNER JOIN dbo.PR AS p
        ON px.WBS1 = p.WBS1
    INNER JOIN (SELECT  DISTINCT WBS1 FROM  dbo.LD WHERE RIGHT(EmOrg, 3) = '992') AS l
        ON p.SiblingWBS1 = l.WBS1
           AND  IsNull (px.CustCorporateMarketing, '') = 'No'
	Left Join UDIC_OrganizationStructure o on p.Org = o.CustOrganization
    COMMIT TRANSACTION;


    -- Set CustCorporateMarketing = 'No' on CLOSED Opportunities if the Marketing Team has not charged to the Opportunity.
    BEGIN TRANSACTION;
    UPDATE  px
    SET px.CustCorporateMarketing = 'No'
    FROM    dbo.ProjectCustomTabFields AS px
    INNER JOIN dbo.PR AS p
        ON px.WBS1 = p.WBS1
    INNER JOIN dbo.PR AS promo
        ON p.SiblingWBS1 = promo.WBS1
           AND  promo.Status = 'D'
           AND  promo.WBS2 = ' '
    WHERE IsNull (px.CustCorporateMarketing, '') = 'Yes'
	  AND NOT EXISTS (
        SELECT  'x'
        FROM    dbo.LD AS l
        WHERE   l.WBS1 = promo.WBS1
                AND RIGHT(l.EmOrg, 3) = '992'
		);
    COMMIT TRANSACTION;
END;
GO
