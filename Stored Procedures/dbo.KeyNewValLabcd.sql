SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[KeyNewValLabcd]
	@ColumnName 	Nvarchar(255),
	@RetValue 		Nvarchar(500) output,
	@RetWhere		Nvarchar(200) output,
	@NewValue 		Nvarchar(20) = '',
	@OldValue 		Nvarchar(20) = '',
	@Level			smallInt,
	@LC1Start    	smallint,
	@LC1Length   	smallint,
	@LC2Start    	smallint,
	@LC2Length   	smallint,
	@LC3Start    	smallint,
	@LC3Length   	smallint,
	@LC4Start    	smallint,
	@LC4Length   	smallint,
	@LC5Start    	smallint,
	@LC5Length   	smallint,
	@LClevels	 	smallint,
   @Delimiter     Nvarchar(1) = ''
as
declare @modColName Nvarchar(200)
declare @minusVal integer
declare @i integer
declare @sqlStr Nvarchar(1000)
declare @strStart Nvarchar(2),
		  @strLength Nvarchar(1)
begin
	set @i = 1
set @sqlstr = ''
set @Delimiter = isNull(@Delimiter,'')

while (@i <= @LCLevels)
begin
	select @strStart = convert(Nvarchar,case @i when 1 then @LC1Start
						 when 2 then @LC2Start
						 when 3 then @LC3Start
						 when 4 then @LC4Start
						 when 5 then @LC5Start end),
			 @strLength = convert(Nvarchar,case @i when 1 then @LC1Length
						 when 2 then @LC2Length
						 when 3 then @LC3Length
						 when 4 then @LC4Length
						 when 5 then @LC5Length end)

	if (@i > 1 and @i <= @LCLevels)
		set @sqlStr = @sqlStr + ' + ''' + @Delimiter + ''''
	if (@i > 1)
			set @sqlstr = @sqlstr + ' + '
	if (@i = @level)
		set @sqlStr = @sqlStr + '''' + @NewValue + ''''
	else 
		set @sqlStr = @sqlStr + 'substring(' + @columnName + ',' + @strStart + ',' + @strLength +')'


	set @i = @i + 1

end

	select @strStart = convert(Nvarchar,case @level when 1 then @LC1Start
						 when 2 then @LC2Start
						 when 3 then @LC3Start
						 when 4 then @LC4Start
						 when 5 then @LC5Start end),
			 @strLength = convert(Nvarchar,case @level when 1 then @LC1Length
						 when 2 then @LC2Length
						 when 3 then @LC3Length
						 when 4 then @LC4Length
						 when 5 then @LC5Length end)

	set @RetWhere = ' substring(' + @columnName + ',' + @strStart + ',' + @strLength +') = N''' +  @OldValue + ''''
	set @RetValue = @SqlStr
end
GO
