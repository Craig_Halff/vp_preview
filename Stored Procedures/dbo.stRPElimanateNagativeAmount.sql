SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPElimanateNagativeAmount]
AS

BEGIN -- Procedure stRPElimanateNagativeAmount

---- Define the variable talbes that holds the plan with negative amount on Expense/Consultant/Unit
  DECLARE @tabPNExpenseNegative TABLE (
        PlanID /*N*/varchar(32) COLLATE database_default
    UNIQUE(PlanID)
  )

  DECLARE @tabRPExpenseNegative TABLE (
        PlanID /*N*/varchar(32) COLLATE database_default
    UNIQUE(PlanID)
  )

  DECLARE @tabPNConsultantNegative TABLE (
        PlanID /*N*/varchar(32) COLLATE database_default
    UNIQUE(PlanID)
  )

  DECLARE @tabRPConsultantNegative TABLE (
        PlanID /*N*/varchar(32) COLLATE database_default
    UNIQUE(PlanID)
  )

  DECLARE @tabRPUnitNegative TABLE (
        PlanID /*N*/varchar(32) COLLATE database_default
    UNIQUE(PlanID)
  )

---- Store all Plan with negative Expense/Consultant/Unit TPD

	INSERT @tabPNExpenseNegative(PlanID)
		  SELECT DISTINCT PlanID FROM PNPlannedExpenses WHERE (PeriodCost < 0 OR PeriodBill < 0)

	INSERT @tabRPExpenseNegative(PlanID)
		  SELECT DISTINCT PlanID FROM RPPlannedExpenses WHERE (PeriodCost < 0 OR PeriodBill < 0)

	INSERT @tabPNConsultantNegative(PlanID)
		  SELECT DISTINCT PlanID FROM PNPlannedConsultant WHERE (PeriodCost < 0 OR PeriodBill < 0)

	INSERT @tabRPConsultantNegative(PlanID)
		  SELECT DISTINCT PlanID FROM RPPlannedConsultant WHERE (PeriodCost < 0 OR PeriodBill < 0)

	INSERT @tabRPUnitNegative(PlanID)
		  SELECT DISTINCT PlanID FROM RPPlannedUnit WHERE (PeriodCost < 0 OR PeriodBill < 0)

---- Remove the negative TPD records in iAccess Plan that have negative amount 

      DELETE FROM PNPlannedLabor WHERE (PeriodHrs < 0 or PeriodBill < 0 OR PeriodCost < 0) 
	  DELETE FROM RPPlannedLabor WHERE (PeriodHrs < 0 or PeriodBill < 0 OR PeriodCost < 0) 

	  DELETE FROM PNPlannedExpenses WHERE (PeriodCost < 0 OR PeriodBill < 0)
	  DELETE FROM RPPlannedExpenses WHERE (PeriodCost < 0 OR PeriodBill < 0)

	  DELETE FROM PNPlannedConsultant WHERE (PeriodCost < 0 OR PeriodBill < 0)
	  DELETE FROM RPPlannedConsultant WHERE (PeriodCost < 0 OR PeriodBill < 0)

	  DELETE FROM RPPlannedUnit WHERE (PeriodCost < 0 OR PeriodBill < 0 OR PeriodQty < 0)

	  DELETE FROM PNBaselineLabor WHERE (PeriodHrs < 0 or PeriodBill < 0 OR PeriodCost < 0) 
	  DELETE FROM RPBaselineLabor WHERE (PeriodHrs < 0 or PeriodBill < 0 OR PeriodCost < 0) 

	  DELETE FROM PNBaselineExpenses WHERE (PeriodCost < 0 OR PeriodBill < 0)
	  DELETE FROM RPBaselineExpenses WHERE (PeriodCost < 0 OR PeriodBill < 0)

	  DELETE FROM PNBaselineConsultant WHERE (PeriodCost < 0 OR PeriodBill < 0)
	  DELETE FROM RPBaselineConsultant WHERE (PeriodCost < 0 OR PeriodBill < 0)

	---- Set negative  TPD records that have negative revenue, 
	---- which may caused by having entered negative compensation amounts on the Analysis tabs, 
	---- in this case the planned laobr is positvie, otherwise it will be removed in the previous SQLs

      UPDATE RPPlannedLabor SET PeriodRev = 0 WHERE (PeriodRev < 0)  
      UPDATE RPPlannedExpenses SET PeriodRev = 0 WHERE (PeriodRev < 0)  
      UPDATE RPPlannedConsultant SET PeriodRev = 0 WHERE (PeriodRev < 0)  
      UPDATE RPPlannedUnit SET PeriodRev = 0 WHERE (PeriodRev < 0)  

      UPDATE RPBaselineLabor SET PeriodRev = 0 WHERE (PeriodRev < 0)  
      UPDATE RPBaselineExpenses SET PeriodRev = 0 WHERE (PeriodRev < 0)  
      UPDATE RPBaselineConsultant SET PeriodRev = 0 WHERE (PeriodRev < 0)  
      UPDATE RPBaselineUnit SET PeriodRev = 0 WHERE (PeriodRev < 0)  


/******************************************************************************/
   ---- Recalculate Expense and Consultnat cost/billing amount since we may remove some zero amount
	UPDATE PNExpense
	  SET PlannedExpCost =  ISNULL((SELECT SUM(PeriodCost) 
		 FROM PNPlannedExpenses P  WHERE P.PlanID = PNExpense.PlanID 
		 AND P.ExpenseID = PNExpense.ExpenseID AND P.ExpenseID IS NOT NULL),0),
	  PlannedExpBill = ISNULL((SELECT SUM(PeriodBill) 
		 FROM PNPlannedExpenses P  WHERE P.PlanID = PNExpense.PlanID 
		 AND P.ExpenseID = PNExpense.ExpenseID AND P.ExpenseID IS NOT NULL),0)
	  WHERE PNExpense.PlanID IN (SELECT PlanID FROM @tabPNExpenseNegative)
	  
	UPDATE RPExpense 
	  SET PlannedExpCost = ISNULL((SELECT  SUM(PeriodCost) 
		 FROM RPPlannedExpenses P  WHERE P.PlanID = RPExpense.PlanID 
		 AND P.ExpenseID = RPExpense.ExpenseID AND P.ExpenseID IS NOT NULL),0),
	  PlannedExpBill = ISNULL((SELECT SUM(PeriodBill) 
		 FROM RPPlannedExpenses P  WHERE P.PlanID = RPExpense.PlanID 
		 AND P.ExpenseID = RPExpense.ExpenseID AND P.ExpenseID IS NOT NULL),0)
	WHERE RPExpense.PlanID IN (SELECT PlanID FROM @tabRPExpenseNegative)

	-- Consultant
	UPDATE PNConsultant 
	  SET PlannedConCost = ISNULL((SELECT  SUM(PeriodCost) 
		 FROM PNPlannedConsultant P  WHERE P.PlanID = PNConsultant.PlanID 
		 AND P.ConsultantID = PNConsultant.ConsultantID AND P.ConsultantID IS NOT NULL),0),
	  PlannedConBill = ISNULL((SELECT SUM(PeriodBill) 
		 FROM PNPlannedConsultant P  WHERE P.PlanID = PNConsultant.PlanID 
		 AND P.ConsultantID = PNConsultant.ConsultantID AND P.ConsultantID IS NOT NULL),0)	 
    WHERE PNConsultant.PlanID IN (SELECT PlanID FROM @tabPNConsultantNegative)

	UPDATE RPConsultant 
	  SET PlannedConCost = ISNULL((SELECT  SUM(PeriodCost) 
		 FROM RPPlannedConsultant P  WHERE P.PlanID = RPConsultant.PlanID 
		 AND P.ConsultantID = RPConsultant.ConsultantID AND P.ConsultantID IS NOT NULL),0),
	  PlannedConBill = ISNULL((SELECT SUM(PeriodBill) 
		 FROM RPPlannedConsultant P  WHERE P.PlanID = RPConsultant.PlanID 
		 AND P.ConsultantID = RPConsultant.ConsultantID AND P.ConsultantID IS NOT NULL),0)	 
	WHERE RPConsultant.PlanID IN (SELECT PlanID FROM @tabRPConsultantNegative) 


	UPDATE RPUnit
	  SET PlannedUntCost = ISNULL((SELECT  SUM(PeriodCost) 
		 FROM RPPlannedUnit P  WHERE P.PlanID = RPUnit.PlanID 
		 AND P.UnitID = RPUnit.UnitID AND P.UnitID IS NOT NULL),0),
	  PlannedUntBill = ISNULL((SELECT SUM(PeriodBill) 
		 FROM RPPlannedUnit P  WHERE P.PlanID = RPUnit.PlanID 
		 AND P.UnitID = RPUnit.UnitID AND P.UnitID IS NOT NULL),0)	 
    WHERE RPUnit.PlanID IN (SELECT PlanID FROM @tabRPUnitNegative) 

/******************************************************************************/
   -- Remove the records that have negative revenue fees.

      DELETE FROM RPCompensationFee 
	  WHERE (PeriodDirLabCost < 0 OR PeriodDirExpCost < 0 OR PeriodDirLabBill < 0 OR PeriodDirExpBill < 0 OR PeriodCost < 0 OR PeriodBill < 0 ) 

	  DELETE FROM RPCompensationFee 
	  WHERE (PeriodDirLabCost < 0 OR PeriodDirExpCost < 0 OR PeriodDirLabBill < 0 OR PeriodDirExpBill < 0 OR PeriodCost < 0 OR PeriodBill < 0 ) 
      
	  DELETE FROM RPConsultantFee WHERE (PeriodCost < 0 OR PeriodBill < 0 ) 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

END -- stRPElimanateNagativeAmount
GO
