SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Storage_Save] ( @fieldsSQL nvarchar(max), @updateValuesSql nvarchar(max), @insertValuesSql nvarchar(max), @deleteSeqs nvarchar(max))
             AS EXEC spCCG_EI_Config_Storage_Save @fieldsSQL,@updateValuesSql,@insertValuesSql,@deleteSeqs
GO
