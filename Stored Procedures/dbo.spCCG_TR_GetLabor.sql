SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_TR_GetLabor](@WBS1 varchar(32), @WBS2 varchar(7), @WBS3 varchar(7), @ThruDate varchar(32))
AS BEGIN

	set nocount on
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	declare @NonBillLaborCode varchar(32), @MultiCompanyEnabled varchar(32), @PrOrg varchar(32)
	select @MultiCompanyEnabled=MulticompanyEnabled from CFGSystem
	if @MultiCompanyEnabled='Y'
	begin
		select @PrOrg=Org from PR where WBS1=@WBS1 and WBS2=@WBS2 and WBS3=@WBS3
		--print 'PrOrg = ' + IsNull(@PrOrg,'')
		select @NonBillLaborCode=NonBillLaborCode from CFGBillMain where Company=Left(@PrOrg,Len(Company))
	end else
		select @NonBillLaborCode=NonBillLaborCode from CFGBillMain
	--print @NonBillLaborCode

	select tr.Seq, d.Org, PR1.ProjectCurrencyCode as PCC, PR1.BillingCurrencyCode as BCC,
		IsNull(tr.NewBillStatus,d.BillStatus) as BillStatus, IsNull(tr.NewBillStatus,d.BillStatus) as BillStatusDB, d.BillStatus as BillStatusOrig, 
		
		IsNull(tr.ModComment,Case When a.BillStatus='H' and d.BillStatus='B' Then null Else ISNULL(case when tr.OriginalPkey is null then a.ModificationComment else null end,'') end) as ModComment, 
		Case When @NonBillLaborCode is not null and d.LaborCode LIKE @NonBillLaborCode Then 'False' Else 'True' End as Billable, 
		IsNull(tr.NewWBS1,d.WBS1) as WBS1, IsNull(tr.NewWBS1,d.WBS1) as WBS1DB, d.WBS1 as WBS1Orig, IsNull(PR1tr.Name,PR1.Name) as WBS1Name, PR1.Name as WBS1NameOrig,
		IsNull(tr.NewWBS2,d.WBS2) as WBS2, IsNull(tr.NewWBS2,d.WBS2) as WBS2DB, d.WBS2 as WBS2Orig, IsNull(PR1tr.Name,PR2.Name) as WBS2Name, PR2.Name as WBS2NameOrig,
		IsNull(tr.NewWBS3,d.WBS3) as WBS3, IsNull(tr.NewWBS3,d.WBS3) as WBS3DB, d.WBS3 as WBS3Orig, IsNull(PR1tr.Name,PR3.Name) as WBS3Name, PR3.Name as WBS3NameOrig,
		
		IsNull(tr.NewTransDate,d.TransDate) as TransDate, IsNull(tr.NewTransDate,d.TransDate) as TransDateDB, d.TransDate as TransDateOrig,
		d.Employee, EM.LastName + ', ' + IsNull(EM.FirstName,'') as EmployeeName,
		d.LaborCode, IsNull(lc1.LC1Label,'') + Case When lc2.LC2Label is null Then '' Else ' / ' + lc2.LC2Label End + Case When lc3.LC3Label is null Then '' Else ' / ' + lc3.LC3Label End as LaborCodeName,
		d.Category, BTLaborCats.Description as CategoryName, 
		IsNull(tr.NewRegHrs,d.RegHrs) + IsNull(tr.NewOvtHrs,d.OvtHrs) + IsNull(tr.NewSpecialOvtHrs,d.SpecialOvtHrs) AS Hours,
		0.0 as HoursDB, d.RegHrs + d.OvtHrs + d.SpecialOvtHrs AS HoursOrig,
		d.RegAmtBillingCurrency + d.OvtAmtBillingCurrency + d.SpecialOvtAmtBillingCurrency as Cost,
		Case When d.RegHrs+d.OvtHrs+d.SpecialOvtHrs=0 Then 0.0 Else BillExt / (d.RegHrs+d.OvtHrs+d.SpecialOvtHrs) End as BillRate,
		d.BillExt, 
		LTrim(IsNull(tr.NewComment,dbo.fnCCG_ConvertHtml(d.Comment))) as Comment, LTrim(IsNull(tr.NewComment,dbo.fnCCG_ConvertHtml(d.Comment))) as CommentDB, LTrim(dbo.fnCCG_ConvertHtml(d.Comment)) as CommentOrig,
		d.TableName as OriginalTable, d.Period as OriginalPeriod, d.PostSeq as OriginalPostSeq, d.PKey as OriginalPKey, 
		IsNull(tr.NewRegHrs,d.RegHrs) as RegHrs,						IsNull(tr.NewRegHrs,d.RegHrs) as RegHrsDB,						d.RegHrs as RegHrsOrig,
		IsNull(tr.NewOvtHrs,d.OvtHrs) as OvtHrs,						IsNull(tr.NewOvtHrs,d.OvtHrs) as OvtHrsDB,						d.OvtHrs as OvtHrsOrig,
		IsNull(tr.NewSpecialOvtHrs,d.SpecialOvtHrs) as SpecialOvtHrs,	IsNull(tr.NewSpecialOvtHrs,d.SpecialOvtHrs) as SpecialOvtHrsDB,	d.SpecialOvtHrs as SpecialOvtHrsOrig,
		d.RateBillingCurrency,							d.RegAmtBillingCurrency, 
		d.OvtRateBillingCurrency,		d.OvtPct,		d.OvtAmtBillingCurrency, 
		d.SpecialOvtRateBillingCurrency,d.SpecialOvtPct,d.SpecialOvtAmtBillingCurrency, 
		Case When tr.NewWBS1 is null and tr.NewWBS2 is null and tr.NewWBS3 is null Then d.XferWBS1 Else d.WBS1 End as XferWBS1, 
		Case When tr.NewWBS1 is null and tr.NewWBS2 is null and tr.NewWBS3 is null Then d.XferWBS2 Else d.WBS2 End as XferWBS2, 
		Case When tr.NewWBS1 is null and tr.NewWBS2 is null and tr.NewWBS3 is null Then d.XferWBS3 Else d.WBS3 End as XferWBS3, 
		Case When tr.NewWBS1 is null and tr.NewWBS2 is null and tr.NewWBS3 is null Then d.XferLaborCode Else null End as XferLaborCode, 
		Case When tr.NewWBS1 is null and tr.NewWBS2 is null and tr.NewWBS3 is null Then d.XferCategory Else null End as XferCategory,
		tr.[Status], PR1.ChargeType, a.IncludeFuture, a.BillNonBillable, a.SubmittedBy, a.SubmittedDate, a.ApprovedBy, a.ApprovedDate, 
		Case When IsNull(a.SubmittedBy,'')='' Then 'False' Else 'True' End as Submitted,
		Case When IsNull(a.ApprovedBy, '')='' Then 'False' Else 'True' End as Approved, convert(bit,0) as Approve,
		IsNull(tr.ApprComment,ISNULL(case when tr.OriginalPkey is null then a.ApprovalComment else null end,'')) as ApprovalComment, a.BillStatus as ApprovedBillStatus,
		'' as HistoryButton, Case When EverModified.Period is null Then 'N' Else 'Y' End as HasBeenModified, 
		Case When @NonBillLaborCode is not null and d.LaborCode LIKE @NonBillLaborCode Then 'N' Else 'Y' End as BillExtend
	from (
		select 'LD' as TableName, Period, PostSeq, PKey, PrOrg as Org,
			WBS1, WBS2, WBS3, TransDate, BillStatus, LaborCode, Category, Employee, BillExt, Comment,
			RegHrs,		  RateBillingCurrency,		    RegAmtBillingCurrency, 
			OvtHrs,		  OvtRateBillingCurrency,		OvtPct,			OvtAmtBillingCurrency, 
			SpecialOvtHrs, SpecialOvtRateBillingCurrency, SpecialOvtPct,	SpecialOvtAmtBillingCurrency, 
			XferWBS1, XferWBS2, XferWBS3, XferLaborCode, XferCategory
		from LD
		where LD.WBS1 = @WBS1 and LD.BillStatus in ('B','H','W') and LD.SuppressBill <> 'Y' 
		  
		  and (@WBS2=' ' or @WBS2=LD.WBS2) and (@WBS3=' ' or @WBS3=LD.WBS3)
		UNION ALL
		select 'BILD' as TableName, Period, PostSeq, PKey, PrOrg as Org,
			WBS1, WBS2, WBS3, TransDate, BillStatus, LaborCode, Category, Employee, BillExt, Comment,
			RegHrs,		  RateBillingCurrency,		    RegAmtBillingCurrency, 
			OvtHrs,		  OvtRateBillingCurrency,		OvtPct,			OvtAmtBillingCurrency, 
			SpecialOvtHrs, SpecialOvtRateBillingCurrency, SpecialOvtPct,	SpecialOvtAmtBillingCurrency, 
			XferWBS1, XferWBS2, XferWBS3, XferLaborCode, XferCategory
		from BILD LD
		where LD.WBS1 = @WBS1 and LD.BillStatus in ('B','H','W') and LD.SuppressBill <> 'Y' 
		  
		  and (@WBS2=' ' or @WBS2=LD.WBS2) and (@WBS3=' ' or @WBS3=LD.WBS3)
	) as d
	left join CCG_TR_Approvals a on a.TableName=d.TableName and a.Period=d.Period and a.PostSeq=d.PostSeq and a.PKey=d.PKey
	left join (
		select distinct TableName, Period, PostSeq, PKey from (
			select OriginalTable as TableName, OriginalPeriod as Period, OriginalPostSeq as PostSeq, OriginalPKey as PKey from CCG_TR_History
			union
			select NewTable, NewPeriod, NewPostSeq, NewPKey from CCG_TR_History
		) as yaya
	) as EverModified on EverModified.TableName=d.TableName and EverModified.Period=d.Period and EverModified.PostSeq=d.PostSeq and EverModified.PKey=d.PKey
	left join CCG_TR tr on tr.OriginalTable=d.TableName and tr.OriginalPeriod=d.Period and tr.OriginalPostSeq=d.PostSeq and tr.OriginalPKey=d.PKey
	left join PR PR1 on PR1.WBS1=d.WBS1 and PR1.WBS2=' '
	left join PR PR2 on PR2.WBS1=d.WBS1 and PR2.WBS2=d.WBS2 and PR2.WBS2<>' ' and PR2.WBS3=' '
	left join PR PR3 on PR3.WBS1=d.WBS1 and PR3.WBS2=d.WBS2 and PR3.WBS3=d.WBS3 and PR3.WBS3<>' '
	left join PR PR1tr on PR1tr.WBS1=tr.NewWBS1 and PR1tr.WBS2=' '
	left join PR PR2tr on PR2tr.WBS1=tr.NewWBS1 and PR2tr.WBS2=tr.NewWBS2 and PR2tr.WBS2<>' ' and PR2tr.WBS3=' '
	left join PR PR3tr on PR3tr.WBS1=tr.NewWBS1 and PR3tr.WBS2=tr.NewWBS2 and PR3tr.WBS3=tr.NewWBS3 and PR3tr.WBS3<>' '
	left join BTLaborCats on BTLaborCats.Category=d.Category
	left join CFGPostControl PC on d.Period=PC.Period and d.PostSeq=PC.PostSeq 
	left join EM on d.Employee=EM.Employee 
	left join (
	  select Code as LC1Code, Label as LC1Label, LC1Start, LC1Length
	  from CFGLCCodes, CFGFormat
	  where LCLevel=1 
	) as lc1 on lc1.LC1Code = Substring(d.LaborCode, LC1Start, LC1Length)
	left join (
	  select Code as LC2Code, Label as LC2Label, LC2Start, LC2Length
	  from CFGLCCodes, CFGFormat
	  where LCLevel=2
	) as lc2 on lc2.LC2Code = Substring(d.LaborCode, LC2Start, LC2Length)
	left join (
	  select Code as LC3Code, Label as LC3Label, LC3Start, LC3Length
	  from CFGLCCodes, CFGFormat
	  where LCLevel=3
	) as lc3 on lc3.LC3Code = Substring(d.LaborCode, LC3Start, LC3Length)
	where (IsNull(tr.NewTransDate,'12/31/2030') <= @ThruDate or d.TransDate <= @ThruDate)
	order by IsNull(tr.NewWBS1,d.WBS1),IsNull(tr.NewWBS2,d.WBS2),IsNull(tr.NewWBS3,d.WBS3),
		IsNull(tr.NewTransDate,d.TransDate),EM.LastName + ', ' + IsNull(EM.FirstName,'') --7,12,17,22,26
END
GO
