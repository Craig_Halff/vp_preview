SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[EmployeeInTables] @Employee Nvarchar(20),@TableName Nvarchar(50) output
as
declare
  @BaseStmt		Nvarchar(1000),
  @SqlStmt		Nvarchar(max)

begin 
  set @TableName = ''

  set @BaseStmt = 
	'Select ''?Table'' as TableName from ?Table where ?Employee = ''' + @Employee + ''''  

  set @SqlStmt = 'declare ChkCursor cursor for Select top 1 TableName from  ('

  if rtrim(@Employee) <> '' 
  begin
    set @SqlStmt = @SqlStmt + replace(replace(@BaseStmt,'?Table','RPAssignment'),'?Employee','ResourceID')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','PNAssignment'),'?Employee','ResourceID')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','Activity'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','BTRCTEmpls'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','BTROTEmpls'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','BTRRTEmpls'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','ekDetail'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','ekMaster'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','EMPayrollDetail'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','erDetail'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','EX'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','EXAdvance'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','exChecks'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','exMaster'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','laDetail'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','laMaster'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerAP'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerAR'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerEX'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerMisc'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','MktCampaign'),'?Employee','CampaignMgr')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','MktCampaign'),'?Employee','MktgMgr')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','MktCampaign'),'?Employee','Manager3')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','PYChecks'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','tkDetail'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','tkMaster'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','tsDetail'),'?Employee','Employee')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','tsMaster'),'?Employee','Employee')

    set @SqlStmt = @SqlStmt + ') as TableCheck'

    execute (@SqlStmt)
    open ChkCursor
    fetch ChkCursor into @TableName
    close ChkCursor
    deallocate ChkCursor
  end
end -- EmployeeInTables  
GO
