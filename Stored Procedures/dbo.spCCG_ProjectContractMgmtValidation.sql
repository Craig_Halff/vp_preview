SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[spCCG_ProjectContractMgmtValidation] @WBS1 varchar (32), @ContractNumber varchar (255)
AS
/*
Copyright 2019 (c) Central Consulting Group, Inc.  All rights reserved.
09/19/2019	David Springer
			Validate Period = today's Period for any changes.
			Call this on a Contract Details CHANGE workflow
*/
DECLARE @TodaysPeriod	varchar (6),
		@ContractPeriod	varchar (6)
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

	Select @TodaysPeriod = Period From CFGDates Where convert (date, getDate()) between AccountPdStart and AccountPdEnd
	Select @ContractPeriod = Period From Contracts Where WBS1 = @WBS1 and ContractNumber = @ContractNumber

	If @TodaysPeriod <> @ContractPeriod
		RAISERROR ('Contract values cannot be changed outside the original contract period.                                                             ', 16, 1)
END
GO
