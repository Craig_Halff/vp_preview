SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_KeyConvert_GetSessions]
AS BEGIN
	select top 100 NULL as SessionSeq, sum(case when w.Entity = 'Account' then 1 else 0 end) as CountAccounts,
			sum(case when w.Entity = 'Employee' then 1 else 0 end) as CountEmployees,
			sum(case when w.Entity = 'Vendor' then 1 else 0 end) as CountVendors,
			sum(case when w.Entity = 'WBS1' then 1 else 0 end) as CountWBS1s,
			sum(case when w.Entity = 'WBS2' then 1 else 0 end) as CountWBS2s,
			sum(case when w.Entity = 'WBS3' then 1 else 0 end) as CountWBS3s,
			NULL as CreateDate, 0 as NullOrder, 0 as ErrorsVendors
		from KeyConvertWork w
		where w.PKey not in (select l.PKey from CCG_KeyConvertLog l where l.Entity = w.Entity and isnull(l.Notes,'') = '')
	UNION
	select s.Seq as SessionSeq, s.CountAccounts, s.CountEmployees, s.CountVendors, s.CountWBS1s, s.CountWBS2s, s.CountWBS3s, 
			s.CreateDate, 1 as NullOrder, s.ErrorsVendors			
		from CCG_KeyConvertSessions s
	order by NullOrder asc, CreateDate desc
END
GO
