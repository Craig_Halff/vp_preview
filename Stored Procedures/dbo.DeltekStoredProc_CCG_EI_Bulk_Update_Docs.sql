SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Bulk_Update_Docs] ( @deleteFileIds varchar(max), @insertValuesSql nvarchar(max)= '', @insertPkgValuesSql nvarchar(max)= '', @updateValuesSql nvarchar(max)= '')
             AS EXEC spCCG_EI_Bulk_Update_Docs @deleteFileIds,@insertValuesSql,@insertPkgValuesSql,@updateValuesSql
GO
