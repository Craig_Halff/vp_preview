SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_CCGGrid_Instances]
AS
BEGIN
	SET NOCOUNT ON;

    SELECT count(*) as NumInstances
		FROM CCG_EI_ConfigGrid cg
			INNER JOIN CCG_GRID_ConfigInstance ci ON ci.GridInstanceName = cg.GridInstanceName
		WHERE GridVisibleInEI = 'Y';
END;
GO
