SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_GetTransDocsFilelistOverride](@Username Nvarchar(32), @CorE char(1), @Invoice Nvarchar(12), @WBS1 Nvarchar(30))
AS BEGIN
	declare @sql Nvarchar(max) = N''
	if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[spCCG_EI_GetTransDocsFilelistOverride]') and type in (N'P', N'PC'))
	begin
		set @sql = 'exec spCCG_EI_GetTransDocsFilelistOverride '''+replace(@Username,'''','''''')+''', '''+@CorE+''',
			'''+replace(@Invoice,'''','''''')+''', '''+replace(@WBS1,'''','''''')+''''
		exec (@sql)
	end
END
GO
