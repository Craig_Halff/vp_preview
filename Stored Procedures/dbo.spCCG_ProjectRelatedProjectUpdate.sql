SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectRelatedProjectUpdate] @WBS1 varchar(32), @ModUser varchar (255)
AS
/*
Copyright (c) 2019 Central Consulting Group. All rights reserved.
08/15/2019	David Springer
			Update suffix Project in parent Project Related Projects grid.
			Call from Project CHANGE workflow when Fee has changed.
09/23/2019	David Springer
			Changed from Fee Fields to Contract Details at lowest level
*/ 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
DECLARE @ParentWBS1 varchar (32)
BEGIN

	If not exists (Select 'x' From Projects_RelatedProjects Where CustRelatedProject = @WBS1)
		exec spCCG_ProjectRelatedProjectInsert @WBS1, @ModUser

	Update pro
	Set pro.ModUser = @ModUser, 
		pro.ModDate = convert (date, getDate()), -- date field does not allow for time
		pro.CustRelatedProjectFeeDirLab = p2.Labor,
		pro.CustRelatedProjectFeeDirExp = p2.DE,
		pro.CustRelatedProjectConsultFee = p2.DC, 
		pro.CustRelatedProjectReimbAllowExp = p2.RE, 
		pro.CustRelatedProjectReimbAllowCons = p2.RC,
		pro.CustRelatedProjectFeeTotal = p2.Labor + p2.DE + p2.DC + p2.RE + p2.RC,
		pro.CustRelatedProjectStatus = p1.Status,
		@ParentWBS1 = pro.WBS1
	From PR p1, Projects_RelatedProjects pro,
		(Select d.WBS1, Sum (d.FeeDirLab) Labor, Sum (d.FeeDirExp) DE, Sum (d.ConsultFee) DC, Sum (d.ReimbAllowExp) RE, Sum (d.ReimbAllowCons) RC
		From PR p, Contracts c, ContractDetails d
		Where p.Sublevel = 'N'
		  and p.WBS1 = c.WBS1
		  and c.FeeIncludeInd = 'Y'
		  and p.WBS1 = d.WBS1
		  and p.WBS2 = d.WBS2
		  and p.WBS3 = d.WBS3
		  and c.ContractNumber = d.ContractNumber
		Group by d.WBS1) p2
	Where p1.WBS1 = @WBS1
	  and p1.WBS2 = ' ' -- project level only
	  and p1.WBS1 not like '%.000'
	  and p1.WBS1 = p2.WBS1
	  and p1.WBS1 = pro.CustRelatedProject
	  and pro.WBS2 = ' ' -- project level only

	exec spCCG_ProjectRelatedProjectFeeAdjustment @ParentWBS1

/* Use this if the grid and projects get out of sync
Update x
Set x.CustRelatedProjectFeeDirLab = p.FeeDirLab
From PR p, Projects_RelatedProjects x
Where p.WBS1 = x.CustRelatedProject
  and p.WBS2 = ' '
  and p.FeeDirLab <> x.CustRelatedProjectFeeDirLab

Update x
Set x.CustRelatedProjectFeeDirExp = p.FeeDirExp
From PR p, Projects_RelatedProjects x
Where p.WBS1 = x.CustRelatedProject
  and p.WBS2 = ' '
  and p.FeeDirExp <> x.CustRelatedProjectFeeDirExp

Update x
Set x.CustRelatedProjectConsultFee = p.ConsultFee
From PR p, Projects_RelatedProjects x
Where p.WBS1 = x.CustRelatedProject
  and p.WBS2 = ' '
  and p.ConsultFee <> x.CustRelatedProjectConsultFee

Update x
Set x.CustRelatedProjectReimbAllowCons = p.ReimbAllowCons
From PR p, Projects_RelatedProjects x
Where p.WBS1 = x.CustRelatedProject
  and p.WBS2 = ' '
  and p.ReimbAllowCons <> x.CustRelatedProjectReimbAllowCons

Update x
Set x.CustRelatedProjectReimbAllowExp = p.ReimbAllowExp
From PR p, Projects_RelatedProjects x
Where p.WBS1 = x.CustRelatedProject
  and p.WBS2 = ' '
  and p.ReimbAllowExp <> x.CustRelatedProjectReimbAllowExp

Update Projects_RelatedProjects
Set CustRelatedProjectFeeTotal = CustRelatedProjectConsultFee + CustRelatedProjectFeeDirExp + CustRelatedProjectFeeDirLab + CustRelatedProjectReimbAllowCons + CustRelatedProjectReimbAllowExp
Where CustRelatedProjectFeeTotal <> CustRelatedProjectConsultFee + CustRelatedProjectFeeDirExp + CustRelatedProjectFeeDirLab + CustRelatedProjectReimbAllowCons + CustRelatedProjectReimbAllowExp

Update p
Set p.FeeDirLab = px.CustOriginalFeeDirLab - IsNull (ro.DirLab, 0),
	p.FeeDirExp = px.CustOriginalFeeDirExp - IsNull (ro.DirExp, 0),
	p.Fee = px.CustOriginalFeeDirLab - IsNull (ro.DirLab, 0) + px.CustOriginalFeeDirExp - IsNull (ro.DirExp, 0),
	p.ConsultFee = px.CustOriginalConsultFee - IsNull (ro.DirCons, 0),
	p.ReimbAllowExp = px.CustOriginalReimbAllowExp - IsNull (ro.ReimbExp, 0),
	p.ReimbAllowCons = px.CustOriginalReimbAllowCons - IsNull (ro.ReimbCons, 0),
	p.ReimbAllow = px.CustOriginalReimbAllowExp - IsNull (ro.ReimbExp, 0) + px.CustOriginalReimbAllowCons - IsNull (ro.ReimbCons, 0)
From PR p, ProjectCustomTabFields px
	Join
	(Select WBS1, 
		Sum (CustRelatedProjectFeeDirLab) DirLab, 
		Sum (CustRelatedProjectFeeDirExp) DirExp, 
		Sum (CustRelatedProjectConsultFee) DirCons, 
		Sum (CustRelatedProjectReimbAllowExp) ReimbExp, 
		Sum (CustRelatedProjectReimbAllowCons) ReimbCons
	From Projects_RelatedProjects
	Where WBS2 = ' '  -- project level
	Group by WBS1) ro on px.WBS1 = ro.WBS1
Where p.WBS1 like '%000'
	and p.WBS2 = ' '  -- project level
	and p.WBS1 = px.WBS1
	and p.WBS2 = px.WBS2

--  Update Original Revenue Total Field
Update ProjectCustomTabFields
Set CustOriginalFeeTotal = CustOriginalFeeDirLab + CustOriginalFeeDirExp + CustOriginalConsultFee + CustOriginalReimbAllowExp + CustOriginalReimbAllowCons
Where WBS1 like '%000'
	and WBS2 = ' '
	and CustOriginalFeeTotal <> CustOriginalFeeDirLab + CustOriginalFeeDirExp + CustOriginalConsultFee + CustOriginalReimbAllowExp + CustOriginalReimbAllowCons
*/
END
GO
