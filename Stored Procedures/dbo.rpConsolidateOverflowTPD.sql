SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpConsolidateOverflowTPD]
  @strPlanID VARCHAR(32)
AS

BEGIN -- Procedure rpConsolidateOverflowTPD

  SET NOCOUNT ON
  SET ANSI_WARNINGS OFF
    
  DECLARE @strCalcExpBill char
  DECLARE @strCalcConBill char
  
  DECLARE @intHrDecimals int
  DECLARE @intQtyDecimals int
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  DECLARE @intRtCostDecimals int
  DECLARE @intRtBillDecimals int
  DECLARE @intLabRevDecimals int
  DECLARE @intECURevDecimals int
  
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strMatchWBS1Wildcard varchar(1)
  DECLARE @strUnpostedFlg varchar(1)
  DECLARE @sintGRMethod smallint
  DECLARE @sintCostRtMethod smallint
  DECLARE @sintBillingRtMethod smallint
  
  DECLARE @intCostRtTableNo int
  DECLARE @intBillingRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int

  DECLARE @dtMaxEndDate datetime
  
  -- Get Parameters from RPPlan
  
  SELECT
    @strCompany = Company,
    @strUnpostedFlg = UnpostedFlg,
    @sintGRMethod = GRMethod,
    @intGenResTableNo = GenResTableNo,
    @intGRBillTableNo = GRBillTableNo,
    @sintCostRtMethod = CostRtMethod,
    @sintBillingRtMethod = BillingRtMethod,
    @intCostRtTableNo = CostRtTableNo,
    @intBillingRtTableNo = BillingRtTableNo,
    @strCalcExpBill = CalcExpBillAmtFlg, 
    @strCalcConBill = CalcConBillAmtFlg 
    FROM RPPlan WHERE PlanID = @strPlanID
  
  -- Get decimal settings.
  
  SELECT @intHrDecimals = HrDecimals,
         @intQtyDecimals = QtyDecimals,
         @intAmtCostDecimals = AmtCostDecimals,
         @intAmtBillDecimals = AmtBillDecimals,
         @intRtCostDecimals = RtCostDecimals,
         @intRtBillDecimals = RtBillDecimals,
         @intLabRevDecimals = LabRevDecimals, 
         @intECURevDecimals = ECURevDecimals
    FROM dbo.RP$tabDecimals(@strPlanID)
    
  -- Get Parameters from CFGResourcePlanning
  
  SELECT
    @strMatchWBS1Wildcard = MatchWBS1Wildcard
    FROM CFGResourcePlanning WHERE Company = @strCompany
    
  -- Calculate the last End Date of all calendar intervals.
  
  SELECT @dtMaxEndDate = MAX(EndDate)
    FROM RPCalendarInterval WHERE PlanID = @strPlanID
 
-- Labor >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
  -- Save overflow Labor Hrs.
  
  SELECT TimePhaseID AS TimePhaseID, PlanID AS PlanID, TaskID AS TaskID, AssignmentID AS AssignmentID,
    StartDate AS StartDate, EndDate AS EndDate, PeriodHrs AS PeriodHrs
    INTO #SaveLabTPD FROM
    (SELECT TPD.TimePhaseID AS TimePhaseID, TPD.PlanID AS PlanID, TPD.TaskID AS TaskID, TPD.AssignmentID AS AssignmentID,
       TPD.StartDate AS StartDate, TPD.EndDate AS EndDate, PeriodHrs AS PeriodHrs
       FROM RPPlannedLabor AS TPD INNER JOIN RPAssignment AS A
         ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID AND TPD.AssignmentID = A.AssignmentID
       WHERE TPD.PlanID = @strPlanID AND TPD.StartDate > @dtMaxEndDate
     UNION ALL
     SELECT TPD.TimePhaseID AS TimePhaseID, TPD.PlanID AS PlanID, TPD.TaskID AS TaskID, TPD.AssignmentID AS AssignmentID,
       TPD.StartDate AS StartDate, TPD.EndDate AS EndDate, PeriodHrs AS PeriodHrs
       FROM RPPlannedLabor AS TPD INNER JOIN
         (SELECT UT.TaskID, SUM(UT.ChildrenCount) AS ChildrenCount, 
                 UT.CostRate AS CostRate, UT.BillingRate AS BillingRate FROM
            (SELECT PT.TaskID AS TaskID, COUNT(CT.TaskID) AS ChildrenCount, 
                    PT.CostRate AS CostRate, PT.BillingRate AS BillingRate
               FROM RPTask AS PT LEFT JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
               WHERE PT.PlanID = @strPlanID GROUP BY PT.TaskID, PT.CostRate, PT.BillingRate
             UNION
             SELECT PT.TaskID AS TaskID, COUNT(A.AssignmentID) AS ChildrenCount, 
                    PT.CostRate AS CostRate, PT.BillingRate AS BillingRate 
               FROM RPTask AS PT LEFT JOIN RPAssignment AS A ON PT.PlanID = A.PlanID AND PT.TaskID = A.TaskID
               WHERE PT.PlanID = @strPlanID GROUP BY PT.TaskID, PT.CostRate, PT.BillingRate) AS UT
          GROUP BY UT.TaskID, UT.CostRate, UT.BillingRate) AS T
         ON TPD.TaskID = T.TaskID AND T.ChildrenCount = 0
       WHERE TPD.PlanID = @strPlanID AND TPD.AssignmentID IS NULL AND TPD.StartDate > @dtMaxEndDate) AS X
 
  -- Delete overflow records.
  
  DELETE TPD FROM RPPlannedLabor AS TPD INNER JOIN #SaveLabTPD AS X
    ON TPD.TimePhaseID = X.TimePhaseID AND TPD.PlanID = X.PlanID AND TPD.TaskID = X.TaskID
  
  -- Insert saved records into RPPlannedLabor.
  
  INSERT INTO RPPlannedLabor(TimePhaseID, PlanID, TaskID, AssignmentID,
    StartDate, EndDate, PeriodHrs, CostRate, BillingRate,
		PeriodCount, PeriodScale, CreateDate, ModDate)
    (SELECT 
       REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
       TPD.PlanID, 
       TPD.TaskID, 
       TPD.AssignmentID, 
       MIN(TPD.StartDate) AS StartDate, 
       MAX(TPD.EndDate) AS EndDate, 
       ROUND(ISNULL(SUM(PeriodHrs), 0), @intHrDecimals) AS PeriodHrs,
       dbo.RP$LabRate
         (A.ResourceID,
          ISNULL(A.Category, 0),
          A.GRLBCD,
          ISNULL(A.LaborCode, T.LaborCode),
          @sintCostRtMethod,
          @intCostRtTableNo,
          @sintGRMethod,
          @intGenResTableNo,
          MIN(TPD.StartDate),
          MAX(TPD.EndDate),
          @intRtCostDecimals,
          ISNULL(EM.ProvCostRate, 0),
          ISNULL(A.CostRate, T.CostRate)) AS CostRate,
       dbo.RP$LabRate
         (A.ResourceID,
          ISNULL(A.Category, 0),
          A.GRLBCD,
          ISNULL(A.LaborCode, T.LaborCode),
          @sintBillingRtMethod,
          @intBillingRtTableNo,
          @sintGRMethod,
          @intGRBillTableNo,
          MIN(TPD.StartDate),
          MAX(TPD.EndDate),
          @intRtBillDecimals,
          ISNULL(EM.ProvBillRate, 0),
          ISNULL(A.BillingRate, T.BillingRate)) AS BillingRate,                 
       1 AS PeriodCount, 'o' AS PeriodScale,
       LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate, 
       LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
       FROM #SaveLabTPD AS TPD
         INNER JOIN RPTask AS T ON TPD.PlanID = T.PlanID AND TPD.TaskID = T.TaskID
         LEFT JOIN RPAssignment AS A
           ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID AND TPD.AssignmentID = A.AssignmentID
         LEFT JOIN EM ON A.ResourceID = EM.Employee
       GROUP BY TPD.PlanID, TPD.TaskID, TPD.AssignmentID,
         A.ResourceID, A.Category, A.GRLBCD, A.LaborCode, A.CostRate, A.BillingRate,
         T.LaborCode, T.CostRate, T.BillingRate, EM.ProvCostRate, EM.ProvBillRate)
       
-- Expense >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
  -- Save overflow Expense Cost and Bill.
  
  SELECT TimePhaseID AS TimePhaseID, PlanID AS PlanID, TaskID AS TaskID, ExpenseID AS ExpenseID,
    StartDate AS StartDate, EndDate AS EndDate, PeriodCost AS PeriodCost, PeriodBill AS PeriodBill
    INTO #SaveExpTPD FROM
      (SELECT TPD.TimePhaseID AS TimePhaseID, TPD.PlanID AS PlanID, TPD.TaskID AS TaskID, TPD.ExpenseID AS ExpenseID,
         TPD.StartDate AS StartDate, TPD.EndDate AS EndDate, PeriodCost AS PeriodCost,
         CASE WHEN @strCalcExpBill = 'N' THEN PeriodBill ELSE 0.0 END AS PeriodBill
         FROM RPPlannedExpenses AS TPD INNER JOIN RPExpense AS E
           ON TPD.PlanID = E.PlanID AND TPD.TaskID = E.TaskID AND TPD.ExpenseID = E.ExpenseID
         WHERE TPD.PlanID = @strPlanID AND TPD.StartDate > @dtMaxEndDate
       UNION ALL
       SELECT TPD.TimePhaseID AS TimePhaseID, TPD.PlanID AS PlanID, TPD.TaskID AS TaskID, TPD.ExpenseID AS ExpenseID,
         TPD.StartDate AS StartDate, TPD.EndDate AS EndDate, PeriodCost AS PeriodCost,
         CASE WHEN @strCalcExpBill = 'N' THEN PeriodBill ELSE 0.0 END AS PeriodBill
         FROM RPPlannedExpenses AS TPD INNER JOIN
           (SELECT UT.TaskID, SUM(UT.ChildrenCount) AS ChildrenCount, 
                   UT.CostRate AS CostRate, UT.BillingRate AS BillingRate FROM
              (SELECT PT.TaskID AS TaskID, COUNT(CT.TaskID) AS ChildrenCount, 
                      PT.CostRate AS CostRate, PT.BillingRate AS BillingRate
                 FROM RPTask AS PT LEFT JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
                 WHERE PT.PlanID = @strPlanID GROUP BY PT.TaskID, PT.CostRate, PT.BillingRate
               UNION
               SELECT PT.TaskID AS TaskID, COUNT(E.ExpenseID) AS ChildrenCount, 
                      PT.CostRate AS CostRate, PT.BillingRate AS BillingRate 
                 FROM RPTask AS PT LEFT JOIN RPExpense AS E ON PT.PlanID = E.PlanID AND PT.TaskID = E.TaskID
                 WHERE PT.PlanID = @strPlanID GROUP BY PT.TaskID, PT.CostRate, PT.BillingRate) AS UT
            GROUP BY UT.TaskID, UT.CostRate, UT.BillingRate) AS T
           ON TPD.TaskID = T.TaskID AND T.ChildrenCount = 0
         WHERE TPD.PlanID = @strPlanID AND TPD.ExpenseID IS NULL AND TPD.StartDate > @dtMaxEndDate) AS X
 
  -- Delete overflow records.
  
  DELETE TPD FROM RPPlannedExpenses AS TPD INNER JOIN #SaveExpTPD AS X
    ON TPD.TimePhaseID = X.TimePhaseID AND TPD.PlanID = X.PlanID AND TPD.TaskID = X.TaskID
  
  -- Insert saved records into RPPlannedExpenses.
  
  INSERT INTO RPPlannedExpenses(TimePhaseID, PlanID, TaskID, ExpenseID,
    StartDate, EndDate, PeriodCost, PeriodBill, PeriodCount, PeriodScale, CreateDate, ModDate)
    (SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
       PlanID, TaskID, ExpenseID, 
       MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate, 
       ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals) AS PeriodCost, 
       ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals) AS PeriodBill, 
       1 AS PeriodCount, 'o' AS PeriodScale,
       LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate, 
       LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
       FROM #SaveExpTPD
       GROUP BY PlanID, TaskID, ExpenseID)
       
-- Consultant >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
  -- Save overflow Consultant Cost and Bill.
  
  SELECT TimePhaseID AS TimePhaseID, PlanID AS PlanID, TaskID AS TaskID, ConsultantID AS ConsultantID,
    StartDate AS StartDate, EndDate AS EndDate, PeriodCost AS PeriodCost, PeriodBill AS PeriodBill
    INTO #SaveConTPD FROM
      (SELECT TPD.TimePhaseID AS TimePhaseID, TPD.PlanID AS PlanID, TPD.TaskID AS TaskID, TPD.ConsultantID AS ConsultantID,
         TPD.StartDate AS StartDate, TPD.EndDate AS EndDate, PeriodCost AS PeriodCost,
         CASE WHEN @strCalcConBill = 'N' THEN PeriodBill ELSE 0.0 END AS PeriodBill
         FROM RPPlannedConsultant AS TPD INNER JOIN RPConsultant AS C
           ON TPD.PlanID = C.PlanID AND TPD.TaskID = C.TaskID AND TPD.ConsultantID = C.ConsultantID
         WHERE TPD.PlanID = @strPlanID AND TPD.StartDate > @dtMaxEndDate
       UNION ALL
       SELECT TPD.TimePhaseID AS TimePhaseID, TPD.PlanID AS PlanID, TPD.TaskID AS TaskID, TPD.ConsultantID AS ConsultantID,
         TPD.StartDate AS StartDate, TPD.EndDate AS EndDate, PeriodCost AS PeriodCost,
         CASE WHEN @strCalcConBill = 'N' THEN PeriodBill ELSE 0.0 END AS PeriodBill
         FROM RPPlannedConsultant AS TPD INNER JOIN
           (SELECT UT.TaskID, SUM(UT.ChildrenCount) AS ChildrenCount, 
                   UT.CostRate AS CostRate, UT.BillingRate AS BillingRate FROM
              (SELECT PT.TaskID AS TaskID, COUNT(CT.TaskID) AS ChildrenCount, 
                      PT.CostRate AS CostRate, PT.BillingRate AS BillingRate
                 FROM RPTask AS PT LEFT JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
                 WHERE PT.PlanID = @strPlanID GROUP BY PT.TaskID, PT.CostRate, PT.BillingRate
               UNION
               SELECT PT.TaskID AS TaskID, COUNT(C.ConsultantID) AS ChildrenCount, 
                      PT.CostRate AS CostRate, PT.BillingRate AS BillingRate 
                 FROM RPTask AS PT LEFT JOIN RPConsultant AS C ON PT.PlanID = C.PlanID AND PT.TaskID = C.TaskID
                 WHERE PT.PlanID = @strPlanID GROUP BY PT.TaskID, PT.CostRate, PT.BillingRate) AS UT
            GROUP BY UT.TaskID, UT.CostRate, UT.BillingRate) AS T
           ON TPD.TaskID = T.TaskID AND T.ChildrenCount = 0
         WHERE TPD.PlanID = @strPlanID AND TPD.ConsultantID IS NULL AND TPD.StartDate > @dtMaxEndDate) AS X
 
  -- Delete overflow records.
  
  DELETE TPD FROM RPPlannedConsultant AS TPD INNER JOIN #SaveConTPD AS X
    ON TPD.TimePhaseID = X.TimePhaseID AND TPD.PlanID = X.PlanID AND TPD.TaskID = X.TaskID
  
  -- Insert saved records into RPPlannedConsultant.
  
  INSERT INTO RPPlannedConsultant(TimePhaseID, PlanID, TaskID, ConsultantID,
    StartDate, EndDate, PeriodCost, PeriodBill, PeriodCount, PeriodScale, CreateDate, ModDate)
    (SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
       PlanID, TaskID, ConsultantID, 
       MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate, 
       ROUND(ISNULL(SUM(PeriodCost), 0), @intAmtCostDecimals) AS PeriodCost, 
       ROUND(ISNULL(SUM(PeriodBill), 0), @intAmtBillDecimals) AS PeriodBill, 
       1 AS PeriodCount, 'o' AS PeriodScale,
       LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate, 
       LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
       FROM #SaveConTPD
       GROUP BY PlanID, TaskID, ConsultantID)
       
-- Unit >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
  -- Save overflow Unit Qty.
  
  SELECT TimePhaseID AS TimePhaseID, PlanID AS PlanID, TaskID AS TaskID, UnitID AS UnitID,
    StartDate AS StartDate, EndDate AS EndDate, PeriodQty AS PeriodQty
    INTO #SaveUntTPD FROM
    (SELECT TPD.TimePhaseID AS TimePhaseID, TPD.PlanID AS PlanID, TPD.TaskID AS TaskID, TPD.UnitID AS UnitID,
       TPD.StartDate AS StartDate, TPD.EndDate AS EndDate, PeriodQty AS PeriodQty
       FROM RPPlannedUnit AS TPD INNER JOIN RPUnit AS A
         ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID AND TPD.UnitID = A.UnitID
       WHERE TPD.PlanID = @strPlanID AND TPD.StartDate > @dtMaxEndDate
     UNION ALL
     SELECT TPD.TimePhaseID AS TimePhaseID, TPD.PlanID AS PlanID, TPD.TaskID AS TaskID, TPD.UnitID AS UnitID,
       TPD.StartDate AS StartDate, TPD.EndDate AS EndDate, PeriodQty AS PeriodQty
       FROM RPPlannedUnit AS TPD INNER JOIN
         (SELECT UT.TaskID, SUM(UT.ChildrenCount) AS ChildrenCount, 
                 UT.CostRate AS CostRate, UT.BillingRate AS BillingRate FROM
            (SELECT PT.TaskID AS TaskID, COUNT(CT.TaskID) AS ChildrenCount, 
                    PT.CostRate AS CostRate, PT.BillingRate AS BillingRate
               FROM RPTask AS PT LEFT JOIN RPTask AS CT ON PT.PlanID = CT.PlanID AND PT.OutlineNumber = CT.ParentOutlineNumber
               WHERE PT.PlanID = @strPlanID GROUP BY PT.TaskID, PT.CostRate, PT.BillingRate
             UNION
             SELECT PT.TaskID AS TaskID, COUNT(A.UnitID) AS ChildrenCount, 
                    PT.CostRate AS CostRate, PT.BillingRate AS BillingRate 
               FROM RPTask AS PT LEFT JOIN RPUnit AS A ON PT.PlanID = A.PlanID AND PT.TaskID = A.TaskID
               WHERE PT.PlanID = @strPlanID GROUP BY PT.TaskID, PT.CostRate, PT.BillingRate) AS UT
          GROUP BY UT.TaskID, UT.CostRate, UT.BillingRate) AS T
         ON TPD.TaskID = T.TaskID AND T.ChildrenCount = 0
       WHERE TPD.PlanID = @strPlanID AND TPD.UnitID IS NULL AND TPD.StartDate > @dtMaxEndDate) AS X
 
  -- Delete overflow records.
  
  DELETE TPD FROM RPPlannedUnit AS TPD INNER JOIN #SaveUntTPD AS X
    ON TPD.TimePhaseID = X.TimePhaseID AND TPD.PlanID = X.PlanID AND TPD.TaskID = X.TaskID
  
  -- Insert saved records into RPPlannedUnit.
  
  INSERT INTO RPPlannedUnit(TimePhaseID, PlanID, TaskID, UnitID,
    StartDate, EndDate, PeriodQty, PeriodCount, PeriodScale, CreateDate, ModDate)
    (SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID, 
       PlanID, TaskID, UnitID, 
       MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate, 
       ROUND(ISNULL(SUM(PeriodQty), 0), @intHrDecimals) AS PeriodQty, 
       1 AS PeriodCount, 'o' AS PeriodScale,
       LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate, 
       LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
       FROM #SaveUntTPD
       GROUP BY PlanID, TaskID, UnitID)
       
  SET ANSI_WARNINGS ON  
  SET NOCOUNT OFF

END -- rpConsolidateOverflowTPD
GO
