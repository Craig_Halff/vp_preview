SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[VendorInTables] @Vendor Nvarchar(20),@TableName Nvarchar(50) output
as
declare
  @BaseStmt		Nvarchar(1000),
  @SqlStmt		Nvarchar(max)

begin 
  set @TableName = ''

  set @BaseStmt = 
	'Select ''?Table'' as TableName from ?Table where ?Vendor = N''' + @Vendor + ''''  

  set @SqlStmt = 'declare ChkCursor cursor for Select top 1 TableName from  ('

  if rtrim(@Vendor) <> '' 
  begin
    set @SqlStmt = @SqlStmt + replace(replace(@BaseStmt,'?Table','apMaster'),'?Vendor','Vendor')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','apppChecks'),'?Vendor','Vendor')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','BIED'),'?Vendor','Vendor')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','billConDetail'),'?Vendor','Vendor') + ' and OriginalTable <> ''LedgerEX'''
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','billExpDetail'),'?Vendor','Vendor') + ' and OriginalTable <> ''LedgerEX'''
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','BTEVVends'),'?Vendor','Vendor')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','cvMaster'),'?Vendor','Vendor')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','EB'),'?Vendor','Vendor')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','EM'),'?Vendor','Vendor')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','Form1099Data'),'?Vendor','Vendor')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','jeDetail'),'?Vendor','Vendor')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerAP'),'?Vendor','Vendor')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerAR'),'?Vendor','Vendor')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerEX'),'?Vendor','Vendor')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','LedgerMisc'),'?Vendor','Vendor')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','VO'),'?Vendor','Vendor')
    set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','POMaster'),'?Vendor','Vendor')
	set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','VendorItem'),'?Vendor','Vendor')
	set @SqlStmt = @SqlStmt + ' union ' + replace(replace(@BaseStmt,'?Table','CompanyItem'),'?Vendor','PrimaryVendor')

    set @SqlStmt = @SqlStmt + ') as TableCheck'

    execute (@SqlStmt)
    open ChkCursor
    fetch ChkCursor into @TableName
    close ChkCursor
    deallocate ChkCursor
  end
end -- VendorInTables  
GO
