SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_PAT_Config_Vendor_LoadObj] (
      @TaxAuditingEnabled                 bit,
      @preload                            Nvarchar(10),
      @pendingAll                         bit,
      @EMPLOYEEID                         Nvarchar(50),
      @OrderByName                        bit
) AS
BEGIN
      -- EXEC [spCCG_PAT_Config_Vendor_LoadObj] 1,'Y',1,'',1
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
    DECLARE @sql Nvarchar(max) = N'
        SELECT ve.Vendor, Name, Status, vea.Company, RegAccount, ReimbAccount, OHAccount,
                PayTerms, DefaultExpenseCode as ExpenseCode, TaxAuditingEnabled, ReadyForProcessing ' +
				(CASE WHEN @TaxAuditingEnabled=1 THEN ', vea.DefaultTaxCode ' ELSE '' END) + '
			FROM VE '

    IF SUBSTRING(@preload, 1, 1) = 'P'
    BEGIN
        -- limit to vendors with pending only
        IF @pendingAll = 1 AND @preload <> 'PN'
            SET @sql += '
                INNER JOIN (
                    SELECT distinct Vendor
                        FROM CCG_PAT_Payable p
                            INNER JOIN CCG_PAT_ConfigStages s on p.Stage = s.Stage
								and s.SubType not in (''NoExport'', ''Exported'')
                ) pv on pv.Vendor = VE.Vendor '
        ELSE IF @pendingAll = 0 AND @preload <> 'PA'
            SET @sql += '
                INNER JOIN (
                    SELECT distinct Vendor
                        FROM CCG_PAT_Payable p
                            INNER JOIN CCG_PAT_Pending pend on p.Seq = pend.PayableSeq
                                                and pend.Employee = N''' + @EMPLOYEEID + '''
                ) pv on pv.Vendor = VE.Vendor '
    END

	SET @sql += '
			LEFT JOIN VEAccounting vea on ve.vendor = vea.vendor
			LEFT JOIN CFGMainData m on vea.Company = m.Company
		WHERE VE.status <> ''D'' and VE.Vendor is not null
		ORDER BY ' + (CASE WHEN @OrderByName = 1 THEN '2' ELSE '1' END)

	EXEC(@sql)
END
GO
