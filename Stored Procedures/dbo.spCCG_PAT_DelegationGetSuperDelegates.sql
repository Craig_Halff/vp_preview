SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_DelegationGetSuperDelegates]
AS
BEGIN
/*
	Copyright (c) 2020 EleVia Software. All rights reserved.

	EXEC [spCCG_PAT_DelegationGetSuperDelegates]
*/
	SET NOCOUNT ON

	--DECLARE @ACCTROLES Nvarchar(1024) = (SELECT TOP 1 AcctRoles FROM CCG_PAT_Config)
	SELECT EM.Employee
		FROM CCG_PAT_ConfigRoles R
			INNER JOIN SEUser SU ON SU.Role = R.Role --OR (CHARINDEX(';'+SU.Role+';', ';'+@ACCTROLES+';') > 0)
			INNER JOIN EM ON SU.Employee = EM.Employee
		WHERE EM.Status = 'A' AND R.AdvancedOptions LIKE N'%SuperDelegate%'
		ORDER BY EM.Employee
END
GO
