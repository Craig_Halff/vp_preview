SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[HAI_spCCG_TerminationNotification]
	@Original						VARCHAR(5)
	, @Supervisor					NVARCHAR(20)
	, @FirstName					NVARCHAR(75)
	, @LastName						NVARCHAR(75)
	, @CustHireTermDate				NVARCHAR(20)
	, @BillingCategory				NVARCHAR(20)
	, @Title						NVARCHAR(50)
	, @Org							NVARCHAR(20)
	, @CustTermReplacementPercent	NVARCHAR(500)
	, @CustTermReplacementDate		NVARCHAR(500)
	, @CustTermEmailMonitorReq		NVARCHAR(500)
	, @CustTermEmailMonitorEmployee	NVARCHAR(500)
	, @CustTermPhoneMonitorReq		NVARCHAR(500)
	, @CustTermPhoneMonitorEmployee	NVARCHAR(500)
	, @CustTermComments				NVARCHAR(500)
	/*
		NOTE: SOME OF THESE PARAMS COULD BE DERIVED FROM THE EM TABLE BY EMPLOYEE NUMBER
	*/
AS

BEGIN
	SET NOCOUNT ON

	DECLARE @Body								NVARCHAR(MAX) = 'N'
		, @CSS									NVARCHAR(MAX) = 'N'
		, @SupervisorFName						NVARCHAR(100)
		, @SupervisorEmail						NVARCHAR(100)
		, @BillingCategoryDesc					NVARCHAR(50)
		, @CustTermEmailMonitorEmployeeName		NVARCHAR(255)
		, @CustTermPhoneMonitorEmployeeName		NVARCHAR(255)

	-- Remove extra whitespace
	SET @Original = RTRIM(LTRIM(@Original))
	SET @Supervisor = RTRIM(LTRIM(@Supervisor))
	SET @FirstName = RTRIM(LTRIM(@FirstName))
	SET @LastName = RTRIM(LTRIM(@LastName))
	SET @CustHireTermDate = RTRIM(LTRIM(@CustHireTermDate))
	SET @BillingCategory = RTRIM(LTRIM(@BillingCategory))
	SET @Title = RTRIM(LTRIM(@Title))
	SET @Org = RTRIM(LTRIM(@Org))
	SET @CustTermReplacementPercent = RTRIM(LTRIM(@CustTermReplacementPercent))
	SET @CustTermReplacementDate = RTRIM(LTRIM(@CustTermReplacementDate))
	SET @CustTermEmailMonitorReq = RTRIM(LTRIM(@CustTermEmailMonitorReq))
	SET @CustTermEmailMonitorEmployee = RTRIM(LTRIM(@CustTermEmailMonitorEmployee))
	SET @CustTermPhoneMonitorReq = RTRIM(LTRIM(@CustTermPhoneMonitorReq))
	SET @CustTermPhoneMonitorEmployee = RTRIM(LTRIM(@CustTermPhoneMonitorEmployee))
	SET @CustTermComments = RTRIM(LTRIM( @CustTermComments))

	-- Set to NULL if empty string
	IF @Supervisor = '' SET @Supervisor = NULL
	IF @FirstName = '' SET @FirstName = NULL
	IF @LastName = '' SET @LastName = NULL
	IF @CustHireTermDate = '' SET @CustHireTermDate = NULL
	IF @BillingCategory = '' SET @BillingCategory = NULL
	IF @Title = '' SET  @Title = NULL
	IF @Org = '' SET @Org = NULL
	IF @CustTermReplacementPercent = '' SET @CustTermReplacementPercent = NULL
	IF @CustTermReplacementDate = '' SET @CustTermReplacementDate = NULL
	IF @CustTermEmailMonitorReq = '' SET @CustTermEmailMonitorReq = NULL
	IF @CustTermEmailMonitorEmployee = '' SET @CustTermEmailMonitorEmployee = NULL
	IF @CustTermPhoneMonitorReq	= '' SET @CustTermPhoneMonitorReq = NULL
	IF @CustTermPhoneMonitorEmployee = '' SET @CustTermPhoneMonitorEmployee =  NULL
	IF @CustTermComments = '' SET @CustTermComments = NULL

	-- Lookups
	SELECT TOP 1 @SupervisorFName = FirstName
	,@SupervisorEmail = Email
	FROM [dbo].[EM]
	WHERE Employee = @Supervisor

	IF @BillingCategory IS NOT NULL
		SELECT TOP 1 @BillingCategoryDesc = Description
		FROM [dbo].[BTLaborCats]
		WHERE Category = @BillingCategory

	IF @CustTermEmailMonitorEmployee IS NOT NULL
		SELECT TOP 1 @CustTermEmailMonitorEmployeeName = FirstName + ' ' + LastName 
		FROM [dbo].[EM]
		WHERE Employee = @CustTermEmailMonitorEmployee

	IF @CustTermPhoneMonitorEmployee IS NOT NULL
		SELECT TOP 1 @CustTermPhoneMonitorEmployeeName = FirstName + ' ' + LastName 
		FROM [dbo].[EM]
		WHERE Employee = @CustTermPhoneMonitorEmployee

	-- Error checking
	IF @FirstName IS NULL
		THROW 51000, 'First Name param cannot be null or blank.', 1;
	IF @LastName IS NULL
		THROW 51000, 'Last Name param cannot be null or blank.', 1;
	IF @SupervisorEmail IS NULL OR @SupervisorFName IS NULL
		THROW 51000, 'Supervisor param is blank or invalid.', 1; 

	-- CSS style rules
	SET @CSS = 
'
<style type="text/css">
	.lbl { font-weight:bold; }
	.hilite { background-color: #FFFF00;}
</style>
'

	SET @Body = 
'<p>' + @SupervisorFName + ',</p>' + 
'<p>Please confirm the following information and reply with the missing information in yellow.</p>' + 
'<table>' + 
	'<tr>' + 
		'<td class="lbl">Name:</td>' +  
		'<td>' + 
			ISNULL(@FirstName, '') + ' ' + ISNULL(@LastName, '') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Start Date:</td>' +  
		'<td>' + 
			ISNULL(@CustHireTermDate, '--') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Labor Category:</td>' +  
		'<td>' + 
			ISNULL(@BillingCategoryDesc, '--') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Title:</td>' +  
		'<td>' + 
			ISNULL(@Title, '--') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Organization:</td>' +  
		'<td>' + 
			ISNULL(@Org, '--') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Likelihood of Replacement:</td>' +  
		'<td>' + 
			ISNULL(@CustTermReplacementPercent, '<i class="hilite">0%, 25%, 50%, 75%, 100%</i>') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Likely Replacement Date:</td>' +  
		'<td>' + 
			ISNULL(@CustTermReplacementDate, '<i class="hilite">Date</i>') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Email Monitoring Required:</td>' +  
		'<td>' + 
			ISNULL(@CustTermEmailMonitorReq, '<i class="hilite">Yes or No</i>') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Email Monitored By:</td>' +  
		'<td>' + 
			ISNULL(@CustTermEmailMonitorEmployeeName, '<i class="hilite">Employee</i>') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Phone Monitoring Required:</td>' +  
		'<td>' + 
			ISNULL(@CustTermPhoneMonitorReq, '<i class="hilite">Yes or No</i>') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Phone Monitored By:</td>' +  
		'<td>' + 
			ISNULL(@CustTermPhoneMonitorEmployeeName, '<i class="hilite">Employee</i>') + 
		'</td>' +  
	'</tr>' + 
	'<tr>' + 
		'<td class="lbl">Any Other Comments:</td>' +  
		'<td>' + 
			ISNULL(@CustTermComments, '<i class="hilite">Any Special Requirements</i>') + 
		'</td>' +  
	'</tr>' + 
'</table>' + 
'<br />' + 
'<p>Thanks,</p>' + 
'<p>Carlie Money</p>'


	--Email Recipient Information Below
	INSERT INTO CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
	VALUES 
	(
		'Employee Termination Notification', 
		'employeenotice@halff.com', 
		'jgray@halff.com', -- @SupervisorEmail 
 		NULL, 
		CASE WHEN @Original = 'N' 
			THEN 'Reminder: ' 
			ELSE '' 
		END + 'Termination Information Required - ' + @FirstName + ' ' + @LastName, 
		@CSS + @Body, 
		getDate(), 
		0
	)

END
GO
