SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[BuildGLSummary] @visionuser NVARCHAR(32)
AS
  BEGIN
      DECLARE @sessionID VARCHAR(32)

      SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
      SET NOCOUNT ON

      IF @visionuser IS NOT NULL AND @visionuser <> ''
        SELECT @sessionID = @visionuser
      ELSE
        SELECT @sessionID = 'NoUser'

      SELECT @sessionID = Substring(@sessionID, 1, 16) +
                          + CONVERT(NVARCHAR(25), Datepart(yyyy, Getutcdate()), 121)
                          + CONVERT(NVARCHAR(25), Datepart(mm, Getutcdate()), 121)
                          + CONVERT(NVARCHAR(25), Datepart(dd, Getutcdate()), 121)
                          + CONVERT(NVARCHAR(25), Datepart(hh, Getutcdate()), 121)
                          + CONVERT(NVARCHAR(25), Datepart(mi, Getutcdate()), 121)
                          + CONVERT(NVARCHAR(25), Datepart(ss, Getutcdate()), 121)
                          + CONVERT(NVARCHAR(25), Datepart(ms, Getutcdate()), 121)

      DELETE FROM GLSummary
      INSERT INTO GLSummary (Account, Period, Org, TransType, Amount, CBAmount, CreditAmount, DebitAmount, CBCreditAmount, CBDebitAmount)
      SELECT Account,
             Isnull(Period, 0)      AS Period,
             Isnull(Org, '')        AS Org,
			 Isnull(TransType, '')  AS TransType,
             Sum(Amount)            AS Amount,
             Sum(CBAmount)          AS CBAmount,
             Sum(CreditAmount)      AS CreditAmount,
             Sum(DebitAmount)       AS DebitAmount,
             Sum(CBCreditAmount)    AS CBCreditAmount,
             Sum(CBDebitAmount)     AS CBDebitAmount			 
      FROM   (SELECT Account,
                     Period,
                     Org,
					 TransType,
                     Sum(Amount)   AS Amount,
                     Sum(CBAmount) AS CBAmount,
                     Sum(CASE WHEN Amount < 0 THEN Abs(Amount) ELSE 0 END)      AS CreditAmount,
                     Sum(CASE WHEN Amount > 0 THEN Amount ELSE 0 END)			AS DebitAmount,
                     Sum(CASE WHEN CBAmount < 0 THEN Abs(CBAmount) ELSE 0 END)  AS CBCreditAmount,
                     Sum(CASE WHEN CBAmount > 0 THEN CBAmount ELSE 0 END)	    AS CBDebitAmount
              FROM   LedgerMisc
              WHERE  SkipGL = 'N'
                     AND Account IS NOT NULL
              GROUP  BY Account,
                        Period,
                        Org,
						TransType
              UNION ALL
              SELECT Account,
                     Period,
                     Org,
					 TransType,
                     Sum(Amount)   AS Amount,
                     Sum(CBAmount) AS CBAmount,
                     Sum(CASE WHEN Amount < 0 THEN Abs(Amount) ELSE 0 END)      AS CreditAmount,
                     Sum(CASE WHEN Amount > 0 THEN Amount ELSE 0 END)			AS DebitAmount,
                     Sum(CASE WHEN CBAmount < 0 THEN Abs(CBAmount) ELSE 0 END)  AS CBCreditAmount,
                     Sum(CASE WHEN CBAmount > 0 THEN CBAmount ELSE 0 END)	    AS CBDebitAmount
              FROM   LedgerAR
              WHERE  SkipGL = 'N'
                     AND Account IS NOT NULL
              GROUP  BY Account,
                        Period,
                        Org,
						TransType
              UNION ALL
              SELECT Account,
                     Period,
                     Org,
					 TransType,
                     Sum(Amount)   AS Amount,
                     Sum(CBAmount) AS CBAmount,
                     Sum(CASE WHEN Amount < 0 THEN Abs(Amount) ELSE 0 END)      AS CreditAmount,
                     Sum(CASE WHEN Amount > 0 THEN Amount ELSE 0 END)			AS DebitAmount,
                     Sum(CASE WHEN CBAmount < 0 THEN Abs(CBAmount) ELSE 0 END)  AS CBCreditAmount,
                     Sum(CASE WHEN CBAmount > 0 THEN CBAmount ELSE 0 END)	    AS CBDebitAmount
              FROM   LedgerAP
              WHERE  SkipGL = 'N'
                     AND Account IS NOT NULL
              GROUP  BY Account,
                        Period,
                        Org,
						TransType
              UNION ALL
              SELECT Account,
                     Period,
                     Org,
					 TransType,
                     Sum(Amount)   AS Amount,
                     Sum(CBAmount) AS CBAmount,
                     Sum(CASE WHEN Amount < 0 THEN Abs(Amount) ELSE 0 END)      AS CreditAmount,
                     Sum(CASE WHEN Amount > 0 THEN Amount ELSE 0 END)			AS DebitAmount,
                     Sum(CASE WHEN CBAmount < 0 THEN Abs(CBAmount) ELSE 0 END)  AS CBCreditAmount,
                     Sum(CASE WHEN CBAmount > 0 THEN CBAmount ELSE 0 END)	    AS CBDebitAmount
              FROM   LedgerEX
              WHERE  SkipGL = 'N'
                     AND Account IS NOT NULL
              GROUP  BY Account,
                        Period,
                        Org,
						TransType) AS inneryQry
      GROUP  BY Account,
                Isnull(Period, 0),
                Isnull(Org, ''),
				Isnull(TransType, '')

      UPDATE STATISTICS GLSummary

      UPDATE FW_CFGSystem
      SET    GLSummaryLastUpdate = Getutcdate()
  END -- BuildGLSummary
GO
