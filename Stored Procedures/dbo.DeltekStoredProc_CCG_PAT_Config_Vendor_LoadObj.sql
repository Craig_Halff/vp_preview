SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_Vendor_LoadObj] ( @TaxAuditingEnabled bit, @preload nvarchar(10), @pendingAll bit, @EMPLOYEEID nvarchar(50), @OrderByName bit)
             AS EXEC spCCG_PAT_Config_Vendor_LoadObj @TaxAuditingEnabled,@preload,@pendingAll,@EMPLOYEEID,@OrderByName
GO
