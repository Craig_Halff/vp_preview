SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Ultra_Get_Open_Periods]
	@isVersion73	bit = 1--remove
AS BEGIN
	SET NOCOUNT ON;


		SELECT period, period
            FROM cfgdates
            WHERE Period in (SELECT Distinct Period FROM CFGDatesStatus WHERE Closed<>'Y')
                or DateDiff(month, AccountPdEnd, Getdate()) < 3
            ORDER BY 1 desc

END

GO
