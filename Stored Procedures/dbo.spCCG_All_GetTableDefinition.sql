SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_All_GetTableDefinition] (@tableName varchar(500), @sql varchar(max) OUTPUT)
AS BEGIN
	-- Simplified to only produce basic TABLE definitions
	set nocount on
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	declare @tableCC_Cols table (ColName varchar(1000), DataType varchar(200),
		IsNullable varchar(10), MaxLen int, IsIdentityColumn varchar(10), IsUnique int, ColDefault varchar(2000))
	insert into @tableCC_Cols
		select * from dbo.[fnCCG_All_GetTableColumns](@tableName)

	set @sql = '
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].['+@tableName+']'') AND type in (''U''))
BEGIN
	CREATE TABLE [dbo].'+@tableName+ ' (
'+ IsNull(STUFF((
		SELECT ', '+char(13)+char(10)+'		['+c.ColName+'] ' + c.DataType
			+ (case when c.MaxLen is null then '' else '('+(case when c.MaxLen = -1 then 'max' else cast(c.MaxLen as varchar) end) + ')' end)
			+ (Case when c.IsNullable = 'YES' then ' NULL' else ' NOT NULL' end) +
			+ (case when c.ColDefault is not null then ' DEFAULT'+c.ColDefault else '' end)
			FROM @tableCC_Cols c
			FOR XML PATH(''), TYPE
		).value('.', 'nvarchar(MAX)'),1,4,''),'') + '
	) '

	if exists (select 'x' from @tableCC_Cols c where c.IsUnique = 1)
		set @sql += '
	CONSTRAINT [PK_'+@tableName+'] PRIMARY KEY NONCLUSTERED
	(
		' + IsNull(STUFF((
		SELECT ', ['+c.ColName+'] ASC'
			FROM @tableCC_Cols c
			where c.IsUnique = 1
			FOR XML PATH(''), TYPE
		).value('.', 'nvarchar(MAX)'),1,2,''),'') + '
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = OFF, ALLOW_PAGE_LOCKS = OFF) ON [PRIMARY]'
	set @sql += '
END
GO--
'
END
GO
