SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpSpreadExpConVar]
  @strPlanID varchar(32),
  @dtETCDate datetime,
  @strMatchWBS1Wildcard varchar(1) = 'N',
  @strCommitmentFlg varchar(1) = 'N',
  @strExcludeUNFlg varchar(1) = 'N',
  @strCalcBill varchar(1) = 'N',
  @strType varchar(1) = 'E',
  @strIgnoreVariance varchar(1) = 'N'
AS

BEGIN -- Procedure rpSpreadExpConVar

  SET NOCOUNT ON
  
  DECLARE @intAmtCostDecimals int
  DECLARE @intAmtBillDecimals int
  
  DECLARE @dtCalendarMin datetime
  
  DECLARE @strCompany Nvarchar(14)
  
  SELECT @dtCalendarMin = MIN(StartDate) FROM #Calendar

  DECLARE @tabExpCon TABLE
    (RowID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     PlannedCost decimal(19,4),
     PlannedBill decimal(19,4),
     VARCost decimal(19,4),
     VARBill decimal(19,4),
     RemainWD float,
     MinDate datetime,
     MaxDate datetime)
     
  DECLARE @tabETCTPD TABLE
    (RowID varchar(32) COLLATE database_default,
     TaskID varchar(32) COLLATE database_default,
     StartDate datetime,
     EndDate datetime,
     PeriodCost decimal(19,4),
     PeriodBill decimal(19,4))
  
    -- Get decimal settings.
  
  SELECT @intAmtCostDecimals = AmtCostDecimals,
         @intAmtBillDecimals = AmtBillDecimals
    FROM dbo.RP$tabDecimals(@strPlanID)
    
  -- Get Current Company setting.
  
  SELECT @strCompany = dbo.GetActiveCompany()

  -- Get all Expense/Consultant rows that have time-phased data span across ETC Date.
  
  IF (@strType = 'E')
    INSERT @tabExpCon
      SELECT DISTINCT 
        EC.ExpenseID AS RowID, 
        EC.TaskID AS TaskID,
        EC.PlannedExpCost AS PlannedCost,
        EC.PlannedExpBill AS PlannedBill,
        0 AS VARCost,
        0 AS VARBill,
        dbo.DLTK$NumWorkingDays(@dtETCDate, XTPD.EndDate, @strCompany) AS RemainWD,
        XTPD.StartDate AS MinDate,
        XTPD.EndDate AS MaxDate
        FROM RPExpense AS EC
          LEFT JOIN 
            (SELECT TaskID, ExpenseID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate
				       FROM RPPlannedExpenses WHERE PlanID = @strPlanID AND ExpenseID IS NOT NULL
			         GROUP BY TaskID, ExpenseID) AS XTPD 
            ON EC.TaskID = XTPD.TaskID AND EC.ExpenseID = XTPD.ExpenseID
        WHERE EC.PlanID = @strPlanID AND 
          ((@strIgnoreVariance != 'Y' AND XTPD.StartDate <= @dtETCDate) OR
           (@strIgnoreVariance = 'Y'))
  ELSE
    INSERT @tabExpCon
      SELECT DISTINCT 
        EC.ConsultantID AS RowID, 
        EC.TaskID AS TaskID, 
        EC.PlannedConCost AS PlannedCost,
        EC.PlannedConBill AS PlannedBill, 
        0 AS VARCost,
        0 AS VARBill,
        dbo.DLTK$NumWorkingDays(@dtETCDate, XTPD.EndDate, @strCompany) AS RemainWD,
        XTPD.StartDate AS MinDate,
        XTPD.EndDate AS MaxDate
        FROM RPConsultant AS EC
          LEFT JOIN
            (SELECT TaskID, ConsultantID, MIN(StartDate) AS StartDate, MAX(EndDate) AS EndDate
				       FROM RPPlannedConsultant WHERE PlanID = @strPlanID AND ConsultantID IS NOT NULL
			         GROUP BY TaskID, ConsultantID) AS XTPD 
            ON EC.TaskID = XTPD.TaskID AND EC.ConsultantID = XTPD.ConsultantID
        WHERE EC.PlanID = @strPlanID AND 
          ((@strIgnoreVariance != 'Y' AND XTPD.StartDate <= @dtETCDate) OR
           (@strIgnoreVariance = 'Y'))
    
  -- Compute ETC time-phased data for the selected Expense/Consultant rows.
  
  IF (@strType = 'E')
    INSERT @tabETCTPD
      SELECT 
        TPD.ExpenseID AS RowID, 
        TPD.TaskID AS TaskID, 
        CASE WHEN TPD.StartDate <= @dtETCDate THEN @dtETCDate ELSE TPD.StartDate END AS StartDate, 
        TPD.EndDate AS EndDate,
        ROUND(CASE WHEN TPD.StartDate <= @dtETCDate AND TPD.EndDate >= @dtETCDate
                   THEN PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)
                   ELSE PeriodCost END, @intAmtCostDecimals) AS PeriodCost,
        ROUND(CASE WHEN TPD.StartDate <= @dtETCDate AND TPD.EndDate >= @dtETCDate
                   THEN PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)
                   ELSE PeriodBill END, @intAmtBillDecimals) AS PeriodBill
        FROM RPPlannedExpenses AS TPD 
          INNER JOIN @tabExpCon AS EC ON TPD.PlanID = @strPlanID AND TPD.TaskID = EC.TaskID AND TPD.ExpenseID = EC.RowID
        WHERE TPD.PlanID = @strPlanID AND TPD.EndDate >= @dtETCDate
  ELSE
    INSERT @tabETCTPD
      SELECT 
        TPD.ConsultantID AS RowID, 
        TPD.TaskID AS TaskID, 
        CASE WHEN TPD.StartDate <= @dtETCDate THEN @dtETCDate ELSE TPD.StartDate END AS StartDate, 
        TPD.EndDate AS EndDate,
        ROUND(CASE WHEN TPD.StartDate <= @dtETCDate AND TPD.EndDate >= @dtETCDate
                   THEN PeriodCost * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)
                   ELSE PeriodCost END, @intAmtCostDecimals) AS PeriodCost,
        ROUND(CASE WHEN TPD.StartDate <= @dtETCDate AND TPD.EndDate >= @dtETCDate
                   THEN PeriodBill * dbo.DLTK$ProrateRatio(@dtETCDate, TPD.EndDate, TPD.StartDate, TPD.EndDate, @strCompany)
                   ELSE PeriodBill END, @intAmtBillDecimals) AS PeriodBill
        FROM RPPlannedConsultant AS TPD 
          INNER JOIN @tabExpCon AS EC ON TPD.PlanID = @strPlanID AND TPD.TaskID = EC.TaskID AND TPD.ConsultantID = EC.RowID
        WHERE TPD.PlanID = @strPlanID AND TPD.EndDate >= @dtETCDate
      
  -- Compute JTD time-phased data for the selected Expense/Consultant rows.
  
  SELECT X.TaskID AS TaskID, X.RowID AS RowID,  
    CI.StartDate AS StartDate,  
    CI.EndDate AS EndDate,  
    ROUND(SUM(ISNULL(X.PeriodCost, 0)), 2) AS PeriodCost,
    ROUND(SUM(ISNULL(X.PeriodBill, 0)), 2) AS PeriodBill
    INTO #JTDTPD
    FROM @tabExpCon AS ZEC
      INNER JOIN dbo.RP$tabJTDExpCon(@strPlanID, @dtETCDate, @strCommitmentFlg, @strMatchWBS1Wildcard, @strExcludeUNFlg, @strType) AS X 
        ON (ZEC.TaskID = X.TaskID AND ZEC.RowID = X.RowID)  
      INNER JOIN #Calendar AS CI  
        ON (CASE WHEN (@strIgnoreVariance != 'Y' AND X.TransDate < ZEC.MinDate) THEN ZEC.MinDate  
                 WHEN (@strIgnoreVariance = 'Y' AND X.TransDate < @dtCalendarMin) THEN @dtCalendarMin 
                 ELSE X.TransDate END) BETWEEN CI.StartDate AND CI.EndDate  
    GROUP BY X.RowID, CI.PeriodScale, CI.StartDate, X.TaskID, CI.EndDate, ZEC.MinDate, ZEC.MaxDate
      
  -- Compute Variance = Planned - JTD.
  
  UPDATE @tabExpCon 
    SET VARCost = ROUND(ISNULL((PlannedCost - ISNULL(JTDCost, 0)), 0), @intAmtCostDecimals),
        VARBill = ROUND(ISNULL((PlannedBill - ISNULL(JTDBill, 0)), 0), @intAmtBillDecimals)
    FROM @tabExpCon AS EC LEFT JOIN
      (SELECT TaskID, RowID,
         ROUND(SUM(ISNULL(PeriodCost, 0)), 2) AS JTDCost,
         ROUND(SUM(ISNULL(PeriodBill, 0)), 2) AS JTDBill
         FROM #JTDTPD
         GROUP BY TaskID, RowID) AS YEC
      ON EC.TaskID = YEC.TaskID AND EC.RowID = YEC.RowID
       
  -- Eliminate Assignment rows with Variance less than zero.
  
  DELETE @tabExpCon WHERE @strIgnoreVariance != 'Y' AND (VARCost < 0 AND (VARBill < 0 OR @strCalcBill = 'Y'))
  
  -- Delete time-phased data row for the selected Expense/Consultant rows.
  
  IF (@strType = 'E')
    DELETE RPPlannedExpenses
      FROM RPPlannedExpenses AS TPD INNER JOIN @tabExpCon AS EC
        ON TPD.PlanID = @strPlanID AND TPD.TaskID = EC.TaskID AND TPD.ExpenseID = EC.RowID
  ELSE
    DELETE RPPlannedConsultant
      FROM RPPlannedConsultant AS TPD INNER JOIN @tabExpCon AS EC
        ON TPD.PlanID = @strPlanID AND TPD.TaskID = EC.TaskID AND TPD.ConsultantID = EC.RowID
        
  -- Recreate time-phased data row for the selected Expense/Consultant rows.
  -- Revised Planned Cost = JTD Cost + ETC Cost + Spread Cost
  -- If NOT CalculatingBill 
  --   Then Planned Bill = JTD Bill + ETC Bill + Spread Bill
  --   Else Planned Bill = 0 (will be calculated later by rpCalcTPD.
  -- Spread Cost = 0 for days before ETC Date.
  
  IF (@strType = 'E')
    INSERT INTO RPPlannedExpenses(TimePhaseID, PlanID, TaskID, ExpenseID,
      StartDate, EndDate, PeriodCost, PeriodBill,
      PeriodCount, PeriodScale, CreateDate, ModDate)
      SELECT
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
        @strPlanID AS PlanID, 
        TaskID, 
        ExpenseID,
        StartDate, 
        EndDate, 
        PeriodCost, 
        PeriodBill,
        PeriodCount, 
        PeriodScale,
        LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate, 
        LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
        FROM
          (SELECT
             EC.TaskID AS TaskID, 
             EC.RowID AS ExpenseID,
             CASE WHEN (MinDate BETWEEN CI.StartDate AND CI.EndDate) THEN MinDate ELSE CI.StartDate END AS StartDate,
             CASE WHEN (MaxDate BETWEEN CI.StartDate AND CI.EndDate) THEN MaxDate ELSE CI.EndDate END AS EndDate,
             ROUND((CASE WHEN CI.PeriodScale <> 'o' THEN ISNULL(JTD.PeriodCost, 0) ELSE 0 END + 
                    (CASE WHEN @strIgnoreVariance = 'Y' THEN ISNULL(ETC.PeriodCost, 0)
                          ELSE 0 END) +
                    (CASE WHEN @strIgnoreVariance = 'Y' THEN 0
                          WHEN (MaxDate < @dtETCDate AND CI.StartDate <= @dtETCDate AND CI.EndDate >= @dtETCDate AND CI.PeriodScale <> 'o') THEN VARCost
                          WHEN ETC.StartDate IS NULL THEN 0
                          WHEN RemainWD = 0 THEN 0
                          ELSE ((VARCost * dbo.DLTK$NumWorkingDays(ETC.StartDate, ETC.EndDate, @strCompany)) / 
                                RemainWD) END)), @intAmtCostDecimals) AS PeriodCost,
             CASE WHEN @strCalcBill = 'N'
                  THEN ROUND((CASE WHEN CI.PeriodScale <> 'o' THEN ISNULL(JTD.PeriodBill, 0) ELSE 0 END + 
                              (CASE WHEN @strIgnoreVariance = 'Y' THEN ISNULL(ETC.PeriodBill, 0) 
                                    ELSE 0 END) +
                              (CASE WHEN @strIgnoreVariance = 'Y' THEN 0
                                    WHEN (MaxDate < @dtETCDate AND CI.StartDate <= @dtETCDate AND CI.EndDate >= @dtETCDate AND CI.PeriodScale <> 'o') THEN VARBill
                                    WHEN ETC.StartDate IS NULL THEN 0
                                    WHEN RemainWD = 0 THEN 0
                                    ELSE ((VARBill * dbo.DLTK$NumWorkingDays(ETC.StartDate, ETC.EndDate, @strCompany)) / 
                                          RemainWD) END)), @intAmtBillDecimals)
                  ELSE 0
                  END AS PeriodBill,
             1 AS PeriodCount, 
             CI.PeriodScale AS PeriodScale
             FROM #Calendar AS CI LEFT JOIN @tabExpCon AS EC ON 1 = 1
               LEFT JOIN @tabETCTPD AS ETC ON (ETC.StartDate BETWEEN CI.StartDate AND CI.EndDate
                 AND EC.TaskID = ETC.TaskID AND EC.RowID = ETC.RowID)
               LEFT JOIN #JTDTPD AS JTD ON (JTD.StartDate BETWEEN CI.StartDate AND CI.EndDate
                 AND EC.TaskID = JTD.TaskID AND EC.RowID = JTD.RowID)
             WHERE (@strIgnoreVariance != 'Y' AND
                    (ETC.TaskID IS NOT NULL OR 
                     JTD.TaskID IS NOT NULL OR 
                     (MaxDate < @dtETCDate AND CI.StartDate <= @dtETCDate AND CI.EndDate >= @dtETCDate)))
               OR (@strIgnoreVariance = 'Y')                   
          ) AS Z
        WHERE Z.PeriodCost > 0 OR Z.PeriodBill > 0
  ELSE
    INSERT INTO RPPlannedConsultant(TimePhaseID, PlanID, TaskID, ConsultantID,
      StartDate, EndDate, PeriodCost, PeriodBill,
      PeriodCount, PeriodScale, CreateDate, ModDate)
      SELECT
        REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
        @strPlanID AS PlanID, 
        TaskID, 
        ConsultantID,
        StartDate, 
        EndDate, 
        PeriodCost, 
        PeriodBill,
        PeriodCount, 
        PeriodScale,
        LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS CreateDate, 
        LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) AS ModDate
        FROM
          (SELECT
             EC.TaskID AS TaskID, 
             EC.RowID AS ConsultantID,
             CASE WHEN (MinDate BETWEEN CI.StartDate AND CI.EndDate) THEN MinDate ELSE CI.StartDate END AS StartDate,
             CASE WHEN (MaxDate BETWEEN CI.StartDate AND CI.EndDate) THEN MaxDate ELSE CI.EndDate END AS EndDate,
             ROUND((CASE WHEN CI.PeriodScale <> 'o' THEN ISNULL(JTD.PeriodCost, 0) ELSE 0 END + 
                    (CASE WHEN @strIgnoreVariance = 'Y' THEN ISNULL(ETC.PeriodCost, 0)
                          ELSE 0 END) +
                    (CASE WHEN @strIgnoreVariance = 'Y' THEN 0
                          WHEN (MaxDate < @dtETCDate AND CI.StartDate <= @dtETCDate AND CI.EndDate >= @dtETCDate AND CI.PeriodScale <> 'o') THEN VARCost
                          WHEN ETC.StartDate IS NULL THEN 0
                          WHEN RemainWD = 0 THEN 0
                          ELSE ((VARCost * dbo.DLTK$NumWorkingDays(ETC.StartDate, ETC.EndDate, @strCompany)) / 
                                RemainWD) END)), @intAmtCostDecimals) AS PeriodCost,
             CASE WHEN @strCalcBill = 'N'
                  THEN ROUND((CASE WHEN CI.PeriodScale <> 'o' THEN ISNULL(JTD.PeriodBill, 0) ELSE 0 END + 
                              (CASE WHEN @strIgnoreVariance = 'Y' THEN ISNULL(ETC.PeriodBill, 0) 
                                    ELSE 0 END) +
                              (CASE WHEN @strIgnoreVariance = 'Y' THEN 0
                                    WHEN (MaxDate < @dtETCDate AND CI.StartDate <= @dtETCDate AND CI.EndDate >= @dtETCDate) THEN VARBill
                                    WHEN ETC.StartDate IS NULL THEN 0
                                    WHEN RemainWD = 0 THEN 0
                                    ELSE ((VARBill * dbo.DLTK$NumWorkingDays(ETC.StartDate, ETC.EndDate, @strCompany)) / 
                                          RemainWD) END)), @intAmtBillDecimals)
                  ELSE 0
                  END AS PeriodBill,
             1 AS PeriodCount, 
             CI.PeriodScale AS PeriodScale
             FROM #Calendar AS CI LEFT JOIN @tabExpCon AS EC ON 1 = 1
               LEFT JOIN @tabETCTPD AS ETC ON (ETC.StartDate BETWEEN CI.StartDate AND CI.EndDate
                 AND EC.TaskID = ETC.TaskID AND EC.RowID = ETC.RowID)
               LEFT JOIN #JTDTPD AS JTD ON (JTD.StartDate BETWEEN CI.StartDate AND CI.EndDate
                 AND EC.TaskID = JTD.TaskID AND EC.RowID = JTD.RowID)
             WHERE (@strIgnoreVariance != 'Y' AND
                    (ETC.TaskID IS NOT NULL OR 
                     JTD.TaskID IS NOT NULL OR 
                     (MaxDate < @dtETCDate AND CI.StartDate <= @dtETCDate AND CI.EndDate >= @dtETCDate)))
               OR (@strIgnoreVariance = 'Y')                   
          ) AS Z
        WHERE Z.PeriodCost > 0 OR Z.PeriodBill > 0
                  
  -- Adjust Planned time-phased data to compensate for rounding errors.
 
  IF (@strIgnoreVariance != 'Y')
    BEGIN
      IF (@strType = 'E')
        UPDATE RPPlannedExpenses 
          SET PeriodCost = ROUND((TPD.PeriodCost + DH.DeltaCost), @intAmtCostDecimals),
              PeriodBill = CASE WHEN @strCalcBill = 'N'
                                THEN (TPD.PeriodBill + DH.DeltaBill)
                                ELSE 0 END                      
          FROM RPPlannedExpenses AS TPD 
            INNER JOIN @tabExpCon AS EC ON TPD.EndDate = EC.MaxDate
              AND TPD.PlanID = @strPlanID AND TPD.TaskID = EC.TaskID AND TPD.ExpenseID = EC.RowID
            INNER JOIN
              (SELECT XEC.TaskID, XEC.RowID, -- Compute Delta Hours
                (XEC.PlannedCost - SUM(ISNULL(XTPD.PeriodCost, 0))) AS DeltaCost,
                (XEC.PlannedBill - SUM(ISNULL(XTPD.PeriodBill, 0))) AS DeltaBill
                FROM RPPlannedExpenses AS XTPD INNER JOIN @tabExpCon AS XEC 
                  ON XTPD.PlanID = @strPlanID AND XTPD.TaskID = XEC.TaskID AND XTPD.ExpenseID = XEC.RowID
                GROUP BY XEC.TaskID, XEC.RowID, XEC.PlannedCost, XEC.PlannedBill) AS DH
              ON TPD.PlanID = @strPlanID AND TPD.TaskID = DH.TaskID AND TPD.ExpenseID = DH.RowID
      ELSE
        UPDATE RPPlannedConsultant 
          SET PeriodCost = ROUND((TPD.PeriodCost + DH.DeltaCost), @intAmtCostDecimals),
              PeriodBill = CASE WHEN @strCalcBill = 'N'
                                THEN (TPD.PeriodBill + DH.DeltaBill)
                                ELSE 0 END                      
          FROM RPPlannedConsultant AS TPD 
            INNER JOIN @tabExpCon AS EC ON TPD.EndDate = EC.MaxDate
              AND TPD.PlanID = @strPlanID AND TPD.TaskID = EC.TaskID AND TPD.ConsultantID = EC.RowID
            INNER JOIN
              (SELECT XEC.TaskID, XEC.RowID, 
                (XEC.PlannedCost - SUM(ISNULL(XTPD.PeriodCost, 0))) AS DeltaCost,
                (XEC.PlannedBill - SUM(ISNULL(XTPD.PeriodBill, 0))) AS DeltaBill
                FROM RPPlannedConsultant AS XTPD INNER JOIN @tabExpCon AS XEC 
                  ON XTPD.PlanID = @strPlanID AND XTPD.TaskID = XEC.TaskID AND XTPD.ConsultantID = XEC.RowID
                GROUP BY XEC.TaskID, XEC.RowID, XEC.PlannedCost, XEC.PlannedBill) AS DH
              ON TPD.PlanID = @strPlanID AND TPD.TaskID = DH.TaskID AND TPD.ConsultantID = DH.RowID
    END -- If-Then
    
  -- Update Expense/Consultant Start/End Date, if necessary.
  
  IF (@strType = 'E')
    UPDATE RPExpense SET StartDate = TPD.StartDate, EndDate = TPD.EndDate
      FROM RPExpense AS E
        INNER JOIN
          (SELECT XEC.TaskID AS TaskID, XEC.RowID AS ExpenseID,
             MIN(XTPD.StartDate) AS StartDate, MAX(XTPD.EndDate) AS EndDate
             FROM RPPlannedExpenses AS XTPD INNER JOIN @tabExpCon AS XEC 
               ON XTPD.PlanID = @strPlanID AND XTPD.TaskID = XEC.TaskID AND XTPD.ExpenseID = XEC.RowID
             GROUP BY XEC.TaskID, XEC.RowID) AS TPD
          ON E.PlanID = @strPlanID AND E.TaskID = TPD.TaskID AND E.ExpenseID = TPD.ExpenseID
  ELSE
    UPDATE RPConsultant SET StartDate = TPD.StartDate, EndDate = TPD.EndDate
      FROM RPConsultant AS C
        INNER JOIN
          (SELECT XEC.TaskID AS TaskID, XEC.RowID AS ConsultantID,
             MIN(XTPD.StartDate) AS StartDate, MAX(XTPD.EndDate) AS EndDate
             FROM RPPlannedConsultant AS XTPD INNER JOIN @tabExpCon AS XEC 
               ON XTPD.PlanID = @strPlanID AND XTPD.TaskID = XEC.TaskID AND XTPD.ConsultantID = XEC.RowID
             GROUP BY XEC.TaskID, XEC.RowID) AS TPD
          ON C.PlanID = @strPlanID AND C.TaskID = TPD.TaskID AND C.ConsultantID = TPD.ConsultantID
   
  SET NOCOUNT OFF

END -- rpSpreadExpConVar
GO
