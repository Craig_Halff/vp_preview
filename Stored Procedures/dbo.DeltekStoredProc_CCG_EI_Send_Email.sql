SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Send_Email]
	@from			Nvarchar(80),
	@to				Nvarchar(200),
	@subject		Nvarchar(120),
	@subjectbatch	Nvarchar(120),
	@body			Nvarchar(max),
	@ccSender		bit = 0,
	@maxDelay		int = NULL,
	@priority		varchar(12) = NULL
AS
BEGIN
	/*
	DeltekStoredProc_CCG_EI_Send_Email
		@from = 'steve.stolz@CentralConsultingGroup.com',
		@to = 'steve.stolz@CentralConsultingGroup.com',
		@subject = 'Invoice 1999015.00 ready for write-off approval',
		@subjectbatch = 'Electronic Invoicing Reminder',
		@body = 'Please have your invoice(s) approved within 3 days.',
		@ccSender = '0',
		@maxDelay = '240',
		@priority = ''
	*/
	SET NOCOUNT ON;
	IF @subjectbatch = '' SET @subjectbatch = NULL;
	IF @priority = '' SET @priority = NULL;

	INSERT into CCG_Email_Messages
		(SourceApplication, Sender, ToList, CCList, BCCList,
			Subject, GroupSubject, Body, MaxDelay, Priority)
		VALUES ('Electronic Invoicing', @from, @to, (Case When @ccSender = 1 Then @from Else NULL END), null,
			@subject, @subjectbatch, @body, @maxDelay, @priority);

	SELECT @@ROWCOUNT;
END;
GO
