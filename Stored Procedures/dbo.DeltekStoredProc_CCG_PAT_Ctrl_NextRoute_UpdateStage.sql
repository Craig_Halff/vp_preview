SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Ctrl_NextRoute_UpdateStage]
	@seq		int,
	@stageName	varchar(30)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CCG_PAT_Pending SET Stage = @stageName
		WHERE Seq = @seq;
END;

GO
