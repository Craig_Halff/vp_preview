SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_All_GetDefinitions] (@sql varchar(max) OUTPUT)
AS BEGIN
	set nocount on
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	declare @rtnVal varchar(max) = ''
	declare @ObjectName varchar(1000), @Type varchar(10), @Definition varchar(max)
	declare cur cursor fast_forward for select ObjectName, ObjectType, [Definition] from #tableDefs order by TopOrder desc

	open cur
	fetch next from cur into @ObjectName, @Type, @Definition
	while @@FETCH_STATUS = 0
	begin
		set @ObjectName = replace(replace(replace(@ObjectName, 'dbo.', ''), ']', ''), '[', '')
		set @rtnVal += char(13)+char(10)+'------------------------------------------------------------------------------------------------------------------------------------------------------'+char(13)+char(10)
		if left(@Definition, 3) = '<<<'
		begin
			set @rtnVal += '---- '+@ObjectName+': ' + @Definition+char(13)+char(10)
		end
		else if left(@type, 1) = 'U'
		begin
			declare @rtnVal2 varchar(max) = ''
			exec spCCG_All_GetTableDefinition @ObjectName, @rtnVal2 output
			set @rtnVal += @rtnVal2
		end
		else begin
			set @rtnVal +=
'IF NOT EXISTS (select ''x'' from sys.objects where object_id = OBJECT_ID(N''[dbo].['+@ObjectName+']'') AND type in ('''+@Type+'''))
BEGIN' + char(13)+char(10)
			if @type in ('P', 'PC') set @rtnVal += '	EXEC dbo.sp_executesql @statement = N''CREATE PROCEDURE [dbo].['+@ObjectName+'] AS'''
			else if @type in ('FN', 'IF', 'FS') set @rtnVal += '	EXEC dbo.sp_executesql @statement = N''CREATE FUNCTION [dbo].['+@ObjectName+']() RETURNS INT AS BEGIN RETURN NULL END'''
			else if @type in ('TF', 'FT') set @rtnVal += '	EXEC dbo.sp_executesql @statement = N''CREATE FUNCTION [dbo].['+@ObjectName+']() RETURNS @T TABLE(Value varchar(500)) AS BEGIN RETURN END'''
			set @rtnVal += char(13)+char(10)+ 'END' +char(13)+char(10)+ 'GO'+char(13)+char(10)

			set @Definition = rtrim(ltrim(replace(replace(replace(@Definition, CHAR(13) + CHAR(10), ' [[BREAK1310]] '), CHAR(13), ' [[BREAK13]] '), CHAR(10), ' [[BREAK10]] ')))
			set @Definition = replace(replace(replace(replace(replace(replace(@Definition, ' [[BREAK1310]] ', CHAR(13) + CHAR(10)), ' [[BREAK13]] ', CHAR(13)), ' [[BREAK10]] ', CHAR(10)),
				'[[BREAK1310]]', ''), '[[BREAK13]]', ''), '[[BREAK10]]', '')
			set @Definition = rtrim(ltrim(replace(replace(left(@Definition, 200), 'CREATE PROCEDURE ', 'ALTER PROCEDURE '), 'CREATE FUNCTION ', 'ALTER FUNCTION ') +
				substring(@Definition, 201, 500000)))

			set @rtnVal += @Definition
			set @rtnVal += char(13)+char(10)+ 'GO' +char(13)+char(10)
		end
		fetch next from cur into @ObjectName, @Type, @Definition
	end
	close cur
	deallocate cur
	set @sql = @rtnVal
END
GO
