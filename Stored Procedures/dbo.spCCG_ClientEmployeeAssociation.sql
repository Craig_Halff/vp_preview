SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ClientEmployeeAssociation]
AS
/*
Copyright (c) 2020 Central Consulting Group. All rights reserved.
06/12/2020	David Springer
			Write Project Manager to the employee associations grid.
			Write Lead Engineer (Supervisor) to the employee associations grid.
			Call this from a scheduled workflow.
*/
BEGIN
SET NOCOUNT ON

--	Insert Project Manager from Owning Client if not in the grid
	Insert Into EMClientAssoc
	(ClientID, Employee, Relationship, CreateUser, CreateDate)
	Select distinct p.ClientID, p.ProjMgr, '03', 'CCG', GETDATE()
	From PR p,
		(Select ClientID, max (CreateDate) CreateDate
		 From PR
		 Where CreateDate is not null
		   and ProjMgr is not null
		 Group by ClientID) s
	Where p.ClientID = s.ClientID
	  and p.CreateDate = s.CreateDate
	  and p.ProjMgr is not null
	  and not exists (Select * From EMClientAssoc Where ClientID = p.ClientID and Employee = p.ProjMgr)
	  and exists (Select * From EM Where Employee = p.ProjMgr)

--	Insert Project Manager from Billing Client if not in the grid
	Insert Into EMClientAssoc
	(ClientID, Employee, Relationship, CreateUser, CreateDate)
	Select distinct p.BillingClientID, p.ProjMgr, '03', 'CCG', GETDATE()
	From PR p,
		(Select BillingClientID, max (CreateDate) CreateDate
		 From PR
		 Where CreateDate is not null
		   and ProjMgr is not null
		 Group by BillingClientID) s
	Where p.BillingClientID = s.BillingClientID
	  and p.CreateDate = s.CreateDate
	  and p.ProjMgr is not null
	  and not exists (Select * From EMClientAssoc Where ClientID = p.BillingClientID and Employee = p.ProjMgr)
	  and exists (Select * From EM Where Employee = p.ProjMgr)

--  Insert the Lead Engineer (Supervisor) from Owning Client if not in the grid
	Insert Into EMClientAssoc
	(ClientID, Employee, Relationship, CreateUser, CreateDate)
	Select distinct p.ClientID, p.Supervisor, '04', 'CCG', GETDATE()
	From PR p,
		(Select ClientID, max (CreateDate) CreateDate
		 From PR
		 Where CreateDate is not null
		   and Supervisor is not null
		 Group by ClientID) s
	Where p.ClientID = s.ClientID
	  and p.CreateDate = s.CreateDate
	  and p.Supervisor is not null
	  and not exists (Select * From EMClientAssoc Where ClientID = p.ClientID and Employee = p.Supervisor)
	  and exists (Select * From EM Where Employee = p.Supervisor)

--  Insert the Lead Engineer (Supervisor) from Billing Client if not in the grid
	Insert Into EMClientAssoc
	(ClientID, Employee, Relationship, CreateUser, CreateDate)
	Select distinct p.BillingClientID, p.Supervisor, '04', 'CCG', GETDATE()
	From PR p,
		(Select BillingClientID, max (CreateDate) CreateDate
		 From PR
		 Where CreateDate is not null
		   and Supervisor is not null
		 Group by BillingClientID) s
	Where p.BillingClientID = s.BillingClientID
	  and p.CreateDate = s.CreateDate
	  and p.Supervisor is not null
	  and not exists (Select * From EMClientAssoc Where ClientID = p.BillingClientID and Employee = p.Supervisor)
	  and exists (Select * From EM Where Employee = p.Supervisor)

END
GO
