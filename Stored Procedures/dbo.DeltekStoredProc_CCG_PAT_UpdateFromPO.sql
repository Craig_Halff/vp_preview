SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_UpdateFromPO] ( @PO_UDIC_UID varchar(50), @UpdatePOStatus varchar(50)= null, @Employee varchar(50)= null)
             AS EXEC spCCG_PAT_UpdateFromPO @PO_UDIC_UID,@UpdatePOStatus,@Employee
GO
