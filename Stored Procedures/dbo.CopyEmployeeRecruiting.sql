SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CopyEmployeeRecruiting]
  @strEmployeeID varchar(20)
AS

DECLARE @SQLstr varchar(1000)

BEGIN -- Procedure CopyEmployeeRecruiting

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
--> This stored procedure copies data from EmployeeCustomTabFields to Employees_RecruitingActivitySummary >
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    
  set @SQLstr = 
   'INSERT Employees_RecruitingActivitySummary(Employee, Seq, custInterviewDate, custJobDescription, custInterviewType, custInterviewer, custProposedSalary) ' + 
   'SELECT Employee, REPLACE(CAST(NEWID() AS VARCHAR(36)), ''-'', ''''), custUpcomingInterviewDate, custJobDescription, custUpcomingInterviewType, custUpcomingInterviewer, 0 ' +
   'FROM EmployeeCustomTabFields WHERE Employee = ' +  @strEmployeeID

  EXEC(@SQLstr)

  SET NOCOUNT OFF

END -- CopyEmployeeRecruiting
GO
