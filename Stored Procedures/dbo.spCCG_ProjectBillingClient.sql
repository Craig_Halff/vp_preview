SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectBillingClient] (@WBS1 varchar(32), @OpportunityID varchar (32))
AS 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
/* 
Copyright (c) 2017 Central Consulting Group.  All rights reserved.
03/02/2017 David Springer
           Get Billing Company & Billing Contact from Opportunity.
08/15/2019 David Springer
           Added Per Diem Reimbursed
*/
BEGIN

   Update p
   Set p.BillingClientID = o.custBillingCompany,
       p.BillingContactID = o.custBillingContact
   From PR p, OpportunityCustomTabFields o
   Where p.WBS1 = @WBS1
     and o.OpportunityID = @OpportunityID
     and o.custBillingCompany is not null

-- Billing Address
   Update p
   Set p.CLBillingAddr = c.CLAddress
   From PR p, OpportunityCustomTabFields o, Contacts c
   Where p.WBS1 = @WBS1
     and o.OpportunityID = @OpportunityID
     and o.custBillingContact = c.ContactID
     and not exists (Select * From CLAddress 
                     Where ClientID = p.BillingClientID
                       and Address = p.CLBillingAddr)
					   
	Update px
	Set px.CustPerDiemReimbursed = cx.CustPerDiemReimbursed
	From PR p, ProjectCustomTabFields px, ClientCustomTabFields cx
	Where p.WBS1 = @WBS1
	  and p.WBS2 = ' '
	  and p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2
	  and p.BillingClientID = cx.ClientID
	  and cx.CustPerDiemReimbursed = 'Y'
END
GO
