SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectRelatedProjectFeeAdjustment] @WBS1 varchar(32)
AS
/*
Copyright (c) 2019 Central Consulting Group. All rights reserved.
08/15/2019	David Springer
			Update parent Project Fee  based on Original Fee values and Related Projects grid.
			Called in spCCG_ProjectRelatedProjectInsert & spCCG_ProjectRelatedProjectUpdate procedures.
			Call on Project CHANGE workflow when Original Fees have changed on .000 suffix
			Call on Project Related Projects DELETE workflow.
*/ 
BEGIN
SET NOCOUNT ON

If @WBS1 like '%.000'
	Begin
-- Update Custom Revenue Field
	Update p
	Set p.FeeDirLab = px.CustOriginalFeeDirLab - IsNull (ro.DirLab, 0),
		p.FeeDirExp = px.CustOriginalFeeDirExp - IsNull (ro.DirExp, 0),
		p.Fee = px.CustOriginalFeeDirLab - IsNull (ro.DirLab, 0) + px.CustOriginalFeeDirExp - IsNull (ro.DirExp, 0),
		p.ConsultFee = px.CustOriginalConsultFee - IsNull (ro.DirCons, 0),
		p.ReimbAllowExp = px.CustOriginalReimbAllowExp - IsNull (ro.ReimbExp, 0),
		p.ReimbAllowCons = px.CustOriginalReimbAllowCons - IsNull (ro.ReimbCons, 0),
		p.ReimbAllow = px.CustOriginalReimbAllowExp - IsNull (ro.ReimbExp, 0) + px.CustOriginalReimbAllowCons - IsNull (ro.ReimbCons, 0)
	From PR p, ProjectCustomTabFields px
		Left Join
		(Select WBS1, 
			Sum (CustRelatedProjectFeeDirLab) DirLab, 
			Sum (CustRelatedProjectFeeDirExp) DirExp, 
			Sum (CustRelatedProjectConsultFee) DirCons, 
			Sum (CustRelatedProjectReimbAllowExp) ReimbExp, 
			Sum (CustRelatedProjectReimbAllowCons) ReimbCons
		From Projects_RelatedProjects
		Where WBS2 = ' '  -- project level
		Group by WBS1) ro on px.WBS1 = ro.WBS1
	Where p.WBS1 = @WBS1
	  and p.WBS2 = ' '  -- project level
	  and p.WBS1 = px.WBS1
	  and p.WBS2 = px.WBS2

--  Update Original Revenue Total Field
	Update ProjectCustomTabFields
	Set CustOriginalFeeTotal = CustOriginalFeeDirLab + CustOriginalFeeDirExp + CustOriginalConsultFee + CustOriginalReimbAllowExp + CustOriginalReimbAllowCons
	Where WBS1 = @WBS1
	  and WBS2 = ' '
	End
END
GO
