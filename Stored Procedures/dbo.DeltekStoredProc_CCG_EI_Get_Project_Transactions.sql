SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Get_Project_Transactions]
	@wbs1	Nvarchar(32)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT Convert(varchar(20), ISNULL(ThruDate, d.AccountPdEnd), 101) as ThruDate
		FROM CCG_EI_ConfigInvoiceGroups ig
			INNER JOIN ProjectCustomTabFields c
				on c.WBS1 = @wbs1 and c.WBS2 = ' ' and c.WBS3 = ' ' and c.custInvoiceGroup = ig.InvoiceGroup
			LEFT JOIN CFGDates d
				on d.Period = ig.ThruPeriod;
END;
GO
