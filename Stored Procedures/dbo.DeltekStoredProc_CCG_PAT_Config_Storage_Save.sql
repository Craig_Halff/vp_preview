SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_Storage_Save]
	@fieldsSQL			Nvarchar(max),
	@updateValuesSql	Nvarchar(max),
	@insertValuesSql	Nvarchar(max),
	@deleteSeqs			Nvarchar(max)
AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_Config_Storage_Save] 'asdasd', '', '', 'aa' --failure test
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	int;
	DECLARE @ids		TABLE (id int);
	DECLARE @i			int;
	DECLARE @newI		int;
	DECLARE @seq		Nvarchar(50);

	-- Ensure that our inputs are safe (preventing SQL injection)
	SET @safeSql = dbo.fnCCG_PAT_RegexMatch('<CSV>', @fieldsSQL);
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch('<INSERT VALUES>|', @updateValuesSql) * 2;
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch('<INSERT VALUES>|', @insertValuesSql) * 4;
	SET @safeSql = @safeSql | dbo.fnCCG_PAT_RegexMatch('<CSV>', @deleteSeqs) * 8;
	IF @safeSql < 15 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS Nvarchar(4));
		RETURN;
	END;

	BEGIN TRANSACTION;
	BEGIN TRY
		-- Updates
		IF ISNULL(@updateValuesSql, '') <> '' BEGIN
			SET @sSQL = N'
				UPDATE cs
					SET
						[Description]	= temp.[Description],
						[UseType]		= temp.[UseType],
						[StorageType]	= temp.[StorageType],
						[Server]		= temp.[Server],
						[Catalog]		= temp.[Catalog],
						[Root]			= temp.[Root],
						[UserName]		= temp.[UserName],
						[Password]		= temp.[Password],
						[RevisionType]	= temp.[RevisionType],
						[MarkupType]	= temp.[MarkupType],
						[Status]		= temp.[Status],
						[Detail]		= temp.[Detail],
						[AllowDelete]	= temp.[AllowDelete],
						[DateChanged]	= temp.[DateChanged],
						[ChangedBy]		= temp.[ChangedBy]
					FROM CCG_PAT_ConfigStorage cs
					JOIN (
						VALUES ' + @updateValuesSql + '
					) temp (' + @fieldsSQL + ', Seq) ON temp.Seq = cs.Seq';
			--PRINT @sSQL;
			EXEC (@sSQL);
		END;

		-- Inserts
		IF ISNULL(@insertValuesSql, '') <> '' BEGIN
			SET @sSQL = N'
				INSERT INTO CCG_PAT_ConfigStorage
					(' + @fieldsSQL + ')
					VALUES
					' + @insertValuesSql;
			--PRINT @sSQL;
			EXEC (@sSQL);
		END;

		-- Deletes
		IF ISNULL(@deleteSeqs, '') <> '' BEGIN
			SET @sSQL = N'
				DELETE FROM CCG_PAT_ConfigStorage WHERE Seq in (' + @deleteSeqs + ')';
			--PRINT @sSQL;
			EXEC (@sSQL);
		END;
	END TRY
	BEGIN CATCH
		SELECT
			ERROR_NUMBER() AS ErrorNumber
			,ERROR_SEVERITY() AS ErrorSeverity
			,ERROR_STATE() AS ErrorState
			,ERROR_PROCEDURE() AS ErrorProcedure
			,ERROR_LINE() AS ErrorLine
			,ERROR_MESSAGE() AS ErrorMessage;

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;
	END CATCH;

	IF @@TRANCOUNT > 0 COMMIT TRANSACTION;
END;
GO
