SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPUpdateVersionID]
  @strPlanID varchar(32),
  @bitCalledFromRM bit = 0,
  @bitUpdatePlanModified bit = 1
AS

BEGIN -- Procedure stRPUpdateVersionID

  DECLARE @strVersionID varchar(32) = REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')
  DECLARE @strUserName nvarchar(42)

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to update VersonID
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON
  IF @bitUpdatePlanModified = 1
	  BEGIN
		  UPDATE PNPlan SET 
			VersionID = @strVersionID,
			ModDate = GETUTCDATE(),
			ModUser = CASE WHEN DATALENGTH(@strUserName) = 0 THEN N'UNKNOWN' ELSE  @strUserName END  
			WHERE PlanID = @strPlanID
	  END
	  ELSE
		BEGIN
		  UPDATE PNPlan SET 
			VersionID = @strVersionID
			WHERE PlanID = @strPlanID
	  END

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  IF (@bitCalledFromRM = 1)
    BEGIN
	  IF @bitUpdatePlanModified = 1
	  BEGIN
		  UPDATE RPPlan SET 
			VersionID = @strVersionID,
			ModDate = GETUTCDATE(),
			ModUser =  CASE WHEN DATALENGTH(@strUserName) = 0 THEN N'UNKNOWN' ELSE  @strUserName END  
			WHERE PlanID = @strPlanID
	  END
	  ELSE
		BEGIN
		  UPDATE RPPlan SET 
			VersionID = @strVersionID
			WHERE PlanID = @strPlanID
	  END
  END /* END IF (@bitCalledFromRM = 1) */

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  SET NOCOUNT OFF

END -- stRPUpdateVersionID
GO
