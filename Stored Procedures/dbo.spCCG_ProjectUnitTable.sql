SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_ProjectUnitTable] @WBS1 varchar(32), @UnitTableAllLevels varchar (1), @UnitTable varchar (255)
AS
/*
Copyright (c) 2018 Central Consulting Group. All rights reserved.
02/05/2018 David Springer
           Manage Unit Table via Unit Table All Levels checkbox.
           Call from Project CHANGE workflow when Unit Table All Levels has changed.
05/10/2018 David Springer
           Update project when single WBS2 has a unit table set
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
BEGIN
	If @UnitTableAllLevels = 'Y'
		Begin
		Update PR
		Set UnitTable = @UnitTable
		Where WBS1 = @WBS1

		Update ProjectCustomTabFields
		Set CustUnitTableAllLevels = @UnitTableAllLevels
		Where WBS1 = @WBS1

		End

	If @UnitTableAllLevels = 'N'
		Begin
		Update PR
		Set UnitTable = null
		Where WBS1 = @WBS1
		  and Sublevel = 'Y'

		Update ProjectCustomTabFields
		Set CustUnitTableAllLevels = @UnitTableAllLevels
		Where WBS1 = @WBS1

	--  Check if this is the only unit table and update project to match
		Update p
		Set p.UnitTable = c.UnitTable
		From PR p,
			(Select WBS1, Max (UnitTable) UnitTable
			From PR
			Where Sublevel = 'N'
			Group by WBS1
			Having Count (distinct UnitTable) = 1) c
		Where p.WBS1 = @WBS1
		  and p.WBS1 = c.WBS1

		Update p
		Set p.CustUnitTableAllLevels = 'Y'
		From ProjectCustomTabFields p,
			(Select WBS1
			From PR
			Where Sublevel = 'N'
			Group by WBS1
			Having Count (distinct UnitTable) = 1) c
		Where p.WBS1 = @WBS1
		  and p.WBS1 = c.WBS1
		End
END
GO
