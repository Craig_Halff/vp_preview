SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPSyncWBSStructure]
  @strPlanID varchar(32)
AS

BEGIN -- Procedure stRPSyncWBSStructure

  SET NOCOUNT ON

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @strCompany nvarchar(14)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strWBS1 nvarchar(30)
  DECLARE @strUserName nvarchar(42) = N'SyncWBSStructure'
  DECLARE @strExpTab varchar(1)
  DECLARE @strConTab varchar(1)

  DECLARE @dtToday datetime 
  DECLARE @dtTomorrow datetime
  DECLARE @dtMinDate datetime
  DECLARE @dtMaxDate datetime

  DECLARE @siWBS2Length smallint
  DECLARE @siWBS3Length smallint

  DECLARE @tiExpWBSLevel tinyint
  DECLARE @tiConWBSLevel tinyint

  DECLARE @strTaskID varchar (32)
  DECLARE @RowID varchar (40)
  
  DECLARE @tabPR TABLE(
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    Name nvarchar(255) COLLATE database_default,
    ChargeType varchar(1) COLLATE database_default,
    ProjectType nvarchar(10) COLLATE database_default,
    Org nvarchar(30) COLLATE database_default,
    ClientID varchar(32) COLLATE database_default,
    ProjMgr nvarchar(20) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    Status varchar(1) COLLATE database_default,
    SubLevel varchar(1) COLLATE database_default,
    WBS2Count int,
    WBS3Count int
    UNIQUE (WBS1, WBS2, WBS3, Status)
  )

  DECLARE @tabExpPR TABLE 
    (WBS1 nvarchar(30) COLLATE database_default,
     WBS2 nvarchar(30) COLLATE database_default,
     WBS3 nvarchar(30) COLLATE database_default,
     WBSLevel tinyint,
     IsLeaf bit
     UNIQUE(WBS1, WBS2, WBS3, WBSLevel, IsLeaf)
    )

  DECLARE @tabConPR TABLE 
    (WBS1 nvarchar(30) COLLATE database_default,
     WBS2 nvarchar(30) COLLATE database_default,
     WBS3 nvarchar(30) COLLATE database_default,
     WBSLevel tinyint,
     IsLeaf bit
     UNIQUE(WBS1, WBS2, WBS3, WBSLevel, IsLeaf)
    )

  DECLARE @tabWBS TABLE(
    RowID int IDENTITY(1,1),
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    NewTaskID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    Name nvarchar(255) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChildrenCount int,
    OutlineLevel int,
    ChargeType varchar(1) COLLATE database_default,
    ProjectType nvarchar(10) COLLATE database_default,
    WBSType varchar(4) COLLATE database_default,
    Org nvarchar(30) COLLATE database_default,
    ClientID varchar(32) COLLATE database_default,
    ProjMgr nvarchar(20) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    Status varchar(1) COLLATE database_default
    UNIQUE (RowID, WBS1, WBS2, WBS3, ParentOutlineNumber, OutlineNumber, Status)
  )

  DECLARE @tabTask TABLE(
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChildrenCount int,
    OutlineLevel int,
    WBSType varchar(4) COLLATE database_default,
    StartDate datetime,
    EndDate datetime
    UNIQUE (PlanID, TaskID, WBS1, WBS2, WBS3, ParentOutlineNumber, OutlineNumber)
  )

  DECLARE @tabDeleteTask TABLE(
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default
    UNIQUE (PlanID, TaskID)
  )

  DECLARE @tabNonLeafAssignment TABLE(
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default
    UNIQUE (PlanID, TaskID, AssignmentID)
  )

  DECLARE @tabNonLeafExpense TABLE(
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    ExpenseID varchar(32) COLLATE database_default
    UNIQUE (PlanID, TaskID, ExpenseID)
  )

  DECLARE @tabNonLeafConsultant TABLE(
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    ConsultantID varchar(32) COLLATE database_default
    UNIQUE (PlanID, TaskID, ConsultantID)
  )

  DECLARE @tabLaborCodeTask TABLE(
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    UNIQUE (PlanID, TaskID, WBS1, WBS2, WBS3)
  )
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get Company and WBS1 from Plan

  SELECT 
    @strCompany = P.Company,
    @strWBS1 = P.WBS1,
    @tiExpWBSLevel = P.ExpWBSLevel,
    @tiConWBSLevel = P.ConWBSLevel
    FROM PNPlan AS P
    WHERE P.PlanID = @strPlanID

  -- Get WBS2Length and WBS3Length.

  SELECT
    @siWBS2Length = WBS2Length,
    @siWBS3Length = WBS3Length
    FROM CFGFormat

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, and RABIBC flags.
  
  SELECT
    @strMultiCompanyEnabled = MultiCompanyEnabled
    FROM FW_CFGSystem

  -- Get Plan Settings from CFGResourcePlanning

  SELECT 
    @strExpTab = ExpTab,
    @strConTab = ConTab,
    @tiExpWBSLevel = ExpWBSLevel,
    @tiConWBSLevel = ConWBSLevel
    FROM CFGResourcePlanning WHERE Company = CASE WHEN @strMultiCompanyEnabled = 'Y' THEN @strCompany ELSE ' ' END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get the current date in current local and not the UTC date.
  
  SET @dtToday = DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0)
  SET @dtTomorrow = DATEADD(dd, 1, @dtToday)  

  -- Compute Project's Min/Max Dates to be used later for Start/End Dates.

  SELECT 
    @dtMinDate = COALESCE(StartDate, EstCompletionDate, @dtTomorrow),
    @dtMaxDate = CASE WHEN (EstCompletionDate IS NOT NULL AND EstCompletionDate >= @dtMinDate) THEN EstCompletionDate ELSE @dtMinDate END
    FROM PR 
    WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save PR rows here to be used in subsequent steps for performance optimization.

  INSERT @tabPR(
    WBS1,
    WBS2,
    WBS3,
    Name,
    ChargeType,
    ProjectType,
    Org,
    ClientID,
    ProjMgr,
    StartDate,
    EndDate,
    Status,
    SubLevel,
    WBS2Count,
    WBS3Count
  )
    SELECT
      PR.WBS1 AS WBS1,
      PR.WBS2 AS WBS2,
      PR.WBS3 AS WBS3,
      PR.Name AS Name,
      PR.ChargeType AS ChargeType,
      PR.ProjectType AS ProjectType,
      PR.Org AS Org,
      PR.ClientID AS ClientID,
      PR.ProjMgr AS ProjMgr,
      PR.StartDate AS StartDate,
      PR.EndDate  AS EndDate,
      PR.Status AS Status,
      PR.SubLevel AS SubLevel,
      CASE
        WHEN WBS2 = ' ' AND WBS3 = ' ' 
        THEN (COUNT(*) OVER (PARTITION BY WBS1 ORDER BY WBS3 DESC, WBS2, WBS1 ROWS BETWEEN 1 FOLLOWING AND UNBOUNDED FOLLOWING))
        ELSE 0
      END AS WBS2Count,
      CASE
        WHEN WBS2 <> ' ' AND WBS3 = ' '
        THEN (COUNT(*) OVER (PARTITION BY WBS1, WBS2 ORDER BY WBS1, WBS2, WBS3 ROWS BETWEEN 1 FOLLOWING AND UNBOUNDED FOLLOWING))
        ELSE 0
      END AS WBS3Count
      FROM PR
      WHERE PR.WBS1 = @strWBS1

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Save PNTask rows here to be used in subsequent steps for performance optimization.

  INSERT @tabTask(
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    ParentOutlineNumber,
    OutlineNumber,
    ChildrenCount,
    OutlineLevel,
    WBSType,
    StartDate,
    EndDate
  )
    SELECT
      T.PlanID AS PlanID,
      T.TaskID AS TaskID,
      T.WBS1 AS WBS1,
      ISNULL(T.WBS2, ' ') AS WBS2,
      ISNULL(T.WBS3, ' ') AS WBS3,
      T.ParentOutlineNumber AS ParentOutlineNumber,
      T.OutlineNumber AS OutlineNumber,
      T.ChildrenCount AS ChildrenCount,
      T.OutlineLevel AS OutlineLevel,
      T.WBSType AS WBSType,
      T.StartDate AS StartDate,
      T.EndDate AS EndDate
      FROM PNTask AS T
      WHERE T.PlanID = @strPlanID AND T.WBSType <> 'LBCD'

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  -- WBS1 Row. Combining PR and PNTask data.
  INSERT @tabWBS(
    PlanID,
    TaskID,
    NewTaskID,
    WBS1,
    WBS2,
    WBS3,
    Name,
    ParentOutlineNumber,
    OutlineNumber,
    ChildrenCount,
    OutlineLevel,
    ChargeType,
    ProjectType,
    WBSType,
    Org,
    ClientID,
    ProjMgr,
    StartDate,
    EndDate,
    Status
  )
    SELECT
      @strPlanID AS PlanID,
      X.TaskID AS TaskID,
      X.NewTaskID AS NewTaskID,
      X.WBS1 AS WBS1,
      X.WBS2 AS WBS2,
      X.WBS3 AS WBS3,
      X.Name,
      NULL AS ParentOutlineNumber,
      '001' AS OutlineNumber,
      X.WBS2Count AS ChildrenCount,
      0 AS OutlineLevel,
      X.ChargeType AS ChargeType,
      X.ProjectType AS ProjectType,
      'WBS1' AS WBSType,
      X.Org,
      X.ClientID,
      X.ProjMgr,
      X.StartDate,
      X.EndDate,
      X.Status
      FROM ( /* X */
        SELECT
          XZ.TaskID AS TaskID,
          XZ.NewTaskID AS NewTaskID,
          XZ.WBS1 AS WBS1,
          XZ.WBS2 AS WBS2,
          XZ.WBS3 AS WBS3,
          XZ.Name AS Name,
          XZ.ChargeType AS ChargeType,
          XZ.ProjectType AS ProjectType,
          XZ.Org AS Org,
          XZ.ClientID AS ClientID,
          XZ.ProjMgr AS ProjMgr,
          XZ.StartDate AS StartDate,
          XZ.EndDate AS EndDate,
          XZ.Status AS Status,
          XZ.WBS2Count AS WBS2Count,
          XZ.MIN_T_WBS1 AS MIN_T_WBS1,
          ROW_NUMBER() OVER (PARTITION BY XZ.WBS1 ORDER BY XZ.WBS1) AS RowID
          FROM ( /* XZ */
            SELECT
              T.TaskID AS TaskID,
              CASE
                WHEN T.TaskID IS NULL
                THEN REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')
                ELSE NULL 
              END AS NewTaskID,
              PR.WBS1 AS WBS1,
              PR.WBS2 AS WBS2,
              PR.WBS3 AS WBS3,
              PR.Name AS Name,
              PR.ChargeType AS ChargeType,
              PR.ProjectType AS ProjectType,
              PR.Org AS Org,
              PR.ClientID AS ClientID,
              PR.ProjMgr AS ProjMgr,
              COALESCE(T.StartDate, @dtMinDate) AS StartDate,
              COALESCE(T.EndDate, @dtMaxDate)  AS EndDate,
              PR.Status AS Status,
              PR.WBS2Count AS WBS2Count,
              /* There is a possibility of existence of multiple rows in PNTask table (e.g. a result of bug)       */
              /* that are mapped to the same WBS1|WBS2|WBS3 branch. If so, select the row with MIN(PNTask.TaskID). */
              MIN(T.TaskID) OVER (PARTITION BY T.WBS1 ORDER BY PR.WBS1, PR.WBS2, PR.WBS3) AS MIN_T_WBS1
              FROM @tabPR AS PR
                LEFT JOIN @tabTask AS T ON PR.WBS1 = T.WBS1 AND PR.WBS2 = T.WBS2 AND PR.WBS3 = T.WBS3
              WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' '
          ) AS XZ
          WHERE ((XZ.TaskID IS NOT NULL AND XZ.TaskID = XZ.MIN_T_WBS1) OR XZ.TaskID IS NULL)
      ) AS X

  -- WBS2 Rows.

  IF(@siWBS2Length > 0)
    BEGIN

      INSERT @tabWBS(
        PlanID,
        TaskID,
        NewTaskID,
        WBS1,
        WBS2,
        WBS3,
        Name,
        ParentOutlineNumber,
        OutlineNumber,
        ChildrenCount,
        OutlineLevel,
        ChargeType,
        ProjectType,
        WBSType,
        Org,
        ClientID,
        ProjMgr,
        StartDate,
        EndDate,
        Status
      )
        SELECT
          @strPlanID AS PlanID,
          X.TaskID AS TaskID,
          X.NewTaskID AS NewTaskID,
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.Name,
          '001' AS ParentOutlineNumber,
          '001' + '.' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(RowID, 36)), 3) AS OutlineNumber,
          X.WBS3Count AS ChildrenCount,
          1 AS OutlineLevel,
          X.ChargeType AS ChargeType,
          X.ProjectType AS ProjectType,
          'WBS2' AS WBSType,
          X.Org,
          X.ClientID,
          X.ProjMgr,
          X.StartDate,
          X.EndDate,
          X.Status
          FROM ( /* X */
            SELECT
              XZ.TaskID AS TaskID,
              XZ.NewTaskID AS NewTaskID,
              XZ.WBS1 AS WBS1,
              XZ.WBS2 AS WBS2,
              XZ.WBS3 AS WBS3,
              XZ.Name AS Name,
              XZ.ChargeType AS ChargeType,
              XZ.ProjectType AS ProjectType,
              XZ.Org AS Org,
              XZ.ClientID AS ClientID,
              XZ.ProjMgr AS ProjMgr,
              XZ.StartDate AS StartDate,
              XZ.EndDate AS EndDate,
              XZ.Status AS Status,
              XZ.WBS3Count AS WBS3Count,
              XZ.MIN_T_WBS2 AS MIN_T_WBS2,
              ROW_NUMBER() OVER (PARTITION BY XZ.WBS1 ORDER BY XZ.WBS1, XZ.WBS2) AS RowID
              FROM ( /* XZ */
                SELECT
                  T.TaskID AS TaskID,
                  CASE
                    WHEN T.TaskID IS NULL
                    THEN REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')
                    ELSE NULL 
                  END AS NewTaskID,
                  PR.WBS1 AS WBS1,
                  PR.WBS2 AS WBS2,
                  PR.WBS3 AS WBS3,
                  PR.Name AS Name,
                  PR.ChargeType AS ChargeType,
                  PR.ProjectType AS ProjectType,
                  PR.Org AS Org,
                  PR.ClientID AS ClientID,
                  PR.ProjMgr AS ProjMgr,
                  COALESCE(T.StartDate, @dtMinDate) AS StartDate,
                  COALESCE(T.EndDate, @dtMaxDate)  AS EndDate,
                  PR.Status AS Status,
                  PR.WBS3Count AS WBS3Count,
                  /* There is a possibility of existence of multiple rows in PNTask table (e.g. a result of bug)       */
                  /* that are mapped to the same WBS1|WBS2|WBS3 branch. If so, select the row with MIN(PNTask.TaskID). */
                  MIN(T.TaskID) OVER (PARTITION BY PR.WBS1, T.WBS2 ORDER BY PR.WBS1, PR.WBS2, PR.WBS3) AS MIN_T_WBS2
                  FROM @tabPR AS PR
                    LEFT JOIN @tabTask AS T ON PR.WBS1 = T.WBS1 AND PR.WBS2 = T.WBS2 AND PR.WBS3 = T.WBS3
                  WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 <> ' ' AND PR.WBS3 = ' '
              ) AS XZ
              WHERE ((XZ.TaskID IS NOT NULL AND XZ.TaskID = XZ.MIN_T_WBS2) OR XZ.TaskID IS NULL)
          ) AS X

    END /* END IF(@siWBS2Length > 0) */

  -- WBS3 Rows.

  IF(@siWBS3Length > 0)
    BEGIN

      INSERT @tabWBS(
        PlanID,
        TaskID,
        NewTaskID,
        WBS1,
        WBS2,
        WBS3,
        Name,
        ParentOutlineNumber,
        OutlineNumber,
        ChildrenCount,
        OutlineLevel,
        ChargeType,
        ProjectType,
        WBSType,
        Org,
        ClientID,
        ProjMgr,
        StartDate,
        EndDate,
        Status
      )
        SELECT DISTINCT
          @strPlanID AS PlanID,
          X.TaskID AS TaskID,
          X.NewTaskID AS NewTaskID,
          X.WBS1 AS WBS1,
          X.WBS2 AS WBS2,
          X.WBS3 AS WBS3,
          X.Name,
          WX.OutlineNumber AS ParentOutlineNumber,
          WX.OutlineNumber + '.' + RIGHT('000' + CONVERT(VARCHAR(3), dbo.DLTK$DecToBase36(X.RowID, 36)), 3) AS OutlineNumber,
          0 AS ChildrenCount,
          WX.OutlineLevel + 1 AS OutlineLevel,
          X.ChargeType AS ChargeType,
          X.ProjectType AS ProjectType,
          'WBS3' AS WBSType,
          X.Org,
          X.ClientID,
          X.ProjMgr,
          X.StartDate,
          X.EndDate,
          X.Status
          FROM ( /* X */
            SELECT
              XZ.TaskID AS TaskID,
              XZ.NewTaskID AS NewTaskID,
              XZ.WBS1 AS WBS1,
              XZ.WBS2 AS WBS2,
              XZ.WBS3 AS WBS3,
              XZ.Name AS Name,
              XZ.ChargeType AS ChargeType,
              XZ.ProjectType AS ProjectType,
              XZ.Org AS Org,
              XZ.ClientID AS ClientID,
              XZ.ProjMgr AS ProjMgr,
              XZ.StartDate AS StartDate,
              XZ.EndDate AS EndDate,
              XZ.Status AS Status,
              XZ.MIN_T_WBS3 AS MIN_T_WBS3,
              ROW_NUMBER() OVER (PARTITION BY XZ.WBS2 ORDER BY XZ.WBS1, XZ.WBS2, XZ.WBS3) AS RowID
              FROM ( /* XZ */
                SELECT DISTINCT
                  T.TaskID AS TaskID,
                  CASE
                    WHEN T.TaskID IS NULL
                    THEN REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')
                    ELSE NULL 
                  END AS NewTaskID,
                  PR.WBS1 AS WBS1,
                  PR.WBS2 AS WBS2,
                  PR.WBS3 AS WBS3,
                  PR.Name AS Name,
                  PR.ChargeType AS ChargeType,
                  PR.ProjectType AS ProjectType,
                  PR.Org AS Org,
                  PR.ClientID AS ClientID,
                  PR.ProjMgr AS ProjMgr,
                  COALESCE(T.StartDate, @dtMinDate) AS StartDate,
                  COALESCE(T.EndDate, @dtMaxDate)  AS EndDate,
                  PR.Status AS Status,
                  /* There is a possibility of existence of multiple rows in PNTask table (e.g. a result of bug)       */
                  /* that are mapped to the same WBS1|WBS2|WBS3 branch. If so, select the row with MIN(PNTask.TaskID). */
                  MIN(T.TaskID) OVER (PARTITION BY PR.WBS2, T.WBS3 ORDER BY PR.WBS1, PR.WBS2, PR.WBS3) AS MIN_T_WBS3
                  FROM @tabPR AS PR
                    LEFT JOIN @tabTask AS T ON PR.WBS1 = T.WBS1 AND PR.WBS2 = T.WBS2 AND PR.WBS3 = T.WBS3
                  WHERE PR.WBS1 = @strWBS1 AND PR.WBS2 <> ' ' AND PR.WBS3 <> ' '
              ) AS XZ
              WHERE ((XZ.TaskID IS NOT NULL AND XZ.TaskID = XZ.MIN_T_WBS3) OR XZ.TaskID IS NULL)
          ) AS X
            INNER JOIN @tabWBS AS WX ON X.WBS1 = WX.WBS1 AND X.WBS2 = WX.WBS2 AND WX.WBS3 = ' '

    END /* END IF(@siWBS3Length > 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collect PNTask rows that are not mapped to any WBS rows in PR table for deletion latter.

  -- There is a possibility that duplicate PNTask rows were created for a combination of WBS1|WBS2|WBS3, 
  -- so we need to get rid of the extra RPTask rows as well.

  INSERT @tabDeleteTask(
    PlanID,
    TaskID
  )
    SELECT
      T.PlanID AS PlanID,
      T.TaskID AS TaskID
      FROM @tabTask AS T
        LEFT JOIN @tabWBS AS W
          ON T.WBS1 = W.WBS1 AND T.WBS2 = W.WBS2 AND T.WBS3 = W.WBS3 AND T.TaskID = W.TaskID
	  WHERE W.RowID IS NULL 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collect Assignment rows that are at non-leaf level so that they can be moved later.

  INSERT @tabNonLeafAssignment(
    PlanID,
    TaskID,
    AssignmentID
  )
    SELECT
      A.PlanID AS PlanID,
      A.TaskID AS TaskID,
      A.AssignmentID AS AssignmentID
    FROM PNAssignment AS A
      INNER JOIN @tabPR AS PR ON A.WBS1 = PR.WBS1 AND ISNULL(A.WBS2, ' ') = PR.WBS2 AND ISNULL(A.WBS3, ' ') = PR.WBS3
      WHERE A.PlanID = @strPlanID AND PR.SubLevel = 'Y'

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

 -- Collect Expense rows that are at non-leaf level so that they can be moved later.

  IF (@strExpTab = 'Y')
    BEGIN 

      -- Determine WBS level where Expense rows can be located.

      INSERT @tabExpPR(
        WBS1,
        WBS2,
        WBS3,
        WBSLevel,
        IsLeaf
      )
        SELECT
          WBS1,
          WBS2,
          WBS3,
          WBSLevel,
          CASE
            WHEN WBSLevel < @tiExpWBSLevel
            THEN
              CASE
                WHEN SubLevel = 'Y'
                THEN 0
                ELSE 1
              END
            ELSE 1
          END AS IsLeaf
          FROM
            (SELECT
               WBS1, WBS2, WBS3,
               CASE 
                 WHEN WBS2 = ' ' AND WBS3 = ' ' THEN 1
                 WHEN WBS2 != ' ' AND WBS3 = ' ' THEN 2
                 WHEN WBS2 != ' ' AND WBS3 != ' ' THEN 3
               END AS WBSLevel,
               SubLevel
               FROM @tabPR
            ) AS X
          WHERE WBSLevel <= @tiExpWBSLevel

      INSERT @tabNonLeafExpense(
        PlanID,
        TaskID,
        ExpenseID
      )
        SELECT
          E.PlanID AS PlanID,
          E.TaskID AS TaskID,
          E.ExpenseID AS ExpenseID
        FROM PNExpense AS E
          INNER JOIN @tabExpPR AS PR ON E.WBS1 = PR.WBS1 AND ISNULL(E.WBS2, ' ') = PR.WBS2 AND ISNULL(E.WBS3, ' ') = PR.WBS3
          WHERE E.PlanID = @strPlanID AND PR.IsLeaf = 0

    END /* END IF (@strExpTab = 'Y') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

 -- Collect Consultant rows that are at non-leaf level so that they can be moved later.

  IF (@strConTab = 'Y')
    BEGIN

      -- Determine WBS level where Consultant rows can be located.

      INSERT @tabConPR(
        WBS1,
        WBS2,
        WBS3,
        WBSLevel,
        IsLeaf
      )
        SELECT
          WBS1,
          WBS2,
          WBS3,
          WBSLevel,
          CASE
            WHEN WBSLevel < @tiExpWBSLevel
            THEN
              CASE
                WHEN SubLevel = 'Y'
                THEN 0
                ELSE 1
              END
            ELSE 1
          END AS IsLeaf
          FROM
            (SELECT
               WBS1, WBS2, WBS3,
               CASE 
                 WHEN WBS2 = ' ' AND WBS3 = ' ' THEN 1
                 WHEN WBS2 != ' ' AND WBS3 = ' ' THEN 2
                 WHEN WBS2 != ' ' AND WBS3 != ' ' THEN 3
               END AS WBSLevel,
               SubLevel
               FROM @tabPR
            ) AS X
          WHERE WBSLevel <= @tiConWBSLevel

      INSERT @tabNonLeafConsultant(
        PlanID,
        TaskID,
        ConsultantID
      )
        SELECT
          C.PlanID AS PlanID,
          C.TaskID AS TaskID,
          C.ConsultantID AS ConsultantID
        FROM PNConsultant AS C
          INNER JOIN @tabConPR AS PR ON C.WBS1 = PR.WBS1 AND ISNULL(C.WBS2, ' ') = PR.WBS2 AND ISNULL(C.WBS3, ' ') = PR.WBS3
          WHERE C.PlanID = @strPlanID AND PR.IsLeaf = 0

    END /* END IF (@strConTab = 'Y') */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  --Before we change anything here, we need set the outline number correctly for Task and Laborcode
  --update  parentOutline Number for all tasks
	
	   UPDATE PNTask SET ParentOutlineNumber = P.OutlineNumber FROM
       PNTASK INNER JOIN PNTask P ON P.PlanID = PNTask.PlanID AND P.WBS1 = PNTask.WBS1 AND P.WBSType = 'WBS1'
       WHERE PNTask.PlanID = @strPlanID AND PNTask.WBSType = 'WBS2'

       UPDATE RPTask SET ParentOutlineNumber = P.OutlineNumber FROM
       RPTask INNER JOIN RPTask P ON P.PlanID = RPTask.PlanID AND P.WBS1 = RPTask.WBS1 AND P.WBSType = 'WBS1'
       WHERE RPTask.PlanID = @strPlanID AND RPTask.WBSType = 'WBS2'

       UPDATE PNTask SET ParentOutlineNumber = P.OutlineNumber FROM
       PNTASK INNER JOIN PNTask P ON P.PlanID = PNTask.PlanID AND P.WBS1 = PNTask.WBS1 AND P.WBS2 = PNTask.WBS2 AND P.WBSType = 'WBS2'
       WHERE PNTask.PlanID = @strPlanID AND PNTask.WBSType = 'WBS3'

       UPDATE RPTask SET ParentOutlineNumber = P.OutlineNumber FROM
       RPTask INNER JOIN RPTask P ON P.PlanID = RPTask.PlanID AND P.WBS1 = RPTask.WBS1 AND P.WBS2 = RPTask.WBS2 AND P.WBSType = 'WBS2'
       WHERE RPTask.PlanID = @strPlanID AND RPTask.WBSType = 'WBS3'

       --reorder the outline number
       EXEC dbo.stRPReorderOutlineNumber @strPlanID

       --update  parentOutline Number for all labor codes UNDER WBS1
       UPDATE PNTask SET ParentOutlineNumber = P.OutlineNumber  FROM
       PNTASK INNER JOIN PNTask P
       ON P.PlanID = PNTask.PlanID 
       AND P.WBS1 = PNTask.WBS1 
       AND P.WBS2 = PNTask.WBS2 
       AND  P.WBS3 = PNTask.WBS3
       AND P.WBSType<>'LBCD'
       WHERE PNTask.PlanID = @strPlanID AND PNTask.WBSType = 'LBCD'

       UPDATE RPTask SET ParentOutlineNumber = P.OutlineNumber  FROM
       RPTask INNER JOIN RPTask P
       ON P.PlanID = RPTask.PlanID 
       AND P.WBS1 = RPTask.WBS1 
       AND P.WBS2 = RPTask.WBS2 
       AND P.WBS3 = RPTask.WBS3
       AND P.WBSType<>'LBCD'
       WHERE RPTask.PlanID = @strPlanID AND RPTask.WBSType = 'LBCD'

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Inserting PNTask and RPTask rows which did not previously exist
    -- but there are corresponding rows in PR table.

    INSERT PNTask (
      PlanID,
      TaskID,
      WBS1,
      WBS2,
      WBS3,
      Name,
      ParentOutlineNumber,
      OutlineNumber,
      ChildrenCount,
      OutlineLevel,
      ChargeType,
      ProjectType,
      WBSType,

      Org,
      ClientID,
      ProjMgr,
      StartDate,
      EndDate,
      Status,
 
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
      X.PlanID AS PlanID,
      X.NewTaskID AS TaskID,
      X.WBS1 AS WBS1,
      CASE WHEN X.WBS2 = ' ' THEN NULL ELSE X.WBS2 END AS WBS2,
      CASE WHEN X.WBS3 = ' ' THEN NULL ELSE X.WBS3 END AS WBS3,
      X.Name AS Name,
      X.ParentOutlineNumber AS ParentOutlineNumber,
      X.OutlineNumber AS OutlineNumber,
      X.ChildrenCount AS ChildrenCount,
      X.OutlineLevel AS OutlineLevel,
      X.ChargeType AS ChargeType,
      X.ProjectType AS ProjectType,
      X.WBSType AS WBSType,
      X.Org AS Org,
      X.ClientID AS ClientID,
      X.ProjMgr AS ProjMgr,
      X.StartDate AS StartDate,
      X.EndDate AS EndDate,
      X.Status AS Status,
      @strUserName AS CreateUser,
      CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
      @strUserName AS ModUser,
      CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate
      FROM @tabWBS AS X
      WHERE X.TaskID IS NULL

    INSERT RPTask (
      PlanID,
      TaskID,
      WBS1,
      WBS2,
      WBS3,
      Name,
      ParentOutlineNumber,
      OutlineNumber,
      ChildrenCount,
      OutlineLevel,
      ChargeType,
      ProjectType,
      WBSType,

      Org,
      ClientID,
      ProjMgr,
      StartDate,
      EndDate,
      Status,
 
      CreateUser,
      CreateDate,
      ModUser,
      ModDate
    )
      SELECT
      X.PlanID AS PlanID,
      X.NewTaskID AS TaskID,
      X.WBS1 AS WBS1,
      CASE WHEN X.WBS2 = ' ' THEN NULL ELSE X.WBS2 END AS WBS2,
      CASE WHEN X.WBS3 = ' ' THEN NULL ELSE X.WBS3 END AS WBS3,
      X.Name AS Name,
      X.ParentOutlineNumber AS ParentOutlineNumber,
      X.OutlineNumber AS OutlineNumber,
      X.ChildrenCount AS ChildrenCount,
      X.OutlineLevel AS OutlineLevel,
      X.ChargeType AS ChargeType,
      X.ProjectType AS ProjectType,
      X.WBSType AS WBSType,
      X.Org AS Org,
      X.ClientID AS ClientID,
      X.ProjMgr AS ProjMgr,
      X.StartDate AS StartDate,
      X.EndDate AS EndDate,
      X.Status AS Status,
      @strUserName AS CreateUser,
      CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate,
      @strUserName AS ModUser,
      CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDate
      FROM @tabWBS AS X
      WHERE X.TaskID IS NULL
  
  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Update existing PNTask and RPTask rows with PR data and new OutlineNumber.

    UPDATE T SET
      Name = W.Name,
      ParentOutlineNumber = W.ParentOutlineNumber,
      OutlineNumber = W.OutlineNumber,
      ChildrenCount = W.ChildrenCount,
      OutlineLevel = W.OutlineLevel,
      ChargeType = W.ChargeType,
      ProjectType = W.ProjectType,
      WBSType = W.WBSType,
      Org = W.Org,
      ClientID = W.ClientID,
      ProjMgr = W.ProjMgr,
      Status = W.Status,
      ModUser = @strUserName,
      ModDate = CONVERT(VARCHAR, GETUTCDATE(), 121)
      FROM PNTask AS T
        INNER JOIN @tabWBS AS W ON T.PlanID = W.PlanID AND T.TaskID = W.TaskID
      WHERE T.PlanID = @strPlanID AND W.TaskID IS NOT NULL

    UPDATE T SET
      Name = W.Name,
      ParentOutlineNumber = W.ParentOutlineNumber,
      OutlineNumber = W.OutlineNumber,
      ChildrenCount = W.ChildrenCount,
      OutlineLevel = W.OutlineLevel,
      ChargeType = W.ChargeType,
      ProjectType = W.ProjectType,
      WBSType = W.WBSType,
      Org = W.Org,
      ClientID = W.ClientID,
      ProjMgr = W.ProjMgr,
      Status = W.Status,
      ModUser = @strUserName,
      ModDate = CONVERT(VARCHAR, GETUTCDATE(), 121)
      FROM RPTask AS T
        INNER JOIN @tabWBS AS W ON T.PlanID = W.PlanID AND T.TaskID = W.TaskID
      WHERE T.PlanID = @strPlanID AND W.TaskID IS NOT NULL

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Delete PNTask and RPTask rows that are no longer mapped to PR table.

    IF EXISTS(SELECT 1 FROM @tabDeleteTask)
      BEGIN

        DECLARE @strDeleteTaskID varchar(32)

        DECLARE csr20170504_PNTask CURSOR LOCAL FAST_FORWARD FOR
          SELECT TaskID
            FROM @tabDeleteTask AS RP

        OPEN csr20170504_PNTask
        FETCH NEXT FROM csr20170504_PNTask INTO @strDeleteTaskID

        WHILE (@@FETCH_STATUS = 0)
          BEGIN

            EXECUTE dbo.stRPDelTask @strPlanID, @strDeleteTaskID
            FETCH NEXT FROM csr20170504_PNTask INTO @strDeleteTaskID

          END /* END WHILE (@@FETCH_STATUS = 0) */

        CLOSE csr20170504_PNTask
        DEALLOCATE csr20170504_PNTask

      END /* END IF EXISTS(SELECT 1 FROM @tabDeleteTask) */

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  -- After the labor code task under those deleted wbs tasks are removed, we can
  -- save PNTask labor code rows here to be used in moving them to the lowest level of WBS level

  INSERT @tabLaborCodeTask(
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3
  )
    SELECT
      T.PlanID AS PlanID,
      T.TaskID AS TaskID,
      T.WBS1 AS WBS1,
      T.WBS2  AS WBS2,
      T.WBS3 AS WBS3
      FROM PNTask AS T
      WHERE T.PlanID = @strPlanID AND T.WBSType = 'LBCD'

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    -- Move Non-Leaf Assignment down to first leaf WBS in the same branch.

    IF EXISTS(SELECT 1 FROM @tabNonLeafAssignment)
      BEGIN

        UPDATE A SET
          TaskID = XT.CT_TaskID,
          WBS1 = XT.CT_WBS1,
          WBS2 = XT.CT_WBS2,
          WBS3 = XT.CT_WBS3,
          ModUser = @strUserName,
          ModDate = CONVERT(VARCHAR, GETUTCDATE(), 121)
         FROM PNAssignment AS A
            INNER JOIN @tabNonLeafAssignment AS LA ON A.PlanID = LA.PlanID AND A.TaskID = LA.TaskID AND A.AssignmentID = LA.AssignmentID
            INNER JOIN (
              SELECT TOP 1
                PT.PlanID AS PlanID,
                PT.TaskID AS PT_TaskID,
                CT.TaskID AS CT_TaskID,
                CT.WBS1 AS CT_WBS1,
                CT.WBS2 AS CT_WBS2,
                CT.WBS3 AS CT_WBS3,
                ROW_NUMBER() OVER(PARTITION BY PT.PlanID, PT.TaskID, PT.OutlineNumber ORDER BY CT.OutlineNumber) AS RowNum
                FROM PNTask AS PT 
                  LEFT JOIN PNTask AS CT 
                    ON PT.PlanID = CT.PlanID AND CT.OutlineNumber LIKE PT.OutlineNumber + '%' 
                      AND CT.ChildrenCount = 0 AND PT.TaskID <> CT.TaskID
                WHERE CT.PlanID IS NOT NULL AND PT.PlanID = @strPlanID 
            ) XT ON LA.PlanID = XT.PlanID AND LA.TaskID = XT.PT_TaskID

        UPDATE A SET
          TaskID = XT.CT_TaskID,
          WBS1 = XT.CT_WBS1,
          WBS2 = XT.CT_WBS2,
          WBS3 = XT.CT_WBS3,
          ModUser = @strUserName,
          ModDate = CONVERT(VARCHAR, GETUTCDATE(), 121)
          FROM RPAssignment AS A
            INNER JOIN @tabNonLeafAssignment AS LA ON A.PlanID = LA.PlanID AND A.TaskID = LA.TaskID AND A.AssignmentID = LA.AssignmentID
            INNER JOIN (
              SELECT TOP 1
                PT.PlanID AS PlanID,
                PT.TaskID AS PT_TaskID,
                CT.TaskID AS CT_TaskID,
                CT.WBS1 AS CT_WBS1,
                CT.WBS2 AS CT_WBS2,
                CT.WBS3 AS CT_WBS3,
                ROW_NUMBER() OVER(PARTITION BY PT.PlanID, PT.TaskID, PT.OutlineNumber ORDER BY CT.OutlineNumber) AS RowNum
                FROM RPTask AS PT 
                  LEFT JOIN RPTask AS CT 
                    ON PT.PlanID = CT.PlanID AND CT.OutlineNumber LIKE PT.OutlineNumber + '%' 
                      AND CT.ChildrenCount = 0 AND PT.TaskID <> CT.TaskID
                WHERE CT.PlanID IS NOT NULL AND PT.PlanID = @strPlanID 
            ) XT ON LA.PlanID = XT.PlanID AND LA.TaskID = XT.PT_TaskID

       --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        UPDATE TPD SET
          TaskID = PA.TaskID,
          ModUser = @strUserName,
          ModDate = CONVERT(VARCHAR, GETUTCDATE(), 121)
          FROM PNPlannedLabor AS TPD
            INNER JOIN @tabNonLeafAssignment AS LA ON TPD.PlanID = LA.PlanID AND TPD.TaskID = LA.TaskID AND TPD.AssignmentID = LA.AssignmentID
            INNER JOIN PNAssignment AS PA ON TPD.PlanID = PA.PlanID AND TPD.AssignmentID = PA.AssignmentID AND LA.AssignmentID = PA.AssignmentID
          WHERE TPD.PlanID = @strPlanID AND TPD.AssignmentID IS NOT NULL 

        UPDATE TPD SET
          TaskID = PA.TaskID,
          ModUser = @strUserName,
          ModDate = CONVERT(VARCHAR, GETUTCDATE(), 121)
          FROM RPPlannedLabor AS TPD
            INNER JOIN @tabNonLeafAssignment AS LA ON TPD.PlanID = LA.PlanID AND TPD.TaskID = LA.TaskID AND TPD.AssignmentID = LA.AssignmentID
            INNER JOIN RPAssignment AS PA ON TPD.PlanID = PA.PlanID AND TPD.AssignmentID = PA.AssignmentID AND LA.AssignmentID = PA.AssignmentID
          WHERE TPD.PlanID = @strPlanID AND TPD.AssignmentID IS NOT NULL 

      END /* END IF EXISTS(SELECT 1 FROM @tabNonLeafAssignment) */

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move Non-Leaf Expense down to first leaf WBS in the same branch.

    IF EXISTS(SELECT 1 FROM @tabNonLeafExpense)
      BEGIN

        UPDATE E SET
          TaskID = XT.CT_TaskID,
          WBS1 = XT.CT_WBS1,
          WBS2 = XT.CT_WBS2,
          WBS3 = XT.CT_WBS3,
          ModUser = @strUserName,
          ModDate = CONVERT(VARCHAR, GETUTCDATE(), 121)
          FROM PNExpense AS E
            INNER JOIN @tabNonLeafExpense AS LE ON E.PlanID = LE.PlanID AND E.TaskID = LE.TaskID AND E.ExpenseID = LE.ExpenseID
            INNER JOIN (
              SELECT TOP 1
                PT.PlanID AS PlanID,
                PT.TaskID AS PT_TaskID,
                CT.TaskID AS CT_TaskID,
                CT.WBS1 AS CT_WBS1,
                CT.WBS2 AS CT_WBS2,
                CT.WBS3 AS CT_WBS3,
                ROW_NUMBER() OVER(PARTITION BY PT.PlanID, PT.TaskID, PT.OutlineNumber ORDER BY CT.OutlineNumber) AS RowNum
                FROM PNTask AS PT 
                  LEFT JOIN PNTask AS CT 
                    ON PT.PlanID = CT.PlanID AND CT.OutlineNumber LIKE PT.OutlineNumber + '%' 
                      AND CT.ChildrenCount = 0 AND PT.TaskID <> CT.TaskID
                WHERE CT.PlanID IS NOT NULL AND PT.PlanID = @strPlanID 
            ) XT ON LE.PlanID = XT.PlanID AND LE.TaskID = XT.PT_TaskID

        UPDATE E SET
          TaskID = XT.CT_TaskID,
          WBS1 = XT.CT_WBS1,
          WBS2 = XT.CT_WBS2,
          WBS3 = XT.CT_WBS3,
          ModUser = @strUserName,
          ModDate = CONVERT(VARCHAR, GETUTCDATE(), 121)
          FROM RPExpense AS E
            INNER JOIN @tabNonLeafExpense AS LE ON E.PlanID = LE.PlanID AND E.TaskID = LE.TaskID AND E.ExpenseID = LE.ExpenseID
            INNER JOIN (
              SELECT TOP 1
                PT.PlanID AS PlanID,
                PT.TaskID AS PT_TaskID,
                CT.TaskID AS CT_TaskID,
                CT.WBS1 AS CT_WBS1,
                CT.WBS2 AS CT_WBS2,
                CT.WBS3 AS CT_WBS3,
                ROW_NUMBER() OVER(PARTITION BY PT.PlanID, PT.TaskID, PT.OutlineNumber ORDER BY CT.OutlineNumber) AS RowNum
                FROM RPTask AS PT 
                  LEFT JOIN RPTask AS CT 
                    ON PT.PlanID = CT.PlanID AND CT.OutlineNumber LIKE PT.OutlineNumber + '%' 
                      AND CT.ChildrenCount = 0 AND PT.TaskID <> CT.TaskID
                WHERE CT.PlanID IS NOT NULL AND PT.PlanID = @strPlanID 
            ) XT ON LE.PlanID = XT.PlanID AND LE.TaskID = XT.PT_TaskID

       --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        UPDATE TPD SET
          TaskID = PE.TaskID,
          ModUser = @strUserName,
          ModDate = CONVERT(VARCHAR, GETUTCDATE(), 121)
          FROM PNPlannedExpenses AS TPD
            INNER JOIN @tabNonLeafExpense AS LE ON TPD.PlanID = LE.PlanID AND TPD.TaskID = LE.TaskID AND TPD.ExpenseID = LE.ExpenseID
            INNER JOIN PNExpense AS PE ON TPD.PlanID = PE.PlanID AND TPD.ExpenseID = PE.ExpenseID AND LE.ExpenseID = PE.ExpenseID
          WHERE TPD.PlanID = @strPlanID AND TPD.ExpenseID IS NOT NULL 

        UPDATE TPD SET
          TaskID = PE.TaskID,
          ModUser = @strUserName,
          ModDate = CONVERT(VARCHAR, GETUTCDATE(), 121)
          FROM RPPlannedExpenses AS TPD
            INNER JOIN @tabNonLeafExpense AS LE ON TPD.PlanID = LE.PlanID AND TPD.TaskID = LE.TaskID AND TPD.ExpenseID = LE.ExpenseID
            INNER JOIN RPExpense AS PE ON TPD.PlanID = PE.PlanID AND TPD.ExpenseID = PE.ExpenseID AND LE.ExpenseID = PE.ExpenseID
          WHERE TPD.PlanID = @strPlanID AND TPD.ExpenseID IS NOT NULL 

      END /* END IF EXISTS(SELECT 1 FROM @tabNonLeafExpense) */

  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Move Non-Leaf Consultant down to first leaf WBS in the same branch.

    IF EXISTS(SELECT 1 FROM @tabNonLeafConsultant)
      BEGIN

        UPDATE C SET
          TaskID = XT.CT_TaskID,
          WBS1 = XT.CT_WBS1,
          WBS2 = XT.CT_WBS2,
          WBS3 = XT.CT_WBS3,
          ModUser = @strUserName,
          ModDate = CONVERT(VARCHAR, GETUTCDATE(), 121)
          FROM PNConsultant AS C
            INNER JOIN @tabNonLeafConsultant AS LC ON C.PlanID = LC.PlanID AND C.TaskID = LC.TaskID AND C.ConsultantID = LC.ConsultantID
            INNER JOIN (
              SELECT TOP 1
                PT.PlanID AS PlanID,
                PT.TaskID AS PT_TaskID,
                CT.TaskID AS CT_TaskID,
                CT.WBS1 AS CT_WBS1,
                CT.WBS2 AS CT_WBS2,
                CT.WBS3 AS CT_WBS3,
                ROW_NUMBER() OVER(PARTITION BY PT.PlanID, PT.TaskID, PT.OutlineNumber ORDER BY CT.OutlineNumber) AS RowNum
                FROM PNTask AS PT 
                  LEFT JOIN PNTask AS CT 
                    ON PT.PlanID = CT.PlanID AND CT.OutlineNumber LIKE PT.OutlineNumber + '%' 
                      AND CT.ChildrenCount = 0 AND PT.TaskID <> CT.TaskID
                WHERE CT.PlanID IS NOT NULL AND PT.PlanID = @strPlanID 
            ) XT ON LC.PlanID = XT.PlanID AND LC.TaskID = XT.PT_TaskID

        UPDATE C SET
          TaskID = XT.CT_TaskID,
          ModUser = @strUserName,
          WBS1 = XT.CT_WBS1,
          WBS2 = XT.CT_WBS2,
          WBS3 = XT.CT_WBS3,
          ModDate = CONVERT(VARCHAR, GETUTCDATE(), 121)
          FROM RPConsultant AS C
            INNER JOIN @tabNonLeafConsultant AS LC ON C.PlanID = LC.PlanID AND C.TaskID = LC.TaskID AND C.ConsultantID = LC.ConsultantID
            INNER JOIN (
              SELECT TOP 1
                PT.PlanID AS PlanID,
                PT.TaskID AS PT_TaskID,
                CT.TaskID AS CT_TaskID,
                CT.WBS1 AS CT_WBS1,
                CT.WBS2 AS CT_WBS2,
                CT.WBS3 AS CT_WBS3,
                ROW_NUMBER() OVER(PARTITION BY PT.PlanID, PT.TaskID, PT.OutlineNumber ORDER BY CT.OutlineNumber) AS RowNum
                FROM RPTask AS PT 
                  LEFT JOIN RPTask AS CT 
                    ON PT.PlanID = CT.PlanID AND CT.OutlineNumber LIKE PT.OutlineNumber + '%' 
                      AND CT.ChildrenCount = 0 AND PT.TaskID <> CT.TaskID
                WHERE CT.PlanID IS NOT NULL AND PT.PlanID = @strPlanID 
            ) XT ON LC.PlanID = XT.PlanID AND LC.TaskID = XT.PT_TaskID

       --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        UPDATE TPD SET
          TaskID = PC.TaskID,
          ModUser = @strUserName,
          ModDate = CONVERT(VARCHAR, GETUTCDATE(), 121)
          FROM PNPlannedConsultant AS TPD
            INNER JOIN @tabNonLeafConsultant AS LC ON TPD.PlanID = LC.PlanID AND TPD.TaskID = LC.TaskID AND TPD.ConsultantID = LC.ConsultantID
            INNER JOIN PNConsultant AS PC ON TPD.PlanID = PC.PlanID AND TPD.ConsultantID = PC.ConsultantID AND LC.ConsultantID = PC.ConsultantID
          WHERE TPD.PlanID = @strPlanID AND TPD.ConsultantID IS NOT NULL 

        UPDATE TPD SET
          TaskID = PC.TaskID,
          ModUser = @strUserName,
          ModDate = CONVERT(VARCHAR, GETUTCDATE(), 121)
          FROM RPPlannedConsultant AS TPD
            INNER JOIN @tabNonLeafConsultant AS LC ON TPD.PlanID = LC.PlanID AND TPD.TaskID = LC.TaskID AND TPD.ConsultantID = LC.ConsultantID
            INNER JOIN RPConsultant AS PC ON TPD.PlanID = PC.PlanID AND TPD.ConsultantID = PC.ConsultantID AND LC.ConsultantID = PC.ConsultantID
          WHERE TPD.PlanID = @strPlanID AND TPD.ConsultantID IS NOT NULL 

      END /* END IF EXISTS(SELECT 1 FROM @tabNonLeafConsultant) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   --Move Labor code level Tasks to the lowest WBS Level
   -- 1. The Labor code under WBS1 Level need to move under the first WBS2 Level or under WBS3 Level
	 UPDATE PNTask SET ParentOutlineNumber = ParentTask.OutlineNumber, 
					   WBS2 = ParentTask.WBS2, 
					   WBS3 = ParentTask.WBS3,
					   OutlineLevel = CASE WHEN ParentTask.WBS3 IS NULL THEN 2 ELSE 3 END FROM 
	 (SELECT TOP 1 PlanID,OutlineNumber,WBS2, WBS3 FROM PNTask WHERE PlanID = @strPlanID AND
	    WBS2 IS NOT NULL AND ChildrenCount = 0 ORDER BY OutlineNumber ) AS ParentTask 
		INNER JOIN PNTask ON ParentTask.PlanID = PNTask.PlanID
     Where PNTask.PlanID = @strPlanID AND WBStype = 'LBCD' AND PNTask.WBS2 IS NULL

	 --RPTask
	 UPDATE RPTask SET ParentOutlineNumber = ParentTask.OutlineNumber, 
					   WBS2 = ParentTask.WBS2, 
					   WBS3 = ParentTask.WBS3,
					   OutlineLevel = CASE WHEN ParentTask.WBS3 IS NULL THEN 2 ELSE 3 END FROM 
	 (SELECT TOP 1 PlanID,OutlineNumber,WBS2, WBS3 FROM RPTask WHERE PlanID = @strPlanID AND
	    WBS2 IS NOT NULL AND ChildrenCount = 0 ORDER BY OutlineNumber ) AS ParentTask 
		INNER JOIN RPTask ON ParentTask.PlanID = RPTask.PlanID
     Where RPTask.PlanID = @strPlanID AND WBStype = 'LBCD' AND RPTask.WBS2 IS NULL

	 ---- 2. Labor code under WBS2 Level and need to move under WBS3 Level
	 -- if no labor code under WBS1 are updated. then we need to move the labor code under WBS2 to WBS3 level
	 If @@ROWCOUNT = 0 
	 BEGIN
		 DECLARE laborCodeCursor CURSOR FOR 
		 SELECT  TaskID  FROM @tabLaborCodeTask WHERE WBS2 IS NOT NULL 

		 OPEN laborCodeCursor
		 FETCH NEXT FROM laborCodeCursor INTO @strTaskID 
		 WHILE @@FETCH_STATUS = 0 
		 BEGIN
			 UPDATE PNTask SET ParentOutlineNumber = ParentTask.OutlineNumber, 
							   WBS3 = ParentTask.WBS3,
							   OutlineLevel = 3 FROM 
			 (SELECT TOP 1 PNTask.PlanID AS PlanID,PNTask.OutlineNumber AS OutlineNumber, PNTask.WBS3 AS WBS3 FROM PNTask 
			  INNER JOIN @tabLaborCodeTask laborCodeTask ON laborCodeTask.PlanID = PNTask.PlanID AND
			  PNTask.WBS2 = laborCodeTask.WBS2 WHERE laborCodeTask.TaskID = @strTaskID
			  AND PNTask.WBS3 IS NOT NULL ORDER BY OutlineNumber) AS ParentTask 
			  INNER JOIN PNTASK ON ParentTask.PlanID = PNTASK.PlanID	     
			 Where PNTask.PlanID = @strPlanID AND TaskID = @strTaskID AND PNTASK.OutlineLevel = 2

			 --RPTask
			 UPDATE RPTask SET ParentOutlineNumber = ParentTask.OutlineNumber, 
							   WBS3 = ParentTask.WBS3,
							   OutlineLevel = 3 FROM 
			 (SELECT TOP 1 RPTask.PlanID AS PlanID,RPTask.OutlineNumber AS OutlineNumber, RPTask.WBS3 AS WBS3 FROM RPTask 
			  INNER JOIN @tabLaborCodeTask laborCodeTask ON laborCodeTask.PlanID = RPTask.PlanID AND
			  RPTask.WBS2 = laborCodeTask.WBS2 WHERE laborCodeTask.TaskID = @strTaskID
			  AND RPTask.WBS3 IS NOT NULL ORDER BY OutlineNumber) AS ParentTask 
			  INNER JOIN RPTask ON ParentTask.PlanID = RPTask.PlanID	     
			 Where RPTask.PlanID = @strPlanID AND TaskID = @strTaskID AND RPTask.OutlineLevel = 2

			 FETCH NEXT FROM laborCodeCursor INTO @strTaskID
		 END --WHILE @@FETCH_STATUS = 0 
		 CLOSE laborCodeCursor
		 DEALLOCATE laborCodeCursor
	 END --IF @@RowCount = 0	
	  
	 --3 Reorder the outline number and childrenCount for the plan	 
	SET @RowID = 'P~' + @strPlanID
	EXECUTE dbo.stRPReCalcPlan @strRowID = @RowID, @bitReorderOutlineNumber = 1,@bitCalledFromRM = 1
  --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Extend date span of WBS rows, if needed.

  EXECUTE dbo.stRPExtendDateSpan @strPlanID, 1

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Add JTD Assignments, Expenses, Consultants, if needed.
 
  DECLARE @strRowID nvarchar(255)

  SELECT 
    @strRowID = '|' + T.TaskID
    FROM PNTask AS T
    WHERE T.PlanID = @strPlanID AND T.OutlineLevel = 0

  EXECUTE dbo.stRPAddJTDAssignment @strRowID, 1
  EXECUTE dbo.stRPAddJTDExpense @strRowID, 1
  EXECUTE dbo.stRPAddJTDConsultant @strRowID, 1

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  COMMIT TRANSACTION

  SET NOCOUNT OFF
     
END -- stRPSyncWBSStructure
GO
