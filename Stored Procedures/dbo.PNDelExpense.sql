SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PNDelExpense]
  @strPlanID varchar(32),
  @strExpenseID varchar(32)
AS

BEGIN -- Procedure PNDelExpense

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to delete a Expense row and its associated rows.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT ON
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    
  DELETE PNPlannedExpenses WHERE PlanID = @strPlanID AND ExpenseID = @strExpenseID
  DELETE PNBaselineExpenses WHERE PlanID = @strPlanID AND ExpenseID = @strExpenseID
  DELETE PNExpense WHERE PlanID = @strPlanID AND ExpenseID = @strExpenseID

  SET NOCOUNT OFF

END -- PNDelExpense
GO
