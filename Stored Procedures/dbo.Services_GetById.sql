SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Services_GetById]
(
	@id	NVARCHAR(32)
)
AS
BEGIN
  ------------------------------------------------------------------------
  SET NOCOUNT ON;

  ------------------------------------------------------------------------
  SELECT 
    S.UDIC_UID
   ,S.CustomCurrencyCode
   ,S.CreateUser
   ,S.CreateDate
   ,S.ModUser
   ,S.ModDate
   ,S.CustServiceCode
   ,S.CustServiceName
   ,S.CustServiceCategory
   ,S.CustServiceSubCategory
   ,S.CustSF330Code
   ,S.CustSortOrder
  FROM
    dbo.UDIC_Service AS S
  WHERE
	S.UDIC_UID = @id
  ------------------------------------------------------------------------
END
GO
