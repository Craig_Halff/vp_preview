SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_GetPhaseTaskRows] ( @sRole varchar(32), @prnamelen int, @wbs1 nvarchar(max), @wbs2 nvarchar(max), @ExcludeDormant char(1)= 'N', @WBS3forWBS1s char(1)= 'N', @quickLoad int= 0)
             AS EXEC spCCG_EI_GetPhaseTaskRows @sRole,@prnamelen,@wbs1,@wbs2,@ExcludeDormant,@WBS3forWBS1s,@quickLoad
GO
