SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stCopyPlan]
  @strPlanID varchar(32),
  @strUserName Nvarchar(32),
  @strNewPlanID varchar(32) = NULL,
  @strNewPlanName Nvarchar(255) = NULL,
  @strNewPlanNumber Nvarchar(30) = NULL,
  @strStructureOnly char(1) = 'N'
AS

BEGIN -- Procedure stCopyPlan

  SET NOCOUNT ON
  
  declare @strTopDownOnly char(1) = 'N'
  DECLARE @strAllocMethod varchar(1)

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- If New Plan ID was not passed in, get a new value for this field.
  
  SET @strNewPlanID = CASE WHEN @strNewPlanID IS NULL 
                           THEN REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') 
                           ELSE @strNewPlanID END
                           
	-- Use Temp Tables to keep new ID.
if @strTopDownOnly = 'N'
		begin
			SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS NewTaskID, TaskID INTO #Task FROM PNTask
			WHERE PlanID = @strPlanID
        
			SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS NewAssignmentID, AssignmentID INTO #Assignment FROM PNAssignment
			WHERE PlanID = @strPlanID

			SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS NewExpenseID, ExpenseID INTO #Expense FROM PNExpense
			WHERE PlanID = @strPlanID

			SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS NewConsultantID, ConsultantID INTO #Consultant FROM PNConsultant
			WHERE PlanID = @strPlanID
			  
			SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS NewUnitID, UnitID INTO #Unit FROM RPUnit
			WHERE PlanID = @strPlanID
			  
			SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS NewActivityID, A.AssignmentID AS AssignmentID, A.ResourceID AS ResourceID INTO #Activity 
			  FROM PNAssignment AS A INNER JOIN Activity AS AC ON A.AssignmentID = AC.AssignmentID
			WHERE PlanID = @strPlanID AND ResourceID IS NOT NULL  

		end
/* no need for 1.0
		SELECT REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS NewAccordionFormatID, AccordionFormatID INTO #AccordionFormat FROM RPAccordionFormat
		WHERE PlanID = @strPlanID
*/
  -->> PNPlan
  
  INSERT INTO PNPlan
    (PlanID,
     PlanName,
     PlanNumber,  
     StartDate,
     EndDate,
     StartingDayOfWeek,
     Status,
     Company,
     WBS1,
     ClientID,
     OpportunityID,
     Org,
     Principal,
     ProjMgr,
     Supervisor,
     Probability,
     PctCompleteFormula,
     LabMultType,
     Multiplier,
     ReimbMethod,
     PctComplByPeriodFlg,
     RevenueMethod,
     AnalysisBasis,
     BudgetType,
     CostCurrencyCode,
     BillingCurrencyCode,
     CostRtMethod,
     CostRtTableNo,
     BillingRtMethod,
     BillingRtTableNo,
     LabBillMultiplier,
     GRMethod,
     GenResTableNo,
     GRBillTableNo,
     CalcExpBillAmtFlg,
     CalcConBillAmtFlg,
     ExpBillRtMethod,
     ExpBillRtTableNo,
     ExpBillMultiplier,
     ConBillRtMethod,
     ConBillRtTableNo,
     ConBillMultiplier,
     UntBillMultiplier,
     TopDownFee,
	 ExpWBSLevel,
     ConWBSLevel,
     CompensationFee,
     CompensationFeeBill,
	 CompensationFeeDirLab,
	 CompensationFeeDirLabBill,
	 CompensationFeeDirExp,
	 CompensationFeeDirExpBill,
     ConsultantFee,
     ConsultantFeeBill,
     ContingencyAmt,
     ContingencyPct,
     OverheadPct,
	 ReimbAllowanceExp,
	 ReimbAllowanceExpBill,
	 ReimbAllowanceCon,
	 ReimbAllowanceConBill,
     ReimbAllowance,
     ReimbAllowanceBill,
     TargetMultCost,
     TargetMultBill,
     AvailableFlg,
     UnPostedFlg,
     CommitmentFlg,
     UtilizationIncludeFlg,
     ProjectedMultiplier,
     ProjectedRatio,
     WeightedLabCost,
     WeightedLabBill,
     WeightedExpCost,
     WeightedExpBill,
     WeightedConCost,
     WeightedConBill,
     WeightedUntCost,
     WeightedUntBill,
     PlannedLaborHrs,
     PlannedLabCost,
     PlannedLabBill,
     PlannedExpCost,
     PlannedExpBill,
     PlannedConCost,
     PlannedConBill,
     PlannedUntQty,
     PlannedUntCost,
     PlannedUntBill,
     PlannedDirExpCost,
     PlannedDirExpBill,
     PlannedDirConCost,
     PlannedDirConBill,
     PlannedDirUntCost,
     PlannedDirUntBill,
     PctComplete,
     PctCompleteBill,
     PctCompleteLabCost,
     PctCompleteLabBill,
     PctCompleteExpCost,
     PctCompleteExpBill,
     PctCompleteConCost,
     PctCompleteConBill,
     PctCompleteUntCost,
     PctCompleteUntBill,
     LabRevenue,
     ExpRevenue,
     ConRevenue,
     UntRevenue,
     BaselineStart,
     BaselineFinish,
     BaselineLaborHrs,
     BaselineLabCost,
     BaselineLabBill,
     BaselineExpCost,
     BaselineExpBill,
     BaselineConCost,
     BaselineConBill,
     BaselineUntQty,
     BaselineUntCost,
     BaselineUntBill,
     BaselineRevenue,
	 LCLevel1Enabled,
	 LCLevel2Enabled,
	 LCLevel3Enabled,
	 LCLevel4Enabled,
	 LCLevel5Enabled,
     CreateUser,
     ModUser, 
     CheckedOutUser,
     CreateDate,
     ModDate,
     CheckedOutDate,
     CheckedOutID,
	 StoredCurrency)
    SELECT
      @strNewPlanID AS PlanID,
      CASE WHEN @strNewPlanName IS NULL 
           THEN 'Copy of ' + PlanName 
           ELSE @strNewPlanName END AS PlanName,
      @strNewPlanNumber AS PlanNumber,
      StartDate,
      EndDate,
      StartingDayOfWeek,
      Status,
      Company,
      CASE WHEN @strTopDownOnly = 'Y' THEN NULL ELSE WBS1 END,
      ClientID,
      OpportunityID,
      Org,
      Principal,
      ProjMgr,
      Supervisor,
      Probability,
      PctCompleteFormula,
      LabMultType,
      Multiplier,
      ReimbMethod,
      PctComplByPeriodFlg,
      RevenueMethod,
      AnalysisBasis,
      BudgetType,
      CostCurrencyCode,
      BillingCurrencyCode,
      CostRtMethod,
      CostRtTableNo,
      BillingRtMethod,
      BillingRtTableNo,
      LabBillMultiplier,
      GRMethod,
      GenResTableNo,
      GRBillTableNo,
      CalcExpBillAmtFlg,
      CalcConBillAmtFlg,
      ExpBillRtMethod,
      ExpBillRtTableNo,
      ExpBillMultiplier,
      ConBillRtMethod,
      ConBillRtTableNo,
      ConBillMultiplier,
      UntBillMultiplier,
      TopDownFee,
	  ExpWBSLevel,
      ConWBSLevel,
	  CompensationFee,
	  CompensationFeeBill,
	  CompensationFeeDirLab,
	  CompensationFeeDirLabBill,
	  CompensationFeeDirExp,
	  CompensationFeeDirExpBill,
	  ConsultantFee,
	  ConsultantFeeBill,
	  ContingencyAmt,
	  ContingencyPct,
	  OverheadPct,
	  ReimbAllowanceExp,
	  ReimbAllowanceExpBill,
	  ReimbAllowanceCon,
	  ReimbAllowanceConBill,
	  ReimbAllowance,
	  ReimbAllowanceBill, 
	  TargetMultCost,
      TargetMultBill,
      AvailableFlg,
      UnPostedFlg,
      CommitmentFlg,
      'N' AS UtilizationIncludeFlg,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE ProjectedMultiplier END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE ProjectedRatio END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE WeightedLabCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE WeightedLabBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE WeightedExpCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE WeightedExpBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE WeightedConCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE WeightedConBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE WeightedUntCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE WeightedUntBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PlannedLaborHrs END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PlannedLabCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PlannedLabBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PlannedExpCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PlannedExpBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PlannedConCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PlannedConBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PlannedUntQty END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PlannedUntCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PlannedUntBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PlannedDirExpCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PlannedDirExpBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PlannedDirConCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PlannedDirConBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PlannedDirUntCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PlannedDirUntBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PctComplete END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PctCompleteBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PctCompleteLabCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PctCompleteLabBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PctCompleteExpCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PctCompleteExpBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PctCompleteConCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PctCompleteConBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PctCompleteUntCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE PctCompleteUntBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE LabRevenue END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE ExpRevenue END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE ConRevenue END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE UntRevenue END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE BaselineStart END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE BaselineFinish END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE BaselineLaborHrs END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE BaselineLabCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE BaselineLabBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE BaselineExpCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE BaselineExpBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE BaselineConCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE BaselineConBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE BaselineUntQty END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE BaselineUntCost END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE BaselineUntBill END,
      CASE WHEN @strStructureOnly = 'Y' or @strTopDownOnly = 'Y' THEN 0 ELSE BaselineRevenue END,
	  LCLevel1Enabled,
	  LCLevel2Enabled,
	  LCLevel3Enabled,
      LCLevel4Enabled,
      LCLevel5Enabled,
      @strUserName AS CreateUser,
      @strUserName AS ModUser, 
      @strUserName AS CheckedOutUser, 
      CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate ,
	  CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDateDate, 
	  CONVERT(VARCHAR, GETUTCDATE(), 121) AS CheckedOutDate,
	  @strNewPlanID AS CheckedOutID,
	  StoredCurrency
    FROM PNPlan WHERE PlanID = @strPlanID
    

if @strTopDownOnly = 'N'
begin

	-- Get Project Allocation method data.
	SELECT @strAllocMethod = AllocMethod FROM PR WHERE PR.WBS1 = @strNewPlanNumber AND PR.WBS2 = ' ' AND PR.WBS3 = ' '

  -->> PNTask

  INSERT INTO PNTask
    (TaskID,
     PlanID,
     Name,
     WBSType,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     StartDate,
     EndDate,
	 RevenueStartDate,
	 RevenueEndDate,
     CostRate,
     BillingRate,
     ExpBillRate,
     ConBillRate,
     OverheadPct,
     ChildrenCount,
     OutlineLevel,
     OutlineNumber,
     ParentOutlineNumber,
     PlannedLaborHrs,
     PlannedLabCost,
     PlannedLabBill,
     PlannedExpCost,
     PlannedExpBill,
     PlannedConCost,
     PlannedConBill,
     PlannedUntQty,
     PlannedUntCost,
     PlannedUntBill,
     PlannedDirExpCost,
     PlannedDirExpBill,
     PlannedDirConCost,
     PlannedDirConBill,
     PlannedDirUntCost,
     PlannedDirUntBill,
     LabRevenue,
     ExpRevenue,
     ConRevenue,
     UntRevenue,
     PctCompleteLabCost,
     PctCompleteLabBill,
     PctCompleteExpCost,
     PctCompleteExpBill,
     PctCompleteConCost,
     PctCompleteConBill,
     PctCompleteUntCost,
     PctCompleteUntBill,
     WeightedLabCost,
     WeightedLabBill,
     WeightedExpCost,
     WeightedExpBill,
     WeightedConCost,
     WeightedConBill,
     WeightedUntCost,
     WeightedUntBill,
     BaselineStart,
     BaselineFinish,
     BaselineLaborHrs,
     BaselineLabCost,
     BaselineLabBill,
     BaselineExpCost,
     BaselineExpBill,
     BaselineConCost,
     BaselineConBill,
     BaselineUntQty,
     BaselineUntCost,
     BaselineUntBill,
     BaselineRevenue,
     CompensationFee,
     CompensationFeeBill,
	 CompensationFeeDirLab,
	 CompensationFeeDirLabBill,
	 CompensationFeeDirExp,
	 CompensationFeeDirExpBill,
     ConsultantFee,
     ConsultantFeeBill,
     ReimbAllowance,
     ReimbAllowanceBill,
	 ReimbAllowanceExp,
	 ReimbAllowanceExpBill,
	 ReimbAllowanceCon,
	 ReimbAllowanceConBill,
     LabParentState,
     ExpParentState,
     ConParentState,
     UntParentState,
     ConVState,
     ExpVState,
     LabVState,
     UntVState,
	 ProjMgr, 
	 ChargeType,  
	 ProjectType, 
	 ClientID,  
	 Status, 
	 Org,  
	 Notes,  
     CreateUser,
     ModUser,
     CreateDate,
     ModDate)
    SELECT
      NewTaskID,
      @strNewPlanID,
      Name,
      WBSType,
      WBS1,
      WBS2,
      WBS3,
      LaborCode,
      StartDate,
      EndDate,
	  CASE WHEN @strAllocMethod = 'r' THEN StartDate ELSE NULL END AS RevenueStartDate,
	  CASE WHEN @strAllocMethod = 'r' THEN EndDate ELSE NULL END AS RevenueEndDate,
      CostRate,
      BillingRate,
      ExpBillRate,
      ConBillRate,
      OverheadPct,
      ChildrenCount,
      OutlineLevel,
      OutlineNumber,
      ParentOutlineNumber,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedLaborHrs END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedLabCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedLabBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedExpCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedExpBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedConCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedConBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedUntQty END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedUntCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedUntBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedDirExpCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedDirExpBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedDirConCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedDirConBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedDirUntCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedDirUntBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE LabRevenue END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE ExpRevenue END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE ConRevenue END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE UntRevenue END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PctCompleteLabCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PctCompleteLabBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PctCompleteExpCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PctCompleteExpBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PctCompleteConCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PctCompleteConBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PctCompleteUntCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PctCompleteUntBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE WeightedLabCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE WeightedLabBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE WeightedExpCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE WeightedExpBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE WeightedConCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE WeightedConBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE WeightedUntCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE WeightedUntBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineStart END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineFinish END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineLaborHrs END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineLabCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineLabBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineExpCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineExpBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineConCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineConBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineUntQty END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineUntCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineUntBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineRevenue END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE CompensationFee END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE CompensationFeeBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE CompensationFeeDirLab END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE CompensationFeeDirLabBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE CompensationFeeDirExp END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE CompensationFeeDirExpBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE ConsultantFee END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE ConsultantFeeBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE ReimbAllowance END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE ReimbAllowanceBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE ReimbAllowanceExp END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE ReimbAllowanceExpBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE ReimbAllowanceCon END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE ReimbAllowanceConBill END,
      LabParentState,
      ExpParentState,
      ConParentState,
      UntParentState,
      ConVState,
      ExpVState,
      LabVState,
      UntVState,
	  ProjMgr, 
      ChargeType,  
	  ProjectType, 
	  ClientID,  
	  Status, 
	  Org,  
	  Notes,  
      @strUserName AS CreateUser,
      @strUserName AS ModUser,
      CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate ,
	  CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDateDate 
    FROM #Task AS XT INNER JOIN PNTask AS T ON XT.TaskID = T.TaskID

    
  -->> Activity
  
  INSERT INTO Activity
    (ActivityID, 
     AssignmentID,
     Employee, 
     Subject, 
     Type,
     RP,
     StartDate, 
     EndDate,
     ShowTimeAs,
     ReminderMinHrDay, 
     CreateUser,
     ModUser,
     CreateDate,
     ModDate)
    SELECT
      NewActivityID,
      NewAssignmentID,
      Employee, 
      Subject, 
      Type,
      RP,
      StartDate, 
      EndDate,
      ShowTimeAs,
      ReminderMinHrDay,
      @strUserName AS CreateUser,
      @strUserName AS ModUser,
      CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate ,
	  CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDateDate       
    FROM #Activity AS XAC INNER JOIN Activity AS AC ON XAC.AssignmentID = AC.AssignmentID
    INNER JOIN #Assignment AS A ON A.AssignmentID = AC.AssignmentID  
  
  INSERT INTO EMActivity (ActivityID, Owner, Employee,CreateUser, ModUser, CreateDate, ModDate)
    SELECT NewActivityID, 'Y', ResourceID,
      @strUserName AS CreateUser,
      @strUserName AS ModUser,
      CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate ,
	  CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDateDate           
    FROM #Activity
  
  -->> PNAssignment
  
  INSERT INTO PNAssignment
    (AssignmentID,
     PlanID,
     TaskID,
     WBS1,
     WBS2,
     WBS3,
     LaborCode,
     GRLBCD,
     Category,
     StartDate,
     EndDate,
     ResourceID,
     GenericResourceID,
     SortSeq,
     HardBooked,
     CostRate,
     BillingRate,
     PlannedLaborHrs,
     PlannedLabCost,
     PlannedLabBill,
     LabRevenue,
     PctCompleteLabBill,
     PctCompleteLabCost,
     WeightedLabCost,
     WeightedLabBill,
     BaselineStart,
     BaselineFinish,
     BaselineLaborHrs,
     BaselineLabCost,
     BaselineLabBill,
     BaselineRevenue,
     CreateUser,
     ModUser,
     CreateDate,
     ModDate)
    SELECT
      NewAssignmentID,
      @strNewPlanID,
      NewTaskID,
      WBS1,
      WBS2,
      WBS3,
      LaborCode,
      GRLBCD,
      Category,
      StartDate,
      EndDate,
      A.ResourceID AS ResourceID,
      GenericResourceID,
      SortSeq,
      HardBooked,
      CostRate,
      BillingRate,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedLaborHrs END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedLabCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedLabBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE LabRevenue END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PctCompleteLabBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PctCompleteLabCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE WeightedLabCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE WeightedLabBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineStart END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineFinish END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineLaborHrs END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineLabCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineLabBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineRevenue END,
      @strUserName AS CreateUser,
      @strUserName AS ModUser,
      CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate ,
	  CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDateDate       
    FROM #Assignment AS XA INNER JOIN PNAssignment AS A ON XA.AssignmentID = A.AssignmentID
      INNER JOIN #Task AS XT ON A.TaskID = XT.TaskID
 
  -->> PNExpense
  
  INSERT INTO PNExpense
    (ExpenseID,
     PlanID,
     TaskID,
     WBS1,
     WBS2,
     WBS3,
     Account,
     Vendor,
     StartDate,
     EndDate,
     SortSeq,
     ExpBillRate,
     DirectAcctFlg,
     PlannedExpCost,
     PlannedExpBill,
     ExpRevenue,
     PlannedDirExpCost,
     PlannedDirExpBill,
     PctCompleteExpCost,
     PctCompleteExpBill,
     WeightedExpCost,
     WeightedExpBill,
     BaselineStart,
     BaselineFinish,
     BaselineExpCost,
     BaselineExpBill,
     CreateUser,
     ModUser,
     CreateDate,
     ModDate)
    SELECT
      NewExpenseID,
      @strNewPlanID,
      NewTaskID,
      WBS1,
      WBS2,
      WBS3,
      Account,
      Vendor,
      StartDate,
      EndDate,
      SortSeq,
      ExpBillRate,
      DirectAcctFlg,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedExpCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedExpBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE ExpRevenue END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedDirExpCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedDirExpBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PctCompleteExpCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PctCompleteExpBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE WeightedExpCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE WeightedExpBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineStart END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineFinish END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineExpCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineExpBill END,
      @strUserName AS CreateUser,
      @strUserName AS ModUser,
      CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate ,
	  CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDateDate       
    FROM #Expense AS XE INNER JOIN PNExpense AS E ON XE.ExpenseID = E.ExpenseID
      INNER JOIN #Task AS XT ON E.TaskID = XT.TaskID
      
  -->> PNConsultant
  
  INSERT INTO PNConsultant
    (ConsultantID,
     PlanID,
     TaskID,
     WBS1,
     WBS2,
     WBS3,
     Account,
     Vendor,
     StartDate,
     EndDate,
     SortSeq,
     ConBillRate,
     DirectAcctFlg,
     PlannedConCost,
     PlannedConBill,
     ConRevenue,
     PlannedDirConCost,
     PlannedDirConBill,
     PctCompleteConCost,
     PctCompleteConBill,
     WeightedConCost,
     WeightedConBill,
     BaselineStart,
     BaselineFinish,
     BaselineConCost,
     BaselineConBill,
     CreateUser,
     ModUser,
     CreateDate,
     ModDate)
    SELECT
      NewConsultantID,
      @strNewPlanID,
      NewTaskID,
      WBS1,
      WBS2,
      WBS3,
      Account,
      Vendor,
      StartDate,
      EndDate,
      SortSeq,
      ConBillRate,
      DirectAcctFlg,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedConCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedConBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE ConRevenue END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedDirConCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PlannedDirConBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PctCompleteConCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE PctCompleteConBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE WeightedConCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE WeightedConBill END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineStart END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineFinish END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineConCost END,
      CASE WHEN @strStructureOnly = 'Y' THEN 0 ELSE BaselineConBill END,
      @strUserName AS CreateUser,
      @strUserName AS ModUser,
      CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate ,
	  CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDateDate       
    FROM #Consultant AS XC INNER JOIN PNConsultant AS C ON XC.ConsultantID = C.ConsultantID
      INNER JOIN #Task AS XT ON C.TaskID = XT.TaskID
      
  -- If the user wants to copy the entire Plan then copy the Time-Phased data.
  
  IF (@strStructureOnly != 'Y')
    BEGIN
    
      -->> PNPlannedExpenses
      
      INSERT INTO PNPlannedLabor
        (TimePhaseID,
         PlanID,
         TaskID,
         AssignmentID,
         StartDate,
         EndDate,
         PeriodHrs,
         PeriodCost,
         PeriodBill,
         CostRate,
         BillingRate,
         CreateUser,
         ModUser,
         CreateDate,
         ModDate)
        SELECT
          REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
          @strNewPlanID,
          NewTaskID,
          NewAssignmentID,
          StartDate,
          EndDate,
          PeriodHrs,
          PeriodCost,
          PeriodBill,
          CostRate,
          BillingRate,
          @strUserName AS CreateUser,
          @strUserName AS ModUser,
          CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate ,
	      CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDateDate           
        FROM PNPlannedLabor AS TPD
          INNER JOIN #Task AS XT ON TPD.TaskID = XT.TaskID
          LEFT JOIN #Assignment AS XA ON TPD.AssignmentID = XA.AssignmentID
          
      -->> PNPlannedExpenses
      
      INSERT INTO PNPlannedExpenses
        (TimePhaseID,
         PlanID,
         TaskID,
         ExpenseID,
         StartDate,
         EndDate,
         PeriodCost,
         PeriodBill,
         CreateUser,
         ModUser,
         CreateDate,
         ModDate)
        SELECT
          REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
          @strNewPlanID,
          NewTaskID,
          NewExpenseID,
          StartDate,
          EndDate,
          PeriodCost,
          PeriodBill,
          @strUserName AS CreateUser,
          @strUserName AS ModUser,
		  CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate ,
		  CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDateDate           
        FROM PNPlannedExpenses AS TPD
          INNER JOIN #Task AS XT ON TPD.TaskID = XT.TaskID
          LEFT JOIN #Expense AS XE ON TPD.ExpenseID = XE.ExpenseID

      -->> PNPlannedConsultant
      
      INSERT INTO PNPlannedConsultant
        (TimePhaseID,
         PlanID,
         TaskID,
         ConsultantID,
         StartDate,
         EndDate,
         PeriodCost,
         PeriodBill,
         CreateUser,
         ModUser,
         CreateDate,
         ModDate)
        SELECT
          REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
          @strNewPlanID,
          NewTaskID,
          NewConsultantID,
          StartDate,
          EndDate,
          PeriodCost,
          PeriodBill,
          @strUserName AS CreateUser,
          @strUserName AS ModUser,
	      CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate ,
		  CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDateDate 
        FROM RPPlannedConsultant AS TPD
          INNER JOIN #Task AS XT ON TPD.TaskID = XT.TaskID
          LEFT JOIN #Consultant AS XC ON TPD.ConsultantID = XC.ConsultantID
    END -- End If
        
  -- Drop temporary tables.

  DROP TABLE #Task
  DROP TABLE #Assignment
  DROP TABLE #Expense
  DROP TABLE #Consultant
  DROP TABLE #Activity

end  

  INSERT INTO RPWBSLevelFormat
    (WBSFormatID,
     PlanID,
     FmtLevel,
     WBSType,
     WBSMatch,
     CreateUser,
     ModUser,
     CreateDate,
     ModDate)
    SELECT 
      REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS WBSFormatID,
      @strNewPlanID,
      FmtLevel,
      WBSType,
      WBSMatch,
      @strUserName AS CreateUser,
      @strUserName AS ModUser,
      CONVERT(VARCHAR, GETUTCDATE(), 121) AS CreateDate ,
	  CONVERT(VARCHAR, GETUTCDATE(), 121) AS ModDateDate 
    FROM RPWBSLevelFormat WHERE PlanID = @strPlanID
    
  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Start copy the custom fields and grids data
  
	declare @strCustomColumn Nvarchar(max)
	declare @strCustomColumnNames Nvarchar(max)  
	declare @sqlStmt Nvarchar(max)
	
	declare @gridTableName as Nvarchar(100)
	declare @gridID as Nvarchar(100)
	
	set @strCustomColumnNames = ''

	-- First copy all Custom field data 
	DECLARE CustomColumnInfo INSENSITIVE CURSOR FOR	
		select FW_CustomColumns.Name   from FW_CustomColumns where infocenterarea = 'ProjectPlan' 
			AND  GridID = 'X'
	
	BEGIN
		OPEN CustomColumnInfo

		FETCH NEXT from CustomColumnInfo INTO @strCustomColumn
		WHILE @@FETCH_STATUS = 0
			begin
				set @strCustomColumnNames = @strCustomColumnNames + @strCustomColumn + ','
				FETCH NEXT from CustomColumnInfo INTO @strCustomColumn
			end   
		CLOSE CustomColumnInfo
		DEALLOCATE CustomColumnInfo
    END

	if  @strCustomColumnNames <> ''
		begin
			set @strCustomColumnNames =  SUBSTRING(@strCustomColumnNames,1, LEN(@strCustomColumnNames) -1)
 			set @strCustomColumn = 'PlanID, ' + @strCustomColumnNames
		--insert the custom field data for the new plan
			set @sqlStmt = 'Insert into PlanningCustomTabFields (' + @strCustomColumn  + ') '    
					  + 'Select ''' + @strNewPlanID + ''',' +  @strCustomColumnNames + ' from PlanningCustomTabFields '
				      + ' where PlanID = ''' +  @strPlanID + ''''
			Exec(@sqlStmt)			 
		end
	else
	    begin
			-- add a new record in the table PlanningCustomTabFields
			Insert Into PlanningCustomTabFields (PlanID) values (@strNewPlanID)
	    end
	
	--Now copy all grid data for the new plan
	DECLARE GridTableInfo INSENSITIVE CURSOR FOR	
		select gridID,TableName   from FW_CustomGrids where infocenterarea = 'ProjectPlan' 
			 
	BEGIN
		OPEN GridTableInfo

		FETCH NEXT from GridTableInfo INTO @gridID,@gridTableName
		WHILE @@FETCH_STATUS = 0
			begin
				-- for each grid table we need to copy records there,
				-- we get the all columns in the grid table
				
				set @strCustomColumnNames = ''
				
				DECLARE CustomColumnInfo INSENSITIVE CURSOR FOR	
					select FW_CustomColumns.Name   from FW_CustomColumns where infocenterarea = 'ProjectPlan' 
						AND  GridID = @gridID
				BEGIN
					OPEN CustomColumnInfo

					FETCH NEXT from CustomColumnInfo INTO @strCustomColumn
					WHILE @@FETCH_STATUS = 0
						begin
							set @strCustomColumnNames = @strCustomColumnNames + @strCustomColumn + ','
							FETCH NEXT from CustomColumnInfo INTO @strCustomColumn
						end   
					CLOSE CustomColumnInfo
					DEALLOCATE CustomColumnInfo
			    END	
			    if  @strCustomColumnNames <> ''
					begin
						set @strCustomColumnNames =  'seq, ' + SUBSTRING(@strCustomColumnNames,1, LEN(@strCustomColumnNames) -1)
 						set @strCustomColumn = 'PlanID, ' + @strCustomColumnNames
					--insert the custom field data for the new plan
						set @sqlStmt = 'Insert into ' + @gridTableName +  '(' + @strCustomColumn  + ') '    
									+ 'Select ''' + @strNewPlanID + ''',' +  @strCustomColumnNames + ' from ' +  @gridTableName
									+ ' where PlanID = ''' +  @strPlanID + ''''
						Exec(@sqlStmt)
					end				
				FETCH NEXT from GridTableInfo INTO @gridID,@gridTableName
			end   
		CLOSE GridTableInfo
		DEALLOCATE GridTableInfo
    END	

  -- End copy custom fields ang grids

  SET NOCOUNT OFF

END -- stCopyPlan
GO
