SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[spCCG_ProjectRoleDescriptionHistory]
	@WBS1			varchar (32),
	@Employee		varchar (32), 
	@DescriptionOld	varchar (8000),
	@DescriptionNew	varchar (8000), 
	@ModUser		varchar (225)
AS 
/* 
Copyright (c) 2018 Central Consulting Group.  All rights reserved.
11/19/2019	David Springer
			Write description to Project Description History grid
			Call this on Project Descriptions grid INSERT or CHANGE workflow.
*/
DECLARE @ModEmp varchar (32)
BEGIN
SET NOCOUNT ON

	Select @ModEmp = dbo.fnCCG_UserEmployeeNo (@ModUser)

	Insert Into Projects_RoleDescriptionHistory
	(WBS1, WBS2, WBS3, Seq, CustRoleDescriptionDate, CustRoleDescriptionEmployee, CustRoleDescriptionOld, CustRoleDescriptionNew, CustRoleDescriptionModUser)
	Values (@WBS1, ' ', ' ', Replace (NewID(), '-', ''), getDate(), @Employee, @DescriptionOld, @DescriptionNew, @ModEmp)
END
GO
