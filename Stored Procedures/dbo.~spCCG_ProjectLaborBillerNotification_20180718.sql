SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--CREATE Procedure [dbo].[spCCG_ProjectLaborBillerNotification_20180718] 
CREATE Procedure [dbo].[~spCCG_ProjectLaborBillerNotification_20180718] 
AS
/*
Copyright 2017 (c) Central Consulting Group.   All rights reserved.
08/24/2017 David Springer
           Write Biller into EI CCG_Email table with regular projects with BillExt = 0.

- REVISED 1/5/2018 Jackson Gray
- REVISED 5/4/2018 Jackson Gray and Conrad Harrison - Changes and Original Coding Commented
- REVISED 6/6/2018 Conrad Harrison - Changes and Original Coding Commented
- REVISED 7/18/2018 Conrad Harrison - Accounting for refined queries and an email sent to each biller
*/

SET NOCOUNT ON
DECLARE @Biller					varchar (8000),
        @Email					varchar (100),
		@LastName				varchar (50),
		@FirstName				varchar (25),
		@WBS1					varchar (32),
		@WBS2					varchar (7),
		@WBS3					varchar (7),
		@TransDate				datetime,
		@Employee				varchar (255),
		@Category				varchar (8000),
		@Hours					decimal (19,4),
		@Multiplier				decimal (19,4),
		@BilledInvoice			varchar (12),
        @Body					varchar (max),
		@BodyMissingFee			varchar (max),
		@BodyNeedToBeMoved		varchar (max),
		@BodyLumpSum			varchar (max),
		@CSS					varchar (max),
        @Loop1					integer,
		@CustFeeType			varchar (12),
		@Org					varchar (12),
		@Amount					decimal (19,4),
		@BillExt				decimal (19,4),
		@BillStatus				varchar (2),
		@RowCount				int,
		@RowCountIncorrect		int,
		@RowCountMissingFee		int,
		@RowCountNeedToBeMoved	int,
		@RowCountLumpSum		int,
		-- INDIVIDUAL BILLER ROWCOUNT TOTALS
		@RowCountIncorrectTOTAL			int,
		@RowCountMissingFeeTOTAL		int,
		@RowCountNeedToBeMovedTOTAL		int,
		@RowCountLumpSumTOTAL			int,

		@Fee					varchar (25),
		@FeeType				varchar (25),
		@ChargeType				varchar (25),
		@SubLevel				varchar (25),
		@CustBillingType		varchar (25),
		@Status					varchar (25),
		@CustJTDBilled			varchar (25),
		@Header					varchar (100),
		
		--**TESTING EMAILS
		@TestEmail1									VARCHAR(100),
		@TestEmail2									VARCHAR(100),
		@TestEmail3									VARCHAR(100),
		@TestEmail4									VARCHAR(100),
		@TestEmail5									VARCHAR(100),
		@TestEmail6									VARCHAR(100),
		@TestEmail7									VARCHAR(100),
		@TestEmail8									VARCHAR(100),
		@TestEmailDestination						VARCHAR(100)

		SET @RowCount = 0
		SET @RowCountIncorrect = 0
		SET @RowCountMissingFee	= 0
		SET @RowCountNeedToBeMoved = 0
		SET @RowCountLumpSum = 0

		-- INDIVIDUAL BILLER ROWCOUNT TOTALS
		SET @RowCountIncorrectTOTAL = 0
		SET @RowCountMissingFeeTOTAL = 0
		SET @RowCountNeedToBeMovedTOTAL = 0
		SET @RowCountLumpSumTOTAL = 0
		SET @Header = ''
		SET @Body = ''
		SET @BodyMissingFee = ''
		SET @BodyNeedToBeMoved = ''
		SET @BodyLumpSum = ''
		
		--SETTING TEST EMAILS
		SET @TestEmail1	= 'jsagel@halff.com'							
		SET @TestEmail2	= 'jsagel@halff.com'								
		SET @TestEmail3 = 'jsagel@halff.com'							
		SET @TestEmail4	= 'jsagel@halff.com'							
		SET @TestEmail5	= 'jsagel@halff.com'								
		SET @TestEmail6	= 'jsagel@halff.com'							
		SET @TestEmail7	= 'jsagel@halff.com'	
		SET @TestEmail8	= 'jsagel@halff.com'		

		--SET @TestEmail1	= 'dfurr@halff.com'							
		--SET @TestEmail2	= 'dfurr@halff.com'								
		--SET @TestEmail3 = 'dfurr@halff.com'							
		--SET @TestEmail4	= 'dfurr@halff.com'							
		--SET @TestEmail5	= 'dfurr@halff.com'								
		--SET @TestEmail6	= 'dfurr@halff.com'							
		--SET @TestEmail7	= 'dfurr@halff.com'	
		--SET @TestEmail8	= 'dfurr@halff.com'
	
		--SET @TestEmail1	= 'dcoyer@halff.com'							
		--SET @TestEmail2	= 'jgray@halff.com'								
		--SET @TestEmail3	= 'matthewcestarte@halff.com'							
		--SET @TestEmail4	= 'ehajek@halff.com'							
		--SET @TestEmail5	= 'dfurr@halff.com'								
		--SET @TestEmail6	= 'brackenadams@halff.com'							
		--SET @TestEmail7	= 'jaust@halff.com'	
		--SET @TestEmail8	= 'sgates@halff.com'


	-- CSS style rules
	Set @CSS = 
'
<style type="text/css">
	p {padding-top:15px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;}
	td, th {border: 1px solid black;padding: 5px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;font-size:10.5pt;}
	th {background-color: #6E6E6E;color: white;text-align:center;}
	.c1, .c2,. c3, .c4, .c5, .c6, .c9 {text-align:left;}
	.c7, .c8 , .c4r, .c5r {text-align:right;}
</style>
'

DECLARE curBiller insensitive cursor for
	Select Distinct IsNull (p.Biller, '001975') as Biller, e.EMail, e.LastName, e.FirstName
	From PR p, LD l, EMMain e
	Where p.WBS1 = l.WBS1
		and p.WBS2 = ' '  --- project level for Biller
		and p.WBS1 not like 'Z%'
		and p.ChargeType = 'R'
		and p.Biller = e.Employee
		and p.Status <> 'D'
	order by Biller 
		
	SET @Body = ''
	SET	@BodyMissingFee	=''		
	SET	@BodyNeedToBeMoved =''		
	SET	@BodyLumpSum =''			

-- Get each biller
OPEN curBiller
FETCH NEXT FROM curBiller INTO @Biller, @Email, @LastName, @FirstName
While @@FETCH_STATUS = 0
BEGIN

	SET @Header = '<p><strong>' + IsNull(@LastName, '') + ', ' + IsNull(@FirstName, '') + '</strong></p>'
	SET @RowCount = @RowCount + 1

	--SETTING TEST EMAILS
	Set @TestEmailDestination =
		CASE WHEN @RowCount = 1 THEN @TestEmail1
			 WHEN @RowCount = 2 THEN @TestEmail2
			 WHEN @RowCount = 3 THEN @TestEmail3
			 WHEN @RowCount = 4 THEN @TestEmail4
			 WHEN @RowCount = 5 THEN @TestEmail5
			 WHEN @RowCount = 6 THEN @TestEmail6
			 WHEN @RowCount = 7 THEN @TestEmail7
			 WHEN @RowCount = 8 THEN @TestEmail8
			 Else 'jsagel@halff.com'
			 --Else 'dfurr@halff.com'
		End

	-- Get each Incorrect Labor Billing Notification
DECLARE curProjects insensitive cursor for
	SELECT * 
	FROM 
	(
		SELECT Biller,
		CustFeeType,
		[Org],
		IsNull(WBS1, '') As WBS1
		, IsNull(WBS2, '') As WBS2
		, IsNull(WBS3, '') As WBS3
		, IsNull(Name, '') As Name
		, TransDate
		, IsNull([Labor Category], '') AS [Labor Category]
		, IsNull([Hours], 0) As [Hours]
		, IsNull([Amount], 0) As [Amount]
		, [BillExt]
		, Round([BillExt] / [Amount], 2) AS [Multiplier]
		, IsNull(BillStatus, '') As BillStatus
		, IsNull(BilledInvoice, '') As BilledInvoice
			FROM 
			(
				Select distinct p.Biller
				, x.CustFeeType
				, p.org Org
				, l.WBS1
				, l.WBS2
				, l.WBS3
				, l.Name
				, l.TransDate
				, ltrim (str (l.Category)) + ' - ' + c.Description 'Labor Category'
				, l.RegHrs + l.OvtHrs + l.SpecialOvtHrs 'Hours'
				, l.RegAmt + l.OvtAmt + l.SpecialOvtAmt 'Amount'
				, l.BillExt
				, l.BillStatus
				, l.BilledInvoice
					From PR p, ProjectCustomTabFields x, LD l
							Left Join BTLaborCats c on c.Category = l.Category
					Where p.WBS1 = l.WBS1
						and x.WBS1 = l.WBS1
						and x.WBS2 = l.WBS2
						and x.WBS3 = l.WBS3
						and p.WBS2 = ' '  --- project level for Biller
						and p.WBS1 not like 'Z%'
						and p.ChargeType = 'R'
						and p.Biller = @Biller
						and l.LaborCode = '01'
						and l.BillStatus <> 'T'
						and l.TransDate > '2017-04-24'
						and l.RegAmt + l.OvtAmt <> '0'
					) AS A
				) AS B
		WHERE B.Multiplier = 0.00 or B.Multiplier = 1.00
		ORDER BY Biller, WBS1, Multiplier ASC
		
		--TESTING PRINT HERE
		--Print @Header
		Print '*****************************************************************************************'
		PRINT @Header
		Print 'Row Count: ' + CAST(@RowCount AS VARCHAR(MAX)) + ' Biller #: ' + CAST(@Biller AS VARCHAR(MAX)) + ' Last Name: ' + CAST(@LastName AS VARCHAR(MAX)) --+ ' Fetch Status: ' + CAST(@@Fetch_Status as varchar(max))
		Print '*****************************************************************************************'
		-- Open the table with headers
		Set @Body = 
		'<p><strong>Incorrect Billing Extensions</strong></p> 
		<table cellpadding=''5'' cellspacing=''0'' style=''border:1px solid black;border-collapse:collapse;''>
			<thead>
				<tr>
					<th>Project</th>
					<th>Phase</th>
					<th>Task</th>
					<th>Employee</th>
					<th>Date</th>
					<th>Category</th>
					<th>Hours</th>
					<th>Multiplier</th>
					<th>Billed<br/>Invoice</th>
				</tr>
			</thead>
			<tbody>
		'

	Open curProjects
	Fetch next from curProjects Into @Biller, @CustFeeType, @Org, @WBS1, @WBS2, @WBS3, @Employee, @TransDate, @Category, @Hours, @Amount, @BillExt, @Multiplier, @BillStatus, @BilledInvoice 
	While @@FETCH_STATUS = 0
	Begin

		Set @RowCountIncorrect = @RowCountIncorrect + 1
		
		Set @Body = @Body + 
		'
		<tr>
			<td class="c1"> ' + @WBS1 + '</td>
			<td class="c2"> ' + @WBS2 + '</td>
			<td class="c3"> ' + @WBS3 + '</td>
			<td class="c4"> ' + @Employee + '</td>
			<td class="c5"> ' + Convert(varchar(50), @TransDate, 101) + '</td>
			<td class="c6"> ' + @Category + '</td>
			<td class="c7"> ' + Cast(Convert(decimal(19,2), @Hours) As varchar(50)) + '</td>
			<td class="c8"> ' + Cast(Convert(decimal(19,2), @Multiplier) As varchar(50)) + '</td>
			<td class="c9"> ' + @BilledInvoice + '</td>
		</tr>
		'
	--TESTING PRINTS HERE
	Print 'Row Count (incorrect billing): ' + CAST(@RowCountIncorrect AS VARCHAR(MAX)) + ' Biller: ' + CAST(@Biller AS VARCHAR(MAX)) + ' Last Name: ' + CAST(@LastName AS VARCHAR(MAX)) --+ ' Fetch Status: ' + CAST(@@Fetch_Status as varchar(max))

	Fetch next from curProjects Into @Biller, @CustFeeType, @Org, @WBS1, @WBS2, @WBS3, @Employee, @TransDate, @Category, @Hours, @Amount, @BillExt, @Multiplier, @BillStatus, @BilledInvoice 
	End
	Close curProjects
	Deallocate curProjects

	-- Close the table
	Set @Body = @Body + 
		'	</tbody>
		</table>
		'

	SET @RowCountIncorrectTOTAL = @RowCountIncorrect
		--TESTING PRINTS HERE
	Print 'Row Count TOTAL (incorrect billing): ' + CAST(@RowCountIncorrectTOTAL AS VARCHAR(MAX)) 

	
-- INSERT MISSING FEE TABLES HERE

	DECLARE curBillerMissingFee insensitive cursor for
			Select p.WBS1, p.WBS2, p.WBS3, IsNull (p.Biller, '001975') Biller, e.EMail, e.LastName, e.FirstName
			From PR p, EMMain e, ProjectCustomTabFields x
						  Where p.WBS1 not like 'Z%'
							and p.WBS1 not like '%CATC%'
							and p.WBS1 = x.WBS1
							and p.WBS2 = x.WBS2
							and p.WBS3 = x.WBS3
							and p.Biller = e.Employee
							and p.Biller = @Biller
							and exists (Select 'x' From PR Where WBS1 = x.WBS1 and WBS2 = ' ' and Status <> 'D')
							and p.ChargeType = 'R'
							and (x.CustFeeType = 'Sublevel Terms' or x.CustFeeType Is Null)
							and p.SubLevel = 'N'
							--order by 1

		SET @RowCountMissingFee	= 0
	
	--SET HEADER
	Set @BodyMissingFee = 
	'<p><strong>Missing Fee Types</strong></p> 
	<table cellpadding=''5'' cellspacing=''0'' style=''border:1px solid black;border-collapse:collapse;''>
		<thead>
			<tr>
				<th>Project</th>
				<th>Phase</th>
				<th>Task</th>
			</tr>
		</thead>
		<tbody>
	'

	--Get each biller
	OPEN curBillerMissingFee
	FETCH NEXT FROM curBillerMissingFee INTO @WBS1, @WBS2, @WBS3, @Biller, @Email, @LastName, @FirstName
	While @@FETCH_STATUS = 0
	BEGIN

	
		--Print 'TESTING @WBS1 INPUT: ' + CAST(@WBS1 as varchar(max)) 
		--Print 'TESTING @BILLER INPUT: ' + CAST(@Biller as varchar(max)) 
		--Print 'TESTING @EMAIL INPUT: ' + CAST(@Email as varchar(max)) 
		--Print 'TESTING @LASTNAME INPUT: ' + CAST(@LastName as varchar(max)) 
		--Print 'TESTING @FIRSTNAME INPUT: ' + CAST(@FirstName as varchar(max)) 

	--Print 'Previous Missing Fee RowCount: ' + CAST(@RowCountMissingFee as varchar(max))
	SET @RowCountMissingFee = @RowCountMissingFee + 1
	--Print 'Previous Missing Fee RowCount: ' + CAST(@RowCountMissingFee as varchar(max))

	Set @BodyMissingFee = @BodyMissingFee + 
		'<tr>
			<td class="c1"> ' + @WBS1 + '</td>
			<td class="c2"> ' + @WBS2 + '</td>
			<td class="c3"> ' + @WBS3 + '</td>
		</tr>
		'
	
	--TESTING PRINTS HERE
	Print 'Row Count (Missing Fee): ' + CAST(@RowCountMissingFee AS VARCHAR(MAX)) + ' Biller: ' + CAST(@Biller AS VARCHAR(MAX)) + ' Last Name: ' + CAST(@LastName AS VARCHAR(MAX)) --+ ' Fetch Status: ' + CAST(@@Fetch_Status as varchar(max))

	FETCH NEXT FROM curBillerMissingFee INTO @WBS1, @WBS2, @WBS3, @Biller, @Email, @LastName, @FirstName
	END
	CLOSE curBillerMissingFee
	DEALLOCATE curBillerMissingFee

	--Close the table
	Set @BodyMissingFee = @BodyMissingFee + 
		'	</tbody>
		</table>
		'

	SET @RowCountMissingFeeTOTAL = @RowCountMissingFee
	--TESTING PRINTS HERE
	Print 'Row Count TOTAL (Missing Fee): ' + CAST(@RowCountMissingFeeTOTAL AS VARCHAR(MAX)) 
	--Print @BodyMissingFee

	---- INSERTING MISSING FEE TABLES ABOVE

	---- INSERT INCORRECT BILLING TYPE OR BILLED AMOUNTS NEED TO BE MOVED TABLES HERE
		DECLARE curBillerNeedtoBeMoved insensitive cursor for

		Select distinct p.WBS1,
				p.Biller
				, x.CustFeeType
				, p.ChargeType
				, x.CustJTDBilled
				, p.org Org
				, p.WBS2
				, p.WBS3
				, p.SubLevel
				, p.[Status]

		 FROM PR p, ProjectCustomTabFields x, EMMain e
									  Where p.WBS1 not like 'Z%'
									  and exists (Select 'x' From PR Where WBS1 = x.WBS1 and WBS2 = ' ' and Status <> 'D')
									  and p.ChargeType = 'R'
									  and p.WBS1 = x.WBS1
									  and p.WBS2 = x.WBS2
									  and p.WBS3 = x.WBS3
									  and p.Biller = e.Employee
									  and p.Biller = @Biller
									  and x.CustFeeType = 'Non-Billable'
									  and x.CustJTDBilled <> 0

		SET @RowCountNeedToBeMoved = 0
		
					-- Open the table with headers
			Set @BodyNeedToBeMoved = 
	'<p><strong>Incorrect Billing Type or Billed Amounts Need to be Moved</strong></p> 
	<table cellpadding=''5'' cellspacing=''0'' style=''border:1px solid black;border-collapse:collapse;''>
		<thead>
			<tr>
				<th>Project</th>
				<th>Phase</th>
				<th>Task</th>
				<th>Fee Type</th>
				<th>Billed</th>
			</tr>
		</thead>
		<tbody>
	'

		-- Get each biller
		OPEN curBillerNeedtoBeMoved
		FETCH NEXT FROM curBillerNeedtoBeMoved INTO @WBS1, @Biller, @CustFeeType, @ChargeType, @CustJTDBilled, @Org, @WBS2, @WBS3, @SubLevel, @Status
		While @@FETCH_STATUS = 0
		BEGIN
				--Print 'Previous Need to Be Moved RowCount: ' + CAST(@RowCountNeedToBeMoved as varchar(max))
				SET @RowCountNeedToBeMoved = @RowCountNeedToBeMoved + 1
				--Print 'Current Need to Be Moved RowCount: ' + CAST(@RowCountNeedToBeMoved as varchar(max))

				IF @CustJTDBilled IS NOT NULL
					BEGIN
						SET @CustJTDBilled = REPLACE(@CustJTDBilled, ',', '')
						DECLARE @d3 decimal(19,4) 
						SET @d3 = CAST(@CustJTDBilled as decimal(19,4))
						SET @CustJTDBilled = FORMAT(@d3, '$#,0.00;($#,0.00)')
					END

				Set @BodyNeedToBeMoved = @BodyNeedToBeMoved + 
	'		<tr>
				<td class="c1"> ' + @WBS1 + '</td>
				<td class="c2"> ' + @WBS2 + '</td>
				<td class="c3"> ' + @WBS3 + '</td>
				<td class="c4"> ' + @CustFeeType + '</td>
				<td class="c5r"> ' + @CustJTDBilled + '</td>
			</tr>
	'
		--TESTING PRINTS HERE
		Print 'Row Count (To Be Moved): ' + CAST(@RowCountNeedToBeMoved AS VARCHAR(MAX)) + ' Biller: ' + CAST(@Biller AS VARCHAR(MAX)) + ' Last Name: ' + CAST(@LastName AS VARCHAR(MAX)) 

	
		FETCH NEXT FROM curBillerNeedtoBeMoved INTO @WBS1, @Biller, @CustFeeType, @ChargeType, @CustJTDBilled, @Org, @WBS2, @WBS3, @SubLevel, @Status
		END
		CLOSE curBillerNeedtoBeMoved
		DEALLOCATE curBillerNeedtoBeMoved
		
					Set @BodyNeedToBeMoved = @BodyNeedToBeMoved + 
	'	</tbody>
	</table>
	'

	SET @RowCountNeedToBeMovedTOTAL = @RowCountNeedToBeMoved
	--TESTING PRINTS HERE
	Print 'Row Count TOTAL (To Be Moved): ' + CAST(@RowCountNeedToBeMovedTOTAL AS VARCHAR(MAX)) 
	--Print @BodyNeedToBeMoved

	---- INSERT INCORRECT BILLING TYPE OR BILLED AMOUNTS NEED TO BE MOVED TABLES ABOVE
	
	---- INSERT LUMP SUM LOCATION CLOSED WHEN REMAINING TO BE BILLED TABLES HERE

		DECLARE curBillerLumpSum insensitive cursor for
		Select p.WBS1
		, p.WBS2
		, p.WBS3
		, p.Biller
		, p.Fee + p.ConsultFee
		, p.ChargeType
		, p.[Status]
		, x.CustJTDBilled
		, x.CustBillingType

		   FROM PR p, ProjectCustomTabFields x, EMMain e
									  Where p.WBS1 not like 'Z%'
									  and p.WBS1 = x.WBS1
									  and p.WBS2 = x.WBS2
									  and p.WBS3 = x.WBS3
								      and p.Biller = e.Employee
									  and p.Biller = @Biller
									  and exists (Select 'x' From PR Where WBS1 = x.WBS1 and WBS2 = ' ' and Status <> 'D')
									  and p.ChargeType = 'R'
									  and x.CustBillingType like 'Lump Sum%'
									  and x.CustJTDBilled <> (p.FeeDirLab + p.FeeDirExp + p.ConsultFee)
									  and p.Status = 'D'
									  order by 1, 2, 3

		SET @RowCountLumpSum = 0
									  			Set @BodyLumpSum = 
	'<p><strong>Lump Sum Location Closed When Remaining to be Billed</strong></p> 
	<table cellpadding=''5'' cellspacing=''0'' style=''border:1px solid black;border-collapse:collapse;''>
		<thead>
			<tr>
				<th>Project</th>
				<th>Phase</th>
				<th>Task</th>
				<th>Fee</th>
				<th>Billed</th>
			</tr>
		</thead>
		<tbody>
	'

		OPEN curBillerLumpSum
		FETCH NEXT FROM curBillerLumpSum INTO @WBS1, @WBS2, @WBS3, @Biller, @Fee, @ChargeType, @Status, @CustJTDBilled, @CustBillingType
		While @@FETCH_STATUS = 0
		BEGIN
				--Print 'Previous Lump Sum Row Count: ' + CAST(@RowCountLumpSum as varchar(max))
				SET @RowCountLumpSum = @RowCountLumpSum + 1
				--Print 'Previous CURRENT Lump Sum Row Count: ' + CAST(@RowCountLumpSum as varchar(max))

				IF @Fee IS NOT NULL
					BEGIN
						SET @Fee = REPLACE(@Fee, ',', '')
						DECLARE @d decimal(19,4) 
						SET @d = CAST(@Fee as decimal(19,4))
						SET @Fee = FORMAT(@d, '$#,0.00;($#,0.00)')
					END

				IF @Fee IS NOT NULL
					BEGIN
						SET @CustJTDBilled = REPLACE(@CustJTDBilled, ',', '')
						DECLARE @d2 decimal(19,4) 
						SET @d2 = CAST(@CustJTDBilled as decimal(19,4))
						SET @CustJTDBilled = FORMAT(@d2, '$#,0.00;($#,0.00)')
					END

				Set @BodyLumpSum = @BodyLumpSum + 
	'		<tr>
				<td class="c1"> ' + @WBS1 + '</td>
				<td class="c2"> ' + @WBS2 + '</td>
				<td class="c3"> ' + @WBS3 + '</td>
				<td class="c4r"> ' + @Fee + '</td>
				<td class="c5r"> ' + @CustJTDBilled + '</td>
			</tr>
	'
		--TESTING PRINTS HERE
		Print 'Row Count (Lump Sum): ' + CAST(@RowCountLumpSum AS VARCHAR(MAX)) + ' Biller: ' + CAST(@Biller AS VARCHAR(MAX)) + ' Last Name: ' + CAST(@LastName AS VARCHAR(MAX))

		FETCH NEXT FROM curBillerLumpSum INTO @WBS1, @WBS2, @WBS3, @Biller, @Fee, @ChargeType, @Status, @CustJTDBilled, @CustBillingType
		END
		CLOSE curBillerLumpSum
		DEALLOCATE curBillerLumpSum

		
			Set @BodyLumpSum = @BodyLumpSum + 
	'	</tbody>
	</table>
	'
	
	SET @RowCountLumpSumTOTAL = @RowCountLumpSum
	--TESTING PRINTS HERE
	Print 'Row Count TOTAL (Lump Sum): ' + CAST(@RowCountLumpSumTOTAL AS VARCHAR(MAX)) 
	--Print @BodyLumpSum

--LUMP SUM REMAINING TO BE BILLED ABOVE

--**ADJUST EMAIL DELIVERABLE MESSAGE FOR EACH ROWCOUNT IF STATEMENT AND EACH BODY

----CASE 1
	IF @RowCountIncorrectTOTAL > 0 and @RowCountMissingFeeTOTAL > 0 and @RowCountNeedToBeMovedTOTAL > 0 and @RowCountLumpSumTOTAL > 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Incorrect Billing Extension', @CSS + @Header + @Body 
			+ @BodyMissingFee + @BodyNeedToBeMoved + @BodyLumpSum
			, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END

--CASE 2
	IF @RowCountIncorrectTOTAL > 0 and @RowCountMissingFeeTOTAL > 0 and @RowCountNeedToBeMovedTOTAL > 0 and @RowCountLumpSumTOTAL = 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Incorrect Billing Extension', @CSS + @Header + @Body 
			+ @BodyMissingFee + @BodyNeedToBeMoved 
			, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END

--CASE 3
	IF @RowCountIncorrectTOTAL > 0 and @RowCountMissingFeeTOTAL > 0 and @RowCountNeedToBeMovedTOTAL = 0 and @RowCountLumpSumTOTAL = 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Incorrect Billing Extension', @CSS + @Header + @Body 
			+ @BodyMissingFee 
			, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END

--CASE 4
	IF @RowCountIncorrectTOTAL > 0 and @RowCountMissingFeeTOTAL > 0 and @RowCountNeedToBeMovedTOTAL = 0 and @RowCountLumpSumTOTAL > 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Incorrect Billing Extension', @CSS + @Header + @Body 
			+ @BodyMissingFee + @BodyLumpSum
			, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END

--CASE 5
	IF @RowCountIncorrectTOTAL > 0 and @RowCountMissingFeeTOTAL = 0 and @RowCountNeedToBeMovedTOTAL > 0 and @RowCountLumpSumTOTAL > 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Incorrect Billing Extension', @CSS + @Header + @Body 
			+ @BodyNeedToBeMoved + @BodyLumpSum
			, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END

--CASE 6
	IF @RowCountIncorrectTOTAL > 0 and @RowCountMissingFeeTOTAL = 0 and @RowCountNeedToBeMovedTOTAL = 0 and @RowCountLumpSumTOTAL > 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Incorrect Billing Extension', @CSS + @Header + @Body 
			+ @BodyLumpSum
			, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END

--CASE 7
	IF @RowCountIncorrectTOTAL > 0 and @RowCountMissingFeeTOTAL = 0 and @RowCountNeedToBeMovedTOTAL = 0 and @RowCountLumpSumTOTAL = 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Incorrect Billing Extension', @CSS + @Header + @Body 
			, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END

--CASE 8
	IF @RowCountIncorrectTOTAL > 0 and @RowCountMissingFeeTOTAL = 0 and @RowCountNeedToBeMovedTOTAL > 0 and @RowCountLumpSumTOTAL = 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Incorrect Billing Extension', @CSS + @Header + @Body 
			+ @BodyNeedToBeMoved
			, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END

--CASE 9
	IF @RowCountIncorrectTOTAL = 0 and @RowCountMissingFeeTOTAL > 0 and @RowCountNeedToBeMovedTOTAL > 0 and @RowCountLumpSumTOTAL > 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Incorrect Billing Extension', @CSS + @Header  
			+ @BodyMissingFee + @BodyNeedToBeMoved + @BodyLumpSum
			, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END


--CASE 10
	IF @RowCountIncorrectTOTAL = 0 and (@RowCountMissingFeeTOTAL > 0 and @RowCountNeedToBeMovedTOTAL > 0 and @RowCountLumpSumTOTAL = 0)
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Incorrect Billing Extension', @CSS + @Header  
			+ @BodyMissingFee + @BodyNeedToBeMoved
			, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END

--CASE 11
	IF @RowCountIncorrectTOTAL = 0 and @RowCountMissingFeeTOTAL > 0 and @RowCountNeedToBeMovedTOTAL = 0 and @RowCountLumpSumTOTAL = 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Incorrect Billing Extension', @CSS + @Header  
			+ @BodyMissingFee 
			, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END

--CASE 12
	IF @RowCountIncorrectTOTAL = 0 and @RowCountMissingFeeTOTAL > 0 and @RowCountNeedToBeMovedTOTAL = 0 and @RowCountLumpSumTOTAL > 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Incorrect Billing Extension', @CSS + @Header  
			+ @BodyMissingFee + @BodyLumpSum
			, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END

--CASE 13
	IF @RowCountIncorrectTOTAL = 0 and @RowCountMissingFeeTOTAL = 0 and @RowCountNeedToBeMovedTOTAL > 0 and @RowCountLumpSumTOTAL > 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Incorrect Billing Extension', @CSS + @Header  
			+ @BodyNeedToBeMoved + @BodyLumpSum
			, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END

--CASE 14
	IF @RowCountIncorrectTOTAL = 0 and @RowCountMissingFeeTOTAL = 0 and @RowCountNeedToBeMovedTOTAL = 0 and @RowCountLumpSumTOTAL > 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Incorrect Billing Extension', @CSS + @Header  
			 + @BodyLumpSum
			, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END

--CASE 15
	--IF @RowCountIncorrectTOTAL = 0 and @RowCountMissingFeeTOTAL = 0 and @RowCountNeedToBeMovedTOTAL = 0 and @RowCountLumpSumTOTAL = 0
	--	BEGIN
	--		Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
	--		Values ('Incorrect Billing Extensions', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Incorrect Billing Extension', @CSS + @Header  
	--		--INSERT A CONGRATS MESSAGE
	--		, getDate(), 1)
	--		--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
	--	END

--CASE 16
	IF @RowCountIncorrectTOTAL = 0 and @RowCountMissingFeeTOTAL = 0 and @RowCountNeedToBeMovedTOTAL > 0 and @RowCountLumpSumTOTAL = 0
		BEGIN
			Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
			Values ('Incorrect Billing Extensions', @TestEmailDestination, @TestEmailDestination, @TestEmailDestination, 'Incorrect Billing Extension', @CSS + @Header  
			+ @BodyNeedToBeMoved
			, getDate(), 1)
			--Values ('Incorrect Billing Extensions', 'kcarter@halff.com', @Email, 'kcarter@halff.com', 'Incorrect Billing Extension', @CSS + @Body, getDate(), 1)
		END

FETCH NEXT FROM curBiller INTO @Biller, @Email, @LastName, @FirstName
END
CLOSE curBiller
DEALLOCATE curBiller
GO
