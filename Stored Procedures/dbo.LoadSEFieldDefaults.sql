SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[LoadSEFieldDefaults]
 @applyTabSecurity varchar(1),
 @SpecificRole nvarchar(20) = NULL
AS
BEGIN

PRINT ' '
PRINT 'Loading SEField Defaults'

declare  @Role nvarchar(20) = '' COLLATE database_default
declare  @InfocenterArea nvarchar(32) = '' COLLATE database_default
declare  @Access varchar(1) = '' COLLATE database_default
declare  @AccessAllTabs varchar(1) = '' COLLATE database_default

 ---- Temp table to store all known fields
Create Table dbo.#KnownFields
(infocenterArea nvarchar(32) COLLATE database_default,
gridid nvarchar(125) COLLATE database_default,
componentID nvarchar(125) COLLATE database_default,
tablename nvarchar(100) COLLATE database_default,
fieldName nvarchar(125) COLLATE database_default,
tabID nvarchar(125) COLLATE database_default,
essential bit
)

--- Temp table to store the default fields that are added for a role that does not have access to a hub
Create Table dbo.#DefaultFields
(infocenterArea nvarchar(32) COLLATE database_default,
gridid nvarchar(125) COLLATE database_default,
componentID nvarchar(125) COLLATE database_default,
tablename nvarchar(100) COLLATE database_default,
fieldName nvarchar(125) COLLATE database_default
)

-- Load Temp table
Insert into dbo.#KnownFields 
Select 'Activity' as infocenterArea, 'X' as GridID, 'FW_ATTACHMENTS' as componentID, null as tablename, null as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'CLActivity' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'ActivityFileLinks' as componentID, null as tablename, null as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'ActivityEmployeeContacts' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.WBS3' as componentID, 'Activity' as tablename, 'WBS3' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.WBS2' as componentID, 'Activity' as tablename, 'WBS2' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.WBS1' as componentID, 'Activity' as tablename, 'WBS1' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.VendorName' as componentID, 'Activity' as tablename, 'VendorName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.VendorContactName' as componentID, 'Activity' as tablename, 'VendorContactName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.Vendor' as componentID, 'Activity' as tablename, 'Vendor' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.UpdateID' as componentID, 'Activity' as tablename, 'UpdateID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.TypeName' as componentID, 'Activity' as tablename, 'TypeName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.Type' as componentID, 'Activity' as tablename, 'Type' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.TimeRecurrence' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.TimeFromValue' as componentID, 'Activity' as tablename, 'TimeFromValue' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.TimeFromInterval' as componentID, 'Activity' as tablename, 'TimeFromInterval' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.TaskCompletionDate' as componentID, 'Activity' as tablename, 'TaskCompletionDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.Subject' as componentID, 'Activity' as tablename, 'Subject' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.StartDateObj' as componentID, 'Activity' as tablename, 'StartDateObj' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.StartDate' as componentID, 'Activity' as tablename, 'StartDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ShowTimeAs' as componentID, 'Activity' as tablename, 'ShowTimeAs' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RP' as componentID, 'Activity' as tablename, 'RP' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ReminderUnit' as componentID, 'Activity' as tablename, 'ReminderUnit' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ReminderMinHrDay' as componentID, 'Activity' as tablename, 'ReminderMinHrDay' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ReminderInd' as componentID, 'Activity' as tablename, 'ReminderInd' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ReminderDate' as componentID, 'Activity' as tablename, 'ReminderDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrYearlyOccurMonth' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurMonth' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrYearlyOccurInd' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurInd' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrYearlyOccurDay' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurDay' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrYearlyOccur' as componentID, 'Activity' as tablename, 'RecurrYearlyOccur' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrYearlyMonth' as componentID, 'Activity' as tablename, 'RecurrYearlyMonth' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrYearlyDay' as componentID, 'Activity' as tablename, 'RecurrYearlyDay' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrWeeklyWed' as componentID, 'Activity' as tablename, 'RecurrWeeklyWed' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrWeeklyTue' as componentID, 'Activity' as tablename, 'RecurrWeeklyTue' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrWeeklyThu' as componentID, 'Activity' as tablename, 'RecurrWeeklyThu' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrWeeklySun' as componentID, 'Activity' as tablename, 'RecurrWeeklySun' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrWeeklySat' as componentID, 'Activity' as tablename, 'RecurrWeeklySat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrWeeklyMon' as componentID, 'Activity' as tablename, 'RecurrWeeklyMon' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrWeeklyFri' as componentID, 'Activity' as tablename, 'RecurrWeeklyFri' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrWeeklyFreq' as componentID, 'Activity' as tablename, 'RecurrWeeklyFreq' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrType' as componentID, 'Activity' as tablename, 'RecurrType' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrStartDate' as componentID, 'Activity' as tablename, 'RecurrStartDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrMonthlyOccurInd' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurInd' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrMonthlyOccurFreq' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurFreq' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrMonthlyOccurDay' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurDay' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrMonthlyOccur' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccur' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrMonthlyFreq' as componentID, 'Activity' as tablename, 'RecurrMonthlyFreq' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrMonthlyDay' as componentID, 'Activity' as tablename, 'RecurrMonthlyDay' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrID' as componentID, 'Activity' as tablename, 'RecurrID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrEndType' as componentID, 'Activity' as tablename, 'RecurrEndType' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrEndDate' as componentID, 'Activity' as tablename, 'RecurrEndDate' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrenceInd' as componentID, 'Activity' as tablename, 'RecurrenceInd' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrDailyWeekDay' as componentID, 'Activity' as tablename, 'RecurrDailyWeekDay' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.RecurrDailyFreq' as componentID, 'Activity' as tablename, 'RecurrDailyFreq' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ReadID' as componentID, 'Activity' as tablename, 'ReadID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ProjName' as componentID, 'Activity' as tablename, 'ProjName' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.PrivateInd' as componentID, 'Activity' as tablename, 'PrivateInd' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.PriorityDesc' as componentID, 'Activity' as tablename, 'PriorityDesc' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.Priority' as componentID, 'Activity' as tablename, 'Priority' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.PrimaryClient' as componentID, 'Activity' as tablename, 'PrimaryClient' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.OwnerName' as componentID, 'Activity' as tablename, 'OwnerName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.Owner' as componentID, 'Activity' as tablename, 'Owner' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.OutlookType' as componentID, 'Activity' as tablename, 'OutlookType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.Notes' as componentID, 'Activity' as tablename, 'Notes' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ModUser' as componentID, 'Activity' as tablename, 'ModUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ModUserName' as componentID, 'Activity' as tablename, 'ModUserName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ModDate' as componentID, 'Activity' as tablename, 'ModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.MktName' as componentID, 'Activity' as tablename, 'MktName' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.MaxOccurences' as componentID, 'Activity' as tablename, 'MaxOccurences' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.Location' as componentID, 'Activity' as tablename, 'Location' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.LeadID' as componentID, 'Activity' as tablename, 'LeadID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.isTask' as componentID, 'Activity' as tablename, 'isTask' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.isCalendar' as componentID, 'Activity' as tablename, 'isCalendar' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.InstanceStartDate' as componentID, 'Activity' as tablename, 'InstanceStartDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.HasNotes' as componentID, 'Activity' as tablename, 'HasNotes' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.EndDateSort' as componentID, 'Activity' as tablename, 'EndDateSort' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.EndDateObj' as componentID, 'Activity' as tablename, 'EndDateObj' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.EndDateFilter' as componentID, 'Activity' as tablename, 'EndDateFilter' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.EndDate' as componentID, 'Activity' as tablename, 'EndDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.Employee' as componentID, 'Activity' as tablename, 'Employee' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.Duration' as componentID, 'Activity' as tablename, 'Duration' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.CreateUser' as componentID, 'Activity' as tablename, 'CreateUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.CreateUserName' as componentID, 'Activity' as tablename, 'CreateUserName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.CreateEmployee' as componentID, 'Activity' as tablename, 'CreateEmployee' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.CreateDate' as componentID, 'Activity' as tablename, 'CreateDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ContactPhoneForVendor' as componentID, 'Activity' as tablename, 'ContactPhoneForVendor' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ContactNameForVendor' as componentID, 'Activity' as tablename, 'ContactNameForVendor' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ContactName' as componentID, 'Activity' as tablename, 'ContactName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ContactIDForVendor' as componentID, 'Activity' as tablename, 'ContactIDForVendor' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ContactID' as componentID, 'Activity' as tablename, 'ContactID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.CompletionInd' as componentID, 'Activity' as tablename, 'CompletionInd' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ClientName' as componentID, 'Activity' as tablename, 'ClientName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ClientID' as componentID, 'Activity' as tablename, 'ClientID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.CampaignID' as componentID, 'Activity' as tablename, 'CampaignID' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.Attendees' as componentID, 'Activity' as tablename, 'Attendees' as fieldname, 'noassociation' as tabID, 1 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.Associations' as componentID, 'Activity' as tablename, 'Associations' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.AllDayEventInd' as componentID, 'Activity' as tablename, 'AllDayEventInd' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.acType' as componentID, 'Activity' as tablename, 'acType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ActivityID' as componentID, 'Activity' as tablename, 'ActivityID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ActivityTypeFlag' as componentID, 'Activity' as tablename, 'ActivityTypeFlag' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.SingleOccurrenceDateTime' as componentID, 'Activity' as tablename, 'SingleOccurrenceDateTime' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ConversationID' as componentID, 'Activity' as tablename, 'ConversationID' as fieldname, 'noassociation' as tabID, 0 as essential union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ParentMaxOccurences' as componentID, 'Activity' as tablename, 'ParentMaxOccurences' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ParentRecurrStartDate' as componentID, 'Activity' as tablename, 'ParentRecurrStartDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ParentRecurrEndDate' as componentID, 'Activity' as tablename, 'ParentRecurrEndDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ParentRecurrEndType' as componentID, 'Activity' as tablename, 'ParentRecurrEndType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ParentRecurrType' as componentID, 'Activity' as tablename, 'ParentRecurrType' as fieldname, 'noassociation' as tabID, 0 as essential union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.OccurrenceDate' as componentID, 'Activity' as tablename, 'OccurrenceDate' as fieldname, 'noassociation' as tabID, 0 as essential union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ExceptionType' as componentID, 'Activity' as tablename, 'ExceptionType' as fieldname, 'noassociation' as tabID, 0 as essential union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.TimeZoneOffset' as componentID, 'Activity' as tablename, 'TimeZoneOffset' as fieldname, 'noassociation' as tabID, 0 as essential union
Select 'Activity' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'PKey' as componentID, 'FW_ATTACHMENTS' as tablename, 'PKey' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key3' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key3' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key2' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key2' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key1' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key1' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileSize' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileSize' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileName' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileName' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileID' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileDescription' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileDescription' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'ContentType' as componentID, 'FW_ATTACHMENTS' as tablename, 'ContentType' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'CategoryDescription' as componentID, 'FW_ATTACHMENTS' as tablename, 'CategoryDescription' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'CategoryCode' as componentID, 'FW_ATTACHMENTS' as tablename, 'CategoryCode' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Application' as componentID, 'FW_ATTACHMENTS' as tablename, 'Application' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'ActivityID' as componentID, 'FW_ATTACHMENTS' as tablename, 'ActivityID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'EMActivity' as GridID, 'WorkPhone' as componentID, 'EMActivity' as tablename, 'WorkPhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'EMActivity' as GridID, 'Title' as componentID, 'EMActivity' as tablename, 'Title' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'EMActivity' as GridID, 'Owner' as componentID, 'EMActivity' as tablename, 'Owner' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'EMActivity' as GridID, 'Org' as componentID, 'EMActivity' as tablename, 'Org' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'EMActivity' as GridID, 'Name' as componentID, 'EMActivity' as tablename, 'Name' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'EMActivity' as GridID, 'ModUser' as componentID, 'EMActivity' as tablename, 'ModUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'EMActivity' as GridID, 'ModDate' as componentID, 'EMActivity' as tablename, 'ModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'EMActivity' as GridID, 'LastFirst' as componentID, 'EMActivity' as tablename, 'LastFirst' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'EMActivity' as GridID, 'FullName' as componentID, 'EMActivity' as tablename, 'FullName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'EMActivity' as GridID, 'FirstLast' as componentID, 'EMActivity' as tablename, 'FirstLast' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'EMActivity' as GridID, 'Employee' as componentID, 'EMActivity' as tablename, 'Employee' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'EMActivity' as GridID, 'Email' as componentID, 'EMActivity' as tablename, 'Email' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'EMActivity' as GridID, 'CreateUser' as componentID, 'EMActivity' as tablename, 'CreateUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'EMActivity' as GridID, 'CreateDate' as componentID, 'EMActivity' as tablename, 'CreateDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'EMActivity' as GridID, 'ActivityID' as componentID, 'EMActivity' as tablename, 'ActivityID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'Type' as componentID, 'ContactActivity' as tablename, 'Type' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'Title' as componentID, 'ContactActivity' as tablename, 'Title' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'PrimaryInd' as componentID, 'ContactActivity' as tablename, 'PrimaryInd' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'Phone' as componentID, 'ContactActivity' as tablename, 'Phone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'Name' as componentID, 'ContactActivity' as tablename, 'Name' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'ModUser' as componentID, 'ContactActivity' as tablename, 'ModUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'ModDate' as componentID, 'ContactActivity' as tablename, 'ModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'LastFirst' as componentID, 'ContactActivity' as tablename, 'LastFirst' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'FirstLast' as componentID, 'ContactActivity' as tablename, 'FirstLast' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'Email' as componentID, 'ContactActivity' as tablename, 'Email' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'CreateUser' as componentID, 'ContactActivity' as tablename, 'CreateUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'CreateDate' as componentID, 'ContactActivity' as tablename, 'CreateDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'ContactPhotoModDate' as componentID, 'ContactActivity' as tablename, 'ContactPhotoModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'ContactID' as componentID, 'ContactActivity' as tablename, 'ContactID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'ContactHasPhoto' as componentID, 'ContactActivity' as tablename, 'ContactHasPhoto' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'Company' as componentID, 'ContactActivity' as tablename, 'Company' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'ClientID' as componentID, 'ContactActivity' as tablename, 'ClientID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'CellPhone' as componentID, 'ContactActivity' as tablename, 'CellPhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ContactActivity' as GridID, 'ActivityID' as componentID, 'ContactActivity' as tablename, 'ActivityID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'CLActivity' as GridID, 'PrimaryInd' as componentID, 'CLActivity' as tablename, 'PrimaryInd' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'CLActivity' as GridID, 'Name' as componentID, 'CLActivity' as tablename, 'Name' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'CLActivity' as GridID, 'ModUser' as componentID, 'CLActivity' as tablename, 'ModUser' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'CLActivity' as GridID, 'ModDate' as componentID, 'CLActivity' as tablename, 'ModDate' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'CLActivity' as GridID, 'FirmName' as componentID, 'CLActivity' as tablename, 'FirmName' as fieldname, 'overview' as tabID, 0 as essential union
Select 'Activity' as infocenterArea, 'CLActivity' as GridID, 'CreateUser' as componentID, 'CLActivity' as tablename, 'CreateUser' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'CLActivity' as GridID, 'CreateDate' as componentID, 'CLActivity' as tablename, 'CreateDate' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'CLActivity' as GridID, 'ClientID' as componentID, 'CLActivity' as tablename, 'ClientID' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'CLActivity' as GridID, 'ActivityID' as componentID, 'CLActivity' as tablename, 'ActivityID' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityFileLinks' as GridID, 'LinkID' as componentID, 'ActivityFileLinks' as tablename, 'LinkID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityFileLinks' as GridID, 'Graphic' as componentID, 'ActivityFileLinks' as tablename, 'Graphic' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityFileLinks' as GridID, 'FilePath' as componentID, 'ActivityFileLinks' as tablename, 'FilePath' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityFileLinks' as GridID, 'Description' as componentID, 'ActivityFileLinks' as tablename, 'Description' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityFileLinks' as GridID, 'ActivityID' as componentID, 'ActivityFileLinks' as tablename, 'ActivityID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityEmployeeContacts' as GridID, 'Title' as componentID, 'ActivityEmployeeContacts' as tablename, 'Title' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityEmployeeContacts' as GridID, 'RecordType' as componentID, 'ActivityEmployeeContacts' as tablename, 'RecordType' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityEmployeeContacts' as GridID, 'RecordID' as componentID, 'ActivityEmployeeContacts' as tablename, 'RecordID' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityEmployeeContacts' as GridID, 'PrimaryInd' as componentID, 'ActivityEmployeeContacts' as tablename, 'PrimaryInd' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityEmployeeContacts' as GridID, 'Phone' as componentID, 'ActivityEmployeeContacts' as tablename, 'Phone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityEmployeeContacts' as GridID, 'Owner' as componentID, 'ActivityEmployeeContacts' as tablename, 'Owner' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityEmployeeContacts' as GridID, 'LastFirst' as componentID, 'ActivityEmployeeContacts' as tablename, 'LastFirst' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityEmployeeContacts' as GridID, 'FirstLast' as componentID, 'ActivityEmployeeContacts' as tablename, 'FirstLast' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityEmployeeContacts' as GridID, 'Employee' as componentID, 'ActivityEmployeeContacts' as tablename, 'Employee' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityEmployeeContacts' as GridID, 'Email' as componentID, 'ActivityEmployeeContacts' as tablename, 'Email' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityEmployeeContacts' as GridID, 'ContactID' as componentID, 'ActivityEmployeeContacts' as tablename, 'ContactID' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityEmployeeContacts' as GridID, 'ContactHasPhoto' as componentID, 'ActivityEmployeeContacts' as tablename, 'ContactHasPhoto' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityEmployeeContacts' as GridID, 'ClientID' as componentID, 'ActivityEmployeeContacts' as tablename, 'ClientID' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityEmployeeContacts' as GridID, 'CellPhone' as componentID, 'ActivityEmployeeContacts' as tablename, 'CellPhone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Activity' as infocenterArea, 'ActivityEmployeeContacts' as GridID, 'ActivityID' as componentID, 'ActivityEmployeeContacts' as tablename, 'ActivityID' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'GLGroupDetail' as componentID, null as tablename, null as fieldname, 'Generaltab' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CACompanyMapping' as componentID, null as tablename, null as fieldname, 'Generaltab' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.WarningMsg' as componentID, 'CA' as tablename, 'WarningMsg' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.UnrealizedLossAccountWithName' as componentID, 'CA' as tablename, 'UnrealizedLossAccountWithName' as fieldname, 'Generaltab' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.UnrealizedLossAccountName' as componentID, 'CA' as tablename, 'UnrealizedLossAccountName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.UnrealizedLossAccount' as componentID, 'CA' as tablename, 'UnrealizedLossAccount' as fieldname, 'Generaltab' as tabID, 1 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.UnrealizedGainAccountWithName' as componentID, 'CA' as tablename, 'UnrealizedGainAccountWithName' as fieldname, 'Generaltab' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.UnrealizedGainAccountName' as componentID, 'CA' as tablename, 'UnrealizedGainAccountName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.UnrealizedGainAccount' as componentID, 'CA' as tablename, 'UnrealizedGainAccount' as fieldname, 'Generaltab' as tabID, 1 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.TypeDesc' as componentID, 'CA' as tablename, 'TypeDesc' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.Type' as componentID, 'CA' as tablename, 'Type' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.StatusDesc' as componentID, 'CA' as tablename, 'StatusDesc' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.Status' as componentID, 'CA' as tablename, 'Status' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.QBOAccountID' as componentID, 'CA' as tablename, 'QBOAccountID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.Name' as componentID, 'CA' as tablename, 'Name' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.GlobalAccount' as componentID, 'CA' as tablename, 'GlobalAccount' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.FasAccount' as componentID, 'CA' as tablename, 'FasAccount' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.Detail' as componentID, 'CA' as tablename, 'Detail' as fieldname, 'Generaltab' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.CustomCurrencyCode' as componentID, 'CA' as tablename, 'CustomCurrencyCode' as fieldname, 'noassociation' as tabID, 1 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.CashBasisRevaluation' as componentID, 'CA' as tablename, 'CashBasisRevaluation' as fieldname, 'Generaltab' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.CashBasisAccountWithName' as componentID, 'CA' as tablename, 'CashBasisAccountWithName' as fieldname, 'Generaltab' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.CashBasisAccount' as componentID, 'CA' as tablename, 'CashBasisAccount' as fieldname, 'Generaltab' as tabID, 1 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.caName' as componentID, 'CA' as tablename, 'caName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.AccountWithName' as componentID, 'CA' as tablename, 'AccountWithName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.AccountCurrencyCode' as componentID, 'CA' as tablename, 'AccountCurrencyCode' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.Account' as componentID, 'CA' as tablename, 'Account' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLTable' as GridID, 'TableNo' as componentID, 'GLTable' as tablename, 'TableNo' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLTable' as GridID, 'TableName' as componentID, 'GLTable' as tablename, 'TableName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLTable' as GridID, 'orgName' as componentID, 'GLTable' as tablename, 'orgName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLTable' as GridID, 'FilterOrg' as componentID, 'GLTable' as tablename, 'FilterOrg' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLTable' as GridID, 'FilterCode' as componentID, 'GLTable' as tablename, 'FilterCode' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLParentHeading' as GridID, 'TableNo' as componentID, 'GLParentHeading' as tablename, 'TableNo' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLParentHeading' as GridID, 'SubTotal' as componentID, 'GLParentHeading' as tablename, 'SubTotal' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLParentHeading' as GridID, 'SortOrder' as componentID, 'GLParentHeading' as tablename, 'SortOrder' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLParentHeading' as GridID, 'ShowDetail' as componentID, 'GLParentHeading' as tablename, 'ShowDetail' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLParentHeading' as GridID, 'GLGroup' as componentID, 'GLParentHeading' as tablename, 'GLGroup' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLParentHeading' as GridID, 'ExcludeTotal' as componentID, 'GLParentHeading' as tablename, 'ExcludeTotal' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLParentHeading' as GridID, 'Description' as componentID, 'GLParentHeading' as tablename, 'Description' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLParentDetail' as GridID, 'TableNo' as componentID, 'GLParentDetail' as tablename, 'TableNo' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLParentDetail' as GridID, 'GLGroup' as componentID, 'GLParentDetail' as tablename, 'GLGroup' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLParentDetail' as GridID, 'DetailGroupID' as componentID, 'GLParentDetail' as tablename, 'DetailGroupID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLGroupHeading' as GridID, 'TableNo' as componentID, 'GLGroupHeading' as tablename, 'TableNo' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLGroupHeading' as GridID, 'SubTotal' as componentID, 'GLGroupHeading' as tablename, 'SubTotal' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLGroupHeading' as GridID, 'SortOrder' as componentID, 'GLGroupHeading' as tablename, 'SortOrder' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLGroupHeading' as GridID, 'ShowDetail' as componentID, 'GLGroupHeading' as tablename, 'ShowDetail' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLGroupHeading' as GridID, 'GLGroup' as componentID, 'GLGroupHeading' as tablename, 'GLGroup' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLGroupHeading' as GridID, 'ExcludeTotal' as componentID, 'GLGroupHeading' as tablename, 'ExcludeTotal' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLGroupHeading' as GridID, 'Description' as componentID, 'GLGroupHeading' as tablename, 'Description' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLGroupDetail' as GridID, 'TableNo' as componentID, 'GLGroupDetail' as tablename, 'TableNo' as fieldname, 'Generaltab' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLGroupDetail' as GridID, 'StartAccount' as componentID, 'GLGroupDetail' as tablename, 'StartAccount' as fieldname, 'Generaltab' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLGroupDetail' as GridID, 'GroupTable' as componentID, 'GLGroupDetail' as tablename, 'GroupTable' as fieldname, 'Generaltab' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLGroupDetail' as GridID, 'GLGroup' as componentID, 'GLGroupDetail' as tablename, 'GLGroup' as fieldname, 'Generaltab' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLGroupDetail' as GridID, 'EndAccount' as componentID, 'GLGroupDetail' as tablename, 'EndAccount' as fieldname, 'Generaltab' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLGroupDetail' as GridID, 'DetailGroupName' as componentID, 'GLGroupDetail' as tablename, 'DetailGroupName' as fieldname, 'Generaltab' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLGroup' as GridID, 'GroupType' as componentID, 'GLGroup' as tablename, 'GroupType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLGroup' as GridID, 'Description' as componentID, 'GLGroup' as tablename, 'Description' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'GLGroup' as GridID, 'Code' as componentID, 'GLGroup' as tablename, 'Code' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'CACompanyMapping' as GridID, 'Firmname' as componentID, 'CACompanyMapping' as tablename, 'Firmname' as fieldname, 'Generaltab' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'CACompanyMapping' as GridID, 'Company' as componentID, 'CACompanyMapping' as tablename, 'Company' as fieldname, 'Generaltab' as tabID, 0 as essential union 
Select 'ChartOfAccounts' as infocenterArea, 'CACompanyMapping' as GridID, 'Account' as componentID, 'CACompanyMapping' as tablename, 'Account' as fieldname, 'Generaltab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'PRContactAssoc' as componentID, null as tablename, null as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'MktCampaignContactAssoc' as componentID, null as tablename, null as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'FW_ATTACHMENTS' as componentID, null as tablename, null as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'EMContactAssoc' as componentID, null as tablename, null as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'ContactToContactAssoc' as componentID, null as tablename, null as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Zip' as componentID, 'Contacts' as tablename, 'Zip' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Website' as componentID, 'Contacts' as tablename, 'Website' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.VName' as componentID, 'Contacts' as tablename, 'VName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Vendor' as componentID, 'Contacts' as tablename, 'Vendor' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.VEAddress' as componentID, 'Contacts' as tablename, 'VEAddress' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.UpdateCLAddress' as componentID, 'Contacts' as tablename, 'UpdateCLAddress' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.TopTPSubject' as componentID, 'Contacts' as tablename, 'TopTPSubject' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.TopTPStartDate' as componentID, 'Contacts' as tablename, 'TopTPStartDate' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.TopTPCreateUserName' as componentID, 'Contacts' as tablename, 'TopTPCreateUserName' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.TopTPCreateEmployee' as componentID, 'Contacts' as tablename, 'TopTPCreateEmployee' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.TopTPContactID' as componentID, 'Contacts' as tablename, 'TopTPContactID' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.TopTPActivityID' as componentID, 'Contacts' as tablename, 'TopTPActivityID' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.TLSyncModDate' as componentID, 'Contacts' as tablename, 'TLSyncModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.TLInternalKey' as componentID, 'Contacts' as tablename, 'TLInternalKey' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Title' as componentID, 'Contacts' as tablename, 'Title' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.TaxRegistrationNumber' as componentID, 'Contacts' as tablename, 'TaxRegistrationNumber' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.TaxCountryCode' as componentID, 'Contacts' as tablename, 'TaxCountryCode' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Suffix' as componentID, 'Contacts' as tablename, 'Suffix' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.StatusReason' as componentID, 'Contacts' as tablename, 'StatusReason' as fieldname, 'sidebar' as tabID, 1 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.StatusDate' as componentID, 'Contacts' as tablename, 'StatusDate' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.State' as componentID, 'Contacts' as tablename, 'State' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Source' as componentID, 'Contacts' as tablename, 'Source' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.SFLastModifiedDate' as componentID, 'Contacts' as tablename, 'SFLastModifiedDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.SFID' as componentID, 'Contacts' as tablename, 'SFID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Salutation' as componentID, 'Contacts' as tablename, 'Salutation' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Rating' as componentID, 'Contacts' as tablename, 'Rating' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.QualifiedStatus' as componentID, 'Contacts' as tablename, 'QualifiedStatus' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.QBOLastUpdated' as componentID, 'Contacts' as tablename, 'QBOLastUpdated' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.QBOIsMainContact' as componentID, 'Contacts' as tablename, 'QBOIsMainContact' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.QBOID' as componentID, 'Contacts' as tablename, 'QBOID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.ProjectDescription' as componentID, 'Contacts' as tablename, 'ProjectDescription' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.ProfessionalSuffix' as componentID, 'Contacts' as tablename, 'ProfessionalSuffix' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.PrimaryInd' as componentID, 'Contacts' as tablename, 'PrimaryInd' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.PreferredName' as componentID, 'Contacts' as tablename, 'PreferredName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.PhotoModDate' as componentID, 'Contacts' as tablename, 'PhotoModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.PhoneFormat' as componentID, 'Contacts' as tablename, 'PhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Phone' as componentID, 'Contacts' as tablename, 'Phone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Payment' as componentID, 'Contacts' as tablename, 'Payment' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.PagerFormat' as componentID, 'Contacts' as tablename, 'PagerFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Pager' as componentID, 'Contacts' as tablename, 'Pager' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Owner' as componentID, 'Contacts' as tablename, 'Owner' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.OtherAddress' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 1 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.ModUser' as componentID, 'Contacts' as tablename, 'ModUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.ModUserName' as componentID, 'Contacts' as tablename, 'ModUserName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.ModDate' as componentID, 'Contacts' as tablename, 'ModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.MiddleName' as componentID, 'Contacts' as tablename, 'MiddleName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Memo' as componentID, 'Contacts' as tablename, 'Memo' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Market' as componentID, 'Contacts' as tablename, 'Market' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.MailingAddress' as componentID, 'Contacts' as tablename, 'MailingAddress' as fieldname, 'overview' as tabID, 1 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.LastName' as componentID, 'Contacts' as tablename, 'LastName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.LastActivityField' as componentID, null as tablename, null as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.HomePhoneFormat' as componentID, 'Contacts' as tablename, 'HomePhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.HomePhone' as componentID, 'Contacts' as tablename, 'HomePhone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.HasPhoto' as componentID, 'Contacts' as tablename, 'HasPhoto' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.flName' as componentID, 'Contacts' as tablename, 'flName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirstName' as componentID, 'Contacts' as tablename, 'FirstName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmZip' as componentID, 'Contacts' as tablename, 'FirmZip' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmState' as componentID, 'Contacts' as tablename, 'FirmState' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmPager' as componentID, 'Contacts' as tablename, 'FirmPager' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmMailingAddress' as componentID, 'Contacts' as tablename, 'FirmMailingAddress' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmDescription' as componentID, 'Contacts' as tablename, 'FirmDescription' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmCountry' as componentID, 'Contacts' as tablename, 'FirmCountry' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmCity' as componentID, 'Contacts' as tablename, 'FirmCity' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmBusinessPhoneFormat' as componentID, 'Contacts' as tablename, 'FirmBusinessPhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmBusinessPhone' as componentID, 'Contacts' as tablename, 'FirmBusinessPhone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmBusinessFaxFormat' as componentID, 'Contacts' as tablename, 'FirmBusinessFaxFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmBusinessFax' as componentID, 'Contacts' as tablename, 'FirmBusinessFax' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmAddressDescription' as componentID, 'Contacts' as tablename, 'FirmAddressDescription' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmAddress4' as componentID, 'Contacts' as tablename, 'FirmAddress4' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmAddress3' as componentID, 'Contacts' as tablename, 'FirmAddress3' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmAddress2' as componentID, 'Contacts' as tablename, 'FirmAddress2' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmAddress1' as componentID, 'Contacts' as tablename, 'FirmAddress1' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmAddress' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FaxFormat' as componentID, 'Contacts' as tablename, 'FaxFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Fax' as componentID, 'Contacts' as tablename, 'Fax' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.emOwnerfl' as componentID, 'Contacts' as tablename, 'emOwnerfl' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Email' as componentID, 'Contacts' as tablename, 'Email' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.CustomCurrencyCode' as componentID, 'Contacts' as tablename, 'CustomCurrencyCode' as fieldname, 'overview' as tabID, 1 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.CreateUser' as componentID, 'Contacts' as tablename, 'CreateUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.CreateUserName' as componentID, 'Contacts' as tablename, 'CreateUserName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.CreateDate' as componentID, 'Contacts' as tablename, 'CreateDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Country' as componentID, 'Contacts' as tablename, 'Country' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.ContactStatus' as componentID, 'Contacts' as tablename, 'ContactStatus' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.ContactNameField' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.ContactInformation' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.ContactID' as componentID, 'Contacts' as tablename, 'ContactID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Company' as componentID, 'Contacts' as tablename, 'Company' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.ClientName' as componentID, 'Contacts' as tablename, 'ClientName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.ClientID' as componentID, 'Contacts' as tablename, 'ClientID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Client' as componentID, 'Contacts' as tablename, 'Client' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.CLAddress' as componentID, 'Contacts' as tablename, 'CLAddress' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.City' as componentID, 'Contacts' as tablename, 'City' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.CellPhoneFormat' as componentID, 'Contacts' as tablename, 'CellPhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.CellPhone' as componentID, 'Contacts' as tablename, 'CellPhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Billing' as componentID, 'Contacts' as tablename, 'Billing' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.AjeraSync' as componentID, 'Contacts' as tablename, 'AjeraSync' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Addressee' as componentID, 'Contacts' as tablename, 'Addressee' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Address4' as componentID, 'Contacts' as tablename, 'Address4' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Address3' as componentID, 'Contacts' as tablename, 'Address3' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Address2' as componentID, 'Contacts' as tablename, 'Address2' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Address1' as componentID, 'Contacts' as tablename, 'Address1' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Accounting' as componentID, 'Contacts' as tablename, 'Accounting' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'ContactMoreInfo' as componentID, null as tablename, null as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'ContactFileLinks' as componentID, null as tablename, null as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Activity' as componentID, null as tablename, null as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'ZIP' as componentID, 'QualifiedContact' as tablename, 'ZIP' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'StatusDate' as componentID, 'QualifiedContact' as tablename, 'StatusDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'State' as componentID, 'QualifiedContact' as tablename, 'State' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'QualifiedStatus' as componentID, 'QualifiedContact' as tablename, 'QualifiedStatus' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'PhoneFormat' as componentID, 'QualifiedContact' as tablename, 'PhoneFormat' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'Phone' as componentID, 'QualifiedContact' as tablename, 'Phone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'LastName' as componentID, 'QualifiedContact' as tablename, 'LastName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'FirstName' as componentID, 'QualifiedContact' as tablename, 'FirstName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'ContactID' as componentID, 'QualifiedContact' as tablename, 'ContactID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'ClientName' as componentID, 'QualifiedContact' as tablename, 'ClientName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'ClientID' as componentID, 'QualifiedContact' as tablename, 'ClientID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'City' as componentID, 'QualifiedContact' as tablename, 'City' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'CampaignName' as componentID, 'QualifiedContact' as tablename, 'CampaignName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'CampaignID' as componentID, 'QualifiedContact' as tablename, 'CampaignID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'Address4' as componentID, 'QualifiedContact' as tablename, 'Address4' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'Address3' as componentID, 'QualifiedContact' as tablename, 'Address3' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'Address2' as componentID, 'QualifiedContact' as tablename, 'Address2' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'QualifiedContact' as GridID, 'Address1' as componentID, 'QualifiedContact' as tablename, 'Address1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'WBS1' as componentID, 'PRContactAssoc' as tablename, 'WBS1' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'VendorInd' as componentID, 'PRContactAssoc' as tablename, 'VendorInd' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'TeamStatus' as componentID, 'PRContactAssoc' as tablename, 'TeamStatus' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'StageDescription' as componentID, 'PRContactAssoc' as tablename, 'StageDescription' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'RoleDescription' as componentID, 'PRContactAssoc' as tablename, 'RoleDescription' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'Role' as componentID, 'PRContactAssoc' as tablename, 'Role' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'PRStatus' as componentID, 'PRContactAssoc' as tablename, 'PRStatus' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'PRName' as componentID, 'PRContactAssoc' as tablename, 'PRName' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'PrimaryInd' as componentID, 'PRContactAssoc' as tablename, 'PrimaryInd' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'PRContactName' as componentID, 'PRContactAssoc' as tablename, 'PRContactName' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'PRContactID' as componentID, 'PRContactAssoc' as tablename, 'PRContactID' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'PRClientName' as componentID, 'PRContactAssoc' as tablename, 'PRClientName' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'PRClientID' as componentID, 'PRContactAssoc' as tablename, 'PRClientID' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'ModUser' as componentID, 'PRContactAssoc' as tablename, 'ModUser' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'ModDate' as componentID, 'PRContactAssoc' as tablename, 'ModDate' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'DefaultType' as componentID, 'PRContactAssoc' as tablename, 'DefaultType' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'CreateUser' as componentID, 'PRContactAssoc' as tablename, 'CreateUser' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'CreateDate' as componentID, 'PRContactAssoc' as tablename, 'CreateDate' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'ContactID' as componentID, 'PRContactAssoc' as tablename, 'ContactID' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'ClientRole' as componentID, 'PRContactAssoc' as tablename, 'ClientRole' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'ClientInd' as componentID, 'PRContactAssoc' as tablename, 'ClientInd' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'PRContactAssoc' as GridID, 'AjeraSync' as componentID, 'PRContactAssoc' as tablename, 'AjeraSync' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'Type' as componentID, 'MktCampaignContactAssoc' as tablename, 'Type' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'Status' as componentID, 'MktCampaignContactAssoc' as tablename, 'Status' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'Stage' as componentID, 'MktCampaignContactAssoc' as tablename, 'Stage' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'Name' as componentID, 'MktCampaignContactAssoc' as tablename, 'Name' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'ModUser' as componentID, 'MktCampaignContactAssoc' as tablename, 'ModUser' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'ModDate' as componentID, 'MktCampaignContactAssoc' as tablename, 'ModDate' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'LaunchDate' as componentID, 'MktCampaignContactAssoc' as tablename, 'LaunchDate' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'CreateUser' as componentID, 'MktCampaignContactAssoc' as tablename, 'CreateUser' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'CreateDate' as componentID, 'MktCampaignContactAssoc' as tablename, 'CreateDate' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'ContactID' as componentID, 'MktCampaignContactAssoc' as tablename, 'ContactID' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'CampaignMgrID' as componentID, 'MktCampaignContactAssoc' as tablename, 'CampaignMgrID' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'CampaignMgr' as componentID, 'MktCampaignContactAssoc' as tablename, 'CampaignMgr' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'CampaignID' as componentID, 'MktCampaignContactAssoc' as tablename, 'CampaignID' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'PKey' as componentID, 'FW_ATTACHMENTS' as tablename, 'PKey' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key3' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key3' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key2' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key2' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key1' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key1' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileSize' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileSize' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileName' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileName' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileID' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileDescription' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileDescription' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'ContentType' as componentID, 'FW_ATTACHMENTS' as tablename, 'ContentType' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'ContactID' as componentID, 'FW_ATTACHMENTS' as tablename, 'ContactID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'CategoryDescription' as componentID, 'FW_ATTACHMENTS' as tablename, 'CategoryDescription' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'CategoryCode' as componentID, 'FW_ATTACHMENTS' as tablename, 'CategoryCode' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Application' as componentID, 'FW_ATTACHMENTS' as tablename, 'Application' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ZIP' as componentID, 'EMContactAssoc' as tablename, 'ZIP' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'YearsOtherFirms' as componentID, 'EMContactAssoc' as tablename, 'YearsOtherFirms' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'WorkPhoneFormat' as componentID, 'EMContactAssoc' as tablename, 'WorkPhoneFormat' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'WorkPhone' as componentID, 'EMContactAssoc' as tablename, 'WorkPhone' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Vendor' as componentID, 'EMContactAssoc' as tablename, 'Vendor' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'UtilizationRatio' as componentID, 'EMContactAssoc' as tablename, 'UtilizationRatio' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'UseTotalHrsAsStd' as componentID, 'EMContactAssoc' as tablename, 'UseTotalHrsAsStd' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Type' as componentID, 'EMContactAssoc' as tablename, 'Type' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'TLSyncModDate' as componentID, 'EMContactAssoc' as tablename, 'TLSyncModDate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'TLInternalKey' as componentID, 'EMContactAssoc' as tablename, 'TLInternalKey' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'TKGroup' as componentID, 'EMContactAssoc' as tablename, 'TKGroup' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'TKAdminLevel' as componentID, 'EMContactAssoc' as tablename, 'TKAdminLevel' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'TKAdminEdit' as componentID, 'EMContactAssoc' as tablename, 'TKAdminEdit' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Title' as componentID, 'EMContactAssoc' as tablename, 'Title' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ThirdPartySickPay' as componentID, 'EMContactAssoc' as tablename, 'ThirdPartySickPay' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'TerminationDate' as componentID, 'EMContactAssoc' as tablename, 'TerminationDate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'TaxRegistrationNumber' as componentID, 'EMContactAssoc' as tablename, 'TaxRegistrationNumber' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'TargetRatio' as componentID, 'EMContactAssoc' as tablename, 'TargetRatio' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'TalentUserID' as componentID, 'EMContactAssoc' as tablename, 'TalentUserID' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'TalentModDate' as componentID, 'EMContactAssoc' as tablename, 'TalentModDate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Supervisor' as componentID, 'EMContactAssoc' as tablename, 'Supervisor' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Suffix' as componentID, 'EMContactAssoc' as tablename, 'Suffix' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'StatutoryEmployee' as componentID, 'EMContactAssoc' as tablename, 'StatutoryEmployee' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Status' as componentID, 'EMContactAssoc' as tablename, 'Status' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'State' as componentID, 'EMContactAssoc' as tablename, 'State' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'SSN' as componentID, 'EMContactAssoc' as tablename, 'SSN' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'SEPAIBAN' as componentID, 'EMContactAssoc' as tablename, 'SEPAIBAN' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'SEPABIC' as componentID, 'EMContactAssoc' as tablename, 'SEPABIC' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Salutation' as componentID, 'EMContactAssoc' as tablename, 'Salutation' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'RetirementPlan' as componentID, 'EMContactAssoc' as tablename, 'RetirementPlan' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'RequireStartEndTime' as componentID, 'EMContactAssoc' as tablename, 'RequireStartEndTime' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'RelationshipDesc' as componentID, 'EMContactAssoc' as tablename, 'RelationshipDesc' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Relationship' as componentID, 'EMContactAssoc' as tablename, 'Relationship' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Region' as componentID, 'EMContactAssoc' as tablename, 'Region' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ReadyForProcessing' as componentID, 'EMContactAssoc' as tablename, 'ReadyForProcessing' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ReadyForApproval' as componentID, 'EMContactAssoc' as tablename, 'ReadyForApproval' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'RaiseDate' as componentID, 'EMContactAssoc' as tablename, 'RaiseDate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ProvCostSpecialOTPct' as componentID, 'EMContactAssoc' as tablename, 'ProvCostSpecialOTPct' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ProvCostRate' as componentID, 'EMContactAssoc' as tablename, 'ProvCostRate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ProvCostOTPct' as componentID, 'EMContactAssoc' as tablename, 'ProvCostOTPct' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ProvBillSpecialOTPct' as componentID, 'EMContactAssoc' as tablename, 'ProvBillSpecialOTPct' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ProvBillRate' as componentID, 'EMContactAssoc' as tablename, 'ProvBillRate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ProvBillOTPct' as componentID, 'EMContactAssoc' as tablename, 'ProvBillOTPct' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ProfessionalSuffix' as componentID, 'EMContactAssoc' as tablename, 'ProfessionalSuffix' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'PriorYearsFirm' as componentID, 'EMContactAssoc' as tablename, 'PriorYearsFirm' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'PreferredName' as componentID, 'EMContactAssoc' as tablename, 'PreferredName' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'PayType' as componentID, 'EMContactAssoc' as tablename, 'PayType' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'PaySpecialOvtPct' as componentID, 'EMContactAssoc' as tablename, 'PaySpecialOvtPct' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'PayRateTableNo' as componentID, 'EMContactAssoc' as tablename, 'PayRateTableNo' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'PayRateMeth' as componentID, 'EMContactAssoc' as tablename, 'PayRateMeth' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'PayRate' as componentID, 'EMContactAssoc' as tablename, 'PayRate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'PayOvtPct' as componentID, 'EMContactAssoc' as tablename, 'PayOvtPct' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'PaychexRateNumber' as componentID, 'EMContactAssoc' as tablename, 'PaychexRateNumber' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'PaychexCode3' as componentID, 'EMContactAssoc' as tablename, 'PaychexCode3' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'PaychexCode2' as componentID, 'EMContactAssoc' as tablename, 'PaychexCode2' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'PaychexCode1' as componentID, 'EMContactAssoc' as tablename, 'PaychexCode1' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'OtherPay5' as componentID, 'EMContactAssoc' as tablename, 'OtherPay5' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'OtherPay4' as componentID, 'EMContactAssoc' as tablename, 'OtherPay4' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'OtherPay3' as componentID, 'EMContactAssoc' as tablename, 'OtherPay3' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'OtherPay2' as componentID, 'EMContactAssoc' as tablename, 'OtherPay2' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'OtherPay' as componentID, 'EMContactAssoc' as tablename, 'OtherPay' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Org' as componentID, 'EMContactAssoc' as tablename, 'Org' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'OccupationalCode' as componentID, 'EMContactAssoc' as tablename, 'OccupationalCode' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Name' as componentID, 'EMContactAssoc' as tablename, 'Name' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ModUser' as componentID, 'EMContactAssoc' as tablename, 'ModUser' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ModDate' as componentID, 'EMContactAssoc' as tablename, 'ModDate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'MobilePhoneFormat' as componentID, 'EMContactAssoc' as tablename, 'MobilePhoneFormat' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'MobilePhone' as componentID, 'EMContactAssoc' as tablename, 'MobilePhone' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'MiddleName' as componentID, 'EMContactAssoc' as tablename, 'MiddleName' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Memo' as componentID, 'EMContactAssoc' as tablename, 'Memo' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Location' as componentID, 'EMContactAssoc' as tablename, 'Location' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'LocaleMethod' as componentID, 'EMContactAssoc' as tablename, 'LocaleMethod' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Locale' as componentID, 'EMContactAssoc' as tablename, 'Locale' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'LastName' as componentID, 'EMContactAssoc' as tablename, 'LastName' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Language' as componentID, 'EMContactAssoc' as tablename, 'Language' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'JobCostType' as componentID, 'EMContactAssoc' as tablename, 'JobCostType' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'JobCostRate' as componentID, 'EMContactAssoc' as tablename, 'JobCostRate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'JCSpecialOvtPct' as componentID, 'EMContactAssoc' as tablename, 'JCSpecialOvtPct' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'JCOvtPct' as componentID, 'EMContactAssoc' as tablename, 'JCOvtPct' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'IncludeLocalJurisOnly' as componentID, 'EMContactAssoc' as tablename, 'IncludeLocalJurisOnly' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'HoursPerDay' as componentID, 'EMContactAssoc' as tablename, 'HoursPerDay' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'HomePhoneFormat' as componentID, 'EMContactAssoc' as tablename, 'HomePhoneFormat' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'HomePhone' as componentID, 'EMContactAssoc' as tablename, 'HomePhone' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'HomeCompany' as componentID, 'EMContactAssoc' as tablename, 'HomeCompany' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'HireDate' as componentID, 'EMContactAssoc' as tablename, 'HireDate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'GeographicCode' as componentID, 'EMContactAssoc' as tablename, 'GeographicCode' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'FirstName' as componentID, 'EMContactAssoc' as tablename, 'FirstName' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'FaxFormat' as componentID, 'EMContactAssoc' as tablename, 'FaxFormat' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Fax' as componentID, 'EMContactAssoc' as tablename, 'Fax' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ExportInd' as componentID, 'EMContactAssoc' as tablename, 'ExportInd' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'EmployeePhoto' as componentID, 'EMContactAssoc' as tablename, 'EmployeePhoto' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'EmployeeName' as componentID, 'EMContactAssoc' as tablename, 'EmployeeName' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'EmployeeCompany' as componentID, 'EMContactAssoc' as tablename, 'EmployeeCompany' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Employee' as componentID, 'EMContactAssoc' as tablename, 'Employee' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'EmailPayrollRemittance' as componentID, 'EMContactAssoc' as tablename, 'EmailPayrollRemittance' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'EmailExpenseRemittance' as componentID, 'EMContactAssoc' as tablename, 'EmailExpenseRemittance' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'EMail' as componentID, 'EMContactAssoc' as tablename, 'EMail' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'EKGroup' as componentID, 'EMContactAssoc' as tablename, 'EKGroup' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'EKAdminLevel' as componentID, 'EMContactAssoc' as tablename, 'EKAdminLevel' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'EKAdminEdit' as componentID, 'EMContactAssoc' as tablename, 'EKAdminEdit' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'DisableTSRevAudit' as componentID, 'EMContactAssoc' as tablename, 'DisableTSRevAudit' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'DefaultLC5' as componentID, 'EMContactAssoc' as tablename, 'DefaultLC5' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'DefaultLC4' as componentID, 'EMContactAssoc' as tablename, 'DefaultLC4' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'DefaultLC3' as componentID, 'EMContactAssoc' as tablename, 'DefaultLC3' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'DefaultLC2' as componentID, 'EMContactAssoc' as tablename, 'DefaultLC2' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'DefaultLC1' as componentID, 'EMContactAssoc' as tablename, 'DefaultLC1' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'DefaultBreakStartDateTime' as componentID, 'EMContactAssoc' as tablename, 'DefaultBreakStartDateTime' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'DefaultBreakEndDateTime' as componentID, 'EMContactAssoc' as tablename, 'DefaultBreakEndDateTime' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'CreateUser' as componentID, 'EMContactAssoc' as tablename, 'CreateUser' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'CreateDate' as componentID, 'EMContactAssoc' as tablename, 'CreateDate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Country' as componentID, 'EMContactAssoc' as tablename, 'Country' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'CostRateTableNo' as componentID, 'EMContactAssoc' as tablename, 'CostRateTableNo' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'CostRateMeth' as componentID, 'EMContactAssoc' as tablename, 'CostRateMeth' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ContactID' as componentID, 'EMContactAssoc' as tablename, 'ContactID' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ConsultantInd' as componentID, 'EMContactAssoc' as tablename, 'ConsultantInd' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ClieOpTransactionType' as componentID, 'EMContactAssoc' as tablename, 'ClieOpTransactionType' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ClieOpAccountType' as componentID, 'EMContactAssoc' as tablename, 'ClieOpAccountType' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ClieOpAccount' as componentID, 'EMContactAssoc' as tablename, 'ClieOpAccount' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ClieOp' as componentID, 'EMContactAssoc' as tablename, 'ClieOp' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ClientVendorInd' as componentID, 'EMContactAssoc' as tablename, 'ClientVendorInd' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ClientID' as componentID, 'EMContactAssoc' as tablename, 'ClientID' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'City' as componentID, 'EMContactAssoc' as tablename, 'City' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'CheckHours' as componentID, 'EMContactAssoc' as tablename, 'CheckHours' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ChangeDefaultLC' as componentID, 'EMContactAssoc' as tablename, 'ChangeDefaultLC' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'BillingPool' as componentID, 'EMContactAssoc' as tablename, 'BillingPool' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'BillingCategory' as componentID, 'EMContactAssoc' as tablename, 'BillingCategory' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'AvailableForCRM' as componentID, 'EMContactAssoc' as tablename, 'AvailableForCRM' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'AllowChargeUnits' as componentID, 'EMContactAssoc' as tablename, 'AllowChargeUnits' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'AllowBreakTime' as componentID, 'EMContactAssoc' as tablename, 'AllowBreakTime' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ADPRateCode' as componentID, 'EMContactAssoc' as tablename, 'ADPRateCode' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ADPFileNumber' as componentID, 'EMContactAssoc' as tablename, 'ADPFileNumber' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'ADPCompanyCode' as componentID, 'EMContactAssoc' as tablename, 'ADPCompanyCode' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Address3' as componentID, 'EMContactAssoc' as tablename, 'Address3' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Address2' as componentID, 'EMContactAssoc' as tablename, 'Address2' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'EMContactAssoc' as GridID, 'Address1' as componentID, 'EMContactAssoc' as tablename, 'Address1' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactToContactAssoc' as GridID, 'ToMiddleName' as componentID, 'ContactToContactAssoc' as tablename, 'ToMiddleName' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactToContactAssoc' as GridID, 'ToLastName' as componentID, 'ContactToContactAssoc' as tablename, 'ToLastName' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactToContactAssoc' as GridID, 'ToFirstName' as componentID, 'ContactToContactAssoc' as tablename, 'ToFirstName' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactToContactAssoc' as GridID, 'ToContactID' as componentID, 'ContactToContactAssoc' as tablename, 'ToContactID' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactToContactAssoc' as GridID, 'Relationship' as componentID, 'ContactToContactAssoc' as tablename, 'Relationship' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactToContactAssoc' as GridID, 'Name' as componentID, 'ContactToContactAssoc' as tablename, 'Name' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactToContactAssoc' as GridID, 'ModUser' as componentID, 'ContactToContactAssoc' as tablename, 'ModUser' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactToContactAssoc' as GridID, 'ModDate' as componentID, 'ContactToContactAssoc' as tablename, 'ModDate' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactToContactAssoc' as GridID, 'FromContactID' as componentID, 'ContactToContactAssoc' as tablename, 'FromContactID' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactToContactAssoc' as GridID, 'Description' as componentID, 'ContactToContactAssoc' as tablename, 'Description' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactToContactAssoc' as GridID, 'CreateUser' as componentID, 'ContactToContactAssoc' as tablename, 'CreateUser' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactToContactAssoc' as GridID, 'CreateDate' as componentID, 'ContactToContactAssoc' as tablename, 'CreateDate' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactToContactAssoc' as GridID, 'ContactID' as componentID, 'ContactToContactAssoc' as tablename, 'ContactID' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactMoreInfo' as GridID, 'ModUser' as componentID, 'ContactMoreInfo' as tablename, 'ModUser' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactMoreInfo' as GridID, 'ModDate' as componentID, 'ContactMoreInfo' as tablename, 'ModDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactMoreInfo' as GridID, 'Info' as componentID, 'ContactMoreInfo' as tablename, 'Info' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactMoreInfo' as GridID, 'CreateUser' as componentID, 'ContactMoreInfo' as tablename, 'CreateUser' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactMoreInfo' as GridID, 'CreateDate' as componentID, 'ContactMoreInfo' as tablename, 'CreateDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactMoreInfo' as GridID, 'ContactID' as componentID, 'ContactMoreInfo' as tablename, 'ContactID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactMoreInfo' as GridID, 'CategoryDesc' as componentID, 'ContactMoreInfo' as tablename, 'CategoryDesc' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactMoreInfo' as GridID, 'Category' as componentID, 'ContactMoreInfo' as tablename, 'Category' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactFileLinks' as GridID, 'LinkID' as componentID, 'ContactFileLinks' as tablename, 'LinkID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactFileLinks' as GridID, 'Graphic' as componentID, 'ContactFileLinks' as tablename, 'Graphic' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactFileLinks' as GridID, 'FilePath' as componentID, 'ContactFileLinks' as tablename, 'FilePath' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactFileLinks' as GridID, 'Description' as componentID, 'ContactFileLinks' as tablename, 'Description' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'ContactFileLinks' as GridID, 'ContactID' as componentID, 'ContactFileLinks' as tablename, 'ContactID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'WBS3' as componentID, 'Activity' as tablename, 'WBS3' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'WBS2' as componentID, 'Activity' as tablename, 'WBS2' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'WBS1' as componentID, 'Activity' as tablename, 'WBS1' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'VendorName' as componentID, 'Activity' as tablename, 'VendorName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'VendorContactName' as componentID, 'Activity' as tablename, 'VendorContactName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'Vendor' as componentID, 'Activity' as tablename, 'Vendor' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'UpdateID' as componentID, 'Activity' as tablename, 'UpdateID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'TypeName' as componentID, 'Activity' as tablename, 'TypeName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'Type' as componentID, 'Activity' as tablename, 'Type' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'TaskCompletionDate' as componentID, 'Activity' as tablename, 'TaskCompletionDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'Subject' as componentID, 'Activity' as tablename, 'Subject' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'StartDateObj' as componentID, 'Activity' as tablename, 'StartDateObj' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'StartDate' as componentID, 'Activity' as tablename, 'StartDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'ShowTimeAs' as componentID, 'Activity' as tablename, 'ShowTimeAs' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RP' as componentID, 'Activity' as tablename, 'RP' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'ReminderUnit' as componentID, 'Activity' as tablename, 'ReminderUnit' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'ReminderMinHrDay' as componentID, 'Activity' as tablename, 'ReminderMinHrDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'ReminderInd' as componentID, 'Activity' as tablename, 'ReminderInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'ReminderDate' as componentID, 'Activity' as tablename, 'ReminderDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccurMonth' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurMonth' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccurInd' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccurDay' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccur' as componentID, 'Activity' as tablename, 'RecurrYearlyOccur' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyMonth' as componentID, 'Activity' as tablename, 'RecurrYearlyMonth' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyDay' as componentID, 'Activity' as tablename, 'RecurrYearlyDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyWed' as componentID, 'Activity' as tablename, 'RecurrWeeklyWed' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyTue' as componentID, 'Activity' as tablename, 'RecurrWeeklyTue' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyThu' as componentID, 'Activity' as tablename, 'RecurrWeeklyThu' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklySun' as componentID, 'Activity' as tablename, 'RecurrWeeklySun' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklySat' as componentID, 'Activity' as tablename, 'RecurrWeeklySat' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyMon' as componentID, 'Activity' as tablename, 'RecurrWeeklyMon' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyFri' as componentID, 'Activity' as tablename, 'RecurrWeeklyFri' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyFreq' as componentID, 'Activity' as tablename, 'RecurrWeeklyFreq' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrType' as componentID, 'Activity' as tablename, 'RecurrType' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrStartDate' as componentID, 'Activity' as tablename, 'RecurrStartDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccurInd' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccurFreq' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurFreq' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccurDay' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccur' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccur' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyFreq' as componentID, 'Activity' as tablename, 'RecurrMonthlyFreq' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyDay' as componentID, 'Activity' as tablename, 'RecurrMonthlyDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrID' as componentID, 'Activity' as tablename, 'RecurrID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrEndType' as componentID, 'Activity' as tablename, 'RecurrEndType' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrEndDate' as componentID, 'Activity' as tablename, 'RecurrEndDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrenceInd' as componentID, 'Activity' as tablename, 'RecurrenceInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrDailyWeekDay' as componentID, 'Activity' as tablename, 'RecurrDailyWeekDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'RecurrDailyFreq' as componentID, 'Activity' as tablename, 'RecurrDailyFreq' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'ReadID' as componentID, 'Activity' as tablename, 'ReadID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'ProjName' as componentID, 'Activity' as tablename, 'ProjName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'PrivateInd' as componentID, 'Activity' as tablename, 'PrivateInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'PriorityDesc' as componentID, 'Activity' as tablename, 'PriorityDesc' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'Priority' as componentID, 'Activity' as tablename, 'Priority' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'PrimaryClient' as componentID, 'Activity' as tablename, 'PrimaryClient' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'OwnerName' as componentID, 'Activity' as tablename, 'OwnerName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'Owner' as componentID, 'Activity' as tablename, 'Owner' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'OutlookType' as componentID, 'Activity' as tablename, 'OutlookType' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'Notes' as componentID, 'Activity' as tablename, 'Notes' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'ModUser' as componentID, 'Activity' as tablename, 'ModUser' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'ModDate' as componentID, 'Activity' as tablename, 'ModDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'MktName' as componentID, 'Activity' as tablename, 'MktName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'MaxOccurences' as componentID, 'Activity' as tablename, 'MaxOccurences' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'Location' as componentID, 'Activity' as tablename, 'Location' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'LeadID' as componentID, 'Activity' as tablename, 'LeadID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'isTask' as componentID, 'Activity' as tablename, 'isTask' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'isCalendar' as componentID, 'Activity' as tablename, 'isCalendar' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'InstanceStartDate' as componentID, 'Activity' as tablename, 'InstanceStartDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'HasNotes' as componentID, 'Activity' as tablename, 'HasNotes' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'EndDateSort' as componentID, 'Activity' as tablename, 'EndDateSort' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'EndDateObj' as componentID, 'Activity' as tablename, 'EndDateObj' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'EndDateFilter' as componentID, 'Activity' as tablename, 'EndDateFilter' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'EndDate' as componentID, 'Activity' as tablename, 'EndDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'Employee' as componentID, 'Activity' as tablename, 'Employee' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'Duration' as componentID, 'Activity' as tablename, 'Duration' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'CreateUser' as componentID, 'Activity' as tablename, 'CreateUser' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'CreateEmployee' as componentID, 'Activity' as tablename, 'CreateEmployee' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'CreateDate' as componentID, 'Activity' as tablename, 'CreateDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'ContactPhoneForVendor' as componentID, 'Activity' as tablename, 'ContactPhoneForVendor' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'ContactNameForVendor' as componentID, 'Activity' as tablename, 'ContactNameForVendor' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'ContactName' as componentID, 'Activity' as tablename, 'ContactName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'ContactIDForVendor' as componentID, 'Activity' as tablename, 'ContactIDForVendor' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'ContactID' as componentID, 'Activity' as tablename, 'ContactID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'CompletionInd' as componentID, 'Activity' as tablename, 'CompletionInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'ClientName' as componentID, 'Activity' as tablename, 'ClientName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'ClientID' as componentID, 'Activity' as tablename, 'ClientID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'CampaignID' as componentID, 'Activity' as tablename, 'CampaignID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'Attendees' as componentID, 'Activity' as tablename, 'Attendees' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'Associations' as componentID, 'Activity' as tablename, 'Associations' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'AllDayEventInd' as componentID, 'Activity' as tablename, 'AllDayEventInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'acType' as componentID, 'Activity' as tablename, 'acType' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Contacts' as infocenterArea, 'Activity' as GridID, 'ActivityID' as componentID, 'Activity' as tablename, 'ActivityID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'tsControl' as GridID, 'TotalHrs' as componentID, 'tsControl' as tablename, 'TotalHrs' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'tsControl' as GridID, 'Period' as componentID, 'tsControl' as tablename, 'Period' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'tsControl' as GridID, 'EndDate' as componentID, 'tsControl' as tablename, 'EndDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'tsControl' as GridID, 'Description' as componentID, 'tsControl' as tablename, 'Description' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'tsControl' as GridID, 'Creator' as componentID, 'tsControl' as tablename, 'Creator' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'tsControl' as GridID, 'Batch' as componentID, 'tsControl' as tablename, 'Batch' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'FW_ATTACHMENTS' as componentID, null as tablename, null as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EQICEMAssoc' as componentID, null as tablename, null as fieldname, 'assets' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EMTKGROUPS' as componentID, null as tablename, null as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EMSkills' as componentID, null as tablename, null as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EMResume' as componentID, null as tablename, null as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EMRegistration' as componentID, null as tablename, null as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EMProjectAssoc' as componentID, null as tablename, null as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EMPLOYEEFILELINKS' as componentID, null as tablename, null as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EMPAYROLLWITHHOLDING' as componentID, null as tablename, null as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EMPAYROLLCONTRIBUTION' as componentID, null as tablename, null as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EMLOCALE' as componentID, null as tablename, null as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EMEKGROUPS' as componentID, null as tablename, null as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EMDirectDeposit' as componentID, null as tablename, null as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EMDegree' as componentID, null as tablename, null as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EMContactAssoc' as componentID, null as tablename, null as fieldname, 'firmContact' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EMClientAssoc' as componentID, null as tablename, null as fieldname, 'firmContact' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EMCitizenship' as componentID, null as tablename, null as fieldname, 'personal_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EMAccrual' as componentID, null as tablename, null as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ZIP' as componentID, 'EM' as tablename, 'ZIP' as fieldname, 'personal_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.YearsOtherFirms' as componentID, 'EM' as tablename, 'YearsOtherFirms' as fieldname, 'overview' as tabID, 1 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.WorkSchedule' as componentID, 'EM' as tablename, 'WorkSchedule' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.WorkPhoneFormat' as componentID, 'EM' as tablename, 'WorkPhoneFormat' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.WorkPhone' as componentID, 'EM' as tablename, 'WorkPhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.VisaType' as componentID, 'EM' as tablename, 'VisaType' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.VisaNumber' as componentID, 'EM' as tablename, 'VisaNumber' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.VisaExpDate' as componentID, 'EM' as tablename, 'VisaExpDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.VisaCountry' as componentID, 'EM' as tablename, 'VisaCountry' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.VetType' as componentID, 'EM' as tablename, 'VetType' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.VetStatus' as componentID, 'EM' as tablename, 'VetStatus' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Vendor' as componentID, 'EM' as tablename, 'Vendor' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.UtilizationRatio' as componentID, 'EM' as tablename, 'UtilizationRatio' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.UseTotalHrsAsStd' as componentID, 'EM' as tablename, 'UseTotalHrsAsStd' as fieldname, 'details' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.UIPaymentMethod' as componentID, 'EM' as tablename, 'UIPaymentMethod' as fieldname, 'accounting' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Type' as componentID, 'EM' as tablename, 'Type' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.TotalYearsWithThisFirm' as componentID, 'EM' as tablename, 'TotalYearsWithThisFirm' as fieldname, 'overview' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.TotalYearsExperience' as componentID, 'EM' as tablename, 'TotalYearsExperience' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.TKGroup' as componentID, 'EM' as tablename, 'TKGroup' as fieldname, 'timeAndExpenseTab' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.TKAdminLevel' as componentID, 'EM' as tablename, 'TKAdminLevel' as fieldname, 'timeAndExpenseTab' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.TKAdminEdit' as componentID, 'EM' as tablename, 'TKAdminEdit' as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.TitleName' as componentID, 'EM' as tablename, 'TitleName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Title' as componentID, 'EM' as tablename, 'Title' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ThirdPartySickPay' as componentID, 'EM' as tablename, 'ThirdPartySickPay' as fieldname, 'payroll' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.TerminationType' as componentID, 'EM' as tablename, 'TerminationType' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.TerminationReason' as componentID, 'EM' as tablename, 'TerminationReason' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.TerminationDate' as componentID, 'EM' as tablename, 'TerminationDate' as fieldname, 'details' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Terminated' as componentID, 'EM' as tablename, 'Terminated' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.TaxRegistrationNumber' as componentID, 'EM' as tablename, 'TaxRegistrationNumber' as fieldname, 'personal_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.TaxAuditingEnabled' as componentID, 'EM' as tablename, 'TaxAuditingEnabled' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.TaxRegistrationCountry' as componentID, 'EM' as tablename, 'TaxRegistrationCountry' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.TargetRatio' as componentID, 'EM' as tablename, 'TargetRatio' as fieldname, 'details' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.TalentUserID' as componentID, 'EM' as tablename, 'TalentUserID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.SupervisorName' as componentID, 'EM' as tablename, 'SupervisorName' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Supervisor' as componentID, 'EM' as tablename, 'Supervisor' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Suffix' as componentID, 'EM' as tablename, 'Suffix' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.StatutoryEmployee' as componentID, 'EM' as tablename, 'StatutoryEmployee' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Status' as componentID, 'EM' as tablename, 'Status' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.State' as componentID, 'EM' as tablename, 'State' as fieldname, 'personal_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.SSN' as componentID, 'EM' as tablename, 'SSN' as fieldname, 'personal_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.SOCCodeDescription' as componentID, 'EM' as tablename, 'SOCCodeDescription' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.SOCCode' as componentID, 'EM' as tablename, 'SOCCode' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.SeparationDate' as componentID, 'EM' as tablename, 'SeparationDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.SEPAIBAN' as componentID, 'EM' as tablename, 'SEPAIBAN' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.SEPABIC' as componentID, 'EM' as tablename, 'SEPABIC' as fieldname, 'accounting' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.SeniorityDate' as componentID, 'EM' as tablename, 'SeniorityDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.SecondaryContactRelationship' as componentID, 'EM' as tablename, 'SecondaryContactRelationship' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.SecondaryContactPhoneFormat' as componentID, 'EM' as tablename, 'SecondaryContactPhoneFormat' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.SecondaryContactPhone' as componentID, 'EM' as tablename, 'SecondaryContactPhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.SecondaryContact' as componentID, 'EM' as tablename, 'SecondaryContact' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Salutation' as componentID, 'EM' as tablename, 'Salutation' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.RetirementPlan' as componentID, 'EM' as tablename, 'RetirementPlan' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.RequireStartEndTime' as componentID, 'EM' as tablename, 'RequireStartEndTime' as fieldname, 'timeAndExpenseTab' as tabID, 1 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.RehireEligible' as componentID, 'EM' as tablename, 'RehireEligible' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.RehireDate' as componentID, 'EM' as tablename, 'RehireDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ReadyForProcessing' as componentID, 'EM' as tablename, 'ReadyForProcessing' as fieldname, 'accounting' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ReadyForApproval' as componentID, 'EM' as tablename, 'ReadyForApproval' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.RaiseDate' as componentID, 'EM' as tablename, 'RaiseDate' as fieldname, 'details' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.QBOVendorID' as componentID, 'EM' as tablename, 'QBOVendorID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.QBOLastUpdated' as componentID, 'EM' as tablename, 'QBOLastUpdated' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.QBOID' as componentID, 'EM' as tablename, 'QBOID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.QBOAddressID' as componentID, 'EM' as tablename, 'QBOAddressID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ProvCostSpecialOTPct' as componentID, 'EM' as tablename, 'ProvCostSpecialOTPct' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ProvCostRate' as componentID, 'EM' as tablename, 'ProvCostRate' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ProvCostOTPct' as componentID, 'EM' as tablename, 'ProvCostOTPct' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ProvBillSpecialOTPct' as componentID, 'EM' as tablename, 'ProvBillSpecialOTPct' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ProvBillRate' as componentID, 'EM' as tablename, 'ProvBillRate' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ProvBillOTPct' as componentID, 'EM' as tablename, 'ProvBillOTPct' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ProfessionalSuffix' as componentID, 'EM' as tablename, 'ProfessionalSuffix' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PriorYearsFirm' as componentID, 'EM' as tablename, 'PriorYearsFirm' as fieldname, 'overview' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PrimaryContactRelationship' as componentID, 'EM' as tablename, 'PrimaryContactRelationship' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PrimaryContactPhoneFormat' as componentID, 'EM' as tablename, 'PrimaryContactPhoneFormat' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PrimaryContactPhone' as componentID, 'EM' as tablename, 'PrimaryContactPhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PrimaryContact' as componentID, 'EM' as tablename, 'PrimaryContact' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PreferredName' as componentID, 'EM' as tablename, 'PreferredName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PIMID' as componentID, 'EM' as tablename, 'PIMID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PhotoModDate' as componentID, 'EM' as tablename, 'PhotoModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PayType' as componentID, 'EM' as tablename, 'PayType' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PaySpecialOvtPct' as componentID, 'EM' as tablename, 'PaySpecialOvtPct' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PayrollFrequency' as componentID, 'EM' as tablename, 'PayrollFrequency' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PayRateTableNo' as componentID, 'EM' as tablename, 'PayRateTableNo' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PayRateMeth' as componentID, 'EM' as tablename, 'PayRateMeth' as fieldname, 'payroll' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PayRate' as componentID, 'EM' as tablename, 'PayRate' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PayOvtPct' as componentID, 'EM' as tablename, 'PayOvtPct' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PaychexRateNumber' as componentID, 'EM' as tablename, 'PaychexRateNumber' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PaychexCode3' as componentID, 'EM' as tablename, 'PaychexCode3' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PaychexCode2' as componentID, 'EM' as tablename, 'PaychexCode2' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PaychexCode1' as componentID, 'EM' as tablename, 'PaychexCode1' as fieldname, 'accounting' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PassportNumber' as componentID, 'EM' as tablename, 'PassportNumber' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PassportExpDate' as componentID, 'EM' as tablename, 'PassportExpDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PassportCountry' as componentID, 'EM' as tablename, 'PassportCountry' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.OtherPay5' as componentID, 'EM' as tablename, 'OtherPay5' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.OtherPay4' as componentID, 'EM' as tablename, 'OtherPay4' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.OtherPay3' as componentID, 'EM' as tablename, 'OtherPay3' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.OtherPay2' as componentID, 'EM' as tablename, 'OtherPay2' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.OtherPay' as componentID, 'EM' as tablename, 'OtherPay' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.OrganizationName' as componentID, 'EM' as tablename, 'OrganizationName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Org' as componentID, 'EM' as tablename, 'Org' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.OccupationalCode' as componentID, 'EM' as tablename, 'OccupationalCode' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ModUser' as componentID, 'EM' as tablename, 'ModUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ModUserName' as componentID, 'EM' as tablename, 'ModUserName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ModDate' as componentID, 'EM' as tablename, 'ModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.MobilePhoneFormat' as componentID, 'EM' as tablename, 'MobilePhoneFormat' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.MobilePhone' as componentID, 'EM' as tablename, 'MobilePhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.MiddleName' as componentID, 'EM' as tablename, 'MiddleName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Memo' as componentID, 'EM' as tablename, 'Memo' as fieldname, 'personal_web' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.MaritalStatus' as componentID, 'EM' as tablename, 'MaritalStatus' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.MailZIP' as componentID, 'EM' as tablename, 'MailZIP' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.MailState' as componentID, 'EM' as tablename, 'MailState' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.MailCountry' as componentID, 'EM' as tablename, 'MailCountry' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.MailCity' as componentID, 'EM' as tablename, 'MailCity' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.MailAddressSameAsHome' as componentID, 'EM' as tablename, 'MailAddressSameAsHome' as fieldname, 'noassociation' as tabID, 1 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.MailAddress3' as componentID, 'EM' as tablename, 'MailAddress3' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.MailAddress2' as componentID, 'EM' as tablename, 'MailAddress2' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.MailAddress1' as componentID, 'EM' as tablename, 'MailAddress1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.LocationDescription' as componentID, 'EM' as tablename, 'LocationDescription' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Location' as componentID, 'EM' as tablename, 'Location' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.LocaleMethod' as componentID, 'EM' as tablename, 'LocaleMethod' as fieldname, 'payroll' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Locale' as componentID, 'EM' as tablename, 'Locale' as fieldname, 'payroll' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.LastName' as componentID, 'EM' as tablename, 'LastName' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.LastDayWorked' as componentID, 'EM' as tablename, 'LastDayWorked' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.LastDayPaid' as componentID, 'EM' as tablename, 'LastDayPaid' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Language' as componentID, 'EM' as tablename, 'Language' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.JobLevelDescription' as componentID, 'EM' as tablename, 'JobLevelDescription' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.JobLevel' as componentID, 'EM' as tablename, 'JobLevel' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.JobCostType' as componentID, 'EM' as tablename, 'JobCostType' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.JobCostRate' as componentID, 'EM' as tablename, 'JobCostRate' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.JCSpecialOvtPct' as componentID, 'EM' as tablename, 'JCSpecialOvtPct' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.JCOvtPct' as componentID, 'EM' as tablename, 'JCOvtPct' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.IncludeLocalJurisOnly' as componentID, 'EM' as tablename, 'IncludeLocalJurisOnly' as fieldname, 'payroll' as tabID, 1 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.I9Verification' as componentID, 'EM' as tablename, 'I9Verification' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.HoursPerDay' as componentID, 'EM' as tablename, 'HoursPerDay' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.HomePhoneFormat' as componentID, 'EM' as tablename, 'HomePhoneFormat' as fieldname, 'personal_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.HomePhone' as componentID, 'EM' as tablename, 'HomePhone' as fieldname, 'personal_web' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.HomeEMail' as componentID, 'EM' as tablename, 'HomeEMail' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.HomeCompanyName' as componentID, 'EM' as tablename, 'HomeCompanyName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.HomeCompany' as componentID, 'EM' as tablename, 'HomeCompany' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.HireDate' as componentID, 'EM' as tablename, 'HireDate' as fieldname, 'details' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.HasPhoto' as componentID, 'EM' as tablename, 'HasPhoto' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.GeographicCode' as componentID, 'EM' as tablename, 'GeographicCode' as fieldname, 'payroll' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Gender' as componentID, 'EM' as tablename, 'Gender' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.FullName' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.FLSAStatus' as componentID, 'EM' as tablename, 'FLSAStatus' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.FirstName' as componentID, 'EM' as tablename, 'FirstName' as fieldname, 'noassociation' as tabID, 0 as essential union 
select 'Employees' as infocenterArea, 'X' as GridID, 'EM.FormW4Version' as componentID, 'EM' as tablename, 'FormW4Version' as fieldname, 'payroll' as tabID, 0 as essential union
select 'Employees' as infocenterArea, 'X' as GridID, 'EM.FormW4Step2' as componentID, 'EM' as tablename, 'FormW4Step2' as fieldname, 'payroll' as tabID, 0 as essential union
select 'Employees' as infocenterArea, 'X' as GridID, 'EM.FormW4Dependents' as componentID, 'EM' as tablename, 'FormW4Dependents' as fieldname, 'payroll' as tabID, 0 as essential union
select 'Employees' as infocenterArea, 'X' as GridID, 'EM.FormW4DependentsOther' as componentID, 'EM' as tablename, 'FormW4DependentsOther' as fieldname, 'payroll' as tabID, 0 as essential union
select 'Employees' as infocenterArea, 'X' as GridID, 'EM.FormW4OtherTaxCredit' as componentID, 'EM' as tablename, 'FormW4OtherTaxCredit' as fieldname, 'payroll' as tabID, 0 as essential union
select 'Employees' as infocenterArea, 'X' as GridID, 'EM.FormW4OtherIncome' as componentID, 'EM' as tablename, 'FormW4OtherIncome' as fieldname, 'payroll' as tabID, 0 as essential union
select 'Employees' as infocenterArea, 'X' as GridID, 'EM.FormW4Deductions' as componentID, 'EM' as tablename, 'FormW4Deductions' as fieldname, 'payroll' as tabID, 0 as essential union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.FaxFormat' as componentID, 'EM' as tablename, 'FaxFormat' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.FAX' as componentID, 'EM' as tablename, 'FAX' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ExitInterviewerName' as componentID, 'EM' as tablename, 'ExitInterviewerName' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ExitInterviewer' as componentID, 'EM' as tablename, 'ExitInterviewer' as fieldname, 'noassociation' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Ethnicity' as componentID, 'EM' as tablename, 'Ethnicity' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.EmployeeCompanyName' as componentID, 'EM' as tablename, 'EmployeeCompanyName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.EmployeeCompany' as componentID, 'EM' as tablename, 'EmployeeCompany' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Employee' as componentID, 'EM' as tablename, 'Employee' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.EmailPayrollRemittance' as componentID, 'EM' as tablename, 'EmailPayrollRemittance' as fieldname, 'accounting' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.EmailExpenseRemittance' as componentID, 'EM' as tablename, 'EmailExpenseRemittance' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.EMail' as componentID, 'EM' as tablename, 'EMail' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.EKGroup' as componentID, 'EM' as tablename, 'EKGroup' as fieldname, 'timeAndExpenseTab' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.EKAdminLevel' as componentID, 'EM' as tablename, 'EKAdminLevel' as fieldname, 'timeAndExpenseTab' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.EKAdminEdit' as componentID, 'EM' as tablename, 'EKAdminEdit' as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.DisableTSRevAudit' as componentID, 'EM' as tablename, 'DisableTSRevAudit' as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.DefaultTaxLocation' as componentID, 'EM' as tablename, 'DefaultTaxLocation' as fieldname, 'timeAndExpenseTab' as tabID, 1 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Disabled' as componentID, 'EM' as tablename, 'Disabled' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.DefaultLC5' as componentID, 'EM' as tablename, 'DefaultLC5' as fieldname, 'timeAndExpenseTab' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.DefaultLC4' as componentID, 'EM' as tablename, 'DefaultLC4' as fieldname, 'timeAndExpenseTab' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.DefaultLC3' as componentID, 'EM' as tablename, 'DefaultLC3' as fieldname, 'timeAndExpenseTab' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.DefaultLC2' as componentID, 'EM' as tablename, 'DefaultLC2' as fieldname, 'timeAndExpenseTab' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.DefaultLC1' as componentID, 'EM' as tablename, 'DefaultLC1' as fieldname, 'timeAndExpenseTab' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.DefaultBreakStartDateTime' as componentID, 'EM' as tablename, 'DefaultBreakStartDateTime' as fieldname, 'timeAndExpenseTab' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.DefaultBreakEndDateTime' as componentID, 'EM' as tablename, 'DefaultBreakEndDateTime' as fieldname, 'timeAndExpenseTab' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.CreateUser' as componentID, 'EM' as tablename, 'CreateUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.CreateUserName' as componentID, 'EM' as tablename, 'CreateUserName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.CreateDate' as componentID, 'EM' as tablename, 'CreateDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Country' as componentID, 'EM' as tablename, 'Country' as fieldname, 'personal_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.CostRateTableNo' as componentID, 'EM' as tablename, 'CostRateTableNo' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.CostRateMeth' as componentID, 'EM' as tablename, 'CostRateMeth' as fieldname, 'accounting' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ContactInfo' as componentID, null as tablename, null as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ConsultantInd' as componentID, 'EM' as tablename, 'ConsultantInd' as fieldname, 'overview' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ClieOpTransactionType' as componentID, 'EM' as tablename, 'ClieOpTransactionType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ClieOpAccountType' as componentID, 'EM' as tablename, 'ClieOpAccountType' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ClieOpAccount' as componentID, 'EM' as tablename, 'ClieOpAccount' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ClieOp' as componentID, 'EM' as tablename, 'ClieOp' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ClientVendorInd' as componentID, 'EM' as tablename, 'ClientVendorInd' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ClientID' as componentID, 'EM' as tablename, 'ClientID' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ClendorName' as componentID, 'EM' as tablename, 'ClendorName' as fieldname, 'sidebar' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Class' as componentID, 'EM' as tablename, 'Class' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.City' as componentID, 'EM' as tablename, 'City' as fieldname, 'personal_web' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.CitizenshipStatus' as componentID, 'EM' as tablename, 'CitizenshipStatus' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.CheckHours' as componentID, 'EM' as tablename, 'CheckHours' as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ChangeDefaultLC' as componentID, 'EM' as tablename, 'ChangeDefaultLC' as fieldname, 'timeAndExpenseTab' as tabID, 1 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.BirthDate' as componentID, 'EM' as tablename, 'BirthDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.BillingCategory' as componentID, 'EM' as tablename, 'BillingCategory' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.AvailableTo' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.AvailableForCRM' as componentID, 'EM' as tablename, 'AvailableForCRM' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.AllowChargeUnits' as componentID, 'EM' as tablename, 'AllowChargeUnits' as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.AllowBreakTime' as componentID, 'EM' as tablename, 'AllowBreakTime' as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.AlienNumber' as componentID, 'EM' as tablename, 'AlienNumber' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ADPRateCodeORPaychexRateNumber' as componentID, null as tablename, null as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ADPRateCode' as componentID, 'EM' as tablename, 'ADPRateCode' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ADPFileNumber' as componentID, 'EM' as tablename, 'ADPFileNumber' as fieldname, 'accounting' as tabID, 1 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ADPCompanyCode' as componentID, 'EM' as tablename, 'ADPCompanyCode' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Address3' as componentID, 'EM' as tablename, 'Address3' as fieldname, 'personal_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Address2' as componentID, 'EM' as tablename, 'Address2' as fieldname, 'personal_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Address1' as componentID, 'EM' as tablename, 'Address1' as fieldname, 'personal_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Address' as componentID, null as tablename, null as fieldname, 'personal_web' as tabID, 0 as essential union 
--Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ADA' as componentID, 'EM' as tablename, 'ADA' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'Activity' as componentID, null as tablename, null as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'PR' as GridID, 'TotalHours' as componentID, 'PR' as tablename, 'TotalHours' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'PR' as GridID, 'RT_Status' as componentID, 'PR' as tablename, 'RT_Status' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'PR' as GridID, 'ProjMgrName' as componentID, 'PR' as tablename, 'ProjMgrName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'PR' as GridID, 'ProjectName' as componentID, 'PR' as tablename, 'ProjectName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'PR' as GridID, 'ProjectID' as componentID, 'PR' as tablename, 'ProjectID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'PR' as GridID, 'MinDate' as componentID, 'PR' as tablename, 'MinDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'PR' as GridID, 'MaxDate' as componentID, 'PR' as tablename, 'MaxDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'PR' as GridID, 'KonaSpace' as componentID, 'PR' as tablename, 'KonaSpace' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'PR' as GridID, 'ClientName' as componentID, 'PR' as tablename, 'ClientName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'PKey' as componentID, 'FW_ATTACHMENTS' as tablename, 'PKey' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key3' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key3' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key2' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key2' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key1' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key1' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileSize' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileSize' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileName' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileName' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileID' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileDescription' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileDescription' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Employee' as componentID, 'FW_ATTACHMENTS' as tablename, 'Employee' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'ContentType' as componentID, 'FW_ATTACHMENTS' as tablename, 'ContentType' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'CategoryDescription' as componentID, 'FW_ATTACHMENTS' as tablename, 'CategoryDescription' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'CategoryCode' as componentID, 'FW_ATTACHMENTS' as tablename, 'CategoryCode' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Application' as componentID, 'FW_ATTACHMENTS' as tablename, 'Application' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EQICEMAssoc' as GridID, 'SerialNumber' as componentID, 'EQICEMAssoc' as tablename, 'SerialNumber' as fieldname, 'assets' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EQICEMAssoc' as GridID, 'ModelNumber' as componentID, 'EQICEMAssoc' as tablename, 'ModelNumber' as fieldname, 'assets' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EQICEMAssoc' as GridID, 'Manufacturer' as componentID, 'EQICEMAssoc' as tablename, 'Manufacturer' as fieldname, 'assets' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EQICEMAssoc' as GridID, 'Description' as componentID, 'EQICEMAssoc' as tablename, 'Description' as fieldname, 'assets' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EQICEMAssoc' as GridID, 'Company' as componentID, 'EQICEMAssoc' as tablename, 'Company' as fieldname, 'assets' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EQICEMAssoc' as GridID, 'AssignedDate' as componentID, 'EQICEMAssoc' as tablename, 'AssignedDate' as fieldname, 'assets' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EQICEMAssoc' as GridID, 'AssetNumber' as componentID, 'EQICEMAssoc' as tablename, 'AssetNumber' as fieldname, 'assets' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMTKGROUPS' as GridID, 'TKGroupDesc' as componentID, 'EMTKGROUPS' as tablename, 'TKGroupDesc' as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMTKGROUPS' as GridID, 'TKGroup' as componentID, 'EMTKGROUPS' as tablename, 'TKGroup' as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMTKGROUPS' as GridID, 'Employee' as componentID, 'EMTKGROUPS' as tablename, 'Employee' as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMTKGROUPS' as GridID, 'Company' as componentID, 'EMTKGROUPS' as tablename, 'Company' as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMTKGROUPS' as GridID, 'AllowEdit' as componentID, 'EMTKGROUPS' as tablename, 'AllowEdit' as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMTIMERATIOS' as GridID, 'YTD' as componentID, 'EMTIMERATIOS' as tablename, 'YTD' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMTIMERATIOS' as GridID, 'ShowRatioYTD' as componentID, 'EMTIMERATIOS' as tablename, 'ShowRatioYTD' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMTIMERATIOS' as GridID, 'ShowRatioQTD' as componentID, 'EMTIMERATIOS' as tablename, 'ShowRatioQTD' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMTIMERATIOS' as GridID, 'ShowRatioMTD' as componentID, 'EMTIMERATIOS' as tablename, 'ShowRatioMTD' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMTIMERATIOS' as GridID, 'ShowRatioCur' as componentID, 'EMTIMERATIOS' as tablename, 'ShowRatioCur' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMTIMERATIOS' as GridID, 'Ratios' as componentID, 'EMTIMERATIOS' as tablename, 'Ratios' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMTIMERATIOS' as GridID, 'QTD' as componentID, 'EMTIMERATIOS' as tablename, 'QTD' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMTIMERATIOS' as GridID, 'MTD' as componentID, 'EMTIMERATIOS' as tablename, 'MTD' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMTIMERATIOS' as GridID, 'CUR' as componentID, 'EMTIMERATIOS' as tablename, 'CUR' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMSkills' as GridID, 'SkillUsageDescription' as componentID, 'EMSkills' as tablename, 'SkillUsageDescription' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMSkills' as GridID, 'SkillUsage' as componentID, 'EMSkills' as tablename, 'SkillUsage' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMSkills' as GridID, 'SkillLevelDescription' as componentID, 'EMSkills' as tablename, 'SkillLevelDescription' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMSkills' as GridID, 'SkillLevel' as componentID, 'EMSkills' as tablename, 'SkillLevel' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMSkills' as GridID, 'SkillDescription' as componentID, 'EMSkills' as tablename, 'SkillDescription' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMSkills' as GridID, 'Skill' as componentID, 'EMSkills' as tablename, 'Skill' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMSkills' as GridID, 'PrimaryInd' as componentID, 'EMSkills' as tablename, 'PrimaryInd' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMSkills' as GridID, 'Employee' as componentID, 'EMSkills' as tablename, 'Employee' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMResume' as GridID, 'ResumeText' as componentID, 'EMResume' as tablename, 'ResumeText' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMResume' as GridID, 'ResumeCategoryDescription' as componentID, 'EMResume' as tablename, 'ResumeCategoryDescription' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMResume' as GridID, 'ResumeCategory' as componentID, 'EMResume' as tablename, 'ResumeCategory' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMResume' as GridID, 'Employee' as componentID, 'EMResume' as tablename, 'Employee' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMResume' as GridID, 'DefaultInd' as componentID, 'EMResume' as tablename, 'DefaultInd' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMRegistration' as GridID, 'TypeDescription' as componentID, 'EMRegistration' as tablename, 'TypeDescription' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMRegistration' as GridID, 'StatusDescription' as componentID, 'EMRegistration' as tablename, 'StatusDescription' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMRegistration' as GridID, 'StateRegisteredDescription' as componentID, 'EMRegistration' as tablename, 'StateRegisteredDescription' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMRegistration' as GridID, 'StateRegistered' as componentID, 'EMRegistration' as tablename, 'StateRegistered' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMRegistration' as GridID, 'Seq' as componentID, 'EMRegistration' as tablename, 'Seq' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMRegistration' as GridID, 'RegistrationType' as componentID, 'EMRegistration' as tablename, 'RegistrationType' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMRegistration' as GridID, 'RegistrationNo' as componentID, 'EMRegistration' as tablename, 'RegistrationNo' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMRegistration' as GridID, 'RegistrationDescription' as componentID, 'EMRegistration' as tablename, 'RegistrationDescription' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMRegistration' as GridID, 'Registration' as componentID, 'EMRegistration' as tablename, 'Registration' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMRegistration' as GridID, 'RecordID' as componentID, 'EMRegistration' as tablename, 'RecordID' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMRegistration' as GridID, 'LastRenewed' as componentID, 'EMRegistration' as tablename, 'LastRenewed' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMRegistration' as GridID, 'IncludeInProposal' as componentID, 'EMRegistration' as tablename, 'IncludeInProposal' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMRegistration' as GridID, 'Employee' as componentID, 'EMRegistration' as tablename, 'Employee' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMRegistration' as GridID, 'DateExpires' as componentID, 'EMRegistration' as tablename, 'DateExpires' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMRegistration' as GridID, 'DateEarned' as componentID, 'EMRegistration' as tablename, 'DateEarned' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMRegistration' as GridID, 'CountryRegisteredDescription' as componentID, 'EMRegistration' as tablename, 'CountryRegisteredDescription' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMRegistration' as GridID, 'CountryRegistered' as componentID, 'EMRegistration' as tablename, 'CountryRegistered' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'WBS3' as componentID, 'EMProjectAssoc' as tablename, 'WBS3' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'WBS2' as componentID, 'EMProjectAssoc' as tablename, 'WBS2' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'WBS1' as componentID, 'EMProjectAssoc' as tablename, 'WBS1' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'TotalContractValue' as componentID, 'EMProjectAssoc' as tablename, 'TotalContractValue' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'TeamStatus' as componentID, 'EMProjectAssoc' as tablename, 'TeamStatus' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'SupervisorName' as componentID, 'EMProjectAssoc' as tablename, 'SupervisorName' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'Status' as componentID, 'EMProjectAssoc' as tablename, 'Status' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'StartDate' as componentID, 'EMProjectAssoc' as tablename, 'StartDate' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'Stage' as componentID, 'EMProjectAssoc' as tablename, 'Stage' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'RoleDescription' as componentID, 'EMProjectAssoc' as tablename, 'RoleDescription' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'RoleDesc' as componentID, 'EMProjectAssoc' as tablename, 'RoleDesc' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'Role' as componentID, 'EMProjectAssoc' as tablename, 'Role' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'Revenue' as componentID, 'EMProjectAssoc' as tablename, 'Revenue' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'RecordID' as componentID, 'EMProjectAssoc' as tablename, 'RecordID' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'ProjMgrName' as componentID, 'EMProjectAssoc' as tablename, 'ProjMgrName' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'ProjectZip' as componentID, 'EMProjectAssoc' as tablename, 'ProjectZip' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'ProjectType' as componentID, 'EMProjectAssoc' as tablename, 'ProjectType' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'ProjectState' as componentID, 'EMProjectAssoc' as tablename, 'ProjectState' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'ProjectCounty' as componentID, 'EMProjectAssoc' as tablename, 'ProjectCounty' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'ProjectCountry' as componentID, 'EMProjectAssoc' as tablename, 'ProjectCountry' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'ProjectCity' as componentID, 'EMProjectAssoc' as tablename, 'ProjectCity' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'ProjectAddress3' as componentID, 'EMProjectAssoc' as tablename, 'ProjectAddress3' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'ProjectAddress2' as componentID, 'EMProjectAssoc' as tablename, 'ProjectAddress2' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'ProjectAddress1' as componentID, 'EMProjectAssoc' as tablename, 'ProjectAddress1' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'Project' as componentID, 'EMProjectAssoc' as tablename, 'Project' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'ProfServicesComplDate' as componentID, 'EMProjectAssoc' as tablename, 'ProfServicesComplDate' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'PrincipalName' as componentID, 'EMProjectAssoc' as tablename, 'PrincipalName' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'PrimaryContactWorkPhone' as componentID, 'EMProjectAssoc' as tablename, 'PrimaryContactWorkPhone' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'PrimaryContactID' as componentID, 'EMProjectAssoc' as tablename, 'PrimaryContactID' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'PrimaryContactEmail' as componentID, 'EMProjectAssoc' as tablename, 'PrimaryContactEmail' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'PrimaryContact' as componentID, 'EMProjectAssoc' as tablename, 'PrimaryContact' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'PrimaryClientID' as componentID, 'EMProjectAssoc' as tablename, 'PrimaryClientID' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'PrimaryClient' as componentID, 'EMProjectAssoc' as tablename, 'PrimaryClient' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'OwnerName' as componentID, 'EMProjectAssoc' as tablename, 'OwnerName' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'OrgName' as componentID, 'EMProjectAssoc' as tablename, 'OrgName' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'Memo' as componentID, 'EMProjectAssoc' as tablename, 'Memo' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'LongName' as componentID, 'EMProjectAssoc' as tablename, 'LongName' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'FirmCost' as componentID, 'EMProjectAssoc' as tablename, 'FirmCost' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'Fee' as componentID, 'EMProjectAssoc' as tablename, 'Fee' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'EstStartDate' as componentID, 'EMProjectAssoc' as tablename, 'EstStartDate' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'EstCompletionDate' as componentID, 'EMProjectAssoc' as tablename, 'EstCompletionDate' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'EndDate' as componentID, 'EMProjectAssoc' as tablename, 'EndDate' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'Employee' as componentID, 'EMProjectAssoc' as tablename, 'Employee' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'CRMHours' as componentID, 'EMProjectAssoc' as tablename, 'CRMHours' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'ContractDate' as componentID, 'EMProjectAssoc' as tablename, 'ContractDate' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'ConstComplDate' as componentID, 'EMProjectAssoc' as tablename, 'ConstComplDate' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'CloseDate' as componentID, 'EMProjectAssoc' as tablename, 'CloseDate' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'ActualStartDate' as componentID, 'EMProjectAssoc' as tablename, 'ActualStartDate' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMProjectAssoc' as GridID, 'ActCompletionDate' as componentID, 'EMProjectAssoc' as tablename, 'ActCompletionDate' as fieldname, 'experience_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPLOYEEFILELINKS' as GridID, 'LinkID' as componentID, 'EmployeeFileLinks' as tablename, 'LinkID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPLOYEEFILELINKS' as GridID, 'Graphic' as componentID, 'EmployeeFileLinks' as tablename, 'Graphic' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPLOYEEFILELINKS' as GridID, 'FilePath' as componentID, 'EmployeeFileLinks' as tablename, 'FilePath' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPLOYEEFILELINKS' as GridID, 'Employee' as componentID, 'EmployeeFileLinks' as tablename, 'Employee' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPLOYEEFILELINKS' as GridID, 'Description' as componentID, 'EmployeeFileLinks' as tablename, 'Description' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'UseSecondaryExemptions' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'UseSecondaryExemptions' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'UsePrimaryExemptions' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'UsePrimaryExemptions' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'Type' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'Type' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'SystemSuppress' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'SystemSuppress' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'Suppress' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'Suppress' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'PrintOnCheck' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'PrintOnCheck' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'OtherExemptions' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'OtherExemptions' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'Method' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'Method' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'Locale' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'Locale' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'LimitLinkCode' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'LimitLinkCode' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'Limit' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'Limit' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'FilingStatusRequired' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'FilingStatusRequired' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'FilingStatus' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'FilingStatus' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'Exemptions' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'Exemptions' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'EmployeeCompany' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'EmployeeCompany' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'Employee' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'Employee' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'Code' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'Code' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'AmtPct' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'AmtPct' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLWITHHOLDING' as GridID, 'AdditionalAmt' as componentID, 'EMPAYROLLWITHHOLDING' as tablename, 'AdditionalAmt' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLCONTRIBUTION' as GridID, 'SystemSuppress' as componentID, 'EMPAYROLLCONTRIBUTION' as tablename, 'SystemSuppress' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLCONTRIBUTION' as GridID, 'Suppress' as componentID, 'EMPAYROLLCONTRIBUTION' as tablename, 'Suppress' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLCONTRIBUTION' as GridID, 'Method' as componentID, 'EMPAYROLLCONTRIBUTION' as tablename, 'Method' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLCONTRIBUTION' as GridID, 'Limit' as componentID, 'EMPAYROLLCONTRIBUTION' as tablename, 'Limit' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLCONTRIBUTION' as GridID, 'EmployeeCompany' as componentID, 'EMPAYROLLCONTRIBUTION' as tablename, 'EmployeeCompany' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLCONTRIBUTION' as GridID, 'Employee' as componentID, 'EMPAYROLLCONTRIBUTION' as tablename, 'Employee' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLCONTRIBUTION' as GridID, 'Code' as componentID, 'EMPAYROLLCONTRIBUTION' as tablename, 'Code' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLCONTRIBUTION' as GridID, 'AmtPct' as componentID, 'EMPAYROLLCONTRIBUTION' as tablename, 'AmtPct' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMPAYROLLCONTRIBUTION' as GridID, 'AdditionalAmt' as componentID, 'EMPAYROLLCONTRIBUTION' as tablename, 'AdditionalAmt' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMLOCALE' as GridID, 'WagePct' as componentID, 'EMLOCALE' as tablename, 'WagePct' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMLOCALE' as GridID, 'Type' as componentID, 'EMLOCALE' as tablename, 'Type' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMLOCALE' as GridID, 'Locale' as componentID, 'EMLOCALE' as tablename, 'Locale' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMLOCALE' as GridID, 'EmployeeCompany' as componentID, 'EMLOCALE' as tablename, 'EmployeeCompany' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMLOCALE' as GridID, 'Employee' as componentID, 'EMLOCALE' as tablename, 'Employee' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMLOCALE' as GridID, 'Code' as componentID, 'EMLOCALE' as tablename, 'Code' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMLOCALE' as GridID, 'Active' as componentID, 'EMLOCALE' as tablename, 'Active' as fieldname, 'payroll' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMEKGROUPS' as GridID, 'Employee' as componentID, 'EMEKGROUPS' as tablename, 'Employee' as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMEKGROUPS' as GridID, 'EKGroupDesc' as componentID, 'EMEKGROUPS' as tablename, 'EKGroupDesc' as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMEKGROUPS' as GridID, 'EKGroup' as componentID, 'EMEKGROUPS' as tablename, 'EKGroup' as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMEKGROUPS' as GridID, 'Company' as componentID, 'EMEKGROUPS' as tablename, 'Company' as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMEKGROUPS' as GridID, 'AllowEdit' as componentID, 'EMEKGROUPS' as tablename, 'AllowEdit' as fieldname, 'timeAndExpenseTab' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDirectDeposit' as GridID, 'Status' as componentID, 'EMDirectDeposit' as tablename, 'Status' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDirectDeposit' as GridID, 'Seq' as componentID, 'EMDirectDeposit' as tablename, 'Seq' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDirectDeposit' as GridID, 'Override' as componentID, 'EMDirectDeposit' as tablename, 'Override' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDirectDeposit' as GridID, 'Method' as componentID, 'EMDirectDeposit' as tablename, 'Method' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDirectDeposit' as GridID, 'exMethod' as componentID, 'EMDirectDeposit' as tablename, 'exMethod' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDirectDeposit' as GridID, 'exAmtPct' as componentID, 'EMDirectDeposit' as tablename, 'exAmtPct' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDirectDeposit' as GridID, 'EmployeeCompany' as componentID, 'EMDirectDeposit' as tablename, 'EmployeeCompany' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDirectDeposit' as GridID, 'Employee' as componentID, 'EMDirectDeposit' as tablename, 'Employee' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDirectDeposit' as GridID, 'BankID' as componentID, 'EMDirectDeposit' as tablename, 'BankID' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDirectDeposit' as GridID, 'AmtPct' as componentID, 'EMDirectDeposit' as tablename, 'AmtPct' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDirectDeposit' as GridID, 'AccountType' as componentID, 'EMDirectDeposit' as tablename, 'AccountType' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDirectDeposit' as GridID, 'Account' as componentID, 'EMDirectDeposit' as tablename, 'Account' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDegree' as GridID, 'YearEarned' as componentID, 'EMDegree' as tablename, 'YearEarned' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDegree' as GridID, 'Specialty' as componentID, 'EMDegree' as tablename, 'Specialty' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDegree' as GridID, 'Seq' as componentID, 'EMDegree' as tablename, 'Seq' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDegree' as GridID, 'RecordID' as componentID, 'EMDegree' as tablename, 'RecordID' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDegree' as GridID, 'Institution' as componentID, 'EMDegree' as tablename, 'Institution' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDegree' as GridID, 'IncludeInProposal' as componentID, 'EMDegree' as tablename, 'IncludeInProposal' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDegree' as GridID, 'Employee' as componentID, 'EMDegree' as tablename, 'Employee' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDegree' as GridID, 'DegreeDescription' as componentID, 'EMDegree' as tablename, 'DegreeDescription' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMDegree' as GridID, 'Degree' as componentID, 'EMDegree' as tablename, 'Degree' as fieldname, 'professional' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMContactAssoc' as GridID, 'RelationshipDesc' as componentID, 'EMContactAssoc' as tablename, 'RelationshipDesc' as fieldname, 'firmContact' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMContactAssoc' as GridID, 'Relationship' as componentID, 'EMContactAssoc' as tablename, 'Relationship' as fieldname, 'firmContact' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMContactAssoc' as GridID, 'Employee' as componentID, 'EMContactAssoc' as tablename, 'Employee' as fieldname, 'firmContact' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMContactAssoc' as GridID, 'ContactStatus' as componentID, 'EMContactAssoc' as tablename, 'ContactStatus' as fieldname, 'firmContact' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMContactAssoc' as GridID, 'ContactName' as componentID, 'EMContactAssoc' as tablename, 'ContactName' as fieldname, 'firmContact' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMContactAssoc' as GridID, 'ContactID' as componentID, 'EMContactAssoc' as tablename, 'ContactID' as fieldname, 'firmContact' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMClientAssoc' as GridID, 'Status' as componentID, 'EMClientAssoc' as tablename, 'Status' as fieldname, 'firmContact' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMClientAssoc' as GridID, 'RelationshipDesc' as componentID, 'EMClientAssoc' as tablename, 'RelationshipDesc' as fieldname, 'firmContact' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMClientAssoc' as GridID, 'Relationship' as componentID, 'EMClientAssoc' as tablename, 'Relationship' as fieldname, 'firmContact' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMClientAssoc' as GridID, 'FirmName' as componentID, 'EMClientAssoc' as tablename, 'FirmName' as fieldname, 'firmContact' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMClientAssoc' as GridID, 'Employee' as componentID, 'EMClientAssoc' as tablename, 'Employee' as fieldname, 'firmContact' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMClientAssoc' as GridID, 'ClientID' as componentID, 'EMClientAssoc' as tablename, 'ClientID' as fieldname, 'firmContact' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMCitizenship' as GridID, 'RecordID' as componentID, 'EMCitizenship' as tablename, 'RecordID' as fieldname, 'personal_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMCitizenship' as GridID, 'Expiration' as componentID, 'EMCitizenship' as tablename, 'Expiration' as fieldname, 'personal_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMCitizenship' as GridID, 'Employee' as componentID, 'EMCitizenship' as tablename, 'Employee' as fieldname, 'personal_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMCitizenship' as GridID, 'CountryDescription' as componentID, 'EMCitizenship' as tablename, 'CountryDescription' as fieldname, 'personal_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMCitizenship' as GridID, 'Country' as componentID, 'EMCitizenship' as tablename, 'Country' as fieldname, 'personal_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMCitizenship' as GridID, 'CitizenshipTypeDescription' as componentID, 'EMCitizenship' as tablename, 'CitizenshipTypeDescription' as fieldname, 'personal_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMCitizenship' as GridID, 'CitizenshipType' as componentID, 'EMCitizenship' as tablename, 'CitizenshipType' as fieldname, 'personal_web' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMBenefitHours' as GridID, 'UnpostedTime' as componentID, 'EMBenefitHours' as tablename, 'UnpostedTime' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMBenefitHours' as GridID, 'StartBal' as componentID, 'EMBenefitHours' as tablename, 'StartBal' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMBenefitHours' as GridID, 'EnableApprovalWorkflow' as componentID, 'EMBenefitHours' as tablename, 'EnableApprovalWorkflow' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMBenefitHours' as GridID, 'Employee' as componentID, 'EMBenefitHours' as tablename, 'Employee' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMBenefitHours' as GridID, 'Description' as componentID, 'EMBenefitHours' as tablename, 'Description' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMBenefitHours' as GridID, 'CurYrTaken' as componentID, 'EMBenefitHours' as tablename, 'CurYrTaken' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMBenefitHours' as GridID, 'CurYrEarn' as componentID, 'EMBenefitHours' as tablename, 'CurYrEarn' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMBenefitHours' as GridID, 'CurBal' as componentID, 'EMBenefitHours' as tablename, 'CurBal' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMAccrual' as GridID, 'UseSchedule' as componentID, 'EMAccrual' as tablename, 'UseSchedule' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMAccrual' as GridID, 'ScheduleID' as componentID, 'EMAccrual' as tablename, 'ScheduleID' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMAccrual' as GridID, 'ScheduleDescription' as componentID, 'EMAccrual' as tablename, 'ScheduleDescription' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMAccrual' as GridID, 'Maximum' as componentID, 'EMAccrual' as tablename, 'Maximum' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMAccrual' as GridID, 'MaxHoursPerProcess' as componentID, 'EMAccrual' as tablename, 'MaxHoursPerProcess' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMAccrual' as GridID, 'HoursPerYear' as componentID, 'EMAccrual' as tablename, 'HoursPerYear' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMAccrual' as GridID, 'HoursPerHourWorked' as componentID, 'EMAccrual' as tablename, 'HoursPerHourWorked' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMAccrual' as GridID, 'HasMaximum' as componentID, 'EMAccrual' as tablename, 'HasMaximum' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMAccrual' as GridID, 'HasCarryoverLimit' as componentID, 'EMAccrual' as tablename, 'HasCarryoverLimit' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMAccrual' as GridID, 'EmployeeCompany' as componentID, 'EMAccrual' as tablename, 'EmployeeCompany' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMAccrual' as GridID, 'Employee' as componentID, 'EMAccrual' as tablename, 'Employee' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMAccrual' as GridID, 'Description' as componentID, 'EMAccrual' as tablename, 'Description' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMAccrual' as GridID, 'DefaultScheduleID' as componentID, 'EMAccrual' as tablename, 'DefaultScheduleID' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMAccrual' as GridID, 'Code' as componentID, 'EMAccrual' as tablename, 'Code' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMAccrual' as GridID, 'ChangeDate' as componentID, 'EMAccrual' as tablename, 'ChangeDate' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'EMAccrual' as GridID, 'CarryoverLimit' as componentID, 'EMAccrual' as tablename, 'CarryoverLimit' as fieldname, 'accounting' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'WBS3' as componentID, 'Activity' as tablename, 'WBS3' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'WBS2' as componentID, 'Activity' as tablename, 'WBS2' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'WBS1' as componentID, 'Activity' as tablename, 'WBS1' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'VendorName' as componentID, 'Activity' as tablename, 'VendorName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'VendorContactName' as componentID, 'Activity' as tablename, 'VendorContactName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'Vendor' as componentID, 'Activity' as tablename, 'Vendor' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'UpdateID' as componentID, 'Activity' as tablename, 'UpdateID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'TypeName' as componentID, 'Activity' as tablename, 'TypeName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'Type' as componentID, 'Activity' as tablename, 'Type' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'TaskCompletionDate' as componentID, 'Activity' as tablename, 'TaskCompletionDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'Subject' as componentID, 'Activity' as tablename, 'Subject' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'StartDateObj' as componentID, 'Activity' as tablename, 'StartDateObj' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'StartDate' as componentID, 'Activity' as tablename, 'StartDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'ShowTimeAs' as componentID, 'Activity' as tablename, 'ShowTimeAs' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RP' as componentID, 'Activity' as tablename, 'RP' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'ReminderUnit' as componentID, 'Activity' as tablename, 'ReminderUnit' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'ReminderMinHrDay' as componentID, 'Activity' as tablename, 'ReminderMinHrDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'ReminderInd' as componentID, 'Activity' as tablename, 'ReminderInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'ReminderDate' as componentID, 'Activity' as tablename, 'ReminderDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccurMonth' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurMonth' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccurInd' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccurDay' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccur' as componentID, 'Activity' as tablename, 'RecurrYearlyOccur' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyMonth' as componentID, 'Activity' as tablename, 'RecurrYearlyMonth' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyDay' as componentID, 'Activity' as tablename, 'RecurrYearlyDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyWed' as componentID, 'Activity' as tablename, 'RecurrWeeklyWed' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyTue' as componentID, 'Activity' as tablename, 'RecurrWeeklyTue' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyThu' as componentID, 'Activity' as tablename, 'RecurrWeeklyThu' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklySun' as componentID, 'Activity' as tablename, 'RecurrWeeklySun' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklySat' as componentID, 'Activity' as tablename, 'RecurrWeeklySat' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyMon' as componentID, 'Activity' as tablename, 'RecurrWeeklyMon' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyFri' as componentID, 'Activity' as tablename, 'RecurrWeeklyFri' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyFreq' as componentID, 'Activity' as tablename, 'RecurrWeeklyFreq' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrType' as componentID, 'Activity' as tablename, 'RecurrType' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrStartDate' as componentID, 'Activity' as tablename, 'RecurrStartDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccurInd' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccurFreq' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurFreq' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccurDay' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccur' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccur' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyFreq' as componentID, 'Activity' as tablename, 'RecurrMonthlyFreq' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyDay' as componentID, 'Activity' as tablename, 'RecurrMonthlyDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrID' as componentID, 'Activity' as tablename, 'RecurrID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrEndType' as componentID, 'Activity' as tablename, 'RecurrEndType' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrEndDate' as componentID, 'Activity' as tablename, 'RecurrEndDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrenceInd' as componentID, 'Activity' as tablename, 'RecurrenceInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrDailyWeekDay' as componentID, 'Activity' as tablename, 'RecurrDailyWeekDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'RecurrDailyFreq' as componentID, 'Activity' as tablename, 'RecurrDailyFreq' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'ReadID' as componentID, 'Activity' as tablename, 'ReadID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'ProjName' as componentID, 'Activity' as tablename, 'ProjName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'PrivateInd' as componentID, 'Activity' as tablename, 'PrivateInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'PriorityDesc' as componentID, 'Activity' as tablename, 'PriorityDesc' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'Priority' as componentID, 'Activity' as tablename, 'Priority' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'PrimaryClient' as componentID, 'Activity' as tablename, 'PrimaryClient' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'OwnerName' as componentID, 'Activity' as tablename, 'OwnerName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'Owner' as componentID, 'Activity' as tablename, 'Owner' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'OutlookType' as componentID, 'Activity' as tablename, 'OutlookType' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'Notes' as componentID, 'Activity' as tablename, 'Notes' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'ModUser' as componentID, 'Activity' as tablename, 'ModUser' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'ModDate' as componentID, 'Activity' as tablename, 'ModDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'MktName' as componentID, 'Activity' as tablename, 'MktName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'MaxOccurences' as componentID, 'Activity' as tablename, 'MaxOccurences' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'Location' as componentID, 'Activity' as tablename, 'Location' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'LeadID' as componentID, 'Activity' as tablename, 'LeadID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'isTask' as componentID, 'Activity' as tablename, 'isTask' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'isCalendar' as componentID, 'Activity' as tablename, 'isCalendar' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'InstanceStartDate' as componentID, 'Activity' as tablename, 'InstanceStartDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'HasNotes' as componentID, 'Activity' as tablename, 'HasNotes' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'EndDateSort' as componentID, 'Activity' as tablename, 'EndDateSort' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'EndDateObj' as componentID, 'Activity' as tablename, 'EndDateObj' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'EndDateFilter' as componentID, 'Activity' as tablename, 'EndDateFilter' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'EndDate' as componentID, 'Activity' as tablename, 'EndDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'Employee' as componentID, 'Activity' as tablename, 'Employee' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'Duration' as componentID, 'Activity' as tablename, 'Duration' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'CreateUser' as componentID, 'Activity' as tablename, 'CreateUser' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'CreateEmployee' as componentID, 'Activity' as tablename, 'CreateEmployee' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'CreateDate' as componentID, 'Activity' as tablename, 'CreateDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'ContactPhoneForVendor' as componentID, 'Activity' as tablename, 'ContactPhoneForVendor' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'ContactNameForVendor' as componentID, 'Activity' as tablename, 'ContactNameForVendor' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'ContactName' as componentID, 'Activity' as tablename, 'ContactName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'ContactIDForVendor' as componentID, 'Activity' as tablename, 'ContactIDForVendor' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'ContactID' as componentID, 'Activity' as tablename, 'ContactID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'CompletionInd' as componentID, 'Activity' as tablename, 'CompletionInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'ClientName' as componentID, 'Activity' as tablename, 'ClientName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'ClientID' as componentID, 'Activity' as tablename, 'ClientID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'CampaignID' as componentID, 'Activity' as tablename, 'CampaignID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'Attendees' as componentID, 'Activity' as tablename, 'Attendees' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'Associations' as componentID, 'Activity' as tablename, 'Associations' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'AllDayEventInd' as componentID, 'Activity' as tablename, 'AllDayEventInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'acType' as componentID, 'Activity' as tablename, 'acType' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Employees' as infocenterArea, 'Activity' as GridID, 'ActivityID' as componentID, 'Activity' as tablename, 'ActivityID' as fieldname, 'activities' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.Vendor' as componentID, 'VEAccounting' as tablename, 'Vendor' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.ThisYear1099' as componentID, 'VEAccounting' as tablename, 'ThisYear1099' as fieldname, 'vendor' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.SEPAIBAN' as componentID, 'VEAccounting' as tablename, 'SEPAIBAN' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.SEPABIC' as componentID, 'VEAccounting' as tablename, 'SEPABIC' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.Req1099' as componentID, 'VEAccounting' as tablename, 'Req1099' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.RegAccount' as componentID, 'VEAccounting' as tablename, 'RegAccount' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.PurchasingctName' as componentID, 'VEAccounting' as tablename, 'PurchasingctName' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.PurchasingContactID' as componentID, 'VEAccounting' as tablename, 'PurchasingContactID' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.POTemplate' as componentID, 'VEAccounting' as tablename, 'POTemplate' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.PayTerms' as componentID, 'VEAccounting' as tablename, 'PayTerms' as fieldname, 'vendor' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.OHAccount' as componentID, 'VEAccounting' as tablename, 'OHAccount' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.Memo' as componentID, 'VEAccounting' as tablename, 'Memo' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.MatchMethod' as componentID, 'VEAccounting' as tablename, 'MatchMethod' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.LastYear1099' as componentID, 'VEAccounting' as tablename, 'LastYear1099' as fieldname, 'vendor' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.ElectronicPaymentMethodID' as componentID, 'VEAccounting' as tablename, 'ElectronicPaymentMethodID' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.EFTStatus' as componentID, 'VEAccounting' as tablename, 'EFTStatus' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.EFTRemittance' as componentID, 'VEAccounting' as tablename, 'EFTRemittance' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.EFTFields' as componentID, null as tablename, null as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.EFTEmail' as componentID, 'VEAccounting' as tablename, 'EFTEmail' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.EFTClieOp' as componentID, 'VEAccounting' as tablename, 'EFTClieOp' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.EFTBankID' as componentID, 'VEAccounting' as tablename, 'EFTBankID' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.EFTAddenda' as componentID, 'VEAccounting' as tablename, 'EFTAddenda' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.EFTAccountType' as componentID, 'VEAccounting' as tablename, 'EFTAccountType' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.EFTAccount' as componentID, 'VEAccounting' as tablename, 'EFTAccount' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.DiscPeriod' as componentID, 'VEAccounting' as tablename, 'DiscPeriod' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.DiscPct' as componentID, 'VEAccounting' as tablename, 'DiscPct' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.DiscCode' as componentID, 'VEAccounting' as tablename, 'DiscCode' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.DefaultVoucherInformation' as componentID, null as tablename, null as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.DefaultTaxCode' as componentID, 'VEAccounting' as tablename, 'DefaultTaxCode' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.DefaultExpenseCode' as componentID, 'VEAccounting' as tablename, 'DefaultExpenseCode' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.Company' as componentID, 'VEAccounting' as tablename, 'Company' as fieldname, 'vendor' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.CheckPerVoucher' as componentID, 'VEAccounting' as tablename, 'CheckPerVoucher' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.BankZip' as componentID, 'VEAccounting' as tablename, 'BankZip' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.BankState' as componentID, 'VEAccounting' as tablename, 'BankState' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.BankName' as componentID, 'VEAccounting' as tablename, 'BankName' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.BankingInformation' as componentID, null as tablename, null as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.BankIDType' as componentID, 'VEAccounting' as tablename, 'BankIDType' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.BankID' as componentID, 'VEAccounting' as tablename, 'BankID' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.BankCountry' as componentID, 'VEAccounting' as tablename, 'BankCountry' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.BankCity' as componentID, 'VEAccounting' as tablename, 'BankCity' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.BankAddress' as componentID, null as tablename, null as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.BankAddr4' as componentID, 'VEAccounting' as tablename, 'BankAddr4' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.BankAddr3' as componentID, 'VEAccounting' as tablename, 'BankAddr3' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.BankAddr2' as componentID, 'VEAccounting' as tablename, 'BankAddr2' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.BankAddr1' as componentID, 'VEAccounting' as tablename, 'BankAddr1' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.BankAccountIDType' as componentID, 'VEAccounting' as tablename, 'BankAccountIDType' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.BankAccountID' as componentID, 'VEAccounting' as tablename, 'BankAccountID' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.AccountNumber' as componentID, 'VEAccounting' as tablename, 'AccountNumber' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'FW_ATTACHMENTS' as componentID, null as tablename, null as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'EMClientAssoc' as componentID, null as tablename, null as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Contacts' as componentID, null as tablename, null as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'ClientToClientAssoc' as componentID, null as tablename, null as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'ClientFileLinks' as componentID, null as tablename, null as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'ClientAlias' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'ClendorProjectAssoc' as componentID, null as tablename, null as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.WomanOwned' as componentID, 'Clendor' as tablename, 'WomanOwned' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.WebSite' as componentID, 'Clendor' as tablename, 'WebSite' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.VName' as componentID, 'Clendor' as tablename, 'VName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.VetOwnedSmallBusiness' as componentID, 'Clendor' as tablename, 'VetOwnedSmallBusiness' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.VendorInd' as componentID, 'Clendor' as tablename, 'VendorInd' as fieldname, 'sidebar' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Vendor' as componentID, 'Clendor' as tablename, 'Vendor' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.TypeOfTIN' as componentID, 'Clendor' as tablename, 'TypeOfTIN' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Type' as componentID, 'Clendor' as tablename, 'Type' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.TopTPSubject' as componentID, 'Clendor' as tablename, 'TopTPSubject' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.TopTPStartDate' as componentID, 'Clendor' as tablename, 'TopTPStartDate' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.TopTPCreateUserName' as componentID, 'Clendor' as tablename, 'TopTPCreateUserName' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.TopTPCreateEmployee' as componentID, 'Clendor' as tablename, 'TopTPCreateEmployee' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.TopTPContactID' as componentID, 'Clendor' as tablename, 'TopTPContactID' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.TopTPActivityID' as componentID, 'Clendor' as tablename, 'TopTPActivityID' as fieldname, 'sidebar' as tabID, 0 as essential union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.TLSyncModDate' as componentID, 'Clendor' as tablename, 'TLSyncModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.TLInternalKey' as componentID, 'Clendor' as tablename, 'TLInternalKey' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Status' as componentID, 'Clendor' as tablename, 'Status' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.SpecialtyType' as componentID, 'Clendor' as tablename, 'SpecialtyType' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Specialty' as componentID, 'Clendor' as tablename, 'Specialty' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.SortName' as componentID, 'Clendor' as tablename, 'SortName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.SocioEconomicStatus' as componentID, 'Clendor' as tablename, 'SocioEconomicStatus' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.SmallBusiness' as componentID, 'Clendor' as tablename, 'SmallBusiness' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.SFLastModifiedDate' as componentID, 'Clendor' as tablename, 'SFLastModifiedDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.SFID' as componentID, 'Clendor' as tablename, 'SFID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.RecordType' as componentID, 'Clendor' as tablename, 'RecordType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Recommend' as componentID, 'Clendor' as tablename, 'Recommend' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.RecentWBSName' as componentID, 'Clendor' as tablename, 'RecentWBSName' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.RecentWBS1' as componentID, 'Clendor' as tablename, 'RecentWBS1' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.RecentPRRole' as componentID, 'Clendor' as tablename, 'RecentPRRole' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.RecentPRRoleDescription' as componentID, 'Clendor' as tablename, 'RecentPRRoleDescription' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ReadyForProcessingForCompany' as componentID, 'Clendor' as tablename, 'ReadyForProcessingForCompany' as fieldname, 'vendor' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ReadyForProcessing' as componentID, 'Clendor' as tablename, 'ReadyForProcessing' as fieldname, 'vendor' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ReadyForApproval' as componentID, 'Clendor' as tablename, 'ReadyForApproval' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.QBOLastUpdated' as componentID, 'Clendor' as tablename, 'QBOLastUpdated' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.QBOID' as componentID, 'Clendor' as tablename, 'QBOID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PriorWork' as componentID, 'Clendor' as tablename, 'PriorWork' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PrimaryZip' as componentID, 'Clendor' as tablename, 'PrimaryZip' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PrimaryState' as componentID, 'Clendor' as tablename, 'PrimaryState' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PrimaryPhone' as componentID, 'Clendor' as tablename, 'PrimaryPhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PrimaryFax' as componentID, 'Clendor' as tablename, 'PrimaryFax' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PrimaryEmail' as componentID, 'Clendor' as tablename, 'PrimaryEmail' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PrimaryCountry' as componentID, 'Clendor' as tablename, 'PrimaryCountry' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PrimaryCity' as componentID, 'Clendor' as tablename, 'PrimaryCity' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PrimaryAddress4' as componentID, 'Clendor' as tablename, 'PrimaryAddress4' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PrimaryAddress3' as componentID, 'Clendor' as tablename, 'PrimaryAddress3' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PrimaryAddress2' as componentID, 'Clendor' as tablename, 'PrimaryAddress2' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PrimaryAddress1' as componentID, 'Clendor' as tablename, 'PrimaryAddress1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PhotoModDate' as componentID, 'Clendor' as tablename, 'PhotoModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PaymentZip' as componentID, 'Clendor' as tablename, 'PaymentZip' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PaymentState' as componentID, 'Clendor' as tablename, 'PaymentState' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PaymentPhoneFormat' as componentID, 'Clendor' as tablename, 'PaymentPhoneFormat' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PaymentPhone' as componentID, 'Clendor' as tablename, 'PaymentPhone' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PaymentFaxFormat' as componentID, 'Clendor' as tablename, 'PaymentFaxFormat' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PaymentFax' as componentID, 'Clendor' as tablename, 'PaymentFax' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PaymentEmail' as componentID, 'Clendor' as tablename, 'PaymentEmail' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PaymentCountry' as componentID, 'Clendor' as tablename, 'PaymentCountry' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PaymentCity' as componentID, 'Clendor' as tablename, 'PaymentCity' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PaymentAddress4' as componentID, 'Clendor' as tablename, 'PaymentAddress4' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PaymentAddress3' as componentID, 'Clendor' as tablename, 'PaymentAddress3' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PaymentAddress2' as componentID, 'Clendor' as tablename, 'PaymentAddress2' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PaymentAddress1' as componentID, 'Clendor' as tablename, 'PaymentAddress1' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PaymentAddress' as componentID, null as tablename, null as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ParentName' as componentID, 'Clendor' as tablename, 'ParentName' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ParentLevel4' as componentID, 'Clendor' as tablename, 'ParentLevel4' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ParentLevel3' as componentID, 'Clendor' as tablename, 'ParentLevel3' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ParentLevel2' as componentID, 'Clendor' as tablename, 'ParentLevel2' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ParentLevel1' as componentID, 'Clendor' as tablename, 'ParentLevel1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ParentID' as componentID, 'Clendor' as tablename, 'ParentID' as fieldname, 'overview' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Owner' as componentID, 'Clendor' as tablename, 'Owner' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.OutstandingAR' as componentID, 'Clendor' as tablename, 'OutstandingAR' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Org5Name' as componentID, 'Clendor' as tablename, 'Org5Name' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Org5' as componentID, 'Clendor' as tablename, 'Org5' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Org4Name' as componentID, 'Clendor' as tablename, 'Org4Name' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Org4' as componentID, 'Clendor' as tablename, 'Org4' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Org3Name' as componentID, 'Clendor' as tablename, 'Org3Name' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Org3' as componentID, 'Clendor' as tablename, 'Org3' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Org2Name' as componentID, 'Clendor' as tablename, 'Org2Name' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Org2' as componentID, 'Clendor' as tablename, 'Org2' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Org1Name' as componentID, 'Clendor' as tablename, 'Org1Name' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Org1' as componentID, 'Clendor' as tablename, 'Org1' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.OrgName' as componentID, 'Clendor' as tablename, 'OrgName' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Org' as componentID, 'Clendor' as tablename, 'Org' as fieldname, 'vendor' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Name' as componentID, 'Clendor' as tablename, 'Name' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.MostRecentProject' as componentID, null as tablename, null as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ModUser' as componentID, 'Clendor' as tablename, 'ModUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ModUserName' as componentID, 'Clendor' as tablename, 'ModUserName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ModDate' as componentID, 'Clendor' as tablename, 'ModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.MinorityBusiness' as componentID, 'Clendor' as tablename, 'MinorityBusiness' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Memo' as componentID, 'Clendor' as tablename, 'Memo' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Market' as componentID, 'Clendor' as tablename, 'Market' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.LastActivityField' as componentID, null as tablename, null as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.IQID' as componentID, 'Clendor' as tablename, 'IQID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.InvoiceUnpaid' as componentID, 'Clendor' as tablename, 'InvoiceUnpaid' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Invoice' as componentID, 'Clendor' as tablename, 'Invoice' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Incumbent' as componentID, 'Clendor' as tablename, 'Incumbent' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.HUBZone' as componentID, 'Clendor' as tablename, 'HUBZone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.HBCU' as componentID, 'Clendor' as tablename, 'HBCU' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.HasPhoto' as componentID, 'Clendor' as tablename, 'HasPhoto' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.HasHierarchy' as componentID, 'Clendor' as tablename, 'HasHierarchy' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.GovernmentAgency' as componentID, 'Clendor' as tablename, 'GovernmentAgency' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.FirmType' as componentID, null as tablename, null as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.FirmSpecialty' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.FirmAvailableTo' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.FedID' as componentID, 'Clendor' as tablename, 'FedID' as fieldname, 'vendor' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ExportInd' as componentID, 'Clendor' as tablename, 'ExportInd' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Employees' as componentID, 'Clendor' as tablename, 'Employees' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.emParentfl' as componentID, 'Clendor' as tablename, 'emParentfl' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.emOwnerTitle' as componentID, 'Clendor' as tablename, 'emOwnerTitle' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.emOwnerPhone' as componentID, 'Clendor' as tablename, 'emOwnerPhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.emOwnerLocation' as componentID, 'Clendor' as tablename, 'emOwnerLocation' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.emOwnerfl' as componentID, 'Clendor' as tablename, 'emOwnerfl' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.emOwnerEmail' as componentID, 'Clendor' as tablename, 'emOwnerEmail' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.emOwner' as componentID, 'Clendor' as tablename, 'emOwner' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.EightA' as componentID, 'Clendor' as tablename, 'EightA' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.DisadvBusiness' as componentID, 'Clendor' as tablename, 'DisadvBusiness' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.DisabledVetOwnedSmallBusiness' as componentID, 'Clendor' as tablename, 'DisabledVetOwnedSmallBusiness' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.CustomCurrencyCode' as componentID, 'Clendor' as tablename, 'CustomCurrencyCode' as fieldname, 'overview' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.CurrentStatus' as componentID, 'Clendor' as tablename, 'CurrentStatus' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.CreateUser' as componentID, 'Clendor' as tablename, 'CreateUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.CreateUserName' as componentID, 'Clendor' as tablename, 'CreateUserName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.CreateDate' as componentID, 'Clendor' as tablename, 'CreateDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.CostAmount' as componentID, 'Clendor' as tablename, 'CostAmount' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Competitor' as componentID, 'Clendor' as tablename, 'Competitor' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ClientInd' as componentID, 'Clendor' as tablename, 'ClientInd' as fieldname, 'sidebar' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ClientID' as componentID, 'Clendor' as tablename, 'ClientID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Client' as componentID, 'Clendor' as tablename, 'Client' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Category' as componentID, 'Clendor' as tablename, 'Category' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.BillingZip' as componentID, 'Clendor' as tablename, 'BillingZip' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.BillingState' as componentID, 'Clendor' as tablename, 'BillingState' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.BillingPhone' as componentID, 'Clendor' as tablename, 'BillingPhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.BillingFax' as componentID, 'Clendor' as tablename, 'BillingFax' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.BillingEmail' as componentID, 'Clendor' as tablename, 'BillingEmail' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.BillingCountry' as componentID, 'Clendor' as tablename, 'BillingCountry' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.BillingCity' as componentID, 'Clendor' as tablename, 'BillingCity' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.BillingAddress4' as componentID, 'Clendor' as tablename, 'BillingAddress4' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.BillingAddress3' as componentID, 'Clendor' as tablename, 'BillingAddress3' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.BillingAddress2' as componentID, 'Clendor' as tablename, 'BillingAddress2' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.BillingAddress1' as componentID, 'Clendor' as tablename, 'BillingAddress1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.AvailableForCRM' as componentID, 'Clendor' as tablename, 'AvailableForCRM' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.AnnualRevenue' as componentID, 'Clendor' as tablename, 'AnnualRevenue' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.AlaskaNative' as componentID, 'Clendor' as tablename, 'AlaskaNative' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.AjeraSync' as componentID, 'Clendor' as tablename, 'AjeraSync' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'CLAddress' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Activity' as componentID, null as tablename, null as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'VEAccounting' as GridID, 'Vendor' as componentID, 'VEAccounting' as tablename, 'Vendor' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'VEAccounting' as GridID, 'PayTermsDesc' as componentID, 'VEAccounting' as tablename, 'PayTermsDesc' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'VEAccounting' as GridID, 'ModUser' as componentID, 'VEAccounting' as tablename, 'ModUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'VEAccounting' as GridID, 'ModDate' as componentID, 'VEAccounting' as tablename, 'ModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'VEAccounting' as GridID, 'MemoPrintOnCheck' as componentID, 'VEAccounting' as tablename, 'MemoPrintOnCheck' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'VEAccounting' as GridID, 'EFTTransactionType' as componentID, 'VEAccounting' as tablename, 'EFTTransactionType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'VEAccounting' as GridID, 'DiscCodeDesc' as componentID, 'VEAccounting' as tablename, 'DiscCodeDesc' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'VEAccounting' as GridID, 'DefaultTaxCodeDesc' as componentID, 'VEAccounting' as tablename, 'DefaultTaxCodeDesc' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'VEAccounting' as GridID, 'DefaultExpenseCodeDesc' as componentID, 'VEAccounting' as tablename, 'DefaultExpenseCodeDesc' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'VEAccounting' as GridID, 'CreateUser' as componentID, 'VEAccounting' as tablename, 'CreateUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'VEAccounting' as GridID, 'CreateDate' as componentID, 'VEAccounting' as tablename, 'CreateDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'VEAccounting' as GridID, 'BankIDTypeDesc' as componentID, 'VEAccounting' as tablename, 'BankIDTypeDesc' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'VEAccounting' as GridID, 'BankAccountIDTypeDesc' as componentID, 'VEAccounting' as tablename, 'BankAccountIDTypeDesc' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'MktCampaignClientAssoc' as GridID, 'Type' as componentID, 'MktCampaignClientAssoc' as tablename, 'Type' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'MktCampaignClientAssoc' as GridID, 'Status' as componentID, 'MktCampaignClientAssoc' as tablename, 'Status' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'MktCampaignClientAssoc' as GridID, 'Stage' as componentID, 'MktCampaignClientAssoc' as tablename, 'Stage' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'MktCampaignClientAssoc' as GridID, 'Name' as componentID, 'MktCampaignClientAssoc' as tablename, 'Name' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'MktCampaignClientAssoc' as GridID, 'ModUser' as componentID, 'MktCampaignClientAssoc' as tablename, 'ModUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'MktCampaignClientAssoc' as GridID, 'ModDate' as componentID, 'MktCampaignClientAssoc' as tablename, 'ModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'MktCampaignClientAssoc' as GridID, 'CreateUser' as componentID, 'MktCampaignClientAssoc' as tablename, 'CreateUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'MktCampaignClientAssoc' as GridID, 'CreateDate' as componentID, 'MktCampaignClientAssoc' as tablename, 'CreateDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'MktCampaignClientAssoc' as GridID, 'ContactID' as componentID, 'MktCampaignClientAssoc' as tablename, 'ContactID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'MktCampaignClientAssoc' as GridID, 'Contact' as componentID, 'MktCampaignClientAssoc' as tablename, 'Contact' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'MktCampaignClientAssoc' as GridID, 'ClientID' as componentID, 'MktCampaignClientAssoc' as tablename, 'ClientID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'MktCampaignClientAssoc' as GridID, 'CampaignMgr' as componentID, 'MktCampaignClientAssoc' as tablename, 'CampaignMgr' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'MktCampaignClientAssoc' as GridID, 'CampaignID' as componentID, 'MktCampaignClientAssoc' as tablename, 'CampaignID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'PKey' as componentID, 'FW_ATTACHMENTS' as tablename, 'PKey' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key3' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key3' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key2' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key2' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key1' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key1' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileSize' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileSize' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileName' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileName' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileID' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileDescription' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileDescription' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'ContentType' as componentID, 'FW_ATTACHMENTS' as tablename, 'ContentType' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'ClientID' as componentID, 'FW_ATTACHMENTS' as tablename, 'ClientID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'CategoryDescription' as componentID, 'FW_ATTACHMENTS' as tablename, 'CategoryDescription' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'CategoryCode' as componentID, 'FW_ATTACHMENTS' as tablename, 'CategoryCode' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Application' as componentID, 'FW_ATTACHMENTS' as tablename, 'Application' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ZIP' as componentID, 'EMClientAssoc' as tablename, 'ZIP' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'YearsOtherFirms' as componentID, 'EMClientAssoc' as tablename, 'YearsOtherFirms' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'WorkPhoneFormat' as componentID, 'EMClientAssoc' as tablename, 'WorkPhoneFormat' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'WorkPhone' as componentID, 'EMClientAssoc' as tablename, 'WorkPhone' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Vendor' as componentID, 'EMClientAssoc' as tablename, 'Vendor' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'UtilizationRatio' as componentID, 'EMClientAssoc' as tablename, 'UtilizationRatio' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'UseTotalHrsAsStd' as componentID, 'EMClientAssoc' as tablename, 'UseTotalHrsAsStd' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Type' as componentID, 'EMClientAssoc' as tablename, 'Type' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'TLSyncModDate' as componentID, 'EMClientAssoc' as tablename, 'TLSyncModDate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'TLInternalKey' as componentID, 'EMClientAssoc' as tablename, 'TLInternalKey' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'TKGroup' as componentID, 'EMClientAssoc' as tablename, 'TKGroup' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'TKAdminLevel' as componentID, 'EMClientAssoc' as tablename, 'TKAdminLevel' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'TKAdminEdit' as componentID, 'EMClientAssoc' as tablename, 'TKAdminEdit' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Title' as componentID, 'EMClientAssoc' as tablename, 'Title' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ThirdPartySickPay' as componentID, 'EMClientAssoc' as tablename, 'ThirdPartySickPay' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'TerminationDate' as componentID, 'EMClientAssoc' as tablename, 'TerminationDate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'TaxRegistrationNumber' as componentID, 'EMClientAssoc' as tablename, 'TaxRegistrationNumber' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'TargetRatio' as componentID, 'EMClientAssoc' as tablename, 'TargetRatio' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'TalentUserID' as componentID, 'EMClientAssoc' as tablename, 'TalentUserID' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'TalentModDate' as componentID, 'EMClientAssoc' as tablename, 'TalentModDate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Supervisor' as componentID, 'EMClientAssoc' as tablename, 'Supervisor' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Suffix' as componentID, 'EMClientAssoc' as tablename, 'Suffix' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'StatutoryEmployee' as componentID, 'EMClientAssoc' as tablename, 'StatutoryEmployee' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Status' as componentID, 'EMClientAssoc' as tablename, 'Status' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'State' as componentID, 'EMClientAssoc' as tablename, 'State' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'SSN' as componentID, 'EMClientAssoc' as tablename, 'SSN' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'SEPAIBAN' as componentID, 'EMClientAssoc' as tablename, 'SEPAIBAN' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'SEPABIC' as componentID, 'EMClientAssoc' as tablename, 'SEPABIC' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Salutation' as componentID, 'EMClientAssoc' as tablename, 'Salutation' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'RetirementPlan' as componentID, 'EMClientAssoc' as tablename, 'RetirementPlan' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'RequireStartEndTime' as componentID, 'EMClientAssoc' as tablename, 'RequireStartEndTime' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'RelationshipDesc' as componentID, 'EMClientAssoc' as tablename, 'RelationshipDesc' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Relationship' as componentID, 'EMClientAssoc' as tablename, 'Relationship' as fieldname, 'team' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Region' as componentID, 'EMClientAssoc' as tablename, 'Region' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ReadyForProcessing' as componentID, 'EMClientAssoc' as tablename, 'ReadyForProcessing' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ReadyForApproval' as componentID, 'EMClientAssoc' as tablename, 'ReadyForApproval' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'RaiseDate' as componentID, 'EMClientAssoc' as tablename, 'RaiseDate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ProvCostSpecialOTPct' as componentID, 'EMClientAssoc' as tablename, 'ProvCostSpecialOTPct' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ProvCostRate' as componentID, 'EMClientAssoc' as tablename, 'ProvCostRate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ProvCostOTPct' as componentID, 'EMClientAssoc' as tablename, 'ProvCostOTPct' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ProvBillSpecialOTPct' as componentID, 'EMClientAssoc' as tablename, 'ProvBillSpecialOTPct' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ProvBillRate' as componentID, 'EMClientAssoc' as tablename, 'ProvBillRate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ProvBillOTPct' as componentID, 'EMClientAssoc' as tablename, 'ProvBillOTPct' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ProfessionalSuffix' as componentID, 'EMClientAssoc' as tablename, 'ProfessionalSuffix' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'PriorYearsFirm' as componentID, 'EMClientAssoc' as tablename, 'PriorYearsFirm' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'PreferredName' as componentID, 'EMClientAssoc' as tablename, 'PreferredName' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'PayType' as componentID, 'EMClientAssoc' as tablename, 'PayType' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'PaySpecialOvtPct' as componentID, 'EMClientAssoc' as tablename, 'PaySpecialOvtPct' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'PayRateTableNo' as componentID, 'EMClientAssoc' as tablename, 'PayRateTableNo' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'PayRateMeth' as componentID, 'EMClientAssoc' as tablename, 'PayRateMeth' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'PayRate' as componentID, 'EMClientAssoc' as tablename, 'PayRate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'PayOvtPct' as componentID, 'EMClientAssoc' as tablename, 'PayOvtPct' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'PaychexRateNumber' as componentID, 'EMClientAssoc' as tablename, 'PaychexRateNumber' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'PaychexCode3' as componentID, 'EMClientAssoc' as tablename, 'PaychexCode3' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'PaychexCode2' as componentID, 'EMClientAssoc' as tablename, 'PaychexCode2' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'PaychexCode1' as componentID, 'EMClientAssoc' as tablename, 'PaychexCode1' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'OtherPay5' as componentID, 'EMClientAssoc' as tablename, 'OtherPay5' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'OtherPay4' as componentID, 'EMClientAssoc' as tablename, 'OtherPay4' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'OtherPay3' as componentID, 'EMClientAssoc' as tablename, 'OtherPay3' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'OtherPay2' as componentID, 'EMClientAssoc' as tablename, 'OtherPay2' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'OtherPay' as componentID, 'EMClientAssoc' as tablename, 'OtherPay' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Org' as componentID, 'EMClientAssoc' as tablename, 'Org' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'OccupationalCode' as componentID, 'EMClientAssoc' as tablename, 'OccupationalCode' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Name' as componentID, 'EMClientAssoc' as tablename, 'Name' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ModUser' as componentID, 'EMClientAssoc' as tablename, 'ModUser' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ModDate' as componentID, 'EMClientAssoc' as tablename, 'ModDate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'MobilePhoneFormat' as componentID, 'EMClientAssoc' as tablename, 'MobilePhoneFormat' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'MobilePhone' as componentID, 'EMClientAssoc' as tablename, 'MobilePhone' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'MiddleName' as componentID, 'EMClientAssoc' as tablename, 'MiddleName' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Memo' as componentID, 'EMClientAssoc' as tablename, 'Memo' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Location' as componentID, 'EMClientAssoc' as tablename, 'Location' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'LocaleMethod' as componentID, 'EMClientAssoc' as tablename, 'LocaleMethod' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Locale' as componentID, 'EMClientAssoc' as tablename, 'Locale' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'LastName' as componentID, 'EMClientAssoc' as tablename, 'LastName' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Language' as componentID, 'EMClientAssoc' as tablename, 'Language' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'JobCostType' as componentID, 'EMClientAssoc' as tablename, 'JobCostType' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'JobCostRate' as componentID, 'EMClientAssoc' as tablename, 'JobCostRate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'JCSpecialOvtPct' as componentID, 'EMClientAssoc' as tablename, 'JCSpecialOvtPct' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'JCOvtPct' as componentID, 'EMClientAssoc' as tablename, 'JCOvtPct' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'IncludeLocalJurisOnly' as componentID, 'EMClientAssoc' as tablename, 'IncludeLocalJurisOnly' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'HoursPerDay' as componentID, 'EMClientAssoc' as tablename, 'HoursPerDay' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'HomePhoneFormat' as componentID, 'EMClientAssoc' as tablename, 'HomePhoneFormat' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'HomePhone' as componentID, 'EMClientAssoc' as tablename, 'HomePhone' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'HomeCompany' as componentID, 'EMClientAssoc' as tablename, 'HomeCompany' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'HireDate' as componentID, 'EMClientAssoc' as tablename, 'HireDate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'GeographicCode' as componentID, 'EMClientAssoc' as tablename, 'GeographicCode' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'FirstName' as componentID, 'EMClientAssoc' as tablename, 'FirstName' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'FaxFormat' as componentID, 'EMClientAssoc' as tablename, 'FaxFormat' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Fax' as componentID, 'EMClientAssoc' as tablename, 'Fax' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ExportInd' as componentID, 'EMClientAssoc' as tablename, 'ExportInd' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'EmployeePhoto' as componentID, 'EMClientAssoc' as tablename, 'EmployeePhoto' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'EmployeeName' as componentID, 'EMClientAssoc' as tablename, 'EmployeeName' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'EmployeeCompany' as componentID, 'EMClientAssoc' as tablename, 'EmployeeCompany' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Employee' as componentID, 'EMClientAssoc' as tablename, 'Employee' as fieldname, 'team' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'EmailPayrollRemittance' as componentID, 'EMClientAssoc' as tablename, 'EmailPayrollRemittance' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'EmailExpenseRemittance' as componentID, 'EMClientAssoc' as tablename, 'EmailExpenseRemittance' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'EMail' as componentID, 'EMClientAssoc' as tablename, 'EMail' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'EKGroup' as componentID, 'EMClientAssoc' as tablename, 'EKGroup' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'EKAdminLevel' as componentID, 'EMClientAssoc' as tablename, 'EKAdminLevel' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'EKAdminEdit' as componentID, 'EMClientAssoc' as tablename, 'EKAdminEdit' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'DisableTSRevAudit' as componentID, 'EMClientAssoc' as tablename, 'DisableTSRevAudit' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'DefaultLC5' as componentID, 'EMClientAssoc' as tablename, 'DefaultLC5' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'DefaultLC4' as componentID, 'EMClientAssoc' as tablename, 'DefaultLC4' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'DefaultLC3' as componentID, 'EMClientAssoc' as tablename, 'DefaultLC3' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'DefaultLC2' as componentID, 'EMClientAssoc' as tablename, 'DefaultLC2' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'DefaultLC1' as componentID, 'EMClientAssoc' as tablename, 'DefaultLC1' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'DefaultBreakStartDateTime' as componentID, 'EMClientAssoc' as tablename, 'DefaultBreakStartDateTime' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'DefaultBreakEndDateTime' as componentID, 'EMClientAssoc' as tablename, 'DefaultBreakEndDateTime' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'CreateUser' as componentID, 'EMClientAssoc' as tablename, 'CreateUser' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'CreateDate' as componentID, 'EMClientAssoc' as tablename, 'CreateDate' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Country' as componentID, 'EMClientAssoc' as tablename, 'Country' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'CostRateTableNo' as componentID, 'EMClientAssoc' as tablename, 'CostRateTableNo' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'CostRateMeth' as componentID, 'EMClientAssoc' as tablename, 'CostRateMeth' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ConsultantInd' as componentID, 'EMClientAssoc' as tablename, 'ConsultantInd' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ClieOpTransactionType' as componentID, 'EMClientAssoc' as tablename, 'ClieOpTransactionType' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ClieOpAccountType' as componentID, 'EMClientAssoc' as tablename, 'ClieOpAccountType' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ClieOpAccount' as componentID, 'EMClientAssoc' as tablename, 'ClieOpAccount' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ClieOp' as componentID, 'EMClientAssoc' as tablename, 'ClieOp' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ClientVendorInd' as componentID, 'EMClientAssoc' as tablename, 'ClientVendorInd' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ClientID' as componentID, 'EMClientAssoc' as tablename, 'ClientID' as fieldname, 'team' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'City' as componentID, 'EMClientAssoc' as tablename, 'City' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'CheckHours' as componentID, 'EMClientAssoc' as tablename, 'CheckHours' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ChangeDefaultLC' as componentID, 'EMClientAssoc' as tablename, 'ChangeDefaultLC' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'BillingPool' as componentID, 'EMClientAssoc' as tablename, 'BillingPool' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'BillingCategory' as componentID, 'EMClientAssoc' as tablename, 'BillingCategory' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'AvailableForCRM' as componentID, 'EMClientAssoc' as tablename, 'AvailableForCRM' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'AllowChargeUnits' as componentID, 'EMClientAssoc' as tablename, 'AllowChargeUnits' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'AllowBreakTime' as componentID, 'EMClientAssoc' as tablename, 'AllowBreakTime' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ADPRateCode' as componentID, 'EMClientAssoc' as tablename, 'ADPRateCode' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ADPFileNumber' as componentID, 'EMClientAssoc' as tablename, 'ADPFileNumber' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'ADPCompanyCode' as componentID, 'EMClientAssoc' as tablename, 'ADPCompanyCode' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Address3' as componentID, 'EMClientAssoc' as tablename, 'Address3' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Address2' as componentID, 'EMClientAssoc' as tablename, 'Address2' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'EMClientAssoc' as GridID, 'Address1' as componentID, 'EMClientAssoc' as tablename, 'Address1' as fieldname, 'team' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Zip' as componentID, 'Contacts' as tablename, 'Zip' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Website' as componentID, 'Contacts' as tablename, 'Website' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'VName' as componentID, 'Contacts' as tablename, 'VName' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Vendor' as componentID, 'Contacts' as tablename, 'Vendor' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'VEAddress' as componentID, 'Contacts' as tablename, 'VEAddress' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'UpdateCLAddress' as componentID, 'Contacts' as tablename, 'UpdateCLAddress' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'TopTPSubject' as componentID, 'Contacts' as tablename, 'TopTPSubject' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'TopTPStartDate' as componentID, 'Contacts' as tablename, 'TopTPStartDate' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'TopTPCreateUserName' as componentID, 'Contacts' as tablename, 'TopTPCreateUserName' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'TopTPCreateEmployee' as componentID, 'Contacts' as tablename, 'TopTPCreateEmployee' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'TopTPContactID' as componentID, 'Contacts' as tablename, 'TopTPContactID' as fieldname, 'contacts' as tabID, 0 as essential union  
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'TopTPActivityID' as componentID, 'Contacts' as tablename, 'TopTPActivityID' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'TLSyncModDate' as componentID, 'Contacts' as tablename, 'TLSyncModDate' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'TLInternalKey' as componentID, 'Contacts' as tablename, 'TLInternalKey' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Title' as componentID, 'Contacts' as tablename, 'Title' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'TaxRegistrationNumber' as componentID, 'Contacts' as tablename, 'TaxRegistrationNumber' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'TaxCountryCode' as componentID, 'Contacts' as tablename, 'TaxCountryCode' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Suffix' as componentID, 'Contacts' as tablename, 'Suffix' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'StatusReason' as componentID, 'Contacts' as tablename, 'StatusReason' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'StatusDate' as componentID, 'Contacts' as tablename, 'StatusDate' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'State' as componentID, 'Contacts' as tablename, 'State' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Source' as componentID, 'Contacts' as tablename, 'Source' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'SFLastModifiedDate' as componentID, 'Contacts' as tablename, 'SFLastModifiedDate' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'SFID' as componentID, 'Contacts' as tablename, 'SFID' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Salutation' as componentID, 'Contacts' as tablename, 'Salutation' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Rating' as componentID, 'Contacts' as tablename, 'Rating' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'QualifiedStatus' as componentID, 'Contacts' as tablename, 'QualifiedStatus' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'QBOLastUpdated' as componentID, 'Contacts' as tablename, 'QBOLastUpdated' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'QBOIsMainContact' as componentID, 'Contacts' as tablename, 'QBOIsMainContact' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'QBOID' as componentID, 'Contacts' as tablename, 'QBOID' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'ProjectDescription' as componentID, 'Contacts' as tablename, 'ProjectDescription' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'ProfessionalSuffix' as componentID, 'Contacts' as tablename, 'ProfessionalSuffix' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'PrimaryInd' as componentID, 'Contacts' as tablename, 'PrimaryInd' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'PreferredName' as componentID, 'Contacts' as tablename, 'PreferredName' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'PhotoModDate' as componentID, 'Contacts' as tablename, 'PhotoModDate' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'PhoneFormat' as componentID, 'Contacts' as tablename, 'PhoneFormat' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Phone' as componentID, 'Contacts' as tablename, 'Phone' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Payment' as componentID, 'Contacts' as tablename, 'Payment' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'PagerFormat' as componentID, 'Contacts' as tablename, 'PagerFormat' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Pager' as componentID, 'Contacts' as tablename, 'Pager' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Owner' as componentID, 'Contacts' as tablename, 'Owner' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'ModUser' as componentID, 'Contacts' as tablename, 'ModUser' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'ModDate' as componentID, 'Contacts' as tablename, 'ModDate' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'MiddleName' as componentID, 'Contacts' as tablename, 'MiddleName' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Memo' as componentID, 'Contacts' as tablename, 'Memo' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Market' as componentID, 'Contacts' as tablename, 'Market' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'MailingAddress' as componentID, 'Contacts' as tablename, 'MailingAddress' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'LastName' as componentID, 'Contacts' as tablename, 'LastName' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'HomePhoneFormat' as componentID, 'Contacts' as tablename, 'HomePhoneFormat' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'HomePhone' as componentID, 'Contacts' as tablename, 'HomePhone' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'HasPhoto' as componentID, 'Contacts' as tablename, 'HasPhoto' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'flName' as componentID, 'Contacts' as tablename, 'flName' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FirstName' as componentID, 'Contacts' as tablename, 'FirstName' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FirmZip' as componentID, 'Contacts' as tablename, 'FirmZip' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FirmState' as componentID, 'Contacts' as tablename, 'FirmState' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FirmPager' as componentID, 'Contacts' as tablename, 'FirmPager' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FirmMailingAddress' as componentID, 'Contacts' as tablename, 'FirmMailingAddress' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FirmDescription' as componentID, 'Contacts' as tablename, 'FirmDescription' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FirmCountry' as componentID, 'Contacts' as tablename, 'FirmCountry' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FirmCity' as componentID, 'Contacts' as tablename, 'FirmCity' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FirmBusinessPhoneFormat' as componentID, 'Contacts' as tablename, 'FirmBusinessPhoneFormat' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FirmBusinessPhone' as componentID, 'Contacts' as tablename, 'FirmBusinessPhone' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FirmBusinessFaxFormat' as componentID, 'Contacts' as tablename, 'FirmBusinessFaxFormat' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FirmBusinessFax' as componentID, 'Contacts' as tablename, 'FirmBusinessFax' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FirmAddressDescription' as componentID, 'Contacts' as tablename, 'FirmAddressDescription' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FirmAddress4' as componentID, 'Contacts' as tablename, 'FirmAddress4' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FirmAddress3' as componentID, 'Contacts' as tablename, 'FirmAddress3' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FirmAddress2' as componentID, 'Contacts' as tablename, 'FirmAddress2' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FirmAddress1' as componentID, 'Contacts' as tablename, 'FirmAddress1' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'FaxFormat' as componentID, 'Contacts' as tablename, 'FaxFormat' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Fax' as componentID, 'Contacts' as tablename, 'Fax' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'emOwnerfl' as componentID, 'Contacts' as tablename, 'emOwnerfl' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Email' as componentID, 'Contacts' as tablename, 'Email' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'CustomCurrencyCode' as componentID, 'Contacts' as tablename, 'CustomCurrencyCode' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'CreateUser' as componentID, 'Contacts' as tablename, 'CreateUser' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'CreateDate' as componentID, 'Contacts' as tablename, 'CreateDate' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Country' as componentID, 'Contacts' as tablename, 'Country' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'ContactStatus' as componentID, 'Contacts' as tablename, 'ContactStatus' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'ContactID' as componentID, 'Contacts' as tablename, 'ContactID' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Company' as componentID, 'Contacts' as tablename, 'Company' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'ClientName' as componentID, 'Contacts' as tablename, 'ClientName' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'ClientID' as componentID, 'Contacts' as tablename, 'ClientID' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Client' as componentID, 'Contacts' as tablename, 'Client' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'CLAddress' as componentID, 'Contacts' as tablename, 'CLAddress' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'City' as componentID, 'Contacts' as tablename, 'City' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'CellPhoneFormat' as componentID, 'Contacts' as tablename, 'CellPhoneFormat' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'CellPhone' as componentID, 'Contacts' as tablename, 'CellPhone' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Billing' as componentID, 'Contacts' as tablename, 'Billing' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'AjeraSync' as componentID, 'Contacts' as tablename, 'AjeraSync' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Addressee' as componentID, 'Contacts' as tablename, 'Addressee' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Address4' as componentID, 'Contacts' as tablename, 'Address4' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Address3' as componentID, 'Contacts' as tablename, 'Address3' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Address2' as componentID, 'Contacts' as tablename, 'Address2' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Address1' as componentID, 'Contacts' as tablename, 'Address1' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Contacts' as GridID, 'Accounting' as componentID, 'Contacts' as tablename, 'Accounting' as fieldname, 'contacts' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientToClientAssoc' as GridID, 'ToClientName' as componentID, 'ClientToClientAssoc' as tablename, 'ToClientName' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientToClientAssoc' as GridID, 'ToClientID' as componentID, 'ClientToClientAssoc' as tablename, 'ToClientID' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientToClientAssoc' as GridID, 'ToClient' as componentID, 'ClientToClientAssoc' as tablename, 'ToClient' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientToClientAssoc' as GridID, 'Relationship' as componentID, 'ClientToClientAssoc' as tablename, 'Relationship' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientToClientAssoc' as GridID, 'ModUser' as componentID, 'ClientToClientAssoc' as tablename, 'ModUser' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientToClientAssoc' as GridID, 'ModDate' as componentID, 'ClientToClientAssoc' as tablename, 'ModDate' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientToClientAssoc' as GridID, 'FromClientID' as componentID, 'ClientToClientAssoc' as tablename, 'FromClientID' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientToClientAssoc' as GridID, 'Description' as componentID, 'ClientToClientAssoc' as tablename, 'Description' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientToClientAssoc' as GridID, 'CreateUser' as componentID, 'ClientToClientAssoc' as tablename, 'CreateUser' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientToClientAssoc' as GridID, 'CreateDate' as componentID, 'ClientToClientAssoc' as tablename, 'CreateDate' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientFileLinks' as GridID, 'LinkID' as componentID, 'ClientFileLinks' as tablename, 'LinkID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientFileLinks' as GridID, 'Graphic' as componentID, 'ClientFileLinks' as tablename, 'Graphic' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientFileLinks' as GridID, 'FilePath' as componentID, 'ClientFileLinks' as tablename, 'FilePath' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientFileLinks' as GridID, 'Description' as componentID, 'ClientFileLinks' as tablename, 'Description' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientFileLinks' as GridID, 'ClientID' as componentID, 'ClientFileLinks' as tablename, 'ClientID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientAlias' as GridID, 'PKey' as componentID, 'ClientAlias' as tablename, 'PKey' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientAlias' as GridID, 'ModUser' as componentID, 'ClientAlias' as tablename, 'ModUser' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientAlias' as GridID, 'ModDate' as componentID, 'ClientAlias' as tablename, 'ModDate' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientAlias' as GridID, 'CreateUser' as componentID, 'ClientAlias' as tablename, 'CreateUser' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientAlias' as GridID, 'CreateDate' as componentID, 'ClientAlias' as tablename, 'CreateDate' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientAlias' as GridID, 'ClientID' as componentID, 'ClientAlias' as tablename, 'ClientID' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClientAlias' as GridID, 'Alias' as componentID, 'ClientAlias' as tablename, 'Alias' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'WBS3' as componentID, 'ClendorProjectAssoc' as tablename, 'WBS3' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'WBS2' as componentID, 'ClendorProjectAssoc' as tablename, 'WBS2' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'WBS1' as componentID, 'ClendorProjectAssoc' as tablename, 'WBS1' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'VendorInd' as componentID, 'ClendorProjectAssoc' as tablename, 'VendorInd' as fieldname, 'associations' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'Stage' as componentID, 'ClendorProjectAssoc' as tablename, 'Stage' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'RoleName' as componentID, 'ClendorProjectAssoc' as tablename, 'RoleName' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'RoleDescription' as componentID, 'ClendorProjectAssoc' as tablename, 'RoleDescription' as fieldname, 'associations' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'Role' as componentID, 'ClendorProjectAssoc' as tablename, 'Role' as fieldname, 'associations' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'PRStatus' as componentID, 'ClendorProjectAssoc' as tablename, 'PRStatus' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'PRProjectCurrencyCode' as componentID, 'ClendorProjectAssoc' as tablename, 'PRProjectCurrencyCode' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'PRName' as componentID, 'ClendorProjectAssoc' as tablename, 'PRName' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'PrimaryInd' as componentID, 'ClendorProjectAssoc' as tablename, 'PrimaryInd' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'PRContactName' as componentID, 'ClendorProjectAssoc' as tablename, 'PRContactName' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'PRContactID' as componentID, 'ClendorProjectAssoc' as tablename, 'PRContactID' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'PRClientName' as componentID, 'ClendorProjectAssoc' as tablename, 'PRClientName' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'PRClientID' as componentID, 'ClendorProjectAssoc' as tablename, 'PRClientID' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'ModUser' as componentID, 'ClendorProjectAssoc' as tablename, 'ModUser' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'ModDate' as componentID, 'ClendorProjectAssoc' as tablename, 'ModDate' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'DefaultType' as componentID, 'ClendorProjectAssoc' as tablename, 'DefaultType' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'CreateUser' as componentID, 'ClendorProjectAssoc' as tablename, 'CreateUser' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'CreateDate' as componentID, 'ClendorProjectAssoc' as tablename, 'CreateDate' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'CostAmount' as componentID, 'ClendorProjectAssoc' as tablename, 'CostAmount' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'ClientInd' as componentID, 'ClendorProjectAssoc' as tablename, 'ClientInd' as fieldname, 'associations' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'ClientID' as componentID, 'ClendorProjectAssoc' as tablename, 'ClientID' as fieldname, 'associations' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'ClientConfidential' as componentID, 'ClendorProjectAssoc' as tablename, 'ClientConfidential' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'ClendorProjectAssoc' as GridID, 'Address' as componentID, 'ClendorProjectAssoc' as tablename, 'Address' as fieldname, 'associations' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'Zip' as componentID, 'CLAddress' as tablename, 'Zip' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'TaxRegistrationNumber' as componentID, 'CLAddress' as tablename, 'TaxRegistrationNumber' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'TaxCountryCode' as componentID, 'CLAddress' as tablename, 'TaxCountryCode' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'StateDescription' as componentID, 'CLAddress' as tablename, 'StateDescription' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'State' as componentID, 'CLAddress' as tablename, 'State' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'QBOLastUpdated' as componentID, 'CLAddress' as tablename, 'QBOLastUpdated' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'QBOIsShippingAddr' as componentID, 'CLAddress' as tablename, 'QBOIsShippingAddr' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'QBOIsBillingAddr' as componentID, 'CLAddress' as tablename, 'QBOIsBillingAddr' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'QBOID' as componentID, 'CLAddress' as tablename, 'QBOID' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'QBOAddressID' as componentID, 'CLAddress' as tablename, 'QBOAddressID' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'PrimaryInd' as componentID, 'CLAddress' as tablename, 'PrimaryInd' as fieldname, 'overview' as tabID, 1 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'PhoneFormat' as componentID, 'CLAddress' as tablename, 'PhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'Phone' as componentID, 'CLAddress' as tablename, 'Phone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'Payment' as componentID, 'CLAddress' as tablename, 'Payment' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'ModUser' as componentID, 'CLAddress' as tablename, 'ModUser' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'ModDate' as componentID, 'CLAddress' as tablename, 'ModDate' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'FaxFormat' as componentID, 'CLAddress' as tablename, 'FaxFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'FAX' as componentID, 'CLAddress' as tablename, 'FAX' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'Email' as componentID, 'CLAddress' as tablename, 'Email' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'CreateUser' as componentID, 'CLAddress' as tablename, 'CreateUser' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'CreateDate' as componentID, 'CLAddress' as tablename, 'CreateDate' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'Country' as componentID, 'CLAddress' as tablename, 'Country' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'ClientID' as componentID, 'CLAddress' as tablename, 'ClientID' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'CLAddressID' as componentID, 'CLAddress' as tablename, 'CLAddressID' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'City' as componentID, 'CLAddress' as tablename, 'City' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'Billing' as componentID, 'CLAddress' as tablename, 'Billing' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'Addressee' as componentID, 'CLAddress' as tablename, 'Addressee' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'Address4' as componentID, 'CLAddress' as tablename, 'Address4' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'Address3' as componentID, 'CLAddress' as tablename, 'Address3' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'Address2' as componentID, 'CLAddress' as tablename, 'Address2' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'Address1' as componentID, 'CLAddress' as tablename, 'Address1' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'Address' as componentID, 'CLAddress' as tablename, 'Address' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'Accounting' as componentID, 'CLAddress' as tablename, 'Accounting' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'WBS3' as componentID, 'Activity' as tablename, 'WBS3' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'WBS2' as componentID, 'Activity' as tablename, 'WBS2' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'WBS1' as componentID, 'Activity' as tablename, 'WBS1' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'VendorName' as componentID, 'Activity' as tablename, 'VendorName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'VendorContactName' as componentID, 'Activity' as tablename, 'VendorContactName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'Vendor' as componentID, 'Activity' as tablename, 'Vendor' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'UpdateID' as componentID, 'Activity' as tablename, 'UpdateID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'TypeName' as componentID, 'Activity' as tablename, 'TypeName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'Type' as componentID, 'Activity' as tablename, 'Type' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'TaskCompletionDate' as componentID, 'Activity' as tablename, 'TaskCompletionDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'Subject' as componentID, 'Activity' as tablename, 'Subject' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'StartDateObj' as componentID, 'Activity' as tablename, 'StartDateObj' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'StartDate' as componentID, 'Activity' as tablename, 'StartDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'ShowTimeAs' as componentID, 'Activity' as tablename, 'ShowTimeAs' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RP' as componentID, 'Activity' as tablename, 'RP' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'ReminderUnit' as componentID, 'Activity' as tablename, 'ReminderUnit' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'ReminderMinHrDay' as componentID, 'Activity' as tablename, 'ReminderMinHrDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'ReminderInd' as componentID, 'Activity' as tablename, 'ReminderInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'ReminderDate' as componentID, 'Activity' as tablename, 'ReminderDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccurMonth' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurMonth' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccurInd' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccurDay' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccur' as componentID, 'Activity' as tablename, 'RecurrYearlyOccur' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyMonth' as componentID, 'Activity' as tablename, 'RecurrYearlyMonth' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyDay' as componentID, 'Activity' as tablename, 'RecurrYearlyDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyWed' as componentID, 'Activity' as tablename, 'RecurrWeeklyWed' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyTue' as componentID, 'Activity' as tablename, 'RecurrWeeklyTue' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyThu' as componentID, 'Activity' as tablename, 'RecurrWeeklyThu' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklySun' as componentID, 'Activity' as tablename, 'RecurrWeeklySun' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklySat' as componentID, 'Activity' as tablename, 'RecurrWeeklySat' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyMon' as componentID, 'Activity' as tablename, 'RecurrWeeklyMon' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyFri' as componentID, 'Activity' as tablename, 'RecurrWeeklyFri' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyFreq' as componentID, 'Activity' as tablename, 'RecurrWeeklyFreq' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrType' as componentID, 'Activity' as tablename, 'RecurrType' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrStartDate' as componentID, 'Activity' as tablename, 'RecurrStartDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccurInd' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccurFreq' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurFreq' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccurDay' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccur' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccur' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyFreq' as componentID, 'Activity' as tablename, 'RecurrMonthlyFreq' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyDay' as componentID, 'Activity' as tablename, 'RecurrMonthlyDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrID' as componentID, 'Activity' as tablename, 'RecurrID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrEndType' as componentID, 'Activity' as tablename, 'RecurrEndType' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrEndDate' as componentID, 'Activity' as tablename, 'RecurrEndDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrenceInd' as componentID, 'Activity' as tablename, 'RecurrenceInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrDailyWeekDay' as componentID, 'Activity' as tablename, 'RecurrDailyWeekDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'RecurrDailyFreq' as componentID, 'Activity' as tablename, 'RecurrDailyFreq' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'ReadID' as componentID, 'Activity' as tablename, 'ReadID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'ProjName' as componentID, 'Activity' as tablename, 'ProjName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'PrivateInd' as componentID, 'Activity' as tablename, 'PrivateInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'PriorityDesc' as componentID, 'Activity' as tablename, 'PriorityDesc' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'Priority' as componentID, 'Activity' as tablename, 'Priority' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'PrimaryClient' as componentID, 'Activity' as tablename, 'PrimaryClient' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'OwnerName' as componentID, 'Activity' as tablename, 'OwnerName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'Owner' as componentID, 'Activity' as tablename, 'Owner' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'OutlookType' as componentID, 'Activity' as tablename, 'OutlookType' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'Notes' as componentID, 'Activity' as tablename, 'Notes' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'ModUser' as componentID, 'Activity' as tablename, 'ModUser' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'ModDate' as componentID, 'Activity' as tablename, 'ModDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'MktName' as componentID, 'Activity' as tablename, 'MktName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'MaxOccurences' as componentID, 'Activity' as tablename, 'MaxOccurences' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'Location' as componentID, 'Activity' as tablename, 'Location' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'LeadID' as componentID, 'Activity' as tablename, 'LeadID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'isTask' as componentID, 'Activity' as tablename, 'isTask' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'isCalendar' as componentID, 'Activity' as tablename, 'isCalendar' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'InstanceStartDate' as componentID, 'Activity' as tablename, 'InstanceStartDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'HasNotes' as componentID, 'Activity' as tablename, 'HasNotes' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'EndDateSort' as componentID, 'Activity' as tablename, 'EndDateSort' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'EndDateObj' as componentID, 'Activity' as tablename, 'EndDateObj' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'EndDateFilter' as componentID, 'Activity' as tablename, 'EndDateFilter' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'EndDate' as componentID, 'Activity' as tablename, 'EndDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'Employee' as componentID, 'Activity' as tablename, 'Employee' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'Duration' as componentID, 'Activity' as tablename, 'Duration' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'CreateUser' as componentID, 'Activity' as tablename, 'CreateUser' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'CreateEmployee' as componentID, 'Activity' as tablename, 'CreateEmployee' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'CreateDate' as componentID, 'Activity' as tablename, 'CreateDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'ContactPhoneForVendor' as componentID, 'Activity' as tablename, 'ContactPhoneForVendor' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'ContactNameForVendor' as componentID, 'Activity' as tablename, 'ContactNameForVendor' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'ContactName' as componentID, 'Activity' as tablename, 'ContactName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'ContactIDForVendor' as componentID, 'Activity' as tablename, 'ContactIDForVendor' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'ContactID' as componentID, 'Activity' as tablename, 'ContactID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'CompletionInd' as componentID, 'Activity' as tablename, 'CompletionInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'ClientName' as componentID, 'Activity' as tablename, 'ClientName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'ClientID' as componentID, 'Activity' as tablename, 'ClientID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'CampaignID' as componentID, 'Activity' as tablename, 'CampaignID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'Attendees' as componentID, 'Activity' as tablename, 'Attendees' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'Associations' as componentID, 'Activity' as tablename, 'Associations' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'AllDayEventInd' as componentID, 'Activity' as tablename, 'AllDayEventInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'acType' as componentID, 'Activity' as tablename, 'acType' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Firms' as infocenterArea, 'Activity' as GridID, 'ActivityID' as componentID, 'Activity' as tablename, 'ActivityID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaignProjectAssoc' as componentID, null as tablename, null as fieldname, 'opportunitiesAndProjects' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaignFileLinks' as componentID, null as tablename, null as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaignContactAssoc' as componentID, null as tablename, null as fieldname, 'responses' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.WBS3' as componentID, 'MktCampaign' as tablename, 'WBS3' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.WBS2' as componentID, 'MktCampaign' as tablename, 'WBS2' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.WBS1Name' as componentID, 'MktCampaign' as tablename, 'WBS1Name' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.WBS1' as componentID, 'MktCampaign' as tablename, 'WBS1' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Type' as componentID, 'MktCampaign' as tablename, 'Type' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Status' as componentID, 'MktCampaign' as tablename, 'Status' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Revenue' as componentID, 'MktCampaign' as tablename, 'Revenue' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Results' as componentID, null as tablename, null as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Responses' as componentID, 'MktCampaign' as tablename, 'Responses' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.RecordStatus' as componentID, 'MktCampaign' as tablename, 'RecordStatus' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.QualifiedContacts' as componentID, 'MktCampaign' as tablename, 'QualifiedContacts' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.QualifiedClients' as componentID, 'MktCampaign' as tablename, 'QualifiedClients' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Projects' as componentID, 'MktCampaign' as tablename, 'Projects' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.PotentialRevenue' as componentID, 'MktCampaign' as tablename, 'PotentialRevenue' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.PotentialResponses' as componentID, 'MktCampaign' as tablename, 'PotentialResponses' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.PhotoModDate' as componentID, 'MktCampaign' as tablename, 'PhotoModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.PctResponses' as componentID, 'MktCampaign' as tablename, 'PctResponses' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.OrgStatus' as componentID, 'MktCampaign' as tablename, 'OrgStatus' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.OrgName' as componentID, 'MktCampaign' as tablename, 'OrgName' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Org' as componentID, 'MktCampaign' as tablename, 'Org' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Objective' as componentID, 'MktCampaign' as tablename, 'Objective' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Number' as componentID, 'MktCampaign' as tablename, 'Number' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.NextAction' as componentID, 'MktCampaign' as tablename, 'NextAction' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Name' as componentID, 'MktCampaign' as tablename, 'Name' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.ModUser' as componentID, 'MktCampaign' as tablename, 'ModUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.ModUserName' as componentID, 'MktCampaign' as tablename, 'ModUserName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.ModDate' as componentID, 'MktCampaign' as tablename, 'ModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.MktgMgr' as componentID, 'MktCampaign' as tablename, 'MktgMgr' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.MarketingMgrEmployee' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.MarketingCoordinatorEmployee' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Manager3' as componentID, 'MktCampaign' as tablename, 'Manager3' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.LaunchEndDateRange' as componentID, null as tablename, null as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.LaunchDate' as componentID, 'MktCampaign' as tablename, 'LaunchDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.HasPhoto' as componentID, 'MktCampaign' as tablename, 'HasPhoto' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Goals' as componentID, null as tablename, null as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.FirstAction' as componentID, 'MktCampaign' as tablename, 'FirstAction' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.ExchangeRateDate' as componentID, 'MktCampaign' as tablename, 'ExchangeRateDate' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.EndDate' as componentID, 'MktCampaign' as tablename, 'EndDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emMarketingMgrTitle' as componentID, 'MktCampaign' as tablename, 'emMarketingMgrTitle' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emMarketingMgrPhoneFormat' as componentID, 'MktCampaign' as tablename, 'emMarketingMgrPhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emMarketingMgrPhone' as componentID, 'MktCampaign' as tablename, 'emMarketingMgrPhone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emMarketingMgrLocation' as componentID, 'MktCampaign' as tablename, 'emMarketingMgrLocation' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emMarketingMgrFL' as componentID, 'MktCampaign' as tablename, 'emMarketingMgrFL' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emMarketingMgrEmail' as componentID, 'MktCampaign' as tablename, 'emMarketingMgrEmail' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emMarketingMgr' as componentID, 'MktCampaign' as tablename, 'emMarketingMgr' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emMarketingCoordinatorTitle' as componentID, 'MktCampaign' as tablename, 'emMarketingCoordinatorTitle' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emMarketingCoordinatorPhoneFormat' as componentID, 'MktCampaign' as tablename, 'emMarketingCoordinatorPhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emMarketingCoordinatorPhone' as componentID, 'MktCampaign' as tablename, 'emMarketingCoordinatorPhone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emMarketingCoordinatorLocation' as componentID, 'MktCampaign' as tablename, 'emMarketingCoordinatorLocation' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emMarketingCoordinatorFL' as componentID, 'MktCampaign' as tablename, 'emMarketingCoordinatorFL' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emMarketingCoordinatorEmail' as componentID, 'MktCampaign' as tablename, 'emMarketingCoordinatorEmail' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emMarketingCoordinator' as componentID, 'MktCampaign' as tablename, 'emMarketingCoordinator' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emCampaignMgrTitle' as componentID, 'MktCampaign' as tablename, 'emCampaignMgrTitle' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emCampaignMgrPhoneFormat' as componentID, 'MktCampaign' as tablename, 'emCampaignMgrPhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emCampaignMgrPhone' as componentID, 'MktCampaign' as tablename, 'emCampaignMgrPhone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emCampaignMgrLocation' as componentID, 'MktCampaign' as tablename, 'emCampaignMgrLocation' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emCampaignMgrFL' as componentID, 'MktCampaign' as tablename, 'emCampaignMgrFL' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emCampaignMgrEmail' as componentID, 'MktCampaign' as tablename, 'emCampaignMgrEmail' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emCampaignMgr' as componentID, 'MktCampaign' as tablename, 'emCampaignMgr' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Description' as componentID, 'MktCampaign' as tablename, 'Description' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.CustomCurrencyCode' as componentID, 'MktCampaign' as tablename, 'CustomCurrencyCode' as fieldname, 'sidebar' as tabID, 1 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.CurrentAction' as componentID, 'MktCampaign' as tablename, 'CurrentAction' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.CreateUser' as componentID, 'MktCampaign' as tablename, 'CreateUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.CreateUserUser' as componentID, 'MktCampaign' as tablename, 'CreateUserName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.CreateDate' as componentID, 'MktCampaign' as tablename, 'CreateDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.CampaignMgrEmployee' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.CampaignMgr' as componentID, 'MktCampaign' as tablename, 'CampaignMgr' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.CampaignID' as componentID, 'MktCampaign' as tablename, 'CampaignID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Budget' as componentID, 'MktCampaign' as tablename, 'Budget' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.AwardedProjects' as componentID, 'MktCampaign' as tablename, 'AwardedProjects' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Audience' as componentID, 'MktCampaign' as tablename, 'Audience' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.ActualRevenue' as componentID, 'MktCampaign' as tablename, 'ActualRevenue' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.ActualCost' as componentID, 'MktCampaign' as tablename, 'ActualCost' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'FW_ATTACHMENTS' as componentID, null as tablename, null as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'Campaign.Links.Divider' as componentID, null as tablename, null as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'Campaign.Files.Divider' as componentID, null as tablename, null as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'Activity' as componentID, null as tablename, null as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignProjectAssoc' as GridID, 'WBS3' as componentID, 'MktCampaignProjectAssoc' as tablename, 'WBS3' as fieldname, 'opportunitiesAndProjects' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignProjectAssoc' as GridID, 'WBS2' as componentID, 'MktCampaignProjectAssoc' as tablename, 'WBS2' as fieldname, 'opportunitiesAndProjects' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignProjectAssoc' as GridID, 'WBS1' as componentID, 'MktCampaignProjectAssoc' as tablename, 'WBS1' as fieldname, 'opportunitiesAndProjects' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignProjectAssoc' as GridID, 'Stage' as componentID, 'MktCampaignProjectAssoc' as tablename, 'Stage' as fieldname, 'opportunitiesAndProjects' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignProjectAssoc' as GridID, 'PRStatus' as componentID, 'MktCampaignProjectAssoc' as tablename, 'PRStatus' as fieldname, 'opportunitiesAndProjects' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignProjectAssoc' as GridID, 'ProjectCurrencyCode' as componentID, 'MktCampaignProjectAssoc' as tablename, 'ProjectCurrencyCode' as fieldname, 'opportunitiesAndProjects' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignProjectAssoc' as GridID, 'PRName' as componentID, 'MktCampaignProjectAssoc' as tablename, 'PRName' as fieldname, 'opportunitiesAndProjects' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignProjectAssoc' as GridID, 'PRContactName' as componentID, 'MktCampaignProjectAssoc' as tablename, 'PRContactName' as fieldname, 'opportunitiesAndProjects' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignProjectAssoc' as GridID, 'PRContactID' as componentID, 'MktCampaignProjectAssoc' as tablename, 'PRContactID' as fieldname, 'opportunitiesAndProjects' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignProjectAssoc' as GridID, 'PRClientName' as componentID, 'MktCampaignProjectAssoc' as tablename, 'PRClientName' as fieldname, 'opportunitiesAndProjects' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignProjectAssoc' as GridID, 'PRClientID' as componentID, 'MktCampaignProjectAssoc' as tablename, 'PRClientID' as fieldname, 'opportunitiesAndProjects' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignProjectAssoc' as GridID, 'ModUser' as componentID, 'MktCampaignProjectAssoc' as tablename, 'ModUser' as fieldname, 'opportunitiesAndProjects' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignProjectAssoc' as GridID, 'ModDate' as componentID, 'MktCampaignProjectAssoc' as tablename, 'ModDate' as fieldname, 'opportunitiesAndProjects' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignProjectAssoc' as GridID, 'CreateUser' as componentID, 'MktCampaignProjectAssoc' as tablename, 'CreateUser' as fieldname, 'opportunitiesAndProjects' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignProjectAssoc' as GridID, 'CreateDate' as componentID, 'MktCampaignProjectAssoc' as tablename, 'CreateDate' as fieldname, 'opportunitiesAndProjects' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignProjectAssoc' as GridID, 'CampaignID' as componentID, 'MktCampaignProjectAssoc' as tablename, 'CampaignID' as fieldname, 'opportunitiesAndProjects' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignFileLinks' as GridID, 'LinkID' as componentID, 'MktCampaignFileLinks' as tablename, 'LinkID' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignFileLinks' as GridID, 'Graphic' as componentID, 'MktCampaignFileLinks' as tablename, 'Graphic' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignFileLinks' as GridID, 'FilePath' as componentID, 'MktCampaignFileLinks' as tablename, 'FilePath' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignFileLinks' as GridID, 'Description' as componentID, 'MktCampaignFileLinks' as tablename, 'Description' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignFileLinks' as GridID, 'CampaignID' as componentID, 'MktCampaignFileLinks' as tablename, 'CampaignID' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'Rating' as componentID, 'MktCampaignContactAssoc' as tablename, 'Rating' as fieldname, 'responses' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'QualifiedStatus' as componentID, 'MktCampaignContactAssoc' as tablename, 'QualifiedStatus' as fieldname, 'responses' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'ContactStatus' as componentID, 'MktCampaignContactAssoc' as tablename, 'ContactStatus' as fieldname, 'responses' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'ContactID' as componentID, 'MktCampaignContactAssoc' as tablename, 'ContactID' as fieldname, 'responses' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'Contact' as componentID, 'MktCampaignContactAssoc' as tablename, 'Contact' as fieldname, 'responses' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'ClientID' as componentID, 'MktCampaignContactAssoc' as tablename, 'ClientID' as fieldname, 'responses' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'Client' as componentID, 'MktCampaignContactAssoc' as tablename, 'Client' as fieldname, 'responses' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'MktCampaignContactAssoc' as GridID, 'CampaignID' as componentID, 'MktCampaignContactAssoc' as tablename, 'CampaignID' as fieldname, 'responses' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'PKey' as componentID, 'FW_ATTACHMENTS' as tablename, 'PKey' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key3' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key3' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key2' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key2' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key1' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key1' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileSize' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileSize' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileName' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileName' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileID' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileID' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileDescription' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileDescription' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'ContentType' as componentID, 'FW_ATTACHMENTS' as tablename, 'ContentType' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'CategoryDescription' as componentID, 'FW_ATTACHMENTS' as tablename, 'CategoryDescription' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'CategoryCode' as componentID, 'FW_ATTACHMENTS' as tablename, 'CategoryCode' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'CampaignID' as componentID, 'FW_ATTACHMENTS' as tablename, 'CampaignID' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Application' as componentID, 'FW_ATTACHMENTS' as tablename, 'Application' as fieldname, 'filesAndLinks' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'WBS3' as componentID, 'Activity' as tablename, 'WBS3' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'WBS2' as componentID, 'Activity' as tablename, 'WBS2' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'WBS1' as componentID, 'Activity' as tablename, 'WBS1' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'VendorName' as componentID, 'Activity' as tablename, 'VendorName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'VendorContactName' as componentID, 'Activity' as tablename, 'VendorContactName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'Vendor' as componentID, 'Activity' as tablename, 'Vendor' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'UpdateID' as componentID, 'Activity' as tablename, 'UpdateID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'TypeName' as componentID, 'Activity' as tablename, 'TypeName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'Type' as componentID, 'Activity' as tablename, 'Type' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'TaskCompletionDate' as componentID, 'Activity' as tablename, 'TaskCompletionDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'Subject' as componentID, 'Activity' as tablename, 'Subject' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'StartDateObj' as componentID, 'Activity' as tablename, 'StartDateObj' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'StartDate' as componentID, 'Activity' as tablename, 'StartDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'ShowTimeAs' as componentID, 'Activity' as tablename, 'ShowTimeAs' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RP' as componentID, 'Activity' as tablename, 'RP' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'ReminderUnit' as componentID, 'Activity' as tablename, 'ReminderUnit' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'ReminderMinHrDay' as componentID, 'Activity' as tablename, 'ReminderMinHrDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'ReminderInd' as componentID, 'Activity' as tablename, 'ReminderInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'ReminderDate' as componentID, 'Activity' as tablename, 'ReminderDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccurMonth' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurMonth' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccurInd' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccurDay' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccur' as componentID, 'Activity' as tablename, 'RecurrYearlyOccur' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyMonth' as componentID, 'Activity' as tablename, 'RecurrYearlyMonth' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyDay' as componentID, 'Activity' as tablename, 'RecurrYearlyDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyWed' as componentID, 'Activity' as tablename, 'RecurrWeeklyWed' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyTue' as componentID, 'Activity' as tablename, 'RecurrWeeklyTue' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyThu' as componentID, 'Activity' as tablename, 'RecurrWeeklyThu' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklySun' as componentID, 'Activity' as tablename, 'RecurrWeeklySun' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklySat' as componentID, 'Activity' as tablename, 'RecurrWeeklySat' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyMon' as componentID, 'Activity' as tablename, 'RecurrWeeklyMon' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyFri' as componentID, 'Activity' as tablename, 'RecurrWeeklyFri' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyFreq' as componentID, 'Activity' as tablename, 'RecurrWeeklyFreq' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrType' as componentID, 'Activity' as tablename, 'RecurrType' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrStartDate' as componentID, 'Activity' as tablename, 'RecurrStartDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccurInd' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccurFreq' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurFreq' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccurDay' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccur' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccur' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyFreq' as componentID, 'Activity' as tablename, 'RecurrMonthlyFreq' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyDay' as componentID, 'Activity' as tablename, 'RecurrMonthlyDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrID' as componentID, 'Activity' as tablename, 'RecurrID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrEndType' as componentID, 'Activity' as tablename, 'RecurrEndType' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrEndDate' as componentID, 'Activity' as tablename, 'RecurrEndDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrenceInd' as componentID, 'Activity' as tablename, 'RecurrenceInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrDailyWeekDay' as componentID, 'Activity' as tablename, 'RecurrDailyWeekDay' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'RecurrDailyFreq' as componentID, 'Activity' as tablename, 'RecurrDailyFreq' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'ReadID' as componentID, 'Activity' as tablename, 'ReadID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'ProjName' as componentID, 'Activity' as tablename, 'ProjName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'PrivateInd' as componentID, 'Activity' as tablename, 'PrivateInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'PriorityDesc' as componentID, 'Activity' as tablename, 'PriorityDesc' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'Priority' as componentID, 'Activity' as tablename, 'Priority' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'PrimaryClient' as componentID, 'Activity' as tablename, 'PrimaryClient' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'OwnerName' as componentID, 'Activity' as tablename, 'OwnerName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'Owner' as componentID, 'Activity' as tablename, 'Owner' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'OutlookType' as componentID, 'Activity' as tablename, 'OutlookType' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'Notes' as componentID, 'Activity' as tablename, 'Notes' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'ModUser' as componentID, 'Activity' as tablename, 'ModUser' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'ModDate' as componentID, 'Activity' as tablename, 'ModDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'MktName' as componentID, 'Activity' as tablename, 'MktName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'MaxOccurences' as componentID, 'Activity' as tablename, 'MaxOccurences' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'Location' as componentID, 'Activity' as tablename, 'Location' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'LeadID' as componentID, 'Activity' as tablename, 'LeadID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'isTask' as componentID, 'Activity' as tablename, 'isTask' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'isCalendar' as componentID, 'Activity' as tablename, 'isCalendar' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'InstanceStartDate' as componentID, 'Activity' as tablename, 'InstanceStartDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'HasNotes' as componentID, 'Activity' as tablename, 'HasNotes' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'EndDateSort' as componentID, 'Activity' as tablename, 'EndDateSort' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'EndDateObj' as componentID, 'Activity' as tablename, 'EndDateObj' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'EndDateFilter' as componentID, 'Activity' as tablename, 'EndDateFilter' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'EndDate' as componentID, 'Activity' as tablename, 'EndDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'Employee' as componentID, 'Activity' as tablename, 'Employee' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'Duration' as componentID, 'Activity' as tablename, 'Duration' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'CreateUser' as componentID, 'Activity' as tablename, 'CreateUser' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'CreateEmployee' as componentID, 'Activity' as tablename, 'CreateEmployee' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'CreateDate' as componentID, 'Activity' as tablename, 'CreateDate' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'ContactPhoneForVendor' as componentID, 'Activity' as tablename, 'ContactPhoneForVendor' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'ContactNameForVendor' as componentID, 'Activity' as tablename, 'ContactNameForVendor' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'ContactName' as componentID, 'Activity' as tablename, 'ContactName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'ContactIDForVendor' as componentID, 'Activity' as tablename, 'ContactIDForVendor' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'ContactID' as componentID, 'Activity' as tablename, 'ContactID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'CompletionInd' as componentID, 'Activity' as tablename, 'CompletionInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'ClientName' as componentID, 'Activity' as tablename, 'ClientName' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'ClientID' as componentID, 'Activity' as tablename, 'ClientID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'CampaignID' as componentID, 'Activity' as tablename, 'CampaignID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'Attendees' as componentID, 'Activity' as tablename, 'Attendees' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'Associations' as componentID, 'Activity' as tablename, 'Associations' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'AllDayEventInd' as componentID, 'Activity' as tablename, 'AllDayEventInd' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'acType' as componentID, 'Activity' as tablename, 'acType' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'MktCampaigns' as infocenterArea, 'Activity' as GridID, 'ActivityID' as componentID, 'Activity' as tablename, 'ActivityID' as fieldname, 'activities' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PRUNIT' as componentID, null as tablename, null as fieldname, 'units_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PRREVENUE' as componentID, null as tablename, null as fieldname, 'estimated_fee' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PRPROPOSALS' as componentID, null as tablename, null as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PRPROJECTCODES' as componentID, null as tablename, null as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'Project.Links.Divider' as componentID, null as tablename, null as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'Project.Files.Divider' as componentID, null as tablename, null as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PRMILESTONE' as componentID, null as tablename, null as fieldname, 'datesAndCosts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PRLABOR' as componentID, null as tablename, null as fieldname, 'labor_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PRINVOICESUMMARY' as componentID, null as tablename, null as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PRINVOICE' as componentID, null as tablename, null as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PRFILELINKS' as componentID, null as tablename, null as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PRESTIMATESUMMARY' as componentID, null as tablename, null as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PRESTIMATEANALYSIS' as componentID, null as tablename, null as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PREMP' as componentID, null as tablename, null as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PRDESCRIPTIONS' as componentID, null as tablename, null as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PRCOMPETITION' as componentID, null as tablename, null as fieldname, 'competition' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PRCHARGECOMPANIES' as componentID, null as tablename, null as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PRAWARDS' as componentID, null as tablename, null as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR._transType' as componentID, 'PR' as tablename, '_transType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR._rowType' as componentID, 'PR' as tablename, '_rowType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR._RecordAccessTopLevel' as componentID, 'PR' as tablename, '_RecordAccessTopLevel' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR._RecordAccessParent' as componentID, 'PR' as tablename, '_RecordAccessParent' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR._RecordAccess' as componentID, 'PR' as tablename, '_RecordAccess' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR._isStructureEditable' as componentID, 'PR' as tablename, '_isStructureEditable' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR._canRead' as componentID, 'PR' as tablename, '_canRead' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Zip' as componentID, 'PR' as tablename, 'Zip' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.XChargeMult' as componentID, 'PR' as tablename, 'XChargeMult' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.XChargeMethod' as componentID, 'PR' as tablename, 'XChargeMethod' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.XCharge' as componentID, 'PR' as tablename, 'XCharge' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.WeightedRevenue' as componentID, 'PR' as tablename, 'WeightedRevenue' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.WBSNumber' as componentID, 'PR' as tablename, 'WBSNumber' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.WBSInTable' as componentID, 'PR' as tablename, 'WBSInTable' as fieldname, 'noassociation' as tabID, 0 as essential union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.WBS3' as componentID, 'PR' as tablename, 'WBS3' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.WBS2' as componentID, 'PR' as tablename, 'WBS2' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.WBS1' as componentID, 'PR' as tablename, 'WBS1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.VersionID' as componentID, 'PR' as tablename, 'VersionID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.UtilizationScheduleFlg' as componentID, 'PR' as tablename, 'UtilizationScheduleFlg' as fieldname, 'sidebar' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.UtilizationScheduleFlgDescription' as componentID, 'PR' as tablename, 'UtilizationScheduleFlgDescription' as fieldname, 'sidebar' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.UnitTable' as componentID, 'PR' as tablename, 'UnitTable' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.TotalProjectCost' as componentID, 'PR' as tablename, 'TotalProjectCost' as fieldname, 'datesAndCosts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.TotalFunc' as componentID, 'PR' as tablename, 'TotalFunc' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.TotalDirectFeesFunc' as componentID, 'PR' as tablename, 'TotalDirectFeesFunc' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.TotalDirectFeesBilling' as componentID, 'PR' as tablename, 'TotalDirectFeesBilling' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.TotalDirectFees' as componentID, 'PR' as tablename, 'TotalDirectFees' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.TotalCostComment' as componentID, 'PR' as tablename, 'TotalCostComment' as fieldname, 'datesAndCosts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.TotalContractValue' as componentID, 'PR' as tablename, 'TotalContractValue' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.TotalBilling' as componentID, 'PR' as tablename, 'TotalBilling' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Total' as componentID, 'PR' as tablename, 'Total' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.TLSyncModDate' as componentID, 'PR' as tablename, 'TLSyncModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.TLProjectName' as componentID, 'PR' as tablename, 'TLProjectName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.TLProjectID' as componentID, 'PR' as tablename, 'TLProjectID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.TLInternalKey' as componentID, 'PR' as tablename, 'TLInternalKey' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.TLChargeBandInternalKey' as componentID, 'PR' as tablename, 'TLChargeBandInternalKey' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.TLChargeBandExternalCode' as componentID, 'PR' as tablename, 'TLChargeBandExternalCode' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.TKCheckRPPlannedHrs' as componentID, 'PR' as tablename, 'TKCheckRPPlannedHrs' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.TKCheckRPDate' as componentID, 'PR' as tablename, 'TKCheckRPDate' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Timescale' as componentID, 'PR' as tablename, 'Timescale' as fieldname, 'estimated_fee' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.TimescaleDescription' as componentID, 'PR' as tablename, 'TimescaleDescription' as fieldname, 'estimated_fee' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.SupervisorTitle' as componentID, 'PR' as tablename, 'SupervisorTitle' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.SupervisorPhoneFormat' as componentID, 'PR' as tablename, 'SupervisorPhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.SupervisorPhone' as componentID, 'PR' as tablename, 'SupervisorPhone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.SupervisorName' as componentID, 'PR' as tablename, 'SupervisorName' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.SupervisorEmail' as componentID, 'PR' as tablename, 'SupervisorEmail' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.SupervisorComponent' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Supervisor' as componentID, 'PR' as tablename, 'Supervisor' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.SubLevel' as componentID, 'PR' as tablename, 'SubLevel' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Status' as componentID, 'PR' as tablename, 'Status' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.State' as componentID, 'PR' as tablename, 'State' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.StartDate' as componentID, 'PR' as tablename, 'StartDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.StageStep' as componentID, 'PR' as tablename, 'StageStep' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.StageInfo' as componentID, null as tablename, null as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.StageDescription' as componentID, 'PR' as tablename, 'StageDescription' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Stage' as componentID, 'PR' as tablename, 'Stage' as fieldname, 'sidebar' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.SourceWbs3' as componentID, 'PR' as tablename, 'SourceWbs3' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.SourceWbs2' as componentID, 'PR' as tablename, 'SourceWbs2' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.SourceWbs1' as componentID, 'PR' as tablename, 'SourceWbs1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.SourceType' as componentID, 'PR' as tablename, 'SourceType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Source' as componentID, 'PR' as tablename, 'Source' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Solicitation' as componentID, 'PR' as tablename, 'Solicitation' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.SiblingWBS1Name' as componentID, 'PR' as tablename, 'SiblingWBS1Name' as fieldname, 'overview' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.WBS1ReadyForProcessing' as componentID, 'PR' as tablename, 'WBS1ReadyForProcessing' as fieldname, 'noassociation' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.SiblingWBS1' as componentID, 'PR' as tablename, 'SiblingWBS1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.SFLastModifiedDate' as componentID, 'PR' as tablename, 'SFLastModifiedDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.SFID' as componentID, 'PR' as tablename, 'SFID' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ServProCode' as componentID, 'PR' as tablename, 'ServProCode' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevUpsetWBS3Name' as componentID, 'PR' as tablename, 'RevUpsetWBS3Name' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevUpsetWBS3' as componentID, 'PR' as tablename, 'RevUpsetWBS3' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevUpsetWBS2SubLevel' as componentID, 'PR' as tablename, 'RevUpsetWBS2SubLevel' as fieldname, 'noassociation' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevUpsetWBS2Name' as componentID, 'PR' as tablename, 'RevUpsetWBS2Name' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevUpsetWBS2' as componentID, 'PR' as tablename, 'RevUpsetWBS2' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevUpsetLimits' as componentID, 'PR' as tablename, 'RevUpsetLimits' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevUpsetIncludeReimbCons' as componentID, 'PR' as tablename, 'RevUpsetIncludeReimbCons' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevUpsetIncludeReimb' as componentID, 'PR' as tablename, 'RevUpsetIncludeReimb' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevUpsetIncludeCons' as componentID, 'PR' as tablename, 'RevUpsetIncludeCons' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevUpsetIncludeCompDirExp' as componentID, 'PR' as tablename, 'RevUpsetIncludeCompDirExp' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevUpsetIncludeComp' as componentID, 'PR' as tablename, 'RevUpsetIncludeComp' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevUpsetCategoryToAdjust' as componentID, 'PR' as tablename, 'RevUpsetCategoryToAdjust' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevType5' as componentID, 'PR' as tablename, 'RevType5' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevType4' as componentID, 'PR' as tablename, 'RevType4' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevType3' as componentID, 'PR' as tablename, 'RevType3' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevType2' as componentID, 'PR' as tablename, 'RevType2' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevType' as componentID, 'PR' as tablename, 'RevType' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RevenueMethod' as componentID, 'PR' as tablename, 'RevenueMethod' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Revenue' as componentID, 'PR' as tablename, 'Revenue' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RestrictChargeCompanies' as componentID, 'PR' as tablename, 'RestrictChargeCompanies' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Responsibility' as componentID, 'PR' as tablename, 'Responsibility' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.RequireComments' as componentID, 'PR' as tablename, 'RequireComments' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ReimbAllowFunctionalCurrency' as componentID, 'PR' as tablename, 'ReimbAllowFunctionalCurrency' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ReimbAllowExpFunctionalCurrency' as componentID, 'PR' as tablename, 'ReimbAllowExpFunctionalCurrency' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ReimbAllowExpBillingCurrency' as componentID, 'PR' as tablename, 'ReimbAllowExpBillingCurrency' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ReimbAllowExp' as componentID, 'PR' as tablename, 'ReimbAllowExp' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ReimbAllowConsFunctionalCurrency' as componentID, 'PR' as tablename, 'ReimbAllowConsFunctionalCurrency' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ReimbAllowConsBillingCurrency' as componentID, 'PR' as tablename, 'ReimbAllowConsBillingCurrency' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ReimbAllowCons' as componentID, 'PR' as tablename, 'ReimbAllowCons' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ReimbAllowBillingCurrency' as componentID, 'PR' as tablename, 'ReimbAllowBillingCurrency' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ReimbAllow' as componentID, 'PR' as tablename, 'ReimbAllow' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Referable' as componentID, 'PR' as tablename, 'Referable' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ReceivablesInfo' as componentID, null as tablename, null as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ReadyForProcessing' as componentID, 'PR' as tablename, 'ReadyForProcessing' as fieldname, 'noassociation' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ReadyForApproval' as componentID, 'PR' as tablename, 'ReadyForApproval' as fieldname, 'overview' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ReadOnly' as componentID, 'PR' as tablename, 'ReadOnly' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PublicNoticeDate' as componentID, 'PR' as tablename, 'PublicNoticeDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProposalManagerTitle' as componentID, 'PR' as tablename, 'ProposalManagerTitle' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProposalManagerPhoneFormat' as componentID, 'PR' as tablename, 'ProposalManagerPhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProposalManagerPhone' as componentID, 'PR' as tablename, 'ProposalManagerPhone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProposalManagerName' as componentID, 'PR' as tablename, 'ProposalManagerName' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProposalManagerLocation' as componentID, 'PR' as tablename, 'ProposalManagerLocation' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProposalManagerEmail' as componentID, 'PR' as tablename, 'ProposalManagerEmail' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProposalManagerComponent' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProposalManager' as componentID, 'PR' as tablename, 'ProposalManager' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProjMgrTitle' as componentID, 'PR' as tablename, 'ProjMgrTitle' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProjMgrPhoneFormat' as componentID, 'PR' as tablename, 'ProjMgrPhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProjMgrPhone' as componentID, 'PR' as tablename, 'ProjMgrPhone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProjMgrName' as componentID, 'PR' as tablename, 'ProjMgrName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProjMgrEmail' as componentID, 'PR' as tablename, 'ProjMgrEmail' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProjMgrComponent' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProjMgr' as componentID, 'PR' as tablename, 'ProjMgr' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProjectType' as componentID, 'PR' as tablename, 'ProjectType' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProjectTemplate' as componentID, 'PR' as tablename, 'ProjectTemplate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProjectInformation' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProjectExchangeRate' as componentID, 'PR' as tablename, 'ProjectExchangeRate' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProjectCurrencyLabel' as componentID, null as tablename, null as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProjectCurrencyCode2' as componentID, null as tablename, null as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProjectCurrencyCode' as componentID, 'PR' as tablename, 'ProjectCurrencyCode' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProfServicesComplDate' as componentID, 'PR' as tablename, 'ProfServicesComplDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Probability' as componentID, 'PR' as tablename, 'Probability' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PRINVOICESUMMARY.LRTransDate' as componentID, 'PR' as tablename, 'PRINVOICESUMMARY.LRTransDate' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PRINVOICESUMMARY.LRInvoiceNumber' as componentID, 'PR' as tablename, 'PRINVOICESUMMARY.LRInvoiceNumber' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PRINVOICESUMMARY.LRAmountBillingCurrency' as componentID, 'PR' as tablename, 'PRINVOICESUMMARY.LRAmountBillingCurrency' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PRINVOICESUMMARY.AvgAge' as componentID, 'PR' as tablename, 'PRINVOICESUMMARY.AvgAge' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PrincipalTitle' as componentID, 'PR' as tablename, 'PrincipalTitle' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PrincipalPhoneFormat' as componentID, 'PR' as tablename, 'PrincipalPhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PrincipalPhone' as componentID, 'PR' as tablename, 'PrincipalPhone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PrincipalName' as componentID, 'PR' as tablename, 'PrincipalName' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PrincipalEmail' as componentID, 'PR' as tablename, 'PrincipalEmail' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PrincipalComponent' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Principal' as componentID, 'PR' as tablename, 'Principal' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PrimaryClient' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PreAwardWBS1' as componentID, 'PR' as tablename, 'PreAwardWBS1' as fieldname, 'noassociation' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PORMBRate' as componentID, 'PR' as tablename, 'PORMBRate' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.POCNSRate' as componentID, 'PR' as tablename, 'POCNSRate' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PlanStartDate' as componentID, 'PR' as tablename, 'PlanStartDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PlanOutlineNumber' as componentID, 'PR' as tablename, 'PlanOutlineNumber' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PlanID' as componentID, 'PR' as tablename, 'PlanID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PlanEndDate' as componentID, 'PR' as tablename, 'PlanEndDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PIMID' as componentID, 'PR' as tablename, 'PIMID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PhotoModDate' as componentID, 'PR' as tablename, 'PhotoModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PhoneFormat' as componentID, 'PR' as tablename, 'PhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Phone' as componentID, 'PR' as tablename, 'Phone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PeriodOfPerformance' as componentID, 'PR' as tablename, 'PeriodOfPerformance' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PctComp' as componentID, 'PR' as tablename, 'PctComp' as fieldname, 'compensation' as tabID, 0 as essential union 
select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PayRateTableNo_LaborRate' as componentID, 'PR' as tablename, 'PayRateTableNo_LaborRate' as fieldname, 'noassociation' as tabID, 0 as essential  union
select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PayRateTableNo_CategoryRate' as componentID, 'PR' as tablename, 'PayRateTableNo_CategoryRate' as fieldname, 'noassociation' as tabID, 0 as essential  union
select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PayRateTableNo_LaborCode' as componentID, 'PR' as tablename, 'PayRateTableNo_LaborCode' as fieldname, 'noassociation' as tabID, 0 as essential  union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PayRateTableNo' as componentID, 'PR' as tablename, 'PayRateTableNo' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PayRateMeth' as componentID, 'PR' as tablename, 'PayRateMeth' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.parentId' as componentID, 'PR' as tablename, 'parentId' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.OwnerPhoneFormat' as componentID, 'PR' as tablename, 'OwnerPhoneFormat' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.OwnerPhone' as componentID, 'PR' as tablename, 'OwnerPhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.OwnerName' as componentID, 'PR' as tablename, 'OwnerName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.OwnerContactID' as componentID, 'PR' as tablename, 'OwnerContactID' as fieldname, 'overview' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.OwnerClientID' as componentID, 'PR' as tablename, 'OwnerClientID' as fieldname, 'overview' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.OwnerCityStateZip' as componentID, 'PR' as tablename, 'OwnerCityStateZip' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.OwnerAddress' as componentID, 'PR' as tablename, 'OwnerAddress' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.OurRole' as componentID, 'PR' as tablename, 'OurRole' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.OrgStatus' as componentID, 'PR' as tablename, 'OrgStatus' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.OrgName' as componentID, 'PR' as tablename, 'OrgName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Org5Name' as componentID, 'PR' as tablename, 'Org5Name' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Org5' as componentID, 'PR' as tablename, 'Org5' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Org4Name' as componentID, 'PR' as tablename, 'Org4Name' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Org4' as componentID, 'PR' as tablename, 'Org4' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Org3Name' as componentID, 'PR' as tablename, 'Org3Name' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Org3' as componentID, 'PR' as tablename, 'Org3' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Org2Name' as componentID, 'PR' as tablename, 'Org2Name' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Org2' as componentID, 'PR' as tablename, 'Org2' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Org1Name' as componentID, 'PR' as tablename, 'Org1Name' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Org1' as componentID, 'PR' as tablename, 'Org1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Org' as componentID, 'PR' as tablename, 'Org' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.OpenDate' as componentID, 'PR' as tablename, 'OpenDate' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Name' as componentID, 'PR' as tablename, 'Name' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.NAICSDescriptionConcat' as componentID, 'PR' as tablename, 'NAICSDescriptionConcat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.NAICSDescription' as componentID, 'PR' as tablename, 'NAICSDescription' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.NAICS' as componentID, 'PR' as tablename, 'NAICS' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.MultAmt' as componentID, 'PR' as tablename, 'MultAmt' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ModUser' as componentID, 'PR' as tablename, 'ModUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ModUserName' as componentID, 'PR' as tablename, 'ModUserName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ModDate' as componentID, 'PR' as tablename, 'ModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Memo' as componentID, 'PR' as tablename, 'Memo' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.MasterContract' as componentID, 'PR' as tablename, 'MasterContract' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.MarketingCoordinatorTitle' as componentID, 'PR' as tablename, 'MarketingCoordinatorTitle' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.MarketingCoordinatorPhoneFormat' as componentID, 'PR' as tablename, 'MarketingCoordinatorPhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.MarketingCoordinatorPhone' as componentID, 'PR' as tablename, 'MarketingCoordinatorPhone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.MarketingCoordinatorName' as componentID, 'PR' as tablename, 'MarketingCoordinatorName' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.MarketingCoordinatorLocation' as componentID, 'PR' as tablename, 'MarketingCoordinatorLocation' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.MarketingCoordinatorEmail' as componentID, 'PR' as tablename, 'MarketingCoordinatorEmail' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.MarketingCoordinatorComponent' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.MarketingCoordinator' as componentID, 'PR' as tablename, 'MarketingCoordinator' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.LostToName' as componentID, 'PR' as tablename, 'LostToName' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.LostTo' as componentID, 'PR' as tablename, 'LostTo' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.LongName' as componentID, 'PR' as tablename, 'LongName' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.LocationComponent' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Locale' as componentID, 'PR' as tablename, 'Locale' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.LineItemApprovalEK' as componentID, 'PR' as tablename, 'LineItemApprovalEK' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.LineItemApproval' as componentID, 'PR' as tablename, 'LineItemApproval' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Level3Number' as componentID, 'PR' as tablename, 'Level3Number' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Level3Name' as componentID, 'PR' as tablename, 'Level3Name' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Level2Number' as componentID, 'PR' as tablename, 'Level2Number' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Level2Name' as componentID, 'PR' as tablename, 'Level2Name' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Level1Number' as componentID, 'PR' as tablename, 'Level1Number' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Level1Name' as componentID, 'PR' as tablename, 'Level1Name' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.LabPctComp2' as componentID, null as tablename, null as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.LabPctComp' as componentID, 'PR' as tablename, 'LabPctComp' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.LaborStartDate' as componentID, null as tablename, null as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.LaborActCompletionDate' as componentID, null as tablename, null as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.LabCostTable' as componentID, 'PR' as tablename, 'LabCostTable' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.LabBillTable' as componentID, 'PR' as tablename, 'LabBillTable' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.key' as componentID, 'PR' as tablename, 'key' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.IsUserAnEmployee' as componentID, 'PR' as tablename, 'IsUserAnEmployee' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.IsMainWBS1' as componentID, 'PR' as tablename, 'IsMainWBS1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.IsBillingGroupWBS1' as componentID, 'PR' as tablename, 'IsBillingGroupWBS1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.IQLastUpdate' as componentID, 'PR' as tablename, 'IQLastUpdate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.IQID' as componentID, 'PR' as tablename, 'IQID' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.InvoiceDivider' as componentID, null as tablename, null as fieldname, 'invoices' as tabID, 0 as essential union 
select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ICBillingLabTableNo_RateTable' as componentID, 'PR' as tablename, 'ICBillingLabTableNo_RateTable' as fieldname, 'noassociation' as tabID, 0 as essential  union
select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ICBillingLabTableNo_CategoryTable' as componentID, 'PR' as tablename, 'ICBillingLabTableNo_CategoryTable' as fieldname, 'noassociation' as tabID, 0 as essential  union
select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ICBillingLabTableNo_LaborCode' as componentID, 'PR' as tablename, 'ICBillingLabTableNo_LaborCode' as fieldname, 'noassociation' as tabID, 0 as essential  union
select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ICBillingExpTableNo_CategoryTable' as componentID, 'PR' as tablename, 'ICBillingExpTableNo_CategoryTable' as fieldname, 'noassociation' as tabID, 0 as essential  union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ICBillingLabTableNo' as componentID, 'PR' as tablename, 'ICBillingLabTableNo' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ICBillingLabMult' as componentID, 'PR' as tablename, 'ICBillingLabMult' as fieldname, 'accounting_web' as tabID, 0 as essential union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ICBillingLabMethodDescription' as componentID, 'PR' as tablename, 'ICBillingLabMethodDescription' as fieldname, 'noassociation' as tabID, 0 as essential union  
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ICBillingLabMethod' as componentID, 'PR' as tablename, 'ICBillingLabMethod' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ICBillingLabMethodDescription' as componentID, 'PR' as tablename, 'ICBillingLabMethodDescription' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ICBillingLab' as componentID, 'PR' as tablename, 'ICBillingLab' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ICBillingExpTableNo' as componentID, 'PR' as tablename, 'ICBillingExpTableNo' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ICBillingExpMethodDescription' as componentID, 'PR' as tablename, 'ICBillingExpMethodDescription' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ICBillingExpMult' as componentID, 'PR' as tablename, 'ICBillingExpMult' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ICBillingExpMethod' as componentID, 'PR' as tablename, 'ICBillingExpMethod' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ICBillingExpMethodDescription' as componentID, 'PR' as tablename, 'ICBillingExpMethodDescription' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ICBillingExp' as componentID, 'PR' as tablename, 'ICBillingExp' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.HasPlanData' as componentID, 'PR' as tablename, 'HasPlanData' as fieldname, 'noassociation' as tabID, 0 as essential union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.HasFees' as componentID, 'PR' as tablename, 'HasFees' as fieldname, 'noassociation' as tabID, 0 as essential union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.HasJtd' as componentID, 'PR' as tablename, 'HasJtd' as fieldname, 'noassociation' as tabID, 0 as essential union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.HasPhoto' as componentID, 'PR' as tablename, 'HasPhoto' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.HasDefaultReference' as componentID, 'PR' as tablename, 'HasDefaultReference' as fieldname, 'noassociation' as tabID, 0 as essential  union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.HasBillingTerms' as componentID, 'PR' as tablename, 'HasBillingTerms' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FunctionalCurrencyLabel' as componentID, null as tablename, null as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FirmCostComment' as componentID, 'PR' as tablename, 'FirmCostComment' as fieldname, 'datesAndCosts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FirmCost' as componentID, 'PR' as tablename, 'FirmCost' as fieldname, 'datesAndCosts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FESurchargePct' as componentID, 'PR' as tablename, 'FESurchargePct' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FESurcharge' as componentID, 'PR' as tablename, 'FESurcharge' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FEOtherPct' as componentID, 'PR' as tablename, 'FEOtherPct' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FEOther' as componentID, 'PR' as tablename, 'FEOther' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FeeFunctionalCurrency' as componentID, 'PR' as tablename, 'FeeFunctionalCurrency' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FeeDirLabFunctionalCurrency' as componentID, 'PR' as tablename, 'FeeDirLabFunctionalCurrency' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FeeDirLabBillingCurrency' as componentID, 'PR' as tablename, 'FeeDirLabBillingCurrency' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FeeDirLab' as componentID, 'PR' as tablename, 'FeeDirLab' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FeeDirExpFunctionalCurrency' as componentID, 'PR' as tablename, 'FeeDirExpFunctionalCurrency' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FeeDirExpBillingCurrency' as componentID, 'PR' as tablename, 'FeeDirExpBillingCurrency' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FeeDirExp' as componentID, 'PR' as tablename, 'FeeDirExp' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FeeBillingCurrency' as componentID, 'PR' as tablename, 'FeeBillingCurrency' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Fee' as componentID, 'PR' as tablename, 'Fee' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FederalInd' as componentID, 'PR' as tablename, 'FederalInd' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FEAddlExpensesPct' as componentID, 'PR' as tablename, 'FEAddlExpensesPct' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FEAddlExpenses' as componentID, 'PR' as tablename, 'FEAddlExpenses' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.FaxFormat' as componentID, 'PR' as tablename, 'FaxFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Fax' as componentID, 'PR' as tablename, 'Fax' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ExpPctComp2' as componentID, null as tablename, null as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ExpPctComp' as componentID, 'PR' as tablename, 'ExpPctComp' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ExpenseStartDate' as componentID, null as tablename, null as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ExpenseActCompletionDate' as componentID, null as tablename, null as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.EstStartDate' as componentID, 'PR' as tablename, 'EstStartDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.EstFees' as componentID, 'PR' as tablename, 'EstFees' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.EstEndDate' as componentID, 'PR' as tablename, 'EstEndDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.EstConstructionCost' as componentID, 'PR' as tablename, 'EstConstructionCost' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.EstCompletionDate' as componentID, 'PR' as tablename, 'EstCompletionDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.EndDate' as componentID, 'PR' as tablename, 'EndDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.EMail' as componentID, 'PR' as tablename, 'EMail' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Duration' as componentID, 'PR' as tablename, 'Duration' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Description' as componentID, 'PR' as tablename, 'Description' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.DefaultTaskType' as componentID, 'PR' as tablename, 'DefaultTaskType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.DefaultEffortDriven' as componentID, 'PR' as tablename, 'DefaultEffortDriven' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.DaysOpen' as componentID, 'PR' as tablename, 'DaysOpen' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CustomCurrencyCode' as componentID, 'PR' as tablename, 'CustomCurrencyCode' as fieldname, 'noassociation' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CRMHours' as componentID, 'PR' as tablename, 'CRMHours' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CreateUser' as componentID, 'PR' as tablename, 'CreateUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CreateUserName' as componentID, 'PR' as tablename, 'CreateUserName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CreateDate' as componentID, 'PR' as tablename, 'CreateDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.County' as componentID, 'PR' as tablename, 'County' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Country' as componentID, 'PR' as tablename, 'Country' as fieldname, 'overview' as tabID, 0 as essential union 
select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CostRateTableNo_LaborRate' as componentID, 'PR' as tablename, 'CostRateTableNo_LaborRate' as fieldname, 'noassociation' as tabID, 0 as essential  union
select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CostRateTableNo_CategoryRate' as componentID, 'PR' as tablename, 'CostRateTableNo_CategoryRate' as fieldname, 'noassociation' as tabID, 0 as essential  union
select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CostRateTableNo_LaborCode' as componentID, 'PR' as tablename, 'CostRateTableNo_LaborCode' as fieldname, 'noassociation' as tabID, 0 as essential  union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CostRateTableNo' as componentID, 'PR' as tablename, 'CostRateTableNo' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CostRateMeth' as componentID, 'PR' as tablename, 'CostRateMeth' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CopyPlan' as componentID, 'PR' as tablename, 'CopyPlan' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CopyChildren' as componentID, 'PR' as tablename, 'CopyChildren' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CopyBillingTerms' as componentID, 'PR' as tablename, 'CopyBillingTerms' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContractTypeGovCon' as componentID, 'PR' as tablename, 'ContractTypeGovCon' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContractDate' as componentID, 'PR' as tablename, 'ContractDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactWorkPhoneFormat' as componentID, 'PR' as tablename, 'ContactWorkPhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactWorkPhone' as componentID, 'PR' as tablename, 'ContactWorkPhone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactTitle' as componentID, 'PR' as tablename, 'ContactTitle' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactSuffix' as componentID, 'PR' as tablename, 'ContactSuffix' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactSalutation' as componentID, 'PR' as tablename, 'ContactSalutation' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactPager' as componentID, 'PR' as tablename, 'ContactPager' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactName' as componentID, 'PR' as tablename, 'ContactName' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactMailingAddress' as componentID, 'PR' as tablename, 'ContactMailingAddress' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactLastName' as componentID, 'PR' as tablename, 'ContactLastName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactID' as componentID, 'PR' as tablename, 'ContactID' as fieldname, 'overview' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactHomePhoneFormat' as componentID, 'PR' as tablename, 'ContactHomePhoneFormat' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactHomePhone' as componentID, 'PR' as tablename, 'ContactHomePhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactHomeAddress' as componentID, 'PR' as tablename, 'ContactHomeAddress' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactFirstName' as componentID, 'PR' as tablename, 'ContactFirstName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactFax' as componentID, 'PR' as tablename, 'ContactFax' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactEmail' as componentID, 'PR' as tablename, 'ContactEmail' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactCellPhoneFormat' as componentID, 'PR' as tablename, 'ContactCellPhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactCellPhone' as componentID, 'PR' as tablename, 'ContactCellPhone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ContactBusinessAddress' as componentID, 'PR' as tablename, 'ContactBusinessAddress' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ConsultFeeFunctionalCurrency' as componentID, 'PR' as tablename, 'ConsultFeeFunctionalCurrency' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ConsultFeeBillingCurrency' as componentID, 'PR' as tablename, 'ConsultFeeBillingCurrency' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ConsultFee' as componentID, 'PR' as tablename, 'ConsultFee' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ConstComplDate' as componentID, 'PR' as tablename, 'ConstComplDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ComplDateComment' as componentID, 'PR' as tablename, 'ComplDateComment' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CompetitionTypeDescription' as componentID, 'PR' as tablename, 'CompetitionTypeDescription' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CompetitionType' as componentID, 'PR' as tablename, 'CompetitionType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ClosedReasonDescription' as componentID, 'PR' as tablename, 'ClosedReasonDescription' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ClosedReason' as componentID, 'PR' as tablename, 'ClosedReason' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ClosedNotes' as componentID, 'PR' as tablename, 'ClosedNotes' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CloseDate' as componentID, 'PR' as tablename, 'CloseDate' as fieldname, 'sidebar' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Closed' as componentID, 'PR' as tablename, 'Closed' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ClientPrimaryAddress' as componentID, 'PR' as tablename, 'ClientPrimaryAddress' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ClientPhone' as componentID, 'PR' as tablename, 'ClientPhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ClientName' as componentID, 'PR' as tablename, 'ClientName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ClientID' as componentID, 'PR' as tablename, 'ClientID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ClientConfidential' as componentID, 'PR' as tablename, 'ClientConfidential' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ClientCityStateZip' as componentID, 'PR' as tablename, 'ClientCityStateZip' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ClientAlias' as componentID, 'PR' as tablename, 'ClientAlias' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ClientAddress' as componentID, 'PR' as tablename, 'ClientAddress' as fieldname, 'overview' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CLBillingAddr' as componentID, 'PR' as tablename, 'CLBillingAddr' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.CLAddress' as componentID, 'PR' as tablename, 'CLAddress' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.City' as componentID, 'PR' as tablename, 'City' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ChargeType' as componentID, 'PR' as tablename, 'ChargeType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BusinessDeveloperLeadTitle' as componentID, 'PR' as tablename, 'BusinessDeveloperLeadTitle' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BusinessDeveloperLeadPhoneFormat' as componentID, 'PR' as tablename, 'BusinessDeveloperLeadPhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BusinessDeveloperLeadPhone' as componentID, 'PR' as tablename, 'BusinessDeveloperLeadPhone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BusinessDeveloperLeadName' as componentID, 'PR' as tablename, 'BusinessDeveloperLeadName' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BusinessDeveloperLeadLocation' as componentID, 'PR' as tablename, 'BusinessDeveloperLeadLocation' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BusinessDeveloperLeadEmail' as componentID, 'PR' as tablename, 'BusinessDeveloperLeadEmail' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BusinessDeveloperLeadComponent' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BusinessDeveloperLead' as componentID, 'PR' as tablename, 'BusinessDeveloperLead' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BudOHRate' as componentID, 'PR' as tablename, 'BudOHRate' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BudgetSource' as componentID, 'PR' as tablename, 'BudgetSource' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BudgetLevel' as componentID, 'PR' as tablename, 'BudgetLevel' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BudgetedLevels' as componentID, 'PR' as tablename, 'BudgetedLevels' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.LaborCode1' as componentID, 'PR' as tablename, 'LaborCode1' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.LaborCode2' as componentID, 'PR' as tablename, 'LaborCode2' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.LaborCode3' as componentID, 'PR' as tablename, 'LaborCode3' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.LaborCode4' as componentID, 'PR' as tablename, 'LaborCode4' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.LaborCode5' as componentID, 'PR' as tablename, 'LaborCode5' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BudgetedFlag' as componentID, 'PR' as tablename, 'BudgetedFlag' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BudgetedFlagDescription' as componentID, 'PR' as tablename, 'BudgetedFlagDescription' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillWBS3' as componentID, 'PR' as tablename, 'BillWBS3' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillWBS2' as componentID, 'PR' as tablename, 'BillWBS2' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillWBS1' as componentID, 'PR' as tablename, 'BillWBS1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingExchangeRate' as componentID, 'PR' as tablename, 'BillingExchangeRate' as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingCurrencyLabel' as componentID, null as tablename, null as fieldname, 'compensation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingCurrencyCode2' as componentID, null as tablename, null as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingCurrencyCode' as componentID, 'PR' as tablename, 'BillingCurrencyCode' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingContactID' as componentID, 'PR' as tablename, 'BillingContactID' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingClientsPhoneFormat' as componentID, 'PR' as tablename, 'BillingClientsPhoneFormat' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingClientsPhone' as componentID, 'PR' as tablename, 'BillingClientsPhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingClientPhoneFormat' as componentID, 'PR' as tablename, 'BillingClientPhoneFormat' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingClientPhone' as componentID, 'PR' as tablename, 'BillingClientPhone' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingClientName' as componentID, 'PR' as tablename, 'BillingClientName' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingClientMobilePhoneFormat' as componentID, 'PR' as tablename, 'BillingClientMobilePhoneFormat' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingClientMobilePhone' as componentID, 'PR' as tablename, 'BillingClientMobilePhone' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingClientID' as componentID, 'PR' as tablename, 'BillingClientID' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingClientEmail' as componentID, 'PR' as tablename, 'BillingClientEmail' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingClientCountry' as componentID, 'PR' as tablename, 'BillingClientCountry' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingClientContact' as componentID, 'PR' as tablename, 'BillingClientContact' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingClientCityStateZip' as componentID, 'PR' as tablename, 'BillingClientCityStateZip' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingClientAddress' as componentID, 'PR' as tablename, 'BillingClientAddress' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillingClient' as componentID, null as tablename, null as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillerTitle' as componentID, 'PR' as tablename, 'BillerTitle' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillerPhoneFormat' as componentID, 'PR' as tablename, 'BillerPhoneFormat' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillerPhone' as componentID, 'PR' as tablename, 'BillerPhone' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillerName' as componentID, 'PR' as tablename, 'BillerName' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillerEmail' as componentID, 'PR' as tablename, 'BillerEmail' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillerComponent' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Biller' as componentID, 'PR' as tablename, 'Biller' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillByDefaultOtherExp' as componentID, 'PR' as tablename, 'BillByDefaultOtherExp' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillByDefaultORTable' as componentID, 'PR' as tablename, 'BillByDefaultORTable' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillByDefaultConsultants' as componentID, 'PR' as tablename, 'BillByDefaultConsultants' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillByDefault' as componentID, 'PR' as tablename, 'BillByDefault' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillByDefaultDescription' as componentID, 'PR' as tablename, 'BillByDefaultDescription' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillableWarning' as componentID, 'PR' as tablename, 'BillableWarning' as fieldname, 'accounting_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillableWarningDescription' as componentID, 'PR' as tablename, 'BillableWarningDescription' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BidDate' as componentID, 'PR' as tablename, 'BidDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AwardType' as componentID, 'PR' as tablename, 'AwardType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AvailableTo' as componentID, null as tablename, null as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AvailableForCRM' as componentID, 'PR' as tablename, 'AvailableForCRM' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AssociatedContactFirmType' as componentID, 'PR' as tablename, 'AssociatedContactFirmType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AssociatedContactFirmRole' as componentID, 'PR' as tablename, 'AssociatedContactFirmRole' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AllocMethodDescription' as componentID, 'PR' as tablename, 'AllocMethodDescription' as fieldname, 'noassociation' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AllocMethod' as componentID, 'PR' as tablename, 'AllocMethod' as fieldname, 'estimated_fee' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AllocMethodDescription' as componentID, 'PR' as tablename, 'AllocMethodDescription' as fieldname, 'estimated_fee' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AjeraWIPReimbursable' as componentID, 'PR' as tablename, 'AjeraWIPReimbursable' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AjeraWIPLabor' as componentID, 'PR' as tablename, 'AjeraWIPLabor' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AjeraWIPConsultant' as componentID, 'PR' as tablename, 'AjeraWIPConsultant' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AjeraSync' as componentID, 'PR' as tablename, 'AjeraSync' as fieldname, 'noassociation' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AjeraSpentReimbursable' as componentID, 'PR' as tablename, 'AjeraSpentReimbursable' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AjeraSpentLabor' as componentID, 'PR' as tablename, 'AjeraSpentLabor' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AjeraSpentConsultant' as componentID, 'PR' as tablename, 'AjeraSpentConsultant' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AjeraReceivedReimbursable' as componentID, 'PR' as tablename, 'AjeraReceivedReimbursable' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AjeraReceivedLabor' as componentID, 'PR' as tablename, 'AjeraReceivedLabor' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AjeraReceivedConsultant' as componentID, 'PR' as tablename, 'AjeraReceivedConsultant' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AjeraCostReimbursable' as componentID, 'PR' as tablename, 'AjeraCostReimbursable' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AjeraCostLabor' as componentID, 'PR' as tablename, 'AjeraCostLabor' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AjeraCostConsultant' as componentID, 'PR' as tablename, 'AjeraCostConsultant' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AjeraBilledReimbursable' as componentID, 'PR' as tablename, 'AjeraBilledReimbursable' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AjeraBilledLabor' as componentID, 'PR' as tablename, 'AjeraBilledLabor' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AjeraBilledConsultant' as componentID, 'PR' as tablename, 'AjeraBilledConsultant' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AgedAccountsReceivableChart' as componentID, null as tablename, null as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Address3' as componentID, 'PR' as tablename, 'Address3' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Address2' as componentID, 'PR' as tablename, 'Address2' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Address1' as componentID, 'PR' as tablename, 'Address1' as fieldname, 'overview' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ActEstCompletionDate' as componentID, 'PR' as tablename, 'ActEstCompletionDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ActCompletionDate' as componentID, 'PR' as tablename, 'ActCompletionDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'MKTCAMPAIGNPROJECTASSOC' as componentID, null as tablename, null as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'LB' as componentID, null as tablename, null as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'FW_ATTACHMENTS' as componentID, null as tablename, null as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'EB' as componentID, null as tablename, null as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'ContractsPendingAccountReview' as componentID, null as tablename, null as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'CONTRACTS' as componentID, null as tablename, null as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'CONTRACTDETAILS' as componentID, null as tablename, null as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'CLENDORPROJECTASSOC' as componentID, null as tablename, null as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'Activity' as componentID, null as tablename, null as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRUNIT' as GridID, 'WBS1' as componentID, 'PRUNIT' as tablename, 'WBS1' as fieldname, 'units_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRUNIT' as GridID, 'UnitTable' as componentID, 'PRUNIT' as tablename, 'UnitTable' as fieldname, 'units_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRUNIT' as GridID, 'UnitName' as componentID, 'PRUNIT' as tablename, 'UnitName' as fieldname, 'units_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRUNIT' as GridID, 'UnitID' as componentID, 'PRUNIT' as tablename, 'UnitID' as fieldname, 'units_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRUNIT' as GridID, 'Unit' as componentID, 'PRUNIT' as tablename, 'Unit' as fieldname, 'units_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRUNIT' as GridID, 'SeqNo' as componentID, 'PRUNIT' as tablename, 'SeqNo' as fieldname, 'units_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRUNIT' as GridID, 'PlannedQty' as componentID, 'PRUNIT' as tablename, 'PlannedQty' as fieldname, 'units_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRUNIT' as GridID, 'PlannedCost' as componentID, 'PRUNIT' as tablename, 'PlannedCost' as fieldname, 'units_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRUNIT' as GridID, 'PlannedBill' as componentID, 'PRUNIT' as tablename, 'PlannedBill' as fieldname, 'units_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRUNIT' as GridID, 'ModUser' as componentID, 'PRUNIT' as tablename, 'ModUser' as fieldname, 'units_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRUNIT' as GridID, 'ModDate' as componentID, 'PRUNIT' as tablename, 'ModDate' as fieldname, 'units_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRUNIT' as GridID, 'DirectAcctFlg' as componentID, 'PRUNIT' as tablename, 'DirectAcctFlg' as fieldname, 'units_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRUNIT' as GridID, 'CreateUser' as componentID, 'PRUNIT' as tablename, 'CreateUser' as fieldname, 'units_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRUNIT' as GridID, 'CreateDate' as componentID, 'PRUNIT' as tablename, 'CreateDate' as fieldname, 'units_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRUNIT' as GridID, 'CostRate' as componentID, 'PRUNIT' as tablename, 'CostRate' as fieldname, 'units_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRUNIT' as GridID, 'BillingRate' as componentID, 'PRUNIT' as tablename, 'BillingRate' as fieldname, 'units_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRREVENUE' as GridID, 'WBS3' as componentID, 'PRREVENUE' as tablename, 'WBS3' as fieldname, 'estimated_fee' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRREVENUE' as GridID, 'WBS2' as componentID, 'PRREVENUE' as tablename, 'WBS2' as fieldname, 'estimated_fee' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRREVENUE' as GridID, 'WBS1' as componentID, 'PRREVENUE' as tablename, 'WBS1' as fieldname, 'estimated_fee' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRREVENUE' as GridID, 'RevenueDate' as componentID, 'PRREVENUE' as tablename, 'RevenueDate' as fieldname, 'estimated_fee' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRREVENUE' as GridID, 'RevenueAmt' as componentID, 'PRREVENUE' as tablename, 'RevenueAmt' as fieldname, 'estimated_fee' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRREVENUE' as GridID, 'RevAllocID' as componentID, 'PRREVENUE' as tablename, 'RevAllocID' as fieldname, 'estimated_fee' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRREVENUE' as GridID, 'PercentRevenue' as componentID, 'PRREVENUE' as tablename, 'PercentRevenue' as fieldname, 'estimated_fee' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRREVENUE' as GridID, 'ModUser' as componentID, 'PRREVENUE' as tablename, 'ModUser' as fieldname, 'estimated_fee' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRREVENUE' as GridID, 'ModDate' as componentID, 'PRREVENUE' as tablename, 'ModDate' as fieldname, 'estimated_fee' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRREVENUE' as GridID, 'Description' as componentID, 'PRREVENUE' as tablename, 'Description' as fieldname, 'estimated_fee' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRREVENUE' as GridID, 'CreateUser' as componentID, 'PRREVENUE' as tablename, 'CreateUser' as fieldname, 'estimated_fee' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRREVENUE' as GridID, 'CreateDate' as componentID, 'PRREVENUE' as tablename, 'CreateDate' as fieldname, 'estimated_fee' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'WBS1' as componentID, 'CustomProposal' as tablename, 'WBS1' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'Type' as componentID, 'CustomProposal' as tablename, 'Type' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'SubmittalDate' as componentID, 'CustomProposal' as tablename, 'SubmittalDate' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'Status' as componentID, 'CustomProposal' as tablename, 'Status' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'Source' as componentID, 'CustomProposal' as tablename, 'Source' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'ProposalType' as componentID, 'CustomProposal' as tablename, 'ProposalType' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'ProposalManagerNamefl' as componentID, 'CustomProposal' as tablename, 'ProposalManagerNamefl' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'ProposalManagerName' as componentID, 'CustomProposal' as tablename, 'ProposalManagerName' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'ProposalManager' as componentID, 'CustomProposal' as tablename, 'ProposalManager' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'ProjectName' as componentID, 'CustomProposal' as tablename, 'ProjectName' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'OrgName' as componentID, 'CustomProposal' as tablename, 'OrgName' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'Org' as componentID, 'CustomProposal' as tablename, 'Org' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'Number' as componentID, 'CustomProposal' as tablename, 'Number' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'Notes' as componentID, 'CustomProposal' as tablename, 'Notes' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'Name' as componentID, 'CustomProposal' as tablename, 'Name' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'ModUser' as componentID, 'CustomProposal' as tablename, 'ModUser' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'ModDate' as componentID, 'CustomProposal' as tablename, 'ModDate' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'LinkedSF330' as componentID, 'CustomProposal' as tablename, 'LinkedSF330' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'Fee' as componentID, 'CustomProposal' as tablename, 'Fee' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'DueDate' as componentID, 'CustomProposal' as tablename, 'DueDate' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'DateAdvertised' as componentID, 'CustomProposal' as tablename, 'DateAdvertised' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'CustomPropID' as componentID, 'CustomProposal' as tablename, 'CustomPropID' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'CreateUser' as componentID, 'CustomProposal' as tablename, 'CreateUser' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'CreateDate' as componentID, 'CustomProposal' as tablename, 'CreateDate' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'ClientName' as componentID, 'CustomProposal' as tablename, 'ClientName' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'ClientID' as componentID, 'CustomProposal' as tablename, 'ClientID' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROPOSALS' as GridID, 'AwardDate' as componentID, 'CustomProposal' as tablename, 'AwardDate' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROJECTCODES' as GridID, 'WBS3' as componentID, 'PRPROJECTCODES' as tablename, 'WBS3' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROJECTCODES' as GridID, 'WBS2' as componentID, 'PRPROJECTCODES' as tablename, 'WBS2' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROJECTCODES' as GridID, 'WBS1' as componentID, 'PRPROJECTCODES' as tablename, 'WBS1' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROJECTCODES' as GridID, 'Seq' as componentID, 'PRPROJECTCODES' as tablename, 'Seq' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROJECTCODES' as GridID, 'ProjectCodeSF330' as componentID, 'PRPROJECTCODES' as tablename, 'ProjectCodeSF330' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROJECTCODES' as GridID, 'ProjectCode' as componentID, 'PRPROJECTCODES' as tablename, 'ProjectCode' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROJECTCODES' as GridID, 'ModUser' as componentID, 'PRPROJECTCODES' as tablename, 'ModUser' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROJECTCODES' as GridID, 'ModDate' as componentID, 'PRPROJECTCODES' as tablename, 'ModDate' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROJECTCODES' as GridID, 'Fees' as componentID, 'PRPROJECTCODES' as tablename, 'Fees' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROJECTCODES' as GridID, 'Description' as componentID, 'PRPROJECTCODES' as tablename, 'Description' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROJECTCODES' as GridID, 'CreateUser' as componentID, 'PRPROJECTCODES' as tablename, 'CreateUser' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRPROJECTCODES' as GridID, 'CreateDate' as componentID, 'PRPROJECTCODES' as tablename, 'CreateDate' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRMILESTONE' as GridID, 'WBSName' as componentID, 'PRMilestone' as tablename, 'WBSName' as fieldname, 'datesAndCosts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRMILESTONE' as GridID, 'WBSLongName' as componentID, 'PRMilestone' as tablename, 'WBSLongName' as fieldname, 'datesAndCosts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRMILESTONE' as GridID, 'WBS3' as componentID, 'PRMilestone' as tablename, 'WBS3' as fieldname, 'datesAndCosts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRMILESTONE' as GridID, 'WBS2' as componentID, 'PRMilestone' as tablename, 'WBS2' as fieldname, 'datesAndCosts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRMILESTONE' as GridID, 'WBS1' as componentID, 'PRMilestone' as tablename, 'WBS1' as fieldname, 'datesAndCosts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRMILESTONE' as GridID, 'SystemInd' as componentID, 'PRMilestone' as tablename, 'SystemInd' as fieldname, 'datesAndCosts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRMILESTONE' as GridID, 'RecordID' as componentID, 'PRMilestone' as tablename, 'RecordID' as fieldname, 'datesAndCosts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRMILESTONE' as GridID, 'Notes' as componentID, 'PRMilestone' as tablename, 'Notes' as fieldname, 'datesAndCosts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRMILESTONE' as GridID, 'EndDate' as componentID, 'PRMilestone' as tablename, 'EndDate' as fieldname, 'datesAndCosts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRMILESTONE' as GridID, 'Description' as componentID, 'PRMilestone' as tablename, 'Description' as fieldname, 'datesAndCosts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRMILESTONE' as GridID, 'Code' as componentID, 'PRMilestone' as tablename, 'Code' as fieldname, 'datesAndCosts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRLABOR' as GridID, 'WBS1' as componentID, 'PRLABOR' as tablename, 'WBS1' as fieldname, 'labor_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRLABOR' as GridID, 'SeqNo' as componentID, 'PRLABOR' as tablename, 'SeqNo' as fieldname, 'labor_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRLABOR' as GridID, 'Name' as componentID, 'PRLABOR' as tablename, 'Name' as fieldname, 'labor_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRLABOR' as GridID, 'ModUser' as componentID, 'PRLABOR' as tablename, 'ModUser' as fieldname, 'labor_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRLABOR' as GridID, 'ModDate' as componentID, 'PRLABOR' as tablename, 'ModDate' as fieldname, 'labor_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRLABOR' as GridID, 'LaborID' as componentID, 'PRLABOR' as tablename, 'LaborID' as fieldname, 'labor_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRLABOR' as GridID, 'GenericResourceName' as componentID, 'PRLABOR' as tablename, 'GenericResourceName' as fieldname, 'labor_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRLABOR' as GridID, 'GenericResourceID' as componentID, 'PRLABOR' as tablename, 'GenericResourceID' as fieldname, 'labor_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRLABOR' as GridID, 'EstimateHrs' as componentID, 'PRLABOR' as tablename, 'EstimateHrs' as fieldname, 'labor_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRLABOR' as GridID, 'EstimateCost' as componentID, 'PRLABOR' as tablename, 'EstimateCost' as fieldname, 'labor_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRLABOR' as GridID, 'EstimateBill' as componentID, 'PRLABOR' as tablename, 'EstimateBill' as fieldname, 'labor_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRLABOR' as GridID, 'CreateUser' as componentID, 'PRLABOR' as tablename, 'CreateUser' as fieldname, 'labor_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRLABOR' as GridID, 'CreateDate' as componentID, 'PRLABOR' as tablename, 'CreateDate' as fieldname, 'labor_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRLABOR' as GridID, 'CostRate' as componentID, 'PRLABOR' as tablename, 'CostRate' as fieldname, 'labor_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRLABOR' as GridID, 'Category' as componentID, 'PRLABOR' as tablename, 'Category' as fieldname, 'labor_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRLABOR' as GridID, 'BillingRate' as componentID, 'PRLABOR' as tablename, 'BillingRate' as fieldname, 'labor_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICESUMMARY' as GridID, 'Reject' as componentID, 'PRINVOICESUMMARY' as tablename, 'Reject' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICESUMMARY' as GridID, 'InvoiceTotal' as componentID, 'PRINVOICESUMMARY' as tablename, 'InvoiceTotal' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICESUMMARY' as GridID, 'InvoiceNumber' as componentID, 'PRINVOICESUMMARY' as tablename, 'InvoiceNumber' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICESUMMARY' as GridID, 'InvoiceDate' as componentID, 'PRINVOICESUMMARY' as tablename, 'InvoiceDate' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICESUMMARY' as GridID, 'HasInvoice' as componentID, 'PRINVOICESUMMARY' as tablename, 'HasInvoice' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICESUMMARY' as GridID, 'HasComment' as componentID, 'PRINVOICESUMMARY' as tablename, 'HasComment' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICESUMMARY' as GridID, 'Approve' as componentID, 'PRINVOICESUMMARY' as tablename, 'Approve' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'WBS1' as componentID, 'PRINVOICE' as tablename, 'WBS1' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'ViewFlg' as componentID, 'PRINVOICE' as tablename, 'ViewFlg' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'TotalARAmt' as componentID, 'PRINVOICE' as tablename, 'TotalARAmt' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'TaxAmt' as componentID, 'PRINVOICE' as tablename, 'TaxAmt' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'RetainerAmt' as componentID, 'PRINVOICE' as tablename, 'RetainerAmt' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'RetainageAmt' as componentID, 'PRINVOICE' as tablename, 'RetainageAmt' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'PaidAmt' as componentID, 'PRINVOICE' as tablename, 'PaidAmt' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'LinkCompany' as componentID, 'PRINVOICE' as tablename, 'LinkCompany' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'InvoiceNumber' as componentID, 'PRINVOICE' as tablename, 'InvoiceNumber' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'InvoiceDate' as componentID, 'PRINVOICE' as tablename, 'InvoiceDate' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'InvoiceAmt' as componentID, 'PRINVOICE' as tablename, 'InvoiceAmt' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'InvHasCreditMemo' as componentID, 'PRINVOICE' as tablename, 'InvHasCreditMemo' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'InterestAmt' as componentID, 'PRINVOICE' as tablename, 'InterestAmt' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'ICCompany' as componentID, 'PRINVOICE' as tablename, 'ICCompany' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'ICBillInvoice' as componentID, 'PRINVOICE' as tablename, 'ICBillInvoice' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'Edited' as componentID, 'PRINVOICE' as tablename, 'Edited' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'DueAmt' as componentID, 'PRINVOICE' as tablename, 'DueAmt' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'DetailsFlg' as componentID, 'PRINVOICE' as tablename, 'DetailsFlg' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'DaysOutstanding' as componentID, 'PRINVOICE' as tablename, 'DaysOutstanding' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'CreditMemoRefNoTransDate' as componentID, 'PRINVOICE' as tablename, 'CreditMemoRefNoTransDate' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'CreditMemoRefNo' as componentID, 'PRINVOICE' as tablename, 'CreditMemoRefNo' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'CreditMemoAmt' as componentID, 'PRINVOICE' as tablename, 'CreditMemoAmt' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRINVOICE' as GridID, 'CommentFlg' as componentID, 'PRINVOICE' as tablename, 'CommentFlg' as fieldname, 'invoices' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRFILELINKS' as GridID, 'WBS3' as componentID, 'PRFileLinks' as tablename, 'WBS3' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRFILELINKS' as GridID, 'WBS2' as componentID, 'PRFileLinks' as tablename, 'WBS2' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRFILELINKS' as GridID, 'WBS1' as componentID, 'PRFileLinks' as tablename, 'WBS1' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRFILELINKS' as GridID, 'LinkID' as componentID, 'PRFileLinks' as tablename, 'LinkID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRFILELINKS' as GridID, 'Graphic' as componentID, 'PRFileLinks' as tablename, 'Graphic' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRFILELINKS' as GridID, 'FilePath' as componentID, 'PRFileLinks' as tablename, 'FilePath' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRFILELINKS' as GridID, 'Description' as componentID, 'PRFileLinks' as tablename, 'Description' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREXPENSE' as GridID, 'WBS1' as componentID, 'PREXPENSE' as tablename, 'WBS1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREXPENSE' as GridID, 'SeqNo' as componentID, 'PREXPENSE' as tablename, 'SeqNo' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREXPENSE' as GridID, 'OppExpCost' as componentID, 'PREXPENSE' as tablename, 'OppExpCost' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREXPENSE' as GridID, 'OppExpBill' as componentID, 'PREXPENSE' as tablename, 'OppExpBill' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREXPENSE' as GridID, 'ModUser' as componentID, 'PREXPENSE' as tablename, 'ModUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREXPENSE' as GridID, 'ModDate' as componentID, 'PREXPENSE' as tablename, 'ModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREXPENSE' as GridID, 'ExpenseID' as componentID, 'PREXPENSE' as tablename, 'ExpenseID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREXPENSE' as GridID, 'DirectAcctFlg' as componentID, 'PREXPENSE' as tablename, 'DirectAcctFlg' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREXPENSE' as GridID, 'CreateUser' as componentID, 'PREXPENSE' as tablename, 'CreateUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREXPENSE' as GridID, 'CreateDate' as componentID, 'PREXPENSE' as tablename, 'CreateDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREXPENSE' as GridID, 'Billable' as componentID, 'PREXPENSE' as tablename, 'Billable' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREXPENSE' as GridID, 'AccountName' as componentID, 'PREXPENSE' as tablename, 'AccountName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREXPENSE' as GridID, 'Account' as componentID, 'PREXPENSE' as tablename, 'Account' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATESUMMARY' as GridID, 'WBS1' as componentID, 'PRESTIMATESUMMARY' as tablename, 'WBS1' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATESUMMARY' as GridID, 'RowType' as componentID, 'PRESTIMATESUMMARY' as tablename, 'RowType' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATESUMMARY' as GridID, 'PlannedHours' as componentID, 'PRESTIMATESUMMARY' as tablename, 'PlannedHours' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATESUMMARY' as GridID, 'PlannedCostReimbursable' as componentID, 'PRESTIMATESUMMARY' as tablename, 'PlannedCostReimbursable' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATESUMMARY' as GridID, 'PlannedCostDirect' as componentID, 'PRESTIMATESUMMARY' as tablename, 'PlannedCostDirect' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATESUMMARY' as GridID, 'PlannedCost' as componentID, 'PRESTIMATESUMMARY' as tablename, 'PlannedCost' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATESUMMARY' as GridID, 'PlannedBillReimbursable' as componentID, 'PRESTIMATESUMMARY' as tablename, 'PlannedBillReimbursable' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATESUMMARY' as GridID, 'PlannedBillDirect' as componentID, 'PRESTIMATESUMMARY' as tablename, 'PlannedBillDirect' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATESUMMARY' as GridID, 'PlannedBill' as componentID, 'PRESTIMATESUMMARY' as tablename, 'PlannedBill' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATESUMMARY' as GridID, 'Description' as componentID, 'PRESTIMATESUMMARY' as tablename, 'Description' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATESUMMARY' as GridID, 'CostRate' as componentID, 'PRESTIMATESUMMARY' as tablename, 'CostRate' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATESUMMARY' as GridID, 'BillingRate' as componentID, 'PRESTIMATESUMMARY' as tablename, 'BillingRate' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATEANALYSIS' as GridID, 'WBS1' as componentID, 'PRESTIMATEANALYSIS' as tablename, 'WBS1' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATEANALYSIS' as GridID, 'TotalCost' as componentID, 'PRESTIMATEANALYSIS' as tablename, 'TotalCost' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATEANALYSIS' as GridID, 'TotalBill' as componentID, 'PRESTIMATEANALYSIS' as tablename, 'TotalBill' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATEANALYSIS' as GridID, 'GrossMarginPercent' as componentID, 'PRESTIMATEANALYSIS' as tablename, 'GrossMarginPercent' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATEANALYSIS' as GridID, 'GrossMargin' as componentID, 'PRESTIMATEANALYSIS' as tablename, 'GrossMargin' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRESTIMATEANALYSIS' as GridID, 'Description' as componentID, 'PRESTIMATEANALYSIS' as tablename, 'Description' as fieldname, 'summary_estimates' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'Zip' as componentID, 'PREMP' as tablename, 'Zip' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'WBS3' as componentID, 'PREMP' as tablename, 'WBS3' as fieldname, 'team_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'WBS2' as componentID, 'PREMP' as tablename, 'WBS2' as fieldname, 'team_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'WBS1' as componentID, 'PREMP' as tablename, 'WBS1' as fieldname, 'team_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'Title' as componentID, 'PREMP' as tablename, 'Title' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'TeamStatus' as componentID, 'PREMP' as tablename, 'TeamStatus' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'State' as componentID, 'PREMP' as tablename, 'State' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'StartDate' as componentID, 'PREMP' as tablename, 'StartDate' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'RoleDescription' as componentID, 'PREMP' as tablename, 'RoleDescription' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'Role' as componentID, 'PREMP' as tablename, 'Role' as fieldname, 'team_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'RecordType' as componentID, 'PREMP' as tablename, 'RecordType' as fieldname, 'team_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'RecordID' as componentID, 'PREMP' as tablename, 'RecordID' as fieldname, 'team_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'PrimaryInd' as componentID, 'PREMP' as tablename, 'PrimaryInd' as fieldname, 'team_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'Phone' as componentID, 'PREMP' as tablename, 'Phone' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'ModUser' as componentID, 'PREMP' as tablename, 'ModUser' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'ModDate' as componentID, 'PREMP' as tablename, 'ModDate' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'lfName' as componentID, 'PREMP' as tablename, 'lfName' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'flName' as componentID, 'PREMP' as tablename, 'flName' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'FirmName' as componentID, 'PREMP' as tablename, 'FirmName' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'FirmID' as componentID, 'PREMP' as tablename, 'FirmID' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'EndDate' as componentID, 'PREMP' as tablename, 'EndDate' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'Employee' as componentID, 'PREMP' as tablename, 'Employee' as fieldname, 'team_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'email' as componentID, 'PREMP' as tablename, 'email' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'ctName' as componentID, 'PREMP' as tablename, 'ctName' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'CRMHours' as componentID, 'PREMP' as tablename, 'CRMHours' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'CreateUser' as componentID, 'PREMP' as tablename, 'CreateUser' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'CreateDate' as componentID, 'PREMP' as tablename, 'CreateDate' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'City' as componentID, 'PREMP' as tablename, 'City' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'CFGRoleDescription' as componentID, 'PREMP' as tablename, 'CFGRoleDescription' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'AjeraSync' as componentID, 'PREMP' as tablename, 'AjeraSync' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PREMP' as GridID, 'Address1' as componentID, 'PREMP' as tablename, 'Address1' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRDESCRIPTIONS' as GridID, 'WBS3' as componentID, 'PRDESCRIPTIONS' as tablename, 'WBS3' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRDESCRIPTIONS' as GridID, 'WBS2' as componentID, 'PRDESCRIPTIONS' as tablename, 'WBS2' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRDESCRIPTIONS' as GridID, 'WBS1' as componentID, 'PRDESCRIPTIONS' as tablename, 'WBS1' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRDESCRIPTIONS' as GridID, 'ModUser' as componentID, 'PRDESCRIPTIONS' as tablename, 'ModUser' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRDESCRIPTIONS' as GridID, 'ModDate' as componentID, 'PRDESCRIPTIONS' as tablename, 'ModDate' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRDESCRIPTIONS' as GridID, 'Description' as componentID, 'PRDESCRIPTIONS' as tablename, 'Description' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRDESCRIPTIONS' as GridID, 'DescCategoryDescription' as componentID, 'PRDESCRIPTIONS' as tablename, 'DescCategoryDescription' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRDESCRIPTIONS' as GridID, 'DescCategory' as componentID, 'PRDESCRIPTIONS' as tablename, 'DescCategory' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRDESCRIPTIONS' as GridID, 'DefaultInd' as componentID, 'PRDESCRIPTIONS' as tablename, 'DefaultInd' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRDESCRIPTIONS' as GridID, 'CreateUser' as componentID, 'PRDESCRIPTIONS' as tablename, 'CreateUser' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRDESCRIPTIONS' as GridID, 'CreateDate' as componentID, 'PRDESCRIPTIONS' as tablename, 'CreateDate' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'WBS1' as componentID, 'PRCONTACTASSOC' as tablename, 'WBS1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'TeamStatus' as componentID, 'PRCONTACTASSOC' as tablename, 'TeamStatus' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'RoleDescription' as componentID, 'PRCONTACTASSOC' as tablename, 'RoleDescription' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'Role' as componentID, 'PRCONTACTASSOC' as tablename, 'Role' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'PrimaryInd' as componentID, 'PRCONTACTASSOC' as tablename, 'PrimaryInd' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ModUser' as componentID, 'PRCONTACTASSOC' as tablename, 'ModUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ModDate' as componentID, 'PRCONTACTASSOC' as tablename, 'ModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'CreateUser' as componentID, 'PRCONTACTASSOC' as tablename, 'CreateUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'CreateDate' as componentID, 'PRCONTACTASSOC' as tablename, 'CreateDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ContactTitle' as componentID, 'PRCONTACTASSOC' as tablename, 'ContactTitle' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ContactSuffix' as componentID, 'PRCONTACTASSOC' as tablename, 'ContactSuffix' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ContactSalutation' as componentID, 'PRCONTACTASSOC' as tablename, 'ContactSalutation' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ContactPager' as componentID, 'PRCONTACTASSOC' as tablename, 'ContactPager' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ContactName' as componentID, 'PRCONTACTASSOC' as tablename, 'ContactName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ContactLastName' as componentID, 'PRCONTACTASSOC' as tablename, 'ContactLastName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ContactID' as componentID, 'PRCONTACTASSOC' as tablename, 'ContactID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ContactHomePhoneFormat' as componentID, 'PRCONTACTASSOC' as tablename, 'ContactHomePhoneFormat' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ContactHomePhone' as componentID, 'PRCONTACTASSOC' as tablename, 'ContactHomePhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ContactFirstName' as componentID, 'PRCONTACTASSOC' as tablename, 'ContactFirstName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ContactFax' as componentID, 'PRCONTACTASSOC' as tablename, 'ContactFax' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ContactEmail' as componentID, 'PRCONTACTASSOC' as tablename, 'ContactEmail' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ContactCellPhoneFormat' as componentID, 'PRCONTACTASSOC' as tablename, 'ContactCellPhoneFormat' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ContactCellPhone' as componentID, 'PRCONTACTASSOC' as tablename, 'ContactCellPhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ContactBusPhoneFormat' as componentID, 'PRCONTACTASSOC' as tablename, 'ContactBusPhoneFormat' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'ContactBusPhone' as componentID, 'PRCONTACTASSOC' as tablename, 'ContactBusPhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONTACTASSOC' as GridID, 'AjeraSync' as componentID, 'PRCONTACTASSOC' as tablename, 'AjeraSync' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONSULTANT' as GridID, 'WBS1' as componentID, 'PRCONSULTANT' as tablename, 'WBS1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONSULTANT' as GridID, 'SeqNo' as componentID, 'PRCONSULTANT' as tablename, 'SeqNo' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONSULTANT' as GridID, 'OppConCost' as componentID, 'PRCONSULTANT' as tablename, 'OppConCost' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONSULTANT' as GridID, 'OppConBill' as componentID, 'PRCONSULTANT' as tablename, 'OppConBill' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONSULTANT' as GridID, 'ModUser' as componentID, 'PRCONSULTANT' as tablename, 'ModUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONSULTANT' as GridID, 'ModDate' as componentID, 'PRCONSULTANT' as tablename, 'ModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONSULTANT' as GridID, 'DirectAcctFlg' as componentID, 'PRCONSULTANT' as tablename, 'DirectAcctFlg' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONSULTANT' as GridID, 'CreateUser' as componentID, 'PRCONSULTANT' as tablename, 'CreateUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONSULTANT' as GridID, 'CreateDate' as componentID, 'PRCONSULTANT' as tablename, 'CreateDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONSULTANT' as GridID, 'ConsultantID' as componentID, 'PRCONSULTANT' as tablename, 'ConsultantID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONSULTANT' as GridID, 'Billable' as componentID, 'PRCONSULTANT' as tablename, 'Billable' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONSULTANT' as GridID, 'AccountName' as componentID, 'PRCONSULTANT' as tablename, 'AccountName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCONSULTANT' as GridID, 'Account' as componentID, 'PRCONSULTANT' as tablename, 'Account' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCOMPETITION' as GridID, 'Weakness' as componentID, 'PRCompetitionAssoc' as tablename, 'Weakness' as fieldname, 'competition' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCOMPETITION' as GridID, 'WBS3' as componentID, 'PRCompetitionAssoc' as tablename, 'WBS3' as fieldname, 'competition' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCOMPETITION' as GridID, 'WBS2' as componentID, 'PRCompetitionAssoc' as tablename, 'WBS2' as fieldname, 'competition' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCOMPETITION' as GridID, 'WBS1' as componentID, 'PRCompetitionAssoc' as tablename, 'WBS1' as fieldname, 'competition' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCOMPETITION' as GridID, 'Strengths' as componentID, 'PRCompetitionAssoc' as tablename, 'Strengths' as fieldname, 'competition' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCOMPETITION' as GridID, 'RecordID' as componentID, 'PRCompetitionAssoc' as tablename, 'RecordID' as fieldname, 'competition' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCOMPETITION' as GridID, 'Notes' as componentID, 'PRCompetitionAssoc' as tablename, 'Notes' as fieldname, 'competition' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCOMPETITION' as GridID, 'Incumbent' as componentID, 'PRCompetitionAssoc' as tablename, 'Incumbent' as fieldname, 'competition' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCOMPETITION' as GridID, 'clName' as componentID, 'PRCompetitionAssoc' as tablename, 'clName' as fieldname, 'competition' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCOMPETITION' as GridID, 'ClientID' as componentID, 'PRCompetitionAssoc' as tablename, 'ClientID' as fieldname, 'competition' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCHARGECOMPANIES' as GridID, 'WBS3' as componentID, 'PRCHARGECOMPANIES' as tablename, 'WBS3' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCHARGECOMPANIES' as GridID, 'WBS2' as componentID, 'PRCHARGECOMPANIES' as tablename, 'WBS2' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCHARGECOMPANIES' as GridID, 'WBS1' as componentID, 'PRCHARGECOMPANIES' as tablename, 'WBS1' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCHARGECOMPANIES' as GridID, 'FirmName' as componentID, 'PRCHARGECOMPANIES' as tablename, 'FirmName' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRCHARGECOMPANIES' as GridID, 'Company' as componentID, 'PRCHARGECOMPANIES' as tablename, 'Company' as fieldname, 'accounting_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRAWARDS' as GridID, 'WBS3' as componentID, 'PRAWARDS' as tablename, 'WBS3' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRAWARDS' as GridID, 'WBS2' as componentID, 'PRAWARDS' as tablename, 'WBS2' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRAWARDS' as GridID, 'WBS1' as componentID, 'PRAWARDS' as tablename, 'WBS1' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRAWARDS' as GridID, 'RecordID' as componentID, 'PRAWARDS' as tablename, 'RecordID' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRAWARDS' as GridID, 'ModUser' as componentID, 'PRAWARDS' as tablename, 'ModUser' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRAWARDS' as GridID, 'ModDate' as componentID, 'PRAWARDS' as tablename, 'ModDate' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRAWARDS' as GridID, 'Institution' as componentID, 'PRAWARDS' as tablename, 'Institution' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRAWARDS' as GridID, 'Description' as componentID, 'PRAWARDS' as tablename, 'Description' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRAWARDS' as GridID, 'CreateUser' as componentID, 'PRAWARDS' as tablename, 'CreateUser' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRAWARDS' as GridID, 'CreateDate' as componentID, 'PRAWARDS' as tablename, 'CreateDate' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRAWARDS' as GridID, 'AwardDate' as componentID, 'PRAWARDS' as tablename, 'AwardDate' as fieldname, 'proposals' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRARAGING' as GridID, 'TotalARAmt' as componentID, 'PRARAGING' as tablename, 'TotalARAmt' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRARAGING' as GridID, 'RetainageAmt' as componentID, 'PRARAGING' as tablename, 'RetainageAmt' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRARAGING' as GridID, 'ReceivedAmt' as componentID, 'PRARAGING' as tablename, 'ReceivedAmt' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRARAGING' as GridID, 'BilledAmt' as componentID, 'PRARAGING' as tablename, 'BilledAmt' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRARAGING' as GridID, 'AROver90' as componentID, 'PRARAGING' as tablename, 'AROver90' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRARAGING' as GridID, 'AROver60' as componentID, 'PRARAGING' as tablename, 'AROver60' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRARAGING' as GridID, 'AROver45' as componentID, 'PRARAGING' as tablename, 'AROver45' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRARAGING' as GridID, 'AROver30' as componentID, 'PRARAGING' as tablename, 'AROver30' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PRARAGING' as GridID, 'AROver120' as componentID, 'PRARAGING' as tablename, 'AROver120' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, '_rowType' as componentID, 'PNTASK' as tablename, '_rowType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'WBSType' as componentID, 'PNTASK' as tablename, 'WBSType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'WBS3' as componentID, 'PNTASK' as tablename, 'WBS3' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'WBS2' as componentID, 'PNTASK' as tablename, 'WBS2' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'WBS1' as componentID, 'PNTASK' as tablename, 'WBS1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'TaskID' as componentID, 'PNTASK' as tablename, 'TaskID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'Status' as componentID, 'PNTASK' as tablename, 'Status' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'StartDate' as componentID, 'PNTASK' as tablename, 'StartDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'ScheduledStart' as componentID, 'PNTASK' as tablename, 'ScheduledStart' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'ScheduledFinish' as componentID, 'PNTASK' as tablename, 'ScheduledFinish' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'ProjMgr' as componentID, 'PNTASK' as tablename, 'ProjMgr' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'ProjectType' as componentID, 'PNTASK' as tablename, 'ProjectType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'PlanID' as componentID, 'PNTASK' as tablename, 'PlanID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'ParentOutlineNumber' as componentID, 'PNTASK' as tablename, 'ParentOutlineNumber' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'OutlineNumber' as componentID, 'PNTASK' as tablename, 'OutlineNumber' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'OutlineLevel' as componentID, 'PNTASK' as tablename, 'OutlineLevel' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'Org' as componentID, 'PNTASK' as tablename, 'Org' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'Name' as componentID, 'PNTASK' as tablename, 'Name' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'ModUser' as componentID, 'PNTASK' as tablename, 'ModUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'ModDate' as componentID, 'PNTASK' as tablename, 'ModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'LateStart' as componentID, 'PNTASK' as tablename, 'LateStart' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'LateFinish' as componentID, 'PNTASK' as tablename, 'LateFinish' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'LaborCode' as componentID, 'PNTASK' as tablename, 'LaborCode' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'EndDate' as componentID, 'PNTASK' as tablename, 'EndDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'EarlyStart' as componentID, 'PNTASK' as tablename, 'EarlyStart' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'EarlyFinish' as componentID, 'PNTASK' as tablename, 'EarlyFinish' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'CreateUser' as componentID, 'PNTASK' as tablename, 'CreateUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'CreateDate' as componentID, 'PNTASK' as tablename, 'CreateDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'ClientID' as componentID, 'PNTASK' as tablename, 'ClientID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'ChargeType' as componentID, 'PNTASK' as tablename, 'ChargeType' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'BaselineStart' as componentID, 'PNTASK' as tablename, 'BaselineStart' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'BaselineFinish' as componentID, 'PNTASK' as tablename, 'BaselineFinish' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'ActualStart' as componentID, 'PNTASK' as tablename, 'ActualStart' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'PNTASK' as GridID, 'ActualFinish' as componentID, 'PNTASK' as tablename, 'ActualFinish' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'MKTCAMPAIGNPROJECTASSOC' as GridID, 'WBS3' as componentID, 'MKTCAMPAIGNPROJECTASSOC' as tablename, 'WBS3' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'MKTCAMPAIGNPROJECTASSOC' as GridID, 'WBS2' as componentID, 'MKTCAMPAIGNPROJECTASSOC' as tablename, 'WBS2' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'MKTCAMPAIGNPROJECTASSOC' as GridID, 'WBS1' as componentID, 'MKTCAMPAIGNPROJECTASSOC' as tablename, 'WBS1' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'MKTCAMPAIGNPROJECTASSOC' as GridID, 'TypeDescription' as componentID, 'MKTCAMPAIGNPROJECTASSOC' as tablename, 'TypeDescription' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'MKTCAMPAIGNPROJECTASSOC' as GridID, 'Type' as componentID, 'MKTCAMPAIGNPROJECTASSOC' as tablename, 'Type' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'MKTCAMPAIGNPROJECTASSOC' as GridID, 'Status' as componentID, 'MKTCAMPAIGNPROJECTASSOC' as tablename, 'Status' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'MKTCAMPAIGNPROJECTASSOC' as GridID, 'RecordStatus' as componentID, 'MKTCAMPAIGNPROJECTASSOC' as tablename, 'RecordStatus' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'MKTCAMPAIGNPROJECTASSOC' as GridID, 'ModUser' as componentID, 'MKTCAMPAIGNPROJECTASSOC' as tablename, 'ModUser' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'MKTCAMPAIGNPROJECTASSOC' as GridID, 'ModDate' as componentID, 'MKTCAMPAIGNPROJECTASSOC' as tablename, 'ModDate' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'MKTCAMPAIGNPROJECTASSOC' as GridID, 'CreateUser' as componentID, 'MKTCAMPAIGNPROJECTASSOC' as tablename, 'CreateUser' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'MKTCAMPAIGNPROJECTASSOC' as GridID, 'CreateDate' as componentID, 'MKTCAMPAIGNPROJECTASSOC' as tablename, 'CreateDate' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'MKTCAMPAIGNPROJECTASSOC' as GridID, 'CampaignName' as componentID, 'MKTCAMPAIGNPROJECTASSOC' as tablename, 'CampaignName' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'MKTCAMPAIGNPROJECTASSOC' as GridID, 'CampaignMgrName' as componentID, 'MKTCAMPAIGNPROJECTASSOC' as tablename, 'CampaignMgrName' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'MKTCAMPAIGNPROJECTASSOC' as GridID, 'CampaignMgr' as componentID, 'MKTCAMPAIGNPROJECTASSOC' as tablename, 'CampaignMgr' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'MKTCAMPAIGNPROJECTASSOC' as GridID, 'CampaignID' as componentID, 'MKTCAMPAIGNPROJECTASSOC' as tablename, 'CampaignID' as fieldname, 'campaigns' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'WBS3' as componentID, 'LB' as tablename, 'WBS3' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'WBS2' as componentID, 'LB' as tablename, 'WBS2' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'WBS1' as componentID, 'LB' as tablename, 'WBS1' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'StartDate' as componentID, 'LB' as tablename, 'StartDate' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'Rate' as componentID, 'LB' as tablename, 'Rate' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'Pct' as componentID, 'LB' as tablename, 'Pct' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'LaborCodeName' as componentID, 'LB' as tablename, 'LaborCodeName' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'LaborCode' as componentID, 'LB' as tablename, 'LaborCode' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'JTDSpentCost' as componentID, 'LB' as tablename, 'JTDSpentCost' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'JTDSpentBill' as componentID, 'LB' as tablename, 'JTDSpentBill' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'JTDHours' as componentID, 'LB' as tablename, 'JTDHours' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'JTDAdded' as componentID, 'LB' as tablename, 'JTDAdded' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'HrsBud' as componentID, 'LB' as tablename, 'HrsBud' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'ETCHrs' as componentID, 'LB' as tablename, 'ETCHrs' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'ETCAmt' as componentID, 'LB' as tablename, 'ETCAmt' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'EndDate' as componentID, 'LB' as tablename, 'EndDate' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'EACHrs' as componentID, 'LB' as tablename, 'EACHrs' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'EACAmt' as componentID, 'LB' as tablename, 'EACAmt' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'BudgetedLessJTDCost' as componentID, 'LB' as tablename, 'BudgetedLessJTDCost' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'BudgetedLessJTDBill' as componentID, 'LB' as tablename, 'BudgetedLessJTDBill' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'BillRate' as componentID, 'LB' as tablename, 'BillRate' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'BillETCAmt' as componentID, 'LB' as tablename, 'BillETCAmt' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'BillEACAmt' as componentID, 'LB' as tablename, 'BillEACAmt' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'BillBud' as componentID, 'LB' as tablename, 'BillBud' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'AmtBud' as componentID, 'LB' as tablename, 'AmtBud' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'LB' as GridID, 'Actual' as componentID, 'LB' as tablename, 'Actual' as fieldname, 'laborTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'WBS3' as componentID, 'FW_ATTACHMENTS' as tablename, 'WBS3' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'WBS2' as componentID, 'FW_ATTACHMENTS' as tablename, 'WBS2' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'WBS1' as componentID, 'FW_ATTACHMENTS' as tablename, 'WBS1' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'PKey' as componentID, 'FW_ATTACHMENTS' as tablename, 'PKey' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key3' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key3' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key2' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key2' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key1' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key1' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileSize' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileSize' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileName' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileName' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileID' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileDescription' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileDescription' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'ContentType' as componentID, 'FW_ATTACHMENTS' as tablename, 'ContentType' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'CategoryDescription' as componentID, 'FW_ATTACHMENTS' as tablename, 'CategoryDescription' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'CategoryCode' as componentID, 'FW_ATTACHMENTS' as tablename, 'CategoryCode' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Application' as componentID, 'FW_ATTACHMENTS' as tablename, 'Application' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'WBS3' as componentID, 'EMPROJECTASSOC' as tablename, 'WBS3' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'WBS2' as componentID, 'EMPROJECTASSOC' as tablename, 'WBS2' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'WBS1' as componentID, 'EMPROJECTASSOC' as tablename, 'WBS1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'TeamStatus' as componentID, 'EMPROJECTASSOC' as tablename, 'TeamStatus' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'StartDate' as componentID, 'EMPROJECTASSOC' as tablename, 'StartDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'RoleDescription' as componentID, 'EMPROJECTASSOC' as tablename, 'RoleDescription' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'Role' as componentID, 'EMPROJECTASSOC' as tablename, 'Role' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'RecordID' as componentID, 'EMPROJECTASSOC' as tablename, 'RecordID' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'ModUser' as componentID, 'EMPROJECTASSOC' as tablename, 'ModUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'ModDate' as componentID, 'EMPROJECTASSOC' as tablename, 'ModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'EndDate' as componentID, 'EMPROJECTASSOC' as tablename, 'EndDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'EmployeeTitle' as componentID, 'EMPROJECTASSOC' as tablename, 'EmployeeTitle' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'EmployeeSuffix' as componentID, 'EMPROJECTASSOC' as tablename, 'EmployeeSuffix' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'EmployeeSalutation' as componentID, 'EMPROJECTASSOC' as tablename, 'EmployeeSalutation' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'EmployeeName' as componentID, 'EMPROJECTASSOC' as tablename, 'EmployeeName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'EmployeeLastName' as componentID, 'EMPROJECTASSOC' as tablename, 'EmployeeLastName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'EmployeeFirstName' as componentID, 'EMPROJECTASSOC' as tablename, 'EmployeeFirstName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'EmployeeFax' as componentID, 'EMPROJECTASSOC' as tablename, 'EmployeeFax' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'EmployeeEmail' as componentID, 'EMPROJECTASSOC' as tablename, 'EmployeeEmail' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'EmployeeCellPhoneFormat' as componentID, 'EMPROJECTASSOC' as tablename, 'EmployeeCellPhoneFormat' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'EmployeeCellPhone' as componentID, 'EMPROJECTASSOC' as tablename, 'EmployeeCellPhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'EmployeeBusPhoneFormat' as componentID, 'EMPROJECTASSOC' as tablename, 'EmployeeBusPhoneFormat' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'EmployeeBusPhone' as componentID, 'EMPROJECTASSOC' as tablename, 'EmployeeBusPhone' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'Employee' as componentID, 'EMPROJECTASSOC' as tablename, 'Employee' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'CRMHours' as componentID, 'EMPROJECTASSOC' as tablename, 'CRMHours' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'CreateUser' as componentID, 'EMPROJECTASSOC' as tablename, 'CreateUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EMPROJECTASSOC' as GridID, 'CreateDate' as componentID, 'EMPROJECTASSOC' as tablename, 'CreateDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'WBS3' as componentID, 'EB' as tablename, 'WBS3' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'WBS2' as componentID, 'EB' as tablename, 'WBS2' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'WBS1' as componentID, 'EB' as tablename, 'WBS1' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'VendorName' as componentID, 'EB' as tablename, 'VendorName' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'Vendor' as componentID, 'EB' as tablename, 'Vendor' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'StartDate' as componentID, 'EB' as tablename, 'StartDate' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'Pct' as componentID, 'EB' as tablename, 'Pct' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'JTDSpentCost' as componentID, 'EB' as tablename, 'JTDSpentCost' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'JTDSpentBill' as componentID, 'EB' as tablename, 'JTDSpentBill' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'JTDAdded' as componentID, 'EB' as tablename, 'JTDAdded' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'ETCAmt' as componentID, 'EB' as tablename, 'ETCAmt' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'EndDate' as componentID, 'EB' as tablename, 'EndDate' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'EACAmt' as componentID, 'EB' as tablename, 'EACAmt' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'BudgetedLessJTDCost' as componentID, 'EB' as tablename, 'BudgetedLessJTDCost' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'BudgetedLessJTDBill' as componentID, 'EB' as tablename, 'BudgetedLessJTDBill' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'BillETCAmt' as componentID, 'EB' as tablename, 'BillETCAmt' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'BillEACAmt' as componentID, 'EB' as tablename, 'BillEACAmt' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'BillBud' as componentID, 'EB' as tablename, 'BillBud' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'AmtBud' as componentID, 'EB' as tablename, 'AmtBud' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'Actual' as componentID, 'EB' as tablename, 'Actual' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'AccountType' as componentID, 'EB' as tablename, 'AccountType' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'AccountName' as componentID, 'EB' as tablename, 'AccountName' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'EB' as GridID, 'Account' as componentID, 'EB' as tablename, 'Account' as fieldname, 'expenseTab' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'WBS1' as componentID, 'CONTRACTS' as tablename, 'WBS1' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'TotalFunctionalCurrency' as componentID, 'CONTRACTS' as tablename, 'TotalFunctionalCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'TotalBillingCurrency' as componentID, 'CONTRACTS' as tablename, 'TotalBillingCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'Total' as componentID, 'CONTRACTS' as tablename, 'Total' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'RequestDate' as componentID, 'CONTRACTS' as tablename, 'RequestDate' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ReimbAllowFunctionalCurrency' as componentID, 'CONTRACTS' as tablename, 'ReimbAllowFunctionalCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ReimbAllowExpFunctionalCurrency' as componentID, 'CONTRACTS' as tablename, 'ReimbAllowExpFunctionalCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ReimbAllowExpBillingCurrency' as componentID, 'CONTRACTS' as tablename, 'ReimbAllowExpBillingCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ReimbAllowExp' as componentID, 'CONTRACTS' as tablename, 'ReimbAllowExp' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ReimbAllowConsFunctionalCurrency' as componentID, 'CONTRACTS' as tablename, 'ReimbAllowConsFunctionalCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ReimbAllowConsBillingCurrency' as componentID, 'CONTRACTS' as tablename, 'ReimbAllowConsBillingCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ReimbAllowCons' as componentID, 'CONTRACTS' as tablename, 'ReimbAllowCons' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ReimbAllowBillingCurrency' as componentID, 'CONTRACTS' as tablename, 'ReimbAllowBillingCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ReimbAllow' as componentID, 'CONTRACTS' as tablename, 'ReimbAllow' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'Period' as componentID, 'CONTRACTS' as tablename, 'Period' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'Notes' as componentID, 'CONTRACTS' as tablename, 'Notes' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'FeeIncludeInd' as componentID, 'CONTRACTS' as tablename, 'FeeIncludeInd' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'FeeFunctionalCurrency' as componentID, 'CONTRACTS' as tablename, 'FeeFunctionalCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'FeeDirLabFunctionalCurrency' as componentID, 'CONTRACTS' as tablename, 'FeeDirLabFunctionalCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'FeeDirLabBillingCurrency' as componentID, 'CONTRACTS' as tablename, 'FeeDirLabBillingCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'FeeDirLab' as componentID, 'CONTRACTS' as tablename, 'FeeDirLab' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'FeeDirExpFunctionalCurrency' as componentID, 'CONTRACTS' as tablename, 'FeeDirExpFunctionalCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'FeeDirExpBillingCurrency' as componentID, 'CONTRACTS' as tablename, 'FeeDirExpBillingCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'FeeDirExp' as componentID, 'CONTRACTS' as tablename, 'FeeDirExp' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'FeeBillingCurrency' as componentID, 'CONTRACTS' as tablename, 'FeeBillingCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'Fee' as componentID, 'CONTRACTS' as tablename, 'Fee' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ContractTypeDesc' as componentID, 'CONTRACTS' as tablename, 'ContractTypeDesc' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ContractType' as componentID, 'CONTRACTS' as tablename, 'ContractType' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ContractStatusDesc' as componentID, 'CONTRACTS' as tablename, 'ContractStatusDesc' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ContractStatus' as componentID, 'CONTRACTS' as tablename, 'ContractStatus' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ContractNumber' as componentID, 'CONTRACTS' as tablename, 'ContractNumber' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ContractDescription' as componentID, 'CONTRACTS' as tablename, 'ContractDescription' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ConsultFeeFunctionalCurrency' as componentID, 'CONTRACTS' as tablename, 'ConsultFeeFunctionalCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ConsultFeeBillingCurrency' as componentID, 'CONTRACTS' as tablename, 'ConsultFeeBillingCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ConsultFee' as componentID, 'CONTRACTS' as tablename, 'ConsultFee' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTS' as GridID, 'ApprovedDate' as componentID, 'CONTRACTS' as tablename, 'ApprovedDate' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, '_RecordAccess' as componentID, 'CONTRACTDETAILS' as tablename, '_RecordAccess' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'WBSNumber' as componentID, 'CONTRACTDETAILS' as tablename, 'WBSNumber' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'WBS3Name' as componentID, 'CONTRACTDETAILS' as tablename, 'WBS3Name' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'WBS3' as componentID, 'CONTRACTDETAILS' as tablename, 'WBS3' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'WBS2Name' as componentID, 'CONTRACTDETAILS' as tablename, 'WBS2Name' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'WBS2' as componentID, 'CONTRACTDETAILS' as tablename, 'WBS2' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'WBS1' as componentID, 'CONTRACTDETAILS' as tablename, 'WBS1' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'TotalFunctionalCurrency' as componentID, 'CONTRACTDETAILS' as tablename, 'TotalFunctionalCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'TotalDirectFeesFunc' as componentID, 'CONTRACTDETAILS' as tablename, 'TotalDirectFeesFunc' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'TotalDirectFeesBilling' as componentID, 'CONTRACTDETAILS' as tablename, 'TotalDirectFeesBilling' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'TotalDirectFees' as componentID, 'CONTRACTDETAILS' as tablename, 'TotalDirectFees' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'TotalBillingCurrency' as componentID, 'CONTRACTDETAILS' as tablename, 'TotalBillingCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'Total' as componentID, 'CONTRACTDETAILS' as tablename, 'Total' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'SubLevel' as componentID, 'CONTRACTDETAILS' as tablename, 'SubLevel' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'ReimbAllowFunctionalCurrency' as componentID, 'CONTRACTDETAILS' as tablename, 'ReimbAllowFunctionalCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'ReimbAllowExpFunctionalCurrency' as componentID, 'CONTRACTDETAILS' as tablename, 'ReimbAllowExpFunctionalCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'ReimbAllowExpBillingCurrency' as componentID, 'CONTRACTDETAILS' as tablename, 'ReimbAllowExpBillingCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'ReimbAllowExp' as componentID, 'CONTRACTDETAILS' as tablename, 'ReimbAllowExp' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'ReimbAllowConsFunctionalCurrency' as componentID, 'CONTRACTDETAILS' as tablename, 'ReimbAllowConsFunctionalCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'ReimbAllowConsBillingCurrency' as componentID, 'CONTRACTDETAILS' as tablename, 'ReimbAllowConsBillingCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'ReimbAllowCons' as componentID, 'CONTRACTDETAILS' as tablename, 'ReimbAllowCons' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'ReimbAllowBillingCurrency' as componentID, 'CONTRACTDETAILS' as tablename, 'ReimbAllowBillingCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'ReimbAllow' as componentID, 'CONTRACTDETAILS' as tablename, 'ReimbAllow' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'ReadyForProcessing' as componentID, 'CONTRACTDETAILS' as tablename, 'ReadyForProcessing' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'parentId' as componentID, 'CONTRACTDETAILS' as tablename, 'parentId' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'Name' as componentID, 'CONTRACTDETAILS' as tablename, 'Name' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'key' as componentID, 'CONTRACTDETAILS' as tablename, 'key' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'hasChildren' as componentID, 'CONTRACTDETAILS' as tablename, 'hasChildren' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'FunctionalCurrencyCode' as componentID, 'CONTRACTDETAILS' as tablename, 'FunctionalCurrencyCode' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'FeeFunctionalCurrency' as componentID, 'CONTRACTDETAILS' as tablename, 'FeeFunctionalCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'FeeDirLabFunctionalCurrency' as componentID, 'CONTRACTDETAILS' as tablename, 'FeeDirLabFunctionalCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'FeeDirLabBillingCurrency' as componentID, 'CONTRACTDETAILS' as tablename, 'FeeDirLabBillingCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'FeeDirLab' as componentID, 'CONTRACTDETAILS' as tablename, 'FeeDirLab' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'FeeDirExpFunctionalCurrency' as componentID, 'CONTRACTDETAILS' as tablename, 'FeeDirExpFunctionalCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'FeeDirExpBillingCurrency' as componentID, 'CONTRACTDETAILS' as tablename, 'FeeDirExpBillingCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'FeeDirExp' as componentID, 'CONTRACTDETAILS' as tablename, 'FeeDirExp' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'FeeBillingCurrency' as componentID, 'CONTRACTDETAILS' as tablename, 'FeeBillingCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'Fee' as componentID, 'CONTRACTDETAILS' as tablename, 'Fee' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'DummyRow' as componentID, 'CONTRACTDETAILS' as tablename, 'DummyRow' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'depth' as componentID, 'CONTRACTDETAILS' as tablename, 'depth' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'ContractNumber' as componentID, 'CONTRACTDETAILS' as tablename, 'ContractNumber' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'ContractDescription' as componentID, 'CONTRACTDETAILS' as tablename, 'ContractDescription' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'ConsultFeeFunctionalCurrency' as componentID, 'CONTRACTDETAILS' as tablename, 'ConsultFeeFunctionalCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'ConsultFeeBillingCurrency' as componentID, 'CONTRACTDETAILS' as tablename, 'ConsultFeeBillingCurrency' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTDETAILS' as GridID, 'ConsultFee' as componentID, 'CONTRACTDETAILS' as tablename, 'ConsultFee' as fieldname, 'contracts' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTCREDIT' as GridID, 'WBS1' as componentID, 'CONTRACTCREDIT' as tablename, 'WBS1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTCREDIT' as GridID, 'PercentCredit' as componentID, 'CONTRACTCREDIT' as tablename, 'PercentCredit' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTCREDIT' as GridID, 'EmployeeName' as componentID, 'CONTRACTCREDIT' as tablename, 'EmployeeName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTCREDIT' as GridID, 'Employee' as componentID, 'CONTRACTCREDIT' as tablename, 'Employee' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CONTRACTCREDIT' as GridID, 'ContractNumber' as componentID, 'CONTRACTCREDIT' as tablename, 'ContractNumber' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'ZIP' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'ZIP' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'WebSite' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'WebSite' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'WBS3' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'WBS3' as fieldname, 'team_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'WBS2' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'WBS2' as fieldname, 'team_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'WBS1' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'WBS1' as fieldname, 'team_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'VendorInd' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'VendorInd' as fieldname, 'team_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'TeamStatus' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'TeamStatus' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'State' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'State' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'RoleName' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'RoleName' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'RoleDescription' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'RoleDescription' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'Role' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'Role' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'RecordType' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'RecordType' as fieldname, 'team_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'PrimaryInd' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'PrimaryInd' as fieldname, 'team_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'PKey' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'PKey' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'ModUser' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'ModUser' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'ModDate' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'ModDate' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'IsSubconsultant' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'IsSubconsultant' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'IsClient' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'IsClient' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'DisplayAddress' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'DisplayAddress' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'CreateUser' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'CreateUser' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'CreateDate' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'CreateDate' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'Country' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'Country' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'CostAmount' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'CostAmount' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'CompleteAddress' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'CompleteAddress' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'ClientName' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'ClientName' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'ClientInd' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'ClientInd' as fieldname, 'team_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'ClientID' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'ClientID' as fieldname, 'team_web' as tabID, 1 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'ClientConfidential' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'ClientConfidential' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'City' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'City' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'AddressPhone' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'AddressPhone' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'Address2' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'Address2' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'Address1' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'Address1' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'CLENDORPROJECTASSOC' as GridID, 'Address' as componentID, 'CLENDORPROJECTASSOC' as tablename, 'Address' as fieldname, 'team_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'WBS3' as componentID, 'Activity' as tablename, 'WBS3' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'WBS2' as componentID, 'Activity' as tablename, 'WBS2' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'WBS1' as componentID, 'Activity' as tablename, 'WBS1' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'VendorName' as componentID, 'Activity' as tablename, 'VendorName' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'VendorContactName' as componentID, 'Activity' as tablename, 'VendorContactName' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'Vendor' as componentID, 'Activity' as tablename, 'Vendor' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'UpdateID' as componentID, 'Activity' as tablename, 'UpdateID' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'TypeName' as componentID, 'Activity' as tablename, 'TypeName' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'Type' as componentID, 'Activity' as tablename, 'Type' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'TaskCompletionDate' as componentID, 'Activity' as tablename, 'TaskCompletionDate' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'Subject' as componentID, 'Activity' as tablename, 'Subject' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'StartDateObj' as componentID, 'Activity' as tablename, 'StartDateObj' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'StartDate' as componentID, 'Activity' as tablename, 'StartDate' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'ShowTimeAs' as componentID, 'Activity' as tablename, 'ShowTimeAs' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RP' as componentID, 'Activity' as tablename, 'RP' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'ReminderUnit' as componentID, 'Activity' as tablename, 'ReminderUnit' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'ReminderMinHrDay' as componentID, 'Activity' as tablename, 'ReminderMinHrDay' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'ReminderInd' as componentID, 'Activity' as tablename, 'ReminderInd' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'ReminderDate' as componentID, 'Activity' as tablename, 'ReminderDate' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccurMonth' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurMonth' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccurInd' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurInd' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccurDay' as componentID, 'Activity' as tablename, 'RecurrYearlyOccurDay' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyOccur' as componentID, 'Activity' as tablename, 'RecurrYearlyOccur' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyMonth' as componentID, 'Activity' as tablename, 'RecurrYearlyMonth' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrYearlyDay' as componentID, 'Activity' as tablename, 'RecurrYearlyDay' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyWed' as componentID, 'Activity' as tablename, 'RecurrWeeklyWed' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyTue' as componentID, 'Activity' as tablename, 'RecurrWeeklyTue' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyThu' as componentID, 'Activity' as tablename, 'RecurrWeeklyThu' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklySun' as componentID, 'Activity' as tablename, 'RecurrWeeklySun' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklySat' as componentID, 'Activity' as tablename, 'RecurrWeeklySat' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyMon' as componentID, 'Activity' as tablename, 'RecurrWeeklyMon' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyFri' as componentID, 'Activity' as tablename, 'RecurrWeeklyFri' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrWeeklyFreq' as componentID, 'Activity' as tablename, 'RecurrWeeklyFreq' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrType' as componentID, 'Activity' as tablename, 'RecurrType' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrStartDate' as componentID, 'Activity' as tablename, 'RecurrStartDate' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccurInd' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurInd' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccurFreq' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurFreq' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccurDay' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccurDay' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyOccur' as componentID, 'Activity' as tablename, 'RecurrMonthlyOccur' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyFreq' as componentID, 'Activity' as tablename, 'RecurrMonthlyFreq' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrMonthlyDay' as componentID, 'Activity' as tablename, 'RecurrMonthlyDay' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrID' as componentID, 'Activity' as tablename, 'RecurrID' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrEndType' as componentID, 'Activity' as tablename, 'RecurrEndType' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrEndDate' as componentID, 'Activity' as tablename, 'RecurrEndDate' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrenceInd' as componentID, 'Activity' as tablename, 'RecurrenceInd' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrDailyWeekDay' as componentID, 'Activity' as tablename, 'RecurrDailyWeekDay' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'RecurrDailyFreq' as componentID, 'Activity' as tablename, 'RecurrDailyFreq' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'ReadID' as componentID, 'Activity' as tablename, 'ReadID' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'ProjName' as componentID, 'Activity' as tablename, 'ProjName' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'PrivateInd' as componentID, 'Activity' as tablename, 'PrivateInd' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'PriorityDesc' as componentID, 'Activity' as tablename, 'PriorityDesc' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'Priority' as componentID, 'Activity' as tablename, 'Priority' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'PrimaryClient' as componentID, 'Activity' as tablename, 'PrimaryClient' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'OwnerName' as componentID, 'Activity' as tablename, 'OwnerName' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'Owner' as componentID, 'Activity' as tablename, 'Owner' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'OutlookType' as componentID, 'Activity' as tablename, 'OutlookType' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'Notes' as componentID, 'Activity' as tablename, 'Notes' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'ModUser' as componentID, 'Activity' as tablename, 'ModUser' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'ModDate' as componentID, 'Activity' as tablename, 'ModDate' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'MktName' as componentID, 'Activity' as tablename, 'MktName' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'MaxOccurences' as componentID, 'Activity' as tablename, 'MaxOccurences' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'Location' as componentID, 'Activity' as tablename, 'Location' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'LeadID' as componentID, 'Activity' as tablename, 'LeadID' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'isTask' as componentID, 'Activity' as tablename, 'isTask' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'isCalendar' as componentID, 'Activity' as tablename, 'isCalendar' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'InstanceStartDate' as componentID, 'Activity' as tablename, 'InstanceStartDate' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'HasNotes' as componentID, 'Activity' as tablename, 'HasNotes' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'EndDateSort' as componentID, 'Activity' as tablename, 'EndDateSort' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'EndDateObj' as componentID, 'Activity' as tablename, 'EndDateObj' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'EndDateFilter' as componentID, 'Activity' as tablename, 'EndDateFilter' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'EndDate' as componentID, 'Activity' as tablename, 'EndDate' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'Employee' as componentID, 'Activity' as tablename, 'Employee' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'Duration' as componentID, 'Activity' as tablename, 'Duration' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'CreateUser' as componentID, 'Activity' as tablename, 'CreateUser' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'CreateEmployee' as componentID, 'Activity' as tablename, 'CreateEmployee' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'CreateDate' as componentID, 'Activity' as tablename, 'CreateDate' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'ContactPhoneForVendor' as componentID, 'Activity' as tablename, 'ContactPhoneForVendor' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'ContactNameForVendor' as componentID, 'Activity' as tablename, 'ContactNameForVendor' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'ContactName' as componentID, 'Activity' as tablename, 'ContactName' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'ContactIDForVendor' as componentID, 'Activity' as tablename, 'ContactIDForVendor' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'ContactID' as componentID, 'Activity' as tablename, 'ContactID' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'CompletionInd' as componentID, 'Activity' as tablename, 'CompletionInd' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'ClientName' as componentID, 'Activity' as tablename, 'ClientName' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'ClientID' as componentID, 'Activity' as tablename, 'ClientID' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'CampaignID' as componentID, 'Activity' as tablename, 'CampaignID' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'Attendees' as componentID, 'Activity' as tablename, 'Attendees' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'Associations' as componentID, 'Activity' as tablename, 'Associations' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'AllDayEventInd' as componentID, 'Activity' as tablename, 'AllDayEventInd' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'acType' as componentID, 'Activity' as tablename, 'acType' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'Activity' as GridID, 'ActivityID' as componentID, 'Activity' as tablename, 'ActivityID' as fieldname, 'activities_web' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'AR' as GridID, 'WBS3' as componentID, 'AR' as tablename, 'WBS3' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'AR' as GridID, 'WBS2' as componentID, 'AR' as tablename, 'WBS2' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'AR' as GridID, 'WBS1' as componentID, 'AR' as tablename, 'WBS1' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'AR' as GridID, 'UnpaidInvoice' as componentID, 'AR' as tablename, 'UnpaidInvoice' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'AR' as GridID, 'Period' as componentID, 'AR' as tablename, 'Period' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'AR' as GridID, 'PaidPeriod' as componentID, 'AR' as tablename, 'PaidPeriod' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'AR' as GridID, 'IsPaid' as componentID, 'AR' as tablename, 'IsPaid' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'AR' as GridID, 'InvoiceDate' as componentID, 'AR' as tablename, 'InvoiceDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'AR' as GridID, 'Invoice' as componentID, 'AR' as tablename, 'Invoice' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'Projects' as infocenterArea, 'AR' as GridID, 'InvBalanceSourceCurrency' as componentID, 'AR' as tablename, 'InvBalanceSourceCurrency' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'X' as GridID, 'TextLibrary.Name' as componentID, 'TextLibrary' as tablename, 'Name' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'X' as GridID, 'TextLibrary.ModUser' as componentID, 'TextLibrary' as tablename, 'ModUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'X' as GridID, 'TextLibrary.ModUserName' as componentID, 'TextLibrary' as tablename, 'ModUserName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'X' as GridID, 'TextLibrary.ModDate' as componentID, 'TextLibrary' as tablename, 'ModDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'X' as GridID, 'TextLibrary.Document' as componentID, 'TextLibrary' as tablename, 'Document' as fieldname, 'general' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'X' as GridID, 'TextLibrary.DisplayName' as componentID, 'TextLibrary' as tablename, 'DisplayName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'X' as GridID, 'TextLibrary.Description' as componentID, 'TextLibrary' as tablename, 'Description' as fieldname, 'general' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'X' as GridID, 'TextLibrary.CustomCurrencyCode' as componentID, 'TextLibrary' as tablename, 'CustomCurrencyCode' as fieldname, 'general' as tabID, 1 as essential union 
Select 'TextLibrary' as infocenterArea, 'X' as GridID, 'TextLibrary.CreateUser' as componentID, 'TextLibrary' as tablename, 'CreateUser' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'X' as GridID, 'TextLibrary.CreateUserName' as componentID, 'TextLibrary' as tablename, 'CreateUserName' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'X' as GridID, 'TextLibrary.CreateDate' as componentID, 'TextLibrary' as tablename, 'CreateDate' as fieldname, 'noassociation' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'X' as GridID, 'TextLibFileLinks' as componentID, null as tablename, null as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'X' as GridID, 'FW_ATTACHMENTS' as componentID, null as tablename, null as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'X' as GridID, 'Boilerplate.Links.Divider' as componentID, null as tablename, null as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'X' as GridID, 'Boilerplate.Files.Divider' as componentID, null as tablename, null as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'TextLibFileLinks' as GridID, 'Name' as componentID, 'TextLibFileLinks' as tablename, 'Name' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'TextLibFileLinks' as GridID, 'LinkID' as componentID, 'TextLibFileLinks' as tablename, 'LinkID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'TextLibFileLinks' as GridID, 'Graphic' as componentID, 'TextLibFileLinks' as tablename, 'Graphic' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'TextLibFileLinks' as GridID, 'FilePath' as componentID, 'TextLibFileLinks' as tablename, 'FilePath' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'TextLibFileLinks' as GridID, 'Description' as componentID, 'TextLibFileLinks' as tablename, 'Description' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'PKey' as componentID, 'FW_ATTACHMENTS' as tablename, 'PKey' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Name' as componentID, 'FW_ATTACHMENTS' as tablename, 'Name' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key3' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key3' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key2' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key2' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key1' as componentID, 'FW_ATTACHMENTS' as tablename, 'Key1' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileSize' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileSize' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileName' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileName' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileID' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileID' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileDescription' as componentID, 'FW_ATTACHMENTS' as tablename, 'FileDescription' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'ContentType' as componentID, 'FW_ATTACHMENTS' as tablename, 'ContentType' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'CategoryDescription' as componentID, 'FW_ATTACHMENTS' as tablename, 'CategoryDescription' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'CategoryCode' as componentID, 'FW_ATTACHMENTS' as tablename, 'CategoryCode' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union 
Select 'TextLibrary' as infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Application' as componentID, 'FW_ATTACHMENTS' as tablename, 'Application' as fieldname, 'filesAndLinksTab' as tabID, 0 as essential union
--Get all the custom grid fields
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, FW_CustomColumnsData.name as componentID, FW_CustomGridsData.tablename, FW_CustomColumnsData.name as fieldname, cfgscreendesignerdata.tabid as tabid, 0 as essential  from
fw_CustomColumnsData inner join FW_CustomGridsData on fw_CustomColumnsData.InfoCenterArea = FW_CustomGridsData.InfoCenterArea
and fw_CustomColumnsData.gridID = FW_CustomGridsData.gridID
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = IsNull(FW_CustomGridsData.TableName, FW_CustomGridsData.GridID)
union
---Get all the UDIC fields
select fw_CustomColumnsData.infocenterarea, fw_CustomColumnsData.gridid,FW_UDICData.TableName +'.'+ FW_CustomColumnsData.name as componentID,
FW_UDICData.TableName as tablename,FW_CustomColumnsData.name  as fieldname, cfgscreendesignerdata.tabid, 0 as essential  from fw_CustomColumnsData inner join FW_UDICData
on FW_UDICData.UDIC_ID  = FW_CustomColumnsData.infocenterarea 
left join cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = IsNull(FW_UDICData.TableName, FW_UDICData.UDIC_ID) +'.' + FW_CustomColumnsData.name
where FW_CustomColumnsData.gridid='X'
union 
--Get the UDIC_UID for each UDIC
select FW_UDICData.UDIC_ID, 'X' as gridid,FW_UDICData.TableName +'.UDIC_UID' as componentID,
FW_UDICData.TableName as tablename,'UDIC_UID'  as fieldname, 'noassociation' as tabID, 0 as essential  from  FW_UDICData
--Get all the standard hubs custom columns that are not in a grid
union
Select fw_CustomColumnsData.infocenterarea, fw_CustomColumnsData.gridid, 'CA.'+ name as componentID, 'AccountCustomTabFields' as tablename, name as fieldname , CFGScreenDesignerData.tabid, 0 as essential 
from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'CA.'+ FW_CustomColumnsData.name
where fw_CustomColumnsData.InfoCenterArea='ChartOfAccounts' and fw_CustomColumnsData.GridID='X' 
union
Select fw_CustomColumnsData.infocenterarea, fw_CustomColumnsData.gridid, 'PR.'+ name as componentID, 'ProjectCustomTabFields' as tablename, name as fieldname , CFGScreenDesignerData.tabid, 0 as essential 
from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'PR.'+ FW_CustomColumnsData.name
where fw_CustomColumnsData.InfoCenterArea='Projects' and fw_CustomColumnsData.GridID='X' 
union
Select fw_CustomColumnsData.infocenterarea, fw_CustomColumnsData.gridid, 'MktCampaign.'+ name as componentID, 'MktCampaignCustomTabFields' as tablename, name as fieldname, CFGScreenDesignerData.tabid, 0 as essential  
from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'MktCampaign.'+ FW_CustomColumnsData.name
where fw_CustomColumnsData.InfoCenterArea='MktCampaigns' and fw_CustomColumnsData.GridID='X' 
union
Select fw_CustomColumnsData.infocenterarea, fw_CustomColumnsData.gridid,  'Clendor.'+ name as componentID, 'ClientCustomTabFields' as tablename, name as fieldname, CFGScreenDesignerData.tabid, 0 as essential 
from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'Clendor.'+ FW_CustomColumnsData.name
where fw_CustomColumnsData.InfoCenterArea='Firms' and fw_CustomColumnsData.GridID='X' 
union
Select fw_CustomColumnsData.infocenterarea, fw_CustomColumnsData.GridID,  'EM.'+ name as componentID, 'EmployeeCustomTabFields' as tablename, name as fieldname , CFGScreenDesignerData.tabid, 0 as essential 
from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'EM.'+ FW_CustomColumnsData.name
where fw_CustomColumnsData.infocenterarea='Employees' and fw_CustomColumnsData.GridID='X' 
union
Select fw_CustomColumnsData.infocenterarea, fw_CustomColumnsData.GridID,  'Activity.'+ name as componentID, 'ActivityCustomTabFields' as tablename, name as fieldname , CFGScreenDesignerData.tabid, 0 as essential 
from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'Activity.'+ FW_CustomColumnsData.name
where fw_CustomColumnsData.infocenterarea='Activity' and fw_CustomColumnsData.GridID='X' union
Select fw_CustomColumnsData.infocenterarea, fw_CustomColumnsData.gridid,  'TextLibrary.'+ name as componentID, 'TextLibraryCustomTabFields' as tablename, name as fieldname , CFGScreenDesignerData.tabid, 0 as essential 
from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'TextLibrary.'+ FW_CustomColumnsData.name
where fw_CustomColumnsData.infocenterarea='TextLibrary' and fw_CustomColumnsData.GridID='X' 
union 
Select fw_CustomColumnsData.infocenterarea, fw_CustomColumnsData.GridID,  'Contacts.'+ name as componentID, 'ContactCustomTabFields' as tablename, name as fieldname , CFGScreenDesignerData.tabid, 0 as essential 
from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'Contacts.'+ FW_CustomColumnsData.name
where fw_CustomColumnsData.infocenterarea='Contacts' and fw_CustomColumnsData.GridID='X' 
--Get the key columns for each custom grid
union
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'WBS1' as componentID, FW_CustomGridsData.tablename, 'WBS1' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsData
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = IsNull(FW_CustomGridsData.TableName, FW_CustomGridsData.GridID)
where FW_CustomGridsData.InfoCenterArea='Projects'
UNION
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'WBS2' as componentID, FW_CustomGridsData.tablename, 'WBS2' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsData
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = IsNull(FW_CustomGridsData.TableName, FW_CustomGridsData.GridID)
where FW_CustomGridsData.InfoCenterArea='Projects'
UNION
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'WBS3' as componentID, FW_CustomGridsData.tablename, 'WBS3' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsData
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = IsNull(FW_CustomGridsData.TableName, FW_CustomGridsData.GridID)
where FW_CustomGridsData.InfoCenterArea='Projects'
UNION
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'Account' as componentID, FW_CustomGridsData.tablename, 'Account' as fieldname , cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsData
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = IsNull(FW_CustomGridsData.TableName, FW_CustomGridsData.GridID)
where FW_CustomGridsData.InfoCenterArea='ChartOfAccounts'
UNION
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'ContactID' as componentID, FW_CustomGridsData.tablename, 'ContactID' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsData
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = IsNull(FW_CustomGridsData.TableName, FW_CustomGridsData.GridID)
where FW_CustomGridsData.InfoCenterArea='Contacts'
UNION
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'ClientID' as componentID, FW_CustomGridsData.tablename, 'ClientID' as fieldname, cfgscreendesignerdata.tabid, 0 as essential   
from FW_CustomGridsData
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = IsNull(FW_CustomGridsData.TableName, FW_CustomGridsData.GridID)
where FW_CustomGridsData.InfoCenterArea='Firms'
UNION
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'Employee' as componentID, FW_CustomGridsData.tablename, 'Employee' as fieldname, cfgscreendesignerdata.tabid, 0 as essential   
from FW_CustomGridsData
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = IsNull(FW_CustomGridsData.TableName, FW_CustomGridsData.GridID)
where FW_CustomGridsData.InfoCenterArea='Employees'
UNION
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'ActivityID' as componentID, FW_CustomGridsData.tablename, 'ActivityID' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsData
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = IsNull(FW_CustomGridsData.TableName, FW_CustomGridsData.GridID)
where FW_CustomGridsData.InfoCenterArea='Activity'
UNION
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'Name' as componentID, FW_CustomGridsData.tablename, 'Name' as fieldname, cfgscreendesignerdata.tabid, 0 as essential   
from FW_CustomGridsData
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = IsNull(FW_CustomGridsData.TableName, FW_CustomGridsData.GridID)
where FW_CustomGridsData.InfoCenterArea='TextLibrary'
UNION
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'CampaignID' as componentID, FW_CustomGridsData.tablename, 'CampaignID' as fieldname, cfgscreendesignerdata.tabid, 0 as essential   
from FW_CustomGridsData
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = IsNull(FW_CustomGridsData.TableName, FW_CustomGridsData.GridID)
where FW_CustomGridsData.InfoCenterArea='MktCampaigns'
UNION
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'UDIC_UID' as componentID, FW_CustomGridsData.tablename, 'UDIC_UID' as fieldname, cfgscreendesignerdata.tabid, 0 as essential 
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID and fw_customgridsdata.GridType is null
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = IsNull(FW_CustomGridsData.TableName, FW_CustomGridsData.GridID)
UNION
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'Seq' as componentID, FW_CustomGridsData.tablename, 'Seq' as fieldname, cfgscreendesignerdata.tabid, 0 as essential 
from FW_CustomGridsdata
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = IsNull(FW_CustomGridsData.TableName, FW_CustomGridsData.GridID)
Where FW_CustomGridsData.GridType is null
--get fields for special UDIC grids
UNION
select FW_CustomGridsData.infocenterArea, 'Activity', 'UDIC_UID' as componentID, 'Activity', 'UDIC_UID' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = 'Activity'
Where FW_CustomGridsData.GridType ='A'
UNION
select FW_CustomGridsData.infocenterArea, 'Activity', 'ActivityID' as componentID, 'Activity', 'ActivityID' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = 'Activity'
Where FW_CustomGridsData.GridType ='A'
UNION
select FW_CustomGridsData.infocenterArea, 'Activity', 'EndDate' as componentID, 'Activity', 'EndDate' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = 'Activity'
Where FW_CustomGridsData.GridType ='A'
UNION
select FW_CustomGridsData.infocenterArea, 'Activity', 'EndDateSort' as componentID, 'Activity', 'EndDateSort' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = 'Activity'
Where FW_CustomGridsData.GridType ='A'
UNION
select FW_CustomGridsData.infocenterArea, 'Activity', 'StartDate' as componentID, 'Activity', 'StartDate' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = 'Activity'
Where FW_CustomGridsData.GridType ='A'
UNION
select FW_CustomGridsData.infocenterArea, 'Activity', 'EndDateActivityType' as componentID, 'Activity', 'EndDateActivityType' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = 'Activity'
Where FW_CustomGridsData.GridType ='A'
UNION
select FW_CustomGridsData.infocenterArea, 'Activity', 'Subject' as componentID, 'Activity', 'Subject' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = 'Activity'
Where FW_CustomGridsData.GridType ='A'
UNION
select FW_CustomGridsData.infocenterArea, 'Activity', 'Attendees' as componentID, 'Activity', 'Attendees' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = 'Activity'
Where FW_CustomGridsData.GridType ='A'
UNION
select FW_CustomGridsData.infocenterArea, 'Activity', 'Type' as componentID, 'Activity', 'Type' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = 'Activity'
Where FW_CustomGridsData.GridType ='A'
UNION
select FW_CustomGridsData.infocenterArea, 'Activity', 'Associations' as componentID, 'Activity', 'Associations' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = 'Activity'
Where FW_CustomGridsData.GridType ='A'
UNION
select FW_CustomGridsData.infocenterArea, 'Activity', 'ProjName' as componentID, 'Activity', 'ProjName' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = 'Activity'
Where FW_CustomGridsData.GridType ='A'
UNION
select FW_CustomGridsData.infocenterArea, 'Activity', 'MktName' as componentID, 'Activity', 'MktName' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = 'Activity'
Where FW_CustomGridsData.GridType ='A'
UNION
select FW_CustomGridsData.infocenterArea, 'Activity', 'Notes' as componentID, 'Activity', 'Notes' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = 'Activity'
Where FW_CustomGridsData.GridType ='A'
UNION
select FW_CustomGridsData.infocenterArea, 'Activity', 'CompletionInd' as componentID, 'Activity', 'CompletionInd' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = 'Activity'
Where FW_CustomGridsData.GridType ='A'
UNION
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'UDIC_UID' as componentID,FW_CustomGridsData.infocenterArea+'FileLinks', 'UDIC_UID' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = FW_CustomGridsData.InfoCenterArea + 'FileLinks'
Where FW_CustomGridsData.GridType ='F'
UNION
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'LinkID' as componentID,FW_CustomGridsData.infocenterArea+'FileLinks', 'LinkID' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = FW_CustomGridsData.InfoCenterArea + 'FileLinks'
Where FW_CustomGridsData.GridType ='F'
UNION
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'Description' as componentID,FW_CustomGridsData.infocenterArea+'FileLinks', 'Description' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = FW_CustomGridsData.InfoCenterArea + 'FileLinks'
Where FW_CustomGridsData.GridType ='F'
UNION
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'FilePath' as componentID,FW_CustomGridsData.infocenterArea+'FileLinks', 'FilePath' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = FW_CustomGridsData.InfoCenterArea + 'FileLinks'
Where FW_CustomGridsData.GridType ='F'
UNION
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'Graphic' as componentID,FW_CustomGridsData.infocenterArea+'FileLinks', 'Graphic' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID = FW_CustomGridsData.InfoCenterArea + 'FileLinks'
Where FW_CustomGridsData.GridType ='F'
UNION
select FW_CustomGridsData.infocenterArea, 'FW_ATTACHMENTS' as GridID, 'PKey' as componentID,'FW_ATTACHMENTS' as TableName, 'PKey' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID =  'FW_ATTACHMENTS'
Where FW_CustomGridsData.GridType ='T'
UNION
select FW_CustomGridsData.infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key3' as componentID,'FW_ATTACHMENTS' as TableName, 'Key3' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID =  'FW_ATTACHMENTS'
Where FW_CustomGridsData.GridType ='T'
UNION
select FW_CustomGridsData.infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key2' as componentID,'FW_ATTACHMENTS' as TableName, 'Key2' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID =  'FW_ATTACHMENTS'
Where FW_CustomGridsData.GridType ='T'
UNION
select FW_CustomGridsData.infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Key1' as componentID,'FW_ATTACHMENTS' as TableName, 'Key1' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID =  'FW_ATTACHMENTS'
Where FW_CustomGridsData.GridType ='T'
UNION
select FW_CustomGridsData.infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileSize' as componentID,'FW_ATTACHMENTS' as TableName, 'FileSize' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID =  'FW_ATTACHMENTS'
Where FW_CustomGridsData.GridType ='T'
UNION
select FW_CustomGridsData.infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileName' as componentID,'FW_ATTACHMENTS' as TableName, 'FileName' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID =  'FW_ATTACHMENTS'
Where FW_CustomGridsData.GridType ='T'
UNION
select FW_CustomGridsData.infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileID' as componentID,'FW_ATTACHMENTS' as TableName, 'FileID' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID =  'FW_ATTACHMENTS'
Where FW_CustomGridsData.GridType ='T'
UNION
select FW_CustomGridsData.infocenterArea, 'FW_ATTACHMENTS' as GridID, 'FileDescription' as componentID,'FW_ATTACHMENTS' as TableName, 'FileDescription' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID =  'FW_ATTACHMENTS'
Where FW_CustomGridsData.GridType ='T'
UNION
select FW_CustomGridsData.infocenterArea, 'FW_ATTACHMENTS' as GridID, 'ContentType' as componentID,'FW_ATTACHMENTS' as TableName, 'ContentType' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID =  'FW_ATTACHMENTS'
Where FW_CustomGridsData.GridType ='T'
UNION
select FW_CustomGridsData.infocenterArea, 'FW_ATTACHMENTS' as GridID, 'CategoryDescription' as componentID,'FW_ATTACHMENTS' as TableName, 'CategoryDescription' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID =  'FW_ATTACHMENTS'
Where FW_CustomGridsData.GridType ='T'
UNION
select FW_CustomGridsData.infocenterArea, 'FW_ATTACHMENTS' as GridID, 'CategoryCode' as componentID,'FW_ATTACHMENTS' as TableName, 'CategoryCode' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID =  'FW_ATTACHMENTS'
Where FW_CustomGridsData.GridType ='T'
UNION
select FW_CustomGridsData.infocenterArea, 'FW_ATTACHMENTS' as GridID, 'Application' as componentID,'FW_ATTACHMENTS' as TableName, 'Application' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID =  'FW_ATTACHMENTS'
Where FW_CustomGridsData.GridType ='T'
UNION
select FW_CustomGridsData.infocenterArea, 'FW_ATTACHMENTS' as GridID, 'UDIC_UID' as componentID,'FW_ATTACHMENTS' as TableName, 'UDIC_UID' as fieldname, cfgscreendesignerdata.tabid, 0 as essential 
from FW_CustomGridsdata inner join FW_UDICData on FW_CustomGridsdata.InfoCenterArea= FW_UDICData.UDIC_ID 
left join cfgscreendesignerdata on FW_CustomGridsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.componentID =  'FW_ATTACHMENTS'
Where FW_CustomGridsData.GridType ='T'
 --get internal UDIC fields
UNION
select FW_UDICData.UDIC_ID, 'X' as gridid,FW_UDICData.TableName +'.UDICNumber' as componentID,
FW_UDICData.TableName as tablename,'UDICNumber'  as fieldname,  'noassociation' as tabID, 0 as essential  from  FW_UDICData
UNION
select FW_UDICData.UDIC_ID, 'X' as gridid,FW_UDICData.TableName +'.UDICName' as componentID,
FW_UDICData.TableName as tablename,'UDICName'  as fieldname, 'noassociation' as tabID, 0 as essential  from  FW_UDICData
UNION
select FW_UDICData.UDIC_ID, 'X' as gridid,FW_UDICData.TableName +'.PhotoModDate' as componentID,
FW_UDICData.TableName as tablename,'PhotoModDate'  as fieldname, 'noassociation' as tabID, 0 as essential  from  FW_UDICData
UNION
select FW_UDICData.UDIC_ID, 'X' as gridid,FW_UDICData.TableName +'.HasPhoto' as componentID,
FW_UDICData.TableName as tablename,'HasPhoto'  as fieldname, 'noassociation' as tabID, 0 as essential  from  FW_UDICData
UNION
select FW_UDICData.UDIC_ID, 'X' as gridid,FW_UDICData.TableName +'.CustomCurrencyCode' as componentID,
FW_UDICData.TableName as tablename,'CustomCurrencyCode'  as fieldname, 'noassociation' as tabID, 0 as essential  from  FW_UDICData
UNION
select FW_UDICData.UDIC_ID, 'X' as gridid,FW_UDICData.TableName +'.CreateUser' as componentID,
FW_UDICData.TableName as tablename,'CreateUser'  as fieldname, 'noassociation' as tabID, 0 as essential  from  FW_UDICData
UNION
select FW_UDICData.UDIC_ID, 'X' as gridid,FW_UDICData.TableName +'.CreateUserName' as componentID,
FW_UDICData.TableName as tablename,'CreateUserName'  as fieldname, 'noassociation' as tabID, 0 as essential  from  FW_UDICData
UNION
select FW_UDICData.UDIC_ID, 'X' as gridid,FW_UDICData.TableName +'.CreateDate' as componentID,
FW_UDICData.TableName as tablename,'CreateDate'  as fieldname, 'noassociation' as tabID, 0 as essential  from  FW_UDICData
UNION
select FW_UDICData.UDIC_ID, 'X' as gridid,FW_UDICData.TableName +'.ModUser' as componentID,
FW_UDICData.TableName as tablename,'ModUser'  as fieldname, 'noassociation' as tabID, 0 as essential  from  FW_UDICData
UNION
select FW_UDICData.UDIC_ID, 'X' as gridid,FW_UDICData.TableName +'.ModUserName' as componentID,
FW_UDICData.TableName as tablename,'ModUserName'  as fieldname, 'noassociation' as tabID, 0 as essential  from  FW_UDICData
UNION
select FW_UDICData.UDIC_ID, 'X' as gridid,FW_UDICData.TableName +'.ModDate' as componentID,
FW_UDICData.TableName as tablename,'ModDate'  as fieldname, 'noassociation' as tabID, 0 as essential  from  FW_UDICData
--get phone format fields
UNION
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, FW_CustomColumnsData.name +'Format' as componentID, FW_CustomGridsData.tablename, FW_CustomColumnsData.name +'.Format' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  from
fw_CustomColumnsData inner join FW_CustomGridsData on fw_CustomColumnsData.InfoCenterArea = FW_CustomGridsData.InfoCenterArea
and fw_CustomColumnsData.gridID = FW_CustomGridsData.gridID
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = FW_CustomColumnsData.name
where DataType='Phone'
union
select FW_CustomColumnsData.infocenterarea, FW_CustomColumnsData.gridid,FW_UDICData.TableName +'.'+ FW_CustomColumnsData.name +'Format' as componentID,
FW_UDICData.TableName as tablename,FW_CustomColumnsData.name +'Format' as fieldname, cfgscreendesignerdata.tabid, 0 as essential from fw_CustomColumnsData inner join FW_UDICData
on FW_UDICData.UDIC_ID  = FW_CustomColumnsData.infocenterarea
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = FW_CustomColumnsData.name
where FW_CustomColumnsData.gridid='X' and DataType='Phone'
union
Select FW_CustomColumnsData.InfoCenterArea, FW_CustomColumnsData.GridID,  'Contacts.'+ name+'Format' as componentID, 'ContactCustomTabFields' as tablename, name+'Format' as fieldname, cfgscreendesignerdata.tabid, 0 as essential   from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'Contacts.'+ FW_CustomColumnsData.name
where FW_CustomColumnsData.InfoCenterArea='Contacts' and FW_CustomColumnsData.GridID='X' and  DataType='Phone'
union
Select FW_CustomColumnsData.InfoCenterArea, FW_CustomColumnsData.GridID,  'TextLibrary.'+ name+'Format'  as componentID, 'TextLibraryCustomTabFields' as tablename, name+'Format'  as fieldname, cfgscreendesignerdata.tabid, 0 as essential   from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'TextLibrary.'+ FW_CustomColumnsData.name
where FW_CustomColumnsData.InfoCenterArea='TextLibrary' and FW_CustomColumnsData.GridID='X' and DataType='Phone'
union
Select FW_CustomColumnsData.InfoCenterArea, FW_CustomColumnsData.GridID,  'Activity.'+ name+'Format' as componentID, 'ActivityCustomTabFields' as tablename, name+'Format' as fieldname, cfgscreendesignerdata.tabid, 0 as essential   from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'Activity.'+ FW_CustomColumnsData.name
where FW_CustomColumnsData.InfoCenterArea='Activity' and FW_CustomColumnsData.GridID='X' and DataType='Phone'
union
Select FW_CustomColumnsData.InfoCenterArea, FW_CustomColumnsData.GridID,  'EM.'+ name+'Format' as componentID, 'EmployeeCustomTabFields' as tablename, name+'Format' as fieldname, cfgscreendesignerdata.tabid, 0 as essential   from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'EM.'+ FW_CustomColumnsData.name
where FW_CustomColumnsData.InfoCenterArea='Employees' and FW_CustomColumnsData.GridID='X'  and DataType='Phone'
union 
Select FW_CustomColumnsData.infocenterarea, FW_CustomColumnsData.gridid,  'Clendor.'+ name+'Format' as componentID, 'ClientCustomTabFields' as tablename, name+'Format' as fieldname, cfgscreendesignerdata.tabid, 0 as essential  from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'Clendor.'+ FW_CustomColumnsData.name
where FW_CustomColumnsData.InfoCenterArea='Firms' and FW_CustomColumnsData.GridID='X' and DataType='Phone'
union
Select FW_CustomColumnsData.InfoCenterArea, FW_CustomColumnsData.GridID, 'MktCampaign.'+ name +'Format' as componentID, 'MktCampaignCustomTabFields' as tablename, name+'Format' as fieldname, cfgscreendesignerdata.tabid, 0 as essential   from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'MktCampaign.'+ FW_CustomColumnsData.name
where FW_CustomColumnsData.InfoCenterArea='MktCampaigns' and FW_CustomColumnsData.GridID='X' and DataType='Phone'
union
Select FW_CustomColumnsData.InfoCenterArea, FW_CustomColumnsData.GridID, 'PR.'+ name+'Format' as componentID, 'ProjectCustomTabFields' as tablename, name+'Format' as fieldname, cfgscreendesignerdata.tabid, 0 as essential   from FW_CustomColumnsData
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'PR.'+ FW_CustomColumnsData.name
where FW_CustomColumnsData.InfoCenterArea='Projects' and FW_CustomColumnsData.GridID='X' and DataType='Phone'
union
Select FW_CustomColumnsData.InfoCenterArea, FW_CustomColumnsData.GridID, 'CA.'+ name+'Format' as componentID, 'AccountCustomTabFields' as tablename, name+'Format' as fieldname, cfgscreendesignerdata.tabid, 0 as essential   from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'CA.'+ FW_CustomColumnsData.name
where FW_CustomColumnsData.InfoCenterArea='ChartOfAccounts' and FW_CustomColumnsData.GridID='X' and DataType='Phone'
--get special desc fields
union
select FW_CustomGridsData.infocenterArea, FW_CustomGridsData.gridid, 'desc_'+ FW_CustomColumnsData.name  as componentID, FW_CustomGridsData.tablename,'desc_'+ FW_CustomColumnsData.name as fieldname, cfgscreendesignerdata.tabid, 0 as essential  from
fw_CustomColumnsData inner join FW_CustomGridsData on fw_CustomColumnsData.InfoCenterArea = FW_CustomGridsData.InfoCenterArea
and fw_CustomColumnsData.gridID = FW_CustomGridsData.gridID
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID =  FW_CustomColumnsData.name
where DataType like 'udic%' or datatype in ('firm','contact','employee','wbs1','equipment','mkt','textlibrary','account','org', 'dropdown')
union
select FW_CustomColumnsData.infocenterarea, FW_CustomColumnsData.gridid, FW_UDICData.TableName +'.'+'desc_'+ FW_CustomColumnsData.name as componentID,
FW_UDICData.TableName as tablename,'desc_'+ FW_CustomColumnsData.name  as fieldname, cfgscreendesignerdata.tabid, 0 as essential  from fw_CustomColumnsData inner join FW_UDICData
on FW_UDICData.UDIC_ID  = FW_CustomColumnsData.infocenterarea
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = FW_UDICData.TableName +'.'+ FW_CustomColumnsData.name
where FW_CustomColumnsData.gridid='X' and (DataType like 'udic%' or datatype in ('firm','contact','employee','wbs1','equipment','mkt','textlibrary','account','org', 'dropdown'))
union
Select FW_CustomColumnsData.infocenterarea, FW_CustomColumnsData.gridid,  'Contacts.'+'desc_'+ name as componentID, 'ContactCustomTabFields' as tablename, 'desc_'+name as fieldname, cfgscreendesignerdata.tabid, 0 as essential  from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'Contacts.'+ FW_CustomColumnsData.name
where FW_CustomColumnsData.infocenterarea='Contacts' and FW_CustomColumnsData.gridid='X' and ( DataType like 'udic%' or datatype in ('firm','contact','employee','wbs1','equipment','mkt','textlibrary','account','org', 'dropdown'))
union
Select FW_CustomColumnsData.infocenterarea, FW_CustomColumnsData.gridid,  'TextLibrary.'+'desc_'+ name  as componentID, 'TextLibraryCustomTabFields' as tablename, 'desc_'+name  as fieldname, cfgscreendesignerdata.tabid, 0 as essential   from FW_CustomColumnsData
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'TextLibrary.'+ FW_CustomColumnsData.name
where FW_CustomColumnsData.infocenterarea='TextLibrary' and FW_CustomColumnsData.gridid='X' and (DataType like 'udic%' or datatype in ('firm','contact','employee','wbs1','equipment','mkt','textlibrary','account','org', 'dropdown'))
union
Select FW_CustomColumnsData.infocenterarea, FW_CustomColumnsData.gridid,  'Activity.'+'desc_'+ name as componentID, 'ActivityCustomTabFields' as tablename, 'desc_'+name as fieldname, cfgscreendesignerdata.tabid, 0 as essential   from FW_CustomColumnsData
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'Activity.'+ FW_CustomColumnsData.name
where FW_CustomColumnsData.infocenterarea='Activity' and FW_CustomColumnsData.gridid='X' and (DataType like 'udic%' or datatype in ('firm','contact','employee','wbs1','equipment','mkt','textlibrary','account','org', 'dropdown'))
union
Select FW_CustomColumnsData.infocenterarea, FW_CustomColumnsData.gridid,  'EM.'+'desc_'+ name as componentID, 'EmployeeCustomTabFields' as tablename, 'desc_'+name as fieldname, cfgscreendesignerdata.tabid, 0 as essential   from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'EM.'+ FW_CustomColumnsData.name
where FW_CustomColumnsData.infocenterarea='Employees' and FW_CustomColumnsData.gridid='X'  and (DataType like 'udic%' or datatype in ('firm','contact','employee','wbs1','equipment','mkt','textlibrary','account','org', 'dropdown'))
union 
Select FW_CustomColumnsData.infocenterarea, FW_CustomColumnsData.gridid,  'Clendor.'+ 'desc_'+name as componentID, 'ClientCustomTabFields' as tablename, 'desc_'+name as fieldname, cfgscreendesignerdata.tabid, 0 as essential   from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'Clendor.'+ FW_CustomColumnsData.name
where FW_CustomColumnsData.infocenterarea='Firms' and FW_CustomColumnsData.gridid='X' and (DataType like 'udic%' or datatype in ('firm','contact','employee','wbs1','equipment','mkt','textlibrary','account','org', 'dropdown'))
union
Select FW_CustomColumnsData.infocenterarea, FW_CustomColumnsData.gridid, 'MktCampaign.'+'desc_'+ name  as componentID, 'MktCampaignCustomTabFields' as tablename, 'desc_'+name as fieldname, cfgscreendesignerdata.tabid, 0 as essential   from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'MktCampaign.'+ FW_CustomColumnsData.name
where FW_CustomColumnsData.infocenterarea='MktCampaigns' and FW_CustomColumnsData.gridid='X' and (DataType like 'udic%' or datatype in ('firm','contact','employee','wbs1','equipment','mkt','textlibrary','account','org', 'dropdown'))
union
Select FW_CustomColumnsData.infocenterarea, FW_CustomColumnsData.gridid, 'PR.'+'desc_'+ name as componentID, 'ProjectCustomTabFields' as tablename, 'desc_'+name as fieldname, cfgscreendesignerdata.tabid, 0 as essential   from FW_CustomColumnsData
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'PR.'+ FW_CustomColumnsData.name
where FW_CustomColumnsData.infocenterarea='Projects' and FW_CustomColumnsData.gridid='X' and (DataType like 'udic%' or datatype in ('firm','contact','employee','wbs1','equipment','mkt','textlibrary','account','org', 'dropdown'))
union
Select FW_CustomColumnsData.infocenterarea, FW_CustomColumnsData.gridid, 'CA.'+'desc_'+ name as componentID, 'AccountCustomTabFields' as tablename,'desc_'+ name as fieldname, cfgscreendesignerdata.tabid , 0 as essential  from FW_CustomColumnsData 
left join  cfgscreendesignerdata on fw_CustomColumnsData.InfoCenterArea =cfgscreendesignerdata.infocenterarea and cfgscreendesignerdata.gridid = FW_CustomColumnsData.gridid
and cfgscreendesignerdata.componentID = 'CA.'+ FW_CustomColumnsData.name
where FW_CustomColumnsData.infocenterarea='ChartOfAccounts' and FW_CustomColumnsData.gridid='X' and ( DataType like 'udic%' or datatype in ('firm','contact','employee','wbs1','equipment','mkt','textlibrary','account','org', 'dropdown'))

--Default Fields
Insert into dbo.#DefaultFields 
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.ContactID' as componentID, 'Contacts' as tablename, 'ContactID' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Salutation' as componentID, 'Contacts' as tablename, 'Salutation' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirstName' as componentID, 'Contacts' as tablename, 'FirstName' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.MiddleName' as componentID, 'Contacts' as tablename, 'MiddleName' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.LastName' as componentID, 'Contacts' as tablename, 'LastName' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Suffix' as componentID, 'Contacts' as tablename, 'Suffix' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.PreferredName' as componentID, 'Contacts' as tablename, 'PreferredName' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Title' as componentID, 'Contacts' as tablename, 'Title' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.ClientID' as componentID, 'Contacts' as tablename, 'ClientID' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.ClientName' as componentID, 'Contacts' as tablename, 'ClientName' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Email' as componentID, 'Contacts' as tablename, 'Email' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.ContactStatus' as componentID, 'Contacts' as tablename, 'ContactStatus' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Phone' as componentID, 'Contacts' as tablename, 'Phone' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.CellPhone' as componentID, 'Contacts' as tablename, 'CellPhone' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmCity' as componentID, 'Contacts' as tablename, 'FirmCity' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.FirmState' as componentID, 'Contacts' as tablename, 'FirmState' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.HasPhoto' as componentID, 'Contacts' as tablename, 'HasPhoto' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.PhotoModDate' as componentID, 'Contacts' as tablename, 'PhotoModDate' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.QualifiedStatus' as componentID, 'Contacts' as tablename, 'QualifiedStatus' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.flName' as componentID, 'Contacts' as tablename, 'flName' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.Owner' as componentID, 'Contacts' as tablename, 'Owner' as fieldname union
Select 'Contacts' as infocenterArea, 'X' as GridID, 'Contacts.emOwnerfl' as componentID, 'Contacts' as tablename, 'emOwnerfl' as fieldname union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ActivityID' as componentID, 'Activity' as tablename, 'ActivityID' as fieldname union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.Type' as componentID, 'Activity' as tablename, 'Type' as fieldname union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.TypeName' as componentID, 'Activity' as tablename, 'TypeName' as fieldname union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.Subject' as componentID, 'Activity' as tablename, 'Subject' as fieldname union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.StartDate' as componentID, 'Activity' as tablename, 'StartDate' as fieldname union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.StartDateObj' as componentID, 'Activity' as tablename, 'StartDateObj' as fieldname union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.InstanceStartDate' as componentID, 'Activity' as tablename, 'InstanceStartDate' as fieldname union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.EndDate' as componentID, 'Activity' as tablename, 'EndDate' as fieldname union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.EndDateObj' as componentID, 'Activity' as tablename, 'EndDateObj' as fieldname union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.Owner' as componentID, 'Activity' as tablename, 'Owner' as fieldname union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.OwnerName' as componentID, 'Activity' as tablename, 'OwnerName' as fieldname union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ActivityTypeFlag' as componentID, 'Activity' as tablename, 'ActivityTypeFlag' as fieldname union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.SingleOccurrenceDateTime' as componentID, 'Activity' as tablename, 'SingleOccurrenceDateTime' as fieldname union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ParentMaxOccurences' as componentID, 'Activity' as tablename, 'ParentMaxOccurences' as fieldname union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ParentRecurrStartDate' as componentID, 'Activity' as tablename, 'ParentRecurrStartDate' as fieldname union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ParentRecurrEndDate' as componentID, 'Activity' as tablename, 'ParentRecurrEndDate' as fieldname union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ParentRecurrEndType' as componentID, 'Activity' as tablename, 'ParentRecurrEndType' as fieldname union 
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ParentRecurrType' as componentID, 'Activity' as tablename, 'ParentRecurrType' as fieldname union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.OccurrenceDate' as componentID, 'Activity' as tablename, 'OccurrenceDate' as fieldname union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.ExceptionType' as componentID, 'Activity' as tablename, 'ExceptionType' as fieldname union
Select 'Activity' as infocenterArea, 'X' as GridID, 'Activity.TimeZoneOffset' as componentID, 'Activity' as tablename, 'TimeZoneOffset' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Employee' as componentID, 'EM' as tablename, 'Employee' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.EmployeeCompany' as componentID, 'EM' as tablename, 'EmployeeCompany' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Org' as componentID, 'EM' as tablename, 'Org' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Status' as componentID, 'EM' as tablename, 'Status' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.HomeCompany' as componentID, 'EM' as tablename, 'HomeCompany' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Salutation' as componentID, 'EM' as tablename, 'Salutation' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.FirstName' as componentID, 'EM' as tablename, 'FirstName' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.MiddleName' as componentID, 'EM' as tablename, 'MiddleName' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.LastName' as componentID, 'EM' as tablename, 'LastName' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Suffix' as componentID, 'EM' as tablename, 'Suffix' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PreferredName' as componentID, 'EM' as tablename, 'PreferredName' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Title' as componentID, 'EM' as tablename, 'Title' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.EMail' as componentID, 'EM' as tablename, 'EMail' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.WorkPhone' as componentID, 'EM' as tablename, 'WorkPhone' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.HomeCompanyName' as componentID, 'EM' as tablename, 'HomeCompanyName' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.EmployeeCompanyName' as componentID, 'EM' as tablename, 'EmployeeCompanyName' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.OrganizationName' as componentID, 'EM' as tablename, 'OrganizationName' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.TitleName' as componentID, 'EM' as tablename, 'TitleName' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.Location' as componentID, 'EM' as tablename, 'Location' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.LocationDescription' as componentID, 'EM' as tablename, 'LocationDescription' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.MobilePhone' as componentID, 'EM' as tablename, 'MobilePhone' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.HasPhoto' as componentID, 'EM' as tablename, 'HasPhoto' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.PhotoModDate' as componentID, 'EM' as tablename, 'PhotoModDate' as fieldname union
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.BillingCategory' as componentID, 'EM' as tablename, 'BillingCategory' as fieldname union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.HoursPerDay' as componentID, 'EM' as tablename, 'HoursPerDay' as fieldname union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.UtilizationRatio' as componentID, 'EM' as tablename, 'UtilizationRatio' as fieldname union 
Select 'Employees' as infocenterArea, 'X' as GridID, 'EM.ReadyForProcessing' as componentID, 'EM' as tablename, 'ReadyForProcessing' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.VendorInd' as componentID, 'Clendor' as tablename, 'VendorInd' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ClientInd' as componentID, 'Clendor' as tablename, 'ClientInd' as fieldname union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Client' as componentID, 'Clendor' as tablename, 'Client' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Name' as componentID, 'Clendor' as tablename, 'Name' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Type' as componentID, 'Clendor' as tablename, 'Type' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Status' as componentID, 'Clendor' as tablename, 'Status' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ClientID' as componentID, 'Clendor' as tablename, 'ClientID' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Owner' as componentID, 'Clendor' as tablename, 'Owner' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Market' as componentID, 'Clendor' as tablename, 'Market' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.HasPhoto' as componentID, 'Clendor' as tablename, 'HasPhoto' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PhotoModDate' as componentID, 'Clendor' as tablename, 'PhotoModDate' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PrimaryCity' as componentID, 'Clendor' as tablename, 'PrimaryCity' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PrimaryState' as componentID, 'Clendor' as tablename, 'PrimaryState' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.PrimaryPhone' as componentID, 'Clendor' as tablename, 'PrimaryPhone' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.emOwner' as componentID, 'Clendor' as tablename, 'emOwner' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.emOwnerfl' as componentID, 'Clendor' as tablename, 'emOwnerfl' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Vendor' as componentID, 'Clendor' as tablename, 'Vendor' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.Category' as componentID, 'Clendor' as tablename, 'Category' as fieldname union
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ReadyForProcessingForCompany' as componentID, 'Clendor' as tablename, 'ReadyForProcessingForCompany' as fieldname union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'Clendor.ReadyForProcessing' as componentID, 'Clendor' as tablename, 'ReadyForProcessing' as fieldname union 
Select 'Firms' as infocenterArea, 'X' as GridID, 'VEAccounting.Vendor' as componentID, 'VEAccounting' as tablename, 'Vendor' as fieldname union
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'CLAddress.ClientID' as componentID, 'CLAddress' as tablename, 'ClientID' as fieldname union
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'CLAddress.CLAddressID' as componentID, 'CLAddress' as tablename, 'CLAddressID' as fieldname union
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'CLAddress.Address' as componentID, 'CLAddress' as tablename, 'Address' as fieldname union
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'CLAddress.Address1' as componentID, 'CLAddress' as tablename, 'Address1' as fieldname union
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'CLAddress.Address2' as componentID, 'CLAddress' as tablename, 'Address2' as fieldname union
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'CLAddress.Address3' as componentID, 'CLAddress' as tablename, 'Address3' as fieldname union
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'CLAddress.Address4' as componentID, 'CLAddress' as tablename, 'Address4' as fieldname union
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'CLAddress.City' as componentID, 'CLAddress' as tablename, 'City' as fieldname union
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'CLAddress.State' as componentID, 'CLAddress' as tablename, 'State' as fieldname union
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'CLAddress.Zip' as componentID, 'CLAddress' as tablename, 'Zip' as fieldname union
Select 'Firms' as infocenterArea, 'CLAddress' as GridID, 'CLAddress.Country' as componentID, 'CLAddress' as tablename, 'Country' as fieldname union
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.CampaignID' as componentID, 'MktCampaign' as tablename, 'CampaignID' as fieldname union
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Name' as componentID, 'MktCampaign' as tablename, 'Name' as fieldname union
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Number' as componentID, 'MktCampaign' as tablename, 'Number' as fieldname union
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emCampaignMgr' as componentID, 'MktCampaign' as tablename, 'emCampaignMgr' as fieldname union
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.emCampaignMgrFL' as componentID, 'MktCampaign' as tablename, 'emCampaignMgrFL' as fieldname union
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.CampaignMgr' as componentID, 'MktCampaign' as tablename, 'CampaignMgr' as fieldname union
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.HasPhoto' as componentID, 'MktCampaign' as tablename, 'HasPhoto' as fieldname union
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.PhotoModDate' as componentID, 'MktCampaign' as tablename, 'PhotoModDate' as fieldname union
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.LaunchDate' as componentID, 'MktCampaign' as tablename, 'LaunchDate' as fieldname union
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.EndDate' as componentID, 'MktCampaign' as tablename, 'EndDate' as fieldname union
Select 'MktCampaigns' as infocenterArea, 'X' as GridID, 'MktCampaign.Status' as componentID, 'MktCampaign' as tablename, 'Status' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.WBS1' as componentID, 'PR' as tablename, 'WBS1' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.WBS2' as componentID, 'PR' as tablename, 'WBS2' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.WBS3' as componentID, 'PR' as tablename, 'WBS3' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProjMgr' as componentID, 'PR' as tablename, 'ProjMgr' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ClientID' as componentID, 'PR' as tablename, 'ClientID' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.EstFees' as componentID, 'PR' as tablename, 'EstFees' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Org' as componentID, 'PR' as tablename, 'Org' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.StartDate' as componentID, 'PR' as tablename, 'StartDate' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.EndDate' as componentID, 'PR' as tablename, 'EndDate' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ChargeType' as componentID, 'PR' as tablename, 'ChargeType' as fieldname union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ClientConfidential' as componentID, 'PR' as tablename, 'ClientConfidential' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ClientAlias' as componentID, 'PR' as tablename, 'ClientAlias' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Revenue' as componentID, 'PR' as tablename, 'Revenue' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.WeightedRevenue' as componentID, 'PR' as tablename, 'WeightedRevenue' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Probability' as componentID, 'PR' as tablename, 'Probability' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Name' as componentID, 'PR' as tablename, 'Name' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Status' as componentID, 'PR' as tablename, 'Status' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ClientName' as componentID, 'PR' as tablename, 'ClientName' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.OrgName' as componentID, 'PR' as tablename, 'OrgName' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProjMgrName' as componentID, 'PR' as tablename, 'ProjMgrName' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.Stage' as componentID, 'PR' as tablename, 'Stage' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.StageStep' as componentID, 'PR' as tablename, 'StageStep' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.StageDescription' as componentID, 'PR' as tablename, 'StageDescription' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.HasPhoto' as componentID, 'PR' as tablename, 'HasPhoto' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.PhotoModDate' as componentID, 'PR' as tablename, 'PhotoModDate' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.IsMainWBS1' as componentID, 'PR' as tablename, 'IsMainWBS1' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.IsBillingGroupWBS1' as componentID, 'PR' as tablename, 'IsBillingGroupWBS1' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.SubLevel' as componentID, 'PR' as tablename, 'SubLevel' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.parentId' as componentID, 'PR' as tablename, 'parentId' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.SiblingWBS1' as componentID, 'PR' as tablename, 'SiblingWBS1' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.key' as componentID, 'PR' as tablename, 'key' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR._RecordAccess' as componentID, 'PR' as tablename, '_RecordAccess' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.WBS1ReadyForProcessing' as componentID, 'PR' as tablename, 'WBS1ReadyForProcessing' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ReadyForProcessing' as componentID, 'PR' as tablename, 'ReadyForProcessing' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ReadyForApproval' as componentID, 'PR' as tablename, 'ReadyForApproval' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.WBSNumber' as componentID, 'PR' as tablename, 'WBSNumber' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillByDefault' as componentID, 'PR' as tablename, 'BillByDefault' as fieldname union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BillableWarning' as componentID, 'PR' as tablename, 'BillableWarning' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.OwnerContactID' as componentID, 'PR' as tablename, 'OwnerContactID' as fieldname union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.OwnerClientID' as componentID, 'PR' as tablename, 'OwnerClientID' as fieldname union 
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.AllocMethodDescription' as componentID, 'PR' as tablename, 'AllocMethodDescription' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.BudgetLevel' as componentID, 'PR' as tablename, 'BudgetLevel' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.ProjectCurrencyCode' as componentID, 'PR' as tablename, 'ProjectCurrencyCode' as fieldname union
Select 'Projects' as infocenterArea, 'X' as GridID, 'PR.UtilizationScheduleFlgDescription' as componentID, 'PR' as tablename, 'UtilizationScheduleFlgDescription' as fieldname union
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.Account' as componentID, 'CA' as tablename, 'Account' as fieldname union
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.AccountCurrencyCode' as componentID, 'CA' as tablename, 'AccountCurrencyCode' as fieldname union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.AccountWithName' as componentID, 'CA' as tablename, 'AccountWithName' as fieldname union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.Status' as componentID, 'CA' as tablename, 'Status' as fieldname union
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.Name' as componentID, 'CA' as tablename, 'Name' as fieldname union
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.Type' as componentID, 'CA' as tablename, 'Type' as fieldname union
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.TypeDesc' as componentID, 'CA' as tablename, 'TypeDesc' as fieldname union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.StatusDesc' as componentID, 'CA' as tablename, 'StatusDesc' as fieldname union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.GlobalAccount' as componentID, 'CA' as tablename, 'GlobalAccount' as fieldname union 
Select 'ChartOfAccounts' as infocenterArea, 'X' as GridID, 'CA.WarningMsg' as componentID, 'CA' as tablename, 'WarningMsg' as fieldname union 
Select 'TextLibrary' as infocenterArea, 'X' as GridID, 'TextLibrary.Name' as componentID, 'TextLibrary' as tablename, 'Name' as fieldname union
Select 'TextLibrary' as infocenterArea, 'X' as GridID, 'TextLibrary.DisplayName' as componentID, 'TextLibrary' as tablename, 'DisplayName' as fieldname 
union
---Get all the record id and name fields for UDICs
select infocenterarea, gridid,FW_UDICData.TableName +'.'+ FW_CustomColumnsData.name as componentID,
FW_UDICData.TableName as tablename,FW_CustomColumnsData.name  as fieldname from fw_CustomColumnsData inner join FW_UDICData
on FW_UDICData.UDIC_ID  = FW_CustomColumnsData.infocenterarea
where datatype='recordid' or datatype='name'
union
--- Get all the UDIC_UID, UDICName, and UDICNumber fields for the UDICs
select FW_UDICData.UDIC_ID, 'X' as gridid,FW_UDICData.TableName +'.UDIC_UID' as componentID,
FW_UDICData.TableName as tablename,'UDIC_UID'  as fieldname from  FW_UDICData
union
select FW_UDICData.UDIC_ID, 'X' as gridid,FW_UDICData.TableName +'.UDICName' as componentID,
FW_UDICData.TableName as tablename,'UDICName'  as fieldname from  FW_UDICData
union
select FW_UDICData.UDIC_ID, 'X' as gridid,FW_UDICData.TableName +'.UDICNumber' as componentID,
FW_UDICData.TableName as tablename,'UDICNumber'  as fieldname from  FW_UDICData


DECLARE rolecursor CURSOR FOR 
  SELECT role FROM se 
  
  WHERE Role = @SpecificRole OR @SpecificRole IS NULL
BEGIN 
    OPEN rolecursor 

    FETCH next FROM rolecursor INTO @Role 

    WHILE @@FETCH_STATUS = 0 
      BEGIN 
		---Make a list of hubs including the UDICs
          DECLARE hubscursor CURSOR FOR 
            SELECT areas.infocenterarea, 
                   CASE 
                     WHEN se.accessallnavnodes = 'Y' THEN 'Y' 
                     WHEN senavtree.nodeid IS NOT NULL THEN 'Y' 
                     ELSE 'N' 
                   END AS access,
				   se.accessalltabs 
            FROM   (SELECT 'contacts' AS infocenterArea UNION 
                    SELECT 'Activity' UNION 
                    SELECT 'TextLibrary' UNION 
                    SELECT 'Employees' UNION 
                    SELECT 'Firms' UNION 
                    SELECT 'MktCampaigns' UNION 
                    SELECT 'Projects' UNION 
                    SELECT 'ChartOfAccounts' UNION 
                    SELECT udic_id 
                    FROM   fw_udicdata) areas 
                   LEFT JOIN senavtree ON areas.infocenterarea = senavtree.nodeid AND senavtree.role = @Role, se 
            WHERE  se.role = @Role 


          BEGIN 
              OPEN hubscursor 
              FETCH next FROM hubscursor INTO @InfocenterArea, @Access, @AccessAllTabs 
              WHILE @@FETCH_STATUS = 0 
                BEGIN 
					---If the role has access to the hub then add all the fields that the role 
					---does not have hidden
                    IF @Access = 'Y'   
                      BEGIN 
                          INSERT INTO sefield (role, infocenterarea, gridid, componentid, tablename, columnname, readonly, createuser, createdate, moduser, moddate) 
                          SELECT DISTINCT @role, fields.infocenterarea, fields.gridid, fields.componentid, tablename, fieldname, 
                                 CASE WHEN Charindex(',_ALL_,', ',' + Isnull(lockedfor, '') + ',') > 0 OR Charindex(',' + @Role + ',', ',' + lockedfor + ',') > 0 THEN 'Y'
                                 WHEN  @applyTabSecurity ='Y' AND  @AccessAllTabs='N' AND fields.essential=1 AND NOT (fields.tabID  in ('overview','general','sidebar','Generaltab', 'noassociation') or (fields.tabID ='custGeneral' and @InfocenterArea like 'UDIC%'))  THEN 'Y'
                                 ELSE 'N' END, 
                                 'Conversion', Getdate(), 'Conversion', Getdate() 
                          FROM   dbo.#KnownFields fields 
                                 LEFT JOIN cfgscreendesignerdata sd ON fields.infocenterarea = sd.infocenterarea AND fields.gridid = sd.gridid AND fields.componentid = sd.componentid 
                          WHERE  Charindex(',_ALL_,', ',' + Isnull(hiddenfor, '') + ',') = 0
                            AND  Charindex(',' + @Role + ',', ',' + Isnull(hiddenfor, '') + ',') = 0 
                            AND  fields.infocenterarea = @InfocenterArea 
                            AND  (fields.tabID in ('overview','general','sidebar','Generaltab', 'noassociation') or (fields.tabID ='custGeneral' and @InfocenterArea like 'UDIC%') or fields.essential=1  or  @AccessAllTabs='Y' or @applyTabSecurity='N')

						  ---Add any components that are in the screen designer table that the role has access to
                          INSERT INTO sefield (role, infocenterarea, gridid, componentid, tablename, columnname, readonly, createuser, createdate, moduser, moddate) 
                          SELECT DISTINCT @role, sd.infocenterarea, sd.gridid, sd.componentid, NULL, NULL, 
                                 CASE WHEN Charindex(',_ALL_,', ',' + Isnull(lockedfor, '') + ',') > 0 OR Charindex(',' + @Role + ',', ',' + lockedfor + ',') > 0 THEN 'Y' ELSE 'N' END, 
                                 'Conversion', Getdate(), 'Conversion', Getdate() 
                          FROM   cfgscreendesignerdata sd 
                          WHERE  NOT EXISTS (SELECT 'X' FROM dbo.#KnownFields fields 
						                      WHERE fields.infocenterarea = sd.infocenterarea AND fields.gridid = sd.gridid AND fields.componentid = sd.componentid) 
                                                AND Charindex(',_ALL_,', ',' + Isnull(hiddenfor, '') + ',') = 0 AND Charindex(',' + @Role + ',', ',' + Isnull(hiddenfor, '') + ',') = 0 
                                                AND sd.infocenterarea = @InfocenterArea 
                      END 
                    ELSE 
					  --else the role does not have access so just add the default fields for that hub
                      BEGIN 
                          INSERT INTO sefield (role, infocenterarea, gridid, componentid, tablename, columnname, readonly, createuser, createdate, moduser, moddate) 
                          SELECT @role, fields.infocenterarea, fields.gridid, fields.componentid, tablename, fieldname, 'Y', 'Conversion', Getdate(), 'Conversion', Getdate() 
						  FROM   dbo.#DefaultFields fields 
                          WHERE  fields.infocenterarea = @InfocenterArea 
                      END 

                    FETCH next FROM hubscursor INTO @InfocenterArea, @Access, @AccessAllTabs 
                END 
          END 

          CLOSE hubscursor 
          DEALLOCATE hubscursor 

          FETCH next FROM rolecursor INTO @Role 
      END 
END 

CLOSE rolecursor 
DEALLOCATE rolecursor 

DROP TABLE dbo.#KnownFields 
DROP TABLE dbo.#DefaultFields 

PRINT ' '
PRINT 'Load of SEField Defaults complete'
	
END
GO
