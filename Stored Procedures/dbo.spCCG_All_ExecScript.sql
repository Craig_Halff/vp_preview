SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_All_ExecScript] (
	@Script	varchar(max)
)
AS BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	declare @value nvarchar(max) = ''
	create table #tableScript (script varchar(max))

	declare curExecScript cursor fast_forward for select value from dbo.fnCCG_SplitString(@Script, 'GO--')
	open curExecScript
	fetch next from curExecScript into @value
	while @@FETCH_STATUS = 0
	begin
		--select @value
		if rtrim(replace(replace(replace(ltrim(@value), char(13), ''), char(10), ''), '--REMOVEME--', '')) <> ''
			exec (@value)
		fetch next from curExecScript into @value
	end
	close curExecScript
	deallocate curExecScript

	select * from #tableScript
	drop table #tableScript
END
GO
