SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPChangeDuration]
  @strRowID nvarchar(255),
  @strNewStartDate varchar(8),
  @strNewEndDate varchar(8),
  @intShiftMode int, /* 1 = Maintain Pattern, 2 = Distribute Evenly, 3 = No Redistribute */
  @bitMatchChildrenDates bit = 0 /* When 1, makes all children dates match new parent dates. */
AS

BEGIN -- Procedure stRPChangeDuration

/*

  The objective of this SP is to change the durations of WBS rows as well as Assignment rows in the branch that is pointed to by @strRowID.

  1. Which WBS rows and which Assignment rows will be changed are determined by the {StartDate, EndDate} range of those rows.
  2. The new Start or End Date can be reduced down to one working day for all WBS, Assigment, and TPD rows within the selection criteria.
  3. If a row has Start Date later than NewStartDate and End Date earlier than NewEndDate then the row and its decendants will nor be affected. 

*/

  SET NOCOUNT ON
  
  DECLARE @dtOldTopStartDate datetime
  DECLARE @dtOldTopEndDate datetime
  DECLARE @dtNewStartDate datetime
  DECLARE @dtNewEndDate datetime

  DECLARE @dtMinNewDate datetime
  DECLARE @dtMaxNewDate datetime
  DECLARE @dt_MINDATE datetime = CONVERT(datetime, '')
  DECLARE @dt_MAXDATE datetime = CONVERT(datetime, '99991231')
   
  DECLARE @strCompany nvarchar(14)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strAssignmentID varchar(32)
  DECLARE @strResourceID nvarchar(20)
  DECLARE @strGenericResourceID nvarchar(20)
  DECLARE @strResourceType varchar(1)
  DECLARE @strUserName nvarchar(32)
  DECLARE @strAECFlg varchar(1) = ''

  DECLARE @siGRMethod smallint
  DECLARE @siCostRtMethod smallint
  DECLARE @siBillingRtMethod smallint
  
  DECLARE @intCostRtTableNo int
  DECLARE @intBillingRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int

  DECLARE @siLabRateDecimals smallint = 4

  DECLARE @tabAssignment TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ResourceID nvarchar(20) COLLATE database_default,
    GenericResourceID nvarchar(20) COLLATE database_default,
    Category smallint,
    GRLBCD nvarchar(14) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    CostRate decimal(19,4),
    BillingRate decimal(19,4),
    AT_OutlineNumber varchar(255) COLLATE database_default
    UNIQUE(PlanID, TaskID, AssignmentID)
  )

  DECLARE @tabASGTPD TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ASGResourceID nvarchar(20) COLLATE database_default,
    ASGCategory smallint,
    ASGGRLBCD nvarchar(14) COLLATE database_default,
    ASGCostRate decimal(19,4),
    ASGBillingRate decimal(19,4),
    ASGStartDate datetime,
    ASGEndDate datetime,
    MinTPDDate datetime,
    MaxTPDDate datetime,
    SumHrs decimal(19,4)
    UNIQUE(PlanID, TaskID, AssignmentID, MinTPDDate, MaxTPDDate, SumHrs)
  )
  
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Parse @strRowID
  
  SELECT
    @strPlanID = PlanID,
    @strTaskID = TaskID,
    @strAssignmentID = AssignmentID,
    @strResourceID = ResourceID,
    @strGenericResourceID = GenericResourceID,
    @strAECFlg = AECFlg
    FROM dbo.stRP$tabParseRowID(@strRowID)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Get various parameters.

  SELECT 
    @strCompany = P.Company,
    @dtMinNewDate = T.StartDate,
    @dtMaxNewDate = T.EndDate,
    @dtOldTopStartDate = T.StartDate,
    @dtOldTopEndDate = T.EndDate,
    @siGRMethod = P.GRMethod,
    @intGenResTableNo = P.GenResTableNo,
    @intGRBillTableNo = P.GRBillTableNo,
    @siCostRtMethod = P.CostRtMethod,
    @siBillingRtMethod = P.BillingRtMethod,
    @intCostRtTableNo = P.CostRtTableNo,
    @intBillingRtTableNo = P.BillingRtTableNo
    FROM PNTask AS T
      INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID
    WHERE T.TaskID = @strTaskID

  -- Set Date limits.
  -- Only the selected duration to be shrunk to one calendar day either at the EndDate or StartDate of the selected row.  

  SET @dtNewStartDate = 
    CASE
      WHEN DATALENGTH(@strNewStartDate) = 0 THEN @dtMinNewDate
      ELSE CONVERT(datetime, @strNewStartDate)
    END

  SET @dtNewEndDate = 
    CASE
      WHEN DATALENGTH(@strNewEndDate) = 0 THEN @dtMaxNewDate
      ELSE CONVERT(datetime, @strNewEndDate)
    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- If RowID is pointing to an Expense or a Consultant row then do not disturb the Labor Assignment or TPD.

  IF (@strAECFlg NOT IN ('E', 'C'))
    BEGIN

      -- Save off Assignment rows that are in the selected branch

      INSERT @tabAssignment(
        PlanID,
        TaskID,
        AssignmentID,
        ResourceID,
        GenericResourceID,
        Category,
        GRLBCD,
        StartDate,
        EndDate,
        CostRate,
        BillingRate,
        AT_OutlineNumber
      )
        SELECT DISTINCT
          A.PlanID,
          A.TaskID,
          A.AssignmentID,
          A.ResourceID,
          A.GenericResourceID,
          A.Category,
          A.GRLBCD,
          A.StartDate,
          A.EndDate,
          A.CostRate,
          A.BillingRate,
          AT.OutlineNumber AS AT_OutlineNumber
          FROM PNAssignment AS A
            INNER JOIN PNTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
            INNER JOIN PNTask AS PT ON A.PlanID = PT.PlanID 
          WHERE
            A.PlanID = @strPlanID AND
            PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
            AT.OutlineNumber LIKE PT.OutlineNumber + '%' AND
            ((ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') 
              AND ISNULL(A.GenericResourceID, '|') = ISNULL( @strGenericResourceID, '|'))
              OR (@strResourceID IS NULL AND @strGenericResourceID IS NULL))

          -- Remember AssignmentID of the row that has RowID, if any.
      
          SELECT 
            @strAssignmentID = A.AssignmentID
            FROM @tabAssignment AS A
            WHERE ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') 
              AND ISNULL(A.GenericResourceID, '|') = ISNULL( @strGenericResourceID, '|')
      
          -- Group TPD by Assignment.

          INSERT @tabASGTPD(
            PlanID,
            TaskID,
            AssignmentID,
            ASGResourceID,
            ASGCategory,
            ASGGRLBCD,
            ASGCostRate,
            ASGBillingRate,
            ASGStartDate,
            ASGEndDate,
            MinTPDDate,
            MaxTPDDate,
            SumHrs
          )
            SELECT DISTINCT
              A.PlanID AS PlanID,
              A.TaskID AS TaskID,
              A.AssignmentID AS AssignmentID,
              A.ResourceID AS ASGResourceID,
              A.Category AS ASGCategory,
              A.GRLBCD AS ASGGRLBCD,
              A.CostRate AS ASGCostRate,
              A.BillingRate AS ASGBillingRate,
              A.StartDate AS ASGStartDate,
              A.EndDate AS ASGEndDate,
              MIN(TPD.StartDate) AS MinTPDDate,
              MAX(TPD.EndDate) AS MaxTPDDate,
              SUM(TPD.PeriodHrs) AS SumHrs
              FROM PNPlannedLabor AS TPD
                INNER JOIN @tabAssignment AS A
                  ON A.PlanID = TPD.PlanID AND A.TaskID = TPD.TaskID AND A.AssignmentID = TPD.AssignmentID AND TPD.AssignmentID IS NOT NULL
              WHERE TPD.PlanID = @strPlanID
              GROUP BY A.PlanID, A.TaskID, A.AssignmentID, A.ResourceID, 
                A.Category, A.GRLBCD, A.CostRate, A.BillingRate, A.StartDate, A.EndDate

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Redistribute TPD evenly only when Shift Mode is 2.

      IF (@intShiftMode = 2)
        BEGIN

          -- Delete existing TPD under Assigments collected earlier in @tabAssignment.

          DELETE PNPlannedLabor
            WHERE PlanID = @strPlanID AND 
            AssignmentID IN (SELECT DISTINCT AssignmentID FROM @tabAssignment)

          -- Insert new TPD, one for each AssignmentID in @tabASGTPD and @tabAssignment

          IF (DATALENGTH(@strAECFlg) = 0)
            BEGIN

              INSERT PNPlannedLabor(
                TimePhaseID,
                PlanID, 
                TaskID,
                AssignmentID,
                StartDate, 
                EndDate, 
                PeriodHrs,
                CreateUser,
                ModUser,
                CreateDate,
                ModDate
              )
                SELECT
                REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
                TPD.PlanID AS PlanID, 
                TPD.TaskID AS TaskID,
                TPD.AssignmentID AS AssignmentID,
                CASE
                  WHEN (@bitMatchChildrenDates = 1 OR ASGStartDate = @dtOldTopStartDate) THEN @dtNewStartDate
                  WHEN (@dtNewStartDate > TPD.MinTPDDate) THEN @dtNewStartDate
                  WHEN (@dtNewEndDate < TPD.MinTPDDate) THEN @dtNewEndDate
                  ELSE TPD.MinTPDDate
                END AS StartDate,
                CASE
                  WHEN (@bitMatchChildrenDates = 1 OR ASGEndDate = @dtOldTopEndDate) THEN @dtNewEndDate
                  WHEN (@dtNewEndDate < TPD.MaxTPDDate) THEN @dtNewEndDate
                  WHEN (@dtNewStartDate > TPD.MaxTPDDate) THEN @dtNewStartDate
                  ELSE TPD.MaxTPDDate
                END AS EndDate,
                TPD.SumHrs AS PeriodHrs,
                CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stPNShiftDates' ELSE 'SDT_' + @strUserName END AS CreateUser,
                CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stPNShiftDates' ELSE 'SDT_' + @strUserName END AS ModUser,
                LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As CreateDate,
                LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As ModDate 
              FROM @tabASGTPD AS TPD

            END /* END IF (DATALENGTH(@strAECFlg) = 0) */

          ELSE IF (@strAECFlg = 'A' AND @dtNewStartDate <= @dtNewEndDate)
            BEGIN

              INSERT PNPlannedLabor(
                TimePhaseID,
                PlanID, 
                TaskID,
                AssignmentID,
                StartDate, 
                EndDate, 
                PeriodHrs,
                CreateUser,
                ModUser,
                CreateDate,
                ModDate
              )
                SELECT
                REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
                TPD.PlanID AS PlanID, 
                TPD.TaskID AS TaskID,
                TPD.AssignmentID AS AssignmentID,
                @dtNewStartDate AS StartDate,
                @dtNewEndDate AS EndDate,
                TPD.SumHrs AS PeriodHrs,
                CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stPNShiftDates' ELSE 'SDT_' + @strUserName END AS CreateUser,
                CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stPNShiftDates' ELSE 'SDT_' + @strUserName END AS ModUser,
                LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As CreateDate,
                LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As ModDate 
              FROM @tabASGTPD AS TPD

            END /* END ELSE IF (DATALENGTH(@strAECFlg) = 'A') */

        END /* END IF (@intShiftMode = 2) */

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      UPDATE A SET
        StartDate = 
          CASE
            WHEN (DATALENGTH(@strNewStartDate) <> 0 
              AND (@bitMatchChildrenDates = 1 OR A.AssignmentID = ISNULL(@strAssignmentID, '|') OR A.StartDate = @dtOldTopStartDate)) THEN @dtNewStartDate    
            WHEN (DATALENGTH(@strNewStartDate) = 0 AND A.StartDate <= @dtNewEndDate) THEN A.StartDate
            WHEN (A.StartDate > @dtNewEndDate) THEN @dtNewEndDate
            WHEN (DATALENGTH(@strNewStartDate) <> 0 AND @dtNewStartDate > A.StartDate) THEN @dtNewStartDate
            WHEN (DATALENGTH(@strNewStartDate) <> 0 AND @dtNewStartDate <= A.StartDate) THEN A.StartDate
          END,

        EndDate = 
          CASE
            WHEN (DATALENGTH(@strNewEndDate) <> 0 
              AND (@bitMatchChildrenDates = 1 OR A.AssignmentID = ISNULL(@strAssignmentID, '|') OR A.EndDate = @dtOldTopEndDate)) THEN @dtNewEndDate
            WHEN (DATALENGTH(@strNewEndDate) = 0 AND A.EndDate >= @dtNewStartDate) THEN A.EndDate
            WHEN (A.EndDate < @dtNewStartDate) THEN @dtNewStartDate
            WHEN (DATALENGTH(@strNewEndDate) <> 0 AND @dtNewEndDate < A.EndDate) THEN @dtNewEndDate
            WHEN (DATALENGTH(@strNewEndDate) <> 0 AND @dtNewEndDate >= A.EndDate) THEN A.EndDate
          END     
         
        FROM PNAssignment AS A
          INNER JOIN @tabAssignment AS XA
            ON A.PlanID = XA.PlanID AND A.TaskID = XA.TaskID AND A.AssignmentID = XA.AssignmentID

    END /* END IF (@strAECFlg NOT IN ('E', 'C')) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- If RowID is pointing to an Assignment row, then do not change Task {StartDate, EndDate}.

  IF (DATALENGTH(@strAECFlg) = 0)
    BEGIN

      UPDATE CT SET
        StartDate = 
          CASE
            WHEN (DATALENGTH(@strNewStartDate) <> 0 
              AND (@bitMatchChildrenDates = 1 OR CT.TaskID = ISNULL(@strTaskID, '|') OR CT.StartDate = @dtOldTopStartDate)) THEN @dtNewStartDate
            WHEN (DATALENGTH(@strNewStartDate) = 0 AND CT.StartDate <= @dtNewEndDate) THEN CT.StartDate
            WHEN (CT.StartDate > @dtNewEndDate) THEN @dtNewEndDate
            WHEN (DATALENGTH(@strNewStartDate) <> 0 AND @dtNewStartDate > CT.StartDate) THEN @dtNewStartDate
            WHEN (DATALENGTH(@strNewStartDate) <> 0 AND @dtNewStartDate <= CT.StartDate) THEN CT.StartDate
          END,

        EndDate = 
          CASE
            WHEN (DATALENGTH(@strNewEndDate) <> 0 
              AND (@bitMatchChildrenDates = 1 OR CT.TaskID = ISNULL(@strTaskID, '|') OR CT.EndDate = @dtOldTopEndDate)) THEN @dtNewEndDate
            WHEN (DATALENGTH(@strNewEndDate) = 0 AND CT.EndDate >= @dtNewStartDate) THEN CT.EndDate
            WHEN (CT.EndDate < @dtNewStartDate) THEN @dtNewStartDate
            WHEN (DATALENGTH(@strNewEndDate)<> 0 AND @dtNewEndDate < CT.EndDate) THEN @dtNewEndDate
            WHEN (DATALENGTH(@strNewEndDate)<> 0 AND @dtNewEndDate >= CT.EndDate) THEN CT.EndDate
          END,		    
		ModDate=LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19),
		ModUser=@strUserName
        FROM PNTask AS PT
          INNER JOIN PNTask AS CT ON PT.PlanID = CT.PlanID

        WHERE
          PT.PlanID = @strPlanID AND PT.TaskID = @strTaskID AND
          CT.OutlineNumber LIKE PT.OutlineNumber + '%'

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      -- Shifting Expense and Consultant data.
      -- This only needs to be done when the shifting occured at a WBS row.
      -- User cannot shift date on an Expense or Consultant row.

      -- Shifting Expense data.

      UPDATE TPD SET
        StartDate = ET.StartDate,
        EndDate = ET.EndDate
        FROM PNPlannedExpenses AS TPD
          INNER JOIN PNTask AS ET ON TPD.PlanID = ET.PlanID AND TPD.TaskID = ET.TaskID AND TPD.ExpenseID IS NOT NULL
          INNER JOIN PNTask AS PT ON ET.PlanID = PT.PlanID AND ET.OutlineNumber LIKE PT.OutlineNumber + '%'
        WHERE TPD.PlanID = @strPlanID AND PT.TaskID = @strTaskID 

      UPDATE E SET
        StartDate = ET.StartDate,
        EndDate = ET.EndDate
        FROM PNExpense AS E
          INNER JOIN PNTask AS ET ON E.PlanID = ET.PlanID AND E.TaskID = ET.TaskID
          INNER JOIN PNTask AS PT ON ET.PlanID = PT.PlanID AND ET.OutlineNumber LIKE PT.OutlineNumber + '%'
        WHERE E.PlanID = @strPlanID AND PT.TaskID = @strTaskID

      -- Shifting Consultant data.

      UPDATE TPD SET
        StartDate = CT.StartDate,
        EndDate = CT.EndDate
        FROM PNPlannedConsultant AS TPD
          INNER JOIN PNTask AS CT ON TPD.PlanID = CT.PlanID AND TPD.TaskID = CT.TaskID AND TPD.ConsultantID IS NOT NULL
          INNER JOIN PNTask AS PT ON CT.PlanID = PT.PlanID AND CT.OutlineNumber LIKE PT.OutlineNumber + '%'
        WHERE TPD.PlanID = @strPlanID AND PT.TaskID = @strTaskID

      UPDATE C SET
        StartDate = CT.StartDate,
        EndDate = CT.EndDate
        FROM PNConsultant AS C
          INNER JOIN PNTask AS CT ON C.PlanID = CT.PlanID AND C.TaskID = CT.TaskID
          INNER JOIN PNTask AS PT ON CT.PlanID = PT.PlanID AND CT.OutlineNumber LIKE PT.OutlineNumber + '%'
        WHERE C.PlanID = @strPlanID AND PT.TaskID = @strTaskID

    END /* END IF (DATALENGTH(@strAECFlg) = 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
  SET NOCOUNT OFF

END -- stRPChangeDuration
GO
