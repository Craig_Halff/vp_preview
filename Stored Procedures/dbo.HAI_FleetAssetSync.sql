SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--SET QUOTED_IDENTIFIER ON|OFF
--SET ANSI_NULLS ON|OFF
--GO
CREATE PROCEDURE [dbo].[HAI_FleetAssetSync]

-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| 'user_name'
AS
BEGIN

    /****************************************************************************
    Copyright (c) 2021 Halff Associates Inc. All rights reserved.
    Sync Fleet management vehicle entriews to their Asset records.

    01/11/2021	Craig H. Anderson - Created
*****************************************************************************/
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    --SET QUOTED_IDENTIFIER ON|OFF
    --SET ANSI_NULLS ON|OFF
    --GO
    BEGIN TRANSACTION;
    UPDATE  fm
    SET fm.CustStatus = e.AssetStatus
      , fm.CustDisposed = e.DisposalDate
      , fm.CustStartDate = e.InServiceDate
      , fm.CustActive = e.AcquisitionDate
      , fm.CustOrganization = p.Org
    FROM    dbo.UDIC_FleetManagement AS fm
    INNER JOIN dbo.Equipment         AS e
        ON fm.CustAsset = e.EquipmentNumber
    INNER JOIN dbo.PR                p
        ON e.WBS1 = p.WBS1
           AND  e.WBS2 = p.WBS2
           AND  e.WBS3 = p.WBS3
    WHERE   fm.CustStatus <> e.AssetStatus
            OR  fm.CustDisposed <> e.DisposalDate
            OR  fm.CustStartDate <> e.InServiceDate
            OR  fm.CustOrganization <> p.Org;
    COMMIT TRANSACTION;
END;
GO
