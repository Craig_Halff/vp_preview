SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPConvertIAccessToDeltekPS]
AS

BEGIN -- Procedure stRPConvertIAccessToDeltekPS

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- This stored procedure is used to convert data from DRP format to DeltekPS format. 
-- The DRP DB must have DeltekPS 1.0 Schema applied already.
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime

  SELECT 
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE())))
    FROM CFGRMSettings

  SET @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

BEGIN TRANSACTION

  -- At Task level, the following PR fields will be updated.

  -- PR.Name AS Name,
  -- PR.ProjMgr AS ProjMgr,
  -- PR.ChargeType AS ChargeType,
  -- PR.ProjectType AS ProjectType,
  -- PR.ClientID AS ClientID,
  -- PR.Org AS Org,
  -- PR.Status AS Status,

  -- PR.Fee AS CompensationFee,
  -- PR.ConsultFee AS ConsultantFee,
  -- PR.ReimbAllow AS ReimbAllowance,

  -- PR.FeeBillingCurrency AS CompensationFeeBill,
  -- PR.ConsultFeeBillingCurrency AS ConsultantFeeBill,
  -- PR.ReimbAllowBillingCurrency AS ReimbAllowanceBill,

  -- PR.FeeDirLab AS CompensationFeeDirLab,
  -- PR.FeeDirExp AS CompensationFeeDirExp,
  -- PR.ReimbAllowExp AS ReimbAllowanceExp,
  -- PR.ReimbAllowCons AS ReimbAllowanceCon,

  -- PR.FeeDirLabBillingCurrency AS CompensationFeeDirLabBill,
  -- PR.FeeDirExpBillingCurrency AS CompensationFeeDirExpBill,
  -- PR.ReimbAllowExpBillingCurrency AS ReimbAllowanceExpBill,
  -- PR.ReimbAllowConsBillingCurrency AS ReimbAllowanceConBill,

  UPDATE PNTask SET
    PNTask.Name = PR.Name,
    PNTask.ProjMgr = PR.ProjMgr,
    PNTask.ChargeType = PR.ChargeType,
    PNTask.ProjectType = PR.ProjectType,
    PNTask.ClientID = PR.ClientID,
    PNTask.Org = PR.Org,
    PNTask.Status = PR.Status,

    PNTask.CompensationFee = PR.Fee,
    PNTask.ConsultantFee = PR.ConsultFee,
    PNTask.ReimbAllowance = PR.ReimbAllow,

    PNTask.CompensationFeeBill = PR.FeeBillingCurrency,
    PNTask.ConsultantFeeBill = PR.ConsultFeeBillingCurrency,
    PNTask.ReimbAllowanceBill = PR.ReimbAllowBillingCurrency,

    PNTask.CompensationFeeDirLab = PR.FeeDirLab,
    PNTask.CompensationFeeDirExp = PR.FeeDirExp,
    PNTask.ReimbAllowanceExp = PR.ReimbAllowExp,
    PNTask.ReimbAllowanceCon = PR.ReimbAllowCons,

    PNTask.CompensationFeeDirLabBill = PR.FeeDirLabBillingCurrency,
    PNTask.CompensationFeeDirExpBill = PR.FeeDirExpBillingCurrency,
    PNTask.ReimbAllowanceExpBill = PR.ReimbAllowExpBillingCurrency,
    PNTask.ReimbAllowanceConBill = PR.ReimbAllowConsBillingCurrency
  FROM   pr 
    INNER JOIN pntask 
      ON pntask.wbs1 = pr.wbs1 
        AND ISNULL(pntask.wbs2, ' ') = pr.wbs2 
        AND ISNULL(pntask.wbs3, ' ') = pr.wbs3 
        AND pr.planid = pntask.planid 

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  UPDATE RPTask SET
    RPTask.Name = PR.Name,
    RPTask.ProjMgr = PR.ProjMgr,
    RPTask.ChargeType = PR.ChargeType,
    RPTask.ProjectType = PR.ProjectType,
    RPTask.ClientID = PR.ClientID,
    RPTask.Org = PR.Org,
    RPTask.Status = PR.Status,

    RPTask.CompensationFee = PR.Fee,
    RPTask.ConsultantFee = PR.ConsultFee,
    RPTask.ReimbAllowance = PR.ReimbAllow,

    RPTask.CompensationFeeBill = PR.FeeBillingCurrency,
    RPTask.ConsultantFeeBill = PR.ConsultFeeBillingCurrency,
    RPTask.ReimbAllowanceBill = PR.ReimbAllowBillingCurrency,

    RPTask.CompensationFeeDirLab = PR.FeeDirLab,
    RPTask.CompensationFeeDirExp = PR.FeeDirExp,
    RPTask.ReimbAllowanceExp = PR.ReimbAllowExp,
    RPTask.ReimbAllowanceCon = PR.ReimbAllowCons,

    RPTask.CompensationFeeDirLabBill = PR.FeeDirLabBillingCurrency,
    RPTask.CompensationFeeDirExpBill = PR.FeeDirExpBillingCurrency,
    RPTask.ReimbAllowanceExpBill = PR.ReimbAllowExpBillingCurrency,
    RPTask.ReimbAllowanceConBill = PR.ReimbAllowConsBillingCurrency
  FROM   PR 
    INNER JOIN RPTask ON RPTask.WBS1 = PR.WBS1 
        AND ISNULL(RPTask.WBS2, ' ') = PR.WBS2
        AND ISNULL(RPTask.WBS3, ' ') = PR.WBS3 
        AND PR.PlanID = RPTask.PlanID 
	INNER JOIN PNPlan ON PNPlan.PlanID = RPTask.PlanID
      
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  UPDATE PN SET
    PlanName = PR.Name,
    PlanNumber = PR.WBS1,
    ProjMgr = PR.ProjMgr,
    Principal = PR.Principal,
    Supervisor = PR.Supervisor,
    OpportunityID = NULL,
    ClientID = PR.ClientID,
    Org = PR.Org,
    Status = PR.Status,

    CompensationFee = PR.Fee,
    ConsultantFee = PR.ConsultFee,
    ReimbAllowance = PR.ReimbAllow,

    CompensationFeeBill = PR.FeeBillingCurrency,
    ConsultantFeeBill = PR.ConsultFeeBillingCurrency,
    ReimbAllowanceBill = PR.ReimbAllowBillingCurrency,

    CompensationFeeDirLab = PR.FeeDirLab,
    CompensationFeeDirExp = PR.FeeDirExp,
    ReimbAllowanceExp = PR.ReimbAllowExp,
    ReimbAllowanceCon = PR.ReimbAllowCons,

    CompensationFeeDirLabBill = PR.FeeDirLabBillingCurrency,
    CompensationFeeDirExpBill = PR.FeeDirExpBillingCurrency,
    ReimbAllowanceExpBill = PR.ReimbAllowExpBillingCurrency,
    ReimbAllowanceConBill = PR.ReimbAllowConsBillingCurrency

  FROM PNPlan AS PN
    INNER JOIN PR ON PN.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' 

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  UPDATE RP SET
    PlanName = PR.Name,
    PlanNumber = PR.WBS1,
    ProjMgr = PR.ProjMgr,
    Principal = PR.Principal,
    Supervisor = PR.Supervisor,
    OpportunityID = NULL,
    ClientID = PR.ClientID,
    Org = PR.Org,
    Status = PR.Status,

    CompensationFee = PR.Fee,
    ConsultantFee = PR.ConsultFee,
    ReimbAllowance = PR.ReimbAllow,

    CompensationFeeBill = PR.FeeBillingCurrency,
    ConsultantFeeBill = PR.ConsultFeeBillingCurrency,
    ReimbAllowanceBill = PR.ReimbAllowBillingCurrency,

    CompensationFeeDirLab = PR.FeeDirLab,
    CompensationFeeDirExp = PR.FeeDirExp,
    ReimbAllowanceExp = PR.ReimbAllowExp,
    ReimbAllowanceCon = PR.ReimbAllowCons,

    CompensationFeeDirLabBill = PR.FeeDirLabBillingCurrency,
    CompensationFeeDirExpBill = PR.FeeDirExpBillingCurrency,
    ReimbAllowanceExpBill = PR.ReimbAllowExpBillingCurrency,
    ReimbAllowanceConBill = PR.ReimbAllowConsBillingCurrency

  FROM RPPlan AS RP
    INNER JOIN PR ON RP.WBS1 = PR.WBS1 AND PR.WBS2 = ' ' AND PR.WBS3 = ' ' 
    INNER JOIN PNPlan AS PN ON RP.PlanID = PN.PlanID AND RP.WBS1 = PN.WBS1 AND RP.UtilizationIncludeFlg = PN.UtilizationIncludeFlg

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
  -- Move PRAdditionalData data to RPTask.Notes

  UPDATE PNTask 
  SET    PNTask.notes = PA.Notes
  FROM   PRAdditionalData PA
         INNER JOIN PNTask 
                 ON pntask.wbs1 = PA.wbs1 
                    AND ( pntask.wbs2 = PA.wbs2 
                           OR ( pntask.wbs2 IS NULL 
                                AND PA.wbs2 = ' ' ) ) 
                    AND ( pntask.wbs3 = PA.wbs3 
                           OR ( pntask.wbs3 IS NULL 
                                AND PA.wbs3 = ' ' ) ) 

  UPDATE RPTask 
  SET    RPTask.notes = PA.Notes
  FROM   PRAdditionalData PA
         INNER JOIN RPTask 
                 ON RPTask.wbs1 = PA.wbs1 
                    AND ( RPTask.wbs2 = PA.wbs2 
                           OR ( RPTask.wbs2 IS NULL 
                                AND PA.wbs2 = ' ' ) ) 
                    AND ( RPTask.wbs3 = PA.wbs3 
                           OR ( RPTask.wbs3 IS NULL 
                                AND PA.wbs3 = ' ' ) )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  
  -- Need to move HardBooked settings from RPPlannedLabor (TPD) to RPAssignment and PNPlannedLabor (TPD) to PNAssignment.
  -- If TPD rows for an Assignment end after ETCDate, and one of the TPD [Future] chunk has HardBooked = 'Y' 
  --   then Assignment is HardBooked.
  -- If all TPD rows for an Assignment are in the past of ETC Date, and one of the TPD [Past] chunk has HardBooked = 'Y' 
  --   then Assignment is HardBooked.

  UPDATE A SET
    HardBooked = 'Y'
    FROM PNAssignment AS A
      INNER JOIN (
        SELECT DISTINCT
          TPD.PlanID,
          TPD.TaskID,
          TPD.AssignmentID
          FROM PNPlannedLabor AS TPD
            WHERE TPD.EndDate >= @dtETCDate AND TPD.HardBooked = 'Y' AND TPD.AssignmentID IS NOT NULL
        UNION
        SELECT DISTINCT
          ZTPD.PlanID,
          ZTPD.TaskID,
          ZTPD.AssignmentID
          FROM (
            SELECT DISTINCT
              TPD.PlanID,
              TPD.TaskID,
              TPD.AssignmentID,
              MAX(TPD.HardBooked) OVER (PARTITION BY TPD.PlanID, TPD.TaskID, TPD.AssignmentID) AS MaxHardBooked,
              MAX(TPD.EndDate) OVER (PARTITION BY TPD.PlanID, TPD.TaskID, TPD.AssignmentID) AS MaxTPDEnd
              FROM PNPlannedLabor AS TPD
              WHERE TPD.AssignmentID IS NOT NULL
          ) AS ZTPD
          WHERE ZTPD.MaxTPDEnd < @dtETCDate AND ZTPD.MaxHardBooked = 'Y'
      ) AS XTPD ON A.PlanID = XTPD.PlanID AND A.TaskID = XTPD.TaskID AND A.AssignmentID = XTPD.AssignmentID

  UPDATE A SET
    HardBooked = 'Y'
    FROM RPAssignment AS A
      INNER JOIN (
        SELECT DISTINCT
          TPD.PlanID,
          TPD.TaskID,
          TPD.AssignmentID
          FROM RPPlannedLabor AS TPD
            WHERE TPD.EndDate >= @dtETCDate AND TPD.HardBooked = 'Y' AND TPD.AssignmentID IS NOT NULL
        UNION
        SELECT DISTINCT
          ZTPD.PlanID,
          ZTPD.TaskID,
          ZTPD.AssignmentID
          FROM (
            SELECT DISTINCT
              TPD.PlanID,
              TPD.TaskID,
              TPD.AssignmentID,
              MAX(TPD.HardBooked) OVER (PARTITION BY TPD.PlanID, TPD.TaskID, TPD.AssignmentID) AS MaxHardBooked,
              MAX(TPD.EndDate) OVER (PARTITION BY TPD.PlanID, TPD.TaskID, TPD.AssignmentID) AS MaxTPDEnd
              FROM RPPlannedLabor AS TPD
              WHERE TPD.AssignmentID IS NOT NULL
          ) AS ZTPD
          WHERE ZTPD.MaxTPDEnd < @dtETCDate AND ZTPD.MaxHardBooked = 'Y'
      ) AS XTPD ON A.PlanID = XTPD.PlanID AND A.TaskID = XTPD.TaskID AND A.AssignmentID = XTPD.AssignmentID

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

--Synchronize the plan structure to project structure
exec dbo.stRPSyncMultiplePR_Plan
-------

COMMIT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

END -- stRPConvertIAccessToDeltekPS
GO
