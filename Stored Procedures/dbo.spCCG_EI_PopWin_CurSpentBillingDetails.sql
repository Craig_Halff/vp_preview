SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_PopWin_CurSpentBillingDetails] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7), @UserName varchar(32))
AS BEGIN
/*
	Copyright (c) 2020 Central Consulting Group.  All rights reserved.

	[spCCG_EI_PopWin_CurSpentBillingDetails] '2003005.00',' ',' ', ''
*/
	declare @T TABLE (TopOrder	int, Row_Detail varchar(255), Descr	varchar(255), Hrs decimal(19,5), Amount money, ID varchar(30), Which varchar)

	declare @ThruPeriod	int
	declare @ThruDate	datetime

	select @ThruPeriod=ThruPeriod, @ThruDate=ThruDate
		from CCG_EI_ConfigInvoiceGroups cig
			inner join ProjectCustomTabFields pctf on pctf.custInvoiceGroup=cig.InvoiceGroup
		where pctf.WBS1=@WBS1 and pctf.WBS2=' '

	if @ThruPeriod is null	set @ThruPeriod=999999
	If @ThruDate is null	set @ThruDate='12/31/2050'

	insert into @T (TopOrder, Row_Detail, Descr, Hrs, Amount, ID, Which)
		select 1 as TopOrder, 'BackgroundColor=#888888;ForegroundColor=#ffffff;Bold=Y', 'LABOR', null, null, '', ''
		UNION ALL
		select 2 as TopOrder, '', '  ' + ISNULL(LastName + ', ' + FirstName,''), sum(Hrs), Sum(BillExt), Labor.Employee, 'E'
			from (
			select LD.Employee, RegHrs+OvtHrs+SpecialOvtHrs as Hrs, BillExt
			from LD   
			where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and SuppressBill='N' 
				and Period = @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
			) as Labor left join EM on EM.Employee=Labor.Employee group by Labor.Employee, LastName, FirstName	
		UNION ALL
		select 3 as TopOrder, 'BackgroundColor=#888888;ForegroundColor=#ffffff;Bold=Y', 'CONSULTANTS', null, null, '', ''
		UNION ALL
		select 4 as TopOrder, '', '  ' + AccountName, null, Sum(BillExt), Account, 'A'
			from (
				select CA.Name as AccountName, CA.Account,  BillExt
					from LedgerMisc L left join CA  on CA.Account=L.Account
					where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and CA.Type in (6,8) and SuppressBill='N'  
						and Period = @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
				UNION ALL
				select CA.Name as AccountName, CA.Account,  BillExt
					from LedgerAP L left join CA  on CA.Account=L.Account
					where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and CA.Type in (6,8) and SuppressBill='N' 
						and Period = @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
				UNION ALL
				select CA.Name as AccountName, CA.Account,  BillExt
					from LedgerEX L left join CA  on CA.Account=L.Account
					where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and CA.Type in (6,8) and SuppressBill='N' 
						and Period = @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
				UNION ALL
				select CA.Name as AccountName, CA.Account,  BillExt
					from LedgerAR L left join CA  on CA.Account=L.Account
					where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and CA.Type in (6,8) and SuppressBill='N'   
						and Period = @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
			) as Consultants group by AccountName, Account
			UNION ALL
		select 5 as TopOrder, 'BackgroundColor=#888888;ForegroundColor=#ffffff;Bold=Y', 'EXPENSE', null, null, '', ''
		UNION ALL
		select 6 as TopOrder, '', '  ' + AccountName, null, Sum(BillExt), Account, 'A'
			from (
				select CA.Name as AccountName, CA.Account,  BillExt
					from LedgerMisc L left join CA  on CA.Account=L.Account
					where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and CA.Type in (5,7) and SuppressBill='N'  
						and Period = @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
				UNION ALL
				select  CA.Name as AccountName, CA.Account,  BillExt
					from LedgerAP L left join CA  on CA.Account=L.Account
					where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and CA.Type in (5,7) and SuppressBill='N'   
						and Period = @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
				UNION ALL
				select CA.Name as AccountName, CA.Account,  BillExt
					from LedgerEX L left join CA  on CA.Account=L.Account
					where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and CA.Type in (5,7) and SuppressBill='N'   
						and Period = @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
				UNION ALL
				select CA.Name as AccountName, CA.Account, BillExt
					from LedgerAR L left join CA  on CA.Account=L.Account
					where WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and CA.Type in (5,7) and SuppressBill='N'  
						and Period = @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
			) as Expenses group by AccountName, Account

	-- Put labor and expense totals on header lines:
	update @T set Hrs=(select sum(Hrs) from @T where TopOrder=2) where TopOrder=1
	update @T set Amount=(select sum(Amount) from @T where TopOrder=2) where TopOrder=1
	update @T set Amount=(select sum(Amount) from @T where TopOrder=4) where TopOrder=3
	update @T set Amount=(select sum(Amount) from @T where TopOrder=6) where TopOrder=5
	

	-- Add grand total line:
	insert into @T (TopOrder, Row_Detail, Descr, Hrs, Amount)
		select 5 as TopOrder, 'BackgroundColor=#888888;ForegroundColor=#ffffff;Bold=Y', 'GRAND TOTAL', (select sum(Hrs) from @T where TopOrder in (1)), (select sum(Amount) from @T where TopOrder in (1,3,5))
	
	delete from @T where Amount=0 or isnull(rtrim(Descr), '') = ''

	-- Final recordsets
	select '', '', 'N2', 'C2;NS', '', '',   'Current Billing: ' + @WBS1 + case when @WBS2 <> '' then +' - ' + @WBS2 else '' end + case when @WBS3 <> '' then +' - ' + @WBS3 else '' end 
	select Descr as [JTD], Row_Detail, Hrs as [Hours], Amount, case when Row_Detail = '' then '' else '' end Amount_Detail, case when Which = 'E' then 'spCCG_EI_PopWin_CurSpentBillingDetailsByEmp @Key='''+ID+''', @EmpName='''+Ltrim(Descr)+'''' when Which = 'A' then 'spCCG_EI_PopWin_CurSpentBillingDetailsbyAcct @Key='''+ID+''', @AcctName='''+Ltrim(Descr)+'''' else '' end Amount_Link from @T

	select 'width=800
		height=600' as AdvancedOptions
END
GO
