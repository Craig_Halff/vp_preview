SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHAI_LogDeltekGrowth]
AS
    BEGIN


        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
        SET NOCOUNT ON;
        DECLARE @db_name  VARCHAR(50) = 'HalffImport'
              , @obs_date DATE        = CAST(GETDATE() AS DATE);

        DECLARE @names TABLE (
            tableName VARCHAR(128)
        );


        INSERT INTO @names SELECT name FROM dbo.sysobjects WHERE xtype = 'U';

        CREATE TABLE #StorageStats (
            name VARCHAR(255)
          , rows INT
          , reserved VARCHAR(25)
          , data VARCHAR(25)
          , index_size VARCHAR(25)
          , unused VARCHAR(25)
        );

        WHILE (SELECT COUNT(tableName)FROM @names) > 0
            BEGIN
                DECLARE @thisTable VARCHAR(128);
                SET @thisTable = (
                    SELECT TOP 1 tableName FROM @names
                );
                INSERT INTO #StorageStats (
                    name
                  , rows
                  , reserved
                  , data
                  , index_size
                  , unused
                )
                EXEC sys.sp_spaceused @objname = @thisTable;
                --PRINT @thisTable
                DELETE FROM @names WHERE tableName = @thisTable;
            END;
        --
        --
        MERGE DataMart.dbo.dba_StorageStats AS tgt
        USING (SELECT name, rows, reserved, data, index_size, unused FROM #StorageStats) AS src
        ON (
            tgt.obs_date = @obs_date
               AND tgt.db_name = @db_name
               AND tgt.table_name = src.name
        )
        WHEN MATCHED
            THEN UPDATE SET tgt.table_rows = src.rows
                          , tgt.reserved_kb = CAST(REPLACE(src.reserved, ' KB', '') AS INT)
                          , tgt.data_kb = CAST(REPLACE(src.data, ' KB', '') AS INT)
                          , tgt.index_size_kb = CAST(REPLACE(src.index_size, ' KB', '') AS INT)
                          , tgt.unused_kb = CAST(REPLACE(src.unused, ' KB', '') AS INT)
        WHEN NOT MATCHED
            THEN INSERT (
                     obs_date
                   , table_name
                   , db_name
                   , table_rows
                   , reserved_kb
                   , data_kb
                   , index_size_kb
                   , unused_kb
                 )
                 VALUES
                 (@obs_date, src.name, @db_name, src.rows, CAST(REPLACE(src.reserved, ' KB', '') AS INT), CAST(REPLACE(src.data, ' KB', '') AS INT), CAST(REPLACE(src.index_size, ' KB', '') AS INT)
                , CAST(REPLACE(src.unused, ' KB', '') AS INT))
        --OUTPUT deleted.* , $action     , inserted.* INTO #MyTempTable;

        ;
        DROP TABLE #StorageStats;

    END;
GO
