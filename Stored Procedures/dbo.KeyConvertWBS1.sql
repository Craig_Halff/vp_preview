SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[KeyConvertWBS1]
@DeleteExisting integer, -- 0 don't delete existing, 1 delete existing
@ErrMsg Nvarchar(1000) output,
@PassedEntity Nvarchar(50) = 'WBS1',
@UserName Nvarchar(32) = 'VisionKeyConvert' -- Add by DP 4.0
,@Period integer = 0
,@EntityType Nvarchar(20)
as
begin -- Procedure
	SET ANSI_WARNINGS OFF
	set nocount on
	declare @OldWBS1 			Nvarchar(30)
	declare @OldWBS2 			Nvarchar(30)
	declare @OldWBS3 			Nvarchar(30)
	declare @NewWBS1 			Nvarchar(30)
	declare @NewWBS2 			Nvarchar(30)
	declare @NewWBS3 			Nvarchar(30)
	Declare @Existing 		integer
	declare @Entity 			Nvarchar(20)
	declare @length 			integer
	declare @CvtType 			integer
	declare @KeysFetch 		integer
	declare @RetVal 			integer
	declare @diag 				smallint
	declare @TableName 		Nvarchar(100)
	declare @Sql 			Nvarchar(1000)

	declare @Errmsg2 			Nvarchar(1000),
			@custlabel		Nvarchar(100),
			@Custlabelplural 	Nvarchar(100)
	declare @NewChargeType 	Nvarchar(1)
	declare @OldChargeType 	Nvarchar(1)
	declare @newSubLevel 	Nvarchar(1)
	declare @oldSubLevel 	Nvarchar(1)
	declare @message nvarchar(max)
	set @diag = 0
	set @CvtType = 0 -- oldValue -> newValue
	set @Entity = 'WBS1'
	set @length = 30
	set @Existing = 0
	set @KeysFetch = 0

	execute InsertKeyCvtDriver @Entity -- populate driver table

	declare @VPlanSourceExist as varchar(1)
	declare @VPlanSourceExistCount as integer
	declare @VPlanTargetExist as varchar(1)
	declare @NPlanSourceExist as varchar(1)
	declare @NPlanTargetExist as varchar(1)
	declare @OldPlanID varchar(32)
	declare @NewPlanID varchar(32)
	declare @CheckedOutID as varchar(32)

-- Add by DP 4.0
	delete from keyconvertDriver where entity = N'WBS1' and TableName = N'PR' and ColumnName = N'WBS1'
	delete from keyconvertDriver where entity = N'WBS1' and TableName = N'BT'
	DECLARE @OldName Nvarchar(40)
	DECLARE @NewName Nvarchar(40)
	DECLARE @OldAllocMethod varchar(1)
	DECLARE @NewAllocMethod varchar(1)
	DECLARE @OldStoredCurrencySetting Nvarchar(100)
	DECLARE @NewStoredCurrencySetting Nvarchar(100)
	DECLARE @OldStoredCurrencyCode Nvarchar(3)
	DECLARE @NewStoredCurrencyCode Nvarchar(3)
	DECLARE @PKey Nvarchar(50)
	DECLARE @PostControlFlag integer
	set @PostControlFlag = 0
--
	select @custlabel = LabelValue from FW_CFGLabels where LabelName = 'wbs1Label'
	select @custlabelplural = LabelValue from FW_CFGLabels where LabelName = 'wbs1LabelPlural'

	-- Add Storm 1.1
	declare @RPTaskFetch integer
	declare @RPOldPlanID varchar(32)
	declare @RPOldTaskID varchar(32)
	declare @RPNewTaskID varchar(32)
	declare @RPOldWBS1 	Nvarchar(30)
	declare @RPOldWBS2 	Nvarchar(30)
	declare @RPOldWBS3 	Nvarchar(30)
	declare @RPOldLaborCode 	Nvarchar(14)
	declare @RPOldChildrenCount int 
	declare @RPOldParentOutLineNumber varchar(255)
	declare @RPOldOutlineNumber varchar(255)
	declare @RPOldOutlineLevel int	
	declare @RPTableName varchar(30)

	declare KeysCursor cursor for
		select OldKey,
			OldKey2,
			OldKey3,
			NewKey,
			NewKey2,
			NewKey3, PKey from KeyConvertWork where Entity = @EntityType

	open KeysCursor
	fetch next from KeysCursor into 
		@OldWBS1,
		@OldWBS2,
		@OldWBS3,
		@NewWBS1,
		@NewWBS2,
		@NewWBS3,
		@PKey

	set @KeysFetch = @@Fetch_Status

While (@KeysFetch = 0)
	begin
		begin tran

		Set @VPlanSourceExist = 'N'
		Set @VPlanSourceExistCount = 0
		Set @VPlanTargetExist = 'N'
		Set @NPlanSourceExist = 'N'
		Set @NPlanTargetExist = 'N'
		Set @OldPlanID = ''
		Set @NewPlanID = ''
		Set @CheckedOutID = REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')

		if exists (select 'x' from CFGTimeAnalysis where StartWBS1 = @OldWBS1 or EndWBS1 = @oldWBS1)
		begin
		SET @message = dbo.GetMessage('MsgNumSpecInTimeAnalysisSetup',@custlabel,@OldWBS1,@custlabel,'','','','','','')
		RAISERROR(@message,16,3)
			close keysCursor
			deallocate keysCursor
			if (@@Trancount > 0)
			   ROLLBACK TRANSACTION
			return(50004) -- user defined error
		end
-- Add by DP 4.0
		if (@PostControlFlag = 0) 
		begin 
			DECLARE @LastPostSeq integer
			DECLARE @strCompany Nvarchar(14)
			DECLARE @RecordCount integer
			DECLARE @tempString Nvarchar(50)
			if (@PassedEntity = 'WBS1')
				set @tempString = @custlabel + ' Key Convert'
			else
				set @tempString = @custlabel + ' Closeout'

			if (@Period = 0)
				set @Period = isnull((select CurrentPeriod from fw_cfgsystem),0)
			if (@Period > 0)
			begin
				set @LastPostSeq = (select LastPostSeq+1 from cfgdates where period = @Period)
				if (IsNull(@LastPostSeq, 0) = 0)
					Set @LastPostSeq = (select IsNull(max(CFGPostControl.PostSeq), 0) + 1 from CFGPostControl where Period = @Period)

				set @strCompany = dbo.GetActiveCompany()
				if (@strCompany = '')
					set @strCompany = (select MIN(company) from CFGMainData)
				if (@PassedEntity = 'WBS1')
					set @RecordCount = (select count(*) from KeyConvertWork where Entity = @EntityType)
				else
					set @RecordCount = (select count(*) from KeyConvertWork where Entity = N'closeout')

				Update cfgdates Set LastPostSeq = @LastPostSeq Where Period = @Period
				Insert into CFGPostControl (Period, PostSeq, UserName, PostDate, TransType, Completed, PostComment, Company, Batch, W2Quarter, RecordCount, QueueName, PaymentProcessRun) 
 				Values (@Period,@LastPostSeq,@UserName,GETUTCDATE(),N'KC','Y',  @tempString + ' ('+rtrim(convert(Nvarchar(10), @RecordCount))+' keys)', @strCompany, @Entity, @DeleteExisting, @RecordCount, left(@tempString, 40), @custlabel);
			end

			Set @PostControlFlag = 1
		end
		select @OldName = isnull(PR.Name, N''), @OldAllocMethod = AllocMethod, @OldStoredCurrencySetting = RPPlan.StoredCurrency, @OldStoredCurrencyCode = case when RPPlan.StoredCurrency = 'B' then PR.BillingCurrencyCode Else PR.ProjectCurrencyCode End 
		From PR Left Join RPPlan on PR.WBS1=RPPlan.WBS1 Where PR.WBS1 = @OldWBS1 and PR.WBS2=N' ' and PR.WBS3=N' '
		select @NewName = isnull(PR.Name, N''), @NewAllocMethod = AllocMethod, @NewStoredCurrencySetting = RPPlan.StoredCurrency, @NewStoredCurrencyCode = case when RPPlan.StoredCurrency = 'B' then PR.BillingCurrencyCode Else PR.ProjectCurrencyCode End 
		From PR Left Join RPPlan on PR.WBS1=RPPlan.WBS1 Where PR.WBS1 = @NewWBS1 and PR.WBS2=N' ' and PR.WBS3=N' '
		if (@OldStoredCurrencySetting = 'B')
			Set @OldStoredCurrencySetting = 'Billing'
		else
			Set @OldStoredCurrencySetting = @custlabel
		if (@NewStoredCurrencySetting = 'B')
			Set @NewStoredCurrencySetting = 'Billing'
		else
			Set @NewStoredCurrencySetting = @custlabel
--
		set @Existing = 0
		select @Existing =  1 from PR where WBS1 = @NewWBS1 
			and WBS2 = N' '
			and WBS3 = N' '

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------Storm 1.1 - WBS3 children records Only
		Select PlanID, TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, ChildrenCount, OutlineLevel, OutlineNumber, ParentOutlineNumber, 'RPTask' as TableName Into #tempRPPN From RPTask 
		Where RPTask.WBS1 = @OldWBS1 and isnull(RPTask.WBS2,'') = @OldWBS2 and (ISNULL(RPTask.WBS3,' ')<>' ' And OutlineLevel>=2)
		Union All Select PlanID, TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, ChildrenCount, OutlineLevel, OutlineNumber, ParentOutlineNumber, 'PNTask' as TableName From PNTask
		Where PNTask.WBS1 = @OldWBS1 and isnull(PNTask.WBS2,'') = @OldWBS2 and (ISNULL(PNTask.WBS3,' ')<>' ' And OutlineLevel>=2)
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	If (@Existing = 1)
		begin
			  if (@DeleteExisting = 0)
				begin
				SET @message = dbo.GetMessage('MsgNumAnExistingConvWillNotCont',@custlabel,@NewWBS1,@custlabel,@custLabelPlural,'','','','','')
				RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50002) -- user defined error
				end
				if (@NewWBS1 = @OldWBS1)
					begin
					SET @message = dbo.GetMessage('MsgAreSameNumConvNotCont',@custlabelPlural,'','','','','','','','')
					RAISERROR(@message,16,3)
						close keysCursor
						deallocate keysCursor
						if (@@Trancount > 0)
						   ROLLBACK TRANSACTION
						return(50002) -- user defined error
					end

				declare @MulticompanyEnabled Nvarchar(1)
				declare @MulticurrencyEnabled Nvarchar(1)
				declare @NewFunctionalCurrencyCode Nvarchar(3)
				declare @OldFunctionalCurrencyCode Nvarchar(3)
				declare @oldCompany Nvarchar(14)
				declare @newCompany Nvarchar(14)
				select @MulticompanyEnabled = MulticompanyEnabled,
			   		 @MulticurrencyEnabled = MulticurrencyEnabled from FW_CFGSystem
				
				if (@MulticompanyEnabled = 'Y')
					begin
						set @oldCompany = ''
						set @newCompany = ''
						select 
							@oldCompany = substring(old.org,1,Org1Length),
							@newCompany = substring(new.org,1,Org1Length)
						from pr new inner join pr Old on (new.wbs2 = old.wbs2 and new.wbs3 = old.wbs3),cfgformat
						where new.wbs1 = @NewWBS1 and old.wbs1 = @OldWBS1
						and (substring(old.org,1,Org1Length) <> substring(new.org,1,Org1Length))
						order by new.wbs1,
						new.wbs2,
						new.wbs3

						if (@oldCompany <> @newCompany)
							begin
							SET @message = dbo.GetMessage('MsgWithMultiCmpYouCannotMerge',@custlabelPlural,@custlabel,@OldWBS1,@oldCompany,@custlabel,@NewWBS1,@newCompany,'','')
							RAISERROR(@message,16,3)
								close keysCursor
								deallocate keysCursor
								if (@@Trancount > 0)
								   ROLLBACK TRANSACTION
								return(50003) -- user defined error
							end
					end

				if (@MulticurrencyEnabled = 'Y')
					declare @oldCurrency Nvarchar(14)
					declare @newCurrency Nvarchar(14)
					begin
						set @oldCurrency = ''
						set @newCurrency = ''
						select 
							@oldCurrency = old.ProjectCurrencyCode+';'+old.BillingCurrencyCode,
							@newCurrency = new.ProjectCurrencyCode+';'+new.BillingCurrencyCode
						from pr new inner join pr Old on (new.wbs2 = old.wbs2 and new.wbs3 = old.wbs3)
						where new.wbs1 = @NewWBS1 and old.wbs1 = @OldWBS1
						and (old.ProjectCurrencyCode+old.BillingCurrencyCode <> new.ProjectCurrencyCode+new.BillingCurrencyCode)
						order by new.wbs1,
						new.wbs2,
						new.wbs3

						if (@oldCurrency <> @newCurrency)
							begin
							SET @message = dbo.GetMessage('MsgWithMultiCmpYouCantMergeDiffCurr',@custlabelPlural,@custlabel,@OldWBS1,@oldCurrency,@custlabel,@NewWBS1,@newCurrency,'','')
							RAISERROR(@message,16,3)
								close keysCursor
								deallocate keysCursor
								if (@@Trancount > 0)
								   ROLLBACK TRANSACTION
								return(50003) -- user defined error
							end
					end

				if (@MulticurrencyEnabled ='Y' and @oldStoredCurrencyCode <> @newStoredCurrencyCode and @OldAllocMethod = 'r' and @NewAllocMethod = 'r')
					begin
					SET @message = dbo.GetMessage('MsgWithMultiCmpYouCantMergeDiffStoredCurr',LOWER(@custlabelPlural),@custlabel,@OldWBS1,LOWER(@OldStoredCurrencySetting),@oldStoredCurrencyCode,LOWER(@custlabel),@NewWBS1,LOWER(@NewStoredCurrencySetting),@newStoredCurrencyCode)
					RAISERROR(@message,16,3)
						close keysCursor
						deallocate keysCursor
						if (@@Trancount > 0)
							ROLLBACK TRANSACTION
						return(50003) -- user defined error
					end

			--Test for inconsistent chargeTypes and task structures
				set @NewChargeType = ''
				set @OldChargeType = ''
				set @newSubLevel = ''
				set @oldSubLevel = ''
				select 
					/*new.wbs1,
					new.wbs2,
					new.wbs3,
					old.wbs1,
					old.wbs2,
					old.wbs3,*/
					@NewChargeType = new.chargeType,
					@newSubLevel = new.subLevel,
					@OldChargeType = old.chargeType,
					@oldSubLevel = old.subLevel 
				from pr new inner join pr Old on (new.wbs2 = old.wbs2 and new.wbs3 = old.wbs3)
				where new.wbs1 = @NewWBS1 and old.wbs1 = @OldWBS1
				and (case old.ChargeType when 'P' then 'H' else old.ChargeType end
				<> case new.ChargeType when 'P' then 'H' else new.ChargeType end or old.subLevel <> new.subLevel)
				order by new.wbs1,
				new.wbs2,
				new.wbs3
			 if (@NewChargeType <> '') --somthing was returned from the above query = error
				begin
					if (@oldSubLevel <> @NewSublevel)
						begin
						SET @message = dbo.GetMessage('MsgHasDiffWorkBreakDown',@custlabel,@NewWBS1,@custlabel,@oldWBS1,'','','','','')
						RAISERROR(@message,16,3)
							close keysCursor
							deallocate keysCursor
							if (@@Trancount > 0)
							   ROLLBACK TRANSACTION
							return(50003) -- user defined error
						end
					if (@oldChargeType <> @NewChargeType)
						begin
						SET @message = dbo.GetMessage('MsgHasDiffChrgTypeFromConvNotCont',@custlabel,@NewWBS1,@custlabel,@oldWBS1,'','','','','')
						RAISERROR(@message,16,3)
							close keysCursor
							deallocate keysCursor
							if (@@Trancount > 0)
							   ROLLBACK TRANSACTION
							return(50003) -- user defined error
						end

		
				end	--End Test for inconsistent chargeTypes and task structures


			Update PNTask
			set PNTask.CompensationFee = PNTask.CompensationFee + old.CompensationFee,
				PNTask.ConsultantFee = PNTask.ConsultantFee + old.ConsultantFee,
				PNTask.ReimbAllowance = PNTask.ReimbAllowance + old.ReimbAllowance, 
				PNTask.CompensationFeeBill = PNTask.CompensationFeeBill + old.CompensationFeeBill, 
				PNTask.ConsultantFeeBill = PNTask.ConsultantFeeBill + old.ConsultantFeeBill,
				PNTask.ReimbAllowanceBill = PNTask.ReimbAllowanceBill + old.ReimbAllowanceBill,
				PNTask.CompensationFeeDirLab = PNTask.CompensationFeeDirLab + old.CompensationFeeDirLab, 
				PNTask.CompensationFeeDirExp = PNTask.CompensationFeeDirExp + old.CompensationFeeDirExp, 
				PNTask.ReimbAllowanceExp = PNTask.ReimbAllowanceExp + old.ReimbAllowanceExp,
				PNTask.ReimbAllowanceCon = PNTask.ReimbAllowanceCon + old.ReimbAllowanceCon,
				PNTask.CompensationFeeDirLabBill = PNTask.CompensationFeeDirLabBill + old.CompensationFeeDirLabBill, 
				PNTask.CompensationFeeDirExpBill = PNTask.CompensationFeeDirExpBill + old.CompensationFeeDirExpBill,
				PNTask.ReimbAllowanceExpBill = PNTask.ReimbAllowanceExpBill + old.ReimbAllowanceExpBill,
				PNTask.ReimbAllowanceConBill = PNTask.ReimbAllowanceConBill + old.ReimbAllowanceConBill
			from PNTask old inner join PNTask on (isnull(PNTask.WBS2,' ') = isnull(old.WBS2,' ') and isnull(PNTask.WBS3,' ') = isnull(old.WBS3,' '))
			where
				PNTask.WBS1 = @NewWBS1 and
				old.wbs1 = @OldWBS1

			Update RPTask
			set RPTask.CompensationFee = RPTask.CompensationFee + old.CompensationFee,
				RPTask.ConsultantFee = RPTask.ConsultantFee + old.ConsultantFee,
				RPTask.ReimbAllowance = RPTask.ReimbAllowance + old.ReimbAllowance, 
				RPTask.CompensationFeeBill = RPTask.CompensationFeeBill + old.CompensationFeeBill, 
				RPTask.ConsultantFeeBill = RPTask.ConsultantFeeBill + old.ConsultantFeeBill,
				RPTask.ReimbAllowanceBill = RPTask.ReimbAllowanceBill + old.ReimbAllowanceBill,
				RPTask.CompensationFeeDirLab = RPTask.CompensationFeeDirLab + old.CompensationFeeDirLab, 
				RPTask.CompensationFeeDirExp = RPTask.CompensationFeeDirExp + old.CompensationFeeDirExp, 
				RPTask.ReimbAllowanceExp = RPTask.ReimbAllowanceExp + old.ReimbAllowanceExp,
				RPTask.ReimbAllowanceCon = RPTask.ReimbAllowanceCon + old.ReimbAllowanceCon,
				RPTask.CompensationFeeDirLabBill = RPTask.CompensationFeeDirLabBill + old.CompensationFeeDirLabBill, 
				RPTask.CompensationFeeDirExpBill = RPTask.CompensationFeeDirExpBill + old.CompensationFeeDirExpBill,
				RPTask.ReimbAllowanceExpBill = RPTask.ReimbAllowanceExpBill + old.ReimbAllowanceExpBill,
				RPTask.ReimbAllowanceConBill = RPTask.ReimbAllowanceConBill + old.ReimbAllowanceConBill
			from RPTask old inner join RPTask on (isnull(RPTask.WBS2,' ') = isnull(old.WBS2,' ') and isnull(RPTask.WBS3,' ') = isnull(old.WBS3,' '))
			where
				RPTask.WBS1 = @NewWBS1 and
				old.wbs1 = @OldWBS1

-- For checking of Vision/Navigator Planning tables
			declare @RPHardBooking as varchar(1)
			declare @UseBookingForEmpHours as varchar(1)
			declare @UseBookingForGenHours as varchar(1)

			Select @RPHardBooking=SE.RPHardBooking, @UseBookingForEmpHours=UseBookingForEmpHours, @UseBookingForGenHours=UseBookingForGenHours 
			from CFGRMSettings, SE Inner join SEUser on SE.Role=SEUser.Role and SEUser.Username=@UserName

			declare @VPlanSourceCheckedOut as varchar(1)
			declare @NPlanSourceCheckedOut as varchar(1)
			declare @NPlanTargetCheckedOut as varchar(1)
		
			Set @VPlanSourceCheckedOut = 'N'
			Set @NPlanSourceCheckedOut = 'N'
			Set @NPlanTargetCheckedOut = 'N'

			declare @UnpublishedAllMsg as varchar(max)
			set @UnpublishedAllMsg = ''
			declare @publishedFlag as bit
			declare @CheckedOutUserAllMsg as varchar(max)
			set @CheckedOutUserAllMsg = ''
			declare @PlanName as varchar(255)
			declare @CheckedOutUser as varchar(255)
			Declare @oldStartDate datetime,
				@oldEndDate datetime

-- Navigator Plan Source
			if exists (select 'x' from PNTask where WBS1 = @OldWBS1)
				begin
					set @NPlanSourceExist = 'Y'
					select @OldPlanID=PlanID from PNPlan where wbs1=@OldWBS1
					Select @oldStartDate = StartDate,  @oldEndDate = EndDate from PNAccordionFormat
						Where PlanID = @OldPlanID
					if exists (select 'x' from RPTask left join RPPlan on RPTask.PlanID = RPPlan.PlanID where RPPlan.PlanID = @OldPlanID and RPPlan.CheckedOutUser > '' and RPPlan.CheckedOutUser <> @UserName)
						begin
							set @NPlanSourceCheckedOut = 'Y'

							select distinct @PlanName = isnull(RPPlan.PlanNumber+' : ','')+isnull(RPPlan.PlanName,''), @CheckedOutUser = isnull(isnull(EM.FirstName+' ', '')+EM.LastName, RPPlan.CheckedOutUser)
							from RPTask left join RPPlan on RPTask.PlanID = RPPlan.PlanID 
							Left Join SEUser on SEUser.UserName=RPPlan.CheckedOutUser
							Left Join EM on SEUser.Employee=EM.Employee
							where RPPlan.PlanID = @OldPlanID and RPPlan.CheckedOutUser > '' and RPPlan.CheckedOutUser <> @UserName

							if (@CheckedOutUserAllMsg > '')
								SET @CheckedOutUserAllMsg = @CheckedOutUserAllMsg + CHAR(13)+CHAR(10)						
							SET @CheckedOutUserAllMsg = @CheckedOutUserAllMsg + dbo.GetMessage('MsgNavigatorPlanCheckedOutBy',@PlanName,@CheckedOutUser,'','','','','','','')
						end

					select @publishedFlag = [dbo].[stRP$DiffPlanned](@OldPlanID), @PlanName = isnull(RPPlan.PlanNumber+' : ','')+isnull(RPPlan.PlanName,'') from RPPlan where RPPlan.PlanID = @OldPlanID
					if (@publishedFlag = 1)
						begin
							if (@UnpublishedAllMsg > '')
								SET @UnpublishedAllMsg = @UnpublishedAllMsg + CHAR(13)+CHAR(10)						
							SET @UnpublishedAllMsg = @UnpublishedAllMsg + dbo.GetMessage('MsgPlanUnpublished',@PlanName,'','','','','','','','')
						end
				end

-- Navigator Plan Target
			if exists (select 'x' from PNTask where WBS1 = @NewWBS1)
				begin
					set @NPlanTargetExist = 'Y'
					select @NewPlanID=PlanID from PNPlan where wbs1=@NewWBS1
					if exists (select 'x' from RPTask left join RPPlan on RPTask.PlanID = RPPlan.PlanID where RPPlan.PlanID = @NewPlanID and RPPlan.CheckedOutUser > '' and RPPlan.CheckedOutUser <> @UserName)
						begin
							set @NPlanTargetCheckedOut = 'Y'

							select distinct @PlanName = isnull(RPPlan.PlanNumber+' : ','')+isnull(RPPlan.PlanName,''), @CheckedOutUser = isnull(isnull(EM.FirstName+' ', '')+EM.LastName, RPPlan.CheckedOutUser)
							from RPTask left join RPPlan on RPTask.PlanID = RPPlan.PlanID 
							Left Join SEUser on SEUser.UserName=RPPlan.CheckedOutUser
							Left Join EM on SEUser.Employee=EM.Employee
							where RPPlan.PlanID = @NewPlanID and RPPlan.CheckedOutUser > '' and RPPlan.CheckedOutUser <> @UserName

							if (@CheckedOutUserAllMsg > '')
								SET @CheckedOutUserAllMsg = @CheckedOutUserAllMsg + CHAR(13)+CHAR(10)						
							SET @CheckedOutUserAllMsg = @CheckedOutUserAllMsg + dbo.GetMessage('MsgNavigatorPlanCheckedOutBy',@PlanName,@CheckedOutUser,'','','','','','','')
						end

					if (@EntityType <> 'closeout')
						begin
							select @publishedFlag = [dbo].[stRP$DiffPlanned](@NewPlanID), @PlanName = isnull(RPPlan.PlanNumber+' : ','')+isnull(RPPlan.PlanName,'') from RPPlan where RPPlan.PlanID = @NewPlanID
							if (@publishedFlag = 1)
								begin
									if (@UnpublishedAllMsg > '')
										SET @UnpublishedAllMsg = @UnpublishedAllMsg + CHAR(13)+CHAR(10)						
									SET @UnpublishedAllMsg = @UnpublishedAllMsg + dbo.GetMessage('MsgPlanUnpublished',@PlanName,'','','','','','','','')
								end
						end
				end

-- Vision Plan Source
			if exists (select 'x' from RPTask where WBS1 = @OldWBS1 and RPTask.PlanID <> @OldPlanID)
				begin
					set @VPlanSourceExist = 'Y'
					set @VPlanSourceExistCount = 0
					
					if exists (select 'x' from RPTask left join RPPlan on RPTask.PlanID = RPPlan.PlanID where RPTask.WBS1 = @OldWBS1 and RPTask.PlanID <> @OldPlanID and RPPlan.CheckedOutUser > '' and RPPlan.CheckedOutUser <> @UserName)
						begin
							set @VPlanSourceCheckedOut = 'Y'

							DECLARE db_cursor CURSOR FOR 
							select distinct isnull(RPPlan.PlanNumber+' : ','')+isnull(RPPlan.PlanName,''), isnull(isnull(EM.FirstName+' ', '')+EM.LastName, RPPlan.CheckedOutUser) as CheckedOutUser
							from RPTask left join RPPlan on RPTask.PlanID = RPPlan.PlanID 
							Left Join SEUser on SEUser.UserName=RPPlan.CheckedOutUser
							Left Join EM on SEUser.Employee=EM.Employee
							where RPTask.WBS1 = @OldWBS1 and RPTask.PlanID <> @OldPlanID and RPPlan.CheckedOutUser > '' and RPPlan.CheckedOutUser <> @UserName

							OPEN db_cursor  
							FETCH NEXT FROM db_cursor INTO @PlanName, @CheckedOutUser  

							WHILE @@FETCH_STATUS = 0  
							BEGIN  
								if (@CheckedOutUserAllMsg > '')
									SET @CheckedOutUserAllMsg = @CheckedOutUserAllMsg + CHAR(13)+CHAR(10)						
								SET @CheckedOutUserAllMsg = @CheckedOutUserAllMsg + dbo.GetMessage('MsgPlanCheckedOutBy',@PlanName,@CheckedOutUser,'','','','','','','') 
								set @VPlanSourceExistCount = @VPlanSourceExistCount + 1

								FETCH NEXT FROM db_cursor INTO @PlanName, @CheckedOutUser  
							END  

							CLOSE db_cursor  
							DEALLOCATE db_cursor
						end
				end

-- Vision Plan Target
			if exists (select 'x' from RPTask where WBS1 = @NewWBS1 and RPTask.PlanID <> @NewPlanID)
				set @VPlanTargetExist = 'Y'

			declare @PlanningModule as varchar(1)
			if exists (select ModuleID from CFGModuleConfiguration where ModuleID='Planning' and isnull(Password,'') > '')
				set @PlanningModule = 'Y'
			else
				set @PlanningModule = 'N'

-- Checking for Checked out
			if (@PlanningModule = 'Y' and @CheckedOutUserAllMsg > '')
				begin
					SET @message = dbo.GetMessage('MsgFollowingPlansAreCheckedOut',@custlabel,CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10)+@CheckedOutUserAllMsg+CHAR(13)+CHAR(10),@Custlabelplural,'','','','','','')
					RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50005) -- user defined error
				end

-- Checking for Unpublished Plan
			if (@PlanningModule = 'Y' and @UnpublishedAllMsg > '')
				begin
					SET @message = dbo.GetMessage('MsgFollowingPlansAreUnpublished',@custlabel,CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10)+@UnpublishedAllMsg+CHAR(13)+CHAR(10),@Custlabelplural,'','','','','','')
					RAISERROR(@message,16,3)
					close keysCursor
					deallocate keysCursor
					if (@@Trancount > 0)
					   ROLLBACK TRANSACTION
					return(50005) -- user defined error
				end
		
-- Checking for Expense and Consultant WBS Level
			if (@NPlanSourceExist='Y' and @NPlanTargetExist='Y') 
				Begin
					declare @ExpTab as varchar(1)
					declare @ConTab as varchar(1)
					if (@MulticompanyEnabled = 'Y')
						select @ExpTab=ExpTab, @ConTab=ConTab from CFGResourcePlanning left join 
						PR on PR.WBS1=@OldWBS1 and left(PR.Org, (select Org1Length from cfgformat)) = CFGResourcePlanning.Company
					else
						select @ExpTab=ExpTab, @ConTab=ConTab from CFGResourcePlanning

					if (@ExpTab = 'Y' or @ConTab = 'Y')
						Begin
							declare @ExpWBSLevelSource as smallint
							declare @ExpWBSLevelTarget as smallint
							declare @ConWBSLevelSource as smallint
							declare @ConWBSLevelTarget as smallint
							select @ExpWBSLevelSource=isnull(ExpWBSLevel,0), @ConWBSLevelSource=isnull(ConWBSLevel,0) from PNPlan Where WBS1=@OldWBS1
							select @ExpWBSLevelTarget=isnull(ExpWBSLevel,0), @ConWBSLevelTarget=isnull(ConWBSLevel,0) from PNPlan Where WBS1=@NewWBS1
							if ((@ExpTab = 'Y' and @ExpWBSLevelSource <> @ExpWBSLevelTarget) and (@ConTab = 'Y' and @ConWBSLevelSource <> @ConWBSLevelTarget))
								begin
									SET @message = dbo.GetMessage('MsgExpConLevelNotMatched',@Custlabelplural,'','','','','','','','')
									RAISERROR(@message,16,3)
									close keysCursor
									deallocate keysCursor
									if (@@Trancount > 0)
									   ROLLBACK TRANSACTION
									return(50005) -- user defined error
								end
							if ((@ExpTab = 'Y' and @ExpWBSLevelSource <> @ExpWBSLevelTarget) and (@ConTab = 'N' or @ConWBSLevelSource = @ConWBSLevelTarget))							
								begin
									SET @message = dbo.GetMessage('MsgExpLevelNotMatched',@Custlabelplural,'','','','','','','','')
									RAISERROR(@message,16,3)
									close keysCursor
									deallocate keysCursor
									if (@@Trancount > 0)
									   ROLLBACK TRANSACTION
									return(50005) -- user defined error
								end
							if ((@ExpTab = 'N' or @ExpWBSLevelSource = @ExpWBSLevelTarget) and( @ConTab = 'Y' and @ConWBSLevelSource <> @ConWBSLevelTarget))
								begin
									SET @message = dbo.GetMessage('MsgConLevelNotMatched',@Custlabelplural,'','','','','','','','')
									RAISERROR(@message,16,3)
									close keysCursor
									deallocate keysCursor
									if (@@Trancount > 0)
									   ROLLBACK TRANSACTION
									return(50005) -- user defined error
								end
						End			
				End
			
-- Remove the Vision plan of the Nav plan
			if (@OldPlanID > '' and @NPlanSourceExist='Y')
			begin
		if (@diag = 1) print 'Execute dbo.PMDelPlan @OldPlanID'					 
				Execute dbo.PMDelPlan @OldPlanID
			end
			if (@NewPlanID > '' and @NPlanTargetExist='Y')
			begin
		if (@diag = 1) print 'Execute dbo.PMDelPlan @NewPlanID'					 
				Execute dbo.PMDelPlan @NewPlanID
			end

-- Check out plans
			if (@VPlanSourceExist = 'Y')
				Update RPPlan Set CheckedOutDate=GETDATE(), CheckedOutUser=@UserName, CheckedOutID=@CheckedOutID
				From RPPlan Left Join RPTask on RPPlan.PlanID=RPTask.PlanID
				where RPTask.WBS1 = @OldWBS1

			if (@NPlanSourceExist = 'Y' or @NPlanTargetExist = 'Y')
				Update PNPlan Set CheckedOutDate=GETDATE(), CheckedOutUser=@UserName, CheckedOutID=@CheckedOutID
				From PNPlan Left Join PNTask on PNPlan.PlanID=PNTask.PlanID
				where PNTask.WBS1 in (@OldWBS1, @NewWBS1)

-- Combine Navigator Plans Start
			if (@NPlanSourceExist = 'Y' and @NPlanTargetExist = 'Y')
				Begin
-- combine duplicate rsources in PNAssignment, PNExpense, PNConsultant
					declare @TempFetch integer
					declare @TempFetch2 integer
					declare @OldTaskID		varchar(32)
					declare @NewTaskID		varchar(32)
					declare @ResourceID		Nvarchar(20),
							@GenericResourceID		Nvarchar(20),
							@OldAssignmentID	Nvarchar(32),
							@NewAssignmentID	Nvarchar(32)
					declare @WBS1	Nvarchar(30),
							@WBS2	Nvarchar(30),
							@WBS3	Nvarchar(30)
					declare @Account		Nvarchar(13),
							@Vendor		Nvarchar(20),
							@OldExpenseID	Nvarchar(32),
							@NewExpenseID	Nvarchar(32)
					declare @OldConsultantID	Nvarchar(32),
							@NewConsultantID	Nvarchar(32)
					declare @LaborCode	Nvarchar(14),
							@WBSType	Nvarchar(4)



-- Move Planned and Basedline for the duplicate PNAssignment, Delete duplicate PNAssignment
					declare AssignmentCursor cursor for
						select TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, isnull(ResourceID,'') as ResourceID, isnull(GenericResourceID,'') as GenericResourceID, MIN(AssignmentID) as AssignmentID  
						from PNAssignment where PlanID=@OldPlanID and WBS1=@OldWBS1 and
						exists (select 'x' from PNAssignment new where PlanID=@NewPlanID and new.wbs1 = @NewWBS1 
							and isnull(new.wbs2,'') = isnull(PNAssignment.wbs2, '')
							and isnull(new.wbs3,'') = isnull(PNAssignment.wbs3,'')
							and isnull(new.LaborCode,'') = isnull(PNAssignment.LaborCode,'')
							and isnull(new.ResourceID,'') = isnull(PNAssignment.ResourceID,'')
							and isnull(new.GenericResourceID,'') = isnull(PNAssignment.GenericResourceID,''))
						group by TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(LaborCode,''), isnull(ResourceID,''), isnull(GenericResourceID,'')
					open AssignmentCursor
					fetch next from AssignmentCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @LaborCode, @ResourceID, @GenericResourceID, @OldAssignmentID
					set @TempFetch = @@Fetch_Status
					While (@TempFetch = 0)
					begin
if (@diag = 1) Print 'declare AssignmentCursor2 @NewPlanID, @NewWBS1 : ' + @NewPlanID +'/'+ @NewWBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+ @ResourceID +'/'+@GenericResourceID
						select @NewTaskID=TaskID, @NewAssignmentID=AssignmentID from PNAssignment 
							where PlanID=@NewPlanID and WBS1=@NewWBS1 
							and isnull(WBS2,'')=@WBS2 
							and isnull(WBS3,'')=@WBS3 
							and isnull(LaborCode,'')=@LaborCode 
							and isnull(ResourceID,'')=@ResourceID 
							and isnull(GenericResourceID,'')=@GenericResourceID 

if (@diag = 1) Print 'Update PNPlannedLabor and PNBaselineLabor ' + @NewPlanID +'/'+ @NewWBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+ @ResourceID +'/'+@GenericResourceID
						if (@UseBookingForEmpHours = 'Y')
							Update PNPlannedLabor Set HardBooked=@RPHardBooking 
							From PNPlannedLabor Left Join PNAssignment on PNPlannedLabor.AssignmentID=PNAssignment.AssignmentID
							Where PNPlannedLabor.AssignmentID=@OldAssignmentID and PNAssignment.ResourceID > '' and PNPlannedLabor.HardBooked<>@RPHardBooking 
						if (@UseBookingForGenHours = 'Y')
							Update PNPlannedLabor Set HardBooked=@RPHardBooking 
							From PNPlannedLabor Left Join PNAssignment on PNPlannedLabor.AssignmentID=PNAssignment.AssignmentID
							Where PNPlannedLabor.AssignmentID=@OldAssignmentID and PNAssignment.GenericResourceID > '' and PNPlannedLabor.HardBooked<>@RPHardBooking 

						Update PNPlannedLabor Set PlanID=@NewPlanID, TaskID=@NewTaskID, AssignmentID=@NewAssignmentID where AssignmentID=@OldAssignmentID
						Update PNBaselineLabor Set PlanID=@NewPlanID, TaskID=@NewTaskID, AssignmentID=@NewAssignmentID where AssignmentID=@OldAssignmentID

if (@diag = 1) Print 'delete PNAssignment Where TaskID=@OldTaskID and WBS1=@WBS1 ' + @OldTaskID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+ @ResourceID +'/'+@GenericResourceID
						delete PNAssignment Where TaskID=@OldTaskID and WBS1=@WBS1 
							and isnull(WBS2,'')=@WBS2 
							and isnull(WBS3,'')=@WBS3 
							and isnull(LaborCode,'')=@LaborCode 
							and isnull(ResourceID,'')=@ResourceID
							and isnull(GenericResourceID,'')=@GenericResourceID 

						fetch next from AssignmentCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @LaborCode, @ResourceID, @GenericResourceID, @OldAssignmentID
						set @TempFetch = @@Fetch_Status
					end
					close AssignmentCursor
					deallocate AssignmentCursor

-- Move Planned and Basedline for the duplicate PNExpense, Delete duplicate PNExpense
					declare ExpenseCursor cursor for
						select TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Account,'') as Account, isnull(Vendor,'') as Vendor, MIN(ExpenseID) as ExpenseID  
						from PNExpense where PlanID=@OldPlanID and WBS1=@OldWBS1 and
						exists (select 'x' from PNExpense new where PlanID=@NewPlanID and new.wbs1 = @NewWBS1 
							and isnull(new.wbs2,'') = isnull(PNExpense.wbs2, '')
							and isnull(new.wbs3,'') = isnull(PNExpense.wbs3,'')
							and isnull(new.Account,'') = isnull(PNExpense.Account,'')
							and isnull(new.Vendor,'') = isnull(PNExpense.Vendor,''))
						group by TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
					open ExpenseCursor
					fetch next from ExpenseCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldExpenseID
					set @TempFetch = @@Fetch_Status
					While (@TempFetch = 0)
					begin
						select @NewTaskID=TaskID, @NewExpenseID=ExpenseID from PNExpense 
							where PlanID=@NewPlanID and WBS1=@NewWBS1 
							and isnull(WBS2,'')=@WBS2 
							and isnull(WBS3,'')=@WBS3 
							and isnull(Account,'')=@Account 
							and isnull(Vendor,'')=@Vendor 

						Update PNPlannedExpenses Set PlanID=@NewPlanID, TaskID=@NewTaskID, ExpenseID=@NewExpenseID where ExpenseID=@OldExpenseID
						Update PNBaselineExpenses Set PlanID=@NewPlanID, TaskID=@NewTaskID, ExpenseID=@NewExpenseID where ExpenseID=@OldExpenseID

						delete PNExpense Where TaskID=@OldTaskID and WBS1=@WBS1 
							and isnull(WBS2,'')=@WBS2 
							and isnull(WBS3,'')=@WBS3 
							and isnull(Account,'')=@Account
							and isnull(Vendor,'')=@Vendor 

						fetch next from ExpenseCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldExpenseID
						set @TempFetch = @@Fetch_Status
					end
					close ExpenseCursor
					deallocate ExpenseCursor

-- Move Planned and Basedline for the duplicate PNConsultant, Delete duplicate PNConsultant
					declare ConsultantCursor cursor for
						select TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(Account,'') as Account, isnull(Vendor,'') as Vendor, MIN(ConsultantID) as ConsultantID  
						from PNConsultant where PlanID=@OldPlanID and WBS1=@OldWBS1 and
						exists (select 'x' from PNConsultant new where PlanID=@NewPlanID and new.wbs1 = @NewWBS1 
							and isnull(new.wbs2,'') = isnull(PNConsultant.wbs2, '')
							and isnull(new.wbs3,'') = isnull(PNConsultant.wbs3,'')
							and isnull(new.Account,'') = isnull(PNConsultant.Account,'')
							and isnull(new.Vendor,'') = isnull(PNConsultant.Vendor,''))
						group by TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(Account,''), isnull(Vendor,'')
					open ConsultantCursor
					fetch next from ConsultantCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldConsultantID
					set @TempFetch = @@Fetch_Status
					While (@TempFetch = 0)
					begin
						select @NewTaskID=TaskID, @NewConsultantID=ConsultantID from PNConsultant 
							where PlanID=@NewPlanID and WBS1=@NewWBS1 
							and isnull(WBS2,'')=@WBS2 
							and isnull(WBS3,'')=@WBS3 
							and isnull(Account,'')=@Account 
							and isnull(Vendor,'')=@Vendor 

						Update PNPlannedConsultant Set PlanID=@NewPlanID, TaskID=@NewTaskID, ConsultantID=@NewConsultantID where ConsultantID=@OldConsultantID
						Update PNBaselineConsultant Set PlanID=@NewPlanID, TaskID=@NewTaskID, ConsultantID=@NewConsultantID where ConsultantID=@OldConsultantID

						delete PNConsultant Where TaskID=@OldTaskID and WBS1=@WBS1 
							and isnull(WBS2,'')=@WBS2 
							and isnull(WBS3,'')=@WBS3 
							and isnull(Account,'')=@Account
							and isnull(Vendor,'')=@Vendor 

						fetch next from ConsultantCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @Account, @Vendor, @OldConsultantID
						set @TempFetch = @@Fetch_Status
					end
					close ConsultantCursor
					deallocate ConsultantCursor

-- Move Everything related for the duplicate PNTask
					declare TaskCursor cursor for
						select TaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, isnull(WBSType,'') as WBSType 
						from PNTask where PlanID=@OldPlanID and WBS1=@OldWBS1 and
						exists (select 'x' from PNTask new where PlanID=@NewPlanID and new.wbs1 = @NewWBS1 
							and isnull(new.wbs2,'') = isnull(PNTask.wbs2, '')
							and isnull(new.wbs3,'') = isnull(PNTask.wbs3,'')
							and isnull(new.LaborCode,'') = isnull(PNTask.LaborCode,'')
							and isnull(new.WBSType,'') = isnull(PNTask.WBSType,''))
						group by TaskID, WBS1, isnull(WBS2,''), isnull(WBS3,''), isnull(LaborCode,''), isnull(WBSType,'')
					open TaskCursor
					fetch next from TaskCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @LaborCode, @WBSType
					set @TempFetch = @@Fetch_Status
					While (@TempFetch = 0)
					begin
if (@diag = 1) Print 'Update all related rptask tables ' + @NewPlanID +'/'+ @NewWBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+@WBSType
						select @NewTaskID = TaskID from PNTask 
							where PlanID=@NewPlanID and WBS1=@NewWBS1 
							and isnull(WBS2,'')=@WBS2 
							and isnull(WBS3,'')=@WBS3 
							and isnull(LaborCode,'')=@LaborCode 
							and isnull(WBSType,'')=@WBSType 

if (@diag = 1) Print 'Update PNTask Where TaskID=@OldTaskID and WBS1=@WBS1 ' + @OldTaskID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+@WBSType
						if (@UseBookingForEmpHours = 'Y')
							Update PNPlannedLabor Set HardBooked=@RPHardBooking 
							From PNPlannedLabor Left Join PNAssignment on PNPlannedLabor.AssignmentID=PNAssignment.AssignmentID
							Where PNPlannedLabor.TaskID=@OldTaskID and PNAssignment.ResourceID > '' and PNPlannedLabor.HardBooked<>@RPHardBooking 
						if (@UseBookingForGenHours = 'Y')
							Update PNPlannedLabor Set HardBooked=@RPHardBooking 
							From PNPlannedLabor Left Join PNAssignment on PNPlannedLabor.AssignmentID=PNAssignment.AssignmentID
							Where PNPlannedLabor.TaskID=@OldTaskID and PNAssignment.GenericResourceID > '' and PNPlannedLabor.HardBooked<>@RPHardBooking 

						Update PNPlannedLabor Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
						Update PNPlannedExpenses Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
						Update PNPlannedConsultant Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID

						Update PNBaselineLabor Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
						Update PNBaselineExpenses Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
						Update PNBaselineConsultant Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID

						Update PNAssignment Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
						Update PNExpense Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
						Update PNConsultant Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID

						if (@OldAllocMethod = 'r' and @NewAllocMethod <> 'r')
							begin	--delete Old
								Delete PNPlannedRevenueLabor where TaskID=@OldTaskID
								Delete PNBaselineRevenueLabor where TaskID=@OldTaskID
								Delete RPPlannedRevenueLabor where TaskID=@OldTaskID
								Delete RPBaselineRevenueLabor where TaskID=@OldTaskID
							end
						if (@OldAllocMethod = 'r' and @NewAllocMethod = 'r')
							begin	--merge Old into new
								Update PNPlannedRevenueLabor Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
								Update PNBaselineRevenueLabor Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
								Update RPPlannedRevenueLabor Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
								Update RPBaselineRevenueLabor Set PlanID=@NewPlanID, TaskID=@NewTaskID where TaskID=@OldTaskID
							end

if (@diag = 1) Print 'delete PNTask Where TaskID=@OldTaskID and WBS1=@WBS1 ' + @OldTaskID +'/'+ @WBS1 +'/'+ @WBS2 +'/'+ @WBS3 +'/'+ @LaborCode +'/'+@WBSType
						delete PNTask Where TaskID=@OldTaskID

						fetch next from TaskCursor into @OldTaskID, @WBS1, @WBS2, @WBS3, @LaborCode, @WBSType
						set @TempFetch = @@Fetch_Status
					end
					close TaskCursor
					deallocate TaskCursor

-- merge PNTask, PNAssignment, PNExpense, PNConsultant
					Update PNPlannedLabor Set PlanID=@NewPlanID Where PlanID=@OldPlanID
					Update PNPlannedExpenses Set PlanID=@NewPlanID Where PlanID=@OldPlanID
					Update PNPlannedConsultant Set PlanID=@NewPlanID Where PlanID=@OldPlanID

					Update PNBaselineLabor Set PlanID=@NewPlanID Where PlanID=@OldPlanID
					Update PNBaselineExpenses Set PlanID=@NewPlanID Where PlanID=@OldPlanID
					Update PNBaselineConsultant Set PlanID=@NewPlanID Where PlanID=@OldPlanID

					Update PNTask Set PlanID=@NewPlanID, WBS1=@NewWBS1 Where PlanID=@OldPlanID and PNTask.WBS1 = @OldWBS1
					Update PNAssignment Set PlanID=@NewPlanID, WBS1=@NewWBS1 Where PlanID=@OldPlanID and PNAssignment.WBS1 = @OldWBS1
					Update PNExpense Set PlanID=@NewPlanID, WBS1=@NewWBS1 Where PlanID=@OldPlanID and PNExpense.WBS1 = @OldWBS1
					Update PNConsultant Set PlanID=@NewPlanID, WBS1=@NewWBS1 Where PlanID=@OldPlanID and PNConsultant.WBS1 = @OldWBS1

					if (@OldAllocMethod = 'r' and @NewAllocMethod <> 'r')
						begin	--delete Old
							Delete PNPlannedRevenueLabor where PlanID=@OldPlanID
							Delete PNBaselineRevenueLabor where PlanID=@OldPlanID
							Delete RPPlannedRevenueLabor where PlanID=@OldPlanID
							Delete RPBaselineRevenueLabor where PlanID=@OldPlanID
						end
					if (@OldAllocMethod = 'r' and @NewAllocMethod = 'r')
						begin	--merge Old into new
							Update PNPlannedRevenueLabor Set PlanID=@NewPlanID Where PlanID=@OldPlanID
							Update PNBaselineRevenueLabor Set PlanID=@NewPlanID Where PlanID=@OldPlanID
							Update RPPlannedRevenueLabor Set PlanID=@NewPlanID Where PlanID=@OldPlanID
							Update RPBaselineRevenueLabor Set PlanID=@NewPlanID Where PlanID=@OldPlanID

							Update PNTask Set RevenueStartDate = isnull(MinStartDate1, MinStartDate2)
							From PNTask Inner Join 
							(	select PlanID, TaskID, outlinenumber,
								(select Min(StartDate) from PNPlannedRevenueLabor where PlanID=PNTask.PlanID and TaskID=PNTask.TaskID) as MinStartDate1
								, (	select Min(StartDate) from PNPlannedRevenueLabor L 
									inner join ( select PlanID, TaskID from PNTask TT where TT.PlanID=PNTask.PlanID and TT.OutlineNumber like PNTask.OutlineNumber+'.%' ) T 
									on T.PlanID=L.PlanID and T.TaskID=L.TaskID
								  ) as MinStartDate2
								from PNTask  Where PNTask.PlanID=@NewPlanID 
							) T on PNTask.PlanID=T.PlanID and PNTask.TaskID=T.TaskID
							Where PNTask.PlanID=@NewPlanID and RevenueStartDate > isnull(MinStartDate1, MinStartDate2)

							Update PNTask Set RevenueEndDate = isnull(MaxEndDate1, MaxEndDate2)
							From PNTask Inner Join 
							(	select PlanID, TaskID, outlinenumber,
								(select Max(EndDate) from PNPlannedRevenueLabor where PlanID=PNTask.PlanID and TaskID=PNTask.TaskID) as MaxEndDate1
								, (	select Max(EndDate) from PNPlannedRevenueLabor L 
									inner join ( select PlanID, TaskID from PNTask TT where TT.PlanID=PNTask.PlanID and TT.OutlineNumber like PNTask.OutlineNumber+'.%' ) T 
									on T.PlanID=L.PlanID and T.TaskID=L.TaskID
								  ) as MaxEndDate2
								from PNTask  Where PNTask.PlanID=@NewPlanID 
							) T on PNTask.PlanID=T.PlanID and PNTask.TaskID=T.TaskID
							Where PNTask.PlanID=@NewPlanID and RevenueEndDate < isnull(MaxEndDate1, MaxEndDate2)

							Update RPTask Set RevenueStartDate = isnull(MinStartDate1, MinStartDate2)
							From RPTask Inner Join 
							(	select PlanID, TaskID, outlinenumber,
								(select Min(StartDate) from RPPlannedRevenueLabor where PlanID=RPTask.PlanID and TaskID=RPTask.TaskID) as MinStartDate1
								, (	select Min(StartDate) from RPPlannedRevenueLabor L 
									inner join ( select PlanID, TaskID from RPTask TT where TT.PlanID=RPTask.PlanID and TT.OutlineNumber like RPTask.OutlineNumber+'.%' ) T 
									on T.PlanID=L.PlanID and T.TaskID=L.TaskID
								  ) as MinStartDate2
								from RPTask  Where RPTask.PlanID=@NewPlanID 
							) T on RPTask.PlanID=T.PlanID and RPTask.TaskID=T.TaskID
							Where RPTask.PlanID=@NewPlanID and RevenueStartDate > isnull(MinStartDate1, MinStartDate2)

							Update RPTask Set RevenueEndDate = isnull(MaxEndDate1, MaxEndDate2)
							From RPTask Inner Join 
							(	select PlanID, TaskID, outlinenumber,
								(select Max(EndDate) from RPPlannedRevenueLabor where PlanID=RPTask.PlanID and TaskID=RPTask.TaskID) as MaxEndDate1
								, (	select Max(EndDate) from RPPlannedRevenueLabor L 
									inner join ( select PlanID, TaskID from RPTask TT where TT.PlanID=RPTask.PlanID and TT.OutlineNumber like RPTask.OutlineNumber+'.%' ) T 
									on T.PlanID=L.PlanID and T.TaskID=L.TaskID
								  ) as MaxEndDate2
								from RPTask  Where RPTask.PlanID=@NewPlanID 
							) T on RPTask.PlanID=T.PlanID and RPTask.TaskID=T.TaskID
							Where RPTask.PlanID=@NewPlanID and RevenueEndDate < isnull(MaxEndDate1, MaxEndDate2)

						end
				End
-- Combine Navigator Plans End

			if (@OldPlanID > '' and @NPlanSourceExist='Y')
			begin
				Execute dbo.PMDelPlan @OldPlanID
				Execute dbo.stRPPublish @OldPlanID
			end
			if (@NewPlanID > '' and @NPlanTargetExist='Y')
			begin
				Execute dbo.PMDelPlan @NewPlanID
				Execute dbo.stRPPublish @NewPlanID
			end
-- Convert Navigator Plan to Vision Plan
			if (@NPlanSourceExist = 'Y' and @NPlanTargetExist = 'N' and @VPlanTargetExist = 'Y')
				Begin
if (@Diag = 1)	print 'EXECUTE dbo.PMDelPlan @OldPlanID : ' + @OldPlanID
					EXECUTE dbo.PMDelPlan @OldPlanID
if (@Diag = 1)	print 'EXECUTE dbo.stRPPublish @OldPlanID : ' + @OldPlanID
					EXECUTE dbo.stRPPublish @OldPlanID
if (@Diag = 1)	print 'EXECUTE dbo.PNDelNavPlan @OldPlanID : ' + @OldPlanID
					EXECUTE dbo.PNDelNavPlan @OldPlanID
				End

/* Removed by DP 4.0
			  exec @RetVal = KeyCvt @Entity = @Entity,
											@CvtType = @CvtType,
											@Length = 20,
											@ErrMsg = @ErrMsg2 output,
											@Diag = @Diag,
											@ConstrFlag = 3 -- UpdateTables
*/
							--Project -> Project
/*new for 3.0*/
--PRChargeCompanies	WBS1
--PRChargeCompanies	WBS2
--PRChargeCompanies	WBS3
--PRChargeCompanies	Company
	         Delete FROM PRChargeCompanies WHERE PRChargeCompanies.WBS1 = @OldWBS1 and
				exists (select 'x' from PRChargeCompanies new where new.wbs1 = @NewWBS1 
					and new.Company = PRChargeCompanies.Company 
					and new.wbs2 = PRChargeCompanies.wbs2 
					and new.wbs3 = PRChargeCompanies.wbs3)
/* end 3.0*/

/*new for 5.0*/
	         Delete FROM CustomProposalProject WHERE CustomProposalProject.WBS1 = @OldWBS1 and
				exists (select 'x' from CustomProposalProject new where new.wbs1 = @NewWBS1 
					and new.CustomPropID = CustomProposalProject.CustomPropID 
					and new.SectionID = CustomProposalProject.SectionID 
					and new.wbs2 = CustomProposalProject.wbs2 
					and new.wbs3 = CustomProposalProject.wbs3)
/* end 5.0*/



/*
ProjectsFromLeads       LeadID       1       ProjectsFromLeadsPK
ProjectsFromLeads       WBS1         2       ProjectsFromLeadsPK
ProjectsFromLeads       WBS2         3       ProjectsFromLeadsPK
ProjectsFromLeads       WBS3         4       ProjectsFromLeadsPK
PRProjectCodesTemplate  WBS1         1       PRProjectCodesTemplatePK
PRProjectCodesTemplate  WBS2         2       PRProjectCodesTemplatePK
PRProjectCodesTemplate  WBS3         3       PRProjectCodesTemplatePK
PRProjectCodesTemplate  ProjectCode  4       PRProjectCodesTemplatePK
PRTemplate              WBS1         1       PRTemplatePK
PRTemplate              WBS2         2       PRTemplatePK
PRTemplate              WBS3         3       PRTemplatePK
SF255ProjectGraphics    SF255ID      1       SF255ProjectGraphicsPK
SF255ProjectGraphics    WBS1         2       SF255ProjectGraphicsPK
SF255ProjectGraphics    FederalInd   3       SF255ProjectGraphicsPK
SF255ProjectGraphics    LinkID       4       SF255ProjectGraphicsPK*/
/*new 2.0 tables*/

	         Delete FROM PRProjectCodesTemplate WHERE PRProjectCodesTemplate.WBS1 = @OldWBS1 and
				exists (select 'x' from PRProjectCodesTemplate new where new.wbs1 = @NewWBS1 
					and new.ProjectCode = PRProjectCodesTemplate.ProjectCode 
					and new.wbs2 = PRProjectCodesTemplate.wbs2 
					and new.wbs3 = PRProjectCodesTemplate.wbs3)

	         Delete FROM PRTemplate WHERE PRTemplate.WBS1 = @OldWBS1 and
				exists (select 'x' from PRTemplate new where new.wbs1 = @NewWBS1 
					and new.wbs2 = PRTemplate.wbs2 
					and new.wbs3 = PRTemplate.wbs3)

	         Delete FROM SF255ProjectGraphics WHERE SF255ProjectGraphics.WBS1 = @OldWBS1 and
				exists (select 'x' from SF255ProjectGraphics new where new.wbs1 = @NewWBS1 
					and new.SF255ID = SF255ProjectGraphics.SF255ID 
					and new.FederalInd = SF255ProjectGraphics.FederalInd 
					and new.LinkID = SF255ProjectGraphics.LinkID)

				delete from MktCampaignProjectAssoc where MktCampaignProjectAssoc.WBS1 = @OldWBS1 and 
				exists (select 'x' from MktCampaignProjectAssoc new where new.wbs1 = @NewWBS1 
									and new.WBS2 = MktCampaignProjectAssoc.WBS2 
									and new.WBS3 = MktCampaignProjectAssoc.WBS3
									and new.CampaignID = MktCampaignProjectAssoc.CampaignID)

				delete from MktCampaignProjectAssocTemplate where MktCampaignProjectAssocTemplate.WBS1 = @OldWBS1 and 
				exists (select 'x' from MktCampaignProjectAssocTemplate new where new.wbs1 = @NewWBS1 
									and new.WBS2 = MktCampaignProjectAssocTemplate.WBS2 
									and new.WBS3 = MktCampaignProjectAssocTemplate.WBS3
									and new.CampaignID = MktCampaignProjectAssocTemplate.CampaignID)
/*end 2.0 tables */
				delete from reportWBSList where wbs1 = @oldwbs1

-- Removed by DP
--		         Delete FROM PR WHERE PR.WBS1 = @OldWBS1 and
--				exists (select 'x' from pr new where new.wbs1 = @NewWBS1 and new.wbs2 = pr.wbs2 and new.wbs3 = pr.wbs3)
-- Added by DP
				Delete from PRSummaryWBSList where WBS1 = @OldWBS1
--
				delete from EMProjectAssoc where EMProjectAssoc.WBS1 = @OldWBS1 and
				exists (select 'x' from EMProjectAssoc new where new.WBS1 = @NewWBS1 and new.wbs2 = EMProjectAssoc.WBS2 and new.wbs3 = EMProjectAssoc.wbs3 and new.Employee = EMProjectAssoc.Employee and new.Role = EMProjectAssoc.Role)

				delete from PRAwards where PRAwards.WBS1 = @OldWBS1 and
				exists (select 'x' from PRAwards new where new.WBS1 = @NewWBS1 and new.wbs2 = PRAwards.WBS2 and new.wbs3 = PRAwards.wbs3 and new.RecordID = PRAwards.RecordID)

				delete from PRDescriptions where PRDescriptions.WBS1 = @OldWBS1 and
				exists (select 'x' from PRDescriptions new where new.WBS1 = @NewWBS1 and new.wbs2 = PRDescriptions.WBS2 and new.wbs3 = PRDescriptions.wbs3 and new.DescCategory = PRDescriptions.DescCategory)

				delete from PRFileLinks where PRFileLinks.WBS1 = @OldWBS1 and
				exists (select 'x' from PRFileLinks new where new.WBS1 = @NewWBS1 and new.wbs2 = PRFileLinks.WBS2 and new.wbs3 = PRFileLinks.wbs3 and new.LinkID = PRFileLinks.LinkID)

				delete from PRProjectCodes where PRProjectCodes.WBS1 = @OldWBS1 and
				exists (select 'x' from PRProjectCodes new where new.WBS1 = @NewWBS1 and new.wbs2 =  PRProjectCodes.WBS2 and new.wbs3 = PRProjectCodes.wbs3 and new.ProjectCode = PRProjectCodes.ProjectCode)

				IF EXISTS (SELECT 'x' FROM sys.objects WHERE name = 'ProjectCustomTabFields' AND type = 'U')
					begin				
						delete from ProjectCustomTabFields where ProjectCustomTabFields.WBS1 = @OldWBS1 and
						exists (select 'x' from ProjectCustomTabFields new where new.WBS1 = @NewWBS1 and new.wbs2 =  ProjectCustomTabFields.WBS2 and new.wbs3 = ProjectCustomTabFields.wbs3)
					end
				delete from inMaster where inMaster.WBS1 = @OldWBS1  and
				exists (select 'x' from inMaster new where new.WBS1 = @NewWBS1 and new.wbs2 = inMaster.WBS2 and new.wbs3 = inMaster.wbs3 and new.Batch = inMaster.Batch and new.Invoice = inMaster.Invoice)

				delete from PRClientAssoc where PRClientAssoc.WBS1 = @OldWBS1 and
				exists (select 'x' from PRClientAssoc new where new.WBS1 = @NewWBS1 and PRClientAssoc.ClientID = new.ClientID)

				delete from PRContactAssoc where PRContactAssoc.WBS1 = @OldWBS1 and
				exists (select 'x' from PRContactAssoc new where new.WBS1 = @NewWBS1 and PRContactAssoc.ContactID = new.ContactID)

				delete from PRSubscr where PRSubscr.WBS1 = @oldWBS1 and
				exists (select 'x' from PRSubscr new where new.WBS1 = @NewWBS1 and PRSubscr.userName = new.username)

				delete from ClendorProjectAssoc where ClendorProjectAssoc.WBS1 = @OldWBS1 and
				exists (select 'x' from ClendorProjectAssoc new where new.WBS1 = @NewWBS1 and ClendorProjectAssoc.ClientID = new.ClientID)

				Update ClendorProjectAssoc Set Role=' ' from ClendorProjectAssoc where ClendorProjectAssoc.WBS1 = @OldWBS1 and Role='sysOwner' and 
				exists (select 'x' from ClendorProjectAssoc new where new.WBS1 = @NewWBS1 and Role='sysOwner')

				Update PRContactAssoc Set Role=' ' from PRContactAssoc where PRContactAssoc.WBS1 = @OldWBS1 and Role='sysOwner' and
				exists (select 'x' from PRContactAssoc new where new.WBS1 = @NewWBS1 and Role='sysOwner')

				Update ClendorProjectAssoc Set PrimaryInd='N' from ClendorProjectAssoc where ClendorProjectAssoc.WBS1 = @OldWBS1 and PrimaryInd='Y' and 
				exists (select 'x' from ClendorProjectAssoc new where new.WBS1 = @NewWBS1 and PrimaryInd='Y')

				Update PRContactAssoc Set PrimaryInd='N' from PRContactAssoc where PRContactAssoc.WBS1 = @OldWBS1 and PrimaryInd='Y' and
				exists (select 'x' from PRContactAssoc new where new.WBS1 = @NewWBS1 and PrimaryInd='Y')

				delete from PRCompetitionAssoc where PRCompetitionAssoc.WBS1 = @OldWBS1 and
				exists (select 'x' from PRCompetitionAssoc new where new.WBS1 = @NewWBS1 and PRCompetitionAssoc.ClientID = new.ClientID)

				Delete FROM InvoiceApprover Where InvoiceApprover.WBS1 = @OldWBS1 
                And Exists (Select 'x' from InvoiceApprover AS Tmp Where Tmp.WBS1 = @NewWBS1 and tmp.Employee = InvoiceApprover.Employee)
 
				Delete FROM InvoiceApproval Where InvoiceApproval.WBS1 = @OldWBS1 
				And Exists (Select 'x' from InvoiceApproval AS Tmp Where Tmp.WBS1 = @NewWBS1 and tmp.Invoice = InvoiceApproval.Invoice and tmp.PreInvoice = InvoiceApproval.PreInvoice)
 
				Delete FROM InvoiceApprovalActions Where InvoiceApprovalActions.WBS1 = @OldWBS1 
				And Exists (Select 'x' from InvoiceApprovalActions AS Tmp Where Tmp.WBS1 = @NewWBS1 and tmp.Invoice = InvoiceApprovalActions.Invoice and tmp.PreInvoice = InvoiceApprovalActions.PreInvoice and tmp.Seq = InvoiceApprovalActions.Seq)

				Delete FROM InvoiceVoid Where InvoiceVoid.WBS1 = @OldWBS1 
                And Exists (Select 'x' from InvoiceVoid AS Tmp Where Tmp.WBS1 = @NewWBS1 and tmp.Invoice = InvoiceVoid.Invoice)

				update EB
				set eb.AmtBud =  eb.AmtBud + old.AmtBud,
				eb.BillBud = eb.BillBud + old.BillBud,
				eb.EtcAmt =  eb.EtcAmt + old.EtcAmt,
				eb.EacAmt = eb.EacAmt + old.EacAmt
				from eb old inner join eb on (eb.WBS2 = old.WBS2 and	eb.WBS3 = old.WBS3 and 	eb.Vendor = old.Vendor and	eb.Account = old.Account)
				where
					eb.WBS1 = @NewWBS1 and
					old.WBS1 = @OldWBS1


				Delete from EB where WBS1 = @OldWBS1 
					and exists (select 'x' from EB new where  new.WBS1 = @newWBS1 and
																			EB.WBS2 = new.WBS2 and
																			EB.WBS3 = new.WBS3 and 
																			eb.Vendor = new.Vendor and
																			EB.Account = new.Account)
				update LB 
				set LB.BillRate = LB.BillRate + old.BillRate,
					LB.HrsBud = LB.HrsBud + old.HrsBud,
					LB.AmtBud = LB.AmtBud + old.AmtBud,
					LB.BillBud = LB.BillBud + old.BillBud,
					LB.EtcHrs = LB.EtcHrs + old.EtcHrs,
					LB.EtcAmt = LB.EtcAmt + old.EtcAmt,
					LB.EacHrs = LB.EacHrs + old.EacHrs,
					LB.EacAmt = LB.EacAmt + old.EacAmt
				from LB old inner join LB on (LB.WBS2 = old.WBS2 and LB.WBS3 = old.WBS3 and LB.LaborCode = old.LaborCode)
				where
					LB.WBS1 = @NewWBS1 and
					old.wbs1 = @OldWBS1

				Delete from LB where WBS1 = @OldWBS1 
					and exists (select 'x' from LB new where  new.WBS1 = @newWBS1 and
																			LB.WBS2 = new.WBS2 and
																			LB.WBS3 = new.WBS3 and 
																			LB.LaborCode = new.LaborCode)
        --Delete from LB where lbWBS1 = @OldWBS1
        --Delete from EB where ebWBS1 = @OldWBS1
			update PRSummarySub set
				PRSummarySub.Revenue = PRSummarySub.Revenue + old.Revenue,
				PRSummarySub.DirectConsCost = PRSummarySub.DirectConsCost + old.DirectConsCost,
				PRSummarySub.DirectOtherCost = PRSummarySub.DirectOtherCost + old.DirectOtherCost,
				PRSummarySub.DirectConsBilling = PRSummarySub.DirectConsBilling + old.DirectConsBilling,
				PRSummarySub.DirectOtherBilling = PRSummarySub.DirectOtherBilling + old.DirectOtherBilling,
				PRSummarySub.InDirectCost = PRSummarySub.InDirectCost + old.InDirectCost,
				PRSummarySub.InDirectBilling = PRSummarySub.InDirectBilling + old.InDirectBilling,
				PRSummarySub.ReimbConsCost = PRSummarySub.ReimbConsCost + old.ReimbConsCost,
				PRSummarySub.ReimbOtherCost = PRSummarySub.ReimbOtherCost + old.ReimbOtherCost,
				PRSummarySub.ReimbConsBilling = PRSummarySub.ReimbConsBilling + old.ReimbConsBilling,
				PRSummarySub.ReimbOtherBilling = PRSummarySub.ReimbOtherBilling + old.ReimbOtherBilling,
				PRSummarySub.Hours = PRSummarySub.Hours + old.Hours,
				PRSummarySub.LaborCost = PRSummarySub.LaborCost + old.LaborCost,
				PRSummarySub.LaborBilling = PRSummarySub.LaborBilling + old.LaborBilling,
				PRSummarySub.Billed = PRSummarySub.Billed + old.Billed,
				PRSummarySub.BilledLab = PRSummarySub.BilledLab + old.BilledLab,
				PRSummarySub.BilledCons = PRSummarySub.BilledCons + old.BilledCons,
				PRSummarySub.BilledExp = PRSummarySub.BilledExp + old.BilledExp,
				PRSummarySub.BilledFee = PRSummarySub.BilledFee + old.BilledFee,
				PRSummarySub.BilledUnit = PRSummarySub.BilledUnit + old.BilledUnit,
				PRSummarySub.BilledAddOn = PRSummarySub.BilledAddOn + old.BilledAddOn,
				PRSummarySub.BilledTaxes = PRSummarySub.BilledTaxes + old.BilledTaxes,
				PRSummarySub.BilledInterest = PRSummarySub.BilledInterest + old.BilledInterest,
				PRSummarySub.BilledOther = PRSummarySub.BilledOther + old.BilledOther,
				PRSummarySub.AR = PRSummarySub.AR + old.AR,
				PRSummarySub.Received = PRSummarySub.Received + old.Received,
				PRSummarySub.Overhead = PRSummarySub.Overhead + old.Overhead,
				PRSummarySub.RevenueBillingCurrency = PRSummarySub.RevenueBillingCurrency + old.RevenueBillingCurrency,
				PRSummarySub.RevenueProjectCurrency = PRSummarySub.RevenueProjectCurrency + old.RevenueProjectCurrency,
				PRSummarySub.DirectConsCostProjectCurrency = PRSummarySub.DirectConsCostProjectCurrency + old.DirectConsCostProjectCurrency,
				PRSummarySub.DirectOtherCostProjectCurrency = PRSummarySub.DirectOtherCostProjectCurrency + old.DirectOtherCostProjectCurrency,
				PRSummarySub.ReimbConsCostProjectCurrency = PRSummarySub.ReimbConsCostProjectCurrency + old.ReimbConsCostProjectCurrency,
				PRSummarySub.ReimbOtherCostProjectCurrency = PRSummarySub.ReimbOtherCostProjectCurrency + old.ReimbOtherCostProjectCurrency,
				PRSummarySub.InDirectCostProjectCurrency = PRSummarySub.InDirectCostProjectCurrency + old.InDirectCostProjectCurrency,
				PRSummarySub.LaborCostProjectCurrency = PRSummarySub.LaborCostProjectCurrency + old.LaborCostProjectCurrency,
				PRSummarySub.BilledBillingCurrency = PRSummarySub.BilledBillingCurrency + old.BilledBillingCurrency,
				PRSummarySub.BilledLabBillingCurrency = PRSummarySub.BilledLabBillingCurrency + old.BilledLabBillingCurrency,
				PRSummarySub.BilledConsBillingCurrency = PRSummarySub.BilledConsBillingCurrency + old.BilledConsBillingCurrency,
				PRSummarySub.BilledExpBillingCurrency = PRSummarySub.BilledExpBillingCurrency + old.BilledExpBillingCurrency,
				PRSummarySub.BilledFeeBillingCurrency = PRSummarySub.BilledFeeBillingCurrency + old.BilledFeeBillingCurrency,
				PRSummarySub.BilledUnitBillingCurrency = PRSummarySub.BilledUnitBillingCurrency + old.BilledUnitBillingCurrency,
				PRSummarySub.BilledAddOnBillingCurrency = PRSummarySub.BilledAddOnBillingCurrency + old.BilledAddOnBillingCurrency,
				PRSummarySub.BilledTaxesBillingCurrency = PRSummarySub.BilledTaxesBillingCurrency + old.BilledTaxesBillingCurrency,
				PRSummarySub.BilledInterestBillingCurrency = PRSummarySub.BilledInterestBillingCurrency + old.BilledInterestBillingCurrency,
				PRSummarySub.BilledOtherBillingCurrency = PRSummarySub.BilledOtherBillingCurrency + old.BilledOtherBillingCurrency,
				PRSummarySub.BilledProjectCurrency = PRSummarySub.BilledProjectCurrency + old.BilledProjectCurrency,
				PRSummarySub.BilledLabProjectCurrency = PRSummarySub.BilledLabProjectCurrency + old.BilledLabProjectCurrency,
				PRSummarySub.BilledConsProjectCurrency = PRSummarySub.BilledConsProjectCurrency + old.BilledConsProjectCurrency,
				PRSummarySub.BilledExpProjectCurrency = PRSummarySub.BilledExpProjectCurrency + old.BilledExpProjectCurrency,
				PRSummarySub.BilledFeeProjectCurrency = PRSummarySub.BilledFeeProjectCurrency + old.BilledFeeProjectCurrency,
				PRSummarySub.BilledUnitProjectCurrency = PRSummarySub.BilledUnitProjectCurrency + old.BilledUnitProjectCurrency,
				PRSummarySub.BilledAddOnProjectCurrency = PRSummarySub.BilledAddOnProjectCurrency + old.BilledAddOnProjectCurrency,
				PRSummarySub.BilledTaxesProjectCurrency = PRSummarySub.BilledTaxesProjectCurrency + old.BilledTaxesProjectCurrency,
				PRSummarySub.BilledInterestProjectCurrency = PRSummarySub.BilledInterestProjectCurrency + old.BilledInterestProjectCurrency,
				PRSummarySub.BilledOtherProjectCurrency = PRSummarySub.BilledOtherProjectCurrency + old.BilledOtherProjectCurrency,
				PRSummarySub.ARProjectCurrency = PRSummarySub.ARProjectCurrency + old.ARProjectCurrency,
				PRSummarySub.ReceivedProjectCurrency = PRSummarySub.ReceivedProjectCurrency + old.ReceivedProjectCurrency,
				PRSummarySub.ReceivedBillingCurrency = PRSummarySub.ReceivedBillingCurrency + old.ReceivedBillingCurrency,
				PRSummarySub.OverheadProjectCurrency = PRSummarySub.OverheadProjectCurrency + old.OverheadProjectCurrency,
				PRSummarySub.LaborCostBillingCurrency = PRSummarySub.LaborCostBillingCurrency + old.LaborCostBillingCurrency,
				PRSummarySub.DirectConsCostBillingCurrency = PRSummarySub.DirectConsCostBillingCurrency + old.DirectConsCostBillingCurrency,
				PRSummarySub.DirectOtherCostBillingCurrency = PRSummarySub.DirectOtherCostBillingCurrency + old.DirectOtherCostBillingCurrency,
				PRSummarySub.ReimbConsCostBillingCurrency = PRSummarySub.ReimbConsCostBillingCurrency + old.ReimbConsCostBillingCurrency,
				PRSummarySub.ReimbOtherCostBillingCurrency = PRSummarySub.ReimbOtherCostBillingCurrency + old.ReimbOtherCostBillingCurrency,
				PRSummarySub.InDirectCostBillingCurrency = PRSummarySub.InDirectCostBillingCurrency + old.InDirectCostBillingCurrency,
				PRSummarySub.RegHours = PRSummarySub.RegHours + old.RegHours,
				PRSummarySub.OvtHours = PRSummarySub.OvtHours + old.OvtHours,
				PRSummarySub.SpecialOvtHours = PRSummarySub.SpecialOvtHours + old.SpecialOvtHours,
				PRSummarySub.LaborRegAmt = PRSummarySub.LaborRegAmt + old.LaborRegAmt,
				PRSummarySub.LaborOvtAmt = PRSummarySub.LaborOvtAmt + old.LaborOvtAmt,
				PRSummarySub.LaborSpecialOvtAmt = PRSummarySub.LaborSpecialOvtAmt + old.LaborSpecialOvtAmt,
				PRSummarySub.LaborRegAmtProjectCurrency = PRSummarySub.LaborRegAmtProjectCurrency + old.LaborRegAmtProjectCurrency,
				PRSummarySub.LaborOvtAmtProjectCurrency = PRSummarySub.LaborOvtAmtProjectCurrency + old.LaborOvtAmtProjectCurrency,
				PRSummarySub.LaborSpecialOvtAmtProjectCurrency = PRSummarySub.LaborSpecialOvtAmtProjectCurrency + old.LaborSpecialOvtAmtProjectCurrency,
				PRSummarySub.LaborRegAmtBillingCurrency = PRSummarySub.LaborRegAmtBillingCurrency + old.LaborRegAmtBillingCurrency,
				PRSummarySub.LaborOvtAmtBillingCurrency = PRSummarySub.LaborOvtAmtBillingCurrency + old.LaborOvtAmtBillingCurrency,
				PRSummarySub.LaborSpecialOvtAmtBillingCurrency = PRSummarySub.LaborSpecialOvtAmtBillingCurrency + old.LaborSpecialOvtAmtBillingCurrency

			from PRSummarySub old inner join PRSummarySub on 
				(PRSummarySub.WBS2 = old.WBS2 and	PRSummarySub.WBS3 = old.WBS3 and 	PRSummarySub.Period = old.Period)
			where
				PRSummarySub.WBS1 = @NewWBS1 and
				old.WBS1 = @OldWBS1

				Delete from PRSummarySub where WBS1 = @OldWBS1 
					and exists (select 'x' from PRSummarySub new where  new.WBS1 = @newWBS1 and new.WBS2 = PRSummarySub.WBS2 and new.WBS3 = PRSummarySub.WBS3 and new.Period = PRSummarySub.Period)

			update PRSummaryMain set
				PRSummaryMain.Revenue = PRSummaryMain.Revenue + old.Revenue,
				PRSummaryMain.Billed = PRSummaryMain.Billed + old.Billed,
				PRSummaryMain.BilledLab = PRSummaryMain.BilledLab + old.BilledLab,
				PRSummaryMain.BilledCons = PRSummaryMain.BilledCons + old.BilledCons,
				PRSummaryMain.BilledExp = PRSummaryMain.BilledExp + old.BilledExp,
				PRSummaryMain.BilledFee = PRSummaryMain.BilledFee + old.BilledFee,
				PRSummaryMain.BilledUnit = PRSummaryMain.BilledUnit + old.BilledUnit,
				PRSummaryMain.BilledAddOn = PRSummaryMain.BilledAddOn + old.BilledAddOn,
				PRSummaryMain.BilledTaxes = PRSummaryMain.BilledTaxes + old.BilledTaxes,
				PRSummaryMain.BilledInterest = PRSummaryMain.BilledInterest + old.BilledInterest,
				PRSummaryMain.BilledOther = PRSummaryMain.BilledOther + old.BilledOther,
				PRSummaryMain.AR = PRSummaryMain.AR + old.AR,
				PRSummaryMain.Received = PRSummaryMain.Received + old.Received,
				PRSummaryMain.LaborCost = PRSummaryMain.LaborCost + old.LaborCost,
				PRSummaryMain.LaborBilling = PRSummaryMain.LaborBilling + old.LaborBilling,
				PRSummaryMain.Unbilled = PRSummaryMain.Unbilled + old.Unbilled,
				PRSummaryMain.SpentCostLessOH = PRSummaryMain.SpentCostLessOH + old.SpentCostLessOH,
				PRSummaryMain.SpentBilling = PRSummaryMain.SpentBilling + old.SpentBilling,
				PRSummaryMain.GrossMargin = PRSummaryMain.GrossMargin + old.GrossMargin,
				PRSummaryMain.NetRevenueCost = PRSummaryMain.NetRevenueCost + old.NetRevenueCost,
				PRSummaryMain.NetRevenueBilling = PRSummaryMain.NetRevenueBilling + old.NetRevenueBilling,
				PRSummaryMain.ProfitCostLessOH = PRSummaryMain.ProfitCostLessOH + old.ProfitCostLessOH,
				PRSummaryMain.ProfitBilling = PRSummaryMain.ProfitBilling + old.ProfitBilling,
				PRSummaryMain.Overhead = PRSummaryMain.Overhead + old.Overhead,
				PRSummaryMain.RevenueBillingCurrency = PRSummaryMain.RevenueBillingCurrency + old.RevenueBillingCurrency,
				PRSummaryMain.RevenueProjectCurrency = PRSummaryMain.RevenueProjectCurrency + old.RevenueProjectCurrency,
				PRSummaryMain.ARProjectCurrency = PRSummaryMain.ARProjectCurrency + old.ARProjectCurrency,
				PRSummaryMain.ARBillingCurrency = PRSummaryMain.ARBillingCurrency + old.ARBillingCurrency,
				PRSummaryMain.ReceivedProjectCurrency = PRSummaryMain.ReceivedProjectCurrency + old.ReceivedProjectCurrency,
				PRSummaryMain.ReceivedBillingCurrency = PRSummaryMain.ReceivedBillingCurrency + old.ReceivedBillingCurrency,
				PRSummaryMain.LaborCostProjectCurrency = PRSummaryMain.LaborCostProjectCurrency + old.LaborCostProjectCurrency,
				PRSummaryMain.LaborCostBillingCurrency = PRSummaryMain.LaborCostBillingCurrency + old.LaborCostBillingCurrency,
				PRSummaryMain.SpentCostLessOHProjectCurrency = PRSummaryMain.SpentCostLessOHProjectCurrency + old.SpentCostLessOHProjectCurrency,
				PRSummaryMain.SpentCostLessOHBillingCurrency = PRSummaryMain.SpentCostLessOHBillingCurrency + old.SpentCostLessOHBillingCurrency,
				PRSummaryMain.GrossMarginProjectCurrency = PRSummaryMain.GrossMarginProjectCurrency + old.GrossMarginProjectCurrency,
				PRSummaryMain.GrossMarginBillingCurrency = PRSummaryMain.GrossMarginBillingCurrency + old.GrossMarginBillingCurrency,
				PRSummaryMain.NetRevenueCostProjectCurrency = PRSummaryMain.NetRevenueCostProjectCurrency + old.NetRevenueCostProjectCurrency,
				PRSummaryMain.NetRevenueCostBillingCurrency = PRSummaryMain.NetRevenueCostBillingCurrency + old.NetRevenueCostBillingCurrency,
				PRSummaryMain.ProfitCostLessOHProjectCurrency = PRSummaryMain.ProfitCostLessOHProjectCurrency + old.ProfitCostLessOHProjectCurrency,
				PRSummaryMain.ProfitCostLessOHBillingCurrency = PRSummaryMain.ProfitCostLessOHBillingCurrency + old.ProfitCostLessOHBillingCurrency,
				PRSummaryMain.OverheadProjectCurrency = PRSummaryMain.OverheadProjectCurrency + old.OverheadProjectCurrency

			from PRSummaryMain old inner join PRSummaryMain on 
				(PRSummaryMain.WBS2 = old.WBS2 and	PRSummaryMain.WBS3 = old.WBS3 and 	PRSummaryMain.Period = old.Period)
			where
				PRSummaryMain.WBS1 = @NewWBS1 and
				old.WBS1 = @OldWBS1

				Delete from PRSummaryMain where WBS1 = @OldWBS1 
					and exists (select 'x' from PRSummaryMain new where  new.WBS1 = @newWBS1 and new.WBS2 = PRSummaryMain.WBS2 and new.WBS3 = PRSummaryMain.WBS3 and new.Period = PRSummaryMain.Period)

		Delete from billInvMaster where BillWBS1 = @OldWBS1 
			and exists (select 'x' from billInvMaster new where  new.BillWBS1 = @newWBS1 and new.BillWBS2 = billInvMaster.BillWBS2 and new.BillWBS3 = billInvMaster.BillWBS3 and new.Invoice = billInvMaster.Invoice)

	     Delete from BTA where WBS1 = @OldWBS1
	     Delete from BTF where WBS1 = @OldWBS1
	     Delete from BTSchedule where WBS1 = @OldWBS1
	     Delete from BTTaxCodes where WBS1 = @OldWBS1
	     Delete from BT where WBS1 = @OldWBS1
        Delete from BTBG where MainWBS1 = @OldWBS1
        Delete from BTBGSubs where MainWBS1 = @OldWBS1
        Delete from BTBGSubs where SubWBS1 = @OldWBS1
        Delete from billAddDetail where MainWBS1 = @OldWBS1
        Delete from billARDetail where MainWBS1 = @OldWBS1
        Delete from billBTDDetail where MainWBS1 = @OldWBS1
        Delete from billConDetail where MainWBS1 = @OldWBS1
        Delete from billExpDetail where MainWBS1 = @OldWBS1
        Delete from billFeeDetail where MainWBS1 = @OldWBS1
        Delete from billIntDetail where MainWBS1 = @OldWBS1
        Delete from billINDetail where MainWBS1 = @OldWBS1
        Delete from billINMaster where MainWBS1 = @OldWBS1
        Delete from billINDetail where WBS1 = @OldWBS1
        Delete from billINMaster where WBS1 = @OldWBS1
        Delete from billInvMaster where MainWBS1 = @OldWBS1
        Delete from billInvSums where MainWBS1 = @OldWBS1
        Delete from billLabDetail where MainWBS1 = @OldWBS1
        Delete from billLimDetail where MainWBS1 = @OldWBS1
        Delete from billRetDetail where MainWBS1 = @OldWBS1
        Delete from billTaxDetail where MainWBS1 = @OldWBS1
        Delete from billUnitDetail where MainWBS1 = @OldWBS1

        Delete from billFeeAllocation where MainWBS1 = @OldWBS1
        Delete from billExpWriteOffDelete where MainWBS1 = @OldWBS1
        Delete from billLabWriteOffDelete where MainWBS1 = @OldWBS1
        Delete from billUnitWriteOffDelete where MainWBS1 = @OldWBS1

/*added 3.0 sp5*/
        Delete from ICBillInvMaster where MainWBS1 = @OldWBS1
        Delete from ICBillLabDetail where MainWBS1 = @OldWBS1
        Delete from ICBillExpDetail where MainWBS1 = @OldWBS1
        Delete from ICBillTaxDetail where MainWBS1 = @OldWBS1
        Delete from ICBillInvSums where MainWBS1 = @OldWBS1

/*new for 6.2*/
		Delete FROM PRAdditionalData WHERE PRAdditionalData.WBS1 = @OldWBS1 and
			exists (select 'x' from PRAdditionalData new where new.wbs1 = @NewWBS1 
				and new.wbs2 = PRAdditionalData.wbs2 
				and new.wbs3 = PRAdditionalData.wbs3)
/*end 6.2*/

		DELETE FROM DocumentsPR WHERE DocumentsPR.WBS1 = @OldWBS1 AND 
		EXISTS (SELECT 'x' FROM DocumentsPR new 
		WHERE new.WBS1 = @NewWBS1 AND new.WBS2 =DocumentsPR.WBS2 AND new.WBS3 = DocumentsPR.WBS3 
		AND new.ListID = DocumentsPR.ListID AND new.FolderURL = DocumentsPR.FolderURL AND new.DocID = DocumentsPR.DocID)

        Delete from RevenueGroup where MainWBS1 = @OldWBS1 and
		exists (select 'x' from RevenueGroup new where new.MainWBS1 = @NewWBS1)
		Delete from RevenueGroupSubs where RevenueGroupSubs.MainWBS1 = @OldWBS1 and
		exists (select 'x' from RevenueGroupSubs new where new.MainWBS1 = @NewWBS1 and new.SubWBS1 = RevenueGroupSubs.SubWBS1)
		Delete from RevenueGroupSubs where RevenueGroupSubs.SubWBS1 = @OldWBS1 and
		exists (select 'x' from RevenueGroupSubs new where new.SubWBS1 = @NewWBS1 and new.MainWBS1 = RevenueGroupSubs.MainWBS1)

			Update PRF 
			set PRF.RegOH = PRF.RegOH + old.regoh,
				 PRF.GandAOH = PRF.GandAOH + old.GandAOH,
				 PRF.RegOHProjectCurrency =  PRF.RegOHProjectCurrency + old.RegOHProjectCurrency,
				 PRF.GandAOHProjectCurrency =  PRF.GandAOHProjectCurrency + old.GandAOHProjectCurrency
			from PRF old inner join PRF on (PRF.WBS2 = old.WBS2 and PRF.WBS3 = old.WBS3 and PRF.Period = old.Period)
			where PRF.WBS1 = @NewWBS1 and 
					old.WBS1 = @OldWbs1

        Delete FROM PRF  
                WHERE PRF.WBS1 = @OldWBS1 and 
					exists (select 'x' from PRF new where new.WBS1 = @NewWBS1 and
																	  new.WBS2 = PRF.WBS2 and
																	  new.WBS3 = PRF.WBS3 and
																	  new.Period = PRF.Period)
/*new for 7.4*/
		Delete from ARPreInvoice where ARPreInvoice.WBS1 = @OldWBS1 and 
			exists (select 'x' from ARPreInvoice new 
				where new.WBS1 = @NewWBS1 and new.WBS2 =ARPreInvoice.WBS2 and new.WBS3 = ARPreInvoice.WBS3 
				and new.PreInvoice = ARPreInvoice.PreInvoice)
		Delete from ARPreInvoiceDetail where ARPreInvoiceDetail.WBS1 = @OldWBS1 and 
			exists (select 'x' from ARPreInvoiceDetail new 
				where new.WBS1 = @NewWBS1 and new.WBS2 =ARPreInvoiceDetail.WBS2 and new.WBS3 = ARPreInvoiceDetail.WBS3 
				and new.PreInvoice = ARPreInvoiceDetail.PreInvoice and new.Seq = ARPreInvoiceDetail.Seq)
		Delete from BTFPhaseGroupData where BTFPhaseGroupData.WBS1 = @OldWBS1 and 
			exists (select 'x' from BTFPhaseGroupData new 
				where new.WBS1 = @NewWBS1 and new.WBS2 =BTFPhaseGroupData.WBS2 and new.WBS3 = BTFPhaseGroupData.WBS3 
				and new.Seq = BTFPhaseGroupData.Seq and new.PhaseGroup = BTFPhaseGroupData.PhaseGroup)
		Delete from BTFPhaseGroupDescriptions where BTFPhaseGroupDescriptions.WBS1 = @OldWBS1 and 
			exists (select 'x' from BTFPhaseGroupDescriptions new 
				where new.WBS1 = @NewWBS1 and new.WBS2 =BTFPhaseGroupDescriptions.WBS2 and new.WBS3 = BTFPhaseGroupDescriptions.WBS3 
				and new.Seq = BTFPhaseGroupDescriptions.Seq and new.PhaseGroup = BTFPhaseGroupDescriptions.PhaseGroup
				and new.UICultureName = BTFPhaseGroupDescriptions.UICultureName)

-- 7.4 Remove the Fee Estimate Service Profile of the From Project if To Project has Service Profile
		Delete FROM PRFEFuncGroups WHERE PRFEFuncGroups.WBS1 = @OldWBS1 and
			exists (select 'x' from PRFEFuncGroups new where new.wbs1 = @NewWBS1)
		Delete FROM PRFECostGroups WHERE PRFECostGroups.WBS1 = @OldWBS1 and
			exists (select 'x' from PRFEFuncGroups new where new.wbs1 = @NewWBS1)
		Delete FROM PRFEPhases WHERE PRFEPhases.WBS1 = @OldWBS1 and
			exists (select 'x' from PRFEFuncGroups new where new.wbs1 = @NewWBS1)
		Delete FROM PRFESpecialServices WHERE PRFESpecialServices.WBS1 = @OldWBS1 and
			exists (select 'x' from PRFEFuncGroups new where new.wbs1 = @NewWBS1)
			
		Update PR
			set PR.ServProCode = old.ServProCode,
			PR.FESurchargePct = old.FESurchargePct,
			PR.FESurcharge = old.FESurcharge,
			PR.FEAddlExpensesPct = old.FEAddlExpensesPct,
			PR.FEAddlExpenses = old.FEAddlExpenses,
			PR.FEOtherPct = old.FEOtherPct,
			PR.FEOther = old.FEOther
		from PR old inner join PR on (PR.WBS2 = ' ' and old.WBS2 = ' ')
		where
			old.WBS1 = @OldWBS1 and
			exists (select 'x' from PRFEFuncGroups new where new.wbs1 = @OldWBS1) and
			PR.WBS1 = @NewWBS1 and
			not exists (select 'x' from PRFEFuncGroups new where new.wbs1 = @NewWBS1)
			
		Delete FROM billPriorInvoices WHERE billPriorInvoices.MainWBS1 = @OldWBS1 and
			exists (select 'x' from billPriorInvoices new where new.MainWBS1 = @NewWBS1 
				and new.Invoice = billPriorInvoices.Invoice 
				and new.RecdSeq = billPriorInvoices.RecdSeq)
		Delete FROM billPreInvoice WHERE billPreInvoice.WBS1 = @OldWBS1 and
			exists (select 'x' from billPreInvoice new where new.wbs1 = @NewWBS1 
				and new.Invoice = billPreInvoice.Invoice 
				and new.PreInvoice = billPreInvoice.PreInvoice)
/*end 7.4*/

--Updating paid period to most recent
			Update AR
				set AR.PaidPeriod = old.PaidPeriod
			from AR old inner join AR on (AR.WBS2 = old.WBS2 and AR.WBS3 = old.WBS3 and AR.Invoice = old.Invoice )
			where
				AR.WBS1 = @NewWBS1 and
				old.WBS1 = @OldWBS1 and
				old.PaidPeriod > AR.PaidPeriod

			Delete from AR where AR.WBS1 = @OldWBS1 and 
				exists (select 'x' from AR new 
					where new.WBS1 = @NewWBS1 and new.WBS2 =AR.WBS2 and new.WBS3 = AR.WBS3 and new.Invoice = AR.invoice)

			
				/*handle duplicate custom tab records, possible when copying one project to another*/
				declare custTable cursor for
				select tableName from FW_CustomGrids where infocenterarea = N'Projects'
				
				open custTable
				fetch next from custTable into @TableName
				while (@@FETCH_STATUS = 0)
				begin
					set @Sql = 'Delete FROM ' + @TableName + ' WHERE ' + @TableName + '.WBS1 =N''' +  @OldWBS1 + ''' and ' +
								  'exists (select ''x'' from ' + @TableName + ' new where new.wbs1 = N''' + @NewWBS1 + ''' and new.wbs2 = ' + @TableName + '.wbs2 and new.wbs3 = ' + @TableName + '.wbs3 and new.seq = ' + @TableName + '.seq)'
					if (@Diag = 1)
						print @Sql
					execute(@Sql)
					fetch next from custTable into @TableName
				end
				close custTable
				deallocate custTable

-- Add by DP Europa
			declare @SyncProjToContractFees Nvarchar(1)
			declare @recalcComp Nvarchar(1)

-- Force @SyncProjToContractFees = 'N', @recalcComp = 'Y' for Storm 1.1.1
			--select @SyncProjToContractFees = SyncProjToContractFees, @recalcComp = AutoSumComp from FW_CFGSystem
			set @SyncProjToContractFees = 'N' 
			Set @recalcComp = 'Y'
-- Force @SyncProjToContractFees = 'N', @recalcComp = 'Y' for Storm 1.1.1

			--if (@SyncProjToContractFees = 'Y') 
			--	Begin
					update PR 
					set PR.Fee = PR.Fee + old.Fee,
						PR.ReimbAllow = PR.ReimbAllow + old.ReimbAllow,
						PR.ConsultFee = PR.ConsultFee + old.ConsultFee,
						PR.FeeBillingCurrency = PR.FeeBillingCurrency + old.FeeBillingCurrency,
						PR.ReimbAllowBillingCurrency = PR.ReimbAllowBillingCurrency + old.ReimbAllowBillingCurrency,
						PR.ConsultFeeBillingCurrency = PR.ConsultFeeBillingCurrency + old.ConsultFeeBillingCurrency,
						PR.FeeFunctionalCurrency = PR.FeeFunctionalCurrency + old.FeeFunctionalCurrency,
						PR.ReimbAllowFunctionalCurrency = PR.ReimbAllowFunctionalCurrency + old.ReimbAllowFunctionalCurrency,
						PR.ConsultFeeFunctionalCurrency = PR.ConsultFeeFunctionalCurrency + old.ConsultFeeFunctionalCurrency,
						PR.FeeDirLab = PR.FeeDirLab + old.FeeDirLab,
						PR.FeeDirExp = PR.FeeDirExp + old.FeeDirExp,
						PR.ReimbAllowExp = PR.ReimbAllowExp + old.ReimbAllowExp,
						PR.ReimbAllowCons = PR.ReimbAllowCons + old.ReimbAllowCons,
						PR.FeeDirLabBillingCurrency = PR.FeeDirLabBillingCurrency + old.FeeDirLabBillingCurrency,
						PR.FeeDirExpBillingCurrency = PR.FeeDirExpBillingCurrency + old.FeeDirExpBillingCurrency,
						PR.ReimbAllowExpBillingCurrency = PR.ReimbAllowExpBillingCurrency + old.ReimbAllowExpBillingCurrency,
						PR.ReimbAllowConsBillingCurrency = PR.ReimbAllowConsBillingCurrency + old.ReimbAllowConsBillingCurrency,
						PR.FeeDirLabFunctionalCurrency = PR.FeeDirLabFunctionalCurrency + old.FeeDirLabFunctionalCurrency,
						PR.FeeDirExpFunctionalCurrency = PR.FeeDirExpFunctionalCurrency + old.FeeDirExpFunctionalCurrency,
						PR.ReimbAllowExpFunctionalCurrency = PR.ReimbAllowExpFunctionalCurrency + old.ReimbAllowExpFunctionalCurrency,
						PR.ReimbAllowConsFunctionalCurrency = PR.ReimbAllowConsFunctionalCurrency + old.ReimbAllowConsFunctionalCurrency
					from PR old inner join PR on (PR.WBS2 = old.WBS2 and PR.WBS3 = old.WBS3)
					where
						PR.WBS1 = @NewWBS1 and
						old.wbs1 = @OldWBS1
				--End

-- Add by DP 4.0
			select * into #TempKeyConvert2 from PR where WBS1 = @OldWBS1 and 
			not exists (select 'x' from pr new where new.wbs1 = @NewWBS1 and new.wbs2 = pr.wbs2 and new.wbs3 = pr.wbs3)
			Update #TempKeyConvert2 set WBS1=@NewWBS1
			Insert PR select * from #TempKeyConvert2
			Drop Table #TempKeyConvert2

			select * into #TempKeyConvertBT2 from BT where WBS1 = @OldWBS1 and 
			not exists (select 'x' from BT new where new.wbs1 = @NewWBS1 and new.wbs2 = BT.wbs2 and new.wbs3 = BT.wbs3)
			Update #TempKeyConvertBT2 set WBS1=@NewWBS1
			Insert BT select * from #TempKeyConvertBT2
			Drop Table #TempKeyConvertBT2
			
-- RonK 07/15/09: Added Contracts for Rubik--update for merged WBS1s.
			-- Contracts Records:  *********************************
			-- First update non-duplicate Contracts to the target WBS1.
			Update Contracts Set Contracts.WBS1 = @NewWBS1 where Contracts.WBS1 = @OldWBS1 and not exists 
				(select 'x' from Contracts new where new.wbs1 = @NewWBS1 and new.ContractNumber = Contracts.ContractNumber)
													  
			-- Next update Contract records duplicated in the Target WBS1.
			Update Contracts Set Contracts.Notes = isnull(Contracts.Notes,'') + isnull(old.Notes,'')
			from Contracts old inner join Contracts on (Contracts.ContractNumber = old.ContractNumber)
			where Contracts.WBS1 = @NewWBS1 and old.WBS1 = @OldWbs1
			
			-- Next remove old Contracts records now merged.
        	Delete FROM Contracts WHERE Contracts.WBS1 = @OldWBS1 and exists 
				(select 'x' from Contracts new where new.wbs1 = @NewWBS1 and new.ContractNumber = Contracts.ContractNumber)
			
			-- ContractCredit Records:   
			-- First update non-duplicate ContractCredit to the target WBS1.
 			Update ContractCredit Set ContractCredit.WBS1 = @NewWBS1 WHERE ContractCredit.WBS1 = @OldWBS1 and not exists
				(select 'x' from ContractCredit new WHERE new.wbs1 = @NewWBS1 and new.ContractNumber = ContractCredit.ContractNumber
													 and new.Employee = ContractCredit.Employee)
													 
			-- Note: There are no merged fields for the ContractCredit records duplicated in the Target WBS1
			
			-- Next remove old ContractCredit records now merged. (If duplicated they will just be dropped)
        	Delete FROM ContractCredit WHERE WBS1 = @OldWBS1 
			
			-- ContractDetails Records:   ********************************
			-- First update non-duplicate ContractDetails to the target WBS1.
			Update ContractDetails Set WBS1 = @NewWBS1 where WBS1 = @OldWBS1 and not exists
				(select 'x' from ContractDetails new where new.wbs1 = @NewWBS1 and new.ContractNumber = ContractDetails.ContractNumber 
													  and new.wbs2 = ContractDetails.wbs2 and new.wbs3 = ContractDetails.wbs3)
													  
			-- Next update ContractDetails records duplicated in the Target WBS1.
			Update ContractDetails
			set ContractDetails.Fee = ContractDetails.Fee + old.Fee,
				ContractDetails.ReimbAllow = ContractDetails.ReimbAllow + old.ReimbAllow,
				ContractDetails.ConsultFee = ContractDetails.ConsultFee + old.ConsultFee, 
				ContractDetails.Total = ContractDetails.Total + old.Total, 
				ContractDetails.FeeBillingCurrency = ContractDetails.FeeBillingCurrency + old.FeeBillingCurrency,
				ContractDetails.ReimbAllowBillingCurrency = ContractDetails.ReimbAllowBillingCurrency + old.ReimbAllowBillingCurrency,
				ContractDetails.ConsultFeeBillingCurrency = ContractDetails.ConsultFeeBillingCurrency + old.ConsultFeeBillingCurrency, 
				ContractDetails.TotalBillingCurrency = ContractDetails.TotalBillingCurrency + old.TotalBillingCurrency, 
				ContractDetails.FeeFunctionalCurrency = ContractDetails.FeeFunctionalCurrency + old.FeeFunctionalCurrency,
				ContractDetails.ReimbAllowFunctionalCurrency = ContractDetails.ReimbAllowFunctionalCurrency + old.ReimbAllowFunctionalCurrency,
				ContractDetails.ConsultFeeFunctionalCurrency = ContractDetails.ConsultFeeFunctionalCurrency + old.ConsultFeeFunctionalCurrency, 
				ContractDetails.TotalFunctionalCurrency = ContractDetails.TotalFunctionalCurrency + old.TotalFunctionalCurrency,
				ContractDetails.FeeDirLab = ContractDetails.FeeDirLab + old.FeeDirLab,
				ContractDetails.FeeDirExp = ContractDetails.FeeDirExp + old.FeeDirExp,
				ContractDetails.ReimbAllowExp = ContractDetails.ReimbAllowExp + old.ReimbAllowExp,
				ContractDetails.ReimbAllowCons = ContractDetails.ReimbAllowCons + old.ReimbAllowCons,
				ContractDetails.FeeDirLabBillingCurrency = ContractDetails.FeeDirLabBillingCurrency + old.FeeDirLabBillingCurrency,
				ContractDetails.FeeDirExpBillingCurrency = ContractDetails.FeeDirExpBillingCurrency + old.FeeDirExpBillingCurrency,
				ContractDetails.ReimbAllowExpBillingCurrency = ContractDetails.ReimbAllowExpBillingCurrency + old.ReimbAllowExpBillingCurrency,
				ContractDetails.ReimbAllowConsBillingCurrency = ContractDetails.ReimbAllowConsBillingCurrency + old.ReimbAllowConsBillingCurrency,
				ContractDetails.FeeDirLabFunctionalCurrency = ContractDetails.FeeDirLabFunctionalCurrency + old.FeeDirLabFunctionalCurrency,
				ContractDetails.FeeDirExpFunctionalCurrency = ContractDetails.FeeDirExpFunctionalCurrency + old.FeeDirExpFunctionalCurrency,
				ContractDetails.ReimbAllowExpFunctionalCurrency = ContractDetails.ReimbAllowExpFunctionalCurrency + old.ReimbAllowExpFunctionalCurrency,
				ContractDetails.ReimbAllowConsFunctionalCurrency = ContractDetails.ReimbAllowConsFunctionalCurrency + old.ReimbAllowConsFunctionalCurrency
			from ContractDetails old inner join ContractDetails on (ContractDetails.ContractNumber = old.ContractNumber 
													and ContractDetails.WBS2 = old.WBS2 and ContractDetails.WBS3 = old.WBS3)
			where ContractDetails.WBS1 = @NewWBS1 and old.WBS1 = @OldWBS1
			
			-- Next remove old ContractDetails records now merged. (Shouldn't be anything else left on Old WBS1 ID)
        	Delete FROM ContractDetails WHERE WBS1 = @OldWBS1 
									  				  
			-- Must handle situation where now there are WBS2-only and WBS2/WBS3 ContractDetails for the same Contract.
			declare @ContractNumber	Nvarchar(30)
			declare @CombineWBS2 	Nvarchar(30)
			declare @FirstWBS3 		Nvarchar(30)

			declare contractTable cursor for
				Select ContractNumber, WBS2 as CombineWBS2, Min(WBS3) as FirstWBS3 
				from ContractDetails where WBS1 = @NewWBS1 and WBS3 > N' ' and exists
					(select 'x' from ContractDetails WBS2Only where WBS2Only.ContractNumber = ContractDetails.ContractNumber
						and WBS2Only.WBS1 = ContractDetails.WBS1 and WBS2Only.WBS2 = ContractDetails.WBS2 and WBS2Only.WBS3 <= N' ')
				group by ContractNumber, WBS2
								
			open contractTable
			fetch next from contractTable into @ContractNumber, @CombineWBS2, @FirstWBS3
			while (@@FETCH_STATUS = 0)
			begin
				Update ContractDetails
					set ContractDetails.Fee = ContractDetails.Fee + WBS2Only.Fee,
						ContractDetails.ReimbAllow = ContractDetails.ReimbAllow + WBS2Only.ReimbAllow,
						ContractDetails.ConsultFee = ContractDetails.ConsultFee + WBS2Only.ConsultFee, 
						ContractDetails.Total = ContractDetails.Total + WBS2Only.Total, 
						ContractDetails.FeeBillingCurrency = ContractDetails.FeeBillingCurrency + WBS2Only.FeeBillingCurrency,
						ContractDetails.ReimbAllowBillingCurrency = ContractDetails.ReimbAllowBillingCurrency + WBS2Only.ReimbAllowBillingCurrency,
						ContractDetails.ConsultFeeBillingCurrency = ContractDetails.ConsultFeeBillingCurrency + WBS2Only.ConsultFeeBillingCurrency, 
						ContractDetails.TotalBillingCurrency = ContractDetails.TotalBillingCurrency + WBS2Only.TotalBillingCurrency, 
						ContractDetails.FeeFunctionalCurrency = ContractDetails.FeeFunctionalCurrency + WBS2Only.FeeFunctionalCurrency,
						ContractDetails.ReimbAllowFunctionalCurrency = ContractDetails.ReimbAllowFunctionalCurrency + WBS2Only.ReimbAllowFunctionalCurrency,
						ContractDetails.ConsultFeeFunctionalCurrency = ContractDetails.ConsultFeeFunctionalCurrency + WBS2Only.ConsultFeeFunctionalCurrency, 
						ContractDetails.TotalFunctionalCurrency = ContractDetails.TotalFunctionalCurrency + WBS2Only.TotalFunctionalCurrency,
						ContractDetails.FeeDirLab = ContractDetails.FeeDirLab + WBS2Only.FeeDirLab,
						ContractDetails.FeeDirExp = ContractDetails.FeeDirExp + WBS2Only.FeeDirExp,
						ContractDetails.ReimbAllowExp = ContractDetails.ReimbAllowExp + WBS2Only.ReimbAllowExp,
						ContractDetails.ReimbAllowCons = ContractDetails.ReimbAllowCons + WBS2Only.ReimbAllowCons,
						ContractDetails.FeeDirLabBillingCurrency = ContractDetails.FeeDirLabBillingCurrency + WBS2Only.FeeDirLabBillingCurrency,
						ContractDetails.FeeDirExpBillingCurrency = ContractDetails.FeeDirExpBillingCurrency + WBS2Only.FeeDirExpBillingCurrency,
						ContractDetails.ReimbAllowExpBillingCurrency = ContractDetails.ReimbAllowExpBillingCurrency + WBS2Only.ReimbAllowExpBillingCurrency,
						ContractDetails.ReimbAllowConsBillingCurrency = ContractDetails.ReimbAllowConsBillingCurrency + WBS2Only.ReimbAllowConsBillingCurrency,
						ContractDetails.FeeDirLabFunctionalCurrency = ContractDetails.FeeDirLabFunctionalCurrency + WBS2Only.FeeDirLabFunctionalCurrency,
						ContractDetails.FeeDirExpFunctionalCurrency = ContractDetails.FeeDirExpFunctionalCurrency + WBS2Only.FeeDirExpFunctionalCurrency,
						ContractDetails.ReimbAllowExpFunctionalCurrency = ContractDetails.ReimbAllowExpFunctionalCurrency + WBS2Only.ReimbAllowExpFunctionalCurrency,
						ContractDetails.ReimbAllowConsFunctionalCurrency = ContractDetails.ReimbAllowConsFunctionalCurrency + WBS2Only.ReimbAllowConsFunctionalCurrency
					from ContractDetails WBS2Only inner join ContractDetails 
			 				on (ContractDetails.ContractNumber = WBS2Only.ContractNumber
			 						  and ContractDetails.WBS1 = WBS2Only.WBS1
			 						  and ContractDetails.WBS2 = WBS2Only.WBS2)
					Where WBS2Only.WBS1 = @NewWBS1 and WBS2Only.WBS3 <= N' ' 
			 			and ContractDetails.ContractNumber = @ContractNumber and ContractDetails.WBS1 = @NewWBS1 
			 			and ContractDetails.WBS2 = @CombineWBS2 and ContractDetails.WBS3 = @FirstWBS3 
			 		
				-- Next remove WBS2-Only ContractDetails record now merged.
        			Delete FROM ContractDetails WHERE ContractNumber = @ContractNumber and WBS1 = @NewWBS1 
        												    and WBS2 = @CombineWBS2 and WBS3 <= N' ' 
				fetch next from contractTable into @ContractNumber, @CombineWBS2, @FirstWBS3
			end

			if (@SyncProjToContractFees = 'Y') 
				Begin
					--SQLSyncFeesToContracts_Reset
					update PR set Fee = 0, ConsultFee = 0, ReimbAllow = 0, FeeBillingCurrency = 0, ConsultFeeBillingCurrency =0, 
					ReimbAllowBillingCurrency =0,   FeeFunctionalCurrency = 0, ConsultFeeFunctionalCurrency = 0, ReimbAllowFunctionalCurrency = 0,  
					FeeDirLab = 0,
					FeeDirExp = 0,
					ReimbAllowExp = 0,
					ReimbAllowCons = 0,
					FeeDirLabBillingCurrency = 0,
					FeeDirExpBillingCurrency = 0,
					ReimbAllowExpBillingCurrency = 0,
					ReimbAllowConsBillingCurrency = 0,
					FeeDirLabFunctionalCurrency = 0,
					FeeDirExpFunctionalCurrency = 0,
					ReimbAllowExpFunctionalCurrency = 0,
					ReimbAllowConsFunctionalCurrency = 0
					where WBS1 = @NewWBS1

					--SQLSyncFeesToContracts_ForPhaseMC
					update PR
					set PR.Fee = C.Fee, PR.ConsultFee = C.ConsultFee, PR.ReimbAllow = C.ReimbAllow,
						PR.FeeBillingCurrency = C.FeeBillingCurrency, PR.ConsultFeeBillingCurrency = C.ConsultFeeBillingCurrency, PR.ReimbAllowBillingCurrency = C.ReimbAllowBillingCurrency,
						PR.FeeFunctionalCurrency = C.FeeFunctionalCurrency, PR.ConsultFeeFunctionalCurrency = C.ConsultFeeFunctionalCurrency, PR.ReimbAllowFunctionalCurrency = C.ReimbAllowFunctionalCurrency,
						PR.FeeDirLab = C.FeeDirLab,
						PR.FeeDirExp = C.FeeDirExp,
						PR.ReimbAllowExp = C.ReimbAllowExp,
						PR.ReimbAllowCons = C.ReimbAllowCons,
						PR.FeeDirLabBillingCurrency = C.FeeDirLabBillingCurrency,
						PR.FeeDirExpBillingCurrency = C.FeeDirExpBillingCurrency,
						PR.ReimbAllowExpBillingCurrency = C.ReimbAllowExpBillingCurrency,
						PR.ReimbAllowConsBillingCurrency = C.ReimbAllowConsBillingCurrency,
						PR.FeeDirLabFunctionalCurrency = C.FeeDirLabFunctionalCurrency,
						PR.FeeDirExpFunctionalCurrency = C.FeeDirExpFunctionalCurrency,
						PR.ReimbAllowExpFunctionalCurrency = C.ReimbAllowExpFunctionalCurrency,
						PR.ReimbAllowConsFunctionalCurrency = C.ReimbAllowConsFunctionalCurrency
					from (select CD.WBS1, CD.WBS2, Sum(CD.Fee) Fee, Sum(CD.ConsultFee) ConsultFee, Sum(CD.ReimbAllow) ReimbAllow,
						Sum(CD.FeeBillingCurrency) FeeBillingCurrency, Sum(CD.ConsultFeeBillingCurrency) ConsultFeeBillingCurrency, Sum(CD.ReimbAllowBillingCurrency) ReimbAllowBillingCurrency,
						Sum(CD.FeeFunctionalCurrency) FeeFunctionalCurrency, Sum(CD.ConsultFeeFunctionalCurrency) ConsultFeeFunctionalCurrency, Sum(CD.ReimbAllowFunctionalCurrency) ReimbAllowFunctionalCurrency,
						Sum(CD.FeeDirLab) FeeDirLab, Sum(CD.FeeDirExp) FeeDirExp, Sum(CD.ReimbAllowExp) ReimbAllowExp, Sum(CD.ReimbAllowCons) ReimbAllowCons, 
						Sum(CD.FeeDirLabBillingCurrency) FeeDirLabBillingCurrency, Sum(CD.FeeDirExpBillingCurrency) FeeDirExpBillingCurrency,
						Sum(CD.ReimbAllowExpBillingCurrency) ReimbAllowExpBillingCurrency, Sum(CD.ReimbAllowConsBillingCurrency) ReimbAllowConsBillingCurrency, 
						Sum(CD.FeeDirLabFunctionalCurrency) FeeDirLabFunctionalCurrency, Sum(CD.FeeDirExpFunctionalCurrency) FeeDirExpFunctionalCurrency, 
						Sum(CD.ReimbAllowExpFunctionalCurrency) ReimbAllowExpFunctionalCurrency, Sum(CD.ReimbAllowConsFunctionalCurrency) ReimbAllowConsFunctionalCurrency
					from Contracts C, ContractDetails CD 
					where C.ContractNumber = CD.ContractNumber and C.WBS1 = CD.WBS1  and C.FeeIncludeInd = N'Y'
					 and CD.WBS1 = @NewWBS1 and  (CD.WBS2 <> N' ' or CD.WBS3 <> N' ')   
					group by CD.WBS1, CD.WBS2)  C
					where C.WBS1 = PR.WBS1 and C.WBS2 = PR.WBS2 and  PR.WBS3 = N' '

					--SQLSyncFeesToContracts_ForTaskMC
					update PR
					set PR.Fee = C.Fee, PR.ConsultFee = C.ConsultFee, PR.ReimbAllow = C.ReimbAllow,
						PR.FeeBillingCurrency = C.FeeBillingCurrency, PR.ConsultFeeBillingCurrency = C.ConsultFeeBillingCurrency, PR.ReimbAllowBillingCurrency = C.ReimbAllowBillingCurrency,
						PR.FeeFunctionalCurrency = C.FeeFunctionalCurrency, PR.ConsultFeeFunctionalCurrency = C.ConsultFeeFunctionalCurrency, PR.ReimbAllowFunctionalCurrency = C.ReimbAllowFunctionalCurrency,
						PR.FeeDirLab = C.FeeDirLab,
						PR.FeeDirExp = C.FeeDirExp,
						PR.ReimbAllowExp = C.ReimbAllowExp,
						PR.ReimbAllowCons = C.ReimbAllowCons,
						PR.FeeDirLabBillingCurrency = C.FeeDirLabBillingCurrency,
						PR.FeeDirExpBillingCurrency = C.FeeDirExpBillingCurrency,
						PR.ReimbAllowExpBillingCurrency = C.ReimbAllowExpBillingCurrency,
						PR.ReimbAllowConsBillingCurrency = C.ReimbAllowConsBillingCurrency,
						PR.FeeDirLabFunctionalCurrency = C.FeeDirLabFunctionalCurrency,
						PR.FeeDirExpFunctionalCurrency = C.FeeDirExpFunctionalCurrency,
						PR.ReimbAllowExpFunctionalCurrency = C.ReimbAllowExpFunctionalCurrency,
						PR.ReimbAllowConsFunctionalCurrency = C.ReimbAllowConsFunctionalCurrency
					from (select CD.WBS1,WBS2,WBS3,Sum(CD.Fee) Fee, Sum(CD.ConsultFee) ConsultFee, Sum(CD.ReimbAllow) ReimbAllow,
						Sum(CD.FeeBillingCurrency) FeeBillingCurrency, Sum(CD.ConsultFeeBillingCurrency) ConsultFeeBillingCurrency, Sum(CD.ReimbAllowBillingCurrency) ReimbAllowBillingCurrency,
						Sum(CD.FeeFunctionalCurrency) FeeFunctionalCurrency, Sum(CD.ConsultFeeFunctionalCurrency) ConsultFeeFunctionalCurrency, Sum(CD.ReimbAllowFunctionalCurrency) ReimbAllowFunctionalCurrency,
						Sum(CD.FeeDirLab) FeeDirLab, Sum(CD.FeeDirExp) FeeDirExp, Sum(CD.ReimbAllowExp) ReimbAllowExp, Sum(CD.ReimbAllowCons) ReimbAllowCons, 
						Sum(CD.FeeDirLabBillingCurrency) FeeDirLabBillingCurrency, Sum(CD.FeeDirExpBillingCurrency) FeeDirExpBillingCurrency,
						Sum(CD.ReimbAllowExpBillingCurrency) ReimbAllowExpBillingCurrency, Sum(CD.ReimbAllowConsBillingCurrency) ReimbAllowConsBillingCurrency, 
						Sum(CD.FeeDirLabFunctionalCurrency) FeeDirLabFunctionalCurrency, Sum(CD.FeeDirExpFunctionalCurrency) FeeDirExpFunctionalCurrency, 
						Sum(CD.ReimbAllowExpFunctionalCurrency) ReimbAllowExpFunctionalCurrency, Sum(CD.ReimbAllowConsFunctionalCurrency) ReimbAllowConsFunctionalCurrency
					from Contracts C, ContractDetails CD 
					where C.ContractNumber = CD.ContractNumber  and C.WBS1 = CD.WBS1 and C.FeeIncludeInd = N'Y' and C.WBS1 = @NewWBS1 and not (CD.WBS2 = N' ' ) and  not ( CD.WBS3 = N' ')
					group by CD.WBS1,WBS2,WBS3)  C
					where C.WBS1 = PR.WBS1 and C.WBS2 = PR.WBS2 and C.WBS3 = PR.WBS3

					if (@recalcComp = 'Y')
						Begin
							--SQLSyncFeesToContracts_ForProjectMC
							update PR
							set PR.Fee = C.Fee, PR.ConsultFee = C.ConsultFee, PR.ReimbAllow = C.ReimbAllow,
								PR.FeeBillingCurrency = C.FeeBillingCurrency, PR.ConsultFeeBillingCurrency = C.ConsultFeeBillingCurrency,
								PR.ReimbAllowBillingCurrency = C.ReimbAllowBillingCurrency,
								PR.FeeFunctionalCurrency = C.FeeFunctionalCurrency, 
								PR.ConsultFeeFunctionalCurrency = C.ConsultFeeFunctionalCurrency,
								PR.ReimbAllowFunctionalCurrency = C.ReimbAllowFunctionalCurrency,
								PR.FeeDirLab = C.FeeDirLab,
								PR.FeeDirExp = C.FeeDirExp,
								PR.ReimbAllowExp = C.ReimbAllowExp,
								PR.ReimbAllowCons = C.ReimbAllowCons,
								PR.FeeDirLabBillingCurrency = C.FeeDirLabBillingCurrency,
								PR.FeeDirExpBillingCurrency = C.FeeDirExpBillingCurrency,
								PR.ReimbAllowExpBillingCurrency = C.ReimbAllowExpBillingCurrency,
								PR.ReimbAllowConsBillingCurrency = C.ReimbAllowConsBillingCurrency,
								PR.FeeDirLabFunctionalCurrency = C.FeeDirLabFunctionalCurrency,
								PR.FeeDirExpFunctionalCurrency = C.FeeDirExpFunctionalCurrency,
								PR.ReimbAllowExpFunctionalCurrency = C.ReimbAllowExpFunctionalCurrency,
								PR.ReimbAllowConsFunctionalCurrency = C.ReimbAllowConsFunctionalCurrency
							 from (select CD.WBS1,Sum(CD.Fee) Fee, Sum(CD.ConsultFee) ConsultFee, Sum(CD.ReimbAllow) ReimbAllow,
								Sum(CD.FeeBillingCurrency) FeeBillingCurrency, Sum(CD.ConsultFeeBillingCurrency) ConsultFeeBillingCurrency, Sum(CD.ReimbAllowBillingCurrency) ReimbAllowBillingCurrency,
								Sum(CD.FeeFunctionalCurrency) FeeFunctionalCurrency, Sum(CD.ConsultFeeFunctionalCurrency) ConsultFeeFunctionalCurrency, Sum(CD.ReimbAllowFunctionalCurrency) ReimbAllowFunctionalCurrency,
								Sum(CD.FeeDirLab) FeeDirLab, Sum(CD.FeeDirExp) FeeDirExp, Sum(CD.ReimbAllowExp) ReimbAllowExp, Sum(CD.ReimbAllowCons) ReimbAllowCons, 
								Sum(CD.FeeDirLabBillingCurrency) FeeDirLabBillingCurrency, Sum(CD.FeeDirExpBillingCurrency) FeeDirExpBillingCurrency,
								Sum(CD.ReimbAllowExpBillingCurrency) ReimbAllowExpBillingCurrency, Sum(CD.ReimbAllowConsBillingCurrency) ReimbAllowConsBillingCurrency, 
								Sum(CD.FeeDirLabFunctionalCurrency) FeeDirLabFunctionalCurrency, Sum(CD.FeeDirExpFunctionalCurrency) FeeDirExpFunctionalCurrency, 
								Sum(CD.ReimbAllowExpFunctionalCurrency) ReimbAllowExpFunctionalCurrency, Sum(CD.ReimbAllowConsFunctionalCurrency) ReimbAllowConsFunctionalCurrency
							from Contracts C, ContractDetails CD 
							where C.ContractNumber = CD.ContractNumber and C.WBS1 = CD.WBS1  and C.FeeIncludeInd = N'Y'
							 and C.WBS1 = @NewWBS1 and not ( WBS2 = N' ' and WBS3 = N' ' )
							group by CD.WBS1)  C
							where C.WBS1 = PR.WBS1  and  PR.WBS2 = N' ' and  PR.WBS3 = N' '

							-- Execute UpdatePRFees							
							EXECUTE dbo.UpdatePRFees @NewWBS1, 'N'
						End
					else
						Begin
							--SQLSyncFeesNoAutoSumup_ForProjectMC
							update PR
							set PR.Fee = C.Fee, PR.ConsultFee = C.ConsultFee, PR.ReimbAllow = C.ReimbAllow,
								PR.FeeBillingCurrency = C.FeeBillingCurrency, PR.ConsultFeeBillingCurrency = C.ConsultFeeBillingCurrency,
								PR.ReimbAllowBillingCurrency = C.ReimbAllowBillingCurrency,
								PR.FeeFunctionalCurrency = C.FeeFunctionalCurrency, 
								PR.ConsultFeeFunctionalCurrency = C.ConsultFeeFunctionalCurrency,
								PR.ReimbAllowFunctionalCurrency = C.ReimbAllowFunctionalCurrency,
								PR.FeeDirLab = C.FeeDirLab,
								PR.FeeDirExp = C.FeeDirExp,
								PR.ReimbAllowExp = C.ReimbAllowExp,
								PR.ReimbAllowCons = C.ReimbAllowCons,
								PR.FeeDirLabBillingCurrency = C.FeeDirLabBillingCurrency,
								PR.FeeDirExpBillingCurrency = C.FeeDirExpBillingCurrency,
								PR.ReimbAllowExpBillingCurrency = C.ReimbAllowExpBillingCurrency,
								PR.ReimbAllowConsBillingCurrency = C.ReimbAllowConsBillingCurrency,
								PR.FeeDirLabFunctionalCurrency = C.FeeDirLabFunctionalCurrency,
								PR.FeeDirExpFunctionalCurrency = C.FeeDirExpFunctionalCurrency,
								PR.ReimbAllowExpFunctionalCurrency = C.ReimbAllowExpFunctionalCurrency,
								PR.ReimbAllowConsFunctionalCurrency = C.ReimbAllowConsFunctionalCurrency
							 from (select CD.WBS1,Sum(CD.Fee) Fee, Sum(CD.ConsultFee) ConsultFee, Sum(CD.ReimbAllow) ReimbAllow,
								Sum(CD.FeeBillingCurrency) FeeBillingCurrency, Sum(CD.ConsultFeeBillingCurrency) ConsultFeeBillingCurrency, Sum(CD.ReimbAllowBillingCurrency) ReimbAllowBillingCurrency,
								Sum(CD.FeeFunctionalCurrency) FeeFunctionalCurrency, Sum(CD.ConsultFeeFunctionalCurrency) ConsultFeeFunctionalCurrency, Sum(CD.ReimbAllowFunctionalCurrency) ReimbAllowFunctionalCurrency,
								Sum(CD.FeeDirLab) FeeDirLab, Sum(CD.FeeDirExp) FeeDirExp, Sum(CD.ReimbAllowExp) ReimbAllowExp, Sum(CD.ReimbAllowCons) ReimbAllowCons, 
								Sum(CD.FeeDirLabBillingCurrency) FeeDirLabBillingCurrency, Sum(CD.FeeDirExpBillingCurrency) FeeDirExpBillingCurrency,
								Sum(CD.ReimbAllowExpBillingCurrency) ReimbAllowExpBillingCurrency, Sum(CD.ReimbAllowConsBillingCurrency) ReimbAllowConsBillingCurrency, 
								Sum(CD.FeeDirLabFunctionalCurrency) FeeDirLabFunctionalCurrency, Sum(CD.FeeDirExpFunctionalCurrency) FeeDirExpFunctionalCurrency, 
								Sum(CD.ReimbAllowExpFunctionalCurrency) ReimbAllowExpFunctionalCurrency, Sum(CD.ReimbAllowConsFunctionalCurrency) ReimbAllowConsFunctionalCurrency
							from Contracts C, ContractDetails CD 
							where C.ContractNumber = CD.ContractNumber and C.WBS1 = CD.WBS1 and C.FeeIncludeInd = N'Y'
							 and CD.WBS1 = @NewWBS1 and  ( CD.WBS2 = N' ' and CD.WBS3 = N' ' )
							group by CD.WBS1)  C
							where C.WBS1 = PR.WBS1  and  PR.WBS2 = N' ' and  PR.WBS3 = N' '
						End
				End

			close contractTable
			deallocate contractTable

			Update PR SEt SiblingWBS1 = null Where SiblingWBS1 = @OldWBS1
		END --Existing
	else 
		Begin
-- Add by DP 4.0
			select * into #TempKeyConvert from PR where WBS1 = @OldWBS1
			Update #TempKeyConvert set WBS1=@NewWBS1
			Insert PR select * from #TempKeyConvert
			Drop Table #TempKeyConvert
			set @NewName = @OldName

			select * into #TempKeyConvertBT from BT where WBS1 = @OldWBS1
			Update #TempKeyConvertBT set WBS1=@NewWBS1
			Insert BT select * from #TempKeyConvertBT
			Drop Table #TempKeyConvertBT
--
		end
	exec @RetVal = KeyCvt @Entity = @Entity,
					@CvtType = @CvtType,
					@Length = 30,
					@ErrMsg = @ErrMsg2 output,
					@Diag = @Diag,
					@NewValue = @NewWBS1,
					@OldValue = @OldWBS1,
					@ConstrFlag = 3 -- UpdateTables
--
	if (@RetVal = 0)
		begin
-- Add by DP 4.0
				Delete FROM BT WHERE BT.WBS1 = @OldWBS1 and
					exists (select 'x' from BT new where new.wbs1 = @NewWBS1 and new.wbs2 = BT.wbs2 and new.wbs3 = BT.wbs3)
				Delete FROM PR WHERE PR.WBS1 = @OldWBS1 and
					exists (select 'x' from pr new where new.wbs1 = @NewWBS1 and new.wbs2 = pr.wbs2 and new.wbs3 = pr.wbs3)

				--If TrafficLIVE Web Service is enabled, update TLSyncModDate to track change to PR field.
				If (Select EnableWebService FROM CFGIntegrationWS WHERE Type = 'TL') = 'Y'
				Begin
					UPDATE PR SET TLSyncModDate = GETUTCDATE() WHERE WBS1 = @NewWBS1 AND WBS2 = ' ' AND WBS3 = ' '
				End
				-- Update ModUser/ModDate
				UPDATE PR SET ModUser = @UserName, ModDate = GETUTCDATE() WHERE WBS1 = @NewWBS1 AND WBS2 = ' ' AND WBS3 = ' '

				if (@Period > 0)
				begin
					insert into KeyConvertLog (PKey, Entity, OldKey, NewKey, OldName, NewName, Period, PostSeq, OldKey2, OldKey3, NewKey2, NewKey3, OldName2, OldName3, NewName2, NewName3, WBSName)
					select PKey, Entity, OldKey, NewKey, @OldName, @NewName, @Period, @LastPostSeq, N'', N'', N'', N'', N'', N'', N'', N'', WBSName from keyConvertWork where Pkey = @PKey
				end
--

				if (@NPlanSourceExist = 'Y' and @NPlanTargetExist = 'Y')
				Begin

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--- Storm 1.1 Update 1st level under WBS3 with ParentOutlineNumber				

				declare RPTaskCursor cursor for 
						select PlanID, TaskID, WBS1, WBS2, WBS3, LaborCode, ChildrenCount, OutlineLevel, OutlineNumber, ParentOutlineNumber, TableName from #TempRPPN 
				open RpTaskCursor
				fetch next from RPTaskCursor into 
					@RPOldPlanID,
					@RPOldTaskID,
					@RPOldWBS1,
					@RPOldWBS2,
					@RPOldWBS3,
					@RPOldLaborCode,
					@RPOldChildrenCount,
					@RPOldOutLineLevel,
					@RPOldOutLineNumber,
					@RPOldParentOutlineNumber,
					@RPTableName
				set @RPTaskfetch = @@FETCH_STATUS

				while  (@RPTaskFetch = 0)
				begin
					if (@RPTableName = 'PNTask') 
						begin 							
							Update PNTask Set PlanID=@NewPlanID, WBS1=@NewWBS1 From PNTask
							Where PNTask.PlanID=@RPOldPlanID and PNTask.WBS1 = @RPOldWBS1 and isnull(PNTask.WBS2,'') = @RPOldWBS2 And isnull(PNTask.WBS3,'')=@RPOldWBS3 
							And isnull(PNTask.LaborCode,'')=@RPOldLaborCode and PNTask.OutlineLevel >= @RPOldOutlineLevel 

							Update PNAssignment Set PlanID=@NewPlanID, WBS1=@newWBS1, WBS2=@Newwbs2 from PNAssignment
							where PNAssignment.PlanID=@RPOldPlanID and PNAssignment.TaskID=@RPOldTaskID and PNAssignment.WBS1=@RPOldWBS1 and PNAssignment.WBS2=@RPOldWBS2 and PNAssignment.WBS3=@RPOldWBS3

							Update PNExpense Set PlanID=@NewPlanID, WBS1=@NewWBS1, WBS2=@NewWBS2 from PNExpense
							where PNExpense.PlanID=@RPOldPlanID and PNExpense.TaskID=@RPOldTaskID and PNExpense.WBS1=@RPOldWBS1 and PNExpense.WBS2=@RPOldWBS2 and PNExpense.WBS3=@RPOldWBS3

							Update PNConsultant Set PlanID=@NewPlanID, WBS1=@NewWBS1, WBS2=@NewWBS2 from PNConsultant
							where PNConsultant.PlanID=@RPOldPlanID and PNConsultant.TaskID=@RPOldTaskID and PNConsultant.WBS1=@RPOldWBS1 and PNConsultant.WBS2=@RPOldWBS2 and PNConsultant.WBS3=@RPOldWBS3
						end

					if (@RPTableName = 'RPTask') 
						begin
							Update RPTask Set PlanID=@NewPlanID, WBS1=@NewWBS1, WBS2=@NewWBS2 from RPTask
							Where RPTask.PlanID=@RPOldPlanID and RPTask.WBS1 = @RPOldWBS1 and RPTask.WBS2 = @RPOldWBS2 And RPTask.WBS3=@RPOldWBS3 and ISNULL(RPTask.WBSType,'')='' and RPTask.OutlineLevel >= @RPOldOutlineLevel 

							Update RPAssignment Set PlanID=@NewPlanID, WBS1=@newWBS1, WBS2=@Newwbs2 from RPAssignment
							where RPAssignment.PlanID=@RPOldPlanID and RPAssignment.TaskID=@RPOldTaskID and RPAssignment.WBS1=@RPOldWBS1 and RPAssignment.WBS2=@RPOldWBS2 and RPAssignment.WBS3=@RPOldWBS3

							Update RPExpense Set PlanID=@NewPlanID, WBS1=@newWBS1, WBS2=@Newwbs2 from RPExpense
							where RPExpense.PlanID=@RPOldPlanID and RPExpense.TaskID=@RPOldTaskID and RPExpense.WBS1=@RPOldWBS1 and RPExpense.WBS2=@RPOldWBS2 and RPExpense.WBS3=@RPOldWBS3

							Update RPConsultant Set PlanID=@NewPlanID, WBS1=@NewWBS1, WBS2=@NewWBS2 from RPConsultant
							where RPConsultant.PlanID=@RPOldPlanID and RPConsultant.TaskID=@RPOldTaskID and RPConsultant.WBS1=@RPOldWBS1 and RPConsultant.WBS2=@RPOldWBS2 and RPConsultant.WBS3=@RPOldWBS3

							Update RPUnit Set PlanID=@NewPlanID, WBS1=@NewWBS1, WBS2=@NewWBS2 from RPUnit
							where RPUnit.PlanID=@RPOldPlanID and RPUnit.TaskID=@RPOldTaskID and RPUnit.WBS1=@RPOldWBS1 and RPUnit.WBS2=@RPOldWBS2 and RPUnit.WBS3=@RPOldWBS3
						End

					fetch next from RPTaskCursor into 
					@RPOldPlanID,
					@RPOldTaskID,
					@RPOldWBS1,
					@RPOldWBS2,
					@RPOldWBS3,
					@RPOldLaborCode,
					@RPOldChildrenCount,
					@RPOldOutLineLevel,
					@RPOldOutLineNumber,
					@RPOldParentOutlineNumber,
					@RPTableName
					set @RPTaskfetch = @@FETCH_STATUS
				End
				close RPTaskCursor
				deallocate RPTaskCursor

				-- Update/Delete Duplicate outlinelevel >= 3 Records
				declare RPPNDupTaskCursor cursor for 
					select PlanID, Min(TaskID) As OrigTaskID, Max(TaskID) As MergeTaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, OutlineLevel, OutlineNumber, ParentOutlineNumber, 'RPTask' tableName from RPTask
					Where OutlineLevel >= 3 And WBS1 = @NewWBS1 and WBS2 = @NewWBS2 
					group by PlanID, WBS1, WBS2, WBS3, LaborCode, OutlineLevel, ParentOutlineNumber, OutlineNumber
					having count(*) > 1
					union 
					select PlanID, Min(TaskID) As OrigTaskID, Max(TaskID) As MergeTaskID, WBS1, isnull(WBS2,'') as WBS2, isnull(WBS3,'') as WBS3, isnull(LaborCode,'') as LaborCode, OutlineLevel, OutlineNumber, ParentOutlineNumber, 'PNTask' tableName from PNTask
					Where OutlineLevel >= 3 And WBS1 = @NewWBS1 and WBS2 = @NewWBS2 
					group by PlanID, WBS1, WBS2, WBS3, LaborCode, OutlineLevel, ParentOutlineNumber, OutlineNumber
					having count(*) > 1

				open RPPNDupTaskCursor
				fetch next from RPPNDupTaskCursor into 
					@RPOldPlanID,
					@RPOldTaskID,
					@RPNewTaskID, 
					@RPOldWBS1,
					@RPOldWBS2,
					@RPOldWBS3,
					@RPOldLaborCode,
					@RPOldOutLineLevel,
					@RPOldOutLineNumber,
					@RPOldParentOutlineNumber, 
					@RPTableName
				set @RPTaskfetch = @@FETCH_STATUS
				while  (@RPTaskFetch = 0)
				begin
					if (@RPTableName = 'PNTask') 
						begin 							
							Update PNTask
							set PNTask.CompensationFee = PNTask.CompensationFee + old.CompensationFee,
								PNTask.ConsultantFee = PNTask.ConsultantFee + old.ConsultantFee,
								PNTask.ReimbAllowance = PNTask.ReimbAllowance + old.ReimbAllowance, 
								PNTask.CompensationFeeBill = PNTask.CompensationFeeBill + old.CompensationFeeBill, 
								PNTask.ConsultantFeeBill = PNTask.ConsultantFeeBill + old.ConsultantFeeBill,
								PNTask.ReimbAllowanceBill = PNTask.ReimbAllowanceBill + old.ReimbAllowanceBill,
								PNTask.CompensationFeeDirLab = PNTask.CompensationFeeDirLab + old.CompensationFeeDirLab, 
								PNTask.CompensationFeeDirExp = PNTask.CompensationFeeDirExp + old.CompensationFeeDirExp, 
								PNTask.ReimbAllowanceExp = PNTask.ReimbAllowanceExp + old.ReimbAllowanceExp,
								PNTask.ReimbAllowanceCon = PNTask.ReimbAllowanceCon + old.ReimbAllowanceCon,
								PNTask.CompensationFeeDirLabBill = PNTask.CompensationFeeDirLabBill + old.CompensationFeeDirLabBill, 
								PNTask.CompensationFeeDirExpBill = PNTask.CompensationFeeDirExpBill + old.CompensationFeeDirExpBill,
								PNTask.ReimbAllowanceExpBill = PNTask.ReimbAllowanceExpBill + old.ReimbAllowanceExpBill,
								PNTask.ReimbAllowanceConBill = PNTask.ReimbAllowanceConBill + old.ReimbAllowanceConBill
							from PNTask old inner join PNTask on 1=1
							where
								PNTask.WBS1 = @RPOldWBS1 and old.wbs1 = @RPOldWBS1 
								and isnull(PNTask.WBS2,'') = @RPOldWBS2 and isnull(old.WBS2,'') = @RPOldWBS2 
								and isnull(PNTask.WBS3,'') = @RPOldWBS3 and isnull(old.WBS3,'') = @RPOldwbs3
								and isnull(PNTask.LaborCode,'') = @RPOldLaborCode and isnull(old.LaborCode,'') = @RPOldLaborCode 
								and PNTask.ParentOutlineNumber = @RPOldParentOutLineNumber and old.ParentOutlineNumber = @RPOldParentOutLineNumber
								and PNTask.OutlineLevel = @RPOldOutlineLevel and old.OutlineLevel = @RPOldOutlineLevel
								and PNTask.OutlineNumber = @RPOldOutlineNumber and old.OutlineNumber = @RPOldOutlineNumber
								and PNTask.PlanID = @RPOldPlanID and old.PlanID = @RPOldPlanID
								and PNTask.TaskID = @RPNewTaskID and old.TaskID = @RPOldTaskID

							Update PNAssignment Set TaskID = @RPNewTaskID 
							where PNAssignment.PlanID=@RPOldPlanID and PNAssignment.TaskID=@RPOldTaskID and PNAssignment.WBS1=@RPOldWBS1 and PNAssignment.WBS2=@RPOldWBS2 and PNAssignment.WBS3=@RPOldWBS3

							Update PNExpense Set TaskID = @RPNewTaskID 
							where PNExpense.PlanID=@RPOldPlanID and PNExpense.TaskID=@RPOldTaskID and PNExpense.WBS1=@RPOldWBS1 and PNExpense.WBS2=@RPOldWBS2 and PNExpense.WBS3=@RPOldWBS3

							Update PNConsultant Set TaskID = @RPNewTaskID 
							where PNConsultant.PlanID=@RPOldPlanID and PNConsultant.TaskID=@RPOldTaskID and PNConsultant.WBS1=@RPOldWBS1 and PNConsultant.WBS2=@RPOldWBS2 and PNConsultant.WBS3=@RPOldWBS3

							Delete From PNTask 
							Where PNTask.PlanID=@RPOldPlanID and PNTask.TaskID = @RPOldTaskID and PNTask.WBS1 = @RPOldWBS1 and PNTask.WBS2 = @RPOldWBS2 And PNTask.WBS3=@RPOldWBS3 
								And PNTask.OutlineLevel = @RPOldOutlineLevel And PNTask.OutlineNumber = @RPOldOutlineNumber and PNTask.ParentOutlineNumber = @RPOldParentOutLineNumber
						end

					if (@RPTableName = 'RPTask') 
						begin
							Update RPTask
							set RPTask.CompensationFee = RPTask.CompensationFee + old.CompensationFee,
								RPTask.ConsultantFee = RPTask.ConsultantFee + old.ConsultantFee,
								RPTask.ReimbAllowance = RPTask.ReimbAllowance + old.ReimbAllowance, 
								RPTask.CompensationFeeBill = RPTask.CompensationFeeBill + old.CompensationFeeBill, 
								RPTask.ConsultantFeeBill = RPTask.ConsultantFeeBill + old.ConsultantFeeBill,
								RPTask.ReimbAllowanceBill = RPTask.ReimbAllowanceBill + old.ReimbAllowanceBill,
								RPTask.CompensationFeeDirLab = RPTask.CompensationFeeDirLab + old.CompensationFeeDirLab, 
								RPTask.CompensationFeeDirExp = RPTask.CompensationFeeDirExp + old.CompensationFeeDirExp, 
								RPTask.ReimbAllowanceExp = RPTask.ReimbAllowanceExp + old.ReimbAllowanceExp,
								RPTask.ReimbAllowanceCon = RPTask.ReimbAllowanceCon + old.ReimbAllowanceCon,
								RPTask.CompensationFeeDirLabBill = RPTask.CompensationFeeDirLabBill + old.CompensationFeeDirLabBill, 
								RPTask.CompensationFeeDirExpBill = RPTask.CompensationFeeDirExpBill + old.CompensationFeeDirExpBill,
								RPTask.ReimbAllowanceExpBill = RPTask.ReimbAllowanceExpBill + old.ReimbAllowanceExpBill,
								RPTask.ReimbAllowanceConBill = RPTask.ReimbAllowanceConBill + old.ReimbAllowanceConBill
							from RPTask old inner join RPTask on 1=1
							where
								RPTask.WBS1 = @OldWBS1 and old.wbs1 = @OldWBS1 
								and isnull(RPTask.WBS2,'') = @OldWBS2 and isnull(old.WBS2,'') = @OldWBS2 
								and isnull(RPTask.WBS3,'') = @OldWBS3 and isnull(old.WBS3,'') = @Oldwbs3
								and isnull(RPTask.LaborCode,'') = @RPOldLaborCode and isnull(old.LaborCode,'') = @RPOldLaborCode 
								and RPTask.ParentOutlineNumber = @RPOldParentOutLineNumber and old.ParentOutlineNumber = @RPOldParentOutLineNumber
								and RPTask.OutlineLevel = @RPOldOutlineLevel and old.OutlineLevel = @RPOldOutlineLevel
								and RPTask.OutlineNumber = @RPOldOutlineNumber and old.OutlineNumber = @RPOldOutlineNumber
								and RPTask.PlanID = @RPOldPlanID and old.PlanID = @RPOldPlanID
								and RPTask.TaskID = @RPNewTaskID and old.TaskID = @RPOldTaskID								

							Update RPAssignment Set TaskID = @RPNewTaskID
							where RPAssignment.PlanID=@RPOldPlanID and RPAssignment.TaskID=@RPOldTaskID and RPAssignment.WBS1=@RPOldWBS1 and RPAssignment.WBS2=@RPOldWBS2 and RPAssignment.WBS3=@RPOldWBS3

							Update RPExpense Set TaskID = @RPNewTaskID
							where RPExpense.PlanID=@RPOldPlanID and RPExpense.TaskID=@RPOldTaskID and RPExpense.WBS1=@RPOldWBS1 and RPExpense.WBS2=@RPOldWBS2 and RPExpense.WBS3=@RPOldWBS3

							Update RPConsultant Set TaskID = @RPNewTaskID
							where RPConsultant.PlanID=@RPOldPlanID and RPConsultant.TaskID=@RPOldTaskID and RPConsultant.WBS1=@RPOldWBS1 and RPConsultant.WBS2=@RPOldWBS2 and RPConsultant.WBS3=@RPOldWBS3

							Update RPUnit Set TaskID = @RPNewTaskID
							where RPUnit.PlanID=@RPOldPlanID and RPUnit.TaskID=@RPOldTaskID and RPUnit.WBS1=@RPOldWBS1 and RPUnit.WBS2=@RPOldWBS2 and RPUnit.WBS3=@RPOldWBS3

							delete From RPTask 
							Where RPTask.PlanID=@RPOldPlanID and RPTask.TaskID = @RPOldTaskID and RPTask.WBS1 = @RPOldWBS1 and RPTask.WBS2 = @RPOldWBS2 And RPTask.WBS3=@RPOldWBS3 
								And RPTask.OutlineLevel = @RPOldOutlineLevel And RPTask.OutlineNumber = @RPOldOutlineNumber and RPTask.ParentOutlineNumber = @RPOldParentOutLineNumber
						End
					
					fetch next from RPPNDupTaskCursor into 
					@RPOldPlanID,
					@RPOldTaskID,
					@RPNewTaskID, 
					@RPOldWBS1,
					@RPOldWBS2,
					@RPOldWBS3,
					@RPOldLaborCode,
					@RPOldOutLineLevel,
					@RPOldOutLineNumber,
					@RPOldParentOutlineNumber, 
					@RPTableName
					set @RPTaskfetch = @@FETCH_STATUS
				End
				close RPPNDupTaskCursor
				deallocate RPPNDupTaskCursor

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

			end

				if (@NPlanSourceExist='Y')
				begin
					print 'Message: Delete PNPlan Where PlanID=OldPlanID: ' + @OldPlanID
					Delete PNPlan Where PlanID=@OldPlanID
				end

--To Publish a Nav plan
				if (@NewPlanID > '' and @NPlanTargetExist='Y')
				begin
					Execute dbo.PMDelPlan @OldPlanID
					Execute dbo.PMDelPlan @NewPlanID
					Execute dbo.stRPPublish @NewPlanID
				end

				PRINT '>>> Start Processing PlanID = "' + @NewPlanID + '", WBS1 = "' + @NewWBS1 + '"'
				Update RPTask Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update RPAssignment Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update RPExpense Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update RPConsultant Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update RPUnit Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update PNTask Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update PNAssignment Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update PNExpense Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				Update PNConsultant Set WBS2=Case When WBS2 in ('','<none>') then null else WBS2 End, WBS3=Case When WBS3 in ('','<none>') then null else WBS3 End Where PlanID=@NewPlanID 
				EXECUTE dbo.stRPSyncWBSStructure @NewPlanID
				PRINT '<<< End Processing PlanID = "' + @NewPlanID + '", WBS1 = "' + @NewWBS1 + '"'

--Undo Check out
				if (@VPlanSourceExist = 'Y')
					Update RPPlan Set CheckedOutDate=NULL, CheckedOutUser=NULL, CheckedOutID=NULL
					From RPPlan 
					where CheckedOutID=@CheckedOutID
				
				if (@NPlanSourceExist = 'Y' or @NPlanTargetExist = 'Y')
					Update PNPlan Set CheckedOutDate=NULL, CheckedOutUser=NULL, CheckedOutID=NULL
					From PNPlan 
					where CheckedOutID=@CheckedOutID

				delete from keyConvertWork where current of keysCursor
				commit Transaction
			end
	else
		begin
			set @ErrMsg = @ErrMsg + @ErrMsg2
			rollback transaction
		end

	Drop Table #tempRPPN

	fetch next from KeysCursor into 
		@OldWBS1,
		@OldWBS2,
		@OldWBS3,
		@NewWBS1,
		@NewWBS2,
		@NewWBS3,
		@PKey
	set @KeysFetch = @@Fetch_Status
	end --while

	close keysCursor
	deallocate keysCursor
	set nocount off
	SET ANSI_WARNINGS ON
	return(0)
End -- Procedure
GO
