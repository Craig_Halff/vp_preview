SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_VendorGet] ( @vendorId nvarchar(20), @TaxAuditingEnabled bit)
             AS EXEC spCCG_PAT_Config_VendorGet @vendorId,@TaxAuditingEnabled
GO
