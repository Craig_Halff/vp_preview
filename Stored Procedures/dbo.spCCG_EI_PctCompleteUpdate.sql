SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_PctCompleteUpdate]
	@wbs1						Nvarchar(32),
	@wbs2						Nvarchar(7),
	@wbs3						Nvarchar(7),
	@seq						int,
	@newFeePctCpl				decimal(19,4),
	@sNewFeeToDate				varchar(30),
	@sNewFeePctCpl_TopLevel		varchar(30),
	@sNewFeeToDate_TopLevel		varchar(30),
	@KEY_EMPLOYEEID				Nvarchar(30),
	@DataType					varchar(20),
	@ColumnLabel				varchar(500),
	@OldValue					decimal(19,4),
	@NewValue					decimal(19,4),
	@saveFeePctCplImmediately	bit,
	@feeMeth					varchar(20),
	@feeBasis					varchar(10),
	@category					int
AS
BEGIN
	/*
	-- Was called [DeltekStoredProc_CCG_EI_Pct_Complete_Update]
	spCCG_EI_PctCompleteUpdate @wbs1 = '2003005.00', @wbs2 = ' ', @wbs3 = ' ', @seq = '0',
		@newFeePctCpl = '20.0000', @newFeeToDate = '0.8400', @KEY_EMPLOYEEID = '00001',
		@DataType = 'P', @ColumnLabel = '% Comp', @OldValue = '0.0000', @NewValue = '20.0000',
		@saveFeePctCplImmediately = '1', @feeMeth = '1', @feeBasis = 'P'
	*/
	SET NOCOUNT ON

	DECLARE @sOldValue varchar(100), @sNewValue varchar(100)
	--print 'here1'
	DECLARE @newFeeToDate decimal(19,4) = cast(nullif(@sNewFeeToDate, '') as decimal(19,4))
	--print 'here2'
	print @newFeeToDate

	IF ISNULL(@wbs2,'') = '' set @wbs2 = ' '
	IF ISNULL(@wbs3,'') = '' set @wbs3 = ' '

	Set @sOldValue = Cast(@OldValue as varchar(100))
	Set @sNewValue = Cast(@NewValue as varchar(100))

	BEGIN TRANSACTION

	-- Save a history of this update
    INSERT INTO CCG_EI_HistoryUpdateColumns
		(ActionTaken, ActionDate, ActionTakenBy, WBS1, WBS2, WBS3, BTFSeq,
			DataType, ConfigCustomColumn, ColumnLabel, OldValue, NewValue, OldValueDecimal, NewValueDecimal)
		VALUES ('Change', getutcdate(), @KEY_EMPLOYEEID, @wbs1, @wbs2, @wbs3, @seq,
			@DataType, NULL, @ColumnLabel, @sOldValue, @sNewValue, @OldValue, @NewValue)

	-- Write the new values into the EI table:
	IF EXISTS (SELECT 'x' FROM CCG_EI_FeePctCpl WHERE WBS1 = @wbs1 and WBS2 = @wbs2 and WBS3 = @wbs3 and Seq = @seq)
	BEGIN
		IF isnull(@category, 0) = 0
			UPDATE CCG_EI_FeePctCpl
				SET FeePctCpl  = @newFeePctCpl, FeePctCpl1 = @newFeePctCpl, FeePctCpl2 = @newFeePctCpl, FeePctCpl3 = @newFeePctCpl, FeePctCpl4 = @newFeePctCpl,
					FeePctCpl5 = @newFeePctCpl, FeeToDate  = @newFeeToDate,
					FeeToDate1 = null, FeeToDate2 = null, FeeToDate3 = null, FeeToDate4 = null, FeeToDate5 = null
				WHERE WBS1 = @wbs1 and WBS2 = @wbs2 and WBS3 = @wbs3 and Seq = @seq
		ELSE BEGIN -- update single billing category
			UPDATE CCG_EI_FeePctCpl
				SET FeePctCpl = cast(@sNewFeePctCpl_TopLevel as decimal(19,4)), FeeToDate = cast(@sNewFeeToDate_TopLevel as decimal(19,4)),
					FeePctCpl1 = (case @category when 1 then @newFeePctCpl else FeePctCpl1 end),
					FeePctCpl2 = (case @category when 2 then @newFeePctCpl else FeePctCpl2 end),
					FeePctCpl3 = (case @category when 3 then @newFeePctCpl else FeePctCpl3 end),
					FeePctCpl4 = (case @category when 4 then @newFeePctCpl else FeePctCpl4 end),
					FeePctCpl5 = (case @category when 5 then @newFeePctCpl else FeePctCpl5 end),
					FeeToDate1 = (case @category when 1 then @newFeeToDate else FeeToDate1 end),
					FeeToDate2 = (case @category when 2 then @newFeeToDate else FeeToDate2 end),
					FeeToDate3 = (case @category when 3 then @newFeeToDate else FeeToDate3 end),
					FeeToDate4 = (case @category when 4 then @newFeeToDate else FeeToDate4 end),
					FeeToDate5 = (case @category when 5 then @newFeeToDate else FeeToDate5 end)
				WHERE WBS1 = @wbs1 and WBS2 = @wbs2 and WBS3 = @wbs3 and Seq = @seq
		END
	END
    ELSE BEGIN
		IF isnull(@category, 0) = 0
			INSERT INTO CCG_EI_FeePctCpl (WBS1, WBS2, WBS3, Seq, FeePctCpl, FeeToDate, FeePctCpl1, FeeToDate1, FeePctCpl2, FeeToDate2,
					FeePctCpl3, FeeToDate3, FeePctCpl4, FeeToDate4, FeePctCpl5, FeeToDate5)
				VALUES (@wbs1, @wbs2, @wbs3, @seq, @newFeePctCpl, @newFeeToDate, @newFeePctCpl, null, @newFeePctCpl, null,
					@newFeePctCpl, null, @newFeePctCpl, null, @newFeePctCpl, null)
		ELSE
			INSERT INTO CCG_EI_FeePctCpl (WBS1, WBS2, WBS3, Seq, FeePctCpl, FeeToDate,
					FeePctCpl1, FeeToDate1, FeePctCpl2, FeeToDate2, FeePctCpl3, FeeToDate3, FeePctCpl4, FeeToDate4, FeePctCpl5, FeeToDate5)
				VALUES (@wbs1, @wbs2, @wbs3, @seq, cast(@sNewFeePctCpl_TopLevel as decimal(19,4)), cast(@sNewFeeToDate_TopLevel as decimal(19,4)),
					(case @category when 1 then @newFeePctCpl else null end), (case @category when 1 then @newFeeToDate else null end),
					(case @category when 2 then @newFeePctCpl else null end), (case @category when 2 then @newFeeToDate else null end),
					(case @category when 3 then @newFeePctCpl else null end), (case @category when 3 then @newFeeToDate else null end),
					(case @category when 4 then @newFeePctCpl else null end), (case @category when 4 then @newFeeToDate else null end),
					(case @category when 5 then @newFeePctCpl else null end), (case @category when 5 then @newFeeToDate else null end))
	END

	IF @saveFeePctCplImmediately = 1 BEGIN
		-- Save to Vision
		exec spCCG_EI_PctCompleteSaveToVision @wbs1 = @wbs1, @wbs2 = @wbs2, @wbs3 = @wbs3, @seq = @seq, @wbsList = '', @removeFromEI = 1

		-- Now remove from EI table
		DELETE FROM CCG_EI_FeePctCpl
			WHERE wbs1 = @wbs1 and wbs2 = @wbs2 and wbs3 = @wbs3
				and Seq = (Case @seq When '0' THEN Seq Else @seq End)
	END

	COMMIT TRANSACTION
END
GO
