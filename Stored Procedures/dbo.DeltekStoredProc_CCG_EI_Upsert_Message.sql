SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Upsert_Message]
	@isUpdate			bit,
	@wbs1				Nvarchar(32),
	@dateEntered		varchar(100),
	@dateExisting		varchar(100),
	@enteredById		Nvarchar(32),
	@role				Nvarchar(32),
	@message			text
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @rowsAffected int;
	Set @role = ISNULL(@role, '');

	If @isUpdate = 0 BEGIN
		-- This is a new message
		Insert Into CCG_EI_Messages (WBS1, DateEntered, EnteredBy, [Role], [Message])
			Values (@wbs1, @dateEntered, @enteredById, @role, @message);

		SET @rowsAffected = @@ROWCOUNT;
	END;
	ELSE BEGIN
		-- This is an update
		If DataLength(@message) > 0
			Update CCG_EI_Messages Set DateEntered = @dateEntered, [Message] = @message
				Where WBS1 = @wbs1 And DateEntered = @dateExisting And [Role] = @role;
		Else
			-- If the message was cleared out, then delete
			Delete From CCG_EI_Messages
				Where WBS1 = @wbs1 And DateEntered = @dateExisting And [Role] = @role;

		SET @rowsAffected = @@ROWCOUNT;
	END;

	Select @rowsAffected;
END;

GO
