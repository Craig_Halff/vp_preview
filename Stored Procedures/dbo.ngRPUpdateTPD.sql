SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ngRPUpdateTPD]
  @strRowID Nvarchar(255),
  @strScopeStartDate varchar(8),
  @strScopeEndDate varchar(8),
  @decHrs decimal(19,4)
AS

BEGIN -- Procedure ngRPUpdateTPD

  SET NOCOUNT ON
  
  DECLARE @dtScopeStartDate datetime
  DECLARE @dtScopeEndDate datetime
  DECLARE @dtMinTPD datetime
  DECLARE @dtMaxTPD datetime
  DECLARE @dtScopeBeforeDate datetime
  DECLARE @dtScopeAfterDate datetime
  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime

  DECLARE @intOutlineLevel int
   
  DECLARE @strCompany Nvarchar(14)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strResourceID Nvarchar(20)
  DECLARE @strGenericResourceID Nvarchar(20)
  DECLARE @strResourceType varchar(1)
  DECLARE @strIDPrefix Nvarchar(255)
  DECLARE @strUserName Nvarchar(32)

  DECLARE @siGRMethod smallint
  DECLARE @siCostRtMethod smallint
  DECLARE @siBillingRtMethod smallint
  
  DECLARE @intCostRtTableNo int
  DECLARE @intBillingRtTableNo int
  DECLARE @intGenResTableNo int
  DECLARE @intGRBillTableNo int

  DECLARE @siLabRateDecimals smallint
  DECLARE @siHrDecimals smallint

  DECLARE @decScopeTotalHrs decimal(19,4) = 0
  DECLARE @decScopeTotalWD decimal(19,4) = 0
  DECLARE @decBeforeWD decimal(19,4) = 0
  DECLARE @decAfterWD decimal(19,4) = 0
  DECLARE @decDeltalHrs decimal(19,4) = 0

  DECLARE @bitIsOnLeafTask as bit = 0
   
  -- Declare Temp tables.
  
	DECLARE @tabCalendar TABLE (
		StartDate	datetime,
		EndDate	datetime
    UNIQUE(StartDate, EndDate)
	)

  DECLARE @tabAssignment TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    ResourceID Nvarchar(20) COLLATE database_default,
    Category smallint,
    GRLBCD Nvarchar(14) COLLATE database_default,
    StartDate datetime,
    EndDate datetime,
    CostRate decimal(19,4),
    BillingRate decimal(19,4),
    ScopeWD decimal(19,4),
    AT_OutlineNumber varchar(255) COLLATE database_default,
    AT_OutlineLevel int
    UNIQUE(PlanID, TaskID, AssignmentID)
  )

  DECLARE @tabTask TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    Name Nvarchar(255) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    OutlineLevel int
    UNIQUE(PlanID, TaskID, OutlineNumber)
  )

	DECLARE @tabTPD TABLE(
	  TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
	  AssignmentID varchar(32) COLLATE database_default,
    StartDate datetime, 
    EndDate datetime, 
    PeriodHrs decimal(19,4),
    HardBooked varchar(1) COLLATE database_default
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, StartDate, EndDate)
  ) 

	DECLARE @tabS1TPD TABLE (
	  RowID varchar(32) COLLATE database_default,
    TimePhaseID varchar(32) COLLATE database_default,
    CIStartDate datetime, 
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    StartDate datetime, 
    EndDate datetime, 
    PeriodHrs decimal(19,4), 
    HardBooked varchar(1) COLLATE database_default
    UNIQUE(RowID, PlanID, TaskID, AssignmentID, StartDate, EndDate)
  )

	DECLARE @tabS2TPD TABLE (
    TimePhaseID varchar(32) COLLATE database_default,
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    AssignmentID varchar(32) COLLATE database_default,
    StartDate datetime, 
    EndDate datetime, 
    PeriodHrs decimal(19,4), 
    CostRate decimal(19,4),
    BillingRate decimal(19,4),
    HardBooked varchar(1) COLLATE database_default
    UNIQUE(TimePhaseID, PlanID, TaskID, AssignmentID, StartDate, EndDate)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

  SELECT 
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE())))
    FROM CFGRMSettings

  -- Set Dates

  SET @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)
  SET @dtScopeStartDate = CONVERT(datetime, @strScopeStartDate)
  SET @dtScopeEndDate = CONVERT(datetime, @strScopeEndDate)
  SET @dtScopeStartDate = /* Need to adjust Scope Start Date when ETC Date is between Scope Start and End Dates. */
    CASE 
      WHEN @dtETCDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate 
      THEN @dtETCDate 
      ELSE @dtScopeStartDate 
    END
  SET @dtScopeBeforeDate = DATEADD(DAY, -1, @dtScopeStartDate)
  SET @dtScopeAfterDate = DATEADD(DAY, 1, @dtScopeEndDate)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Parse @strRowID
  -- ParentRowID is a string with the following format
  --   1. E~<RPAssignment.ResourceID>|<RPTask.TaskID>
  --   2. G~<RPAssignment.GenericResourceID>|<RPTask.TaskID>

  SET @strResourceType = SUBSTRING(@strRowID, 1, 1)
	DECLARE @intBarPos int = CHARINDEX('|', @strRowID)

	IF (@intBarPos >=1)
		SET @strIDPrefix = LEFT(@strRowID, @intBarPos - 1) 
	ELSE
		SET @strIDPrefix = ''
	
	IF (SUBSTRING(@strRowID, 2, 1) = '~')
		BEGIN
			IF (@strResourceType = 'E')
				BEGIN
					SET @strResourceID = REPLACE(@strIDPrefix, 'E~', '')
					SET @strGenericResourceID = NULL
				END
			ELSE
				BEGIN
					SET @strGenericResourceID = REPLACE(@strIDPrefix, 'G~', '')
					SET @strResourceID = NULL
				END
		END
	ELSE
		BEGIN
					SET @strGenericResourceID = NULL
					SET @strResourceID = NULL
		END

	IF (@intBarPos >=0)
		SET @strTaskID = SUBSTRING(@strRowID, @intBarPos + 1, LEN(@strRowID))

  -- Get various parameters.

  SELECT 
    @strCompany = P.Company,
    @siGRMethod = P.GRMethod,
    @intGenResTableNo = P.GenResTableNo,
    @intGRBillTableNo = P.GRBillTableNo,
    @siCostRtMethod = P.CostRtMethod,
    @siBillingRtMethod = P.BillingRtMethod,
    @intCostRtTableNo = P.CostRtTableNo,
    @intBillingRtTableNo = P.BillingRtTableNo,
    @strPlanID = T.PlanID,
    @intOutlineLevel = T.OutlineLevel 
    FROM RPTask AS T
      INNER JOIN RPPlan AS P ON T.PlanID = P.PlanID
    WHERE T.TaskID = @strTaskID

  --decimals set basend on the company
  SELECT 
    @siHrDecimals = HrDecimals,
    @siLabRateDecimals = LabRateDecimals
    FROM CFGResourcePlanning Where Company = @strCompany

  -- Determine whether the input Task is a leaf Task.

  SELECT
    @bitIsOnLeafTask = 
      CASE
        WHEN A.AssignmentID IS NOT NULL
        THEN 1
        ELSE 0
      END
    FROM RPAssignment AS A
    WHERE PlanID = @strPlanID AND TaskID = @strTaskID
      AND ((ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') 
      AND ISNULL(A.GenericResourceID, '|') = ISNULL( @strGenericResourceID, '|'))
				OR (@strResourceID IS NULL AND @strGenericResourceID IS NULL))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabAssignment(
    PlanID,
    TaskID,
    AssignmentID,
    ResourceID,
    Category,
    GRLBCD,
    StartDate,
    EndDate,
    CostRate,
    BillingRate,
    ScopeWD,
    AT_OutlineNumber,
    AT_OutlineLevel
  )
    SELECT DISTINCT
      A.PlanID,
      A.TaskID,
      A.AssignmentID,
      A.ResourceID,
      A.Category,
      A.GRLBCD,
      A.StartDate,
      A.EndDate,
      A.CostRate,
      A.BillingRate,
      CASE
        WHEN (((A.StartDate > @dtScopeEndDate) OR (A.EndDate < @dtScopeStartDate)) AND A.TaskID <> @strTaskID)
        THEN 0
        ELSE
          dbo.DLTK$NumWorkingDays(
            CASE
              WHEN (A.StartDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
              THEN A.StartDate
              ELSE @dtScopeStartDate
            END, 
            CASE
              WHEN (A.EndDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
              THEN A.EndDate
              ELSE @dtScopeEndDate
            END, 
            @strCompany
          )
      END AS ScopeWD,
      AT.OutlineNumber AS AT_OutlineNumber,
      AT.OutlineLevel AS AT_OutlineLevel
      FROM RPAssignment AS A
        INNER JOIN RPTask AS AT ON A.PlanID = AT.PlanID AND A.TaskID = AT.TaskID
        INNER JOIN RPTask AS PT ON A.PlanID = PT.PlanID 
      WHERE
        PT.TaskID = @strTaskID AND
        AT.OutlineNumber LIKE PT.OutlineNumber + '%' AND
        ((ISNULL(A.ResourceID, '|') = ISNULL(@strResourceID, '|') 
				AND ISNULL(A.GenericResourceID, '|') = ISNULL( @strGenericResourceID, '|'))
					OR (@strResourceID IS NULL AND @strGenericResourceID IS NULL))

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabTask(
    PlanID,
    TaskID,
    Name,
    ParentOutlineNumber,
    OutlineNumber,
    OutlineLevel
  )
    SELECT DISTINCT
      PT.PlanID AS PlanID,
      PT.TaskID AS TaskID,
      PT.Name AS Name,
      PT.ParentOutlineNumber AS ParentOutlineNumber,
      PT.OutlineNumber AS OutlineNumber,
      PT.OutlineLevel AS OutlineLevel
      FROM @tabAssignment AS A
        INNER JOIN RPTask AS PT ON A.PlanID = PT.PlanID
      WHERE PT.PlanID = @strPlanID AND PT.OutlineLevel >= @intOutlineLevel AND PT.OutlineLevel <= A.AT_OutlineLevel
        AND A.AT_OutlineNumber LIKE PT.OutlineNumber + '%' 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Collecting Labor Time-Phased Data to be used in subsequent calculations.
  -- The TPD records come from only the Assignment record indentified by @strAssignmentID.

  INSERT @tabTPD(
    TimePhaseID,
    PlanID,
    TaskID,
    AssignmentID,
    StartDate,
    EndDate,
    PeriodHrs,
    HardBooked
  )
    SELECT
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID,
      TPD.TaskID AS TaskID,
      TPD.AssignmentID AS AssignmentID,
      TPD.StartDate AS StartDate,
      TPD.EndDate AS EndDate,
      TPD.PeriodHrs AS PeriodHrs,
      TPD.HardBooked AS HardBooked
      FROM RPPlannedLabor AS TPD
        INNER JOIN @tabAssignment AS A ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID AND TPD.AssignmentID = A.AssignmentID AND TPD.AssignmentID IS NOT NULL
      WHERE TPD.PeriodHrs <> 0 AND 
        TPD.StartDate <= @dtScopeEndDate AND TPD.EndDate >= @dtScopeStartDate

  SELECT 
    @dtMinTPD = COALESCE(MIN(StartDate), @dtScopeEndDate), 
    @dtMaxTPD = COALESCE(MAX(EndDate), @dtScopeStartDate)
    FROM @tabTPD AS TPD

  SET @decBeforeWD = dbo.DLTK$NumWorkingDays(@dtMinTPD, @dtScopeBeforeDate, @strCompany)
  SET @decAfterWD = dbo.DLTK$NumWorkingDays(@dtScopeAfterDate, @dtMaxTPD, @strCompany)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Build Calendar.
  -- Calendar has at most 3 ranges {Before, Main, After}.

  IF (@decBeforeWD > 0)
    BEGIN

	    INSERT @tabCalendar(
	      StartDate,
	      EndDate
	    )	
		    SELECT 
		      @dtMinTPD AS StartDate,
		      @dtScopeBeforeDate AS EndDate

    END

	INSERT @tabCalendar(
	  StartDate,
	  EndDate
	)	
		SELECT 
		  @dtScopeStartDate AS StartDate,
		  @dtScopeEndDate AS EndDate

  IF (@decAfterWD > 0)
    BEGIN

	    INSERT @tabCalendar(
	      StartDate,
	      EndDate
	    )	
		    SELECT 
		      @dtScopeAfterDate AS StartDate,
		      @dtMaxTPD AS EndDate

    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Split up TPD in @tabTPD to match with Calendar Intervals.
  -- @tabTPD could contain chunks that are larger than a calendar interval which need to be broken up.
  -- @tabTPD could contain chunks that are overlapping with the calendar interval boundaries which need to be broken off.
  -- @tabTPD could contain chunks that are smaller and contain within the span of a calendar interval.

  INSERT @tabS1TPD(
    RowID,
    TimePhaseID,
    CIStartDate,
    PlanID, 
    TaskID,
    AssignmentID,
    StartDate, 
    EndDate, 
    PeriodHrs,
    HardBooked
   )
     SELECT
      REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS RowID,
      TimePhaseID AS TimePhaseID,
      CIStartDate AS CIStartDate,
      PlanID AS PlanID, 
      TaskID AS TaskID,
      AssignmentID AS AssignmentID,
      StartDate AS StartDate, 
      EndDate AS EndDate, 
      ROUND(ISNULL(PeriodHrs, 0), @siHrDecimals) AS PeriodHrs,
      HardBooked AS HardBooked
    FROM (
      SELECT 
        CI.StartDate AS CIStartDate, 
        TPD.TimePhaseID AS TimePhaseID,
        TPD.PlanID AS PlanID, 
        TPD.TaskID AS TaskID,
        TPD.AssignmentID AS AssignmentID,
        CASE WHEN TPD.StartDate > CI.StartDate THEN TPD.StartDate ELSE CI.StartDate END AS StartDate, 
        CASE WHEN TPD.EndDate < CI.EndDate THEN TPD.EndDate ELSE CI.EndDate END AS EndDate,
        CASE 
          WHEN (TPD.StartDate < CI.StartDate OR TPD.EndDate > CI.EndDate)
          THEN PeriodHrs * 
            dbo.DLTK$ProrateRatio(
              CASE 
                WHEN TPD.StartDate > CI.StartDate 
                THEN TPD.StartDate 
                ELSE CI.StartDate 
              END, 
              CASE 
                WHEN TPD.EndDate < CI.EndDate 
                THEN TPD.EndDate 
                ELSE CI.EndDate 
              END, 
              TPD.StartDate, TPD.EndDate,
              @strCompany)
          ELSE PeriodHrs 
        END AS PeriodHrs,
        TPD.HardBooked AS HardBooked
        FROM @tabCalendar AS CI 
          INNER JOIN @tabTPD AS TPD 
            ON TPD.StartDate <= CI.EndDate AND TPD.EndDate >= CI.StartDate
    ) AS X
    WHERE (PeriodHrs IS NOT NULL AND PeriodHrs > 0)

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     
  -- Adjust time-phased data to compensate for rounding errors after the splitting process.

  UPDATE @tabS1TPD SET PeriodHrs = (TPD.PeriodHrs + D.DeltaHrs)
    FROM @tabS1TPD AS TPD 
      INNER JOIN (
        SELECT YTPD.TimePhaseID AS TimePhaseID, (YTPD.PeriodHrs - SUM(ISNULL(XTPD.PeriodHrs, 0))) AS DeltaHrs
          FROM @tabS1TPD AS XTPD 
            INNER JOIN @tabTPD AS YTPD ON XTPD.TimePhaseID = YTPD.TimePhaseID
          GROUP BY YTPD.TimePhaseID, YTPD.PeriodHrs
      ) AS D ON TPD.TimePhaseID = D.TimePhaseID
    WHERE RowID IN (
      SELECT RowID FROM @tabS1TPD AS ATPD 
        INNER JOIN (
          SELECT TimePhaseID AS TimePhaseID, MAX(EndDate) AS EndDate FROM @tabS1TPD GROUP BY TimePhaseID
        ) AS BTPD ON BTPD.TimePhaseID = ATPD.TimePhaseID AND BTPD.EndDate = ATPD.EndDate
    )
             
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- At this point we have a table with TPD rows aligned on the scope boundaries.
  -- Let's find out whether there is any hour within the scope. 
  -- This information is needed to decide whether the spread of new hours will be proportional or based on Work-Days.

  SELECT @decScopeTotalHrs = SUM(PeriodHrs)
    FROM @tabS1TPD
    WHERE StartDate <= @dtScopeEndDate AND EndDate >= @dtScopeStartDate

  -- Copy from @tabS1TPD those TPD rows that are outside of scope to @tabS2TPD.
  -- These TPD rows will retain the original hours as in @tabS1TPD.

  INSERT @tabS2TPD(
    TimePhaseID,
    PlanID, 
    TaskID,
    AssignmentID,
    StartDate, 
    EndDate, 
    PeriodHrs,
    CostRate,
    BillingRate,
    HardBooked
   )
     SELECT
      TPD.RowID AS TimePhaseID,
      TPD.PlanID AS PlanID, 
      TPD.TaskID AS TaskID,
      TPD.AssignmentID AS AssignmentID,
      TPD.StartDate AS StartDate, 
      TPD.EndDate AS EndDate, 
      TPD.PeriodHrs AS PeriodHrs,
      dbo.RP$LabRate(
        A.ResourceID,
        ISNULL(A.Category, 0),
        A.GRLBCD,
        NULL,
        @siCostRtMethod,
        @intCostRtTableNo,
        @siGRMethod,
        @intGenResTableNo,
        TPD.StartDate,
        TPD.EndDate,
        @siLabRateDecimals,
        0,
        ISNULL(A.CostRate, 0)
      ) AS CostRate,
      dbo.RP$LabRate(
        A.ResourceID,
        ISNULL(A.Category, 0),
        A.GRLBCD,
        NULL,
        @siBillingRtMethod,
        @intBillingRtTableNo,
        @siGRMethod,
        @intGRBillTableNo,
        TPD.StartDate,
        TPD.EndDate,
        @siLabRateDecimals,
        0,
        ISNULL(A.BillingRate, 0)
      ) AS BillingRate,
      TPD.HardBooked AS HardBooked
    FROM @tabS1TPD AS TPD
      INNER JOIN @tabAssignment AS A ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID AND TPD.AssignmentID = A.AssignmentID
    WHERE TPD.EndDate < @dtScopeStartDate OR TPD.StartDate > @dtScopeEndDate AND TPD.PeriodHrs > 0

  -- Spread new hours to TPD rows within the scope.

  IF (@decScopeTotalHrs > 0)
    BEGIN /* Spread proportional */

      INSERT @tabS2TPD(
        TimePhaseID,
        PlanID, 
        TaskID,
        AssignmentID,
        StartDate, 
        EndDate, 
        PeriodHrs,
        CostRate,
        BillingRate,
        HardBooked
      )
        SELECT
        TPD.RowID AS TimePhaseID,
        TPD.PlanID AS PlanID, 
        TPD.TaskID AS TaskID,
        TPD.AssignmentID AS AssignmentID,
        TPD.StartDate AS StartDate, 
        TPD.EndDate AS EndDate, 
        ROUND(ISNULL((@decHrs * (TPD.PeriodHrs / @decScopeTotalHrs)), 0.0000), @siHrDecimals) AS PeriodHrs,
        dbo.RP$LabRate(
          A.ResourceID,
          ISNULL(A.Category, 0),
          A.GRLBCD,
          NULL,
          @siCostRtMethod,
          @intCostRtTableNo,
          @siGRMethod,
          @intGenResTableNo,
          TPD.StartDate,
          TPD.EndDate,
          @siLabRateDecimals,
          0,
          ISNULL(A.CostRate, 0)
        ) AS CostRate,
        dbo.RP$LabRate(
          A.ResourceID,
          ISNULL(A.Category, 0),
          A.GRLBCD,
          NULL,
          @siBillingRtMethod,
          @intBillingRtTableNo,
          @siGRMethod,
          @intGRBillTableNo,
          TPD.StartDate,
          TPD.EndDate,
          @siLabRateDecimals,
          0,
          ISNULL(A.BillingRate, 0)
        ) AS BillingRate,
        TPD.HardBooked AS HardBooked
      FROM @tabS1TPD AS TPD
        INNER JOIN @tabAssignment AS A ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID AND TPD.AssignmentID = A.AssignmentID
      WHERE TPD.StartDate <= @dtScopeEndDate AND TPD.EndDate >= @dtScopeStartDate AND 
        (TPD.PeriodHrs IS NOT NULL AND TPD.PeriodHrs > 0)

    END /* END IF (@decScopeTotalHrs > 0) THEN */
  ELSE
    BEGIN /* Spread based on Leaf Work Days */

      -- Calculate total work days for all of Assignments that crossed the scope range.

      SELECT @decScopeTotalWD = SUM(ScopeWD)
        FROM @tabAssignment

    --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      IF (@decScopeTotalWD > 0)
        BEGIN

          INSERT @tabS2TPD(
            TimePhaseID,
            PlanID, 
            TaskID,
            AssignmentID,
            StartDate, 
            EndDate, 
            PeriodHrs,
            CostRate,
            BillingRate,
            HardBooked
          )
            SELECT
            REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
            A.PlanID AS PlanID, 
            A.TaskID AS TaskID,
            A.AssignmentID AS AssignmentID,
            CASE
              WHEN (A.StartDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
              THEN A.StartDate
              ELSE @dtScopeStartDate
            END AS StartDate, 
            CASE
              WHEN (A.EndDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
              THEN A.EndDate
              ELSE @dtScopeEndDate
            END AS EndDate, 
            ROUND(ISNULL((@decHrs * (A.ScopeWD / @decScopeTotalWD)), 0.0000), @siHrDecimals) AS PeriodHrs,
            dbo.RP$LabRate(
              A.ResourceID,
              ISNULL(A.Category, 0),
              A.GRLBCD,
              NULL,
              @siCostRtMethod,
              @intCostRtTableNo,
              @siGRMethod,
              @intGenResTableNo,
              CASE
                WHEN (A.StartDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
                THEN A.StartDate
                ELSE @dtScopeStartDate
              END,
              CASE
                WHEN (A.EndDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
                THEN A.EndDate
                ELSE @dtScopeEndDate
              END,
              @siLabRateDecimals,
              0,
              ISNULL(A.CostRate, 0)
            ) AS CostRate,
            dbo.RP$LabRate(
              A.ResourceID,
              ISNULL(A.Category, 0),
              A.GRLBCD,
              NULL,
              @siBillingRtMethod,
              @intBillingRtTableNo,
              @siGRMethod,
              @intGRBillTableNo,
              CASE
                WHEN (A.StartDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
                THEN A.StartDate
                ELSE @dtScopeStartDate
              END,
              CASE
                WHEN (A.EndDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
                THEN A.EndDate
                ELSE @dtScopeEndDate
              END,
              @siLabRateDecimals,
              0,
              ISNULL(A.BillingRate, 0)
            ) AS BillingRate,
            'N' AS HardBooked
          FROM @tabAssignment AS A

        END /* END IF (@decScopeTotalWD > 0) THEN */

      ELSE IF (@decScopeTotalWD = 0 AND @bitIsOnLeafTask = 1)
        BEGIN

          INSERT @tabS2TPD(
            TimePhaseID,
            PlanID, 
            TaskID,
            AssignmentID,
            StartDate, 
            EndDate, 
            PeriodHrs,
            CostRate,
            BillingRate,
            HardBooked
          )
            SELECT
            REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS TimePhaseID,
            A.PlanID AS PlanID, 
            A.TaskID AS TaskID,
            A.AssignmentID AS AssignmentID,
            CASE
              WHEN (A.StartDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
              THEN A.StartDate
              ELSE @dtScopeStartDate
            END AS StartDate, 
            CASE
              WHEN (A.EndDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
              THEN A.EndDate
              ELSE @dtScopeEndDate
            END AS EndDate, 
            @decHrs AS PeriodHrs,
            dbo.RP$LabRate(
              A.ResourceID,
              ISNULL(A.Category, 0),
              A.GRLBCD,
              NULL,
              @siCostRtMethod,
              @intCostRtTableNo,
              @siGRMethod,
              @intGenResTableNo,
              CASE
                WHEN (A.StartDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
                THEN A.StartDate
                ELSE @dtScopeStartDate
              END,
              CASE
                WHEN (A.EndDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
                THEN A.EndDate
                ELSE @dtScopeEndDate
              END,
              @siLabRateDecimals,
              0,
              ISNULL(A.CostRate, 0)
            ) AS CostRate,
            dbo.RP$LabRate(
              A.ResourceID,
              ISNULL(A.Category, 0),
              A.GRLBCD,
              NULL,
              @siBillingRtMethod,
              @intBillingRtTableNo,
              @siGRMethod,
              @intGRBillTableNo,
              CASE
                WHEN (A.StartDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
                THEN A.StartDate
                ELSE @dtScopeStartDate
              END,
              CASE
                WHEN (A.EndDate BETWEEN @dtScopeStartDate AND @dtScopeEndDate)
                THEN A.EndDate
                ELSE @dtScopeEndDate
              END,
              @siLabRateDecimals,
              0,
              ISNULL(A.BillingRate, 0)
            ) AS BillingRate,
            'N' AS HardBooked
          FROM @tabAssignment AS A

        END /* END ELSE IF (@decScopeTotalWD = 0 AND @bitIsOnLeafTask = 1) */

    END /* END IF (@decScopeTotalHrs > 0) ELSE */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Need to adjust hours for the last TPD row within the scope to compensate for rounding errors.

  SELECT @decDeltalHrs = (@decHrs - SUM(ISNULL(XTPD.PeriodHrs, 0)))
    FROM @tabS2TPD AS XTPD 
    WHERE StartDate <= @dtScopeEndDate AND EndDate >= @dtScopeStartDate

  UPDATE @tabS2TPD SET PeriodHrs = (TPD.PeriodHrs + @decDeltalHrs)
    FROM @tabS2TPD AS TPD 
    WHERE @decDeltalHrs <> 0 AND
      TimePhaseID IN (
        SELECT TimePhaseID
          FROM (
            SELECT 
              TPD.TimePhaseID AS TimePhaseID, 
              RANK() OVER (PARTITION BY TPD.PlanID ORDER BY A.AT_OutlineNumber DESC, TPD.EndDate DESC) AS SortRank
              FROM @tabS2TPD AS TPD
                INNER JOIN @tabAssignment AS A 
                  ON TPD.PlanID = A.PlanID AND TPD.TaskID = A.TaskID AND TPD.AssignmentID = A.AssignmentID
              WHERE TPD.StartDate <= @dtScopeEndDate AND TPD.EndDate >= @dtScopeStartDate
          ) AS BTPD 
          WHERE SortRank = 1
      )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION

  DELETE RPPlannedLabor 
    WHERE 
      PlanID = @strPlanID 
      AND (
        TimePhaseID IN (SELECT DISTINCT TimePhaseID FROM @tabTPD) 
        OR PeriodHrs = 0
      )

  INSERT RPPlannedLabor(
    TimePhaseID,
    PlanID, 
    TaskID,
    AssignmentID,
    StartDate, 
    EndDate, 
    PeriodHrs,
    CostRate,
    BillingRate,
    HardBooked,
    CreateUser,
    ModUser,
    CreateDate,
    ModDate
  )
     SELECT
      TPD.TimePhaseID AS TimePhaseID,
      TPD.PlanID AS PlanID, 
      TPD.TaskID AS TaskID,
      TPD.AssignmentID AS AssignmentID,
      TPD.StartDate AS StartDate, 
      TPD.EndDate AS EndDate, 
      TPD.PeriodHrs AS PeriodHrs,
      TPD.CostRate,
      TPD.BillingRate,
      TPD.HardBooked AS HardBooked,
      CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'ngRPUpdateTPD' ELSE 'UTPD_' + @strUserName END AS CreateUser,
      CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'ngRPUpdateTPD' ELSE 'UTPD_' + @strUserName END AS ModUser,
      LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As CreateDate,
      LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As ModDate 
    FROM @tabS2TPD AS TPD
    WHERE PeriodHrs > 0

  COMMIT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     
  SET NOCOUNT OFF

END -- ngRPUpdateTPD
GO
