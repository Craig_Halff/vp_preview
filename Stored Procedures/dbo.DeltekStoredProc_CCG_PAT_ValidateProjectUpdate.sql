SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_ValidateProjectUpdate] (
	@includeInactive	bit,
	@regularOnly		bit,
	@whereClause		Nvarchar(max)
) AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_ValidateProjectUpdate] 1, 1, 'wbs2 = '' '''
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max);

	SET @sql = N'
		SELECT wbs1, wbs2, wbs3, sublevel, status, chargetype, readyForProcessing
			FROM PR
			WHERE ' + @whereClause + ' and readyForProcessing = ''Y''
				and status IN (''A''' +
					(CASE WHEN @includeInactive = 1 THEN ', ''I''' ELSE '' END) + ') ' +
					(CASE WHEN @regularOnly = 1 THEN ' and chargetype = ''R'' ' ELSE '' END) + '
			ORDER BY 1, 2, 3';

	PRINT @sql
	EXEC(@sql);
END;

GO
