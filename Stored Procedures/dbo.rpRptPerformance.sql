SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[rpRptPerformance] 
  @dtETCDate datetime,
  @strUnposted varchar(1),
  @strCommitment varchar(1),
  @strBudgetType varchar(1),
  @strExpTab varchar(1),
  @strConTab varchar(1),
  @strUnitTab varchar(1),
  @rpIncEmp varchar(1),
  @rpIncNBemp varchar(1),
  @rpIncGR varchar(1),
  @rpIncUAtaskLabor varchar(1),
  @rpIncNBexpAcct varchar(1),
  @rpIncUAtaskExp varchar(1),
  @rpIncNBconAcct varchar(1),
  @rpIncUAtaskCon varchar(1),
  @rpIncNBunitAcct varchar(1),
  @rpIncUAtaskUnit varchar(1),
  @ReportAtBillingInBillingCurr varchar(1) = 'N'
AS

BEGIN --procedure rpRptPerformance start

  declare @strSelect as varchar(300)

  declare @strPlanID varchar(32)
  declare @sintGRMethod smallint
  declare @strBudgetToUse varchar(1)
  declare @decOverheadPct decimal(19,4)
  declare @strOutlineLevel int

  DECLARE planInfo  CURSOR FOR Select PlanID, GRMethod, useBudgetType, overheadPct from #rptPlanList

  BEGIN --open planInfo
     OPEN planInfo

     FETCH from planInfo INTO @strPlanID, @sintGRMethod, @strBudgetToUse, @decOverheadPct
     WHILE @@FETCH_STATUS = 0 
       begin --while @@FETCH_STATUS = 0 start

	   SELECT @strOutlineLevel = MIN(rpTask.OutlineLevel) FROM RPTask WHERE rpTask.planID = @strPlanID and rpTask.WBSType = 'WBS1'

       if  (@rpIncNBemp='Y' and @rpIncEmp='Y')
         begin
           insert #rptLaborJTD
           select planID=@strPlanID,
			useBudgetType = @strBudgetToUse,
			overheadPct = @decOverheadPct,
			employee=aJTD.employee,
	       category=0,
	       grlbcd='',
           MAX(case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
           COUNT( distinct case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
      	   SUM(PeriodHrs) AS JTDHrs,    
	       SUM(case when @strBudgetToUse='B' then PeriodBill else PeriodCost end) AS JTDCostBill
           from dbo.RP$rptNonBudgetEmpJTD(@strPlanID,@dtETCDate,@strOutlineLevel,NULL,@strUnposted,'Y','N',@ReportAtBillingInBillingCurr) aJTD 	
           GROUP BY aJTD.employee
         end
       else
         begin
           if ((@rpIncEmp='Y' OR @rpIncGR='Y') and @rpIncNBemp='N')
             begin
               insert #rptLaborJTD
               select planID=@strPlanID,
			useBudgetType = @strBudgetToUse,
			overheadPct = @decOverheadPct,
			employee=Max(ISNULL(rpA.resourceID,'')),
	       category=ISNULL(rpA.category,0),
	       grlbcd=ISNULL(rpA.GRLBCD,''),
           MAX(case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
           COUNT( distinct case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
      	       SUM(PeriodHrs) AS JTDHrs,    
	       SUM(case when @strBudgetToUse='B' then PeriodBill else PeriodCost end) AS JTDCostBill
               from dbo.RP$rptAssignJTD(@strPlanID,@dtETCDate,NULL,@strUnposted,@sintGRMethod,'N',@ReportAtBillingInBillingCurr) aJTD 	
               join rpAssignment rpA on rpA.assignmentID = aJTD.assignmentID
               where (@rpIncEmp='Y' and rpA.resourceID is not null) OR (@rpIncNBemp='N' and (@rpIncGR='Y' and (rpA.category>0 OR rpA.grlbcd is not null)))
               GROUP BY rpA.resourceID,rpA.category,rpA.grlbcd
             end

           if  (@rpIncUAtaskLabor='Y' and @rpIncNBemp='N')
             begin
               insert #rptLaborJTD
               select  planID=@strPlanID,
			useBudgetType = @strBudgetToUse,
			overheadPct = @decOverheadPct,
			employee='',
	       category=0,
	       grlbcd='',
           MAX(case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
           COUNT( distinct case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
      	       SUM(PeriodHrs) AS JTDHrs,    
	       SUM(case when @strBudgetToUse='B' then PeriodBill else PeriodCost end) AS JTDCostBill
              from dbo.RP$rptTaskJTDLabor (@strPlanID, @dtETCDate, @strUnposted, 'N', @sintGRMethod,@ReportAtBillingInBillingCurr) tJTD 	
              join rpTask rpT on rpT.planID = tJTD.planID and rpT.taskID=tJTD.taskID
              where rpT.childrenCount=0 and rpT.wbsType is not null and not exists (select 'X' from rpAssignment rpA where rpA.planID=rpT.planID and rpA.taskID=rpT.taskID)
           end
         end -- if else begin (@rpIncNBemp='Y' and @rpIncEmp='Y') 


       if @strExpTab = 'Y'
         begin
           if @rpIncNBexpAcct='Y'
             begin
               insert #rptExpJTD
               select planID=@strPlanID,
                    Account=eJTD.Account,
                    Vendor=eJTD.Vendor,
           MAX(case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
           COUNT( distinct case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
	       SUM(case when @strBudgetToUse='B' then PeriodBill else PeriodCost end) AS JTDHrs
              from dbo.RP$rptNonBudgetExpConJTD(@strPlanID,@dtETCDate,@strOutlineLevel,NULL,@strCommitment,'Y','N',@strUnitTab,'E',@ReportAtBillingInBillingCurr) eJTD
              GROUP BY eJTD.Account, eJTD.Vendor
             end
           else
             begin

               insert #rptExpJTD
               select planID=@strPlanID,
					Account=rpE.Account,
                       Vendor=rpE.Vendor,
           MAX(case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
           COUNT( distinct case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
	          SUM(case when @strBudgetToUse='B' then PeriodBill else PeriodCost end) AS JTDHrs
               from dbo.RP$rptExpConJTD(@strPlanID,@dtETCDate,NULL,@strCommitment,'N',@strUnitTab,'E',@ReportAtBillingInBillingCurr) eJTD
               join rpExpense rpE on rpE.ExpenseID = eJTD.RowID
               GROUP BY rpE.Account, rpE.Vendor

               if @rpIncUAtaskExp='Y'
                 begin
                   insert #rptExpJTD
                   select 	planID=@strPlanID,
							Account='',
                          Vendor='',
           MAX(case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
           COUNT( distinct case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
	             SUM(case when @strBudgetToUse='B' then PeriodBill else PeriodCost end) AS JTDHrs
                   from dbo.RP$rptTaskJTDExpCon(@strPlanID,@dtETCDate,@strCommitment,@strUnitTab,'N','E',@ReportAtBillingInBillingCurr) etJTD  	
                   join rpTask rpT on rpT.planID = etJTD.planID and rpT.taskID=etJTD.taskID
                   where rpT.childrenCount=0 and rpT.wbsType is not null and not exists (select 'X' from rpExpense rpE where rpE.planID=rpT.planID and rpE.taskID=rpT.taskID)
               end
         end  -- if else @rpIncNBexpAcct='Y' 
       end --if @strExpTab = 'Y'


       if @strConTab = 'Y'
         begin
           if @rpIncNBconAcct='Y'
             begin
               insert #rptConJTD
               select planID=@strPlanID,
				Account=cJTD.Account,
                    Vendor=cJTD.Vendor,
           MAX(case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
           COUNT( distinct case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
	       SUM(case when @strBudgetToUse='B' then PeriodBill else PeriodCost end) AS JTDHrs
              from dbo.RP$rptNonBudgetExpConJTD(@strPlanID,@dtETCDate,@strOutlineLevel,NULL,@strCommitment,'Y','N',@strUnitTab,'C',@ReportAtBillingInBillingCurr) cJTD
              GROUP BY cJTD.Account, cJTD.Vendor
            end
          else
            begin
              insert #rptConJTD
              select planID=@strPlanID,
				Account=rpCon.Account,
                       Vendor=rpCon.Vendor,
           MAX(case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
           COUNT( distinct case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
	          SUM(case when @strBudgetToUse='B' then PeriodBill else PeriodCost end) AS JTDHrs
             from dbo.RP$rptExpConJTD(@strPlanID,@dtETCDate,NULL,@strCommitment,'N',@strUnitTab,'C',@ReportAtBillingInBillingCurr) cJTD
             join rpConsultant rpCon on rpCon.ConsultantID = cJTD.RowID
            GROUP BY rpCon.Account, rpCon.Vendor

             if @rpIncUAtaskCon='Y'
               begin
                 insert #rptConJTD
                 select planID=@strPlanID,
					Account='',
                      Vendor='',
           MAX(case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
           COUNT( distinct case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
	         SUM(case when @strBudgetToUse='B' then PeriodBill else PeriodCost end) AS JTDHrs
                 from dbo.RP$rptTaskJTDExpCon(@strPlanID,@dtETCDate,@strCommitment,@strUnitTab,'N','C',@ReportAtBillingInBillingCurr) ctJTD  	
                 join rpTask rpT on rpT.planID = ctJTD.planID and rpT.taskID=ctJTD.taskID
                 where rpT.childrenCount=0 and rpT.wbsType is not null and not exists (select 'X' from rpConsultant rpCon where rpCon.planID=rpT.planID and rpCon.taskID=rpT.taskID)
               end
           end  --if else @rpIncNBconAcct='Y' 
         end --if @strConTab = 'Y'

       if @strUnitTab = 'Y'
         begin
          if @rpIncNBunitAcct='Y'
             begin
               insert #rptUnitJTD
               select planID=@strPlanID,
			Account=uJTD.Account,
	       Unit=uJTD.Unit,
                    UnitTable=uJTD.UnitTable,
           MAX(case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
           COUNT( distinct case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
	       SUM(case when @strBudgetToUse='B' then PeriodBill else PeriodCost end) AS JTDHrs
              from dbo.RP$rptNonBudgetUnitJTD(@strPlanID,@dtETCDate,@strOutlineLevel,'Y',NULL,@ReportAtBillingInBillingCurr) uJTD
              GROUP BY uJTD.Account, uJTD.UnitTable, uJTD.Unit
            end
          else
             begin
             insert #rptUnitJTD
             select planID=@strPlanID,
			Account=rpU.Account,
	       Unit=rpU.Unit,
                    UnitTable=rpU.UnitTable,
           MAX(case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
           COUNT( distinct case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
	       SUM(case when @strBudgetToUse='B' then PeriodBill else PeriodCost end) AS JTDHrs
            from dbo.RP$rptUnitJTD(@strPlanID,@dtETCDate,NULL,@ReportAtBillingInBillingCurr) uJTD
            join rpUnit rpU on rpU.UnitID = uJTD.UnitID
            GROUP BY rpU.Account, rpU.UnitTable, rpU.Unit

            if @rpIncNBunitAcct='N' and @rpIncUAtaskUnit='Y'
              begin
                insert #rptUnitJTD
                select planID=@strPlanID,
				Account='',
	       Unit='',
                    UnitTable='',
           MAX(case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
           COUNT( distinct case when @strBudgetToUse='B' then JTDBillCurrencyCode else JTDCostCurrencyCode end),
	       SUM(case when @strBudgetToUse='B' then PeriodBill else PeriodCost end) AS JTDHrs
                from dbo.RP$rptTaskJTDUnit(@strPlanID,'N',@dtETCDate,@ReportAtBillingInBillingCurr) utJTD  	
                join rpTask rpT on rpT.planID = utJTD.planID and rpT.taskID=utJTD.taskID
                where rpT.childrenCount=0 and rpT.wbsType is not null and not exists (select 'X' from rpUnit rpU where rpU.planID=rpT.planID and rpU.taskID=rpT.taskID)
              end
          end --if else @rpIncNBunitAcct='Y' 
       end -- if @strUnitTab = 'Y'

       insert #rptPlanRevJTD
       select @strPlanID,
       MAX(JTDRevenueCurrencyCode),
       COUNT ( distinct JTDRevenueCurrencyCode),
	   SUM(ActualRevenue) as ActualRevenue
       from dbo.RP$rptPlanJTDRevenue(@strPlanID,@dtETCDate)     

       FETCH from planInfo INTO @strPlanID, @sintGRMethod, @strBudgetToUse, @decOverheadPct

       end  --while @@FETCH_STATUS = 0 end    
     CLOSE planInfo
     DEALLOCATE planInfo  
  END --open planInfo

END -- procedure rpRptPerformance end
GO
