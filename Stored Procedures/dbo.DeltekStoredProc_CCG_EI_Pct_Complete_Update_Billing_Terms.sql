SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Pct_Complete_Update_Billing_Terms] ( @wbs1 nvarchar(32), @wbs2 nvarchar(7), @wbs3 nvarchar(7), @KEY_EMPLOYEEID nvarchar(30), @oldFeeMethod varchar(50), @newFeeMethod varchar(5), @changeFeeMethod bit, @oldFeeBasis varchar(1), @newFeeBasis varchar(1), @changeFeeBasis bit, @oldFeeFactor1 varchar(50), @newFeeFactor1 varchar(50), @changeFeeFactor1 bit, @oldFeeFactor2 varchar(50), @newFeeFactor2 varchar(50), @changeFeeFactor2 bit, @oldFeeLabel varchar(15), @newFeeLabel varchar(15), @changeFeeLabel bit)
             AS EXEC spCCG_EI_Pct_Complete_Update_Billing_Terms @wbs1,@wbs2,@wbs3,@KEY_EMPLOYEEID,@oldFeeMethod,@newFeeMethod,@changeFeeMethod,@oldFeeBasis,@newFeeBasis,@changeFeeBasis,@oldFeeFactor1,@newFeeFactor1,@changeFeeFactor1,@oldFeeFactor2,@newFeeFactor2,@changeFeeFactor2,@oldFeeLabel,@newFeeLabel,@changeFeeLabel
GO
