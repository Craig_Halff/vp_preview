SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPShiftDates_RevForecast]
  @strRowID nvarchar(255),
  @strFromStartDate varchar(8),
  @strFromEndDate varchar(8),
  @strToStartDate varchar(8),
  @strToEndDate varchar(8),
  @intShiftMode int, /* 1 = Maintain Pattern, 2 = Distribute Evenly, 3 = No Redistribute */
  @bitShiftAll bit, /* This flag is applicable only for ShiftMode = {1, 3}. 0 = Shift using the "From" range, 1 = Shift the entire Assignment Row */
  @bitFixStartDate bit, /* This flag is applicable only for ShiftMode = {1, 3}. 1 = Hold "To" StartDate fixed, 0 = Hold "To" EndDate fixed */
  @bitCalledFromRM bit = 1,
  @bitMatchChildrenDates bit = 0 /* When 1, makes all children dates match new parent dates. */
AS

BEGIN -- Procedure stRPShiftDates

  SET NOCOUNT ON
  
DECLARE @dtFromStartDate datetime
  DECLARE @dtFromEndDate datetime

  DECLARE @dtToStartDate datetime
  DECLARE @dtToEndDate datetime

   
  DECLARE @strCompany nvarchar(14)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strAT_TaskID varchar(32)
  DECLARE @strAssignmentID varchar(32)
  DECLARE @strResourceID nvarchar(20)
  DECLARE @strGenericResourceID nvarchar(20)
  DECLARE @strResourceType varchar(1)
  DECLARE @strIDPrefix nvarchar(255)
  DECLARE @strUserName nvarchar(32)
  DECLARE @strCalcID varchar(34)

  DECLARE @decSumHrs decimal(19,4) = 0

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  --SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

  --Set PlanID and TaskID From the RowID

   SELECT
    @strPlanID = PlanID,
    @strTaskID = TaskID
    FROM dbo.stRP$tabParseRowID(@strRowID)

	SET @bitCalledFromRM = 1
  -- Set Dates

  SET @dtFromStartDate = 
    CASE 
      WHEN DATALENGTH(@strFromStartDate) = 0 
      THEN NULL
      ELSE CONVERT(datetime, @strFromStartDate)
    END

  SET @dtFromEndDate = 
    CASE
      WHEN DATALENGTH(@strFromEndDate) = 0
      THEN NULL
      ELSE CONVERT(datetime, @strFromEndDate)
    END

  SET @dtToStartDate = 
    CASE
      WHEN DATALENGTH(@strToStartDate) = 0
      THEN NULL
      ELSE CONVERT(datetime, @strToStartDate)
    END

  SET @dtToEndDate = 
    CASE
    WHEN DATALENGTH(@strToEndDate) = 0
    THEN NULL
    ELSE CONVERT(datetime, @strToEndDate)
  END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. E~<PNAssignment.ResourceID>|<PNTask.TaskID>
  --   2. G~<PNAssignment.GenericResourceID>|<PNTask.TaskID>

  SET @strIDPrefix = LEFT(@strRowID, CHARINDEX('|', @strRowID) - 1)
  SET @strResourceType = SUBSTRING(@strIDPrefix, 1, 1)

  IF (@strResourceType = 'E')
    BEGIN
      SET @strResourceID = REPLACE(@strIDPrefix, 'E~', '')
      SET @strGenericResourceID = NULL
    END
  ELSE IF (@strResourceType = 'G')
    BEGIN
      SET @strGenericResourceID = REPLACE(@strIDPrefix, 'G~', '')
      SET @strResourceID = NULL
    END
  ELSE
    BEGIN
      SET @strResourceID = NULL
      SET @strGenericResourceID = NULL
    END

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@intShiftMode = 1) /* 1 = Maintain Pattern */
    BEGIN

      EXEC dbo.stRPShiftTask_RevForecast
        @strRowID,
        @strFromStartDate,
        @strFromEndDate,
        @strToStartDate,
        @strToEndDate,
        @bitShiftAll, 
        @bitFixStartDate 

    END /* END IF (@intShiftMode = 1) */
    
  ELSE IF (@intShiftMode IN (2, 3)) /* --> (@intShiftMode = 2 or 3) */
    BEGIN

      EXEC dbo.stRPChangeDuration_RevForecast
        @strRowID,
        @strToStartDate,
        @strToEndDate,
        @intShiftMode,
        @bitMatchChildrenDates

    END /* END ELSE IF (@intShiftMode = 3) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    -- If this SP was called from Resource Management then need to update RP tables.
    -- A shift of dates from Resource Management will affect Expense and Consultant data as well as Assignment data.

    IF (@bitCalledFromRM = 1)
      BEGIN

        -- The most efficient way is to delete existing TPD in RPPlannedLabor for the given Plan and reinsert from PNPlannedLabor.

        DELETE RPPlannedRevenueLabor WHERE PlanID = @strPlanID 

        INSERT RPPlannedRevenueLabor(
          TimePhaseID,
          PlanID, 
          TaskID,
          StartDate, 
          EndDate, 
		  PeriodCost,
		  PeriodBill,
          CreateUser,
          ModUser,
          CreateDate,
          ModDate
        )
           SELECT
            TPD.TimePhaseID AS TimePhaseID,
            TPD.PlanID AS PlanID, 
            TPD.TaskID AS TaskID,
            TPD.StartDate AS StartDate, 
            TPD.EndDate AS EndDate, 
            TPD.PeriodCost AS PeriodCost,
            TPD.PeriodBill AS PeriodBill,
            CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stRPShiftDates' ELSE 'SHD_' + @strUserName END AS CreateUser,
            CASE WHEN DATALENGTH(@strUserName) = 0 THEN 'stRPShiftDates' ELSE 'SHD_' + @strUserName END AS ModUser,
            LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As CreateDate,
            LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19) As ModDate 
          FROM PNPlannedRevenueLabor AS TPD
          WHERE TPD.PlanID = @strPlanID

         UPDATE RT SET
          RevenueStartDate = CASE WHEN RT.RevenueStartDate <> PT.RevenueStartDate THEN PT.RevenueStartDate ELSE RT.RevenueStartDate END, 
          RevenueEndDate = CASE WHEN RT.RevenueEndDate <> PT.RevenueEndDate THEN PT.RevenueEndDate ELSE RT.RevenueEndDate END
          FROM RPTask AS RT
            INNER JOIN PNTask AS PT
              ON RT.PlanID = PT.PlanID AND RT.TaskID = PT.TaskID 
          WHERE RT.PlanID = @strPlanID 
            AND (RT.RevenueStartDate <> PT.RevenueStartDate OR RT.RevenueEndDate <> PT.RevenueEndDate)

      END /* END IF (@bitCalledFromRM = 1) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  -- Need to extend date for upper level task
  SET @strCalcID = 'P~' + @strPlanID
  EXECUTE dbo.stRPExtendDateSpan_RevForecast @strPlanID, @bitCalledFromRM

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Update VesionID.

  EXECUTE dbo.stRPUpdateVersionID @strPlanID, @bitCalledFromRM 

  -- Set LastPlanAction.

  --For the current version 3.5, we always set the PN from Plan Hub, not possible from RM, therefore need to update Last Action all the time.
    UPDATE PNPlan SET LastPlanAction = 'SAVED' WHERE PlanID = @strPlanID

  --IF (@bitCalledFromRM = 0)
  --  BEGIN
  --    UPDATE PNPlan SET LastPlanAction = 'SAVED' WHERE PlanID = @strPlanID
  --  END /* END IF (@bitCalledFromRM = 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     
  SET NOCOUNT OFF

END -- stRPShiftDates_RevForecast
GO
