SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_ProjectLocatorValidation] @WBS1 VARCHAR(30)
/*
Copyright (c) 2019 Halff Associates Inc. All rights reserved.
11/05/2019 Craig H. Anderson
           Validate that Project Locator has been run.
*/
AS
    BEGIN
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
        SET NOCOUNT ON;

        IF NOT EXISTS (SELECT 1 FROM dbo.HAI_PrjLocator AS l WHERE l.WBS1 = RIGHT(@WBS1, LEN(@WBS1) - 1))
            RAISERROR('Define physical location of the project.                                                                                                      ', 16, 1);
    END;
GO
