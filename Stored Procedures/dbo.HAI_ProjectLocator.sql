SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[HAI_ProjectLocator] @WBS1 VARCHAR(10)
AS
    /*
Copyright (c) 2019 Halff Associates Inc. All rights reserved.
10/03/2019 Joseph Sagel
           Populate Address information into Vision from HAI_PrjLocator

1/20/2020 J Gray
	NOTE:	The 'CFGStatesDescriptions' additions assume that all the possible domain inputs of 'HAI_PrjLocator.State' exist in the table; otherwise, the enty is omitted.
			If this is not the intended behavior, this should be replaced with a form of a LEFT JOIN instead.
9/29/2021	Craig H. Anderson
			Wrapped in an IF EXIST to save time/writes.
*/
    BEGIN
        SET NOCOUNT ON;

        --	Update Project Records
        IF EXISTS (SELECT 1 FROM dbo.HAI_PrjLocator WHERE WBS1 = RIGHT(@WBS1, LEN(@WBS1) - 1))
            BEGIN

                -- Update Project Records
                UPDATE p
                SET p.Address1 = l.Address
                  , p.City = l.City
                  , p.State = d.Code
                  , p.Zip = l.Zip
                  , p.County = l.County
                  , p.Country = l.Country
                FROM dbo.PR                                   p
                    LEFT OUTER JOIN dbo.HAI_PrjLocator        l
                        ON RIGHT(p.WBS1, LEN(p.WBS1) - 1) = l.WBS1
                    LEFT OUTER JOIN dbo.CFGStatesDescriptions d WITH (NOLOCK)
                        ON l.State = d.Description
                WHERE
                    l.WBS1 = RIGHT(p.WBS1, LEN(@WBS1) - 1)
                    AND (
                        p.WBS2 = ' '
                            OR p.WBS2 = ''
                    );


                -- Delete record from Project Locator Table
                DELETE FROM dbo.HAI_PrjLocator WHERE WBS1 = RIGHT(@WBS1, LEN(@WBS1) - 1);
            END;
    END;
GO
