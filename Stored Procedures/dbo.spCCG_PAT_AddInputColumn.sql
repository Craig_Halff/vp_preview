SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_PAT_AddInputColumn](
	@Header			varchar(255),
	@FieldName		varchar(255),
	@FieldType		varchar(255),
	@NullString		varchar(20),
	@ColType		char(1),
	@ColSeq			int,
	@ColDecimals	int,
	@ColWidth		int = 0,
	@ColEnabledWBS1	char(1)='Y',
	@ColEnabledWBS2	char(1)='Y',
	@ColEnabledWBS3	char(1)='Y',
	@ColStatus		char(1)='Y',
	@ColViewRoles			varchar(255)='',
	@ColEditRoles			varchar(255)='',
	@ColRequiredRoles		varchar(255)='',
	@ColRequiredExpression	varchar(255)='')
AS BEGIN
/*
	Copyright (c) 2011 Central Consulting Group.  All rights reserved.
	
	select * from CCG_PAT_ConfigCustomColumns
	select * from CCG_PAT_CustomColumns

	Examples:
	exec spCCG_PAT_AddInputColumn 'Fee % Compl', 'FeePctComplete', 'decimal(19,5)', 'NULL', 'N', 100, 0, 0, 'Y', 'Y', 'N'
	exec spCCG_PAT_AddInputColumn 'Fee Billing', 'FeeBilling', 'decimal(19,5)', 'NULL', 'N', 100, 2, 0
*/
	set nocount on
	declare @sql	varchar(max)
	declare @debug	char(1)
	set @debug='N'
	
	print @Header
	-- Add column into CCG_PAT_CustomColumns
	set @sql='if not exists (select ''x'' from syscolumns a,sysobjects b where a.id=b.id and a.name=''' + @FieldName + ''' and b.name=''CCG_PAT_CustomColumns'') ' + CHAR(13) + CHAR(10) +
		' begin' + CHAR(13) + CHAR(10) +
		'	print ''	Adding column ' + @FieldName + ' to CCG_PAT_CustomColumns'' ' + CHAR(13) + CHAR(10) +
		'	alter table CCG_PAT_CustomColumns with nocheck add ' + @FieldName + ' ' + @FieldType + ' ' + @NullString + CHAR(13) + CHAR(10) +
		' end'
	if @debug='Y' print @sql
	execute(@sql)
	
	set @sql='if not exists (select ''x'' from syscolumns a,sysobjects b where a.id=b.id and a.name=''' + @FieldName + '_ModEmp'' and b.name=''CCG_PAT_CustomColumns'') ' + CHAR(13) + CHAR(10) +
		' begin' + CHAR(13) + CHAR(10) +
		'	print ''	Adding column ' + @FieldName + '_ModEmp to CCG_PAT_CustomColumns'' ' + CHAR(13) + CHAR(10) +
		'	alter table CCG_PAT_CustomColumns with nocheck add ' + @FieldName + '_ModEmp varchar(20) ' + @NullString + CHAR(13) + CHAR(10) +
		' end'
	if @debug='Y' print @sql
	execute(@sql)
	
	--select * from CCG_PAT_ConfigCustomColumns
	-- Add column into EI configuration
	set @sql='if not exists (select ''x'' from CCG_PAT_ConfigCustomColumns where DatabaseField=''' + @FieldName + ''')' + CHAR(13) + CHAR(10) +
		' begin' + CHAR(13) + CHAR(10) +
		'	print ''	Adding configuration row for ' + @FieldName + '''' + CHAR(13) + CHAR(10) +
		'	insert into CCG_PAT_ConfigCustomColumns (Label, DataType, DisplayOrder, InitialValue,' + CHAR(13) + CHAR(10) +
		'		DatabaseField, ColumnUpdate, Width, Decimals,' + CHAR(13) + CHAR(10) +
		'		ViewRoles, EditRoles, RequiredRoles, RequiredExpression,' + CHAR(13) + CHAR(10) +
		'		EnabledWBS1, EnabledWBS2, EnabledWBS3, Status, ModDate, ModUser)' + CHAR(13) + CHAR(10) +
		'	values (''' + @Header + ''', ''' + @ColType + ''', ' + Cast(@ColSeq as varchar(10)) + ', ''dbo.fnCCG_PAT_' + @FieldName + ''',' + CHAR(13) + CHAR(10) +
		'		''' + @FieldName + ''', ''spCCG_PAT_' + @FieldName  + ''', ' + Cast(@ColWidth as varchar(10)) + ', ' + + Cast(@ColDecimals as varchar(10)) + + ', ' + CHAR(13) + CHAR(10) +
		'		''' + @ColViewRoles   + ''', ''' + @ColEditRoles   + ''', ''' + @ColRequiredRoles + ''', ''' + @ColRequiredExpression + ''',   ' + CHAR(13) + CHAR(10) +
		'		''' + @ColEnabledWBS1 + ''', ''' + @ColEnabledWBS2 + ''', ''' + @ColEnabledWBS3   + ''', ''' + @ColStatus             + ''',   ' + CHAR(13) + CHAR(10) +
		'		GETDATE(),''CCG'')' + CHAR(13) + CHAR(10) +
		' end'
	if @debug='Y' print @sql
	execute(@sql)
END
GO
