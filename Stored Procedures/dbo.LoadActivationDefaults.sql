SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[LoadActivationDefaults]
	@DefaultOrg				nvarchar(30)
AS
DECLARE
    @WBS1Length                smallint,
    @WBS1LeadZeros             varchar(1),
    @WBS1Delimiter1            nvarchar(1),
    @WBS1Delimiter1Position    smallint,
    @WBS1Delimiter2            nvarchar(1),
    @WBS1Delimiter2Position    smallint,
    @WBS2Length                smallint,
    @WBS2LeadZeros             varchar(1),
    @WBS2Delimiter             nvarchar(1),
    @WBS2DelimiterPosition     smallint,
    @WBS1                      nvarchar(30),
    @WBS2                      nvarchar(30),
    @WBS3                      nvarchar(30),
    @Org                       nvarchar(30),
    @Cnt                       int,
    @CntStr                    varchar(10),
    @SqlStr                    varchar(max)

BEGIN
    set nocount on

    select 
        @WBS1Length = WBS1Length,
        @WBS1LeadZeros = WBS1LeadZeros,
        @WBS1Delimiter1 = WBS1Delimiter1,
        @WBS1Delimiter1Position = WBS1Delimiter1Position,
        @WBS1Delimiter2 = WBS1Delimiter2,
        @WBS1Delimiter2Position = WBS1Delimiter2Position,
        @WBS2Length = WBS2Length,
        @WBS2LeadZeros = WBS2LeadZeros,
        @WBS2Delimiter = WBS2Delimiter,
        @WBS2DelimiterPosition = WBS2DelimiterPosition
    from CFGFormat

    if exists (select 'x' from CFGVisionSystem where ActivationComplete = 'N')
    begin
        print ' '
        print 'Starting Activation'

        -- if Orgs are enabled then add WBS rows and set Category queries
        if @DefaultOrg <> ''  
        begin
            print ' '
            print 'Adding WBS rows for each Organization'

            -- remove the WBS1 delimiter
            update PR set WBS1 = replace(WBS1,@WBS1Delimiter1,'')

            -- create PR temp table to build the new WBS rows
            select * into #PR from PR

            -- If WBS2 Length is 0 then add Projects at the WBS1 level only
            if @WBS2Length = 0
            begin
                -- set counter 
                set @Cnt = 0

                declare OrgCursor cursor for select Org from Organization where Org <> @DefaultOrg order by Org
                open OrgCursor
                fetch OrgCursor into @Org
                while @@fetch_Status = 0
                begin
                    set @Cnt = @Cnt + 1
            
                    -- if Org combinations exceeds 99 then start with AA   
                    if @Cnt between 100 and 125 set @CntStr = char(65) + char(@Cnt - 35) -- A
                    else if @Cnt between 126 and 151 set @CntStr = char(66) + char(@Cnt - 61)
                    else if @Cnt between 152 and 177 set @CntStr = char(67) + char(@Cnt - 87)
                    else if @Cnt between 178 and 203 set @CntStr = char(68) + char(@Cnt - 113)
                    else if @Cnt between 204 and 229 set @CntStr = char(69) + char(@Cnt - 139)
                    else if @Cnt between 230 and 255 set @CntStr = char(70) + char(@Cnt - 165)
                    else if @Cnt between 256 and 281 set @CntStr = char(71) + char(@Cnt - 191)
                    else if @Cnt between 282 and 307 set @CntStr = char(72) + char(@Cnt - 217)
                    else if @Cnt between 308 and 333 set @CntStr = char(73) + char(@Cnt - 243)
                    else if @Cnt between 334 and 359 set @CntStr = char(74) + char(@Cnt - 269)
                    else if @Cnt between 360 and 385 set @CntStr = char(75) + char(@Cnt - 295)
                    else if @Cnt between 386 and 411 set @CntStr = char(76) + char(@Cnt - 321)
                    else if @Cnt between 412 and 437 set @CntStr = char(77) + char(@Cnt - 347)
                    else if @Cnt between 438 and 463 set @CntStr = char(78) + char(@Cnt - 373)
                    else if @Cnt between 464 and 489 set @CntStr = char(79) + char(@Cnt - 399)
                    else if @Cnt between 490 and 515 set @CntStr = char(80) + char(@Cnt - 425)
                    else if @Cnt between 516 and 541 set @CntStr = char(81) + char(@Cnt - 451)
                    else if @Cnt between 542 and 567 set @CntStr = char(82) + char(@Cnt - 477)
                    else if @Cnt between 568 and 593 set @CntStr = char(83) + char(@Cnt - 503)
                    else if @Cnt between 594 and 619 set @CntStr = char(84) + char(@Cnt - 529)
                    else if @Cnt between 620 and 645 set @CntStr = char(85) + char(@Cnt - 555)
                    else if @Cnt between 646 and 671 set @CntStr = char(86) + char(@Cnt - 581)
                    else if @Cnt between 672 and 697 set @CntStr = char(87) + char(@Cnt - 607)
                    else if @Cnt between 698 and 723 set @CntStr = char(88) + char(@Cnt - 633)
                    else if @Cnt between 724 and 749 set @CntStr = char(89) + char(@Cnt - 659)
                    else if @Cnt between 750 and 775 set @CntStr = char(90) + char(@Cnt - 685) -- Z
                    else set @CntStr = @Cnt

                    -- update WBS1 and insert back into PR
                    if @Cnt <= 775
                    begin
                        update #PR set WBS1 = left(WBS1,len(WBS1)-2) + right('00'+ @CntStr,2), Org = @Org
                        insert into PR select * from #PR 
                    end

                    fetch OrgCursor into @Org
                end

                close OrgCursor
                deallocate OrgCursor

                -- update PR Name and LongName to include Org Name
                update PR set PR.Name = left(PR.Name + ' - ' + Organization.Name,40), PR.LongName = left(PR.LongName + ' - ' + Organization.Name,255) 
                from PR, Organization where PR.Org = Organization.Org
            end

            -- If WBS2 Length is greater than 0 then add rows at the WBS2 level 
            if @WBS2Length > 0
            begin
                -- set counter to -1 so the first WBS2 will be 0
                set @Cnt = -1

                declare OrgCursor cursor for select Org from Organization order by Org
                open OrgCursor
                fetch OrgCursor into @Org
                while @@fetch_Status = 0
                begin
                    set @Cnt = @Cnt + 1
    
                    -- if WBS2 length = 1 and Org combinations exceeds 9 start with A and limit to 36 total 
                    if @WBS2Length = 1
                    begin
                        if @Cnt between 10 and 35 set @CntStr = char(@Cnt + 55)
                        else set @CntStr = @Cnt

                        -- update WBS2 and insert back into PR
                        if @Cnt <= 35
                        begin
                            update #PR set WBS2 = right('0000000000'+ @CntStr,@WBS2Length), Org = @Org
                            insert into PR select * from #PR 
                        end
                    end

                    -- if WBS2 length = 2 and Org combinations exceeds 99 start with AA  
                    if @WBS2Length = 2
                    begin
                        if @Cnt between 100 and 125 set @CntStr = char(65) + char(@Cnt - 35) -- A
                        else if @Cnt between 126 and 151 set @CntStr = char(66) + char(@Cnt - 61)
                        else if @Cnt between 152 and 177 set @CntStr = char(67) + char(@Cnt - 87)
                        else if @Cnt between 178 and 203 set @CntStr = char(68) + char(@Cnt - 113)
                        else if @Cnt between 204 and 229 set @CntStr = char(69) + char(@Cnt - 139)
                        else if @Cnt between 230 and 255 set @CntStr = char(70) + char(@Cnt - 165)
                        else if @Cnt between 256 and 281 set @CntStr = char(71) + char(@Cnt - 191)
                        else if @Cnt between 282 and 307 set @CntStr = char(72) + char(@Cnt - 217)
                        else if @Cnt between 308 and 333 set @CntStr = char(73) + char(@Cnt - 243)
                        else if @Cnt between 334 and 359 set @CntStr = char(74) + char(@Cnt - 269)
                        else if @Cnt between 360 and 385 set @CntStr = char(75) + char(@Cnt - 295)
                        else if @Cnt between 386 and 411 set @CntStr = char(76) + char(@Cnt - 321)
                        else if @Cnt between 412 and 437 set @CntStr = char(77) + char(@Cnt - 347)
                        else if @Cnt between 438 and 463 set @CntStr = char(78) + char(@Cnt - 373)
                        else if @Cnt between 464 and 489 set @CntStr = char(79) + char(@Cnt - 399)
                        else if @Cnt between 490 and 515 set @CntStr = char(80) + char(@Cnt - 425)
                        else if @Cnt between 516 and 541 set @CntStr = char(81) + char(@Cnt - 451)
                        else if @Cnt between 542 and 567 set @CntStr = char(82) + char(@Cnt - 477)
                        else if @Cnt between 568 and 593 set @CntStr = char(83) + char(@Cnt - 503)
                        else if @Cnt between 594 and 619 set @CntStr = char(84) + char(@Cnt - 529)
                        else if @Cnt between 620 and 645 set @CntStr = char(85) + char(@Cnt - 555)
                        else if @Cnt between 646 and 671 set @CntStr = char(86) + char(@Cnt - 581)
                        else if @Cnt between 672 and 697 set @CntStr = char(87) + char(@Cnt - 607)
                        else if @Cnt between 698 and 723 set @CntStr = char(88) + char(@Cnt - 633)
                        else if @Cnt between 724 and 749 set @CntStr = char(89) + char(@Cnt - 659)
                        else if @Cnt between 750 and 775 set @CntStr = char(90) + char(@Cnt - 685) -- Z
                        else set @CntStr = @Cnt

                        -- update WBS2 and insert back into PR
                        if @Cnt <= 775
                        begin
                            update #PR set WBS2 = right('0000000000'+ @CntStr,@WBS2Length), Org = @Org
                            insert into PR select * from #PR
                        end
                    end

                    -- if WBS2 length = 3 and Org combinations exceeds 999 start with AAA
                    if @WBS2Length = 3
                    begin
                        if @cnt between 1000 and 1025 set @CntStr = char(65) + char(65) + char(@Cnt - 935) -- AA
                        else if @Cnt between 1026 and 1051 set @CntStr = char(65) + char(66) + char(@Cnt - 961)
                        else if @Cnt between 1052 and 1077 set @CntStr = char(65) + char(67) + char(@Cnt - 987)
                        else if @Cnt between 1078 and 1103 set @CntStr = char(65) + char(68) + char(@Cnt - 1013)
                        else if @Cnt between 1104 and 1129 set @CntStr = char(65) + char(69) + char(@Cnt - 1039)
                        else if @Cnt between 1130 and 1155 set @CntStr = char(65) + char(70) + char(@Cnt - 1065)
                        else if @Cnt between 1156 and 1181 set @CntStr = char(65) + char(71) + char(@Cnt - 1091)
                        else if @Cnt between 1182 and 1207 set @CntStr = char(65) + char(72) + char(@Cnt - 1117)
                        else if @Cnt between 1208 and 1233 set @CntStr = char(65) + char(73) + char(@Cnt - 1143)
                        else if @Cnt between 1234 and 1259 set @CntStr = char(65) + char(74) + char(@Cnt - 1169)
                        else if @Cnt between 1260 and 1285 set @CntStr = char(65) + char(75) + char(@Cnt - 1195)
                        else if @Cnt between 1286 and 1311 set @CntStr = char(65) + char(76) + char(@Cnt - 1221)
                        else if @Cnt between 1312 and 1337 set @CntStr = char(65) + char(77) + char(@Cnt - 1247)
                        else if @Cnt between 1338 and 1363 set @CntStr = char(65) + char(78) + char(@Cnt - 1273)
                        else if @Cnt between 1364 and 1389 set @CntStr = char(65) + char(79) + char(@Cnt - 1299)
                        else if @Cnt between 1390 and 1415 set @CntStr = char(65) + char(80) + char(@Cnt - 1325)
                        else if @Cnt between 1416 and 1441 set @CntStr = char(65) + char(81) + char(@Cnt - 1351)
                        else if @Cnt between 1442 and 1467 set @CntStr = char(65) + char(82) + char(@Cnt - 1377)
                        else if @Cnt between 1468 and 1493 set @CntStr = char(65) + char(83) + char(@Cnt - 1403)
                        else if @Cnt between 1494 and 1519 set @CntStr = char(65) + char(84) + char(@Cnt - 1429)
                        else if @Cnt between 1520 and 1545 set @CntStr = char(65) + char(85) + char(@Cnt - 1455)
                        else if @Cnt between 1546 and 1571 set @CntStr = char(65) + char(86) + char(@Cnt - 1481)
                        else if @Cnt between 1572 and 1597 set @CntStr = char(65) + char(87) + char(@Cnt - 1507)
                        else if @Cnt between 1598 and 1623 set @CntStr = char(65) + char(88) + char(@Cnt - 1533)
                        else if @Cnt between 1624 and 1649 set @CntStr = char(65) + char(89) + char(@Cnt - 1559)
                        else if @Cnt between 1650 and 1675 set @CntStr = char(65) + char(90) + char(@Cnt - 1585) -- AZ
                        else if @Cnt between 1676 and 1701 set @CntStr = char(66) + char(65) + char(@Cnt - 1611) -- BA
                        else if @Cnt between 1702 and 1727 set @CntStr = char(66) + char(66) + char(@Cnt - 1637)
                        else if @Cnt between 1728 and 1753 set @CntStr = char(66) + char(67) + char(@Cnt - 1663)
                        else if @Cnt between 1754 and 1779 set @CntStr = char(66) + char(68) + char(@Cnt - 1689)
                        else if @Cnt between 1780 and 1805 set @CntStr = char(66) + char(69) + char(@Cnt - 1715)
                        else if @Cnt between 1806 and 1831 set @CntStr = char(66) + char(70) + char(@Cnt - 1741)
                        else if @Cnt between 1832 and 1857 set @CntStr = char(66) + char(71) + char(@Cnt - 1767)
                        else if @Cnt between 1858 and 1883 set @CntStr = char(66) + char(72) + char(@Cnt - 1793)
                        else if @Cnt between 1884 and 1909 set @CntStr = char(66) + char(73) + char(@Cnt - 1819)
                        else if @Cnt between 1910 and 1935 set @CntStr = char(66) + char(74) + char(@Cnt - 1845)
                        else if @Cnt between 1936 and 1961 set @CntStr = char(66) + char(75) + char(@Cnt - 1871)
                        else if @Cnt between 1962 and 1987 set @CntStr = char(66) + char(76) + char(@Cnt - 1897)
                        else if @Cnt between 1988 and 2013 set @CntStr = char(66) + char(77) + char(@Cnt - 1923)
                        else if @Cnt between 2014 and 2039 set @CntStr = char(66) + char(78) + char(@Cnt - 1949)
                        else if @Cnt between 2040 and 2065 set @CntStr = char(66) + char(79) + char(@Cnt - 1975)
                        else if @Cnt between 2066 and 2091 set @CntStr = char(66) + char(80) + char(@Cnt - 2001)
                        else if @Cnt between 2092 and 2117 set @CntStr = char(66) + char(81) + char(@Cnt - 2027)
                        else if @Cnt between 2118 and 2143 set @CntStr = char(66) + char(82) + char(@Cnt - 2053)
                        else if @Cnt between 2144 and 2169 set @CntStr = char(66) + char(83) + char(@Cnt - 2079)
                        else if @Cnt between 2170 and 2195 set @CntStr = char(66) + char(84) + char(@Cnt - 2105)
                        else if @Cnt between 2196 and 2221 set @CntStr = char(66) + char(85) + char(@Cnt - 2131)
                        else if @Cnt between 2222 and 2247 set @CntStr = char(66) + char(86) + char(@Cnt - 2157)
                        else if @Cnt between 2248 and 2273 set @CntStr = char(66) + char(87) + char(@Cnt - 2183)
                        else if @Cnt between 2274 and 2299 set @CntStr = char(66) + char(88) + char(@Cnt - 2209)
                        else if @Cnt between 2300 and 2325 set @CntStr = char(66) + char(89) + char(@Cnt - 2235)
                        else if @Cnt between 2326 and 2351 set @CntStr = char(66) + char(90) + char(@Cnt - 2261) -- BZ
                        else if @Cnt between 2352 and 2377 set @CntStr = char(67) + char(65) + char(@Cnt - 2287) -- CA
                        else if @Cnt between 2378 and 2403 set @CntStr = char(67) + char(66) + char(@Cnt - 2313)
                        else if @Cnt between 2404 and 2429 set @CntStr = char(67) + char(67) + char(@Cnt - 2339)
                        else if @Cnt between 2430 and 2455 set @CntStr = char(67) + char(68) + char(@Cnt - 2365)
                        else if @Cnt between 2456 and 2481 set @CntStr = char(67) + char(69) + char(@Cnt - 2391)
                        else if @Cnt between 2482 and 2507 set @CntStr = char(67) + char(70) + char(@Cnt - 2417)
                        else if @Cnt between 2508 and 2533 set @CntStr = char(67) + char(71) + char(@Cnt - 2443)
                        else if @Cnt between 2534 and 2559 set @CntStr = char(67) + char(72) + char(@Cnt - 2469)
                        else if @Cnt between 2560 and 2585 set @CntStr = char(67) + char(73) + char(@Cnt - 2495)
                        else if @Cnt between 2586 and 2611 set @CntStr = char(67) + char(74) + char(@Cnt - 2521)
                        else if @Cnt between 2612 and 2637 set @CntStr = char(67) + char(75) + char(@Cnt - 2547)
                        else if @Cnt between 2638 and 2663 set @CntStr = char(67) + char(76) + char(@Cnt - 2573)
                        else if @Cnt between 2664 and 2689 set @CntStr = char(67) + char(77) + char(@Cnt - 2599)
                        else if @Cnt between 2690 and 2715 set @CntStr = char(67) + char(78) + char(@Cnt - 2625)
                        else if @Cnt between 2716 and 2741 set @CntStr = char(67) + char(79) + char(@Cnt - 2651)
                        else if @Cnt between 2742 and 2767 set @CntStr = char(67) + char(80) + char(@Cnt - 2677)
                        else if @Cnt between 2768 and 2793 set @CntStr = char(67) + char(81) + char(@Cnt - 2703)
                        else if @Cnt between 2794 and 2819 set @CntStr = char(67) + char(82) + char(@Cnt - 2729)
                        else if @Cnt between 2820 and 2845 set @CntStr = char(67) + char(83) + char(@Cnt - 2755)
                        else if @Cnt between 2846 and 2871 set @CntStr = char(67) + char(84) + char(@Cnt - 2781)
                        else if @Cnt between 2872 and 2897 set @CntStr = char(67) + char(85) + char(@Cnt - 2807)
                        else if @Cnt between 2898 and 2923 set @CntStr = char(67) + char(86) + char(@Cnt - 2833)
                        else if @Cnt between 2924 and 2949 set @CntStr = char(67) + char(87) + char(@Cnt - 2859)
                        else if @Cnt between 2950 and 2975 set @CntStr = char(67) + char(88) + char(@Cnt - 2885)
                        else if @Cnt between 2976 and 3001 set @CntStr = char(67) + char(89) + char(@Cnt - 2911)
                        else if @Cnt between 3002 and 3027 set @CntStr = char(67) + char(90) + char(@Cnt - 2937) -- CZ
                        else set @CntStr = @cnt

                        -- update WBS2 and insert back into PR
                        if @Cnt <= 2351
                        begin
                            update #PR set WBS2 = right('0000000000'+ @CntStr,@WBS2Length), Org = @Org
                            insert into PR select * from #PR
                        end
                    end

                    if @WBS2Length > 3
                    begin
                        -- update WBS2 and insert back into PR
                        update #PR set WBS2 = right('0000000000'+ convert(varchar,@Cnt),@WBS2Length), Org = @Org
                        insert into PR select * from #PR 
                    end

                    fetch OrgCursor into @Org
                end

                close OrgCursor
                deallocate OrgCursor

                -- update PR Name and LongName to include Org Name
                update PR set PR.Name = left(PR.Name + ' - ' + Organization.Name,40), PR.LongName = left(PR.LongName + ' - ' + Organization.Name,255) 
                from PR, Organization where PR.Org = Organization.Org and PR.WBS2 <> ' '

                -- set the SubLevel flag on the WBS1 rows to Y
                update PR set SubLevel = 'Y' where WBS2 = ' ' and WBS3 = ' '

                -- update Configuration tables to relect WBS2 row where necessary
                update CFGAutoPosting set LaborCreditWBS2 = right('0000000000',@WBS2Length), MiscCreditWBS2 = right('0000000000',@WBS2Length), PrintsCreditWBS2 = right('0000000000',@WBS2Length), ConsAccrWBS2 = right('0000000000',@WBS2Length), EmplExpCreditWBS2 = right('0000000000',@WBS2Length), EmplAdvCreditWBS2 = right('0000000000',@WBS2Length)
                update CFGTKCategory set WBS2 = right('0000000000',@WBS2Length)

            end

            -- reset the WBS1 delimiter
            if @WBS1Delimiter1Position > 0 
            begin
                update PR set WBS1 = substring(WBS1,1,@WBS1Delimiter1Position-1) + @WBS1Delimiter1 + substring(WBS1,@WBS1Delimiter1Position,30)
            end

            -- update CFGTKCategory queries
            -- if WBS2Length = 0 set WBS1Query column
            if @WBS2Length = 0
            begin
                update CFGTKCategory set WBS1Query = 'Select WBS1 From PR WHERE PR.WBS2 = '' '' AND PR.WBS3 ='' '' AND PR.Name = ''Vacation'' AND PR.Org = (SELECT EMAllCompany.Org FROM EMAllCompany WHERE EMAllCompany.Employee = :emp AND EMAllCompany.EmployeeCompany=:activecompany)' where Category = 'VAC'
                update CFGTKCategory set WBS1Query = 'Select WBS1 From PR WHERE PR.WBS2 = '' '' AND PR.WBS3 ='' '' AND PR.Name = ''Sick Leave'' AND PR.Org = (SELECT EMAllCompany.Org FROM EMAllCompany WHERE EMAllCompany.Employee = :emp AND EMAllCompany.EmployeeCompany=:activecompany)' where Category = 'SIC'
                update CFGTKCategory set WBS1Query = 'Select WBS1 From PR WHERE PR.WBS2 = '' '' AND PR.WBS3 ='' '' AND PR.Name = ''Holiday'' AND PR.Org = (SELECT EMAllCompany.Org FROM EMAllCompany WHERE EMAllCompany.Employee = :emp AND EMAllCompany.EmployeeCompany=:activecompany)' where Category = 'HOL'
            end
            -- if WBS2Length > 0 set WBS2Query column
            if @WBS2Length > 0
            begin
                select @SqlStr = 'Select WBS2 From PR WHERE PR.WBS2 > '' '' AND PR.WBS3='' '' AND PR.WBS1 = ''' + WBS1 + ''' AND PR.Org = (SELECT EMAllCompany.Org FROM EMAllCompany WHERE EMAllCompany.Employee = :emp AND EMAllCompany.EmployeeCompany=:activecompany )' from PR where Name = 'Vacation'
                update CFGTKCategory set WBS2Query = @SqlStr where Category = 'VAC'
                select @SqlStr = 'Select WBS2 From PR WHERE PR.WBS2 > '' '' AND PR.WBS3='' '' AND PR.WBS1 = ''' + WBS1 + ''' AND PR.Org = (SELECT EMAllCompany.Org FROM EMAllCompany WHERE EMAllCompany.Employee = :emp AND EMAllCompany.EmployeeCompany=:activecompany )' from PR where Name = 'Sick Leave'
                update CFGTKCategory set WBS2Query = @SqlStr where Category = 'SIC'
                select @SqlStr = 'Select WBS2 From PR WHERE PR.WBS2 > '' '' AND PR.WBS3='' '' AND PR.WBS1 = ''' + WBS1 + ''' AND PR.Org = (SELECT EMAllCompany.Org FROM EMAllCompany WHERE EMAllCompany.Employee = :emp AND EMAllCompany.EmployeeCompany=:activecompany )' from PR where Name = 'Holiday'
                update CFGTKCategory set WBS2Query = @SqlStr where Category = 'HOL'
            end
            -- set BillCategoryQuery column for all TKCategories
            update CFGTKCategory set BillCategoryQuery = 'SELECT EMAllCompany.BillingCategory FROM EMAllCompany WHERE EMAllCompany.Employee = :emp AND EMAllCompany.EmployeeCompany=:activecompany'
        
        end

        -- If MultiCompany then add Chart of Account rows
        if exists (select 'x' from FW_CFGSystem where MulticompanyEnabled = 'Y')
        begin
            print ' '
            print 'Starting Multicompany Activation'
            print ' '
            print 'Adding Multicompany entries to the Chart of Accounts'
            insert CA (Account, Name, CashBasisAccount, FASAccount, Status, Detail, GlobalAccount, AccountCurrencyCode, CustomCurrencyCode, Type, UnrealizedLossAccount, UnrealizedGainAccount, CashBasisRevaluation) 
            select '115.00', 'Intercompany AR', NULL, NULL, 'A', 1, 'Y', NULL, (select FunctionalCurrencyCode from CFGMainData), 1, NULL, NULL, 'N' where not exists (select 'x' from CA where Account = '115.00')
            insert CA (Account, Name, CashBasisAccount, FASAccount, Status, Detail, GlobalAccount, AccountCurrencyCode, CustomCurrencyCode, Type, UnrealizedLossAccount, UnrealizedGainAccount, CashBasisRevaluation) 
            select '115.01', 'Intercompany Suspense', NULL, NULL, 'A', 1, 'Y', NULL, (select FunctionalCurrencyCode from CFGMainData), 1, NULL, NULL, 'N' where not exists (select 'x' from CA where Account = '115.01')
            insert CA (Account, Name, CashBasisAccount, FASAccount, Status, Detail, GlobalAccount, AccountCurrencyCode, CustomCurrencyCode, Type, UnrealizedLossAccount, UnrealizedGainAccount, CashBasisRevaluation) 
            select '214.00', 'Intercompany AP', NULL, NULL, 'A', 1, 'Y', NULL, (select FunctionalCurrencyCode from CFGMainData), 2, NULL, NULL, 'N' where not exists (select 'x' from CA where Account = '214.00')
            insert CA (Account, Name, CashBasisAccount, FASAccount, Status, Detail, GlobalAccount, AccountCurrencyCode, CustomCurrencyCode, Type, UnrealizedLossAccount, UnrealizedGainAccount, CashBasisRevaluation) 
            select '604.00', 'Foreign Employee Direct Labor', NULL, NULL, 'A', 1, 'Y', NULL, (select FunctionalCurrencyCode from CFGMainData), 8, NULL, NULL, 'N' where not exists (select 'x' from CA where Account = '604.00')
            insert CA (Account, Name, CashBasisAccount, FASAccount, Status, Detail, GlobalAccount, AccountCurrencyCode, CustomCurrencyCode, Type, UnrealizedLossAccount, UnrealizedGainAccount, CashBasisRevaluation) 
            select '704.00', 'Foreign Employee Indirect Labor', NULL, NULL, 'A', 1, 'Y', NULL, (select FunctionalCurrencyCode from CFGMainData), 9, NULL, NULL, 'N' where not exists (select 'x' from CA where Account = '704.00')

            print ' '
            print 'Setting CFGOrgSetup.OrgIntercompany to the Default Orginazation'
            update CFGOrgSetup set OrgIntercompany = @DefaultOrg

            print ' '
            print 'Setting CFGAutoPosting Intercompany and Foreign Employee Accounts'
            update CFGAutoPosting set AcctsReceivableIntercompany = '115.00', AcctsPayableIntercompany = '214.00', SuspenseIntercompany = '115.01', ForeignEMTypeDirectAccount = '604.00', ForeignEMTypeIndirectAccount = '704.00'

            print ' '
            print 'Multicompany Activation complete'
        end

        if exists (select 'x' from FW_CFGSystem where MulticurrencyEnabled = 'Y')
        begin
            print ' ' 
            print 'Starting Multicurrency Activation'
            print ' '
            print 'Adding Multicurrency entries to the Chart of Accounts'
            insert CA (Account, Name, CashBasisAccount, FASAccount, Status, Detail, GlobalAccount, AccountCurrencyCode, CustomCurrencyCode, Type, UnrealizedLossAccount, UnrealizedGainAccount, CashBasisRevaluation) 
            select '178.00', 'Unrealized Revaluation Gain', NULL, NULL, 'A', 1, 'Y', NULL, (select FunctionalCurrencyCode from CFGMainData), 1, NULL, NULL, 'N' where not exists (select 'x' from CA where Account = '178.00')
            insert CA (Account, Name, CashBasisAccount, FASAccount, Status, Detail, GlobalAccount, AccountCurrencyCode, CustomCurrencyCode, Type, UnrealizedLossAccount, UnrealizedGainAccount, CashBasisRevaluation) 
            select '178.01', 'Unbilled Unrealized Gain', NULL, NULL, 'A', 1, 'Y', NULL, (select FunctionalCurrencyCode from CFGMainData), 1, NULL, NULL, 'N' where not exists (select 'x' from CA where Account = '178.01')
            insert CA (Account, Name, CashBasisAccount, FASAccount, Status, Detail, GlobalAccount, AccountCurrencyCode, CustomCurrencyCode, Type, UnrealizedLossAccount, UnrealizedGainAccount, CashBasisRevaluation) 
            select '278.00', 'Unrealized Revaluation Loss', NULL, NULL, 'A', 1, 'Y', NULL, (select FunctionalCurrencyCode from CFGMainData), 2, NULL, NULL, 'N' where not exists (select 'x' from CA where Account = '278.00')
            insert CA (Account, Name, CashBasisAccount, FASAccount, Status, Detail, GlobalAccount, AccountCurrencyCode, CustomCurrencyCode, Type, UnrealizedLossAccount, UnrealizedGainAccount, CashBasisRevaluation) 
            select '278.01', 'Unbilled Unrealized Loss', NULL, NULL, 'A', 1, 'Y', NULL, (select FunctionalCurrencyCode from CFGMainData), 2, NULL, NULL, 'N' where not exists (select 'x' from CA where Account = '278.01')
            insert CA (Account, Name, CashBasisAccount, FASAccount, Status, Detail, GlobalAccount, AccountCurrencyCode, CustomCurrencyCode, Type, UnrealizedLossAccount, UnrealizedGainAccount, CashBasisRevaluation) 
            select '778.00', 'Realized Revaluation Gain', NULL, NULL, 'A', 1, 'Y', NULL, (select FunctionalCurrencyCode from CFGMainData), 9, NULL, NULL, 'N' where not exists (select 'x' from CA where Account = '778.00')
            insert CA (Account, Name, CashBasisAccount, FASAccount, Status, Detail, GlobalAccount, AccountCurrencyCode, CustomCurrencyCode, Type, UnrealizedLossAccount, UnrealizedGainAccount, CashBasisRevaluation) 
            select '778.01', 'Unbilled Realized Gain', NULL, NULL, 'A', 1, 'Y', NULL, (select FunctionalCurrencyCode from CFGMainData), 9, NULL, NULL, 'N' where not exists (select 'x' from CA where Account = '778.01')
            insert CA (Account, Name, CashBasisAccount, FASAccount, Status, Detail, GlobalAccount, AccountCurrencyCode, CustomCurrencyCode, Type, UnrealizedLossAccount, UnrealizedGainAccount, CashBasisRevaluation) 
            select '779.00', 'Realized Revaluation Loss', NULL, NULL, 'A', 1, 'Y', NULL, (select FunctionalCurrencyCode from CFGMainData), 9, NULL, NULL, 'N' where not exists (select 'x' from CA where Account = '779.00')
            insert CA (Account, Name, CashBasisAccount, FASAccount, Status, Detail, GlobalAccount, AccountCurrencyCode, CustomCurrencyCode, Type, UnrealizedLossAccount, UnrealizedGainAccount, CashBasisRevaluation) 
            select '779.01', 'Unbilled Realized Loss', NULL, NULL, 'A', 1, 'Y', NULL, (select FunctionalCurrencyCode from CFGMainData), 9, NULL, NULL, 'N' where not exists (select 'x' from CA where Account = '779.01')
    
            print ' '
            print 'Updating CFGMainData.TriangulationCurrencyCode to FunctionalCurrencyCode'
            update CFGMainData set TriangulationCurrencyCode = FunctionalCurrencyCode

            -- set WBS1, WBS2 and WBS3 variables for the System Overhead Projects 
            select @WBS1 = min(WBS1) from PR where replace(WBS1,@WBS1Delimiter1,'') like '%98%'
            if @WBS2Length > 0 select @WBS2 = min(WBS2) from PR where replace(WBS1,@WBS1Delimiter1,'') like '%98%' and WBS2 <> ' ' else set @WBS2 = ' ' 
            set @WBS3 = ' '

            print ' '
            print 'Updating CFGAutoPosting Realized and Unrealized columns'
            update CFGAutoPosting set
                RealizedGainsWBS1 = @WBS1,
                RealizedGainsWBS2 = @WBS2,
                RealizedGainsWBS3 = @WBS3,
                RealizedGainsAccount = '778.00',
                RealizedLossesWBS1 = @WBS1,
                RealizedLossesWBS2 = @WBS2,
                RealizedLossesWBS3 = @WBS3,
                RealizedLossesAccount = '779.00',
                UnrealizedGainsWBS1 = @WBS1,
                UnrealizedGainsWBS2 = @WBS2,
                UnrealizedGainsWBS3 = @WBS3,
                UnrealizedGainsAccount = '178.00',
                UnrealizedLossesWBS1 = @WBS1,
                UnrealizedLossesWBS2 = @WBS2,
                UnrealizedLossesWBS3 = @WBS3,
                UnrealizedLossesAccount = '278.00',
                UnbilledRealizedGainsWBS1 = @WBS1,
                UnbilledRealizedGainsWBS2 = @WBS2,
                UnbilledRealizedGainsWBS3 = @WBS3,
                UnbilledRealizedGainsAccount = '778.01',
                UnbilledRealizedLossesWBS1 = @WBS1,
                UnbilledRealizedLossesWBS2 = @WBS2,
                UnbilledRealizedLossesWBS3 = @WBS3,
                UnbilledRealizedLossesAccount = '779.01',
                UnbilledUnrealizedGainsWBS1 = @WBS1,
                UnbilledUnrealizedGainsWBS2 = @WBS2,
                UnbilledUnrealizedGainsWBS3 = @WBS3,
                UnbilledUnrealizedGainsAccount = '178.01',
                UnbilledUnrealizedLossesWBS1 = @WBS1,
                UnbilledUnrealizedLossesWBS2 = @WBS2,
                UnbilledUnrealizedLossesWBS3 = @WBS3,
                UnbilledUnrealizedLossesAccount = '278.01'

            print ' '
            print 'Updating Organization Realized and Unrealized columns'
             update Organization set
                RealizedGainsWBS1 = PR.WBS1,
                RealizedGainsWBS2 = PR.WBS2,
                RealizedGainsWBS3 = PR.WBS3,
                RealizedLossesWBS1 = PR.WBS1,
                RealizedLossesWBS2 = PR.WBS2,
                RealizedLossesWBS3 = PR.WBS3,
                UnrealizedGainsWBS1 = PR.WBS1,
                UnrealizedGainsWBS2 = PR.WBS2,
                UnrealizedGainsWBS3 = PR.WBS3,
                UnrealizedLossesWBS1 = PR.WBS1,
                UnrealizedLossesWBS2 = PR.WBS2,
                UnrealizedLossesWBS3 = PR.WBS3,
                UnbilledRealizedGainsWBS1 = PR.WBS1,
                UnbilledRealizedGainsWBS2 = PR.WBS2,
                UnbilledRealizedGainsWBS3 = PR.WBS3,
                UnbilledRealizedLossesWBS1 = PR.WBS1,
                UnbilledRealizedLossesWBS2 = PR.WBS2,
                UnbilledRealizedLossesWBS3 = PR.WBS3,
                UnbilledUnrealizedGainsWBS1 = PR.WBS1,
                UnbilledUnrealizedGainsWBS2 = PR.WBS2,
                UnbilledUnrealizedGainsWBS3 = PR.WBS3,
                UnbilledUnrealizedLossesWBS1 = PR.WBS1,
                UnbilledUnrealizedLossesWBS2 = PR.WBS2,
                UnbilledUnrealizedLossesWBS3 = PR.WBS3
            from PR where Organization.Org = PR.Org and PR.WBS1 like '%98%'

            print ' '
            print 'Multicurrency Activation complete'
        end

        -- set WBS1, WBS2 and WBS3 variables for the System Regular Project 
        select @WBS1 = min(WBS1) from PR where replace(WBS1,@WBS1Delimiter1,'') like '%99%'
        if @WBS2Length > 0 select @WBS2 = min(WBS2) from PR where replace(WBS1,@WBS1Delimiter1,'') like '%99%' and WBS2 <> ' ' 
        if @WBS2 is null set @WBS2 = ' '
        set @WBS3 = ' '

        print ' '
        print 'Updating CFGOHMain Variance columns'
        update CFGOHMain set OHVarianceWBS1 = @WBS1, OHVarianceWBS2 = @WBS2, OHVarianceWBS3 = @WBS3

        -- set WBS1, WBS2 and WBS3 variables for the Asset Overhead Project 
        select @WBS1 = min(WBS1) from PR where replace(WBS1,@WBS1Delimiter1,'') like '%98%'
        if @WBS2Length > 0 select @WBS2 = min(WBS2) from PR where replace(WBS1,@WBS1Delimiter1,'') like '%98%' and WBS2 <> ' ' 
        if @WBS2 is null set @WBS2 = ' '
        set @WBS3 = ' '

        print ' '
        print 'Updating CFGAssetMain Default Overhead columns'
        update CFGAssetMain set FAAccount = '820.01', FAWBS2 = @WBS2, FAWBS3 = @WBS3, DOWBS1 = @WBS1, DOWBS2 = @WBS2, DOWBS3 = @WBS3

        -- special update if Planning is enabled and Accounting is not enabled to set the WBS1/2/3 format 
        if exists (select 'x' from CFGModuleConfiguration where ModuleID = 'Acct' and Password is null) 
        and exists (select 'x' from CFGModuleConfiguration where ModuleID = 'Planning' and Password is not null) 
        begin
            print ' '
            print 'Setting AutoSumComp and EffectiveDateEnabled flags to Y and WBS1/2/3 format to length of 30 with no delimiter and no leading zeros'
            update FW_CFGSystem Set AutoSumComp = 'Y'
            update CFGBillMainData Set EffectiveDateEnabled = 'Y'
            exec keycvt @Entity=N'WBS1',@CvtType=2,@ErrMsg=null,@Changeside=N'Left',@length=30,@delim1=N'',@delimpos1=0,@delim2=N'',@delimpos2=0
            exec keycvt @Entity=N'WBS2',@CvtType=2,@ErrMsg=null,@Changeside=N'Left',@length=30,@delim1=N'',@delimpos1=0,@delim2=N'',@delimpos2=0
            exec keycvt @Entity=N'WBS3',@CvtType=2,@ErrMsg=null,@Changeside=N'Left',@length=30,@delim1=N'',@delimpos1=0,@delim2=N'',@delimpos2=0
        end

        -- special insert to load CFGProjectStageData and CFGProjectStageDescriptions if  CRM is not enabled 
        if exists (select 'x' from CFGModuleConfiguration where ModuleID = 'CRM' and Password is null) 
        begin
            print ' '
            print 'Adding rows to CFGProjectStageData and CFGProjectStageDescriptions when Planning is enabled and CRM is not enabled'
            insert into CFGProjectStageData (Code,Step) select 'A','I' where not exists (select 'x' from CFGProjectStageData where Code = 'A')
            insert into CFGProjectStageData (Code,Step) select 'B','W' where not exists (select 'x' from CFGProjectStageData where Code = 'B')
            insert into CFGProjectStageData (Code,Step) select 'C','L' where not exists (select 'x' from CFGProjectStageData where Code = 'C')
            insert into CFGProjectStageDescriptions (Code,UICultureName,Description,Seq) select 'A','en-US','In Pursuit',0 where not exists (select 'x' from CFGProjectStageDescriptions where Code = 'A' and UICultureName = 'en-US')
            insert into CFGProjectStageDescriptions (Code,UICultureName,Description,Seq) select 'B','en-US','Won',1 where not exists (select 'x' from CFGProjectStageDescriptions where Code = 'B' and UICultureName = 'en-US')
            insert into CFGProjectStageDescriptions (Code,UICultureName,Description,Seq) select 'C','en-US','Lost',2 where not exists (select 'x' from CFGProjectStageDescriptions where Code = 'C' and UICultureName = 'en-US')
            /* other languages still needed */
        end

        -- if only CRM is enabled then remove Projects and references except the System Regular Project and remove Report Favorites, set CFGMainData.RevGenEnabled to N
        if (select count(*) from CFGModuleConfiguration where ModuleID in ('Acct','PSA') and Password is not null) = 0 and
           (select count(*) from CFGModuleConfiguration where ModuleID in ('CRM','CRMAdvanced') and Password is not null) > 0
        begin
            delete from PR where Name not like 'System Regular Project%'
            update PR set Status = 'D'
            delete from CFGTKCategory
            delete from FW_ReportFavorites
            delete from FW_ReportOptions
            update CFGMainData set RevGenEnabled = 'N'
        end

        print ' '
        print 'Setting ActivationComplete flag'
        update CFGVisionSystem set ActivationComplete = 'Y'

        print ' '
        print 'Activation complete'
    end
    else
    begin
        print ' '
        print 'Activation already applied, exiting'
    end

END -- LoadActivationDefaults
GO
