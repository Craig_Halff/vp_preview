SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_GetUsers] (
	@VP3	bit = 0
)
AS
BEGIN
	/* Copyright (c) 2020 EleVia Software.  All rights reserved. */
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	declare @sql varchar(max)

	IF @VP3 = 0
	BEGIN
		-- Can we get EI users only?
		select u.Username, u.Username as DisplayUsername
			from SEUser u 
				left join EM on EM.Employee = u.Employee
			where EM.Status is null or EM.Status <> 'T'
			order by 1
	END
	ELSE
	BEGIN
		set @sql = '
			select u.Username, u.DisplayUsername
			from SEUser u 
				left join EM on EM.Employee = u.Employee
			where EM.Status is null or EM.Status <> ''T''
			order by 1'
		exec(@sql)
	END
END
GO
