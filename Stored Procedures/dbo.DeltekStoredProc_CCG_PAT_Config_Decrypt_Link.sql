SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Config_Decrypt_Link]
	@emailLink	Nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dbo.fnCCG_PAT_GetD(@emailLink) AS decoded;
END;

GO
