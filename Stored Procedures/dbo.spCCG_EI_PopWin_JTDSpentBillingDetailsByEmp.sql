SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[spCCG_EI_PopWin_JTDSpentBillingDetailsByEmp] (@WBS1 varchar(30), @WBS2 varchar(7), @WBS3 varchar(7), @Key varchar(32), @EmpName varchar(200), @UserName varchar(32))
AS BEGIN
/*
	Copyright (c) 2018 Elevia Software.  All rights reserved.
*/
	declare @ThruPeriod int, @ThruDate datetime

	select @ThruPeriod=ThruPeriod, @ThruDate=ThruDate
		from CCG_EI_ConfigInvoiceGroups cig
			inner join ProjectCustomTabFields pctf on pctf.custInvoiceGroup=cig.InvoiceGroup
		where pctf.WBS1=@WBS1 and pctf.WBS2=' '

	if @ThruPeriod is null	set @ThruPeriod=999999
	if @ThruDate is null	set @ThruDate='12/31/2050'

	declare @T TABLE (TopOrder	int, Row_Detail varchar(3000), Descr		varchar(3000), Value1		datetime,		Value2		decimal(19,2),	Value3		money,			Value4		varchar(3000))
	insert into @T (TopOrder, Descr, Value1, Value2, Value3, Value4)
		select 2 as TopOrder, ISNULL(LastName + ', ' + FirstName,''), TransDate, Hrs, BillExt, dbo.fnCCG_StripHtml(Comment)
			from (
			select Employee, TransDate, RegHrs+OvtHrs+SpecialOvtHrs as Hrs, BillExt, Comment
			from LD   
			where Employee = @Key and WBS1=@WBS1 and (WBS2=@WBS2 or @WBS2=' ') and (WBS3=@WBS3 or @WBS3=' ') and ProjectCost='Y' and SuppressBill='N' 
				and Period <= @ThruPeriod and (TransDate <= @ThruDate or TransDate is null)
			) as Labor left join EM on EM.Employee=Labor.Employee group by Labor.Employee, LastName, FirstName, TransDate, Hrs, BillExt, Comment	

	-- Add grand total line:
	insert into @T (TopOrder, Row_Detail, Descr, Value2, Value3)
		select 90 as TopOrder, 'BackgroundColor=#888888;ForegroundColor=#ffffff;Bold=Y', 'TOTAL',sum(Value2),sum(Value3) from @T where TopOrder in (2,3)
	
	-- Final recordsets
	select '', '', 'D;Align=Center', 'N2', 'C2;NS', 'A;ML',   'JTD Billing by Employee: '+ @EmpName +'  '+ @WBS1 + case when @WBS2 <> '' then +' - ' + @WBS2 else '' end + case when @WBS3 <> '' then +' - ' + @WBS3 else '' end 
	select Descr as [Employee Name], Row_Detail, Value1 as [Date], Value2 as [Hours], Value3 as [Amount], Value4 as [Comments] from @T
END
GO
