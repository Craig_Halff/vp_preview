SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteContact] @ContactID varchar(32)
AS
    DELETE FROM ContactActivity WHERE ContactID = @ContactID
    DELETE FROM WorkflowContactActivity WHERE ContactID = @ContactID
    DELETE FROM ContactCustomTabFields WHERE ContactID = @ContactID
    DELETE FROM ContactFileLinks WHERE ContactID = @ContactID
    DELETE FROM ContactMoreInfo WHERE ContactID = @ContactID
    DELETE FROM ContactsFromLeads WHERE ContactID = @ContactID
    DELETE FROM ContactSubscr WHERE ContactID = @ContactID
    DELETE FROM ContactToContactAssoc WHERE FromContactID = @ContactID
    DELETE FROM ContactToContactAssoc WHERE ToContactID = @ContactID
    DELETE FROM CustomProposalContact WHERE ContactID = @ContactID
    DELETE FROM EMContactAssoc WHERE ContactID = @ContactID
    DELETE FROM MktCampaignContactAssoc WHERE ContactID = @ContactID    
    --DELETE FROM OpportunityContactAssoc WHERE ContactID = @ContactID
    DELETE FROM PRContactAssoc WHERE ContactID = @ContactID
    DELETE FROM PRContactAssocTemplate WHERE ContactID = @ContactID
    DELETE FROM MktCampaignClientAssoc WHERE ContactID = @ContactID    
    --DELETE FROM OpportunityFromContacts WHERE ContactID = @ContactID    
    DELETE FROM ClientFromContacts WHERE ContactID = @ContactID    

    EXEC DeleteCustomGridTabData 'Contacts',@ContactID, '', ''

    UPDATE Activity set ContactID = null WHERE ContactID = @ContactID
    UPDATE WorkflowActivity set ContactID = null WHERE ContactID = @ContactID
    --UPDATE Opportunity set ContactID = null WHERE ContactID = @ContactID
    UPDATE PR set BillingContactID = null WHERE BillingContactID = @ContactID
    UPDATE PR set ContactID = null WHERE ContactID = @ContactID
    UPDATE PRTemplate set BillingContactID = null WHERE BillingContactID = @ContactID
    UPDATE PRTemplate set ContactID = null WHERE ContactID = @ContactID
GO
