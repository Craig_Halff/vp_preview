SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Ctrl_CopyFrom] ( @v nvarchar(100), @vendorID nvarchar(20), @originatingVendorEnabled bit)
             AS EXEC spCCG_PAT_Ctrl_CopyFrom @v,@vendorID,@originatingVendorEnabled
GO
