SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_CallGetPOData] ( @SPName varchar(500), @SelectType varchar(20), @SelectParam1 varchar(255), @SelectParam2 varchar(255), @OutputType varchar(20), @User varchar(50)= null, @BatchLoadSeq varchar(10)= null)
             AS EXEC spCCG_PAT_CallGetPOData @SPName,@SelectType,@SelectParam1,@SelectParam2,@OutputType,@User,@BatchLoadSeq
GO
