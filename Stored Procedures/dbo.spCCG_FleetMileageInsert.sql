SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_FleetMileageInsert] @UDIC_UID varchar (32)
AS
/*
Copyright 2019 (c) Central Consulting Group.   All rights reserved.
08/02/2019	David Springer
			Write next blank record.
			Call from Fleet Mileage Log INSERT workflows
*/
DECLARE @Seq integer
BEGIN
SET NOCOUNT ON

If not exists (Select 'x' From UDIC_FleetManagement_MileageLog Where UDIC_UID = @UDIC_UID and CustProject is null and CustMiles = 0)
	Begin
	Select @Seq = Min (CustSequence) - 1
	From UDIC_FleetManagement_MileageLog
	Where UDIC_UID = @UDIC_UID

	If @Seq is null 
		Set @Seq = -1

	Insert Into UDIC_FleetManagement_MileageLog
	(UDIC_UID, Seq, CustSequence, CustSource)
	Values
	(@UDIC_UID, Replace (NewID(), '-', ''), @Seq, 'Manual')
	End
END
GO
