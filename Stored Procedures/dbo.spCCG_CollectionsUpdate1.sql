SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_CollectionsUpdate1]
AS 
/* 
Copyright (c) 2020 Central Consulting Group.  All rights reserved.
06/18/2020	David Springer
			Update UDIC_Collections 
			Notify Project Accountant
			Call this from a Collections scheduled workflow
06/25/2020  Craig H. Anderson
            Update CSS block.
08/10/2020  Craig H. Anderson
		    Modified to use PAEmail for go-live.  Added Jami Kubik to CcList on email going out as Jami.
09/01/2020	Craig H. Anderson
			Initial update now takes into account when PA may have skipped the 'Verified Invoice Received' step
			to avoid repeat notices
02/01/2021	Added requirement that balance > 0 to avaoid notifications for Credits
*/
DECLARE @PAEmpNo		varchar (32),
		@PAName			varchar (255),
		@PAEmail		varchar (255),
		@WBS1			varchar (32),
		@Project		varchar (2000),
		@UDIC_UID		varchar (32),
		@InvoiceNo		varchar (255),
		@InvoiceDate	Date,
		@Balance		float,
		@DaysOut		integer,
		@RequestedAction	varchar (255),
		@RequestedActionDate	date,
		@CSS			varchar (max),
        @Body			varchar (max),
		@Loop1			integer,
		@Loop2			integer,
		@Loop3			integer

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
	Update c
	Set c.CustRequestedAction = 'Verify Invoice Received',
        c.CustRequestedActionStatus = 'Incomplete',
		c.CustRequestedActionStart = convert (date, GetDate()),
		c.CustRequestedActionOwner = c.CustBiller,
		c.ModUser = 'CollectionsUpdate1',
		c.ModDate = GETDATE()
	From UDIC_Collection c
	Where c.CustCollectionStatus = 'Open'
	  and c.CustRequestedAction is null
	  and datediff(day, c.CustInvoiceDate, getdate()) > 45
	  AND c.CustBalance > 0.0
	  and not exists (Select 'x' From UDIC_Collection_History
	                  WHERE UDIC_UID = c.UDIC_UID
					      AND CustHistoryResponse IN ('Verified Invoice Received', 'Legal Involvement', 'Write Off'))

-- Must be below update
DECLARE curPA insensitive cursor for
		Select distinct pa.Employee, IsNull (pa.PreferredName, pa.FirstName), pa.Email
		From UDIC_Collection c, EM pa
		Where c.CustCollectionStatus = 'Open'
		  and c.CustBiller = pa.Employee
		  AND c.CustRequestedAction = 'Verify Invoice Received'
		  AND c.CustRequestedActionStatus = 'Incomplete'
		Order by 1

-- CSS style rules
	Set @CSS = 
'
<style type="text/css">
    table {width:100%;max-width:7.5in;border-collapse: collapse; font-size:10pt;}
	p {padding-top:10pt;font-family: Calibri, Arial, sans-serif; font-size:10pt;}
	td, th {border: 1pt solid black;padding: 5px;font-family:Calibri, Arial, sans-serif; font-size:10pt;}
	th {background-color: rbg(68,114,96);color: white; text-align:center;font-size:10.5pt;padding-left:5px;padding-right:5px;}
	.aLeft {text-align:left;}
	.aCenter {text-align:center;}
	.aRight {text-align:right;}
	.Table_Header tr th {background-color: #4472c4;color: white;}
	.Project_Header {background-color:#CCCCCC; color:black;}
	tfoot tr td {border:none;font-weight:bold;}
    tfoot tr td.aRight {border-bottom: 3pt double;black;}
    caption {color:white;font-size: 1.25em;}
</style>
'
--	############  Project Accountant  ##############
	OPEN curPA
	FETCH NEXT FROM curPA INTO @PAEmpNo, @PAName, @PAEmail
	Set @Loop1 = @@FETCH_STATUS
	While @Loop1 = 0
		BEGIN
	--	Open the table with headers
		Set @Body = '<p>' + @PAName + ',</p><p>Please verify that our clients have received the invoices below, and note it in the Collections Info Center.</p>'

        Set @Body = @Body +'
        <table cellpadding="5" cellspacing="0" style="border:1px solid black;">
	        <caption>Invoices Over 45-days Old</caption>
            <thead class="Table_Header">
		        <tr>
			        <th style="width:3em;">Invoice<br>Number</th>
			        <th style="width:3em;">Invoice<br>Date</th>
			        <th style="width:4em;">Invoice<br>Balance</th>
			        <th style="width:2em;">Days<br>Out</th>
			        <th style="width:4em;">Requested<br>Action<br>Date</th>
			        <th>Requested<br>Action</th>
		        </tr>
	        </thead>
	        <tbody>
';
	--	############  Project  ##############
		DECLARE curProject insensitive cursor for
				Select distinct p.WBS1, bc.Name + ' (' + IsNull (bcc.PreferredName, bcc.FirstName) + ' ' + bcc.LastName + '): ' + p.WBS1 + ' - ' + p.Name + ' (PM ' + IsNull (pm.PreferredName, pm.FirstName)  + ' ' + pm.LastName + ')' Project
				From UDIC_Collection c, PR p, EM pm, CL bc, Contacts bcc
				Where c.CustCollectionStatus = 'Open'
				  and datediff(day, c.CustInvoiceDate, getdate()) > 45
					and c.CustRequestedAction = 'Verify Invoice Received'
					AND c.CustRequestedActionStatus = 'Incomplete'
					and c.CustBiller = @PAEmpNo
					and c.CustProjMgr = pm.Employee
					and c.CustProject = p.WBS1
					and p.WBS2 = ' '
					and c.CustBillingClient = bc.ClientID
					and c.CustBillingContact = bcc.ContactID
				Order by 2

		OPEN curProject
		FETCH NEXT FROM curProject INTO @WBS1, @Project
		Set @Loop3 = @@FETCH_STATUS
		While @Loop3 = 0
			BEGIN
			DECLARE curInvoice insensitive cursor for
					Select c.UDIC_UID, c.CustInvoiceNumber, c.CustInvoiceDate, c.CustBalance, c.CustDaysOutstanding, c.CustRequestedActionStart, c.CustRequestedAction
					From UDIC_Collection c
					Where c.CustCollectionStatus = 'Open'
						and c.CustRequestedAction = 'Verify Invoice Received'
						and c.CustProject = @WBS1
					Order by 1
			Set @Body =  @Body +
				'<tr>
					<td class="Project_Header" colspan="6">' + @Project + '</td>
				</tr>'
			OPEN curInvoice
			FETCH NEXT FROM curInvoice INTO @UDIC_UID, @InvoiceNo, @InvoiceDate, @Balance, @DaysOut, @RequestedActionDate, @RequestedAction
			While @@FETCH_STATUS = 0
				BEGIN
				Set @Body = @Body +
					'<tr>
						<td class="aRight">' + @InvoiceNo + '</td>
						<td class="aCenter">' + convert (varchar (10), @InvoiceDate, 101) + '</td>
						<td class="aRight">' + convert (varchar, cast (@Balance as money), 1)+ '</td>
						<td class="aRight">' + convert (varchar, cast (@DaysOut as integer), 1)+ '</td>
						<td class="aCenter">' + convert (varchar (10), @RequestedActionDate, 101) + '</td>
						<td class="aLeft">' + @RequestedAction + '</td>
					</tr>'

				Insert Into UDIC_Collection_History
				(UDIC_UID, Seq, CustHistoryActionStatus, CustHistoryAction, CustHistoryActionStart)
				Values (@UDIC_UID, Replace (NewID(), '-', ''), 'Sent', 'PA to Verify Invoice Received', getDate())

			--	End Invoice
				FETCH NEXT FROM curInvoice INTO @UDIC_UID, @InvoiceNo, @InvoiceDate, @Balance, @DaysOut, @RequestedActionDate, @RequestedAction
				END
			CLOSE curInvoice
			DEALLOCATE curInvoice

		--	End Project
			FETCH NEXT FROM curProject INTO @WBS1, @Project
			Set @Loop3 = @@FETCH_STATUS
			END
		CLOSE curProject
		DEALLOCATE curProject

		Set @Body = @Body +
			'</tbody>
			</table>'
		IF @Body IS NOT NULL
		BEGIN
			INSERT Into dbo.CCG_Email_Messages (SourceApplication, Sender, ToList,CCList, [Subject], Body, CreateDateTime, MaxDelay)
			Values ('PA Collections Notification', 'jKubik@Halff.com', @PAEmail , 'jKubik@Halff.com' ,'Weekly Collections Invoice Receipt Verifications ' + @PAEmail, @CSS + @Body, getDate(), 0)
			--VALUES ('PA Collections Notification', 'cAnderson@Halff.com', 'cAnderson@Halff.com' , 'cAnderson@Halff.com' ,'Weekly Collections Invoice Receipt Verifications ' +'cAnderson@Halff.com', @CSS + @Body, getDate(), 0)
		END
	--	End Project Accountant
		FETCH NEXT FROM curPA INTO @PAEmpNo, @PAName, @PAEmail
		Set @Loop1 = @@FETCH_STATUS

		END
	CLOSE curPA
	DEALLOCATE curPA

END
GO
