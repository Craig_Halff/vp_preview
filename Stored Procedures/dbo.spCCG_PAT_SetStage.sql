SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_SetStage] (@PendingSeq int, @PayableSeq int, @Stage varchar(30), @ChangedBy Nvarchar(32))
--WITH ENCRYPTION
AS
BEGIN
/*
	Copyright (c) 2020 EleVia Software. All rights reserved.
*/
	set nocount on

	BEGIN TRY

		BEGIN TRANSACTION

		declare @stageType varchar(10)
		declare @priorStage varchar(50)
		declare @desc Nvarchar(101)
		declare @payableSeqCheck int
		declare @delegateFor Nvarchar(32)
		declare @parallelSort int
		declare @ModDate datetime = GetUTCDate()

		if @PendingSeq > 0
		BEGIN
			select @payableSeqCheck = payableSeq, @desc = ISNULL(ISNULL(r.RouteLabel,p.[Route]),'') + ' ' + ISNULL([Description],'')
				,@priorStage = p.Stage,
				@delegateFor = case when p.Employee = @ChangedBy then null else p.Employee end,
				@parallelSort = case when parallel = 'Y' then SortOrder else null end
			from CCG_PAT_Pending p
			left join CCG_PAT_ConfigRoutes r on p.[Route] = r.[Route]
			where Seq = @PendingSeq

			if ISNULL(@payableSeqCheck,-1) <> ISNULL(@PayableSeq,-2)
				RAISERROR ('Seq parameters mismatch', -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		END
		else
		BEGIN
			set @desc = ''--this happens when stage change was not in pending mode.  TBD if this is allow.
		END

		select @stageType = ISNULL([TYPE],' ') from CCG_PAT_ConfigStages where Stage = @Stage

		if ISNULL(@priorStage,'')=''--if we did not get one from pending, get from main payable table
			select @priorStage = p.Stage from CCG_PAT_Payable p
			where Seq = @PayableSeq

		if ISNULL(@PayableSeq,0) <= 0 or  @stageType IS NULL
			RAISERROR ('Invalid Stage or Seq parameters', -- Message text.
               16, -- Severity.
               1 -- State.
               );

		--Set stage of item's table to track last stage

		update CCG_PAT_Payable set Stage = @Stage,ModUser = @ChangedBy, ModDate = @ModDate where Seq = @PayableSeq

		if @stageType = 'Abort'
		  begin
				INSERT INTO CCG_PAT_History(PayableSeq,Stage,ActionDate,ActionTaken,ActionTakenBy,ActionRecipient,Description,ParallelSort)
				select PayableSeq,@Stage, @ModDate,N'Abort Route',@ChangedBy,Employee,Description,case when Parallel='Y' then SortOrder else null end from CCG_PAT_Pending where PayableSeq = @PayableSeq

				delete from CCG_PAT_Pending where PayableSeq = @PayableSeq
				set @parallelSort = null
		  end
		else if @PendingSeq > 0
		BEGIN
			  --Update Pending Table depending on the stage type of Continue, Abort, or other
			  if @stageType = 'Continue' or @stageType = 'Approve'
					delete from CCG_PAT_Pending where Seq = @PendingSeq
			  else
			  begin
					update CCG_PAT_Pending set Stage = @Stage where Seq = @PendingSeq
					set @parallelSort = null
			  end
		END



		--ADD History Record
		INSERT INTO CCG_PAT_History(PayableSeq,Stage,ActionDate,ActionTaken,ActionTakenBy,DelegateFor,[Description],PriorStage,PriorStageDateSet,ParallelSort)
		VALUES(@PayableSeq,@Stage, @ModDate,N'Stage Change',@ChangedBy,@delegateFor, LEFT(LTRIM(RTRIM(@desc)),255),@priorStage,dbo.fnCCG_PAT_GetPriorStageChangeActionDateForPayable(@PayableSeq, @ModDate),@parallelSort)

		if @stageType = 'Approve'
		BEGIN
			update h
			set h.ApprovedDetail = p.ApprovedDetail
			from CCG_PAT_History h
			inner join CCG_PAT_Payable p on p.Seq = h.PayableSeq
			where h.Seq = SCOPE_IDENTITY()
		END

		select 0 as Result, '' as ErrorMessage, convert(varchar(23), @ModDate, 121) as ModDate
		commit transaction
	END TRY
	BEGIN CATCH
		/* SQL found some error - return the message and number: */
		select -1 as Result, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as varchar(20)) + ')' as ErrorMessage
		rollback transaction
	END CATCH

END
GO
