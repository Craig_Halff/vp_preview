SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_ConfigSaveGeneral] (@newValuesSql Nvarchar(max))
AS
BEGIN
	/* Copyright (c) 2017 EleVia Software and Central Consulting Group.  All rights reserved. */
	/*
	spCCG_EI_ConfigSaveGeneral
		@newValuesSql = 'Series = 50, ActivationPassword = ''j+F83PH9RvHxJgWZNg4mjaUNs2DYdSMEMHyvuMyKdlO7BNH88/bBoxcLLxW0iTYd'', LogLevel = ''Warning'', LogFile = ''C:\Temp\EI.log'', InvoiceNumberLength = 7, ProjectNameLength = 40, DraftInvoiceWildcardString = ''*Project [:PR.WBS1] *Draft?2*.pdf'', UnbilledDetailWildcardString = ''Project [:PR.WBS1] Invoice Draft U*.pdf'', FinalInvoiceWildcardString = ''*Project [:PR.WBS1] *.pdf'', AcctRoles = ''DEFAULT;'', ROAcctRoles = ''CRMADM;'', SupportMessages = ''Y'', NotificationInterval = 2, SupportPeriodBillThru = ''N'', SupportDateBillThru = ''Y'', ShowResetVisible = ''A'', FreezeColumns = ''Col1Only'', AdvanceOnStageChange = ''NONE'', FilterSQL = ''Y'', FilterBillGroup = ''N'', HideDormant = ''Y'', FilterPDF = ''N'', FilterPDFDays = -1, MainGridFiltering = ''T'', SupportPackaging = ''Y'', DocsEnabled = ''Y'', DocsStorage = ''Copy'', DocsPackaging = ''N'', SupportBuiltInFeePctCpl = ''Q'', AllowFeePctCplReduction = ''Y'', AllowFeeToExceed100Pct = ''Y'', SaveFeePctCplImmediately = ''Y'', PctCompleteEnableUnitsColumns = ''Y'', SyncInvoiceStageToVision = ''Y'', ConfigEmployees = ''00001;00002'', SubgridMode = ''Wbs3SubGrid'''
	*/
	SET NOCOUNT ON
	declare @sSQL		Nvarchar(max)
	declare @safeSql	int					-- int value indicates which parameter failed the security check

	set @sSQL = 'UPDATE CCG_EI_Config SET ' + @newValuesSql
	exec (@sSQL)
END
GO
