SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_EI_FileHelper_getXFDFDB]
      @FILEID varchar(50),
      @revisionSeq int
AS
BEGIN
      SET NOCOUNT ON
      select * from CCG_EI_XFDF where DOCS_PR_FILEID = @FILEID and RevisionSeq = @revisionSeq
END
GO
