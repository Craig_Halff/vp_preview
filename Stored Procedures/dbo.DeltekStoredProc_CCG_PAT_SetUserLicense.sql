SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_SetUserLicense] ( @Username nvarchar(32), @OkdLicense char(1))
             AS EXEC spCCG_PAT_SetUserLicense @Username,@OkdLicense
GO
