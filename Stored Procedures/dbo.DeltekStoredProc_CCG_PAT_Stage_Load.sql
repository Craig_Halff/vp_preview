SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Stage_Load]
	@VISION_LANGUAGE	varchar(10)
AS
BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT T1.Stage, IsNull(T2.StageLabel, IsNull(T1.StageLabel, '')) as StageLabel,
			IsNull(T2.StageDescription, IsNull(T1.StageDescription, '')) as StageDescription,
            T1.Status, T1.[Type], T1.[SubType], IsNull(T2.EmailSubject,
			IsNull(T1.EmailSubject, '')) as EmailSubject,
            IsNull(T2.EmailSubjectBatch, IsNull(T1.EmailSubjectBatch, '')) as EmailSubjectBatch,
            IsNull(T2.EmailMessage, IsNull(T1.EmailMessage, '')) as EmailMessage,
            T1.[AccountingOnly], T1.[RequireStamp], T1.[ValidRoles], T1.SortOrder
        FROM CCG_PAT_ConfigStages T1
            LEFT JOIN CCG_PAT_ConfigStagesDescriptions T2
                on T2.Stage = T1.Stage and T2.UICultureName = @VISION_LANGUAGE
        ORDER BY SortOrder, StageLabel;
END;

GO
