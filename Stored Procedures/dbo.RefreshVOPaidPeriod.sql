SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RefreshVOPaidPeriod] AS 
BEGIN
	SET NOCOUNT ON
	UPDATE VO SET VO.PaidPeriod = 999999 WHERE VO.PaidPeriod <> 999999

	UPDATE VO SET VO.PaidPeriod = L.MaxPeriod
	From VO Inner Join (
		SELECT vendor, voucher, max(period) as MaxPeriod
		FROM (
		SELECT LedgerAP.Vendor, LedgerAP.Voucher, (LedgerAP.Period), LedgerAP.AmountSourceCurrency 
		FROM LedgerAP LEFT JOIN CFGPostControl ON LedgerAP.Period = CFGPostControl.Period AND LedgerAP.PostSeq = CFGPostControl.PostSeq
		WHERE ((LedgerAP.TransType = N'AP' AND LedgerAP.SubType = 'L'))
				AND ISNULL(CFGPostControl.TransType, '') <> N'BE' 
		union all
		SELECT LedgerAP.Vendor, LedgerAP.Voucher, (LedgerAP.Period), -LedgerAP.AmountSourceCurrency 
		FROM LedgerAP LEFT JOIN CFGPostControl ON LedgerAP.Period = CFGPostControl.Period AND LedgerAP.PostSeq = CFGPostControl.PostSeq
		WHERE ((LedgerAP.TransType = N'PP' AND LedgerAP.SubType <> 'X')) 
				AND ISNULL(CFGPostControl.TransType, '') <> N'BE' 
		) as x
		GROUP BY Vendor, Voucher 
		HAVING Sum(AmountSourceCurrency) BETWEEN -0.00005 AND 0.00005
	) L on VO.vendor=L.vendor and VO.Voucher=L.Voucher
END
GO
