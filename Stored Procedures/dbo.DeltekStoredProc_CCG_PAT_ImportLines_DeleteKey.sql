SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_ImportLines_DeleteKey] (
	@key			varchar(50),
	@type			varchar(50)
) AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM CCG_PAT_ConfigOptions WHERE ConfigID = @key and type = @type;

	SELECT @@ROWCOUNT as RowsAffected;
END;

GO
