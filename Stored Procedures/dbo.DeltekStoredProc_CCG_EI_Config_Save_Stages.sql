SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Save_Stages]
	@fieldsSQL			Nvarchar(max),
	@newValuesSql		Nvarchar(max),
	@VISION_LANGUAGE	varchar(10)
AS
BEGIN
	/*
	DeltekStoredProc_CCG_EI_Config_Save_Stages
		@fieldsSQL = 'Stage, StageLabel, StageDescription, InvoiceType, RightDoc, RequireStamp, Status',
		@newValuesSql = '(''Accounting'', ''Accounting'', ''Accounting'', '''', ''Unbilled'', null, ''A''), (''PM Input'', ''PM Input'', ''PM Input'', ''Final'', ''Package'', null, ''A''), (''Review'', ''Review'', ''Review'', ''Draft'', ''Unbilled'', null, ''A''), (''Invoice as is'', ''Invoice as is'', ''Invoice as is'', '''', ''Unbilled'', null, ''A''), (''Re-Review'', ''Re-Review'', ''Re-Review'', ''Draft'', ''Unbilled'', null, ''A''), (''Invoice as noted'', ''Invoice as noted'', ''Invoice as noted'', '''', ''Unbilled'', null, ''A''), (''Do not bill this month'', ''Do not bill this month'', ''Do not bill this month'', '''', ''Unbilled'', null, ''A''), (''Send Draft'', ''Please send draft for review'', ''Please send draft for review'', '''', ''Unbilled'', null, ''A''), (''Ready For Final'', ''Ready for final'', ''Ready for final'', '''', ''Unbilled'', null, ''A''), (''Revise and return'', ''Revise and return'', ''Revise and return'', '''', ''Unbilled'', null, ''A''), (''Approved'', ''Approved'', ''Approved'', '''', ''Unbilled'', null, ''A''), (''Approved - DNI'', ''Approved - DNI'', ''Approved - DNI'', '''', ''Unbilled'', null, ''A''), (''Write-off approval'', ''Write-off approval'', ''Write-off approval'', '''', ''Unbilled'', null, ''A''), (''Final'', ''Final'', ''Final'', ''Final'', ''Package'', null, ''A''), (''Write-off approved'', ''Write-off approved'', ''Write-off approved'', '''', ''Unbilled'', null, ''I'')',
		@VISION_LANGUAGE = 'en-US'
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	int;			-- INT value indicates which parameter failed the security check

	-- Ensure that our inputs are safe (preventing SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<CSV>', @fieldsSQL);
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<INSERT VALUES>', @newValuesSql) * 2;
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @VISION_LANGUAGE) * 4;
	IF @safeSql < 7 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS varchar(4));
		RETURN;
	END;

	BEGIN TRANSACTION;

	SET @sSQL = N'
		MERGE CCG_EI_ConfigStages WITH (HOLDLOCK) AS Target
		USING (VALUES ' + @newValuesSql + N'
			) As Source (' + @fieldsSQL + N')
			ON Target.Stage = Source.Stage
		WHEN MATCHED THEN
			UPDATE SET
				[StageLabel]		= Source.[StageLabel],
				[StageDescription]	= Source.[StageDescription],
				[Status]			= Source.[Status],
				[InvoiceType]		= Source.[InvoiceType],
				[RightDoc]			= Source.[RightDoc],
				[RequireStamp]		= Source.[RequireStamp]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (' + @fieldsSQL + N')
			VALUES (Source.' + REPLACE(@fieldsSQL, N', ', N', Source.') + N');';
			-- WHEN NOT MATCHED BY SOURCE THEN DELETE;
	--PRINT (@sSQL);
	EXEC (@sSQL);

	-- Update the multilingual descriptions table
	SET @sSQL = N'
		MERGE CCG_EI_ConfigStagesDescriptions WITH (HOLDLOCK) AS Target
		USING CCG_EI_ConfigStages AS Source ON Target.Stage = Source.Stage
		WHEN MATCHED AND Target.UICultureName = '''+@VISION_LANGUAGE+N''' THEN
			UPDATE SET
				[StageLabel]		= Source.[StageLabel],
				[StageDescription]	= Source.[StageDescription]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (Stage, UICultureName, StageLabel, StageDescription)
				VALUES (Source.Stage, '''+@VISION_LANGUAGE+N''', Source.StageLabel, Source.StageDescription)
		WHEN NOT MATCHED BY SOURCE THEN DELETE;';
	EXEC (@sSQL);

	COMMIT TRANSACTION;
END;
GO
