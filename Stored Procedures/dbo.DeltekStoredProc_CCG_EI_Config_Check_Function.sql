SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Check_Function]
	@functionName	varchar(1000)
AS
BEGIN
	SET NOCOUNT ON;
	SET @functionName = REPLACE(@functionName, 'dbo.', 'dbo].[');

	SELECT oname, otype,
		MAX(CASE WHEN parameter_id = 0 AND is_output = 1 THEN system_type_id ELSE null END) as rtype,
		MAX(CASE WHEN parameter_id = 1 AND UPPER(name) = '@WBS1' THEN system_type_id ELSE null END) as wbs1,
		max(case when parameter_id = 2 and UPPER(name) = '@WBS2' then system_type_id else null end) as wbs2,
		max(case when parameter_id = 3 and UPPER(name) = '@WBS3' then system_type_id else null end) as wbs3,
		Max(case when parameter_id = 4 and UPPER(name) = '@USERNAME' then system_type_id else null end) as username
	FROM (
		SELECT o.name as oname, type as otype, p.name, p.parameter_id, p.system_type_id, p.is_output
		FROM sys.objects o, sys.parameters p
		WHERE o.object_id = p.object_id
			AND o.object_id = OBJECT_ID(N'[' + @functionName + ']')
	) d
	GROUP BY oname, otype;

END;
GO
