SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_ProjectPhaseTeamFeeUpdate] @WBS1 varchar (30)
AS
/*
Copyright 2021 Central Consulting Group.   All rights reserved.
08/23/2021	David Springer
			Call this from a Project Phases grid INSERT, CHANGE and DELETE workflow when fee has changed
*/
SET NOCOUNT ON
BEGIN

	Update t
	Set t.CustTeamLaborFee = Round (IsNull (c.LaborFee, 0) * t.CustTeamPct / 100, 2)
	From Projects_CustPhaseTeams t
		Left Join
			(Select WBS1, Sum (CustPhaseFeeDirLab) LaborFee
			From Projects_Phases
			Where WBS2 = ' '
			Group by WBS1) c on c.WBS1 = t.WBS1
	Where t.WBS1 = @WBS1 
	  and t.WBS2 = ' '

END

GO
