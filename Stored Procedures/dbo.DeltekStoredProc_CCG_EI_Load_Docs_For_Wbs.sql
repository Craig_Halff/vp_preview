SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Load_Docs_For_Wbs] ( @wbs1 nvarchar(30))
             AS EXEC spCCG_EI_Load_Docs_For_Wbs @wbs1
GO
