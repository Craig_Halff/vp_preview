SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[~HAI_TSR_TimeAnalysisByPracticeArea]
	@CompanyId varchar(2),
	@PracticeArea varchar(75),
	@LastSunday DATETIME,
	@LastSundayMinus364Days DATETIME,
	@LastMondayAndTime DATETIME,
	@LastMondayMinus363DaysAndTime DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SET TRANSACTION isolation level READ uncommitted; 

IF EXISTS (SELECT NAME 
           FROM   sysobjects 
           WHERE  NAME = 'setContextInfo' 
                  AND type = 'P') 
  EXECUTE dbo.Setcontextinfo 
    @StrCompany = /*N*/@CompanyId, 
    @StrAuditingEnabled = 'N', 
    @strUserName = /*N*/'HIT', 
    @StrCultureName = 'en-US'; 

SELECT Isnull(Substring(EM.org, 7, 2), '')   AS group1, 
       Min(groupCFGOrgCodes3.label)          AS groupDesc1, 
       Isnull(em.employee, '')               AS group2, 
       Min(CASE 
             WHEN EM.firstname IS NULL THEN EM.lastname 
             ELSE EM.lastname + ', ' + EM.firstname 
           END)                              AS groupDesc2, 
       em.employee, 
       emLD.linetype, 
       Min(CompanyLabels.label)              AS HomeCompany, 
       Min(EM.status)                        AS Status, 
       Min(EM.targetratio)                   AS TargetRatio, 
       Min(EM.jobcostrate)                   AS JobCostRate, 
       Min(EM.jobcosttype)                   AS JobCostType, 
       Min(EM.hoursperday)                   AS HoursPerDay, 
       Min(EM.hiredate)                      AS HireDate, 
       Min(EM.terminationdate)               AS TerminationDate, 
       Min(EM.provcostrate)                  AS ProvCostRate, 
       Min(EM.provbillrate)                  AS ProvBillRate, 
       Min(EM.provcostotpct)                 AS ProvCostOTPct, 
       Min(EM.provbillotpct)                 AS ProvBillOTPct, 
       Min(EM.provcostspecialotpct)          AS ProvCostSpecialOTPct, 
       Min(EM.provbillspecialotpct)          AS ProvBillSpecialOTPct, 
       Min(EM.org)                           AS Org, 
       Min(EM.lastname)                      AS LastName, 
       Min(EM.firstname)                     AS FirstName, 
       Min(EM.usetotalhrsasstd)              AS UseTotalHrsAsStd, 
       Sum(emLD.dir)                         AS Dir, 
       Sum(emLD.realizationhrs)              AS RealizationHrs, 
       Sum(emLD.realizationamt)              AS RealizationAmt, 
       Sum(emLD.indir)                       AS Indir, 
       Sum(CASE 
             WHEN cfgtimeanalysisheadings.benefit = 'Y' THEN emLD.indir 
             ELSE 0 
           END)                              AS Benefit, 
       Sum(CASE 
             WHEN emLD.reportcolumn = 1 THEN emLD.indir 
             ELSE 0 
           END)                              AS Col1, 
       Sum(CASE 
             WHEN emLD.reportcolumn = 2 THEN emLD.indir 
             ELSE 0 
           END)                              AS Col2, 
       Sum(CASE 
             WHEN emLD.reportcolumn = 11 THEN emLD.indir 
             ELSE 0 
           END)                              AS Col11, 
       Sum(CASE 
             WHEN emLD.reportcolumn = 5 THEN emLD.indir 
             ELSE 0 
           END)                              AS Col5, 
       Sum(CASE 
             WHEN emLD.reportcolumn = 6 THEN emLD.indir 
             ELSE 0 
           END)                              AS Col6, 
       Sum(CASE 
             WHEN emLD.reportcolumn = 7 THEN emLD.indir 
             ELSE 0 
           END)                              AS Col7, 
       ( Sum(emLD.indir) - Sum(CASE 
                                 WHEN ( emLD.reportcolumn = 1 
                                         OR emLD.reportcolumn = 2 
                                         OR emLD.reportcolumn = 11 
                                         OR emLD.reportcolumn = 5 
                                         OR emLD.reportcolumn = 6 
                                         OR emLD.reportcolumn = 7 ) THEN 
                                 emLD.indir 
                                 ELSE 0 
                               END) ) + 0.00 AS Other, 
       Sum(emld.nonbillhrs)                  AS NBHrs, 
       /***STDHoursCols***/ 0.00             AS NumWorkingHours, 
       ''                                    AS currencyCode, 
       ''                                    AS currencyCodeCount, 
       ''                                    AS decimalPlaces, 
       ''                                    AS currencySymbol, 
       0                                     AS dummy 
FROM   emallcompany EM 
       LEFT JOIN employeecustomtabfields 
              ON employeecustomtabfields.employee = em.employee 
       LEFT JOIN cfgorgcodes AS CompanyLabels 
              ON EM.homecompany = CompanyLabels.code 
                 AND CompanyLabels.orglevel = 1 
       LEFT JOIN cfgorgcodes AS groupCFGOrgCodes3 
              ON Substring(EM.org, 7, 2) = groupCFGOrgCodes3.code 
                 AND groupCFGOrgCodes3.orglevel = 3 
       LEFT JOIN cfgmaindata AS CFGMainDataEM 
              ON CFGMainDataEM.company = /*N*/@CompanyId 
       /***LEFTJOINCFGCurrency***/ 
       LEFT JOIN (SELECT 'CUR'                                         AS 
                         lineType, 
                         1                                             AS pdType 
                         , 
                         ld.employee, 
                         EM.employeecompany, 
                         Sum(0)                                        AS 
                         ReportColumn, 
                         Sum(ld.reghrs + ld.ovthrs + ld.specialovthrs) AS Dir, 
                         Sum(CASE 
                               WHEN EM.provbillrate IS NULL 
                                     OR EM.provbillrate = 0 THEN 0 
                               ELSE 
realizationamountbillingcurrency / EM.provbillrate 
                             END)                                      AS 
                         RealizationHrs, 
                         Sum(CASE 
                               WHEN ld.billstatus = 'T' THEN 0 
                               ELSE realizationamountbillingcurrency 
                             END)                                      AS 
                         RealizationAmt, 
                         0                                             AS 
                         NonBillHrs, 
                         0                                             AS Indir 
                  FROM   ld, 
                         emallcompany AS EM 
                         LEFT JOIN organization 
                                ON EM.org = organization.org 
                  WHERE  ( ld.employee = EM.employee ) 
                         AND ( ld.transdate BETWEEN @LastMondayMinus363DaysAndTime 
                                                    AND 
                                                    @LastMondayAndTime ) 
                         AND ( ld.chargetype = 'R' ) 
                         AND NOT EXISTS (SELECT 'x' 
                                         FROM   cfgpostcontrol 
                                         WHERE  cfgpostcontrol.company = 
                                                EM.employeecompany 
                                                AND ld.period = 
cfgpostcontrol.period 
                                                AND ld.postseq = 
cfgpostcontrol.postseq 
                                                AND xchargestatus = 'X') 
                         AND Isnull(Substring(ld.emorg, 1, 2), ' ') = 
                             EM.employeecompany 
                         AND (( ( Substring(EM.org, 1, 2) = /*N*/@CompanyId ) 
                                AND EXISTS (SELECT 'x' 
                                            FROM   employeecustomtabfields 
                                    INNER JOIN fw_customcolumnvalues CCV 
                                            ON CCV.code = 
                         employeecustomtabfields.custteampracticearea 
                         AND CCV.infocenterarea = /*N*/'Employees' 
                         AND CCV.colname = 'CustTeamPracticeArea' 
                         WHERE  EM.employee = employeecustomtabfields.employee 
                         AND ( CCV.datavalue = /*N*/@PracticeArea )) 
                         AND ( EXISTS (SELECT 'x' 
                         FROM   cfgemployeestatus 
                         WHERE  cfgemployeestatus.status = EM.status 
                         AND (( cfgemployeestatus.status = /*N*/'A' ))) 
                         OR ( ( EM.terminationdate >= CONVERT(DATETIME, Replace( 
                                                      @LastSundayMinus364Days, 
                                                 'T', 
                                                 ' ' 
                                                 ), 121 
                               ) 
                         ) 
                         AND ( EM.terminationdate <= CONVERT(DATETIME, Replace( 
                                   @LastSunday 
                                                     , 
                                                     'T', 
                                                     ' '), 
                                   121) 
                         ) ) ) )) 
                  GROUP  BY ld.employee, 
                            EM.employeecompany 
                  UNION ALL 
                  SELECT 'CUR'                                         AS 
                         lineType, 
                         1                                             AS pdType 
                         , 
                         ld.employee, 
                         EM.employeecompany, 
                         0                                             AS 
                         ReportColumn, 
                         0                                             AS Dir, 
                         0                                             AS 
                         RealizationHrs, 
                         0                                             AS 
                         RealizationAmt, 
                         Sum(ld.reghrs + ld.ovthrs + ld.specialovthrs) AS 
                         NonBillHrs, 
                         0                                             AS InDir 
                  FROM   ld, 
                         emallcompany AS EM 
                         LEFT JOIN organization 
                                ON EM.org = organization.org 
                  WHERE  ( ld.employee = EM.employee ) 
                         AND ( ld.transdate BETWEEN @LastMondayMinus363DaysAndTime 
                                                    AND 
                                                    @LastMondayAndTime ) 
                         AND ( ld.chargetype = 'R' ) 
                         AND ( ld.laborcode LIKE /*N*/'03' ) 
                         AND NOT EXISTS (SELECT 'x' 
                                         FROM   cfgpostcontrol 
                                         WHERE  cfgpostcontrol.company = 
                                                EM.employeecompany 
                                                AND ld.period = 
cfgpostcontrol.period 
                                                AND ld.postseq = 
cfgpostcontrol.postseq 
                                                AND xchargestatus = 'X') 
                         AND Isnull(Substring(ld.emorg, 1, 2), ' ') = 
                             EM.employeecompany 
                         AND (( ( Substring(EM.org, 1, 2) = /*N*/@CompanyId ) 
                                AND EXISTS (SELECT 'x' 
                                            FROM   employeecustomtabfields 
                                    INNER JOIN fw_customcolumnvalues CCV 
                                            ON CCV.code = 
                         employeecustomtabfields.custteampracticearea 
                         AND CCV.infocenterarea = /*N*/'Employees' 
                         AND CCV.colname = 'CustTeamPracticeArea' 
                         WHERE  EM.employee = employeecustomtabfields.employee 
                         AND ( CCV.datavalue = /*N*/@PracticeArea )) 
                         AND ( EXISTS (SELECT 'x' 
                         FROM   cfgemployeestatus 
                         WHERE  cfgemployeestatus.status = EM.status 
                         AND (( cfgemployeestatus.status = /*N*/'A' ))) 
                         OR ( ( EM.terminationdate >= CONVERT(DATETIME, Replace( 
                                                      @LastSundayMinus364Days, 
                                                 'T', 
                                                 ' ' 
                                                 ), 121 
                               ) 
                         ) 
                         AND ( EM.terminationdate <= CONVERT(DATETIME, Replace( 
                                   @LastSunday 
                                                     , 
                                                     'T', 
                                                     ' '), 
                                   121) 
                         ) ) ) )) 
                  GROUP  BY ld.employee, 
                            EM.employeecompany 
                  UNION ALL 
                  SELECT 'CUR'                                         AS 
                         lineType, 
                         1                                             AS pdType 
                         , 
                         ld.employee, 
                         EM.employeecompany, 
                         reportcolumn, 
                         0                                             AS Dir, 
                         0                                             AS 
                         RealizationHrs, 
                         0                                             AS 
                         RealizationAmt, 
                         0                                             AS 
                         NonBillHrs, 
                         Sum(ld.reghrs + ld.ovthrs + ld.specialovthrs) AS InDir 
                  FROM   cfgtimeanalysis, 
                         ld, 
                         emallcompany AS EM 
                         LEFT JOIN organization 
                                ON EM.org = organization.org 
                  WHERE  cfgtimeanalysis.company = EM.employeecompany 
                         AND ( ld.employee = EM.employee ) 
                         AND ( ld.wbs1 BETWEEN cfgtimeanalysis.startwbs1 AND 
                                               cfgtimeanalysis.endwbs1 ) 
                         AND ( reportcolumn > 0 ) 
                         AND ( ld.transdate BETWEEN @LastMondayMinus363DaysAndTime 
                                                    AND 
                                                    @LastMondayAndTime ) 
                         AND ( ld.chargetype <> 'R' ) 
                         AND NOT EXISTS (SELECT 'x' 
                                         FROM   cfgpostcontrol 
                                         WHERE  cfgpostcontrol.company = 
                                                EM.employeecompany 
                                                AND ld.period = 
cfgpostcontrol.period 
                                                AND ld.postseq = 
cfgpostcontrol.postseq 
                                                AND xchargestatus = 'X') 
                         AND Isnull(Substring(ld.emorg, 1, 2), ' ') = 
                             EM.employeecompany 
                         AND (( ( Substring(EM.org, 1, 2) = /*N*/@CompanyId ) 
                                AND EXISTS (SELECT 'x' 
                                            FROM   employeecustomtabfields 
                                    INNER JOIN fw_customcolumnvalues CCV 
                                            ON CCV.code = 
                         employeecustomtabfields.custteampracticearea 
                         AND CCV.infocenterarea = /*N*/'Employees' 
                         AND CCV.colname = 'CustTeamPracticeArea' 
                         WHERE  EM.employee = employeecustomtabfields.employee 
                         AND ( CCV.datavalue = /*N*/@PracticeArea )) 
                         AND ( EXISTS (SELECT 'x' 
                         FROM   cfgemployeestatus 
                         WHERE  cfgemployeestatus.status = EM.status 
                         AND (( cfgemployeestatus.status = /*N*/'A' ))) 
                         OR ( ( EM.terminationdate >= CONVERT(DATETIME, Replace( 
                                                      @LastSundayMinus364Days, 
                                                 'T', 
                                                 ' ' 
                                                 ), 121 
                               ) 
                         ) 
                         AND ( EM.terminationdate <= CONVERT(DATETIME, Replace( 
                                   @LastSunday 
                                                     , 
                                                     'T', 
                                                     ' '), 
                                   121) 
                         ) ) ) )) 
                  GROUP  BY ld.employee, 
                            reportcolumn, 
                            EM.employeecompany 
                  UNION ALL 
                  SELECT 'CUR'                                         AS 
                         lineType, 
                         1                                             AS pdType 
                         , 
                         ld.employee, 
                         EM.employeecompany, 
                         0                                             AS 
                         ReportColumn, 
                         0                                             AS Dir, 
                         0                                             AS 
                         RealizationHrs, 
                         0                                             AS 
                         RealizationAmt, 
                         0                                             AS 
                         NonBillHrs, 
                         Sum(ld.reghrs + ld.ovthrs + ld.specialovthrs) AS InDir 
                  FROM   ld, 
                         emallcompany AS EM 
                         LEFT JOIN organization 
                                ON EM.org = organization.org 
                  WHERE  ( ld.employee = EM.employee ) 
                         AND ( ld.chargetype <> 'R' ) 
                         AND ( ld.transdate BETWEEN @LastMondayMinus363DaysAndTime 
                                                    AND 
                                                    @LastMondayAndTime ) 
                         AND NOT EXISTS (SELECT 'x' 
                                         FROM   cfgtimeanalysis 
                                         WHERE  cfgtimeanalysis.company = 
                                                EM.employeecompany 
                                                AND ld.wbs1 BETWEEN 
                                                    cfgtimeanalysis.startwbs1 
                                                    AND 
                                                    cfgtimeanalysis.endwbs1 
                                                AND reportcolumn > 0) 
                         AND NOT EXISTS (SELECT 'x' 
                                         FROM   cfgpostcontrol 
                                         WHERE  cfgpostcontrol.company = 
                                                EM.employeecompany 
                                                AND ld.period = 
cfgpostcontrol.period 
                                                AND ld.postseq = 
cfgpostcontrol.postseq 
                                                AND xchargestatus = 'X') 
                         AND Isnull(Substring(ld.emorg, 1, 2), ' ') = 
                             EM.employeecompany 
                         AND (( ( Substring(EM.org, 1, 2) = /*N*/@CompanyId ) 
                                AND EXISTS (SELECT 'x' 
                                            FROM   employeecustomtabfields 
                                    INNER JOIN fw_customcolumnvalues CCV 
                                            ON CCV.code = 
                         employeecustomtabfields.custteampracticearea 
                         AND CCV.infocenterarea = /*N*/'Employees' 
                         AND CCV.colname = 'CustTeamPracticeArea' 
                         WHERE  EM.employee = employeecustomtabfields.employee 
                         AND ( CCV.datavalue = /*N*/@PracticeArea )) 
                         AND ( EXISTS (SELECT 'x' 
                         FROM   cfgemployeestatus 
                         WHERE  cfgemployeestatus.status = EM.status 
                         AND (( cfgemployeestatus.status = /*N*/'A' ))) 
                         OR ( ( EM.terminationdate >= CONVERT(DATETIME, Replace( 
                                                      @LastSundayMinus364Days, 
                                                 'T', 
                                                 ' ' 
                                                 ), 121 
                               ) 
                         ) 
                         AND ( EM.terminationdate <= CONVERT(DATETIME, Replace( 
                                   @LastSunday 
                                                     , 
                                                     'T', 
                                                     ' '), 
                                   121) 
                         ) ) ) )) 
                  GROUP  BY ld.employee, 
                            EM.employeecompany 
                  UNION ALL 
                  SELECT 'CUR'                                 AS lineType, 
                         1                                     AS pdType, 
                         ledgermisc.employee, 
                         EM.employeecompany, 
                         0                                     AS ReportColumn, 
                         0                                     AS Dir, 
                         Sum(CASE 
                               WHEN EM.provbillrate IS NULL 
                                     OR EM.provbillrate = 0 THEN 0 
                               ELSE 
realizationamountbillingcurrency / EM.provbillrate 
                             END)                              AS RealizationHrs 
                         , 
                         Sum( 
                  realizationamountbillingcurrency) AS RealizationAmt, 
                         0                                     AS NonBillHrs, 
                         0                                     AS InDir 
                  FROM   ledgermisc 
                         LEFT JOIN un 
                                ON un.unittable = ledgermisc.unittable 
                                   AND un.unit = ledgermisc.unit, 
                         emallcompany AS EM 
                         LEFT JOIN organization 
                                ON EM.org = organization.org 
                  WHERE  ( ledgermisc.employee = EM.employee ) 
                         AND ( ledgermisc.transdate BETWEEN 
                               @LastMondayMinus363DaysAndTime AND 
                                   @LastMondayAndTime ) 
                         AND ( ledgermisc.transtype = 'UN' 
                               AND ledgermisc.subtype = 'B' ) 
                         AND ledgermisc.projectcost = 'Y' 
                         AND ledgermisc.realizationamountbillingcurrency <> 0 
                         AND NOT EXISTS (SELECT 'x' 
                                         FROM   cfgpostcontrol 
                                         WHERE  cfgpostcontrol.company = 
                                                EM.employeecompany 
                                                AND ledgermisc.period = 
                                                    cfgpostcontrol.period 
                                                AND ledgermisc.postseq = 
                                                    cfgpostcontrol.postseq 
                                                AND xchargestatus = 'X') 
                         AND un.employeespecificrevenue = 'Y' 
                         AND Isnull(Substring(ledgermisc.emorg, 1, 2), ' ') = 
                             EM.employeecompany 
                         AND (( ( Substring(EM.org, 1, 2) = /*N*/@CompanyId ) 
                                AND EXISTS (SELECT 'x' 
                                            FROM   employeecustomtabfields 
                                    INNER JOIN fw_customcolumnvalues CCV 
                                            ON CCV.code = 
                         employeecustomtabfields.custteampracticearea 
                         AND CCV.infocenterarea = /*N*/'Employees' 
                         AND CCV.colname = 'CustTeamPracticeArea' 
                         WHERE  EM.employee = employeecustomtabfields.employee 
                         AND ( CCV.datavalue = /*N*/@PracticeArea )) 
                         AND ( EXISTS (SELECT 'x' 
                         FROM   cfgemployeestatus 
                         WHERE  cfgemployeestatus.status = EM.status 
                         AND (( cfgemployeestatus.status = /*N*/'A' ))) 
                         OR ( ( EM.terminationdate >= CONVERT(DATETIME, Replace( 
                                                      @LastSundayMinus364Days, 
                                                 'T', 
                                                 ' ' 
                                                 ), 121 
                               ) 
                         ) 
                         AND ( EM.terminationdate <= CONVERT(DATETIME, Replace( 
                                   @LastSunday 
                                                     , 
                                                     'T', 
                                                     ' '), 
                                   121) 
                         ) ) ) )) 
                  GROUP  BY ledgermisc.employee, 
                            EM.employeecompany 
                  UNION ALL 
                  SELECT 'CUR'    AS lineType, 
                         1        AS pdType, 
                         bild.employee, 
                         EM.employeecompany, 
                         0        AS ReportColumn, 
                         0        AS Dir, 
                         Sum(CASE 
                               WHEN EM.provbillrate IS NULL 
                                     OR EM.provbillrate = 0 THEN 0 
                               ELSE 
realizationamountbillingcurrency / EM.provbillrate 
                             END) AS RealizationHrs, 
                         Sum(CASE 
                               WHEN bild.billstatus = 'T' THEN 0 
                               ELSE realizationamountbillingcurrency 
                             END) AS RealizationAmt, 
                         0        AS NonBillHrs, 
                         0        AS InDir 
                  FROM   bild, 
                         emallcompany AS EM 
                         LEFT JOIN organization 
                                ON EM.org = organization.org 
                  WHERE  ( bild.employee = EM.employee ) 
                         AND ( bild.transdate BETWEEN @LastMondayMinus363DaysAndTime 
                                                      AND 
                                                      @LastMondayAndTime 
                             ) 
                         AND ( bild.transtype = 'TS' ) 
                         AND bild.projectcost = 'Y' 
                         AND bild.realizationamountbillingcurrency <> 0 
                         AND NOT EXISTS (SELECT 'x' 
                                         FROM   cfgpostcontrol 
                                         WHERE  cfgpostcontrol.company = 
                                                EM.employeecompany 
                                                AND bild.period = 
cfgpostcontrol.period 
                                                AND bild.postseq = 
                                                    cfgpostcontrol.postseq 
                                                AND xchargestatus = 'X') 
                         AND Isnull(Substring(bild.emorg, 1, 2), ' ') = 
                             EM.employeecompany 
                         AND (( ( Substring(EM.org, 1, 2) = /*N*/@CompanyId ) 
                                AND EXISTS (SELECT 'x' 
                                            FROM   employeecustomtabfields 
                                    INNER JOIN fw_customcolumnvalues CCV 
                                            ON CCV.code = 
                         employeecustomtabfields.custteampracticearea 
                         AND CCV.infocenterarea = /*N*/'Employees' 
                         AND CCV.colname = 'CustTeamPracticeArea' 
                         WHERE  EM.employee = employeecustomtabfields.employee 
                         AND ( CCV.datavalue = /*N*/@PracticeArea )) 
                         AND ( EXISTS (SELECT 'x' 
                         FROM   cfgemployeestatus 
                         WHERE  cfgemployeestatus.status = EM.status 
                         AND (( cfgemployeestatus.status = /*N*/'A' ))) 
                         OR ( ( EM.terminationdate >= CONVERT(DATETIME, Replace( 
                                                      @LastSundayMinus364Days, 
                                                 'T', 
                                                 ' ' 
                                                 ), 121 
                               ) 
                         ) 
                         AND ( EM.terminationdate <= CONVERT(DATETIME, Replace( 
                                   @LastSunday 
                                                     , 
                                                     'T', 
                                                     ' '), 
                                   121) 
                         ) ) ) )) 
                  GROUP  BY bild.employee, 
                            EM.employeecompany 
                  UNION ALL 
                  SELECT 'CUR' AS lineType, 
                         1     AS pdType, 
                         EM.employee, 
                         EM.employeecompany, 
                         0     AS ReportColumn, 
                         0     AS Dir, 
                         0     AS RealizationHrs, 
                         0     AS RealizationAmt, 
                         0     AS NonBillHrs, 
                         0     AS Indir 
                  FROM   emallcompany AS EM 
                         LEFT JOIN organization 
                                ON EM.org = organization.org 
                  WHERE  EM.employee = EM.employee 
                         AND (( ( Substring(EM.org, 1, 2) = /*N*/@CompanyId ) 
                                AND EXISTS (SELECT 'x' 
                                            FROM   employeecustomtabfields 
                                    INNER JOIN fw_customcolumnvalues CCV 
                                            ON CCV.code = 
                         employeecustomtabfields.custteampracticearea 
                         AND CCV.infocenterarea = /*N*/'Employees' 
                         AND CCV.colname = 'CustTeamPracticeArea' 
                         WHERE  EM.employee = employeecustomtabfields.employee 
                         AND ( CCV.datavalue = /*N*/@PracticeArea )) 
                         AND ( EXISTS (SELECT 'x' 
                         FROM   cfgemployeestatus 
                         WHERE  cfgemployeestatus.status = EM.status 
                         AND (( cfgemployeestatus.status = /*N*/'A' ))) 
                         OR ( ( EM.terminationdate >= CONVERT(DATETIME, Replace( 
                                                      @LastSundayMinus364Days, 
                                                 'T', 
                                                 ' ' 
                                                 ), 121 
                               ) 
                         ) 
                         AND ( EM.terminationdate <= CONVERT(DATETIME, Replace( 
                                   @LastSunday 
                                                     , 
                                                     'T', 
                                                     ' '), 
                                   121) 
                         ) ) ) )) 
                  GROUP  BY EM.employee, 
                            EM.employeecompany 
                 /***StartYTDColumns***//***EndYTDColumns***/ 
                 /***StartUNIONALL***//***EndUNIONALL***/ 
                 /***StartMTDColumns***//***EndMTDColumns***/ 
                 ) emLD 
              ON em.employee = emLD.employee 
                 AND em.employeecompany = emLD.employeecompany 
       LEFT JOIN cfgtimeanalysisheadings 
              ON emLD.reportcolumn = cfgtimeanalysisheadings.reportcolumn 
/***STDHoursStrMulti***/ 
WHERE  (( ( Substring(EM.org, 1, 2) = /*N*/@CompanyId ) 
          AND EXISTS (SELECT 'x' 
                      FROM   employeecustomtabfields 
                             INNER JOIN fw_customcolumnvalues CCV 
                                     ON CCV.code = 
employeecustomtabfields.custteampracticearea 
AND CCV.infocenterarea = /*N*/'Employees' 
AND CCV.colname = 'CustTeamPracticeArea' 
WHERE  EM.employee = employeecustomtabfields.employee 
AND ( CCV.datavalue = /*N*/@PracticeArea )) 
AND ( EXISTS (SELECT 'x' 
FROM   cfgemployeestatus 
WHERE  cfgemployeestatus.status = EM.status 
AND (( cfgemployeestatus.status = /*N*/'A' ))) 
OR ( ( EM.terminationdate >= CONVERT(DATETIME, Replace(@LastSundayMinus364Days, 
                        'T', 
                        ' ' 
                        ), 121 
      ) 
) 
AND ( EM.terminationdate <= CONVERT(DATETIME, Replace( 
          @LastSunday 
                            , 
                            'T', 
                            ' '), 
          121) 
) ) ) )) 
GROUP  BY Isnull(Substring(EM.org, 7, 2), ''), 
          Isnull(em.employee, ''), 
          em.employee, 
          emLD.linetype 
ORDER  BY group1, 
          group2, 
          emLD.linetype

END
GO
