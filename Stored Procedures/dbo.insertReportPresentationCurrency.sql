SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[insertReportPresentationCurrency]
	@presentationCurrencyCode Nvarchar(3), @targetDate datetime, @sessionID varchar(32)
as
declare
@fromCurrencyCode Nvarchar(3),
@triangulationCurrencyCode Nvarchar(3),
@rate float(8),
@rate2 float(8),
@triangulationRate float(8),
@presentationCurrencyDecimalPlaces int,
@presentationCurrencyCurrencySymbol Nvarchar(5),
@lastRun datetime,
@enabledCurrenciesEOF bit,
@foundExchangeRate int,
@presentationCurrencyCursorEOF bit,
@rateErrorMessage Nvarchar(50),
@company Nvarchar(14),
@presentationCurrencyRate float(2),
@lastCompany Nvarchar(14)
 
begin

Select @lastCompany = 'dfltCompForIRPC'

/* obtain the presentationCurrency decimal places and currency symbol */
Declare presentationCurrencyCursor Cursor For 
	Select DecimalPlaces, CurrencySymbol From FW_CFGCurrency Where Code = @PresentationCurrencyCode

Open presentationCurrencyCursor

Fetch Next From presentationCurrencyCursor Into @presentationCurrencyDecimalPlaces, @presentationCurrencyCurrencySymbol
	Select @presentationCurrencyCursorEOF = @@FETCH_STATUS

If @presentationCurrencyCursorEOF = 1
begin
	Select @presentationCurrencyDecimalPlaces = 0
	Select @presentationCurrencyCurrencySymbol = ''
end

Close presentationCurrencyCursor
Deallocate presentationCurrencyCursor

 
/* delete any existing rows from the ReportPresentationCurrency table */
begin tran
	Delete from ReportPresentationCurrency where SessionID = @sessionID  Or DateDiff(hh, LastRun, getDate()) > 24 
commit
 

Select @presentationCurrencyRate = 1


/*enabled currencies cursor */
--NH 08/07/2012: Change to establish exchange rates between all enabled currencies for all companies instead of only for enabled currencies of a company
DECLARE enabledCurrencies CURSOR FOR
Select CFGMainData.Company, EnabledCurrencies.CurrencyCode, CFGMainData.TriangulationCurrencyCode From CFGMainData, (Select distinct CurrencyCode From FW_CFGEnabledCurrencies) EnabledCurrencies
Order By CFGMainData.Company, EnabledCurrencies.CurrencyCode

--Select  CFGMainData.Company, FW_CFGEnabledCurrencies.CurrencyCode, CFGMainData.TriangulationCurrencyCode From CFGMainData, FW_CFGEnabledCurrencies Where CFGMainData.Company = FW_CFGEnabledCurrencies.Company 
--Order By CFGMainData.Company, FW_CFGEnabledCurrencies.CurrencyCode
 
Open enabledCurrencies

FETCH NEXT from enabledCurrencies Into @company, @fromCurrencyCode, @triangulationCurrencyCode
	Select @enabledCurrenciesEOF = @@FETCH_STATUS

/* Loop */
While (@enabledCurrenciesEOF = 0)
BEGIN
	/* establish default values */
	Select @rate = 0
	Select @rate2 = 0
	Select @triangulationRate = 0
	Select @foundExchangeRate = 0
	Select @rateErrorMessage = ''
		
	/* first row entered into the ReportPresentationCurrencyTable will be the Presentation Currency itself */
	/* only insert once for each company */
	if @company <> @lastCompany
	begin
		Insert Into ReportPresentationCurrency(SessionID, Company, FromCurrencyCode, PresentationCurrencyCode, Rate, PresentationCurrencyDecimalPlaces, PresentationCurrencyCurrencySymbol, LastRun) values
		(@sessionID, @company, @presentationCurrencyCode, @presentationCurrencyCode, @presentationCurrencyRate, @presentationCurrencyDecimalPlaces, @presentationCurrencyCurrencySymbol, getDate())
	end

	If @fromCurrencyCode <> @presentationCurrencyCode
	begin

	/* look for a direct exchange rate */
	Execute @foundExchangeRate = GetExchangeRate @fromCurrencyCode, @presentationCurrencyCode,@targetDate, @rate output, 0
 
	if @foundExchangeRate = 1
	 begin 
		 Insert Into ReportPresentationCurrency(SessionID, Company, FromCurrencyCode, PresentationCurrencyCode, Rate, PresentationCurrencyDecimalPlaces, PresentationCurrencyCurrencySymbol, LastRun) values
		  (@sessionID, @company, @fromCurrencyCode, @presentationCurrencyCode, @rate, @presentationCurrencyDecimalPlaces, @presentationCurrencyCurrencySymbol, getDate())
	 end
	 

	/* look for an inverse exchange rate */
	if @foundExchangeRate = 0
	begin
		Execute @foundExchangeRate = GetExchangeRate @presentationCurrencyCode, @fromCurrencyCode, @targetDate, @rate output, 1
 
		if @foundExchangeRate = 1
		 
		begin
			 Insert Into ReportPresentationCurrency(SessionID, Company, FromCurrencyCode, PresentationCurrencyCode, Rate, PresentationCurrencyDecimalPlaces, PresentationCurrencyCurrencySymbol, LastRun) values


			 (@sessionID, @company, @fromCurrencyCode, @presentationCurrencyCode, @rate, @presentationCurrencyDecimalPlaces, @presentationCurrencyCurrencySymbol, getDate())
		end

		/* look for a triangulation exchange rate */

		if @foundExchangeRate = 0
		begin
			if @triangulationCurrencyCode = @fromCurrencyCode /* error in the vb class multicurrency when this occurs but will set the rate to 0 */
			begin
				Select @rate = 0
				Select @foundExchangeRate = 1
				Select @rateErrorMessage = 'Exchange rate not found from ' + @fromCurrencyCode + ' to ' + @presentationCurrencyCode
			end

			if @triangulationCurrencyCode = @presentationCurrencyCode /* error in the vb class multicurrency when this occurs but will set the rate to 0 */
			begin
				Select @rate = 0
				Select @foundExchangeRate = 1
				Select @rateErrorMessage = 'Exchange rate not found from ' + @fromCurrencyCode + ' to ' + @presentationCurrencyCode
			end

			if @triangulationCurrencyCode = '' /* error in the vb class multicurrency when this occurs but will set the rate to 0 */
			begin
				Select @rate = 0
				Select @foundExchangeRate = 1
				Select @rateErrorMessage = 'Exchange rate not found from ' + @fromCurrencyCode + ' to ' + @presentationCurrencyCode
			end

			if @foundExchangeRate = 1 
			begin
				Insert Into ReportPresentationCurrency(SessionID, Company, FromCurrencyCode, PresentationCurrencyCode, Rate, PresentationCurrencyDecimalPlaces, PresentationCurrencyCurrencySymbol, Message, LastRun) values
			 	(@sessionID, @company, @fromCurrencyCode, @presentationCurrencyCode, @rate, @presentationCurrencyDecimalPlaces, @presentationCurrencyCurrencySymbol, @rateErrorMessage, getDate())
			end

			/* leg one - toCurrencyCode = triangulationCurrencyCode */
			if @foundExchangeRate = 0
			begin
				Select @rateErrorMessage = Null

				Execute @foundExchangeRate = GetExchangeRate @fromCurrencyCode, @triangulationCurrencyCode, @targetDate, @rate output, 0
				
				if @foundExchangeRate = 0
				begin
					/* look for an inverse exchange rate */
					Execute @foundExchangeRate = GetExchangeRate @triangulationCurrencyCode, @fromCurrencyCode, @targetDate, @rate output, 1
				end

				if @foundExchangeRate = 0
				 	Select @rateErrorMessage = 'Exchange rate not found from ' + @fromCurrencyCode + ' to ' + @presentationCurrencyCode

				/* leg two - fromCurrencyCode - triangulationCurrencyCode and toCurrencyCode = presentationCurrencyCode */
				Execute @foundExchangeRate = GetExchangeRate @triangulationCurrencyCode, @presentationCurrencyCode, @targetDate, @rate2 output, 0
				
				if @foundExchangeRate = 0
				begin
					/* look for an inverse exchange rate */
					Execute @foundExchangeRate = GetExchangeRate @presentationCurrencyCode, @triangulationCurrencyCode, @targetDate, @rate2 output, 1
				end

				if @foundExchangeRate = 0
					Select @rateErrorMessage = 'Exchange rate not found from ' + @fromCurrencyCode + ' to ' + @presentationCurrencyCode
			
				Select @triangulationRate = @rate * @rate2
					
				Insert Into ReportPresentationCurrency(SessionID, Company, FromCurrencyCode, PresentationCurrencyCode, Rate, PresentationCurrencyDecimalPlaces, PresentationCurrencyCurrencySymbol, Message, LastRun) values
			 	(@sessionID, @company, @fromCurrencyCode, @presentationCurrencyCode, @triangulationRate, @presentationCurrencyDecimalPlaces, @presentationCurrencyCurrencySymbol, @rateErrorMessage, getDate())
			end
		end
	end

	end --If @fromCurrencyCode <> @presentationCurrencyCode
	 
	Select @lastCompany = @company
	FETCH NEXT from enabledCurrencies Into @company, @fromCurrencyCode, @triangulationCurrencyCode
	Select @enabledCurrenciesEOF = @@FETCH_STATUS


END
	
	Close enabledCurrencies
	Deallocate enabledCurrencies
	
END -- InsertReportPresentationCurrency




 

/* SP_KeyNewVal.SQL */
/* Stored Procedure for key convert utility */
/* Last Updated : 09/06/2019 */
/* created for Vision */
/* 09/26/2014 Added 7.4 fields */
/* 09/06/2019 Added N Prefix */

Print ' '
Print 'Drop KeyNewVal procedure (if it exists)...'
GO
