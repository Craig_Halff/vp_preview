SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,> 
-- Modified on May 8, 2018 by Conrad Harrison
-- Modified on Oct 3, 2018 by Jeff Aust
-- =============================================
CREATE   PROCEDURE [dbo].[HAI_QAWarning]
	@WBS1 VARCHAR(30),
    @CustQAReview1Goal VARCHAR(255),
    @CustQAReview2Goal VARCHAR(255),
    @CustQAReview3Goal VARCHAR(255),
    @CustQAReview1Date DATE,
    @CustQAReview2Date DATE,
    @CustQAReview3Date DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @QAWarning1 VARCHAR(255) = 'Missing ' + @CustQAReview1Goal + '%, ' + @CustQAReview2Goal + '%, ' + @CustQAReview3Goal + '%';
	DECLARE @QAWarning2 VARCHAR(255) = 'Missing ' + @CustQAReview2Goal + '%, ' + @CustQAReview3Goal + '%';
	DECLARE @QAWarning3 VARCHAR(255) = 'Missing ' + @CustQAReview3Goal + '%';
	DECLARE @QAWarning4 VARCHAR(255) = 'Missing ' + @CustQAReview1Goal + '%, ' + @CustQAReview2Goal + '%';
	DECLARE @QAWarning5 VARCHAR(255) = 'Missing ' + @CustQAReview2Goal + '%';
	DECLARE @QAWarning6 VARCHAR(255) = 'Missing ' + @CustQAReview1Goal + '%';
	DECLARE @CustQAWarning VARCHAR(255) = '';

	SELECT @CustQAWarning = 
	(
		SELECT TOP 1 
		CASE WHEN CustPercentComplete > @CustQAReview3Goal THEN
			CASE WHEN @CustQAReview3Date IS NULL or CustQAReview3Date = '' THEN
				CASE WHEN @CustQAReview2Date IS NULL or CustQAReview2Date = '' THEN
					CASE WHEN @CustQAReview1Date IS NULL or CustQAReview1Date = ''
						THEN 
							@QAWarning1
						ELSE 
							@QAWarning2
					END
				ELSE
					@QAWarning3
				END
			ELSE
				''
			END
		ELSE
			CASE WHEN CustPercentComplete > @CustQAReview2Goal THEN
				CASE WHEN @CustQAReview3Date IS NULL or CustQAReview3Date = '' THEN
					CASE WHEN @CustQAReview2Date IS NULL or CustQAReview2Date = '' THEN
						CASE WHEN @CustQAReview1Date IS NULL or CustQAReview1Date = '' THEN
							@QAWarning4	
						ELSE
							@QAWarning5
						END
					ELSE
						''
					END
				ELSE
					''
				END
			ELSE
				CASE WHEN CustPercentComplete > @CustQAReview1Goal THEN
					CASE WHEN @CustQAReview3Date IS NULL or CustQAReview3Date = '' THEN
						CASE WHEN @CustQAReview2Date IS NULL or CustQAReview2Date = '' THEN			
							CASE WHEN @CustQAReview1Date IS NULL or CustQAReview1Date = '' THEN
								@QAWarning6
							ELSE 
								''
							END
						ELSE 
							''
						END
					ELSE
						''
					END
				END
			END

		END AS [CustQAWarning]
		FROM 
		(
			-- MODIFY FIELDS AS NECESSARY HERE TO WORK WITH THEM ABOVE
			SELECT WBS1,
			    CAST(REPLACE(CustPercentComplete, '%', '') AS INT) AS CustPercentComplete,
			    CAST(@CustQAReview1Goal AS INT) AS CustQAReview1Goal,
			    CAST(@CustQAReview2Goal AS INT) AS CustQAReview2Goal,
			    CAST(@CustQAReview3Goal AS INT) AS CustQAReview3Goal,
			    @CustQAReview1Date as CustQAReview1Date,
			    @CustQAReview2Date as CustQAReview2Date,
			    @CustQAReview3Date as CustQAReview3Date,
			    CustQAWarning
			FROM ProjectCustomTabFields
			WHERE WBS1 = @WBS1
    		  AND WBS2 = ''
		) AS alpha
	)

	-- UPDATE THE FIELD
	UPDATE ProjectCustomTabFields
	   SET CustQAWarning = @CustQAWarning
	 WHERE WBS1 = @WBS1
	   AND WBS2 = ''

END

GO
