SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- EDIT 2 LINES BELOW BASED ON WHICH UTILIZATION
CREATE Procedure [dbo].[~HAI_spCCG_UtilizationNotification_TeamLeader]
@Email VARCHAR(100),
@RetVal NVARCHAR(MAX) OUTPUT
--COMMENTED TWO CODE LINES OUT BELOW
--CREATE Procedure [dbo].[HAI_spCCG_UtilizationNotification_TeamLeader_TEST]
AS
/*
- Created on 5/17/2018 by Conrad Harrison
- Modified on 6/19/2018 by Conrad Harrison
*/

SET NOCOUNT ON
DECLARE --@Email								varchar (100),
	    @TeamLeader							varchar (100),
		@PreviousTeamLeader					varchar (100),
		@RowCount							integer,
		@Director							varchar (100),
		@OpsMngr							varchar (100),
		@PracticeLeader						varchar (100),
		@MgmtLeader							varchar (100),
		@Office								varchar (100),
		@Practice							varchar (100),
		@LastName							varchar (50),
		@FirstName							varchar (50),
		@PreferredName						varchar (50),
		@Employee							varchar (100),
		@CustTargetRatio					decimal (19,1),
		@CustTargetRatio_FinalTotal      	decimal (19,1),
		@Cust1WeekUtilization				decimal (19,1),
		@Cust1WeekUtilization_FinalTotal	decimal (19,1),
		@Diff1W								decimal (19,1),
		@Diff1W_SubTotal					decimal (19,1),
		@Diff1W_FinalTotal					decimal (19,1),
		@Cust4WeekUtilization				decimal (19,1),
		@Cust4WeekUtilization_FinalTotal	decimal (19,1),
		@Diff4W								decimal (19,1),
		@Diff4W_FinalTotal					decimal (19,1),
		@CustAnnualUtilization				decimal (19,1),
		@CustAnnualUtilization_FinalTotal	decimal (19,1),
		@DiffAnnual							decimal (19,1),
		@DiffAnnual_FinalTotal				decimal (19,1),
		@PercentSymbol						varchar (5),
		@Body								varchar (max),
		@CSS            					varchar (max),
        @Loop1								integer,
		@redText							varchar(50),
		@redTextLW							varchar(50),
		@redTextLWF							varchar(50),  
		@redText4W							varchar(50),  
		@redText4WF							varchar(50),  
		@redText12Wr						varchar(50),  
		@redText12WrF						varchar(50),
		-- ADDING THE FOLLOWING SUBTOTALS IN
		@Cust1WeekHoursTotal				decimal (19,1),
		@Cust1WeekHoursDirect				decimal (19,1),
		@Cust1WeekHoursExpected				decimal (19,1),
		@Cust4WeekHoursTotal				decimal (19,1),
		@Cust4WeekHoursDirect				decimal (19,1),
		@Cust4WeekHoursExpected				decimal (19,1),
		@CustAnnualHoursTotal				decimal (19,1),
		@CustAnnualHoursDirect				decimal (19,1),
		@CustAnnualHoursExpected			decimal (19,1),
		@Cust1WeekUsage						decimal (19,1),
		@Cust4WeekUsage						decimal (19,1),
		@CustAnnualUsage					decimal (19,1),
		@CustTargetRatioUsage				decimal (19,1),
		@CustTargetRatioUsageSum			decimal (19,1),
		@Cust1WeekUsageSum					decimal (19,1),
		@Cust4WeekUsageSum					decimal (19,1),
		@CustAnnualUsageSum					decimal (19,1),
		@FinalTotalHours1Week				decimal (19,1),
		@FinalTotalHours4Week				decimal (19,1),
		@FinalTotalHoursAnnual				decimal (19,1),
		@Cust1WeekHoursBenefit				decimal (19,1),   
		@Cust4WeekHoursBenefit				decimal (19,1),   
		@CustAnnualHoursBenefit				decimal (19,1),
		@Cust1WeekHoursTotalSum				decimal (19,1),
		@Cust4WeekHoursTotalSum				decimal (19,1),
		@CustAnnualHoursTotalSum			decimal (19,1),
		@CustTargetRatio1Week_FinalTotal	decimal (19,1),
		@CustTargetRatio4Week_FinalTotal	decimal (19,1),
		@CustTargetRatioAnnual_FinalTotal	decimal (19,1),
		@CustTargetRatioUsage1WeekSum		decimal (19,1),
		@CustTargetRatio1WeekUsage			decimal (19,1),
		@CustTargetRatioUsage4WeekSum		decimal (19,1),
		@CustTargetRatio4WeekUsage			decimal (19,1),
		@CustTargetRatioUsageAnnualSum		decimal (19,1),
		@CustTargetRatioAnnualUsage			decimal (19,1),

		--**ADDING IN DIRECT TOTALS SUM
		@Cust1WeekHoursDirectSUM			decimal (19,1),
		@Cust4WeekHoursDirectSUM			decimal (19,1),
		@CustAnnualHoursDirectSUM			decimal (19,1),
		@Cust1WeekHoursExpectedMinusBenefitsSum				decimal (19,1),
		@Cust4WeekHoursExpectedMinusBenefitsSum				decimal (19,1),
		@CustAnnualHoursExpectedMinusBenefitsSum				decimal (19,1)

		SET @RowCount = 0
		SET @CustTargetRatio_FinalTotal = 0
		SET @Cust1WeekUtilization_FinalTotal = 0
		SET @Diff1W_FinalTotal = 0
		SET @Cust4WeekUtilization_FinalTotal = 0
		SET @Diff4W_FinalTotal = 0
		SET @CustAnnualUtilization_FinalTotal = 0
		SET @DiffAnnual_FinalTotal = 0
		Set @PercentSymbol = '%'

		-- ADDING THE FOLLOWING SUBTOTALS IN
		SET @Cust1WeekHoursTotal = 0				
		SET @Cust1WeekHoursDirect = 0
		SET @Cust1WeekHoursExpected = 0				
		SET @Cust4WeekHoursTotal = 0				
		SET @Cust4WeekHoursDirect = 0
		SET @Cust4WeekHoursExpected = 0				
		SET @CustAnnualHoursTotal = 0				
		SET @CustAnnualHoursDirect = 0				
		SET @CustAnnualHoursExpected = 0
		SET @Cust1WeekUsage	= 0					
		SET @Cust4WeekUsage	= 0					
		SET @CustAnnualUsage = 0	
		SET @Cust1WeekUsageSum	= 0					
		SET @Cust4WeekUsageSum	= 0					
		SET @CustAnnualUsageSum = 0	
		SET @CustTargetRatioUsage = 0		
		SET @CustTargetRatioUsageSum = 0	
		SET @FinalTotalHours1Week = 0				
		SET @FinalTotalHours4Week = 0				
		SET @FinalTotalHoursAnnual = 0		
		SET @Cust1WeekHoursBenefit = 0			
		SET @Cust4WeekHoursBenefit = 0				
		SET @CustAnnualHoursBenefit = 0		
		SET	@Cust1WeekHoursTotalSum	= 0			
		SET @Cust4WeekHoursTotalSum	= 0			
		SET @CustAnnualHoursTotalSum = 0	
		SET @CustTargetRatio1Week_FinalTotal = 0
		SET @CustTargetRatio4Week_FinalTotal = 0
		SET @CustTargetRatioAnnual_FinalTotal = 0
		SET @CustTargetRatioUsage1WeekSum = 0
		SET @CustTargetRatio1WeekUsage = 0
		SET @CustTargetRatioUsage4WeekSum = 0
		SET @CustTargetRatio4WeekUsage = 0
		SET @CustTargetRatioUsageAnnualSum = 0
		SET @CustTargetRatioAnnualUsage = 0

				--**ADDING IN DIRECT TOTALS SUM
		SET @Cust1WeekHoursDirectSUM = 0
		SET @Cust4WeekHoursDirectSUM = 0
		SET @CustAnnualHoursDirectSUM = 0
		SET @Cust1WeekHoursExpectedMinusBenefitsSum = 0
		SET @Cust4WeekHoursExpectedMinusBenefitsSum = 0
		SET @CustAnnualHoursExpectedMinusBenefitsSum = 0
   
BEGIN

-- CSS style rules

--**COMMENTED STYLING OUT FOR COMBINED PROCEDURE

--	SET @CSS = 
--'
--<style type="text/css">
--	--p {padding-top:15px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;}
--	th {border: 2px solid black;padding: 5px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;font-size:10.5pt;}
--	th {background-color: #808080;color: white;text-align:center;}
--	.team {text-align:left; border-left: 2px solid Black; background-color: white; border-bottom: 1px solid #6E6E6E;}
--	.teamB {font-weight: bold; text-align:left; border-left: 2px solid Black; background-color: white; border-bottom: 1px solid #6E6E6E;}
--	.LW {color: black; background-color: white; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
--	.L4W {color: black; background-color: #e6e6e6; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;} 
--	.L12W {background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
--	.L12Wr {color: black; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-right: 2px solid Black; border-bottom: 1px solid #6E6E6E;}
--	.LWF {color: black; font-weight: bold; background-color: white; text-align:right; border-left: 2px solid Black; border-top: 2px solid Black;}
--	.LWFLeft {font-weight: bold; background-color: white; text-align:left; border-left: 2px solid Black; border-top: 2px solid Black;}
--	.L4WF {color: black; font-weight: bold; background-color: #e6e6e6; text-align:right; border-left: 2px solid Black; border-top: 2px solid Black;} 
--	.L12WF {font-weight: bold; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-top: 2px solid Black;}
--	.L12WrF {color: black; font-weight: bold; background-color: #cccccc; text-align:right; border-left: 2px solid Black; border-right: 2px solid Black; border-top: 2px solid Black;}
--	.redText {color:#FF0000;}

--	</style>
--'

DECLARE curUtilization insensitive cursor for
		SELECT 
			oe.Email, 
			oe.LastName + ', ' + IsNull (oe.PreferredName, oe.FirstName) TeamLeader, 
			e.LastName + ', ' + IsNull (e.PreferredName, e.FirstName) Employee, 
			u.Cust1WeekUtilization, 
			u.CustTargetRatio, 
			--ADDING IN TOTAL HOURS BELOW
			u.Cust1WeekHoursTotal,
			-- ADDING IN DIRECT HOURS BELOW
			u.Cust1WeekHoursDirect,
			-- ADDING IN EXPECTED HOURS BELOW
			u.Cust1WeekHoursExpected,
			-- ADDING IN BENEFIT HOURS BELOW
			u.Cust1WeekHoursBenefit,
			CASE When u.Cust1WeekHoursExpected - u.Cust1WeekHoursBenefit = 0 Then 0 
				Else u.Cust1WeekUtilization - u.CustTargetRatio 
				END Diff1W, 
			u.Cust4WeekUtilization, 
			u.CustTargetRatio,
			--ADDING IN TOTAL HOURS BELOW
			u.Cust4WeekHoursTotal,
			-- ADDING IN DIRECT HOURS BELOW
			u.Cust4WeekHoursDirect,
			-- ADDING IN EXPECTED HOURS BELOW
			u.Cust4WeekHoursExpected,
			-- ADDING IN BENEFIT HOURS BELOW
			u.Cust4WeekHoursBenefit,
			CASE When u.Cust4WeekHoursExpected - u.Cust4WeekHoursBenefit = 0 Then 0 
				Else u.Cust4WeekUtilization - u.CustTargetRatio 
				END Diff4W, 
			u.CustAnnualUtilization, 
			u.CustTargetRatio, 
			--ADDING IN TOTAL HOURS BELOW
			u.CustAnnualHoursTotal,
			-- ADDING IN DIRECT HOURS BELOW
			u.CustAnnualHoursDirect,
			-- ADDING IN EXPECTED HOURS BELOW
			u.CustAnnualHoursExpected,
			-- ADDING IN BENEFIT HOURS BELOW
			u.CustAnnualHoursBenefit,
			CASE When u.CustAnnualHoursExpected - u.CustAnnualHoursBenefit = 0 
				Then 0 Else u.CustAnnualUtilization - u.CustTargetRatio 
				END DiffAnnual
			From UDIC_Utilization u, 
				EM e, 
				EmployeeCustomTabFields  ex
			Left Join EM oe on oe.Employee = ex.CustEKApprover
			Where u.CustEmployeeNumber = e.Employee
				and oe.Email = @Email
				and e.Employee = ex.Employee
			Order by Diff1W ASC

			Set @Body = 
'<table cellpadding=''5'' cellspacing=''0'' style=''border:1px solid black;border-collapse:collapse;''>
	<thead>
	    <tr>
               	<th rowspan="2">Name</th>
                <th colspan="3">Last Week</a></th>
                <th colspan="3">Last 4 Weeks</a></th>
                <th colspan="3">Last 12 Months</a></th>
            </tr>
            <tr>
                <th>Actual</th>
                <th>Goal</th>
                <th>Variance</th>
                <th>Actual</th>
                <th>Goal</th>
                <th>Variance</th>
                <th>Actual</th>
                <th>Goal</th>
                <th>Variance</th>
            </tr>
	</thead>
	<tbody>
'	
      -- Get each Utilization
	OPEN curUtilization
	FETCH NEXT FROM curUtilization INTO @Email
	, @TeamLeader
	, @Employee
	, @Cust1WeekUtilization 
	, @CustTargetRatio
	-- ADDED IN FOUR LINES BELOW
	, @Cust1WeekHoursTotal				
	, @Cust1WeekHoursDirect	
	, @Cust1WeekHoursExpected
	, @Cust1WeekHoursBenefit		
	, @Diff1W
	, @Cust4WeekUtilization
	, @CustTargetRatio
	-- ADDED IN FOUR LINES BELOW
	, @Cust4WeekHoursTotal				
	, @Cust4WeekHoursDirect	
	, @Cust4WeekHoursExpected	
	, @Cust4WeekHoursBenefit
	, @Diff4W
	, @CustAnnualUtilization
	, @CustTargetRatio
	-- ADDED IN FOUR LINES BELOW
	, @CustAnnualHoursTotal				
	, @CustAnnualHoursDirect
	, @CustAnnualHoursExpected
	, @CustAnnualHoursBenefit			
	, @DiffAnnual
	
	Set @Loop1 = @@FETCH_STATUS
	While @Loop1 = 0
						
	BEGIN

				SET @RowCount = @RowCount + 1
				SET @CustTargetRatio1WeekUsage = @Cust1WeekHoursTotal * @CustTargetRatio
				SET @CustTargetRatio4WeekUsage = @Cust4WeekHoursTotal * @CustTargetRatio
				SET @CustTargetRatioAnnualUsage = @CustAnnualHoursTotal * @CustTargetRatio
				
				SET @Cust1WeekUsage =  @Cust1WeekHoursTotal * @Cust1WeekUtilization
				SET @Cust4WeekUsage =  @Cust4WeekHoursTotal * @Cust4WeekUtilization
				SET @CustAnnualUsage =  @CustAnnualHoursTotal * @CustAnnualUtilization
	
				SET @CustTargetRatioUsage1WeekSum = @CustTargetRatioUsage1WeekSum + @CustTargetRatio1WeekUsage
				SET @CustTargetRatioUsage4WeekSum = @CustTargetRatioUsage4WeekSum + @CustTargetRatio4WeekUsage
				SET @CustTargetRatioUsageAnnualSum = @CustTargetRatioUsageAnnualSum + @CustTargetRatioAnnualUsage
				SET @Cust1WeekUsageSum =  @Cust1WeekUsageSum + @Cust1WeekUsage
				SET @Cust4WeekUsageSum =  @Cust4WeekUsageSum + @Cust4WeekUsage
				SET @CustAnnualUsageSum =  @CustAnnualUsageSum + @CustAnnualUsage

				SET	@Cust1WeekHoursTotalSum	= @Cust1WeekHoursTotalSum + @Cust1WeekHoursTotal		
				SET @Cust4WeekHoursTotalSum	= @Cust4WeekHoursTotalSum + @Cust4WeekHoursTotal				
				SET @CustAnnualHoursTotalSum = @CustAnnualHoursTotalSum + @CustAnnualHoursTotal	

				----** CODE BELOW FOR THE NEW WEIGHTED AVG APPROACH

				SET @Cust1WeekHoursDirectSUM = @Cust1WeekHoursDirectSUM + @Cust1WeekHoursDirect
				SET @Cust4WeekHoursDirectSUM = @Cust4WeekHoursDirectSUM + @Cust4WeekHoursDirect
				SET @CustAnnualHoursDirectSUM = @CustAnnualHoursDirectSUM + @CustAnnualHoursDirect
				SET @Cust1WeekHoursExpectedMinusBenefitsSum = @Cust1WeekHoursExpectedMinusBenefitsSum + @Cust1WeekHoursExpected - @Cust1WeekHoursBenefit 
				SET @Cust4WeekHoursExpectedMinusBenefitsSum = @Cust4WeekHoursExpectedMinusBenefitsSum + @Cust4WeekHoursExpected - @Cust4WeekHoursBenefit 
				SET @CustAnnualHoursExpectedMinusBenefitsSum = @CustAnnualHoursExpectedMinusBenefitsSum + @CustAnnualHoursExpected - @CustAnnualHoursBenefit 
							
				Select @redTextLW = (Case
								When @Cust1WeekUtilization < @CustTargetRatio Then ' style="color:#FF0000;"'
								When @Cust1WeekUtilization >= @CustTargetRatio Then '' End)

				Select @redText4W = (Case
								When @Cust4WeekUtilization < @CustTargetRatio Then ' style="color:#FF0000;"'
								When @Cust4WeekUtilization >= @CustTargetRatio Then '' End)

				Select @redText12Wr = (Case
								When @CustAnnualUtilization < @CustTargetRatio Then ' style="color:#FF0000;"'
								When @CustAnnualUtilization >= @CustTargetRatio Then '' End)
	
			Set @Body = @Body + 
            
'				<tr>
                    <td class="team">' + @Employee + '</td> 
                    <td class="LW">' + CAST(@Cust1WeekUtilization AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="LW">' + CAST(@CustTargetRatio AS VARCHAR(10)) + @PercentSymbol + '</td>
					<td class="LW" ' + @redTextLW + '>' + CAST(@Diff1W AS VARCHAR(10)) + @PercentSymbol + '</td>
					<td class="L4W">' + CAST(@Cust4WeekUtilization AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="L4W">' + CAST(@CustTargetRatio AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="L4W" ' + @redText4W + '>' + CAST(@Diff4W AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="L12W">' + CAST(@CustAnnualUtilization AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="L12W">' + CAST(@CustTargetRatio AS VARCHAR(10)) + @PercentSymbol + '</td>
                    <td class="L12Wr" ' + @redText12Wr + '>' + CAST(@DiffAnnual AS VARCHAR(10)) + @PercentSymbol + '</td>
                </tr>
'

	FETCH NEXT FROM curUtilization INTO @Email
	, @TeamLeader
	, @Employee
	, @Cust1WeekUtilization 
	, @CustTargetRatio
	-- ADDED IN FOUR LINES BELOW
	, @Cust1WeekHoursTotal				
	, @Cust1WeekHoursDirect	
	, @Cust1WeekHoursExpected
	, @Cust1WeekHoursBenefit		
	, @Diff1W
	, @Cust4WeekUtilization
	, @CustTargetRatio
	-- ADDED IN FOUR LINES BELOW
	, @Cust4WeekHoursTotal				
	, @Cust4WeekHoursDirect	
	, @Cust4WeekHoursExpected	
	, @Cust4WeekHoursBenefit
	, @Diff4W
	, @CustAnnualUtilization
	, @CustTargetRatio
	-- ADDED IN FOUR LINES BELOW
	, @CustAnnualHoursTotal				
	, @CustAnnualHoursDirect
	, @CustAnnualHoursExpected
	, @CustAnnualHoursBenefit			
	, @DiffAnnual

	Set @Loop1 = @@FETCH_STATUS

	END
	CLOSE curUtilization
	DEALLOCATE curUtilization

	SET @Body = @Body + '</tbody>'

			SET @CustTargetRatio1Week_FinalTotal =  @CustTargetRatioUsage1WeekSum / @Cust1WeekHoursTotalSum
			SET @CustTargetRatio4Week_FinalTotal =  @CustTargetRatioUsage4WeekSum / @Cust4WeekHoursTotalSum
			SET @CustTargetRatioAnnual_FinalTotal =  @CustTargetRatioUsageAnnualSum / @CustAnnualHoursTotalSum
			
			----**NEW WEIGHTED AVERAGE CALCULATION BELOW

			SET @Cust1WeekUtilization_FinalTotal =  (@Cust1WeekHoursDirectSUM / @Cust1WeekHoursExpectedMinusBenefitsSum)*100
			SET @Diff1W_FinalTotal =  (@Cust1WeekUtilization_FinalTotal) - (@CustTargetRatio1Week_FinalTotal)
			SET @Cust4WeekUtilization_FinalTotal =  (@Cust4WeekHoursDirectSUM / @Cust4WeekHoursExpectedMinusBenefitsSum)*100
			SET @Diff4W_FinalTotal =  (@Cust4WeekUtilization_FinalTotal) - (@CustTargetRatio4Week_FinalTotal)
			SET @CustAnnualUtilization_FinalTotal =  (@CustAnnualHoursDirectSUM / @CustAnnualHoursExpectedMinusBenefitsSum)*100
			SET @DiffAnnual_FinalTotal =  (@CustAnnualUtilization_FinalTotal) - (@CustTargetRatioAnnual_FinalTotal)

				Select @redTextLWF = (Case
								When @Cust1WeekUtilization_FinalTotal < @CustTargetRatio1Week_FinalTotal Then ' style="color:#FF0000;"'
								When @Cust1WeekUtilization_FinalTotal >= @CustTargetRatio1Week_FinalTotal Then '' End)

				Select @redText4WF = (Case
								When @Cust4WeekUtilization_FinalTotal < @CustTargetRatio4Week_FinalTotal Then ' style="color:#FF0000;"'
								When @Cust4WeekUtilization_FinalTotal >= @CustTargetRatio4Week_FinalTotal Then '' End)

				Select @redText12WrF = (Case
								When @CustAnnualUtilization_FinalTotal < @CustTargetRatioAnnual_FinalTotal Then ' style="color:#FF0000;"'
								When @CustAnnualUtilization_FinalTotal >= @CustTargetRatioAnnual_FinalTotal Then '' End)
	Set @Body = @Body + 
'
	    <tfoot>
            <tr>
                <td class="LWFleft" >Final Totals</td>
                <td class="LWF">' + CAST(@Cust1WeekUtilization_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="LWF">' + CAST(@CustTargetRatio1Week_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="LWF" ' + @redTextLWF + '>' + CAST(@Diff1W_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L4WF">' + CAST(@Cust4WeekUtilization_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L4WF">' + CAST(@CustTargetRatio4Week_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L4WF" ' + @redText4WF + '>' + CAST(@Diff4W_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L12WF">' + CAST(@CustAnnualUtilization_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L12WF">' + CAST(@CustTargetRatioAnnual_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
                <td class="L12WrF" ' + @redText12WrF + '>' + CAST(@DiffAnnual_FinalTotal AS VARCHAR(10)) + @PercentSymbol + '</td>
            </tr>    
      </tfoot>
	  </table>
	  <br/>
'
	SET @RetVal = @Body;

	--**COMMENTED EMAIL RECIPIENT OUT BELOW FOR COMBINED PROCEDURE

	--Email Recipient Information Below
	--Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, Subject, Body, CreateDateTime, MaxDelay)
	--Values ('Team Leader Utilization', 'charrison@halff.com', 'charrison@halff.com', 'charrison@halff.com', 'Team Leader Utilization', @CSS + @Body, getDate(), 1)
	
END
GO
