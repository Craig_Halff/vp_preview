SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_ProjectTimeByRole] ( @RptStoreId uniqueidentifier, @BeginStages varchar(255), @EndStages varchar(512), @BeginDate datetime, @EndDate datetime, @InclWeekends char(1))
             AS EXEC spCCG_EI_ProjectTimeByRole @RptStoreId,@BeginStages,@EndStages,@BeginDate,@EndDate,@InclWeekends
GO
