SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_LoadProjectInfo] (
	@WBS1	Nvarchar(30),
	@WBS2	Nvarchar(7),
	@WBS3	Nvarchar(7)
) AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_LoadProjectInfo] '2003005.00', ' ', ' '
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	SELECT name, chargetype, ReadyForProcessing
		FROM PR
		WHERE wbs1 = @WBS1 and WBS2 = N' ';

	SELECT name, chargetype, ReadyForProcessing
		FROM PR
		WHERE wbs1 = @WBS1 and (WBS2 = @WBS2 and ISNULL(@WBS2, N'') <> N'') and WBS3 = N' ';

	SELECT name, chargetype, ReadyForProcessing
		FROM PR
		WHERE wbs1 = @WBS1 and (WBS2 = @WBS2 and ISNULL(@WBS2, N'') <> N'') and (WBS3 = @WBS3 and ISNULL(@WBS3, N'') <> N'');
END;


GO
