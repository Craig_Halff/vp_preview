SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spCCG_EI_Load_Stamps]
	@VISION_LANGUAGE varchar(10)
AS
BEGIN
	-- EXEC [dbo].[spCCG_EI_Load_Stamps] 'en-US'
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT T1.Seq, IsNull(IsNull(T2.[Label], T3.[Label]), T1.[Label]) as Label, StampType, DisplayOrder,
			IsNull(IsNull(T2.Content, T3.Content), T1.Content) as Content, PositionType,
			IsNull(IsNull(T2.Width,T3.Width), T1.Width) as Width, ValidRoles,
			SetStage, SetMessage, Status, ModDate, ModUser, PageSet, ShowIn
		FROM CCG_EI_ConfigStamps T1
			LEFT JOIN CCG_EI_ConfigStampsDescriptions T2 on T2.Seq = T1.Seq AND T2.UICultureName = @VISION_LANGUAGE
            LEFT JOIN CCG_EI_ConfigStampsDescriptions T3 on T3.Seq = T1.Seq AND T3.UICultureName = 'en-US'
		ORDER BY DisplayOrder
END
GO
