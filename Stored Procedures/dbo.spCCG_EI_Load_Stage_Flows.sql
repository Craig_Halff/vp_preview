SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_EI_Load_Stage_Flows]
	@VISION_LANGUAGE varchar(10)
AS
BEGIN
	-- EXEC [dbo].[spCCG_EI_Load_Stage_Flows] 'en-US'
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	select T1.StageFlow, T2.StageFlowDescription, T1.Status, T1.Stage, T1.StageOrder, stageFlow.Code as VisionStageFlow
		from CCG_EI_ConfigStageFlows T1
			left join CCG_EI_ConfigStageFlowsDescriptions T2
				ON T2.StageFlow = T1.StageFlow AND T2.Stage = T1.Stage AND T2.UICultureName = 'en-US'
			left join FW_CustomColumnValuesData stageFlow ON (stageFlow.DataValue = T1.StageFlow or stageFlow.Code = T1.StageFlow)
				and InfoCenterArea = 'Projects' and ColName = 'CustInvoiceStageFlow' and stageFlow.UICultureName = @VISION_LANGUAGE
		order by StageFlow, StageOrder
END
GO
