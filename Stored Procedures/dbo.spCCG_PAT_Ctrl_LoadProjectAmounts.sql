SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[spCCG_PAT_Ctrl_LoadProjectAmounts] (
	@custFields					Nvarchar(max),
	@outerApplies				Nvarchar(max),
	@useVisionAmountField		Nvarchar(100),
	@visionAmounts				bit,
	@originatingVendorEnabled	bit,
	@taxAuditingEnabled			bit,
	@vendorID					Nvarchar(20),
	@voucher					Nvarchar(12),
	@seq						int
) AS
BEGIN
	/*
	select * from CCG_PAT_Payable
	select * from CCG_PAT_ProjectAmount
	select * from CCG_PAT_Projectamounttax
		EXEC [DeltekStoredProc_CCG_PAT_Ctrl_LoadProjectAmounts41]
		'','',
			'AmountSourceCurrency',0 , 0,  1, '000000AMEX', '', 44
	EXEC [DeltekStoredProc_CCG_PAT_Ctrl_LoadProjectAmounts41]
		',Cust_1.ColValue as ''Cust_1'', Cust_1.ColDetail as ''Cust_1_DETAIL'',
			dbo.fnCCG_PAT_JTDInvoicedFromVendor_Link(1105,PA.WBS1,PA.WBS2,PA.WBS3) as ''Cust_1_LINK'',
			dbo.fnCCG_PAT_JTDPaidToVendor(1105,PA.WBS1,PA.WBS2,PA.WBS3) as ''Cust_2'',
			dbo.fnCCG_PAT_JTDPaidToVendor_Link(1105,PA.WBS1,PA.WBS2,PA.WBS3) as ''Cust_2_LINK'',
			dbo.fnCCG_PAT_LastPaidDate(1105,PA.WBS1,PA.WBS2,PA.WBS3) as ''Cust_7'',
			dbo.fnCCG_PAT_JTDBilledToClient(1105,PA.WBS1,PA.WBS2,PA.WBS3) as ''Cust_3'',
			Cust_9.ColValue as ''Cust_9'', Cust_9.ColDetail as ''Cust_9_DETAIL'',
			dbo.fnCCG_PAT_PaymentTerms(1105,PA.WBS1,PA.WBS2,PA.WBS3) as ''Cust_8'',
			dbo.fnCCG_PAT_JTDVoucherDetailsLink(1105,PA.WBS1,PA.WBS2,PA.WBS3) as ''Cust_10''',
			' OUTER APPLY dbo.fnCCG_PAT_JTDInvoicedFromVendor_Formatted(1105,PA.WBS1,PA.WBS2,PA.WBS3) as Cust_1 OUTER APPLY dbo.fnCCG_PAT_JTDPaidByClient(1105,PA.WBS1,PA.WBS2,PA.WBS3) as Cust_9',
			'AmountSourceCurrency', 0, 0,  0, '000000AMEX', '', 1105
	*/
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SET NOCOUNT ON;
	DECLARE @sql Nvarchar(max) = N''

    IF @visionAmounts = 1 BEGIN
        SET @sql = N'
            SELECT -5 as PASeq,PA.PKey, PA.WBS1, PA.WBS2, PA.WBS3,
					PA.' + @useVisionAmountField + ' as Amount,
                    null as ExpenseCode, PA.Account as GLAccount,
					PA.SuppressBill, PA.Desc2 as Description, ';

        IF @originatingVendorEnabled = 1
            SET @sql = @sql + '
                    PA.OriginatingVendor, ';


		SET @sql = @sql + '
                    PA.TransactionAmount as NetAmount, PR1.Name as WBS1Name, PR.Name as WBSName,
                    PR.Sublevel, PR.ChargeType, PR.Status, case when
                        PR1.ReadyForProcessing = ''N'' or PR.ReadyForProcessing = ''N'' then ''N''
                        else PR.ReadyForProcessing
                    end as ReadyForProcessing ' +
					@custFields + '
                FROM LedgerAP PA';


    END
	ELSE BEGIN
		SET @sql = @sql + '
            SELECT Seq as PASeq, PA.Pkey,PA.WBS1, PA.WBS2, PA.WBS3, PA.Amount, PA.ExpenseCode,
					PA.GLAccount, PA.SuppressBill, PA.Description, PA.OriginatingVendor,
					PA.TaxAmount, PA.NetAmount,
					PR1.Name as WBS1Name, PR.Name as WBSName, PR.Sublevel, PR.ChargeType,
					PR.Status, case
                        when PR1.ReadyForProcessing = ''N'' or PR.ReadyForProcessing = ''N'' then ''N''
                        else PR.ReadyForProcessing
                    end as ReadyForProcessing ' + @custFields + '
                FROM CCG_PAT_ProjectAmount PA';
    END

	SET @sql = @sql + '
                    LEFT JOIN PR PR1 on PA.WBS1 = PR1.WBS1 and PR1.WBS2 = N'' ''
                    LEFT JOIN PR on PA.WBS1 = PR.WBS1 and PA.WBS2 = PR.WBS2
						and PA.WBS3 = PR.WBS3 ' +
					@outerApplies;

    IF @visionAmounts = 1
		SET @sql = @sql + '
                WHERE PA.Vendor = N''' + @vendorID + '''
					and PA.Voucher = N''' + @voucher + '''
                    and PA.TransType = ''AP'' and PA.SubType = ''L''
					and PA.PostSeq <> 0
                ORDER BY PA.Line';
    ELSE
		SET @sql = @sql + '
                WHERE PA.PayableSeq = ' + CAST(@seq As Nvarchar(100)) + '
                ORDER BY 1 ';

	-- PRINT @sql
	EXEC(@sql);

	IF @taxAuditingEnabled = 1
	BEGIN
		IF @visionAmounts = 1
		BEGIN
			SET @sql = N' SELECT t.*
			FROM LedgerAP PA
			LEFT JOIN LedgerAPTax t
				on PA.Period = t.Period and PA.PostSeq = t.PostSeq and PA.PKey = t.PKey
				WHERE PA.Vendor = N''' + @vendorID + '''
			and PA.Voucher = N''' + @voucher + '''
			and PA.TransType = ''AP'' and PA.SubType = ''L''
			and PA.PostSeq <> 0
			ORDER BY PA.Line,t.Seq,t.TaxCode';

		END
		ELSE
		BEGIN
			SET @sql = N' SELECT *
			FROM CCG_PAT_ProjectAmountTax t
			WHERE PayableSeq = ' + CAST(@seq As Nvarchar(100)) + '
				ORDER BY Pkey,Seq,TaxCode';
		END
		--tax code for second resultset
		PRINT @sql
		EXEC(@sql)
	END
END
GO
