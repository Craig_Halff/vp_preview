SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[DeltekStoredProc_CCG_EMAIL_Config]
AS
BEGIN
	SET NOCOUNT ON;

	select * from CCG_Email_Config
	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CCG_Email_Log]') AND type in (N'U'))
	BEGIN
		exec('insert into CCG_Email_Log (Action) values (''CONFIG_LOAD'')')
	END

END;

GO
