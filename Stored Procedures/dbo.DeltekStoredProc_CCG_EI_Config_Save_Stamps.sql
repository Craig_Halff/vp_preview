SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_EI_Config_Save_Stamps]
	@fieldsSQL			Nvarchar(max),
	@updateValuesSql	Nvarchar(max),
	@insertValuesSql	Nvarchar(max),
	@deleteSeqs			varchar(max),
	@VISION_LANGUAGE	varchar(10)
AS
BEGIN
	/*
	DeltekStoredProc_CCG_EI_Config_Save_Stamps
		@fieldsSQL = 'Label, StampType, DisplayOrder, Content, PositionType, PageSet, Width, ValidRoles, ShowIn, SetStage, SetMessage, Status, ModDate, ModUser',
		@updateValuesSql = '(''Another One'', ''Text'', 0, ''C:\Temp\ANOTHER'', ''TopRight'', ''All'', 25, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 28), (''Bill to budget, write off remaining'', ''Text'', 1, ''Bill to budget, write off remaining'', ''Free'', ''All'', 50, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 26), (''Approved'', ''Text'', 2, ''Approved\nby [:FIRSTNAME] [:LASTNAME]\non [:DATE] at [:TIME]'', ''TopRight'', ''First'', 20, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 15), (''Approved As-Is'', ''Text'', 3, ''Approved As-Is\nby [:FIRSTNAME] [:LASTNAME]\non [:DATE] at [:TIME]'', ''TopRight'', ''First'', 20, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 18), (''Hold Indefinitely'', ''Text'', 10, ''Hold Indefinitely'', ''TopRight'', ''First'', 20, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 16), (''Hold Until Next Month'', ''Text'', 11, ''Hold Until Next Month'', ''TopRight'', ''First'', 20, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 19), (''Hold As Noted'', ''Text'', 12, ''Hold As Noted'', ''TopRight'', ''First'', 20, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 20), (''Hold item'', ''Text'', 15, ''Hold item'', ''Free'', ''All'', 9, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 27), (''----------------'', ''Text'', 19, ''-'', ''TopRight'', ''All'', 25, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 25), (''Check'', ''Image'', 20, ''c:\temp\OK32.png'', ''Free'', ''All'', 5, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 17), (''Check - Red - Small'', ''Image'', 21, ''C:\Temp\Red Check 16x16.gif'', ''Free'', ''All'', 2, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 21), (''Check - Green - Small'', ''Image'', 22, ''C:\Temp\Green Check 16x16.png'', ''Free'', ''All'', 2, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 22), (''Accepted'', ''Image'', 23, ''C:\Temp\Accepted.jpg'', ''Free'', ''All'', 8, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 23), (''Rejected'', ''Image'', 24, ''C:\Temp\Rejected.jpg'', ''Free'', ''All'', 8, '''', ''Main'', '''', '''', ''A'', GetDate(), ''ADMIN'', 24)',
		@insertValuesSql = '',
		@deleteSeqs = '',
		@VISION_LANGUAGE = 'en-US'
	*/
	SET NOCOUNT ON;
	DECLARE @sSQL		Nvarchar(max);
	DECLARE @safeSql	int;
	DECLARE @ids		TABLE (id int);
	DECLARE @i			int;
	DECLARE @newI		int;
	DECLARE @seq		varchar(50);

	-- Ensure that our inputs are safe (preventing SQL injection)
	SET @safeSql = dbo.fnCCG_EI_RegexMatch('<CSV>', @fieldsSQL);
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<INSERT VALUES>|', @updateValuesSql) * 2;
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<INSERT VALUES>|', @insertValuesSql) * 4;
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('(<NUMBER>\|?)*', @deleteSeqs) * 8;
	SET @safeSql = @safeSql | dbo.fnCCG_EI_RegexMatch('<STRING VALUE>', @VISION_LANGUAGE) * 16;
	IF @safeSql < 31 BEGIN
		SELECT 'ERROR: Unsafe parameter(s) used! Execution cancelled. CODE: ' + CAST(@safeSql AS varchar(4));
		RETURN;
	END;

	BEGIN TRANSACTION;
	-- Stamps updates
	IF @updateValuesSql <> '' AND @updateValuesSql IS NOT NULL BEGIN
		SET @sSQL = N'
			UPDATE cs
				SET
					[Label]			= temp.[Label],
					[StampType]		= temp.[StampType],
					[DisplayOrder]	= temp.[DisplayOrder],
					[Content]		= temp.[Content],
					[PositionType]	= temp.[PositionType],
					[Width]			= temp.[Width],
					[ValidRoles]	= temp.[ValidRoles],
					[SetStage]		= temp.[SetStage],
					[SetMessage]	= temp.[SetMessage],
					[Status]		= temp.[Status],
					[ModDate]		= temp.[ModDate],
					[ModUser]		= temp.[ModUser],
					[PageSet]		= temp.[PageSet],
					[ShowIn]		= temp.[ShowIn]
				FROM CCG_EI_ConfigStamps cs
				JOIN (
					VALUES ' + @updateValuesSql + N'
				) temp (' + @fieldsSQL + N', Seq) ON temp.Seq = cs.Seq';
		--PRINT @sSQL;
		EXEC (@sSQL);
	END;

	-- Stamps inserts
	IF @insertValuesSql <> '' AND @insertValuesSql IS NOT NULL BEGIN
		SET @sSQL = N'
			INSERT INTO CCG_EI_ConfigStamps
				(' + @fieldsSQL + N')
				VALUES
				' + @insertValuesSql;
		--PRINT @sSQL;
		EXEC (@sSQL);
	END;

	-- Do Stamps deletes
	SET @i = 0;
	BEGIN TRY
		WHILE @i >= 0 And @i < Len(@deleteSeqs) BEGIN
			SET @newI = CHARINDEX('|', @deleteSeqs, @i + 1);
			IF @newI <= 0 SET @newI = Len(@deleteSeqs) + 1;
			SET @seq = SUBSTRING(@deleteSeqs, @i, @newI - @i);

			--PRINT @seq;
			DELETE FROM CCG_EI_ConfigStamps WHERE Seq = @seq;
			DELETE FROM CCG_EI_ConfigStampsDescriptions WHERE Seq = @seq;

			SET @i = @newI + 1;
		END;
	END TRY
	BEGIN CATCH
		SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_MESSAGE() AS ErrorMessage;
	END CATCH;

	-- Update the multilingual descriptions table
	SET @sSQL = N'
		MERGE CCG_EI_ConfigStampsDescriptions WITH (HOLDLOCK) AS Target
		USING CCG_EI_ConfigStamps AS Source	ON Target.Seq = Source.Seq
		WHEN MATCHED AND Target.UICultureName = '''+@VISION_LANGUAGE+N''' THEN
			UPDATE SET Target.Label = Source.Label, Target.Content = Source.Content
		WHEN NOT MATCHED BY TARGET THEN
			INSERT (Seq, UICultureName, Label, Content)
				VALUES (Source.Seq, '''+@VISION_LANGUAGE+N''', Source.Label, Source.Content)
		WHEN NOT MATCHED BY SOURCE THEN DELETE;';
	EXEC (@sSQL);

	COMMIT TRANSACTION;
END;
GO
