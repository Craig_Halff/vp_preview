SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_stRPFixTPDOnlyOnWBSLevel]
  @strPlanID varchar(32)
AS

BEGIN -- Procedure stRPReCalcPlan
  
  SET NOCOUNT ON

  DECLARE @strTaskID varchar(32)
  DECLARE @strNewAssignmentID varchar(32)
  DECLARE @strNonAllocatedGRCode nvarchar(20)

  -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  --Check if the plan contains Planned and/or Baseline TPD Hours Only On Mapped WBS or Labor Code Row  
 IF NOT EXISTS(
        SELECT 'X'  FROM RPPlannedLabor P 
		INNER JOIN RPTask T ON P.PlanID = T.PlanID AND P.TaskID = T.TaskID AND T.ChildrenCount = 0
		LEFT JOIN RPAssignment A ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID AND (
		A.ResourceID IS NOT NULL OR A.GenericResourceID IS NOT NULL)
		WHERE PeriodHrs > 0 AND P.PlanID = @strPlanID AND P.AssignmentID IS NULL  AND A.AssignmentID IS NULL
        )
  AND NOT EXISTS(
        SELECT 'X'  FROM RPBaselineLabor P 
		INNER JOIN RPTask T ON P.PlanID = T.PlanID AND P.TaskID = T.TaskID AND T.ChildrenCount = 0
		LEFT JOIN RPAssignment A ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID AND (
		A.ResourceID IS NOT NULL OR A.GenericResourceID IS NOT NULL)
		WHERE PeriodHrs > 0 AND P.PlanID = @strPlanID AND P.AssignmentID IS NULL  AND A.AssignmentID IS NULL
        ) RETURN

--Retreve the created unallocated GR Code
  SELECT TOP 1 @strNonAllocatedGRCode = Code 
	FROM GR WHERE Code =  'UNALLOCATED' OR Code = 'UNALLOCATED1' ORDER BY Code DESC

		-- For each WBS level Task, we need create an generic assignment under the WBSLevel
  DECLARE taskCursor CURSOR FOR 
	SELECT  DISTINCT P.TaskID  FROM RPPlannedLabor P 
		INNER JOIN RPTask T ON P.PlanID = T.PlanID AND P.TaskID = T.TaskID AND T.ChildrenCount = 0
		LEFT JOIN RPAssignment A ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID AND (
		A.ResourceID IS NOT NULL OR A.GenericResourceID IS NOT NULL)
		WHERE PeriodHrs > 0 AND P.PlanID = @strPlanID AND P.AssignmentID IS NULL  AND A.AssignmentID IS NULL

	UNION
	SELECT  DISTINCT P.TaskID  FROM RPBaselineLabor P 
		INNER JOIN RPTask T ON P.PlanID = T.PlanID AND P.TaskID = T.TaskID AND T.ChildrenCount = 0
		LEFT JOIN RPAssignment A ON A.PlanID = T.PlanID AND A.TaskID = T.TaskID AND (
		A.ResourceID IS NOT NULL OR A.GenericResourceID IS NOT NULL)
		WHERE PeriodHrs > 0 AND P.PlanID = @strPlanID AND P.AssignmentID IS NULL  AND A.AssignmentID IS NULL

         
	OPEN taskCursor
	FETCH NEXT FROM taskCursor INTO @strTaskID 
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		-- Create an unallocated GR Assignment under this WBS Level task which has no assignment but has TPD 
		SET @strNewAssignmentID = REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '')  

		INSERT RPAssignment(
		AssignmentID,
		PlanID,
		TaskID,
		WBS1,
		WBS2,
		WBS3,
		LaborCode,
		GenericResourceID,
		StartDate,
		EndDate
		) 
		SELECT 
			@strNewAssignmentID,
			T.PlanID,
			T.TaskID,
			T.WBS1,
			T.WBS2,
			T.WBS3,
			LaborCode,
			@strNonAllocatedGRCode,
			T.StartDate,
			T.EndDate
			FROM RPTask T
		WHERE PlanID = @strPlanID AND TaskID = @strTaskID
	  
		--Reset the corresponding TPD under assignment
		UPDATE RPPlannedLabor SET AssignmentID = @strNewAssignmentID WHERE PlanID = @strPlanID AND TaskID = @strTaskID
		UPDATE RPBaselineLabor SET AssignmentID = @strNewAssignmentID WHERE PlanID = @strPlanID AND TaskID = @strTaskID

        UPDATE RPAssignment SET AssignmentID = @strNewAssignmentID WHERE PlanID = @strPlanID AND TaskID = @strTaskID

		FETCH NEXT FROM taskCursor INTO @strTaskID
	END --WHILE @@FETCH_STATUS = 0 
	CLOSE taskCursor
	DEALLOCATE taskCursor

  SET NOCOUNT OFF
     
END -- sp_stRPFixTPDOnlyOnWBSLevel
GO
