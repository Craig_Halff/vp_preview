SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_ProjectTotalCompensation] @WBS1 varchar(30) = '%'
As
/*
Copyright 2021 Central Consulting Group.   All rights reserved.
09/07/2021	David Springer
			Calculate Total Compensation
			Call this from any workflow or procedure that is changeing the Contract Details/Fee fields
			Called in spCCG_ProjectFeeAdjustment, spCCG_ProjectContractMgmtInsert
*/
BEGIN

	Update px
	Set px.CustTotalCompensation = IsNull (c.Total, 0)
	From ProjectCustomTabFields px
		Left Join -- must be lowest level
		(Select c.WBS1, Sum (d.Total) Total
		From Contracts c, ContractDetails d, PR p WITH (NOLOCK)
		Where c.ContractNumber = d.ContractNumber
		  and c.WBS1 = d.WBS1
		  and c.FeeIncludeInd = 'Y'
		  and d.WBS1 = p.WBS1
		  and d.WBS2 = p.WBS2
		  and d.WBS3 = p.WBS3
		  and p.SubLevel = 'N'
		Group by c.WBS1) c on px.WBS1 = c.WBS1
	Where px.WBS1 like @WBS1
	  and px.WBS2 = ' '
	  and px.CustTotalCompensation <> IsNull (c.Total, 0)
END
GO
