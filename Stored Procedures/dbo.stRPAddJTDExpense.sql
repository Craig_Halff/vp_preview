SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPAddJTDExpense]
  @strRowID nvarchar(255),
  @bitCalledFromRM bit = 0
AS

BEGIN -- Procedure stRPAddJTDExpense
      
SET NOCOUNT ON

  DECLARE @dtETCDate datetime
  DECLARE @dtJTDDate datetime

  DECLARE @intOutlineLevel int
   
  DECLARE @strCompany nvarchar(14)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strTopTaskID varchar(32)
  DECLARE @strWBS1 nvarchar(30)
  DECLARE @strUserName nvarchar(32)

  DECLARE @intRowCount int 

  DECLARE @intExpWBSLevel int 

  -- Declare Temp tables.
  
  DECLARE @tabWBS TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    ParentOutlineNumber varchar(255) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    ChildrenCount int,
    OutlineLevel int,
    StartDate datetime,
    EndDate datetime,
    HasLBCD bit
    UNIQUE (WBS1, WBS2, WBS3, ParentOutlineNumber, OutlineNumber, HasLBCD)
  )

  DECLARE @tabExpense TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    Account nvarchar(13) COLLATE database_default,
    Vendor nvarchar(20) COLLATE database_default,
    JTDCost decimal(19,4),	
    JTDBill decimal(19,4),
    DirectAcctFlg varchar(1) COLLATE database_default
    UNIQUE (RowID, WBS1, WBS2, WBS3, Account, Vendor)
  )

   DECLARE @tabExpenseTask TABLE (
    RowID int IDENTITY(1,1),
    WBS1 nvarchar(30) COLLATE database_default,
    WBS2 nvarchar(30) COLLATE database_default,
    WBS3 nvarchar(30) COLLATE database_default,
    OutlineNumber varchar(255) COLLATE database_default,
    Account nvarchar(13) COLLATE database_default,
    Vendor nvarchar(20) COLLATE database_default,
    DirectAcctFlg varchar(1) COLLATE database_default
    UNIQUE (RowID, WBS1, WBS2, WBS3, OutlineNumber, Account, Vendor)
  )

  DECLARE @tabInserted TABLE (
    PlanID varchar(32) COLLATE database_default,
    TaskID varchar(32) COLLATE database_default,
    ExpenseID VARCHAR(32) COLLATE database_default
    UNIQUE (PlanID, TaskID, ExpenseID)
  )

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

  SELECT 
    @dtJTDDate = ISNULL(JTDDate, DATEADD(DAY, 0, DATEDIFF(DAY, '', GETDATE())))
    FROM CFGRMSettings

  -- Set Dates

  SET @dtETCDate = DATEADD(DAY, 1, @dtJTDDate)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Parse @strRowID
  -- @strRowID is a string with the following format
  --   1. For an Assignment row, @strRowID = 'E~<PNAssignment.ResourceID>|<PNTask.TaskID>
  --   2. For an Assignment row, @strRowID = 'G~<PNAssignment.GenericResourceID>|<PNTask.TaskID>'
  --   3. For a WBS row, @strRowID = '|<PNTask.TaskID>'

  -- Parsing for TaskID.

  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

  -- Setting various Plan parameter.

  SELECT 
    @strCompany = P.Company,
    @strPlanID = PT.PlanID,
    @strWBS1 = PT.WBS1,
    @strTopTaskID = TT.TaskID
    FROM PNTask AS PT 
      LEFT JOIN PNTask AS TT ON PT.PlanID = TT.PlanID AND TT.OutlineLevel = 0
      INNER JOIN PNPlan AS P ON PT.PlanID = P.PlanID
    WHERE PT.TaskID = @strTaskID

  -- If @strRowID is not for the top-most Task row then leave the SP.
  -- We only need to execute this SP for the top-most Task row.
  -- If there is no data in LD table for the given WBS1 then leave the SP.

  IF((@strTaskID <> @strTopTaskID) OR  (
   (NOT EXISTS(SELECT 'X' FROM LedgerAP WHERE WBS1 = @strWBS1)) AND (NOT EXISTS(SELECT 'X' FROM LedgerAR WHERE WBS1 = @strWBS1)) 
   AND (NOT EXISTS(SELECT 'X' FROM LedgerEX WHERE WBS1 = @strWBS1)) AND (NOT EXISTS(SELECT 'X' FROM LedgerMisc WHERE WBS1 = @strWBS1))
   AND (NOT EXISTS(SELECT 'X' FROM POCommitment WHERE WBS1 = @strWBS1)))) RETURN 

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

-- get the Expense WBS Level
  SELECT @intExpWBSLevel = ExpWBSLevel FROM PNPlan WHERE PlanID = @strPlanID

  INSERT @tabWBS(
    PlanID,
    TaskID,
    WBS1,
    WBS2,
    WBS3,
    ParentOutlineNumber,
    OutlineNumber,
    ChildrenCount,
    OutlineLevel,
    StartDate,
    EndDate,
    HasLBCD
  )
    SELECT
      T.PlanID AS PlanID,
      T.TaskID AS TaskID,
      T.WBS1 AS WBS1,
      ISNULL(T.WBS2, ' ') AS WBS2,
      ISNULL(T.WBS3, ' ') AS WBS3,
      T.ParentOutlineNumber AS ParentOutlineNumber,
      T.OutlineNumber AS OutlineNumber,
      T.ChildrenCount AS ChildrenCount,
      T.OutlineLevel + 1 As OutLineLevel,
      T.StartDate AS StartDate,
      T.EndDate AS EndDate,
      CASE
        WHEN (EXISTS(SELECT 'X' FROM PNTask AS LT WHERE LT.PlanID = @strPlanID AND T.OutlineNumber = LT.ParentOutlineNumber AND LT.WBSType = 'LBCD'))
        THEN 1
        ELSE 0
      END AS HasLBCD
      FROM PNTask AS T
      WHERE T.PlanID = @strPlanID AND T.WBSType IN ('WBS1', 'WBS2', 'WBS3')

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  INSERT @tabExpense(
    WBS1,
    WBS2,
    WBS3,
    Account,
    Vendor,
    JTDCost,
    JTDBill,
    DirectAcctFlg
  )
    SELECT
      X.WBS1 AS WBS1,
      X.WBS2 AS WBS2,
      X.WBS3 AS WBS3,
      X.Account AS Account,
      X.Vendor AS Vendor,
      SUM(X.JTDCost) AS JTDCost,
      SUM(X.JTDBill) AS JTDBill,
      X.DirectAcctFlg AS DirectAcctFlg
      FROM (
        SELECT
          Ledger.WBS1 AS WBS1,
          Ledger.WBS2 AS WBS2,
          Ledger.WBS3 AS WBS3,
          CA.Account AS Account,
          Ledger.Vendor AS Vendor,
          SUM(Ledger.AmountProjectCurrency) AS JTDCost,
          SUM(Ledger.BillExt) AS JTDBill,
          CASE WHEN CA.Type = 7 THEN 'Y' ELSE 'N' END AS DirectAcctFlg
          FROM LedgerAP Ledger 		   
            INNER JOIN CA ON Ledger.Account = CA.Account 
          WHERE Ledger.WBS1 = @strWBS1 
            AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate
            AND CA.Type IN (5, 7, 9)
          GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, CA.Account, Ledger.Vendor, CA.Type
        UNION ALL
        SELECT
          Ledger.WBS1 AS WBS1,
          Ledger.WBS2 AS WBS2,
          Ledger.WBS3 AS WBS3,
          CA.Account AS Account,
          Ledger.Vendor AS Vendor,
          SUM(Ledger.AmountProjectCurrency) AS JTDCost,
          SUM(Ledger.BillExt) AS JTDBill,
          CASE WHEN CA.Type = 7 THEN 'Y' ELSE 'N' END AS DirectAcctFlg
          FROM LedgerEX Ledger 		   
            INNER JOIN CA ON Ledger.Account = CA.Account 
          WHERE Ledger.WBS1 = @strWBS1 
            AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate
            AND CA.Type IN (5, 7, 9)
          GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, CA.Account, Ledger.Vendor, CA.Type
        UNION ALL
        SELECT
          Ledger.WBS1 AS WBS1,
          Ledger.WBS2 AS WBS2,
          Ledger.WBS3 AS WBS3,
          CA.Account AS Account,
          Ledger.Vendor AS Vendor,
          SUM(Ledger.AmountProjectCurrency) AS JTDCost,
          SUM(Ledger.BillExt) AS JTDBill,
          CASE WHEN CA.Type = 7 THEN 'Y' ELSE 'N' END AS DirectAcctFlg
          FROM LedgerAR Ledger 		   
            INNER JOIN CA ON Ledger.Account = CA.Account 
          WHERE Ledger.WBS1 = @strWBS1 
            AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate
            AND CA.Type IN (5, 7, 9)
          GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, CA.Account, Ledger.Vendor, CA.Type
        UNION ALL
        SELECT
          Ledger.WBS1 AS WBS1,
          Ledger.WBS2 AS WBS2,
          Ledger.WBS3 AS WBS3,
          CA.Account AS Account,
          Ledger.Vendor AS Vendor,
          SUM(Ledger.AmountProjectCurrency) AS JTDCost,
          SUM(Ledger.BillExt) AS JTDBill,
          CASE WHEN CA.Type = 7 THEN 'Y' ELSE 'N' END AS DirectAcctFlg
          FROM LedgerMisc Ledger 		   
            INNER JOIN CA ON Ledger.Account = CA.Account 
          WHERE Ledger.WBS1 = @strWBS1 
            AND Ledger.ProjectCost = 'Y' AND Ledger.TransDate <= @dtJTDDate
            AND CA.Type IN (5, 7, 9)
          GROUP BY Ledger.WBS1, Ledger.WBS2, Ledger.WBS3, CA.Account, Ledger.Vendor, CA.Type
        UNION ALL
        SELECT
          POC.WBS1 AS WBS1,
          POC.WBS2 AS WBS2,
          POC.WBS3 AS WBS3,
          POC.Account AS Account,
          POM.Vendor AS Vendor,
          SUM(POC.AmountProjectCurrency) AS JTDCost,
          SUM(POC.BillExt) AS JTDBill,
          CASE WHEN CA.Type = 7 THEN 'Y' ELSE 'N' END AS DirectAcctFlg
          FROM POCommitment AS POC 
            INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NULL)
            INNER JOIN POMaster AS POM ON POD.MasterPKey = POM.MasterPKey
            INNER JOIN CA ON POC.Account = CA.Account
          WHERE POM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (5, 7, 9)  
          GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor, CA.Type
        UNION ALL
        SELECT
          POC.WBS1 AS WBS1,
          POC.WBS2 AS WBS2,
          POC.WBS3 AS WBS3,
          POC.Account AS Account,
          POM.Vendor AS Vendor,
          SUM(POC.AmountProjectCurrency) AS JTDCost,
          SUM(POC.BillExt) AS JTDBill,
          CASE WHEN CA.Type = 7 THEN 'Y' ELSE 'N' END AS DirectAcctFlg
          FROM POCommitment AS POC 
            INNER JOIN PODetail AS POD ON (POC.PODetailPKey = POD.PODetailPKey AND COPKey IS NOT NULL)
            INNER JOIN POCOMaster AS POCOM ON POD.COPKey = POCOM.PKey
            INNER JOIN POMaster AS POM ON POCOM.MasterPKey = POM.MasterPKey
            INNER JOIN CA ON POC.Account = CA.Account
          WHERE POCOM.OrderDate <= @dtJTDDate AND POC.WBS1 = @strWBS1 AND CA.Type IN (5, 7, 9) 
          GROUP BY POC.WBS1, POC.WBS2, POC.WBS3, POC.Account, POM.Vendor, CA.Type
      ) AS X
      GROUP BY X.WBS1, X.WBS2, X.WBS3, X.Account, X.Vendor, X.DirectAcctFlg

  -- Grab the Task rows which are less than or equal to @intExpWBSLevel.
  -- A Project WBS Tree may have lesser levels than the @intExpWBSLevel.

  INSERT @tabExpenseTask(
    WBS1,
    WBS2,
    WBS3,
    OutlineNumber,
    Account,
    Vendor,
    DirectAcctFlg
  )
    SELECT DISTINCT
      X.WBS1 AS WBS1,
      X.WBS2 AS WBS2,
      X.WBS3 AS WBS3,
      X.OutlineNumber AS OutlineNumber,
      X.Account AS Account,
      X.Vendor AS Vendor,
      X.DirectAcctFlg
      FROM (
        SELECT DISTINCT
          W.WBS1 AS WBS1,
          W.WBS2 AS WBS2,
          W.WBS3 AS WBS3,
          W.OutlineNumber AS OutlineNumber,
          W.OutlineLevel AS OutlineLevel,
          ZLE.Account AS Account,
          ZLE.Vendor AS Vendor,
          ZLE.DirectAcctFlg,
          CASE
            WHEN OutlineLevel < @intExpWBSLevel
            THEN
              CASE
                WHEN (ChildrenCount > 0 AND HasLBCD = 1) THEN 1
                WHEN (ChildrenCount > 0 AND HasLBCD = 0) THEN 0
                ELSE 1
              END
            ELSE 1
          END AS IsLeaf
          FROM  @tabWBS AS W
            INNER JOIN @tabExpense AS ZLE 
              ON ZLE.WBS1 = W.WBS1 
                AND ZLE.WBS2 LIKE  CASE WHEN W.WBS2 = ' ' THEN '%' ELSE W.WBS2 + '%' END
                AND ZLE.WBS3 LIKE  CASE WHEN W.WBS3 = ' ' THEN '%' ELSE W.WBS3 + '%' END
                AND W.OutlineLevel <= @intExpWBSLevel  
          WHERE JTDCost <> 0 OR JTDBill <> 0
      ) AS X
      WHERE X.IsLeaf = 1

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

BEGIN TRANSACTION

  -- Find and insert Un-Planned resources (with JTD) into PNExpense table.

  INSERT PNExpense(
    PlanID,
    TaskID,
    ExpenseID,
    WBS1,
    WBS2,
    WBS3,
    Account,
    Vendor,
    DirectAcctFlg,
    StartDate,
    EndDate
  )
    OUTPUT
      INSERTED.PlanID,
      INSERTED.TaskID,
      INSERTED.ExpenseID
      INTO @tabInserted(
        PlanID,
        TaskID,
        ExpenseID
      )
    SELECT  
      @strPlanID AS PlanID,
      T.TaskID AS TaskID,
      REPLACE(CAST(NEWID() AS VARCHAR(36)), '-', '') AS ExpenseID,
      LE.WBS1 AS WBS1,
      CASE WHEN LE.WBS2 = ' ' Then NULL ELSE LE.WBS2 END AS WBS2,
      CASE WHEN LE.WBS3 = ' ' Then NULL ELSE LE.WBS3 END AS WBS3,
      LE.Account AS Account,
      LE.Vendor AS Vendor,
      LE.DirectAcctFlg,
      T.StartDate AS StartDate,
      T.EndDate AS EndDate
      FROM (
        SELECT DISTINCT
          WBS1 AS WBS1,
          WBS2 AS WBS2,
          WBS3 AS WBS3,
          OutlineNumber AS OutlineNumber,
          Account AS Account,
          Vendor AS Vendor,
          DirectAcctFlg
          FROM @tabExpenseTask AS XLE
      ) AS LE
        LEFT JOIN PNExpense AS E ON LE.WBS1 = E.WBS1 AND LE.WBS2 = ISNULL(E.WBS2, ' ') AND LE.WBS3 = ISNULL(E.WBS3, ' ') 
          AND LE.Account = E.Account AND ISNULL(LE.Vendor, '') = ISNULL(E.Vendor, '') 
          AND E.Account IS NOT NULL AND E.PlanID = @strPlanID 
        INNER JOIN @tabWBS AS T ON LE.OutlineNumber = T.OutlineNumber
      WHERE E.ExpenseID IS NULL
     
  -- Determine whether any row was inserted.

  SET @intRowCount = @@ROWCOUNT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Find and insert Un-Planned resources (with JTD) into RPExpense table.

  IF (@bitCalledFromRM = 1 AND @intRowCount > 0)
    BEGIN

      INSERT RPExpense(
        PlanID,
        TaskID,
        ExpenseID,
        WBS1,
        WBS2,
        WBS3,
        Account,
        Vendor,
        DirectAcctFlg,
        StartDate,
        EndDate
      )
        SELECT  
          PE.PlanID AS PlanID,
          PE.TaskID AS TaskID,
          PE.ExpenseID AS ExpenseID,
          PE.WBS1 AS WBS1,
          PE.WBS2 AS WBS2,
          PE.WBS3 AS WBS3,
          PE.Account AS Account,
          PE.Vendor AS Vendor,
          PE.DirectAcctFlg AS DirectAcctFlg,
          PE.StartDate AS StartDate,
          PE.EndDate AS EndDate
          FROM PNExpense AS PE
            INNER JOIN @tabInserted AS I
              ON PE.PlanID = I.PlanID AND PE.TaskID = I.TaskID AND PE.ExpenseID = I.ExpenseID

    END /* END IF (@bitCalledFromRM = 1 AND @intRowCount > 0) */

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  IF (@intRowCount > 0)
    BEGIN
      -- Update VesionID.
      EXECUTE dbo.stRPUpdateVersionID @strPlanID, @bitCalledFromRM,0
      -- Set PNPlan.LastPlanAction = 'SAVED'
      UPDATE PNPlan SET LastPlanAction = 'SAVED' WHERE PlanID = @strPlanID
    END /* END IF (@intRowCount > 0) */
         
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  COMMIT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SET NOCOUNT OFF

END -- stRPAddJTDExpense

GO
