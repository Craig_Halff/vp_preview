SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_Main_FormUltra_Shown]
	@EMPLOYEEID		Nvarchar(20)
AS
BEGIN
	-- EXEC [DeltekStoredProc_CCG_PAT_Main_FormUltra_Shown] ''
	SET NOCOUNT ON;
	SELECT seq, Detail
        FROM CCG_PAT_ConfigOptions
        WHERE changedBy = @EMPLOYEEID and type = 'GridOptions'
        ORDER BY datechanged desc
END;

GO
