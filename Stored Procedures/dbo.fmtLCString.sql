SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[fmtLCString]
   @LCSplit    Nvarchar(5),
   @ETLCTable  Nvarchar(30),
   @IDStr      Nvarchar(50)  output,
   @NameStr    Nvarchar(50)  output,
   @CodeStr    Nvarchar(255) output,
   @LabelStr   Nvarchar(255) output,
   @FromStr    Nvarchar(255) output
as
declare
   @count int
begin
    if @LCSplit is null
      return 1
    select @count = 1
    select @CodeStr = '',
           @LabelStr = '',
           @fromSTr  = ''
    while @count <= len(@LCSplit)
      Begin
         select @CodeStr  = @CodeStr + 'CFGLC' + substring(@LCSplit, @count, 1) + '.code + '
         select @LabelStr = @LabelStr + 'CFGLC' + substring(@LCSplit, @count, 1) + '.label + ''/'' + '
--         select @FromStr  = @FromStr + 'CFGLC' + substring(@LCSplit, @count, 1) + ','
         select @FromStr  = @FromStr + '(SELECT Code, Label FROM CFGLCCodes WHERE LCLevel = ' + substring(@LCSplit, @count, 1) + ') CFGLC' + substring(@LCSplit, @count, 1) + ','
         select @count    = @count + 1
      End
    select @CodeStr  = substring(@CodeStr,1, len(@CodeStr) - 2)    --  remove this "+ "
    select @LabelStr = 'substring(' + substring(@LabelStr,1, len(@LabelStr) - 8) + ',1,30)' --  remove this "+ '/' + '"
    select @FromStr  = substring(@FromStr,1, len(@FromStr) - 1)    --  remove this ","

/*udt04 has an extra field rate that cannot be null, has to be handled individually*/

    select @CodeStr = case @ETLCTable
                      when 'ET_UDT04' THEN @CodeStr + ',0'
                      when 'ET_UDT07' THEN @CodeStr + ',0,0'
                      else @CodeStr
                      end

    select @IDStr = case @ETLCTable
                     when 'ET_UDT09' THEN 'UDT09_ID'
                     when 'ET_UDT07' THEN 'UDT07_ID, UDT07_RATE ,labor_rate'
                     when 'ET_UDT03' THEN 'UDT03_ID'
                     when 'ET_UDT04' THEN 'UDT04_ID, UDT04_RATE'
                     when 'ET_UDT05' THEN 'UDT05_ID'
                     when 'ET_UDT06' THEN 'UDT06_ID'
                    end
    select @NameStr = case @ETLCTable
                     when 'ET_UDT09' THEN 'UDT09_NAME'
                     when 'ET_UDT07' THEN 'UDT07_NAME'
                     when 'ET_UDT03' THEN 'UDT03_NAME'
                     when 'ET_UDT04' THEN 'UDT04_NAME'
                     when 'ET_UDT05' THEN 'UDT05_NAME'
                     when 'ET_UDT06' THEN 'UDT06_NAME'
                    end
end
GO
