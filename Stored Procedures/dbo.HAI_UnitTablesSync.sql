SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--SET QUOTED_IDENTIFIER ON|OFF
--SET ANSI_NULLS ON|OFF
--GO
CREATE PROCEDURE [dbo].[HAI_UnitTablesSync]
AS
BEGIN

    /***************************************************************************
    Copyright (c) 2021 Halff Associates Inc. All rights reserved.
    Description line

    01/05/2021	Craig H. Anderson - Created
	****************************************************************************/

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    DECLARE @standardTable varchar(30) = '000001 - STANDARD UNIT TABLE';

    BEGIN
        --RAISERROR ('Syncing Units in all  Unit Tables to Standard Unit Table values', 0, 1) WITH NOWAIT;
        -- ITEM 8a
        UPDATE  uu1
		SET   uu1.SingleLabel = uu2.SingleLabel
			, uu1.PluralLabel = uu2.PluralLabel
			, uu1.Format = uu2.Format
			, uu1.ConsolBill = uu2.ConsolBill
			, uu1.ShowDate = uu2.ShowDate
			, uu1.ShowCalc = uu2.ShowCalc
			, uu1.PostAccount = CASE
                        WHEN uu1.UnitType = 'Labor'
                            AND uu1.BillingRate <> 0 THEN
                            '401.00'
                        WHEN uu1.UnitType <> 'Labor'
                            AND uu1.BillingRate <> 0 THEN
                            '402.00'
                        ELSE
                            NULL
						END
			, uu1.CostRate = uu2.CostRate
			, uu1.RegAccount = uu2.RegAccount
			, uu1.OHAccount = uu1.OHAccount
			, uu1.CreditWBS1 = uu2.CreditWBS1
			, uu1.CreditWBS2 = uu2.CreditWBS2
			, uu1.CreditWBS3 = uu2.CreditWBS3
			, uu1.CreditAccount = uu2.CreditAccount
			, uu1.ConsolCost = uu2.ConsolCost
			, uu1.AvailableForTimesheet = uu2.AvailableForTimesheet
			, uu1.EmployeeSpecificRevenue = uu2.EmployeeSpecificRevenue
			, uu1.Item = uu2.Item
			, uu1.UnitType = uu2.UnitType
		FROM dbo.UNUnit AS uu1
			INNER JOIN dbo.UNUnit AS uu2
				ON uu1.Unit = uu2.Unit
				AND  uu2.UnitTable = @standardTable
			INNER JOIN dbo.UNTable ut
				ON uu1.UnitTable = ut.UnitTable
        WHERE   uu1.UnitTable <> @standardTable
                AND RIGHT(uu1.Unit, 1) IN ( 'C', 'H', 'M' )
                AND ut.Company = '01';

        -- ITEM 8b
        UPDATE  uu1
		SET   uu1.SingleLabel = uu2.SingleLabel
			, uu1.PluralLabel = uu2.PluralLabel
			, uu1.Format = uu2.Format
			, uu1.ConsolBill = uu2.ConsolBill
			, uu1.ShowDate = uu2.ShowDate
			, uu1.ShowCalc = uu2.ShowCalc
			, uu1.PostAccount = CASE
                        WHEN uu1.UnitType = 'Labor'
                            AND uu1.BillingRate <> 0 THEN
                            '401.00'
                        WHEN uu1.UnitType <> 'Labor'
                            AND uu1.BillingRate <> 0 THEN
                            '402.00'
                        ELSE
                            NULL
						END
			, uu1.CostRate = uu2.CostRate
			, uu1.RegAccount = uu2.RegAccount
			, uu1.OHAccount = uu1.OHAccount
			, uu1.CreditWBS1 = uu2.CreditWBS1
			, uu1.CreditWBS2 = uu2.CreditWBS2
			, uu1.CreditWBS3 = uu2.CreditWBS3
			, uu1.CreditAccount = uu2.CreditAccount
			, uu1.ConsolCost = uu2.ConsolCost
			, uu1.AvailableForTimesheet = uu2.AvailableForTimesheet
			, uu1.EmployeeSpecificRevenue = uu2.EmployeeSpecificRevenue
			, uu1.Item = uu2.Item
			, uu1.UnitType = uu2.UnitType
		FROM dbo.UNUnit AS uu1
			INNER JOIN dbo.UNUnit AS uu2
				ON uu1.Unit = uu2.Unit
				AND  uu2.UnitTable = @standardTable
			INNER JOIN dbo.UNTable ut
				ON uu1.UnitTable = ut.UnitTable
        WHERE   uu1.UnitTable <> @standardTable
                AND RIGHT(uu1.Unit, 1) IN ( 'I' )
                AND ut.Company = '01';

        -- ITEM 8c
        UPDATE  uu1
		SET   uu1.SingleLabel = uu2.SingleLabel
			, uu1.PluralLabel = uu2.PluralLabel
			, uu1.Format = uu2.Format
			, uu1.ConsolBill = uu2.ConsolBill
			, uu1.ShowDate = uu2.ShowDate
			, uu1.ShowCalc = uu2.ShowCalc
			, uu1.PostAccount = CASE
                        WHEN uu1.UnitType = 'Labor'
                            AND uu1.BillingRate <> 0 THEN
                            '401.00'
                        WHEN uu1.UnitType <> 'Labor'
                            AND uu1.BillingRate <> 0 THEN
                            '402.00'
                        ELSE
                            NULL
						END
			, uu1.CostRate = uu2.CostRate
			, uu1.RegAccount = uu2.RegAccount
			, uu1.OHAccount = uu1.OHAccount
			, uu1.CreditWBS1 = uu2.CreditWBS1
			, uu1.CreditWBS2 = uu2.CreditWBS2
			, uu1.CreditWBS3 = uu2.CreditWBS3
			, uu1.CreditAccount = uu2.CreditAccount
			, uu1.ConsolCost = uu2.ConsolCost
			, uu1.AvailableForTimesheet = uu2.AvailableForTimesheet
			, uu1.EmployeeSpecificRevenue = uu2.EmployeeSpecificRevenue
			, uu1.Item = uu2.Item
			, uu1.UnitType = uu2.UnitType
		FROM dbo.UNUnit AS uu1
			INNER JOIN dbo.UNUnit AS uu2
				ON uu1.Unit = uu2.Unit
				AND  uu2.UnitTable = @standardTable
			INNER JOIN dbo.UNTable ut
				ON uu1.UnitTable = ut.UnitTable
        WHERE   uu1.UnitTable <> @standardTable
                AND RIGHT(uu1.Unit, 1) NOT IN ( 'I', 'C', 'H', 'M' )
                AND ut.Company = '01';

        -- Dates and Calcs should be shown if Billing Rate > 0, i.e. we are billing it
        -- and thus it will appear on the invoice
        UPDATE  uu1
        SET uu1.ShowDate = 'Y', uu1.ShowCalc = 'Y'
        FROM    dbo.UNUnit AS uu1
        WHERE   uu1.BillingRate <> 0.0
                AND (
                    uu1.ShowDate <> 'Y'
                    OR  uu1.ShowCalc <> 'Y'
                )
                AND uu1.UnitTable <> @standardTable;
		
		-- SET Post Account (revenue) for units with a billing rate set 
        UPDATE  uu1
        SET uu1.PostAccount = IIF(uu1.UnitType = 'Labor', '401.00', '402.00')
        FROM    dbo.UNUnit AS uu1
        WHERE   uu1.BillingRate > 0.0
                AND uu1.UnitTable <> @standardTable;

        -- For non Labor units, charge costs to Reimbursable account if BillingRate <> 0
        -- and the corresponding Direct account otherwise.
        UPDATE  uu1
        SET uu1.RegAccount = IIF(uu1.BillingRate <> 0.0
                                , '5' + RIGHT(uu1.RegAccount, 5)
                                , '6' + RIGHT(uu1.RegAccount, 5))
        FROM    dbo.UNUnit AS uu1
        WHERE   uu1.UnitType <> 'Labor'
                AND uu1.UnitTable <> @standardTable;

    END;
END;
GO
