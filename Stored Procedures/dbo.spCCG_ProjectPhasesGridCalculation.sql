SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_ProjectPhasesGridCalculation] @WBS1 varchar (30)
AS
/*
Copyright 2017 Central Consulting Group.   All rights reserved.
03/10/2017	David Springer
				Call this from an Project Phase INSERT, CHANGE & DELETE workflows.
				Called from spCC_ProjectTaskGridCalculation procedure.
08/23/2021	David Springer
			Revised for Vantagepoint Project
*/
SET NOCOUNT ON
BEGIN
--	Update Project Est. Fee & Standard Fee fields from Phases grid after Phases are up to date
	Update p
	Set p.Revenue = IsNull (pp.Total, 0),
		p.WeightedRevenue = IsNull (pp.Total, 0) * p.Probability / 100,
		p.FeeDirLab =  IsNull (pp.Labor, 0),
		p.FeeDirExp =  IsNull (pp.DirExp, 0),
		p.ConsultFee = IsNull (pp.DirCons, 0),
		p.ReimbAllowExp =  IsNull (pp.ReimbExp, 0),
		p.ReimbAllowCons = IsNull (pp.ReimbCons, 0)
	From PR p
		Left Join
		(Select WBS1, 
			Sum (CustPhaseFeeDirLab) Labor,
			Sum (IIF (CustPhaseBillingType like 'Lump%', CustPhaseExpenseFee, 0)) DirExp,
			Sum (IIF (CustPhaseBillingType like 'Lump%', CustPhaseConsultFee, 0)) DirCons,
			Sum (IIF (CustPhaseBillingType like 'Cost Plus%', CustPhaseReimbAllowExp, 0)) ReimbExp,
			Sum (IIF (CustPhaseBillingType like 'Cost Plus%', CustPhaseReimbAllowCons, 0)) ReimbCons,
			Sum (CustPhaseTotal)  Total
		From Projects_Phases
		Group by WBS1) pp on pp.WBS1 = p.WBS1
	Where p.WBS1 = @WBS1
		and p.WBS2 = ' '

--	Calculate Phase Percent
	Update pp
	Set pp.CustPhasePercent = pp.CustPhaseTotal / p.Revenue * 100
	From Projects_Phases pp, PR p
	Where p.WBS1 = @WBS1
		and p.WBS2 = ' '
		and p.WBS1 = pp.WBS1
		and p.WBS2 = pp.WBS2
		and p.Revenue <> 0

--	If there are more than one Billing Type/Tax Checkbox used in all the Phases
--	Set the Project Billing Type = Sublevel Terms
	If 1 < (Select Count (Distinct CustPhaseBillingType + CustPhaseTax) From Projects_Phases Where WBS1 = @WBS1 and WBS2 = ' ')
		or 'Lump Sum' in (Select distinct CustPhaseBillingType From Projects_Phases Where WBS1 = @WBS1 and WBS2 = ' ') 
		or 'Lump Sum by Task' in (Select distinct CustPhaseBillingType From Projects_Phases Where WBS1 = @WBS1 and WBS2 = ' ')
		or 'Cost Plus Max' in (Select distinct Left (CustPhaseBillingType, 13) From Projects_Phases Where WBS1 = @WBS1 and WBS2 = ' ')
		Begin
		Update ProjectCustomTabFields
		Set CustBillingType = 'Sublevel Terms'
		Where WBS1 = @WBS1
		End

--	If only one Billing Type/Tax Checkbox used in all the Phases
--	Set the Project Billing Type = Phase Billing Type
	If 1 = (Select Count (Distinct CustPhaseBillingType + CustPhaseTax) 
			From Projects_Phases
			Where WBS1 = @WBS1
				and CustPhaseBillingType not like 'Lump Sum%'
				and CustPhaseBillingType not like 'Cost Plus%Max%')
				and not exists (Select 'x' From Projects_Phases
							Where WBS1 = @WBS1
							and (CustPhaseBillingType like 'Lump Sum%'
								or  CustPhaseBillingType like 'Cost Plus%Max%'))
		Begin
		Update px
		Set px.CustBillingType = pp.CustPhaseBillingType
		From ProjectCustomTabFields px, 
			(Select distinct CustPhaseBillingType
			From Projects_Phases
			Where WBS1 = @WBS1
			  and WBS2 = ' ') pp
		Where px.WBS1 = @WBS1
		  and px.WBS2 = ' '
		End
END

GO
