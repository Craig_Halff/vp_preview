SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_EI_CheckPctCompleteChanges] (@WBS1 Nvarchar(30))
AS
BEGIN
	-- Copyright (c) 2019 EleVia Software and Central Consulting Group.  All rights reserved. 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

	select count('x') as ModifiedCount
		from CCG_EI_FeePctCpl pc
		where pc.WBS1 = @WBS1
END
GO
