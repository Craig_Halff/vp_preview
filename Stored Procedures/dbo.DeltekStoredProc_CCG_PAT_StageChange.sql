SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

             CREATE PROCEDURE [dbo].[DeltekStoredProc_CCG_PAT_StageChange] ( @PayableSeq int, @OldStage varchar(70), @NewStage varchar(70), @Username varchar(255), @Role varchar(255))
             AS EXEC spCCG_PAT_StageChange @PayableSeq,@OldStage,@NewStage,@Username,@Role
GO
