SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[stRPUpdateFeeAmt]
  @strRowID nvarchar(255),
  @strColName nvarchar(255),
  @decAmt decimal(19,4)
AS

BEGIN -- Procedure stRPUpdateFeeAmt

  SET NOCOUNT ON
  
  DECLARE @strReportAtBillingInBillingCurr varchar(1)
  DECLARE @strMultiCompanyEnabled varchar(1)
  DECLARE @strMultiCurrencyEnabled varchar(1)
  DECLARE @strCompany nvarchar(14)
  DECLARE @strPlanID varchar(32)
  DECLARE @strTaskID varchar(32)
  DECLARE @strIDPrefix nvarchar(255)
  DECLARE @strUserName nvarchar(32)
  DECLARE @strCostCurrencyCode nvarchar(3)
  DECLARE @strBillingCurrencyCode nvarchar(3)

  DECLARE @siAmtDecimals smallint

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  SELECT @strUserName = LTRIM(RTRIM(REPLACE(dbo.FW_GetUsername(), CHAR(0), N'')))

  -- Get MultiCompanyEnabled, MultiCurrencyEnabled, and RABIBC flags.
  
  SELECT
    @strReportAtBillingInBillingCurr = CASE WHEN MultiCurrencyEnabled = 'N' THEN 'N' ELSE ReportAtBillingInBillingCurr END,
    @strMultiCompanyEnabled = MultiCompanyEnabled,
    @strMultiCurrencyEnabled = MultiCurrencyEnabled 
    FROM FW_CFGSystem

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  -- Parsing for TaskID.

  SET @strTaskID = SUBSTRING(@strRowID, (CHARINDEX('|', @strRowID) + 1), LEN(@strRowID))

  -- Get various parameters.

  SELECT 
    @strPlanID = T.PlanID,
    @strCompany = P.Company,
    @strCostCurrencyCode = P.CostCurrencyCode,
    @strBillingCurrencyCode = CASE WHEN @strReportAtBillingInBillingCurr = 'Y' THEN P.BillingCurrencyCode ELSE P.CostCurrencyCode END
    FROM PNTask AS T
      INNER JOIN PNPlan AS P ON T.PlanID = P.PlanID
    WHERE T.TaskID = @strTaskID

  -- Decimals set basend on the logic hidden inside the stRP$tabPlanDecimals function

  SELECT 
    @siAmtDecimals = 
      CASE 
        WHEN PATINDEX( '%Cost' , @strColName ) > 0 THEN AmtCostDecimals 
        WHEN PATINDEX( '%Bill' , @strColName ) > 0 THEN AmtBillDecimals 
        ELSE 4 
      END
    FROM dbo.stRP$tabPlanDecimals(@strPlanID)

  -- Round input Amount to AmtDecimals settings.

  SET @decAmt = ROUND(@decAmt, @siAmtDecimals)

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  BEGIN TRANSACTION

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    IF (@strColName = 'ContractLaborCost')
      BEGIN

        UPDATE T SET
          CompensationFeeDirLab = @decAmt,
          CompensationFeeDirLabBill = 
            CASE
              WHEN @strBillingCurrencyCode = @strCostCurrencyCode
              THEN @decAmt
              ELSE CompensationFeeDirLabBill
            END,
          ModUser = CASE WHEN DATALENGTH(@strUserName) = 0 THEN N'stRPUpdateFeeAmt' ELSE N'UFA_' + @strUserName END,
          ModDate = LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19)
          FROM PNTask AS T
          WHERE T.PlanID = @strPlanID AND T.TaskID = @strTaskID

      END /* IF (@strColName = 'ContractLaborCost') */

    IF (@strColName = 'ContractLaborBill')
      BEGIN

        UPDATE T SET
          CompensationFeeDirLabBill = @decAmt,
          CompensationFeeDirLab = 
            CASE
              WHEN @strCostCurrencyCode = @strBillingCurrencyCode
              THEN @decAmt
              ELSE CompensationFeeDirLab
            END,
          ModUser = CASE WHEN DATALENGTH(@strUserName) = 0 THEN N'stRPUpdateFeeAmt' ELSE N'UFA_' + @strUserName END,
          ModDate = LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19)
          FROM PNTask AS T
          WHERE T.PlanID = @strPlanID AND T.TaskID = @strTaskID

      END /* IF (@strColName = 'ContractLaborBill') */
 
  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    IF (@strColName = 'ContractDirectExpCost')
      BEGIN

        UPDATE T SET
          CompensationFeeDirExp = @decAmt,
          CompensationFeeDirExpBill = 
            CASE
              WHEN @strBillingCurrencyCode = @strCostCurrencyCode
              THEN @decAmt
              ELSE CompensationFeeDirExpBill
            END,
          ModUser = CASE WHEN DATALENGTH(@strUserName) = 0 THEN N'stRPUpdateFeeAmt' ELSE N'UFA_' + @strUserName END,
          ModDate = LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19)
          FROM PNTask AS T
          WHERE T.PlanID = @strPlanID AND T.TaskID = @strTaskID

      END /* IF (@strColName = 'ContractDirectExpCost') */

    IF (@strColName = 'ContractDirectExpBill')
      BEGIN

        UPDATE T SET
          CompensationFeeDirExpBill = @decAmt,
          CompensationFeeDirExp = 
            CASE
              WHEN @strCostCurrencyCode = @strBillingCurrencyCode
              THEN @decAmt
              ELSE CompensationFeeDirExp
            END,
          ModUser = CASE WHEN DATALENGTH(@strUserName) = 0 THEN N'stRPUpdateFeeAmt' ELSE N'UFA_' + @strUserName END,
          ModDate = LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19)
          FROM PNTask AS T
          WHERE T.PlanID = @strPlanID AND T.TaskID = @strTaskID

      END /* IF (@strColName = 'ContractDirectExpBill') */

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    IF (@strColName = 'ContractReimbExpCost')
      BEGIN

        UPDATE T SET
          ReimbAllowanceExp = @decAmt,
          ReimbAllowanceExpBill = 
            CASE
              WHEN @strBillingCurrencyCode = @strCostCurrencyCode
              THEN @decAmt
              ELSE ReimbAllowanceExpBill
            END,
          ModUser = CASE WHEN DATALENGTH(@strUserName) = 0 THEN N'stRPUpdateFeeAmt' ELSE N'UFA_' + @strUserName END,
          ModDate = LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19)
          FROM PNTask AS T
          WHERE T.PlanID = @strPlanID AND T.TaskID = @strTaskID

      END /* IF (@strColName = 'ContractReimbExpCost') */

    IF (@strColName = 'ContractReimbExpBill')
      BEGIN

        UPDATE T SET
          ReimbAllowanceExpBill = @decAmt,
          ReimbAllowanceExp = 
            CASE
              WHEN @strCostCurrencyCode = @strBillingCurrencyCode
              THEN @decAmt
              ELSE ReimbAllowanceExp
            END,
          ModUser = CASE WHEN DATALENGTH(@strUserName) = 0 THEN N'stRPUpdateFeeAmt' ELSE N'UFA_' + @strUserName END,
          ModDate = LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19)
          FROM PNTask AS T
          WHERE T.PlanID = @strPlanID AND T.TaskID = @strTaskID

      END /* IF (@strColName = 'ContractReimbExpBill') */

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    IF (@strColName = 'ContractDirectConsultCost')
      BEGIN

        UPDATE T SET
          ConsultantFee = @decAmt,
          ConsultantFeeBill = 
            CASE
              WHEN @strBillingCurrencyCode = @strCostCurrencyCode
              THEN @decAmt
              ELSE ConsultantFeeBill
            END,
          ModUser = CASE WHEN DATALENGTH(@strUserName) = 0 THEN N'stRPUpdateFeeAmt' ELSE N'UFA_' + @strUserName END,
          ModDate = LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19)
          FROM PNTask AS T
          WHERE T.PlanID = @strPlanID AND T.TaskID = @strTaskID

      END /* IF (@strColName = 'ContractDirectConsultCost') */

    IF (@strColName = 'ContractDirectConsultBill')
      BEGIN

        UPDATE T SET
          ConsultantFeeBill = @decAmt,
          ConsultantFee = 
            CASE
              WHEN @strCostCurrencyCode = @strBillingCurrencyCode
              THEN @decAmt
              ELSE ConsultantFee
            END,
          ModUser = CASE WHEN DATALENGTH(@strUserName) = 0 THEN N'stRPUpdateFeeAmt' ELSE N'UFA_' + @strUserName END,
          ModDate = LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19)
          FROM PNTask AS T
          WHERE T.PlanID = @strPlanID AND T.TaskID = @strTaskID

      END /* IF (@strColName = 'ContractDirectConsultBill') */

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    IF (@strColName = 'ContractReimbConsultCost')
      BEGIN

        UPDATE T SET
          ReimbAllowanceCon = @decAmt,
          ReimbAllowanceConBill = 
            CASE
              WHEN @strBillingCurrencyCode = @strCostCurrencyCode
              THEN @decAmt
              ELSE ReimbAllowanceConBill
            END,
          ModUser = CASE WHEN DATALENGTH(@strUserName) = 0 THEN N'stRPUpdateFeeAmt' ELSE N'UFA_' + @strUserName END,
          ModDate = LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19)
          FROM PNTask AS T
          WHERE T.PlanID = @strPlanID AND T.TaskID = @strTaskID

      END /* IF (@strColName = 'ContractReimbConsultCost') */

    IF (@strColName = 'ContractReimbConsultBill')
      BEGIN

        UPDATE T SET
          ReimbAllowanceConBill = @decAmt,
          ReimbAllowanceCon = 
            CASE
              WHEN @strCostCurrencyCode = @strBillingCurrencyCode
              THEN @decAmt
              ELSE ReimbAllowanceCon
            END,
          ModUser = CASE WHEN DATALENGTH(@strUserName) = 0 THEN N'stRPUpdateFeeAmt' ELSE N'UFA_' + @strUserName END,
          ModDate = LEFT(CONVERT(VARCHAR, GETUTCDATE(), 121), 19)
          FROM PNTask AS T
          WHERE T.PlanID = @strPlanID AND T.TaskID = @strTaskID

      END /* IF (@strColName = 'ContractReimbConsultBill') */

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Sum up Fees.

    EXECUTE dbo.stRPSumUpFees @strPlanID

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    -- Update VesionID.

    EXECUTE dbo.stRPUpdateVersionID @strPlanID, 1 

    -- Set LastPlanAction.

    UPDATE PNPlan SET LastPlanAction = 'SAVED' WHERE PlanID = @strPlanID

  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  COMMIT

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     
  SET NOCOUNT OFF

END -- stRPUpdateFeeAmt
GO
