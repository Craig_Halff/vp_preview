SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[spCCG_PAT_CheckNotifications] (
	@Employee				Nvarchar(20),	-- Employee from employee-based scheduled workflow
	@OverrideSubject		Nvarchar(120),	-- If length > 0, then this becomes the email subject
	@OverrideSenderEmail	Nvarchar(100),	-- Can set to an email address to override default
	@MsgPriority			varchar(10) = NULL,	/* Optional: starts with L for Low or H for High else normal		*/
	@IncludeSupervisorAge	int = -1,			/* -1=never, 0=always, X=if action is X days old (5.0)				*/
	@InclWeekends			char(1) = 'Y',		/* Y = include weekends and holidays in count, N = exclude them		*/
	@RoundingHoursCutoff	int = 22			/* Number of hours at which interval rounds up to count as full day	*/
)
--WITH ENCRYPTION
AS BEGIN
/*
	Copyright (c) 2020 EleVia Software. All rights reserved.
	03/13/2012 JRK Initial Version
	03/17/2017 JAM Send notifications to delegates as well
	12/28/2018 JAM MsgPriority, IncludeSupervisorAge, InclWeekends, RoundingHoursCutoff params added for more control

 	EXEC spCCG_PAT_CheckNotifications '00009', 'Test Re-Notification', '', 'H', 0, 'N', 22
	select * from CCG_Email_Messages
	select * from CCG_PAT_Renotify
	delete from CCG_Email_Messages
	delete from CCG_PAT_Renotify
	update CCG_PAT_Pending set DateLastNotificationSent = null
	update CCG_PAT_Config set NotificationInterval = 1
*/
	set nocount on
	BEGIN TRY
		declare @Username					Nvarchar(32)
		declare @NotificationIntervalDays	int
		declare @EmailLink					Nvarchar(1024)		-- 5.0 sps
		declare @EncTimeout					int						-- 5.0 sps
		declare @LastNotificationSent		datetime				-- Date last notification was sent for this project/stage
		declare @ChangedBy					Nvarchar(32)		-- Employee who last changed the stage
		declare @ChangedByEmail				Nvarchar(50)		-- Sizeof EM.Email
		declare @DaysSinceLastNotification	int
		declare @StageFlow					Nvarchar(50)
		declare @Stage						varchar(50)
		declare @Email						Nvarchar(255)
		declare @SupEmail					Nvarchar(255)
		declare @Language					varchar(10)
		declare @delegation					varchar(1)
		declare @Subject					Nvarchar(120)
		declare @EmailSubjectBatch			Nvarchar(120)
		declare @Body						Nvarchar(max)
		declare @Field						Nvarchar(100)
		declare @WHERE						Nvarchar(1000)
		declare @STATUS_JOIN				Nvarchar(255)
		declare @STATUS_WHERE				Nvarchar(255)
		declare @ApprovalRequired			char(1)
		declare @EmpCompany					varchar(14) = ''

		select @EmailLink=EmailLink, @EncTimeout=EncTimeout, @ApprovalRequired = DelegationApproval, @delegation = ISNULL(Delegation,'Y'), @NotificationIntervalDays = NotificationInterval
			from CCG_PAT_Config

		--print 'Interval: ' + Convert(Nvarchar(10),@NotificationIntervalHours)
		if IsNull(@NotificationIntervalDays, 0) <= 0 return			-- No notification interval set

		/* 5.0 sps */
		if @EmailLink <> N''
		begin
			select @Username=IsNull(Min(Username),'') from SEUser where Employee=@Employee and DisableLogin='N'

			-- Determine if this is a Cloud deployment or non-Cloud (look for database connection info)
			if CHARINDEX(N'database',@EmailLink) > 0 or CHARINDEX(N'dbup',@EmailLink) > 0						-- not Cloud
				set @EmailLink = REPLACE(@EmailLink, N'[:USERENC]', N'userenc=' + dbo.fnCCG_PAT_GetE(@Username))
			else
				set @EmailLink = REPLACE(@EmailLink, N'[:USERENC]', N'user=' + dbo.fnCCG_HexScrambleCode(@Username))
			set @EmailLink = REPLACE(@EmailLink, N'[:USER]', N'user=' + dbo.fnCCG_HexScrambleCode(@Username))
			set @EmailLink = REPLACE(@EmailLink, N'[:EDENC]',   N'edenc='   + dbo.fnCCG_PAT_GetE(Convert(Nvarchar(20), DATEADD(d, @EncTimeout, getdate()),101)))

			print @EmailLink
		end

		if isnull(@InclWeekends, 'Y') = 'N'
		begin
			if exists (select 'x' from CFGSystem where MulticompanyEnabled = 'Y')
			begin
				declare @sql2 nvarchar(max) = 'select @homeCompany = HomeCompany from EM where Employee = '''+@Employee+''''
				EXECUTE SP_EXECUTESQL @sql2, N'@homeCompany varchar(14) OUTPUT', @homeCompany = @EmpCompany OUTPUT
			end
			if DATEPART(dw, getdate()) in (1, 7) return																-- Weekend
			if exists (select 'x' from CFGHoliday
				Where Company = @EmpCompany and HolidayDate = DateAdd(dd, DateDiff(dd, 0, getdate()), 0)) return	-- Holiday
		end

		-- Get Employee / supervisor email address
		select @Email = Email, @Language = [Language] from EM where Employee = @Employee
		select @SupEmail = Sup.Email from EM inner join EM Sup on Sup.Employee = EM.Supervisor where EM.Employee = @Employee
		--print 'Email: ' + @Email
		if Len(ISNULL(@Email,'')) < 1 return					-- Employee has no email address
		if Len(ISNULL(@Language,'')) < 1 begin
			Print 'Employee has no Language'
			set @Language = 'en-US'
		end

		declare @GUID uniqueidentifier, @nowutc datetime, @sql nvarchar(max) = N'', @sqlCC nvarchar(500)
		declare @daysCalculation nvarchar(1000) = (case
			when isnull(@InclWeekends, 'Y') <> 'N' then '(cast(datediff(minute, IsNull(Pending.DateLastNotificationSent, ''1/1/2000''), getutcdate()) as decimal(19,2)) / 60.0 - ('+cast(@RoundingHoursCutoff as varchar(5))+' - 12.0)) / 24.0 '
			else '(cast(dbo.fnCCG_EI_GetBusinessDays(IsNull(Pending.DateLastNotificationSent, ''1/1/2000''), getutcdate(), '''+@EmpCompany+''') as decimal(19,2)) / 60.0 - ('+cast(@RoundingHoursCutoff as varchar(5))+' - 12.0)) / 24.0' end)
		set @nowutc=GETUTCDATE()
		set @GUID = NEWID()

		--print @GUID
		set @sql += N'
			INSERT INTO CCG_PAT_Renotify (ID, InsertDate, Employee, PendingSeq,PayableSeq, ItemDesc,Stage, DateChanged, ChangedBy,
				DateLastNotificationSent, EmailSubjectBatch, EmailSubject, EmailMessage)
			SELECT ''' + CAST(@GUID as nvarchar(36)) + ''', ''' + Cast(@nowutc as nvarchar) + ''', EM.Employee,
				Pending.Seq,Pending.PayableSeq, VE.Name + '' - '' + PAT.PayableNumber,stages.Stage, PAT.ModDate, PAT.ModUser, Pending.DateLastNotificationSent,
				ISNULL(emailLang.EmailSubjectBatch, stages.EmailSubjectBatch), ISNULL(emailLang.EmailSubject, stages.EmailSubject),
				ISNULL(emailLang.EmailMessage, stages.EmailMessage)
			FROM CCG_PAT_Pending pending
				INNER JOIN (select PayableSeq,MIN(SortOrder) as nextRouteOrder from CCG_PAT_Pending group by PayableSeq) nextRoute on nextRoute.PayableSeq = pending.PayableSeq
					and nextRoute.nextRouteOrder = pending.SortOrder
				INNER JOIN CCG_PAT_Payable PAT on PAT.Seq = Pending.PayableSeq
				INNER JOIN EM on EM.Employee = pending.Employee
				INNER JOIN VE on VE.Vendor = PAT.Vendor
				INNER JOIN CCG_PAT_ConfigStages stages on stages.stage = pending.stage or (ISNULL(pending.stage, '''') = '''' and PAT.Stage = stages.Stage)
				LEFT JOIN CCG_PAT_ConfigStagesDescriptions emailLang on stages.stage = emailLang.stage and emailLang.UICultureName ='''+ @Language +'''
				LEFT JOIN CCG_PAT_Delegation d ON d.Employee = pending.Employee
					AND (isnull(d.ApprovedBy,'''') <> '''' or '''+@ApprovalRequired+''' = ''N'')
					AND GETDATE() BETWEEN d.FromDate AND d.ToDate ' + N'
			WHERE ((pending.Employee = N'''+@Employee+''' AND (d.Employee IS NULL OR d.Dual = ''Y'')) OR d.Delegate = N'''+@Employee+''')
				and ISNULL(emailLang.EmailSubject, ISNULL(stages.EmailSubject, '''')) <> ''''
				and (ceiling(round('+@daysCalculation+', 0)) >= ' + Cast(@NotificationIntervalDays as nvarchar) +
					/* Send ANOTHER notification for this WBS1 for another employee (delegate or delegator) if an automated notification has been sent within the last 3 hours (=during the same scheduled workflow session) [JAM] */
					+ (case when @delegation = 'Y' then ' or (d.Dual = ''Y'' and datediff(mi, PAT.ModDate, Pending.DateLastNotificationSent) > 2 '   /* must not be right after sending the initial email on stage change */
						+ 'and datediff(hh, Pending.DateLastNotificationSent, getutcdate()) <= 3) ' else '' end) + '
				) '
			--+
			--@STATUS_WHERE + @WHERE
		print @sql
		exec sp_executesql @sql
--  	EXEC spCCG_PAT_CheckNotifications '00003', '', '', ''

		--if @IncludeSupervisorAge > -1
		--	set @sqlCC = 'case when CCSender = ''Y'' then Sender.Email + '';'' else '''' end'
		--else
		--	set @sqlCC = 'case when CCSender = ''Y'' then Sender.Email else '''' end'
		set @sqlCC = ''''''

		/* Check if we're to include the employee's supervisor on the email: */
		if IsNull(@SupEmail,'') <> '' and @IncludeSupervisorAge = 0	/* always */
			set @sqlCC = @sqlCC + ' + ''' + @SupEmail + ''''
		else if IsNull(@SupEmail,'') <> '' and @IncludeSupervisorAge > 0 /* only if past due ages exceeds this many days */
			set @sqlCC = @sqlCC + ' + case When DATEDIFF(d, DateChanged, GETUTCDATE()) >= ' + Cast(@IncludeSupervisorAge as varchar) +
				' Then ''' + @SupEmail + ''' Else '''' End'

		set @sql = N''
		set @sql += N'
			INSERT INTO CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, BCCList, GroupSubject, [Subject], Body, Priority)
			SELECT ''Payables Approval Tracking'', Case When Len(''' + @OverrideSenderEmail + ''')>1 Then N''' + @OverrideSenderEmail + ''' Else ISNULL(SenderU.Email,Sender.Email) End,
				N''' + @Email + ''', ' + @sqlCC + ', null,
				Case When Len(''' + @OverrideSubject + ''')>1 Then N''' + @OverrideSubject + ''' Else EmailSubjectBatch + '' [re-notification]'' End,
				left(Replace(EmailSubject, ''[:ItemDesc]'', ItemDesc), 102) + '' [re-notification]'',
				Replace(Replace(Replace(EmailMessage, ''[:ItemDesc]'', ItemDesc),''[:LINK]'', N''' + @EmailLink + ''') ' + N'
					, ''[:RENOTIFYDAYS]'', '' ['' + case when DateLastNotificationSent is null then '''' else Cast(DATEDIFF(d, IsNull(DateLastNotificationSent,''1/1/2000''), getutcdate()) as Nvarchar)
						+ '' day '' end + ''re-notification]''),
				case when LEFT(UPPER(''' + isnull(@MsgPriority,'NULL') + '''),1)=''N'' then null else ''' + isnull(@MsgPriority,'NULL') + ''' end
			FROM CCG_PAT_Renotify
				LEFT JOIN EM Sender on Sender.Employee=CCG_PAT_Renotify.ChangedBy
				LEFT JOIN SEUSER on SEUSER.Username = CCG_PAT_Renotify.ChangedBy
				LEFT JOIN EM SenderU on SenderU.Employee = SEUSER.Employee
			WHERE ID = ''' + CAST(@GUID as Nvarchar(36)) + ''''
		print @sql
		exec sp_executesql @sql

		---- Add history entry:
		set @sql = N'
			INSERT INTO CCG_PAT_History (ActionTaken, ActionDate, ActionTakenBy, ActionRecipient, PayableSeq, Stage, DelegateFor)
			SELECT N''Email Reminder Sent'', ''' + Cast(@nowutc as nvarchar) + ''',
				''Re-Notification Script'', N''' + @Employee + ''', PayableSeq, Stage,
				CASE WHEN Employee != N'''+@Employee+''' THEN Employee ELSE NULL END
			FROM CCG_PAT_Renotify
			WHERE ID = ''' + CAST(@GUID as nvarchar(36)) + ''''
		print @sql
		exec sp_executesql @sql

		---- Reset DateLastNotificationDate in Pending table:
		set @sql = N'
			UPDATE pending SET DateLastNotificationSent = getutcdate()
				FROM CCG_PAT_Pending pending
					INNER JOIN CCG_PAT_Renotify r on r.PendingSeq = pending.Seq
					LEFT JOIN CCG_PAT_Delegation d ON d.Employee = pending.Employee
						AND ISNULL(d.ApprovedBy,'''') <> ''''
						AND GETDATE() BETWEEN d.FromDate AND d.ToDate
				WHERE r.ID = ''' + CAST(@GUID as nvarchar(36)) + '''
					AND ((pending.Employee = N'''+@Employee+''' AND d.Employee IS NULL) OR d.Delegate = N'''+@Employee+''')'
		print @sql
		exec sp_executesql @sql

		---- Reset DateLastNotificationDate in Payable table:
		set @sql = N'
			UPDATE CCG_PAT_Payable SET DateLastNotificationSent = getutcdate()
				FROM CCG_PAT_Payable
					INNER JOIN CCG_PAT_Renotify r on r.PayableSeq = CCG_PAT_Payable.Seq
				WHERE ID = ''' + CAST(@GUID as nvarchar(36)) + ''''
		print @sql
		exec sp_executesql @sql

		return 0
	END TRY
	BEGIN CATCH
		declare @res int
		set @res = ERROR_NUMBER()
		return @res
		-- SQL found some error - return the message and number:
		--select -1 as Result, IsNull(ERROR_MESSAGE(), 'Unknown error') + ' (' + Cast(IsNull(ERROR_NUMBER(),-1) as Nvarchar(20)) + ')' as ErrorMessage
	END CATCH
END
GO
