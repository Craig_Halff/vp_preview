SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCCG_PAT_CallGetPOData] (
	@SPName			varchar(500),
	@SelectType		varchar(20),
	@SelectParam1	varchar(255),
	@SelectParam2	varchar(255),
	@OutputType		varchar(20),
	@User			varchar(50) = null,
	@BatchLoadSeq	varchar(10) = null
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	if isnull(@SPName, '') = '' set @SPName = 'spCCG_PAT_GetPOData'
	else if left(@SPName, 5) <> 'spCCG' and left(@SPName, 16) <> 'DeltekStoredProc' set @SPName = 'spCCG_PAT_' + @SPName

	-- Look this procedure up to find if it has the @BatchLoadSeq param
	declare @spType int = (SELECT max(case when lower(p.name) = '@batchloadseq' then 2 else 1 end)			-- Look for optional parameter
		FROM sys.objects o
			inner join sys.parameters p on p.object_id = o.object_id
		WHERE o.object_id = OBJECT_ID(N'[dbo].['+@SPName+']') AND type in ('P', 'PC'))

	IF @spType is not null
	BEGIN
		if @spType = 1 exec @SPName @SelectType, @SelectParam1, @SelectParam2, @OutputType, @User			-- no @BatchLoadSeq parameter
		else exec @SPName @SelectType, @SelectParam1, @SelectParam2, @OutputType, @User, @BatchLoadSeq		-- has @BatchLoadSeq parameter
	END
	ELSE select -1 as Result, 'Stored procedure does not exist' as ErrorMessage
END
GO
