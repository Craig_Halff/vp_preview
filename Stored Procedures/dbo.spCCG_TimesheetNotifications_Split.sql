SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[spCCG_TimesheetNotifications_Split] @AltApprover varchar (1), @OpsMgr varchar (1), @MgmtLeader varchar (1)
AS
/*
Copyright 2017 (c) Central Consulting Group.   All rights reserved.
08/31/2017 David Springer
           Notify employees and management of unsubmitted and unapproved timesheets
		   @AltApprover = Y or N to determine whether to include cc
*/
SET NOCOUNT ON
DECLARE @EndDate          date,
		@TKGroup          varchar (255),
		@EKApprover       varchar (8000),
		@EmailTo          varchar (200),
        @EmailApprover    varchar (200),
        @EmailAltApprover varchar (200),
		@EmailOpsMgr      varchar (200),
		@EmailMgmtLeader  varchar (200),
		@Employee	      varchar (255),
		@Company          varchar (10),
		@CompanyFrom      varchar (255),
		@Status           varchar (255),
		@ExpectedHours    float,
		@Hours            float,
		@StatusOrder	  int,
		@CSS              varchar (max),
        @Body             varchar (max),
		@Class			  varchar (100),
		@Loop1            integer,
		@numRows		  integer,		
		@numRowsApproved  integer,		
		@Subject		  varchar(255),
		@CCList			  varchar(255)

BEGIN
	-- Set the appropriate subject line
	If @AltApprover = 'N' AND @OpsMgr = 'N'
		Set @Subject = 'Reminder to Review and Approve Timesheets (12/31)'
	Else
		Set @Subject = 'Timesheet Review and Approval Past Due (12/31)'

	-- CSS style rules
	Set @CSS = 
'
<style type="text/css">
	p {padding-top:15px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;}
	td, th {border: 1px solid black;padding: 5px;font-family:''Trebuchet MS'', ''Lucida Sans Unicode'', ''Lucida Grande'', ''Lucida Sans'', Arial, sans-serif;}
	th {background-color: #6E6E6E;color: white;text-align:center;}
	.c1 {text-align:left;}
	.c2 {text-align:center;}
	.c3, .c4 {text-align:right;}
	.red {color:#FF0000;}
	.blue {color:#0000FF;}
	.black {color:#000000;}
</style>
'

	-- Table variable to hold query results
	DECLARE @EmpList table 
	( 
		Employee varchar(255) NOT NULL, 
		Company varchar(10), 
		[Status] varchar(255), 
		ExpectedHours float, 
		[Hours] float,
		StatusOrder int
	)

	-- Determine timesheet end date for processing records
	Select @EndDate = EndDate From tkControl Where Cast(DateAdd(dd, -6, GetDate()) as date) between StartDate and EndDate and Company = '01'

	DECLARE curEKApprover insensitive cursor for
		Select distinct a1.TKGroup + ' - ' + IsNull (a1.PreferredName, a1.FirstName) + ' ' + IsNull (a1.LastName, '') TKGroup, IsNull (ex.CustEkApprover, ''), IsNull (a1.Email, '') EmailApprover, IsNull (a2.Email, '') EmailAltApprover, IsNull (a3.Email, '') EmailOpsMgr, IsNull (a5.Email, '') EmailMgmtLeader
		From EMAllCompany a1, EM e, EmployeeCustomTabFields ex
			Left Join EMMain a2 on a2.Employee = ex.CustEKAlternateApprover
			Left Join EMMain a3 on a3.Employee = ex.CustEKOperationsManager
			Left Join EMMain a4 on a4.Employee = ex.CustEKDirector
			Left Join EMMain a5 on a5.Employee = ex.CustEKManagementLeader
		Where e.Employee = ex.Employee
			--and e.Org not like '02%'
			and Left(e.Org,2) not in ('02')
			and e.Status = 'A'
			and ex.CustEKApprover = a1.Employee
			and ex.CustEKAlternateApprover = a2.Employee
			and 
			(
				not exists (Select 'x' From tkMaster Where Employee = e.Employee and EndDate = @EndDate)
				or
				exists (Select 'x' From tkMaster Where Employee = e.Employee and EndDate = @EndDate and Submitted in ('N', 'Y')) -- In progress & Submitted
			)

	-- Get each EKApprover
	OPEN curEKApprover
	FETCH NEXT FROM curEKApprover INTO @TKGroup, @EKApprover, @EmailApprover, @EmailAltApprover, @EmailOpsMgr, @EmailMgmtLeader
	Set @Loop1 = @@FETCH_STATUS
	While @Loop1 = 0
	BEGIN
			-- Clear table
			Delete From @EmpList

			-- Insert results
			Insert Into @EmpList
			Select IsNull (e.PreferredName, e.FirstName) + ' ' + IsNull (e.LastName, '') Employee, 
			IsNull (e.EmployeeCompany, '') EmployeeCompany,
			CASE When t.Submitted = 'N' Then 'In Progress'
					When t.Submitted = 'Y' Then 'Submitted'
					When t.Submitted = 'A' Then 'Approved'
					When t.Submitted = 'P' Then 'Processed' -- not needed here but for completeness
					Else 'Missing'
			END Status, 
			IsNull (e.HoursPerDay, 0) * 2 ExpectedHours, 
			IsNull (d.Hrs, 0.0000) Hrs,
			CASE When t.Submitted = 'N' Then 1
					When t.Submitted = 'Y' Then 2
					When t.Submitted = 'A' Then 3
					Else 0
			END StatusOrder 
			From EmployeeCustomTabFields ex, EM e
				Left Join tkMaster t on t.Employee = e.Employee and t.EndDate = @EndDate
				Left Join
				(Select Employee, EndDate, sum (RegHrs + OvtHrs + SpecialOvtHrs) Hrs
				From tkDetail
				Group by Employee, EndDate) d on d.Employee = e.Employee and d.EndDate = t.EndDate
			Where e.Employee = ex.Employee
				and e.[Status]='A'														-- active employees
				and (CustTrueHireDate Is Null Or CustTrueHireDate < @EndDate)			-- who have been here prior to this period end date
				and (CustUnpaidLeaveEnd Is Null Or CustUnpaidLeaveEnd > @EndDate)		-- and who if returning from leave has been here since this end date
				and ex.CustEKApprover = @EKApprover										-- this group/team
				and IsNull (t.Submitted, 'N') <> 'P'									-- exclude processed
				and IsNull (t.Submitted, 'N') <> CASE When @MgmtLeader = 'Y' Then 'A' Else '' END 
		
			Select @numRows = Count(*) From @EmpList										-- total number of results
			Select @numRowsApproved = Count(*) From @EmpList Where [Status] = 'Approved'	-- number of 'Approved'

			If (@numRows > 0							-- At least 1 result
				And @numRowsApproved <> @numRows)		-- not all results are 'Approved'
				Begin
			
					Declare curEmps insensitive cursor for 
						Select Employee, Company, [Status], ExpectedHours, [Hours], StatusOrder 
						From @EmpList 
						Order By StatusOrder, Employee
	
					-- Open the table with headers
					Set @Body = 
'<p><strong>' + IsNull(@TKGroup, '') + '</strong></p> 
<table cellpadding=''5'' cellspacing=''0'' style=''border:1px solid black;border-collapse:collapse;''>
	<thead>
		<tr>
			<th>Employee</th>
			<th>Status</th>
			<th>Expected<br>Hours</th>
			<th>Hours<br>Entered</th>
		</tr>
	</thead>
	<tbody>
'

					Open curEmps
					Fetch next from curEmps Into @Employee, @Company, @Status, @ExpectedHours, @Hours, @StatusOrder
					While @@FETCH_STATUS = 0
					Begin

						-- Set the class of the row
						If (@Status = 'Approved')
							Set @Class = ' class="black"' 
						Else If (@Status = 'Submitted')
							Set @Class=' class="blue"'
						Else 
							Set @Class = ' class="red"' 

						Set @Body = @Body + 
'		<tr' + @Class + '> 
			<td class="c1">' + IsNull (@Employee, '') + '</td> 
			<td class="c2">' + IsNull (@Status, '') + '</td> 
			<td class="c3">' + convert (varchar, convert (decimal(8,2), IsNull (@ExpectedHours, 0))) + '</td> 
			<td class="c4">' + convert (varchar, convert (decimal(8,2), IsNull (@Hours, 0))) + '</td> 
		</tr>
'
						Fetch next from curEmps Into @Employee, @Company, @Status, @ExpectedHours, @Hours, @StatusOrder
					End
					Close curEmps
					Deallocate curEmps

					-- Close the table
					Set @Body = @Body + 
'	</tbody>
</table>
'
					Set @CompanyFrom = ''
					Set @CCList = ''

					If @Company in ('01','05') Set @CompanyFrom = 'cmoney@halff.com'
					Else If @Company in ('03','04') Set @CompanyFrom = 'cmoney@halff.com'
			
					If IsNull (@AltApprover, '') <> 'Y' Set @EmailAltApprover = ''
					If IsNull (@OpsMgr, '') <> 'Y' Set @EmailOpsMgr = ''
					If IsNull (@MgmtLeader, '') <> 'Y' Set @EmailMgmtLeader = ''

					-- Payroll
					If IsNull (@OpsMgr, '') = 'Y' 
						Begin
							Select @EmailOpsMgr = STUFF(( Select ';' + email
							From
							(
								Select @EmailOpsMgr As email
								Union
								Select @CompanyFrom As email
							) As A
							Where A.email <> ''
							For
								XML PATH('')
							), 1, 1, '') 
						End

					-- To Email
					If IsNull (@MgmtLeader, '') = 'Y' 
						Begin
							Set @EmailTo = IsNull (@EmailOpsMgr, '')
							Set @EmailApprover = ''
							Set @EmailAltApprover = ''
							Set @EmailOpsMgr = ''
						End
					Else
						Set @EmailTo = IsNull (@EmailApprover, '')

					-- Build the CCList from non-empty values
					Select @CCList = Stuff(( Select ';' + email
					From
					(
						Select @EmailAltApprover As email
						Union
						Select @EmailOpsMgr As email
						Union
						Select @EmailMgmtLeader As email
					) As A
					Where A.email <> ''
					For
						XML PATH('')
					), 1, 1, '') 

					Insert Into CCG_Email_Messages (SourceApplication, Sender, ToList, CCList, [Subject], Body, CreateDateTime, MaxDelay)
					Values ('Timesheet Notification', IsNull(@CompanyFrom, ''), IsNull(@EmailTo, ''), IsNull(@CCList, ''), @Subject, @CSS + @Body, getDate(), 1)
		--			Values ('Timesheet Notification', IsNull(@CompanyFrom, ''), IsNull('jsagel@halff.com', ''), IsNull('jsagel@halff.com', ''), @Subject, @CSS + @Body, getDate(), 1)
			End

		FETCH NEXT FROM curEKApprover INTO @TKGroup, @EKApprover,  @EmailApprover, @EmailAltApprover, @EmailOpsMgr, @EmailMgmtLeader
		Set @Loop1 = @@FETCH_STATUS

	END
	CLOSE curEKApprover
	DEALLOCATE curEKApprover

END


GO
